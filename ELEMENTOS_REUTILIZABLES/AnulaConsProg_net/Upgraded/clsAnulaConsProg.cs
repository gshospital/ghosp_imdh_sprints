using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace AnulaConsProg
{
	public class clsAnulaConsProg
	{
		public string vNomAplicacion = String.Empty;
		public string vAyuda = String.Empty;

		//Private instancia As frmAnulaConsProg

		public bool AnulacionConsProg(int a�oRegi, int numeroRegi, ref SqlConnection Conexion, int operacion, ref string motivo)
		{

			//Set instancia = New frmAnulaConsProg
			Serrores.oClass = new Mensajes.ClassMensajes();
			vNomAplicacion = Serrores.ObtenerNombreAplicacion(Conexion);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			vAyuda = Serrores.ObtenerFicheroAyuda();

            CDAyuda.Instance.setHelpFile(frmAnulaConsProg.DefInstance, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

			frmAnulaConsProg.DefInstance.cargaValores(a�oRegi, numeroRegi, Conexion, operacion);

			frmAnulaConsProg.DefInstance.ShowDialog();

			if (frmAnulaConsProg.DefInstance.stMotivo.Trim() != "")
			{
				motivo = frmAnulaConsProg.DefInstance.stMotivo;
				return frmAnulaConsProg.DefInstance.blnRetorno;
			}
			else
			{
				return false;
			}
		}
	}
}