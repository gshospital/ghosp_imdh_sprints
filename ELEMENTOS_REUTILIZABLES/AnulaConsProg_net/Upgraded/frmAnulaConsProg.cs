using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;
using ElementosCompartidos;
using Telerik.WinControls.UI;

namespace AnulaConsProg
{
	public partial class frmAnulaConsProg
		: Telerik.WinControls.UI.RadForm
	{
		public bool blnRetorno = false;
		public int inta�oRegi = 0;
		public int lngNumeroRegi = 0;
		public SqlConnection rdConexion = null;
		public string stMotivo = String.Empty;
		public int intOperacion = 0;
		public frmAnulaConsProg()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}

		public void cargaValores(int a�oRegi, int numeroRegi, SqlConnection Conexion, int operacion)
		{
			inta�oRegi = a�oRegi;
			lngNumeroRegi = numeroRegi;
			rdConexion = Conexion;
			intOperacion = operacion;
		}

		private void cbbAnulaciones_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			cmbAceptar.Enabled = true;
		}

		private void cmbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string stSql = String.Empty;
			DataSet rdoTemp = null;            
			stMotivo = Convert.ToString(cbbAnulaciones.SelectedValue);

			//Si estamos anulando intervenciones o consultas.
			//    If intOperacion = 2 Or intOperacion = 0 Then
			//        stSql = "select count(*) numero from amosaleq where gmosaleq in (SELECT isnull(gmosaleq,'-1')  FROM CMANUCIT where gmanucit='" & stMotivo & "')"
			//        Set rdoTemp = rdConexion.OpenResultset(stSql, 1, 1)
			//        If rdoTemp("numero") = 0 Then stMotivo = stMotivo & "@"
			//    End If

			blnRetorno = true;
			this.Close();
		}

		private void cmbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			blnRetorno = false;
			this.Close();
		}
		
		private void frmAnulaConsProg_Load(Object eventSender, EventArgs eventArgs)
		{
			try
			{
                CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
				this.Refresh();

				cmbAceptar.Enabled = false;
				cmbCancelar.Enabled = true;

				if (intOperacion == 0)
				{
					this.Text = this.Text + " consultas.";
					frmConsultas.Enabled = true;
					frmConsultas.Visible = true;
					frmPrograma.Enabled = false;
					frmPrograma.Visible = false;
					frmIntervencion.Enabled = false;
					frmIntervencion.Visible = false;
					cargaConsulta();
				}
				else if (intOperacion == 1)
				{ 
					this.Text = this.Text + " programaciones.";
					frmConsultas.Enabled = false;
					frmConsultas.Visible = false;
					frmPrograma.Enabled = true;
					frmPrograma.Visible = true;
					frmIntervencion.Enabled = false;
					frmIntervencion.Visible = false;
					cargaProg();
				}
				else if (intOperacion == 2)
				{ 
					this.Text = this.Text + " intervenciones.";
					frmConsultas.Enabled = false;
					frmConsultas.Visible = false;
					frmPrograma.Enabled = false;
					frmPrograma.Visible = false;
					frmIntervencion.Enabled = true;
					frmIntervencion.Visible = true;
					cargaInter();
				}
				else if (intOperacion == 3)
				{ 
					this.Text = this.Text + " solicitud.";
					frmIntervencion.Text = "Solicitud";
					frmConsultas.Enabled = false;
					frmConsultas.Visible = false;
					frmPrograma.Enabled = false;
					frmPrograma.Visible = false;
					frmIntervencion.Enabled = true;
					frmIntervencion.Visible = true;
					cargaSoli();
				}

				if (!cargaCombo())
				{
					//'    blnRetorno = False
					//'    Unload Me
				}
			}
			catch (System.Exception excep)
			{
				RadMessageBox.Show(excep.Message, "Error: Anulaci�n", MessageBoxButtons.OK, RadMessageIcon.Error);
			}

		}

		private bool cargaCombo()
		{
			bool ErrCargaCombo = false;
			bool result = false;
			string stSql = String.Empty;
			DataSet rdoTemp = null;
			cbbAnulaciones.Items.Clear();

			try
			{
				ErrCargaCombo = true;
				stSql = "SELECT gmanucit, dmanucit  FROM CMANUCIT WHERE gmosaleq is not null and (fborrado is null or fborrado >= GETDATE()) AND ";

				if (intOperacion == 0)
				{
					stSql = stSql + "ianulcon = 'S' ";
				}
				else if (intOperacion == 1)
				{ 
					stSql = stSql + "ianulpro = 'S' ";
				}
				else if (intOperacion == 2)
				{ 
					stSql = stSql + "ianulint = 'S' ";
				}
				else if (intOperacion == 3)
				{ 
					stSql = stSql + "ianusol = 'S' ";
				}
				stSql = stSql + "order by dmanucit ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rdConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					//camaya
                    //UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
					rdoTemp.MoveFirst();
					for (int contador = 0; contador <= rdoTemp.Tables[0].Rows.Count - 1; contador++)
					{						
						cbbAnulaciones.Items.Add(new RadListDataItem(Convert.ToString(rdoTemp.Tables[0].Rows[contador]["dmanucit"]), Convert.ToInt32(rdoTemp.Tables[0].Rows[contador]["gmanucit"])));						
						//UPGRADE_ISSUE: (1040) MoveNext function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
						rdoTemp.MoveNext();

					}
					result = true;
				}
				else
				{
					short tempRefParam = 1520;
					string[] tempRefParam2 = new string []{ "motivos", "realizar la anulaci�n"};
					Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam,rdConexion, tempRefParam2);
					result = false;
				}
				
				rdoTemp.Close();

				ErrCargaCombo = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!ErrCargaCombo)
				{
					throw excep;
				}

				if (ErrCargaCombo)
				{
					RadMessageBox.Show(excep.Message, "Error: CargaCombo.", MessageBoxButtons.OK, RadMessageIcon.Error);
					return false;
				}
			}
			return false;
		}

		private void cargaConsulta()
		{
			bool ErrCargaConsulta = false;
			string stSql = String.Empty;
			DataSet rdoTemp = null;

			try
			{
				ErrCargaConsulta = true;
				stSql = "SELECT CCONSULT.ffeccita, DSERVICI.dnomserv, DCODPRES.dprestac, DPERSONA.dap1pers, " + 
				        "ISNULL(DPERSONA.dap2pers,'') as 'dap2pers', DPERSONA.dnompers " + 
				        "From CCONSULT " + 
				        "INNER JOIN DSERVICI ON CCONSULT.gservici = DSERVICI.gservici " + 
				        "INNER JOIN DCODPRES ON CCONSULT.gprestac = DCODPRES.gprestac " + 
				        "INNER JOIN DPERSONA ON CCONSULT.gpersona = DPERSONA.gpersona " + 
				        "Where (CCONSULT.ganoregi = " + Conversion.Str(inta�oRegi).Trim() + ") And (CCONSULT.gnumregi = " + Conversion.Str(lngNumeroRegi).Trim() + ")";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rdConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{					
					lbFechaCita[1].Text = Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["ffeccita"]).ToString("dd/MM/yyyy HH:mm");					
					lbReponsableCita[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap1pers"]).Trim() + " " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap2pers"]).Trim() + ", " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnompers"]).Trim();					
					lbServiciocita[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnomserv"]);					
					lbPrestacionCita[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dprestac"]);
				}
				
				rdoTemp.Close();

				ErrCargaConsulta = false;
			}
			catch (Exception excep)
			{
				if (!ErrCargaConsulta)
				{
					throw excep;
				}

				if (ErrCargaConsulta)
				{
					RadMessageBox.Show(excep.Message, "Error: CargaConsulta", MessageBoxButtons.OK, RadMessageIcon.Error);
					cbbAnulaciones.Enabled = false;
				}
			}
		}

		private void cargaProg()
		{
			bool ErrCargaConsulta = false;
			string stSql = String.Empty;
			DataSet rdoTemp = null;

			try
			{
				ErrCargaConsulta = true;
				stSql = "SELECT QPROGQUI.finterve, DSERVICI.dnomserv, DCODPRES.dprestac, DPERSONA.dap1pers, " + 
				        "ISNULL(DPERSONA.dap2pers,'') as 'dap2pers', DPERSONA.dnompers " + 
				        "From QPROGQUI " + 
				        "INNER JOIN DSERVICI ON QPROGQUI.gservici = DSERVICI.gservici " + 
				        "INNER JOIN DCODPRES ON QPROGQUI.gprestac = DCODPRES.gprestac " + 
				        "INNER JOIN DPERSONA ON QPROGQUI.gmedicoc = DPERSONA.gpersona " + 
				        "Where (QPROGQUI.ganoregi = " + Conversion.Str(inta�oRegi).Trim() + ") And (QPROGQUI.gnumregi = " + Conversion.Str(lngNumeroRegi).Trim() + ")";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rdConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{					
					lblFechaProg[1].Text = Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["finterve"]).ToString("dd/MM/yyyy");					
					lblResponsableProg[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap1pers"]).Trim() + " " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap2pers"]).Trim() + ", " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnompers"]).Trim();					
					lblServicioPrograma[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnomserv"]);					
					lblProcedimiento[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dprestac"]);
				}
				
				rdoTemp.Close();

				ErrCargaConsulta = false;
			}
			catch (Exception excep)
			{
				if (!ErrCargaConsulta)
				{
					throw excep;
				}

				if (ErrCargaConsulta)
				{
					RadMessageBox.Show(excep.Message, "Error: CargaProg", MessageBoxButtons.OK, RadMessageIcon.Error);
					cbbAnulaciones.Enabled = false;
				}
			}
		}
		private void cargaInter()
		{
			bool ErrCargaConsulta = false;
			string stSql = String.Empty;
			DataSet rdoTemp = null;

			try
			{
				ErrCargaConsulta = true;
				stSql = "SELECT QINTQUIR.finterve, DSERVICI.dnomserv, DCODPRES.dprestac, DPERSONA.dap1pers, " + 
				        "ISNULL(DPERSONA.dap2pers,'') as 'dap2pers', DPERSONA.dnompers " + 
				        "From QINTQUIR " + 
				        "INNER JOIN DSERVICI ON QINTQUIR.gservici = DSERVICI.gservici " + 
				        "INNER JOIN DCODPRES ON QINTQUIR.gprestac = DCODPRES.gprestac " + 
				        "INNER JOIN DPERSONA ON QINTQUIR.gmedicoc = DPERSONA.gpersona " + 
				        "Where (QINTQUIR.ganoregi = " + Conversion.Str(inta�oRegi).Trim() + ") And (QINTQUIR.gnumregi = " + Conversion.Str(lngNumeroRegi).Trim() + ")";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rdConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{					
					lbFechaInter[1].Text = Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["finterve"]).ToString("dd/MM/yyyy");					
					lbReponsableInter[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap1pers"]).Trim() + " " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap2pers"]).Trim() + ", " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnompers"]).Trim();					
					lbServicioInter[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnomserv"]);					
					lbPrestacionInter[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dprestac"]);
				}				
				rdoTemp.Close();

				ErrCargaConsulta = false;
			}
			catch (Exception excep)
			{
				if (!ErrCargaConsulta)
				{
					throw excep;
				}

				if (ErrCargaConsulta)
				{
					RadMessageBox.Show(excep.Message, "Error: CargaInter", MessageBoxButtons.OK, RadMessageIcon.Error);
					cbbAnulaciones.Enabled = false;
				}
			}
		}

		private void cargaSoli()
		{
			bool ErrCargaSoli = false;
			string stSql = String.Empty;
			DataSet rdoTemp = null;

			try
			{
				ErrCargaSoli = true;
				stSql = "SELECT EPRESALT.fpeticio, DSERVICI.dnomserv, DCODPRES.dprestac, DPERSONA.dap1pers, " + 
				        "ISNULL(DPERSONA.dap2pers,'') as 'dap2pers', DPERSONA.dnompers " + 
				        "From AEPISLES with(nolock) INNER JOIN EPRESALT with(nolock) ON  AEPISLES.itiprela = EPRESALT.itiposer AND AEPISLES.ganorela = EPRESALT.ganoregi AND AEPISLES.gnumrela = EPRESALT.gnumregi AND AEPISLES.gprerela = EPRESALT.gprestac AND AEPISLES.fpetrela = EPRESALT.fpeticio " + 
				        "INNER JOIN DCODPRES with(nolock) ON EPRESALT.gprestac = DCODPRES.gprestac " + 
				        "INNER JOIN DSERVICI with(nolock) ON DCODPRES.gservici = DSERVICI.gservici " + 
				        "INNER JOIN DPERSONA with(nolock) ON EPRESALT.gpersoli = DPERSONA.gpersona " + 
				        "Where (AEPISLES.ganoregi = " + Conversion.Str(inta�oRegi).Trim() + ") And (AEPISLES.gnumregi = " + Conversion.Str(lngNumeroRegi).Trim() + ")";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rdConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count == 0)
				{
					stSql = Serrores.Replace(stSql, "EPRESALT", "EPRESTAC");
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, rdConexion);
					rdoTemp = new DataSet();
					tempAdapter_2.Fill(rdoTemp);
				}
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{					
					lbFechaInter[1].Text = Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fpeticio"]).ToString("dd/MM/yyyy");					
					lbReponsableInter[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap1pers"]).Trim() + " " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dap2pers"]).Trim() + ", " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnompers"]).Trim();					
					lbServicioInter[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnomserv"]);					
					lbPrestacionInter[1].Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dprestac"]);
				}
				
				rdoTemp.Close();

				ErrCargaSoli = false;
			}
			catch (Exception excep)
			{
				if (!ErrCargaSoli)
				{
					throw excep;
				}

				if (ErrCargaSoli)
				{

					RadMessageBox.Show(excep.Message, "Error: CargaSoli", MessageBoxButtons.OK, RadMessageIcon.Error);
					cbbAnulaciones.Enabled = false;
				}
			}
		}
		private void frmAnulaConsProg_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}