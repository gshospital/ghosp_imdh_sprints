using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace AnulaConsProg
{
	partial class frmAnulaConsProg
	{

		#region "Upgrade Support "
		private static frmAnulaConsProg m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmAnulaConsProg DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmAnulaConsProg();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbbAnulaciones", "_lbPrestacionInter_1", "_lbServicioInter_1", "_lbReponsableInter_1", "_lbFechaInter_1", "_lbPrestacionInter_0", "_lbServicioInter_0", "_lbReponsableInter_0", "_lbFechaInter_0", "frmIntervencion", "_lbFechaCita_0", "_lbReponsableCita_0", "_lbServiciocita_0", "_lbPrestacionCita_0", "_lbFechaCita_1", "_lbReponsableCita_1", "_lbServiciocita_1", "_lbPrestacionCita_1", "frmConsultas", "_lblFechaProg_0", "_lblFechaProg_1", "_lblServicioPrograma_0", "_lblServicioPrograma_1", "_lblResponsableProg_0", "_lblResponsableProg_1", "_lblProcedimiento_0", "_lblProcedimiento_1", "frmPrograma", "Label1", "Frame1", "cmbCancelar", "cmbAceptar", "lbFechaCita", "lbFechaInter", "lbPrestacionCita", "lbPrestacionInter", "lbReponsableCita", "lbReponsableInter", "lbServicioInter", "lbServiciocita", "lblFechaProg", "lblProcedimiento", "lblResponsableProg", "lblServicioPrograma"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadDropDownList cbbAnulaciones;
		private Telerik.WinControls.UI.RadTextBox _lbPrestacionInter_1;
		private Telerik.WinControls.UI.RadTextBox _lbServicioInter_1;
		private Telerik.WinControls.UI.RadTextBox _lbReponsableInter_1;
		private Telerik.WinControls.UI.RadTextBox _lbFechaInter_1;
		private Telerik.WinControls.UI.RadLabel _lbPrestacionInter_0;
		private System.Windows.Forms.Label _lbServicioInter_0;
		private Telerik.WinControls.UI.RadLabel _lbReponsableInter_0;
		private System.Windows.Forms.Label _lbFechaInter_0;
		public Telerik.WinControls.UI.RadGroupBox frmIntervencion;
		private Telerik.WinControls.UI.RadLabel _lbFechaCita_0;
		private System.Windows.Forms.Label _lbReponsableCita_0;
		private System.Windows.Forms.Label _lbServiciocita_0;
		private System.Windows.Forms.Label _lbPrestacionCita_0;
		private Telerik.WinControls.UI.RadLabel _lbFechaCita_1;
		private System.Windows.Forms.Label _lbReponsableCita_1;
		private System.Windows.Forms.Label _lbServiciocita_1;
		private System.Windows.Forms.Label _lbPrestacionCita_1;
		public Telerik.WinControls.UI.RadGroupBox frmConsultas;
		private System.Windows.Forms.Label _lblFechaProg_0;
		private System.Windows.Forms.Label _lblFechaProg_1;
		private System.Windows.Forms.Label _lblServicioPrograma_0;
		private System.Windows.Forms.Label _lblServicioPrograma_1;
		private System.Windows.Forms.Label _lblResponsableProg_0;
		private System.Windows.Forms.Label _lblResponsableProg_1;
		private System.Windows.Forms.Label _lblProcedimiento_0;
		private System.Windows.Forms.Label _lblProcedimiento_1;
		public Telerik.WinControls.UI.RadGroupBox frmPrograma;
		public System.Windows.Forms.Label Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cmbCancelar;
		public Telerik.WinControls.UI.RadButton cmbAceptar;
		public Telerik.WinControls.UI.RadLabel[] lbFechaCita = new Telerik.WinControls.UI.RadLabel[2];
		public System.Windows.Forms.Label[] lbFechaInter = new System.Windows.Forms.Label[2];
		public System.Windows.Forms.Label[] lbPrestacionCita = new System.Windows.Forms.Label[2];
		public Telerik.WinControls.UI.RadLabel[] lbPrestacionInter = new Telerik.WinControls.UI.RadLabel[2];
		public System.Windows.Forms.Label[] lbReponsableCita = new System.Windows.Forms.Label[2];
		public Telerik.WinControls.UI.RadLabel[] lbReponsableInter = new Telerik.WinControls.UI.RadLabel[2];
		public System.Windows.Forms.Label[] lbServicioInter = new System.Windows.Forms.Label[2];
		public System.Windows.Forms.Label[] lbServiciocita = new System.Windows.Forms.Label[2];
		public System.Windows.Forms.Label[] lblFechaProg = new System.Windows.Forms.Label[2];
		public System.Windows.Forms.Label[] lblProcedimiento = new System.Windows.Forms.Label[2];
		public System.Windows.Forms.Label[] lblResponsableProg = new System.Windows.Forms.Label[2];
		public System.Windows.Forms.Label[] lblServicioPrograma = new System.Windows.Forms.Label[2];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbbAnulaciones = new Telerik.WinControls.UI.RadDropDownList();
            this.frmIntervencion = new Telerik.WinControls.UI.RadGroupBox();
            this._lbPrestacionInter_1 = new Telerik.WinControls.UI.RadTextBox();
            this._lbServicioInter_1 = new Telerik.WinControls.UI.RadTextBox();
            this._lbReponsableInter_1 = new Telerik.WinControls.UI.RadTextBox();
            this._lbFechaInter_1 = new Telerik.WinControls.UI.RadTextBox();
            this._lbPrestacionInter_0 = new Telerik.WinControls.UI.RadLabel();
            this._lbServicioInter_0 = new System.Windows.Forms.Label();
            this._lbReponsableInter_0 = new Telerik.WinControls.UI.RadLabel();
            this._lbFechaInter_0 = new System.Windows.Forms.Label();
            this.frmConsultas = new Telerik.WinControls.UI.RadGroupBox();
            this._lbFechaCita_0 = new Telerik.WinControls.UI.RadLabel();
            this._lbReponsableCita_0 = new System.Windows.Forms.Label();
            this._lbServiciocita_0 = new System.Windows.Forms.Label();
            this._lbPrestacionCita_0 = new System.Windows.Forms.Label();
            this._lbFechaCita_1 = new Telerik.WinControls.UI.RadLabel();
            this._lbReponsableCita_1 = new System.Windows.Forms.Label();
            this._lbServiciocita_1 = new System.Windows.Forms.Label();
            this._lbPrestacionCita_1 = new System.Windows.Forms.Label();
            this.frmPrograma = new Telerik.WinControls.UI.RadGroupBox();
            this._lblFechaProg_0 = new System.Windows.Forms.Label();
            this._lblFechaProg_1 = new System.Windows.Forms.Label();
            this._lblServicioPrograma_0 = new System.Windows.Forms.Label();
            this._lblServicioPrograma_1 = new System.Windows.Forms.Label();
            this._lblResponsableProg_0 = new System.Windows.Forms.Label();
            this._lblResponsableProg_1 = new System.Windows.Forms.Label();
            this._lblProcedimiento_0 = new System.Windows.Forms.Label();
            this._lblProcedimiento_1 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.cmbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cmbAceptar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAnulaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmIntervencion)).BeginInit();
            this.frmIntervencion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lbPrestacionInter_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbServicioInter_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbReponsableInter_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbFechaInter_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbPrestacionInter_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbReponsableInter_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmConsultas)).BeginInit();
            this.frmConsultas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lbFechaCita_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbFechaCita_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPrograma)).BeginInit();
            this.frmPrograma.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.cbbAnulaciones);
            this.Frame1.Controls.Add(this.frmIntervencion);
            this.Frame1.Controls.Add(this.frmConsultas);
            this.Frame1.Controls.Add(this.frmPrograma);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(8, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(641, 225);
            this.Frame1.TabIndex = 2;
            // 
            // cbbAnulaciones
            // 
            this.cbbAnulaciones.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbAnulaciones.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbAnulaciones.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cbbAnulaciones.Location = new System.Drawing.Point(181, 174);
            this.cbbAnulaciones.Name = "cbbAnulaciones";
            this.cbbAnulaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbAnulaciones.Size = new System.Drawing.Size(328, 20);
            this.cbbAnulaciones.TabIndex = 22;
            // 
            // frmIntervencion
            // 
            this.frmIntervencion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmIntervencion.Controls.Add(this._lbPrestacionInter_1);
            this.frmIntervencion.Controls.Add(this._lbServicioInter_1);
            this.frmIntervencion.Controls.Add(this._lbReponsableInter_1);
            this.frmIntervencion.Controls.Add(this._lbFechaInter_1);
            this.frmIntervencion.Controls.Add(this._lbPrestacionInter_0);
            this.frmIntervencion.Controls.Add(this._lbServicioInter_0);
            this.frmIntervencion.Controls.Add(this._lbReponsableInter_0);
            this.frmIntervencion.Controls.Add(this._lbFechaInter_0);
            this.frmIntervencion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.frmIntervencion.HeaderText = "Intervención";
            this.frmIntervencion.Location = new System.Drawing.Point(16, 24);
            this.frmIntervencion.Name = "frmIntervencion";
            this.frmIntervencion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmIntervencion.Size = new System.Drawing.Size(609, 137);
            this.frmIntervencion.TabIndex = 23;
            this.frmIntervencion.Text = "Intervención";
            // 
            // _lbPrestacionInter_1
            // 
            this._lbPrestacionInter_1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._lbPrestacionInter_1.Enabled = false;
            this._lbPrestacionInter_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbPrestacionInter_1.Location = new System.Drawing.Point(72, 96);
            this._lbPrestacionInter_1.Name = "_lbPrestacionInter_1";
            this._lbPrestacionInter_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbPrestacionInter_1.Size = new System.Drawing.Size(529, 20);
            this._lbPrestacionInter_1.TabIndex = 31;
            // 
            // _lbServicioInter_1
            // 
            this._lbServicioInter_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbServicioInter_1.Location = new System.Drawing.Point(72, 64);
            this._lbServicioInter_1.Name = "_lbServicioInter_1";
            this._lbServicioInter_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbServicioInter_1.Size = new System.Drawing.Size(529, 20);
            this._lbServicioInter_1.TabIndex = 30;
            this._lbServicioInter_1.Enabled = false;
            // 
            // _lbReponsableInter_1
            // 
            this._lbReponsableInter_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbReponsableInter_1.Enabled = false;
            this._lbReponsableInter_1.Location = new System.Drawing.Point(248, 33);
            this._lbReponsableInter_1.Name = "_lbReponsableInter_1";
            this._lbReponsableInter_1.Size = new System.Drawing.Size(353, 20);
            this._lbReponsableInter_1.TabIndex = 29;
            this._lbReponsableInter_1.Enabled = false;
            // 
            // _lbFechaInter_1
            // 
            this._lbFechaInter_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbFechaInter_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbFechaInter_1.Location = new System.Drawing.Point(72, 33);
            this._lbFechaInter_1.Name = "_lbFechaInter_1";
            this._lbFechaInter_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbFechaInter_1.Size = new System.Drawing.Size(97, 20);
            this._lbFechaInter_1.TabIndex = 28;
            this._lbFechaInter_1.Enabled = false;
            // 
            // _lbPrestacionInter_0
            // 
            this._lbPrestacionInter_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbPrestacionInter_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbPrestacionInter_0.Location = new System.Drawing.Point(8, 100);
            this._lbPrestacionInter_0.Name = "_lbPrestacionInter_0";
            this._lbPrestacionInter_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbPrestacionInter_0.Size = new System.Drawing.Size(60, 18);
            this._lbPrestacionInter_0.TabIndex = 27;
            this._lbPrestacionInter_0.Text = "Prestación:";
            // 
            // _lbServicioInter_0
            // 
            this._lbServicioInter_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbServicioInter_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbServicioInter_0.Location = new System.Drawing.Point(8, 68);
            this._lbServicioInter_0.Name = "_lbServicioInter_0";
            this._lbServicioInter_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbServicioInter_0.Size = new System.Drawing.Size(57, 17);
            this._lbServicioInter_0.TabIndex = 26;
            this._lbServicioInter_0.Text = "Servicio:";
            // 
            // _lbReponsableInter_0
            // 
            this._lbReponsableInter_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbReponsableInter_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbReponsableInter_0.Location = new System.Drawing.Point(176, 36);
            this._lbReponsableInter_0.Name = "_lbReponsableInter_0";
            this._lbReponsableInter_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbReponsableInter_0.Size = new System.Drawing.Size(72, 18);
            this._lbReponsableInter_0.TabIndex = 25;
            this._lbReponsableInter_0.Text = "Responsable:";
            // 
            // _lbFechaInter_0
            // 
            this._lbFechaInter_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbFechaInter_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbFechaInter_0.Location = new System.Drawing.Point(8, 36);
            this._lbFechaInter_0.Name = "_lbFechaInter_0";
            this._lbFechaInter_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbFechaInter_0.Size = new System.Drawing.Size(41, 17);
            this._lbFechaInter_0.TabIndex = 24;
            this._lbFechaInter_0.Text = "Fecha:";
            // 
            // frmConsultas
            // 
            this.frmConsultas.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmConsultas.Controls.Add(this._lbFechaCita_0);
            this.frmConsultas.Controls.Add(this._lbReponsableCita_0);
            this.frmConsultas.Controls.Add(this._lbServiciocita_0);
            this.frmConsultas.Controls.Add(this._lbPrestacionCita_0);
            this.frmConsultas.Controls.Add(this._lbFechaCita_1);
            this.frmConsultas.Controls.Add(this._lbReponsableCita_1);
            this.frmConsultas.Controls.Add(this._lbServiciocita_1);
            this.frmConsultas.Controls.Add(this._lbPrestacionCita_1);
            this.frmConsultas.ForeColor = System.Drawing.SystemColors.ControlText;
            this.frmConsultas.HeaderText = "Consulta";
            this.frmConsultas.Location = new System.Drawing.Point(16, 24);
            this.frmConsultas.Name = "frmConsultas";
            this.frmConsultas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmConsultas.Size = new System.Drawing.Size(609, 137);
            this.frmConsultas.TabIndex = 3;
            this.frmConsultas.Text = "Consulta";
            // 
            // _lbFechaCita_0
            // 
            this._lbFechaCita_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbFechaCita_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbFechaCita_0.Location = new System.Drawing.Point(8, 36);
            this._lbFechaCita_0.Name = "_lbFechaCita_0";
            this._lbFechaCita_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbFechaCita_0.Size = new System.Drawing.Size(37, 18);
            this._lbFechaCita_0.TabIndex = 11;
            this._lbFechaCita_0.Text = "Fecha:";
            // 
            // _lbReponsableCita_0
            // 
            this._lbReponsableCita_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbReponsableCita_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbReponsableCita_0.Location = new System.Drawing.Point(176, 36);
            this._lbReponsableCita_0.Name = "_lbReponsableCita_0";
            this._lbReponsableCita_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbReponsableCita_0.Size = new System.Drawing.Size(65, 17);
            this._lbReponsableCita_0.TabIndex = 10;
            this._lbReponsableCita_0.Text = "Responsable:";
            // 
            // _lbServiciocita_0
            // 
            this._lbServiciocita_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbServiciocita_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbServiciocita_0.Location = new System.Drawing.Point(8, 68);
            this._lbServiciocita_0.Name = "_lbServiciocita_0";
            this._lbServiciocita_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbServiciocita_0.Size = new System.Drawing.Size(41, 17);
            this._lbServiciocita_0.TabIndex = 9;
            this._lbServiciocita_0.Text = "Servicio:";
            // 
            // _lbPrestacionCita_0
            // 
            this._lbPrestacionCita_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbPrestacionCita_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbPrestacionCita_0.Location = new System.Drawing.Point(8, 100);
            this._lbPrestacionCita_0.Name = "_lbPrestacionCita_0";
            this._lbPrestacionCita_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbPrestacionCita_0.Size = new System.Drawing.Size(57, 17);
            this._lbPrestacionCita_0.TabIndex = 8;
            this._lbPrestacionCita_0.Text = "Prestación:";
            // 
            // _lbFechaCita_1
            // 
            this._lbFechaCita_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbFechaCita_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbFechaCita_1.Location = new System.Drawing.Point(72, 33);
            this._lbFechaCita_1.Name = "_lbFechaCita_1";
            this._lbFechaCita_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbFechaCita_1.Size = new System.Drawing.Size(2, 2);
            this._lbFechaCita_1.TabIndex = 7;
            // 
            // _lbReponsableCita_1
            // 
            this._lbReponsableCita_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._lbReponsableCita_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbReponsableCita_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbReponsableCita_1.Location = new System.Drawing.Point(248, 33);
            this._lbReponsableCita_1.Name = "_lbReponsableCita_1";
            this._lbReponsableCita_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbReponsableCita_1.Size = new System.Drawing.Size(353, 24);
            this._lbReponsableCita_1.TabIndex = 6;
            // 
            // _lbServiciocita_1
            // 
            this._lbServiciocita_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._lbServiciocita_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbServiciocita_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbServiciocita_1.Location = new System.Drawing.Point(72, 64);
            this._lbServiciocita_1.Name = "_lbServiciocita_1";
            this._lbServiciocita_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbServiciocita_1.Size = new System.Drawing.Size(529, 24);
            this._lbServiciocita_1.TabIndex = 5;
            // 
            // _lbPrestacionCita_1
            // 
            this._lbPrestacionCita_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._lbPrestacionCita_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbPrestacionCita_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lbPrestacionCita_1.Location = new System.Drawing.Point(72, 96);
            this._lbPrestacionCita_1.Name = "_lbPrestacionCita_1";
            this._lbPrestacionCita_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbPrestacionCita_1.Size = new System.Drawing.Size(529, 24);
            this._lbPrestacionCita_1.TabIndex = 4;
            // 
            // frmPrograma
            // 
            this.frmPrograma.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmPrograma.Controls.Add(this._lblFechaProg_0);
            this.frmPrograma.Controls.Add(this._lblFechaProg_1);
            this.frmPrograma.Controls.Add(this._lblServicioPrograma_0);
            this.frmPrograma.Controls.Add(this._lblServicioPrograma_1);
            this.frmPrograma.Controls.Add(this._lblResponsableProg_0);
            this.frmPrograma.Controls.Add(this._lblResponsableProg_1);
            this.frmPrograma.Controls.Add(this._lblProcedimiento_0);
            this.frmPrograma.Controls.Add(this._lblProcedimiento_1);
            this.frmPrograma.ForeColor = System.Drawing.SystemColors.ControlText;
            this.frmPrograma.HeaderText = "Programación";
            this.frmPrograma.Location = new System.Drawing.Point(16, 24);
            this.frmPrograma.Name = "frmPrograma";
            this.frmPrograma.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmPrograma.Size = new System.Drawing.Size(609, 137);
            this.frmPrograma.TabIndex = 12;
            this.frmPrograma.Text = "Programación";
            // 
            // _lblFechaProg_0
            // 
            this._lblFechaProg_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblFechaProg_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lblFechaProg_0.Location = new System.Drawing.Point(8, 28);
            this._lblFechaProg_0.Name = "_lblFechaProg_0";
            this._lblFechaProg_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblFechaProg_0.Size = new System.Drawing.Size(73, 17);
            this._lblFechaProg_0.TabIndex = 20;
            this._lblFechaProg_0.Text = "Fecha:";
            // 
            // _lblFechaProg_1
            // 
            this._lblFechaProg_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._lblFechaProg_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblFechaProg_1.Location = new System.Drawing.Point(88, 25);
            this._lblFechaProg_1.Name = "_lblFechaProg_1";
            this._lblFechaProg_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblFechaProg_1.Size = new System.Drawing.Size(105, 24);
            this._lblFechaProg_1.TabIndex = 19;
            // 
            // _lblServicioPrograma_0
            // 
            this._lblServicioPrograma_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblServicioPrograma_0.Location = new System.Drawing.Point(8, 60);
            this._lblServicioPrograma_0.Name = "_lblServicioPrograma_0";
            this._lblServicioPrograma_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblServicioPrograma_0.Size = new System.Drawing.Size(65, 17);
            this._lblServicioPrograma_0.TabIndex = 18;
            this._lblServicioPrograma_0.Text = "Servicio:";
            // 
            // _lblServicioPrograma_1
            // 
            this._lblServicioPrograma_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._lblServicioPrograma_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblServicioPrograma_1.Location = new System.Drawing.Point(88, 57);
            this._lblServicioPrograma_1.Name = "_lblServicioPrograma_1";
            this._lblServicioPrograma_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblServicioPrograma_1.Size = new System.Drawing.Size(505, 24);
            this._lblServicioPrograma_1.TabIndex = 17;
            // 
            // _lblResponsableProg_0
            // 
            this._lblResponsableProg_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblResponsableProg_0.Location = new System.Drawing.Point(208, 28);
            this._lblResponsableProg_0.Name = "_lblResponsableProg_0";
            this._lblResponsableProg_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblResponsableProg_0.Size = new System.Drawing.Size(73, 17);
            this._lblResponsableProg_0.TabIndex = 16;
            this._lblResponsableProg_0.Text = "Responsable:";
            // 
            // _lblResponsableProg_1
            // 
            this._lblResponsableProg_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._lblResponsableProg_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblResponsableProg_1.Location = new System.Drawing.Point(288, 25);
            this._lblResponsableProg_1.Name = "_lblResponsableProg_1";
            this._lblResponsableProg_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblResponsableProg_1.Size = new System.Drawing.Size(305, 24);
            this._lblResponsableProg_1.TabIndex = 15;
            // 
            // _lblProcedimiento_0
            // 
            this._lblProcedimiento_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblProcedimiento_0.Location = new System.Drawing.Point(8, 92);
            this._lblProcedimiento_0.Name = "_lblProcedimiento_0";
            this._lblProcedimiento_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblProcedimiento_0.Size = new System.Drawing.Size(73, 17);
            this._lblProcedimiento_0.TabIndex = 14;
            this._lblProcedimiento_0.Text = "Procedimiento:";
            // 
            // _lblProcedimiento_1
            // 
            this._lblProcedimiento_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._lblProcedimiento_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblProcedimiento_1.Location = new System.Drawing.Point(88, 89);
            this._lblProcedimiento_1.Name = "_lblProcedimiento_1";
            this._lblProcedimiento_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblProcedimiento_1.Size = new System.Drawing.Size(505, 24);
            this._lblProcedimiento_1.TabIndex = 13;
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(64, 174);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(121, 19);
            this.Label1.TabIndex = 21;
            this.Label1.Text = "Motivo de anulación:";
            // 
            // cmbCancelar
            // 
            this.cmbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbCancelar.Location = new System.Drawing.Point(560, 240);
            this.cmbCancelar.Name = "cmbCancelar";
            this.cmbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmbCancelar.Size = new System.Drawing.Size(89, 25);
            this.cmbCancelar.TabIndex = 1;
            this.cmbCancelar.Text = "&Cancelar";
            this.cmbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmbCancelar.Click += new System.EventHandler(this.cmbCancelar_Click);
            // 
            // cmbAceptar
            // 
            this.cmbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbAceptar.Location = new System.Drawing.Point(456, 241);
            this.cmbAceptar.Name = "cmbAceptar";
            this.cmbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmbAceptar.Size = new System.Drawing.Size(89, 25);
            this.cmbAceptar.TabIndex = 0;
            this.cmbAceptar.Text = "&Aceptar";
            this.cmbAceptar.Click += new System.EventHandler(this.cmbAceptar_Click);
            // 
            // frmAnulaConsProg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 280);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cmbCancelar);
            this.Controls.Add(this.cmbAceptar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 30);
            this.Name = "frmAnulaConsProg";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Anulación de ";
            this.Closed += new System.EventHandler(this.frmAnulaConsProg_Closed);
            this.Load += new System.EventHandler(this.frmAnulaConsProg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbAnulaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmIntervencion)).EndInit();
            this.frmIntervencion.ResumeLayout(false);
            this.frmIntervencion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lbPrestacionInter_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbServicioInter_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbReponsableInter_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbFechaInter_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbPrestacionInter_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbReponsableInter_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmConsultas)).EndInit();
            this.frmConsultas.ResumeLayout(false);
            this.frmConsultas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lbFechaCita_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbFechaCita_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPrograma)).EndInit();
            this.frmPrograma.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			InitializelblServicioPrograma();
			InitializelblResponsableProg();
			InitializelblProcedimiento();
			InitializelblFechaProg();
			InitializelbServiciocita();
			InitializelbServicioInter();
			InitializelbReponsableInter();
			InitializelbReponsableCita();
			InitializelbPrestacionInter();
			InitializelbPrestacionCita();
			InitializelbFechaInter();
			InitializelbFechaCita();
		}
		void InitializelblServicioPrograma()
		{
			this.lblServicioPrograma = new System.Windows.Forms.Label[2];
			this.lblServicioPrograma[0] = _lblServicioPrograma_0;
			this.lblServicioPrograma[1] = _lblServicioPrograma_1;
		}
		void InitializelblResponsableProg()
		{
			this.lblResponsableProg = new System.Windows.Forms.Label[2];
			this.lblResponsableProg[0] = _lblResponsableProg_0;
			this.lblResponsableProg[1] = _lblResponsableProg_1;
		}
		void InitializelblProcedimiento()
		{
			this.lblProcedimiento = new System.Windows.Forms.Label[2];
			this.lblProcedimiento[0] = _lblProcedimiento_0;
			this.lblProcedimiento[1] = _lblProcedimiento_1;
		}
		void InitializelblFechaProg()
		{
			this.lblFechaProg = new System.Windows.Forms.Label[2];
			this.lblFechaProg[0] = _lblFechaProg_0;
			this.lblFechaProg[1] = _lblFechaProg_1;
		}
		void InitializelbServiciocita()
		{
			this.lbServiciocita = new System.Windows.Forms.Label[2];
			this.lbServiciocita[0] = _lbServiciocita_0;
			this.lbServiciocita[1] = _lbServiciocita_1;
		}
		void InitializelbServicioInter()
		{
			//this.lbServicioInter = new System.Windows.Forms.Label[2];
			////this.lbServicioInter[1] = _lbServicioInter_1;
			////this.lbServicioInter[0] = _lbServicioInter_0;
		}
		void InitializelbReponsableInter()
		{
			//this.lbReponsableInter = new Telerik.WinControls.UI.RadLabel[2];
			//this.lbReponsableInter[1] = _lbReponsableInter_1;
			//this.lbReponsableInter[0] = _lbReponsableInter_0;
		}
		void InitializelbReponsableCita()
		{
			this.lbReponsableCita = new System.Windows.Forms.Label[2];
			this.lbReponsableCita[0] = _lbReponsableCita_0;
			this.lbReponsableCita[1] = _lbReponsableCita_1;
		}
		void InitializelbPrestacionInter()
		{
			//this.lbPrestacionInter = new Telerik.WinControls.UI.RadLabel[2];
			////this.lbPrestacionInter[1] = _lbPrestacionInter_1;
			//this.lbPrestacionInter[0] = _lbPrestacionInter_0;
		}
		void InitializelbPrestacionCita()
		{
			this.lbPrestacionCita = new System.Windows.Forms.Label[2];
			this.lbPrestacionCita[0] = _lbPrestacionCita_0;
			this.lbPrestacionCita[1] = _lbPrestacionCita_1;
		}
		void InitializelbFechaInter()
		{
			//this.lbFechaInter = new System.Windows.Forms.Label[2];
			//this.lbFechaInter[1] = _lbFechaInter_1;
			//this.lbFechaInter[0] = _lbFechaInter_0;
		}
		void InitializelbFechaCita()
		{
			this.lbFechaCita = new Telerik.WinControls.UI.RadLabel[2];
			this.lbFechaCita[0] = _lbFechaCita_0;
			this.lbFechaCita[1] = _lbFechaCita_1;
		}
		#endregion
	}
}