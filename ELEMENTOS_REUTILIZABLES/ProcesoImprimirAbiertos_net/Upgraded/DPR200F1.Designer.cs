using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ProcesoImpAbiertos
{
	partial class DPR200F1
	{

		#region "Upgrade Support "
		private static DPR200F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DPR200F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DPR200F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_obOrden_0", "_obOrden_1", "_obOrden_2", "FrOrdenar", "sdcDesde", "sdcHasta", "LblDesde", "LblHasta", "FrIntervalo", "cbCancelar", "cbImprimir", "obOrden"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		private Telerik.WinControls.UI.RadRadioButton _obOrden_0;
		private Telerik.WinControls.UI.RadRadioButton _obOrden_1;
		private Telerik.WinControls.UI.RadRadioButton _obOrden_2;
		public Telerik.WinControls.UI.RadGroupBox FrOrdenar;
		public Telerik.WinControls.UI.RadDateTimePicker sdcDesde;
		public Telerik.WinControls.UI.RadDateTimePicker sdcHasta;
		public Telerik.WinControls.UI.RadLabel LblDesde;
		public Telerik.WinControls.UI.RadLabel LblHasta;
		public Telerik.WinControls.UI.RadGroupBox FrIntervalo;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadRadioButton[] obOrden = new Telerik.WinControls.UI.RadRadioButton[3];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.FrOrdenar = new Telerik.WinControls.UI.RadGroupBox();
            this._obOrden_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._obOrden_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._obOrden_2 = new Telerik.WinControls.UI.RadRadioButton();
            this.FrIntervalo = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcDesde = new Telerik.WinControls.UI.RadDateTimePicker();
            this.sdcHasta = new Telerik.WinControls.UI.RadDateTimePicker();
            this.LblDesde = new Telerik.WinControls.UI.RadLabel();
            this.LblHasta = new Telerik.WinControls.UI.RadLabel();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.FrOrdenar)).BeginInit();
            this.FrOrdenar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._obOrden_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._obOrden_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._obOrden_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrIntervalo)).BeginInit();
            this.FrIntervalo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LblDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LblHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // FrOrdenar
            // 
            this.FrOrdenar.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrOrdenar.Controls.Add(this._obOrden_0);
            this.FrOrdenar.Controls.Add(this._obOrden_1);
            this.FrOrdenar.Controls.Add(this._obOrden_2);
            this.FrOrdenar.HeaderText = "Ordenar por...";
            this.FrOrdenar.Location = new System.Drawing.Point(2, 0);
            this.FrOrdenar.Name = "FrOrdenar";
            this.FrOrdenar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrOrdenar.Size = new System.Drawing.Size(325, 48);
            this.FrOrdenar.TabIndex = 8;
            this.FrOrdenar.Text = "Ordenar por...";
            // 
            // _obOrden_0
            // 
            this._obOrden_0.CheckState = System.Windows.Forms.CheckState.Checked;
            this._obOrden_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._obOrden_0.Location = new System.Drawing.Point(17, 21);
            this._obOrden_0.Name = "_obOrden_0";
            this._obOrden_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._obOrden_0.Size = new System.Drawing.Size(62, 18);
            this._obOrden_0.TabIndex = 0;
            this._obOrden_0.Text = "Paciente";
            this._obOrden_0.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // _obOrden_1
            // 
            this._obOrden_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._obOrden_1.Location = new System.Drawing.Point(109, 21);
            this._obOrden_1.Name = "_obOrden_1";
            this._obOrden_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._obOrden_1.Size = new System.Drawing.Size(112, 18);
            this._obOrden_1.TabIndex = 1;
            this._obOrden_1.Text = "Fecha de Apertura";
            // 
            // _obOrden_2
            // 
            this._obOrden_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._obOrden_2.Location = new System.Drawing.Point(249, 21);
            this._obOrden_2.Name = "_obOrden_2";
            this._obOrden_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._obOrden_2.Size = new System.Drawing.Size(59, 18);
            this._obOrden_2.TabIndex = 2;
            this._obOrden_2.Text = "Servicio";
            // 
            // FrIntervalo
            // 
            this.FrIntervalo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrIntervalo.Controls.Add(this.sdcDesde);
            this.FrIntervalo.Controls.Add(this.sdcHasta);
            this.FrIntervalo.Controls.Add(this.LblDesde);
            this.FrIntervalo.Controls.Add(this.LblHasta);
            this.FrIntervalo.HeaderText = "Intervalo";
            this.FrIntervalo.Location = new System.Drawing.Point(2, 53);
            this.FrIntervalo.Name = "FrIntervalo";
            this.FrIntervalo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrIntervalo.Size = new System.Drawing.Size(326, 50);
            this.FrIntervalo.TabIndex = 7;
            this.FrIntervalo.Text = "Intervalo";
            // 
            // sdcDesde
            // 
            this.sdcDesde.CustomFormat = "dd/MM/yyyy";
            this.sdcDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcDesde.Location = new System.Drawing.Point(49, 20);
            this.sdcDesde.Name = "sdcDesde";
            this.sdcDesde.Size = new System.Drawing.Size(109, 20);
            this.sdcDesde.TabIndex = 3;
            this.sdcDesde.TabStop = false;           
            // 
            // sdcHasta
            // 
            this.sdcHasta.CustomFormat = "dd/MM/yyyy";
            this.sdcHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcHasta.Location = new System.Drawing.Point(210, 20);
            this.sdcHasta.Name = "sdcHasta";
            this.sdcHasta.Size = new System.Drawing.Size(109, 20);
            this.sdcHasta.TabIndex = 4;
            this.sdcHasta.TabStop = false;           
            // 
            // LblDesde
            // 
            this.LblDesde.Cursor = System.Windows.Forms.Cursors.Default;
            this.LblDesde.Location = new System.Drawing.Point(6, 23);
            this.LblDesde.Name = "LblDesde";
            this.LblDesde.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblDesde.Size = new System.Drawing.Size(37, 18);
            this.LblDesde.TabIndex = 10;
            this.LblDesde.Text = "Desde";
            // 
            // LblHasta
            // 
            this.LblHasta.Cursor = System.Windows.Forms.Cursors.Default;
            this.LblHasta.Location = new System.Drawing.Point(170, 23);
            this.LblHasta.Name = "LblHasta";
            this.LblHasta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblHasta.Size = new System.Drawing.Size(34, 18);
            this.LblHasta.TabIndex = 9;
            this.LblHasta.Text = "Hasta";
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(245, 108);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(83, 32);
            this.cbCancelar.TabIndex = 6;
            this.cbCancelar.Text = "&Cancelar";            
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.Location = new System.Drawing.Point(157, 108);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(83, 32);
            this.cbImprimir.TabIndex = 5;
            this.cbImprimir.Text = "&Imprimir";            
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // DPR200F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 143);
            this.Controls.Add(this.FrOrdenar);
            this.Controls.Add(this.FrIntervalo);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbImprimir);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 23);
            this.Name = "DPR200F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Informe de procesos abiertos - DPR200F1";
            this.Closed += new System.EventHandler(this.DPR200F1_Closed);
            this.Load += new System.EventHandler(this.DPR200F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FrOrdenar)).EndInit();
            this.FrOrdenar.ResumeLayout(false);
            this.FrOrdenar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._obOrden_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._obOrden_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._obOrden_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrIntervalo)).EndInit();
            this.FrIntervalo.ResumeLayout(false);
            this.FrIntervalo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LblDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LblHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		void ReLoadForm(bool addEvents)
		{
			InitializeobOrden();
		}
		void InitializeobOrden()
		{
			this.obOrden = new Telerik.WinControls.UI.RadRadioButton[3];
			this.obOrden[0] = _obOrden_0;
			this.obOrden[1] = _obOrden_1;
			this.obOrden[2] = _obOrden_2;
		}
		#endregion
	}
}