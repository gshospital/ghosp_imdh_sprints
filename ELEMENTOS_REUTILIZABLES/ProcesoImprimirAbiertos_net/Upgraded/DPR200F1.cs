using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace ProcesoImpAbiertos
{
	public partial class DPR200F1
		: Telerik.WinControls.UI.RadForm
	{

		string NOMBRE_DSN_SQLSERVER = String.Empty;
		string NOMBRE_DSN_ACCESS = String.Empty;
		SqlConnection rc = null;
		string vstTipoSer = String.Empty;
		string gUsuario_Crystal = String.Empty;
		string gContrase�a_Crystal = String.Empty;
		object LISTADO_GENERAL = null;
		string stPATHINFORMES = String.Empty;
		Mensajes.ClassMensajes oClass = null;
		string NOMBRE_APLICACION = String.Empty;
		public DPR200F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
			DataSet dcDatos = null;
			string StSql = String.Empty;
			int icont = 0;
			object LISTADO_PARTICULAR = null;
			string LitPersona = String.Empty;
			string LitPersona1 = String.Empty;

			try
			{
				if (sdcDesde.Value.Date > sdcHasta.Value.Date)
				{					
					short tempRefParam = 1020;
					string[] tempRefParam2 = new string[] { "La fecha de inicio", "mayor", "la fecha de fin"};
					Serrores.iresume = Convert.ToInt32(oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam,  rc, tempRefParam2));
					sdcHasta.Focus();
					return;
				}
				//**********************************************************************************
				//**********************************************************************************
				this.Cursor = Cursors.WaitCursor;
				LISTADO_PARTICULAR = LISTADO_GENERAL;
				// meto en el informe el grupo,direccion y nombre de hospital
				StSql = "select valfanu1 from sconsglo where gconsglo = 'GrupoHo'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rc);
				dcDatos = new DataSet();
				tempAdapter.Fill(dcDatos);
                //camaya todo_x_4
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //LISTADO_PARTICULAR.Formulas["Form1"] = "\"" + Convert.ToString(dcDatos.Tables[0].Rows[0]["valfanu1"]).Trim() + "\"";

                StSql = "select valfanu1 from sconsglo where gconsglo = 'NOMBREHO'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, rc);
				dcDatos = new DataSet();
				tempAdapter_2.Fill(dcDatos);
                //camaya todo_x_4
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //LISTADO_PARTICULAR.Formulas["Form2"] = "\"" + Convert.ToString(dcDatos.Tables[0].Rows[0]["valfanu1"]).Trim() + "\"";

                StSql = "select valfanu1 from sconsglo where gconsglo = 'direccho'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, rc);
				dcDatos = new DataSet();
				tempAdapter_3.Fill(dcDatos);
                //camaya todo_x_4
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Formulas["Form3"] = "\"" + Convert.ToString(dcDatos.Tables[0].Rows[0]["valfanu1"]).Trim() + "\"";

                // meto en el informe el intervalo
                //camaya todo_x_4
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Formulas["Intervalo"] = "\"" + "Desde " + sdcDesde.Value.Date + " Hasta  " + sdcHasta.Value.Date + "\"";

                // meto en el informe la ordenaci�n seleccionada
                for (icont = 0; icont <= 2; icont++)
				{
					if (obOrden[icont].IsChecked)
					{
                        //camaya todo_x_4
                        //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                        //LISTADO_PARTICULAR.Formulas["Orden"] = "\"" + "Ordenado por " + obOrden[icont].Text + "\"";
                        break;
					}
				}

				LitPersona = Serrores.ObtenerLiteralPersona(rc);
				LitPersona = LitPersona.ToUpper();
                //camaya todo_x_4
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Formulas["LitPers"] = "\"" + LitPersona + "\"";
                LitPersona1 = "LISTADO DE " + LitPersona.ToUpper() + "S CON PROCESOS ABIERTOS";
                //camaya todo_x_4
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Formulas["LitPersTitulo"] = "\"" + LitPersona1 + "\"";

                //******************* O.Frias (SQL-2005) ******************

                StSql = "SELECT DPROCESOVA.finiproc, DPROCESOVA.gdiagnos, DPROCESOVA.odiagnos, DDIAGNOS.dnombdia, " + 
				        "DSERVICI.dnomserv, DPACIENT.dnombpac, DPACIENT.dape1pac , DPACIENT.dape2pac " + 
				        "FROM DPROCESOVA " + 
				        "INNER JOIN DPACIENT ON DPROCESOVA.gidenpac = DPACIENT.gidenpac " + 
				        "LEFT OUTER JOIN DSERVICI ON DPROCESOVA.gservici = DSERVICI.gservici " + 
				        "LEFT OUTER JOIN DDIAGNOS ON DPROCESOVA.gdiagnos = DDIAGNOS.gcodidia AND " + 
				        "DPROCESOVA.finvaldi = DDIAGNOS.finvaldi  " + 
				        "Where (DPROCESOVA.finiproc >= " + Serrores.FormatFecha(sdcDesde) + " ) And " + 
				        "(DPROCESOVA.finiproc <= " + Serrores.FormatFecha(sdcHasta) + " ) ";

				//******************* O.Frias (SQL-2005) *******************
				switch(icont)
				{
					case 0 :  
						StSql = StSql + "\r" + "\n" + "order by DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac "; 
						break;
					case 1 :  
						StSql = StSql + "\r" + "\n" + "order by DPROCESOVA.finiproc"; 
						break;
					case 2 :  
						StSql = StSql + "\r" + "\n" + "order by DSERVICI.dnomserv"; 
						break;
				}
                //camaya todo_x_4
                ////UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Connect = "DSN=" + NOMBRE_DSN_SQLSERVER + ";UID=" + gUsuario_Crystal + ";PWD=" + gContrase�a_Crystal + "";
                ////UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.ReportFileName = stPATHINFORMES + "\\DPR200R1.rpt";
                ////UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.SQLQuery = StSql;
                ////UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Destination = (int) Crystal.DestinationConstants.crptToWindow;
                ////UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
                ////UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Action = 1;
                ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.PageCount();
                ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Reset();
                ////UPGRADE_ISSUE: (2036) Form property DPR200F1.MousePointer does not support custom mousepointers. More Information: http://www.vbtonet.com/ewis/ewi2036.aspx
                this.Cursor = Cursors.Default;
                ////UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.PrinterStopPage = 0;
                LISTADO_PARTICULAR = null;
			}
			catch (Exception e)
			{
                
                this.Cursor = Cursors.Default;

                if (Information.Err().Number == 32755)
				{
					return;
				}
                //camaya todo_x_4
                ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.PageCount();
                ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Reset();
                ////UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.PrinterStopPage = 0;
                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Formulas["Grupo"] = "";
                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Formulas["NombreH"] = "";
                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Formulas["DireccionH"] = "";
                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Formulas["Titulo"] = "";
                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //LISTADO_PARTICULAR.Formulas["NInforme"] = "";
                //LISTADO_PARTICULAR = null;

                Serrores.AnalizaError("Imprimir_Procesos", Path.GetDirectoryName(Application.ExecutablePath), "", "Imprimir Procesos Abiertos: cbImprimir", e);
				dcDatos = null;
				//**********************************************************************************
				//**********************************************************************************
			}

		}
		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}
		
		private void DPR200F1_Load(Object eventSender, EventArgs eventArgs)
		{
			oClass = new Mensajes.ClassMensajes();
			sdcDesde.MaxDate = DateTime.Today;
			sdcHasta.MaxDate = DateTime.Today;
			sdcDesde.Value = DateTime.Today;
			sdcHasta.Value = DateTime.Today;		

            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
            
			string LitPersona = Serrores.ObtenerLiteralPersona(rc);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
			obOrden[0].Text = LitPersona;

		}

		private void DPR200F1_Closed(Object eventSender, EventArgs eventArgs)
		{
			oClass = null;
			LISTADO_GENERAL = null;
		}
		public void RecogerDatos(SqlConnection Conexion, object LISTADO, string DSN, string gUsuario, string GPassword, string PATHINFORMES, string NOMAPLICACION)
		{
			//Recoge los par�metros mandados desde la clase
			rc = Conexion;
			LISTADO_GENERAL = LISTADO;
			NOMBRE_DSN_SQLSERVER = DSN;
			gUsuario_Crystal = gUsuario;
			gContrase�a_Crystal = GPassword;
			stPATHINFORMES = PATHINFORMES;
			NOMBRE_APLICACION = NOMAPLICACION;
		}
	}
}