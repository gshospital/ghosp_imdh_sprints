using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace ProcesoImpAbiertos
{
	public class ClImpAbiertos
	{

		public void Iniciar(SqlConnection Conexion, object LISTADO_GENERAL, string NOMBRE_DSN_SQLSERVER, string gUsuario_Crystal, string GPassword_Crystal, string VSTPATHINFORMES, string NOMBRE_APLICACION)
		{
			DPR200F1.DefInstance.RecogerDatos(Conexion, LISTADO_GENERAL, NOMBRE_DSN_SQLSERVER, gUsuario_Crystal, GPassword_Crystal, VSTPATHINFORMES, NOMBRE_APLICACION);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			DPR200F1 tempLoadForm = DPR200F1.DefInstance;
			DPR200F1.DefInstance.ShowDialog();
			DPR200F1.DefInstance = null;
		}
	}
}