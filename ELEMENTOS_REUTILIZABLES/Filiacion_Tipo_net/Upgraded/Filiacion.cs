using System;
using System.Data;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Microsoft.CSharp;
//using System.Reflection;
//using System.Linq;
//using Telerik.WinControls;

namespace filiacionDLL
{
	public class Filiacion
	{

		string stFORMFILI = String.Empty;
		public Filiacion()
		{
			Mensajes.ClassMensajes clasemensaje = new Mensajes.ClassMensajes();
		}
		public void Load(object VAR1, object VAR2, object stFiliacion, object stModulo, SqlConnection Conexion, object stIdPaciente_optional, string Codigo_Usuario, string UserName, string ComputerName)
		{
			object stIdPaciente = (stIdPaciente_optional == Type.Missing) ? null : stIdPaciente_optional as object;
            string Modulo = stModulo.ToString();
            stFORMFILI = "E";
            //object result;
           
            filiacionE.Filiacion oTipoFil = new filiacionE.Filiacion();
            //ejecuta la funcion que posee el modulo de clase
            if (stIdPaciente_optional != Type.Missing)
            {
                oTipoFil.Load(VAR1, VAR2, Convert.ToString(stFiliacion), ref Modulo, Conexion, stIdPaciente, Codigo_Usuario, UserName, ComputerName);
            }
            else
            {
                oTipoFil.Load(VAR1, VAR2, Convert.ToString(stFiliacion), ref Modulo, Conexion, null, Codigo_Usuario, UserName, ComputerName);
            }
                //try
                //{
                //    Assembly dllFiliacion = Assembly.LoadFrom("NET_filiacion" + stFORMFILI + ".dll");
                //    Type filiacionType = dllFiliacion.GetType("filiacion" + stFORMFILI + ".Filiacion");

                //    if (filiacionType != null)
                //    {
                //        object[] parametersArray;
                //        if (stIdPaciente_optional != Type.Missing)
                //        {
                //            parametersArray = new object[] { VAR1, VAR2, Convert.ToString(stFiliacion), Modulo, Conexion, stIdPaciente, Codigo_Usuario, UserName, ComputerName };
                //        }
                //        else
                //        {
                //            // VAR1: Objeto de tipo filiacionDLL (clase)
                //            // VAR2: Objeto de formulario que lo invoca (Admision -> menu_admision AGI000F1)
                //            parametersArray = new object[] { VAR1, VAR2, Convert.ToString(stFiliacion), Modulo, Conexion, null, Codigo_Usuario, UserName, ComputerName };
                //        }

                //        object classInstance = Activator.CreateInstance(filiacionType, null);
                //        MethodInfo load = filiacionType.GetMethods()
                //                                       .Where(m => m.Name == "Load" && m.GetParameters().Length == parametersArray.Length)
                //                                       .FirstOrDefault();
                //        result = load.Invoke(classInstance, parametersArray);
                //    }
                //}
                //catch (Exception ex)
                //{
                //    RadMessageBox.Show("Error al cargar dll: " + ex.Message);
                //}
            
            }

        public void Load(object VAR1, object VAR2, object stFiliacion, object stModulo, SqlConnection Conexion, object stIdPaciente_optional, string Codigo_Usuario, string UserName)
		{
			Load(VAR1, VAR2, stFiliacion, stModulo, Conexion, stIdPaciente_optional, Codigo_Usuario, UserName, String.Empty);
		}

		public void Load(object VAR1, object VAR2, object stFiliacion, object stModulo, SqlConnection Conexion, object stIdPaciente_optional, string Codigo_Usuario)
		{
			Load(VAR1, VAR2, stFiliacion, stModulo, Conexion, stIdPaciente_optional, Codigo_Usuario, String.Empty, String.Empty);
		}

		public void Load(object VAR1, object VAR2, object stFiliacion, object stModulo, SqlConnection Conexion, object stIdPaciente_optional)
		{
			Load(VAR1, VAR2, stFiliacion, stModulo, Conexion, stIdPaciente_optional, String.Empty, String.Empty, String.Empty);
		}

		public void Load(object VAR1, object VAR2, object stFiliacion, object stModulo, SqlConnection Conexion)
		{
            //Load(VAR1, VAR2, stFiliacion, stModulo, Conexion, Type.Missing, String.Empty, String.Empty, String.Empty);
            Load(VAR1, VAR2, stFiliacion, stModulo, Conexion, Type.Missing, String.Empty, String.Empty, String.Empty);
        }

		public void Openlab(ref string VarOpenlab, ref string vap1, ref string vap2, ref string nom, object oObjeto_optional)
		{
			dynamic oObjeto = (oObjeto_optional == Type.Missing) ? null : oObjeto_optional as object;
			object objeto = null;			
			oObjeto.stIdpac();
			filiacionE.Filiacion oTipoFil = new filiacionE.Filiacion();
			//ejecuta la funcion que posee el modulo de clase
			if (oObjeto_optional != Type.Missing)
			{
				string tempRefParam = "0";
				string tempRefParam2 = "0";
				string tempRefParam3 = String.Empty;
				short tempRefParam4 = -1;
				oTipoFil.Openlab(ref VarOpenlab, vap1, vap2, nom, tempRefParam, tempRefParam2, objeto, tempRefParam3, tempRefParam4);
			}
			else
			{
				string tempRefParam5 = String.Empty;
				string tempRefParam6 = String.Empty;
				object tempRefParam7 = Type.Missing;
				string tempRefParam8 = String.Empty;
				short tempRefParam9 = -1;
				oTipoFil.Openlab(ref VarOpenlab, vap1, vap2, nom, tempRefParam5, tempRefParam6, tempRefParam7, tempRefParam8, tempRefParam9);
			}
		}

		public void Openlab(ref string VarOpenlab, ref string vap1, ref string vap2, ref string nom)
		{
			Openlab(ref VarOpenlab, ref vap1, ref vap2, ref nom, Type.Missing);
		}


		public void Web(ref string VarOpenlab, ref string stIdPaciente)
		{
			filiacionE.Filiacion oTipoFil = new filiacionE.Filiacion();
			//ejecuta la funcion que posee el modulo de clase
			if (stIdPaciente.Trim() != "")
			{
				oTipoFil.Web(ref VarOpenlab, ref stIdPaciente);
			}
			else
			{
				string tempRefParam = "";
				oTipoFil.Web(ref VarOpenlab, ref tempRefParam);
			}
		}
	}
}