using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLConsultaFiliacE
{
	partial class DFI130F1
	{

		#region "Upgrade Support "
		private static DFI130F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI130F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI130F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbCerrar", "Label3", "Label9", "Label8", "Label7", "Label6", "Label12", "Label13", "Label4", "Label2", "Label1", "Label32", "lbPApe1", "lbPApe2", "lbPNombre", "lbPFNac", "lbPSexo", "lbPNIF", "lbPDomicilio", "lbPCodPostal", "lbPTelf1", "lbPTelf2", "lbPEstCivil", "lbPCatSoc", "lbPFUltMov", "Label67", "lbPPob", "Label96", "Label97", "lbPTelf3", "Label74", "Label75", "lbPTarjetaEuropea", "lbPTarjetaSanitaria", "Label86", "Label92", "lbCipAutonomico", "Label93", "LbLPaisNac", "chbPExitus", "Label53", "Label11", "lbPFFallec", "Label10", "frmPExitus", "chbPExtr", "lbPExDireccion", "Label76", "Label46", "Label62", "lbExPoblacion", "Label26", "lbPPais", "Label102", "frmPExtr", "_rbAutorizacion_1", "_rbAutorizacion_0", "Label45", "Label40", "Label15", "Frame9", "tbObservaciones", "Frame13", "_SSTab1_TabPage0", "sprhistoria", "Frame1", "chbPIngres", "Label72", "Label47", "lblUHospit", "Label107", "Label16", "lblcama", "Frame2", "_rbEstado_2", "_rbEstado_1", "_rbEstado_0", "lblFPrest", "lblUniPet", "lblUbiFisi", "Label18", "Label17", "Label27", "Label28", "Label29", "Label71", "Label19", "_frmestado_0", "_SSTab1_TabPage1", "cmdConsultaEntidades", "chkZona", "Label50", "Frame15", "rbPensionista", "rbActivo", "Label55", "Label54", "Frame7", "lbCodCIAS", "lbRENSS", "lbREInspeccion", "Label44", "Label39", "Label31", "Frame6", "Label83", "Label82", "Label81", "lbREFechaCaducidad", "lbREFechaAlta", "lbRECobertura", "lbRENPoliza", "lbRESociedad", "Label30", "Label14", "Frame5", "_rbIndicador_3", "_rbIndicador_2", "Label65", "lbRENomTitular", "Label60", "Label59", "Label5", "Frame4", "Frame3", "chbREPropioPac", "Label84", "Frame16", "chbREExtrangero", "Label78", "lbREExDireccion", "Label70", "lbREEXtpoblacion", "Label63", "Label61", "Label99", "lbREPais", "Frame10", "Label21", "Label22", "Label20", "Label23", "Label24", "Label25", "Label33", "lbREApe1", "lbREApe2", "lbRENombre", "lbRENIF", "lbREDomicilio", "lbREPoblacion", "lbRECodPostal", "frmPrivado", "_rbRERegimen_2", "_rbRERegimen_0", "_rbRERegimen_1", "Label64", "Label58", "Label57", "Label56", "frmRERegimen", "_SSTab1_TabPage2", "_rbTutor_0", "_rbTutor_1", "Frame14", "Option1", "Option2", "Label43", "Label48", "Label49", "Frame12", "chbFExtrangero", "lbFPoblacion", "Label66", "Label68", "Label69", "lbFpais", "Label73", "Frame8", "sprContactos", "Frame11", "lbNIFContacto", "lbPNIFContacto", "lbFCParent", "lbFCTelf2", "lbFCTelf1", "lbFCCodPostal", "lbFCDomicilio", "lbFCNombre", "lbFCApel2", "lbFCApel1", "Label34", "Label35", "Label36", "Label37", "Label38", "lbFCPob", "Label41", "Label42", "Label51", "Label52", "_SSTab1_TabPage3", "tbOtrasCircunstanciasGradoDep", "_lbGradoDep_5", "_lbGradoDep_4", "_lbGradoDep_3", "_lbGradoDep_2", "_lbGradoDep_1", "_lbGradoDep_0", "_lbDependencia_3", "_lbDependencia_5", "_lbDependencia_4", "_lbDependencia_2", "_lbDependencia_1", "_lbDependencia_0", "frmGradoDependencia", "lbCategEspecial", "lbParentesco", "lbNumTrabajador", "Label80", "Label79", "Label77", "fraColectivos", "_SSTab1_TabPage4", "Check1", "LbFechaResolucion2", "LbExpediente2", "LbJuzgado2", "Label91", "Label90", "Label89", "Frame18", "LbFechaResolucion1", "LbExpediente1", "LbJuzgado1", "Label88", "Label87", "Label85", "Frame17", "Label101", "_SSTab1_TabPage5", "SSTab1", "lblTituloFechaUltMod", "lblFechaUltMod", "frmestado", "lbDependencia", "lbGradoDep", "rbAutorizacion", "rbEstado", "rbIndicador", "rbRERegimen", "rbTutor", "sprContactos_Sheet1", "sprhistoria_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label9;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadLabel Label13;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label32;
		public Telerik.WinControls.UI.RadTextBox lbPApe1;
		public Telerik.WinControls.UI.RadTextBox lbPApe2;
		public Telerik.WinControls.UI.RadTextBox lbPNombre;
		public Telerik.WinControls.UI.RadTextBox lbPFNac;
		public Telerik.WinControls.UI.RadTextBox lbPSexo;
		public Telerik.WinControls.UI.RadTextBox lbPNIF;
		public Telerik.WinControls.UI.RadTextBox lbPDomicilio;
		public Telerik.WinControls.UI.RadTextBox lbPCodPostal;
		public Telerik.WinControls.UI.RadTextBox lbPTelf1;
		public Telerik.WinControls.UI.RadTextBox lbPTelf2;
		public Telerik.WinControls.UI.RadTextBox lbPEstCivil;
		public Telerik.WinControls.UI.RadTextBox lbPCatSoc;
		public Telerik.WinControls.UI.RadTextBox lbPFUltMov;
		public Telerik.WinControls.UI.RadLabel Label67;
		public Telerik.WinControls.UI.RadTextBox lbPPob;
		public Telerik.WinControls.UI.RadLabel Label96;
		public Telerik.WinControls.UI.RadLabel Label97;
		public Telerik.WinControls.UI.RadTextBox lbPTelf3;
		public Telerik.WinControls.UI.RadLabel Label74;
		public Telerik.WinControls.UI.RadLabel Label75;
		public Telerik.WinControls.UI.RadTextBox lbPTarjetaEuropea;
		public Telerik.WinControls.UI.RadTextBox lbPTarjetaSanitaria;
		public Telerik.WinControls.UI.RadLabel Label86;
		public Telerik.WinControls.UI.RadLabel Label92;
		public Telerik.WinControls.UI.RadTextBox lbCipAutonomico;
		public Telerik.WinControls.UI.RadLabel Label93;
		public Telerik.WinControls.UI.RadTextBox LbLPaisNac;
		public Telerik.WinControls.UI.RadCheckBox chbPExitus;
		public Telerik.WinControls.UI.RadTextBox lbPFFallec;
		public Telerik.WinControls.UI.RadLabel Label10;
		public Telerik.WinControls.UI.RadGroupBox frmPExitus;
		public Telerik.WinControls.UI.RadCheckBox chbPExtr;
		public Telerik.WinControls.UI.RadTextBox lbPExDireccion;
		public Telerik.WinControls.UI.RadLabel Label76;
		public Telerik.WinControls.UI.RadLabel Label62;
		public Telerik.WinControls.UI.RadTextBox lbExPoblacion;
		public Telerik.WinControls.UI.RadTextBox lbPPais;
		public Telerik.WinControls.UI.RadLabel Label102;
		public Telerik.WinControls.UI.RadGroupBox frmPExtr;
		private Telerik.WinControls.UI.RadRadioButton _rbAutorizacion_1;
		private Telerik.WinControls.UI.RadRadioButton _rbAutorizacion_0;
		public Telerik.WinControls.UI.RadGroupBox Frame9;
		public Telerik.WinControls.UI.RadTextBoxControl tbObservaciones;
		public Telerik.WinControls.UI.RadGroupBox Frame13;
		private Telerik.WinControls.UI.RadPageViewPage _SSTab1_TabPage0;
        public UpgradeHelpers.Spread.FpSpread sprhistoria;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadCheckBox chbPIngres;
		public Telerik.WinControls.UI.RadTextBox lblUHospit;
		public Telerik.WinControls.UI.RadLabel Label107;
		public Telerik.WinControls.UI.RadLabel Label16;
		public Telerik.WinControls.UI.RadTextBox lblcama;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		private Telerik.WinControls.UI.RadRadioButton _rbEstado_2;
		private Telerik.WinControls.UI.RadRadioButton _rbEstado_1;
		private Telerik.WinControls.UI.RadRadioButton _rbEstado_0;
		public Telerik.WinControls.UI.RadTextBox lblFPrest;
		public Telerik.WinControls.UI.RadTextBox lblUniPet;
		public Telerik.WinControls.UI.RadTextBox lblUbiFisi;
		public Telerik.WinControls.UI.RadLabel Label18;
		public Telerik.WinControls.UI.RadLabel Label17;
		public Telerik.WinControls.UI.RadLabel Label19;
		private Telerik.WinControls.UI.RadGroupBox _frmestado_0;
		private Telerik.WinControls.UI.RadPageViewPage _SSTab1_TabPage1;
		public Telerik.WinControls.UI.RadButton cmdConsultaEntidades;
		public Telerik.WinControls.UI.RadCheckBox chkZona;
		public Telerik.WinControls.UI.RadPanel Frame15;
		public Telerik.WinControls.UI.RadRadioButton rbPensionista;
		public Telerik.WinControls.UI.RadRadioButton rbActivo;
		public Telerik.WinControls.UI.RadGroupBox Frame7;
		public Telerik.WinControls.UI.RadTextBox lbCodCIAS;
		public Telerik.WinControls.UI.RadTextBox lbRENSS;
		public Telerik.WinControls.UI.RadTextBox lbREInspeccion;
		public Telerik.WinControls.UI.RadLabel Label44;
		public Telerik.WinControls.UI.RadLabel Label39;
		public Telerik.WinControls.UI.RadLabel Label31;
		public Telerik.WinControls.UI.RadGroupBox Frame6;
		public Telerik.WinControls.UI.RadLabel Label83;
		public Telerik.WinControls.UI.RadLabel Label82;
		public Telerik.WinControls.UI.RadLabel Label81;
		public Telerik.WinControls.UI.RadTextBox lbREFechaCaducidad;
		public Telerik.WinControls.UI.RadTextBox lbREFechaAlta;
		public Telerik.WinControls.UI.RadTextBox lbRECobertura;
		public Telerik.WinControls.UI.RadTextBox lbRENPoliza;
		public Telerik.WinControls.UI.RadTextBox lbRESociedad;
		public Telerik.WinControls.UI.RadLabel Label30;
		public Telerik.WinControls.UI.RadLabel Label14;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		private Telerik.WinControls.UI.RadRadioButton _rbIndicador_3;
		private Telerik.WinControls.UI.RadRadioButton _rbIndicador_2;
		public Telerik.WinControls.UI.RadLabel Label65;
		public Telerik.WinControls.UI.RadTextBox lbRENomTitular;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadCheckBox chbREPropioPac;
		public Telerik.WinControls.UI.RadPanel Frame16;
		public Telerik.WinControls.UI.RadCheckBox chbREExtrangero;
		public Telerik.WinControls.UI.RadLabel Label78;
		public Telerik.WinControls.UI.RadTextBox lbREExDireccion;
		public Telerik.WinControls.UI.RadLabel Label70;
		public Telerik.WinControls.UI.RadTextBox lbREEXtpoblacion;
		public Telerik.WinControls.UI.RadLabel Label63;
		public Telerik.WinControls.UI.RadLabel Label99;
		public Telerik.WinControls.UI.RadTextBox lbREPais;
		public Telerik.WinControls.UI.RadGroupBox Frame10;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadLabel Label22;
		public Telerik.WinControls.UI.RadLabel Label20;
		public Telerik.WinControls.UI.RadLabel Label23;
		public Telerik.WinControls.UI.RadLabel Label24;
		public Telerik.WinControls.UI.RadLabel Label25;
		public Telerik.WinControls.UI.RadLabel Label33;
		public Telerik.WinControls.UI.RadTextBox lbREApe1;
		public Telerik.WinControls.UI.RadTextBox lbREApe2;
		public Telerik.WinControls.UI.RadTextBox lbRENombre;
		public Telerik.WinControls.UI.RadTextBox lbRENIF;
		public Telerik.WinControls.UI.RadTextBox lbREDomicilio;
		public Telerik.WinControls.UI.RadTextBox lbREPoblacion;
		public Telerik.WinControls.UI.RadTextBox lbRECodPostal;
		public Telerik.WinControls.UI.RadGroupBox frmPrivado;
		private Telerik.WinControls.UI.RadRadioButton _rbRERegimen_2;
		private Telerik.WinControls.UI.RadRadioButton _rbRERegimen_0;
		private Telerik.WinControls.UI.RadRadioButton _rbRERegimen_1;
		public Telerik.WinControls.UI.RadGroupBox frmRERegimen;
		private Telerik.WinControls.UI.RadPageViewPage _SSTab1_TabPage2;
		private Telerik.WinControls.UI.RadRadioButton _rbTutor_0;
		private Telerik.WinControls.UI.RadRadioButton _rbTutor_1;
		public Telerik.WinControls.UI.RadGroupBox Frame14;
		public Telerik.WinControls.UI.RadRadioButton Option1;
		public Telerik.WinControls.UI.RadRadioButton Option2;
		public Telerik.WinControls.UI.RadLabel Label43;
		public Telerik.WinControls.UI.RadGroupBox Frame12;
		public Telerik.WinControls.UI.RadCheckBox chbFExtrangero;
		public Telerik.WinControls.UI.RadTextBox lbFPoblacion;
		public Telerik.WinControls.UI.RadLabel Label66;
		public Telerik.WinControls.UI.RadLabel Label69;
		public Telerik.WinControls.UI.RadTextBox lbFpais;
		public Telerik.WinControls.UI.RadLabel Label73;
		public Telerik.WinControls.UI.RadGroupBox Frame8;
		public UpgradeHelpers.Spread.FpSpread sprContactos;
        public Telerik.WinControls.UI.RadGroupBox Frame11;
		public Telerik.WinControls.UI.RadLabel lbNIFContacto;
		public Telerik.WinControls.UI.RadTextBox lbPNIFContacto;
		public Telerik.WinControls.UI.RadTextBox lbFCParent;
		public Telerik.WinControls.UI.RadTextBox lbFCTelf2;
		public Telerik.WinControls.UI.RadTextBox lbFCTelf1;
		public Telerik.WinControls.UI.RadTextBox lbFCCodPostal;
		public Telerik.WinControls.UI.RadTextBox lbFCDomicilio;
		public Telerik.WinControls.UI.RadTextBox lbFCNombre;
		public Telerik.WinControls.UI.RadTextBox lbFCApel2;
		public Telerik.WinControls.UI.RadTextBox lbFCApel1;
		public Telerik.WinControls.UI.RadLabel Label34;
		public Telerik.WinControls.UI.RadLabel Label35;
		public Telerik.WinControls.UI.RadLabel Label36;
		public Telerik.WinControls.UI.RadLabel Label37;
		public Telerik.WinControls.UI.RadLabel Label38;
		public Telerik.WinControls.UI.RadTextBox lbFCPob;
		public Telerik.WinControls.UI.RadLabel Label41;
		public Telerik.WinControls.UI.RadLabel Label42;
		public Telerik.WinControls.UI.RadLabel Label51;
		public Telerik.WinControls.UI.RadLabel Label52;
		private Telerik.WinControls.UI.RadPageViewPage _SSTab1_TabPage3;
		public Telerik.WinControls.UI.RadTextBoxControl tbOtrasCircunstanciasGradoDep;
		private Telerik.WinControls.UI.RadTextBox _lbGradoDep_5;
		private Telerik.WinControls.UI.RadTextBox _lbGradoDep_4;
		private Telerik.WinControls.UI.RadTextBox _lbGradoDep_3;
		private Telerik.WinControls.UI.RadTextBox _lbGradoDep_2;
		private Telerik.WinControls.UI.RadTextBox _lbGradoDep_1;
		private Telerik.WinControls.UI.RadTextBox _lbGradoDep_0;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_3;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_5;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_4;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_2;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_1;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_0;
		public Telerik.WinControls.UI.RadGroupBox frmGradoDependencia;
		public Telerik.WinControls.UI.RadTextBox lbCategEspecial;
		public Telerik.WinControls.UI.RadTextBox lbParentesco;
		public Telerik.WinControls.UI.RadTextBox lbNumTrabajador;
		public Telerik.WinControls.UI.RadLabel Label80;
		public Telerik.WinControls.UI.RadLabel Label79;
		public Telerik.WinControls.UI.RadLabel Label77;
		public Telerik.WinControls.UI.RadGroupBox fraColectivos;
		private Telerik.WinControls.UI.RadPageViewPage _SSTab1_TabPage4;
		public Telerik.WinControls.UI.RadCheckBox Check1;
		public Telerik.WinControls.UI.RadTextBox LbFechaResolucion2;
		public Telerik.WinControls.UI.RadTextBox LbExpediente2;
		public Telerik.WinControls.UI.RadTextBox LbJuzgado2;
		public Telerik.WinControls.UI.RadLabel Label91;
		public Telerik.WinControls.UI.RadLabel Label90;
		public Telerik.WinControls.UI.RadLabel Label89;
		public Telerik.WinControls.UI.RadGroupBox Frame18;
		public Telerik.WinControls.UI.RadTextBox LbFechaResolucion1;
		public Telerik.WinControls.UI.RadTextBox LbExpediente1;
		public Telerik.WinControls.UI.RadTextBox LbJuzgado1;
		public Telerik.WinControls.UI.RadLabel Label88;
		public Telerik.WinControls.UI.RadLabel Label87;
		public Telerik.WinControls.UI.RadLabel Label85;
		public Telerik.WinControls.UI.RadGroupBox Frame17;
		private Telerik.WinControls.UI.RadPageViewPage _SSTab1_TabPage5;
		public Telerik.WinControls.UI.RadPageView SSTab1;
		public Telerik.WinControls.UI.RadLabel lblTituloFechaUltMod;
		public Telerik.WinControls.UI.RadTextBox lblFechaUltMod;
		public Telerik.WinControls.UI.RadGroupBox[] frmestado = new Telerik.WinControls.UI.RadGroupBox[1];
		public Telerik.WinControls.UI.RadLabel[] lbDependencia = new Telerik.WinControls.UI.RadLabel[6];
		public Telerik.WinControls.UI.RadTextBox[] lbGradoDep = new Telerik.WinControls.UI.RadTextBox[6];
		public Telerik.WinControls.UI.RadRadioButton[] rbAutorizacion = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbEstado = new Telerik.WinControls.UI.RadRadioButton[3];
		public Telerik.WinControls.UI.RadRadioButton[] rbIndicador = new Telerik.WinControls.UI.RadRadioButton[4];
		public Telerik.WinControls.UI.RadRadioButton[] rbRERegimen = new Telerik.WinControls.UI.RadRadioButton[3];
		public Telerik.WinControls.UI.RadRadioButton[] rbTutor = new Telerik.WinControls.UI.RadRadioButton[2];
		
        //private FarPoint.Win.Spread.SheetView sprContactos_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView sprhistoria_Sheet1 = null;
		
            //NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            this.SSTab1 = new Telerik.WinControls.UI.RadPageView();
            this._SSTab1_TabPage0 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label9 = new Telerik.WinControls.UI.RadLabel();
            this.Label8 = new Telerik.WinControls.UI.RadLabel();
            this.Label7 = new Telerik.WinControls.UI.RadLabel();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.Label13 = new Telerik.WinControls.UI.RadLabel();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label32 = new Telerik.WinControls.UI.RadLabel();
            this.lbPApe1 = new Telerik.WinControls.UI.RadTextBox();
            this.lbPApe2 = new Telerik.WinControls.UI.RadTextBox();
            this.lbPNombre = new Telerik.WinControls.UI.RadTextBox();
            this.lbPFNac = new Telerik.WinControls.UI.RadTextBox();
            this.lbPSexo = new Telerik.WinControls.UI.RadTextBox();
            this.lbPNIF = new Telerik.WinControls.UI.RadTextBox();
            this.lbPDomicilio = new Telerik.WinControls.UI.RadTextBox();
            this.lbPCodPostal = new Telerik.WinControls.UI.RadTextBox();
            this.lbPTelf1 = new Telerik.WinControls.UI.RadTextBox();
            this.lbPTelf2 = new Telerik.WinControls.UI.RadTextBox();
            this.lbPEstCivil = new Telerik.WinControls.UI.RadTextBox();
            this.lbPCatSoc = new Telerik.WinControls.UI.RadTextBox();
            this.lbPFUltMov = new Telerik.WinControls.UI.RadTextBox();
            this.Label67 = new Telerik.WinControls.UI.RadLabel();
            this.lbPPob = new Telerik.WinControls.UI.RadTextBox();
            this.Label96 = new Telerik.WinControls.UI.RadLabel();
            this.Label97 = new Telerik.WinControls.UI.RadLabel();
            this.lbPTelf3 = new Telerik.WinControls.UI.RadTextBox();
            this.Label74 = new Telerik.WinControls.UI.RadLabel();
            this.Label75 = new Telerik.WinControls.UI.RadLabel();
            this.lbPTarjetaEuropea = new Telerik.WinControls.UI.RadTextBox();
            this.lbPTarjetaSanitaria = new Telerik.WinControls.UI.RadTextBox();
            this.Label86 = new Telerik.WinControls.UI.RadLabel();
            this.Label92 = new Telerik.WinControls.UI.RadLabel();
            this.lbCipAutonomico = new Telerik.WinControls.UI.RadTextBox();
            this.Label93 = new Telerik.WinControls.UI.RadLabel();
            this.LbLPaisNac = new Telerik.WinControls.UI.RadTextBox();
            this.frmPExitus = new Telerik.WinControls.UI.RadGroupBox();
            this.chbPExitus = new Telerik.WinControls.UI.RadCheckBox();
            this.lbPFFallec = new Telerik.WinControls.UI.RadTextBox();
            this.Label10 = new Telerik.WinControls.UI.RadLabel();
            this.frmPExtr = new Telerik.WinControls.UI.RadGroupBox();
            this.chbPExtr = new Telerik.WinControls.UI.RadCheckBox();
            this.lbPExDireccion = new Telerik.WinControls.UI.RadTextBox();
            this.Label76 = new Telerik.WinControls.UI.RadLabel();
            this.Label62 = new Telerik.WinControls.UI.RadLabel();
            this.lbExPoblacion = new Telerik.WinControls.UI.RadTextBox();
            this.lbPPais = new Telerik.WinControls.UI.RadTextBox();
            this.Label102 = new Telerik.WinControls.UI.RadLabel();
            this.Frame9 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbAutorizacion_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbAutorizacion_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.Frame13 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbObservaciones = new Telerik.WinControls.UI.RadTextBoxControl();
            this._SSTab1_TabPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.sprhistoria = new UpgradeHelpers.Spread.FpSpread();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.chbPIngres = new Telerik.WinControls.UI.RadCheckBox();
            this.lblUHospit = new Telerik.WinControls.UI.RadTextBox();
            this.Label107 = new Telerik.WinControls.UI.RadLabel();
            this.Label16 = new Telerik.WinControls.UI.RadLabel();
            this.lblcama = new Telerik.WinControls.UI.RadTextBox();
            this._frmestado_0 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbEstado_2 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbEstado_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbEstado_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.lblFPrest = new Telerik.WinControls.UI.RadTextBox();
            this.lblUniPet = new Telerik.WinControls.UI.RadTextBox();
            this.lblUbiFisi = new Telerik.WinControls.UI.RadTextBox();
            this.Label18 = new Telerik.WinControls.UI.RadLabel();
            this.Label17 = new Telerik.WinControls.UI.RadLabel();
            this.Label19 = new Telerik.WinControls.UI.RadLabel();
            this._SSTab1_TabPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.cmdConsultaEntidades = new Telerik.WinControls.UI.RadButton();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame6 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame15 = new Telerik.WinControls.UI.RadPanel();
            this.chkZona = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame7 = new Telerik.WinControls.UI.RadGroupBox();
            this.rbPensionista = new Telerik.WinControls.UI.RadRadioButton();
            this.rbActivo = new Telerik.WinControls.UI.RadRadioButton();
            this.lbCodCIAS = new Telerik.WinControls.UI.RadTextBox();
            this.lbRENSS = new Telerik.WinControls.UI.RadTextBox();
            this.lbREInspeccion = new Telerik.WinControls.UI.RadTextBox();
            this.Label44 = new Telerik.WinControls.UI.RadLabel();
            this.Label39 = new Telerik.WinControls.UI.RadLabel();
            this.Label31 = new Telerik.WinControls.UI.RadLabel();
            this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label83 = new Telerik.WinControls.UI.RadLabel();
            this.Label82 = new Telerik.WinControls.UI.RadLabel();
            this.Label81 = new Telerik.WinControls.UI.RadLabel();
            this.lbREFechaCaducidad = new Telerik.WinControls.UI.RadTextBox();
            this.lbREFechaAlta = new Telerik.WinControls.UI.RadTextBox();
            this.lbRECobertura = new Telerik.WinControls.UI.RadTextBox();
            this.lbRENPoliza = new Telerik.WinControls.UI.RadTextBox();
            this.lbRESociedad = new Telerik.WinControls.UI.RadTextBox();
            this.Label30 = new Telerik.WinControls.UI.RadLabel();
            this.Label14 = new Telerik.WinControls.UI.RadLabel();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbIndicador_3 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbIndicador_2 = new Telerik.WinControls.UI.RadRadioButton();
            this.Label65 = new Telerik.WinControls.UI.RadLabel();
            this.lbRENomTitular = new Telerik.WinControls.UI.RadTextBox();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.frmPrivado = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame16 = new Telerik.WinControls.UI.RadPanel();
            this.chbREPropioPac = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame10 = new Telerik.WinControls.UI.RadGroupBox();
            this.chbREExtrangero = new Telerik.WinControls.UI.RadCheckBox();
            this.Label78 = new Telerik.WinControls.UI.RadLabel();
            this.lbREExDireccion = new Telerik.WinControls.UI.RadTextBox();
            this.Label70 = new Telerik.WinControls.UI.RadLabel();
            this.lbREEXtpoblacion = new Telerik.WinControls.UI.RadTextBox();
            this.Label63 = new Telerik.WinControls.UI.RadLabel();
            this.Label99 = new Telerik.WinControls.UI.RadLabel();
            this.lbREPais = new Telerik.WinControls.UI.RadTextBox();
            this.Label21 = new Telerik.WinControls.UI.RadLabel();
            this.Label22 = new Telerik.WinControls.UI.RadLabel();
            this.Label20 = new Telerik.WinControls.UI.RadLabel();
            this.Label23 = new Telerik.WinControls.UI.RadLabel();
            this.Label24 = new Telerik.WinControls.UI.RadLabel();
            this.Label25 = new Telerik.WinControls.UI.RadLabel();
            this.Label33 = new Telerik.WinControls.UI.RadLabel();
            this.lbREApe1 = new Telerik.WinControls.UI.RadTextBox();
            this.lbREApe2 = new Telerik.WinControls.UI.RadTextBox();
            this.lbRENombre = new Telerik.WinControls.UI.RadTextBox();
            this.lbRENIF = new Telerik.WinControls.UI.RadTextBox();
            this.lbREDomicilio = new Telerik.WinControls.UI.RadTextBox();
            this.lbREPoblacion = new Telerik.WinControls.UI.RadTextBox();
            this.lbRECodPostal = new Telerik.WinControls.UI.RadTextBox();
            this.frmRERegimen = new Telerik.WinControls.UI.RadGroupBox();
            this._rbRERegimen_2 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbRERegimen_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbRERegimen_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._SSTab1_TabPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Frame14 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbTutor_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbTutor_1 = new Telerik.WinControls.UI.RadRadioButton();
            this.Frame12 = new Telerik.WinControls.UI.RadGroupBox();
            this.Option1 = new Telerik.WinControls.UI.RadRadioButton();
            this.Option2 = new Telerik.WinControls.UI.RadRadioButton();
            this.Label43 = new Telerik.WinControls.UI.RadLabel();
            this.Frame8 = new Telerik.WinControls.UI.RadGroupBox();
            this.chbFExtrangero = new Telerik.WinControls.UI.RadCheckBox();
            this.lbFPoblacion = new Telerik.WinControls.UI.RadTextBox();
            this.Label66 = new Telerik.WinControls.UI.RadLabel();
            this.Label69 = new Telerik.WinControls.UI.RadLabel();
            this.lbFpais = new Telerik.WinControls.UI.RadTextBox();
            this.Label73 = new Telerik.WinControls.UI.RadLabel();
            this.Frame11 = new Telerik.WinControls.UI.RadGroupBox();
            this.sprContactos = new UpgradeHelpers.Spread.FpSpread();
            this.lbNIFContacto = new Telerik.WinControls.UI.RadLabel();
            this.lbPNIFContacto = new Telerik.WinControls.UI.RadTextBox();
            this.lbFCParent = new Telerik.WinControls.UI.RadTextBox();
            this.lbFCTelf2 = new Telerik.WinControls.UI.RadTextBox();
            this.lbFCTelf1 = new Telerik.WinControls.UI.RadTextBox();
            this.lbFCCodPostal = new Telerik.WinControls.UI.RadTextBox();
            this.lbFCDomicilio = new Telerik.WinControls.UI.RadTextBox();
            this.lbFCNombre = new Telerik.WinControls.UI.RadTextBox();
            this.lbFCApel2 = new Telerik.WinControls.UI.RadTextBox();
            this.lbFCApel1 = new Telerik.WinControls.UI.RadTextBox();
            this.Label34 = new Telerik.WinControls.UI.RadLabel();
            this.Label35 = new Telerik.WinControls.UI.RadLabel();
            this.Label36 = new Telerik.WinControls.UI.RadLabel();
            this.Label37 = new Telerik.WinControls.UI.RadLabel();
            this.Label38 = new Telerik.WinControls.UI.RadLabel();
            this.lbFCPob = new Telerik.WinControls.UI.RadTextBox();
            this.Label41 = new Telerik.WinControls.UI.RadLabel();
            this.Label42 = new Telerik.WinControls.UI.RadLabel();
            this.Label51 = new Telerik.WinControls.UI.RadLabel();
            this.Label52 = new Telerik.WinControls.UI.RadLabel();
            this._SSTab1_TabPage4 = new Telerik.WinControls.UI.RadPageViewPage();
            this.frmGradoDependencia = new Telerik.WinControls.UI.RadGroupBox();
            this.tbOtrasCircunstanciasGradoDep = new Telerik.WinControls.UI.RadTextBoxControl();
            this._lbGradoDep_5 = new Telerik.WinControls.UI.RadTextBox();
            this._lbGradoDep_4 = new Telerik.WinControls.UI.RadTextBox();
            this._lbGradoDep_3 = new Telerik.WinControls.UI.RadTextBox();
            this._lbGradoDep_2 = new Telerik.WinControls.UI.RadTextBox();
            this._lbGradoDep_1 = new Telerik.WinControls.UI.RadTextBox();
            this._lbGradoDep_0 = new Telerik.WinControls.UI.RadTextBox();
            this._lbDependencia_3 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_5 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_4 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_2 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_1 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_0 = new Telerik.WinControls.UI.RadLabel();
            this.fraColectivos = new Telerik.WinControls.UI.RadGroupBox();
            this.lbCategEspecial = new Telerik.WinControls.UI.RadTextBox();
            this.lbParentesco = new Telerik.WinControls.UI.RadTextBox();
            this.lbNumTrabajador = new Telerik.WinControls.UI.RadTextBox();
            this.Label80 = new Telerik.WinControls.UI.RadLabel();
            this.Label79 = new Telerik.WinControls.UI.RadLabel();
            this.Label77 = new Telerik.WinControls.UI.RadLabel();
            this._SSTab1_TabPage5 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Check1 = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame18 = new Telerik.WinControls.UI.RadGroupBox();
            this.LbFechaResolucion2 = new Telerik.WinControls.UI.RadTextBox();
            this.LbExpediente2 = new Telerik.WinControls.UI.RadTextBox();
            this.LbJuzgado2 = new Telerik.WinControls.UI.RadTextBox();
            this.Label91 = new Telerik.WinControls.UI.RadLabel();
            this.Label90 = new Telerik.WinControls.UI.RadLabel();
            this.Label89 = new Telerik.WinControls.UI.RadLabel();
            this.Frame17 = new Telerik.WinControls.UI.RadGroupBox();
            this.LbFechaResolucion1 = new Telerik.WinControls.UI.RadTextBox();
            this.LbExpediente1 = new Telerik.WinControls.UI.RadTextBox();
            this.LbJuzgado1 = new Telerik.WinControls.UI.RadTextBox();
            this.Label88 = new Telerik.WinControls.UI.RadLabel();
            this.Label87 = new Telerik.WinControls.UI.RadLabel();
            this.Label85 = new Telerik.WinControls.UI.RadLabel();
            this.lblTituloFechaUltMod = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaUltMod = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            this._SSTab1_TabPage0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPApe1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPApe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPFNac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPSexo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPNIF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPCodPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTelf1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTelf2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPEstCivil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPCatSoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPFUltMov)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPPob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTelf3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTarjetaEuropea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTarjetaSanitaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCipAutonomico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbLPaisNac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPExitus)).BeginInit();
            this.frmPExitus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbPExitus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPFFallec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPExtr)).BeginInit();
            this.frmPExtr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbPExtr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPExDireccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbExPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPPais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
            this.Frame9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbAutorizacion_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbAutorizacion_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame13)).BeginInit();
            this.Frame13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbObservaciones)).BeginInit();
            this._SSTab1_TabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sprhistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprhistoria.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbPIngres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUHospit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblcama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._frmestado_0)).BeginInit();
            this._frmestado_0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbEstado_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbEstado_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbEstado_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFPrest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUniPet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUbiFisi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            this._SSTab1_TabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdConsultaEntidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame15)).BeginInit();
            this.Frame15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkZona)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbPensionista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCodCIAS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREInspeccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREFechaCaducidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREFechaAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRECobertura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENPoliza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRESociedad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicador_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicador_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENomTitular)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPrivado)).BeginInit();
            this.frmPrivado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame16)).BeginInit();
            this.Frame16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbREPropioPac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame10)).BeginInit();
            this.Frame10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbREExtrangero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREExDireccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREEXtpoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREPais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREApe1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREApe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENIF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRECodPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmRERegimen)).BeginInit();
            this.frmRERegimen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_1)).BeginInit();
            this._SSTab1_TabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame14)).BeginInit();
            this.Frame14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbTutor_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbTutor_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).BeginInit();
            this.Frame12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Option1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Option2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).BeginInit();
            this.Frame8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbFExtrangero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFpais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame11)).BeginInit();
            this.Frame11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sprContactos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprContactos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNIFContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPNIFContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCParent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCTelf2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCTelf1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCCodPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCApel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCApel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCPob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
            this._SSTab1_TabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmGradoDependencia)).BeginInit();
            this.frmGradoDependencia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOtrasCircunstanciasGradoDep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraColectivos)).BeginInit();
            this.fraColectivos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbCategEspecial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbParentesco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNumTrabajador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
            this._SSTab1_TabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Check1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame18)).BeginInit();
            this.Frame18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechaResolucion2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbExpediente2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbJuzgado2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame17)).BeginInit();
            this.Frame17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechaResolucion1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbExpediente1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbJuzgado1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTituloFechaUltMod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaUltMod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCerrar.Location = new System.Drawing.Point(543, 472);
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(81, 32);
            this.cbCerrar.TabIndex = 1;
            this.cbCerrar.Text = "Ce&rrar";
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this._SSTab1_TabPage0);
            this.SSTab1.Controls.Add(this._SSTab1_TabPage1);
            this.SSTab1.Controls.Add(this._SSTab1_TabPage2);
            this.SSTab1.Controls.Add(this._SSTab1_TabPage3);
            this.SSTab1.Controls.Add(this._SSTab1_TabPage4);
            this.SSTab1.Controls.Add(this._SSTab1_TabPage5);
            this.SSTab1.ItemSize = new System.Drawing.Size(104, 26);
            this.SSTab1.Location = new System.Drawing.Point(0, 4);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SelectedPage = this._SSTab1_TabPage3;
            this.SSTab1.Size = new System.Drawing.Size(635, 472);
            this.SSTab1.TabIndex = 0;
            // 
            // _SSTab1_TabPage0
            // 
            this._SSTab1_TabPage0.Controls.Add(this.Label3);
            this._SSTab1_TabPage0.Controls.Add(this.Label9);
            this._SSTab1_TabPage0.Controls.Add(this.Label8);
            this._SSTab1_TabPage0.Controls.Add(this.Label7);
            this._SSTab1_TabPage0.Controls.Add(this.Label6);
            this._SSTab1_TabPage0.Controls.Add(this.Label12);
            this._SSTab1_TabPage0.Controls.Add(this.Label13);
            this._SSTab1_TabPage0.Controls.Add(this.Label4);
            this._SSTab1_TabPage0.Controls.Add(this.Label2);
            this._SSTab1_TabPage0.Controls.Add(this.Label1);
            this._SSTab1_TabPage0.Controls.Add(this.Label32);
            this._SSTab1_TabPage0.Controls.Add(this.lbPApe1);
            this._SSTab1_TabPage0.Controls.Add(this.lbPApe2);
            this._SSTab1_TabPage0.Controls.Add(this.lbPNombre);
            this._SSTab1_TabPage0.Controls.Add(this.lbPFNac);
            this._SSTab1_TabPage0.Controls.Add(this.lbPSexo);
            this._SSTab1_TabPage0.Controls.Add(this.lbPNIF);
            this._SSTab1_TabPage0.Controls.Add(this.lbPDomicilio);
            this._SSTab1_TabPage0.Controls.Add(this.lbPCodPostal);
            this._SSTab1_TabPage0.Controls.Add(this.lbPTelf1);
            this._SSTab1_TabPage0.Controls.Add(this.lbPTelf2);
            this._SSTab1_TabPage0.Controls.Add(this.lbPEstCivil);
            this._SSTab1_TabPage0.Controls.Add(this.lbPCatSoc);
            this._SSTab1_TabPage0.Controls.Add(this.lbPFUltMov);
            this._SSTab1_TabPage0.Controls.Add(this.Label67);
            this._SSTab1_TabPage0.Controls.Add(this.lbPPob);
            this._SSTab1_TabPage0.Controls.Add(this.Label96);
            this._SSTab1_TabPage0.Controls.Add(this.Label97);
            this._SSTab1_TabPage0.Controls.Add(this.lbPTelf3);
            this._SSTab1_TabPage0.Controls.Add(this.Label74);
            this._SSTab1_TabPage0.Controls.Add(this.Label75);
            this._SSTab1_TabPage0.Controls.Add(this.lbPTarjetaEuropea);
            this._SSTab1_TabPage0.Controls.Add(this.lbPTarjetaSanitaria);
            this._SSTab1_TabPage0.Controls.Add(this.Label86);
            this._SSTab1_TabPage0.Controls.Add(this.Label92);
            this._SSTab1_TabPage0.Controls.Add(this.lbCipAutonomico);
            this._SSTab1_TabPage0.Controls.Add(this.Label93);
            this._SSTab1_TabPage0.Controls.Add(this.LbLPaisNac);
            this._SSTab1_TabPage0.Controls.Add(this.frmPExitus);
            this._SSTab1_TabPage0.Controls.Add(this.frmPExtr);
            this._SSTab1_TabPage0.Controls.Add(this.Frame9);
            this._SSTab1_TabPage0.Controls.Add(this.Frame13);
            this._SSTab1_TabPage0.ItemSize = new System.Drawing.SizeF(58F, 28F);
            this._SSTab1_TabPage0.Location = new System.Drawing.Point(10, 37);
            this._SSTab1_TabPage0.Name = "_SSTab1_TabPage0";
            this._SSTab1_TabPage0.Size = new System.Drawing.Size(614, 424);
            this._SSTab1_TabPage0.Text = "Paciente";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(12, 56);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(60, 18);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "Fecha Nac:";
            // 
            // Label9
            // 
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Location = new System.Drawing.Point(242, 160);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(94, 18);
            this.Label9.TabIndex = 6;
            this.Label9.Text = "Valoraci�n Social:";
            // 
            // Label8
            // 
            this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label8.Location = new System.Drawing.Point(12, 160);
            this.Label8.Name = "Label8";
            this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label8.Size = new System.Drawing.Size(64, 18);
            this.Label8.TabIndex = 7;
            this.Label8.Text = "Estado civil:";
            // 
            // Label7
            // 
            this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label7.Location = new System.Drawing.Point(243, 137);
            this.Label7.Name = "Label7";
            this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label7.Size = new System.Drawing.Size(62, 18);
            this.Label7.TabIndex = 8;
            this.Label7.Text = "Tel�fono 2:";
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Location = new System.Drawing.Point(12, 137);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(62, 18);
            this.Label6.TabIndex = 9;
            this.Label6.Text = "Tel�fono 1:";
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Location = new System.Drawing.Point(12, 85);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(56, 18);
            this.Label12.TabIndex = 10;
            this.Label12.Text = "Domicilio:";
            // 
            // Label13
            // 
            this.Label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label13.Location = new System.Drawing.Point(152, 56);
            this.Label13.Name = "Label13";
            this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label13.Size = new System.Drawing.Size(121, 18);
            this.Label13.TabIndex = 11;
            this.Label13.Text = "N.I.F./Otro documento:";
            this.Label13.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(348, 35);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(32, 18);
            this.Label4.TabIndex = 12;
            this.Label4.Text = "Sexo:";
            this.Label4.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(12, 35);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(50, 18);
            this.Label2.TabIndex = 13;
            this.Label2.Text = "Nombre:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(12, 9);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(59, 18);
            this.Label1.TabIndex = 14;
            this.Label1.Text = "Apellido 1:";
            // 
            // Label32
            // 
            this.Label32.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label32.Location = new System.Drawing.Point(321, 9);
            this.Label32.Name = "Label32";
            this.Label32.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label32.Size = new System.Drawing.Size(59, 18);
            this.Label32.TabIndex = 15;
            this.Label32.Text = "Apellido 2:";
            this.Label32.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbPApe1
            // 
            this.lbPApe1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPApe1.Enabled = false;
            this.lbPApe1.Location = new System.Drawing.Point(75, 9);
            this.lbPApe1.Name = "lbPApe1";
            this.lbPApe1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPApe1.Size = new System.Drawing.Size(225, 20);
            this.lbPApe1.TabIndex = 16;
            // 
            // lbPApe2
            // 
            this.lbPApe2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPApe2.Enabled = false;
            this.lbPApe2.Location = new System.Drawing.Point(386, 8);
            this.lbPApe2.Name = "lbPApe2";
            this.lbPApe2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPApe2.Size = new System.Drawing.Size(221, 20);
            this.lbPApe2.TabIndex = 17;
            // 
            // lbPNombre
            // 
            this.lbPNombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPNombre.Enabled = false;
            this.lbPNombre.Location = new System.Drawing.Point(75, 33);
            this.lbPNombre.Name = "lbPNombre";
            this.lbPNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPNombre.Size = new System.Drawing.Size(225, 20);
            this.lbPNombre.TabIndex = 18;
            // 
            // lbPFNac
            // 
            this.lbPFNac.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPFNac.Enabled = false;
            this.lbPFNac.Location = new System.Drawing.Point(75, 56);
            this.lbPFNac.Name = "lbPFNac";
            this.lbPFNac.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPFNac.Size = new System.Drawing.Size(77, 20);
            this.lbPFNac.TabIndex = 19;
            // 
            // lbPSexo
            // 
            this.lbPSexo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPSexo.Enabled = false;
            this.lbPSexo.Location = new System.Drawing.Point(386, 34);
            this.lbPSexo.Name = "lbPSexo";
            this.lbPSexo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPSexo.Size = new System.Drawing.Size(82, 20);
            this.lbPSexo.TabIndex = 20;
            // 
            // lbPNIF
            // 
            this.lbPNIF.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPNIF.Enabled = false;
            this.lbPNIF.Location = new System.Drawing.Point(273, 56);
            this.lbPNIF.Name = "lbPNIF";
            this.lbPNIF.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPNIF.Size = new System.Drawing.Size(118, 20);
            this.lbPNIF.TabIndex = 21;
            // 
            // lbPDomicilio
            // 
            this.lbPDomicilio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPDomicilio.Enabled = false;
            this.lbPDomicilio.Location = new System.Drawing.Point(75, 85);
            this.lbPDomicilio.Name = "lbPDomicilio";
            this.lbPDomicilio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPDomicilio.Size = new System.Drawing.Size(532, 20);
            this.lbPDomicilio.TabIndex = 22;
            // 
            // lbPCodPostal
            // 
            this.lbPCodPostal.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPCodPostal.Enabled = false;
            this.lbPCodPostal.Location = new System.Drawing.Point(92, 110);
            this.lbPCodPostal.Name = "lbPCodPostal";
            this.lbPCodPostal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPCodPostal.Size = new System.Drawing.Size(57, 20);
            this.lbPCodPostal.TabIndex = 23;
            // 
            // lbPTelf1
            // 
            this.lbPTelf1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPTelf1.Enabled = false;
            this.lbPTelf1.Location = new System.Drawing.Point(92, 136);
            this.lbPTelf1.Name = "lbPTelf1";
            this.lbPTelf1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPTelf1.Size = new System.Drawing.Size(145, 20);
            this.lbPTelf1.TabIndex = 24;
            // 
            // lbPTelf2
            // 
            this.lbPTelf2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPTelf2.Enabled = false;
            this.lbPTelf2.Location = new System.Drawing.Point(306, 136);
            this.lbPTelf2.Name = "lbPTelf2";
            this.lbPTelf2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPTelf2.Size = new System.Drawing.Size(149, 20);
            this.lbPTelf2.TabIndex = 25;
            // 
            // lbPEstCivil
            // 
            this.lbPEstCivil.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPEstCivil.Enabled = false;
            this.lbPEstCivil.Location = new System.Drawing.Point(92, 159);
            this.lbPEstCivil.Name = "lbPEstCivil";
            this.lbPEstCivil.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPEstCivil.Size = new System.Drawing.Size(89, 20);
            this.lbPEstCivil.TabIndex = 26;
            // 
            // lbPCatSoc
            // 
            this.lbPCatSoc.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPCatSoc.Enabled = false;
            this.lbPCatSoc.Location = new System.Drawing.Point(337, 159);
            this.lbPCatSoc.Name = "lbPCatSoc";
            this.lbPCatSoc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPCatSoc.Size = new System.Drawing.Size(270, 20);
            this.lbPCatSoc.TabIndex = 27;
            // 
            // lbPFUltMov
            // 
            this.lbPFUltMov.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPFUltMov.Enabled = false;
            this.lbPFUltMov.Location = new System.Drawing.Point(152, 183);
            this.lbPFUltMov.Name = "lbPFUltMov";
            this.lbPFUltMov.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPFUltMov.Size = new System.Drawing.Size(73, 20);
            this.lbPFUltMov.TabIndex = 29;
            // 
            // Label67
            // 
            this.Label67.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label67.Location = new System.Drawing.Point(12, 184);
            this.Label67.Name = "Label67";
            this.Label67.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label67.Size = new System.Drawing.Size(136, 18);
            this.Label67.TabIndex = 32;
            this.Label67.Text = "Fecha �ltimo movimiento:";
            // 
            // lbPPob
            // 
            this.lbPPob.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPPob.Enabled = false;
            this.lbPPob.Location = new System.Drawing.Point(224, 110);
            this.lbPPob.Name = "lbPPob";
            this.lbPPob.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPPob.Size = new System.Drawing.Size(383, 20);
            this.lbPPob.TabIndex = 55;
            // 
            // Label96
            // 
            this.Label96.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label96.Location = new System.Drawing.Point(160, 111);
            this.Label96.Name = "Label96";
            this.Label96.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label96.Size = new System.Drawing.Size(58, 18);
            this.Label96.TabIndex = 56;
            this.Label96.Text = "Poblaci�n:";
            // 
            // Label97
            // 
            this.Label97.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label97.Location = new System.Drawing.Point(12, 111);
            this.Label97.Name = "Label97";
            this.Label97.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label97.Size = new System.Drawing.Size(65, 18);
            this.Label97.TabIndex = 57;
            this.Label97.Text = "C�d. postal:";
            // 
            // lbPTelf3
            // 
            this.lbPTelf3.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPTelf3.Enabled = false;
            this.lbPTelf3.Location = new System.Drawing.Point(498, 136);
            this.lbPTelf3.Name = "lbPTelf3";
            this.lbPTelf3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPTelf3.Size = new System.Drawing.Size(109, 20);
            this.lbPTelf3.TabIndex = 167;
            // 
            // Label74
            // 
            this.Label74.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label74.Location = new System.Drawing.Point(460, 137);
            this.Label74.Name = "Label74";
            this.Label74.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label74.Size = new System.Drawing.Size(36, 18);
            this.Label74.TabIndex = 168;
            this.Label74.Text = "M�vil:";
            // 
            // Label75
            // 
            this.Label75.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label75.Location = new System.Drawing.Point(12, 210);
            this.Label75.Name = "Label75";
            this.Label75.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label75.Size = new System.Drawing.Size(92, 18);
            this.Label75.TabIndex = 30;
            this.Label75.Text = "T. Sanit. Europea:";
            // 
            // lbPTarjetaEuropea
            // 
            this.lbPTarjetaEuropea.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPTarjetaEuropea.Enabled = false;
            this.lbPTarjetaEuropea.Location = new System.Drawing.Point(111, 209);
            this.lbPTarjetaEuropea.Name = "lbPTarjetaEuropea";
            this.lbPTarjetaEuropea.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPTarjetaEuropea.Size = new System.Drawing.Size(262, 20);
            this.lbPTarjetaEuropea.TabIndex = 31;
            // 
            // lbPTarjetaSanitaria
            // 
            this.lbPTarjetaSanitaria.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPTarjetaSanitaria.Enabled = false;
            this.lbPTarjetaSanitaria.Location = new System.Drawing.Point(337, 183);
            this.lbPTarjetaSanitaria.Name = "lbPTarjetaSanitaria";
            this.lbPTarjetaSanitaria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPTarjetaSanitaria.Size = new System.Drawing.Size(270, 20);
            this.lbPTarjetaSanitaria.TabIndex = 201;
            // 
            // Label86
            // 
            this.Label86.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label86.Location = new System.Drawing.Point(242, 184);
            this.Label86.Name = "Label86";
            this.Label86.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label86.Size = new System.Drawing.Size(89, 18);
            this.Label86.TabIndex = 202;
            this.Label86.Text = "Tarjeta Sanitaria:";
            // 
            // Label92
            // 
            this.Label92.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label92.Location = new System.Drawing.Point(392, 57);
            this.Label92.Name = "Label92";
            this.Label92.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label92.Size = new System.Drawing.Size(90, 18);
            this.Label92.TabIndex = 219;
            this.Label92.Text = "CIP Auton�mico:";
            // 
            // lbCipAutonomico
            // 
            this.lbCipAutonomico.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbCipAutonomico.Enabled = false;
            this.lbCipAutonomico.Location = new System.Drawing.Point(483, 56);
            this.lbCipAutonomico.Name = "lbCipAutonomico";
            this.lbCipAutonomico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbCipAutonomico.Size = new System.Drawing.Size(124, 20);
            this.lbCipAutonomico.TabIndex = 220;
            // 
            // Label93
            // 
            this.Label93.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label93.Location = new System.Drawing.Point(378, 210);
            this.Label93.Name = "Label93";
            this.Label93.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label93.Size = new System.Drawing.Size(89, 18);
            this.Label93.TabIndex = 221;
            this.Label93.Text = "Pa�s Nacimiento:";
            // 
            // LbLPaisNac
            // 
            this.LbLPaisNac.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbLPaisNac.Enabled = false;
            this.LbLPaisNac.Location = new System.Drawing.Point(468, 209);
            this.LbLPaisNac.Name = "LbLPaisNac";
            this.LbLPaisNac.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbLPaisNac.Size = new System.Drawing.Size(139, 20);
            this.LbLPaisNac.TabIndex = 222;
            // 
            // frmPExitus
            // 
            this.frmPExitus.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmPExitus.Controls.Add(this.chbPExitus);
            this.frmPExitus.Controls.Add(this.lbPFFallec);
            this.frmPExitus.Controls.Add(this.Label10);
            this.frmPExitus.Enabled = false;
            this.frmPExitus.HeaderText = "Exitus";
            this.frmPExitus.Location = new System.Drawing.Point(220, 297);
            this.frmPExitus.Name = "frmPExitus";
            this.frmPExitus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmPExitus.Size = new System.Drawing.Size(390, 40);
            this.frmPExitus.TabIndex = 3;
            this.frmPExitus.Text = "Exitus";
            // 
            // chbPExitus
            // 
            this.chbPExitus.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbPExitus.Location = new System.Drawing.Point(56, 15);
            this.chbPExitus.Name = "chbPExitus";
            this.chbPExitus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbPExitus.Size = new System.Drawing.Size(49, 18);
            this.chbPExitus.TabIndex = 4;
            this.chbPExitus.Text = "Exitus";
            // 
            // lbPFFallec
            // 
            this.lbPFFallec.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPFFallec.Enabled = false;
            this.lbPFFallec.Location = new System.Drawing.Point(270, 14);
            this.lbPFFallec.Name = "lbPFFallec";
            this.lbPFFallec.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPFFallec.Size = new System.Drawing.Size(105, 20);
            this.lbPFFallec.TabIndex = 28;
            // 
            // Label10
            // 
            this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label10.Location = new System.Drawing.Point(166, 15);
            this.Label10.Name = "Label10";
            this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label10.Size = new System.Drawing.Size(105, 18);
            this.Label10.TabIndex = 5;
            this.Label10.Text = "Fecha fallecimiento:";
            // 
            // frmPExtr
            // 
            this.frmPExtr.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmPExtr.Controls.Add(this.chbPExtr);
            this.frmPExtr.Controls.Add(this.lbPExDireccion);
            this.frmPExtr.Controls.Add(this.Label76);
            this.frmPExtr.Controls.Add(this.Label62);
            this.frmPExtr.Controls.Add(this.lbExPoblacion);
            this.frmPExtr.Controls.Add(this.lbPPais);
            this.frmPExtr.Controls.Add(this.Label102);
            this.frmPExtr.Enabled = false;
            this.frmPExtr.HeaderText = "Residencia en el extranjero";
            this.frmPExtr.Location = new System.Drawing.Point(3, 229);
            this.frmPExtr.Name = "frmPExtr";
            this.frmPExtr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmPExtr.Size = new System.Drawing.Size(607, 66);
            this.frmPExtr.TabIndex = 60;
            this.frmPExtr.Text = "Residencia en el extranjero";
            // 
            // chbPExtr
            // 
            this.chbPExtr.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbPExtr.Location = new System.Drawing.Point(6, 20);
            this.chbPExtr.Name = "chbPExtr";
            this.chbPExtr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbPExtr.Size = new System.Drawing.Size(127, 18);
            this.chbPExtr.TabIndex = 61;
            this.chbPExtr.Text = "Residencia extranjero";
            // 
            // lbPExDireccion
            // 
            this.lbPExDireccion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPExDireccion.Enabled = false;
            this.lbPExDireccion.Location = new System.Drawing.Point(63, 40);
            this.lbPExDireccion.Name = "lbPExDireccion";
            this.lbPExDireccion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPExDireccion.Size = new System.Drawing.Size(532, 20);
            this.lbPExDireccion.TabIndex = 182;
            // 
            // Label76
            // 
            this.Label76.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label76.Location = new System.Drawing.Point(4, 41);
            this.Label76.Name = "Label76";
            this.Label76.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label76.Size = new System.Drawing.Size(55, 18);
            this.Label76.TabIndex = 181;
            this.Label76.Text = "Direcci�n:";
            // 
            // Label62
            // 
            this.Label62.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label62.Location = new System.Drawing.Point(346, 20);
            this.Label62.Name = "Label62";
            this.Label62.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label62.Size = new System.Drawing.Size(58, 18);
            this.Label62.TabIndex = 106;
            this.Label62.Text = "Poblaci�n:";
            // 
            // lbExPoblacion
            // 
            this.lbExPoblacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbExPoblacion.Enabled = false;
            this.lbExPoblacion.Location = new System.Drawing.Point(410, 19);
            this.lbExPoblacion.Name = "lbExPoblacion";
            this.lbExPoblacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbExPoblacion.Size = new System.Drawing.Size(185, 20);
            this.lbExPoblacion.TabIndex = 105;
            // 
            // lbPPais
            // 
            this.lbPPais.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPPais.Enabled = false;
            this.lbPPais.Location = new System.Drawing.Point(174, 19);
            this.lbPPais.Name = "lbPPais";
            this.lbPPais.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPPais.Size = new System.Drawing.Size(155, 20);
            this.lbPPais.TabIndex = 63;
            // 
            // Label102
            // 
            this.Label102.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label102.Location = new System.Drawing.Point(140, 21);
            this.Label102.Name = "Label102";
            this.Label102.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label102.Size = new System.Drawing.Size(28, 18);
            this.Label102.TabIndex = 62;
            this.Label102.Text = "Pa�s:";
            // 
            // Frame9
            // 
            this.Frame9.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame9.Controls.Add(this._rbAutorizacion_1);
            this.Frame9.Controls.Add(this._rbAutorizacion_0);
            this.Frame9.Enabled = false;
            this.Frame9.HeaderText = "Autorizaci�n utilizaci�n datos";
            this.Frame9.Location = new System.Drawing.Point(3, 297);
            this.Frame9.Name = "Frame9";
            this.Frame9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame9.Size = new System.Drawing.Size(211, 41);
            this.Frame9.TabIndex = 127;
            this.Frame9.Text = "Autorizaci�n utilizaci�n datos";
            // 
            // _rbAutorizacion_1
            // 
            this._rbAutorizacion_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbAutorizacion_1.Location = new System.Drawing.Point(112, 15);
            this._rbAutorizacion_1.Name = "_rbAutorizacion_1";
            this._rbAutorizacion_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbAutorizacion_1.Size = new System.Drawing.Size(35, 18);
            this._rbAutorizacion_1.TabIndex = 131;
            this._rbAutorizacion_1.Text = "No";
            // 
            // _rbAutorizacion_0
            // 
            this._rbAutorizacion_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbAutorizacion_0.Location = new System.Drawing.Point(40, 15);
            this._rbAutorizacion_0.Name = "_rbAutorizacion_0";
            this._rbAutorizacion_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbAutorizacion_0.Size = new System.Drawing.Size(29, 18);
            this._rbAutorizacion_0.TabIndex = 130;
            this._rbAutorizacion_0.Text = "Si";
            // 
            // Frame13
            // 
            this.Frame13.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame13.Controls.Add(this.tbObservaciones);
            this.Frame13.HeaderText = "Observaciones";
            this.Frame13.Location = new System.Drawing.Point(3, 339);
            this.Frame13.Name = "Frame13";
            this.Frame13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame13.Size = new System.Drawing.Size(607, 83);
            this.Frame13.TabIndex = 169;
            this.Frame13.Text = "Observaciones";
            // 
            // tbObservaciones
            // 
            this.tbObservaciones.AcceptsReturn = true;
            this.tbObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbObservaciones.IsReadOnly = true;
            this.tbObservaciones.Location = new System.Drawing.Point(10, 14);
            this.tbObservaciones.MaxLength = 0;
            this.tbObservaciones.Multiline = true;
            this.tbObservaciones.Name = "tbObservaciones";
            this.tbObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbObservaciones.Size = new System.Drawing.Size(589, 63);
            this.tbObservaciones.TabIndex = 170;
            // 
            // _SSTab1_TabPage1
            // 
            this._SSTab1_TabPage1.Controls.Add(this.Frame1);
            this._SSTab1_TabPage1.Controls.Add(this.Frame2);
            this._SSTab1_TabPage1.Controls.Add(this._frmestado_0);
            this._SSTab1_TabPage1.ItemSize = new System.Drawing.SizeF(90F, 28F);
            this._SSTab1_TabPage1.Location = new System.Drawing.Point(10, 37);
            this._SSTab1_TabPage1.Name = "_SSTab1_TabPage1";
            this._SSTab1_TabPage1.Size = new System.Drawing.Size(614, 424);
            this._SSTab1_TabPage1.Text = "Estado historia";
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.sprhistoria);
            this.Frame1.HeaderText = "Historias disponibles";
            this.Frame1.Location = new System.Drawing.Point(28, 13);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(569, 149);
            this.Frame1.TabIndex = 71;
            this.Frame1.Text = "Historias disponibles";
            // 
            // sprhistoria
            // 
            this.sprhistoria.Location = new System.Drawing.Point(8, 20);
            // 
            // 
            // 
            gridViewTextBoxColumn1.HeaderText = "N� Historia";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 80;
            gridViewTextBoxColumn2.HeaderText = "Contenido historia";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.HeaderText = "Servicio";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 300;
            gridViewTextBoxColumn4.HeaderText = "D";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn5.HeaderText = "E";
            gridViewTextBoxColumn5.Name = "column5";
            this.sprhistoria.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.sprhistoria.MasterTemplate.EnableSorting = false;
            this.sprhistoria.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.sprhistoria.Name = "sprhistoria";
            this.sprhistoria.ReadOnly = true;
            this.sprhistoria.Size = new System.Drawing.Size(556, 119);
            this.sprhistoria.TabIndex = 72;
            this.sprhistoria.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprhistoria_CellClick);
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.chbPIngres);
            this.Frame2.Controls.Add(this.lblUHospit);
            this.Frame2.Controls.Add(this.Label107);
            this.Frame2.Controls.Add(this.Label16);
            this.Frame2.Controls.Add(this.lblcama);
            this.Frame2.Enabled = false;
            this.Frame2.HeaderText = "Ingreso Paciente";
            this.Frame2.Location = new System.Drawing.Point(28, 283);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(570, 81);
            this.Frame2.TabIndex = 51;
            this.Frame2.Text = "Ingreso Paciente";
            // 
            // chbPIngres
            // 
            this.chbPIngres.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbPIngres.Location = new System.Drawing.Point(8, 24);
            this.chbPIngres.Name = "chbPIngres";
            this.chbPIngres.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbPIngres.Size = new System.Drawing.Size(115, 18);
            this.chbPIngres.TabIndex = 52;
            this.chbPIngres.Text = "Paciente ingresado";
            // 
            // lblUHospit
            // 
            this.lblUHospit.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblUHospit.Enabled = false;
            this.lblUHospit.Location = new System.Drawing.Point(312, 46);
            this.lblUHospit.Name = "lblUHospit";
            this.lblUHospit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblUHospit.Size = new System.Drawing.Size(249, 20);
            this.lblUHospit.TabIndex = 59;
            // 
            // Label107
            // 
            this.Label107.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label107.Location = new System.Drawing.Point(184, 48);
            this.Label107.Name = "Label107";
            this.Label107.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label107.Size = new System.Drawing.Size(122, 18);
            this.Label107.TabIndex = 58;
            this.Label107.Text = "Unidad hospitalizaci�n:";
            // 
            // Label16
            // 
            this.Label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label16.Location = new System.Drawing.Point(8, 48);
            this.Label16.Name = "Label16";
            this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label16.Size = new System.Drawing.Size(97, 18);
            this.Label16.TabIndex = 54;
            this.Label16.Text = "N� cama paciente:";
            // 
            // lblcama
            // 
            this.lblcama.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblcama.Enabled = false;
            this.lblcama.Location = new System.Drawing.Point(112, 46);
            this.lblcama.Name = "lblcama";
            this.lblcama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblcama.Size = new System.Drawing.Size(49, 20);
            this.lblcama.TabIndex = 53;
            // 
            // _frmestado_0
            // 
            this._frmestado_0.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._frmestado_0.Controls.Add(this._rbEstado_2);
            this._frmestado_0.Controls.Add(this._rbEstado_1);
            this._frmestado_0.Controls.Add(this._rbEstado_0);
            this._frmestado_0.Controls.Add(this.lblFPrest);
            this._frmestado_0.Controls.Add(this.lblUniPet);
            this._frmestado_0.Controls.Add(this.lblUbiFisi);
            this._frmestado_0.Controls.Add(this.Label18);
            this._frmestado_0.Controls.Add(this.Label17);
            this._frmestado_0.Controls.Add(this.Label19);
            this._frmestado_0.Enabled = false;
            this._frmestado_0.HeaderText = "Estado Historia";
            this._frmestado_0.Location = new System.Drawing.Point(28, 175);
            this._frmestado_0.Name = "_frmestado_0";
            this._frmestado_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._frmestado_0.Size = new System.Drawing.Size(570, 97);
            this._frmestado_0.TabIndex = 49;
            this._frmestado_0.Text = "Estado Historia";
            // 
            // _rbEstado_2
            // 
            this._rbEstado_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbEstado_2.Location = new System.Drawing.Point(40, 70);
            this._rbEstado_2.Name = "_rbEstado_2";
            this._rbEstado_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbEstado_2.Size = new System.Drawing.Size(58, 18);
            this._rbEstado_2.TabIndex = 118;
            this._rbEstado_2.Text = "Perdida";
            // 
            // _rbEstado_1
            // 
            this._rbEstado_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbEstado_1.Location = new System.Drawing.Point(40, 45);
            this._rbEstado_1.Name = "_rbEstado_1";
            this._rbEstado_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbEstado_1.Size = new System.Drawing.Size(63, 18);
            this._rbEstado_1.TabIndex = 117;
            this._rbEstado_1.Text = "Prestada";
            // 
            // _rbEstado_0
            // 
            this._rbEstado_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbEstado_0.Location = new System.Drawing.Point(40, 21);
            this._rbEstado_0.Name = "_rbEstado_0";
            this._rbEstado_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbEstado_0.Size = new System.Drawing.Size(73, 18);
            this._rbEstado_0.TabIndex = 116;
            this._rbEstado_0.Text = "Disponible";
            // 
            // lblFPrest
            // 
            this.lblFPrest.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblFPrest.Enabled = false;
            this.lblFPrest.Location = new System.Drawing.Point(216, 69);
            this.lblFPrest.Name = "lblFPrest";
            this.lblFPrest.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFPrest.Size = new System.Drawing.Size(129, 20);
            this.lblFPrest.TabIndex = 126;
            // 
            // lblUniPet
            // 
            this.lblUniPet.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblUniPet.Enabled = false;
            this.lblUniPet.Location = new System.Drawing.Point(216, 45);
            this.lblUniPet.Name = "lblUniPet";
            this.lblUniPet.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblUniPet.Size = new System.Drawing.Size(329, 20);
            this.lblUniPet.TabIndex = 125;
            // 
            // lblUbiFisi
            // 
            this.lblUbiFisi.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblUbiFisi.Enabled = false;
            this.lblUbiFisi.Location = new System.Drawing.Point(216, 21);
            this.lblUbiFisi.Name = "lblUbiFisi";
            this.lblUbiFisi.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblUbiFisi.Size = new System.Drawing.Size(329, 20);
            this.lblUbiFisi.TabIndex = 124;
            // 
            // Label18
            // 
            this.Label18.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label18.Location = new System.Drawing.Point(128, 45);
            this.Label18.Name = "Label18";
            this.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label18.Size = new System.Drawing.Size(88, 18);
            this.Label18.TabIndex = 123;
            this.Label18.Text = "Unidad petici�n:";
            // 
            // Label17
            // 
            this.Label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label17.Location = new System.Drawing.Point(128, 21);
            this.Label17.Name = "Label17";
            this.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label17.Size = new System.Drawing.Size(86, 18);
            this.Label17.TabIndex = 122;
            this.Label17.Text = "Ubicaci�n f�sica:";
            // 
            // Label19
            // 
            this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label19.Location = new System.Drawing.Point(128, 69);
            this.Label19.Name = "Label19";
            this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label19.Size = new System.Drawing.Size(88, 18);
            this.Label19.TabIndex = 50;
            this.Label19.Text = "Fecha pr�stamo:";
            // 
            // _SSTab1_TabPage2
            // 
            this._SSTab1_TabPage2.Controls.Add(this.cmdConsultaEntidades);
            this._SSTab1_TabPage2.Controls.Add(this.Frame3);
            this._SSTab1_TabPage2.Controls.Add(this.frmPrivado);
            this._SSTab1_TabPage2.Controls.Add(this.frmRERegimen);
            this._SSTab1_TabPage2.ItemSize = new System.Drawing.SizeF(119F, 28F);
            this._SSTab1_TabPage2.Location = new System.Drawing.Point(10, 37);
            this._SSTab1_TabPage2.Name = "_SSTab1_TabPage2";
            this._SSTab1_TabPage2.Size = new System.Drawing.Size(614, 424);
            this._SSTab1_TabPage2.Text = "R�gimen econ�mico";
            // 
            // cmdConsultaEntidades
            // 
            this.cmdConsultaEntidades.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdConsultaEntidades.Location = new System.Drawing.Point(499, 16);
            this.cmdConsultaEntidades.Name = "cmdConsultaEntidades";
            this.cmdConsultaEntidades.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdConsultaEntidades.Size = new System.Drawing.Size(109, 27);
            this.cmdConsultaEntidades.TabIndex = 223;
            this.cmdConsultaEntidades.Text = "Consulta";
            this.cmdConsultaEntidades.Click += new System.EventHandler(this.cmdConsultaEntidades_Click);
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.Frame6);
            this.Frame3.Controls.Add(this.Frame5);
            this.Frame3.Controls.Add(this.Frame4);
            this.Frame3.HeaderText = "";
            this.Frame3.Location = new System.Drawing.Point(4, 48);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(606, 185);
            this.Frame3.TabIndex = 73;
            // 
            // Frame6
            // 
            this.Frame6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame6.Controls.Add(this.Frame15);
            this.Frame6.Controls.Add(this.Frame7);
            this.Frame6.Controls.Add(this.lbCodCIAS);
            this.Frame6.Controls.Add(this.lbRENSS);
            this.Frame6.Controls.Add(this.lbREInspeccion);
            this.Frame6.Controls.Add(this.Label44);
            this.Frame6.Controls.Add(this.Label39);
            this.Frame6.Controls.Add(this.Label31);
            this.Frame6.HeaderText = "Seguridad social";
            this.Frame6.Location = new System.Drawing.Point(7, 11);
            this.Frame6.Name = "Frame6";
            this.Frame6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame6.Size = new System.Drawing.Size(267, 167);
            this.Frame6.TabIndex = 81;
            this.Frame6.Text = "Seguridad social";
            // 
            // Frame15
            // 
            this.Frame15.Controls.Add(this.chkZona);
            this.Frame15.Cursor = System.Windows.Forms.Cursors.Default;
            this.Frame15.Enabled = false;
            this.Frame15.Location = new System.Drawing.Point(102, 90);
            this.Frame15.Name = "Frame15";
            this.Frame15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame15.Size = new System.Drawing.Size(117, 21);
            this.Frame15.TabIndex = 176;
            // 
            // chkZona
            // 
            this.chkZona.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkZona.Location = new System.Drawing.Point(6, 2);
            this.chkZona.Name = "chkZona";
            this.chkZona.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkZona.Size = new System.Drawing.Size(99, 18);
            this.chkZona.TabIndex = 177;
            this.chkZona.Text = "Dentro de Zona";
            // 
            // Frame7
            // 
            this.Frame7.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame7.Controls.Add(this.rbPensionista);
            this.Frame7.Controls.Add(this.rbActivo);
            this.Frame7.Enabled = false;
            this.Frame7.HeaderText = "";
            this.Frame7.Location = new System.Drawing.Point(7, 92);
            this.Frame7.Name = "Frame7";
            this.Frame7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame7.Size = new System.Drawing.Size(92, 49);
            this.Frame7.TabIndex = 82;
            // 
            // rbPensionista
            // 
            this.rbPensionista.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbPensionista.Location = new System.Drawing.Point(6, 27);
            this.rbPensionista.Name = "rbPensionista";
            this.rbPensionista.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbPensionista.Size = new System.Drawing.Size(77, 18);
            this.rbPensionista.TabIndex = 84;
            this.rbPensionista.Text = "Pensionista";
            // 
            // rbActivo
            // 
            this.rbActivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbActivo.Location = new System.Drawing.Point(6, 7);
            this.rbActivo.Name = "rbActivo";
            this.rbActivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbActivo.Size = new System.Drawing.Size(51, 18);
            this.rbActivo.TabIndex = 83;
            this.rbActivo.Text = "Activo";
            // 
            // lbCodCIAS
            // 
            this.lbCodCIAS.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbCodCIAS.Enabled = false;
            this.lbCodCIAS.Location = new System.Drawing.Point(115, 63);
            this.lbCodCIAS.Name = "lbCodCIAS";
            this.lbCodCIAS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbCodCIAS.Size = new System.Drawing.Size(101, 20);
            this.lbCodCIAS.TabIndex = 90;
            // 
            // lbRENSS
            // 
            this.lbRENSS.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRENSS.Enabled = false;
            this.lbRENSS.Location = new System.Drawing.Point(115, 41);
            this.lbRENSS.Name = "lbRENSS";
            this.lbRENSS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRENSS.Size = new System.Drawing.Size(144, 20);
            this.lbRENSS.TabIndex = 89;
            // 
            // lbREInspeccion
            // 
            this.lbREInspeccion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREInspeccion.Enabled = false;
            this.lbREInspeccion.Location = new System.Drawing.Point(115, 17);
            this.lbREInspeccion.Name = "lbREInspeccion";
            this.lbREInspeccion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREInspeccion.Size = new System.Drawing.Size(144, 20);
            this.lbREInspeccion.TabIndex = 88;
            // 
            // Label44
            // 
            this.Label44.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label44.Location = new System.Drawing.Point(7, 41);
            this.Label44.Name = "Label44";
            this.Label44.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label44.Size = new System.Drawing.Size(108, 18);
            this.Label44.TabIndex = 87;
            this.Label44.Text = "N� Seguridad Social:";
            // 
            // Label39
            // 
            this.Label39.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label39.Location = new System.Drawing.Point(7, 17);
            this.Label39.Name = "Label39";
            this.Label39.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label39.Size = new System.Drawing.Size(62, 18);
            this.Label39.TabIndex = 86;
            this.Label39.Text = "Inspecci�n:";
            // 
            // Label31
            // 
            this.Label31.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label31.Location = new System.Drawing.Point(7, 63);
            this.Label31.Name = "Label31";
            this.Label31.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label31.Size = new System.Drawing.Size(32, 18);
            this.Label31.TabIndex = 85;
            this.Label31.Text = "CIAS:";
            // 
            // Frame5
            // 
            this.Frame5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame5.Controls.Add(this.Label83);
            this.Frame5.Controls.Add(this.Label82);
            this.Frame5.Controls.Add(this.Label81);
            this.Frame5.Controls.Add(this.lbREFechaCaducidad);
            this.Frame5.Controls.Add(this.lbREFechaAlta);
            this.Frame5.Controls.Add(this.lbRECobertura);
            this.Frame5.Controls.Add(this.lbRENPoliza);
            this.Frame5.Controls.Add(this.lbRESociedad);
            this.Frame5.Controls.Add(this.Label30);
            this.Frame5.Controls.Add(this.Label14);
            this.Frame5.HeaderText = "Sociedad";
            this.Frame5.Location = new System.Drawing.Point(280, 11);
            this.Frame5.Name = "Frame5";
            this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame5.Size = new System.Drawing.Size(319, 105);
            this.Frame5.TabIndex = 78;
            this.Frame5.Text = "Sociedad";
            // 
            // Label83
            // 
            this.Label83.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label83.Location = new System.Drawing.Point(172, 83);
            this.Label83.Name = "Label83";
            this.Label83.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label83.Size = new System.Drawing.Size(79, 18);
            this.Label83.TabIndex = 197;
            this.Label83.Text = "Fin (Mes/A�o):";
            // 
            // Label82
            // 
            this.Label82.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label82.Location = new System.Drawing.Point(8, 84);
            this.Label82.Name = "Label82";
            this.Label82.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label82.Size = new System.Drawing.Size(91, 18);
            this.Label82.TabIndex = 196;
            this.Label82.Text = "Inicio (Mes/A�o):";
            // 
            // Label81
            // 
            this.Label81.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label81.Location = new System.Drawing.Point(8, 60);
            this.Label81.Name = "Label81";
            this.Label81.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label81.Size = new System.Drawing.Size(59, 18);
            this.Label81.TabIndex = 195;
            this.Label81.Text = "Cobertura:";
            // 
            // lbREFechaCaducidad
            // 
            this.lbREFechaCaducidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREFechaCaducidad.Enabled = false;
            this.lbREFechaCaducidad.Location = new System.Drawing.Point(258, 82);
            this.lbREFechaCaducidad.Name = "lbREFechaCaducidad";
            this.lbREFechaCaducidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREFechaCaducidad.Size = new System.Drawing.Size(54, 20);
            this.lbREFechaCaducidad.TabIndex = 194;
            // 
            // lbREFechaAlta
            // 
            this.lbREFechaAlta.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREFechaAlta.Enabled = false;
            this.lbREFechaAlta.Location = new System.Drawing.Point(99, 82);
            this.lbREFechaAlta.Name = "lbREFechaAlta";
            this.lbREFechaAlta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREFechaAlta.Size = new System.Drawing.Size(54, 20);
            this.lbREFechaAlta.TabIndex = 193;
            // 
            // lbRECobertura
            // 
            this.lbRECobertura.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRECobertura.Enabled = false;
            this.lbRECobertura.Location = new System.Drawing.Point(69, 59);
            this.lbRECobertura.Name = "lbRECobertura";
            this.lbRECobertura.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRECobertura.Size = new System.Drawing.Size(245, 20);
            this.lbRECobertura.TabIndex = 192;
            // 
            // lbRENPoliza
            // 
            this.lbRENPoliza.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRENPoliza.Enabled = false;
            this.lbRENPoliza.Location = new System.Drawing.Point(69, 37);
            this.lbRENPoliza.Name = "lbRENPoliza";
            this.lbRENPoliza.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRENPoliza.Size = new System.Drawing.Size(245, 20);
            this.lbRENPoliza.TabIndex = 101;
            // 
            // lbRESociedad
            // 
            this.lbRESociedad.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRESociedad.Enabled = false;
            this.lbRESociedad.Location = new System.Drawing.Point(69, 14);
            this.lbRESociedad.Name = "lbRESociedad";
            this.lbRESociedad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRESociedad.Size = new System.Drawing.Size(245, 20);
            this.lbRESociedad.TabIndex = 100;
            // 
            // Label30
            // 
            this.Label30.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label30.Location = new System.Drawing.Point(8, 38);
            this.Label30.Name = "Label30";
            this.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label30.Size = new System.Drawing.Size(55, 18);
            this.Label30.TabIndex = 80;
            this.Label30.Text = "N� p�liza:";
            // 
            // Label14
            // 
            this.Label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label14.Location = new System.Drawing.Point(8, 14);
            this.Label14.Name = "Label14";
            this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label14.Size = new System.Drawing.Size(54, 18);
            this.Label14.TabIndex = 79;
            this.Label14.Text = "Sociedad:";
            // 
            // Frame4
            // 
            this.Frame4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame4.Controls.Add(this._rbIndicador_3);
            this.Frame4.Controls.Add(this._rbIndicador_2);
            this.Frame4.Controls.Add(this.Label65);
            this.Frame4.Controls.Add(this.lbRENomTitular);
            this.Frame4.Controls.Add(this.Label5);
            this.Frame4.Enabled = false;
            this.Frame4.HeaderText = "Indicador de Titular/Beneficiario";
            this.Frame4.Location = new System.Drawing.Point(279, 117);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(319, 61);
            this.Frame4.TabIndex = 74;
            this.Frame4.Text = "Indicador de Titular/Beneficiario";
            // 
            // _rbIndicador_3
            // 
            this._rbIndicador_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbIndicador_3.Location = new System.Drawing.Point(6, 40);
            this._rbIndicador_3.Name = "_rbIndicador_3";
            this._rbIndicador_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbIndicador_3.Size = new System.Drawing.Size(78, 18);
            this._rbIndicador_3.TabIndex = 76;
            this._rbIndicador_3.Text = "Beneficiario";
            // 
            // _rbIndicador_2
            // 
            this._rbIndicador_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbIndicador_2.Location = new System.Drawing.Point(6, 16);
            this._rbIndicador_2.Name = "_rbIndicador_2";
            this._rbIndicador_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbIndicador_2.Size = new System.Drawing.Size(52, 18);
            this._rbIndicador_2.TabIndex = 75;
            this._rbIndicador_2.Text = "Titular";
            // 
            // Label65
            // 
            this.Label65.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label65.Location = new System.Drawing.Point(8, 0);
            this.Label65.Name = "Label65";
            this.Label65.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label65.Size = new System.Drawing.Size(166, 18);
            this.Label65.TabIndex = 114;
            this.Label65.Text = "Indicador de Titular/Beneficiario";
            // 
            // lbRENomTitular
            // 
            this.lbRENomTitular.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRENomTitular.Enabled = false;
            this.lbRENomTitular.Location = new System.Drawing.Point(144, 37);
            this.lbRENomTitular.Name = "lbRENomTitular";
            this.lbRENomTitular.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRENomTitular.Size = new System.Drawing.Size(167, 20);
            this.lbRENomTitular.TabIndex = 99;
            // 
            // Label5
            // 
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Location = new System.Drawing.Point(88, 37);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(50, 18);
            this.Label5.TabIndex = 77;
            this.Label5.Text = "Nombre:";
            // 
            // frmPrivado
            // 
            this.frmPrivado.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmPrivado.Controls.Add(this.Frame16);
            this.frmPrivado.Controls.Add(this.Frame10);
            this.frmPrivado.Controls.Add(this.Label21);
            this.frmPrivado.Controls.Add(this.Label22);
            this.frmPrivado.Controls.Add(this.Label20);
            this.frmPrivado.Controls.Add(this.Label23);
            this.frmPrivado.Controls.Add(this.Label24);
            this.frmPrivado.Controls.Add(this.Label25);
            this.frmPrivado.Controls.Add(this.Label33);
            this.frmPrivado.Controls.Add(this.lbREApe1);
            this.frmPrivado.Controls.Add(this.lbREApe2);
            this.frmPrivado.Controls.Add(this.lbRENombre);
            this.frmPrivado.Controls.Add(this.lbRENIF);
            this.frmPrivado.Controls.Add(this.lbREDomicilio);
            this.frmPrivado.Controls.Add(this.lbREPoblacion);
            this.frmPrivado.Controls.Add(this.lbRECodPostal);
            this.frmPrivado.HeaderText = "Privado";
            this.frmPrivado.Location = new System.Drawing.Point(4, 238);
            this.frmPrivado.Name = "frmPrivado";
            this.frmPrivado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmPrivado.Size = new System.Drawing.Size(606, 181);
            this.frmPrivado.TabIndex = 34;
            this.frmPrivado.Text = "Privado";
            // 
            // Frame16
            // 
            this.Frame16.Controls.Add(this.chbREPropioPac);
            this.Frame16.Cursor = System.Windows.Forms.Cursors.Default;
            this.Frame16.Enabled = false;
            this.Frame16.Location = new System.Drawing.Point(462, 16);
            this.Frame16.Name = "Frame16";
            this.Frame16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame16.Size = new System.Drawing.Size(129, 31);
            this.Frame16.TabIndex = 198;
            // 
            // chbREPropioPac
            // 
            this.chbREPropioPac.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbREPropioPac.Location = new System.Drawing.Point(20, 8);
            this.chbREPropioPac.Name = "chbREPropioPac";
            this.chbREPropioPac.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbREPropioPac.Size = new System.Drawing.Size(98, 18);
            this.chbREPropioPac.TabIndex = 199;
            this.chbREPropioPac.Text = "Propio Paciente";
            // 
            // Frame10
            // 
            this.Frame10.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame10.Controls.Add(this.chbREExtrangero);
            this.Frame10.Controls.Add(this.Label78);
            this.Frame10.Controls.Add(this.lbREExDireccion);
            this.Frame10.Controls.Add(this.Label70);
            this.Frame10.Controls.Add(this.lbREEXtpoblacion);
            this.Frame10.Controls.Add(this.Label63);
            this.Frame10.Controls.Add(this.Label99);
            this.Frame10.Controls.Add(this.lbREPais);
            this.Frame10.Enabled = false;
            this.Frame10.HeaderText = "Residencia en el extranjero";
            this.Frame10.Location = new System.Drawing.Point(8, 110);
            this.Frame10.Name = "Frame10";
            this.Frame10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame10.Size = new System.Drawing.Size(591, 65);
            this.Frame10.TabIndex = 64;
            this.Frame10.Text = "Residencia en el extranjero";
            // 
            // chbREExtrangero
            // 
            this.chbREExtrangero.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbREExtrangero.Location = new System.Drawing.Point(6, 15);
            this.chbREExtrangero.Name = "chbREExtrangero";
            this.chbREExtrangero.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbREExtrangero.Size = new System.Drawing.Size(127, 18);
            this.chbREExtrangero.TabIndex = 65;
            this.chbREExtrangero.Text = "Residencia extranjero";
            // 
            // Label78
            // 
            this.Label78.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label78.Location = new System.Drawing.Point(8, 41);
            this.Label78.Name = "Label78";
            this.Label78.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label78.Size = new System.Drawing.Size(55, 18);
            this.Label78.TabIndex = 184;
            this.Label78.Text = "Direcci�n:";
            // 
            // lbREExDireccion
            // 
            this.lbREExDireccion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREExDireccion.Enabled = false;
            this.lbREExDireccion.Location = new System.Drawing.Point(69, 40);
            this.lbREExDireccion.Name = "lbREExDireccion";
            this.lbREExDireccion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREExDireccion.Size = new System.Drawing.Size(514, 20);
            this.lbREExDireccion.TabIndex = 183;
            // 
            // Label70
            // 
            this.Label70.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label70.Location = new System.Drawing.Point(8, 0);
            this.Label70.Name = "Label70";
            this.Label70.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label70.Size = new System.Drawing.Size(140, 18);
            this.Label70.TabIndex = 115;
            this.Label70.Text = "Residencia en el extranjero";
            // 
            // lbREEXtpoblacion
            // 
            this.lbREEXtpoblacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREEXtpoblacion.Enabled = false;
            this.lbREEXtpoblacion.Location = new System.Drawing.Point(424, 16);
            this.lbREEXtpoblacion.Name = "lbREEXtpoblacion";
            this.lbREEXtpoblacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREEXtpoblacion.Size = new System.Drawing.Size(159, 20);
            this.lbREEXtpoblacion.TabIndex = 108;
            // 
            // Label63
            // 
            this.Label63.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label63.Location = new System.Drawing.Point(366, 18);
            this.Label63.Name = "Label63";
            this.Label63.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label63.Size = new System.Drawing.Size(58, 18);
            this.Label63.TabIndex = 107;
            this.Label63.Text = "Poblaci�n:";
            // 
            // Label99
            // 
            this.Label99.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label99.Location = new System.Drawing.Point(134, 16);
            this.Label99.Name = "Label99";
            this.Label99.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label99.Size = new System.Drawing.Size(28, 18);
            this.Label99.TabIndex = 67;
            this.Label99.Text = "Pa�s:";
            // 
            // lbREPais
            // 
            this.lbREPais.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREPais.Enabled = false;
            this.lbREPais.Location = new System.Drawing.Point(171, 14);
            this.lbREPais.Name = "lbREPais";
            this.lbREPais.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREPais.Size = new System.Drawing.Size(175, 20);
            this.lbREPais.TabIndex = 66;
            // 
            // Label21
            // 
            this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label21.Location = new System.Drawing.Point(10, 88);
            this.Label21.Name = "Label21";
            this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label21.Size = new System.Drawing.Size(79, 18);
            this.Label21.TabIndex = 48;
            this.Label21.Text = "C�digo postal:";
            // 
            // Label22
            // 
            this.Label22.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label22.Location = new System.Drawing.Point(193, 89);
            this.Label22.Name = "Label22";
            this.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label22.Size = new System.Drawing.Size(58, 18);
            this.Label22.TabIndex = 47;
            this.Label22.Text = "Poblaci�n:";
            // 
            // Label20
            // 
            this.Label20.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label20.Location = new System.Drawing.Point(10, 64);
            this.Label20.Name = "Label20";
            this.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label20.Size = new System.Drawing.Size(56, 18);
            this.Label20.TabIndex = 46;
            this.Label20.Text = "Domicilio:";
            // 
            // Label23
            // 
            this.Label23.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label23.Location = new System.Drawing.Point(228, 40);
            this.Label23.Name = "Label23";
            this.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label23.Size = new System.Drawing.Size(121, 18);
            this.Label23.TabIndex = 45;
            this.Label23.Text = "N.I.F./Otro documento:";
            this.Label23.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label24
            // 
            this.Label24.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label24.Location = new System.Drawing.Point(10, 40);
            this.Label24.Name = "Label24";
            this.Label24.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label24.Size = new System.Drawing.Size(50, 18);
            this.Label24.TabIndex = 44;
            this.Label24.Text = "Nombre:";
            // 
            // Label25
            // 
            this.Label25.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label25.Location = new System.Drawing.Point(10, 16);
            this.Label25.Name = "Label25";
            this.Label25.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label25.Size = new System.Drawing.Size(59, 18);
            this.Label25.TabIndex = 43;
            this.Label25.Text = "Apellido 1:";
            // 
            // Label33
            // 
            this.Label33.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label33.Location = new System.Drawing.Point(228, 16);
            this.Label33.Name = "Label33";
            this.Label33.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label33.Size = new System.Drawing.Size(59, 18);
            this.Label33.TabIndex = 42;
            this.Label33.Text = "Apellido 2:";
            this.Label33.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbREApe1
            // 
            this.lbREApe1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREApe1.Enabled = false;
            this.lbREApe1.Location = new System.Drawing.Point(69, 16);
            this.lbREApe1.Name = "lbREApe1";
            this.lbREApe1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREApe1.Size = new System.Drawing.Size(153, 20);
            this.lbREApe1.TabIndex = 41;
            // 
            // lbREApe2
            // 
            this.lbREApe2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREApe2.Enabled = false;
            this.lbREApe2.Location = new System.Drawing.Point(293, 16);
            this.lbREApe2.Name = "lbREApe2";
            this.lbREApe2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREApe2.Size = new System.Drawing.Size(153, 20);
            this.lbREApe2.TabIndex = 40;
            // 
            // lbRENombre
            // 
            this.lbRENombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRENombre.Enabled = false;
            this.lbRENombre.Location = new System.Drawing.Point(69, 40);
            this.lbRENombre.Name = "lbRENombre";
            this.lbRENombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRENombre.Size = new System.Drawing.Size(153, 20);
            this.lbRENombre.TabIndex = 39;
            // 
            // lbRENIF
            // 
            this.lbRENIF.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRENIF.Enabled = false;
            this.lbRENIF.Location = new System.Drawing.Point(349, 40);
            this.lbRENIF.Name = "lbRENIF";
            this.lbRENIF.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRENIF.Size = new System.Drawing.Size(97, 20);
            this.lbRENIF.TabIndex = 38;
            // 
            // lbREDomicilio
            // 
            this.lbREDomicilio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREDomicilio.Enabled = false;
            this.lbREDomicilio.Location = new System.Drawing.Point(69, 64);
            this.lbREDomicilio.Name = "lbREDomicilio";
            this.lbREDomicilio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREDomicilio.Size = new System.Drawing.Size(525, 20);
            this.lbREDomicilio.TabIndex = 37;
            // 
            // lbREPoblacion
            // 
            this.lbREPoblacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbREPoblacion.Enabled = false;
            this.lbREPoblacion.Location = new System.Drawing.Point(253, 89);
            this.lbREPoblacion.Name = "lbREPoblacion";
            this.lbREPoblacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbREPoblacion.Size = new System.Drawing.Size(341, 20);
            this.lbREPoblacion.TabIndex = 36;
            // 
            // lbRECodPostal
            // 
            this.lbRECodPostal.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRECodPostal.Enabled = false;
            this.lbRECodPostal.Location = new System.Drawing.Point(92, 88);
            this.lbRECodPostal.Name = "lbRECodPostal";
            this.lbRECodPostal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRECodPostal.Size = new System.Drawing.Size(65, 20);
            this.lbRECodPostal.TabIndex = 35;
            // 
            // frmRERegimen
            // 
            this.frmRERegimen.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmRERegimen.Controls.Add(this._rbRERegimen_2);
            this.frmRERegimen.Controls.Add(this._rbRERegimen_0);
            this.frmRERegimen.Controls.Add(this._rbRERegimen_1);
            this.frmRERegimen.Enabled = false;
            this.frmRERegimen.HeaderText = "R�gimen Econ�mico";
            this.frmRERegimen.Location = new System.Drawing.Point(4, 8);
            this.frmRERegimen.Name = "frmRERegimen";
            this.frmRERegimen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmRERegimen.Size = new System.Drawing.Size(431, 41);
            this.frmRERegimen.TabIndex = 33;
            this.frmRERegimen.Text = "R�gimen Econ�mico";
            // 
            // _rbRERegimen_2
            // 
            this._rbRERegimen_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbRERegimen_2.Location = new System.Drawing.Point(302, 18);
            this._rbRERegimen_2.Name = "_rbRERegimen_2";
            this._rbRERegimen_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbRERegimen_2.Size = new System.Drawing.Size(58, 18);
            this._rbRERegimen_2.TabIndex = 70;
            this._rbRERegimen_2.Text = "Privado";
            // 
            // _rbRERegimen_0
            // 
            this._rbRERegimen_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbRERegimen_0.Location = new System.Drawing.Point(22, 18);
            this._rbRERegimen_0.Name = "_rbRERegimen_0";
            this._rbRERegimen_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbRERegimen_0.Size = new System.Drawing.Size(102, 18);
            this._rbRERegimen_0.TabIndex = 69;
            this._rbRERegimen_0.Text = "Seguridad social";
            // 
            // _rbRERegimen_1
            // 
            this._rbRERegimen_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbRERegimen_1.Location = new System.Drawing.Point(162, 18);
            this._rbRERegimen_1.Name = "_rbRERegimen_1";
            this._rbRERegimen_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbRERegimen_1.Size = new System.Drawing.Size(66, 18);
            this._rbRERegimen_1.TabIndex = 68;
            this._rbRERegimen_1.Text = "Sociedad";
            // 
            // _SSTab1_TabPage3
            // 
            this._SSTab1_TabPage3.Controls.Add(this.Frame14);
            this._SSTab1_TabPage3.Controls.Add(this.Frame12);
            this._SSTab1_TabPage3.Controls.Add(this.Frame8);
            this._SSTab1_TabPage3.Controls.Add(this.Frame11);
            this._SSTab1_TabPage3.Controls.Add(this.lbNIFContacto);
            this._SSTab1_TabPage3.Controls.Add(this.lbPNIFContacto);
            this._SSTab1_TabPage3.Controls.Add(this.lbFCParent);
            this._SSTab1_TabPage3.Controls.Add(this.lbFCTelf2);
            this._SSTab1_TabPage3.Controls.Add(this.lbFCTelf1);
            this._SSTab1_TabPage3.Controls.Add(this.lbFCCodPostal);
            this._SSTab1_TabPage3.Controls.Add(this.lbFCDomicilio);
            this._SSTab1_TabPage3.Controls.Add(this.lbFCNombre);
            this._SSTab1_TabPage3.Controls.Add(this.lbFCApel2);
            this._SSTab1_TabPage3.Controls.Add(this.lbFCApel1);
            this._SSTab1_TabPage3.Controls.Add(this.Label34);
            this._SSTab1_TabPage3.Controls.Add(this.Label35);
            this._SSTab1_TabPage3.Controls.Add(this.Label36);
            this._SSTab1_TabPage3.Controls.Add(this.Label37);
            this._SSTab1_TabPage3.Controls.Add(this.Label38);
            this._SSTab1_TabPage3.Controls.Add(this.lbFCPob);
            this._SSTab1_TabPage3.Controls.Add(this.Label41);
            this._SSTab1_TabPage3.Controls.Add(this.Label42);
            this._SSTab1_TabPage3.Controls.Add(this.Label51);
            this._SSTab1_TabPage3.Controls.Add(this.Label52);
            this._SSTab1_TabPage3.ItemSize = new System.Drawing.SizeF(118F, 28F);
            this._SSTab1_TabPage3.Location = new System.Drawing.Point(10, 37);
            this._SSTab1_TabPage3.Name = "_SSTab1_TabPage3";
            this._SSTab1_TabPage3.Size = new System.Drawing.Size(614, 424);
            this._SSTab1_TabPage3.Text = "Familiar de contacto";
            // 
            // Frame14
            // 
            this.Frame14.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame14.Controls.Add(this._rbTutor_0);
            this.Frame14.Controls.Add(this._rbTutor_1);
            this.Frame14.Enabled = false;
            this.Frame14.HeaderText = "";
            this.Frame14.Location = new System.Drawing.Point(20, 331);
            this.Frame14.Name = "Frame14";
            this.Frame14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame14.Size = new System.Drawing.Size(285, 33);
            this.Frame14.TabIndex = 173;
            // 
            // _rbTutor_0
            // 
            this._rbTutor_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbTutor_0.Location = new System.Drawing.Point(32, 8);
            this._rbTutor_0.Name = "_rbTutor_0";
            this._rbTutor_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbTutor_0.Size = new System.Drawing.Size(76, 18);
            this._rbTutor_0.TabIndex = 175;
            this._rbTutor_0.Text = "Tutor Legal";
            // 
            // _rbTutor_1
            // 
            this._rbTutor_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbTutor_1.Location = new System.Drawing.Point(144, 8);
            this._rbTutor_1.Name = "_rbTutor_1";
            this._rbTutor_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbTutor_1.Size = new System.Drawing.Size(111, 18);
            this._rbTutor_1.TabIndex = 174;
            this._rbTutor_1.Text = "Cuidador Habitual";
            // 
            // Frame12
            // 
            this.Frame12.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame12.Controls.Add(this.Option1);
            this.Frame12.Controls.Add(this.Option2);
            this.Frame12.Controls.Add(this.Label43);
            this.Frame12.Enabled = false;
            this.Frame12.HeaderText = "";
            this.Frame12.Location = new System.Drawing.Point(316, 331);
            this.Frame12.Name = "Frame12";
            this.Frame12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame12.Size = new System.Drawing.Size(277, 33);
            this.Frame12.TabIndex = 161;
            // 
            // Option1
            // 
            this.Option1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Option1.Location = new System.Drawing.Point(152, 8);
            this.Option1.Name = "Option1";
            this.Option1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Option1.Size = new System.Drawing.Size(35, 18);
            this.Option1.TabIndex = 163;
            this.Option1.Text = "No";
            // 
            // Option2
            // 
            this.Option2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Option2.Location = new System.Drawing.Point(94, 8);
            this.Option2.Name = "Option2";
            this.Option2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Option2.Size = new System.Drawing.Size(29, 18);
            this.Option2.TabIndex = 162;
            this.Option2.Text = "Si";
            // 
            // Label43
            // 
            this.Label43.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label43.Location = new System.Drawing.Point(16, 8);
            this.Label43.Name = "Label43";
            this.Label43.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label43.Size = new System.Drawing.Size(72, 18);
            this.Label43.TabIndex = 166;
            this.Label43.Text = "Responsable:";
            // 
            // Frame8
            // 
            this.Frame8.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame8.Controls.Add(this.chbFExtrangero);
            this.Frame8.Controls.Add(this.lbFPoblacion);
            this.Frame8.Controls.Add(this.Label66);
            this.Frame8.Controls.Add(this.Label69);
            this.Frame8.Controls.Add(this.lbFpais);
            this.Frame8.Controls.Add(this.Label73);
            this.Frame8.Enabled = false;
            this.Frame8.HeaderText = "Residencia en el extranjero";
            this.Frame8.Location = new System.Drawing.Point(22, 373);
            this.Frame8.Name = "Frame8";
            this.Frame8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame8.Size = new System.Drawing.Size(571, 41);
            this.Frame8.TabIndex = 135;
            this.Frame8.Text = "Residencia en el extranjero";
            // 
            // chbFExtrangero
            // 
            this.chbFExtrangero.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbFExtrangero.Location = new System.Drawing.Point(8, 16);
            this.chbFExtrangero.Name = "chbFExtrangero";
            this.chbFExtrangero.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbFExtrangero.Size = new System.Drawing.Size(127, 18);
            this.chbFExtrangero.TabIndex = 136;
            this.chbFExtrangero.Text = "Residencia extranjero";
            // 
            // lbFPoblacion
            // 
            this.lbFPoblacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFPoblacion.Location = new System.Drawing.Point(408, 14);
            this.lbFPoblacion.Name = "lbFPoblacion";
            this.lbFPoblacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFPoblacion.Size = new System.Drawing.Size(157, 20);
            this.lbFPoblacion.TabIndex = 142;
            // 
            // Label66
            // 
            this.Label66.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label66.Location = new System.Drawing.Point(350, 15);
            this.Label66.Name = "Label66";
            this.Label66.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label66.Size = new System.Drawing.Size(58, 18);
            this.Label66.TabIndex = 141;
            this.Label66.Text = "Poblaci�n:";
            // 
            // Label69
            // 
            this.Label69.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label69.Location = new System.Drawing.Point(152, 15);
            this.Label69.Name = "Label69";
            this.Label69.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label69.Size = new System.Drawing.Size(28, 18);
            this.Label69.TabIndex = 139;
            this.Label69.Text = "Pa�s:";
            // 
            // lbFpais
            // 
            this.lbFpais.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFpais.Location = new System.Drawing.Point(185, 14);
            this.lbFpais.Name = "lbFpais";
            this.lbFpais.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFpais.Size = new System.Drawing.Size(157, 20);
            this.lbFpais.TabIndex = 138;
            // 
            // Label73
            // 
            this.Label73.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label73.Location = new System.Drawing.Point(8, 0);
            this.Label73.Name = "Label73";
            this.Label73.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label73.Size = new System.Drawing.Size(140, 18);
            this.Label73.TabIndex = 137;
            this.Label73.Text = "Residencia en el extranjero";
            // 
            // Frame11
            // 
            this.Frame11.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame11.Controls.Add(this.sprContactos);
            this.Frame11.HeaderText = "Personas de Contacto";
            this.Frame11.Location = new System.Drawing.Point(10, 5);
            this.Frame11.Name = "Frame11";
            this.Frame11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame11.Size = new System.Drawing.Size(611, 151);
            this.Frame11.TabIndex = 133;
            this.Frame11.Text = "Personas de Contacto";
            // 
            // sprContactos
            // 
            this.sprContactos.Location = new System.Drawing.Point(12, 24);
            // 
            // 
            // 
            gridViewTextBoxColumn6.HeaderText = "Nombre y Apellidos";
            gridViewTextBoxColumn6.IsPinned = true;
            gridViewTextBoxColumn6.Name = "column1";
            gridViewTextBoxColumn6.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn6.Width = 107;
            gridViewTextBoxColumn7.HeaderText = "NIF";
            gridViewTextBoxColumn7.Name = "column2";
            gridViewTextBoxColumn7.Width = 25;
            gridViewTextBoxColumn8.HeaderText = "Domicilio";
            gridViewTextBoxColumn8.Name = "column3";
            gridViewTextBoxColumn8.Width = 55;
            gridViewTextBoxColumn9.HeaderText = "C�d. Postal";
            gridViewTextBoxColumn9.Name = "column4";
            gridViewTextBoxColumn9.Width = 64;
            gridViewTextBoxColumn10.HeaderText = "Poblaci�n";
            gridViewTextBoxColumn10.Name = "column5";
            gridViewTextBoxColumn10.Width = 57;
            gridViewTextBoxColumn11.HeaderText = "Tel�fono 1";
            gridViewTextBoxColumn11.Name = "column6";
            gridViewTextBoxColumn11.Width = 61;
            gridViewTextBoxColumn12.HeaderText = "Tel�fono 2";
            gridViewTextBoxColumn12.Name = "column7";
            gridViewTextBoxColumn12.Width = 61;
            gridViewTextBoxColumn13.HeaderText = "Parentesco";
            gridViewTextBoxColumn13.Name = "column8";
            gridViewTextBoxColumn13.Width = 63;
            gridViewTextBoxColumn14.HeaderText = "Responsable";
            gridViewTextBoxColumn14.Name = "column9";
            gridViewTextBoxColumn14.Width = 71;
            gridViewTextBoxColumn15.HeaderText = "R. Extranjero";
            gridViewTextBoxColumn15.Name = "column10";
            gridViewTextBoxColumn15.Width = 71;
            gridViewTextBoxColumn16.HeaderText = "Pa�s";
            gridViewTextBoxColumn16.Name = "column11";
            gridViewTextBoxColumn16.Width = 28;
            gridViewTextBoxColumn17.HeaderText = "Poblaci�n";
            gridViewTextBoxColumn17.Name = "column12";
            gridViewTextBoxColumn17.Width = 57;
            gridViewTextBoxColumn18.HeaderText = "M";
            gridViewTextBoxColumn18.Name = "column13";
            gridViewTextBoxColumn18.Width = 18;
            gridViewTextBoxColumn19.HeaderText = "N";
            gridViewTextBoxColumn19.Name = "column14";
            gridViewTextBoxColumn19.Width = 17;
            gridViewTextBoxColumn20.HeaderText = "O";
            gridViewTextBoxColumn20.Name = "column15";
            gridViewTextBoxColumn20.Width = 17;
            this.sprContactos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20});
            this.sprContactos.MasterTemplate.EnableSorting = false;
            this.sprContactos.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.sprContactos.Name = "sprContactos";
            this.sprContactos.ReadOnly = true;
            this.sprContactos.Size = new System.Drawing.Size(587, 117);
            this.sprContactos.TabIndex = 134;
            this.sprContactos.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprContactos_CellClick);
            this.sprContactos.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprContactos_CellDoubleClick);
            // 
            // lbNIFContacto
            // 
            this.lbNIFContacto.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNIFContacto.Location = new System.Drawing.Point(292, 195);
            this.lbNIFContacto.Name = "lbNIFContacto";
            this.lbNIFContacto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNIFContacto.Size = new System.Drawing.Size(121, 18);
            this.lbNIFContacto.TabIndex = 172;
            this.lbNIFContacto.Text = "N.I.F./Otro documento:";
            // 
            // lbPNIFContacto
            // 
            this.lbPNIFContacto.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPNIFContacto.Location = new System.Drawing.Point(414, 195);
            this.lbPNIFContacto.Name = "lbPNIFContacto";
            this.lbPNIFContacto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPNIFContacto.Size = new System.Drawing.Size(119, 20);
            this.lbPNIFContacto.TabIndex = 171;
            // 
            // lbFCParent
            // 
            this.lbFCParent.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFCParent.Location = new System.Drawing.Point(102, 301);
            this.lbFCParent.Name = "lbFCParent";
            this.lbFCParent.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFCParent.Size = new System.Drawing.Size(91, 20);
            this.lbFCParent.TabIndex = 160;
            // 
            // lbFCTelf2
            // 
            this.lbFCTelf2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFCTelf2.Location = new System.Drawing.Point(360, 276);
            this.lbFCTelf2.Name = "lbFCTelf2";
            this.lbFCTelf2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFCTelf2.Size = new System.Drawing.Size(143, 20);
            this.lbFCTelf2.TabIndex = 159;
            // 
            // lbFCTelf1
            // 
            this.lbFCTelf1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFCTelf1.Location = new System.Drawing.Point(102, 276);
            this.lbFCTelf1.Name = "lbFCTelf1";
            this.lbFCTelf1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFCTelf1.Size = new System.Drawing.Size(145, 20);
            this.lbFCTelf1.TabIndex = 158;
            // 
            // lbFCCodPostal
            // 
            this.lbFCCodPostal.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFCCodPostal.Location = new System.Drawing.Point(102, 249);
            this.lbFCCodPostal.Name = "lbFCCodPostal";
            this.lbFCCodPostal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFCCodPostal.Size = new System.Drawing.Size(43, 20);
            this.lbFCCodPostal.TabIndex = 157;
            // 
            // lbFCDomicilio
            // 
            this.lbFCDomicilio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFCDomicilio.Location = new System.Drawing.Point(102, 221);
            this.lbFCDomicilio.Name = "lbFCDomicilio";
            this.lbFCDomicilio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFCDomicilio.Size = new System.Drawing.Size(489, 20);
            this.lbFCDomicilio.TabIndex = 156;
            // 
            // lbFCNombre
            // 
            this.lbFCNombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFCNombre.Location = new System.Drawing.Point(102, 195);
            this.lbFCNombre.Name = "lbFCNombre";
            this.lbFCNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFCNombre.Size = new System.Drawing.Size(177, 20);
            this.lbFCNombre.TabIndex = 155;
            // 
            // lbFCApel2
            // 
            this.lbFCApel2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFCApel2.Location = new System.Drawing.Point(414, 167);
            this.lbFCApel2.Name = "lbFCApel2";
            this.lbFCApel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFCApel2.Size = new System.Drawing.Size(177, 20);
            this.lbFCApel2.TabIndex = 154;
            // 
            // lbFCApel1
            // 
            this.lbFCApel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFCApel1.Location = new System.Drawing.Point(102, 167);
            this.lbFCApel1.Name = "lbFCApel1";
            this.lbFCApel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFCApel1.Size = new System.Drawing.Size(177, 20);
            this.lbFCApel1.TabIndex = 153;
            // 
            // Label34
            // 
            this.Label34.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label34.Location = new System.Drawing.Point(292, 167);
            this.Label34.Name = "Label34";
            this.Label34.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label34.Size = new System.Drawing.Size(59, 18);
            this.Label34.TabIndex = 152;
            this.Label34.Text = "Apellido 2:";
            // 
            // Label35
            // 
            this.Label35.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label35.Location = new System.Drawing.Point(24, 167);
            this.Label35.Name = "Label35";
            this.Label35.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label35.Size = new System.Drawing.Size(59, 18);
            this.Label35.TabIndex = 151;
            this.Label35.Text = "Apellido 1:";
            // 
            // Label36
            // 
            this.Label36.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label36.Location = new System.Drawing.Point(24, 195);
            this.Label36.Name = "Label36";
            this.Label36.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label36.Size = new System.Drawing.Size(50, 18);
            this.Label36.TabIndex = 150;
            this.Label36.Text = "Nombre:";
            // 
            // Label37
            // 
            this.Label37.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label37.Location = new System.Drawing.Point(24, 301);
            this.Label37.Name = "Label37";
            this.Label37.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label37.Size = new System.Drawing.Size(63, 18);
            this.Label37.TabIndex = 149;
            this.Label37.Text = "Parentesco:";
            // 
            // Label38
            // 
            this.Label38.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label38.Location = new System.Drawing.Point(24, 221);
            this.Label38.Name = "Label38";
            this.Label38.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label38.Size = new System.Drawing.Size(56, 18);
            this.Label38.TabIndex = 148;
            this.Label38.Text = "Domicilio:";
            // 
            // lbFCPob
            // 
            this.lbFCPob.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFCPob.Location = new System.Drawing.Point(236, 250);
            this.lbFCPob.Name = "lbFCPob";
            this.lbFCPob.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFCPob.Size = new System.Drawing.Size(355, 20);
            this.lbFCPob.TabIndex = 147;
            // 
            // Label41
            // 
            this.Label41.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label41.Location = new System.Drawing.Point(292, 276);
            this.Label41.Name = "Label41";
            this.Label41.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label41.Size = new System.Drawing.Size(62, 18);
            this.Label41.TabIndex = 146;
            this.Label41.Text = "Tel�fono 2:";
            // 
            // Label42
            // 
            this.Label42.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label42.Location = new System.Drawing.Point(24, 276);
            this.Label42.Name = "Label42";
            this.Label42.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label42.Size = new System.Drawing.Size(62, 18);
            this.Label42.TabIndex = 145;
            this.Label42.Text = "Tel�fono 1:";
            // 
            // Label51
            // 
            this.Label51.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label51.Location = new System.Drawing.Point(24, 249);
            this.Label51.Name = "Label51";
            this.Label51.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label51.Size = new System.Drawing.Size(65, 18);
            this.Label51.TabIndex = 144;
            this.Label51.Text = "C�d. postal:";
            // 
            // Label52
            // 
            this.Label52.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label52.Location = new System.Drawing.Point(178, 251);
            this.Label52.Name = "Label52";
            this.Label52.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label52.Size = new System.Drawing.Size(58, 18);
            this.Label52.TabIndex = 143;
            this.Label52.Text = "Poblaci�n:";
            // 
            // _SSTab1_TabPage4
            // 
            this._SSTab1_TabPage4.Controls.Add(this.frmGradoDependencia);
            this._SSTab1_TabPage4.Controls.Add(this.fraColectivos);
            this._SSTab1_TabPage4.ItemSize = new System.Drawing.SizeF(105F, 28F);
            this._SSTab1_TabPage4.Location = new System.Drawing.Point(10, 37);
            this._SSTab1_TabPage4.Name = "_SSTab1_TabPage4";
            this._SSTab1_TabPage4.Size = new System.Drawing.Size(614, 424);
            this._SSTab1_TabPage4.Text = "Datos Adicionales";
            // 
            // frmGradoDependencia
            // 
            this.frmGradoDependencia.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmGradoDependencia.Controls.Add(this.tbOtrasCircunstanciasGradoDep);
            this.frmGradoDependencia.Controls.Add(this._lbGradoDep_5);
            this.frmGradoDependencia.Controls.Add(this._lbGradoDep_4);
            this.frmGradoDependencia.Controls.Add(this._lbGradoDep_3);
            this.frmGradoDependencia.Controls.Add(this._lbGradoDep_2);
            this.frmGradoDependencia.Controls.Add(this._lbGradoDep_1);
            this.frmGradoDependencia.Controls.Add(this._lbGradoDep_0);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_3);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_5);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_4);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_2);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_1);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_0);
            this.frmGradoDependencia.HeaderText = "Grado de dependencia ";
            this.frmGradoDependencia.Location = new System.Drawing.Point(3, 19);
            this.frmGradoDependencia.Name = "frmGradoDependencia";
            this.frmGradoDependencia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmGradoDependencia.Size = new System.Drawing.Size(608, 197);
            this.frmGradoDependencia.TabIndex = 224;
            this.frmGradoDependencia.Text = "Grado de dependencia ";
            // 
            // tbOtrasCircunstanciasGradoDep
            // 
            this.tbOtrasCircunstanciasGradoDep.AcceptsReturn = true;
            this.tbOtrasCircunstanciasGradoDep.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbOtrasCircunstanciasGradoDep.IsReadOnly = true;
            this.tbOtrasCircunstanciasGradoDep.Location = new System.Drawing.Point(182, 148);
            this.tbOtrasCircunstanciasGradoDep.MaxLength = 255;
            this.tbOtrasCircunstanciasGradoDep.Multiline = true;
            this.tbOtrasCircunstanciasGradoDep.Name = "tbOtrasCircunstanciasGradoDep";
            this.tbOtrasCircunstanciasGradoDep.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbOtrasCircunstanciasGradoDep.Size = new System.Drawing.Size(397, 37);
            this.tbOtrasCircunstanciasGradoDep.TabIndex = 225;
            // 
            // _lbGradoDep_5
            // 
            this._lbGradoDep_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbGradoDep_5.Enabled = false;
            this._lbGradoDep_5.Location = new System.Drawing.Point(384, 108);
            this._lbGradoDep_5.Name = "_lbGradoDep_5";
            this._lbGradoDep_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbGradoDep_5.Size = new System.Drawing.Size(76, 20);
            this._lbGradoDep_5.TabIndex = 237;
            // 
            // _lbGradoDep_4
            // 
            this._lbGradoDep_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbGradoDep_4.Enabled = false;
            this._lbGradoDep_4.Location = new System.Drawing.Point(212, 108);
            this._lbGradoDep_4.Name = "_lbGradoDep_4";
            this._lbGradoDep_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbGradoDep_4.Size = new System.Drawing.Size(74, 20);
            this._lbGradoDep_4.TabIndex = 236;
            // 
            // _lbGradoDep_3
            // 
            this._lbGradoDep_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbGradoDep_3.Enabled = false;
            this._lbGradoDep_3.Location = new System.Drawing.Point(384, 76);
            this._lbGradoDep_3.Name = "_lbGradoDep_3";
            this._lbGradoDep_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbGradoDep_3.Size = new System.Drawing.Size(76, 20);
            this._lbGradoDep_3.TabIndex = 235;
            // 
            // _lbGradoDep_2
            // 
            this._lbGradoDep_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbGradoDep_2.Enabled = false;
            this._lbGradoDep_2.Location = new System.Drawing.Point(212, 76);
            this._lbGradoDep_2.Name = "_lbGradoDep_2";
            this._lbGradoDep_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbGradoDep_2.Size = new System.Drawing.Size(74, 20);
            this._lbGradoDep_2.TabIndex = 234;
            // 
            // _lbGradoDep_1
            // 
            this._lbGradoDep_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbGradoDep_1.Enabled = false;
            this._lbGradoDep_1.Location = new System.Drawing.Point(384, 44);
            this._lbGradoDep_1.Name = "_lbGradoDep_1";
            this._lbGradoDep_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbGradoDep_1.Size = new System.Drawing.Size(76, 20);
            this._lbGradoDep_1.TabIndex = 233;
            // 
            // _lbGradoDep_0
            // 
            this._lbGradoDep_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbGradoDep_0.Enabled = false;
            this._lbGradoDep_0.Location = new System.Drawing.Point(212, 44);
            this._lbGradoDep_0.Name = "_lbGradoDep_0";
            this._lbGradoDep_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbGradoDep_0.Size = new System.Drawing.Size(74, 20);
            this._lbGradoDep_0.TabIndex = 232;
            // 
            // _lbDependencia_3
            // 
            this._lbDependencia_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_3.Location = new System.Drawing.Point(18, 148);
            this._lbDependencia_3.Name = "_lbDependencia_3";
            this._lbDependencia_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_3.Size = new System.Drawing.Size(158, 18);
            this._lbDependencia_3.TabIndex = 231;
            this._lbDependencia_3.Text = "Otras circunstancias de inter�s";
            // 
            // _lbDependencia_5
            // 
            this._lbDependencia_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_5.Location = new System.Drawing.Point(400, 20);
            this._lbDependencia_5.Name = "_lbDependencia_5";
            this._lbDependencia_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_5.Size = new System.Drawing.Size(41, 18);
            this._lbDependencia_5.TabIndex = 230;
            this._lbDependencia_5.Text = "Nivel II";
            // 
            // _lbDependencia_4
            // 
            this._lbDependencia_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_4.Location = new System.Drawing.Point(228, 20);
            this._lbDependencia_4.Name = "_lbDependencia_4";
            this._lbDependencia_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_4.Size = new System.Drawing.Size(38, 18);
            this._lbDependencia_4.TabIndex = 229;
            this._lbDependencia_4.Text = "Nivel I";
            // 
            // _lbDependencia_2
            // 
            this._lbDependencia_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_2.Location = new System.Drawing.Point(18, 115);
            this._lbDependencia_2.Name = "_lbDependencia_2";
            this._lbDependencia_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_2.Size = new System.Drawing.Size(52, 18);
            this._lbDependencia_2.TabIndex = 228;
            this._lbDependencia_2.Text = "Grado III:";
            // 
            // _lbDependencia_1
            // 
            this._lbDependencia_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_1.Location = new System.Drawing.Point(18, 81);
            this._lbDependencia_1.Name = "_lbDependencia_1";
            this._lbDependencia_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_1.Size = new System.Drawing.Size(49, 18);
            this._lbDependencia_1.TabIndex = 227;
            this._lbDependencia_1.Text = "Grado II:";
            // 
            // _lbDependencia_0
            // 
            this._lbDependencia_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_0.Location = new System.Drawing.Point(18, 48);
            this._lbDependencia_0.Name = "_lbDependencia_0";
            this._lbDependencia_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_0.Size = new System.Drawing.Size(46, 18);
            this._lbDependencia_0.TabIndex = 226;
            this._lbDependencia_0.Text = "Grado I:";
            // 
            // fraColectivos
            // 
            this.fraColectivos.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.fraColectivos.Controls.Add(this.lbCategEspecial);
            this.fraColectivos.Controls.Add(this.lbParentesco);
            this.fraColectivos.Controls.Add(this.lbNumTrabajador);
            this.fraColectivos.Controls.Add(this.Label80);
            this.fraColectivos.Controls.Add(this.Label79);
            this.fraColectivos.Controls.Add(this.Label77);
            this.fraColectivos.HeaderText = "Colectivos y Categ. Especial ";
            this.fraColectivos.Location = new System.Drawing.Point(3, 229);
            this.fraColectivos.Name = "fraColectivos";
            this.fraColectivos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fraColectivos.Size = new System.Drawing.Size(608, 165);
            this.fraColectivos.TabIndex = 185;
            this.fraColectivos.Text = "Colectivos y Categ. Especial ";
            // 
            // lbCategEspecial
            // 
            this.lbCategEspecial.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbCategEspecial.Enabled = false;
            this.lbCategEspecial.Location = new System.Drawing.Point(176, 108);
            this.lbCategEspecial.Name = "lbCategEspecial";
            this.lbCategEspecial.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbCategEspecial.Size = new System.Drawing.Size(329, 20);
            this.lbCategEspecial.TabIndex = 191;
            // 
            // lbParentesco
            // 
            this.lbParentesco.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbParentesco.Enabled = false;
            this.lbParentesco.Location = new System.Drawing.Point(176, 72);
            this.lbParentesco.Name = "lbParentesco";
            this.lbParentesco.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbParentesco.Size = new System.Drawing.Size(329, 20);
            this.lbParentesco.TabIndex = 190;
            // 
            // lbNumTrabajador
            // 
            this.lbNumTrabajador.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNumTrabajador.Enabled = false;
            this.lbNumTrabajador.Location = new System.Drawing.Point(176, 36);
            this.lbNumTrabajador.Name = "lbNumTrabajador";
            this.lbNumTrabajador.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNumTrabajador.Size = new System.Drawing.Size(329, 20);
            this.lbNumTrabajador.TabIndex = 189;
            // 
            // Label80
            // 
            this.Label80.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label80.Location = new System.Drawing.Point(18, 37);
            this.Label80.Name = "Label80";
            this.Label80.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label80.Size = new System.Drawing.Size(122, 18);
            this.Label80.TabIndex = 188;
            this.Label80.Text = "N�mero de Trabajador:";
            // 
            // Label79
            // 
            this.Label79.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label79.Location = new System.Drawing.Point(18, 73);
            this.Label79.Name = "Label79";
            this.Label79.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label79.Size = new System.Drawing.Size(151, 18);
            this.Label79.TabIndex = 187;
            this.Label79.Text = "Parentesco con el trabajador:";
            // 
            // Label77
            // 
            this.Label77.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label77.Location = new System.Drawing.Point(18, 109);
            this.Label77.Name = "Label77";
            this.Label77.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label77.Size = new System.Drawing.Size(100, 18);
            this.Label77.TabIndex = 186;
            this.Label77.Text = "Categor�a Especial:";
            // 
            // _SSTab1_TabPage5
            // 
            this._SSTab1_TabPage5.Controls.Add(this.Check1);
            this._SSTab1_TabPage5.Controls.Add(this.Frame18);
            this._SSTab1_TabPage5.Controls.Add(this.Frame17);
            this._SSTab1_TabPage5.ItemSize = new System.Drawing.SizeF(90F, 28F);
            this._SSTab1_TabPage5.Location = new System.Drawing.Point(10, 37);
            this._SSTab1_TabPage5.Name = "_SSTab1_TabPage5";
            this._SSTab1_TabPage5.Size = new System.Drawing.Size(614, 424);
            this._SSTab1_TabPage5.Text = "Datos Juzgado";
            // 
            // Check1
            // 
            this.Check1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Check1.Enabled = false;
            this.Check1.Location = new System.Drawing.Point(26, 39);
            this.Check1.Name = "Check1";
            this.Check1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Check1.Size = new System.Drawing.Size(129, 18);
            this.Check1.TabIndex = 217;
            this.Check1.Text = "Paciente incapacitado";
            // 
            // Frame18
            // 
            this.Frame18.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame18.Controls.Add(this.LbFechaResolucion2);
            this.Frame18.Controls.Add(this.LbExpediente2);
            this.Frame18.Controls.Add(this.LbJuzgado2);
            this.Frame18.Controls.Add(this.Label91);
            this.Frame18.Controls.Add(this.Label90);
            this.Frame18.Controls.Add(this.Label89);
            this.Frame18.HeaderText = "Expediente duplicado";
            this.Frame18.Location = new System.Drawing.Point(24, 181);
            this.Frame18.Name = "Frame18";
            this.Frame18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame18.Size = new System.Drawing.Size(575, 99);
            this.Frame18.TabIndex = 204;
            this.Frame18.Text = "Expediente duplicado";
            // 
            // LbFechaResolucion2
            // 
            this.LbFechaResolucion2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFechaResolucion2.Enabled = false;
            this.LbFechaResolucion2.Location = new System.Drawing.Point(390, 62);
            this.LbFechaResolucion2.Name = "LbFechaResolucion2";
            this.LbFechaResolucion2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFechaResolucion2.Size = new System.Drawing.Size(143, 20);
            this.LbFechaResolucion2.TabIndex = 216;
            // 
            // LbExpediente2
            // 
            this.LbExpediente2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbExpediente2.Enabled = false;
            this.LbExpediente2.Location = new System.Drawing.Point(82, 62);
            this.LbExpediente2.Name = "LbExpediente2";
            this.LbExpediente2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbExpediente2.Size = new System.Drawing.Size(171, 20);
            this.LbExpediente2.TabIndex = 215;
            // 
            // LbJuzgado2
            // 
            this.LbJuzgado2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbJuzgado2.Enabled = false;
            this.LbJuzgado2.Location = new System.Drawing.Point(82, 32);
            this.LbJuzgado2.Name = "LbJuzgado2";
            this.LbJuzgado2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbJuzgado2.Size = new System.Drawing.Size(451, 20);
            this.LbJuzgado2.TabIndex = 214;
            // 
            // Label91
            // 
            this.Label91.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label91.Location = new System.Drawing.Point(276, 64);
            this.Label91.Name = "Label91";
            this.Label91.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label91.Size = new System.Drawing.Size(108, 18);
            this.Label91.TabIndex = 210;
            this.Label91.Text = "Fecha de resoluci�n:";
            // 
            // Label90
            // 
            this.Label90.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label90.Location = new System.Drawing.Point(18, 64);
            this.Label90.Name = "Label90";
            this.Label90.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label90.Size = new System.Drawing.Size(64, 18);
            this.Label90.TabIndex = 209;
            this.Label90.Text = "Expediente:";
            // 
            // Label89
            // 
            this.Label89.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label89.Location = new System.Drawing.Point(18, 32);
            this.Label89.Name = "Label89";
            this.Label89.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label89.Size = new System.Drawing.Size(50, 18);
            this.Label89.TabIndex = 208;
            this.Label89.Text = "Juzgado:";
            // 
            // Frame17
            // 
            this.Frame17.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame17.Controls.Add(this.LbFechaResolucion1);
            this.Frame17.Controls.Add(this.LbExpediente1);
            this.Frame17.Controls.Add(this.LbJuzgado1);
            this.Frame17.Controls.Add(this.Label88);
            this.Frame17.Controls.Add(this.Label87);
            this.Frame17.Controls.Add(this.Label85);
            this.Frame17.HeaderText = "Primer expediente";
            this.Frame17.Location = new System.Drawing.Point(24, 67);
            this.Frame17.Name = "Frame17";
            this.Frame17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame17.Size = new System.Drawing.Size(575, 101);
            this.Frame17.TabIndex = 203;
            this.Frame17.Text = "Primer expediente";
            // 
            // LbFechaResolucion1
            // 
            this.LbFechaResolucion1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFechaResolucion1.Enabled = false;
            this.LbFechaResolucion1.Location = new System.Drawing.Point(390, 62);
            this.LbFechaResolucion1.Name = "LbFechaResolucion1";
            this.LbFechaResolucion1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFechaResolucion1.Size = new System.Drawing.Size(143, 20);
            this.LbFechaResolucion1.TabIndex = 213;
            // 
            // LbExpediente1
            // 
            this.LbExpediente1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbExpediente1.Enabled = false;
            this.LbExpediente1.Location = new System.Drawing.Point(82, 62);
            this.LbExpediente1.Name = "LbExpediente1";
            this.LbExpediente1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbExpediente1.Size = new System.Drawing.Size(171, 20);
            this.LbExpediente1.TabIndex = 212;
            // 
            // LbJuzgado1
            // 
            this.LbJuzgado1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbJuzgado1.Enabled = false;
            this.LbJuzgado1.Location = new System.Drawing.Point(82, 32);
            this.LbJuzgado1.Name = "LbJuzgado1";
            this.LbJuzgado1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbJuzgado1.Size = new System.Drawing.Size(451, 20);
            this.LbJuzgado1.TabIndex = 211;
            // 
            // Label88
            // 
            this.Label88.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label88.Location = new System.Drawing.Point(276, 64);
            this.Label88.Name = "Label88";
            this.Label88.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label88.Size = new System.Drawing.Size(108, 18);
            this.Label88.TabIndex = 207;
            this.Label88.Text = "Fecha de resoluci�n:";
            // 
            // Label87
            // 
            this.Label87.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label87.Location = new System.Drawing.Point(18, 64);
            this.Label87.Name = "Label87";
            this.Label87.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label87.Size = new System.Drawing.Size(64, 18);
            this.Label87.TabIndex = 206;
            this.Label87.Text = "Expediente:";
            // 
            // Label85
            // 
            this.Label85.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label85.Location = new System.Drawing.Point(18, 32);
            this.Label85.Name = "Label85";
            this.Label85.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label85.Size = new System.Drawing.Size(50, 18);
            this.Label85.TabIndex = 205;
            this.Label85.Text = "Juzgado:";
            // 
            // lblTituloFechaUltMod
            // 
            this.lblTituloFechaUltMod.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTituloFechaUltMod.Location = new System.Drawing.Point(8, 481);
            this.lblTituloFechaUltMod.Name = "lblTituloFechaUltMod";
            this.lblTituloFechaUltMod.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTituloFechaUltMod.Size = new System.Drawing.Size(141, 18);
            this.lblTituloFechaUltMod.TabIndex = 180;
            this.lblTituloFechaUltMod.Text = "Fecha �ltima Modificaci�n:";
            // 
            // lblFechaUltMod
            // 
            this.lblFechaUltMod.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblFechaUltMod.Enabled = false;
            this.lblFechaUltMod.Location = new System.Drawing.Point(155, 480);
            this.lblFechaUltMod.Name = "lblFechaUltMod";
            this.lblFechaUltMod.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFechaUltMod.Size = new System.Drawing.Size(75, 20);
            this.lblFechaUltMod.TabIndex = 179;
            // 
            // DFI130F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 505);
            this.Controls.Add(this.cbCerrar);
            this.Controls.Add(this.SSTab1);
            this.Controls.Add(this.lblTituloFechaUltMod);
            this.Controls.Add(this.lblFechaUltMod);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(89, 90);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI130F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Consulta de filiaci�n - DFI130F1";
            this.Activated += new System.EventHandler(this.DFI130F1_Activated);
            this.Closed += new System.EventHandler(this.DFI130F1_Closed);
            this.Load += new System.EventHandler(this.DFI130F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this._SSTab1_TabPage0.ResumeLayout(false);
            this._SSTab1_TabPage0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPApe1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPApe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPFNac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPSexo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPNIF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPCodPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTelf1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTelf2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPEstCivil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPCatSoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPFUltMov)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPPob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTelf3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTarjetaEuropea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPTarjetaSanitaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCipAutonomico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbLPaisNac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPExitus)).EndInit();
            this.frmPExitus.ResumeLayout(false);
            this.frmPExitus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbPExitus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPFFallec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPExtr)).EndInit();
            this.frmPExtr.ResumeLayout(false);
            this.frmPExtr.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbPExtr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPExDireccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbExPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPPais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
            this.Frame9.ResumeLayout(false);
            this.Frame9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbAutorizacion_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbAutorizacion_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame13)).EndInit();
            this.Frame13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbObservaciones)).EndInit();
            this._SSTab1_TabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sprhistoria.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprhistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbPIngres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUHospit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblcama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._frmestado_0)).EndInit();
            this._frmestado_0.ResumeLayout(false);
            this._frmestado_0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbEstado_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbEstado_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbEstado_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFPrest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUniPet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUbiFisi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            this._SSTab1_TabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdConsultaEntidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            this.Frame6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame15)).EndInit();
            this.Frame15.ResumeLayout(false);
            this.Frame15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkZona)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            this.Frame7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbPensionista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCodCIAS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREInspeccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREFechaCaducidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREFechaAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRECobertura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENPoliza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRESociedad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicador_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicador_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENomTitular)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPrivado)).EndInit();
            this.frmPrivado.ResumeLayout(false);
            this.frmPrivado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame16)).EndInit();
            this.Frame16.ResumeLayout(false);
            this.Frame16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbREPropioPac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame10)).EndInit();
            this.Frame10.ResumeLayout(false);
            this.Frame10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbREExtrangero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREExDireccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREEXtpoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREPais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREApe1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREApe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRENIF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbREPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRECodPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmRERegimen)).EndInit();
            this.frmRERegimen.ResumeLayout(false);
            this.frmRERegimen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_1)).EndInit();
            this._SSTab1_TabPage3.ResumeLayout(false);
            this._SSTab1_TabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame14)).EndInit();
            this.Frame14.ResumeLayout(false);
            this.Frame14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbTutor_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbTutor_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).EndInit();
            this.Frame12.ResumeLayout(false);
            this.Frame12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Option1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Option2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).EndInit();
            this.Frame8.ResumeLayout(false);
            this.Frame8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbFExtrangero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFpais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame11)).EndInit();
            this.Frame11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sprContactos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprContactos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNIFContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPNIFContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCParent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCTelf2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCTelf1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCCodPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCApel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCApel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFCPob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
            this._SSTab1_TabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frmGradoDependencia)).EndInit();
            this.frmGradoDependencia.ResumeLayout(false);
            this.frmGradoDependencia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOtrasCircunstanciasGradoDep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbGradoDep_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraColectivos)).EndInit();
            this.fraColectivos.ResumeLayout(false);
            this.fraColectivos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbCategEspecial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbParentesco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNumTrabajador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
            this._SSTab1_TabPage5.ResumeLayout(false);
            this._SSTab1_TabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Check1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame18)).EndInit();
            this.Frame18.ResumeLayout(false);
            this.Frame18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechaResolucion2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbExpediente2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbJuzgado2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame17)).EndInit();
            this.Frame17.ResumeLayout(false);
            this.Frame17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechaResolucion1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbExpediente1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbJuzgado1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTituloFechaUltMod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaUltMod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		void ReLoadForm(bool addEvents)
		{
			InitializerbTutor();
			InitializerbRERegimen();
			InitializerbIndicador();
			InitializerbEstado();
			InitializerbAutorizacion();
			InitializelbGradoDep();
			InitializelbDependencia();
			Initializefrmestado();
		}
		void InitializerbTutor()
		{
			this.rbTutor = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbTutor[0] = _rbTutor_0;
			this.rbTutor[1] = _rbTutor_1;
		}
		void InitializerbRERegimen()
		{
			this.rbRERegimen = new Telerik.WinControls.UI.RadRadioButton[3];
			this.rbRERegimen[2] = _rbRERegimen_2;
			this.rbRERegimen[0] = _rbRERegimen_0;
			this.rbRERegimen[1] = _rbRERegimen_1;
		}
		void InitializerbIndicador()
		{
			this.rbIndicador = new Telerik.WinControls.UI.RadRadioButton[4];
			this.rbIndicador[3] = _rbIndicador_3;
			this.rbIndicador[2] = _rbIndicador_2;
		}
		void InitializerbEstado()
		{
			this.rbEstado = new Telerik.WinControls.UI.RadRadioButton[3];
			this.rbEstado[2] = _rbEstado_2;
			this.rbEstado[1] = _rbEstado_1;
			this.rbEstado[0] = _rbEstado_0;
		}
		void InitializerbAutorizacion()
		{
			this.rbAutorizacion = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbAutorizacion[1] = _rbAutorizacion_1;
			this.rbAutorizacion[0] = _rbAutorizacion_0;
		}
		void InitializelbGradoDep()
		{
            this.lbGradoDep = new Telerik.WinControls.UI.RadTextBox[6];
			this.lbGradoDep[5] = _lbGradoDep_5;
			this.lbGradoDep[4] = _lbGradoDep_4;
			this.lbGradoDep[3] = _lbGradoDep_3;
			this.lbGradoDep[2] = _lbGradoDep_2;
			this.lbGradoDep[1] = _lbGradoDep_1;
			this.lbGradoDep[0] = _lbGradoDep_0;
		}
		void InitializelbDependencia()
		{
			this.lbDependencia = new Telerik.WinControls.UI.RadLabel[6];
			this.lbDependencia[3] = _lbDependencia_3;
			this.lbDependencia[5] = _lbDependencia_5;
			this.lbDependencia[4] = _lbDependencia_4;
			this.lbDependencia[2] = _lbDependencia_2;
			this.lbDependencia[1] = _lbDependencia_1;
			this.lbDependencia[0] = _lbDependencia_0;
		}
		void Initializefrmestado()
		{
			this.frmestado = new Telerik.WinControls.UI.RadGroupBox[1];
			this.frmestado[0] = _frmestado_0;
		}
		#endregion
	}
}