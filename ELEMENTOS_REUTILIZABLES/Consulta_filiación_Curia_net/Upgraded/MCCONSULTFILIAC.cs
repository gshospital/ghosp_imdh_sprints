using System;
using System.Data.SqlClient;
using System.IO;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLConsultaFiliacE
{
	public class MCCONSULTFILIAC
	{


		public void load(object VAR1, object VAR2, SqlConnection Conexion, string stIdPaciente)
		{
			MCONSULTFILIAC.GConexion = Conexion;
            MCONSULTFILIAC.NuevoDFI130F1 = new DFI130F1();
            Serrores.Obtener_TipoBD_FormatoFechaBD(ref MCONSULTFILIAC.GConexion);
            CDAyuda.Instance.setHelpFile(MCONSULTFILIAC.NuevoDFI130F1, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

			//Crea un formulario
			MCONSULTFILIAC.NuevoDFI130F1.REFERENCIAS(VAR1, VAR2, stIdPaciente);
			MCONSULTFILIAC.NuevoDFI130F1.ShowDialog();
		}

		public MCCONSULTFILIAC()
		{
			MCONSULTFILIAC.clasemensaje = new Mensajes.ClassMensajes();
		}

		~MCCONSULTFILIAC()
		{
			//Descarga el objeto formulario
			MCONSULTFILIAC.NuevoDFI130F1.Close();
		}
	}
}