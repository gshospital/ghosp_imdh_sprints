using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using Telerik.WinControls.UI;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLConsultaFiliacE
{
	public partial class DFI130F1
		: RadForm
	{

		string sql = String.Empty;
		DataSet RrSQLIng = null;
		DataSet RrSqlServ = null;
		DataSet RrsqlHist = null;
		string SQLDent = String.Empty;
		DataSet RrSQLDent = null;
		bool PASO = false;
		string NHistoria = String.Empty;
		bool CipAutonomico = false;
		bool CiasProfesional = false;
		public DFI130F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}




		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (PASO)
			{
				MANDAR_DATOS();
			}
			this.Close();
		}

		private void cmdConsultaEntidades_Click(Object eventSender, EventArgs eventArgs)
		{
			DFI140F1.DefInstance.ShowDialog();
		}

		private void DFI130F1_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				//*******************************
				//DATOS PERSONALES DEL PACIENTE *
				//*******************************
				string SQLPROV = String.Empty;
				DataSet RrSQLProv = null;
				string SQLPOB = String.Empty;
				DataSet RrSQLPOB = null;
				string SQLECIVIL = String.Empty;
				DataSet RrSQLECIVIL = null;
				string SQLCATSOC = String.Empty;
				DataSet RrSQLCATSOC = null;
				string SQLentid = String.Empty;
				DataSet RrSQLendtid = null;
				string SQLINSP = String.Empty;
				DataSet RrSQLINSP = null;
				string SQLPRI = String.Empty;
				string CODNIF = String.Empty;
				DataSet RrDPRIVADO = null;
				string SQLPOBLACION = String.Empty;
				DataSet RrSQLPOBLACION = null;
				string SQLPAIS = String.Empty;
				DataSet RrSQLPAIS = null;
				string SQLSoc = String.Empty;
				DataSet RrDSOCIEDA = null;
				string SQLDENTIPAC = String.Empty;
				DataSet RrDENTIPAC = null;
				string SQL_DPARENTE = String.Empty;
				DataSet Rr_DPARENTE = null;
				string SQL_DCATESPE = String.Empty;
				DataSet Rr_DCATESPE = null;
				if (MCONSULTFILIAC.stIdPaciente != "")
				{
					sql = "SELECT * FROM DPACIENT Where GIDENPAC = '" + MCONSULTFILIAC.stIdPaciente + "'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MCONSULTFILIAC.GConexion);
					MCONSULTFILIAC.RrSQL = new DataSet();
					tempAdapter.Fill(MCONSULTFILIAC.RrSQL);
					if (MCONSULTFILIAC.RrSQL.Tables[0].Rows.Count != 0)
					{
						PASO = true;
						lbPNombre.Text = Strings.StrConv(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DNOMBPAC"]), VbStrConv.ProperCase, 0);
						lbPApe1.Text = Strings.StrConv(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DAPE1PAC"]), VbStrConv.ProperCase, 0);
						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DAPE2PAC"]))
						{
							lbPApe2.Text = Strings.StrConv(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DAPE2PAC"]), VbStrConv.ProperCase, 0);
						}
						lbPFNac.Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["FNACIPAC"]).ToString("dd/MM/yyyy");
						if (Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ITIPSEXO"]) == MCONSULTFILIAC.CodSHombre)
						{
							lbPSexo.Text = Strings.StrConv(MCONSULTFILIAC.TexSHombre, VbStrConv.ProperCase, 0);
						}
						if (Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ITIPSEXO"]) == MCONSULTFILIAC.CodSMUJER)
						{
							lbPSexo.Text = Strings.StrConv(MCONSULTFILIAC.TexSMUJER, VbStrConv.ProperCase, 0);
						}
						if (Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ITIPSEXO"]) == MCONSULTFILIAC.CodSINDET)
						{
							lbPSexo.Text = Strings.StrConv(MCONSULTFILIAC.TexSINDET, VbStrConv.ProperCase, 0);
						}
						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NDNINIFP"]))
						{
							lbPNIF.Text = "";
						}
						else
						{
							lbPNIF.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NDNINIFP"]);
						}
						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DDIREPAC"]))
						{
							lbPDomicilio.Text = "";
						}
						else
						{
							lbPDomicilio.Text = Strings.StrConv(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DDIREPAC"]), VbStrConv.ProperCase, 0);
						}
						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GCODIPOS"]))
						{
							if (Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GCODIPOS"]) != "")
							{
								lbPCodPostal.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GCODIPOS"]);
							}
						}

						//(maplaza)(20/08/2007)
						//Tarjeta Sanitaria Europea
						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTARJSAE"]))
						{
							lbPTarjetaEuropea.Text = "";
						}
						else
						{
							lbPTarjetaEuropea.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTARJSAE"]).Trim();
						}
						//Tarjeta Sanitaria
						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTARJETA"]))
						{
							lbPTarjetaSanitaria.Text = "";
						}
						else
						{
							lbPTarjetaSanitaria.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTARJETA"]).Trim();
						}

						//Fecha de la �ltima Modificaci�n (el dato no est� en una pesta�a, sino en la parte inferior de la pantalla).
						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["FULTMOVI"]))
						{
							lblFechaUltMod.Text = "";
							ToolTipMain.SetToolTip(lblFechaUltMod, "");
						}
						else
						{
							lblFechaUltMod.Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["FULTMOVI"]).ToString("dd/MM/yyyy");
							if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gUsuario"]))
							{
								ToolTipMain.SetToolTip(lblFechaUltMod, "");
							}
							else
							{
								ToolTipMain.SetToolTip(lblFechaUltMod, proObtenerusuario(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gUsuario"])));
							}
						}
						//--------

						//OSCAR C ENE 2005
						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["OOBSERVA"]))
						{
							tbObservaciones.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["OOBSERVA"]).Trim();
						}
						//--------


						//BUSCA LA PROVINCIA
						//If Not IsNull(RrSQL("GPROVINC")) Then
						//   SQLPROV = "SELECT DNOMPROV FROM DPROVINC WHERE GPROVINC = '" & RrSQL("GPROVINC") & "'"
						//   Set RrSQLProv = GConexion.OpenResultset(SQLPROV, rdOpenKeyset)
						//   lbPProv.Caption = StrConv(RrSQLProv("DNOMPROV"), vbProperCase)
						//   RrSQLProv.Close
						//   lbPCodPob.Caption = RrSQL("GPOBLACI")
						//End If
						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GPOBLACI"]))
						{
							//BUSCA LA POBLACION
							SQLPOB = "SELECT DPOBLACI FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GPOBLACI"]) + "'";
							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLPOB, MCONSULTFILIAC.GConexion);
							RrSQLPOB = new DataSet();
							tempAdapter_2.Fill(RrSQLPOB);
							lbPPob.Text = Strings.StrConv(Convert.ToString(RrSQLPOB.Tables[0].Rows[0]["DPOBLACI"]), VbStrConv.ProperCase, 0);
							RrSQLPOB.Close();
						}

						//el check de residencia en el extranjero es independiente de la direcci�n en Espa�a.
						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GPAISRES"]))
						{
							//chequear extrangero
							//introducir poblacion
							//buscar pais
							chbPExtr.CheckState = CheckState.Checked;
							lbExPoblacion.Text = (Convert.IsDBNull(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dpoblaci"]).Trim())) ? "" : Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dpoblaci"]).Trim();
							SQLPOB = "SELECT dpaisres FROM dpaisres WHERE Gpaisres = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gpaisres"]) + "'";
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLPOB, MCONSULTFILIAC.GConexion);
							RrSQLPOB = new DataSet();
							tempAdapter_3.Fill(RrSQLPOB);
							if (RrSQLPOB.Tables[0].Rows.Count != 0)
							{
								lbPPais.Text = Convert.ToString(RrSQLPOB.Tables[0].Rows[0]["dpaisres"]).Trim();
								lbExPoblacion.Text = (Convert.IsDBNull(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dpoblaci"]).Trim())) ? "" : Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dpoblaci"]).Trim();
							}
							//(maplaza)(20/08/2007)Campo "ddireext"
							if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ddireext"]))
							{
								lbPExDireccion.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ddireext"]).Trim();
							}
							else
							{
								lbPExDireccion.Text = "";
							}
							//-----
							RrSQLPOB.Close();
						}

						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTELEFO1"]))
						{
							lbPTelf1.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTELEFO1"]).Trim();
						}

						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["Nextesi1"]))
						{
							lbPTelf1.Text = lbPTelf1.Text + " - (" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["Nextesi1"]).Trim() + ")";
						}

						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTELEFO2"]))
						{
							lbPTelf2.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTELEFO2"]).Trim();
						}

						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["Nextesi2"]))
						{
							lbPTelf2.Text = lbPTelf2.Text + "- (" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["Nextesi2"]).Trim() + ")";
						}

						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTELEFO3"]))
						{
							lbPTelf3.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NTELEFO3"]).Trim();
						}

						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["Nextesi3"]))
						{
							lbPTelf3.Text = lbPTelf3.Text + "- (" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["Nextesi3"]).Trim() + ")";
						}




						//BUSCA EL ESTADO CIVIL DEL PACIENTE
						SQLECIVIL = "SELECT DESTACIV FROM DESTACIV WHERE GESTACIV = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GESTACIV"]) + "'";
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLECIVIL, MCONSULTFILIAC.GConexion);
						RrSQLECIVIL = new DataSet();
						tempAdapter_4.Fill(RrSQLECIVIL);
						if (RrSQLECIVIL.Tables[0].Rows.Count != 0)
						{
							lbPEstCivil.Text = Strings.StrConv(Convert.ToString(RrSQLECIVIL.Tables[0].Rows[0]["DESTACIV"]), VbStrConv.ProperCase, 0);
						}
						RrSQLECIVIL.Close();

						//BUSCA LA CATALOGACION SOCIAL
						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GCATASOC"]))
						{
							SQLCATSOC = "SELECT DCATASOC FROM DCATASOC WHERE GCATASOC = '" + StringsHelper.Format(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GCATASOC"], "00") + "'";
							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQLCATSOC, MCONSULTFILIAC.GConexion);
							RrSQLCATSOC = new DataSet();
							tempAdapter_5.Fill(RrSQLCATSOC);
							if (RrSQLCATSOC.Tables[0].Rows.Count != 0)
							{
								lbPCatSoc.Text = Strings.StrConv(Convert.ToString(RrSQLCATSOC.Tables[0].Rows[0]["DCATASOC"]), VbStrConv.ProperCase, 0);
							}
							RrSQLCATSOC.Close();
						}

						//BUSCA LA HISTORIA CLINICA

						SqlDataAdapter tempAdapter_6 = new SqlDataAdapter("SELECT GHISTORIA FROM hdossier WHERE GIDENPAC = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GIDENPAC"]) + "'", MCONSULTFILIAC.GConexion);
						RrSQLIng = new DataSet();
						tempAdapter_6.Fill(RrSQLIng);
						if (RrSQLIng.Tables[0].Rows.Count != 0)
						{
							NHistoria = Convert.ToString(RrSQLIng.Tables[0].Rows[0]["GHISTORIA"]);
						}
						RrSQLIng.Close();

						if (Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["IEXITUSP"]) == "S")
						{
							lbPFFallec.Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["FFALLECI"]).ToString("dd/MM/yyyy");
							chbPExitus.CheckState = CheckState.Checked;
						}
						//AUTORIZACION UTILIZACION DATOS
						if (Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["IAUTUTIL"]) == "S")
						{
							rbAutorizacion[0].IsChecked = true;
						}
						if (Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["IAUTUTIL"]) == "N")
						{
							rbAutorizacion[1].IsChecked = true;
						}
						//---------------------------------------------------------------
						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["FULTMOVI"]))
						{
							lbPFUltMov.Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["FULTMOVI"]).ToString("dd/MM/yyyy");
						}

						if (CipAutonomico)
						{
							lbCipAutonomico.Text = (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ocipauto"])) ? Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ocipauto"]) : "";
						}
						else
						{
							lbCipAutonomico.Text = (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ncipauto"])) ? Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ncipauto"]) : "";
						}
						if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gpaisnac"]))
						{
							sql = "select * from DPAISRES where gpaisres = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gpaisnac"]) + "' ";
							SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql, MCONSULTFILIAC.GConexion);
							RrSQLIng = new DataSet();
							tempAdapter_7.Fill(RrSQLIng);
							if (RrSQLIng.Tables[0].Rows.Count != 0)
							{
								LbLPaisNac.Text = Convert.ToString(RrSQLIng.Tables[0].Rows[0]["dpaisres"]);
							}
							else
							{
								LbLPaisNac.Text = "";
							}
							RrSQLIng.Close();
						}


						//**************************************
						//  ESTADO DE LA HISTORIA DEL PACIENTE *
						//**************************************
						//busca si esta ingresado, la cama y la unidad de hospitalizacion
						SqlDataAdapter tempAdapter_8 = new SqlDataAdapter("SELECT * FROM dcamasbo WHERE GIDENPAC = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GIDENPAC"]) + "' AND IESTCAMA = 'O' ", MCONSULTFILIAC.GConexion);
						RrSQLIng = new DataSet();
						tempAdapter_8.Fill(RrSQLIng);
						if (RrSQLIng.Tables[0].Rows.Count != 0)
						{

							chbPIngres.CheckState = CheckState.Checked;

							lblcama.Text = StringsHelper.Format(RrSQLIng.Tables[0].Rows[0]["gplantas"], "00") + StringsHelper.Format(RrSQLIng.Tables[0].Rows[0]["ghabitac"], "00") + "-" + Convert.ToString(RrSQLIng.Tables[0].Rows[0]["gcamasbo"]);

							if (!Convert.IsDBNull(RrSQLIng.Tables[0].Rows[0]["gcounien"]))
							{
								SqlDataAdapter tempAdapter_9 = new SqlDataAdapter("SELECT dunidenf FROM DUNIENFE  WHERE Gunidenf = '" + Convert.ToString(RrSQLIng.Tables[0].Rows[0]["gcounien"]) + "'", MCONSULTFILIAC.GConexion);
								RrSqlServ = new DataSet();
								tempAdapter_9.Fill(RrSqlServ);
								lblUHospit.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dunidenf"]);
								RrSqlServ.Close();
							}

							RrSQLIng.Close();
						}
						//busca las historias que tiene el paciente encontrado

						SqlDataAdapter tempAdapter_10 = new SqlDataAdapter("SELECT * FROM hdossier WHERE GIDENPAC = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GIDENPAC"]) + "'", MCONSULTFILIAC.GConexion);
						RrSQLIng = new DataSet();
						tempAdapter_10.Fill(RrSQLIng);
						if (RrSQLIng.Tables[0].Rows.Count != 0)
						{
							RrSQLIng.MoveFirst();
							sprhistoria.MaxRows = RrSQLIng.Tables[0].Rows.Count;
							sprhistoria.Row = 1;
                            sprhistoria.ReadOnly = true;
							foreach (DataRow iteration_row in RrSQLIng.Tables[0].Rows)
							{
								//CARGA DE LAS HISTORIAS DEL PACIENTE
								sprhistoria.Col = 1;
								sprhistoria.Text = NHistoria;

								sprhistoria.Col = 2;
								if (Convert.ToString(iteration_row["Icontenido"]) == MCONSULTFILIAC.CODHIST.Trim())
								{
									sprhistoria.Text = MCONSULTFILIAC.TEXTHIST.Trim();
								}
								if (Convert.ToString(iteration_row["Icontenido"]) == MCONSULTFILIAC.CODPLACA.Trim())
								{
									sprhistoria.Text = MCONSULTFILIAC.TEXTPLACA.Trim();
								}

								sprhistoria.Col = 3;
								//busca el servicio
								SqlDataAdapter tempAdapter_11 = new SqlDataAdapter("SELECT dserarch FROM hserarch WHERE gserarch =  " + Convert.ToString(iteration_row["gserpropiet"]) + " ", MCONSULTFILIAC.GConexion);
								RrSqlServ = new DataSet();
								tempAdapter_11.Fill(RrSqlServ);
								sprhistoria.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dserarch"]);
								RrSqlServ.Close();

								sprhistoria.Col = 4;
								sprhistoria.Text = Convert.ToString(iteration_row["Icontenido"]);

								sprhistoria.Col = 5;
								sprhistoria.Text = Convert.ToString(iteration_row["gserpropiet"]);


								//SE A�ADE UNA FILA Y SE POSICIONA EN LA PRIMERA COLUMNA
								sprhistoria.Row++;
								sprhistoria.Col = 1;

							}
						}
						//**********************************
						//  REGIMEN ECONOMICO DEL PACIENTE *
						//**********************************
						chkZona.CheckState = CheckState.Unchecked;
						if (Convert.ToDouble(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]) == MCONSULTFILIAC.CodSS)
						{
							//CODIGO DE LA SEGURIDAD SOCIAL
							//        lbREInspeccion.Caption = TexSS
							rbRERegimen[0].IsChecked = true;
							//rbRERegimen(0).Enabled = False
							//rbRERegimen(1).Enabled = False
							//rbRERegimen(2).Enabled = False
							//busca EL PACIENTE EN LA TABLA DE ENTIDADES

							//        SQLentid = "SELECT * FROM SCONSGLO WHERE gconsglo = 'SEGURSOC'"
							//        Set RrSQLendtid = GConexion.OpenResultset(SQLentid, rdOpenKeyset)
							//        If RrSQLendtid.RowCount <> 0 Then
							//            lbFCEFinan.Caption = Trim(RrSQLendtid("valfanu1"))
							//        End If
							//        RrSQLendtid.Close

							SQLentid = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GIDENPAC"]) + "' AND GSOCIEDA = " + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]);
							SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(SQLentid, MCONSULTFILIAC.GConexion);
							RrSQLendtid = new DataSet();
							tempAdapter_12.Fill(RrSQLendtid);
							if (!Convert.IsDBNull(RrSQLendtid.Tables[0].Rows[0]["ginspecc"]))
							{
								lbREInspeccion.Text = Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["ginspecc"]);
							}
							lbRENSS.Text = (!Convert.IsDBNull(RrSQLendtid.Tables[0].Rows[0]["NAFILIAC"])) ? Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["NAFILIAC"]) : "";
							if (!Convert.IsDBNull(RrSQLendtid.Tables[0].Rows[0]["iactpens"]))
							{
								if ((Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["iactpens"])) == "A")
								{
									rbActivo.IsChecked = true;
									rbPensionista.IsChecked = false;
								}
								else
								{
									if ((Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["iactpens"])) == "P")
									{
										rbActivo.IsChecked = false;
										rbPensionista.IsChecked = true;
									}
								}
							}
							else
							{
								rbActivo.IsChecked = false;
								rbPensionista.IsChecked = false;
							}
							if (!Convert.IsDBNull(RrSQLendtid.Tables[0].Rows[0]["gcodcias"]))
							{
								lbCodCIAS.Text = Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["gcodcias"]).Trim();
							}

							//********************* O.Frias (09/01/2007) *********************
							if (Convert.IsDBNull(RrSQLendtid.Tables[0].Rows[0]["ieszona"]))
							{
								chkZona.CheckState = CheckState.Unchecked;
							}
							else
							{
								chkZona.CheckState = (CheckState) ((Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["ieszona"]) == "S") ? 1 : 0);
							}
							//********************* O.Frias (09/01/2007) *********************


							//BUSCA LA INSPECCON ASOCIADA AL C�DIGO
							SQLINSP = "SELECT  dinspecc  FROM dinspecc WHERE ginspecc = '" + Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["GINSPECC"]) + "'";
							SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(SQLINSP, MCONSULTFILIAC.GConexion);
							RrSQLINSP = new DataSet();
							tempAdapter_13.Fill(RrSQLINSP);
							if (RrSQLINSP.Tables[0].Rows.Count != 0)
							{
								lbREInspeccion.Text = Convert.ToString(RrSQLINSP.Tables[0].Rows[0]["dinspecc"]).Trim();
							}
							RrSQLINSP.Close();


							//INDICADOR DE TITULAR O BENEFICIARIO
							if (Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["ITITUBEN"]) == "T")
							{
								rbIndicador[2].IsChecked = true;
								//rbIndicador(2).Enabled = False
								//rbIndicador(3).Enabled = False
							}
							else
							{
								if (Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["ITITUBEN"]) == "B")
								{
									rbIndicador[3].IsChecked = true;
									//rbIndicador(2).Enabled = False
									//rbIndicador(3).Enabled = False
									lbRENomTitular.Text = Convert.ToString(RrSQLendtid.Tables[0].Rows[0]["DNOMBTIT"]);
								}
							}
							RrSQLendtid.Close();
						}


						if (Convert.ToDouble(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]) == MCONSULTFILIAC.CodPriv)
						{
							//CODIGO DE ENTIDAD PRIVADA
							//    lbFCEFinan.Caption = TexPriv
							rbRERegimen[2].IsChecked = true;
							//rbRERegimen(0).Enabled = False
							//rbRERegimen(1).Enabled = False
							//rbRERegimen(2).Enabled = False
							//BUSCA EL PACIENTE EN LA TABLA DE PAGADORES PRIVADO
							SQLPRI = "SELECT * FROM DPRIVADO WHERE GIDENPAC = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GIDENPAC"]) + "'";
							//SQLPRI = "SELECT * FROM DPRIVADO WHERE NNIFPRIV = '" & RrSQL("NAFILIAC") & "'"
							SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(SQLPRI, MCONSULTFILIAC.GConexion);
							RrDPRIVADO = new DataSet();
							tempAdapter_14.Fill(RrDPRIVADO);
							if (RrDPRIVADO.Tables[0].Rows.Count != 0)
							{
								//SI ESTA EN LA TABLA
								//TRAE LOS DATOS DEL PAGADOR PRIVADO
								lbRENombre.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DNOMBPRI"]).Trim();
								lbREApe1.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DAPE1PRI"]).Trim();
								lbREApe2.Text = (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"])) ? "" : Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"]).Trim();

								//(maplaza)(03/09/2007)Propio Paciente
								if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["IPROPACI"]))
								{
									chbREPropioPac.CheckState = CheckState.Unchecked;
								}
								else if ((Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["IPROPACI"]).Trim() == "S"))
								{ 
									chbREPropioPac.CheckState = CheckState.Checked;
								}
								else
								{
									chbREPropioPac.CheckState = CheckState.Unchecked;
								}
								//------

								if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]))
								{
									lbRENIF.Text = "";
								}
								else
								{
									lbRENIF.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]);
								}

								if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["DDIREPRI"]))
								{
									lbREDomicilio.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DDIREPRI"]).Trim();
								}
								lbRECodPostal.Text = (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"])) ? "" : Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"]).Trim();

								if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"]))
								{
									//busca la poblacion para este codigo de poblacion

									//BUSCA LA POBLACION
									SQLPOBLACION = "SELECT DPOBLACI FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"]) + "'";
									SqlDataAdapter tempAdapter_15 = new SqlDataAdapter(SQLPOBLACION, MCONSULTFILIAC.GConexion);
									RrSQLPOBLACION = new DataSet();
									tempAdapter_15.Fill(RrSQLPOBLACION);
									if (RrSQLPOBLACION.Tables[0].Rows.Count != 0)
									{
										//INTRODUCE EL NOMBRE DE LA POBLACION
										lbREPoblacion.Text = Convert.ToString(RrSQLPOBLACION.Tables[0].Rows[0]["DPOBLACI"]).Trim();
									}
									RrSQLPOBLACION.Close();
									chbREExtrangero.CheckState = CheckState.Unchecked;
								}

								//el check de residencia en el extranjero es independiente de la direcci�n en Espa�a.
								if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"]))
								{
									chbREExtrangero.CheckState = CheckState.Checked;
									lbREEXtpoblacion.Text = (Convert.IsDBNull(Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["dpoblaci"]).Trim())) ? "" : Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["dpoblaci"]).Trim();
									//(maplaza)(20/08/2007)Campo "ddireext"
									if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["ddireext"]))
									{
										lbREExDireccion.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["ddireext"]).Trim();
									}
									else
									{
										lbREExDireccion.Text = "";
									}
									//-----

									//BUSCA EL PAIS
									if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"]))
									{
										SQLPAIS = "SELECT DPAISRES FROM DPAISRES WHERE GPAISRES = '" + Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"]) + "'";
										SqlDataAdapter tempAdapter_16 = new SqlDataAdapter(SQLPAIS, MCONSULTFILIAC.GConexion);
										RrSQLPAIS = new DataSet();
										tempAdapter_16.Fill(RrSQLPAIS);
										lbREPais.Text = Convert.ToString(RrSQLPAIS.Tables[0].Rows[0]["DPAISRES"]);
										RrSQLPAIS.Close();
									}
								}
							}
						}

						if (Convert.ToDouble(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]) != MCONSULTFILIAC.CodPriv && Convert.ToDouble(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]) != MCONSULTFILIAC.CodSS)
						{
							//CODIGO DE SOCIEDAD
							rbRERegimen[1].IsChecked = true;
							//rbRERegimen(0).Enabled = False
							//rbRERegimen(1).Enabled = False
							//rbRERegimen(2).Enabled = False
							//BUSCA EL NOMBRE DE LA SOCIEDAD
							SQLSoc = "SELECT DSOCIEDA FROM DSOCIEDA WHERE GSOCIEDA = " + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]);
							SqlDataAdapter tempAdapter_17 = new SqlDataAdapter(SQLSoc, MCONSULTFILIAC.GConexion);
							RrDSOCIEDA = new DataSet();
							tempAdapter_17.Fill(RrDSOCIEDA);
							//COGE EL NOMBRE DE LA SOCIEDAD Y EL NUMERO DE POLIZA
							//    lbFCEFinan.Caption = Trim(RrDSOCIEDA("DSOCIEDA"))
							lbRESociedad.Text = Convert.ToString(RrDSOCIEDA.Tables[0].Rows[0]["DSOCIEDA"]).Trim();
							RrDSOCIEDA.Close();
							//BUSCA EL TITULAR O BENEFICIARIO
							SQLDENTIPAC = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GIDENPAC"]) + "' AND GSOCIEDA = " + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]);
							SqlDataAdapter tempAdapter_18 = new SqlDataAdapter(SQLDENTIPAC, MCONSULTFILIAC.GConexion);
							RrDENTIPAC = new DataSet();
							tempAdapter_18.Fill(RrDENTIPAC);
							if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["NAFILIAC"]))
							{
								lbRENPoliza.Text = Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["NAFILIAC"]).Trim();
							}
							//(maplaza)(24/08/2007)
							if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["OCOBERTU"]))
							{
								lbRECobertura.Text = Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["OCOBERTU"]).Trim();
							}
							else
							{
								lbRECobertura.Text = "";
							}
							if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["FINIVIGE"]))
							{
								lbREFechaAlta.Text = StringsHelper.Format(DateTime.Parse(Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["FINIVIGE"]).Trim()).Month, "00") + "/" + Convert.ToDateTime(RrDENTIPAC.Tables[0].Rows[0]["FINIVIGE"]).Year.ToString();
							}
							else
							{
								lbREFechaAlta.Text = "";
							}
							if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["FFINVIGE"]))
							{
								lbREFechaCaducidad.Text = StringsHelper.Format(DateTime.Parse(Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["FFINVIGE"]).Trim()).Month, "00") + "/" + Convert.ToDateTime(RrDENTIPAC.Tables[0].Rows[0]["FFINVIGE"]).Year.ToString();
							}
							else
							{
								lbREFechaCaducidad.Text = "";
							}
							//------
							//SI TIENE NOMBRE DE TITULAR ES BENEFICIARIO
							if (Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["ITITUBEN"]) == "T")
							{ // TITULAR
								rbIndicador[2].IsChecked = true;
								rbIndicador[3].IsChecked = false;
							}
							else
							{
								//BENEFICIARIO
								rbIndicador[3].IsChecked = true;
								rbIndicador[2].IsChecked = false;
								if (!Convert.IsDBNull(Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"]).Trim()))
								{
									lbRENomTitular.Text = Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"]).Trim();
								}
							}
							RrDENTIPAC.Close();
						}

						// Grados de dependencia

						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg1n1"]))
						{
							lbGradoDep[0].Text = "";
						}
						else
						{
							lbGradoDep[0].Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg1n1"]).ToString("dd/MM/yyyy");
						}

						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg1n2"]))
						{
							lbGradoDep[1].Text = "";
						}
						else
						{
							lbGradoDep[1].Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg1n2"]).ToString("dd/MM/yyyy");
						}

						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg2n1"]))
						{
							lbGradoDep[2].Text = "";
						}
						else
						{
							lbGradoDep[2].Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg2n1"]).ToString("dd/MM/yyyy");
						}

						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg2n2"]))
						{
							lbGradoDep[3].Text = "";
						}
						else
						{
							lbGradoDep[3].Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg2n2"]).ToString("dd/MM/yyyy");
						}

						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg3n1"]))
						{
							lbGradoDep[4].Text = "";
						}
						else
						{
							lbGradoDep[4].Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg3n1"]).ToString("dd/MM/yyyy");
						}

						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg3n2"]))
						{
							lbGradoDep[5].Text = "";
						}
						else
						{
							lbGradoDep[5].Text = Convert.ToDateTime(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["fdepg3n2"]).ToString("dd/MM/yyyy");
						}

						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["oobsgdep"]))
						{
							tbOtrasCircunstanciasGradoDep.Text = "";
						}
						else
						{
							tbOtrasCircunstanciasGradoDep.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["oobsgdep"]);
						}

						//(maplaza)(20/08/2007)
						//***************************************
						// COLECTIVOS Y CATEGORIZACI�N ESPECIAL *
						//***************************************
						//n�mero de trabajador
						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ntrabaja"]))
						{
							lbNumTrabajador.Text = "";
						}
						else
						{
							lbNumTrabajador.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ntrabaja"]).Trim();
						}

						//parentesco con el trabajador
						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gparetra"]))
						{
							lbParentesco.Text = "";
						}
						else
						{

							SQL_DPARENTE = "SELECT DPARENTE FROM DPARENTE WHERE GPARENTE = " + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GPARETRA"]);
							SqlDataAdapter tempAdapter_19 = new SqlDataAdapter(SQL_DPARENTE, MCONSULTFILIAC.GConexion);
							Rr_DPARENTE = new DataSet();
							tempAdapter_19.Fill(Rr_DPARENTE);
							if (Rr_DPARENTE.Tables[0].Rows.Count != 0)
							{
								if (!Convert.IsDBNull(Rr_DPARENTE.Tables[0].Rows[0]["DPARENTE"]))
								{
									lbParentesco.Text = Convert.ToString(Rr_DPARENTE.Tables[0].Rows[0]["DPARENTE"]).Trim();
								}
							}
							else
							{
								lbParentesco.Text = "";
							}
							Rr_DPARENTE.Close();
						}

						//c�digo de categorizaci�n especial
						if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gcatespe"]))
						{
							lbCategEspecial.Text = "";
						}
						else
						{

							SQL_DCATESPE = "SELECT DCATESPE FROM DCATESPE WHERE GCATESPE = " + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GCATESPE"]);
							SqlDataAdapter tempAdapter_20 = new SqlDataAdapter(SQL_DCATESPE, MCONSULTFILIAC.GConexion);
							Rr_DCATESPE = new DataSet();
							tempAdapter_20.Fill(Rr_DCATESPE);
							if (Rr_DCATESPE.Tables[0].Rows.Count != 0)
							{
								if (!Convert.IsDBNull(Rr_DCATESPE.Tables[0].Rows[0]["DCATESPE"]))
								{
									lbCategEspecial.Text = Convert.ToString(Rr_DCATESPE.Tables[0].Rows[0]["DCATESPE"]).Trim();
								}
							}
							else
							{
								lbCategEspecial.Text = "";
							}
							Rr_DCATESPE.Close();
						}
						//-------

						//************************************
						// FAMILIAR DE CONTACTO DEL PACIENTE *
						//************************************
						//Pesta�a de personas de contacto
						sql = "select * from dperscon where dperscon.gidenpac = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GIDENPAC"]) + "' order by nnumorde";
						SqlDataAdapter tempAdapter_21 = new SqlDataAdapter(sql, MCONSULTFILIAC.GConexion);
						MCONSULTFILIAC.RrSQL = new DataSet();
						tempAdapter_21.Fill(MCONSULTFILIAC.RrSQL);
						if (MCONSULTFILIAC.RrSQL.Tables[0].Rows.Count != 0)
						{
							procarga_contactos_paciente();
						}
						else
						{
							SSTabHelper.SetTabEnabled(SSTab1, 3, false);
						}

						MCONSULTFILIAC.RrSQL.Close();

					}
					else
					{
						PASO = false;
						string tempRefParam = "";
						short tempRefParam2 = 1100;
						string[] tempRefParam3 = new string[]{"Paciente"};
						MCONSULTFILIAC.clasemensaje.RespuestaMensaje(MCONSULTFILIAC.NombreApli, tempRefParam, tempRefParam2, MCONSULTFILIAC.GConexion, tempRefParam3);
						cbCerrar_Click(cbCerrar, new EventArgs());
					}
					CargarDatosJuzgado(MCONSULTFILIAC.stIdPaciente);

				}
				else
				{
					PASO = false;
					string tempRefParam4 = "";
					short tempRefParam5 = 1100;
					string[] tempRefParam6 = new string[]{"Paciente"};
					MCONSULTFILIAC.clasemensaje.RespuestaMensaje(MCONSULTFILIAC.NombreApli, tempRefParam4, tempRefParam5, MCONSULTFILIAC.GConexion, tempRefParam6);
					cbCerrar_Click(cbCerrar, new EventArgs());
				}

				//(maplaza)(17/09/2007)La nueva pesta�a (Colectivos y Categorizaci�n Especial) tendr� restricci�n de acceso. Pero
				//como la restricci�n de acceso no se puede hacer directamente con el control SSTAB, se hace de forma indirecta con
				//el frame que engloba los campos de la nueva pesta�a. Y si el frame (una vez aplicado el control de acceso) no est�
				//visible, entonces tampoco estar� visible la nueva pesta�a del control SSTAB. (El Control de Acceso se ha realizado
				//en el evento "Load" del formulario).

				// Ahora se mostrar� siempre el frame

				//If Not fraColectivos.Visible Then
				//    SSTab1.TabVisible(4) = False
				//End If

			}
		}

		private void DFI130F1_Load(Object eventSender, EventArgs eventArgs)
		{
			MCONSULTFILIAC.NombreApli = Serrores.ObtenerNombreAplicacion(MCONSULTFILIAC.GConexion);
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_FILIACION);
			string SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SEGURSOC'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLENTIDAD, MCONSULTFILIAC.GConexion);
			DataSet RrSQLENTIDAD = new DataSet();
			tempAdapter.Fill(RrSQLENTIDAD);
			//codigo de la SEGURIDAD SOCIAL
			MCONSULTFILIAC.CodSS = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
			//texto de la seguridad social
			MCONSULTFILIAC.TexSS = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
			SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'PRIVADO'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLENTIDAD, MCONSULTFILIAC.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_2.Fill(RrSQLENTIDAD);
			//codigo de regimen economco privado
			MCONSULTFILIAC.CodPriv = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
			//texto de regimen economco privado
			MCONSULTFILIAC.TexPriv = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
			RrSQLENTIDAD.Close();

			string SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SHOMBRE'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLSEXO, MCONSULTFILIAC.GConexion);
			DataSet RrSEXO = new DataSet();
			tempAdapter_3.Fill(RrSEXO);
			MCONSULTFILIAC.CodSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
			MCONSULTFILIAC.TexSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

			SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SMUJER'";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLSEXO, MCONSULTFILIAC.GConexion);
			RrSEXO = new DataSet();
			tempAdapter_4.Fill(RrSEXO);
			MCONSULTFILIAC.CodSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
			MCONSULTFILIAC.TexSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

			SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SINDET'";
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQLSEXO, MCONSULTFILIAC.GConexion);
			RrSEXO = new DataSet();
			tempAdapter_5.Fill(RrSEXO);
			MCONSULTFILIAC.CodSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
			MCONSULTFILIAC.TexSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();
			RrSEXO.Close();

			string SQLPROV = "SELECT * FROM SCONSGLO WHERE GCONSGLO ='CODPROVI'";
			SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQLPROV, MCONSULTFILIAC.GConexion);
			DataSet RrSPROV = new DataSet();
			tempAdapter_6.Fill(RrSPROV);
			MCONSULTFILIAC.CodPROV = Convert.ToInt32(Double.Parse(Convert.ToString(RrSPROV.Tables[0].Rows[0]["NNUMERI1"]).Trim()));
			RrSPROV.Close();

			string SQLCONT = "SELECT * FROM SCONSGLO WHERE GCONSGLO ='DHISTORI'";
			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(SQLCONT, MCONSULTFILIAC.GConexion);
			DataSet RrCONT = new DataSet();
			tempAdapter_7.Fill(RrCONT);
			MCONSULTFILIAC.CODHIST = Convert.ToString(RrCONT.Tables[0].Rows[0]["VALFANU2"]);
			MCONSULTFILIAC.TEXTHIST = Convert.ToString(RrCONT.Tables[0].Rows[0]["VALFANU1"]);
			SQLCONT = "SELECT * FROM SCONSGLO WHERE GCONSGLO ='DPLACAS'";
			SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SQLCONT, MCONSULTFILIAC.GConexion);
			RrCONT = new DataSet();
			tempAdapter_8.Fill(RrCONT);
			MCONSULTFILIAC.CODPLACA = Convert.ToString(RrCONT.Tables[0].Rows[0]["VALFANU2"]);
			MCONSULTFILIAC.TEXTPLACA = Convert.ToString(RrCONT.Tables[0].Rows[0]["VALFANU1"]);
			RrCONT.Close();

			//cip autonomico y pais nacimiento

			CipAutonomico = false;
			CiasProfesional = false;

			string stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'CIPAUTMA' ";
			SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stsqltemp, MCONSULTFILIAC.GConexion);
			DataSet RrTemp = new DataSet();
			tempAdapter_9.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					CipAutonomico = true;
				}
			}
			RrTemp.Close();

			stsqltemp = "select valfanu1,nnumeri1 from sconsglo where gconsglo= 'CIAPROMA' ";
			SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(stsqltemp, MCONSULTFILIAC.GConexion);
			RrTemp = new DataSet();
			tempAdapter_10.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					CiasProfesional = true;
				}
			}
			RrTemp.Close();

			//FORMATO DE FILIACION
			SqlDataAdapter tempAdapter_11 = new SqlDataAdapter("Select valfanu1 from sConsGlo where gConsGlo = 'FORMFILI'", MCONSULTFILIAC.GConexion);
			DataSet Rrglobal = new DataSet();
			tempAdapter_11.Fill(Rrglobal);
			MCONSULTFILIAC.stFORMFILI = Convert.ToString(Rrglobal.Tables[0].Rows[0]["valfanu1"]).Trim();
			Rrglobal.Close();
			//PONER DNI � CEDULA

			stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'idenpers' ";
			SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(stsqltemp, MCONSULTFILIAC.GConexion);
			RrTemp = new DataSet();
			tempAdapter_12.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				MCONSULTFILIAC.DNICEDULA = Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim();
			}
			RrTemp.Close();

			if (MCONSULTFILIAC.stFORMFILI != "E")
			{
				Label13.Text = MCONSULTFILIAC.DNICEDULA + ":";
				Label23.Text = MCONSULTFILIAC.DNICEDULA + ":";
			}
			else
			{
				//(maplaza)(18/05/2006)El literal para esas dos etiquetas ya no es "N.I.F.:", es "N.I.F./Otro documento:"
				//Label13 = "N.I.F.:"
				//Label23 = "N.I.F.:"
				Label13.Text = "N.I.F./Otro documento:";
				Label23.Text = "N.I.F./Otro documento:";
			}

			sprContactos.MaxRows = 0;

			//(maplaza)(17/09/2007)
			if (Serrores.VVstUsuarioApli == "")
			{
				Serrores.Datos_Conexion(MCONSULTFILIAC.GConexion);
			}
			//La pesta�a de categorizaci�n especial, en Consulta, se incluye en el mismo control de acceso que la de filiaci�n,
			//para restringirla a los mismos usuarios. Por eso se manda como letra del m�dulo la "F", que es la misma letra de
			//m�dulo que tiene la dll de Filiaci�n.
			if (!mbAcceso.AplicarControlAcceso(Serrores.VVstUsuarioApli, "F", this, this.Name, ref MCONSULTFILIAC.astrControles, ref MCONSULTFILIAC.astrEventos))
			{
				this.Close();
			}
			//-----

		}



        private void sprhistoria_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex;
			int Row = eventArgs.RowIndex;
			limpiar_pesta�a();
			string SQLEstado = String.Empty;
			DataSet RrSQLEstado = null;
			string SQLUB = String.Empty;
			DataSet RrSQLUB = null;
			string SLQUNI = String.Empty;
			DataSet RrSLQUNI = null;
			string SQLUNPET = String.Empty;
			DataSet RrSQLUNPET = null;
			//Por si est� vacia.

			if (sprhistoria.SelBlockRow == 0 || Row == 0 || sprhistoria.MaxRows == 0)
			{
				return;
			}

            sprhistoria.SelectionMode = GridViewSelectionMode.FullRowSelect;

			sprhistoria.Row = Row;
			sprhistoria.Col = 4;
			string CONTENIDO = sprhistoria.Text;
			sprhistoria.Col = 5;
			string servicio = sprhistoria.Text;

			if (NHistoria == "")
			{
				SQLEstado = "SELECT ghistoria FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + MCONSULTFILIAC.stIdPaciente + "' AND HDOSSIER.ICONTENIDO = '" + CONTENIDO + "' AND HDOSSIER.GSERPROPIET = " + servicio + "";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLEstado, MCONSULTFILIAC.GConexion);
				RrSQLEstado = new DataSet();
				tempAdapter.Fill(RrSQLEstado);
				MCONSULTFILIAC.stHistClinica = Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["ghistoria"]);
				RrSQLEstado.Close();
			}
			else
			{
				MCONSULTFILIAC.stHistClinica = NHistoria;
			}

			SQLEstado = "SELECT * FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + MCONSULTFILIAC.stIdPaciente + "' AND HDOSSIER.GHISTORIA = " + MCONSULTFILIAC.stHistClinica + " AND HDOSSIER.ICONTENIDO = '" + CONTENIDO + "' AND HDOSSIER.GSERPROPIET = " + servicio + "";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLEstado, MCONSULTFILIAC.GConexion);
			RrSQLEstado = new DataSet();
			tempAdapter_2.Fill(RrSQLEstado);

			if (RrSQLEstado.Tables[0].Rows.Count != 0)
			{
				//busca el estado de la historia

				//disponible
				if (Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["IESTHISTORIA"]) == "D")
				{
					rbEstado[0].IsChecked = true;
				}

				//prestada
				if (Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["IESTHISTORIA"]) == "P")
				{
					rbEstado[1].IsChecked = true;
				}

				//perdida
				if (Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["IESTHISTORIA"]) == "R")
				{
					rbEstado[2].IsChecked = true;
				}

				//pasiva
				//    If RrSQLEstado("IESTHISTORIA") = "S" Then rbEstado(3).Value = True

				//busca la ubicacion f�sica
				SQLUB = "select dubicfisic from dubicach where dubicach.gubicfisic = '" + Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["gubicfisic"]) + "'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLUB, MCONSULTFILIAC.GConexion);
				RrSQLUB = new DataSet();
				tempAdapter_3.Fill(RrSQLUB);

				if (RrSQLUB.Tables[0].Rows.Count != 0)
				{
					lblUbiFisi.Text = Convert.ToString(RrSQLUB.Tables[0].Rows[0]["dubicfisic"]).Trim();
				}
				RrSQLUB.Close();

				//busca la unidad de peticion
				if (Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["IESTHISTORIA"]) == "P")
				{

					//caso 1
					SLQUNI = "SELECT GENTIEXT,FPRESTAMOH FROM HMOVEXTE WHERE HMOVEXTE.GHISTORIA = " + MCONSULTFILIAC.stHistClinica + " AND HMOVEXTE.ICONTENIDO = '" + Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["icontenido"]) + "' AND HMOVEXTE.gserpropiet = " + Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["gserpropiet"]) + " AND HMOVEXTE.IESTMOVIH = 'E'";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SLQUNI, MCONSULTFILIAC.GConexion);
					RrSLQUNI = new DataSet();
					tempAdapter_4.Fill(RrSLQUNI);

					if (RrSLQUNI.Tables[0].Rows.Count != 0)
					{

						//INTRODUCE LA FECHA DE PRESTAMO
						lblFPrest.Text = StringsHelper.Format(RrSLQUNI.Tables[0].Rows[0]["FPRESTAMOH"], "dd/mm/yyyy hh:nn:ss AMPM");

						//BUSCA LA UNIDAD DE PETICION
						SQLUNPET = "SELECT DENTIEXT FROM HSOLIEXT WHERE HSOLIEXT.gentiext =  '" + Convert.ToString(RrSLQUNI.Tables[0].Rows[0]["GENTIEXT"]) + "'";
						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQLUNPET, MCONSULTFILIAC.GConexion);
						RrSQLUNPET = new DataSet();
						tempAdapter_5.Fill(RrSQLUNPET);

						if (RrSQLUNPET.Tables[0].Rows.Count != 0)
						{
							lblUniPet.Text = Convert.ToString(RrSQLUNPET.Tables[0].Rows[0]["DENTIEXT"]);
						}

						RrSQLUNPET.Close();

					}
					else
					{

						//CASO 2
						SLQUNI = "SELECT GUNIDENF,FPRESTAMOH FROM HMOVUENF WHERE HMOVUENF.GHISTORIA = " + MCONSULTFILIAC.stHistClinica + " AND HMOVUENF.ICONTENIDO = '" + Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["icontenido"]) + "' AND HMOVUENF.gserpropiet = " + Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["gserpropiet"]) + " AND HMOVUENF.IESTMOVIH = 'E'";
						SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SLQUNI, MCONSULTFILIAC.GConexion);
						RrSLQUNI = new DataSet();
						tempAdapter_6.Fill(RrSLQUNI);

						if (RrSLQUNI.Tables[0].Rows.Count != 0)
						{

							//INTRODUCE LA FECHA DE PRESTAMO
							lblFPrest.Text = Convert.ToString(RrSLQUNI.Tables[0].Rows[0]["FPRESTAMOH"]);

							//BUSCA LA UNIDAD DE PETICION
							SQLUNPET = "SELECT DUNIDENF FROM DUNIENFE WHERE DUNIENFE.GUNIDENF =  '" + Convert.ToString(RrSLQUNI.Tables[0].Rows[0]["GUNIDENF"]) + "'";
							SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(SQLUNPET, MCONSULTFILIAC.GConexion);
							RrSQLUNPET = new DataSet();
							tempAdapter_7.Fill(RrSQLUNPET);
							if (RrSQLUNPET.Tables[0].Rows.Count != 0)
							{
								lblUniPet.Text = Convert.ToString(RrSQLUNPET.Tables[0].Rows[0]["DUNIDENF"]);
							}
							else
							{
								lblUniPet.Text = "";
							}
							RrSQLUNPET.Close();

						}
						else
						{

							//caso 3
							SLQUNI = "SELECT GSERSOLI,FPRESTAMOH FROM HMOVSERV WHERE HMOVSERV.GHISTORIA = " + MCONSULTFILIAC.stHistClinica + " AND HMOVSERV.ICONTENIDO = '" + Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["icontenido"]) + "' AND HMOVSERV.gserpropiet = " + Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["gserpropiet"]) + " AND HMOVSERV.IESTMOVIH = 'E'";
							SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SLQUNI, MCONSULTFILIAC.GConexion);
							RrSLQUNI = new DataSet();
							tempAdapter_8.Fill(RrSLQUNI);

							if (RrSLQUNI.Tables[0].Rows.Count != 0)
							{

								//INTRODUCE LA FECHA DE PRESTAMO
								lblFPrest.Text = Convert.ToString(RrSLQUNI.Tables[0].Rows[0]["FPRESTAMOH"]);

								//BUSCA LA UNIDAD DE PETICION
								SQLUNPET = "SELECT DNOMSERV FROM DSERVICI WHERE DSERVICI.GSERVICI =  " + Convert.ToString(RrSLQUNI.Tables[0].Rows[0]["GSERSOLI"]) + "";
								SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(SQLUNPET, MCONSULTFILIAC.GConexion);
								RrSQLUNPET = new DataSet();
								tempAdapter_9.Fill(RrSQLUNPET);
								if (RrSQLUNPET.Tables[0].Rows.Count != 0)
								{
									lblUniPet.Text = Convert.ToString(RrSQLUNPET.Tables[0].Rows[0]["DNOMSERV"]);
								}
								RrSQLUNPET.Close();
							}
							else
							{
								//caso 4
								SLQUNI = "SELECT gsersoli,fprestamoh FROM hmovcons WHERE hmovcons.GHISTORIA = " + MCONSULTFILIAC.stHistClinica + " AND hmovcons.ICONTENIDO = '" + Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["icontenido"]) + "' AND hmovcons.gserpropiet = " + Convert.ToString(RrSQLEstado.Tables[0].Rows[0]["gserpropiet"]) + " AND hmovcons.IESTMOVIH = 'E'";
								SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(SLQUNI, MCONSULTFILIAC.GConexion);
								RrSLQUNI = new DataSet();
								tempAdapter_10.Fill(RrSLQUNI);

								if (RrSLQUNI.Tables[0].Rows.Count != 0)
								{

									//INTRODUCE LA FECHA DE PRESTAMO
									lblFPrest.Text = Convert.ToString(RrSLQUNI.Tables[0].Rows[0]["FPRESTAMOH"]);

									//BUSCA LA UNIDAD DE PETICION
									SQLUNPET = "SELECT DNOMSERV FROM DSERVICI WHERE DSERVICI.GSERVICI =  " + Convert.ToString(RrSLQUNI.Tables[0].Rows[0]["GSERSOLI"]) + "";
									SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(SQLUNPET, MCONSULTFILIAC.GConexion);
									RrSQLUNPET = new DataSet();
									tempAdapter_11.Fill(RrSQLUNPET);
									if (RrSQLUNPET.Tables[0].Rows.Count != 0)
									{
										lblUniPet.Text = Convert.ToString(RrSQLUNPET.Tables[0].Rows[0]["DNOMSERV"]);
									}
									RrSQLUNPET.Close();
								}
							}
						}
					}
					RrSLQUNI.Close();
				}
			}
			RrSQLEstado.Close();
		}
		public void REFERENCIAS(object Class, object OBJETO, string stIdPac)
		{
			MCONSULTFILIAC.CLASE2 = Class;
			MCONSULTFILIAC.OBJETO2 = OBJETO;
			MCONSULTFILIAC.stIdPaciente = stIdPac;
		}
		private void MANDAR_DATOS()
		{

			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT * FROM DPACIENT WHERE GIDENPAC = '" + MCONSULTFILIAC.stIdPaciente + "'", MCONSULTFILIAC.GConexion);
			MCONSULTFILIAC.RrSQL = new DataSet();
			tempAdapter.Fill(MCONSULTFILIAC.RrSQL);
			MCONSULTFILIAC.Nombre2 = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DNOMBPAC"]);
			MCONSULTFILIAC.Apellido1 = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DAPE1PAC"]);
			if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DAPE2PAC"]))
			{
				MCONSULTFILIAC.Apellido2 = "";
			}
			else
			{
				MCONSULTFILIAC.Apellido2 = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DAPE2PAC"]);
			}
			if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["FNACIPAC"]))
			{
				MCONSULTFILIAC.fechaNac = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["FNACIPAC"]);
			}
			else
			{
				MCONSULTFILIAC.fechaNac = "";
			}
			MCONSULTFILIAC.Sexo = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ITIPSEXO"]);
			if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DDIREPAC"]))
			{
				MCONSULTFILIAC.Domicilio = "";
			}
			else
			{
				MCONSULTFILIAC.Domicilio = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["DDIREPAC"]);
			}
			if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GCODIPOS"]))
			{
				MCONSULTFILIAC.CodPostal = "";
			}
			else
			{
				MCONSULTFILIAC.CodPostal = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GCODIPOS"]);
			}

			if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GPOBLACI"]))
			{
				MCONSULTFILIAC.codPob = "";
			}
			else
			{
				MCONSULTFILIAC.codPob = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GPOBLACI"]);
			}

			if (Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NDNINIFP"]))
			{
				MCONSULTFILIAC.NIF = "";
			}
			else
			{
				MCONSULTFILIAC.NIF = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NDNINIFP"]);
			}
			if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NAFILIAC"]))
			{
				MCONSULTFILIAC.Asegurado = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["NAFILIAC"]);
			}
			else
			{
				MCONSULTFILIAC.Asegurado = "";
			}
			MCONSULTFILIAC.IdPaciente = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GIDENPAC"]);
			//busca la historia clinica del paciente
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter("select ghistoria from hdossier where hdossier.gidenpac = '" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gidenpac"]) + "'", MCONSULTFILIAC.GConexion);
			RrsqlHist = new DataSet();
			tempAdapter_2.Fill(RrsqlHist);
			if (RrsqlHist.Tables[0].Rows.Count != 0)
			{
				MCONSULTFILIAC.Historia = Convert.ToString(RrsqlHist.Tables[0].Rows[0]["ghistoria"]).Trim();
			}
			else
			{
				MCONSULTFILIAC.Historia = "";
			}
			RrsqlHist.Close();
			if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]))
			{
				MCONSULTFILIAC.CodigoSoc = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]);
			}
			else
			{
				MCONSULTFILIAC.CodigoSoc = "";
			}
			DataSet RrSQLDent = null;
			if (Convert.ToDouble(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]) == MCONSULTFILIAC.CodSS)
			{
				SQLDent = "select iactpens, ginspecc from dentipac where gsocieda= " + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gsocieda"]) + " and gidenpac ='" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gidenpac"]) + "'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLDent, MCONSULTFILIAC.GConexion);
				RrSQLDent = new DataSet();
				tempAdapter_3.Fill(RrSQLDent);
				if (RrSQLDent.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["iactpens"]))
					{
						MCONSULTFILIAC.IndAcPen = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["iactpens"]);
					}
					else
					{
						MCONSULTFILIAC.IndAcPen = "";
					}
					if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["ginspecc"]))
					{
						MCONSULTFILIAC.Inspec = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["ginspecc"]);
					}
					else
					{
						MCONSULTFILIAC.Inspec = "";
					}

				}
				RrSQLDent.Close();
			}
			else
			{
				MCONSULTFILIAC.IndAcPen = "";
				MCONSULTFILIAC.Inspec = "";
			}
			MCONSULTFILIAC.Filprov = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ifilprov"]);
			MCONSULTFILIAC.RrSQL.Close();
			MCONSULTFILIAC.OBJETO2.Recoger_datos3(MCONSULTFILIAC.Nombre2, MCONSULTFILIAC.Apellido1, MCONSULTFILIAC.Apellido2, MCONSULTFILIAC.fechaNac, MCONSULTFILIAC.Sexo, MCONSULTFILIAC.Domicilio, MCONSULTFILIAC.NIF, MCONSULTFILIAC.IdPaciente, MCONSULTFILIAC.CodPostal, MCONSULTFILIAC.Asegurado, MCONSULTFILIAC.codPob, MCONSULTFILIAC.Historia, MCONSULTFILIAC.CodigoSoc, MCONSULTFILIAC.Filprov, MCONSULTFILIAC.IndAcPen, MCONSULTFILIAC.Inspec);
			MCONSULTFILIAC.IdPaciente = "";
		}
		private void limpiar_pesta�a()
		{
			for (int i = 0; i <= 2; i++)
			{
				rbEstado[i].IsChecked = false;
			}
			lblUbiFisi.Text = "";
			lblUniPet.Text = "";
			lblFPrest.Text = "";
			//chbPIngres.Value = 0
			//lblcama.Caption = ""
			//lblUHospit.Caption = ""
		}
		private void procarga_contactos_paciente()
		{
			try
			{
				sprContactos.MaxCols = 16;
				sprContactos.SetText(16, 0, "Tipo de Contacto");
				sprContactos.MaxRows = MCONSULTFILIAC.RrSQL.Tables[0].Rows.Count;
				sprContactos.Row = 1;
				while (MCONSULTFILIAC.RrSQL.Tables[0].Rows.Count != 0)
				{
					sprContactos.Col = 1;
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dape1per"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dape1per"]).Trim();
					}
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dape2per"]))
					{
						sprContactos.Text = sprContactos.Text + " " + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dape2per"]).Trim();
					}
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dnombper"]))
					{
						sprContactos.Text = sprContactos.Text + ", " + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dnombper"]).Trim();
					}
					sprContactos.Col = 2;
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ndninifp"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ndninifp"]).Trim();
					}
					sprContactos.Col = 3; //2
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ddireper"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ddireper"]).Trim();
					}
					sprContactos.Col = 4; //3
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gcodipos"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gcodipos"]).Trim();
					}
					sprContactos.Col = 5; //4
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gpoblaci"]))
					{
						sprContactos.Text = poblacion_paciente(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gpoblaci"]).Trim());
					}
					sprContactos.Col = 6; //5
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ntelefo1"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ntelefo1"]).Trim();
					}
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["nextesi1"]))
					{
						sprContactos.Text = sprContactos.Text + "-" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["nextesi1"]).Trim();
					}
					sprContactos.Col = 7; //6
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ntelefo2"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["ntelefo2"]).Trim();
					}
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["nextesi2"]))
					{
						sprContactos.Text = sprContactos.Text + "-" + Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["nextesi2"]).Trim();
					}
					sprContactos.Col = 8; //7
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dparente"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dparente"]).Trim();
					}
					sprContactos.Col = 9; //8
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["irespons"]))
					{
						if (Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["irespons"]).Trim() == "S")
						{
							sprContactos.Text = "Si";
						}
						if (Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["irespons"]).Trim() == "N")
						{
							sprContactos.Text = "No";
						}
					}
					else
					{
						sprContactos.Text = "No";
					}
					sprContactos.Col = 10; //9
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gpaisres"]))
					{
						sprContactos.Text = "Si";
					}
					sprContactos.Col = 11; //10
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gpaisres"]))
					{
						sprContactos.Text = Pais_residencia(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["gpaisres"]).Trim());
					}
					sprContactos.Col = 12; //11
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dpoblaci"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dpoblaci"]).Trim();
						sprContactos.Col = 9;
						sprContactos.Text = "Si";
					}
					sprContactos.Col = 13; //12
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dnombper"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dnombper"]).Trim();
					}
					sprContactos.Col = 14; //13
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dape1per"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dape1per"]).Trim();
					}
					sprContactos.Col = 15; //14
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dape2per"]))
					{
						sprContactos.Text = Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["dape2per"]).Trim();
					}
					sprContactos.Col = 16;
					if (!Convert.IsDBNull(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["itipocon"]))
					{
						switch(Convert.ToString(MCONSULTFILIAC.RrSQL.Tables[0].Rows[0]["itipocon"]))
						{
							case "T" :  
								sprContactos.Text = "TUTOR LEGAL"; 
								break;
							case "C" :  
								sprContactos.Text = "CUIDADOR HABITUAL"; 
								break;
						}
					}
					MCONSULTFILIAC.RrSQL.MoveNext();
					if (MCONSULTFILIAC.RrSQL.Tables[0].Rows.Count != 0)
					{
						sprContactos.Row++;
					}
				}
			}
			catch
			{
				//   MsgBox "Error en Private Sub procarga_entidades_paciente()"
			}
		}
		public string poblacion_paciente(string Codigo)
		{
			//busca la poblacion del paciente
			try
			{
				string sql_descripcion = String.Empty;
				DataSet rrsql_descripcion = null;
				string poblacion = String.Empty;
				sql_descripcion = "select dpoblaci from dpoblaci where gpoblaci = '" + Codigo + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql_descripcion, MCONSULTFILIAC.GConexion);
				rrsql_descripcion = new DataSet();
				tempAdapter.Fill(rrsql_descripcion);
				if (rrsql_descripcion.Tables[0].Rows.Count != 0)
				{
					poblacion = Convert.ToString(rrsql_descripcion.Tables[0].Rows[0]["dpoblaci"]);
				}
				else
				{
					poblacion = "";
				}
				rrsql_descripcion.Close();
				return poblacion;
			}
			catch
			{
				//   MsgBox "Error en Public Function poblacion_paciente(codigo As Integer) As String"
			}
			return System.String.Empty;
		}
		public string Pais_residencia(string Codigo)
		{
			//busca el pais del paciente
			try
			{
				string sql_descripcion = String.Empty;
				DataSet rrsql_descripcion = null;
				string pais = String.Empty;
				sql_descripcion = "select dpaisres from dpaisres where gpaisres = '" + Codigo + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql_descripcion, MCONSULTFILIAC.GConexion);
				rrsql_descripcion = new DataSet();
				tempAdapter.Fill(rrsql_descripcion);
				if (MCONSULTFILIAC.RrSQL.Tables[0].Rows.Count != 0)
				{
					pais = Convert.ToString(rrsql_descripcion.Tables[0].Rows[0]["dpaisres"]);
				}
				else
				{
					pais = "";
				}
				rrsql_descripcion.Close();
				return pais;
			}
			catch
			{
				//    MsgBox "Error en Public Function Pais_residencia(codigo As Integer) As String"
			}
			return System.String.Empty;
		}
        private void sprContactos_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			//if (eventArgs.Button == MouseButtons.Left)
			{
				int Col = eventArgs.ColumnIndex;
				int Row = eventArgs.RowIndex;
				if (Row != 0)
				{
                    sprContactos.ReadOnly = true;
                    sprContactos.SelectionMode = GridViewSelectionMode.FullRowSelect;
					lbFCApel1.Text = "";
					lbFCApel2.Text = "";
					lbFCNombre.Text = "";
					lbFCDomicilio.Text = "";
					lbFCCodPostal.Text = "";
					lbFCPob.Text = "";
					lbFCTelf1.Text = "";
					lbFCTelf2.Text = "";
					lbFCParent.Text = "";
					lbPNIFContacto.Text = "";
					Option2.IsChecked = false;
					Option1.IsChecked = false;
					chbFExtrangero.CheckState = CheckState.Unchecked;
					lbFpais.Text = "";
					lbFPoblacion.Text = "";
					rbTutor[0].IsChecked = false;
					rbTutor[1].IsChecked = false;
					sprContactos.Row = Row;
					cargar_datos_contacto();

				}
			}
		}
        private void sprContactos_CellDoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
            sprContactos.ReadOnly = true;
			lbFCApel1.Text = "";
			lbFCApel2.Text = "";
			lbFCNombre.Text = "";
			lbFCDomicilio.Text = "";
			lbFCCodPostal.Text = "";
			lbFCPob.Text = "";
			lbFCTelf1.Text = "";
			lbFCTelf2.Text = "";
			lbFCParent.Text = "";
			lbPNIFContacto.Text = "";
			Option2.IsChecked = false;
			Option1.IsChecked = false;
			chbFExtrangero.CheckState = CheckState.Unchecked;
			lbFpais.Text = "";
			lbFPoblacion.Text = "";
			rbTutor[0].IsChecked = false;
			rbTutor[1].IsChecked = false;
		}
		private void cargar_datos_contacto()
		{
			sprContactos.Col = 2;
			lbPNIFContacto.Text = sprContactos.Text;
			sprContactos.Col = 3; //2
			lbFCDomicilio.Text = sprContactos.Text;
			sprContactos.Col = 4; //3
			lbFCCodPostal.Text = sprContactos.Text;
			sprContactos.Col = 5; //4
			lbFCPob.Text = sprContactos.Text;
			sprContactos.Col = 6; //5
			lbFCTelf1.Text = sprContactos.Text;
			sprContactos.Col = 7; //6
			lbFCTelf2.Text = sprContactos.Text;
			sprContactos.Col = 8; //7
			lbFCParent.Text = sprContactos.Text;
			sprContactos.Col = 9; //8
			if (sprContactos.Text.Trim() == "Si")
			{
				Option2.IsChecked = true;
			}
			if (sprContactos.Text.Trim() == "No")
			{
				Option1.IsChecked = true;
			}
			sprContactos.Col = 10; //9
			if (sprContactos.Text.Trim() == "Si")
			{
				chbFExtrangero.CheckState = CheckState.Checked;
			}
			if (sprContactos.Text.Trim() == "No")
			{
				chbFExtrangero.CheckState = CheckState.Unchecked;
			}
			sprContactos.Col = 11; //10
			lbFpais.Text = sprContactos.Text;
			sprContactos.Col = 12; //11
			lbFPoblacion.Text = sprContactos.Text;
			sprContactos.Col = 13; //12
			lbFCNombre.Text = sprContactos.Text;
			sprContactos.Col = 14; //13
			lbFCApel1.Text = sprContactos.Text;
			sprContactos.Col = 15; //14
			lbFCApel2.Text = sprContactos.Text;
			sprContactos.Col = 16;
			if (sprContactos.Text.StartsWith("T"))
			{
				rbTutor[0].IsChecked = true;
			}
			if (sprContactos.Text.StartsWith("C"))
			{
				rbTutor[1].IsChecked = true;
			}
		}

		private string proObtenerusuario(string gsuario)
		{
			string result = String.Empty;
			result = "";
			string sql = "select ISNULL(dnompers,'') dnompers, ISNULL(dap1pers,'') dap1pers, ISNULL(dap2pers,'') dap2pers " + 
			             " From dpersona " + 
			             " inner join susuario on susuario.gpersona = dpersona.gpersona " + 
			             " Where susuario.gusuario = '" + gsuario + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MCONSULTFILIAC.GConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(RR.Tables[0].Rows[0]["dnompers"]).Trim() + " " + Convert.ToString(RR.Tables[0].Rows[0]["dap1pers"]).Trim() + " " + Convert.ToString(RR.Tables[0].Rows[0]["dap2pers"]).Trim();
			}
			RR.Close();
			return result;
		}

		private void CargarDatosJuzgado(string stGiden)
		{

			string StSql = "select ienfment,gjuzgad1,nnumexp1,fresolu1,gjuzgad2,nnumexp2,fresolu2,DJUZGADO1.dnomjuzg NomJ1  ,DJUZGADO2.dnomjuzg NomJ2" + 
			               " From AEPISADM " + 
			               "left JOIN DJUZGADO DJUZGADO1 ON DJUZGADO1.gjuzgado = gjuzgad1 " + 
			               "left JOIN DJUZGADO DJUZGADO2 ON DJUZGADO2.gjuzgado = gjuzgad2 " + 
			               " Where gidenpac = '" + stGiden + "'" + 
			               " order by fllegada desc ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MCONSULTFILIAC.GConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrSql.Tables[0].Rows[0]["ienfment"]).Trim().ToUpper() == "S")
				{
					Check1.CheckState = CheckState.Checked;
				}
				LbJuzgado1.Text = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["NomJ1"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["NomJ1"]);
				LbExpediente1.Text = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["nnumexp1"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["nnumexp1"]);
				LbFechaResolucion1.Text = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fresolu1"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["fresolu1"]);
				LbJuzgado2.Text = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["NomJ2"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["NomJ2"]);
				LbExpediente2.Text = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["nnumexp2"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["nnumexp2"]);
				LbFechaResolucion2.Text = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fresolu2"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["fresolu2"]);
				//''    Else
				//''        LbJuzgado1.Caption = ""
				//''        LbExpediente1.Caption = ""
				//''        LbFechaResolucion1.Caption = ""
				//''        LbJuzgado2.Caption = ""
				//''        LbExpediente2.Caption = ""
				//''        LbFechaResolucion2.Caption = ""
				//''    End If

			}
			RrSql.Close();


		}
		private void DFI130F1_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}