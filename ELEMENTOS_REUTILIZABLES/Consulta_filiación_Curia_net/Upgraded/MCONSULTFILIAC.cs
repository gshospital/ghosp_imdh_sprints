using System;
using System.Data;
using System.Data.SqlClient;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLConsultaFiliacE
{
	internal static class MCONSULTFILIAC
	{

		public static SqlConnection GConexion = null;
		public static DFI130F1 NuevoDFI130F1 = null;
		public static object CLASE2 = null;
		public static dynamic OBJETO2 = null;
		public static DataSet RrSQL = null;
		public static string stIdPaciente = String.Empty;
		public static int CodSS = 0; //codigo de la seguridad social
		public static string TexSS = String.Empty; //texto de la seguridad social
		public static int CodPriv = 0; //codigo de privado
		public static string TexPriv = String.Empty; //texto de privado
		public static string CodSHombre = String.Empty; //codigo de sexo HOMBRE
		public static string TexSHombre = String.Empty; //texto de sexo HOMBRE
		public static string CodSMUJER = String.Empty; //codigo de sexo MUJER
		public static string TexSMUJER = String.Empty; //texto de sexo MUJER
		public static string CodSINDET = String.Empty; //codigo de sexo INDETERMINADO
		public static string TexSINDET = String.Empty; //texto de sexo INDETERMINADO
		public static int CodPROV = 0;
		public static string CODPLACA = String.Empty;
		public static string TEXTPLACA = String.Empty;
		public static string CODHIST = String.Empty;
		public static string TEXTHIST = String.Empty;

		public static string vstUsuario_NT = String.Empty;
		public static string Nombre2 = String.Empty; //AL MANDAR LOS DATOS AL FORM ORIGEN
		public static string Apellido1 = String.Empty; //       IDEM
		public static string Apellido2 = String.Empty; //       IDEM
		public static string fechaNac = String.Empty; //       IDEM
		public static string Sexo = String.Empty; //       IDEM
		public static string Domicilio = String.Empty; //       IDEM
		public static string CodPostal = String.Empty; //       IDEM
		public static string codPob = String.Empty;
		public static string Historia = String.Empty;
		public static string NIF = String.Empty; //       IDEM
		public static string Asegurado = String.Empty; //       IDEM
		public static string IdPaciente = String.Empty; //       IDEM
		public static string CodigoSoc = String.Empty;
		public static string Filprov = String.Empty;
		public static string IndAcPen = String.Empty;
		public static string Inspec = String.Empty;

		public static mbAcceso.strControlOculto[] astrControles = null;
		public static mbAcceso.strEventoNulo[] astrEventos = null;
		public static string stHistClinica = String.Empty;
		//Global stIdPaciente As String
		public static string stCodigoSoc = String.Empty;
		public static string stFilprov = String.Empty;
		public static Mensajes.ClassMensajes clasemensaje = null;
		public static string NombreApli = String.Empty;
		public static string stFORMFILI = String.Empty;
		public static string DNICEDULA = String.Empty;
	}
}