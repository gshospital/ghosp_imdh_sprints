using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;

namespace DLLConsultaFiliacE
{
	public partial class DFI140F1
		: RadForm
	{

		public DFI140F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void DFI140F1_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		//Constantes para los n�meros de columna del spread
		int Col_DSocieda = 0;
		int Col_NAfiliac = 0;
		int Col_TitBenef = 0;
		int Col_Cobertura = 0;
		int Col_FechaAlta = 0;
		int Col_FechaCad = 0;
		int Col_FBorrado = 0;
		int Col_GSocieda = 0;

		int fila_seleccionada = 0;

		private void cmdSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void DFI140F1_Load(Object eventSender, EventArgs eventArgs)
		{

			try
			{

				this.Cursor = Cursors.WaitCursor;

				Col_DSocieda = 1;
				Col_NAfiliac = 2;
				Col_TitBenef = 3;
				Col_Cobertura = 4;
				Col_FechaAlta = 5;
				Col_FechaCad = 6;
				Col_FBorrado = 7;
				Col_GSocieda = 8;

				sprSociedades.MaxRows = 0;

				MostrarNombrePaciente(MCONSULTFILIAC.stIdPaciente);
				CargaGrid();

				this.Cursor = Cursors.Default;
			}
			catch (SqlException ex)
			{
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(ex, MCONSULTFILIAC.GConexion);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		private void CargaGrid()
		{

			string StSql = String.Empty;
			DataSet RrSql = null;
			int intContador = 0;
			int lngColorRegistro = 0;

			try
			{

				this.Cursor = Cursors.WaitCursor;

				//hay que descartar los registros de DENTIPAC cuya sociedad se correspondan con la Seguridad Social.
				StSql = "SELECT D.nafiliac, D.ginspecc, D.itituben, D.fborrado, D.gsocieda, D.ocobertu,";
				StSql = StSql + " D.finivige, D.ffinvige, DS.dsocieda as DSOCIEDA, FMOVIMIE FROM DENTIPAC D, DSOCIEDA DS";
				StSql = StSql + " WHERE D.GSOCIEDA=DS.GSOCIEDA AND D.GIDENPAC = '" + MCONSULTFILIAC.stIdPaciente + "'";
				StSql = StSql + " AND D.GSOCIEDA <> " + MCONSULTFILIAC.CodSS.ToString() + " AND (D.ginspecc is null)";

				StSql = StSql + " UNION ALL ";
				StSql = StSql + 
				        "SELECT D.nafiliac, D.ginspecc, D.itituben, D.fborrado, D.gsocieda, D.ocobertu," + " D.finivige, D.ffinvige, DS.dinspecc as DSOCIEDA, FMOVIMIE FROM DINSHPAC D, DINSPECC DS" + " WHERE D.GINSPECC=DS.GINSPECC AND D.GIDENPAC = '" + MCONSULTFILIAC.stIdPaciente + "'" + " AND D.GSOCIEDA = " + MCONSULTFILIAC.CodSS.ToString() + " AND (D.ginspecc is NOT null)";
				//------------------





				StSql = StSql + " ORDER BY FMOVIMIE DESC";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MCONSULTFILIAC.GConexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
					sprSociedades.MaxRows = 0;
					RrSql.MoveFirst();
					intContador = 1;
					foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
					{
						sprSociedades.MaxRows++;
						sprSociedades.Row = intContador;

						if (Convert.IsDBNull(iteration_row["fborrado"]))
						{
							lngColorRegistro = ColorTranslator.ToOle(shpActivado.FillColor);
						}
						else
						{
							lngColorRegistro = ColorTranslator.ToOle(shpDesactivado.FillColor);
						}

						sprSociedades.Col = Col_DSocieda;
						sprSociedades.Text = Convert.ToString(iteration_row["dsocieda"]).Trim();
						sprSociedades.ForeColor = ColorTranslator.FromOle(lngColorRegistro);

						sprSociedades.Col = Col_NAfiliac;
						if (Convert.IsDBNull(iteration_row["nafiliac"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							sprSociedades.Text = Convert.ToString(iteration_row["nafiliac"]).Trim();
						}
						sprSociedades.ForeColor = ColorTranslator.FromOle(lngColorRegistro);

						//Titular/Beneficiario
						sprSociedades.Col = Col_TitBenef;
						if (Convert.IsDBNull(iteration_row["itituben"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							if (Convert.ToString(iteration_row["itituben"]).Trim().ToUpper() == "T")
							{
								sprSociedades.Text = "Titular";
							}
							else if ((Convert.ToString(iteration_row["itituben"]).Trim().ToUpper() == "B"))
							{ 
								sprSociedades.Text = "Beneficiario";
							}
						}
						sprSociedades.ForeColor = ColorTranslator.FromOle(lngColorRegistro);

						//Cobertura
						sprSociedades.Col = Col_Cobertura;
						if (Convert.IsDBNull(iteration_row["ocobertu"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							sprSociedades.Text = Convert.ToString(iteration_row["ocobertu"]).Trim();
						}
						sprSociedades.ForeColor = ColorTranslator.FromOle(lngColorRegistro);

						//Fecha de Alta
						sprSociedades.Col = Col_FechaAlta;
						if (Convert.IsDBNull(iteration_row["finivige"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							sprSociedades.Text = StringsHelper.Format(DateTime.Parse(Convert.ToString(iteration_row["finivige"]).Trim()).Month, "00") + "/" + DateTime.Parse(Convert.ToString(iteration_row["finivige"]).Trim()).Year.ToString();
						}
						sprSociedades.ForeColor = ColorTranslator.FromOle(lngColorRegistro);

						//Fecha de Caducidad
						sprSociedades.Col = Col_FechaCad;
						if (Convert.IsDBNull(iteration_row["ffinvige"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							sprSociedades.Text = StringsHelper.Format(DateTime.Parse(Convert.ToString(iteration_row["ffinvige"]).Trim()).Month, "00") + "/" + DateTime.Parse(Convert.ToString(iteration_row["ffinvige"]).Trim()).Year.ToString();
						}
						sprSociedades.ForeColor = ColorTranslator.FromOle(lngColorRegistro);

						//Fecha de Borrado
						sprSociedades.Col = Col_FBorrado;
						if (Convert.IsDBNull(iteration_row["fborrado"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							System.DateTime TempDate = DateTime.FromOADate(0);
							sprSociedades.Text = (DateTime.TryParse(Convert.ToString(iteration_row["fborrado"]).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : Convert.ToString(iteration_row["fborrado"]).Trim();
						}
						sprSociedades.ForeColor = ColorTranslator.FromOle(lngColorRegistro);

						//C�digo de Sociedad
						sprSociedades.Col = Col_GSocieda;
						if (Convert.IsDBNull(iteration_row["gsocieda"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							sprSociedades.Text = Convert.ToString(iteration_row["gsocieda"]).Trim();
						}
						sprSociedades.ForeColor = ColorTranslator.FromOle(lngColorRegistro);
						sprSociedades.SetColHidden(sprSociedades.Col, true);

						intContador++;
					}
                    sprSociedades.ReadOnly = true;
				}

				RrSql.Close();
				RrSql = null;
				this.Cursor = Cursors.Default;
			}
			catch (SqlException ex)
			{
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(ex, MCONSULTFILIAC.GConexion);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		private void MostrarNombrePaciente(string stId_Paciente)
		{

			DataSet rdoTemp = null;
			string StSql = String.Empty;

			try
			{

				this.Cursor = Cursors.WaitCursor;

				StSql = "SELECT DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac FROM DPACIENT";
				StSql = StSql + " WHERE gidenpac = '" + stId_Paciente + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MCONSULTFILIAC.GConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);

				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					lbPaciente.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape2pac"]).Trim() + ", " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpac"]).Trim();
				}

				rdoTemp.Close();
				rdoTemp = null;

				this.Cursor = Cursors.Default;
			}
			catch (SqlException ex)
			{
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(ex, MCONSULTFILIAC.GConexion);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
		private void DFI140F1_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}