using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLConsultaFiliacE
{
	partial class DFI140F1
	{

		#region "Upgrade Support "
		private static DFI140F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI140F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI140F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprSociedades", "cmdSalir", "lbPaciente", "lbTituloPaciente", "Label4", "shpActivado", "shpDesactivado", "Label2", "Frame1", "ShapeContainer1", "sprSociedades_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread sprSociedades;
        public Telerik.WinControls.UI.RadButton cmdSalir;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadLabel lbTituloPaciente;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Microsoft.VisualBasic.PowerPacks.RectangleShape shpActivado;
		public Microsoft.VisualBasic.PowerPacks.RectangleShape shpDesactivado;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		
        //private FarPoint.Win.Spread.SheetView sprSociedades_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.shpActivado = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.shpDesactivado = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.sprSociedades = new UpgradeHelpers.Spread.FpSpread();
            this.cmdSalir = new Telerik.WinControls.UI.RadButton();
            this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.lbTituloPaciente = new Telerik.WinControls.UI.RadLabel();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sprSociedades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprSociedades.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTituloPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(2, 18);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.shpActivado,
            this.shpDesactivado});
            this.ShapeContainer1.Size = new System.Drawing.Size(735, 175);
            this.ShapeContainer1.TabIndex = 7;
            this.ShapeContainer1.TabStop = false;
            // 
            // shpActivado
            // 
            this.shpActivado.BorderColor = System.Drawing.SystemColors.WindowText;
            this.shpActivado.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.shpActivado.Enabled = false;
            this.shpActivado.FillColor = System.Drawing.Color.Blue;
            this.shpActivado.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.shpActivado.Location = new System.Drawing.Point(10, 146);
            this.shpActivado.Name = "shpActivado";
            this.shpActivado.Size = new System.Drawing.Size(43, 7);
            // 
            // shpDesactivado
            // 
            this.shpDesactivado.BorderColor = System.Drawing.SystemColors.WindowText;
            this.shpDesactivado.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.shpDesactivado.Enabled = false;
            this.shpDesactivado.FillColor = System.Drawing.Color.Gray;
            this.shpDesactivado.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.shpDesactivado.Location = new System.Drawing.Point(10, 164);
            this.shpDesactivado.Name = "shpDesactivado";
            this.shpDesactivado.Size = new System.Drawing.Size(43, 7);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.sprSociedades);
            this.Frame1.Controls.Add(this.cmdSalir);
            this.Frame1.Controls.Add(this.lbPaciente);
            this.Frame1.Controls.Add(this.lbTituloPaciente);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.ShapeContainer1);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(2, 2);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(739, 195);
            this.Frame1.TabIndex = 0;
            // 
            // sprSociedades
            // 
            this.sprSociedades.Location = new System.Drawing.Point(6, 40);
            // 
            // 
            // 
            gridViewTextBoxColumn1.HeaderText = "Sociedad / Inspecci�n";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 140;
            gridViewTextBoxColumn2.HeaderText = "N� Afiliaci�n";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 100;
            gridViewTextBoxColumn3.HeaderText = "Titular/Benef.";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.HeaderText = "Cobertura";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 100;
            gridViewTextBoxColumn5.HeaderText = "F. Inicio";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 70;
            gridViewTextBoxColumn6.HeaderText = "F. Fin";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 70;
            gridViewTextBoxColumn7.HeaderText = "F. Borrado";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.Width = 90;
            gridViewTextBoxColumn8.HeaderText = "Cod. Sociedad";
            gridViewTextBoxColumn8.Name = "column8";
            gridViewTextBoxColumn8.Width = 80;
            this.sprSociedades.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.sprSociedades.MasterTemplate.EnableSorting = false;
            this.sprSociedades.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.sprSociedades.Name = "sprSociedades";
            this.sprSociedades.ReadOnly = true;
            this.sprSociedades.Size = new System.Drawing.Size(724, 105);
            this.sprSociedades.TabIndex = 6;
            // 
            // cmdSalir
            // 
            this.cmdSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSalir.Location = new System.Drawing.Point(650, 154);
            this.cmdSalir.Name = "cmdSalir";
            this.cmdSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSalir.Size = new System.Drawing.Size(81, 31);
            this.cmdSalir.TabIndex = 1;
            this.cmdSalir.Text = "&Salir";
            this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            // 
            // lbPaciente
            // 
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Enabled = false;
            this.lbPaciente.Location = new System.Drawing.Point(80, 14);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(377, 20);
            this.lbPaciente.TabIndex = 5;
            // 
            // lbTituloPaciente
            // 
            this.lbTituloPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbTituloPaciente.Location = new System.Drawing.Point(8, 16);
            this.lbTituloPaciente.Name = "lbTituloPaciente";
            this.lbTituloPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbTituloPaciente.Size = new System.Drawing.Size(58, 18);
            this.lbTituloPaciente.TabIndex = 4;
            this.lbTituloPaciente.Text = "PACIENTE:";
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(70, 173);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(66, 18);
            this.Label4.TabIndex = 3;
            this.Label4.Text = "Desactivada";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(70, 156);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(49, 18);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "Activada";
            // 
            // DFI140F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 200);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI140F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta de Entidades Aseguradoras - DFI140F1";
            this.Closed += new System.EventHandler(this.DFI140F1_Closed);
            this.Load += new System.EventHandler(this.DFI140F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sprSociedades.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprSociedades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTituloPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}