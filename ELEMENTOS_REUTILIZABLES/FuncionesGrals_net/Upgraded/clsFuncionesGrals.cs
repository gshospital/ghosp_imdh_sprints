using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using System.Collections.Generic;
using Microsoft.CSharp;
using Telerik.WinControls.UI;


namespace FuncionesComunes
{
	public class clsFuncionesGrals
	{


		SqlConnection ConexionFunciones = null; // Se obtendra de la ClaseEstadistica.NombreConexion
		public void BorrarTablaBD(SqlConnection ConFunc, string stTablaTempBD)
		{
			try
			{
				SqlCommand tempCommand = new SqlCommand("drop table " + stTablaTempBD, ConFunc);
				tempCommand.ExecuteNonQuery();
			}
			catch
			{
                BorrarTablaBD(ConFunc, stTablaTempBD);
			}
		}

		public SqlConnection Conexion
		{
			get
			{
				return ConexionFunciones;
			}
			set
			{
				ConexionFunciones = value;
			}
		}


		// Rellena el object Lista con la sql que le mandemos en QUERY
		//public void proRellenarLista(List<string>[,] Lista, string Query, ref string StQueryRegreso)
        public void proRellenarLista(RadListControl Lista, string Query, ref string StQueryRegreso)
        {
			StQueryRegreso = Query;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Query, ConexionFunciones);
			DataSet RrUnidades = new DataSet();
			tempAdapter.Fill(RrUnidades);
			int ilista = 0;
			foreach (DataRow iteration_row in RrUnidades.Tables[0].Rows)
			{
				//Lista.AddItem(Convert.ToString(iteration_row[1]).Trim()); // columna 0
                Lista.Items.Add(new RadListDataItem(Convert.ToString(iteration_row[1]).Trim(), Convert.ToString(iteration_row[0]).Trim()));
                //Lista[ilista,1].Add( Convert.ToString(iteration_row[0]).Trim()); // columna 1 codigo
				ilista++;
			}
			RrUnidades.Close();
		}

        // INIC SDSALZAR TODO SPRIN_X_4
        //// devuelve la base de datos abierta
        //public Database AbrirBBDDyBorrarTabla(string TablaTemporal, string Path, string BBDDTemporal)
        //{
        //	Database result = null;
        //	object dbLangSpanish = null;
        //	object DBEngine = null;
        //	//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        //	if (FileSystem.Dir(Path + "\\" + BBDDTemporal, FileAttribute.Normal) != "")
        //	{ // si existe la abro y borro la tabla si existe
        //		//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
        //		result = (Database) DBEngine.Workspaces(0).OpenDatabase(Path + "\\" + BBDDTemporal);
        //		//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
        //		foreach (Tabla in result.TableDefs)
        //		{
        //			//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
        //			if (Convert.ToString(Tabla.Name).ToUpper() == TablaTemporal.ToUpper())
        //			{
        //				//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
        //				result.Execute("DROP TABLE " + TablaTemporal);
        //				break;
        //			}
        //		}
        //	}
        //	else
        //	{
        //		// SI NO EXISTE LA CREO
        //		//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
        //		result = (Database) DBEngine.Workspaces(0).CreateDatabase(Path + "\\" + BBDDTemporal, dbLangSpanish);
        //	}
        //	return result;
        //}
        // FIN SDSALZAR TODO SPRIN_X_4

        // Introduce algunas propiedades en la clase general que dependen de las
        // pantallas de entrada de datos
        public void IntroducirPropiedadesClaseEstadistica(int Destino, System.DateTime Desde, System.DateTime Hasta, dynamic ClaseEstadistica, int Copias = 1)
		{
			ClaseEstadistica.NumeroCopias = Copias;
			ClaseEstadistica.DestinoEstadistica = Destino;
			ClaseEstadistica.ValorDesde = Desde;
			ClaseEstadistica.ValorHasta = Hasta;

		}

		// obtiene de forma parametrizada valores de SCONSGLO
		public string ObtenerValoresSconsglo(string Codigo, string CampoObtener)
		{
			string result = String.Empty;
			string sqlConstante = "select " + CampoObtener + " from SCONSGLO where gconsglo ='" + Codigo.Trim().ToUpper() + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlConstante, Conexion);
			DataSet RrConstante = new DataSet();
			tempAdapter.Fill(RrConstante);
			if (RrConstante.Tables[0].Rows.Count == 1)
			{
				result = (!Convert.IsDBNull(Convert.ToString(RrConstante.Tables[0].Rows[0][0]).Trim())) ? Convert.ToString(RrConstante.Tables[0].Rows[0][0]).Trim() : "";
			}
			RrConstante.Close();
			return result;
		}

        //activa los botones de pantalla e impresora dependiendo del formulario
        //public void proActivarAceptar(object Formulario, object MascaraFecha_optional, object Lista1, object Lista2, string TbCod1, string TbCod2, object CheckTodos)
        public void proActivarAceptar(dynamic Formulario, object MascaraFecha_optional, List<double> Lista1, List<double> Lista2, string TbCod1, string TbCod2, RadCheckBox CheckTodos)
        {
			string MascaraFecha = (MascaraFecha_optional == Type.Missing) ? String.Empty : MascaraFecha_optional as string;

			switch(Convert.ToString((RadForm)Formulario.Name).Trim().ToUpper())
			{
				case "ELISTASNUEVAS" : 
					if (MascaraFecha_optional != Type.Missing)
					{

						//Formulario.cbPantalla.Enabled = (Convert.ToDouble(Lista2.ListCount) > 0 && Information.IsDate("01/" + MascaraFecha));
                        Formulario.cbPantalla.Enabled = (Convert.ToDouble(Lista2.Count) > 0 && Information.IsDate("01/" + MascaraFecha));
                        //Formulario.cbImpresora.Enabled = (Convert.ToDouble(Lista2.ListCount) > 0 && Information.IsDate("01/" + MascaraFecha));
                        Formulario.cbImpresora.Enabled = (Convert.ToDouble(Lista2.Count) > 0 && Information.IsDate("01/" + MascaraFecha));
                        //Formulario.cbRecibirDatos.Enabled = (Convert.ToDouble(Lista2.ListCount) > 0 && Information.IsDate("01/" + MascaraFecha));
                        Formulario.cbRecibirDatos.Enabled = (Convert.ToDouble(Lista2.Count) > 0 && Information.IsDate("01/" + MascaraFecha));
                    }
					else
					{
						// ojo!!! como hay una nueva con listas y fecha desde y hasta se lo mando en tbcod1 y tbcod2 las fechas

						//Formulario.cbPantalla.Enabled = (Convert.ToDouble(Lista2.ListCount) > 0 && Information.IsDate(TbCod1) && Information.IsDate(TbCod2));
                        Formulario.cbPantalla.Enabled = (Convert.ToDouble(Lista2.Count) > 0 && Information.IsDate(TbCod1) && Information.IsDate(TbCod2));
                        //Formulario.cbImpresora.Enabled = (Convert.ToDouble(Lista2.ListCount) > 0 && Information.IsDate(TbCod1) && Information.IsDate(TbCod2));
                        Formulario.cbImpresora.Enabled = (Convert.ToDouble(Lista2.Count) > 0 && Information.IsDate(TbCod1) && Information.IsDate(TbCod2));

                    } 
					 
					break;
				case "EMENSUALNUEVAS" : 
					Formulario.cbPantalla.Enabled = Information.IsDate("01/" + MascaraFecha); 
					Formulario.cbImpresora.Enabled = Information.IsDate("01/" + MascaraFecha); 
					Formulario.cbRecibirDatos.Enabled = Information.IsDate("01/" + MascaraFecha); 
					break;
				case "EDESDEHASTA" : case "EDESDEHASTANUEVA" : case "ELISTASDESDEHASTA" : 
					Formulario.cbPantalla.Enabled = (Convert.ToString(Lista1) != "" && Convert.ToString(Lista2) != ""); 
					Formulario.cbImpresora.Enabled = (Convert.ToString(Lista1) != "" && Convert.ToString(Lista2) != ""); 
					break;
				case "ECODIGOSCAJASTEXTO" : 
					Formulario.cbPantalla.Enabled = (((TbCod1.Trim() != "" && TbCod2.Trim() != "") || Convert.ToDouble(CheckTodos.Checked) == 1) && Information.IsDate("01/" + MascaraFecha)); 
					Formulario.cbImpresora.Enabled = (((TbCod1.Trim() != "" && TbCod2.Trim() != "") || Convert.ToDouble(CheckTodos.Checked) == 1) && Information.IsDate("01/" + MascaraFecha)); 
					Formulario.cbRecibirDatos.Enabled = (((TbCod1.Trim() != "" && TbCod2.Trim() != "") || Convert.ToDouble(CheckTodos.Checked) == 1) && Information.IsDate("01/" + MascaraFecha)); 
					break;
				case "EDOSLISTAS" : 
					//Formulario.cbPantalla.Enabled = (Convert.ToDouble(Lista1.ListCount) > 0 && Convert.ToDouble(Lista2.ListCount) > 0 && Information.IsDate("01/" + MascaraFecha));
                    Formulario.cbPantalla.Enabled = (Convert.ToDouble(Lista1.Count) > 0 && Convert.ToDouble(Lista2.Count) > 0 && Information.IsDate("01/" + MascaraFecha));
                    //Formulario.cbImpresora.Enabled = (Convert.ToDouble(Lista1.ListCount) > 0 && Convert.ToDouble(Lista2.ListCount) > 0 && Information.IsDate("01/" + MascaraFecha));
                    Formulario.cbImpresora.Enabled = (Convert.ToDouble(Lista1.Count) > 0 && Convert.ToDouble(Lista2.Count) > 0 && Information.IsDate("01/" + MascaraFecha));
                    //Formulario.cbRecibirDatos.Enabled = (Convert.ToDouble(Lista1.ListCount) > 0 && Convert.ToDouble(Lista2.ListCount) > 0 && Information.IsDate("01/" + MascaraFecha));
                    Formulario.cbRecibirDatos.Enabled = (Convert.ToDouble(Lista1.Count) > 0 && Convert.ToDouble(Lista2.Count) > 0 && Information.IsDate("01/" + MascaraFecha));
                    break;
			}
		}

		public void proActivarAceptar(object Formulario, object MascaraFecha_optional, List<double> Lista1, List<double> Lista2, string TbCod1, string TbCod2)
		{
			proActivarAceptar(Formulario, MascaraFecha_optional, Lista1, Lista2, TbCod1, TbCod2, null);
		}

		public void proActivarAceptar(object Formulario, object MascaraFecha_optional, List<double> Lista1, List<double> Lista2, string TbCod1)
		{
			proActivarAceptar(Formulario, MascaraFecha_optional, Lista1, Lista2, TbCod1, String.Empty, null);
		}

		public void proActivarAceptar(object Formulario, object MascaraFecha_optional, List<double> Lista1, List<double> Lista2)
		{
			proActivarAceptar(Formulario, MascaraFecha_optional, Lista1, Lista2, String.Empty, String.Empty, null);
		}

		public void proActivarAceptar(object Formulario, object MascaraFecha_optional, List<double> Lista1)
		{
			proActivarAceptar(Formulario, MascaraFecha_optional, Lista1, null, String.Empty, String.Empty, null);
		}

		public void proActivarAceptar(object Formulario, object MascaraFecha_optional)
		{
			proActivarAceptar(Formulario, MascaraFecha_optional, null, null, String.Empty, String.Empty, null);
		}

		public void proActivarAceptar(object Formulario)
		{
			proActivarAceptar(Formulario, Type.Missing, null, null, String.Empty, String.Empty, null);
		}

		public void ObtenerFechas(ref System.DateTime FechaDesde, ref System.DateTime FechaHasta, object MascaraFecha_optional, RadDateTimePicker DateCombo1, RadDateTimePicker DateCombo2)
		{
			//object MascaraFecha = (MascaraFecha_optional == Type.Missing) ? null : MascaraFecha_optional as object;
            RadMaskedEditBox MascaraFecha = (MascaraFecha_optional == Type.Missing) ? null : MascaraFecha_optional as RadMaskedEditBox;
            //  MascaraFecha puede ser mensual o  anual -> mask)
            if (MascaraFecha_optional != Type.Missing)
			{
				if (Convert.ToString(MascaraFecha.Mask) == "9999")
				{ // anual
					FechaDesde = DateTime.Parse("01/01/" + Convert.ToString(MascaraFecha).Trim());
					FechaHasta = DateTime.Parse("31/12/" + Convert.ToString(MascaraFecha).Trim());
				}
				else
				{
					FechaDesde = DateTime.Parse("01/" + Convert.ToString(MascaraFecha).Trim());
					FechaHasta = DateTime.Parse(((int) DateAndTime.DateDiff("d", FechaDesde, FechaDesde.AddMonths(1), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)).ToString() + "/" + Convert.ToString(MascaraFecha).Trim());
					// hallo la difenrencia en tre el uno y el uno del mes sgte
				}
			}
			else
			{
				//combos de fechas
				FechaDesde =DateCombo1.Value;
				FechaHasta = DateCombo2.Value;
			}
		}

		public void ObtenerFechas(ref System.DateTime FechaDesde, ref System.DateTime FechaHasta, object MascaraFecha_optional, RadDateTimePicker DateCombo1)
		{
			ObtenerFechas(ref FechaDesde, ref FechaHasta, MascaraFecha_optional, DateCombo1, null);
		}

		public void ObtenerFechas(ref System.DateTime FechaDesde, ref System.DateTime FechaHasta, object MascaraFecha_optional)
		{
			ObtenerFechas(ref FechaDesde, ref FechaHasta, MascaraFecha_optional, null, null);
		}

		public void ObtenerFechas(ref System.DateTime FechaDesde, ref System.DateTime FechaHasta)
		{
			ObtenerFechas(ref FechaDesde, ref FechaHasta, Type.Missing, null, null);
		}
		public bool ValidarFechas(object mebFecha_optional, RadDateTimePicker Fecha1, RadDateTimePicker Fecha2)
		{
            RadMaskedEditBox mebFecha = (mebFecha_optional == Type.Missing) ? null : mebFecha_optional as RadMaskedEditBox;
			if (mebFecha_optional != Type.Missing)
			{
				if (Convert.ToString(mebFecha.Mask) == "9999")
				{ // si hay qu validar solo con el a�o
					if (Convert.ToString(mebFecha).Trim().Length != 4)
					{
						MessageBox.Show("Formato de a�o incorrecto", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
						mebFecha.Focus();
						return false;
					}
					if (Convert.ToInt32(mebFecha) > DateTime.Now.Year)
					{
						MessageBox.Show("El a�o debe ser menor o igual que el actual", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
						mebFecha.Focus();
						return false;
					}
				}
				else
				{
					// mascara en mes y a�o
					if (StringsHelper.ToDoubleSafe(Strings.Len(Convert.ToString(mebFecha)).ToString().Trim()) != 7)
					{
						MessageBox.Show("Formato de fecha incorrecto (mm/yyyy)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
						mebFecha.Focus();
						return false;
					}
					if (String.CompareOrdinal(Convert.ToString(mebFecha).Substring(0, Math.Min(2, Convert.ToString(mebFecha).Length)), "12") > 0)
					{
						MessageBox.Show("Formato de mes incorrecto", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
						mebFecha.Focus();
						return false;
					}
					if (DateTime.Parse("01/" + Convert.ToString(mebFecha)) > DateTime.Parse("01/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString()))
					{
						MessageBox.Show("El mes-a�o debe ser menor o igual que el actual", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
						mebFecha.Focus();
						return false;
					}
				}
			}
			else
			{
				// FORMULARIO CON COMBOS DE FECHAS
				if (!Information.IsDate(Fecha1))
				{
					MessageBox.Show("Fecha Desde incorrecta", Application.ProductName);
					Fecha1.Focus();
					return false;
				}
				if (!Information.IsDate(Fecha2))
				{
					MessageBox.Show("Fecha Hasta incorrecta", Application.ProductName);
					Fecha2.Focus();
					return false;
				}
				if (Convert.ToDateTime(Fecha1) > DateTime.Now || Convert.ToDateTime(Fecha2) > DateTime.Now)
				{
					MessageBox.Show("Fecha " + ((Convert.ToDateTime(Fecha1) > DateTime.Now) ? "desde" : "hasta") + " debe ser menor que la fecha actual", Application.ProductName);
					if (Convert.ToDateTime(Fecha1) > DateTime.Now)
					{
						Fecha1.Focus();
					}
					else
					{
						Fecha2.Focus();
					}
					return false;
				}
				if (Convert.ToDateTime(Fecha1) > Convert.ToDateTime(Fecha2))
				{
					MessageBox.Show("Fecha desde debe ser menor que la fecha hasta", Application.ProductName);
					return false;
				}
			}
			return true;
		}

		public bool ValidarFechas(object mebFecha_optional, RadDateTimePicker Fecha1)
		{
			return ValidarFechas(mebFecha_optional, Fecha1, null);
		}

		public bool ValidarFechas(object mebFecha_optional)
		{
			return ValidarFechas(mebFecha_optional, null, null);
		}

		public bool ValidarFechas()
		{
			return ValidarFechas(Type.Missing, null, null);
		}


       
        public void RegistrarDsnAccess(dynamic ClaseEstadistica)
        {
            // DSN PARA ESTABLECER EL ORIGEN DE DATOS EN ACCESS PARA LAS TABLAS TEMPORALES
            //una conexi�n DSN para los controles Crystal de la aplicaci�n para los RPT
            const string NOMBRE_DSN_ACCESS = "GHOSP_ACCESS";
            //UPGRADE_TODO: (1067) Member BBDDTemporal is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //UPGRADE_TODO: (1067) Member Path is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            string tstconexion = "Description=" + "Microsoft Access para Estad�sticas " + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "" + "\r" + "dbq=" + Convert.ToString(ClaseEstadistica.Path) + "\\" + Convert.ToString(ClaseEstadistica.BBDDTemporal) + "";
            // INIC SDSALAZAR NOTODO_X_4
            //UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(NOMBRE_DSN_ACCESS, "MicroSoft Access Driver (*.mdb)", true, tstconexion);
            // INIC SDSALAZAR NOTODO_X_4

        }


        // IMPORTANTE : DATEDIFF CON SEMANAS NO ES FIABLE
        private void tratamiento1(int[] arraydias, dynamic ClaseEstadistica)
		{
			// inicializo el array con valores significativos para saber los que quedan por rellenar
			for (int ibucle = 1; ibucle <= 7; ibucle++)
			{
				arraydias[ibucle] = -1;
			}
			// dias que hay entre medias de los dos dias sin incluirlos
			for (int ibucle = DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1); ibucle <= 7; ibucle++)
			{
				if (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) < 7)
				{ // si no hay ninguna semana de diferencia
					arraydias[ibucle] = 1; // le metemos 0
				}
				else
				{
					arraydias[ibucle] = ((int) ((((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) > 0) ? Math.Floor((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) : Math.Ceiling((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))))) + 1; // si no el numero de semanas m�s uno
				}
			}
			int tempForVar = DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1);
			for (int ibucle = 1; ibucle <= tempForVar; ibucle++)
			{
				if (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) < 7)
				{ // si no hay ninguna semana de diferencia
					arraydias[ibucle] = 1; // le metemos 0
				}
				else
				{
					arraydias[ibucle] = ((int) ((((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) > 0) ? Math.Floor((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) : Math.Ceiling((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))))) + 1; // si no el numero de semanas m�s uno
				}
			}
			// del primer dia incluido hasta el ultimo incluido va a ser igual al numero de semanas del intervalo
			for (int ibucle = 1; ibucle <= 7; ibucle++)
			{
				if (arraydias[ibucle] == -1)
				{ // si no se ha rellenado antes rellenarlo ahora
					if (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) < 7)
					{ // si no hay ninguna semana de diferencia
						arraydias[ibucle] = 0;
					}
					else
					{
						arraydias[ibucle] = (int) ((((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) > 0) ? Math.Floor((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) : Math.Ceiling((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7)))); // meto uno en caso de que haya menos de una semana
					}
				}
			}
		}



		// para los dias que existen entre los dos dias del intervalo incluyendo a ambos
		// el numero de dias es el numero de semanas +1
		// para el resto es el numero de semanas
		// para el tratamiento 1 es al contrario

		private void tratamiento2(int[] arraydias, dynamic ClaseEstadistica)
		{
			// inicializo el array con valores significativos para saber los que quedan por rellenar
			for (int ibucle = 1; ibucle <= 7; ibucle++)
			{
				arraydias[ibucle] = -1;
			}
			// dias que hay entre medias de los dos dias sin incluirlos
			int tempForVar = DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1);
			for (int ibucle = DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1); ibucle <= tempForVar; ibucle++)
			{
				if (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) < 7)
				{ // si no hay ninguna semana de diferencia
					arraydias[ibucle] = 1; // le metemos 0
				}
				else
				{
					arraydias[ibucle] = ((int) ((((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) > 0) ? Math.Floor((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) : Math.Ceiling((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))))) + 1; // si no el numero de semanas m�s uno
				}
			}
			// del primer dia incluido hasta el ultimo incluido va a ser igual al numero de semanas del intervalo
			for (int ibucle = 1; ibucle <= 7; ibucle++)
			{
				if (arraydias[ibucle] == -1)
				{
					if (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) < 7)
					{ // si no hay ninguna semana de diferencia
						arraydias[ibucle] = 0;
					}
					else
					{
						arraydias[ibucle] = (int) ((((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) > 0) ? Math.Floor((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) : Math.Ceiling((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7)))); // meto uno en caso de que haya menos de una semana
					}
				}
			}
		}

		// no controlo que te meta el mismo dia para los dos intervalos
		// cuando es el mismo dia p.e. de jueves 1 a jueves 8  para el jueves es el numero
		// de semanas +1  y para los otros el numero de semanas
		private void tratamiento3(int[] arraydias, dynamic ClaseEstadistica)
		{
			for (int ibucle = 1; ibucle <= 7; ibucle++)
			{
				if (ibucle == DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1))
				{
					arraydias[ibucle] = ((int) ((((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) > 0) ? Math.Floor((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) : Math.Ceiling((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))))) + 1;
				}
				else
				{
					arraydias[ibucle] = (int) ((((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) > 0) ? Math.Floor((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))) : Math.Ceiling((double) (((int) DateAndTime.DateDiff("d", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) / ((int) 7))));
				}
			}
		}

		public void IntroducirDiasSemana(int[] array_dias, dynamic ClaseEstadistica)
		{
			// ***** RUTINAS PARA METER EL NUMERO DE DIAS DE CADA SEMANA QUE HAY EN EL INTERVALO *******
			// si el numero de semana de la fecha incial es mayor,igual o menor entonces hacer una cosa
			if (DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1) > DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1))
			{
				tratamiento1(array_dias, ClaseEstadistica);
			}
			if (DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1) < DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1))
			{
				tratamiento2(array_dias, ClaseEstadistica);
			}
			if (DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorDesde), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1) == DateAndTime.DatePart("w", Convert.ToDateTime(ClaseEstadistica.ValorHasta), Microsoft.VisualBasic.FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1))
			{
				tratamiento3(array_dias, ClaseEstadistica);
			}

		}
        
        //INI SDSALAZAR NOTODO_X_4
        //public void proRellenarTramosHoras(Recordset Rs, string NombreCampo)
        //{
        //    for (int ibucle = 0; ibucle <= 23; ibucle++)
        //    {
        //        //UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
        //        Rs.AddNew();
        //        //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
        //        Rs["" + NombreCampo + ""] = (Recordset)(StringsHelper.Format(ibucle, "00") + " horas");
        //        //UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
        //        Rs.Update();
        //    }
        //}
        //FIN SDSALAZAR NOTODO_X_4

        // CUIDADO SI ES URGENCIA DOMICILIARIA
        public DataSet proObtenerTotalesPorDiaSemana(dynamic ClaseEstadistica, string stTabla = "UEPISURG", string stFecha1 = "fllegada", string stFecha2 = "fatencio")
		{
			//sql = "select count(" & stTabla & ".gidenpac) TOTALPACIENTES," & _
			//'            " Case DatePart(dw, fllegada) " & _
			//'                         " when 1 THEN 'DOMINGO'" & _
			//'                         " when 2 THEN 'LUNES'" & _
			//'                         " when 3 THEN 'MARTES'" & _
			//'                         " when 4 THEN 'MIERCOLES'" & _
			//'                         " when 5 THEN 'JUEVES'" & _
			//'                         " when 6 THEN 'VIERNES'" & _
			//'                         " when 7 THEN 'SABADO'" & _
			//'            " End DIASEMANA,DatePart(dw, fllegada) NUMERODIASEMANA,sum(datediff(mi,fllegada,fatencio)) SUM_DIF_LLEGADA_ATENCION" & _
			//'            " From " & _
			//'            " uepisurg " & _
			//'            " where fllegada>='" & Format(ClaseEstadistica.ValorDesde, "mm/dd/yyyy hh:nn:ss") & "'" & _
			//'            " and fllegada<='" & Format(ClaseEstadistica.ValorHasta, "mm/dd/yyyy") & " 23:59:59" & "'"
			string sql = "select count(" + stTabla + ".gidenpac) TOTALPACIENTES," + 
			             " Case DatePart(dw, " + stFecha1 + ") " + 
			             " when 1 THEN 'DOMINGO'" + 
			             " when 2 THEN 'LUNES'" + 
			             " when 3 THEN 'MARTES'" + 
			             " when 4 THEN 'MIERCOLES'" + 
			             " when 5 THEN 'JUEVES'" + 
			             " when 6 THEN 'VIERNES'" + 
			             " when 7 THEN 'SABADO'" + 
			             " End DIASEMANA,DatePart(dw, " + stFecha1 + ") NUMERODIASEMANA,sum(datediff(mi," + stFecha1 + "," + stFecha2 + ")) SUM_DIF_LLEGADA_ATENCION" + 
			             " From " + stTabla + " where " + stFecha1 + ">='" + Convert.ToDateTime(ClaseEstadistica.ValorDesde).ToString("MM/dd/yyyy HH:mm:ss") + "'" + 
			             " and " + stFecha1 + "<='" + Convert.ToDateTime(ClaseEstadistica.ValorHasta).ToString("MM/dd/yyyy") + " 23:59:59" + "'";
			if (Convert.ToString(ClaseEstadistica.ModuloEstadistica) == "U" && Convert.ToDouble(ClaseEstadistica.NumeroEstadistica) == 29)
			{ // Urgencias a domicilio
				sql = sql + " and iurgdomi = 'S'";
			}
			if (Convert.ToString(ClaseEstadistica.ModuloEstadistica) == "C" && Convert.ToDouble(ClaseEstadistica.NumeroEstadistica) == 14)
			{ // Consultas por horas
				sql = sql + " and ipresent = 'S'";
			}
			sql = sql + " GROUP BY  datepart(dw," + stFecha1 + "),DatePart(dw, " + stFecha1 + ")";

			//***************** O.Frias (SQL-2005) *****************
			sql = sql + " ORDER BY  DatePart(dw, " + stFecha1 + ")";

            //***************** O.Frias (SQL-2005) *****************

            //INI SDSALAZAR TODO_X_4
            //return (DataSet)ClaseEstadistica.NombreConexion.OpenResultset(sql, UpgradeStubs.RDO_ResultsetTypeConstants.getrdOpenKeyset());
            //SDSALAZAR SE PONER PARA EMULAR EL RETURN
            return (DataSet)ClaseEstadistica.NombreConexion.OpenResultset(sql, "");
            //FIN SDSALAZAR TODO_X_4
        }


        public DataSet proObtenerTramosEdad(string GFormulario, dynamic ClaseEstadistica, bool Registronulo, object bTipoConexion)
		{
			DataSet Rr = null;
			string sqlRr = "select * from DTRAEDAD where gformulario='" + GFormulario + "' ";
			if (Registronulo)
			{
				sqlRr = sqlRr + "Union select null,null,null,null ";
			}
			sqlRr = sqlRr + "ORDER BY IMESANOS DESC, cedadmin asc";
			if (bTipoConexion != Type.Missing && Convert.ToBoolean(bTipoConexion))
			{
                //INIC SDSALAZAR TODO_X_4
                //UPGRADE_ISSUE: (2064) RDO.ResultsetTypeConstants property ResultsetTypeConstants.rdOpenKeyset was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //UPGRADE_TODO: (1067) Member OpenResultset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //Rr = (DataSet) ClaseEstadistica.OpenResultset(sqlRr, UpgradeStubs.RDO_ResultsetTypeConstants.getrdOpenKeyset());
                //FIN SDSALAZAR TODO_X_4
            }
            else
			{
                //INIC SDSALAZAR TODO_X_4
                //UPGRADE_ISSUE: (2064) RDO.ResultsetTypeConstants property ResultsetTypeConstants.rdOpenKeyset was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //UPGRADE_TODO: (1067) Member NombreConexion is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //Rr = (DataSet) ClaseEstadistica.NombreConexion.OpenResultset(sqlRr, UpgradeStubs.RDO_ResultsetTypeConstants.getrdOpenKeyset());
                //FIN SDSALAZAR TODO_X_4
            }
            return Rr;
			//Rr.Close
		}


		public string ObtenerProvincia(string CodPoblacion)
		{
			string result = String.Empty;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select dnomprov from dprovinc where gprovinc ='" + CodPoblacion.Substring(0, Math.Min(2, CodPoblacion.Length)) + "'", Conexion);
			DataSet Rr = new DataSet();
			tempAdapter.Fill(Rr);
			if (Rr.Tables[0].Rows.Count == 1)
			{
				result = Convert.ToString(Rr.Tables[0].Rows[0][0]).Trim();
			}
			else
			{
				result = "";
			}
			Rr.Close();
			return result;
		}

		public string ObtenerPoblacion(string CodPoblacion)
		{
			string result = String.Empty;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select dpoblaci from dpoblaci where gpoblaci='" + CodPoblacion + "'", Conexion);
			DataSet Rr = new DataSet();
			tempAdapter.Fill(Rr);
			if (Rr.Tables[0].Rows.Count == 1)
			{
				result = Convert.ToString(Rr.Tables[0].Rows[0][0]).Trim();
			}
			else
			{
				result = "";
			}
			Rr.Close();
			return result;
		}

		// Tipo de servicio -> "U" MOTIVOS PARA ESTADISTICAS DE URGENCIAS
		//                   -> "A" MOTIVOS PARA ESTADISTICAS DE ADMISION
		public string TipoMotivoDeAlta(DataSet RrMotivos, string TipoServicio)
		{
			// itraslad , ipasohos, ipasocon, idomicil, ivolunta, icexitus
			if (TipoServicio == "U")
			{ // URGENCIAS
				if (Convert.ToString(RrMotivos.Tables[0].Rows[0]["itraslad"]) == "S")
				{
					return "DER_";
				}
				else
				{
					if (Convert.ToString(RrMotivos.Tables[0].Rows[0]["ipasohos"]) == "S")
					{
						return "INGR_";
					}
					else
					{
						if (Convert.ToString(RrMotivos.Tables[0].Rows[0]["idomicil"]) == "S")
						{
							return "CUR_";
						}
						else
						{
							if (Convert.ToString(RrMotivos.Tables[0].Rows[0]["icexitus"]) == "S")
							{
								return "EXIT_";
							}
							else
							{
								return "OCAUS_";
							}
						}
					}
				}
			}
			else
			{
				// ADMISION
				if (Convert.ToString(RrMotivos.Tables[0].Rows[0]["idomicil"]) == "S")
				{
					return "CUR_";
				}
				else
				{
					if (Convert.ToString(RrMotivos.Tables[0].Rows[0]["itraslad"]) == "S")
					{
						return "DER_";
					}
					else
					{
						if (Convert.ToString(RrMotivos.Tables[0].Rows[0]["icexitus"]) == "S")
						{
							return "EXIT_";
						}
						else
						{
							return "OCAUS_";
						}
					}
				}
			}
		}

		// OBTIENE EL IN PARA CONCATENAR EL TIPO DE PRESTACION OBJETO DE ESTADISTICA
		// SE OBTIENE DE SCONSGLO NNUMER1,2,3
		public string ObtenerQueryTiposPrestaciones(dynamic ClaseEstadistica)
		{
			string result = String.Empty;
			result = "";
			if (Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons1) != "")
			{
				result = Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons1);
				if (Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons2) != "")
				{
					result = result + "," + Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons2);
					if (Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons3) != "")
					{
						result = result + "," + Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons3);
					}
				}
				else
				{
					// si el 2 es blanco
					if (Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons3) != "")
					{
						result = result + "," + Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons3);
					}
				}

			}
			else
			{
				//1 blancos
				if (Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons2) != "")
				{
					result = Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons2);
					if (Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons3) != "")
					{
						result = result + "," + Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons3);
					}
				}
				else
				{
					result = Convert.ToString(ClaseEstadistica.TipoPrestacionEstCons3);
				}
			}
			return result;
		}
	}
}