using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLPacHosp
{
	partial class DFI161F1
	{

		#region "Upgrade Support "
		private static DFI161F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI161F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI161F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbCerrar", "cbBuscar", "_rbSexo_0", "_rbSexo_1", "frmSexo", "tbNombre", "tbApellido1", "tbApellido2", "sdcFechaNac", "Label70", "Label69", "Label68", "Label67", "FrmDatosPersonales", "_Cedula_0", "_Cedula_1", "_Cedula_2", "_Cedula_3", "cbbArchivo", "_rbBuscar_2", "_rbBuscar_1", "_rbBuscar_0", "tbNSS", "tbNHistoria", "MEBDNI", "Label6", "Label1", "Label71", "FrmBuscar", "sprFiliados", "_tsPac_TabPage0", "Label4", "Label3", "Label2", "lbUPaciente", "lbUHistoria", "lbUEntidad", "lbUNAsegurado", "Label32", "_pctAutoInfor_0", "_lblAutoInfor_0", "Label14", "Label13", "Label12", "Label11", "Label10", "Label9", "lbUFAtencion", "lbUHAtencion", "lbUNOrden", "lbUEspec", "lbUBox", "lbUCausa", "Frame3", "_tsPac_TabPage1", "Label33", "Label31", "Label30", "lbHPaciente", "lbHHistoria", "lbHEntidad", "lbHNAsegurado", "Label5", "_pctAutoInfor_1", "_lblAutoInfor_1", "lblTelefono", "Label7", "lblmotausen", "lblfmotausen", "lblfausencia", "lblausente", "Label19", "Label18", "Label17", "Label16", "Label15", "lbHServicio", "lbHHabitacion", "lbHCama", "lbHUnidad", "lbHPlanta", "Frame4", "_tsPac_TabPage2", "Label41", "Label40", "Label39", "lbIPaciente", "lbIHistoria", "lbIEntidad", "lbINAsegurado", "Label34", "Label20", "Label21", "Label22", "lbIFPIngreso", "lbIFPIntervencion", "lbIServicio", "Frame5", "_tsPac_TabPage3", "Label49", "Label48", "Label47", "lbDPaciente", "lbDHistoria", "lbDEntidad", "lbDNAsegurado", "Label42", "_pctAutoInfor_3", "_lblAutoInfor_3", "Label23", "Label24", "Label25", "lbDFAlta", "lbDFIngreso", "lbDMAlta", "Frame6", "_tsPac_TabPage4", "tsPac", "Cedula", "lblAutoInfor", "pctAutoInfor", "rbBuscar", "rbSexo", "sprFiliados_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadButton cbBuscar;
		private Telerik.WinControls.UI.RadRadioButton _rbSexo_0;
		private Telerik.WinControls.UI.RadRadioButton _rbSexo_1;
		public Telerik.WinControls.UI.RadGroupBox frmSexo;
		public Telerik.WinControls.UI.RadTextBoxControl tbNombre;
		public Telerik.WinControls.UI.RadTextBoxControl tbApellido1;
		public Telerik.WinControls.UI.RadTextBoxControl tbApellido2;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechaNac;
		public Telerik.WinControls.UI.RadLabel Label70;
		public Telerik.WinControls.UI.RadLabel Label69;
		public Telerik.WinControls.UI.RadLabel Label68;
		public Telerik.WinControls.UI.RadLabel Label67;
		public Telerik.WinControls.UI.RadGroupBox FrmDatosPersonales;
		private Telerik.WinControls.UI.RadTextBox _Cedula_0;
		private Telerik.WinControls.UI.RadTextBox _Cedula_1;
		private Telerik.WinControls.UI.RadTextBox _Cedula_2;
		private Telerik.WinControls.UI.RadTextBox _Cedula_3;
		public Telerik.WinControls.UI.RadDropDownList cbbArchivo;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_2;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_1;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_0;
		public Telerik.WinControls.UI.RadTextBoxControl tbNSS;
		public Telerik.WinControls.UI.RadTextBoxControl tbNHistoria;
		public Telerik.WinControls.UI.RadMaskedEditBox MEBDNI;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label71;
		public Telerik.WinControls.UI.RadGroupBox FrmBuscar;
		public UpgradeHelpers.Spread.FpSpread sprFiliados;
		private Telerik.WinControls.UI.RadPageViewPage _tsPac_TabPage0;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadTextBox lbUPaciente;
		public Telerik.WinControls.UI.RadTextBox lbUHistoria;
		public Telerik.WinControls.UI.RadTextBox lbUEntidad;
		public Telerik.WinControls.UI.RadTextBox lbUNAsegurado;
		public Telerik.WinControls.UI.RadLabel Label32;
		private System.Windows.Forms.PictureBox _pctAutoInfor_0;
		private Telerik.WinControls.UI.RadLabel _lblAutoInfor_0;
		public Telerik.WinControls.UI.RadLabel Label14;
		public Telerik.WinControls.UI.RadLabel Label13;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadLabel Label11;
		public Telerik.WinControls.UI.RadLabel Label10;
		public Telerik.WinControls.UI.RadLabel Label9;
		public Telerik.WinControls.UI.RadTextBox lbUFAtencion;
		public Telerik.WinControls.UI.RadTextBox lbUHAtencion;
		public Telerik.WinControls.UI.RadTextBox lbUNOrden;
		public Telerik.WinControls.UI.RadTextBox lbUEspec;
		public Telerik.WinControls.UI.RadTextBox lbUBox;
		public Telerik.WinControls.UI.RadTextBox lbUCausa;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		private Telerik.WinControls.UI.RadPageViewPage _tsPac_TabPage1;
		public Telerik.WinControls.UI.RadLabel Label33;
		public Telerik.WinControls.UI.RadLabel Label31;
		public Telerik.WinControls.UI.RadLabel Label30;
		public Telerik.WinControls.UI.RadTextBox lbHPaciente;
		public Telerik.WinControls.UI.RadTextBox lbHHistoria;
		public Telerik.WinControls.UI.RadTextBox lbHEntidad;
		public Telerik.WinControls.UI.RadTextBox lbHNAsegurado;
		public Telerik.WinControls.UI.RadLabel Label5;
		private System.Windows.Forms.PictureBox _pctAutoInfor_1;
		private Telerik.WinControls.UI.RadLabel _lblAutoInfor_1;
		public Telerik.WinControls.UI.RadTextBox lblTelefono;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadTextBox lblmotausen;
		public Telerik.WinControls.UI.RadTextBox lblfmotausen;
		public Telerik.WinControls.UI.RadLabel lblfausencia;
		public Telerik.WinControls.UI.RadLabel lblausente;
		public Telerik.WinControls.UI.RadLabel Label19;
		public Telerik.WinControls.UI.RadLabel Label18;
		public Telerik.WinControls.UI.RadLabel Label17;
		public Telerik.WinControls.UI.RadLabel Label16;
		public Telerik.WinControls.UI.RadLabel Label15;
		public Telerik.WinControls.UI.RadTextBox lbHServicio;
		public Telerik.WinControls.UI.RadTextBox lbHHabitacion;
		public Telerik.WinControls.UI.RadTextBox lbHCama;
		public Telerik.WinControls.UI.RadTextBox lbHUnidad;
		public Telerik.WinControls.UI.RadTextBox lbHPlanta;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		private Telerik.WinControls.UI.RadPageViewPage _tsPac_TabPage2;
		public Telerik.WinControls.UI.RadLabel Label41;
		public Telerik.WinControls.UI.RadLabel Label40;
		public Telerik.WinControls.UI.RadLabel Label39;
		public Telerik.WinControls.UI.RadTextBox lbIPaciente;
		public Telerik.WinControls.UI.RadTextBox lbIHistoria;
		public Telerik.WinControls.UI.RadTextBox lbIEntidad;
		public Telerik.WinControls.UI.RadTextBox lbINAsegurado;
		public Telerik.WinControls.UI.RadLabel Label34;
		public Telerik.WinControls.UI.RadLabel Label20;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadLabel Label22;
		public Telerik.WinControls.UI.RadTextBox lbIFPIngreso;
		public Telerik.WinControls.UI.RadTextBox lbIFPIntervencion;
		public Telerik.WinControls.UI.RadTextBox lbIServicio;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		private Telerik.WinControls.UI.RadPageViewPage _tsPac_TabPage3;
		public Telerik.WinControls.UI.RadLabel Label49;
		public Telerik.WinControls.UI.RadLabel Label48;
		public Telerik.WinControls.UI.RadLabel Label47;
		public Telerik.WinControls.UI.RadTextBox lbDPaciente;
		public Telerik.WinControls.UI.RadTextBox lbDHistoria;
		public Telerik.WinControls.UI.RadTextBox lbDEntidad;
		public Telerik.WinControls.UI.RadTextBox lbDNAsegurado;
		public Telerik.WinControls.UI.RadLabel Label42;
		private System.Windows.Forms.PictureBox _pctAutoInfor_3;
		private Telerik.WinControls.UI.RadLabel _lblAutoInfor_3;
		public Telerik.WinControls.UI.RadLabel Label23;
		public Telerik.WinControls.UI.RadLabel Label24;
		public Telerik.WinControls.UI.RadLabel Label25;
		public Telerik.WinControls.UI.RadTextBox lbDFAlta;
		public Telerik.WinControls.UI.RadTextBox lbDFIngreso;
		public Telerik.WinControls.UI.RadTextBox lbDMAlta;
		public Telerik.WinControls.UI.RadGroupBox Frame6;
		private Telerik.WinControls.UI.RadPageViewPage _tsPac_TabPage4;
		public Telerik.WinControls.UI.RadPageView tsPac;
		public Telerik.WinControls.UI.RadTextBox[] Cedula = new Telerik.WinControls.UI.RadTextBox[4];
		public Telerik.WinControls.UI.RadLabel[] lblAutoInfor = new Telerik.WinControls.UI.RadLabel[4];
		public System.Windows.Forms.PictureBox[] pctAutoInfor = new System.Windows.Forms.PictureBox[4];
		public Telerik.WinControls.UI.RadRadioButton[] rbBuscar = new Telerik.WinControls.UI.RadRadioButton[3];
		public Telerik.WinControls.UI.RadRadioButton[] rbSexo = new Telerik.WinControls.UI.RadRadioButton[2];

        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFI161F1));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            this.tsPac = new Telerik.WinControls.UI.RadPageView();
            this._tsPac_TabPage0 = new Telerik.WinControls.UI.RadPageViewPage();
            this.cbBuscar = new Telerik.WinControls.UI.RadButton();
            this.FrmDatosPersonales = new Telerik.WinControls.UI.RadGroupBox();
            this.frmSexo = new Telerik.WinControls.UI.RadGroupBox();
            this._rbSexo_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbSexo_1 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbNombre = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbApellido1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbApellido2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.sdcFechaNac = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label70 = new Telerik.WinControls.UI.RadLabel();
            this.Label69 = new Telerik.WinControls.UI.RadLabel();
            this.Label68 = new Telerik.WinControls.UI.RadLabel();
            this.Label67 = new Telerik.WinControls.UI.RadLabel();
            this.FrmBuscar = new Telerik.WinControls.UI.RadGroupBox();
            this._Cedula_0 = new Telerik.WinControls.UI.RadTextBox();
            this._Cedula_1 = new Telerik.WinControls.UI.RadTextBox();
            this._Cedula_2 = new Telerik.WinControls.UI.RadTextBox();
            this._Cedula_3 = new Telerik.WinControls.UI.RadTextBox();
            this.cbbArchivo = new Telerik.WinControls.UI.RadDropDownList();
            this._rbBuscar_2 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbBuscar_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbBuscar_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbNSS = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbNHistoria = new Telerik.WinControls.UI.RadTextBoxControl();
            this.MEBDNI = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label71 = new Telerik.WinControls.UI.RadLabel();
            this.sprFiliados = new UpgradeHelpers.Spread.FpSpread();
            this._tsPac_TabPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.lbUPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.lbUHistoria = new Telerik.WinControls.UI.RadTextBox();
            this.lbUEntidad = new Telerik.WinControls.UI.RadTextBox();
            this.lbUNAsegurado = new Telerik.WinControls.UI.RadTextBox();
            this.Label32 = new Telerik.WinControls.UI.RadLabel();
            this._pctAutoInfor_0 = new System.Windows.Forms.PictureBox();
            this._lblAutoInfor_0 = new Telerik.WinControls.UI.RadLabel();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label14 = new Telerik.WinControls.UI.RadLabel();
            this.Label13 = new Telerik.WinControls.UI.RadLabel();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.Label11 = new Telerik.WinControls.UI.RadLabel();
            this.Label10 = new Telerik.WinControls.UI.RadLabel();
            this.Label9 = new Telerik.WinControls.UI.RadLabel();
            this.lbUFAtencion = new Telerik.WinControls.UI.RadTextBox();
            this.lbUHAtencion = new Telerik.WinControls.UI.RadTextBox();
            this.lbUNOrden = new Telerik.WinControls.UI.RadTextBox();
            this.lbUEspec = new Telerik.WinControls.UI.RadTextBox();
            this.lbUBox = new Telerik.WinControls.UI.RadTextBox();
            this.lbUCausa = new Telerik.WinControls.UI.RadTextBox();
            this._tsPac_TabPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Label33 = new Telerik.WinControls.UI.RadLabel();
            this.Label31 = new Telerik.WinControls.UI.RadLabel();
            this.Label30 = new Telerik.WinControls.UI.RadLabel();
            this.lbHPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.lbHHistoria = new Telerik.WinControls.UI.RadTextBox();
            this.lbHEntidad = new Telerik.WinControls.UI.RadTextBox();
            this.lbHNAsegurado = new Telerik.WinControls.UI.RadTextBox();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this._pctAutoInfor_1 = new System.Windows.Forms.PictureBox();
            this._lblAutoInfor_1 = new Telerik.WinControls.UI.RadLabel();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.lblTelefono = new Telerik.WinControls.UI.RadTextBox();
            this.Label7 = new Telerik.WinControls.UI.RadLabel();
            this.lblmotausen = new Telerik.WinControls.UI.RadTextBox();
            this.lblfmotausen = new Telerik.WinControls.UI.RadTextBox();
            this.lblfausencia = new Telerik.WinControls.UI.RadLabel();
            this.lblausente = new Telerik.WinControls.UI.RadLabel();
            this.Label19 = new Telerik.WinControls.UI.RadLabel();
            this.Label18 = new Telerik.WinControls.UI.RadLabel();
            this.Label17 = new Telerik.WinControls.UI.RadLabel();
            this.Label16 = new Telerik.WinControls.UI.RadLabel();
            this.Label15 = new Telerik.WinControls.UI.RadLabel();
            this.lbHServicio = new Telerik.WinControls.UI.RadTextBox();
            this.lbHHabitacion = new Telerik.WinControls.UI.RadTextBox();
            this.lbHCama = new Telerik.WinControls.UI.RadTextBox();
            this.lbHUnidad = new Telerik.WinControls.UI.RadTextBox();
            this.lbHPlanta = new Telerik.WinControls.UI.RadTextBox();
            this._tsPac_TabPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Label41 = new Telerik.WinControls.UI.RadLabel();
            this.Label40 = new Telerik.WinControls.UI.RadLabel();
            this.Label39 = new Telerik.WinControls.UI.RadLabel();
            this.lbIPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.lbIHistoria = new Telerik.WinControls.UI.RadTextBox();
            this.lbIEntidad = new Telerik.WinControls.UI.RadTextBox();
            this.lbINAsegurado = new Telerik.WinControls.UI.RadTextBox();
            this.Label34 = new Telerik.WinControls.UI.RadLabel();
            this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label20 = new Telerik.WinControls.UI.RadLabel();
            this.Label21 = new Telerik.WinControls.UI.RadLabel();
            this.Label22 = new Telerik.WinControls.UI.RadLabel();
            this.lbIFPIngreso = new Telerik.WinControls.UI.RadTextBox();
            this.lbIFPIntervencion = new Telerik.WinControls.UI.RadTextBox();
            this.lbIServicio = new Telerik.WinControls.UI.RadTextBox();
            this._tsPac_TabPage4 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Label49 = new Telerik.WinControls.UI.RadLabel();
            this.Label48 = new Telerik.WinControls.UI.RadLabel();
            this.Label47 = new Telerik.WinControls.UI.RadLabel();
            this.lbDPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.lbDHistoria = new Telerik.WinControls.UI.RadTextBox();
            this.lbDEntidad = new Telerik.WinControls.UI.RadTextBox();
            this.lbDNAsegurado = new Telerik.WinControls.UI.RadTextBox();
            this.Label42 = new Telerik.WinControls.UI.RadLabel();
            this._pctAutoInfor_3 = new System.Windows.Forms.PictureBox();
            this._lblAutoInfor_3 = new Telerik.WinControls.UI.RadLabel();
            this.Frame6 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label23 = new Telerik.WinControls.UI.RadLabel();
            this.Label24 = new Telerik.WinControls.UI.RadLabel();
            this.Label25 = new Telerik.WinControls.UI.RadLabel();
            this.lbDFAlta = new Telerik.WinControls.UI.RadTextBox();
            this.lbDFIngreso = new Telerik.WinControls.UI.RadTextBox();
            this.lbDMAlta = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsPac)).BeginInit();
            this.tsPac.SuspendLayout();
            this._tsPac_TabPage0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmDatosPersonales)).BeginInit();
            this.FrmDatosPersonales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmSexo)).BeginInit();
            this.frmSexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaNac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmBuscar)).BeginInit();
            this.FrmBuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._Cedula_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Cedula_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Cedula_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Cedula_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbArchivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprFiliados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprFiliados.MasterTemplate)).BeginInit();
            this._tsPac_TabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUNAsegurado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._pctAutoInfor_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblAutoInfor_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUFAtencion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUHAtencion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUNOrden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUEspec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUCausa)).BeginInit();
            this._tsPac_TabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHNAsegurado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._pctAutoInfor_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblAutoInfor_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblmotausen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfmotausen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfausencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblausente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHHabitacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHCama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHUnidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHPlanta)).BeginInit();
            this._tsPac_TabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbINAsegurado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIFPIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIFPIntervencion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIServicio)).BeginInit();
            this._tsPac_TabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDNAsegurado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._pctAutoInfor_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblAutoInfor_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDFAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDFIngreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDMAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cbCerrar.Location = new System.Drawing.Point(536, 416);
            this.cbCerrar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(81, 32);
            this.cbCerrar.TabIndex = 20;
            this.cbCerrar.Text = "Ce&rrar";
            this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // tsPac
            // 
            this.tsPac.Controls.Add(this._tsPac_TabPage0);
            this.tsPac.Controls.Add(this._tsPac_TabPage1);
            this.tsPac.Controls.Add(this._tsPac_TabPage2);
            this.tsPac.Controls.Add(this._tsPac_TabPage3);
            this.tsPac.Controls.Add(this._tsPac_TabPage4);
            this.tsPac.AllowDrop = false;
            this.tsPac.AllowShowFocusCues = false;
            
            (tsPac.ViewElement as Telerik.WinControls.UI.RadPageViewStripElement).ShowItemCloseButton = false;
            (tsPac.ViewElement as Telerik.WinControls.UI.RadPageViewStripElement).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;

            this.tsPac.ItemSize = new System.Drawing.Size(123, 18);
            this.tsPac.Location = new System.Drawing.Point(4, 4);
            this.tsPac.Name = "tsPac";
            this.tsPac.SelectedPage = this._tsPac_TabPage0;
            this.tsPac.Size = new System.Drawing.Size(630, 405);
            this.tsPac.TabIndex = 21;
            // 
            // _tsPac_TabPage0
            // 
            this._tsPac_TabPage0.Controls.Add(this.cbBuscar);
            this._tsPac_TabPage0.Controls.Add(this.FrmDatosPersonales);
            this._tsPac_TabPage0.Controls.Add(this.FrmBuscar);
            this._tsPac_TabPage0.Controls.Add(this.sprFiliados);
            this._tsPac_TabPage0.ItemSize = new System.Drawing.SizeF(58F, 28F);
            this._tsPac_TabPage0.Location = new System.Drawing.Point(10, 37);
            this._tsPac_TabPage0.Name = "_tsPac_TabPage0";
            this._tsPac_TabPage0.Size = new System.Drawing.Size(603, 317);
            this._tsPac_TabPage0.Text = "Paciente";
            // 
            // cbBuscar
            // 
            this.cbBuscar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscar.Enabled = false;
            this.cbBuscar.Location = new System.Drawing.Point(520, 84);
            this.cbBuscar.Name = "cbBuscar";
            this.cbBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbBuscar.Size = new System.Drawing.Size(81, 32);
            this.cbBuscar.TabIndex = 7;
            this.cbBuscar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbBuscar.Text = "&Buscar";
            this.cbBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbBuscar.Click += new System.EventHandler(this.cbBuscar_Click);
            // 
            // FrmDatosPersonales
            // 
            this.FrmDatosPersonales.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmDatosPersonales.Controls.Add(this.frmSexo);
            this.FrmDatosPersonales.Controls.Add(this.tbNombre);
            this.FrmDatosPersonales.Controls.Add(this.tbApellido1);
            this.FrmDatosPersonales.Controls.Add(this.tbApellido2);
            this.FrmDatosPersonales.Controls.Add(this.sdcFechaNac);
            this.FrmDatosPersonales.Controls.Add(this.Label70);
            this.FrmDatosPersonales.Controls.Add(this.Label69);
            this.FrmDatosPersonales.Controls.Add(this.Label68);
            this.FrmDatosPersonales.Controls.Add(this.Label67);
            this.FrmDatosPersonales.HeaderText = "Datos Personales";
            this.FrmDatosPersonales.Location = new System.Drawing.Point(10, 4);
            this.FrmDatosPersonales.Name = "FrmDatosPersonales";
            this.FrmDatosPersonales.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmDatosPersonales.Size = new System.Drawing.Size(481, 113);
            this.FrmDatosPersonales.TabIndex = 92;
            this.FrmDatosPersonales.Text = "Datos Personales";
            // 
            // frmSexo
            // 
            this.frmSexo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmSexo.Controls.Add(this._rbSexo_0);
            this.frmSexo.Controls.Add(this._rbSexo_1);
            this.frmSexo.HeaderText = "Sexo";
            this.frmSexo.Location = new System.Drawing.Point(256, 40);
            this.frmSexo.Name = "frmSexo";
            this.frmSexo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmSexo.Size = new System.Drawing.Size(73, 65);
            this.frmSexo.TabIndex = 4;
            this.frmSexo.Text = "Sexo";
            // 
            // _rbSexo_0
            // 
            this._rbSexo_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbSexo_0.Location = new System.Drawing.Point(8, 40);
            this._rbSexo_0.Name = "_rbSexo_0";
            this._rbSexo_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbSexo_0.Size = new System.Drawing.Size(49, 18);
            this._rbSexo_0.TabIndex = 6;
            this._rbSexo_0.Text = "Mujer";
            this._rbSexo_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbSexo_CheckedChanged);
            // 
            // _rbSexo_1
            // 
            this._rbSexo_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbSexo_1.Location = new System.Drawing.Point(8, 16);
            this._rbSexo_1.Name = "_rbSexo_1";
            this._rbSexo_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbSexo_1.Size = new System.Drawing.Size(61, 18);
            this._rbSexo_1.TabIndex = 5;
            this._rbSexo_1.Text = "Hombre";
            this._rbSexo_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbSexo_CheckedChanged);
            // 
            // tbNombre
            // 
            this.tbNombre.AcceptsReturn = true;
            this.tbNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNombre.Location = new System.Drawing.Point(64, 53);
            this.tbNombre.MaxLength = 0;
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNombre.Size = new System.Drawing.Size(153, 20);
            this.tbNombre.TabIndex = 2;
            this.tbNombre.TextChanged += new System.EventHandler(this.tbNombre_TextChanged);
            this.tbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombre_KeyPress);
            // 
            // tbApellido1
            // 
            this.tbApellido1.AcceptsReturn = true;
            this.tbApellido1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbApellido1.Location = new System.Drawing.Point(64, 18);
            this.tbApellido1.MaxLength = 0;
            this.tbApellido1.Name = "tbApellido1";
            this.tbApellido1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbApellido1.Size = new System.Drawing.Size(153, 20);
            this.tbApellido1.TabIndex = 0;
            this.tbApellido1.TextChanged += new System.EventHandler(this.tbApellido1_TextChanged);
            this.tbApellido1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellido1_KeyPress);
            // 
            // tbApellido2
            // 
            this.tbApellido2.AcceptsReturn = true;
            this.tbApellido2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbApellido2.Location = new System.Drawing.Point(324, 18);
            this.tbApellido2.MaxLength = 0;
            this.tbApellido2.Name = "tbApellido2";
            this.tbApellido2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbApellido2.Size = new System.Drawing.Size(153, 20);
            this.tbApellido2.TabIndex = 1;
            this.tbApellido2.TextChanged += new System.EventHandler(this.tbApellido2_TextChanged);
            this.tbApellido2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellido2_KeyPress);
            // 
            // sdcFechaNac
            // 
            this.sdcFechaNac.CustomFormat = "dd/MM/yyyy";
            this.sdcFechaNac.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechaNac.Location = new System.Drawing.Point(104, 86);
            this.sdcFechaNac.Name = "sdcFechaNac";
            this.sdcFechaNac.Size = new System.Drawing.Size(123, 20);
            this.sdcFechaNac.TabIndex = 3;
            this.sdcFechaNac.TabStop = false;           
            this.sdcFechaNac.Leave += new System.EventHandler(this.sdcFechaNac_Leave);
            // 
            // Label70
            // 
            this.Label70.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label70.Location = new System.Drawing.Point(8, 88);
            this.Label70.Name = "Label70";
            this.Label70.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label70.Size = new System.Drawing.Size(98, 18);
            this.Label70.TabIndex = 96;
            this.Label70.Text = "Fecha Nacimiento:";
            // 
            // Label69
            // 
            this.Label69.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label69.Location = new System.Drawing.Point(10, 53);
            this.Label69.Name = "Label69";
            this.Label69.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label69.Size = new System.Drawing.Size(50, 18);
            this.Label69.TabIndex = 95;
            this.Label69.Text = "Nombre:";
            // 
            // Label68
            // 
            this.Label68.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label68.Location = new System.Drawing.Point(6, 22);
            this.Label68.Name = "Label68";
            this.Label68.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label68.Size = new System.Drawing.Size(59, 18);
            this.Label68.TabIndex = 94;
            this.Label68.Text = "Apellido 1:";
            this.Label68.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label67
            // 
            this.Label67.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label67.Location = new System.Drawing.Point(224, 20);
            this.Label67.Name = "Label67";
            this.Label67.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label67.Size = new System.Drawing.Size(59, 18);
            this.Label67.TabIndex = 93;
            this.Label67.Text = "Apellido 2:";
            this.Label67.TextAlignment = System.Drawing.ContentAlignment.TopRight;
            // 
            // FrmBuscar
            // 
            this.FrmBuscar.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmBuscar.Controls.Add(this._Cedula_0);
            this.FrmBuscar.Controls.Add(this._Cedula_1);
            this.FrmBuscar.Controls.Add(this._Cedula_2);
            this.FrmBuscar.Controls.Add(this._Cedula_3);
            this.FrmBuscar.Controls.Add(this.cbbArchivo);
            this.FrmBuscar.Controls.Add(this._rbBuscar_2);
            this.FrmBuscar.Controls.Add(this._rbBuscar_1);
            this.FrmBuscar.Controls.Add(this._rbBuscar_0);
            this.FrmBuscar.Controls.Add(this.tbNSS);
            this.FrmBuscar.Controls.Add(this.tbNHistoria);
            this.FrmBuscar.Controls.Add(this.MEBDNI);
            this.FrmBuscar.Controls.Add(this.Label6);
            this.FrmBuscar.Controls.Add(this.Label1);
            this.FrmBuscar.Controls.Add(this.Label71);
            this.FrmBuscar.HeaderText = "B�squeda por:";
            this.FrmBuscar.Location = new System.Drawing.Point(12, 124);
            this.FrmBuscar.Name = "FrmBuscar";
            this.FrmBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmBuscar.Size = new System.Drawing.Size(593, 97);
            this.FrmBuscar.TabIndex = 97;
            this.FrmBuscar.Text = "B�squeda por:";
            // 
            // _Cedula_0
            // 
            this._Cedula_0.AcceptsReturn = true;
            this._Cedula_0.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._Cedula_0.Location = new System.Drawing.Point(196, 48);
            this._Cedula_0.MaxLength = 2;
            this._Cedula_0.Name = "_Cedula_0";
            this._Cedula_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Cedula_0.Size = new System.Drawing.Size(21, 20);
            this._Cedula_0.TabIndex = 13;
            this._Cedula_0.TextChanged += new System.EventHandler(this.Cedula_TextChanged);
            this._Cedula_0.Enter += new System.EventHandler(this.Cedula_Enter);
            this._Cedula_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cedula_KeyPress);
            this._Cedula_0.Leave += new System.EventHandler(this.Cedula_Leave);
            // 
            // _Cedula_1
            // 
            this._Cedula_1.AcceptsReturn = true;
            this._Cedula_1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._Cedula_1.Location = new System.Drawing.Point(216, 48);
            this._Cedula_1.MaxLength = 2;
            this._Cedula_1.Name = "_Cedula_1";
            this._Cedula_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Cedula_1.Size = new System.Drawing.Size(29, 20);
            this._Cedula_1.TabIndex = 14;
            this._Cedula_1.TextChanged += new System.EventHandler(this.Cedula_TextChanged);
            this._Cedula_1.Enter += new System.EventHandler(this.Cedula_Enter);
            this._Cedula_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cedula_KeyPress);
            this._Cedula_1.Leave += new System.EventHandler(this.Cedula_Leave);
            // 
            // _Cedula_2
            // 
            this._Cedula_2.AcceptsReturn = true;
            this._Cedula_2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._Cedula_2.Location = new System.Drawing.Point(258, 48);
            this._Cedula_2.MaxLength = 4;
            this._Cedula_2.Name = "_Cedula_2";
            this._Cedula_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Cedula_2.Size = new System.Drawing.Size(31, 20);
            this._Cedula_2.TabIndex = 15;
            this._Cedula_2.TextChanged += new System.EventHandler(this.Cedula_TextChanged);
            this._Cedula_2.Enter += new System.EventHandler(this.Cedula_Enter);
            this._Cedula_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cedula_KeyPress);
            this._Cedula_2.Leave += new System.EventHandler(this.Cedula_Leave);
            // 
            // _Cedula_3
            // 
            this._Cedula_3.AcceptsReturn = true;
            this._Cedula_3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._Cedula_3.Location = new System.Drawing.Point(304, 48);
            this._Cedula_3.MaxLength = 5;
            this._Cedula_3.Name = "_Cedula_3";
            this._Cedula_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Cedula_3.Size = new System.Drawing.Size(37, 20);
            this._Cedula_3.TabIndex = 16;
            this._Cedula_3.TextChanged += new System.EventHandler(this.Cedula_TextChanged);
            this._Cedula_3.Enter += new System.EventHandler(this.Cedula_Enter);
            this._Cedula_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cedula_KeyPress);
            this._Cedula_3.Leave += new System.EventHandler(this.Cedula_Leave);
            // 
            // cbbArchivo
            // 
            this.cbbArchivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbArchivo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbArchivo.Location = new System.Drawing.Point(344, 24);
            this.cbbArchivo.Enabled = false;
            this.cbbArchivo.Name = "cbbArchivo";
            this.cbbArchivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbArchivo.Size = new System.Drawing.Size(233, 20);
            this.cbbArchivo.TabIndex = 10;
            // 
            // _rbBuscar_2
            // 
            this._rbBuscar_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_2.Location = new System.Drawing.Point(8, 72);
            this._rbBuscar_2.Name = "_rbBuscar_2";
            this._rbBuscar_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_2.Size = new System.Drawing.Size(217, 18);
            this._rbBuscar_2.TabIndex = 17;
            this._rbBuscar_2.Text = "N� Seguridad Social/N� P�liza sociedad";
            this._rbBuscar_2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbBuscar_CheckedChanged);
            //this._rbBuscar_2.DoubleClick += new System.EventHandler(this.rbBuscar_DoubleClick);
            
            // 
            // _rbBuscar_1
            // 
            this._rbBuscar_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_1.Location = new System.Drawing.Point(8, 48);
            this._rbBuscar_1.Name = "_rbBuscar_1";
            this._rbBuscar_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_1.Size = new System.Drawing.Size(47, 18);
            this._rbBuscar_1.TabIndex = 11;
            this._rbBuscar_1.Text = "D.N.I.";
            this._rbBuscar_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbBuscar_CheckedChanged);
            //this._rbBuscar_1.DoubleClick += new System.EventHandler(this.rbBuscar_DoubleClick);
            
            // 
            // _rbBuscar_0
            // 
            this._rbBuscar_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_0.Location = new System.Drawing.Point(8, 24);
            this._rbBuscar_0.Name = "_rbBuscar_0";
            this._rbBuscar_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_0.Size = new System.Drawing.Size(123, 18);
            this._rbBuscar_0.TabIndex = 8;
            this._rbBuscar_0.Text = "N� de historia cl�nica";
            this._rbBuscar_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbBuscar_CheckedChanged);
            //this._rbBuscar_0.DoubleClick += new System.EventHandler(this.rbBuscar_DoubleClick);
            
            // 
            // tbNSS
            // 
            this.tbNSS.AcceptsReturn = true;
            this.tbNSS.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNSS.Location = new System.Drawing.Point(234, 72);
            this.tbNSS.MaxLength = 0;
            this.tbNSS.Name = "tbNSS";
            this.tbNSS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNSS.Size = new System.Drawing.Size(153, 20);
            this.tbNSS.TabIndex = 18;
            this.tbNSS.TextChanged += new System.EventHandler(this.tbNSS_TextChanged);            
            // 
            // tbNHistoria
            // 
            this.tbNHistoria.AcceptsReturn = true;
            this.tbNHistoria.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNHistoria.Location = new System.Drawing.Point(200, 24);
            this.tbNHistoria.MaxLength = 10;
            this.tbNHistoria.Name = "tbNHistoria";
            this.tbNHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNHistoria.Size = new System.Drawing.Size(93, 20);
            this.tbNHistoria.TabIndex = 9;
            this.tbNHistoria.TextChanged += new System.EventHandler(this.tbNHistoria_TextChanged);
            // 
            // MEBDNI
            // 
            this.MEBDNI.AllowPromptAsInput = false;
            this.MEBDNI.Enabled = false;
            this.MEBDNI.Location = new System.Drawing.Point(200, 48);
            this.MEBDNI.Name = "MEBDNI";
            this.MEBDNI.PromptChar = ' ';
            this.MEBDNI.Size = new System.Drawing.Size(93, 20);
            this.MEBDNI.TabIndex = 12;            
            this.MEBDNI.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;            
            this.MEBDNI.TextChanged += new System.EventHandler(this.MEBDNI_TextChanged);
            this.MEBDNI.Enter += new System.EventHandler(this.MEBDNI_Enter);
            this.MEBDNI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MEBDNI_KeyPress);
            this.MEBDNI.Leave += new System.EventHandler(this.MEBDNI_Leave);
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Location = new System.Drawing.Point(246, 44);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(17, 18);
            this.Label6.TabIndex = 104;
            this.Label6.Text = " _ ";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(290, 44);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(17, 18);
            this.Label1.TabIndex = 103;
            this.Label1.Text = " _ ";
            // 
            // Label71
            // 
            this.Label71.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label71.Location = new System.Drawing.Point(296, 24);
            this.Label71.Name = "Label71";
            this.Label71.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label71.Size = new System.Drawing.Size(46, 18);
            this.Label71.TabIndex = 98;
            this.Label71.Text = "Archivo:";
            // 
            // sprFiliados
            // 
            this.sprFiliados.Location = new System.Drawing.Point(8, 228);                        
            this.sprFiliados.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.sprFiliados.Name = "sprFiliados";
            this.sprFiliados.Size = new System.Drawing.Size(601, 115);
            this.sprFiliados.TabIndex = 19;
            this.sprFiliados.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprFiliados_CellClick);
            this.sprFiliados.AllowAutoSizeColumns = false;
            this.sprFiliados.AutoSizeRows = true;
            this.sprFiliados.AutoScroll = true;
            this.sprFiliados.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;

            gridViewTextBoxColumn1.HeaderText = "Hist. cl�nica";
            gridViewTextBoxColumn1.Name = "Col_Hist_clinica";
            gridViewTextBoxColumn1.Width = 120;
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "Col_Nombre";
            gridViewTextBoxColumn2.Width = 200;
            gridViewTextBoxColumn3.HeaderText = "F. Nacimiento";
            gridViewTextBoxColumn3.Name = "Col_F_Nacimiento";
            gridViewTextBoxColumn3.Width = 110;
            gridViewTextBoxColumn4.HeaderText = "Sexo";
            gridViewTextBoxColumn4.Name = "Col_Sexo";
            gridViewTextBoxColumn4.Width = 80;            
            gridViewTextBoxColumn5.HeaderText = "Domicilio";            
            gridViewTextBoxColumn5.Name = "Col_Domicilio";
            gridViewTextBoxColumn5.Width = 170;
            gridViewTextBoxColumn6.HeaderText = "C�d. Postal";
            gridViewTextBoxColumn6.Name = "Col_CodPostal";
            gridViewTextBoxColumn6.Width = 120;
            gridViewTextBoxColumn7.HeaderText = "Poblaci�n";
            gridViewTextBoxColumn7.Name = "Col_Poblacion";
            gridViewTextBoxColumn7.Width = 120;
            gridViewTextBoxColumn8.HeaderText = "registro";
            gridViewTextBoxColumn8.Name = "Col_registro";
            gridViewTextBoxColumn8.Width = 120;
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn9.HeaderText = "gidenpac";
            gridViewTextBoxColumn9.Name = "Col_gidenpac";
            gridViewTextBoxColumn9.Width = 120;
            gridViewTextBoxColumn9.IsVisible = false;

            this.sprFiliados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            this.gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            // 
            // _tsPac_TabPage1
            // 
            this._tsPac_TabPage1.Controls.Add(this.Label4);
            this._tsPac_TabPage1.Controls.Add(this.Label3);
            this._tsPac_TabPage1.Controls.Add(this.Label2);
            this._tsPac_TabPage1.Controls.Add(this.lbUPaciente);
            this._tsPac_TabPage1.Controls.Add(this.lbUHistoria);
            this._tsPac_TabPage1.Controls.Add(this.lbUEntidad);
            this._tsPac_TabPage1.Controls.Add(this.lbUNAsegurado);
            this._tsPac_TabPage1.Controls.Add(this.Label32);
            this._tsPac_TabPage1.Controls.Add(this._pctAutoInfor_0);
            this._tsPac_TabPage1.Controls.Add(this._lblAutoInfor_0);
            this._tsPac_TabPage1.Controls.Add(this.Frame3);
            this._tsPac_TabPage1.ItemSize = new System.Drawing.SizeF(65F, 28F);
            this._tsPac_TabPage1.Location = new System.Drawing.Point(10, 37);
            this._tsPac_TabPage1.Name = "_tsPac_TabPage1";
            this._tsPac_TabPage1.Size = new System.Drawing.Size(603, 317);
            this._tsPac_TabPage1.Text = "Urgencias";
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(8, 20);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(51, 18);
            this.Label4.TabIndex = 84;
            this.Label4.Text = "Paciente:";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(432, 20);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(64, 18);
            this.Label3.TabIndex = 85;
            this.Label3.Text = "Hist. cl�nica:";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(8, 52);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(82, 18);
            this.Label2.TabIndex = 86;
            this.Label2.Text = "E. financiadora:";
            // 
            // lbUPaciente
            // 
            this.lbUPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUPaciente.Location = new System.Drawing.Point(98, 20);
            this.lbUPaciente.Name = "lbUPaciente";
            this.lbUPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUPaciente.Size = new System.Drawing.Size(331, 19);
            this.lbUPaciente.TabIndex = 87;
            this.lbUPaciente.Enabled = false;
            // 
            // lbUHistoria
            // 
            this.lbUHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUHistoria.Location = new System.Drawing.Point(496, 20);
            this.lbUHistoria.Name = "lbUHistoria";
            this.lbUHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUHistoria.Size = new System.Drawing.Size(73, 19);
            this.lbUHistoria.TabIndex = 88;
            this.lbUHistoria.Enabled = false;
            // 
            // lbUEntidad
            // 
            this.lbUEntidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUEntidad.Location = new System.Drawing.Point(98, 52);
            this.lbUEntidad.Name = "lbUEntidad";
            this.lbUEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUEntidad.Size = new System.Drawing.Size(217, 19);
            this.lbUEntidad.TabIndex = 89;
            this.lbUEntidad.Enabled = false;
            // 
            // lbUNAsegurado
            // 
            this.lbUNAsegurado.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUNAsegurado.Location = new System.Drawing.Point(416, 55);
            this.lbUNAsegurado.Name = "lbUNAsegurado";
            this.lbUNAsegurado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUNAsegurado.Size = new System.Drawing.Size(153, 19);
            this.lbUNAsegurado.TabIndex = 90;
            this.lbUNAsegurado.Enabled = false;
            // 
            // Label32
            // 
            this.Label32.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label32.Location = new System.Drawing.Point(336, 55);
            this.Label32.Name = "Label32";
            this.Label32.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label32.Size = new System.Drawing.Size(78, 18);
            this.Label32.TabIndex = 91;
            this.Label32.Text = "N� asegurado:";
            // 
            // _pctAutoInfor_0
            // 
            this._pctAutoInfor_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._pctAutoInfor_0.Image = ((System.Drawing.Image)(resources.GetObject("_pctAutoInfor_0.Image")));
            this._pctAutoInfor_0.Location = new System.Drawing.Point(193, 277);
            this._pctAutoInfor_0.Name = "_pctAutoInfor_0";
            this._pctAutoInfor_0.Size = new System.Drawing.Size(29, 28);
            this._pctAutoInfor_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._pctAutoInfor_0.TabIndex = 92;
            this._pctAutoInfor_0.TabStop = false;
            // 
            // _lblAutoInfor_0
            // 
            this._lblAutoInfor_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblAutoInfor_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblAutoInfor_0.Location = new System.Drawing.Point(239, 281);
            this._lblAutoInfor_0.Name = "_lblAutoInfor_0";
            this._lblAutoInfor_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblAutoInfor_0.Size = new System.Drawing.Size(160, 17);
            this._lblAutoInfor_0.TabIndex = 107;
            this._lblAutoInfor_0.Text = "No se autoriza informaci�n";
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.Label14);
            this.Frame3.Controls.Add(this.Label13);
            this.Frame3.Controls.Add(this.Label12);
            this.Frame3.Controls.Add(this.Label11);
            this.Frame3.Controls.Add(this.Label10);
            this.Frame3.Controls.Add(this.Label9);
            this.Frame3.Controls.Add(this.lbUFAtencion);
            this.Frame3.Controls.Add(this.lbUHAtencion);
            this.Frame3.Controls.Add(this.lbUNOrden);
            this.Frame3.Controls.Add(this.lbUEspec);
            this.Frame3.Controls.Add(this.lbUBox);
            this.Frame3.Controls.Add(this.lbUCausa);
            this.Frame3.HeaderText = "Urgencias";
            this.Frame3.Location = new System.Drawing.Point(8, 84);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(601, 153);
            this.Frame3.TabIndex = 71;
            this.Frame3.Text = "Urgencias";
            // 
            // Label14
            // 
            this.Label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label14.Location = new System.Drawing.Point(8, 120);
            this.Label14.Name = "Label14";
            this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label14.Size = new System.Drawing.Size(97, 18);
            this.Label14.TabIndex = 83;
            this.Label14.Text = "Causa del ingreso:";
            // 
            // Label13
            // 
            this.Label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label13.Location = new System.Drawing.Point(8, 88);
            this.Label13.Name = "Label13";
            this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label13.Size = new System.Drawing.Size(144, 18);
            this.Label13.TabIndex = 82;
            this.Label13.Text = "Box (si pas� a observaci�n):";
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Location = new System.Drawing.Point(216, 56);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(132, 18);
            this.Label12.TabIndex = 81;
            this.Label12.Text = "Especialidad de atenci�n:";
            // 
            // Label11
            // 
            this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label11.Location = new System.Drawing.Point(8, 56);
            this.Label11.Name = "Label11";
            this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label11.Size = new System.Drawing.Size(70, 18);
            this.Label11.TabIndex = 80;
            this.Label11.Text = "N� de orden:";
            // 
            // Label10
            // 
            this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label10.Location = new System.Drawing.Point(216, 24);
            this.Label10.Name = "Label10";
            this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label10.Size = new System.Drawing.Size(95, 18);
            this.Label10.TabIndex = 79;
            this.Label10.Text = "Hora de atenci�n:";
            // 
            // Label9
            // 
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Location = new System.Drawing.Point(8, 24);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(99, 18);
            this.Label9.TabIndex = 78;
            this.Label9.Text = "Fecha de atenci�n:";
            // 
            // lbUFAtencion
            // 
            this.lbUFAtencion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUFAtencion.Location = new System.Drawing.Point(114, 24);
            this.lbUFAtencion.Name = "lbUFAtencion";
            this.lbUFAtencion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUFAtencion.Size = new System.Drawing.Size(65, 19);
            this.lbUFAtencion.TabIndex = 77;
            this.lbUFAtencion.Enabled = false;
            // 
            // lbUHAtencion
            // 
            this.lbUHAtencion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUHAtencion.Location = new System.Drawing.Point(354, 24);
            this.lbUHAtencion.Name = "lbUHAtencion";
            this.lbUHAtencion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUHAtencion.Size = new System.Drawing.Size(41, 19);
            this.lbUHAtencion.TabIndex = 76;
            this.lbUHAtencion.Enabled = false;
            // 
            // lbUNOrden
            // 
            this.lbUNOrden.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUNOrden.Location = new System.Drawing.Point(114, 56);
            this.lbUNOrden.Name = "lbUNOrden";
            this.lbUNOrden.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUNOrden.Size = new System.Drawing.Size(73, 19);
            this.lbUNOrden.TabIndex = 75;
            this.lbUNOrden.Enabled = false;
            // 
            // lbUEspec
            // 
            this.lbUEspec.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUEspec.Location = new System.Drawing.Point(354, 56);
            this.lbUEspec.Name = "lbUEspec";
            this.lbUEspec.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUEspec.Size = new System.Drawing.Size(240, 19);
            this.lbUEspec.TabIndex = 74;
            this.lbUEspec.Enabled = false;
            // 
            // lbUBox
            // 
            this.lbUBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUBox.Location = new System.Drawing.Point(152, 88);
            this.lbUBox.Name = "lbUBox";
            this.lbUBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUBox.Size = new System.Drawing.Size(73, 19);
            this.lbUBox.TabIndex = 73;
            this.lbUBox.Enabled = false;
            // 
            // lbUCausa
            // 
            this.lbUCausa.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUCausa.Location = new System.Drawing.Point(152, 120);
            this.lbUCausa.Name = "lbUCausa";
            this.lbUCausa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUCausa.Size = new System.Drawing.Size(329, 19);
            this.lbUCausa.TabIndex = 72;
            this.lbUCausa.Enabled = false;
            // 
            // _tsPac_TabPage2
            // 
            this._tsPac_TabPage2.Controls.Add(this.Label33);
            this._tsPac_TabPage2.Controls.Add(this.Label31);
            this._tsPac_TabPage2.Controls.Add(this.Label30);
            this._tsPac_TabPage2.Controls.Add(this.lbHPaciente);
            this._tsPac_TabPage2.Controls.Add(this.lbHHistoria);
            this._tsPac_TabPage2.Controls.Add(this.lbHEntidad);
            this._tsPac_TabPage2.Controls.Add(this.lbHNAsegurado);
            this._tsPac_TabPage2.Controls.Add(this.Label5);
            this._tsPac_TabPage2.Controls.Add(this._pctAutoInfor_1);
            this._tsPac_TabPage2.Controls.Add(this._lblAutoInfor_1);
            this._tsPac_TabPage2.Controls.Add(this.Frame4);
            this._tsPac_TabPage2.ItemSize = new System.Drawing.SizeF(100F, 28F);
            this._tsPac_TabPage2.Location = new System.Drawing.Point(10, 37);
            this._tsPac_TabPage2.Name = "_tsPac_TabPage2";
            this._tsPac_TabPage2.Size = new System.Drawing.Size(603, 317);
            this._tsPac_TabPage2.Text = "Ingresado planta";
            // 
            // Label33
            // 
            this.Label33.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label33.Location = new System.Drawing.Point(8, 20);
            this.Label33.Name = "Label33";
            this.Label33.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label33.Size = new System.Drawing.Size(51, 18);
            this.Label33.TabIndex = 63;
            this.Label33.Text = "Paciente:";
            // 
            // Label31
            // 
            this.Label31.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label31.Location = new System.Drawing.Point(432, 20);
            this.Label31.Name = "Label31";
            this.Label31.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label31.Size = new System.Drawing.Size(64, 18);
            this.Label31.TabIndex = 64;
            this.Label31.Text = "Hist. cl�nica:";
            // 
            // Label30
            // 
            this.Label30.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label30.Location = new System.Drawing.Point(8, 52);
            this.Label30.Name = "Label30";
            this.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label30.Size = new System.Drawing.Size(82, 18);
            this.Label30.TabIndex = 65;
            this.Label30.Text = "E. financiadora:";
            // 
            // lbHPaciente
            // 
            this.lbHPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHPaciente.Location = new System.Drawing.Point(98, 20);
            this.lbHPaciente.Name = "lbHPaciente";
            this.lbHPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHPaciente.Size = new System.Drawing.Size(331, 19);
            this.lbHPaciente.TabIndex = 66;
            this.lbHPaciente.Enabled = false;
            // 
            // lbHHistoria
            // 
            this.lbHHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHHistoria.Location = new System.Drawing.Point(496, 20);
            this.lbHHistoria.Name = "lbHHistoria";
            this.lbHHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHHistoria.Size = new System.Drawing.Size(73, 19);
            this.lbHHistoria.TabIndex = 67;
            this.lbHHistoria.Enabled = false;
            // 
            // lbHEntidad
            // 
            this.lbHEntidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHEntidad.Location = new System.Drawing.Point(98, 52);
            this.lbHEntidad.Name = "lbHEntidad";
            this.lbHEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHEntidad.Size = new System.Drawing.Size(217, 19);
            this.lbHEntidad.TabIndex = 68;
            this.lbHEntidad.Enabled = false;
            // 
            // lbHNAsegurado
            // 
            this.lbHNAsegurado.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHNAsegurado.Location = new System.Drawing.Point(416, 55);
            this.lbHNAsegurado.Name = "lbHNAsegurado";
            this.lbHNAsegurado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHNAsegurado.Size = new System.Drawing.Size(153, 19);
            this.lbHNAsegurado.TabIndex = 69;
            this.lbHNAsegurado.Enabled = false;
            // 
            // Label5
            // 
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Location = new System.Drawing.Point(336, 55);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(78, 18);
            this.Label5.TabIndex = 70;
            this.Label5.Text = "N� asegurado:";
            // 
            // _pctAutoInfor_1
            // 
            this._pctAutoInfor_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._pctAutoInfor_1.Image = ((System.Drawing.Image)(resources.GetObject("_pctAutoInfor_1.Image")));
            this._pctAutoInfor_1.Location = new System.Drawing.Point(193, 277);
            this._pctAutoInfor_1.Name = "_pctAutoInfor_1";
            this._pctAutoInfor_1.Size = new System.Drawing.Size(29, 28);
            this._pctAutoInfor_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._pctAutoInfor_1.TabIndex = 71;
            this._pctAutoInfor_1.TabStop = false;
            // 
            // _lblAutoInfor_1
            // 
            this._lblAutoInfor_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblAutoInfor_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblAutoInfor_1.Location = new System.Drawing.Point(239, 281);
            this._lblAutoInfor_1.Name = "_lblAutoInfor_1";
            this._lblAutoInfor_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblAutoInfor_1.Size = new System.Drawing.Size(160, 17);
            this._lblAutoInfor_1.TabIndex = 108;
            this._lblAutoInfor_1.Text = "No se autoriza informaci�n";
            // 
            // Frame4
            // 
            this.Frame4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame4.Controls.Add(this.lblTelefono);
            this.Frame4.Controls.Add(this.Label7);
            this.Frame4.Controls.Add(this.lblmotausen);
            this.Frame4.Controls.Add(this.lblfmotausen);
            this.Frame4.Controls.Add(this.lblfausencia);
            this.Frame4.Controls.Add(this.lblausente);
            this.Frame4.Controls.Add(this.Label19);
            this.Frame4.Controls.Add(this.Label18);
            this.Frame4.Controls.Add(this.Label17);
            this.Frame4.Controls.Add(this.Label16);
            this.Frame4.Controls.Add(this.Label15);
            this.Frame4.Controls.Add(this.lbHServicio);
            this.Frame4.Controls.Add(this.lbHHabitacion);
            this.Frame4.Controls.Add(this.lbHCama);
            this.Frame4.Controls.Add(this.lbHUnidad);
            this.Frame4.Controls.Add(this.lbHPlanta);
            this.Frame4.HeaderText = "Hospitalizaci�n en planta";            
            this.Frame4.Location = new System.Drawing.Point(8, 85);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(601, 152);
            this.Frame4.TabIndex = 52;
            this.Frame4.Text = "Hospitalizaci�n en planta";
            // 
            // lblTelefono
            // 
            this.lblTelefono.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTelefono.Location = new System.Drawing.Point(535, 114);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTelefono.Size = new System.Drawing.Size(33, 19);
            this.lblTelefono.TabIndex = 106;
            this.lblTelefono.Enabled = false;
            // 
            // Label7
            // 
            this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label7.Location = new System.Drawing.Point(418, 116);
            this.Label7.Name = "Label7";
            this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label7.Size = new System.Drawing.Size(109, 18);
            this.Label7.TabIndex = 105;
            this.Label7.Text = "Tel�fono Habitaci�n:";
            // 
            // lblmotausen
            // 
            this.lblmotausen.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblmotausen.Location = new System.Drawing.Point(180, 86);
            this.lblmotausen.Name = "lblmotausen";
            this.lblmotausen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblmotausen.Size = new System.Drawing.Size(217, 19);
            this.lblmotausen.TabIndex = 102;
            this.lblmotausen.Enabled = false;
            // 
            // lblfmotausen
            // 
            this.lblfmotausen.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblfmotausen.Location = new System.Drawing.Point(180, 118);
            this.lblfmotausen.Name = "lblfmotausen";
            this.lblfmotausen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblfmotausen.Size = new System.Drawing.Size(217, 19);
            this.lblfmotausen.TabIndex = 101;
            this.lblfmotausen.Enabled = false;
            // 
            // lblfausencia
            // 
            this.lblfausencia.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblfausencia.Location = new System.Drawing.Point(10, 117);
            this.lblfausencia.Name = "lblfausencia";
            this.lblfausencia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblfausencia.Size = new System.Drawing.Size(111, 18);
            this.lblfausencia.TabIndex = 100;
            this.lblfausencia.Text = "Fecha de la ausencia:";
            // 
            // lblausente
            // 
            this.lblausente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblausente.Location = new System.Drawing.Point(10, 86);
            this.lblausente.Name = "lblausente";
            this.lblausente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblausente.Size = new System.Drawing.Size(113, 18);
            this.lblausente.TabIndex = 99;
            this.lblausente.Text = "Paciente ausente por:";
            // 
            // Label19
            // 
            this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label19.Location = new System.Drawing.Point(418, 86);
            this.Label19.Name = "Label19";
            this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label19.Size = new System.Drawing.Size(37, 18);
            this.Label19.TabIndex = 62;
            this.Label19.Text = "Cama:";
            // 
            // Label18
            // 
            this.Label18.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label18.Location = new System.Drawing.Point(418, 55);
            this.Label18.Name = "Label18";
            this.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label18.Size = new System.Drawing.Size(62, 18);
            this.Label18.TabIndex = 61;
            this.Label18.Text = "Habitaci�n:";
            // 
            // Label17
            // 
            this.Label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label17.Location = new System.Drawing.Point(418, 23);
            this.Label17.Name = "Label17";
            this.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label17.Size = new System.Drawing.Size(39, 18);
            this.Label17.TabIndex = 60;
            this.Label17.Text = "Planta:";
            // 
            // Label16
            // 
            this.Label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label16.Location = new System.Drawing.Point(10, 23);
            this.Label16.Name = "Label16";
            this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label16.Size = new System.Drawing.Size(166, 18);
            this.Label16.TabIndex = 59;
            this.Label16.Text = "Unidad de enfermer�a o control:";
            // 
            // Label15
            // 
            this.Label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label15.Location = new System.Drawing.Point(10, 55);
            this.Label15.Name = "Label15";
            this.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label15.Size = new System.Drawing.Size(111, 18);
            this.Label15.TabIndex = 58;
            this.Label15.Text = "Servicio responsable:";
            // 
            // lbHServicio
            // 
            this.lbHServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHServicio.Location = new System.Drawing.Point(180, 55);
            this.lbHServicio.Name = "lbHServicio";
            this.lbHServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHServicio.Size = new System.Drawing.Size(217, 19);
            this.lbHServicio.TabIndex = 57;
            this.lbHServicio.Enabled = false;
            // 
            // lbHHabitacion
            // 
            this.lbHHabitacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHHabitacion.Location = new System.Drawing.Point(535, 55);
            this.lbHHabitacion.Name = "lbHHabitacion";
            this.lbHHabitacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHHabitacion.Size = new System.Drawing.Size(33, 19);
            this.lbHHabitacion.TabIndex = 56;
            this.lbHHabitacion.Enabled = false;
            // 
            // lbHCama
            // 
            this.lbHCama.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHCama.Location = new System.Drawing.Point(535, 86);
            this.lbHCama.Name = "lbHCama";
            this.lbHCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHCama.Size = new System.Drawing.Size(33, 19);
            this.lbHCama.TabIndex = 55;
            this.lbHCama.Enabled = false;
            // 
            // lbHUnidad
            // 
            this.lbHUnidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHUnidad.Location = new System.Drawing.Point(180, 23);
            this.lbHUnidad.Name = "lbHUnidad";
            this.lbHUnidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHUnidad.Size = new System.Drawing.Size(217, 19);
            this.lbHUnidad.TabIndex = 54;
            this.lbHUnidad.Enabled = false;
            // 
            // lbHPlanta
            // 
            this.lbHPlanta.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbHPlanta.Location = new System.Drawing.Point(535, 23);
            this.lbHPlanta.Name = "lbHPlanta";
            this.lbHPlanta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbHPlanta.Size = new System.Drawing.Size(33, 19);
            this.lbHPlanta.TabIndex = 53;
            this.lbHPlanta.Enabled = false;
            // 
            // _tsPac_TabPage3
            // 
            this._tsPac_TabPage3.Controls.Add(this.Label41);
            this._tsPac_TabPage3.Controls.Add(this.Label40);
            this._tsPac_TabPage3.Controls.Add(this.Label39);
            this._tsPac_TabPage3.Controls.Add(this.lbIPaciente);
            this._tsPac_TabPage3.Controls.Add(this.lbIHistoria);
            this._tsPac_TabPage3.Controls.Add(this.lbIEntidad);
            this._tsPac_TabPage3.Controls.Add(this.lbINAsegurado);
            this._tsPac_TabPage3.Controls.Add(this.Label34);
            this._tsPac_TabPage3.Controls.Add(this.Frame5);
            this._tsPac_TabPage3.ItemSize = new System.Drawing.SizeF(119F, 28F);
            this._tsPac_TabPage3.Location = new System.Drawing.Point(10, 37);
            this._tsPac_TabPage3.Name = "_tsPac_TabPage3";
            this._tsPac_TabPage3.Size = new System.Drawing.Size(603, 317);
            this._tsPac_TabPage3.Text = "Ingreso programado";
            // 
            // Label41
            // 
            this.Label41.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label41.Location = new System.Drawing.Point(8, 20);
            this.Label41.Name = "Label41";
            this.Label41.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label41.Size = new System.Drawing.Size(51, 18);
            this.Label41.TabIndex = 44;
            this.Label41.Text = "Paciente:";
            // 
            // Label40
            // 
            this.Label40.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label40.Location = new System.Drawing.Point(432, 20);
            this.Label40.Name = "Label40";
            this.Label40.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label40.Size = new System.Drawing.Size(64, 18);
            this.Label40.TabIndex = 45;
            this.Label40.Text = "Hist. cl�nica:";
            // 
            // Label39
            // 
            this.Label39.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label39.Location = new System.Drawing.Point(8, 52);
            this.Label39.Name = "Label39";
            this.Label39.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label39.Size = new System.Drawing.Size(82, 18);
            this.Label39.TabIndex = 46;
            this.Label39.Text = "E. financiadora:";
            // 
            // lbIPaciente
            // 
            this.lbIPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbIPaciente.Location = new System.Drawing.Point(98, 20);
            this.lbIPaciente.Name = "lbIPaciente";
            this.lbIPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbIPaciente.Size = new System.Drawing.Size(331, 19);
            this.lbIPaciente.TabIndex = 47;
            this.lbIPaciente.Enabled = false;
            // 
            // lbIHistoria
            // 
            this.lbIHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbIHistoria.Location = new System.Drawing.Point(496, 20);
            this.lbIHistoria.Name = "lbIHistoria";
            this.lbIHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbIHistoria.Size = new System.Drawing.Size(73, 19);
            this.lbIHistoria.TabIndex = 48;
            this.lbIHistoria.Enabled = false;
            // 
            // lbIEntidad
            // 
            this.lbIEntidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbIEntidad.Location = new System.Drawing.Point(98, 52);
            this.lbIEntidad.Name = "lbIEntidad";
            this.lbIEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbIEntidad.Size = new System.Drawing.Size(217, 19);
            this.lbIEntidad.TabIndex = 49;
            this.lbIEntidad.Enabled = false;
            // 
            // lbINAsegurado
            // 
            this.lbINAsegurado.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbINAsegurado.Location = new System.Drawing.Point(416, 55);
            this.lbINAsegurado.Name = "lbINAsegurado";
            this.lbINAsegurado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbINAsegurado.Size = new System.Drawing.Size(153, 19);
            this.lbINAsegurado.TabIndex = 50;
            this.lbINAsegurado.Enabled = false;
            // 
            // Label34
            // 
            this.Label34.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label34.Location = new System.Drawing.Point(336, 55);
            this.Label34.Name = "Label34";
            this.Label34.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label34.Size = new System.Drawing.Size(78, 18);
            this.Label34.TabIndex = 51;
            this.Label34.Text = "N� asegurado:";
            // 
            // Frame5
            // 
            this.Frame5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame5.Controls.Add(this.Label20);
            this.Frame5.Controls.Add(this.Label21);
            this.Frame5.Controls.Add(this.Label22);
            this.Frame5.Controls.Add(this.lbIFPIngreso);
            this.Frame5.Controls.Add(this.lbIFPIntervencion);
            this.Frame5.Controls.Add(this.lbIServicio);
            this.Frame5.HeaderText = "Ingreso programado";
            this.Frame5.Location = new System.Drawing.Point(8, 84);
            this.Frame5.Name = "Frame5";
            this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame5.Size = new System.Drawing.Size(585, 113);
            this.Frame5.TabIndex = 37;
            this.Frame5.Text = "Ingreso programado";
            // 
            // Label20
            // 
            this.Label20.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label20.Location = new System.Drawing.Point(8, 32);
            this.Label20.Name = "Label20";
            this.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label20.Size = new System.Drawing.Size(136, 18);
            this.Label20.TabIndex = 43;
            this.Label20.Text = "Fecha prevista de ingreso:";
            // 
            // Label21
            // 
            this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label21.Location = new System.Drawing.Point(288, 32);
            this.Label21.Name = "Label21";
            this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label21.Size = new System.Drawing.Size(160, 18);
            this.Label21.TabIndex = 42;
            this.Label21.Text = "Fecha prevista de intervenci�n:";
            // 
            // Label22
            // 
            this.Label22.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label22.Location = new System.Drawing.Point(8, 72);
            this.Label22.Name = "Label22";
            this.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label22.Size = new System.Drawing.Size(111, 18);
            this.Label22.TabIndex = 41;
            this.Label22.Text = "Servicio responsable:";
            // 
            // lbIFPIngreso
            // 
            this.lbIFPIngreso.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbIFPIngreso.Location = new System.Drawing.Point(144, 32);
            this.lbIFPIngreso.Name = "lbIFPIngreso";
            this.lbIFPIngreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbIFPIngreso.Size = new System.Drawing.Size(65, 19);
            this.lbIFPIngreso.TabIndex = 40;
            this.lbIFPIngreso.Enabled = false;
            // 
            // lbIFPIntervencion
            // 
            this.lbIFPIntervencion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbIFPIntervencion.Location = new System.Drawing.Point(448, 32);
            this.lbIFPIntervencion.Name = "lbIFPIntervencion";
            this.lbIFPIntervencion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbIFPIntervencion.Size = new System.Drawing.Size(65, 19);
            this.lbIFPIntervencion.TabIndex = 39;
            this.lbIFPIntervencion.Enabled = false;
            // 
            // lbIServicio
            // 
            this.lbIServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbIServicio.Location = new System.Drawing.Point(144, 72);
            this.lbIServicio.Name = "lbIServicio";
            this.lbIServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbIServicio.Size = new System.Drawing.Size(137, 19);
            this.lbIServicio.TabIndex = 38;
            this.lbIServicio.Enabled = false;
            // 
            // _tsPac_TabPage4
            // 
            this._tsPac_TabPage4.Controls.Add(this.Label49);
            this._tsPac_TabPage4.Controls.Add(this.Label48);
            this._tsPac_TabPage4.Controls.Add(this.Label47);
            this._tsPac_TabPage4.Controls.Add(this.lbDPaciente);
            this._tsPac_TabPage4.Controls.Add(this.lbDHistoria);
            this._tsPac_TabPage4.Controls.Add(this.lbDEntidad);
            this._tsPac_TabPage4.Controls.Add(this.lbDNAsegurado);
            this._tsPac_TabPage4.Controls.Add(this.Label42);
            this._tsPac_TabPage4.Controls.Add(this._pctAutoInfor_3);
            this._tsPac_TabPage4.Controls.Add(this._lblAutoInfor_3);
            this._tsPac_TabPage4.Controls.Add(this.Frame6);
            this._tsPac_TabPage4.ItemSize = new System.Drawing.SizeF(80F, 28F);
            this._tsPac_TabPage4.Location = new System.Drawing.Point(10, 37);
            this._tsPac_TabPage4.Name = "_tsPac_TabPage4";
            this._tsPac_TabPage4.Size = new System.Drawing.Size(603, 317);
            this._tsPac_TabPage4.Text = "Dado de alta";
            // 
            // Label49
            // 
            this.Label49.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label49.Location = new System.Drawing.Point(8, 20);
            this.Label49.Name = "Label49";
            this.Label49.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label49.Size = new System.Drawing.Size(51, 18);
            this.Label49.TabIndex = 29;
            this.Label49.Text = "Paciente:";
            // 
            // Label48
            // 
            this.Label48.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label48.Location = new System.Drawing.Point(432, 20);
            this.Label48.Name = "Label48";
            this.Label48.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label48.Size = new System.Drawing.Size(64, 18);
            this.Label48.TabIndex = 30;
            this.Label48.Text = "Hist. cl�nica:";
            // 
            // Label47
            // 
            this.Label47.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label47.Location = new System.Drawing.Point(8, 52);
            this.Label47.Name = "Label47";
            this.Label47.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label47.Size = new System.Drawing.Size(82, 18);
            this.Label47.TabIndex = 31;
            this.Label47.Text = "E. financiadora:";
            // 
            // lbDPaciente
            // 
            this.lbDPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDPaciente.Location = new System.Drawing.Point(98, 20);
            this.lbDPaciente.Name = "lbDPaciente";
            this.lbDPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDPaciente.Size = new System.Drawing.Size(331, 19);
            this.lbDPaciente.TabIndex = 32;
            this.lbDPaciente.Enabled = false;
            // 
            // lbDHistoria
            // 
            this.lbDHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDHistoria.Location = new System.Drawing.Point(496, 20);
            this.lbDHistoria.Name = "lbDHistoria";
            this.lbDHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDHistoria.Size = new System.Drawing.Size(73, 19);
            this.lbDHistoria.TabIndex = 33;
            this.lbDHistoria.Enabled = false;
            // 
            // lbDEntidad
            // 
            this.lbDEntidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDEntidad.Location = new System.Drawing.Point(98, 52);
            this.lbDEntidad.Name = "lbDEntidad";
            this.lbDEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDEntidad.Size = new System.Drawing.Size(217, 19);
            this.lbDEntidad.TabIndex = 34;
            this.lbDEntidad.Enabled = false;
            // 
            // lbDNAsegurado
            // 
            this.lbDNAsegurado.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDNAsegurado.Location = new System.Drawing.Point(416, 55);
            this.lbDNAsegurado.Name = "lbDNAsegurado";
            this.lbDNAsegurado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDNAsegurado.Size = new System.Drawing.Size(153, 19);
            this.lbDNAsegurado.TabIndex = 35;
            this.lbDNAsegurado.Enabled = false;
            // 
            // Label42
            // 
            this.Label42.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label42.Location = new System.Drawing.Point(336, 55);
            this.Label42.Name = "Label42";
            this.Label42.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label42.Size = new System.Drawing.Size(78, 18);
            this.Label42.TabIndex = 36;
            this.Label42.Text = "N� asegurado:";
            // 
            // _pctAutoInfor_3
            // 
            this._pctAutoInfor_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._pctAutoInfor_3.Image = ((System.Drawing.Image)(resources.GetObject("_pctAutoInfor_3.Image")));
            this._pctAutoInfor_3.Location = new System.Drawing.Point(193, 277);
            this._pctAutoInfor_3.Name = "_pctAutoInfor_3";
            this._pctAutoInfor_3.Size = new System.Drawing.Size(29, 28);
            this._pctAutoInfor_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._pctAutoInfor_3.TabIndex = 37;
            this._pctAutoInfor_3.TabStop = false;
            // 
            // _lblAutoInfor_3
            // 
            this._lblAutoInfor_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblAutoInfor_3.Location = new System.Drawing.Point(239, 281);
            this._lblAutoInfor_3.Name = "_lblAutoInfor_3";
            this._lblAutoInfor_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblAutoInfor_3.Size = new System.Drawing.Size(141, 18);
            this._lblAutoInfor_3.TabIndex = 109;
            this._lblAutoInfor_3.Text = "No se autoriza informaci�n";
            // 
            // Frame6
            // 
            this.Frame6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame6.Controls.Add(this.Label23);
            this.Frame6.Controls.Add(this.Label24);
            this.Frame6.Controls.Add(this.Label25);
            this.Frame6.Controls.Add(this.lbDFAlta);
            this.Frame6.Controls.Add(this.lbDFIngreso);
            this.Frame6.Controls.Add(this.lbDMAlta);
            this.Frame6.HeaderText = "Dado de alta";
            this.Frame6.Location = new System.Drawing.Point(8, 84);
            this.Frame6.Name = "Frame6";
            this.Frame6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame6.Size = new System.Drawing.Size(553, 105);
            this.Frame6.TabIndex = 22;
            this.Frame6.Text = "Dado de alta";
            // 
            // Label23
            // 
            this.Label23.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label23.Location = new System.Drawing.Point(8, 32);
            this.Label23.Name = "Label23";
            this.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label23.Size = new System.Drawing.Size(74, 18);
            this.Label23.TabIndex = 28;
            this.Label23.Text = "Fecha de alta:";
            // 
            // Label24
            // 
            this.Label24.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label24.Location = new System.Drawing.Point(288, 32);
            this.Label24.Name = "Label24";
            this.Label24.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label24.Size = new System.Drawing.Size(93, 18);
            this.Label24.TabIndex = 27;
            this.Label24.Text = "Fecha de ingreso:";
            // 
            // Label25
            // 
            this.Label25.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label25.Location = new System.Drawing.Point(8, 72);
            this.Label25.Name = "Label25";
            this.Label25.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label25.Size = new System.Drawing.Size(81, 18);
            this.Label25.TabIndex = 26;
            this.Label25.Text = "Motivo de alta:";
            // 
            // lbDFAlta
            // 
            this.lbDFAlta.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDFAlta.Location = new System.Drawing.Point(98, 32);
            this.lbDFAlta.Name = "lbDFAlta";
            this.lbDFAlta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDFAlta.Size = new System.Drawing.Size(65, 19);
            this.lbDFAlta.TabIndex = 25;
            this.lbDFAlta.Enabled = false;
            // 
            // lbDFIngreso
            // 
            this.lbDFIngreso.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDFIngreso.Location = new System.Drawing.Point(384, 32);
            this.lbDFIngreso.Name = "lbDFIngreso";
            this.lbDFIngreso.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDFIngreso.Size = new System.Drawing.Size(65, 19);
            this.lbDFIngreso.TabIndex = 24;
            this.lbDFIngreso.Enabled = false;
            // 
            // lbDMAlta
            // 
            this.lbDMAlta.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDMAlta.Location = new System.Drawing.Point(98, 72);
            this.lbDMAlta.Name = "lbDMAlta";
            this.lbDMAlta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDMAlta.Size = new System.Drawing.Size(361, 19);
            this.lbDMAlta.TabIndex = 23;
            this.lbDMAlta.Enabled = false;
            // 
            // DFI161F1
            // 
            this.AcceptButton = this.cbBuscar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cbCerrar;
            this.ClientSize = new System.Drawing.Size(629, 452);
            this.Controls.Add(this.cbCerrar);
            this.Controls.Add(this.tsPac);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 54);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI161F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Localizaci�n de pacientes hospitalizados - DFI161F1";
            this.Activated += new System.EventHandler(this.DFI161F1_Activated);
            this.Closed += new System.EventHandler(this.DFI161F1_Closed);
            this.Load += new System.EventHandler(this.DFI161F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsPac)).EndInit();
            this.tsPac.ResumeLayout(false);
            this._tsPac_TabPage0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmDatosPersonales)).EndInit();
            this.FrmDatosPersonales.ResumeLayout(false);
            this.FrmDatosPersonales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmSexo)).EndInit();
            this.frmSexo.ResumeLayout(false);
            this.frmSexo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaNac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmBuscar)).EndInit();
            this.FrmBuscar.ResumeLayout(false);
            this.FrmBuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._Cedula_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Cedula_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Cedula_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Cedula_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbArchivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprFiliados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprFiliados)).EndInit();
            this._tsPac_TabPage1.ResumeLayout(false);
            this._tsPac_TabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUNAsegurado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._pctAutoInfor_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblAutoInfor_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUFAtencion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUHAtencion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUNOrden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUEspec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUCausa)).EndInit();
            this._tsPac_TabPage2.ResumeLayout(false);
            this._tsPac_TabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHNAsegurado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._pctAutoInfor_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblAutoInfor_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblmotausen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfmotausen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfausencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblausente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHHabitacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHCama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHUnidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbHPlanta)).EndInit();
            this._tsPac_TabPage3.ResumeLayout(false);
            this._tsPac_TabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbINAsegurado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIFPIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIFPIntervencion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbIServicio)).EndInit();
            this._tsPac_TabPage4.ResumeLayout(false);
            this._tsPac_TabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDNAsegurado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._pctAutoInfor_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblAutoInfor_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            this.Frame6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDFAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDFIngreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDMAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}        

        void ReLoadForm(bool addEvents)
		{
			InitializerbSexo();
			InitializerbBuscar();
			InitializepctAutoInfor();
			InitializelblAutoInfor();
			InitializeCedula();
		}
		void InitializerbSexo()
		{
			this.rbSexo = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbSexo[0] = _rbSexo_0;
			this.rbSexo[1] = _rbSexo_1;
		}
		void InitializerbBuscar()
		{
			this.rbBuscar = new Telerik.WinControls.UI.RadRadioButton[3];
			this.rbBuscar[2] = _rbBuscar_2;
			this.rbBuscar[1] = _rbBuscar_1;
			this.rbBuscar[0] = _rbBuscar_0;
		}
		void InitializepctAutoInfor()
		{
			this.pctAutoInfor = new System.Windows.Forms.PictureBox[4];
			this.pctAutoInfor[3] = _pctAutoInfor_3;
			this.pctAutoInfor[1] = _pctAutoInfor_1;
			this.pctAutoInfor[0] = _pctAutoInfor_0;
		}
		void InitializelblAutoInfor()
		{
			this.lblAutoInfor = new Telerik.WinControls.UI.RadLabel[4];
			this.lblAutoInfor[3] = _lblAutoInfor_3;
			this.lblAutoInfor[1] = _lblAutoInfor_1;
			this.lblAutoInfor[0] = _lblAutoInfor_0;
		}
		void InitializeCedula()
		{
			this.Cedula = new Telerik.WinControls.UI.RadTextBox[4];
			this.Cedula[0] = _Cedula_0;
			this.Cedula[1] = _Cedula_1;
			this.Cedula[2] = _Cedula_2;
			this.Cedula[3] = _Cedula_3;
		}
		#endregion
	}
}