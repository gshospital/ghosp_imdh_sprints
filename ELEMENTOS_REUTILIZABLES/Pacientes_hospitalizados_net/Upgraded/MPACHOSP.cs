using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace DLLPacHosp
{
	internal static class MPACHOSP
	{
		public static int iRespuesta = 0;
		public static string NombreApli = String.Empty;
		public static Mensajes.ClassMensajes clasemensaje = null;
		public static string FAyuda = String.Empty;
		public static SqlConnection GConexion = null;
		public static DFI161F1 NuevoDFI161F1 = null;
		public static DLLPacHosp.MCPACHOSP CLASE = null;
		public static object OBJETO2 = null;
		public static DataSet RrSQL = null;
		public static int CodSS = 0; //codigo de la seguridad social
		public static string TexSS = String.Empty; //texto de la seguridad social
		public static int CodPriv = 0; //codigo de privado
		public static string TexPriv = String.Empty; //texto de privado
		public static string CodSHombre = String.Empty; //codigo de sexo HOMBRE
		public static string TexSHombre = String.Empty; //texto de sexo HOMBRE
		public static string CodSMUJER = String.Empty; //codigo de sexo MUJER
		public static string TexSMUJER = String.Empty; //texto de sexo MUJER
		public static string CodSINDET = String.Empty; //codigo de sexo INDETERMINADO
		public static string TexSINDET = String.Empty; //texto de sexo INDETERMINADO
		public static int CodPROV = 0;
		public static bool fcargado = false;
		public static string stIdpaciente = String.Empty;
		public static string vstFechaHoraSis = String.Empty;
		public static string stFORMFILI = String.Empty;
		public static string BDPACIENT = String.Empty;

		internal static void FechaHoraSistema()
		{
			string sql = String.Empty;
			DataSet RsFechaSis = null;
			string stFechaHoraSis = String.Empty; //recupera la fecha/hora del sistema
			try
			{
				sql = "select getdate() go";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, GConexion);
				RsFechaSis = new DataSet();
				tempAdapter.Fill(RsFechaSis);
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				stFechaHoraSis = Convert.ToString(RsFechaSis.Tables[0].Rows[0][0]);
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RsFechaSis.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RsFechaSis.Close();
				vstFechaHoraSis = stFechaHoraSis;
			}
			catch (SqlException ex)
			{				
				Serrores.GestionErrorBaseDatos(ex, GConexion); //Falta ver que se hace con el resume
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
	}
}