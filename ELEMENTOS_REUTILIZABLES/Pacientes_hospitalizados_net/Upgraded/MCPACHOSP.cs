using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace DLLPacHosp
{
	public class MCPACHOSP
	{
		public void load(MCPACHOSP VAR1, object VAR2, SqlConnection Conexion, string stIdpaciente = "")
		{
			MPACHOSP.GConexion = Conexion;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			Serrores.AjustarRelojCliente(Conexion);
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select valfanu1 from sconsglo where gconsglo = 'FORMFILI'", Conexion);

			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			MPACHOSP.stFORMFILI = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			//Crea un formulario
			MPACHOSP.NuevoDFI161F1 = new DFI161F1();
			if (stIdpaciente.Length>0)
			{
				MPACHOSP.NuevoDFI161F1.REFERENCIAS(VAR1, VAR2, stIdpaciente);
				MPACHOSP.NuevoDFI161F1.ShowDialog();
			}
			else
			{
				MPACHOSP.NuevoDFI161F1.REFERENCIAS2(VAR1, VAR2);
				MPACHOSP.NuevoDFI161F1.ShowDialog();
			}

			//Set RrSQL = GConexion.OpenResultset("select * from dpacient where gidenpac = '" & stIdpaciente & "'", rdOpenKeyset, 2)

			//        NuevoDFI161F1.chbFiliacPov.Visible = False
			//        Else
			//        NuevoDFI161F1.chbFiliacPov.Visible = True
			//        NuevoDFI161F1.chbFiliacPov.Value = 1
			//        End If
			//        NuevoDFI161F1.Cargar_Grid
			//        NuevoDFI161F1.sprFiliados_Click 1, 1
			//    End If
			//End If
		}

		public MCPACHOSP()
		{
			MPACHOSP.clasemensaje = new Mensajes.ClassMensajes();
		}

		~MCPACHOSP()
		{
            try
            {
                MPACHOSP.NuevoDFI161F1.Close();
            }
            catch { }
		}
	}
}