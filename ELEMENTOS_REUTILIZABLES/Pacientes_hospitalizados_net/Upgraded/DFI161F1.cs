using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.SupportHelper;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;
using UpgradeStubs;
using System.Reflection;

namespace DLLPacHosp
{
	public partial class DFI161F1
		: RadForm
	{
        bool flagDbClick = false;
        string sqlCausa = String.Empty;
		DataSet RrsqlCausa = null;
		string MENSAJE = String.Empty;
		bool fAbiertoSQL = false;
		bool fGridCargado = false;
		bool fSeleccionado = false;
		string ModCodigo = String.Empty;
		DataSet RrSQL1 = null;
		DataSet RrSQL2 = null;
		DataSet RrSQL3 = null;
		DataSet RrSQL4 = null;
		DataSet RrSQL5 = null;
		string sql = String.Empty;
		string SQL1 = String.Empty;
		string SQL2 = String.Empty;
		string SQL3 = String.Empty;
		string SQL4 = String.Empty;
		string SQL5 = String.Empty;
		bool BuscarPorCuadro = false; //SI SE REALIZA LA BUSQUEDA POR EL CUADRO  "BUSCAR:"
		bool BuscarPorDatos = false; //SI SE REALIZA LA BUSQUEDA POR LOS DATOS PERSONALES
		bool BUSCAR = false; //CONTROLAR QUE SE INTRODUZCAN LOS DATOS MINIMOS PARA LA BUSQUEDA
		string Tabla = String.Empty;
		int NSQL = 0;
		int consulta = 0;
		int Archivo = 0;
		int FILA = 0;
		bool SI_DADO_DE_ALTA = false;
		bool bNoInfDePac = false;

        public DFI161F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);            
        }
            
        public void REFERENCIAS(MCPACHOSP Class, object OBJETO, string stIdPac)
		{
			MPACHOSP.CLASE = Class;
			MPACHOSP.OBJETO2 = OBJETO;
			MPACHOSP.stIdpaciente = stIdPac;
		}

		public void Realizar_Busqueda()
		{
			//AQUI SE REALIZA LA CONSULTA DE LOS PACIENTES QUE CUMPLAN LAS CARACTERISTICAS
			//INTRODICIDAS
			bool Coma = false;
			sql = "SELECT distinct * FROM DPACIENT ";
			if (rbBuscar[0].IsChecked)
			{
				if (tbNHistoria.Text.Trim() != "")
				{
					sql = sql + " ,hdossier ";
				}
			}

			sql = sql + "" + Tabla + " Where ";
			int ilongitud = 0;
			int iPosicion = 0;
			if (MPACHOSP.stIdpaciente == "")
			{
				//Controla si se busca por el N� de DNI
				if (MPACHOSP.stFORMFILI == "P")
				{
					if (ComprobarSiHayNif())
					{
						sql = sql + " dpacient.NDNINIFP = '" + ConstruirNif() + "'";
					}
				}
				else
				{
					if (MEBDNI.Text.Trim() != "")
					{
						if (Coma)
						{
							sql = sql + " And ";
						}
						sql = sql + " dpacient.NDNINIFP like '" + MEBDNI.Text.Trim() + "%'";
						Coma = true;
					}
				}

				//Controla si se busca por el N� de la Seguridad Social
				if (tbNSS.Text != "")
				{
					if (Coma)
				    	{
						sql = sql + " And ";
					}
					sql = sql + " dpacient.NAFILIAC = '" + tbNSS.Text + "'";
					Coma = true;
				}

				//Controla que se ha de buscar por el frame de Datos personales

				//Apellido1
				if (tbApellido1.Text != "")
				{
					if (Coma)
					{
						sql = sql + " And ";
					}
					sql = sql + " dpacient.DAPE1PAC ";
					if (MPACHOSP.BDPACIENT == "C")
					{
						sql = sql + " = '" + tbApellido1.Text.Trim() + "'";
					}
					if (MPACHOSP.BDPACIENT == "I")
					{
						sql = sql + " like '" + tbApellido1.Text.Trim() + "%'";
					}
					if (MPACHOSP.BDPACIENT == "P")
					{
						sql = sql + " like '%" + tbApellido1.Text.Trim() + "%'";
					}
					Coma = true;
				}

				//Apellido2
				if (tbApellido2.Text != "")
				{
					if (Coma)
					{
						sql = sql + " And ";
					}
					sql = sql + " dpacient.DAPE2PAC ";
					if (MPACHOSP.BDPACIENT == "C")
					{
						sql = sql + " = '" + tbApellido2.Text.Trim() + "'";
					}
					if (MPACHOSP.BDPACIENT == "I")
					{
						sql = sql + " like '" + tbApellido2.Text.Trim() + "%'";
					}
					if (MPACHOSP.BDPACIENT == "P")
					{
						sql = sql + " like '%" + tbApellido2.Text.Trim() + "%'";
					}
					Coma = true;
				}

				//nombre
				if (tbNombre.Text != "")
				{
					if (Coma)
					{
						sql = sql + " And ";
					}
					if (tbNombre.Text.Trim().IndexOf(' ') >= 0)
					{
						sql = sql + " (";
						ilongitud = tbNombre.Text.Trim().Length;
						iPosicion = 1;

						while(iPosicion < ilongitud)
						{
							if (Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) != 0)
							{
								if (tbNombre.Text.Trim().Substring(iPosicion - 1, Math.Min(Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) - iPosicion, tbNombre.Text.Trim().Length - (iPosicion - 1))).Trim() != "")
								{
									sql = sql + " dpacient.DNOMBPAC LIKE '%" + tbNombre.Text.Trim().Substring(iPosicion - 1, Math.Min(Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) - iPosicion, tbNombre.Text.Trim().Length - (iPosicion - 1))) + "%'";
									if (iPosicion <= ilongitud)
									{
										sql = sql + " or ";
									}
								}
								iPosicion = Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) + 1;
							}
							else
							{
								break;
							}
						};
						sql = sql + " dpacient.DNOMBPAC LIKE '%" + tbNombre.Text.Trim().Substring(iPosicion - 1) + "%')";
					}
					else
					{
						sql = sql + " dpacient.DNOMBPAC LIKE '%" + tbNombre.Text.Trim() + "%'";
					}
					Coma = true;
				}
				//fecha nacimiento
				if (sdcFechaNac.Text != "")
				{
					if (Coma)
					{
						sql = sql + " And ";
					}
                    //Cambia el formato de la fecha a la de la tabla
                    //DIA/MES/A�O a MES/DIA/A�O                                        
                    sdcFechaNac.Format = DateTimePickerFormat.Short;
                    //sql = sql + " dpacient.FNACIPAC = '" + sdcFechaNac.Text + "'";
                    sql = sql + " dpacient.FNACIPAC = '" + sdcFechaNac.Value.ToString("yyyy/MM/dd") + "'";
                    Coma = true;
				}

				if (rbBuscar[0].IsChecked)
				{
					//si ha introducido el numero de historia pergunta que archivo desea consultar
					if (tbNHistoria.Text != "")
					{
						if (cbbArchivo.SelectedIndex == -1)
						{
							//si no ha introducido ninguno coge el archivo central
							cbbArchivo.SelectedIndex = Archivo - 1;
							if (Coma)
							{
								sql = sql + " and ";
							}
							sql = sql + " hdossier.gserpropiet = " + cbbArchivo.SelectedItem.Value.ToString() + " and hdossier.gidenpac = dpacient.gidenpac and hdossier.ghistoria = " + tbNHistoria.Text + "";
							Coma = true;
						}
						else
						{
							//si lo ha introducido coge el archivo seleccionado
							//que se iguale
							if (Coma)
							{
								sql = sql + " and ";
							}
							sql = sql + " hdossier.gserpropiet = " + cbbArchivo.SelectedItem.Value.ToString() + " and hdossier.gidenpac = dpacient.gidenpac and hdossier.ghistoria =  " + tbNHistoria.Text + "";
							Coma = true;
						}
					}
				}

				//sexo
				if (rbSexo[1].IsChecked)
				{
					if (Coma)
					{
						sql = sql + " And ";
					}
					sql = sql + " dpacient.ITIPSEXO = '" + MPACHOSP.CodSHombre + "'";
					Coma = true;
				}
				else
				{
					if (rbSexo[0].IsChecked)
					{
						if (Coma)
						{
							sql = sql + " And ";
						}
						sql = sql + " dpacient.ITIPSEXO = '" + MPACHOSP.CodSMUJER + "'";
						Coma = true;
						//   Else
						//       If Coma = True Then sql = sql & " And "
						//       sql = sql & " (dpacient.ITIPSEXO = '" & CodSHombre & "' or dpacient.ITIPSEXO = '" & CodSMUJER & "' or dpacient.ITIPSEXO = '" & CodSINDET & "')"
					}
				}
			}
			else
			{
				sql = sql + "dpacient.gidenpac = '" + MPACHOSP.stIdpaciente + "'";
			}
			switch(NSQL)
			{
				case 1 :  // Atendido en urgencias 
					SQL1 = sql + " and dpacient.ifilprov = 'N' and " + 
					       " dpacient.gidenpac = uepisurg.gidenpac and " + 
					       " uepisurg.faltaurg is NULL "; 
					break;
				case 2 :  // Hospitalizado en planta 
					SQL2 = sql + " and dpacient.ifilprov = 'N' and " + 
					       " DCAMASBO.gidenpac = dpacient.gidenpac AND " + 
					       " AEPISADM.gidenpac = dpacient.gidenpac AND DCAMASBO.iestcama = 'O' and" + 
					       " aepisadm.FALTPLAN is NULL"; 
					break;
				case 3 :  // Ingreso programado 
					System.DateTime tempRefParam = DateTime.Now; 
					SQL3 = sql + " and dpacient.ifilprov = 'N' and " + 
					       " dpacient.gidenpac = QPROGQUI.gidenpac and " + 
					       " qprogqui.isitprog = 'P' and " + 
					       " qprogqui.iingambu= 'I' and " + 
					       " qprogqui.fpreving > " + Serrores.FormatFecha(tempRefParam) + ""; 
					break;
				case 4 :  // Dado de alta 
					SQL4 = sql + " and dpacient.ifilprov = 'N' and " + 
					       "" + 
					       " aepisadm.gidenpac = dpacient.gidenpac AND " + 
					       " aepisadm.FALTPLAN is not NULL AND " + 
					       " aepisadm.FALTPLAN BETWEEN dateadd(month, -1, getdate()) " + 
					       " and getdate()"; 
					SQL4 = SQL4 + " and dpacient.ifilprov = 'N' and " + 
					       " aepisadm.faltplan =  " + 
					       " (" + SQL4.Substring(0, Math.Min(7, SQL4.Length)) + "Max(aepisadm.faltplan) " + SQL4.Substring(18) + ")"; 
					break;
				case 5 : 
					SQL5 = sql + " and dpacient.ifilprov = 'N' and " + 
					       " dpacient.gidenpac = uepisurg.gidenpac and " + 
					       " uepisurg.faltaurg is not NULL and " + 
					       " uepisurg.faltaurg BETWEEN " + 
					       " dateadd(month, -1, getdate()) and getdate()"; 
					SQL5 = SQL5 + " and uepisurg.faltaurg = " + 
					       " (" + SQL5.Substring(0, Math.Min(7, SQL5.Length)) + "Max(uepisurg.faltaurg) " + SQL5.Substring(18) + ")"; 
					break;
			}
		}

		private void Resultado_Busqueda()
		{
			//Abre la consulta con los registros coincidentes
			switch(NSQL)
			{
				case 1 : 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(SQL1, MPACHOSP.GConexion); 
					RrSQL1 = new DataSet(); 
					tempAdapter.Fill(RrSQL1); 
					fAbiertoSQL = true; 
					break;
				case 2 : 					 
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQL2, MPACHOSP.GConexion); 
					RrSQL2 = new DataSet(); 
					tempAdapter_2.Fill(RrSQL2); 
					fAbiertoSQL = true; 
					break;
				case 3 : 
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQL3, MPACHOSP.GConexion); 
					RrSQL3 = new DataSet(); 
					tempAdapter_3.Fill(RrSQL3); 
					fAbiertoSQL = true; 
					 
					break;
				case 4 : 
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQL4, MPACHOSP.GConexion); 
					RrSQL4 = new DataSet(); 
					tempAdapter_4.Fill(RrSQL4); 
					fAbiertoSQL = true; 
					 
					break;
				case 5 : 
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQL5, MPACHOSP.GConexion); 
					RrSQL5 = new DataSet(); 
					tempAdapter_5.Fill(RrSQL5); 
					fAbiertoSQL = true; 
					 
					break;
			}
			Controlar_Carga();
		}

        private void Cargar_Grid()
		{
			string SQLNHIST = String.Empty;
			DataSet RrSQLNHIST = null;
			bool repetido = false;
			string SQLPOBLACION = String.Empty;
			DataSet RrPOBLACION = null;
			sprFiliados.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
			//Se�ala que ha cargado algun paciente con las caracteristicas
			fGridCargado = true;
			int NRegistro = 0;
			//carga en el grid la consulta correspondiente
			//en la columna 8 (Columna Oculta) carga el numero de consulta
			//y el numero de registro respecto a la consulta
			if (NSQL == 1)
			{		
				if (sprFiliados.Row == 0)
				{
					sprFiliados.Row++;
				}
				foreach (DataRow iteration_row in RrSQL1.Tables[0].Rows)
				{
					//ASIGNA A CADA COLUMNA UN CAMPO DE LA TABLA
					sprFiliados.MaxRows++;
					sprFiliados.Row = sprFiliados.MaxRows;
					sprFiliados.Col = 1;
					if (rbBuscar[0].IsChecked && tbNHistoria.Text.Trim() != "")
					{
						//si la busqueda es por numero de historia
						sprFiliados.Text = Convert.ToString(iteration_row["ghistoria"]);
					}
					else
					{
						//si no es por numero de historia lo busca
						cbbArchivo.SelectedIndex = Archivo - 1;
						SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + Convert.ToString(iteration_row["GIDENPAC"]) + "' AND HDOSSIER.GSERPROPIET = " + cbbArchivo.SelectedItem.Value.ToString() + " ";
						SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLNHIST, MPACHOSP.GConexion);
						RrSQLNHIST = new DataSet();
						tempAdapter.Fill(RrSQLNHIST);
						
						if (RrSQLNHIST.Tables[0].Rows.Count != 0)
						{
							sprFiliados.Text = Convert.ToString(RrSQLNHIST.Tables[0].Rows[0]["ghistoria"]);
						}						
						RrSQLNHIST.Close();
					}
					sprFiliados.Lock = true;

					sprFiliados.Col = 2;
					sprFiliados.Text = Convert.ToString(iteration_row["DAPE1PAC"]).Trim() + " " + Convert.ToString(iteration_row["DAPE2PAC"]).Trim() + ", " + Convert.ToString(iteration_row["DNOMBPAC"]).Trim();
					sprFiliados.Lock = true;

					sprFiliados.Col = 3;
					
					if (!Convert.IsDBNull(iteration_row["FNACIPAC"]))
					{
						sprFiliados.Text = Convert.ToDateTime(iteration_row["FNACIPAC"]).ToString("dd/MM/yyyy");
					}
					sprFiliados.Lock = true;

					sprFiliados.Col = 4;
					string switchVar = Convert.ToString(iteration_row["ITIPSEXO"]).Trim();
					if (switchVar == MPACHOSP.CodSHombre)
					{
						sprFiliados.Text = MPACHOSP.TexSHombre;
					}
					else if (switchVar == MPACHOSP.CodSMUJER)
					{ 
						sprFiliados.Text = MPACHOSP.TexSMUJER;
					}
					else if (switchVar == MPACHOSP.CodSINDET)
					{ 
						sprFiliados.Text = MPACHOSP.TexSINDET;
					}
					sprFiliados.Lock = true;

					sprFiliados.Col = 5;
					
					if (!Convert.IsDBNull(iteration_row["DDIREPAC"]))
					{
						sprFiliados.Text = Convert.ToString(iteration_row["DDIREPAC"]).Trim();
					}
					sprFiliados.Lock = true;

					sprFiliados.Col = 6;
					
					if (!Convert.IsDBNull(iteration_row["GCODIPOS"]))
					{
						sprFiliados.Text = Convert.ToString(iteration_row["GCODIPOS"]).Trim();
					}
					sprFiliados.Lock = true;

					sprFiliados.Col = 7;
					
					if (!Convert.IsDBNull(iteration_row["GPOBLACI"]))
					{
						//BUSCA LA POBLACON
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter("SELECT DPOBLACI FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(iteration_row["GPOBLACI"]) + "'", MPACHOSP.GConexion);
						RrPOBLACION = new DataSet();
						tempAdapter_2.Fill(RrPOBLACION);
						sprFiliados.Text = Convert.ToString(RrPOBLACION.Tables[0].Rows[0]["DPOBLACI"]).Trim();
						
						RrPOBLACION.Close();
					}
					sprFiliados.Lock = true;

					sprFiliados.Col = 8;
					NRegistro++;
					sprFiliados.Text = NSQL.ToString() + NRegistro.ToString();
					sprFiliados.Lock = true;

					sprFiliados.Col = 9;
					sprFiliados.Text = Convert.ToString(iteration_row["GIDENPAC"]);
					sprFiliados.Lock = true;

					sprFiliados.Row++;
					sprFiliados.Col = 1;

				}
			}
			else if (NSQL == 2)
			{ 
				//se posiciona en el primer registro encontrado
				
				DataSet RrSQLHIST = null;
				foreach (DataRow iteration_row_2 in RrSQL2.Tables[0].Rows)
				{
					//y si ha introducido algun registro en la tabla anterior
					//comprueba que no se repita
					repetido = false;
					if (RrSQL1.Tables[0].Rows.Count != 0)
					{						
						foreach (DataRow iteration_row_3 in RrSQL1.Tables[0].Rows)
						{
							if (iteration_row_2["gidenpac"] == iteration_row_3["gidenpac"])
							{
								repetido = true;
								break;
							}
						}
					}
					//si ya esta el paciente en la consulta anterior
					if (!repetido)
					{
						if (sprFiliados.Row == 0)
						{
							sprFiliados.Row++;
						}
						sprFiliados.MaxRows++;
						sprFiliados.Row = sprFiliados.MaxRows;
						sprFiliados.Col = 1;
						if (rbBuscar[0].IsChecked && tbNHistoria.Text.Trim() != "")
						{
							//si la busqueda es por numero de historia
							sprFiliados.Text = Convert.ToString(iteration_row_2["ghistoria"]);
						}
						else
						{
							//si no es por numero de historia lo busca
							//cbbArchivo.SelectedIndex = Archivo - 1;
							SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + Convert.ToString(iteration_row_2["gidenpac"]) + "' AND HDOSSIER.GSERPROPIET = " + cbbArchivo.SelectedItem.Value.ToString() + " ";
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLNHIST, MPACHOSP.GConexion);
							RrSQLNHIST = new DataSet();
							tempAdapter_3.Fill(RrSQLNHIST);
							
							RrSQLNHIST.MoveFirst();
							if (RrSQLNHIST.Tables[0].Rows.Count != 0)
							{
								sprFiliados.Text = Convert.ToString(RrSQLNHIST.Tables[0].Rows[0]["ghistoria"]);
							}							
							RrSQLNHIST.Close();
						}
						sprFiliados.Lock = true;
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter("select ghistoria from hdossier where hdossier.gidenpac = '" + Convert.ToString(iteration_row_2["gidenpac"]) + "'", MPACHOSP.GConexion);
						RrSQLHIST = new DataSet();
						tempAdapter_4.Fill(RrSQLHIST);
						if (RrSQLHIST.Tables[0].Rows.Count != 0)
						{							
							sprFiliados.Text = Convert.ToString(RrSQLHIST.Tables[0].Rows[0]["ghistoria"].ToString());
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 2;
						sprFiliados.Text = Convert.ToString(iteration_row_2["DAPE1PAC"]).Trim() + " " + Convert.ToString(iteration_row_2["DAPE2PAC"]).Trim() + ", " + Convert.ToString(iteration_row_2["DNOMBPAC"]).Trim();
						sprFiliados.Lock = true;

						sprFiliados.Col = 3;
						sprFiliados.Text = Convert.ToDateTime(iteration_row_2["FNACIPAC"]).ToString("dd/MM/yyyy");
						sprFiliados.Lock = true;

						sprFiliados.Col = 4;
						if (Convert.ToString(iteration_row_2["ITIPSEXO"]) == MPACHOSP.CodSHombre)
						{
							sprFiliados.Text = MPACHOSP.TexSHombre;
						}
						if (Convert.ToString(iteration_row_2["ITIPSEXO"]) == MPACHOSP.CodSMUJER)
						{
							sprFiliados.Text = MPACHOSP.TexSMUJER;
						}
						if (Convert.ToString(iteration_row_2["ITIPSEXO"]) == MPACHOSP.CodSINDET)
						{
							sprFiliados.Text = MPACHOSP.TexSINDET;
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 5;
						
						if (!Convert.IsDBNull(iteration_row_2["DDIREPAC"]))
						{
							sprFiliados.Text = Convert.ToString(iteration_row_2["DDIREPAC"]);
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 6;
						
						if (!Convert.IsDBNull(iteration_row_2["GCODIPOS"]))
						{
							sprFiliados.Text = Convert.ToString(iteration_row_2["GCODIPOS"]);
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 7;
						
						if (!Convert.IsDBNull(iteration_row_2["GPOBLACI"]))
						{
							//BUSCA LA POBLACION ASOCIADA AL CODIGO
							SQLPOBLACION = "SELECT DPOBLACI FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(iteration_row_2["GPOBLACI"]) + "'";
							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQLPOBLACION, MPACHOSP.GConexion);
							RrPOBLACION = new DataSet();
							tempAdapter_5.Fill(RrPOBLACION);
							sprFiliados.Text = Convert.ToString(RrPOBLACION.Tables[0].Rows[0]["DPOBLACI"]);
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 8;
						NRegistro++;
						sprFiliados.Text = NSQL.ToString() + NRegistro.ToString();
						sprFiliados.Lock = true;

						sprFiliados.Col = 9;
						sprFiliados.Text = Convert.ToString(iteration_row_2["gidenpac"]);
						sprFiliados.Lock = true;

						sprFiliados.Row++;
						sprFiliados.Col = 1;
					}					
				}
			}
			else if (NSQL == 3)
			{ 								
				foreach (DataRow iteration_row_4 in RrSQL3.Tables[0].Rows)
				{
					repetido = false;
					if (sprFiliados.MaxRows != 0)
					{
						int tempForVar = sprFiliados.MaxRows;
						for (int I = 1; I <= tempForVar; I++)
						{
							sprFiliados.Col = 9;
							sprFiliados.Row = I;
							if (sprFiliados.Text == Convert.ToString(iteration_row_4["gidenpac"]))
							{
								repetido = true;
								break;
							}
						}
					}
					if (!repetido)
					{
						if (sprFiliados.Row == 0)
						{
							sprFiliados.Row++;
						}
						sprFiliados.MaxRows++;

						//ASIGNA A CADA COLUMNA UN CAMPO DE LA TABLA
						sprFiliados.Row = sprFiliados.MaxRows;
						sprFiliados.Col = 1;
						if (rbBuscar[0].IsChecked && tbNHistoria.Text.Trim() != "")
						{
							//si la busqueda es por numero de historia
							sprFiliados.Text = Convert.ToString(iteration_row_4["ghistoria"]);
						}
						else
						{
							//si no es por numero de historia lo busca
							cbbArchivo.SelectedIndex = Archivo - 1;
							SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + Convert.ToString(iteration_row_4["gidenpac"]) + "' AND HDOSSIER.GSERPROPIET = " + cbbArchivo.SelectedItem.Value.ToString() + " ";
							SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQLNHIST, MPACHOSP.GConexion);
							RrSQLNHIST = new DataSet();
							tempAdapter_6.Fill(RrSQLNHIST);
							
							if (RrSQLNHIST.Tables[0].Rows.Count != 0)
							{
								sprFiliados.Text = Convert.ToString(RrSQLNHIST.Tables[0].Rows[0]["ghistoria"]);
							}
							
							RrSQLNHIST.Close();
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 2;
						sprFiliados.Text = Convert.ToString(iteration_row_4["DAPE1PAC"]).Trim() + " " + Convert.ToString(iteration_row_4["DAPE2PAC"]).Trim() + ", " + Convert.ToString(iteration_row_4["DNOMBPAC"]).Trim();
						sprFiliados.Lock = true;

						sprFiliados.Col = 3;
						sprFiliados.Text = Convert.ToDateTime(iteration_row_4["FNACIPAC"]).ToString("dd/MM/yyyy");
						sprFiliados.Lock = true;

						sprFiliados.Col = 4;
						string switchVar_2 = Convert.ToString(iteration_row_4["ITIPSEXO"]).Trim();
						if (switchVar_2 == MPACHOSP.CodSHombre)
						{
							sprFiliados.Text = MPACHOSP.TexSHombre;
						}
						else if (switchVar_2 == MPACHOSP.CodSMUJER)
						{ 
							sprFiliados.Text = MPACHOSP.TexSMUJER;
						}
						else if (switchVar_2 == MPACHOSP.CodSINDET)
						{ 
							sprFiliados.Text = MPACHOSP.TexSINDET;
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 5;
						
						if (!Convert.IsDBNull(iteration_row_4["DDIREPAC"]))
						{
							sprFiliados.Text = Convert.ToString(iteration_row_4["DDIREPAC"]);
						}
						else
						{
							sprFiliados.Text = "";
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 6;
						
						if (!Convert.IsDBNull(iteration_row_4["GCODIPOS"]))
						{
							sprFiliados.Text = Convert.ToString(iteration_row_4["GCODIPOS"]);
						}
						else
						{
							sprFiliados.Text = "";
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 7;
						
						if (!Convert.IsDBNull(iteration_row_4["GPOBLACI"]))
						{
							//BUSCA LA POBLACON
							SqlDataAdapter tempAdapter_7 = new SqlDataAdapter("SELECT DPOBLACI FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(iteration_row_4["GPOBLACI"]) + "'", MPACHOSP.GConexion);
							RrPOBLACION = new DataSet();
							tempAdapter_7.Fill(RrPOBLACION);
							sprFiliados.Text = Convert.ToString(RrPOBLACION.Tables[0].Rows[0]["DPOBLACI"]).Trim();							
							RrPOBLACION.Close();
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 8;
						NRegistro++;
						sprFiliados.Text = NSQL.ToString() + NRegistro.ToString();
						sprFiliados.Lock = true;

						sprFiliados.Col = 9;
						sprFiliados.Text = Convert.ToString(iteration_row_4["gidenpac"]);
						sprFiliados.Lock = true;

						sprFiliados.Row++;
					}
					sprFiliados.Col = 1;
				}

			}
			else if (NSQL == 4)
			{ 				
				foreach (DataRow iteration_row_5 in RrSQL4.Tables[0].Rows)
				{
					repetido = false;
					if (RrSQL1.Tables[0].Rows.Count != 0)
					{						
						foreach (DataRow iteration_row_6 in RrSQL1.Tables[0].Rows)
						{
							if (iteration_row_5["gidenpac"] == iteration_row_6["gidenpac"])
							{
								repetido = true;
							}
						}
					}
					if (RrSQL2.Tables[0].Rows.Count != 0)
					{						
						foreach (DataRow iteration_row_7 in RrSQL2.Tables[0].Rows)
						{
							if (iteration_row_5["gidenpac"] == iteration_row_7["gidenpac"])
							{
								repetido = true;
							}
						}
					}
					if (RrSQL3.Tables[0].Rows.Count != 0)
					{						
						foreach (DataRow iteration_row_8 in RrSQL3.Tables[0].Rows)
						{
							if (iteration_row_5["gidenpac"] == iteration_row_8["gidenpac"])
							{
								repetido = true;
							}
						}
					}
					if (!repetido)
					{
						if (sprFiliados.Row == 0)
						{
							sprFiliados.Row++;
						}
						//ASIGNA A CADA COLUMNA UN CAMPO DE LA TABLA
						sprFiliados.MaxRows++;
						sprFiliados.Row = sprFiliados.MaxRows;
						sprFiliados.Col = 1;

						if (rbBuscar[0].IsChecked && tbNHistoria.Text.Trim() != "")
						{
							//si la busqueda es por numero de historia
							sprFiliados.Text = Convert.ToString(iteration_row_5["ghistoria"]);
						}
						else
						{
							//si no es por numero de historia lo busca
							cbbArchivo.SelectedIndex = Archivo - 1;
							SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + Convert.ToString(iteration_row_5["gidenpac"]) + "' AND HDOSSIER.GSERPROPIET = " + cbbArchivo.SelectedItem.Value.ToString() + " ";
							SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SQLNHIST, MPACHOSP.GConexion);
							RrSQLNHIST = new DataSet();
							tempAdapter_8.Fill(RrSQLNHIST);							
							if (RrSQLNHIST.Tables[0].Rows.Count != 0)
							{
								sprFiliados.Text = Convert.ToString(RrSQLNHIST.Tables[0].Rows[0]["ghistoria"]);
							}							
							RrSQLNHIST.Close();
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 2;
						sprFiliados.Text = Convert.ToString(iteration_row_5["DAPE1PAC"]).Trim() + " " + Convert.ToString(iteration_row_5["DAPE2PAC"]).Trim() + ", " + Convert.ToString(iteration_row_5["DNOMBPAC"]).Trim();
						sprFiliados.Lock = true;

						sprFiliados.Col = 3;
						sprFiliados.Text = Convert.ToDateTime(iteration_row_5["FNACIPAC"]).ToString("dd/MM/yyyy");
						sprFiliados.Lock = true;

						sprFiliados.Col = 4;
						string switchVar_3 = Convert.ToString(iteration_row_5["ITIPSEXO"]).Trim();
						if (switchVar_3 == MPACHOSP.CodSHombre)
						{
							sprFiliados.Text = MPACHOSP.TexSHombre;
						}
						else if (switchVar_3 == MPACHOSP.CodSMUJER)
						{ 
							sprFiliados.Text = MPACHOSP.TexSMUJER;
						}
						else if (switchVar_3 == MPACHOSP.CodSINDET)
						{ 
							sprFiliados.Text = MPACHOSP.TexSINDET;
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 5;
						
						if (!Convert.IsDBNull(iteration_row_5["DDIREPAC"]))
						{
							sprFiliados.Text = Convert.ToString(iteration_row_5["DDIREPAC"]).Trim();
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 6;
						
						if (!Convert.IsDBNull(iteration_row_5["GCODIPOS"]))
						{
							sprFiliados.Text = Convert.ToString(iteration_row_5["GCODIPOS"]);
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 7;
						
						if (!Convert.IsDBNull(iteration_row_5["GPOBLACI"]))
						{
							//BUSCA LA POBLACON
							SqlDataAdapter tempAdapter_9 = new SqlDataAdapter("SELECT DPOBLACI FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(iteration_row_5["GPOBLACI"]) + "'", MPACHOSP.GConexion);
							RrPOBLACION = new DataSet();
							tempAdapter_9.Fill(RrPOBLACION);
							sprFiliados.Text = Convert.ToString(RrPOBLACION.Tables[0].Rows[0]["DPOBLACI"]).Trim();
							
							RrPOBLACION.Close();
						}
						sprFiliados.Lock = true;


						sprFiliados.Col = 8;
						NRegistro++;
						sprFiliados.Text = NSQL.ToString() + NRegistro.ToString();
						sprFiliados.Lock = true;

						sprFiliados.Col = 9;
						sprFiliados.Text = Convert.ToString(iteration_row_5["gidenpac"]);
						sprFiliados.Lock = true;

						sprFiliados.Row++;
					}
					else
					{
						//paciente repetido

					}
					sprFiliados.Col = 1;
				}

			}
			else if (NSQL == 5)
			{                 				
				sprFiliados.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
				foreach (DataRow iteration_row_9 in RrSQL5.Tables[0].Rows)
				{
					repetido = false;
					if (RrSQL1.Tables[0].Rows.Count != 0)
					{						
						foreach (DataRow iteration_row_10 in RrSQL1.Tables[0].Rows)
						{
							if (iteration_row_9["gidenpac"] == iteration_row_10["gidenpac"])
							{
								repetido = true;
							}
						}
					}
					if (RrSQL2.Tables[0].Rows.Count != 0)
					{						
						foreach (DataRow iteration_row_11 in RrSQL2.Tables[0].Rows)
						{
							if (iteration_row_9["gidenpac"] == iteration_row_11["gidenpac"])
							{
								repetido = true;
							}
						}
					}
					if (RrSQL3.Tables[0].Rows.Count != 0)
					{						
						foreach (DataRow iteration_row_12 in RrSQL3.Tables[0].Rows)
						{
							if (iteration_row_9["gidenpac"] == iteration_row_12["gidenpac"])
							{
								repetido = true;
							}
						}
					}
					if (RrSQL4.Tables[0].Rows.Count != 0)
					{						
						foreach (DataRow iteration_row_13 in RrSQL4.Tables[0].Rows)
						{
							if (iteration_row_9["gidenpac"] == iteration_row_13["gidenpac"])
							{
								repetido = true;
							}
						}
					}
					if (!repetido)
					{
						if (sprFiliados.Row == 0)
						{
							sprFiliados.Row++;
						}
						sprFiliados.MaxRows++;
						//ASIGNA A CADA COLUMNA UN CAMPO DE LA TABLA
						sprFiliados.Row = sprFiliados.MaxRows;
						sprFiliados.Col = 1;
						if (rbBuscar[0].IsChecked && tbNHistoria.Text.Trim() != "")
						{
							//si la busqueda es por numero de historia
							sprFiliados.Text = Convert.ToString(iteration_row_9["ghistoria"]);
						}
						else
						{
							//si no es por numero de historia lo busca
							cbbArchivo.SelectedIndex = Archivo - 1;
							SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + Convert.ToString(iteration_row_9["gidenpac"]) + "' AND HDOSSIER.GSERPROPIET = " + cbbArchivo.SelectedItem.Value.ToString() + " ";
							SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(SQLNHIST, MPACHOSP.GConexion);
							RrSQLNHIST = new DataSet();
							tempAdapter_10.Fill(RrSQLNHIST);
							
							if (RrSQLNHIST.Tables[0].Rows.Count != 0)
							{
								sprFiliados.Text = Convert.ToString(RrSQLNHIST.Tables[0].Rows[0]["ghistoria"]);
							}							
							RrSQLNHIST.Close();
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 2;
						sprFiliados.Text = Convert.ToString(iteration_row_9["DAPE1PAC"]).Trim() + " " + Convert.ToString(iteration_row_9["DAPE2PAC"]).Trim() + ", " + Convert.ToString(iteration_row_9["DNOMBPAC"]).Trim();
						sprFiliados.Lock = true;

						sprFiliados.Col = 3;
						sprFiliados.Text = Convert.ToDateTime(iteration_row_9["FNACIPAC"]).ToString("dd/MM/yyyy");
						sprFiliados.Lock = true;

						sprFiliados.Col = 4;
						string switchVar_4 = Convert.ToString(iteration_row_9["ITIPSEXO"]).Trim();
						if (switchVar_4 == MPACHOSP.CodSHombre)
						{
							sprFiliados.Text = MPACHOSP.TexSHombre;
						}
						else if (switchVar_4 == MPACHOSP.CodSMUJER)
						{ 
							sprFiliados.Text = MPACHOSP.TexSMUJER;
						}
						else if (switchVar_4 == MPACHOSP.CodSINDET)
						{ 
							sprFiliados.Text = MPACHOSP.TexSINDET;
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 5;
						
						if (!Convert.IsDBNull(iteration_row_9["DDIREPAC"]))
						{
							sprFiliados.Text = Convert.ToString(iteration_row_9["DDIREPAC"]).Trim();
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 6;
						
						if (!Convert.IsDBNull(iteration_row_9["GCODIPOS"]))
						{
							sprFiliados.Text = Convert.ToString(iteration_row_9["GCODIPOS"]);
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 7;
						
						if (!Convert.IsDBNull(iteration_row_9["GPOBLACI"]))
						{
							sprFiliados.Text = Convert.ToString(iteration_row_9["GPOBLACI"]);
						}
						sprFiliados.Lock = true;

						sprFiliados.Col = 8;
						NRegistro++;
						sprFiliados.Text = NSQL.ToString() + NRegistro.ToString();
						sprFiliados.Lock = true;

						sprFiliados.Col = 9;
						sprFiliados.Text = Convert.ToString(iteration_row_9["gidenpac"]);
						sprFiliados.Lock = true;

						sprFiliados.Row++;
					}
					sprFiliados.Col = 1;
				}
			}
		}

		private void Bloquear_Cajas()
		{
			tbNombre.IsReadOnly = true;
			tbApellido1.IsReadOnly = true;
			tbApellido2.IsReadOnly = true;
			frmSexo.Enabled = false;
			sdcFechaNac.Enabled = false;
			tbNHistoria.IsReadOnly = true;
            
            cbbArchivo.ReadOnly = true;

			if (MPACHOSP.stFORMFILI == "P")
			{
				Cedula[0].Enabled = true;
				Cedula[1].Enabled = true;
				Cedula[2].Enabled = true;
				Cedula[3].Enabled = true;
			}
			else
			{
				MEBDNI.Enabled = true;
			}
			tbNSS.IsReadOnly = true;
			FrmBuscar.Enabled = false;
		}

		private void cbBuscar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (ComprobarSiHayNif())
			{
				if (!CedulaCorrecta(Cedula[0].Text, Cedula[1].Text, Cedula[2].Text, Cedula[3].Text))
				{
					return;
				}
			}
			this.Cursor = Cursors.WaitCursor;
			sprFiliados.MaxRows = 0;
			bool PBuscar = false;
			string MENSAJE = String.Empty;
			//Comprueba los minimos requisito para realizar una busqueda
			//SI NO INTRODUCE LOS DATOS MINIMOS
			sprFiliados.Row = 0;
			//Realiza la busqueda por los parametros seleccionados
			for (NSQL = 1; NSQL <= 5; NSQL++)
			{
				if (NSQL == 1)
				{
					Tabla = ", UEPISURG ";
				}
				if (NSQL == 2)
				{
					Tabla = ", DCAMASBO , AEPISADM ";
				}
				if (NSQL == 3)
				{
					Tabla = ", QPROGQUI ";
				}
				if (NSQL == 4)
				{
					Tabla = ", AEPISADM ";
				}
				if (NSQL == 5)
				{
					Tabla = ", UEPISURG ";
				}
				Realizar_Busqueda();
				Resultado_Busqueda();
			}
			BUSCAR = false;
			if (!fGridCargado)
			{
				//No exiten datos de "paciente con estas caracter�sticas" para "mostrar"				
				string tempRefParam = "";
				short tempRefParam2 = 1520;
				string[] tempRefParam3 = new string[]{"paciente con estas caracter�sticas", "mostrar"};
				MENSAJE = Convert.ToString(MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam, tempRefParam2, MPACHOSP.GConexion, tempRefParam3));
				MPACHOSP.stIdpaciente = "";
				tbApellido1.Focus();
			}
			if (sprFiliados.MaxRows == 1)
			{
                sprFiliados.Row = 1;
                sprFiliados.Col = 1;
                sprFiliados_CellClick(sprFiliados, null);
			}            
            this.Cursor = Cursors.Default;
        }

		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			MPACHOSP.fcargado = false;

			if (fAbiertoSQL)
			{				
				RrSQL1.Close();				
				RrSQL2.Close();				
				RrSQL3.Close();				
				RrSQL4.Close();				
				RrSQL5.Close();
			}
			this.Close();
		}

		private void Cedula_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void Cedula_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.Cedula, eventSender);
			Cedula[Index].SelectionStart = 0;
			Cedula[Index].SelectionLength = Cedula[Index].Text.Trim().Length;
			//Cedula(Index).SetFocus
		}

		private void Cedula_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			int Index = Array.IndexOf(this.Cedula, eventSender);
			if (KeyAscii != 8)
			{
				switch(Index)
				{
					case 1 : 
						if (KeyAscii > 48 && KeyAscii < 57)
						{
							//MsgBox "Debe ser una letra"
							string tempRefParam = "";
							short tempRefParam2 = 1230;
							string[] tempRefParam3 = new string[] { "c�dula", "Letras"};
							MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam, tempRefParam2, MPACHOSP.GConexion, tempRefParam3);
							KeyAscii = 0;
						} 
						break;
					case 0 : case 2 : case 3 : 
						if (KeyAscii < 48 || KeyAscii > 57)
						{
							//MsgBox "Debe ser num�rico"
							string tempRefParam4 = "";
							short tempRefParam5 = 1230;
                            string[] tempRefParam6 = new string[] { "c�dula", "N�meros"};
							MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam4, tempRefParam5, MPACHOSP.GConexion, tempRefParam6);
							KeyAscii = 0;
						} 
						break;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void Cedula_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.Cedula, eventSender);
			switch(Index)
			{
				case 0 : 
					if (Cedula[Index].Text.Trim() != "")
					{
						if (Conversion.Val(Cedula[Index].Text) <= 0 || Conversion.Val(Cedula[Index].Text) > 99)
						{
							string tempRefParam = "";
							short tempRefParam2 = 1230;
                            string[] tempRefParam3 = new string[] { "c�dula", "valores comprendidos entre 1 y 10"};
							MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam, tempRefParam2, MPACHOSP.GConexion, tempRefParam3);
							Cedula[Index].SelectionStart = 0;
							Cedula[Index].SelectionLength = Cedula[Index].Text.Trim().Length;
							Cedula[Index].Focus();
						}
					} 
					break;
				case 1 : 
					if (Cedula[Index].Text.Trim() != "")
					{
						Cedula[Index].Text = Cedula[Index].Text.Trim().ToUpper();
					} 
					break;
			}
		}

		private void DFI161F1_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				//cargar la tabla de archivos de historia(HSERARCH)
				string HospitalizadoIngresado = String.Empty;

				SqlDataAdapter tempAdapter = new SqlDataAdapter("select * from hserarch", MPACHOSP.GConexion);
				DataSet RrArchivo = new DataSet();
				tempAdapter.Fill(RrArchivo);
                
                cbbArchivo.Items.Clear();
                foreach (DataRow iteration_row in RrArchivo.Tables[0].Rows)
                {
                    cbbArchivo.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dserarch"]).Trim(), Convert.ToString(iteration_row["gserarch"]).Trim()));
                    if (Convert.ToString(iteration_row["icentral"]) == "S")
                    {
                        cbbArchivo.SelectedIndex = RrArchivo.Tables[0].Rows.Count - 1;
                        Archivo = cbbArchivo.Items.Count;
                    }
                }
                                
                RrArchivo.Close();

				cbbArchivo.Enabled = false;
				Limpiar_Cajas();
				Desbloquear_cajas();
				cbBuscar.Enabled = false;
				tbApellido1.Focus();
				if (MPACHOSP.stIdpaciente != "")
				{
					cbBuscar_Click(cbBuscar, new EventArgs());
				}

				string HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(MPACHOSP.GConexion);
				if (HospitalOCentro.Trim().ToUpper() == "N")
				{
					HospitalizadoIngresado = "Ingresados";
				}
				else
				{
					HospitalizadoIngresado = "Hospitalizados";
				}

				string LitPersona = Serrores.ObtenerLiteralPersona(MPACHOSP.GConexion);
				LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();

				tsPac.SelectedPage.Text = LitPersona;
				Label4.Text = LitPersona + ":";
				Label33.Text = LitPersona + ":";
				Label41.Text = LitPersona + ":";
				Label49.Text = LitPersona + ":";

				LitPersona = LitPersona.ToLower() + "s";
				this.Text = "Localizaci�n de " + LitPersona + " " + HospitalizadoIngresado + " - DFI161F1";

				Frame4.Text = HospitalizadoIngresado + " en planta";                
            }
		}
		
		private void DFI161F1_Load(Object eventSender, EventArgs eventArgs)
		{            
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			fAbiertoSQL = false;
			string SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SEGURSOC'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLENTIDAD, MPACHOSP.GConexion);
			DataSet RrSQLENTIDAD = new DataSet();
			tempAdapter.Fill(RrSQLENTIDAD);
			//codigo de la SEGURIDAD SOCIAL
			
			MPACHOSP.CodSS = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
			//texto de la seguridad social
			
			MPACHOSP.TexSS = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
			SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'PRIVADO'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLENTIDAD, MPACHOSP.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_2.Fill(RrSQLENTIDAD);
			//codigo de regimen economco privado
			
			MPACHOSP.CodPriv = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
			//texto de regimen economco privado
			
			MPACHOSP.TexPriv = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
			
			RrSQLENTIDAD.Close();

			string SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SHOMBRE'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLSEXO, MPACHOSP.GConexion);
			DataSet RrSEXO = new DataSet();
			tempAdapter_3.Fill(RrSEXO);
			
			MPACHOSP.CodSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
			
			MPACHOSP.TexSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

			SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SMUJER'";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLSEXO, MPACHOSP.GConexion);
			RrSEXO = new DataSet();
			tempAdapter_4.Fill(RrSEXO);
			
			MPACHOSP.CodSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
			
			MPACHOSP.TexSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

			SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SINDET'";
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQLSEXO, MPACHOSP.GConexion);
			RrSEXO = new DataSet();
			tempAdapter_5.Fill(RrSEXO);
			
			MPACHOSP.CodSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
			
			MPACHOSP.TexSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();
			
			RrSEXO.Close();

			string SQLPROV = "SELECT * FROM SCONSGLO WHERE GCONSGLO ='CODPROVI'";
			SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQLPROV, MPACHOSP.GConexion);
			DataSet RrSPROV = new DataSet();
			tempAdapter_6.Fill(RrSPROV);
			
			MPACHOSP.CodPROV = Convert.ToInt32(Double.Parse(Convert.ToString(RrSPROV.Tables[0].Rows[0]["NNUMERI1"]).Trim()));
			
			RrSPROV.Close();

			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter("SELECT * FROM SCONSGLO WHERE GCONSGLO ='IDENPERS'", MPACHOSP.GConexion);
			RrSPROV = new DataSet();
			tempAdapter_7.Fill(RrSPROV);
			
			rbBuscar[1].Text = Convert.ToString(RrSPROV.Tables[0].Rows[0]["valfanu1"]).Trim();
			Label68.Text = "Apellido 1";
			Label67.Text = "Apellido 2";
			if (MPACHOSP.stFORMFILI == "P")
			{
				MEBDNI.Visible = false;
				Cedula[0].Visible = true;
				Cedula[1].Visible = true;
				Cedula[2].Visible = true;
				Cedula[3].Visible = true;
				Label1.Visible = true;
			}
			else
			{
				MEBDNI.Visible = true;
				Cedula[0].Visible = false;
				Cedula[1].Visible = false;
				Cedula[2].Visible = false;
				Cedula[3].Visible = false;
				Label1.Visible = false;
				if (MPACHOSP.stFORMFILI == "V")
				{
					//''        MEBDNI.Mask = "#########"
					Label68.Text = "Apellidos:";
					Label67.Text = "Apellido c�nyuge:";
				}
			}
			SQLENTIDAD = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO = 'BPACIENT'";
			SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SQLENTIDAD, MPACHOSP.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_8.Fill(RrSQLENTIDAD);
			//codigo de regimen economco privado
			if (RrSQLENTIDAD.Tables[0].Rows.Count == 0)
			{
				MPACHOSP.BDPACIENT = "C";
			}
			else
			{
				
				MPACHOSP.BDPACIENT = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["valfanu1"]).Trim();
			}
			
			RrSQLENTIDAD.Close();

			//Mostrar al usuario si el paciente deja o no informar a las personas que preguntan por �l.

			SQLENTIDAD = "select * from SCONSGLO where gconsglo ='INFOPACI' and valfanu1='S'";
			SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(SQLENTIDAD, MPACHOSP.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_9.Fill(RrSQLENTIDAD);
			bNoInfDePac = (RrSQLENTIDAD.Tables[0].Rows.Count == 1);

			pctAutoInfor[0].Visible = false;
			lblAutoInfor[0].Visible = false;

			pctAutoInfor[1].Visible = false;
			lblAutoInfor[1].Visible = false;

			pctAutoInfor[3].Visible = false;
			lblAutoInfor[3].Visible = false;

			//cargar la tabla de archivos de historia(HSERARCH)
			//Dim RrArchivo As rdoResultset
			//Set RrArchivo = GConexion.OpenResultset("select * from hserarch", rdOpenKeyset, rdConcurValues)
			//Do Until RrArchivo.EOF
			//    cbbArchivo.AddItem Trim(RrArchivo("dserarch"))
			//    cbbArchivo.ItemData(cbbArchivo.NewIndex) = RrArchivo("gserarch")
			//    If RrArchivo("icentral") = "S" Then
			//    cbbArchivo.Text = RrArchivo("dserarch")
			//    Archivo = cbbArchivo.ListCount
			//    End If
			//    RrArchivo.MoveNext
			//Loop
			//RrArchivo.Close
			MPACHOSP.FechaHoraSistema();			
			sdcFechaNac.MaxDate = Convert.ToDateTime(DateTime.Parse(MPACHOSP.vstFechaHoraSis.ToString()).ToShortDateString());

            ////
            //INDRA_DGMORENOG_10/05/2016 - INICIO
            //Proposito: Necesario para activar el evento DoubleClick sobre el contro RadioButton 
            //Sprint 4
            ////
            MethodInfo m = typeof(RadioButton).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
            if (m != null)
            {
                m.Invoke(_rbBuscar_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_2, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });

                m.Invoke(_rbSexo_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbSexo_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
            }
            _rbBuscar_0.MouseDoubleClick += rbBuscar_DblClick;            
            _rbBuscar_1.MouseDoubleClick += rbBuscar_DblClick;            
            _rbBuscar_2.MouseDoubleClick += rbBuscar_DblClick;

            _rbSexo_0.MouseDoubleClick += rbSexo_DblClick;
            _rbSexo_1.MouseDoubleClick += rbSexo_DblClick;
            ////
            //INDRA_DGMORENOG_10/05/2016 - FIN
            //Proposito: Necesario para activar el evento DoubleClick sobre el contro RadioButton 
            //Sprint 4
            ////
        }

        private void MEBDNI_Enter(Object eventSender, EventArgs eventArgs)
		{
			MEBDNI.SelectionStart = 0;
			MEBDNI.SelectionLength = MEBDNI.Text.Trim().Length;
		}

		private void MEBDNI_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			int Rellena_ceros = 0;
			if (MPACHOSP.stFORMFILI == "E")
			{
				Rellena_ceros = 8;
			}
			else
			{
				Rellena_ceros = 9;
			}


			bool paso = false;

			if ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123))
			{
				if (MEBDNI.Text.Trim().Length == 0)
				{
					paso = true;
				}
				else
				{
					paso = Conversion.Val(MEBDNI.Text) == 0;
				}
			}
			int Tama�o = 0;
			int ceros = 0;
			string resto = String.Empty;
			StringBuilder resto2 = new StringBuilder();
			if (!paso)
			{
				if (KeyAscii != 8 && ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123)))
				{
					if (KeyAscii < 48 || KeyAscii > 57)
					{
						Tama�o = MEBDNI.Text.Trim().Length;
						resto = MEBDNI.Text.Trim();
						ceros = Rellena_ceros - Tama�o;
						//    MEBDNI.Text = "         "
						MEBDNI.Text = "";
						for (int I = 1; I <= ceros; I++)
						{
							resto2.Append("0");
						}
						resto2.Append(resto);
						if (resto2.ToString().Trim().Length < 9 && MPACHOSP.stFORMFILI == "E")
						{
							resto2.Append(Strings.Chr(KeyAscii).ToString().ToUpper());
						}
						//    MEBDNI.Text = "         "
						MEBDNI.Text = "";
						MEBDNI.Text = resto2.ToString();
					}
				}
				MEBDNI.Focus();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
            SSTabHelper.SetSelectedTabIndex(tsPac, 0);

        }

		private void MEBDNI_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Rellena_ceros = 0;
			if (MPACHOSP.stFORMFILI == "E")
			{
				Rellena_ceros = 8;
			}
			else
			{
				Rellena_ceros = 9;
			}
			int Tama�o = 0;
			int ceros = 0;
			string resto = String.Empty;
			StringBuilder resto2 = new StringBuilder();
			if (MEBDNI.Text.Trim().Length < 8 && MEBDNI.Text.Trim().Length != 0)
			{
				Tama�o = MEBDNI.Text.Trim().Length;
				resto = MEBDNI.Text.Trim();
				ceros = Rellena_ceros - Tama�o;
				for (int I = 1; I <= ceros; I++)
				{
					resto2.Append("0");
				}
				if (Rellena_ceros == 8)
				{
					MEBDNI.Text = resto2.ToString() + resto + " ";
				}
				if (Rellena_ceros == 9)
				{
					MEBDNI.Text = resto2.ToString() + resto;
				}
			}
			MEBDNI.Text = MEBDNI.Text.ToUpper();
		}

		private bool isInitializingComponent;

        private void rbBuscar_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.rbBuscar, eventSender);
				//Habilita la caja seleccionada para buscar y desabilita las
				//restantes
				switch(Index)
				{
					case 0 :  //Opci�n N� de Historia Clinica 
                        if (flagDbClick)
                        {
                            _rbBuscar_0.IsChecked = false;
                            flagDbClick = false;
                        }
                        else
                        {
                            tbNHistoria.Enabled = true;
                            cbbArchivo.Enabled = true;
                            if (MPACHOSP.stFORMFILI == "P")
                            {
                                Cedula[0].Enabled = false;
                                Cedula[1].Enabled = false;
                                Cedula[2].Enabled = false;
                                Cedula[3].Enabled = false;
                                Cedula[0].Text = "";
                                Cedula[1].Text = "";
                                Cedula[2].Text = "";
                                Cedula[3].Text = "";
                            }
                            else
                            {
                                MEBDNI.Enabled = false;
                                //    MEBDNI.Text = "         "
                                MEBDNI.Text = "";
                            }
                            tbNSS.Enabled = false;
                            tbNSS.Text = "";
                            tbNHistoria.Focus();

                       
                        }
                        break;
                    case 1 :  //Opcion de DNI 
                        if (flagDbClick)
                        {
                            _rbBuscar_1.IsChecked = false;
                            flagDbClick = false;
                        }
                        else
                        {
                            tbNHistoria.Enabled = false;
                            tbNHistoria.Text = "";
                            cbbArchivo.Enabled = false;
                            tbNSS.Enabled = false;
                            tbNSS.Text = "";
                            if (MPACHOSP.stFORMFILI == "P")
                            {
                                Cedula[0].Enabled = true;
                                Cedula[1].Enabled = true;
                                Cedula[2].Enabled = true;
                                Cedula[3].Enabled = true;
                                Cedula[0].Focus();
                            }
                            else
                            {
                                MEBDNI.Enabled = true;
                                MEBDNI.Focus();
                            }
                        }
                        break;
					case 2 :  //Opcion de NSS 						
                        if (flagDbClick)
                        {
                            _rbBuscar_2.IsChecked = false;
                            flagDbClick = false;
                        }
                        else
                        {
                            tbNHistoria.Enabled = false;
                            tbNHistoria.Text = "";
                            cbbArchivo.Enabled = false;
                            MEBDNI.Enabled = false;
                            if (MPACHOSP.stFORMFILI == "P")
                            {
                                Cedula[0].Enabled = false;
                                Cedula[1].Enabled = false;
                                Cedula[2].Enabled = false;
                                Cedula[3].Enabled = false;
                                Cedula[0].Text = "";
                                Cedula[1].Text = "";
                                Cedula[2].Text = "";
                                Cedula[3].Text = "";
                            }
                            else
                            {
                                MEBDNI.Enabled = false;
                                //    MEBDNI.Text = "         "
                                MEBDNI.Text = "";
                            }
                            tbNSS.Enabled = true;
                            tbNSS.Focus();
                        }
                        break;
				}
			}
		}

        private void rbBuscar_DblClick(object sender, System.EventArgs e)
        {
            //Desselecciona la opcion seleccionada, desabilitando la caja
            //y limpiando su contenido
            int Index = Array.IndexOf(this.rbBuscar, sender);            
            flagDbClick = true;
            switch (Index)
			{
				case 0 :  //Opci�n N� de Historia Clinica 
					rbBuscar[0].IsChecked = false;
                    _rbBuscar_0.IsChecked = false;
                    tbNHistoria.Enabled = false; 
					tbNHistoria.Text = ""; 
					cbbArchivo.Enabled = false; 
					break;
				case 1 :  //Opcion de DNI 
					rbBuscar[1].IsChecked = false;
                    _rbBuscar_1.IsChecked = false;
                    if (MPACHOSP.stFORMFILI == "P")
					{
						Cedula[0].Enabled = false;
						Cedula[1].Enabled = false;
						Cedula[2].Enabled = false;
						Cedula[3].Enabled = false;
						Cedula[0].Text = "";
						Cedula[1].Text = "";
						Cedula[2].Text = "";
						Cedula[3].Text = "";
					}
					else
					{
						MEBDNI.Enabled = false;
						//    MEBDNI.Text = "         "
						MEBDNI.Text = "";
					} 
					break;
				case 2 :  //Opcion de NSS 
                    _rbBuscar_2.IsChecked = false;
                    rbBuscar[2].IsChecked = false; 
					tbNSS.Enabled = false; 
					tbNSS.Text = ""; 
					break;
			}

		}

		private void rbSexo_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
                if (flagDbClick)
                {
                    _rbSexo_0.IsChecked = false;
                    _rbSexo_1.IsChecked = false;
                    flagDbClick = false;
                }
                else
                {
                    Activar_buscar();
                }
			}
		}
        
        private void rbSexo_DblClick(object sender, System.EventArgs e)        
		{
            int Index = Array.IndexOf(this.rbSexo, sender);
            flagDbClick = true;
            //Desselecciona la opcion seleccionada
            switch (Index)
			{
				case 0 :  //Opci�n Hombre 
					rbSexo[0].IsChecked = false; 
					Activar_buscar(); 
					break;
				case 1 :  //Opcion Mujer 
					rbSexo[1].IsChecked = false; 
					Activar_buscar(); 
					break;
			}
		}

		private void sdcFechaNac_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		public void sprFiliados_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            int Col = 0;
            int Row = 0;
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex +1;
                Row = eventArgs.RowIndex+1;
            }
            else
            {
                Col = sprFiliados.Col;
                Row = sprFiliados.Row;
            }

			int NRegistro = 0;
			FILA = Row;
			if (fGridCargado && FILA > 0)
			{
				LIMPIAR_FORMULARIO();
                //comrpueba de que consulta es y que registro                
                sprFiliados.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                sprFiliados.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;
				sprFiliados.Col = 8;
				sprFiliados.Row = Row;
				NRegistro = Convert.ToInt32(Double.Parse(sprFiliados.Text.Substring(1)));
				consulta = Convert.ToInt32(Double.Parse(sprFiliados.Text.Substring(0, Math.Min(1, sprFiliados.Text.Length))));
				switch(consulta)
				{
					case 1 : 						
						RrSQL1.setAbsolutePosition(NRegistro); 
						Cargar_DatosPersonales(NRegistro - 1); 
						break;
					case 2 : 						
						RrSQL2.setAbsolutePosition(NRegistro); 
						Cargar_DatosPersonales(NRegistro-1); 
						break;
					case 3 : 						
						RrSQL3.setAbsolutePosition(NRegistro); 
						Cargar_DatosPersonales(NRegistro-1); 
						break;
					case 4 : 						
						RrSQL4.setAbsolutePosition(NRegistro); 
						Cargar_DatosPersonales(NRegistro-1); 
						break;
					case 5 : 						
						RrSQL5.setAbsolutePosition(NRegistro); 
						Cargar_DatosPersonales(NRegistro-1); 
						break;
				}
			}
		}

		private void Controlar_Carga()
		{
			//controla que consulta de pacientes  va a cargar
			switch(NSQL)
			{
				case 1 : 
					//Controla si existen registros con los parametros seleccionados 
					if (RrSQL1.Tables[0].Rows.Count != 0)
					{						
						//Si encuentra algun paciente coincidente lo carga en
						//en el grid
						fGridCargado = true;
						Cargar_Grid();
					} 
					break;
				case 2 : 
					//Controla si existen registros con los parametros seleccionados 
					if (RrSQL2.Tables[0].Rows.Count != 0)
					{						
						//Si encuentra algun paciente coincidente lo carga en
						//en el grid
						fGridCargado = true;
						Cargar_Grid();
					} 
					break;
				case 3 : 
					//Controla si existen registros con los parametros seleccionados 
					if (RrSQL3.Tables[0].Rows.Count != 0)
					{						
						//Si encuentra algun paciente coincidente lo carga en
						//en el grid
						fGridCargado = true;
						Cargar_Grid();
					} 
					break;
				case 4 : 
					//Controla si existen registros con los parametros seleccionados 
					if (RrSQL4.Tables[0].Rows.Count != 0)
					{						
						//Si encuentra algun paciente coincidente lo carga en
						//en el grid
						fGridCargado = true;
						Cargar_Grid();
					} 
					break;
				case 5 : 
					if (RrSQL5.Tables[0].Rows.Count != 0)
					{						
						//Si encuentra algun paciente coincidente lo carga en
						//en el grid
						fGridCargado = true;
						Cargar_Grid();
					} 
					break;
			}
		}

		private void Cargar_DatosPersonales(int NRegistro)
		{
			//carga los datos en la primera ficha y
			//comprueba datos en las fichas restantes
			switch(consulta)
			{
				case 1 : 					
					if (!Convert.IsDBNull(RrSQL1.Tables[0].Rows[NRegistro]["DNOMBPAC"]))
					{						
						tbNombre.Text = Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["DNOMBPAC"]);
					}
					else
					{
						tbNombre.Text = "";
					} 					
					if (!Convert.IsDBNull(RrSQL1.Tables[0].Rows[NRegistro]["DAPE1PAC"]))
					{						
						tbApellido1.Text = Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["DAPE1PAC"]);
					} 					
					if (!Convert.IsDBNull(RrSQL1.Tables[0].Rows[NRegistro]["DAPE2PAC"]))
					{						
						tbApellido2.Text = Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["DAPE2PAC"]);
					}
					else
					{
						tbApellido2.Text = "";
					} 					
					if (!Convert.IsDBNull(RrSQL1.Tables[0].Rows[NRegistro]["FNACIPAC"]))
					{						
						sdcFechaNac.Value = DateTime.Parse(Convert.ToDateTime(RrSQL1.Tables[0].Rows[NRegistro]["FNACIPAC"]).ToString("dd/MM/yyyy"));
					}
					else
					{
						sdcFechaNac.Text = "";
					} 					
					if (Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == MPACHOSP.CodSHombre)
					{
						rbSexo[1].IsChecked = true;
					} 					
					if (Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == MPACHOSP.CodSMUJER)
					{
						rbSexo[0].IsChecked = true;
					} 					
					if (Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == MPACHOSP.CodSINDET)
					{
						rbSexo[0].IsChecked = false;
						rbSexo[1].IsChecked = false;
					} 					
					if (Convert.IsDBNull(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]))
					{
						if (MPACHOSP.stFORMFILI == "P")
						{
							Cedula[0].Text = "";
							Cedula[1].Text = "";
							Cedula[2].Text = "";
							Cedula[3].Text = "";
						}
						else
						{
							//    MEBDNI.Text = "         "
							MEBDNI.Text = "";
						}
					}
					else
					{
						if (MPACHOSP.stFORMFILI == "P")
						{							
							string tempRefParam = Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]);
							if (NifCorrecto(ref tempRefParam))
							{								
								string tempRefParam2 = Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[0].Text = InsertarCedula(0, ref tempRefParam2);								
								string tempRefParam3 = Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[1].Text = InsertarCedula(1, ref tempRefParam3);								
								string tempRefParam4 = Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[2].Text = InsertarCedula(2, ref tempRefParam4);								
								string tempRefParam5 = Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[3].Text = InsertarCedula(3, ref tempRefParam5);
							}
						}
						else
						{							
							MEBDNI.Text = Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(0, Math.Min(8, Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length)) + Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(8, Math.Min(1, Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length - 8));
						}
					} 					
					if (!Convert.IsDBNull(RrSQL1.Tables[0].Rows[NRegistro]["NAFILIAC"]))
					{						
						tbNSS.Text = (!Convert.IsDBNull(RrSQL1.Tables[0].Rows[NRegistro]["NAFILIAC"])) ? Convert.ToString(RrSQL1.Tables[0].Rows[NRegistro]["NAFILIAC"]).Trim() : "";
					} 
					sprFiliados.Row = FILA; 
					sprFiliados.Col = 1; 
					tbNHistoria.Text = sprFiliados.Text; 
					Buscar_DatosRelativos(); 
					break;
				case 2 : 					
					if (!Convert.IsDBNull(RrSQL2.Tables[0].Rows[NRegistro]["DNOMBPAC"]))
					{						
						tbNombre.Text = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["DNOMBPAC"]);
					}
					else
					{
						tbNombre.Text = "";
					} 					
					if (!Convert.IsDBNull(RrSQL2.Tables[0].Rows[NRegistro]["DAPE1PAC"]))
					{						
						tbApellido1.Text = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["DAPE1PAC"]);
					}
					else
					{
						tbApellido1.Text = "";
					} 					
					if (!Convert.IsDBNull(RrSQL2.Tables[0].Rows[NRegistro]["DAPE2PAC"]))
					{						
						tbApellido2.Text = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["DAPE2PAC"]);
					}
					else
					{
						tbApellido2.Text = "";
					} 
					
					if (!Convert.IsDBNull(RrSQL2.Tables[0].Rows[NRegistro]["FNACIPAC"]))
					{						
						sdcFechaNac.Value = DateTime.Parse(Convert.ToDateTime(RrSQL2.Tables[0].Rows[NRegistro]["FNACIPAC"]).ToString("dd/MM/yyyy"));
					}
					else
					{
						sdcFechaNac.Text = "";
					} 
					
					if (Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == MPACHOSP.CodSHombre)
					{
						rbSexo[1].IsChecked = true;
					} 
					
					if (Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == MPACHOSP.CodSMUJER)
					{
						rbSexo[0].IsChecked = true;
					} 
					
					if (Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == MPACHOSP.CodSINDET)
					{
						rbSexo[0].IsChecked = false;
						rbSexo[1].IsChecked = false;
					} 
					
					if (Convert.IsDBNull(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]))
					{
						if (MPACHOSP.stFORMFILI == "P")
						{
							Cedula[0].Text = "";
							Cedula[1].Text = "";
							Cedula[2].Text = "";
							Cedula[3].Text = "";
						}
						else
						{
							//    MEBDNI.Text = "         "
							MEBDNI.Text = "";
						}
					}
					else
					{
						if (MPACHOSP.stFORMFILI == "P")
						{							
							string tempRefParam6 = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]);
							if (NifCorrecto(ref tempRefParam6))
							{								
								string tempRefParam7 = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[0].Text = InsertarCedula(0, ref tempRefParam7);
								
								string tempRefParam8 = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[1].Text = InsertarCedula(1, ref tempRefParam8);
								
								string tempRefParam9 = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[2].Text = InsertarCedula(2, ref tempRefParam9);
								
								string tempRefParam10 = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[3].Text = InsertarCedula(3, ref tempRefParam10);
							}
						}
						else
						{							
							MEBDNI.Text = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(0, Math.Min(8, Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length)) + Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(8, Math.Min(1, Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length - 8));
						}
					} 
					
					if (!Convert.IsDBNull(RrSQL2.Tables[0].Rows[NRegistro]["NAFILIAC"]))
					{						
						tbNSS.Text = Convert.ToString(RrSQL2.Tables[0].Rows[NRegistro]["NAFILIAC"]);
					} 
					sprFiliados.Row = FILA; 
					sprFiliados.Col = 1; 
					tbNHistoria.Text = sprFiliados.Text; 
					Buscar_DatosRelativos(); 
					break;
				case 3 : 					
					tbNombre.Text = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["DNOMBPAC"]); 
					
					tbApellido1.Text = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["DAPE1PAC"]); 
					
					if (!Convert.IsDBNull(RrSQL3.Tables[0].Rows[NRegistro]["DAPE2PAC"]))
					{						
						tbApellido2.Text = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["DAPE2PAC"]);
					} 
					
					if (!Convert.IsDBNull(RrSQL3.Tables[0].Rows[NRegistro]["FNACIPAC"]))
					{						
						sdcFechaNac.Value = DateTime.Parse(Convert.ToDateTime(RrSQL3.Tables[0].Rows[NRegistro]["FNACIPAC"]).ToString("dd/MM/yyyy"));
					}
					else
					{
						sdcFechaNac.Text = "";
					} 
					
					if (Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == MPACHOSP.CodSHombre)
					{
						rbSexo[1].IsChecked = true;
					} 
					
					if (Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == MPACHOSP.CodSMUJER)
					{
						rbSexo[0].IsChecked = true;
					} 
					
					if (Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == MPACHOSP.CodSINDET)
					{
						rbSexo[0].IsChecked = false;
						rbSexo[1].IsChecked = false;
					} 
					
					if (Convert.IsDBNull(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]))
					{
						if (MPACHOSP.stFORMFILI == "P")
						{
							Cedula[0].Text = "";
							Cedula[1].Text = "";
							Cedula[2].Text = "";
							Cedula[3].Text = "";
						}
						else
						{
							//    MEBDNI.Text = "         "
							MEBDNI.Text = "";
						}
					}
					else
					{
						if (MPACHOSP.stFORMFILI == "P")
						{							
							string tempRefParam11 = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]);
							if (NifCorrecto(ref tempRefParam11))
							{								
								string tempRefParam12 = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[0].Text = InsertarCedula(0, ref tempRefParam12);
								
								string tempRefParam13 = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[1].Text = InsertarCedula(1, ref tempRefParam13);
								
								string tempRefParam14 = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[2].Text = InsertarCedula(2, ref tempRefParam14);
								
								string tempRefParam15 = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[3].Text = InsertarCedula(3, ref tempRefParam15);
							}
						}
						else
						{
							//MEBDNI.Text = RrSQL3("NDNINIFP")							
							MEBDNI.Text = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(0, Math.Min(8, Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length)) + Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(8, Math.Min(1, Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length - 8));
						}
					} 
					
					if (!Convert.IsDBNull(RrSQL3.Tables[0].Rows[NRegistro]["NAFILIAC"]))
					{						
						tbNSS.Text = Convert.ToString(RrSQL3.Tables[0].Rows[NRegistro]["NAFILIAC"]);
					} 
					sprFiliados.Row = FILA; 
					sprFiliados.Col = 1; 
					tbNHistoria.Text = sprFiliados.Text; 
					Buscar_DatosRelativos(); 
					break;
				case 4 : 					
					tbNombre.Text = Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["DNOMBPAC"]); 
					
					tbApellido1.Text = Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["DAPE1PAC"]); 
					
					if (!Convert.IsDBNull(RrSQL4.Tables[0].Rows[NRegistro]["DAPE2PAC"]))
					{						
						tbApellido2.Text = Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["DAPE2PAC"]);
					} 
					
					if (!Convert.IsDBNull(RrSQL4.Tables[0].Rows[NRegistro]["FNACIPAC"]))
					{						
						sdcFechaNac.Value = DateTime.Parse(Convert.ToDateTime(RrSQL4.Tables[0].Rows[NRegistro]["FNACIPAC"]).ToString("dd/MM/yyyy"));
					}
					else
					{
						sdcFechaNac.Text = "";
					} 
					
					if (Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == "H")
					{
						rbSexo[1].IsChecked = true;
					} 
					
					if (Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == "M")
					{
						rbSexo[0].IsChecked = true;
					} 
					
					if (Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == "I")
					{
						rbSexo[0].IsChecked = false;
						rbSexo[1].IsChecked = false;
					} 
					
					if (Convert.IsDBNull(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]))
					{
						if (MPACHOSP.stFORMFILI == "P")
						{
							Cedula[0].Text = "";
							Cedula[1].Text = "";
							Cedula[2].Text = "";
							Cedula[3].Text = "";
						}
						else
						{
							//    MEBDNI.Text = "         "
							MEBDNI.Text = "";
						}
					}
					else
					{
						if (MPACHOSP.stFORMFILI == "P")
						{							
							string tempRefParam16 = Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]);
							if (NifCorrecto(ref tempRefParam16))
							{								
								string tempRefParam17 = Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[0].Text = InsertarCedula(0, ref tempRefParam17);
								
								string tempRefParam18 = Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[1].Text = InsertarCedula(1, ref tempRefParam18);
								
								string tempRefParam19 = Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[2].Text = InsertarCedula(2, ref tempRefParam19);
								
								string tempRefParam20 = Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[3].Text = InsertarCedula(3, ref tempRefParam20);
							}
						}
						else
						{
							//MEBDNI.Text = RrSQL4("NDNINIFP")							
							MEBDNI.Text = Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(0, Math.Min(8, Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length)) + Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(8, Math.Min(1, Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length - 8));
						}
					} 
					
					tbNSS.Text = (!Convert.IsDBNull(RrSQL4.Tables[0].Rows[NRegistro]["NAFILIAC"])) ? Convert.ToString(RrSQL4.Tables[0].Rows[NRegistro]["NAFILIAC"]).Trim() : ""; 
					sprFiliados.Row = FILA; 
					sprFiliados.Col = 1; 
					tbNHistoria.Text = sprFiliados.Text; 
					Buscar_DatosRelativos(); 
					break;
				case 5 : 					
					if (!Convert.IsDBNull(RrSQL5.Tables[0].Rows[NRegistro]["DNOMBPAC"]))
					{						
						tbNombre.Text = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["DNOMBPAC"]).Trim();
					} 
					
					if (!Convert.IsDBNull(RrSQL5.Tables[0].Rows[NRegistro]["DAPE1PAC"]))
					{						
						tbApellido1.Text = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["DAPE1PAC"]).Trim();
					} 
					
					if (!Convert.IsDBNull(RrSQL5.Tables[0].Rows[NRegistro]["DAPE2PAC"]))
					{						
						tbApellido2.Text = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["DAPE2PAC"]);
					} 
					
					if (!Convert.IsDBNull(RrSQL5.Tables[0].Rows[NRegistro]["FNACIPAC"]))
					{						
						sdcFechaNac.Value = DateTime.Parse(Convert.ToDateTime(RrSQL5.Tables[0].Rows[NRegistro]["FNACIPAC"]).ToString("dd/MM/yyyy"));
					}
					else
					{
						sdcFechaNac.Text = "";
					} 
					
					if (Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == "H")
					{
						rbSexo[1].IsChecked = true;
					} 
					
					if (Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == "M")
					{
						rbSexo[0].IsChecked = true;
					} 
					 
					if (Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["ITIPSEXO"]) == "I")
					{
						rbSexo[0].IsChecked = false;
						rbSexo[1].IsChecked = false;
					} 
					 					 
					if (Convert.IsDBNull(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]))
					{
						if (MPACHOSP.stFORMFILI == "P")
						{
							Cedula[0].Text = "";
							Cedula[1].Text = "";
							Cedula[2].Text = "";
							Cedula[3].Text = "";
						}
						else
						{
							//    MEBDNI.Text = "         "
							MEBDNI.Text = "";
						}
					}
					else
					{
						if (MPACHOSP.stFORMFILI == "P")
						{							
							string tempRefParam21 = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]);
							if (NifCorrecto(ref tempRefParam21))
							{								
								string tempRefParam22 = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[0].Text = InsertarCedula(0, ref tempRefParam22);
								
								string tempRefParam23 = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[1].Text = InsertarCedula(1, ref tempRefParam23);
								
								string tempRefParam24 = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[2].Text = InsertarCedula(2, ref tempRefParam24);
								
								string tempRefParam25 = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]);
								Cedula[3].Text = InsertarCedula(3, ref tempRefParam25);
							}
						}
						else
						{
							//MEBDNI.Text = RrSQL5("NDNINIFP")							
							MEBDNI.Text = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(0, Math.Min(8, Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length)) + Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]).Substring(8, Math.Min(1, Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NDNINIFP"]).Length - 8));
						}
					} 
					 					 
					if (!Convert.IsDBNull(RrSQL5.Tables[0].Rows[NRegistro]["NAFILIAC"]))
					{
						
						tbNSS.Text = Convert.ToString(RrSQL5.Tables[0].Rows[NRegistro]["NAFILIAC"]).Trim();
					} 
					sprFiliados.Row = FILA; 
					sprFiliados.Col = 1; 
					tbNHistoria.Text = sprFiliados.Text; 
					Buscar_DatosRelativos(); 
					break;
			}
			Bloquear_Cajas();
		}

		//********************************************************************************
		//*                                                                              *
		//*  Modificaciones:                                                             *
		//*                                                                              *
		//*      O.Frias 21/02/2007  Realizo la operativa para mostrar el tel�fono.      *
		//*                                                                              *
		//********************************************************************************
		private void Buscar_DatosRelativos()
		{
			string CodigoPac = String.Empty;
			string sqlserv = String.Empty;
			DataSet RrSqlServ = null;
			string sqlcama = String.Empty;
			DataSet Rrsqlcama = null;
			cbBuscar.Enabled = false;
			string fecha_minima = String.Empty;
			//coge el identificador del paciente selecionado
			switch(consulta)
			{
				case 1 : 					 
					CodigoPac = Convert.ToString(RrSQL1.Tables[0].Rows[0]["gidenpac"]); 
					break;
				case 2 : 					 
					CodigoPac = Convert.ToString(RrSQL2.Tables[0].Rows[0]["gidenpac"]); 
					break;
				case 3 : 					 
					CodigoPac = Convert.ToString(RrSQL3.Tables[0].Rows[0]["gidenpac"]); 
					break;
				case 4 : 					 
					CodigoPac = Convert.ToString(RrSQL4.Tables[0].Rows[0]["gidenpac"]); 
					break;
				case 5 : 					 
					CodigoPac = Convert.ToString(RrSQL5.Tables[0].Rows[0]["gidenpac"]); 
					break;
			}
			//carga los datos de asegurado, historia nombre y entidad financiadora

			lbUPaciente.Text = tbNombre.Text.Trim() + " " + tbApellido1.Text.Trim() + " " + tbApellido2.Text.Trim();
			lbHPaciente.Text = tbNombre.Text.Trim() + " " + tbApellido1.Text.Trim() + " " + tbApellido2.Text.Trim();
			lbIPaciente.Text = tbNombre.Text.Trim() + " " + tbApellido1.Text.Trim() + " " + tbApellido2.Text.Trim();
			lbDPaciente.Text = tbNombre.Text.Trim() + " " + tbApellido1.Text.Trim() + " " + tbApellido2.Text.Trim();


			string SQLDATOS = "select ghistoria from hdossier where hdossier.gidenpac = '" + CodigoPac + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLDATOS, MPACHOSP.GConexion);
			DataSet RrSQLDATOS = new DataSet();
			tempAdapter.Fill(RrSQLDATOS);
			if (RrSQLDATOS.Tables[0].Rows.Count != 0)
			{				
				lbUHistoria.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ghistoria"]);				
				lbHHistoria.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ghistoria"]);				
				lbIHistoria.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ghistoria"]);				
				lbDHistoria.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ghistoria"]);
			}
			
			RrSQLDATOS.Close();

			//comprueba si esta en urgencias
			SQLDATOS = "select * from uepisurg where uepisurg.gidenpac =  '" + CodigoPac + "' and uepisurg.faltaurg is NULL";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLDATOS, MPACHOSP.GConexion);
			RrSQLDATOS = new DataSet();
			tempAdapter_2.Fill(RrSQLDATOS);

			string MENSAJE = String.Empty;
			string sqlCausa = String.Empty;
			DataSet RrsqlCausa = null;
			if (RrSQLDATOS.Tables[0].Rows.Count != 0)
			{                
                if (Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iconfide"]) == "S")
                {                    
                    string tempRefParam = "";
                    short tempRefParam2 = 2800;
                    string[] tempRefParam3 = new string[] { "trae datos confidenciales", "contiuar"};                    
                    DialogResult result = MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam, tempRefParam2, MPACHOSP.GConexion, tempRefParam3);                    
                    if(result == DialogResult.No)
					{
						return;
					}
					else
					{
                        SSTabHelper.SetTabEnabled(tsPac, 1, true);
                        //tsPac.SelectedPage = _tsPac_TabPage0;
						
						if (Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["fatencio"]))
						{
							lbUFAtencion.Text = "";
						}
						else
						{							
							lbUFAtencion.Text = Convert.ToDateTime(RrSQLDATOS.Tables[0].Rows[0]["fatencio"]).ToString("dd/MM/yyyy");
						}
												
						if (Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["fatencio"]))
						{
							lbUHAtencion.Text = "";
						}
						else
						{							
							lbUHAtencion.Text = Convert.ToDateTime(RrSQLDATOS.Tables[0].Rows[0]["fatencio"]).ToString("HH:NN");
						}
						
						lbUNOrden.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gnumurge"]);
						
						sqlserv = "select dnomserv from dservici where gservici = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gservici"]);
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
						RrSqlServ = new DataSet();
						tempAdapter_3.Fill(RrSqlServ);
						
						lbUEspec.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dnomserv"]);
						
						RrSqlServ.Close();
						
						if (Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["isalaesp"]).ToUpper() == "S")
						{
							lbUBox.Text = "";
						}
						else
						{														
							if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gplantas"]) && !Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["ghabitac"]) && !Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gcamasbo"]))
							{								
								lbUBox.Text = StringsHelper.Format(RrSQLDATOS.Tables[0].Rows[0]["gplantas"], "00") + StringsHelper.Format(RrSQLDATOS.Tables[0].Rows[0]["ghabitac"], "00") + "-" + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gcamasbo"]);
							}
							else
							{
								lbUBox.Text = "";
							}
						}
						
						sqlCausa = " select dcausing from UCAUINGR Where gcausing = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gcausing"]);
						//sqlCausa = "select dmotingr from dmotingr where gmotingr = " & RrSQLDATOS("gcausing")
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlCausa, MPACHOSP.GConexion);
						RrsqlCausa = new DataSet();
						tempAdapter_4.Fill(RrsqlCausa);
						if (RrsqlCausa.Tables[0].Rows.Count != 0)
						{							
							lbUCausa.Text = Convert.ToString(RrsqlCausa.Tables[0].Rows[0]["dcausing"]);
						}
						
						RrsqlCausa.Close();						
						pctAutoInfor[0].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);						
						lblAutoInfor[0].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);
					}
				}
				else
				{
                    SSTabHelper.SetTabEnabled(tsPac, 1, true);                                        
                    RrSQLDATOS.MoveFirst();
										
					if (Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["fatencio"]))
					{
						lbUFAtencion.Text = "";
					}
					else
					{						
						lbUFAtencion.Text = Convert.ToDateTime(RrSQLDATOS.Tables[0].Rows[0]["fatencio"]).ToString("dd/MM/yyyy");
					}					
					
					if (Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["fatencio"]))
					{
						lbUHAtencion.Text = "";
					}
					else
					{						
						lbUHAtencion.Text = Convert.ToDateTime(RrSQLDATOS.Tables[0].Rows[0]["fatencio"]).ToString("HH:NN");
					}
					
					lbUNOrden.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ganourge"]) + StringsHelper.Format(RrSQLDATOS.Tables[0].Rows[0]["gnumurge"], "000000");
										
					if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gservici"]))
					{						
						sqlserv = "select dnomserv from dservici where gservici = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gservici"]);
						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
						RrSqlServ = new DataSet();
						tempAdapter_5.Fill(RrSqlServ);
						
						lbUEspec.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dnomserv"]);
						
						RrSqlServ.Close();
					}
					
					if (Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["isalaesp"]).ToUpper() == "S")
					{
						lbUBox.Text = "";
					}
					else
					{												
						if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gplantas"]) && !Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["ghabitac"]) && !Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gcamasbo"]))
						{							
							lbUBox.Text = StringsHelper.Format(RrSQLDATOS.Tables[0].Rows[0]["gplantas"], "00") + StringsHelper.Format(RrSQLDATOS.Tables[0].Rows[0]["ghabitac"], "00") + "-" + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gcamasbo"]);
						}
						else
						{
							lbUBox.Text = "";
						}
					}
					
					sqlCausa = " select dcausing from UCAUINGR Where gcausing = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gcausing"]);
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sqlCausa, MPACHOSP.GConexion);
					RrsqlCausa = new DataSet();
					tempAdapter_6.Fill(RrsqlCausa);
					if (RrsqlCausa.Tables[0].Rows.Count != 0)
					{						
						lbUCausa.Text = Convert.ToString(RrsqlCausa.Tables[0].Rows[0]["dcausing"]);
					}
					
					RrsqlCausa.Close();
										
					if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gsocieda"]))
					{
						//busca la entidad financiadora para este episodio						
						sqlserv = "select dsocieda from dsocieda where gsocieda = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gsocieda"]);
						SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
						RrSqlServ = new DataSet();
						tempAdapter_7.Fill(RrSqlServ);
						if (RrSqlServ.Tables[0].Rows.Count != 0)
						{							
							lbUEntidad.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dsocieda"]).Trim();							
							
							if (!Convert.IsDBNull(Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]).Trim()))
							{
								
								lbUNAsegurado.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]).Trim();
							}
							else
							{
								lbUNAsegurado.Text = "";
							}
						}						
						RrSqlServ.Close();
					}					
					pctAutoInfor[0].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);					
					lblAutoInfor[0].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);
				}
			}
			else
			{
				LIMPIAR_URGENCIAS();                
                SSTabHelper.SetTabEnabled(tsPac, 1, false);
			}
			
			RrSQLDATOS.Close();

			//comprueba si esta Hospitalizado en planta
			//SQLDATOS = "select * from aepisadm where aepisadm.gidenpac =  '" & CodigoPac & "' and aepisadm.faltaadm is NULL"
			SQLDATOS = " select * from aepisadm, amovifin where " + 
			           " aepisadm.gidenpac =  '" + CodigoPac + "'  and " + 
			           " aepisadm.faltplan is NULL and " + 
			           " aepisadm.ganoadme = amovifin.ganoregi and " + 
			           " aepisadm.gnumadme = amovifin.gnumregi and " + 
			           " aepisadm.fllegada = amovifin.fmovimie ";
			SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SQLDATOS, MPACHOSP.GConexion);
			RrSQLDATOS = new DataSet();
			tempAdapter_8.Fill(RrSQLDATOS);
			if (RrSQLDATOS.Tables[0].Rows.Count != 0)
			{				
				if (Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iconfide"]) == "S")
				{					
					string tempRefParam4 = "";
					short tempRefParam5 = 2800;
                    string[] tempRefParam6 = new string[] { "trae datos confidenciales", "contiuar"};
					MENSAJE = Convert.ToString(MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam4, tempRefParam5, MPACHOSP.GConexion, tempRefParam6));
					if (MENSAJE == ((int) System.Windows.Forms.DialogResult.No).ToString())
					{
						return;
					}
					else
					{
						SSTabHelper.SetTabEnabled(tsPac, 2, true);												
						//si existe se busca en la tabla de camas
						sqlcama = "select * from dcamasbo where gidenpac = '" + CodigoPac + "' AND iestcama = 'O'";
						SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(sqlcama, MPACHOSP.GConexion);
						Rrsqlcama = new DataSet();
						tempAdapter_9.Fill(Rrsqlcama);
						//se busca el servicio en la tabla de servicios
						
						sqlserv = "select dnomserv from dservici where gservici = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gserulti"]);
						SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
						RrSqlServ = new DataSet();
						tempAdapter_10.Fill(RrSqlServ);
						
						lbHServicio.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dnomserv"]);						
						RrSqlServ.Close();
						SqlDataAdapter tempAdapter_11 = new SqlDataAdapter("SELECT dunidenf FROM DUNIENFE  WHERE Gunidenf = '" + Convert.ToString(Rrsqlcama.Tables[0].Rows[0]["gcounien"]) + "'", MPACHOSP.GConexion);
						
						RrSqlServ = new DataSet();
						tempAdapter_11.Fill(RrSqlServ);						
						lbHUnidad.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dunidenf"]);						
						RrSqlServ.Close();
												
						if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gmotause"]))
						{
							SqlDataAdapter tempAdapter_12 = new SqlDataAdapter("Select dmotause from dmotause where gmotause = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gmotause"]), MPACHOSP.GConexion);
							
							RrSqlServ = new DataSet();
							tempAdapter_12.Fill(RrSqlServ);
							if (RrSqlServ.Tables[0].Rows.Count != 0)
							{								
								lblmotausen.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dmotause"]);
							}
							else
							{
								lblmotausen.Text = "";
							}							
							RrSqlServ.Close();

							SqlDataAdapter tempAdapter_13 = new SqlDataAdapter("Select fausenci from aausenci where ganoadme  = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ganoadme"]) + " gnumadme  = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gnumadme"]) + " order by fausenci desc ", MPACHOSP.GConexion);
							
							RrSqlServ = new DataSet();
							tempAdapter_13.Fill(RrSqlServ);
							if (RrSqlServ.Tables[0].Rows.Count != 0)
							{
								//puede haber mas de una ausencia								
								RrSqlServ.MoveFirst();								
								lblmotausen.Text = Convert.ToDateTime(RrSqlServ.Tables[0].Rows[0]["fausenci"]).ToString("dd/MM/yyyy HH:NN");
							}
							else
							{
								lblmotausen.Text = "";
							}							
							RrSqlServ.Close();
						}						
						lbHHabitacion.Text = Convert.ToString(Rrsqlcama.Tables[0].Rows[0]["ghabitac"]);						
						lbHPlanta.Text = Convert.ToString(Rrsqlcama.Tables[0].Rows[0]["gplantas"]);						
						lbHCama.Text = Convert.ToString(Rrsqlcama.Tables[0].Rows[0]["gcamasbo"]);

						//************************ O.Frias 21/02/2007 ************************						
						
						lblTelefono.Text = (!Convert.IsDBNull(Rrsqlcama.Tables[0].Rows[0]["ntelecam"])) ? Convert.ToString(Rrsqlcama.Tables[0].Rows[0]["ntelecam"]) : "";
						//************************ O.Frias 21/02/2007 ************************
						
						Rrsqlcama.Close();
						//busca la entidad financiadora para este episodio
						
						sqlserv = " select min(amovifin.fmovimie) From amovifin " + 
						          " Where amovifin.ganoregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ganoregi"]) + " and " + 
						          " amovifin.gnumregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gnumregi"]) + " ";
						SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
						RrSqlServ = new DataSet();
						tempAdapter_14.Fill(RrSqlServ);
						if (RrSqlServ.Tables[0].Rows.Count != 0)
						{
							
							fecha_minima = Convert.ToString(RrSqlServ.Tables[0].Rows[0][0]);
						}

						//******************** O.Frias (SQL-2005) ********************
						//'''            sqlserv = "select amovifin.nafiliac, dsocieda.dsocieda From amovifin , dsocieda " & _
						//''''            " Where amovifin.ganoregi = " & RrSQLDATOS("ganoregi") & " and " & _
						//''''            " amovifin.gnumregi = " & RrSQLDATOS("gnumregi") & " and " & _
						//''''            " amovifin.fmovimie = " & FormatFechaHMS(fecha_minima) & " and " & _
						//''''            " amovifin.gsocieda *= dsocieda.gsocieda"						
						
						object tempRefParam7 = fecha_minima;
						sqlserv = "SELECT AMOVIFIN.nafiliac, DSOCIEDA.dsocieda " + 
						          "FROM AMOVIFIN " + 
						          "LEFT OUTER JOIN DSOCIEDA ON AMOVIFIN.gsocieda = DSOCIEDA.gsocieda " + 
						          "WHERE (AMOVIFIN.ganoregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ganoregi"]) + " ) AND " + 
						          "(AMOVIFIN.gnumregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gnumregi"]) + " ) AND " + 
						          "(AMOVIFIN.fmovimie = " + Serrores.FormatFechaHMS(tempRefParam7) + " ) ";
						fecha_minima = Convert.ToString(tempRefParam7);

						//******************** O.Frias (SQL-2005) ********************
						SqlDataAdapter tempAdapter_15 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
						RrSqlServ = new DataSet();
						tempAdapter_15.Fill(RrSqlServ);
						if (RrSqlServ.Tables[0].Rows.Count != 0)
						{							
							lbHEntidad.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dsocieda"]).Trim();
														
							if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]))
							{
								
								lbHNAsegurado.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]).Trim();
							}
							else
							{
								lbHNAsegurado.Text = "";
							}
						}
						
						RrSqlServ.Close();
						
						pctAutoInfor[1].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);
						
						lblAutoInfor[1].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);
					}
				}
				else
				{
					SSTabHelper.SetTabEnabled(tsPac, 2, true);										
					//si existe se busca en la tabla de camas
					sqlcama = "select * from dcamasbo where gidenpac = '" + CodigoPac + "'  AND iestcama = 'O'";
					SqlDataAdapter tempAdapter_16 = new SqlDataAdapter(sqlcama, MPACHOSP.GConexion);
					Rrsqlcama = new DataSet();
					tempAdapter_16.Fill(Rrsqlcama);
					//se busca el servicio en la tabla de servicios
					
					sqlserv = "select dnomserv from dservici where gservici = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gserulti"]);
					SqlDataAdapter tempAdapter_17 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
					RrSqlServ = new DataSet();
					tempAdapter_17.Fill(RrSqlServ);
					if (RrSqlServ.Tables[0].Rows.Count != 0)
					{						
						lbHServicio.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dnomserv"]);
					}
				
					RrSqlServ.Close();
										
					if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gmotause"]))
					{
						SqlDataAdapter tempAdapter_18 = new SqlDataAdapter("Select dmotause from dmotause where gmotause = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gmotause"]), MPACHOSP.GConexion);
						
						RrSqlServ = new DataSet();
						tempAdapter_18.Fill(RrSqlServ);
						if (RrSqlServ.Tables[0].Rows.Count != 0)
						{							
							lblmotausen.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dmotause"]);
						}
						else
						{
							lblmotausen.Text = "";
						}
						
						RrSqlServ.Close();

						SqlDataAdapter tempAdapter_19 = new SqlDataAdapter("Select fausenci from aausenci where ganoadme  = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ganoadme"]) + " and gnumadme  = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gnumadme"]) + " order by fausenci desc ", MPACHOSP.GConexion);
						
						RrSqlServ = new DataSet();
						tempAdapter_19.Fill(RrSqlServ);
						if (RrSqlServ.Tables[0].Rows.Count != 0)
						{							
							lblfmotausen.Text = Convert.ToDateTime(RrSqlServ.Tables[0].Rows[0]["fausenci"]).ToString("dd/MM/yyyy HH:NN");
						}
						else
						{
							lblfmotausen.Text = "";
						}
						
						RrSqlServ.Close();
					}
					if (Rrsqlcama.Tables[0].Rows.Count != 0)
					{
						SqlDataAdapter tempAdapter_20 = new SqlDataAdapter("SELECT dunidenf FROM DUNIENFE  WHERE Gunidenf = '" + Convert.ToString(Rrsqlcama.Tables[0].Rows[0]["gcounien"]) + "'", MPACHOSP.GConexion);
						
						RrSqlServ = new DataSet();
						tempAdapter_20.Fill(RrSqlServ);
						
						lbHUnidad.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dunidenf"]);
						
						RrSqlServ.Close();
						
						lbHHabitacion.Text = StringsHelper.Format(Rrsqlcama.Tables[0].Rows[0]["ghabitac"], "00");
						
						lbHPlanta.Text = StringsHelper.Format(Rrsqlcama.Tables[0].Rows[0]["gplantas"], "00");
						
						lbHCama.Text = Convert.ToString(Rrsqlcama.Tables[0].Rows[0]["gcamasbo"]);
						//************************ O.Frias 21/02/2007 ************************
						
						
						lblTelefono.Text = (!Convert.IsDBNull(Rrsqlcama.Tables[0].Rows[0]["ntelecam"])) ? Convert.ToString(Rrsqlcama.Tables[0].Rows[0]["ntelecam"]) : "";
						//************************ O.Frias 21/02/2007 ************************
					}
					
					Rrsqlcama.Close();

					//busca la entidad financiadora para este episodio
					
					sqlserv = " select min(amovifin.fmovimie) From amovifin " + 
					          " Where amovifin.ganoregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ganoregi"]) + " and " + 
					          " amovifin.gnumregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gnumregi"]) + " ";
					SqlDataAdapter tempAdapter_21 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
					RrSqlServ = new DataSet();
					tempAdapter_21.Fill(RrSqlServ);
					if (RrSqlServ.Tables[0].Rows.Count != 0)
					{
						
						fecha_minima = Convert.ToString(RrSqlServ.Tables[0].Rows[0][0]);
					}

					//******************** O.Frias (SQL-2005) ********************
					//'''        sqlserv = "select amovifin.nafiliac, dsocieda.dsocieda From amovifin , dsocieda " & _
					//''''        " Where amovifin.ganoregi = " & RrSQLDATOS("ganoregi") & " and " & _
					//''''        " amovifin.gnumregi = " & RrSQLDATOS("gnumregi") & " and " & _
					//''''        " amovifin.fmovimie = " & FormatFechaHMS(fecha_minima) & " and " & _
					//''''        " amovifin.gsocieda *= dsocieda.gsocieda"					
					
					object tempRefParam8 = fecha_minima;
					sqlserv = "SELECT AMOVIFIN.nafiliac, DSOCIEDA.dsocieda " + 
					          "FROM AMOVIFIN " + 
					          "LEFT OUTER JOIN DSOCIEDA ON AMOVIFIN.gsocieda = DSOCIEDA.gsocieda " + 
					          "Where (AMOVIFIN.ganoregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ganoregi"]) + " ) And " + 
					          "(AMOVIFIN.gnumregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gnumregi"]) + " ) And " + 
					          "(AMOVIFIN.fmovimie = " + Serrores.FormatFechaHMS(tempRefParam8) + " ) ";
					fecha_minima = Convert.ToString(tempRefParam8);

					//******************** O.Frias (SQL-2005) ********************

					SqlDataAdapter tempAdapter_22 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
					RrSqlServ = new DataSet();
					tempAdapter_22.Fill(RrSqlServ);
					if (RrSqlServ.Tables[0].Rows.Count != 0)
					{
						
						lbHEntidad.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dsocieda"]).Trim();
						
						
						if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]))
						{
							
							lbHNAsegurado.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]).Trim();
						}
						else
						{
							lbHNAsegurado.Text = "";
						}
					}					
					RrSqlServ.Close();					
					pctAutoInfor[1].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);					
					lblAutoInfor[1].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);
				}
			}
			else
			{
				LIMPIAR_HOSPITALIZADO_PLANTA();
				SSTabHelper.SetTabEnabled(tsPac, 2, false);
			}

			//comprueba si esta en ingreso programado
			object tempRefParam9 = DateTime.Now;
			SQLDATOS = "select * from qprogqui where qprogqui.gidenpac =  '" + CodigoPac + "' and qprogqui.isitprog = 'P' and qprogqui.iingambu= 'I' and qprogqui.fpreving > " + Serrores.FormatFechaHMS(tempRefParam9);
			SqlDataAdapter tempAdapter_23 = new SqlDataAdapter(SQLDATOS, MPACHOSP.GConexion);
			RrSQLDATOS = new DataSet();
			tempAdapter_23.Fill(RrSQLDATOS);
			if (RrSQLDATOS.Tables[0].Rows.Count != 0)
			{
				SSTabHelper.SetTabEnabled(tsPac, 3, true);
				
				RrSQLDATOS.MoveFirst();
				
				lbIFPIngreso.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["fpreving"]);
				
				lbIFPIntervencion.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["finterve"]);
				//se busca el servicio en la tabla de servicios
				
				sqlserv = "select dnomserv from dservici where gservici = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gservici"]);
				SqlDataAdapter tempAdapter_24 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
				RrSqlServ = new DataSet();
				tempAdapter_24.Fill(RrSqlServ);
				
				lbIServicio.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dnomserv"]);
				
				RrSqlServ.Close();
				//busca la entidad financiadora para este episodio
				
				sqlserv = "select dsocieda from dsocieda where gsocieda = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gsocieda"]);
				SqlDataAdapter tempAdapter_25 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
				RrSqlServ = new DataSet();
				tempAdapter_25.Fill(RrSqlServ);
				if (RrSqlServ.Tables[0].Rows.Count != 0)
				{					
					lbIEntidad.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dsocieda"]).Trim();										
					if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]))
					{						
						lbINAsegurado.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]).Trim();
					}
					else
					{
						lbINAsegurado.Text = "";
					}
				}				
				RrSqlServ.Close();
			}
			else
			{
				LIMPIAR_INGRESO_PROGRAMADO();
				SSTabHelper.SetTabEnabled(tsPac, 3, false);
			}

			//Comprueba si esta dado de alta
			//(desde hace un mes a la fecha actual)en urgencias
			System.DateTime tempRefParam10 = DateTime.Now.AddDays(-30);
			object tempRefParam11 = DateTime.Now;
			SQLDATOS = "select * from uepisurg  where uepisurg.gidenpac =  '" + CodigoPac + "' and uepisurg.faltaurg between " + Serrores.FormatFecha(tempRefParam10) + " AND " + Serrores.FormatFechaHMS(tempRefParam11) + " order by uepisurg.faltaurg";
			SqlDataAdapter tempAdapter_26 = new SqlDataAdapter(SQLDATOS, MPACHOSP.GConexion);
			RrSQLDATOS = new DataSet();
			tempAdapter_26.Fill(RrSQLDATOS);
			if (RrSQLDATOS.Tables[0].Rows.Count != 0)
			{
				SSTabHelper.SetTabEnabled(tsPac, 4, true);
				//se posiciona en la �ltima fecha y coge los datos				
				lbDFAlta.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["faltaurg"]);				
				lbDFIngreso.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["fllegada"]);				
				lbDMAlta.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gmotalta"]);
								
				if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gmotalta"]))
				{					
					sqlCausa = "select dmotalta from dmotalta where gmotalta = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gmotalta"]) + "";
					SqlDataAdapter tempAdapter_27 = new SqlDataAdapter(sqlCausa, MPACHOSP.GConexion);
					RrsqlCausa = new DataSet();
					tempAdapter_27.Fill(RrsqlCausa);
					
					lbDMAlta.Text = Convert.ToString(RrsqlCausa.Tables[0].Rows[0]["dmotalta"]);
					
					RrsqlCausa.Close();
				}
				//busca la entidad financiadora para este episodio
								
				if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gsocieda"]))
				{					
					sqlserv = "select dsocieda from dsocieda where gsocieda = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gsocieda"]);
					SqlDataAdapter tempAdapter_28 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
					RrSqlServ = new DataSet();
					tempAdapter_28.Fill(RrSqlServ);
					if (RrSqlServ.Tables[0].Rows.Count != 0)
					{												
						if (!Convert.IsDBNull(RrSqlServ.Tables[0].Rows[0]["dsocieda"]))
						{							
							lbDEntidad.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dsocieda"]).Trim();
						}
						else
						{
							lbDEntidad.Text = "";
						}
												
						if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]))
						{							
							lbDNAsegurado.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]).Trim();
						}
						else
						{
							lbDNAsegurado.Text = "";
						}
					}
					
					RrSqlServ.Close();
				}
				
				pctAutoInfor[3].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);
				
				lblAutoInfor[3].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);
				SI_DADO_DE_ALTA = false;
			}
			else
			{
				SI_DADO_DE_ALTA = true;
			}

			//Comprueba si esta dado de alta
			//(desde hace un mes a la fecha actual)en admision
			System.DateTime tempRefParam12 = DateTime.Now.AddDays(-30);
			System.DateTime tempRefParam13 = DateTime.Now;
			SQLDATOS = "select * from aepisadm, amovifin  where aepisadm.gidenpac =  '" + CodigoPac + "'  and " + 
			           "aepisadm.faltplan >= " + Serrores.FormatFecha(tempRefParam12) + " AND " + 
			           "aepisadm.faltplan <= " + Serrores.FormatFechaHM(tempRefParam13) + "  and " + 
			           " aepisadm.ganoadme = amovifin.ganoregi and " + 
			           " aepisadm.gnumadme = amovifin.gnumregi and " + 
			           " aepisadm.fllegada = amovifin.fmovimie " + 
			           " order by aepisadm.faltplan ";
			SqlDataAdapter tempAdapter_29 = new SqlDataAdapter(SQLDATOS, MPACHOSP.GConexion);
			RrSQLDATOS = new DataSet();
			tempAdapter_29.Fill(RrSQLDATOS);
			if (RrSQLDATOS.Tables[0].Rows.Count != 0)
			{
				if (!SSTabHelper.GetTabEnabled(tsPac, 4))
				{
					SSTabHelper.SetTabEnabled(tsPac, 4, true);
				}
				
				RrSQLDATOS.MoveLast(null);
				
				lbDFAlta.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["faltplan"]);
				
				lbDFIngreso.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["fllegada"]);
								
				if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["gmotalta"]))
				{					
					sqlCausa = "select dmotalta from dmotalta where gmotalta = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gmotalta"]) + "";
					SqlDataAdapter tempAdapter_30 = new SqlDataAdapter(sqlCausa, MPACHOSP.GConexion);
					RrsqlCausa = new DataSet();
					tempAdapter_30.Fill(RrsqlCausa);
					
					lbDMAlta.Text = Convert.ToString(RrsqlCausa.Tables[0].Rows[0]["dmotalta"]);
					
					RrsqlCausa.Close();
				}
				//busca la entidad financiadora para este episodio
				
				sqlserv = " select min(amovifin.fmovimie) From amovifin " + 
				          " Where amovifin.ganoregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ganoregi"]) + " and " + 
				          " amovifin.gnumregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gnumregi"]) + " ";
				SqlDataAdapter tempAdapter_31 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
				RrSqlServ = new DataSet();
				tempAdapter_31.Fill(RrSqlServ);
				if (RrSqlServ.Tables[0].Rows.Count != 0)
				{
					
					fecha_minima = Convert.ToString(RrSqlServ.Tables[0].Rows[0][0]);
				}

				//******************** O.Frias (SQL-2005) ********************
				//''        sqlserv = "select amovifin.nafiliac, dsocieda.dsocieda From amovifin , dsocieda " & _
				//'''        " Where amovifin.ganoregi = " & RrSQLDATOS("ganoregi") & " and " & _
				//'''        " amovifin.gnumregi = " & RrSQLDATOS("gnumregi") & " and " & _
				//'''        " amovifin.fmovimie = " & FormatFechaHMS(fecha_minima) & " and " & _
				//'''        " amovifin.gsocieda *= dsocieda.gsocieda"				
				
				object tempRefParam14 = fecha_minima;
				sqlserv = "SELECT AMOVIFIN.nafiliac, DSOCIEDA.dsocieda " + 
				          "FROM AMOVIFIN " + 
				          "LEFT OUTER JOIN DSOCIEDA ON AMOVIFIN.gsocieda = DSOCIEDA.gsocieda " + 
				          "Where (AMOVIFIN.ganoregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["ganoregi"]) + " ) And " + 
				          "(AMOVIFIN.gnumregi = " + Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["gnumregi"]) + " ) And " + 
				          "(AMOVIFIN.fmovimie = " + Serrores.FormatFechaHMS(tempRefParam14) + " ) ";
				fecha_minima = Convert.ToString(tempRefParam14);

				//******************** O.Frias (SQL-2005) ********************

				SqlDataAdapter tempAdapter_32 = new SqlDataAdapter(sqlserv, MPACHOSP.GConexion);
				RrSqlServ = new DataSet();
				tempAdapter_32.Fill(RrSqlServ);
				if (RrSqlServ.Tables[0].Rows.Count != 0)
				{										
					if (!Convert.IsDBNull(RrSqlServ.Tables[0].Rows[0]["dsocieda"]))
					{
						
						lbDEntidad.Text = Convert.ToString(RrSqlServ.Tables[0].Rows[0]["dsocieda"]).Trim();
					}
					else
					{
						lbDEntidad.Text = "";
					}
										
					if (!Convert.IsDBNull(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]))
					{						
						lbDNAsegurado.Text = Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["nafiliac"]).Trim();
					}
					else
					{
						lbDNAsegurado.Text = "";
					}
				}				
				RrSqlServ.Close();				
				pctAutoInfor[3].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);				
				lblAutoInfor[3].Visible = (((Convert.ToString(RrSQLDATOS.Tables[0].Rows[0]["iautoinf"]) + "").Trim().ToUpper() == "N") && bNoInfDePac);
			}
			else
			{
				if (SI_DADO_DE_ALTA)
				{
					LIMPIAR_DADO_DE_ALTA();
					SSTabHelper.SetTabEnabled(tsPac, 4, false);
				}
			}			
			RrSQLDATOS.Close();
		}

        private void Activar_buscar()
		{
			if (!fGridCargado)
			{
				if (tbApellido1.Text == "")
				{ //si no introduce apellido primero
					if (tbApellido2.Text == "")
					{ //si no introduce apellido segundo
						//comprueba si ha introducido numero de historia
						if (tbNHistoria.Text == "")
						{
							//comprueba si ha introducido el DNI
							if (MPACHOSP.stFORMFILI != "P")
							{
								if (MEBDNI.Text.Trim() == "")
								{
									//comprueba si ha introducido el numero de filiacion
									if (tbNSS.Text == "")
									{
										//comprueba si ha introducido el numero de filiacion
										cbBuscar.Enabled = sdcFechaNac.Text != "";
									}
									else
									{
										cbBuscar.Enabled = true;
									}
								}
								else
								{
									cbBuscar.Enabled = true;
								}
							}
							else
							{
								if (!ComprobarSiHayNif())
								{
									//comprueba si ha introducido el numero de filiacion
									if (tbNSS.Text == "")
									{
										//comprueba si ha introducido el numero de filiacion
										cbBuscar.Enabled = sdcFechaNac.Text != "";
									}
									else
									{
										cbBuscar.Enabled = true;
									}
								}
								else
								{
									cbBuscar.Enabled = true;
								}
							}
						}
						else
						{
							cbBuscar.Enabled = true;
						}
					}
					else
					{
						cbBuscar.Enabled = true;
					}
				}
				else
				{
					cbBuscar.Enabled = true;
				}
			}
			else
			{
				cbBuscar.Enabled = true;
			}
			if (rbSexo[0].IsChecked || rbSexo[1].IsChecked)
			{
				cbBuscar.Enabled = true;
			}
		}

		private void tbApellido1_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbApellido1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbApellido2_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void MEBDNI_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbApellido2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNHistoria_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbNombre_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbNombre_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNSS_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void Limpiar_Cajas()
		{
			tbNombre.Text = "";
			tbApellido1.Text = "";
			tbApellido2.Text = "";
			rbSexo[0].IsChecked = false;
			rbSexo[1].IsChecked = false;
                        
            sdcFechaNac.NullableValue = null;
            sdcFechaNac.SetToNullValue();

            tbNHistoria.Text = "";
			if (MPACHOSP.stFORMFILI == "P")
			{
				Cedula[0].Text = "";
				Cedula[1].Text = "";
				Cedula[2].Text = "";
				Cedula[3].Text = "";
			}
			else
			{
				MEBDNI.Enabled = false;
				//    MEBDNI.Text = "         "
				MEBDNI.Text = "";
			}
			tbNSS.Text = "";
			rbBuscar[0].IsChecked = false;
			rbBuscar[1].IsChecked = false;
			rbBuscar[2].IsChecked = false;
			sprFiliados.MaxRows = 0;
		}

        private void Desbloquear_cajas()
		{
			tbNombre.IsReadOnly = false;
			tbApellido1.IsReadOnly = false;
			tbApellido2.IsReadOnly = false;
			frmSexo.Enabled = true;
			sdcFechaNac.Enabled = true;
			tbNHistoria.IsReadOnly = false;
			
			cbbArchivo.ReadOnly = false;
			if (MPACHOSP.stFORMFILI == "P")
			{
				Cedula[0].Enabled = false;
				Cedula[1].Enabled = false;
				Cedula[2].Enabled = false;
				Cedula[3].Enabled = false;
			}
			else
			{
				MEBDNI.Enabled = false;
			}
			tbNSS.IsReadOnly = false;
			FrmBuscar.Enabled = true;
		}

        public void REFERENCIAS2(MCPACHOSP Class, object OBJETO)
		{ //, stIdPac As Variant)
			MPACHOSP.CLASE = Class;
			MPACHOSP.OBJETO2 = OBJETO;
			MPACHOSP.stIdpaciente = "";
		}

        private void LIMPIAR_URGENCIAS()
		{
			lbUPaciente.Text = "";
			lbUHistoria.Text = "";
			lbUEntidad.Text = "";
			lbUNAsegurado.Text = "";
			lbUFAtencion.Text = "";
			lbUHAtencion.Text = "";
			lbUNOrden.Text = "";
			lbUEspec.Text = "";
			lbUBox.Text = "";
			lbUCausa.Text = "";
			pctAutoInfor[0].Visible = false;
			lblAutoInfor[0].Visible = false;

		}

        private void LIMPIAR_HOSPITALIZADO_PLANTA()
		{
			lbHPaciente.Text = "";
			lbHHistoria.Text = "";
			lbHEntidad.Text = "";
			lbHNAsegurado.Text = "";
			lbHUnidad.Text = "";
			lbHServicio.Text = "";
			lbHPlanta.Text = "";
			lbHHabitacion.Text = "";
			lbHCama.Text = "";
			lblmotausen.Text = "";
			lblfmotausen.Text = "";
			//************************ O.Frias 21/02/2007 ************************
			lblTelefono.Text = "";
			//************************ O.Frias 21/02/2007 ************************
			pctAutoInfor[1].Visible = false;
			lblAutoInfor[1].Visible = false;
		}

		private void LIMPIAR_INGRESO_PROGRAMADO()
		{
			lbIPaciente.Text = "";
			lbIHistoria.Text = "";
			lbIEntidad.Text = "";
			lbINAsegurado.Text = "";
			lbIFPIngreso.Text = "";
			lbIFPIntervencion.Text = "";
			lbIServicio.Text = "";
		}

        private void LIMPIAR_DADO_DE_ALTA()
		{
			lbDPaciente.Text = "";
			lbDHistoria.Text = "";
			lbDEntidad.Text = "";
			lbDNAsegurado.Text = "";
			lbDFAlta.Text = "";
			lbDFIngreso.Text = "";
			lbDMAlta.Text = "";
			pctAutoInfor[3].Visible = false;
			lblAutoInfor[3].Visible = false;
		}

        private void LIMPIAR_FORMULARIO()
		{
			tbApellido1.Text = "";
			tbApellido2.Text = "";
			tbNombre.Text = "";
			sdcFechaNac.Text = "";
			rbSexo[0].IsChecked = false;
			rbSexo[1].IsChecked = false;
			tbNHistoria.Text = "";
			tbNSS.Text = "";
			if (MPACHOSP.stFORMFILI == "P")
			{
				Cedula[0].Text = "";
				Cedula[1].Text = "";
				Cedula[2].Text = "";
				Cedula[3].Text = "";
			}
			else
			{
				//    MEBDNI.Text = "         "
				MEBDNI.Text = "";
			}
			rbBuscar[0].IsChecked = false;
			rbBuscar[1].IsChecked = false;
			rbBuscar[2].IsChecked = false;
		}

        private bool ComprobarSiHayNif()
		{
			return Cedula[0].Text.Trim() != "" || Cedula[1].Text.Trim() != "" || Cedula[2].Text.Trim() != "" || Cedula[3].Text.Trim() != "";
		}

        private bool CedulaCorrecta(string Cedula0, string Cedula1, string Cedula2, string Cedula3)
		{
			bool result = false;
			//Cedula(0) Tiene que estar en dprovinc y menor de 10. Puede ser nulo
			//formato 9(2)
			if (Cedula0.Trim() != "")
			{
				if (Conversion.Val(Cedula0) > 10 || Conversion.Val(Cedula0) < 1)
				{
					string tempRefParam = "";
					short tempRefParam2 = 1230;
                    string[] tempRefParam3 = new string[] { "c�dula", "valores comprendidos entre 1 y 10"};
					MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam, tempRefParam2, MPACHOSP.GConexion, tempRefParam3);
					Cedula[0].SelectionStart = 0;
					Cedula[0].SelectionLength = Cedula[0].Text.Trim().Length;
					Cedula[0].Focus();
					return result;
				}
				else
				{
					if (!EstaEnDProvinc(Cedula0))
					{
						string tempRefParam4 = "";
						short tempRefParam5 = 1100;
                        string[] tempRefParam6 = new string[] { "Provincia"};
						MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam4, tempRefParam5, MPACHOSP.GConexion, tempRefParam6);
						Cedula[0].SelectionStart = 0;
						Cedula[0].SelectionLength = Cedula[0].Text.Trim().Length;
						Cedula[0].Focus();
						return result;
					}
				}
			}
			//Cedula(1) Tiene que estar en dsecprov. Puede ser nulo
			//formato X(2)
			if (Cedula1.Trim() != "")
			{
				if (!EstaEnDsecprov(Cedula1.Trim().ToUpper()))
				{
					string tempRefParam7 = "";
					short tempRefParam8 = 1100;
                    string[] tempRefParam9 = new string[] { "Secci�n"};
					MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam7, tempRefParam8, MPACHOSP.GConexion, tempRefParam9);
					Cedula[1].SelectionStart = 0;
					Cedula[1].SelectionLength = Cedula[1].Text.Trim().Length;
					Cedula[1].Focus();
					return result;
				}
			}
			if (Cedula2.Trim() == "")
			{
				string tempRefParam10 = "";
				short tempRefParam11 = 1040;
                string[] tempRefParam12 = new string[] { "3� de la c�dula"};
				MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam10, tempRefParam11, MPACHOSP.GConexion, tempRefParam12);
				Cedula[2].SelectionStart = 0;
				Cedula[2].SelectionLength = Cedula[2].Text.Trim().Length;
				Cedula[2].Focus();
				return result;
			}
			if (Cedula3.Trim() == "")
			{
				string tempRefParam13 = "";
				short tempRefParam14 = 1040;
                string[] tempRefParam15 = new string[] { "4� de la c�dula"};
				MPACHOSP.clasemensaje.RespuestaMensaje(MPACHOSP.NombreApli, tempRefParam13, tempRefParam14, MPACHOSP.GConexion, tempRefParam15);
				Cedula[3].SelectionStart = 0;
				Cedula[3].SelectionLength = Cedula[3].Text.Trim().Length;
				Cedula[3].Focus();
				return result;
			}
			return true;
		}

        private string ConstruirNif()
		{
			string result = String.Empty;
			result = "";
			if (Cedula[0].Text.Trim() != "")
			{
				result = Cedula[0].Text.Trim();
			}
			if (Cedula[1].Text.Trim() != "")
			{
				result = result + Cedula[1].Text.Trim();
			}
			if (Cedula[2].Text.Trim() != "")
			{
				result = result + "-" + Cedula[2].Text.Trim();
			}
			if (Cedula[3].Text.Trim() != "")
			{
				result = result + "-" + Cedula[3].Text.Trim();
			}
			return result;
		}

        public bool EstaEnDProvinc(string CODIGO)
		{
			bool result = false;
			string sqlPRO = "Select * from dprovinc where gprovinc = '" + StringsHelper.Format(CODIGO, "00") + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlPRO, MPACHOSP.GConexion);
			DataSet RRsqlPRO = new DataSet();
			tempAdapter.Fill(RRsqlPRO);
			result = RRsqlPRO.Tables[0].Rows.Count != 0;
			
			RRsqlPRO.Close();
			return result;
		}

        public bool EstaEnDsecprov(string CODIGO)
		{
			bool result = false;
			string sqlPRO = "Select * from Dsecprov where gsecprov = '" + CODIGO + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlPRO, MPACHOSP.GConexion);
			DataSet RRsqlPRO = new DataSet();
			tempAdapter.Fill(RRsqlPRO);
			result = RRsqlPRO.Tables[0].Rows.Count != 0;
			
			RRsqlPRO.Close();
			return result;
		}

        private string InsertarCedula(int posicion, ref string NIF)
		{
			StringBuilder result = new StringBuilder();
			string cadena = String.Empty;
			int I = 0;
			switch(posicion)
			{
				case 0 : 
					//obtener la cadena a tratar 
					cadena = NIF.Substring(0, Math.Min(NIF.IndexOf('-'), NIF.Length)); 
					//Una vez que tengamos la cadena la dividiremos en la parte numerica y literal 
					for (I = 1; I <= NIF.IndexOf('-'); I++)
					{
						//si es un numero
						if (Strings.Asc(cadena.Substring(I - 1, Math.Min(1, cadena.Length - (I - 1)))[0]) > 47 && Strings.Asc(cadena.Substring(I - 1, Math.Min(1, cadena.Length - (I - 1)))[0]) < 58)
						{
							result.Append(cadena.Substring(I - 1, Math.Min(1, cadena.Length - (I - 1))));
						}
					} 
					break;
				case 1 : 
					//obtener la cadena a tratar 
					cadena = NIF.Substring(0, Math.Min(NIF.IndexOf('-'), NIF.Length)); 
					//Una vez que tengamos la cadena la dividiremos en la parte numerica y literal 
					for (I = 1; I <= NIF.IndexOf('-'); I++)
					{
						//si es un numero
						if (Strings.Asc(cadena.Substring(I - 1, Math.Min(1, cadena.Length - (I - 1)))[0]) < 47 || Strings.Asc(cadena.Substring(I - 1, Math.Min(1, cadena.Length - (I - 1)))[0]) > 58)
						{
							result.Append(cadena.Substring(I - 1, Math.Min(1, cadena.Length - (I - 1))));
						}
					} 
					break;
				case 2 : 
					I = (NIF.IndexOf('-') + 1) + 1; 
					result = new StringBuilder(NIF.Substring(I - 1, Math.Min(Strings.InStr(I, NIF, "-", CompareMethod.Text) - I, NIF.Length - (I - 1)))); 
					break;
				case 3 : 
					I = (NIF.IndexOf('-') + 1); 
					I = Strings.InStr(I + 1, NIF, "-", CompareMethod.Text); 
					result = new StringBuilder(NIF.Substring(I)); 
					break;
			}
			return result.ToString();
		}

        private bool NifCorrecto(ref string NIF)
		{
			bool result = false;
			result = true;
			//si no existen guiones no es correcto
			if ((NIF.IndexOf('-') + 1) == 0)
			{
				result = false;
				//si no existe otro guion
			}
			else if (Strings.InStr((NIF.IndexOf('-') + 1) + 1, NIF, "-", CompareMethod.Text) == 0)
			{ 
				result = false;
			}

			return result;
		}

        private void DFI161F1_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}