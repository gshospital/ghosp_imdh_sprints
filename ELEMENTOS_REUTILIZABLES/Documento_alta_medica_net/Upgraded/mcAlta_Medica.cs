using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;

namespace AltaMedica
{
	public class mcAlta_Medica
	{
		// David 03/05/2004

		private System.DateTime dtFechaNacimiento = DateTime.FromOADate(0);
		private System.DateTime dtFechaSalida = DateTime.FromOADate(0);

		// Fin de David 03/05/2004

		public void proAltaMedica(int iAepisodio, int lNepisodio, Crystal cPrint, SqlConnection Conexion, string gUsuario, string gContrase�a, string PathString, string stDSN)
		{
			//recibe el episodio,el objeto crystal y el DSN
			string stDiagEntrada = String.Empty, stDiagSalida = String.Empty;
			string stDoctor = String.Empty, stCama = String.Empty, stServicio = String.Empty;
			string stSociedad = String.Empty, stNafil = String.Empty;
			int iSoc = 0;
			string stHistoria = String.Empty, stInspecc = String.Empty;
			string stMotalta = String.Empty;
			string stNEntrada = String.Empty, stNSalida = String.Empty;
			string stFSalida = String.Empty, stFEntrada = String.Empty, stRevisio = String.Empty;
			string stGidenpac = String.Empty;
			string gidenpac = String.Empty;


			mbAltaMedica.vRcCon = Conexion;
			Serrores.AjustarRelojCliente(mbAltaMedica.vRcCon);
			string StSql = "select gidenpac  from aepisadm  Where ganoadme= " + iAepisodio.ToString() + " and gnumadme=" + lNepisodio.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, mbAltaMedica.vRcCon);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{				
				gidenpac = Convert.ToString(RrSql.Tables[0].Rows[0]["gidenpac"]);
			}			
			RrSql.Close();


			Serrores.Obtener_Multilenguaje(mbAltaMedica.vRcCon, gidenpac);
			Serrores.ObtenerTextosRPT(mbAltaMedica.vRcCon, "ADMISION", "AGI561R1");
			Serrores.ObtenerMesesSemanas(mbAltaMedica.vRcCon);

			//On Error GoTo ErrAlta
			//errores
			Serrores.oClass = new Mensajes.ClassMensajes();
			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(mbAltaMedica.vRcCon);
			Serrores.vAyuda = Serrores.ObtenerFicheroAyuda();
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref mbAltaMedica.vRcCon);
			//camaya todo_x_4
            //mbAltaMedica.proCabCrystal();
			//datos de amovimie
			proAmovimie(iAepisodio, lNepisodio, ref stCama, ref stDoctor, ref stServicio);
			//datos de amovifin
			proAmovifin(iAepisodio, lNepisodio, ref stSociedad, ref stNafil, ref iSoc);
			//datos de diagnosticos,motalta,dentipac
			proDiag(iAepisodio, lNepisodio, ref stDiagEntrada, ref stDiagSalida, ref stMotalta, iSoc, ref stInspecc, ref stNEntrada, ref stNSalida, ref stFEntrada, ref stFSalida, ref stRevisio, ref stGidenpac, ref stHistoria);


            //UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //camaya todo_x_4
            //cPrint.ReportFileName = "" + PathString + "\\rpt\\agi561r1.rpt";

            //UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //camaya todo_x_4
            //cPrint.Connect = "DSN=" + stDSN + ";UID=" + gUsuario + ";PWD=" + gContrase�a + "";

			StSql = "SELECT DPACIENT.gidenpac, DPACIENT.dnombpac," + " DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.fnacipac, " + " DPACIENT.itipsexo, DPACIENT.ntelefo1,DESTACIV.destaciv " + " From  DPACIENT LEFT JOIN DESTACIV ON  " + " DPACIENT.gestaciv = DESTACIV.gestaciv " + " where gidenpac='" + stGidenpac + "'";

            //UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.SQLQuery = StSql;
            //UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.SubreportToChange = "LogotipoIDC";
            //UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Connect = "DSN=" + stDSN + ";UID=" + gUsuario + ";PWD=" + gContrase�a + "";
            //UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.SubreportToChange = "";



            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["CAB1"] = "\"" + mbAltaMedica.VstrCrystal.VstGrupoHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["CAB2"] = "\"" + mbAltaMedica.VstrCrystal.VstNombreHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["CAB3"] = "\"" + mbAltaMedica.VstrCrystal.VstDireccHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["CAMA"] = "\"" + stCama + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["DIAGENTRADA"] = "\"" + stDiagEntrada + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["DIAGSALIDA"] = "\"" + stDiagSalida + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["HISTORIA"] = "\"" + stHistoria + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["MEDICO"] = "\"" + stDoctor + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["MOTALTA"] = "\"" + stMotalta + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["SERVICIO"] = "\"" + stServicio + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["SOCIEDAD"] = "\"" + stSociedad + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas[INSPECCION] = "\"" + stInspecc + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["NENTRADA"] = "\"" + stNEntrada + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["NSALIDA"] = "\"" + stNSalida + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["FLLEGADA"] = "\"" + stFEntrada + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["FSALIDA"] = "\"" + stFSalida + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["NUEVO"] = "\"" + stRevisio + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["LogoIDC"] = "\"" + Serrores.ExisteLogo(mbAltaMedica.vRcCon) + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Formulas["NAFILIAC"] = "\"" + stNafil.Trim() + "\"";

            // David 03/05/2004

            proFecha(stGidenpac);
            //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //camaya todo_x_4
            //cPrint.Formulas["EDAD"] = "\"" + Serrores.CalcularEdad(dtFechaNacimiento, dtFechaSalida, mbAltaMedica.vRcCon, Serrores.CampoIdioma) + "\"";

            // Fin de David 03/05/2004

            //cPrint.WindowTitle = "Alta M�dica"
            //UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //camaya todo_x_4
            //cPrint.WindowTitle = Serrores.VarRPT[3];

            //UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //camaya todo_x_4
            //cPrint.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
            //UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.Destination = (int) Crystal.DestinationConstants.crptToPrinter;
            //UPGRADE_TODO: (1067) Member WindowShowPrintSetupBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cPrint.WindowShowPrintSetupBtn = true;
            Serrores.LLenarFormulas(mbAltaMedica.vRcCon, cPrint, "AGI561R1", "AGI561R1"); //SI ES RPT PARA SUBREPORT LE MANDO EL NOMBRE DEL RPT

			//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//cPrint.Action = 1;
			//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//cPrint.PrinterStopPage = cPrint.PageCount;

			//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//cPrint.Reset();
			//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//cPrint.PrinterStopPage = 0;

			return;
		}

		private void proAmovimie(int iAno, int lNum, ref string Cama, ref string Medico, ref string Servicio)
		{
			string stAmov = "select gplantor, ghabitor, gcamaori," + " dnompers,dap1pers, dap2pers, " + " dnomserv,DNOMSERVI1,DNOMSERVI2 " + " from (amovimie inner join dservici on amovimie.gservior= dservici.gservici) " + " inner join dpersona on amovimie.gmedicor = dpersona.gpersona " + " Where ganoregi= " + iAno.ToString() + " and gnumregi=" + lNum.ToString() + " order by fmovimie desc"; // para coger el ultimo servicio,cama y persona.
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stAmov, mbAltaMedica.vRcCon);
			DataSet tRrAmov = new DataSet();
			tempAdapter.Fill(tRrAmov);
			if (tRrAmov.Tables[0].Rows.Count != 0)
			{
				
				Cama = StringsHelper.Format(tRrAmov.Tables[0].Rows[0]["gplantor"], "00") + StringsHelper.Format(tRrAmov.Tables[0].Rows[0]["ghabitor"], "00") + Convert.ToString(tRrAmov.Tables[0].Rows[0]["gcamaori"]);
							
				Medico = Convert.ToString(tRrAmov.Tables[0].Rows[0]["dap1pers"]).Trim() + " " + ((Convert.IsDBNull(tRrAmov.Tables[0].Rows[0]["dap2pers"])) ? "" : Convert.ToString(tRrAmov.Tables[0].Rows[0]["dap2pers"]).Trim()) + ", " + Convert.ToString(tRrAmov.Tables[0].Rows[0]["dnompers"]).Trim();
				if (Serrores.CampoIdioma == "IDIOMA0")
				{					
					Servicio = Convert.ToString(tRrAmov.Tables[0].Rows[0]["dnomserv"]).Trim();
				}
				else if (Serrores.CampoIdioma == "IDIOMA1")
				{ 				
					Servicio = (Convert.IsDBNull(tRrAmov.Tables[0].Rows[0]["DNOMSERVI1"])) ? "" : Convert.ToString(tRrAmov.Tables[0].Rows[0]["dnomservI1"]).Trim();
				}
				else if (Serrores.CampoIdioma == "IDIOMA2")
				{ 				
					Servicio = (Convert.IsDBNull(tRrAmov.Tables[0].Rows[0]["DNOMSERVI2"])) ? "" : Convert.ToString(tRrAmov.Tables[0].Rows[0]["dnomservI2"]).Trim();
				}
			}
            			
			tRrAmov.Close();
		}

		private void proAmovifin(int iAno, int lNum, ref string Sociedad, ref string Nafiliac, ref int CodSoc)
		{
			string stAmov = "select dsocieda,nafiliac,amovifin.gsocieda" + " from (amovifin inner join dsocieda on " + " amovifin.gsocieda= dsocieda.gsocieda) " + " Where ganoregi= " + iAno.ToString() + " and gnumregi=" + lNum.ToString() + " order by fmovimie";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stAmov, mbAltaMedica.vRcCon);
			DataSet tRrAmov = new DataSet();
			tempAdapter.Fill(tRrAmov);
			if (tRrAmov.Tables[0].Rows.Count != 0)
			{				
				Nafiliac = Convert.ToString(tRrAmov.Tables[0].Rows[0]["nafiliac"]);				
				Sociedad = Convert.ToString(tRrAmov.Tables[0].Rows[0]["dsocieda"]);				
				CodSoc = Convert.ToInt32(tRrAmov.Tables[0].Rows[0]["gsocieda"]);
			}
			
			tRrAmov.Close();

		}
		private void proDiag(int iAno, int lNum, ref string codEnt, ref string codSal, ref string Motalta, int Soc, ref string Inspecc, ref string NEntrada, ref string NSalida, ref string stFllegada, ref string stFSalida, ref string stRevisio, ref string gidenpac, ref string historia)
		{
			DataSet tRrDiag = null;
			//con el episodio saco la fecha,los diagnosticos y el motivo de alta
			string stAmov = "select ganoadme,gnumadme,fllegada, gmotalta, gdiagini, odiagini,faltplan, " + " gdiagalt, odiagalt,nanoadms,nnumadms,irevisio,gidenpac " + " from aepisadm " + " Where ganoadme= " + iAno.ToString() + " and gnumadme=" + lNum.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stAmov, mbAltaMedica.vRcCon);
			DataSet tRrAmov = new DataSet();
			tempAdapter.Fill(tRrAmov);
			//cosas de aepisadm
			
			gidenpac = Convert.ToString(tRrAmov.Tables[0].Rows[0]["gidenpac"]);			
			NEntrada = Conversion.Str((1000000 * Convert.ToDouble(tRrAmov.Tables[0].Rows[0]["ganoadme"])) + Convert.ToDouble(tRrAmov.Tables[0].Rows[0]["gnumadme"]));
			stFllegada = Convert.ToDateTime(tRrAmov.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm");
			
			
			if (!Convert.IsDBNull(tRrAmov.Tables[0].Rows[0]["faltplan"]))
			{				
				NSalida = Conversion.Str((1000000 * Convert.ToDouble(tRrAmov.Tables[0].Rows[0]["nanoadms"])) + Convert.ToDouble(tRrAmov.Tables[0].Rows[0]["nnumadms"]));				
				stFSalida = Convert.ToDateTime(tRrAmov.Tables[0].Rows[0]["faltplan"]).ToString("dd/MM/yyyy HH:mm");

				// David 03/05/2004				
				dtFechaSalida = Convert.ToDateTime(tRrAmov.Tables[0].Rows[0]["faltplan"]);

				// Fin de David 03/05/2004
			}
			
			if (Convert.ToString(tRrAmov.Tables[0].Rows[0]["irevisio"]) == "S")
			{
				//stRevisio = "Revisi�n"
				stRevisio = Serrores.VarRPT[1];
			}
			else
			{
				//    stRevisio = "Nuevo"
				stRevisio = Serrores.VarRPT[2];
			}

			//compruebo que tiene diagnosticos codificados
			//entrada				
			if (!Convert.IsDBNull(tRrAmov.Tables[0].Rows[0]["gdiagini"]))
			{
				//UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx				
				object tempRefParam = tRrAmov.Tables[0].Rows[0]["fllegada"];
				object tempRefParam2 = tRrAmov.Tables[0].Rows[0]["fllegada"];
				stAmov = "select  dnombdia,dnombdiai1,dnombdiai2 " + " from ddiagnos " + " Where gcodidia= '" + Convert.ToString(tRrAmov.Tables[0].Rows[0]["gdiagini"]).Trim() + "'" + " and finvaldi<=" + Serrores.FormatFechaHM(tempRefParam) + "" + " and (ffivaldi>=" + Serrores.FormatFechaHM(tempRefParam2) + "" + " or ffivaldi is null)";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stAmov, mbAltaMedica.vRcCon);
				tRrDiag = new DataSet();
				tempAdapter_2.Fill(tRrDiag);
				//    If Not IsNull(tRrDiag("dnombdia")) Then
				if (Serrores.CampoIdioma == "IDIOMA0")
				{				
					codEnt = (Convert.IsDBNull(tRrDiag.Tables[0].Rows[0]["dnombdia"])) ? "" : Convert.ToString(tRrDiag.Tables[0].Rows[0]["dnombdia"]).Trim();
				}
				else if (Serrores.CampoIdioma == "IDIOMA1")
				{ 				
					codEnt = (Convert.IsDBNull(tRrDiag.Tables[0].Rows[0]["dnombdiai1"])) ? "" : Convert.ToString(tRrDiag.Tables[0].Rows[0]["dnombdiai1"]).Trim();
				}
				else if (Serrores.CampoIdioma == "IDIOMA2")
				{ 					
					codEnt = (Convert.IsDBNull(tRrDiag.Tables[0].Rows[0]["dnombdiai2"])) ? "" : Convert.ToString(tRrDiag.Tables[0].Rows[0]["dnombdiai2"]).Trim();
				}
				//        codEnt = Trim(tRrDiag("dnombdia"))
				//    End If
				
				tRrDiag.Close();
			}
			else
			{				
				codEnt = (Convert.ToString(tRrAmov.Tables[0].Rows[0]["odiagini"]) + "").Trim();
			}
			//salida		
			
			if (!Convert.IsDBNull(tRrAmov.Tables[0].Rows[0]["gdiagalt"]))
			{				
				if (Convert.IsDBNull(tRrAmov.Tables[0].Rows[0]["faltplan"]))
				{					
					object tempRefParam3 = DateTime.Now;
					object tempRefParam4 = DateTime.Now;
					stAmov = "select  dnombdia,dnombdiai1,dnombdiai2 " + " from ddiagnos " + " Where gcodidia= '" + Convert.ToString(tRrAmov.Tables[0].Rows[0]["gdiagalt"]).Trim() + "'" + " and finvaldi<=" + Serrores.FormatFechaHM(tempRefParam3) + "" + " and (ffivaldi>=" + Serrores.FormatFechaHM(tempRefParam4) + "" + " or ffivaldi is null)";
				}
				else
				{					
					object tempRefParam5 = tRrAmov.Tables[0].Rows[0]["faltplan"];
					object tempRefParam6 = tRrAmov.Tables[0].Rows[0]["faltplan"];
					stAmov = "select  dnombdia,dnombdiai1,dnombdiai2 " + " from ddiagnos " + " Where gcodidia= '" + Convert.ToString(tRrAmov.Tables[0].Rows[0]["gdiagalt"]).Trim() + "'" + " and finvaldi<=" + Serrores.FormatFechaHM(tempRefParam5) + "" + " and (ffivaldi>=" + Serrores.FormatFechaHM(tempRefParam6) + "" + " or ffivaldi is null)";
				}
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stAmov, mbAltaMedica.vRcCon);
				tRrDiag = new DataSet();
				tempAdapter_3.Fill(tRrDiag);
				if (Serrores.CampoIdioma == "IDIOMA0")
				{				
					codSal = (Convert.IsDBNull(tRrDiag.Tables[0].Rows[0]["dnombdia"])) ? "" : Convert.ToString(tRrDiag.Tables[0].Rows[0]["dnombdia"]).Trim();
				}
				else if (Serrores.CampoIdioma == "IDIOMA1")
				{ 					
					codSal = (Convert.IsDBNull(tRrDiag.Tables[0].Rows[0]["dnombdiai1"])) ? "" : Convert.ToString(tRrDiag.Tables[0].Rows[0]["dnombdiai1"]).Trim();
				}
				else if (Serrores.CampoIdioma == "IDIOMA2")
				{ 					
					codSal = (Convert.IsDBNull(tRrDiag.Tables[0].Rows[0]["dnombdiai2"])) ? "" : Convert.ToString(tRrDiag.Tables[0].Rows[0]["dnombdiai2"]).Trim();
				}
				//    If Not IsNull(tRrDiag("dnombdia")) Then
				//        codSal = Trim(tRrDiag("dnombdia"))
				//    End If				
				tRrDiag.Close();
			}
			else
			{				
				codSal = (Convert.ToString(tRrAmov.Tables[0].Rows[0]["odiagalt"]) + "").Trim();
			}
			//motivo de alta			
			
			if (!Convert.IsDBNull(tRrAmov.Tables[0].Rows[0]["faltplan"]))
			{
				//compruebo la fecha de borrado, si lo cumple es esa
							
				object tempRefParam7 = tRrAmov.Tables[0].Rows[0]["faltplan"];
				stAmov = "select  dmotalta,dmotaltai1,dmotaltai2 " + " from dmotalta " + " Where gmotalta= " + Convert.ToString(tRrAmov.Tables[0].Rows[0]["gmotalta"]) + " and (fborrado is null or fborrado <=" + Serrores.FormatFecha(tempRefParam7) + ") " + " order by fborrado desc";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stAmov, mbAltaMedica.vRcCon);
				tRrDiag = new DataSet();
				tempAdapter_4.Fill(tRrDiag);
				if (Serrores.CampoIdioma == "IDIOMA0")
				{				
					Motalta = (Convert.IsDBNull(tRrDiag.Tables[0].Rows[0]["dmotalta"])) ? "" : Convert.ToString(tRrDiag.Tables[0].Rows[0]["dmotalta"]).Trim();
				}
				else if (Serrores.CampoIdioma == "IDIOMA1")
				{ 				
					Motalta = (Convert.IsDBNull(tRrDiag.Tables[0].Rows[0]["dmotaltai1"])) ? "" : Convert.ToString(tRrDiag.Tables[0].Rows[0]["dmotaltai1"]).Trim();
				}
				else if (Serrores.CampoIdioma == "IDIOMA2")
				{ 					
					Motalta = (Convert.IsDBNull(tRrDiag.Tables[0].Rows[0]["dmotaltai2"])) ? "" : Convert.ToString(tRrDiag.Tables[0].Rows[0]["dmotaltai2"]).Trim();
				}
				//    If Not tRrAmov.EOF Then
				//        Motalta = tRrDiag("dmotalta")
				//    End If
				
				tRrDiag.Close();
			}
			//la inspeccion
			
			stAmov = "select dinspecc " + " from (dentipac inner join dinspecc on " + " dentipac.ginspecc= dinspecc.ginspecc) " + " Where dentipac.gidenpac='" + Convert.ToString(tRrAmov.Tables[0].Rows[0]["gidenpac"]) + "'" + " and dentipac.gsocieda=" + Soc.ToString();

			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stAmov, mbAltaMedica.vRcCon);
			tRrAmov = new DataSet();
			tempAdapter_5.Fill(tRrAmov);
			if (tRrAmov.Tables[0].Rows.Count != 0)
			{			
				Inspecc = Convert.ToString(tRrAmov.Tables[0].Rows[0]["dinspecc"]);
			}			
			tRrAmov.Close();
			//la historia
			stAmov = "select ghistoria " + " from hdossier " + " Where gidenpac='" + gidenpac + "'";
			SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stAmov, mbAltaMedica.vRcCon);
			tRrAmov = new DataSet();
			tempAdapter_6.Fill(tRrAmov);
			if (tRrAmov.Tables[0].Rows.Count != 0)
			{				
				historia = Convert.ToString(tRrAmov.Tables[0].Rows[0]["ghistoria"]);
			}
			
			tRrAmov.Close();

		}

		// David 03/05/2004
		private void proFecha(string g)
		{

			string StSql = "SELECT DPACIENT.fnacipac from DPACIENT where gidenpac = '" + g + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, mbAltaMedica.vRcCon);
			DataSet tRrFecha = new DataSet();
			tempAdapter.Fill(tRrFecha);

			if (tRrFecha.Tables[0].Rows.Count != 0)
			{				
				dtFechaNacimiento = Convert.ToDateTime(tRrFecha.Tables[0].Rows[0]["fnacipac"]);
			}
			
			tRrFecha.Close();

			// Si no hay fecha de alta, usamos la actual
			if (dtFechaSalida == DateTime.Parse("00:00:00"))
			{
				dtFechaSalida = DateTime.Now;
			}
		}
	}
}