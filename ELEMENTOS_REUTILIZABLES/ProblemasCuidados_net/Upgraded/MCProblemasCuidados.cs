using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Microsoft.CSharp;

namespace ProblemasCuidados
{
	public class MCProblemasCuidados
	{
        
		private void A�adirCuidadosProblemas(string stCodigoProblema, System.DateTime dtFechaApunte, string Tabla, string CodigoIntervencion)
		{
			int irespuesta = 0;
			StringBuilder sql = new StringBuilder();
			DataSet RrSql = null;
			DataSet RrSql1 = null;
			bool fGrabarCuidado = false;
			bool fSiguienteValidacion = false;
			string stSiPrecisa = String.Empty;
			string stDosisUnica = String.Empty;

			try
			{
				//Buscamos los cuidados asociados al problema
				//Jes�s 03/06/2008
				//A�adimos que se carguen los de d�sis �nica y si precisa
				sql = new StringBuilder("Select ECODCUID.* ");
				sql.Append("From " + Tabla + " , ECODCUID ");
				if (Tabla == "ECUIPROB")
				{
					sql.Append("where ECUIPROB.gprobenf = '" + stCodigoProblema + "' ");
				}
				else
				{
					sql.Append("where EINTVCUI.ginterve = '" + CodigoIntervencion + "' ");
				}
				//sql = sql & "where ECUIPROB.gprobenf = '" & stCodigo & "' "
				sql.Append("and ECODCUID.gcuidenf = " + Tabla + ".gcuidenf ");
				sql.Append("and ((ECODCUID.vpercuid is not null ");
				sql.Append("and ECODCUID.iuniperi is not null) ");
				sql.Append("or ECODCUID.idounica = 'S' ");
				sql.Append("or ECODCUID.iprecisa = 'S') ");
				sql.Append("and ECODCUID.gcuidenf = " + Tabla + ".gcuidenf ");
				sql.Append("and ECODCUID.fborrado is null ");

				//por cada uno de los cuidados que se recogen se tiene que insertar en la tabla de ECUIDADO
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql.ToString(), BProblemasCuidados.RcValoracion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
				
					while(RrSql.Tables[0].Rows.Count != 0)
					{ //por cada uno de los cuidados del problema
					
						
						if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniperi"]) || Convert.IsDBNull(RrSql.Tables[0].Rows[0]["vpercuid"]))
						{
						
							if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iprecisa"]))
							{
								stSiPrecisa = "N";
							}
							else
							{
								
								if (Convert.ToString(RrSql.Tables[0].Rows[0]["iprecisa"]).Trim().ToUpper() != "S")
								{
									stSiPrecisa = "N";
								}
								else
								{
									stSiPrecisa = "S";
								}
							}
						
							
							if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["idounica"]))
							{
								stDosisUnica = "N";
							}
							else
							{
							
								if (Convert.ToString(RrSql.Tables[0].Rows[0]["idounica"]).Trim().ToUpper() != "S")
								{
									stDosisUnica = "N";
								}
								else
								{
									stDosisUnica = "S";
								}
							}
						}
						else
						{
							stSiPrecisa = "N";
							stDosisUnica = "N";
						}
						//Prevalece la opci�n si precisa sobre dosis �nica
						if (stSiPrecisa == "S")
						{
							stDosisUnica = "N";
						}
						fSiguienteValidacion = true;
						
					
						if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniperi"]) || Convert.IsDBNull(RrSql.Tables[0].Rows[0]["vpercuid"]))
						{
							if (stSiPrecisa == "N" && stDosisUnica == "N")
							{
								fSiguienteValidacion = false;
							}
						}

						//comprobamos si iuniperi es "D"  o "V" o es dosis unica o es si precisa
						//hay que comprobar si algunos de los check del turno
						//estan seleccionados
						if (fSiguienteValidacion)
						{
							
							if (Convert.ToString(RrSql.Tables[0].Rows[0]["iuniperi"]).ToUpper() == "D" || Convert.ToString(RrSql.Tables[0].Rows[0]["iuniperi"]).ToUpper() == "V" || stSiPrecisa == "S" || stDosisUnica == "S")
							{
							
								if (Convert.ToString(RrSql.Tables[0].Rows[0]["ima�ana"]).ToUpper() == "S")
								{
									//continuo
									fSiguienteValidacion = true;
								}
								else if (Convert.ToString(RrSql.Tables[0].Rows[0]["itarde"]).ToUpper() == "S")
								{ 
									//continuo
									fSiguienteValidacion = true;
								}
								else if (Convert.ToString(RrSql.Tables[0].Rows[0]["inoche"]).ToUpper() == "S")
								{ 
									//continuo
									fSiguienteValidacion = true;
								}
								else
								{
									//si no es ninguno de ellos muevo al siguiente
									fSiguienteValidacion = false;
								}
							}
							else
							{
								fSiguienteValidacion = true;
							}
						}
						//hay que validar que no se hayan registrado ya
						if (fSiguienteValidacion)
						{
							fGrabarCuidado = true;
							sql = new StringBuilder("select * from ECUIDADO ");
							sql.Append(" Where itiposer = '" + BProblemasCuidados.GstTiposervicio + "' ");
							sql.Append(" and ganoregi = " + BProblemasCuidados.GiA�oRegistro.ToString() + " and gnumregi = " + BProblemasCuidados.GiNumeroRegistro.ToString() + " ");
							//sql = sql & " and fapuntes = " & FormatFechaHMS(dtFechaApunte) & " "
							
							sql.Append(" and gcuidado = '" + Convert.ToString(RrSql.Tables[0].Rows[0]["gcuidenf"]) + "' ");
							sql.Append(" order by fapuntes desc");

							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql.ToString(), BProblemasCuidados.RcValoracion);
							RrSql1 = new DataSet();
							tempAdapter_2.Fill(RrSql1);

							if (RrSql1.Tables[0].Rows.Count != 0)
							{
								
								//si existe ya el registro
								if (Convert.IsDBNull(RrSql1.Tables[0].Rows[0]["ffintrat"]))
								{
									//no hay que hacer nada porque el cuidado ya esta registrado
									fGrabarCuidado = false;
								}
								else
								{
									//se preguntar� si se quiere a�adir
									
								
									short tempRefParam = 1125;
									string [] tempRefParam2 = new string []{"El cuidado " + Convert.ToString(RrSql.Tables[0].Rows[0]["dcuidado"]).Trim() + " ya esta finalizado ", "iniciarlo de nuevo"};
									irespuesta = Convert.ToInt32(BProblemasCuidados.clase_mensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam,  BProblemasCuidados.RcValoracion, tempRefParam2));
									if (irespuesta == 6)
									{ //si
										fGrabarCuidado = true;
									}
									else
									{
										fGrabarCuidado = false;
									}
								}
							}
							else
							{
								//si no existe lo a�ado
								fGrabarCuidado = true;
							}

							if (fGrabarCuidado)
							{
							
								RrSql1.AddNew();
								RrSql1.Tables[0].Rows[0]["itiposer"] = BProblemasCuidados.GstTiposervicio;
								RrSql1.Tables[0].Rows[0]["ganoregi"] = BProblemasCuidados.GiA�oRegistro;
								RrSql1.Tables[0].Rows[0]["gnumregi"] = BProblemasCuidados.GiNumeroRegistro;
								RrSql1.Tables[0].Rows[0]["fapuntes"] = dtFechaApunte;
								
								RrSql1.Tables[0].Rows[0]["gcuidado"] = RrSql.Tables[0].Rows[0]["gcuidenf"];
								//resto de los campos son para actualizar
								//fecha de incio de tratamiento
							
								if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniespe"]))
								{
								
									if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ctespera"]))
									{
									
										switch(Convert.ToString(RrSql.Tables[0].Rows[0]["iuniespe"]))
										{
											case "D" : 
												RrSql1.Tables[0].Rows[0]["finitrat"] = dtFechaApunte.AddDays(Conversion.Val(Convert.ToString(RrSql.Tables[0].Rows[0]["ctespera"]))); 
												break;
											case "H" : 
												RrSql1.Tables[0].Rows[0]["finitrat"] = dtFechaApunte.AddHours(Conversion.Val(Convert.ToString(RrSql.Tables[0].Rows[0]["ctespera"]))); 
												break;
										}
									}
									else
									{
										
										RrSql1.Tables[0].Rows[0]["finitrat"] = dtFechaApunte; //Fecha del apunte
									}
								}
								else
								{
									
									RrSql1.Tables[0].Rows[0]["finitrat"] = dtFechaApunte; //Fecha del apunte
								}
								//tanto el campo vpercuid como el iuniperi no deben ser nulos en este caso
								
								if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["vpercuid"]))
								{
								
									if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniperi"]))
									{
										
										if (Convert.ToString(RrSql.Tables[0].Rows[0]["iuniperi"]).Trim() == "H" || Convert.ToString(RrSql.Tables[0].Rows[0]["iuniperi"]).Trim() == "M")
										{
											
											RrSql1.Tables[0].Rows[0]["ima�ana"] = "S";
											RrSql1.Tables[0].Rows[0]["itarde"] = "S";
											RrSql1.Tables[0].Rows[0]["inoche"] = "S";
										}
										else
										{
											
											RrSql1.Tables[0].Rows[0]["ima�ana"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ima�ana"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["ima�ana"];
											
											RrSql1.Tables[0].Rows[0]["itarde"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["itarde"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["itarde"];
										
											RrSql1.Tables[0].Rows[0]["inoche"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["inoche"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["inoche"];
										}
									
										RrSql1.Tables[0].Rows[0]["vpercuid"] = RrSql.Tables[0].Rows[0]["vpercuid"];
										
										RrSql1.Tables[0].Rows[0]["iuniperi"] = RrSql.Tables[0].Rows[0]["iuniperi"];
									}
								}
								
								if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["vpercuid"]) || Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniperi"]))
								{
									
									RrSql1.Tables[0].Rows[0]["ima�ana"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ima�ana"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["ima�ana"];
									
									RrSql1.Tables[0].Rows[0]["itarde"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["itarde"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["itarde"];
									
									RrSql1.Tables[0].Rows[0]["inoche"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["inoche"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["inoche"];
								
									RrSql1.Tables[0].Rows[0]["vpercuid"] = 1;
								
									RrSql1.Tables[0].Rows[0]["iuniperi"] = "V";
								}
								
								RrSql1.Tables[0].Rows[0]["ffintrat"] = DBNull.Value;
							
								RrSql1.Tables[0].Rows[0]["gusuario"] = BProblemasCuidados.gUsuario;
							
								RrSql1.Tables[0].Rows[0]["oobserva"] = DBNull.Value;
							
								RrSql1.Tables[0].Rows[0]["iprecisa"] = stSiPrecisa;
								
								RrSql1.Tables[0].Rows[0]["idounica"] = stDosisUnica;
								//(maplaza)(26/03/2008)
								if (BProblemasCuidados.GstPlanCuidado.Trim() != "")
								{
									
									RrSql1.Tables[0].Rows[0]["gplantip"] = BProblemasCuidados.GstPlanCuidado.Trim();
								}
								else
								{
									
									RrSql1.Tables[0].Rows[0]["gplantip"] = DBNull.Value;
								}
							
								RrSql1.Tables[0].Rows[0]["gprobenf"] = stCodigoProblema;
								//--------------
								
								string tempQuery = RrSql1.Tables[0].TableName;
								SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
								SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
								tempAdapter_3.Update(RrSql1, RrSql1.Tables[0].TableName);
							}
							else
							{
								//Si fGrabarCuidado = false
							}
							//UPGRADE_ISSUE: (1040) MoveNext function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
							
						}
						else
						{
							//tenemos que pasar al siguiente cuidado
						
							
						}
					};

				}

			
				RrSql.Close();
			}
			catch
			{

                BProblemasCuidados.RcValoracion.RollbackTransScope();
                BProblemasCuidados.fPantalla.ProRecibeError(true);
			}

		}

		//UPGRADE_NOTE: (7001) The following declaration (A�adirCuidadosDePlanesSinProblemas) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void A�adirCuidadosDePlanesSinProblemas(System.DateTime dtFechaApunte)
		//{
				//int irespuesta = 0;
				////UPGRADE_ISSUE: (2068) RDO._rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				//UpgradeStubs.RDO__rdoColumn stCodigoProblema = null;
				//StringBuilder sql = new StringBuilder();
				//DataSet RrSql = null;
				//DataSet RrSql1 = null;
				//bool fGrabarCuidado = false;
				//bool fSiguienteValidacion = false;
				//string stSiPrecisa = String.Empty;
				//string stDosisUnica = String.Empty;
				//
				//try
				//{
					//Buscamos los cuidados asociados al plan sin problama asociado que no est�n ya activos.
					//sql = new StringBuilder("SELECT EPLACUSP.GCUIDENF,DCUIDADO,vpercuid,iuniperi,ima�ana,itarde,inoche,idounica,iprecisa,iuniespe,ctespera FROM EPLACUSP INNER JOIN ECODCUID ");
					//sql.Append("ON EPLACUSP.GCUIDENF = ECODCUID.GCUIDENF where ECODCUID.fborrado is null and ((vpercuid IS NOT NULL AND iuniperi IS NOT NULL) or idounica = 'S' or iprecisa = 'S') AND ");
					//sql.Append("EPLACUSP.GPLANTIP in ('" + BProblemasCuidados.GstPlanCuidado + "') AND not exists (");
					//sql.Append("select '' from ECUIDADO where itiposer='" + BProblemasCuidados.GstTiposervicio + "' and ganoregi=" + BProblemasCuidados.GiA�oRegistro.ToString() + "  and gnumregi=" + BProblemasCuidados.GiNumeroRegistro.ToString() + " and ffintrat is null and ");
					//sql.Append("EPLACUSP.GCUIDENF = ECUIDADO.GCUIDADO )");
					//
					//por cada uno de los cuidados que se recogen se tiene que insertar en la tabla de ECUIDADO
					//SqlDataAdapter tempAdapter = new SqlDataAdapter(sql.ToString(), BProblemasCuidados.RcValoracion);
					//RrSql = new DataSet();
					//tempAdapter.Fill(RrSql);
					//
					//if (RrSql.Tables[0].Rows.Count != 0)
					//{
						////UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
						//RrSql.MoveFirst();
						//
						//while(RrSql.Tables[0].Rows.Count != 0)
						//{ //por cada uno de los cuidados del problema
							////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							//if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniperi"]) || Convert.IsDBNull(RrSql.Tables[0].Rows[0]["vpercuid"]))
							//{
								////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
								//if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iprecisa"]))
								//{
									//stSiPrecisa = "N";
								//}
								//else
								//{
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//if (Convert.ToString(RrSql.Tables[0].Rows[0]["iprecisa"]).Trim().ToUpper() != "S")
									//{
										//stSiPrecisa = "N";
									//}
									//else
									//{
										//stSiPrecisa = "S";
									//}
								//}
								////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
								//if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["idounica"]))
								//{
									//stDosisUnica = "N";
								//}
								//else
								//{
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//if (Convert.ToString(RrSql.Tables[0].Rows[0]["idounica"]).Trim().ToUpper() != "S")
									//{
										//stDosisUnica = "N";
									//}
									//else
									//{
										//stDosisUnica = "S";
									//}
								//}
							//}
							//else
							//{
								//stSiPrecisa = "N";
								//stDosisUnica = "N";
							//}
							//Prevalece la opci�n si precisa sobre dosis �nica
							//if (stSiPrecisa == "S")
							//{
								//stDosisUnica = "N";
							//}
							//fSiguienteValidacion = true;
							////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							//if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniperi"]) || Convert.IsDBNull(RrSql.Tables[0].Rows[0]["vpercuid"]))
							//{
								//if (stSiPrecisa == "N" && stDosisUnica == "N")
								//{
									//fSiguienteValidacion = false;
								//}
							//}
							//
							//comprobamos si iuniperi es "D"  o "V" o es dosis unica o es si precisa
							//hay que comprobar si algunos de los check del turno
							//estan seleccionados
							//if (fSiguienteValidacion)
							//{
								////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								//if (Convert.ToString(RrSql.Tables[0].Rows[0]["iuniperi"]).ToUpper() == "D" || Convert.ToString(RrSql.Tables[0].Rows[0]["iuniperi"]).ToUpper() == "V" || stSiPrecisa == "S" || stDosisUnica == "S")
								//{
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//if (Convert.ToString(RrSql.Tables[0].Rows[0]["ima�ana"]).ToUpper() == "S")
									//{
										//continuo
										//fSiguienteValidacion = true;
									//}
									//else if (Convert.ToString(RrSql.Tables[0].Rows[0]["itarde"]).ToUpper() == "S")
									//{ 
										//continuo
										//fSiguienteValidacion = true;
									//}
									//else if (Convert.ToString(RrSql.Tables[0].Rows[0]["inoche"]).ToUpper() == "S")
									//{ 
										//continuo
										//fSiguienteValidacion = true;
									//}
									//else
									//{
										//si no es ninguno de ellos muevo al siguiente
										//fSiguienteValidacion = false;
									//}
								//}
								//else
								//{
									//fSiguienteValidacion = true;
								//}
							//}
							//hay que validar que no se hayan registrado ya
							//if (fSiguienteValidacion)
							//{
								//fGrabarCuidado = true;
								//sql = new StringBuilder("select * from ECUIDADO ");
								//sql.Append(" Where itiposer = '" + BProblemasCuidados.GstTiposervicio + "' ");
								//sql.Append(" and ganoregi = " + BProblemasCuidados.GiA�oRegistro.ToString() + " and gnumregi = " + BProblemasCuidados.GiNumeroRegistro.ToString() + " ");
								//sql = sql & " and fapuntes = " & FormatFechaHMS(dtFechaApunte) & " "
								////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								//sql.Append(" and gcuidado = '" + Convert.ToString(RrSql.Tables[0].Rows[0]["gcuidenf"]) + "' ");
								//sql.Append(" order by fapuntes desc");
								//
								//SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql.ToString(), BProblemasCuidados.RcValoracion);
								//RrSql1 = new DataSet();
								//tempAdapter_2.Fill(RrSql1);
								//
								//if (RrSql1.Tables[0].Rows.Count != 0)
								//{
									////UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
									//RrSql1.MoveFirst();
									//si existe ya el registro
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
									//if (Convert.IsDBNull(RrSql1.Tables[0].Rows[0]["ffintrat"]))
									//{
										//no hay que hacer nada porque el cuidado ya esta registrado
										//fGrabarCuidado = false;
									//}
									//else
									//{
										//se preguntar� si se quiere a�adir
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										////UPGRADE_WARNING: (1068) clase_mensaje.RespuestaMensaje() of type Variant is being forced to int. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
										//short tempRefParam = 1125;
										//object tempRefParam2 = new object{"El cuidado " + Convert.ToString(RrSql.Tables[0].Rows[0]["dcuidado"]).Trim() + " ya esta finalizado ", "iniciarlo de nuevo"};
										//irespuesta = Convert.ToInt32(BProblemasCuidados.clase_mensaje.RespuestaMensaje(ref Serrores.vNomAplicacion, ref Serrores.vAyuda, ref tempRefParam, ref BProblemasCuidados.RcValoracion, ref tempRefParam2));
										//if (irespuesta == 6)
										//{ //si
											//fGrabarCuidado = true;
										//}
										//else
										//{
											//fGrabarCuidado = false;
										//}
									//}
								//}
								//else
								//{
									//si no existe lo a�ado
									//fGrabarCuidado = true;
								//}
								//
								//if (fGrabarCuidado)
								//{
									////UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql1.AddNew was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									//RrSql1.AddNew();
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//RrSql1.Tables[0].Rows[0]["itiposer"] = BProblemasCuidados.GstTiposervicio;
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//RrSql1.Tables[0].Rows[0]["ganoregi"] = BProblemasCuidados.GiA�oRegistro;
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//RrSql1.Tables[0].Rows[0]["gnumregi"] = BProblemasCuidados.GiNumeroRegistro;
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//RrSql1.Tables[0].Rows[0]["fapuntes"] = dtFechaApunte;
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//RrSql1.Tables[0].Rows[0]["gcuidado"] = RrSql.Tables[0].Rows[0]["gcuidenf"];
									//resto de los campos son para actualizar
									//fecha de incio de tratamiento
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
									//if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniespe"]))
									//{
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
										//if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ctespera"]))
										//{
											////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
											//switch(Convert.ToString(RrSql.Tables[0].Rows[0]["iuniespe"]))
											//{
												//case "D" : 
													////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
													//RrSql1.Tables[0].Rows[0]["finitrat"] = dtFechaApunte.AddDays(Conversion.Val(Convert.ToString(RrSql.Tables[0].Rows[0]["ctespera"]))); 
													//break;
												//case "H" : 
													////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
													//RrSql1.Tables[0].Rows[0]["finitrat"] = dtFechaApunte.AddHours(Conversion.Val(Convert.ToString(RrSql.Tables[0].Rows[0]["ctespera"]))); 
													//break;
											//}
										//}
										//else
										//{
											////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
											//RrSql1.Tables[0].Rows[0]["finitrat"] = dtFechaApunte; //Fecha del apunte
										//}
									//}
									//else
									//{
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										//RrSql1.Tables[0].Rows[0]["finitrat"] = dtFechaApunte; //Fecha del apunte
									//}
									//tanto el campo vpercuid como el iuniperi no deben ser nulos en este caso
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
									//if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["vpercuid"]))
									//{
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
										//if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniperi"]))
										//{
											////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
											//if (Convert.ToString(RrSql.Tables[0].Rows[0]["iuniperi"]).Trim() == "H" || Convert.ToString(RrSql.Tables[0].Rows[0]["iuniperi"]).Trim() == "M")
											//{
												////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
												//RrSql1.Tables[0].Rows[0]["ima�ana"] = "S";
												////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
												//RrSql1.Tables[0].Rows[0]["itarde"] = "S";
												////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
												//RrSql1.Tables[0].Rows[0]["inoche"] = "S";
											//}
											//else
											//{
												////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
												////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
												//RrSql1.Tables[0].Rows[0]["ima�ana"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ima�ana"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["ima�ana"];
												////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
												////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
												//RrSql1.Tables[0].Rows[0]["itarde"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["itarde"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["itarde"];
												////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
												////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
												//RrSql1.Tables[0].Rows[0]["inoche"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["inoche"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["inoche"];
											//}
											////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
											//RrSql1.Tables[0].Rows[0]["vpercuid"] = RrSql.Tables[0].Rows[0]["vpercuid"];
											////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
											//RrSql1.Tables[0].Rows[0]["iuniperi"] = RrSql.Tables[0].Rows[0]["iuniperi"];
										//}
									//}
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
									//if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["vpercuid"]) || Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iuniperi"]))
									//{
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
										//RrSql1.Tables[0].Rows[0]["ima�ana"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ima�ana"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["ima�ana"];
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
										//RrSql1.Tables[0].Rows[0]["itarde"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["itarde"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["itarde"];
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
										//RrSql1.Tables[0].Rows[0]["inoche"] = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["inoche"])) ? DBNull.Value : RrSql.Tables[0].Rows[0]["inoche"];
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										//RrSql1.Tables[0].Rows[0]["vpercuid"] = 1;
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										//RrSql1.Tables[0].Rows[0]["iuniperi"] = "V";
									//}
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
									//RrSql1.Tables[0].Rows[0]["ffintrat"] = DBNull.Value;
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//RrSql1.Tables[0].Rows[0]["gusuario"] = BProblemasCuidados.gUsuario;
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
									//RrSql1.Tables[0].Rows[0]["oobserva"] = DBNull.Value;
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//RrSql1.Tables[0].Rows[0]["iprecisa"] = stSiPrecisa;
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//RrSql1.Tables[0].Rows[0]["idounica"] = stDosisUnica;
									//(maplaza)(26/03/2008)
									//if (BProblemasCuidados.GstPlanCuidado.Trim() != "")
									//{
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										//RrSql1.Tables[0].Rows[0]["gplantip"] = BProblemasCuidados.GstPlanCuidado.Trim();
									//}
									//else
									//{
										////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
										////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
										//RrSql1.Tables[0].Rows[0]["gplantip"] = DBNull.Value;
									//}
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//RrSql1.Tables[0].Rows[0]["gprobenf"] = stCodigoProblema;
									//--------------
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//string tempQuery = RrSql1.Tables(0).TableName;
									//SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
									//SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
									//tempAdapter_3.Update(RrSql1, RrSql1.Tables(0).TableName);
								//}
								//else
								//{
									//Si fGrabarCuidado = false
								//}
								////UPGRADE_ISSUE: (1040) MoveNext function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
								//RrSql.MoveNext();
							//}
							//else
							//{
								//tenemos que pasar al siguiente cuidado
								////UPGRADE_ISSUE: (1040) MoveNext function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
								//RrSql.MoveNext();
							//}
						//};
						//
					//}
					//
					////UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//RrSql.Close();
				//}
				//catch
				//{
					//
					////UPGRADE_ISSUE: (2064) RDO.rdoConnection method RcValoracion.RollbackTrans was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
					////UPGRADE_TODO: (1067) Member ProRecibeError is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					//BProblemasCuidados.fPantalla.ProRecibeError(true);
				//}
				//
		//}

		//(maplaza)(26/03/2008)Se a�ade el argumento opcional "stCodPlan", que indica si hay que guardar el plan en la tabla
		//de cuidados del paciente (ECUIDADO)
		public void Llamada(object Formulario, SqlConnection nConnet, string stEpisodio, string stCodigoProblema, System.DateTime dtFechaApunte, string stUsuario, string stCodPlan, object Tabla_optional, string stCodigoIntervencion, bool stCuidSinProb)
		{
			string Tabla = (Tabla_optional == Type.Missing) ? String.Empty : Tabla_optional as string;
			bool fErrorCuidados = false;
			//se cargan los valores en variables globales
			try
			{
				string vstTabla = String.Empty;
				string vstCodigoIntervencion = String.Empty;

				BProblemasCuidados.RcValoracion = nConnet;
				Serrores.AjustarRelojCliente(BProblemasCuidados.RcValoracion);
				BProblemasCuidados.fPantalla = Formulario;
				BProblemasCuidados.gUsuario = stUsuario;
				BProblemasCuidados.GstTiposervicio = stEpisodio.Substring(0, Math.Min(1, stEpisodio.Length));
				BProblemasCuidados.GiA�oRegistro = Convert.ToInt32(Double.Parse(stEpisodio.Substring(1, Math.Min(4, stEpisodio.Length - 1))));
				BProblemasCuidados.GiNumeroRegistro = Convert.ToInt32(Double.Parse(stEpisodio.Substring(5, Math.Min(stEpisodio.Length - 5, stEpisodio.Length - 5))));
				BProblemasCuidados.GstCodigoProblema = stCodigoProblema;
				BProblemasCuidados.FechaApunte = dtFechaApunte;
				//(maplaza)(26/03/2008)
				BProblemasCuidados.GstPlanCuidado = stCodPlan;
				if (Tabla_optional == Type.Missing)
				{
					//no se tranfiere el argumento
					vstTabla = "ECUIPROB";
					vstCodigoIntervencion = "";
				}
				else
				{
					vstTabla = Tabla.Trim();
					vstCodigoIntervencion = stCodigoIntervencion;
				}
				//---------

				fErrorCuidados = false;

				Serrores.Obtener_TipoBD_FormatoFechaBD(ref BProblemasCuidados.RcValoracion);

				Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(BProblemasCuidados.RcValoracion);
				//If stCuidSinProb Then
				//    A�adirCuidadosDePlanesSinProblemas FechaApunte
				//Else
				A�adirCuidadosProblemas(BProblemasCuidados.GstCodigoProblema, BProblemasCuidados.FechaApunte, vstTabla, vstCodigoIntervencion);
				//End If
			}
			catch
			{

                //se ha producido un error

             BProblemasCuidados.fPantalla.ProRecibeError(true);
            }
		}

		public void Llamada(object Formulario, SqlConnection nConnet, string stEpisodio, string stCodigoProblema, System.DateTime dtFechaApunte, string stUsuario, string stCodPlan, object Tabla_optional, string stCodigoIntervencion)
		{
			Llamada(Formulario, nConnet, stEpisodio, stCodigoProblema, dtFechaApunte, stUsuario, stCodPlan, Tabla_optional, stCodigoIntervencion, false);
		}

		public void Llamada(object Formulario, SqlConnection nConnet, string stEpisodio, string stCodigoProblema, System.DateTime dtFechaApunte, string stUsuario, string stCodPlan, object Tabla_optional)
		{
			Llamada(Formulario, nConnet, stEpisodio, stCodigoProblema, dtFechaApunte, stUsuario, stCodPlan, Tabla_optional, String.Empty, false);
		}

		public void Llamada(object Formulario, SqlConnection nConnet, string stEpisodio, string stCodigoProblema, System.DateTime dtFechaApunte, string stUsuario, string stCodPlan)
		{
			Llamada(Formulario, nConnet, stEpisodio, stCodigoProblema, dtFechaApunte, stUsuario, stCodPlan, Type.Missing, String.Empty, false);
		}

		public void Llamada(object Formulario, SqlConnection nConnet, string stEpisodio, string stCodigoProblema, System.DateTime dtFechaApunte, string stUsuario)
		{
			Llamada(Formulario, nConnet, stEpisodio, stCodigoProblema, dtFechaApunte, stUsuario, "", Type.Missing, String.Empty, false);
		}


		public MCProblemasCuidados()
		{
			try
			{
				BProblemasCuidados.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{

				MessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}
		}

		~MCProblemasCuidados()
		{
			BProblemasCuidados.clase_mensaje = null;
		}
	}
}