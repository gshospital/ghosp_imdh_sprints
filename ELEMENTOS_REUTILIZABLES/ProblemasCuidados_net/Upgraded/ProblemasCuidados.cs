using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ProblemasCuidados
{
	internal static class BProblemasCuidados
	{

		public static SqlConnection RcValoracion = null;
		public static string gUsuario = String.Empty;
		public static string GstTiposervicio = String.Empty;
		public static int GiAņoRegistro = 0;
		public static int GiNumeroRegistro = 0;
		public static string GstCodigoProblema = String.Empty;
		public static System.DateTime FechaApunte = DateTime.FromOADate(0);
		public static dynamic fPantalla = null;
        //(maplaza)(26/03/2008)

        public static string GstPlanCuidado = String.Empty;		
        
        //-----
		public static Mensajes.ClassMensajes clase_mensaje = null;

		public static string stCadenaError = String.Empty;
	}
}