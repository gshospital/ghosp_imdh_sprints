using Microsoft.VisualBasic;
using System;

namespace BusquedaCombos
{
	public class ClsBuscarElemCombos
	{


		// Para una cadena dada busca el elemento que mas se ajuste
		int UltimaSeleccion_BusquedaCombos = 0;
		string UltimoTexto_BusquedaCombos = String.Empty;
		public object BusquedaCombos(string cadena, dynamic ControlCombo)
		{
			if (cadena.ToUpper() == UltimoTexto_BusquedaCombos.Substring(0, Math.Min(cadena.Length, UltimoTexto_BusquedaCombos.Length)).ToUpper() && cadena != "")
			{
				
				ControlCombo.ListIndex = UltimaSeleccion_BusquedaCombos;
				UltimaSeleccion_BusquedaCombos = Convert.ToInt32(ControlCombo.ListIndex);
				ControlCombo.Text = cadena.ToUpper() + Convert.ToString(ControlCombo.List(UltimaSeleccion_BusquedaCombos)).Substring(cadena.Length);
				ControlCombo.SelStart = cadena.Length;
				ControlCombo.SelLength = Strings.Len(Convert.ToString(ControlCombo.Text));
				UltimoTexto_BusquedaCombos = Convert.ToString(ControlCombo.List(ControlCombo.ListIndex));
				return null;
			}
			
			if (Convert.ToDouble(ControlCombo.ListCount) == 0 || cadena == "")
			{
				UltimoTexto_BusquedaCombos = "";
				
				ControlCombo.ListIndex = (-1);
				return null;
			}
			bool bEncontrado = false;
			
			for (int ilista = 0; ilista <= Convert.ToInt32(Convert.ToDouble(ControlCombo.ListCount) - 1); ilista++)
			{
				if (cadena.ToUpper() == Convert.ToString(ControlCombo.List(ilista)).Substring(0, Math.Min(cadena.Length, Convert.ToString(ControlCombo.List(ilista)).Length)).ToUpper())
				{
					UltimaSeleccion_BusquedaCombos = ilista;
					ControlCombo.Text = cadena.ToUpper() + Convert.ToString(ControlCombo.List(ilista)).Substring(cadena.Length);
					ControlCombo.SelStart = cadena.Length;
					ControlCombo.SelLength = Strings.Len(Convert.ToString(ControlCombo.Text));
					UltimoTexto_BusquedaCombos = Convert.ToString(ControlCombo.List(ControlCombo.ListIndex));
					bEncontrado = true;
					return null;
				}
			}
			if (!bEncontrado)
			{
				//     ControlCombo.Text = Mid(ControlCombo.Text, 1, Len(ControlCombo.Text) - 1)

				ControlCombo.Text = UltimoTexto_BusquedaCombos;
				ControlCombo.ListIndex = UltimaSeleccion_BusquedaCombos;
				ControlCombo.SelStart = cadena.Length - 1;
				ControlCombo.SelLength = Strings.Len(Convert.ToString(ControlCombo.Text));
			}

			return null;
		}
	}
}