using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ECitaExterna
{
	partial class Mot_anul
	{

		#region "Upgrade Support "
		private static Mot_anul m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Mot_anul DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Mot_anul();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CbCancelar", "CBAceptar", "cbbMotAnulacion", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadButton CBAceptar;
		public Telerik.WinControls.UI.RadDropDownList cbbMotAnulacion;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mot_anul));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.CbCancelar = new Telerik.WinControls.UI.RadButton();
			this.CBAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.cbbMotAnulacion = new Telerik.WinControls.UI.RadDropDownList();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// CbCancelar
			// 
			this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCancelar.Location = new System.Drawing.Point(378, 122);
			this.CbCancelar.Name = "CbCancelar";
			this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCancelar.Size = new System.Drawing.Size(81, 31);
			this.CbCancelar.TabIndex = 3;
			this.CbCancelar.Text = "&Salir";
			this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
			// 
			// CBAceptar
			// 
			this.CBAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CBAceptar.Enabled = false;
			this.CBAceptar.Location = new System.Drawing.Point(290, 122);
			this.CBAceptar.Name = "CBAceptar";
			this.CBAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CBAceptar.Size = new System.Drawing.Size(81, 31);
			this.CBAceptar.TabIndex = 2;
			this.CBAceptar.Text = "&Aceptar";
			this.CBAceptar.Click += new System.EventHandler(this.CBAceptar_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.cbbMotAnulacion);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(64, 32);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(395, 75);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Motivo de anulación";
			this.Frame1.Visible = true;
			// 
			// cbbMotAnulacion
			// 
			this.cbbMotAnulacion.CausesValidation = true;
			this.cbbMotAnulacion.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbbMotAnulacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
			this.cbbMotAnulacion.Enabled = true;
			this.cbbMotAnulacion.Location = new System.Drawing.Point(22, 26);
			this.cbbMotAnulacion.Name = "cbbMotAnulacion";
			this.cbbMotAnulacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbbMotAnulacion.Size = new System.Drawing.Size(355, 21);
			this.cbbMotAnulacion.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
			this.cbbMotAnulacion.TabIndex = 1;
			this.cbbMotAnulacion.TabStop = true;
			this.cbbMotAnulacion.Visible = true;
            this.cbbMotAnulacion.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbMotAnulacion_SelectedIndexChanged);
            // 
			// Mot_anul
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(539, 181);
			this.Controls.Add(this.CbCancelar);
			this.Controls.Add(this.CBAceptar);
			this.Controls.Add(this.Frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location = new System.Drawing.Point(4, 23);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Mot_anul";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Anulación de Cita";
			this.Closed += new System.EventHandler(this.Mot_anul_Closed);
			this.Load += new System.EventHandler(this.Mot_anul_Load);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}