using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.Reflection;
using CrystalWrapper;
namespace ECitaExterna
{
	public partial class citas_externas
		: Telerik.WinControls.UI.RadForm
    {

		public citas_externas()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}


		private void citas_externas_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}



		public string VstOperacion = String.Empty; //MODIFICAR_CITA o AÑADIR_CITA
		ClaseCitasAdmision objcita1 = null;

		bool bEsUnaConsulta = false; // al modificar ver si la fecha de la cita es mayor que la actual -> true
		int CodProvincia = 0;

		string UltCausaAmbulancia = String.Empty;
		bool bComboCausasAmbulancia = false;

		System.DateTime FechaCitaAntigua = DateTime.FromOADate(0);
		System.DateTime FechaCitaAntOtro = DateTime.FromOADate(0);
		System.DateTime LaFechaCita = DateTime.FromOADate(0);


		Crystal ListadoParticular = null;
		string Codigoprov1 = String.Empty;
		bool bcambiar = false;
		string stCodigoPostal = String.Empty;

        bool flagDbClick = false;


        private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			bool bError = false;
			try
			{
				if (!validar_campos())
				{
					return;
				}
				this.Cursor = Cursors.WaitCursor;
				proMemorizarCita();

				switch(VstOperacion)
				{ //MODIFICAR_CITA , AÑADIR_CITA
					case "MODIFICAR_CITA" : 
						objcita1.EditarCita(ECitasExtenas.RcAdmision, objcita1, LaFechaCita, FechaCitaAntOtro, DateTime.Parse(SdcFecha.MaxDate.ToString())); 
						if (SdcFecha.Text != "")
						{
							// si ha cambiado la fecha u hora de la cita y el campo notifam='S'
							System.DateTime TempDate = DateTime.FromOADate(0);
							if (FechaCitaAntigua != DateTime.Parse((DateTime.TryParse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text))
							{
								ListadoParticular = ECitasExtenas.LISTADO_CRYSTAL;
								string tempRefParam = objcita1.FechaCita;
								bError = ECitasExtenas.proLlamarDllImpresionNotificacion(Convert.ToInt32(Double.Parse(objcita1.NumeroRegistro)), Convert.ToInt32(Double.Parse(objcita1.AñoRegistro)), ref tempRefParam, objcita1.IdentificadorPaciente, ref ListadoParticular);
								ListadoParticular = null;
							}
						} 
						break;
					case "AÑADIR_CITA" : 
						objcita1.AñadirCita(ECitasExtenas.RcAdmision, objcita1, DateTime.Parse(SdcFecha.MaxDate.ToString())); 
						break;
				}
				
				this.Cursor = Cursors.Default;
				this.Close();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "Citas_externas:Aceptar_click", ex);
			}
		}
        private void CbbCausa_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
        {
			if (CbbCausa.SelectedIndex != -1)
			{ //si hay alguno seleccionado
				UltCausaAmbulancia = CbbCausa.SelectedValue.ToString();
			}
			else
			{
				UltCausaAmbulancia = "";
			}
		}
        private void CbbCausa_PopupOpened(object sender, EventArgs e)
        {
			if (!bComboCausasAmbulancia)
			{
				proRellenarCausasAmbulancia();
				bComboCausasAmbulancia = true;
			}
		}

		private void CbbCausa_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
        private void cbbDPPoblacion_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
        {
			string mensaje = String.Empty;
			DataSet RrCodPostal = null;
			bool verdad = false;
			if (cbbDPPoblacion.SelectedIndex > -1)
			{
				//busca el codigo postal
				Codigoprov1 = StringsHelper.Format(cbbDPPoblacion.SelectedValue.ToString().Substring(0, Math.Min(2, cbbDPPoblacion.SelectedValue.ToString().Length)), "00");
				SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT gcodipos FROM DCODIPOS WHERE GPOBLACI = '" + StringsHelper.Format(cbbDPPoblacion.SelectedValue, "000000") + "'", ECitasExtenas.RcAdmision);
				RrCodPostal = new DataSet();
				tempAdapter.Fill(RrCodPostal);
				if (RrCodPostal.Tables[0].Rows.Count != 0)
				{
					if (RrCodPostal.Tables[0].Rows.Count == 1)
					{
						if (mebDPCodPostal.Text == "     ")
						{
							mebDPCodPostal.Text = StringsHelper.Format(RrCodPostal.Tables[0].Rows[0]["gcodipos"], "00000");
						}
					}
					else
					{
						if (mebDPCodPostal.Text == StringsHelper.Format(RrCodPostal.Tables[0].Rows[0]["gcodipos"], "00000"))
						{
							return;
						}
						else
						{
							foreach (DataRow iteration_row in RrCodPostal.Tables[0].Rows)
							{
								if (mebDPCodPostal.Text == StringsHelper.Format(iteration_row["gcodipos"], "00000"))
								{
									verdad = true;
									break;
								}
							}
						}
					}
				}
				else
				{
					RrCodPostal.Close();
				}
			}
		}

		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbDBuscar_Click(Object eventSender, EventArgs eventArgs)
		{
			Poblaciones.MCPoblaciones oPoblaciones = new Poblaciones.MCPoblaciones();
			oPoblaciones.llamada(this, ECitasExtenas.RcAdmision, ref cbbDPPoblacion);
			oPoblaciones = null;
		}

		private void chkAmbulancia_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (chkAmbulancia.CheckState == CheckState.Checked)
			{
				CbbCausa.Enabled = true;
				if (UltCausaAmbulancia != "")
				{
					proBuscarEnCombo(UltCausaAmbulancia, CbbCausa);
				}
			}
			else
			{
				if (CbbCausa.SelectedIndex != -1)
				{
					UltCausaAmbulancia = CbbCausa.SelectedValue.ToString();
				}
				else
				{
					UltCausaAmbulancia = "";
				}
				CbbCausa.Enabled = false;
				CbbCausa.SelectedIndex = -1;
			}
		}

		private void citas_externas_Load(Object eventSender, EventArgs eventArgs)
		{
            activarDobleClickRadioButton();

			string sqlPoblaciones = String.Empty;
			string sqlcitas = String.Empty;
			DataSet RrDPACIENT = null;
			DataSet RrPoblaciones = null;
			string SQLDPOBLACION = String.Empty;

			bool CitaAnulada = false;

			bComboCausasAmbulancia = false;
			ObtenerProvinciaDefecto();
			VstOperacion = menu_citas.DefInstance.VstOperacionCita;
			objcita1 = menu_citas.DefInstance.ObjCitaAdmision;
			FechaCitaAntigua = DateTime.Parse("00:00:00");
			
			Frame5.Text = "Datos del " + Serrores.ObtenerLiteralPersona(ECitasExtenas.RcAdmision);
			Label28.Text = Serrores.ObtenerLiteralPersona(ECitasExtenas.RcAdmision);

			SdcFechaOtroCentro.MaxDate = DateTime.Now;
			//SdcFecha.MinDate = Now

			ECitasExtenas.ProCargaCabecera(this);

			//CARGA LA TABLA DE POBLACIONES
			if (VstOperacion == "MODIFICAR_CITA")
			{ //SI ES UNA MODIFICACION
				//Set RrDPACIENT = RcAdmision.OpenResultset("SELECT * FROM DPACIENT WHERE GIDENPAC = '" & VstGidenpac & "'", rdOpenKeyset, rdConcurValues)
				sqlcitas = "select GPROVINC,fanulaci from acontcex where " + 
				           " ganoadme=" + objcita1.AñoRegistro + 
				           " and gnumadme=" + objcita1.NumeroRegistro + 
				           " and fapuntes=" + Serrores.FormatFechaHMS(objcita1.FechaCitaOtroCentro);
				//                  " and fcitaext=" & FormatFechaHMS(objcita1.FechaCita)
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlcitas, ECitasExtenas.RcAdmision);
				RrDPACIENT = new DataSet();
				tempAdapter.Fill(RrDPACIENT);
				if (!Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]))
				{
					//COMPRUEBA QUE LA PROVINCIA SEA LA QUE ESTA POR DEFECTO
					if (Convert.ToDouble(RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]) == CodProvincia)
					{
						SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + CodProvincia.ToString() + "%' order by dpoblaci";
						Codigoprov1 = CodProvincia.ToString();
					}
					else
					{
						SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]) + "%' order by dpoblaci";
						Codigoprov1 = Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]);
						CodProvincia = Convert.ToInt32(RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]);
					}
				}
				else
				{
					SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + CodProvincia.ToString() + "%' order by dpoblaci";
				}
				
				CitaAnulada = !Convert.IsDBNull(RrDPACIENT.Tables[0].Rows[0]["fanulaci"]);
				RrDPACIENT.Close();
			}
			else
			{
				SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + CodProvincia.ToString() + "%' order by dpoblaci";
			}
			//CargarComboAlfanumerico SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbDPPoblacion
			proRellenarPoblacion(SQLDPOBLACION);

			proRellenarModoTransporte();

			if (VstOperacion == "MODIFICAR_CITA")
			{
				proRellenarCita();
				if (objcita1.FechaCita != "")
				{
					if (DateTime.Parse(objcita1.FechaCita) < DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy HH:mm")))
					{
						//''    If CDate(objcita1.FechaCitaOtroCentro) < CDate(Format(Now, "DD/MM/YYYY HH:NN")) Then
						bEsUnaConsulta = true; // proteger todos los campos
						ProBloquearCampos();
					}
					else
					{
						bEsUnaConsulta = false;
					}
				}
				else
				{
					bEsUnaConsulta = false;
				}
				if (CitaAnulada)
				{
					bEsUnaConsulta = true; // proteger todos los campos
					ProBloquearCampos();
				}
			}
			else
			{
				// nueva cita

				ProInicializarPantalla();
				bEsUnaConsulta = false;
				//    SdcFechaOtroCentro.SetFocus
			}

            if (VstOperacion == "AÑADIR_CITA")
            {
                int i = 0;
                foreach (Telerik.WinControls.UI.RadRadioButton rb in optAcomp)
                {
                    
                    if (menu_citas.DefInstance.optAcomp[i].IsChecked)
                    {
                        this.optAcomp[i].IsChecked = true;
                    }
                    else
                        rb.IsChecked = false;

                    i++;
                }
            }

   //         if (VstOperacion == "AÑADIR_CITA")
			//{
			//	if (menu_citas.DefInstance.optAcomp[0].IsChecked)
			//	{
			//		this.optAcomp[0].IsChecked = true;
			//	}
			//	else if (menu_citas.DefInstance.optAcomp[1].IsChecked)
			//	{ 
			//		this.optAcomp[1].IsChecked = true;
			//	}
			//	else if (menu_citas.DefInstance.optAcomp[2].IsChecked)
			//	{ 
			//		this.optAcomp[2].IsChecked = true;
			//	}
			//	else if (menu_citas.DefInstance.optAcomp[3].IsChecked)
			//	{ 
			//		this.optAcomp[3].IsChecked = true;
			//	}
			//}
		}

		private void citas_externas_Closed(Object eventSender, EventArgs eventArgs)
		{
			if (VstOperacion == "AÑADIR_CITA")
			{
				citas_externas.DefInstance = null;
			}
            if (m_vb6FormDefInstance != null)
                m_vb6FormDefInstance.Dispose();
        }


		private void ProInicializarPantalla()
		{
            SdcFecha.NullableValue = null;
            SdcFechaOtroCentro.Value = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			this.tbConsulta.Text = "";
			this.tbDireCentro.Text = "";
			this.tbHoraCambio.Text = "  :  ";
			this.tbHoraOtroCentro.Text = DateTime.Now.ToString("HH:mm");
			this.tbMedico.Text = "";
			this.tbNombreCentro.Text = "";
			this.tbServicio.Text = "";
			this.optAcomp[0].IsChecked = true;
		}

		private void proRellenarCita()
		{
			try
			{
				if (objcita1.FechaCita != "")
				{
					SdcFecha.Value = DateTime.Parse(DateTime.Parse(objcita1.FechaCita).ToString("dd/MM/yyyy"));
					this.tbHoraCambio.Text = DateTime.Parse(objcita1.FechaCita).ToString("HH:mm");
					FechaCitaAntigua = DateTime.Parse(objcita1.FechaCita);
				}
				else
				{
                    SdcFecha.NullableValue = null;
                    SdcFecha.SetToNullValue();					
				}
				if (objcita1.FechaCitaOtroCentro != "")
				{
					this.SdcFechaOtroCentro.Value = DateTime.Parse(DateTime.Parse(objcita1.FechaCitaOtroCentro).ToString("dd/MM/yyyy"));
					this.tbHoraOtroCentro.Text = DateTime.Parse(objcita1.FechaCitaOtroCentro).ToString("HH:mm");
					FechaCitaAntOtro = DateTime.Parse(objcita1.FechaCitaOtroCentro);
				}

				if (objcita1.Centro.Trim() != "")
				{
					this.tbNombreCentro.Text = objcita1.Centro;
				}
				if (objcita1.CodPostal.Trim() != "")
				{
					mebDPCodPostal.Text = objcita1.CodPostal.Trim();
				}
				if (objcita1.CodPoblacion.Trim() != "")
				{
					for (int I = 0; I <= cbbDPPoblacion.Items.Count - 1; I++)
					{
						if (objcita1.CodPoblacion.Trim() == cbbDPPoblacion.Items[I].Value.ToString())
						{
							cbbDPPoblacion.SelectedIndex = I;
							break;
						}
					}
					//proBuscarPoblacion .CodPoblacion
				}
				else
				{
					//si no tiene poblacion cargamos la poblaciones de la provincia por defecto
					//CPPoblacion1.ProvinciaHospital = CodProvincia
				}


				if (objcita1.DireccCentro.Trim() != "")
				{
					this.tbDireCentro.Text = objcita1.DireccCentro.Trim();
				}
				if (objcita1.Especialidad.Trim() != "")
				{
					this.tbServicio.Text = objcita1.Especialidad.Trim();
				}
				if (objcita1.medico.Trim() != "")
				{
					this.tbMedico.Text = objcita1.medico;
				}
				if (objcita1.Prueba.Trim() != "")
				{
					this.tbConsulta.Text = objcita1.Prueba;
				}

				if (objcita1.NecesitaAmbulancia != "N")
				{
					this.chkAmbulancia.CheckState = CheckState.Checked;
				}

                foreach (RadRadioButton rb in optAcomp)
                {
                    rb.IsChecked = false;
                }

                if (objcita1.NecesitaAcompañante == "N")
				{
					optAcomp[0].IsChecked = true;
				}
				if (objcita1.NecesitaAcompañante == "C")
				{
					optAcomp[1].IsChecked = true;
				}
				if (objcita1.NecesitaAcompañante == "F")
				{
					optAcomp[2].IsChecked = true;
				}
				if (objcita1.NecesitaAcompañante == "E")
				{
					optAcomp[3].IsChecked = true;
				}

				if (objcita1.CodigoCausaAmb != "")
				{
					proRellenarCausasAmbulancia();
					proBuscarEnCombo(objcita1.CodigoCausaAmb, CbbCausa);
				}
				if (objcita1.CodigoModoTrans != "")
				{
					proBuscarEnCombo(objcita1.CodigoModoTrans, CbbModoTrans);
				}
				else
				{
					CbbModoTrans.SelectedIndex = 0;
				}
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "Citas_externas:proRellenarCita", ex);
			}
		}


		private void proRellenarPoblacion(string Sql, bool bVaciar = false)
		{
			DataSet RrCombo = null;
			try
			{

				if (bVaciar || false)
				{
					cbbDPPoblacion.Items.Clear();
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ECitasExtenas.RcAdmision);
				RrCombo = new DataSet();
				tempAdapter.Fill(RrCombo);
				foreach (DataRow iteration_row in RrCombo.Tables[0].Rows)
				{
                    cbbDPPoblacion.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dpoblaci"]), Convert.ToInt32(iteration_row["gpoblaci"])));                    
				}
				RrCombo = null;
				cbbDPPoblacion.SelectedIndex = -1;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "Citas_externas:proRellenarPoblacion", ex);
			}
		}
		private void proRellenarCausasAmbulancia()
		{

			string sqlCausas = String.Empty;
			DataSet RrCausas = null;
			try
			{
				sqlCausas = "select * from dcausambva order by dcauambu";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCausas, ECitasExtenas.RcAdmision);
				RrCausas = new DataSet();
				tempAdapter.Fill(RrCausas);
				foreach (DataRow iteration_row in RrCausas.Tables[0].Rows)
				{
                    CbbCausa.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dcauambu"]), Convert.ToInt32(iteration_row["gcauambu"])));
				}
				bComboCausasAmbulancia = true;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "Citas_externas:proRellenarCausasambulancia", ex);
				proRellenarModoTransporte();
			}
		}

		private void proRellenarModoTransporte()
		{
			string sqlModoTrans = String.Empty;
			DataSet RrModoTrans = null;
			try
			{
                CbbModoTrans.Items.Add("<Sin Definir>");
				sqlModoTrans = "select * from AMODTRAN where fborrado is null order by dmodtran";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlModoTrans, ECitasExtenas.RcAdmision);
				RrModoTrans = new DataSet();
				tempAdapter.Fill(RrModoTrans);
				foreach (DataRow iteration_row in RrModoTrans.Tables[0].Rows)
				{
                    CbbModoTrans.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dmodtran"]), Convert.ToInt32(iteration_row["gmodtran"])));
				}
				CbbModoTrans.SelectedIndex = 0;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "Citas_externas:proRellenarModoTransporte", ex);
			}
		}
		//sacamos la provincia por defecto de SCONSGLO
		private void ObtenerProvinciaDefecto()
		{
			try
			{
				DataSet RrProvinciaPpal = null; // SACAMOS EL CODIGO DE PROVINCIA DE sconsglo
				SqlDataAdapter tempAdapter = new SqlDataAdapter("select valfanu1 from sconsglo where gconsglo='CODPROVI'", ECitasExtenas.RcAdmision);
				RrProvinciaPpal = new DataSet();
				tempAdapter.Fill(RrProvinciaPpal);
				if (!Convert.IsDBNull(RrProvinciaPpal.Tables[0].Rows[0][0]))
				{
					CodProvincia = Convert.ToInt32(RrProvinciaPpal.Tables[0].Rows[0][0]);
				}
				RrProvinciaPpal.Close();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "Citas_externas:ObtenerProvinciaDefecto", ex);
			}
		}

        private void proBuscarEnCombo(string Codigo, RadDropDownList comboacargar)
		{
			for (int iBucle = 0; iBucle <= comboacargar.Items.Count - 1; iBucle++)
			{
				if (Convert.ToString(comboacargar.Items[iBucle].Value).Trim() == Codigo.Trim())
				{
					comboacargar.SelectedIndex = iBucle;
					break;
				}
			}
		}


		// PROTEGE TODOS LOS CAMPOS DEBIDO A QUE LA OPERACION ES DE SOLO LECTURA
		private void ProBloquearCampos()
		{
			SdcFecha.Enabled = false;
			this.tbHoraCambio.Enabled = false;
			SdcFechaOtroCentro.Enabled = false;
			this.tbHoraOtroCentro.Enabled = false;

			this.chkAmbulancia.Enabled = false;

			this.cbbDPPoblacion.Enabled = false;
			this.mebDPCodPostal.Enabled = false;
			this.cbDBuscar.Enabled = false;

			this.tbConsulta.Enabled = false;
			this.tbDireCentro.Enabled = false;
			this.tbMedico.Enabled = false;
			this.tbNombreCentro.Enabled = false;
			this.tbServicio.Enabled = false;
			CbAceptar.Enabled = false;
			CbbCausa.Enabled = false;
			Frame4.Enabled = false;
		}

		private bool validar_campos()
		{
            if (SdcFecha.Text == "")
            {
                SdcFecha.NullableValue = null;
                SdcFecha.SetToNullValue();
            }


			if (!Information.IsDate(SdcFecha.Value.Date) && SdcFecha.Text != "")
			{
				RadMessageBox.Show("Fecha de la cita  no correcta", Application.ProductName);
				SdcFecha.Focus();
				return false;
			}
			if (SdcFecha.Text == "" && this.tbHoraCambio.Text.Trim() != ":")
			{
				RadMessageBox.Show("Fecha de la cita  no correcta", Application.ProductName);
				SdcFecha.Focus();
				return false;
			}
			if (SdcFecha.Text != "" && this.tbHoraCambio.Text.Trim() == ":")
			{
				RadMessageBox.Show("Fecha de la cita  no correcta", Application.ProductName);
				SdcFecha.Focus();
				return false;
			}
			if (!Information.IsDate(tbHoraCambio.Text.Trim()) && this.tbHoraCambio.Text.Trim() != ":")
			{
				RadMessageBox.Show("Hora de la cita no correcta", Application.ProductName);
				tbHoraCambio.Focus();
				return false;
			}
			if (SdcFecha.Text != "" && this.tbHoraCambio.Text.Trim() != ":")
			{
				if (DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text) <= DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy HH:mm")))
				{
                    RadMessageBox.Show("La Fecha-Hora de la cita debe ser mayor que la fecha-hora del sistema " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"), Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
					SdcFecha.Focus();
					return false;
				}
			}

			//fecha solicitud otro centro, obligatoria
			if (SdcFechaOtroCentro.Value.Date.ToString() == "")
			{
				RadMessageBox.Show("Fecha de la solicitud obligatoria", Application.ProductName);
				SdcFechaOtroCentro.Focus();
				return false;
			}
			if (this.tbHoraOtroCentro.Text.Trim() == ":")
			{
				RadMessageBox.Show("Hora de la solicitud obligatoria", Application.ProductName);
				tbHoraOtroCentro.Focus();
				return false;
			}
			if (!Information.IsDate(SdcFechaOtroCentro.Value.Date))
			{
				RadMessageBox.Show("Fecha de la solicitud no correcta", Application.ProductName);
				SdcFechaOtroCentro.Focus();
				return false;
			}
			if (!Information.IsDate(tbHoraOtroCentro.Text.Trim()))
			{
				RadMessageBox.Show("Hora de la solicitud no correcta", Application.ProductName);
				tbHoraOtroCentro.Focus();
				return false;
			}
			if (DateTime.Parse(SdcFechaOtroCentro.Value.ToShortDateString() + " " + tbHoraOtroCentro.Text) > DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy HH:mm")))
			{
				RadMessageBox.Show("La Fecha-Hora del apunte debe ser menor que la fecha-hora del sistema " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"), Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
				SdcFechaOtroCentro.Focus();
				return false;
			}
			if (SdcFecha.Text != "")
			{
				if (DateTime.Parse(SdcFechaOtroCentro.Value.ToShortDateString() + " " + tbHoraOtroCentro.Text) > DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text))
				{
					RadMessageBox.Show("La Fecha-Hora del apunte debe ser menor que la fecha-hora de la cita " + DateTimeHelper.ToString(DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text)), Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
					SdcFechaOtroCentro.Focus();
					return false;
				}
			}

			if (SdcFecha.Text != "")
			{
				if (!ComprobarFechaCitaFechaIngresoPlanta())
				{
					SdcFecha.Focus();
					return false;
				}
			}

			if (this.chkAmbulancia.CheckState == CheckState.Checked)
			{ //SI TIENE INDICADOR DE AMBULANCIA COMPROBAR QUE TIENE CAUSA
				if (CbbCausa.SelectedIndex == -1)
				{
					RadMessageBox.Show("Campo causa ambulancia requerido", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
					CbbCausa.Focus();
					return false;
				}

			}
			return true;
		}


		private bool ComprobarFechaCitaFechaIngresoPlanta()
		{
			bool result = false;
			string sqlFechaIngresoPlanta = String.Empty;
			DataSet RrFechaIngresoPlanta = null;
			try
			{
				sqlFechaIngresoPlanta = "select fingplan,fllegada from aepisadm where gnumadme=" + objcita1.NumeroRegistro;
				sqlFechaIngresoPlanta = sqlFechaIngresoPlanta + " and ganoadme=" + objcita1.AñoRegistro;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlFechaIngresoPlanta, ECitasExtenas.RcAdmision);
				RrFechaIngresoPlanta = new DataSet();
				tempAdapter.Fill(RrFechaIngresoPlanta);
				if (RrFechaIngresoPlanta.Tables[0].Rows.Count == 1)
				{ //si está en admisión
					
					if (!Convert.IsDBNull(RrFechaIngresoPlanta.Tables[0].Rows[0]["fingplan"]))
					{ // si tiene fecha de ingreso en planta
						if (DateTime.Parse(Convert.ToDateTime(RrFechaIngresoPlanta.Tables[0].Rows[0]["fingplan"]).ToString("dd/MM/yyyy HH:mm")) > DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text))
						{
						
							RadMessageBox.Show("La fecha de la cita debe ser mayor que la fecha de ingreso en planta (" + 
							                Convert.ToDateTime(RrFechaIngresoPlanta.Tables[0].Rows[0]["fingplan"]).ToString("dd/MM/yyyy HH:mm") + ")", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
							return false;
						}
					}
					else
					{
						//comprobar con la fecha de llegada
						if (DateTime.Parse(Convert.ToDateTime(RrFechaIngresoPlanta.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm")) > DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text))
						{
						
							RadMessageBox.Show("La fecha de la cita debe ser mayor que la fecha de llegada en admisión (" + 
							                Convert.ToDateTime(RrFechaIngresoPlanta.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm") + ")", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);
							return false;
						}
					}
					result = true;
				}
				return result;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "Citas_externas:ComprobarFechaCitaFechaIngresoPlanta", ex);
				return result;
			}
		}

        private void optAcomp_CheckedChanged(Object eventSender, Telerik.WinControls.UI.StateChangedEventArgs eventArgs)
        {
            int Index = Array.IndexOf(this.optAcomp, eventSender);
            if (flagDbClick)
            {
                optAcomp[Index].IsChecked = false;
                flagDbClick = false;
            }
        }

        //(maplaza)(14/05/2007)Se debe permitir que ninguna de las opciones esté marcada, de modo que se si hace
        //doble click sobre una opción que esté marcada, entonces se desmarca.
        private void optAcomp_DblClick(object sender, System.EventArgs e)        
		{
            int Index = Array.IndexOf(this.optAcomp, sender);
            optAcomp[Index].IsChecked = false;
            flagDbClick = true;
        }

		private void SdcFechaOtroCentro_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_Aceptar();
		}

		//UPGRADE_NOTE: (7001) The following declaration (tbcodPostal_KeyPress) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void tbcodPostal_KeyPress(ref int KeyAscii)
		//{
				//if (KeyAscii >= 48 && KeyAscii <= 579)
				//{
					//return;
				//} // obligo a meter sólo números
				//KeyAscii = 0;
		//}

		private void tbConsulta_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_Aceptar();
		}

		private void tbConsulta_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbConsulta.SelectionStart = 0;
			tbConsulta.SelectionLength = Strings.Len(tbConsulta.Text);
		}

		private void tbConsulta_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]); //convierto a mayusculas

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDireCentro_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDireCentro.SelectionStart = 0;
			tbDireCentro.SelectionLength = Strings.Len(tbDireCentro.Text);
		}

		private void tbDireCentro_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]); //convierto a mayusculas
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbHoraOtroCentro_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_Aceptar();
		}

		private void tbMedico_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbMedico.SelectionStart = 0;
			tbMedico.SelectionLength = Strings.Len(tbMedico.Text);
		}

		private void tbMedico_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]); //convierto a mayusculas

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNombreCentro_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_Aceptar();
		}

		private void tbNombreCentro_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNombreCentro.SelectionStart = 0;
			tbNombreCentro.SelectionLength = Strings.Len(tbNombreCentro.Text);
		}

		private void tbNombreCentro_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]); //convierto a mayusculas
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbServicio_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbServicio.SelectionStart = 0;
			tbServicio.SelectionLength = Strings.Len(tbServicio.Text);
		}

		private void tbServicio_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]); //convierto a mayusculas

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void Activar_Aceptar()
		{
			if (SdcFechaOtroCentro.Value.Date.ToString() != "" && tbHoraOtroCentro.Text.Trim() != ":" && this.tbNombreCentro.Text.Trim() != "" && this.tbConsulta.Text.Trim() != "")
			{
				if (!bEsUnaConsulta)
				{
					CbAceptar.Enabled = true;
				}
			}
			else
			{
				CbAceptar.Enabled = false;
			}

		}


		private void proMemorizarCita()
		{
			try
			{
				LaFechaCita = DateTime.Parse("00:00:00");
				objcita1.FechaCita = (SdcFecha.Text + " " + tbHoraCambio.Text);
				if (SdcFecha.Text != "")
				{
					LaFechaCita = DateTime.Parse(objcita1.FechaCita);
				}
				else
				{
					LaFechaCita = DateTime.Parse("00:00:00");
				}

				objcita1.FechaCitaOtroCentro = (SdcFechaOtroCentro.Value.ToShortDateString() + " " + tbHoraOtroCentro.Text);
				if (this.tbConsulta.Text.Trim() != "")
				{
					objcita1.Prueba = tbConsulta.Text.Trim();
				}
				else
				{
					objcita1.Prueba = "";
				}
				if (tbDireCentro.Text.Trim() != "")
				{
					objcita1.DireccCentro = tbDireCentro.Text.Trim();
				}
				else
				{
					objcita1.DireccCentro = "";
				}
				if (this.tbMedico.Text.Trim() != "")
				{
					objcita1.medico = this.tbMedico.Text.Trim();
				}
				else
				{
					objcita1.medico = "";
				}
				if (this.tbNombreCentro.Text.Trim() != "")
				{
					objcita1.Centro = this.tbNombreCentro.Text.Trim();
				}
				else
				{
					objcita1.Centro = "";
				}
				if (this.tbServicio.Text.Trim() != "")
				{
					objcita1.Especialidad = this.tbServicio.Text.Trim();
				}
				else
				{
					objcita1.Especialidad = "";
				}
				if (chkAmbulancia.CheckState == CheckState.Checked)
				{
					objcita1.NecesitaAmbulancia = "S";
					objcita1.CodigoCausaAmb = CbbCausa.SelectedValue.ToString();
				}
				else
				{
					objcita1.NecesitaAmbulancia = "N";
					objcita1.CodigoCausaAmb = "";
				}

				if (CbbModoTrans.SelectedIndex > 0)
				{
					objcita1.CodigoModoTrans = CbbModoTrans.SelectedValue.ToString();
				}
				else
				{
					objcita1.CodigoModoTrans = "";
				}

				if (optAcomp[0].IsChecked)
				{
					objcita1.NecesitaAcompañante = "N";
				}
				else if (optAcomp[1].IsChecked)
				{ 
					objcita1.NecesitaAcompañante = "C";
				}
				else if (optAcomp[2].IsChecked)
				{ 
					objcita1.NecesitaAcompañante = "F";
				}
				else if (optAcomp[3].IsChecked)
				{ 
					objcita1.NecesitaAcompañante = "E";
				}
				else
				{
					//(maplaza)(14/05/2007)Puede darse el caso de que no esté chequeada ninguna opción
					objcita1.NecesitaAcompañante = "";
					//-----
				}

				if (mebDPCodPostal.Text != "")
				{
					objcita1.CodPostal = mebDPCodPostal.Text.Trim(); //CPPoblacion1.CodPostal
				}
				else
				{
					objcita1.CodPostal = "";
				}


				if (cbbDPPoblacion.SelectedIndex > -1)
				{
					objcita1.CodPoblacion = cbbDPPoblacion.SelectedValue.ToString();
					objcita1.CodProvincia = cbbDPPoblacion.SelectedValue.ToString().Trim().Substring(0, Math.Min(2, cbbDPPoblacion.SelectedValue.ToString().Trim().Length));
				}
				else
				{
					objcita1.CodPoblacion = "";
					objcita1.CodProvincia = "";
				}
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "Citas_externas:proMemorizarCita", ex);
			}
		}


		public void CargarComboAlfanumerico(string Sql, string CampoCod, string campoDes, RadDropDownList comboacargar, bool bVaciar = false)
		{
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ECitasExtenas.RcAdmision);
			DataSet RrCombo = new DataSet();
			tempAdapter.Fill(RrCombo);
			if (bVaciar || false)
			{
				comboacargar.Items.Clear();
			}
			foreach (DataRow iteration_row in RrCombo.Tables[0].Rows)
			{
                String nada = Strings.StrConv(Convert.ToString(iteration_row[campoDes]).Trim(), VbStrConv.ProperCase, 0);
                String nada1 = iteration_row[CampoCod].ToString();
                comboacargar.Items.Add(new RadListDataItem(nada , nada1));
			}
			RrCombo.Close();
			cbbDPPoblacion.SelectedIndex = -1;
		}

		private void mebDPCodPostal_Enter(Object eventSender, EventArgs eventArgs)
		{
			mebDPCodPostal.SelectionStart = 0;
			mebDPCodPostal.SelectionLength = mebDPCodPostal.Text.Trim().Length;
		}

		private void mebDPCodPostal_Leave(Object eventSender, EventArgs eventArgs)
		{

			if (mebDPCodPostal.Text.Trim().Length == 0)
			{
				bcambiar = true;
			}
			else
			{
				if (stCodigoPostal == mebDPCodPostal.Text)
				{
					bcambiar = false;
				}
			}
			if (!bcambiar)
			{
				return;
			}
			if (mebDPCodPostal.Text.Trim().Length == 5 && cbbDPPoblacion.SelectedIndex == -1)
			{
				stCodigoPostal = mebDPCodPostal.Text;
				Buscar_poblaciones();
			}
			if (mebDPCodPostal.Text.Trim().Length == 0)
			{
				stCodigoPostal = mebDPCodPostal.Text;
				if (cbbDPPoblacion.SelectedIndex == -1)
				{
					Cargar_PobDefecto();
				}
			}
			if (mebDPCodPostal.Text.Trim().Length != 0)
			{
				mebDPCodPostal.Text = StringsHelper.Format(mebDPCodPostal.Text, "00000");
			}
		}

		private void Buscar_poblaciones()
		{
			string SQLPOBLACION = String.Empty;
			bool bVaciar = false;
			//busca las poblaciones con ese codigodigo postal
			string SQLCODPOSTAL = "select GPOBLACI from dcodipos where gcodipos = '" + mebDPCodPostal.Text.Trim() + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLCODPOSTAL, ECitasExtenas.RcAdmision);
			DataSet RrSQLCODPOSTAL = new DataSet();
			tempAdapter.Fill(RrSQLCODPOSTAL);
			//si no encuentra ninguna poblacion
			if (RrSQLCODPOSTAL.Tables[0].Rows.Count == 0)
			{
				//SI NO ENCUENTRA POBLACIONES
				RrSQLCODPOSTAL.Close();
			}
			else
			{
				//SI ENCUENTRA LAS POBLACIONES
				if (RrSQLCODPOSTAL.Tables[0].Rows.Count > 1)
				{
					//SI ENCUENTRA LAS POBLACIONES
					cbbDPPoblacion.Items.Clear();
					bVaciar = true;
					foreach (DataRow iteration_row in RrSQLCODPOSTAL.Tables[0].Rows)
					{
						//BUSCA LA POBLACION
						SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(iteration_row["GPOBLACI"]) + "' order by dpoblaci";
						//CargarComboAlfanumerico SQLPOBLACION, "gpoblaci", "dpoblaci", cbbDPPoblacion, bVaciar
						proRellenarPoblacion(SQLPOBLACION, bVaciar);
						bVaciar = false;
					}
					RrSQLCODPOSTAL.Close();
				}
				else
				{
					//SI ENCUENTRA UNA POBLACION
					//BUSCA LA POBLACION
					if (Codigoprov1 != StringsHelper.Format(Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Substring(0, Math.Min(2, Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Length)), "00"))
					{
						SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + StringsHelper.Format(Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Substring(0, Math.Min(2, Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Length)), "00") + "%' order by dpoblaci";

						//CargarComboAlfanumerico SQLPOBLACION, "gpoblaci", "dpoblaci", cbbDPPoblacion
						proRellenarPoblacion(SQLPOBLACION);
					}
					for (int I = 0; I <= cbbDPPoblacion.Items.Count - 1; I++)
					{
						if (cbbDPPoblacion.Items[I].Value.ToString() == RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"].ToString())
						{
							cbbDPPoblacion.SelectedIndex = I;
							break;
						}
					}
					RrSQLCODPOSTAL.Close();
				}
			}
		}

		private void Cargar_PobDefecto()
		{
			string SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + CodProvincia.ToString() + "%' order by Dpoblaci";
			//CargarComboAlfanumerico SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbDPPoblacion, True
			proRellenarPoblacion(SQLDPOBLACION, true);
		}
        /// <summary>
        /// INDRA jproche Necesario para activar el evento DoubleClick sobre el control RadioButton 
        /// </summary>
        private void activarDobleClickRadioButton()
        {
            MethodInfo m = typeof(RadioButton).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
            if (m != null)
            {
                m.Invoke(_optAcomp_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_optAcomp_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_optAcomp_2, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_optAcomp_3, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
            }

            _optAcomp_0.MouseDoubleClick += optAcomp_DblClick;
            _optAcomp_1.MouseDoubleClick += optAcomp_DblClick;
            _optAcomp_2.MouseDoubleClick += optAcomp_DblClick;
            _optAcomp_3.MouseDoubleClick += optAcomp_DblClick;
        }
    }
}