using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using CrystalWrapper;

namespace ECitaExterna
{
	public class ECitasExternas
	{
        public object proload(string Paragidenpac, int ParaAno, int ParaNum, Crystal Paralistado, SqlConnection ParaConexion, string ParaUsuarioAplica, string ParaUsuario, string ParaContrase�a, string ParaPath, string ParaDsnCrystal)
		{
			ECitasExtenas.VstGidenpac = Paragidenpac;
			ECitasExtenas.viGanoadme = ParaAno;
			ECitasExtenas.viGnumadme = ParaNum;
			ECitasExtenas.RcAdmision = ParaConexion;
			Serrores.AjustarRelojCliente(ECitasExtenas.RcAdmision);
			ECitasExtenas.VstCodUsua = ParaUsuarioAplica;
			ECitasExtenas.gUsuario = ParaUsuario;
			ECitasExtenas.gContrase�a = ParaContrase�a;
			ECitasExtenas.PathString = ParaPath;
			ECitasExtenas.VstCrystal = ParaDsnCrystal;
			ECitasExtenas.LISTADO_CRYSTAL = Paralistado;

            /*INDRA jproche_TODO_5_5
             CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
             */

            Serrores.Obtener_TipoBD_FormatoFechaBD(ref ECitasExtenas.RcAdmision);
			Serrores.ObtenerNombreAplicacion(ECitasExtenas.RcAdmision);
			Serrores.AjustarRelojCliente(ECitasExtenas.RcAdmision);
			ECitasExtenas.mnu_adm0113_Click(0);

			return null;
		}

		//(maplaza)(14/05/2007)Se a�aden 2 par�metros: "stModulo", que indica el m�dulo llamante (Hospitalizaci�n �
		//Servicios M�dicos), y "stUniEnf_ServMed", para poder filtrar los datos del listado de Citas Externas por
		//unidad de enfermer�a o por servicio m�dico (dependiendo del m�dulo en el que se est�).
		public object Imprimir_pacientes_citas_externas(Crystal Paralistado, SqlConnection ParaConexion, string ParaUsuarioAplica, string ParaUsuario, string ParaContrase�a, string ParaPath, string ParaDsnCrystal, bool ParaSoloIngresados = true, string stModulo = "", string stUniEnf_ServMed = "")
		{

            //FUNCION DE ENTRADA COMUN A TODO IMDH

            /*INDRA jproche_TODO_5_5
             CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
            */

			ECitasExtenas.LISTADO_CRYSTAL = Paralistado;
			ECitasExtenas.RcAdmision = ParaConexion;

			ECitasExtenas.VstCodUsua = ParaUsuarioAplica;
			ECitasExtenas.gUsuario = ParaUsuario;
			ECitasExtenas.gContrase�a = ParaContrase�a;
			ECitasExtenas.PathString = ParaPath;
			ECitasExtenas.VstCrystal = ParaDsnCrystal;
			//(maplaza)(14/05/2007)Para saber qu� m�dulo realiza la llamada
			ECitasExtenas.Modulo = stModulo;
			//-----
			//(maplaza)(16/05/2007)Se obtiene la Unidad de Enfermer�a, o bien el Servicio M�dico (dependiendo del m�dulo que  realiza la llamada).
			ECitasExtenas.UniEnf_ServMed = stUniEnf_ServMed;
			//-----

			Serrores.Obtener_TipoBD_FormatoFechaBD(ref ECitasExtenas.RcAdmision);
			Serrores.ObtenerNombreAplicacion(ECitasExtenas.RcAdmision);

			listado_citas_externas tempLoadForm = listado_citas_externas.DefInstance;
			listado_citas_externas.DefInstance.RecibeParametros(ParaSoloIngresados);
			listado_citas_externas.DefInstance.ShowDialog();

			return null;
		}
	}
}