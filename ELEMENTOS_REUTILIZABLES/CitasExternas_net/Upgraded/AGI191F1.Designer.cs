using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ECitaExterna
{
	partial class citas_externas
	{

		#region "Upgrade Support "
		private static citas_externas m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static citas_externas DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new citas_externas();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "LbNumsegu", "LbFinancia", "LbHistoria", "Label10", "LbPaciente", "Label11", "Label12", "Label28", "Frame5", "CbAceptar", "CbCancelar", "CbbModoTrans", "frmModoTrans", "tbServicio", "tbMedico", "tbConsulta", "Label3", "Label4", "Label5", "FrmConsulta", "tbHoraCambio", "SdcFecha", "tbHoraOtroCentro", "SdcFechaOtroCentro", "Label19", "Label21", "Label9", "Label13", "FrmFechas", "_optAcomp_3", "_optAcomp_2", "_optAcomp_1", "_optAcomp_0", "Frame4", "CbbCausa", "chkAmbulancia", "Label6", "Frame3", "cbbDPPoblacion", "cbDBuscar", "tbDireCentro", "tbNombreCentro", "mebDPCodPostal", "Label8", "Label27", "Label2", "Label1", "Frame2", "Frame1", "optAcomp", "commandButtonHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadTextBox LbNumsegu;
		public Telerik.WinControls.UI.RadTextBox LbFinancia;
		public Telerik.WinControls.UI.RadTextBox LbHistoria;
		public Telerik.WinControls.UI.RadLabel Label10;
		public Telerik.WinControls.UI.RadTextBox LbPaciente;
		public Telerik.WinControls.UI.RadLabel Label11;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		public Telerik.WinControls.UI.RadButton CbAceptar;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadDropDownList CbbModoTrans;
		public Telerik.WinControls.UI.RadGroupBox frmModoTrans;
		public Telerik.WinControls.UI.RadTextBoxControl tbServicio;
		public Telerik.WinControls.UI.RadTextBoxControl tbMedico;
		public Telerik.WinControls.UI.RadTextBoxControl tbConsulta;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadGroupBox FrmConsulta;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraCambio;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFecha;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraOtroCentro;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaOtroCentro;
		public Telerik.WinControls.UI.RadLabel Label19;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadLabel Label9;
		public Telerik.WinControls.UI.RadLabel Label13;
		public Telerik.WinControls.UI.RadGroupBox FrmFechas;
		private Telerik.WinControls.UI.RadRadioButton _optAcomp_3;
		private Telerik.WinControls.UI.RadRadioButton _optAcomp_2;
		private Telerik.WinControls.UI.RadRadioButton _optAcomp_1;
		private Telerik.WinControls.UI.RadRadioButton _optAcomp_0;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		public Telerik.WinControls.UI.RadDropDownList  CbbCausa;
		public Telerik.WinControls.UI.RadCheckBox chkAmbulancia;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public UpgradeHelpers.MSForms.MSCombobox  cbbDPPoblacion;
		public Telerik.WinControls.UI.RadButton cbDBuscar;
		public Telerik.WinControls.UI.RadTextBoxControl tbDireCentro;
		public Telerik.WinControls.UI.RadTextBoxControl tbNombreCentro;
		public Telerik.WinControls.UI.RadMaskedEditBox mebDPCodPostal;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadLabel Label27;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadRadioButton[] optAcomp = new Telerik.WinControls.UI.RadRadioButton[4];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(citas_externas));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
			this.LbNumsegu = new Telerik.WinControls.UI.RadTextBox();
			this.LbFinancia = new Telerik.WinControls.UI.RadTextBox();
			this.LbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.Label10 = new Telerik.WinControls.UI.RadLabel();
			this.LbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label11 = new Telerik.WinControls.UI.RadLabel();
			this.Label12 = new Telerik.WinControls.UI.RadLabel();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.CbAceptar = new Telerik.WinControls.UI.RadButton();
			this.CbCancelar = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.frmModoTrans = new Telerik.WinControls.UI.RadGroupBox();
			this.CbbModoTrans = new Telerik.WinControls.UI.RadDropDownList ();
			this.FrmConsulta = new Telerik.WinControls.UI.RadGroupBox();
			this.tbServicio = new Telerik.WinControls.UI.RadTextBoxControl();
			this.tbMedico = new Telerik.WinControls.UI.RadTextBoxControl();
			this.tbConsulta = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.FrmFechas = new Telerik.WinControls.UI.RadGroupBox();
			this.tbHoraCambio = new Telerik.WinControls.UI.RadMaskedEditBox();
			this.SdcFecha = new Telerik.WinControls.UI.RadDateTimePicker();
			this.tbHoraOtroCentro = new Telerik.WinControls.UI.RadMaskedEditBox();
			this.SdcFechaOtroCentro = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Label19 = new Telerik.WinControls.UI.RadLabel();
			this.Label21 = new Telerik.WinControls.UI.RadLabel();
			this.Label9 = new Telerik.WinControls.UI.RadLabel();
			this.Label13 = new Telerik.WinControls.UI.RadLabel();
			this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
			this._optAcomp_3 = new Telerik.WinControls.UI.RadRadioButton();
			this._optAcomp_2 = new Telerik.WinControls.UI.RadRadioButton();
			this._optAcomp_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._optAcomp_0 = new Telerik.WinControls.UI.RadRadioButton();
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this.CbbCausa = new Telerik.WinControls.UI.RadDropDownList ();
			this.chkAmbulancia = new Telerik.WinControls.UI.RadCheckBox();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.cbbDPPoblacion = new UpgradeHelpers.MSForms.MSCombobox();
			this.cbDBuscar = new Telerik.WinControls.UI.RadButton();
			this.tbDireCentro = new Telerik.WinControls.UI.RadTextBoxControl();
			this.tbNombreCentro = new Telerik.WinControls.UI.RadTextBoxControl();
			this.mebDPCodPostal = new Telerik.WinControls.UI.RadMaskedEditBox();
			this.Label8 = new Telerik.WinControls.UI.RadLabel();
			this.Label27 = new Telerik.WinControls.UI.RadLabel();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Frame5.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.frmModoTrans.SuspendLayout();
			this.FrmConsulta.SuspendLayout();
			this.FrmFechas.SuspendLayout();
			this.Frame4.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
			// 
			// Frame5
			// 
			this.Frame5.Controls.Add(this.LbNumsegu);
			this.Frame5.Controls.Add(this.LbFinancia);
			this.Frame5.Controls.Add(this.LbHistoria);
			this.Frame5.Controls.Add(this.Label10);
			this.Frame5.Controls.Add(this.LbPaciente);
			this.Frame5.Controls.Add(this.Label11);
			this.Frame5.Controls.Add(this.Label12);
			this.Frame5.Controls.Add(this.Label28);
			this.Frame5.Enabled = true;
			this.Frame5.Location = new System.Drawing.Point(11, 3);
			this.Frame5.Name = "Frame5";
			this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame5.Size = new System.Drawing.Size(638, 74);
			this.Frame5.TabIndex = 28;
			this.Frame5.Text = "Datos del Paciente";
			this.Frame5.Visible = true;
			// 
			// LbNumsegu
			// 
			this.LbNumsegu.Enabled = false;
			this.LbNumsegu.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbNumsegu.Location = new System.Drawing.Point(422, 40);
			this.LbNumsegu.Name = "LbNumsegu";
			this.LbNumsegu.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbNumsegu.Size = new System.Drawing.Size(131, 17);
			this.LbNumsegu.TabIndex = 36;
			// 
			// LbFinancia
			// 
			this.LbFinancia.Enabled = false;
            this.LbFinancia.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbFinancia.Location = new System.Drawing.Point(104, 40);
			this.LbFinancia.Name = "LbFinancia";
			this.LbFinancia.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbFinancia.Size = new System.Drawing.Size(229, 17);
			this.LbFinancia.TabIndex = 35;
			// 
			// LbHistoria
			// 
			this.LbHistoria.Enabled = false;
            this.LbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbHistoria.Location = new System.Drawing.Point(456, 16);
			this.LbHistoria.Name = "LbHistoria";
			this.LbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbHistoria.Size = new System.Drawing.Size(97, 17);
			this.LbHistoria.TabIndex = 34;
			// 
			// Label10
			// 
			this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label10.Location = new System.Drawing.Point(376, 16);
			this.Label10.Name = "Label10";
			this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label10.Size = new System.Drawing.Size(75, 17);
			this.Label10.TabIndex = 33;
			this.Label10.Text = "Historia cl�nica:";
			// 
			// LbPaciente
			// 
			this.LbPaciente.Enabled = false;
            this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbPaciente.Location = new System.Drawing.Point(72, 16);
			this.LbPaciente.Name = "LbPaciente";
			this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbPaciente.Size = new System.Drawing.Size(261, 17);
			this.LbPaciente.TabIndex = 32;
			// 
			// Label11
			// 
			this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label11.Location = new System.Drawing.Point(344, 40);
			this.Label11.Name = "Label11";
			this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label11.Size = new System.Drawing.Size(75, 17);
			this.Label11.TabIndex = 31;
			this.Label11.Text = "N� asegurado:";
			// 
			// Label12
			// 
			this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label12.Location = new System.Drawing.Point(8, 40);
			this.Label12.Name = "Label12";
			this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label12.Size = new System.Drawing.Size(91, 17);
			this.Label12.TabIndex = 30;
			this.Label12.Text = "Ent. Financiadora:";
			// 
			// Label28
			// 
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label28.Location = new System.Drawing.Point(8, 16);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(59, 17);
			this.Label28.TabIndex = 29;
			this.Label28.Text = "Paciente:";
			// 
			// CbAceptar
			// 
			
			this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbAceptar.Enabled = false;
			this.CbAceptar.Location = new System.Drawing.Point(474, 453);
			this.CbAceptar.Name = "CbAceptar";
			this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbAceptar.Size = new System.Drawing.Size(81, 31);
			this.CbAceptar.TabIndex = 19;
			this.CbAceptar.Text = "&Aceptar";
			this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
			// 
			// CbCancelar
			// 
			this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCancelar.Location = new System.Drawing.Point(569, 453);
			this.CbCancelar.Name = "CbCancelar";
			this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCancelar.Size = new System.Drawing.Size(81, 31);
			this.CbCancelar.TabIndex = 20;
			this.CbCancelar.Text = "&Cancelar";
			this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.frmModoTrans);
			this.Frame1.Controls.Add(this.FrmConsulta);
			this.Frame1.Controls.Add(this.FrmFechas);
			this.Frame1.Controls.Add(this.Frame4);
			this.Frame1.Controls.Add(this.Frame3);
			this.Frame1.Controls.Add(this.Frame2);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(11, 85);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(639, 358);
			this.Frame1.TabIndex = 21;
			this.Frame1.Visible = true;
			// 
			// frmModoTrans
			// 
			this.frmModoTrans.Controls.Add(this.CbbModoTrans);
			this.frmModoTrans.Enabled = true;
			this.frmModoTrans.Location = new System.Drawing.Point(385, 260);
			this.frmModoTrans.Name = "frmModoTrans";
			this.frmModoTrans.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmModoTrans.Size = new System.Drawing.Size(245, 43);
			this.frmModoTrans.TabIndex = 48;
			this.frmModoTrans.Text = "Modo de traslado";
			this.frmModoTrans.Visible = true;
			// 
			// CbbModoTrans
			// 
			this.CbbModoTrans.CausesValidation = true;
			this.CbbModoTrans.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbbModoTrans.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
			this.CbbModoTrans.Enabled = true;
			this.CbbModoTrans.Location = new System.Drawing.Point(11, 14);
			this.CbbModoTrans.Name = "CbbModoTrans";
			this.CbbModoTrans.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbbModoTrans.Size = new System.Drawing.Size(225, 21);
            this.CbbModoTrans.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
			this.CbbModoTrans.TabIndex = 14;
			this.CbbModoTrans.TabStop = true;
			this.CbbModoTrans.Visible = true;
			// 
			// FrmConsulta
			// 
			this.FrmConsulta.Controls.Add(this.tbServicio);
			this.FrmConsulta.Controls.Add(this.tbMedico);
			this.FrmConsulta.Controls.Add(this.tbConsulta);
			this.FrmConsulta.Controls.Add(this.Label3);
			this.FrmConsulta.Controls.Add(this.Label4);
			this.FrmConsulta.Controls.Add(this.Label5);
			this.FrmConsulta.Enabled = true;
			this.FrmConsulta.Location = new System.Drawing.Point(9, 164);
			this.FrmConsulta.Name = "FrmConsulta";
			this.FrmConsulta.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmConsulta.Size = new System.Drawing.Size(621, 93);
			this.FrmConsulta.TabIndex = 44;
			this.FrmConsulta.Text = "Actividad";
			this.FrmConsulta.Visible = true;
			// 
			// tbServicio
			// 
			this.tbServicio.AcceptsReturn = true;
			this.tbServicio.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbServicio.Location = new System.Drawing.Point(88, 18);
			this.tbServicio.MaxLength = 40;
			this.tbServicio.Name = "tbServicio";
			this.tbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbServicio.Size = new System.Drawing.Size(313, 19);
			this.tbServicio.TabIndex = 9;
			this.tbServicio.Enter += new System.EventHandler(this.tbServicio_Enter);
			this.tbServicio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbServicio_KeyPress);
			// 
			// tbMedico
			// 
			this.tbMedico.AcceptsReturn = true;
			this.tbMedico.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbMedico.Location = new System.Drawing.Point(88, 42);
			this.tbMedico.MaxLength = 40;
			this.tbMedico.Name = "tbMedico";
			this.tbMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbMedico.Size = new System.Drawing.Size(313, 19);
			this.tbMedico.TabIndex = 10;
			this.tbMedico.Enter += new System.EventHandler(this.tbMedico_Enter);
			this.tbMedico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMedico_KeyPress);
			// 
			// tbConsulta
			// 
			this.tbConsulta.AcceptsReturn = true;
			this.tbConsulta.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbConsulta.Location = new System.Drawing.Point(88, 66);
			this.tbConsulta.MaxLength = 60;
			this.tbConsulta.Name = "tbConsulta";
			this.tbConsulta.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbConsulta.Size = new System.Drawing.Size(417, 19);
			this.tbConsulta.TabIndex = 11;
			this.tbConsulta.Enter += new System.EventHandler(this.tbConsulta_Enter);
			this.tbConsulta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbConsulta_KeyPress);
			this.tbConsulta.TextChanged += new System.EventHandler(this.tbConsulta_TextChanged);
			// 
			// Label3
			// 
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.Location = new System.Drawing.Point(16, 19);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(49, 17);
			this.Label3.TabIndex = 47;
			this.Label3.Text = "Servicio:";
			// 
			// Label4
			// 
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label4.Location = new System.Drawing.Point(16, 43);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(49, 17);
			this.Label4.TabIndex = 46;
			this.Label4.Text = "M�dico:";
			// 
			// Label5
			// 
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label5.Location = new System.Drawing.Point(16, 67);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(49, 17);
			this.Label5.TabIndex = 45;
			this.Label5.Text = "Consulta:";
			// 
			// FrmFechas
			// 
			this.FrmFechas.Controls.Add(this.tbHoraCambio);
			this.FrmFechas.Controls.Add(this.SdcFecha);
			this.FrmFechas.Controls.Add(this.tbHoraOtroCentro);
			this.FrmFechas.Controls.Add(this.SdcFechaOtroCentro);
			this.FrmFechas.Controls.Add(this.Label19);
			this.FrmFechas.Controls.Add(this.Label21);
			this.FrmFechas.Controls.Add(this.Label9);
			this.FrmFechas.Controls.Add(this.Label13);
			this.FrmFechas.Enabled = true;
			this.FrmFechas.Location = new System.Drawing.Point(9, 11);
			this.FrmFechas.Name = "FrmFechas";
			this.FrmFechas.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmFechas.Size = new System.Drawing.Size(621, 50);
			this.FrmFechas.TabIndex = 39;
			this.FrmFechas.Text = "Fechas";
			this.FrmFechas.Visible = true;
            // 
            // tbHoraCambio
            // 
            this.tbHoraCambio.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbHoraCambio.AllowPromptAsInput = false;
			this.tbHoraCambio.Location = new System.Drawing.Point(554, 17);
			this.tbHoraCambio.Mask = "99:99";
			this.tbHoraCambio.Name = "tbHoraCambio";
			this.tbHoraCambio.PromptChar = ' ';
			this.tbHoraCambio.Size = new System.Drawing.Size(45, 20);
			this.tbHoraCambio.TabIndex = 3;
			// 
			// SdcFecha
			// 
			this.SdcFecha.CustomFormat = "dd/MM/yyyy";
			this.SdcFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFecha.Location = new System.Drawing.Point(401, 16);
			this.SdcFecha.Name = "SdcFecha";
			this.SdcFecha.Size = new System.Drawing.Size(105, 20);
			this.SdcFecha.TabIndex = 2;
            // 
            // tbHoraOtroCentro
            // 
            this.tbHoraOtroCentro.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbHoraOtroCentro.AllowPromptAsInput = false;
			this.tbHoraOtroCentro.Location = new System.Drawing.Point(237, 17);
			this.tbHoraOtroCentro.Mask = "99:99";
			this.tbHoraOtroCentro.Name = "tbHoraOtroCentro";
			this.tbHoraOtroCentro.PromptChar = ' ';
			this.tbHoraOtroCentro.Size = new System.Drawing.Size(45, 20);
			this.tbHoraOtroCentro.TabIndex = 1;
			this.tbHoraOtroCentro.Leave += new System.EventHandler(this.tbHoraOtroCentro_Leave);
			// 
			// SdcFechaOtroCentro
			// 
			this.SdcFechaOtroCentro.CustomFormat = "dd/MM/yyyy";
			this.SdcFechaOtroCentro.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFechaOtroCentro.Location = new System.Drawing.Point(88, 16);
			this.SdcFechaOtroCentro.Name = "SdcFechaOtroCentro";
			this.SdcFechaOtroCentro.Size = new System.Drawing.Size(105, 20);
			this.SdcFechaOtroCentro.TabIndex = 0;
			this.SdcFechaOtroCentro.Leave += new System.EventHandler(this.SdcFechaOtroCentro_Leave);
			// 
			// Label19
			// 
			this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label19.Location = new System.Drawing.Point(522, 20);
			this.Label19.Name = "Label19";
			this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label19.Size = new System.Drawing.Size(32, 17);
			this.Label19.TabIndex = 43;
			this.Label19.Text = "Hora:";
			// 
			// Label21
			// 
			this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label21.Location = new System.Drawing.Point(342, 20);
			this.Label21.Name = "Label21";
			this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label21.Size = new System.Drawing.Size(49, 15);
			this.Label21.TabIndex = 42;
			this.Label21.Text = "De la Cita:";
			// 
			// Label9
			// 
			this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label9.Location = new System.Drawing.Point(16, 20);
			this.Label9.Name = "Label9";
			this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label9.Size = new System.Drawing.Size(61, 17);
			this.Label9.TabIndex = 41;
			this.Label9.Text = "Del Apunte:";
			// 
			// Label13
			// 
			this.Label13.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label13.Location = new System.Drawing.Point(204, 20);
			this.Label13.Name = "Label13";
			this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label13.Size = new System.Drawing.Size(30, 17);
			this.Label13.TabIndex = 40;
			this.Label13.Text = "Hora:";
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this._optAcomp_3);
			this.Frame4.Controls.Add(this._optAcomp_2);
			this.Frame4.Controls.Add(this._optAcomp_1);
			this.Frame4.Controls.Add(this._optAcomp_0);
			this.Frame4.Enabled = true;
			this.Frame4.Location = new System.Drawing.Point(8, 306);
			this.Frame4.Name = "Frame4";
			this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame4.Size = new System.Drawing.Size(622, 41);
			this.Frame4.TabIndex = 27;
			this.Frame4.Text = "Acompa�ante";
			this.Frame4.Visible = true;
			// 
			// _optAcomp_3
			// 
			this._optAcomp_3.CausesValidation = true;
			this._optAcomp_3.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._optAcomp_3.IsChecked = false;
			this._optAcomp_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._optAcomp_3.Enabled = true;
			this._optAcomp_3.Location = new System.Drawing.Point(462, 14);
			this._optAcomp_3.Name = "_optAcomp_3";
			this._optAcomp_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optAcomp_3.Size = new System.Drawing.Size(77, 19);
			this._optAcomp_3.TabIndex = 18;
			this._optAcomp_3.TabStop = true;
			this._optAcomp_3.Text = "Externo";
			this._optAcomp_3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_3.Visible = true;
            this._optAcomp_3.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optAcomp_CheckedChanged);
            // 
            // _optAcomp_2
            // 
            this._optAcomp_2.CausesValidation = true;
			this._optAcomp_2.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._optAcomp_2.IsChecked = false;
			this._optAcomp_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._optAcomp_2.Enabled = true;
			this._optAcomp_2.Location = new System.Drawing.Point(336, 14);
			this._optAcomp_2.Name = "_optAcomp_2";
			this._optAcomp_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optAcomp_2.Size = new System.Drawing.Size(77, 19);
			this._optAcomp_2.TabIndex = 17;
			this._optAcomp_2.TabStop = true;
			this._optAcomp_2.Text = "Familiar";
			this._optAcomp_2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_2.Visible = true;
            this._optAcomp_2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optAcomp_CheckedChanged);
            // 
            // _optAcomp_1
            // 
            this._optAcomp_1.CausesValidation = true;
			this._optAcomp_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._optAcomp_1.IsChecked = false;
			this._optAcomp_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._optAcomp_1.Enabled = true;
			this._optAcomp_1.Location = new System.Drawing.Point(181, 14);
			this._optAcomp_1.Name = "_optAcomp_1";
			this._optAcomp_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optAcomp_1.Size = new System.Drawing.Size(109, 19);
			this._optAcomp_1.TabIndex = 16;
			this._optAcomp_1.TabStop = true;
			this._optAcomp_1.Text = "Personal Centro";
			this._optAcomp_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_1.Visible = true;
            this._optAcomp_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optAcomp_CheckedChanged);
            // 
            // _optAcomp_0
            // 
            this._optAcomp_0.CausesValidation = true;
			this._optAcomp_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this._optAcomp_0.IsChecked = false;
			this._optAcomp_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._optAcomp_0.Enabled = true;
			this._optAcomp_0.Location = new System.Drawing.Point(51, 16);
			this._optAcomp_0.Name = "_optAcomp_0";
			this._optAcomp_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optAcomp_0.Size = new System.Drawing.Size(75, 19);
			this._optAcomp_0.TabIndex = 15;
			this._optAcomp_0.TabStop = true;
			this._optAcomp_0.Text = "No Precisa";
			this._optAcomp_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_0.Visible = true;
            this._optAcomp_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optAcomp_CheckedChanged);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.CbbCausa);
			this.Frame3.Controls.Add(this.chkAmbulancia);
			this.Frame3.Controls.Add(this.Label6);
			this.Frame3.Enabled = true;
			this.Frame3.Location = new System.Drawing.Point(8, 260);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(366, 43);
			this.Frame3.TabIndex = 25;
			this.Frame3.Text = "Ambulancia";
			this.Frame3.Visible = true;
			// 
			// CbbCausa
			// 
			this.CbbCausa.CausesValidation = true;
			this.CbbCausa.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbbCausa.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
			this.CbbCausa.Enabled = false;
			this.CbbCausa.Location = new System.Drawing.Point(134, 15);
			this.CbbCausa.Name = "CbbCausa";
			this.CbbCausa.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbbCausa.Size = new System.Drawing.Size(225, 21);
			this.CbbCausa.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
			this.CbbCausa.TabIndex = 13;
			this.CbbCausa.TabStop = true;
			this.CbbCausa.Visible = true;
            this.CbbCausa.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbCausa_SelectedIndexChanged);
            this.CbbCausa.PopupOpened += new System.EventHandler(this.CbbCausa_PopupOpened);
            this.CbbCausa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbCausa_KeyPress);
			// 
			// chkAmbulancia
			// 
			this.chkAmbulancia.CausesValidation = true;
			this.chkAmbulancia.CheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.chkAmbulancia.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.chkAmbulancia.Cursor = System.Windows.Forms.Cursors.Default;
			this.chkAmbulancia.Enabled = true;
			this.chkAmbulancia.Location = new System.Drawing.Point(11, 15);
			this.chkAmbulancia.Name = "chkAmbulancia";
			this.chkAmbulancia.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.chkAmbulancia.Size = new System.Drawing.Size(65, 17);
			this.chkAmbulancia.TabIndex = 12;
			this.chkAmbulancia.TabStop = true;
			this.chkAmbulancia.Text = "Precisa";
			this.chkAmbulancia.Visible = true;
			this.chkAmbulancia.CheckStateChanged += new System.EventHandler(this.chkAmbulancia_CheckStateChanged);
			// 
			// Label6
			// 
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label6.Location = new System.Drawing.Point(86, 15);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(41, 17);
			this.Label6.TabIndex = 26;
			this.Label6.Text = "Causa:";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cbbDPPoblacion);
			this.Frame2.Controls.Add(this.cbDBuscar);
			this.Frame2.Controls.Add(this.tbDireCentro);
			this.Frame2.Controls.Add(this.tbNombreCentro);
			this.Frame2.Controls.Add(this.mebDPCodPostal);
			this.Frame2.Controls.Add(this.Label8);
			this.Frame2.Controls.Add(this.Label27);
			this.Frame2.Controls.Add(this.Label2);
			this.Frame2.Controls.Add(this.Label1);
			this.Frame2.Enabled = true;
			this.Frame2.Location = new System.Drawing.Point(9, 64);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(621, 97);
			this.Frame2.TabIndex = 22;
			this.Frame2.Text = "Centro";
			this.Frame2.Visible = true;
			// 
			// cbbDPPoblacion
			// 
			this.cbbDPPoblacion.CausesValidation = true;
			this.cbbDPPoblacion.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbbDPPoblacion.DropDownStyle  = Telerik.WinControls.RadDropDownStyle.DropDown;
			this.cbbDPPoblacion.Enabled = true;
			this.cbbDPPoblacion.Location = new System.Drawing.Point(192, 65);
			this.cbbDPPoblacion.Name = "cbbDPPoblacion";
			this.cbbDPPoblacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbbDPPoblacion.Size = new System.Drawing.Size(308, 21);
			this.cbbDPPoblacion.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
			this.cbbDPPoblacion.TabIndex = 7;
			this.cbbDPPoblacion.TabStop = true;
			this.cbbDPPoblacion.Visible = true;
            this.cbbDPPoblacion.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbDPPoblacion_SelectedIndexChanged);
            // 
            // cbDBuscar
            // 
            this.cbDBuscar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbDBuscar.Image = (System.Drawing.Image) resources.GetObject("cbDBuscar.Image");
			this.cbDBuscar.Location = new System.Drawing.Point(500, 65);
			this.cbDBuscar.Name = "cbDBuscar";
			this.cbDBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbDBuscar.Size = new System.Drawing.Size(25, 21);
			this.cbDBuscar.TabIndex = 8;
            this.cbDBuscar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbDBuscar.Click += new System.EventHandler(this.cbDBuscar_Click);
			// 
			// tbDireCentro
			// 
			this.tbDireCentro.AcceptsReturn = true;
			this.tbDireCentro.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbDireCentro.Location = new System.Drawing.Point(88, 40);
			this.tbDireCentro.MaxLength = 100;
			this.tbDireCentro.Name = "tbDireCentro";
			this.tbDireCentro.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbDireCentro.Size = new System.Drawing.Size(403, 19);
			this.tbDireCentro.TabIndex = 5;
			this.tbDireCentro.Enter += new System.EventHandler(this.tbDireCentro_Enter);
			this.tbDireCentro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDireCentro_KeyPress);
			// 
			// tbNombreCentro
			// 
			this.tbNombreCentro.AcceptsReturn = true;
			this.tbNombreCentro.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbNombreCentro.Location = new System.Drawing.Point(88, 16);
			this.tbNombreCentro.MaxLength = 30;
			this.tbNombreCentro.Name = "tbNombreCentro";
			this.tbNombreCentro.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbNombreCentro.Size = new System.Drawing.Size(225, 19);
			this.tbNombreCentro.TabIndex = 4;
			this.tbNombreCentro.Enter += new System.EventHandler(this.tbNombreCentro_Enter);
			this.tbNombreCentro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombreCentro_KeyPress);
			this.tbNombreCentro.TextChanged += new System.EventHandler(this.tbNombreCentro_TextChanged);
            // 
            // mebDPCodPostal
            // 
            this.mebDPCodPostal.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.mebDPCodPostal.AllowPromptAsInput = false;
			this.mebDPCodPostal.Location = new System.Drawing.Point(88, 66);
			this.mebDPCodPostal.Mask = "#####";
			this.mebDPCodPostal.Name = "mebDPCodPostal";
			this.mebDPCodPostal.PromptChar = ' ';
			this.mebDPCodPostal.Size = new System.Drawing.Size(49, 21);
			this.mebDPCodPostal.TabIndex = 6;
			this.mebDPCodPostal.Enter += new System.EventHandler(this.mebDPCodPostal_Enter);
			this.mebDPCodPostal.Leave += new System.EventHandler(this.mebDPCodPostal_Leave);
			// 
			// Label8
			// 
			this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label8.Location = new System.Drawing.Point(135, 68);
			this.Label8.Name = "Label8";
			this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label8.Size = new System.Drawing.Size(49, 17);
			this.Label8.TabIndex = 38;
			this.Label8.Text = "Poblaci�n:";
			// 
			// Label27
			// 
			this.Label27.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label27.Location = new System.Drawing.Point(16, 68);
			this.Label27.Name = "Label27";
			this.Label27.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label27.Size = new System.Drawing.Size(67, 19);
			this.Label27.TabIndex = 37;
			this.Label27.Text = "C�d. postal:";
			// 
			// Label2
			// 
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.Location = new System.Drawing.Point(16, 40);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(57, 17);
			this.Label2.TabIndex = 24;
			this.Label2.Text = "Domicilio:";
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(16, 16);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(49, 17);
			this.Label1.TabIndex = 23;
			this.Label1.Text = "Nombre:";
			// 
			// citas_externas
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(659, 493);
			this.Controls.Add(this.Frame5);
			this.Controls.Add(this.CbAceptar);
			this.Controls.Add(this.CbCancelar);
			this.Controls.Add(this.Frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "citas_externas";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "CITAS EN OTRO CENTRO - AGI191F1";
			this.Closed += new System.EventHandler(this.citas_externas_Closed);
			this.Load += new System.EventHandler(this.citas_externas_Load);
			this.Frame5.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.frmModoTrans.ResumeLayout(false);
			this.FrmConsulta.ResumeLayout(false);
			this.FrmFechas.ResumeLayout(false);
			this.Frame4.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeoptAcomp();
		}
		void InitializeoptAcomp()
		{
			this.optAcomp = new Telerik.WinControls.UI.RadRadioButton[4];
			this.optAcomp[3] = _optAcomp_3;
			this.optAcomp[2] = _optAcomp_2;
			this.optAcomp[1] = _optAcomp_1;
			this.optAcomp[0] = _optAcomp_0;
            foreach (Telerik.WinControls.UI.RadRadioButton rb in optAcomp)
            {
                rb.IsChecked = false;
            }
        }
		#endregion
	}
}