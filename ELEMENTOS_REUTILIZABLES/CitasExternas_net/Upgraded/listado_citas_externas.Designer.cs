using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ECitaExterna
{
	partial class listado_citas_externas
	{

		#region "Upgrade Support "
		private static listado_citas_externas m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static listado_citas_externas DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new listado_citas_externas();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_chIngAlt_1", "_chIngAlt_0", "frmEstado", "_optOrdena_2", "_optOrdena_1", "_optOrdena_0", "Frame2", "chkPendienteAsignacionFecha", "tbHoraDesde", "SdcFechaDesde", "tbHoraHasta", "SdcFechaHasta", "Label2", "Label1", "Label19", "Label21", "Frame1", "CbCancelar", "CbAceptar", "chIngAlt", "optOrdena"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		private Telerik.WinControls.UI.RadCheckBox _chIngAlt_1;
		private Telerik.WinControls.UI.RadCheckBox _chIngAlt_0;
		public Telerik.WinControls.UI.RadGroupBox frmEstado;
		private Telerik.WinControls.UI.RadRadioButton _optOrdena_2;
		private Telerik.WinControls.UI.RadRadioButton _optOrdena_1;
		private Telerik.WinControls.UI.RadRadioButton _optOrdena_0;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadCheckBox chkPendienteAsignacionFecha;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraDesde;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaDesde;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraHasta;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaHasta;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label19;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadButton CbAceptar;
		public Telerik.WinControls.UI.RadCheckBox[] chIngAlt = new Telerik.WinControls.UI.RadCheckBox[2];
		public Telerik.WinControls.UI.RadRadioButton[] optOrdena = new Telerik.WinControls.UI.RadRadioButton[3];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(listado_citas_externas));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.frmEstado = new Telerik.WinControls.UI.RadGroupBox();
			this._chIngAlt_1 = new Telerik.WinControls.UI.RadCheckBox();
			this._chIngAlt_0 = new Telerik.WinControls.UI.RadCheckBox();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this._optOrdena_2 = new Telerik.WinControls.UI.RadRadioButton();
			this._optOrdena_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._optOrdena_0 = new Telerik.WinControls.UI.RadRadioButton();
			this.chkPendienteAsignacionFecha = new Telerik.WinControls.UI.RadCheckBox();
			this.tbHoraDesde = new Telerik.WinControls.UI.RadMaskedEditBox();
			this.SdcFechaDesde = new Telerik.WinControls.UI.RadDateTimePicker();
			this.tbHoraHasta = new Telerik.WinControls.UI.RadMaskedEditBox();
			this.SdcFechaHasta = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Label19 = new Telerik.WinControls.UI.RadLabel();
			this.Label21 = new Telerik.WinControls.UI.RadLabel();
			this.CbCancelar = new Telerik.WinControls.UI.RadButton();
			this.CbAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame1.SuspendLayout();
			this.frmEstado.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.frmEstado);
			this.Frame1.Controls.Add(this.Frame2);
			this.Frame1.Controls.Add(this.chkPendienteAsignacionFecha);
			this.Frame1.Controls.Add(this.tbHoraDesde);
			this.Frame1.Controls.Add(this.SdcFechaDesde);
			this.Frame1.Controls.Add(this.tbHoraHasta);
			this.Frame1.Controls.Add(this.SdcFechaHasta);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Controls.Add(this.Label19);
			this.Frame1.Controls.Add(this.Label21);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(12, 5);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(350, 242);
			this.Frame1.TabIndex = 12;
			this.Frame1.Text = "Opciones";
			this.Frame1.Visible = true;
			// 
			// frmEstado
			// 
			this.frmEstado.Controls.Add(this._chIngAlt_1);
			this.frmEstado.Controls.Add(this._chIngAlt_0);
			this.frmEstado.Enabled = true;
			this.frmEstado.Location = new System.Drawing.Point(186, 83);
			this.frmEstado.Name = "frmEstado";
			this.frmEstado.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmEstado.Size = new System.Drawing.Size(123, 107);
			this.frmEstado.TabIndex = 18;
			this.frmEstado.Text = "Pacientes";
			this.frmEstado.Visible = true;
			// 
			// _chIngAlt_1
			// 
			this._chIngAlt_1.CausesValidation = true;
			this._chIngAlt_1.CheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._chIngAlt_1.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chIngAlt_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._chIngAlt_1.Enabled = true;
			this._chIngAlt_1.Location = new System.Drawing.Point(20, 62);
			this._chIngAlt_1.Name = "_chIngAlt_1";
			this._chIngAlt_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chIngAlt_1.Size = new System.Drawing.Size(88, 22);
			this._chIngAlt_1.TabIndex = 8;
			this._chIngAlt_1.TabStop = true;
			this._chIngAlt_1.Text = "Dados de alta";
			this._chIngAlt_1.Visible = true;
			// 
			// _chIngAlt_0
			// 
			this._chIngAlt_0.CausesValidation = true;
			this._chIngAlt_0.CheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._chIngAlt_0.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._chIngAlt_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._chIngAlt_0.Enabled = true;
			this._chIngAlt_0.Location = new System.Drawing.Point(20, 26);
			this._chIngAlt_0.Name = "_chIngAlt_0";
			this._chIngAlt_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._chIngAlt_0.Size = new System.Drawing.Size(88, 22);
			this._chIngAlt_0.TabIndex = 7;
			this._chIngAlt_0.TabStop = true;
			this._chIngAlt_0.Text = "Ingresados";
			this._chIngAlt_0.Visible = true;
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this._optOrdena_2);
			this.Frame2.Controls.Add(this._optOrdena_1);
			this.Frame2.Controls.Add(this._optOrdena_0);
			this.Frame2.Enabled = true;
			this.Frame2.Location = new System.Drawing.Point(33, 83);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(142, 107);
			this.Frame2.TabIndex = 17;
			this.Frame2.Text = "Ordenación del listado";
			this.Frame2.Visible = true;
			// 
			// _optOrdena_2
			// 
			this._optOrdena_2.CausesValidation = true;
			this._optOrdena_2.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrdena_2.IsChecked = false;
			this._optOrdena_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._optOrdena_2.Enabled = true;
			this._optOrdena_2.Location = new System.Drawing.Point(9, 72);
			this._optOrdena_2.Name = "_optOrdena_2";
			this._optOrdena_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optOrdena_2.Size = new System.Drawing.Size(130, 23);
			this._optOrdena_2.TabIndex = 6;
			this._optOrdena_2.TabStop = true;
			this._optOrdena_2.Text = "Control enfermería";
			this._optOrdena_2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrdena_2.Visible = true;
			// 
			// _optOrdena_1
			// 
			this._optOrdena_1.CausesValidation = true;
			this._optOrdena_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrdena_1.IsChecked = false;
			this._optOrdena_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._optOrdena_1.Enabled = true;
			this._optOrdena_1.Location = new System.Drawing.Point(9, 46);
			this._optOrdena_1.Name = "_optOrdena_1";
			this._optOrdena_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optOrdena_1.Size = new System.Drawing.Size(130, 23);
			this._optOrdena_1.TabIndex = 5;
			this._optOrdena_1.TabStop = true;
			this._optOrdena_1.Text = "Nombre del paciente";
			this._optOrdena_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrdena_1.Visible = true;
			// 
			// _optOrdena_0
			// 
			this._optOrdena_0.CausesValidation = true;
			this._optOrdena_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrdena_0.IsChecked = false;
			this._optOrdena_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._optOrdena_0.Enabled = true;
			this._optOrdena_0.Location = new System.Drawing.Point(9, 20);
			this._optOrdena_0.Name = "_optOrdena_0";
			this._optOrdena_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optOrdena_0.Size = new System.Drawing.Size(130, 23);
			this._optOrdena_0.TabIndex = 4;
			this._optOrdena_0.TabStop = true;
			this._optOrdena_0.Text = "Fecha de la cita";
			this._optOrdena_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optOrdena_0.Visible = true;
			// 
			// chkPendienteAsignacionFecha
			// 
			this.chkPendienteAsignacionFecha.CausesValidation = true;
			this.chkPendienteAsignacionFecha.CheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.chkPendienteAsignacionFecha.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.chkPendienteAsignacionFecha.Cursor = System.Windows.Forms.Cursors.Default;
			this.chkPendienteAsignacionFecha.Enabled = true;
			this.chkPendienteAsignacionFecha.Location = new System.Drawing.Point(33, 204);
			this.chkPendienteAsignacionFecha.Name = "chkPendienteAsignacionFecha";
			this.chkPendienteAsignacionFecha.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.chkPendienteAsignacionFecha.Size = new System.Drawing.Size(287, 19);
			this.chkPendienteAsignacionFecha.TabIndex = 9;
			this.chkPendienteAsignacionFecha.TabStop = true;
			this.chkPendienteAsignacionFecha.Text = "Incluir citas pendientes de asignación de Fecha Cita";
			this.chkPendienteAsignacionFecha.Visible = true;
            // 
            // tbHoraDesde
            // 
            this.tbHoraDesde.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbHoraDesde.AllowPromptAsInput = false;
            this.tbHoraDesde.AllowPromptAsInput = false;
			this.tbHoraDesde.Location = new System.Drawing.Point(263, 22);
			this.tbHoraDesde.Mask = "99:99";
			this.tbHoraDesde.Name = "tbHoraDesde";
			this.tbHoraDesde.PromptChar = ' ';
			this.tbHoraDesde.Size = new System.Drawing.Size(45, 20);
			this.tbHoraDesde.TabIndex = 1;
			// 
			// SdcFechaDesde
			// 
			this.SdcFechaDesde.CustomFormat = "dd/MM/yyyy";
			this.SdcFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFechaDesde.Location = new System.Drawing.Point(107, 22);
			this.SdcFechaDesde.Name = "SdcFechaDesde";
			this.SdcFechaDesde.Size = new System.Drawing.Size(105, 20);
			this.SdcFechaDesde.TabIndex = 0;
			this.SdcFechaDesde.Value = System.DateTime.Today;
            // 
            // tbHoraHasta
            // 
            this.tbHoraDesde.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbHoraHasta.AllowPromptAsInput = false;
			this.tbHoraHasta.Location = new System.Drawing.Point(263, 52);
			this.tbHoraHasta.Mask = "99:99";
			this.tbHoraHasta.Name = "tbHoraHasta";
			this.tbHoraHasta.PromptChar = ' ';
			this.tbHoraHasta.Size = new System.Drawing.Size(45, 20);
			this.tbHoraHasta.TabIndex = 3;
			// 
			// SdcFechaHasta
			// 
			this.SdcFechaHasta.CustomFormat = "dd/MM/yyyy";
			this.SdcFechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFechaHasta.Location = new System.Drawing.Point(107, 52);
			this.SdcFechaHasta.Name = "SdcFechaHasta";
			this.SdcFechaHasta.Size = new System.Drawing.Size(105, 20);
			this.SdcFechaHasta.TabIndex = 2;
			this.SdcFechaHasta.Value = System.DateTime.Today;
			// 
			// Label2
			// 
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.Location = new System.Drawing.Point(227, 55);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(33, 17);
			this.Label2.TabIndex = 16;
			this.Label2.Text = "Hora";
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(35, 56);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(69, 15);
			this.Label1.TabIndex = 15;
			this.Label1.Text = "Fecha Hasta";
			// 
			// Label19
			// 
			this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label19.Location = new System.Drawing.Point(227, 25);
			this.Label19.Name = "Label19";
			this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label19.Size = new System.Drawing.Size(33, 17);
			this.Label19.TabIndex = 14;
			this.Label19.Text = "Hora";
			// 
			// Label21
			// 
			this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label21.Location = new System.Drawing.Point(35, 26);
			this.Label21.Name = "Label21";
			this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label21.Size = new System.Drawing.Size(69, 15);
			this.Label21.TabIndex = 13;
			this.Label21.Text = "Fecha Desde";
			// 
			// CbCancelar
			// 
			this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCancelar.Location = new System.Drawing.Point(281, 258);
			this.CbCancelar.Name = "CbCancelar";
			this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCancelar.Size = new System.Drawing.Size(81, 29);
			this.CbCancelar.TabIndex = 11;
			this.CbCancelar.Text = "&Cancelar";
			this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
			// 
			// CbAceptar
			// 
			this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbAceptar.Location = new System.Drawing.Point(192, 258);
			this.CbAceptar.Name = "CbAceptar";
			this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbAceptar.Size = new System.Drawing.Size(81, 29);
			this.CbAceptar.TabIndex = 10;
			this.CbAceptar.Text = "&Aceptar";
			this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
			// 
			// listado_citas_externas
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(373, 297);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.CbCancelar);
			this.Controls.Add(this.CbAceptar);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "listado_citas_externas";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Listado de Citas Externas al Centro";
			this.Closed += new System.EventHandler(this.listado_citas_externas_Closed);
			this.Load += new System.EventHandler(this.listado_citas_externas_Load);
			this.Frame1.ResumeLayout(false);
			this.frmEstado.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeoptOrdena();
			InitializechIngAlt();
		}
		void InitializeoptOrdena()
		{
			this.optOrdena = new Telerik.WinControls.UI.RadRadioButton[3];
			this.optOrdena[2] = _optOrdena_2;
			this.optOrdena[1] = _optOrdena_1;
			this.optOrdena[0] = _optOrdena_0;
		}
		void InitializechIngAlt()
		{
			this.chIngAlt = new Telerik.WinControls.UI.RadCheckBox[2];
			this.chIngAlt[1] = _chIngAlt_1;
			this.chIngAlt[0] = _chIngAlt_0;
		}
		#endregion
	}
}