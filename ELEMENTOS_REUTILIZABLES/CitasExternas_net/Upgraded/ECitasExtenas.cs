using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;

namespace ECitaExterna
{
	internal static class ECitasExtenas
	{

		public static string VstGidenpac = String.Empty;
		public static int viGanoadme = 0;
		public static int viGnumadme = 0;
		//public static object LISTADO_CRYSTAL = null;
        public static Crystal LISTADO_CRYSTAL = null;
        public static string VstCodUsua = String.Empty;
		public static SqlConnection RcAdmision = null;
		public static string gUsuario = String.Empty;
		public static string gContrase�a = String.Empty;
		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public bool VfCabecera;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					return result;
			}
		}
		public static ECitasExtenas.CabCrystal VstrCrystal = ECitasExtenas.CabCrystal.CreateInstance();
		public static string PathString = String.Empty;
		public static string VstCrystal = String.Empty;
		//(maplaza)(14/05/2007)M�dulo que realiza la llamada a la dll
		public static string Modulo = String.Empty;
		//-----
		//(maplaza)(16/05/2007)Unidad de Enfermer�a o Servicio M�dico, para poder fitrar los datos de listado de Citas
		//Externas por unidad de enfermer�a o por servicio m�dico (dependiendo del m�dulo en el que se est�).
		public static string UniEnf_ServMed = String.Empty;
		//-----

		internal static void mnu_adm0113_Click(int Index)
		{
			citas_externas fA�adirCita = null;
			try
			{
				switch(Index)
				{
					case 0 : 
						menu_citas.DefInstance.ShowDialog(); 
						break;
					case 1 : 
						fA�adirCita = new citas_externas(); 
						menu_citas.DefInstance.VstOperacionCita = "A�ADIR_CITA"; 
						citas_externas tempLoadForm = fA�adirCita; 
						fA�adirCita.ShowDialog(); 
						break;
					case 2 :
                        /*INDRA jproche_TODO_5_5 18/05/2015
						menu_citas.DefInstance.SprPacientes_ComboSelChange(menu_citas.DefInstance.SprPacientes, new EventArgs()); 
                        */
						break;
					case 3 : 
						menu_citas.DefInstance.ProAnularCita(); 
						break;
					case 4 : 
						menu_citas.DefInstance.proMemorizarCita(); 
						docu_citas.DefInstance.ShowDialog(); 
						menu_citas.DefInstance.HabilitarBotones(); 
						break;
				}
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), VstCodUsua, "Menu_Admision:menu_adm0113_click", ex);
			}
		}

		internal static bool proLlamarDllImpresionNotificacion(int Numero, int a�o, ref string FechaCita, string Identificador, ref Crystal ObjectoListado)
		{
			//Public sub proLlamarDllImpresionNotificacion(FormularioOrigen As Object, _
			//Numero As Long, A�o As Long, Fechacita As String, Identificador As String, _
			//ObjectoListado As Object)

			//CREO QUE NO ES NECESARIO SEPARAR LA OBTENCION DE LA NOTIFICACION A FAMILIARES EN
			//UNA DLL APARTE YA QUE SE OBTIENE TANTO DESDE ADMISION COMO DESDE SERVICIOS MEDICOS
			//PERO SIEMPRE DESDE ESTA PANTALLA DE DOCUMENTOS DE CITAS EXTERNAS, POR LO QUE SIMPLEMENTE
			//VALDRIA TRAER LA IMPRESION DE LA NOTIFICACION A FAMILIARES EN ESTE BAS COMO FUNCION PUBLICA.

			//Dim claseNotificacion As Object
			//Set claseNotificacion = CreateObject("DLLNotificacion.clasenotificacion")
			//claseNotificacion.recibir FormularioOrigen, CLng(Numero), CInt(A�o), Fechacita, Identificador, _
			//'                        App.Path, ObjectoListado, gUsuario, gContrase�a


			bool result = false;
			string MostrarInformacionPaciente = "N";
			string stSql = "select valfanu1 from sconsglo where gconsglo= 'IJUCITEX' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, RcAdmision);
			DataSet rsAgenda = new DataSet();
			tempAdapter.Fill(rsAgenda);
			if (rsAgenda.Tables[0].Rows.Count != 0)
			{
				MostrarInformacionPaciente = Convert.ToString(rsAgenda.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
			}
			rsAgenda.Close();

			try
			{
               //ObjectoListado.ReportFileName = PathString + "\\agi192r1.rpt";
                ObjectoListado.ReportFileName = PathString + "\\agi192r1_CR11.rpt";
                ObjectoListado.Destination = Crystal.DestinationConstants.crptToPrinter; // 1;
				ObjectoListado.Connect = "DSN=" + VstCrystal + ";UID=" + gUsuario + ";PWD=" + gContrase�a;
				//ObjectoListado.SQLQuery = "select * from dpacient,acontcex,dprovinc,dpoblaci where " & _
				//" acontcex.ganoadme=" & a�o & " and " & _
				//" acontcex.gnumadme=" & Numero & " and " & _
				//" acontcex.gidenpac='" & Identificador & "' and " & _
				//" acontcex.gidenpac=dpacient.gidenpac and " & _
				//" acontcex.gpoblaci*=dpoblaci.gpoblaci and " & _
				//" acontcex.gprovinc*=dprovinc.gprovinc and " & _
				//" fcitaext=" & FormatFechaHMS(FechaCita)

				object tempRefParam = FechaCita;
				ObjectoListado.SQLQuery = "select * from acontcex " + 
				                          " INNER JOIN dpacient ON acontcex.gidenpac = dpacient.gidenpac " + 
				                          " LEFT JOIN dprovinc ON acontcex.gprovinc = dprovinc.gprovinc " + 
				                          " LEFT JOIN dpoblaci ON acontcex.gpoblaci = dpoblaci.gpoblaci " + 
				                          " where " + 
				                          "  acontcex.ganoadme=" + a�o.ToString() + " and " + 
				                          "  acontcex.gnumadme=" + Numero.ToString() + " and " + 
				                          "  acontcex.gidenpac='" + Identificador + "' and " + 
				                          "  fcitaext=" + Serrores.FormatFechaHMS(tempRefParam);
				FechaCita = Convert.ToString(tempRefParam);

				ObjectoListado.WindowTitle = "Notificaci�n a familiares";
                ObjectoListado.WindowState = Crystal.WindowStateConstants.crptNormal; // 2;
				//ObjectoListado.Formulas[1] = "InformacionAlPaciente = \"" + MostrarInformacionPaciente + "\"";
                ObjectoListado.Formulas["InformacionAlPaciente"] = "\"" + MostrarInformacionPaciente + "\"";
                ObjectoListado.Action = 1; //  La Dll rellena todos los datos del rpt a falta del action
				//ObjectoListado.PageCount();
				ObjectoListado.Reset();
				ObjectoListado = null;
               
                return false;
			}
			catch(Exception ex)
			{
				result = true;
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), VstCodUsua, "Citas_externas:proLlamarDllImpresionNotificacion", ex);
                return result;
			}
		}



		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************


			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RcAdmision);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			VstrCrystal.VstGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, RcAdmision);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			VstrCrystal.VstNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, RcAdmision);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			VstrCrystal.VstDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tRrCrystal.Close();
			VstrCrystal.VfCabecera = true;
		}

		internal static void ProCargaCabecera(dynamic ob)
		{
			DataSet rr = null;
			string SQSoci = String.Empty;
			DataSet RrSoci = null;
			DataSet tRramovifin = null;
			string tstNuevo = String.Empty;

			//sql = "SELECT " _
			//& " DPACIENT.DAPE1PAC,DPACIENT.DAPE2PAC,DPACIENT.DNOMBPAC,dpacient.gidenpac, " _
			//& " Dpacient.GSOCIEDA,DPACIENT.IFILPROV,DPACIENT.NAFILIAC, " _
			//& " AEPISADM.gnumadme,AEPISADM.ganoadme " _
			//& " FROM " _
			//& " DPACIENT, AEPISADM, DENTIPAC" _
			//& " WHERE " _
			//& " DPACIENT.GIDENPAC= '" & VstGidenpac & "' and" _
			//& " DPACIENT.GIDENPAC = AEPISADM.GIDENPAC AND " _
			//& " AEPISADM.GIDENPAC *= DENTIPAC.GIDENPAC AND " _
			//& " ganoadme= " & viGanoadme & " AND gnumadme=" & viGnumadme

			string Sql = "SELECT " + " DPACIENT.DAPE1PAC,DPACIENT.DAPE2PAC,DPACIENT.DNOMBPAC,dpacient.gidenpac, " + " Dpacient.GSOCIEDA,DPACIENT.IFILPROV,DPACIENT.NAFILIAC, " + " AEPISADM.gnumadme,AEPISADM.ganoadme,DPACIENT.inecacom " + " FROM " + " AEPISADM " + "   INNER JOIN DPACIENT ON DPACIENT.GIDENPAC = AEPISADM.GIDENPAC " + "   LEFT JOIN DENTIPAC ON AEPISADM.GIDENPAC = DENTIPAC.GIDENPAC " + " WHERE " + " DPACIENT.GIDENPAC= '" + VstGidenpac + "' and" + " ganoadme= " + viGanoadme.ToString() + " AND gnumadme=" + viGnumadme.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, RcAdmision);
			DataSet rrpaciente = new DataSet();
			tempAdapter.Fill(rrpaciente);
			if (rrpaciente.Tables[0].Rows.Count != 0)
			{	
				Sql = "SELECT GHISTORIA,GSERARCH FROM HSERARCH, HDOSSIER " + " WHERE GIDENPAC = '" + Convert.ToString(rrpaciente.Tables[0].Rows[0]["Gidenpac"]) + "' AND " + " GSERPROPIET = GSERARCH AND " + " ICENTRAL='S' and icontenido='H'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, RcAdmision);
				rr = new DataSet();
				tempAdapter_2.Fill(rr);
				if (rr.Tables[0].Rows.Count == 1)
				{
					ob.LbHistoria.Text = Conversion.Str(rr.Tables[0].Rows[0]["GHISTORIA"]);
				}
				else
				{
					ob.LbHistoria.Text = "";
				}
				rr.Close();

				if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["DAPE1PAC"]) != "")
				{
					ob.LbPaciente.Text = Convert.ToString(rrpaciente.Tables[0].Rows[0]["DAPE1PAC"]).Trim();
				}
				if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["DAPE2PAC"]) != "")
				{
					ob.LbPaciente.Text = Convert.ToString(ob.LbPaciente.Text) + " " + Convert.ToString(rrpaciente.Tables[0].Rows[0]["DAPE2PAC"]).Trim();
				}
				if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["DNOMBPAC"]) != "")
				{
					ob.LbPaciente.Text = Convert.ToString(ob.LbPaciente.Text) + " " + Convert.ToString(rrpaciente.Tables[0].Rows[0]["DNOMBPAC"]).Trim();
				}
				if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["NAFILIAC"]) != "")
				{
					ob.LbNumsegu.Text = rrpaciente.Tables[0].Rows[0]["NAFILIAC"].ToString();
				}

				if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["inecacom"]) == "N")
				{
					ob.optAcomp[0].IsChecked = true;
				}
				else if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["inecacom"]) == "C")
				{ 
					ob.optAcomp[1].IsChecked = true;
				}
				else if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["inecacom"]) == "F")
				{ 
					ob.optAcomp[2].IsChecked = true;
				}
				else if (Convert.ToString(rrpaciente.Tables[0].Rows[0]["inecacom"]) == "E")
				{ 
					ob.optAcomp[3].IsChecked = true;
				}

				SQSoci = "SELECT Fmovimie,gsocieda " + " FROM  AMOVIFIN " + " WHERE " + "  AMOVIFIN.GNUMREGI=" + Convert.ToString(rrpaciente.Tables[0].Rows[0]["GNUMADME"]) + " AND " + "  AMOVIFIN.GANOREGI= " + Convert.ToString(rrpaciente.Tables[0].Rows[0]["GANOADME"]) + " order by fmovimie";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQSoci, RcAdmision);
				tRramovifin = new DataSet();
				tempAdapter_3.Fill(tRramovifin);
				tstNuevo = Convert.ToString(tRramovifin.Tables[0].Rows[tRramovifin.MoveLast(null)]["gsocieda"]);
				tRramovifin.Close();

				SQSoci = "SELECT NNUMERI1,valfanu1 FROM  SCONSGLO WHERE gconsglo = 'SEGURSOC'";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQSoci, RcAdmision);
				RrSoci = new DataSet();
				tempAdapter_4.Fill(RrSoci);
				if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == StringsHelper.ToDoubleSafe(tstNuevo))
				{   //Seguridad Social
					ob.LbFinancia.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
					return;
				}
				RrSoci.Close();


				SQSoci = "SELECT NNUMERI1,VALFANU1 FROM  SCONSGLO WHERE gconsglo = 'PRIVADO'";
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQSoci, RcAdmision);
				RrSoci = new DataSet();
				tempAdapter_5.Fill(RrSoci);
				if (Convert.ToInt32(RrSoci.Tables[0].Rows[0]["NNUMERI1"]) == StringsHelper.ToDoubleSafe(tstNuevo))
				{ // Privado
					ob.LbFinancia.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["VALFANU1"]).Trim();
					return;
				}
				RrSoci.Close();

				SQSoci = "SELECT dsocieda,IREGALOJ FROM DSOCIEDA WHERE gSOCIEDA = " + tstNuevo;
				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQSoci, RcAdmision);
				RrSoci = new DataSet();
				tempAdapter_6.Fill(RrSoci);
				ob.LbFinancia.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["DSOCIEDA"]).Trim();
				RrSoci.Close();
			}
		}

		internal static string UnidadEnfermeria(string iCodUnidad)
		{
			//devuelve la descripci�n de la unidad de enfermeria
			string result = String.Empty;
			string Sql = String.Empty;
			DataSet RrDatos = null;

			result = "";
			if (!Convert.IsDBNull(iCodUnidad))
			{
				Sql = "select dunidenf " + "from Dunienfe " + "where Dunienfe.gunidenf ='" + iCodUnidad + "' ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, RcAdmision);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				if (RrDatos.Tables[0].Rows.Count != 0)
				{ //si hay datos se cargan en el grid
					result = (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dunidenf"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["dunidenf"]).Trim() + " ";
				}
				RrDatos.Close();
			}
			return result;
		}

		internal static string Servicio(string iCodServicio)
		{
			//devuelve la descripci�n de la unidad de enfermeria
			string result = String.Empty;
			string Sql = String.Empty;
			DataSet RrDatos = null;

			result = "";
			if (!Convert.IsDBNull(iCodServicio))
			{
				Sql = "select dnomserv " + "from Dservici " + "where Dservici.gservici ='" + iCodServicio + "' ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, RcAdmision);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				if (RrDatos.Tables[0].Rows.Count != 0)
				{ //si hay datos se cargan en el grid
					result = (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dnomserv"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["dnomserv"]).Trim() + " ";
				}
				RrDatos.Close();
			}
			return result;
		}
	}
}