using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ECitaExterna
{
	partial class docu_citas
	{

		#region "Upgrade Support "
		private static docu_citas m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static docu_citas DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new docu_citas();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbImprimir", "cbCancelar", "lbPaciente", "Label1", "Frame3", "ChbVolanteA", "ChbNotificacion", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadCheckBox ChbVolanteA;
		public Telerik.WinControls.UI.RadCheckBox ChbNotificacion;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(docu_citas));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.cbImprimir = new Telerik.WinControls.UI.RadButton();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.ChbVolanteA = new Telerik.WinControls.UI.RadCheckBox();
			this.ChbNotificacion = new Telerik.WinControls.UI.RadCheckBox();
			this.Frame1.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.SuspendLayout();
			// 
			// cbImprimir
			// 
			this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbImprimir.Enabled = false;
			this.cbImprimir.Location = new System.Drawing.Point(189, 131);
			this.cbImprimir.Name = "cbImprimir";
			this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbImprimir.Size = new System.Drawing.Size(81, 32);
			this.cbImprimir.TabIndex = 7;
			this.cbImprimir.Text = "&Imprimir";
			this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(278, 131);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 32);
			this.cbCancelar.TabIndex = 6;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.Frame3);
			this.Frame1.Controls.Add(this.ChbVolanteA);
			this.Frame1.Controls.Add(this.ChbNotificacion);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(0, 0);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(359, 125);
			this.Frame1.TabIndex = 0;
			this.Frame1.Visible = true;
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.lbPaciente);
			this.Frame3.Controls.Add(this.Label1);
			this.Frame3.Enabled = true;
			this.Frame3.Location = new System.Drawing.Point(8, 13);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(335, 45);
			this.Frame3.TabIndex = 3;
			this.Frame3.Text = "Paciente";
			this.Frame3.Visible = true;
			// 
			// lbPaciente
			// 
			this.lbPaciente.Enabled = false;
			this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbPaciente.Location = new System.Drawing.Point(60, 17);
			this.lbPaciente.Name = "lbPaciente";
			this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbPaciente.Size = new System.Drawing.Size(268, 19);
			this.lbPaciente.TabIndex = 5;
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(9, 20);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(43, 19);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "Nombre";
			// 
			// ChbVolanteA
			// 
			this.ChbVolanteA.CausesValidation = true;
			this.ChbVolanteA.CheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.ChbVolanteA.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.ChbVolanteA.Cursor = System.Windows.Forms.Cursors.Default;
			this.ChbVolanteA.Enabled = true;
			this.ChbVolanteA.Location = new System.Drawing.Point(14, 97);
			this.ChbVolanteA.Name = "ChbVolanteA";
			this.ChbVolanteA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ChbVolanteA.Size = new System.Drawing.Size(168, 20);
			this.ChbVolanteA.TabIndex = 2;
			this.ChbVolanteA.TabStop = true;
			this.ChbVolanteA.Text = "Volante para la ambulancia";
			this.ChbVolanteA.Visible = true;
			this.ChbVolanteA.CheckStateChanged += new System.EventHandler(this.ChbVolanteA_CheckStateChanged);
			// 
			// ChbNotificacion
			// 
			this.ChbNotificacion.CausesValidation = true;
			this.ChbNotificacion.CheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.ChbNotificacion.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.ChbNotificacion.Cursor = System.Windows.Forms.Cursors.Default;
			this.ChbNotificacion.Enabled = true;
			this.ChbNotificacion.Location = new System.Drawing.Point(14, 70);
			this.ChbNotificacion.Name = "ChbNotificacion";
			this.ChbNotificacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ChbNotificacion.Size = new System.Drawing.Size(176, 20);
			this.ChbNotificacion.TabIndex = 1;
			this.ChbNotificacion.TabStop = true;
			this.ChbNotificacion.Text = "Notificación a familiares";
			this.ChbNotificacion.Visible = true;
			this.ChbNotificacion.CheckStateChanged += new System.EventHandler(this.ChbNotificacion_CheckStateChanged);
			// 
			// docu_citas
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(368, 169);
			this.Controls.Add(this.cbImprimir);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.Frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "docu_citas";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DOCUMENTOS - AGI192F1";
			this.Closed += new System.EventHandler(this.docu_citas_Closed);
			this.Load += new System.EventHandler(this.docu_citas_Load);
			this.Frame1.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}