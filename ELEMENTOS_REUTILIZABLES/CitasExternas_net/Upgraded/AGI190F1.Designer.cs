using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ECitaExterna
{
	partial class menu_citas
	{

		#region "Upgrade Support "
		private static menu_citas m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static menu_citas DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new menu_citas();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_Toolbar1_Button1", "_Toolbar1_Button2", "_Toolbar1_Button3", "Toolbar1", "cbBuscar", "CheckTodos", "SdcFechaDesde", "Label8", "Frame4", "Line4", "Label4", "Line5", "Label5", "Label7", "Line6", "Frame3", "_optAcomp_3", "_optAcomp_2", "_optAcomp_1", "_optAcomp_0", "cbEstablecer", "FrmAcompPref", "LbNumsegu", "LbFinancia", "LbHistoria", "Label10", "LbPaciente", "Label11", "Label12", "Label28", "Frame1", "SprPacientes", "Frame2", "cbCerrar", "Label6", "LbAltaHospi", "Label3", "Label2", "Label1", "ImageList1", "optAcomp", "ShapeContainer1", "commandButtonHelper1", "SprPacientes_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button1;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button2;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button3;
		public Telerik.WinControls.UI.RadCommandBar Toolbar1;
        private Telerik.WinControls.UI.CommandBarRowElement radCommandBarLineElement1;
        public Telerik.WinControls.UI.CommandBarStripElement radCommandBarStripElement1;
        public Telerik.WinControls.UI.RadButton cbBuscar;
		public Telerik.WinControls.UI.RadCheckBox CheckTodos;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaDesde;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line4;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line5;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line6;
		public System.Windows.Forms.Panel Frame3;
		private Telerik.WinControls.UI.RadRadioButton _optAcomp_3;
		private Telerik.WinControls.UI.RadRadioButton _optAcomp_2;
		private Telerik.WinControls.UI.RadRadioButton _optAcomp_1;
		private Telerik.WinControls.UI.RadRadioButton _optAcomp_0;
		public Telerik.WinControls.UI.RadButton cbEstablecer;
		public Telerik.WinControls.UI.RadGroupBox FrmAcompPref;
		public Telerik.WinControls.UI.RadTextBox LbNumsegu;
		public Telerik.WinControls.UI.RadTextBox LbFinancia;
		public Telerik.WinControls.UI.RadTextBox LbHistoria;
		public Telerik.WinControls.UI.RadLabel Label10;
		public Telerik.WinControls.UI.RadTextBox LbPaciente;
		public Telerik.WinControls.UI.RadLabel Label11;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public UpgradeHelpers.Spread.FpSpread SprPacientes;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel LbAltaHospi;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public System.Windows.Forms.ImageList ImageList1;
		public Telerik.WinControls.UI.RadRadioButton[] optAcomp = new Telerik.WinControls.UI.RadRadioButton[4];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            
            this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu_citas));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.Toolbar1 = new Telerik.WinControls.UI.RadCommandBar();
			this._Toolbar1_Button1 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button2 = new Telerik.WinControls.UI.CommandBarButton();
			this._Toolbar1_Button3 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarLineElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
			this.cbBuscar = new Telerik.WinControls.UI.RadButton();
			this.CheckTodos = new Telerik.WinControls.UI.RadCheckBox();
			this.SdcFechaDesde = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Label8 = new Telerik.WinControls.UI.RadLabel();
			this.Frame3 = new System.Windows.Forms.Panel();
			this.Line4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.Line5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.Label7 = new Telerik.WinControls.UI.RadLabel();
			this.Line6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.FrmAcompPref = new Telerik.WinControls.UI.RadGroupBox();
			this._optAcomp_3 = new Telerik.WinControls.UI.RadRadioButton();
			this._optAcomp_2 = new Telerik.WinControls.UI.RadRadioButton();
			this._optAcomp_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._optAcomp_0 = new Telerik.WinControls.UI.RadRadioButton();
			this.cbEstablecer = new Telerik.WinControls.UI.RadButton();
			this.LbNumsegu = new Telerik.WinControls.UI.RadTextBox();
			this.LbFinancia = new Telerik.WinControls.UI.RadTextBox();
			this.LbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.Label10 = new Telerik.WinControls.UI.RadLabel();
			this.LbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label11 = new Telerik.WinControls.UI.RadLabel();
			this.Label12 = new Telerik.WinControls.UI.RadLabel();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.SprPacientes = new UpgradeHelpers.Spread.FpSpread();
			this.cbCerrar = new Telerik.WinControls.UI.RadButton();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.LbAltaHospi = new Telerik.WinControls.UI.RadLabel();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.ImageList1 = new System.Windows.Forms.ImageList();
			this.Toolbar1.SuspendLayout();
			this.Frame4.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.FrmAcompPref.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
			// 
			// ShapeContainer1
			// 
			this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
			this.ShapeContainer1.Size = new System.Drawing.Size(165, 65);
			this.ShapeContainer1.Shapes.Add(Line4);
			this.ShapeContainer1.Shapes.Add(Line5);
			this.ShapeContainer1.Shapes.Add(Line6);
			// 
			// Toolbar1
			// 
			this.Toolbar1.Dock = System.Windows.Forms.DockStyle.Top;
			this.Toolbar1.ImageList = ImageList1;
			this.Toolbar1.Location = new System.Drawing.Point(0, 0);
			this.Toolbar1.Name = "Toolbar1";
			this.Toolbar1.ShowItemToolTips = true;
			this.Toolbar1.Size = new System.Drawing.Size(981, 28);
			this.Toolbar1.TabIndex = 1;
            this.Toolbar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.radCommandBarLineElement1});
            //
            // radCommandBarLineElement1
            // 
            this.radCommandBarLineElement1.BorderLeftShadowColor = System.Drawing.Color.Empty;
            this.radCommandBarLineElement1.DisplayName = null;
            this.radCommandBarLineElement1.MinSize = new System.Drawing.Size(25, 25);
            this.radCommandBarLineElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.radCommandBarStripElement1});
            this.radCommandBarLineElement1.Text = "";
            // 
            // radCommandBarStripElement1
            // 
            this.radCommandBarStripElement1.DisplayName = "Commands Strip";
            this.radCommandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this._Toolbar1_Button1,
            this._Toolbar1_Button2,
            this._Toolbar1_Button3});
            this.radCommandBarStripElement1.Name = "radCommandBarStripElement1";
            // 
            // 
            // 
            this.radCommandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.radCommandBarStripElement1.StretchHorizontally = true;
            this.radCommandBarStripElement1.StretchVertically = true;
            this.radCommandBarStripElement1.Text = "";
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.radCommandBarStripElement1.GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // _Toolbar1_Button1
            // 
            this._Toolbar1_Button1.ImageIndex = 2;
			this._Toolbar1_Button1.Name = "";
			this._Toolbar1_Button1.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button1.Tag = "";
			this._Toolbar1_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button1.ToolTipText = "Nueva";
			this._Toolbar1_Button1.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button2
			// 
			this._Toolbar1_Button2.Enabled = false;
			this._Toolbar1_Button2.ImageIndex = 0;
			this._Toolbar1_Button2.Name = "";
			this._Toolbar1_Button2.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button2.Tag = "";
			this._Toolbar1_Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button2.ToolTipText = "Documentos";
			this._Toolbar1_Button2.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button3
            // 
            this._Toolbar1_Button3.Enabled = false;
			this._Toolbar1_Button3.ImageIndex = 3;
            this._Toolbar1_Button3.Name = "";
			this._Toolbar1_Button3.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button3.Tag = "";
			this._Toolbar1_Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button3.ToolTipText = "Anulaci�n";
			this._Toolbar1_Button3.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.cbBuscar);
			this.Frame4.Controls.Add(this.CheckTodos);
			this.Frame4.Controls.Add(this.SdcFechaDesde);
			this.Frame4.Controls.Add(this.Label8);
			this.Frame4.Enabled = true;
			this.Frame4.Location = new System.Drawing.Point(810, 32);
			this.Frame4.Name = "Frame4";
			this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame4.Size = new System.Drawing.Size(163, 84);
			this.Frame4.TabIndex = 30;
			this.Frame4.Text = "Selecci�n de Fecha";
			this.Frame4.Visible = true;
			// 
			// cbBuscar
			// 
			this.cbBuscar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbBuscar.Image = (System.Drawing.Image) resources.GetObject("cbBuscar.Image");
			this.cbBuscar.Location = new System.Drawing.Point(130, 42);
			this.cbBuscar.Name = "cbBuscar";
			this.cbBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbBuscar.Size = new System.Drawing.Size(27, 24);
			this.cbBuscar.TabIndex = 9;
			this.cbBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.cbBuscar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.cbBuscar.Click += new System.EventHandler(this.cbBuscar_Click);
			// 
			// CheckTodos
			// 
			this.CheckTodos.CausesValidation = true;
			this.CheckTodos.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
			this.CheckTodos.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.CheckTodos.Cursor = System.Windows.Forms.Cursors.Default;
			this.CheckTodos.Enabled = true;
			this.CheckTodos.Location = new System.Drawing.Point(6, 51);
			this.CheckTodos.Name = "CheckTodos";
			this.CheckTodos.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CheckTodos.Size = new System.Drawing.Size(54, 13);
			this.CheckTodos.TabIndex = 8;
			this.CheckTodos.TabStop = true;
			this.CheckTodos.Text = "Todos";
			this.CheckTodos.Visible = true;
			// 
			// SdcFechaDesde
			// 
			this.SdcFechaDesde.CustomFormat = "dd/MM/yyyy";
			this.SdcFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFechaDesde.Location = new System.Drawing.Point(45, 18);
			this.SdcFechaDesde.Name = "SdcFechaDesde";
			this.SdcFechaDesde.Size = new System.Drawing.Size(111, 19);
			this.SdcFechaDesde.TabIndex = 7;
			// 
			// Label8
			// 
			this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label8.Location = new System.Drawing.Point(6, 22);
			this.Label8.Name = "Label8";
			this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label8.Size = new System.Drawing.Size(45, 13);
			this.Label8.TabIndex = 31;
			this.Label8.Text = "Desde:";
			// 
			// Frame3
			// 
			this.Frame3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Frame3.Controls.Add(this.Label4);
			this.Frame3.Controls.Add(this.Label5);
			this.Frame3.Controls.Add(this.Label7);
			this.Frame3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Frame3.Enabled = true;
			this.Frame3.Location = new System.Drawing.Point(8, 513);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(165, 65);
			this.Frame3.TabIndex = 26;
			this.Frame3.Visible = true;
			// 
			// Line4
			// 
			this.Line4.BorderColor = System.Drawing.Color.Red;
			this.Line4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line4.BorderWidth = 1;
			this.Line4.Enabled = false;
			this.Line4.Name = "Line4";
			this.Line4.Visible = true;
			this.Line4.X1 = (int) 0;
			this.Line4.X2 = (int) 48;
			this.Line4.Y1 = (int) 8;
			this.Line4.Y2 = (int) 8;
			// 
			// Label4
			// 
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label4.ForeColor = System.Drawing.Color.Red;
			this.Label4.Location = new System.Drawing.Point(64, 0);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(89, 11);
			this.Label4.TabIndex = 29;
			this.Label4.Text = "Citas anuladas";
			// 
			// Line5
			// 
			this.Line5.BorderColor = System.Drawing.Color.FromArgb(0, 0, 192);
			this.Line5.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line5.BorderWidth = 1;
			this.Line5.Enabled = false;
			this.Line5.Name = "Line5";
			this.Line5.Visible = true;
			this.Line5.X1 = (int) 0;
			this.Line5.X2 = (int) 48;
			this.Line5.Y1 = (int) 24;
			this.Line5.Y2 = (int) 24;
			// 
			// Label5
			// 
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label5.ForeColor = System.Drawing.Color.FromArgb(0, 0, 192);
			this.Label5.Location = new System.Drawing.Point(64, 16);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(89, 15);
			this.Label5.TabIndex = 28;
			this.Label5.Text = "Citas realizadas";
			// 
			// Label7
			// 
			this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label7.ForeColor = System.Drawing.Color.Green;
			this.Label7.Location = new System.Drawing.Point(64, 32);
			this.Label7.Name = "Label7";
			this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label7.Size = new System.Drawing.Size(89, 23);
			this.Label7.TabIndex = 27;
			this.Label7.Text = "Citas notificadas";
			// 
			// Line6
			// 
			this.Line6.BorderColor = System.Drawing.Color.Green;
			this.Line6.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line6.BorderWidth = 1;
			this.Line6.Enabled = false;
			this.Line6.Name = "Line6";
			this.Line6.Visible = true;
			this.Line6.X1 = (int) 0;
			this.Line6.X2 = (int) 48;
			this.Line6.Y1 = (int) 40;
			this.Line6.Y2 = (int) 40;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.FrmAcompPref);
			this.Frame1.Controls.Add(this.LbNumsegu);
			this.Frame1.Controls.Add(this.LbFinancia);
			this.Frame1.Controls.Add(this.LbHistoria);
			this.Frame1.Controls.Add(this.Label10);
			this.Frame1.Controls.Add(this.LbPaciente);
			this.Frame1.Controls.Add(this.Label11);
			this.Frame1.Controls.Add(this.Label12);
			this.Frame1.Controls.Add(this.Label28);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(7, 32);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(801, 84);
			this.Frame1.TabIndex = 16;
			this.Frame1.Text = "Datos del Paciente";
			this.Frame1.Visible = true;
			// 
			// FrmAcompPref
			// 
			this.FrmAcompPref.Controls.Add(this._optAcomp_3);
			this.FrmAcompPref.Controls.Add(this._optAcomp_2);
			this.FrmAcompPref.Controls.Add(this._optAcomp_1);
			this.FrmAcompPref.Controls.Add(this._optAcomp_0);
			this.FrmAcompPref.Controls.Add(this.cbEstablecer);
			this.FrmAcompPref.Enabled = true;
			this.FrmAcompPref.Location = new System.Drawing.Point(402, 39);
			this.FrmAcompPref.Name = "FrmAcompPref";
			this.FrmAcompPref.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmAcompPref.Size = new System.Drawing.Size(393, 38);
			this.FrmAcompPref.TabIndex = 32;
			this.FrmAcompPref.Text = "Acompa�ante preferente del paciente";
			this.FrmAcompPref.Visible = true;
			// 
			// _optAcomp_3
			// 
			this._optAcomp_3.CausesValidation = true;
			this._optAcomp_3.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_3.IsChecked = false;
			this._optAcomp_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._optAcomp_3.Enabled = true;
			this._optAcomp_3.Location = new System.Drawing.Point(256, 15);
			this._optAcomp_3.Name = "_optAcomp_3";
			this._optAcomp_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optAcomp_3.Size = new System.Drawing.Size(59, 19);
			this._optAcomp_3.TabIndex = 5;
			this._optAcomp_3.TabStop = true;
			this._optAcomp_3.Text = "Externo";
			this._optAcomp_3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_3.Visible = true;
            this._optAcomp_3.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optAcomp_CheckedChanged);
            // 
            // _optAcomp_2
            // 
            this._optAcomp_2.CausesValidation = true;
			this._optAcomp_2.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_2.IsChecked = false;
			this._optAcomp_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._optAcomp_2.Enabled = true;
			this._optAcomp_2.Location = new System.Drawing.Point(193, 15);
			this._optAcomp_2.Name = "_optAcomp_2";
			this._optAcomp_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optAcomp_2.Size = new System.Drawing.Size(55, 19);
			this._optAcomp_2.TabIndex = 4;
			this._optAcomp_2.TabStop = true;
			this._optAcomp_2.Text = "Familiar";
			this._optAcomp_2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_2.Visible = true;
            this._optAcomp_2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optAcomp_CheckedChanged);
            // 
            // _optAcomp_1
            // 
            this._optAcomp_1.CausesValidation = true;
			this._optAcomp_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_1.IsChecked = false;
			this._optAcomp_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._optAcomp_1.Enabled = true;
			this._optAcomp_1.Location = new System.Drawing.Point(88, 15);
			this._optAcomp_1.Name = "_optAcomp_1";
			this._optAcomp_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optAcomp_1.Size = new System.Drawing.Size(95, 19);
			this._optAcomp_1.TabIndex = 3;
			this._optAcomp_1.TabStop = true;
			this._optAcomp_1.Text = "Personal Centro";
			this._optAcomp_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_1.Visible = true;
            this._optAcomp_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optAcomp_CheckedChanged);
            // 
            // _optAcomp_0
            // 
            this._optAcomp_0.CausesValidation = true;
			this._optAcomp_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_0.IsChecked = false;
			this._optAcomp_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._optAcomp_0.Enabled = true;
			this._optAcomp_0.Location = new System.Drawing.Point(6, 15);
			this._optAcomp_0.Name = "_optAcomp_0";
			this._optAcomp_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optAcomp_0.Size = new System.Drawing.Size(74, 19);
			this._optAcomp_0.TabIndex = 2;
			this._optAcomp_0.TabStop = true;
			this._optAcomp_0.Text = "No Precisa";
			this._optAcomp_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._optAcomp_0.Visible = true;
            this._optAcomp_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.optAcomp_CheckedChanged);
            // 
            // cbEstablecer
            // 
            this.cbEstablecer.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbEstablecer.Location = new System.Drawing.Point(325, 12);
			this.cbEstablecer.Name = "cbEstablecer";
			this.cbEstablecer.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbEstablecer.Size = new System.Drawing.Size(62, 21);
			this.cbEstablecer.TabIndex = 6;
			this.cbEstablecer.Text = "Establecer";
			this.cbEstablecer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbEstablecer.Click += new System.EventHandler(this.cbEstablecer_Click);
			// 
			// LbNumsegu
			// 
			this.LbNumsegu.Enabled = false;
			this.LbNumsegu.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbNumsegu.Location = new System.Drawing.Point(665, 16);
			this.LbNumsegu.Name = "LbNumsegu";
			this.LbNumsegu.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbNumsegu.Size = new System.Drawing.Size(131, 17);
			this.LbNumsegu.TabIndex = 24;
			// 
			// LbFinancia
			// 
			this.LbFinancia.Enabled = false;
			this.LbFinancia.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbFinancia.Location = new System.Drawing.Point(97, 51);
			this.LbFinancia.Name = "LbFinancia";
			this.LbFinancia.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbFinancia.Size = new System.Drawing.Size(293, 17);
			this.LbFinancia.TabIndex = 23;
			// 
			// LbHistoria
			// 
			this.LbHistoria.Enabled = false;
            this.LbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbHistoria.Location = new System.Drawing.Point(482, 16);
			this.LbHistoria.Name = "LbHistoria";
			this.LbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbHistoria.Size = new System.Drawing.Size(97, 17);
			this.LbHistoria.TabIndex = 22;
			// 
			// Label10
			// 
			this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label10.Location = new System.Drawing.Point(401, 18);
			this.Label10.Name = "Label10";
			this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label10.Size = new System.Drawing.Size(75, 17);
			this.Label10.TabIndex = 21;
			this.Label10.Text = "Historia cl�nica:";
			// 
			// LbPaciente
			// 
			this.LbPaciente.Enabled = false;
            this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbPaciente.Location = new System.Drawing.Point(59, 16);
			this.LbPaciente.Name = "LbPaciente";
			this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbPaciente.Size = new System.Drawing.Size(331, 17);
			this.LbPaciente.TabIndex = 20;
			// 
			// Label11
			// 
			this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label11.Location = new System.Drawing.Point(588, 18);
			this.Label11.Name = "Label11";
			this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label11.Size = new System.Drawing.Size(75, 17);
			this.Label11.TabIndex = 19;
			this.Label11.Text = "N� asegurado:";
			// 
			// Label12
			// 
			this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label12.Location = new System.Drawing.Point(6, 53);
			this.Label12.Name = "Label12";
			this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label12.Size = new System.Drawing.Size(91, 17);
			this.Label12.TabIndex = 18;
			this.Label12.Text = "Ent. Financiadora:";
			// 
			// Label28
			// 
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label28.Location = new System.Drawing.Point(8, 18);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(59, 17);
			this.Label28.TabIndex = 17;
			this.Label28.Text = "Paciente:";
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.SprPacientes);
			this.Frame2.Enabled = true;
			this.Frame2.Location = new System.Drawing.Point(8, 117);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(965, 391);
			this.Frame2.TabIndex = 11;
			this.Frame2.Text = "Citas existentes";
			this.Frame2.Visible = true;
            // 
            // SprPacientes
            // 
            gridViewTextBoxColumn1.HeaderText = "Fecha de Apunte";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 100;

            gridViewTextBoxColumn2.HeaderText = "Fecha Cita";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 100;

            gridViewTextBoxColumn3.HeaderText = "Centro";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 233;

            gridViewTextBoxColumn4.HeaderText = "Prueba";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 233;

            gridViewTextBoxColumn5.HeaderText = "Motivo Anulaci�n";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 233;

            gridViewTextBoxColumn6.HeaderText = "CodigoCausaAmbulc";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 80;

            gridViewTextBoxColumn7.HeaderText = "CodigoModoTrans";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.Width = 80;

            this.SprPacientes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[]
           {   gridViewTextBoxColumn1,
                gridViewTextBoxColumn2,
                gridViewTextBoxColumn3,
                gridViewTextBoxColumn4,
                gridViewTextBoxColumn5,
                gridViewTextBoxColumn6,
                gridViewTextBoxColumn7
            
           });
            this.SprPacientes.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.SprPacientes.MasterTemplate.EnableSorting = false;
            this.SprPacientes.MasterTemplate.ReadOnly = true;
            this.SprPacientes.Location = new System.Drawing.Point(9, 16);
			this.SprPacientes.Name = "SprPacientes";
			this.SprPacientes.Size = new System.Drawing.Size(943, 363);
			this.SprPacientes.TabIndex = 10;
            this.SprPacientes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPacientes_CellClick);
            this.SprPacientes.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPacientes_CellDoubleClick);
            this.SprPacientes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SprPacientes_MouseDown);
            this.SprPacientes.TopLeftChange += new UpgradeHelpers.Spread.TopLeftChangeEventHandler(this.SprPacientes_TopLeftChange);
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCerrar.Location = new System.Drawing.Point(893, 521);
			this.cbCerrar.Name = "cbCerrar";
			this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCerrar.Size = new System.Drawing.Size(81, 29);
			this.cbCerrar.TabIndex = 0;
			this.cbCerrar.Text = "C&errar";
			this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
			// 
			// Label6
			// 
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label6.ForeColor = System.Drawing.Color.FromArgb(0, 0, 192);
			this.Label6.Location = new System.Drawing.Point(0, 0);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(89, 17);
			this.Label6.TabIndex = 25;
			this.Label6.Text = "Citas realizadas";
			// 
			// LbAltaHospi
			// 
			this.LbAltaHospi.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbAltaHospi.Location = new System.Drawing.Point(45, 0);
			this.LbAltaHospi.Name = "LbAltaHospi";
			this.LbAltaHospi.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbAltaHospi.Size = new System.Drawing.Size(168, 13);
			this.LbAltaHospi.TabIndex = 15;
			this.LbAltaHospi.Text = "Pacientes dados de alta en el d�a";
			// 
			// Label3
			// 
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.Location = new System.Drawing.Point(56, 0);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(236, 16);
			this.Label3.TabIndex = 14;
			this.Label3.Text = "Pacientes ausentes temporalmente";
			// 
			// Label2
			// 
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.Location = new System.Drawing.Point(56, 0);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(236, 16);
			this.Label2.TabIndex = 13;
			this.Label2.Text = "Pacientes ausentes temporalmente";
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(56, 0);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(236, 16);
			this.Label1.TabIndex = 12;
			this.Label1.Text = "Pacientes ausentes temporalmente";
			// 
			// ImageList1
			// 
			this.ImageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.ImageList1.ImageStream = (System.Windows.Forms.ImageListStreamer) resources.GetObject("ImageList1.ImageStream");
			this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
			this.ImageList1.Images.SetKeyName(0, "");
			this.ImageList1.Images.SetKeyName(1, "");
			this.ImageList1.Images.SetKeyName(2, "");
			this.ImageList1.Images.SetKeyName(3, "");
			// 
			// menu_citas
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(981, 564);
			this.Controls.Add(this.Toolbar1);
			this.Controls.Add(this.Frame4);
			this.Controls.Add(this.Frame3);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.Frame2);
			this.Controls.Add(this.cbCerrar);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this.LbAltaHospi);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Frame3.Controls.Add(ShapeContainer1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "menu_citas";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "CONTROL DE CITAS EXTERNAS AL CENTRO - AGI190F1";
			this.Activated += new System.EventHandler(this.menu_citas_Activated);
			base.Closing += new System.ComponentModel.CancelEventHandler(this.menu_citas_Closed);
			this.Load += new System.EventHandler(this.menu_citas_Load);
			this.Toolbar1.ResumeLayout(false);
			this.Frame4.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.FrmAcompPref.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeoptAcomp();
		}
		void InitializeoptAcomp()
		{
			this.optAcomp = new Telerik.WinControls.UI.RadRadioButton[4];
			this.optAcomp[3] = _optAcomp_3;
			this.optAcomp[2] = _optAcomp_2;
			this.optAcomp[1] = _optAcomp_1;
			this.optAcomp[0] = _optAcomp_0;
		}
		#endregion
	}
}