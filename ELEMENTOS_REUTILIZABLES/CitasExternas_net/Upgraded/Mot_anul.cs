using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ECitaExterna
{
	public partial class Mot_anul
		: Telerik.WinControls.UI.RadForm
    {

		public Mot_anul()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void Mot_anul_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		string viGano = String.Empty;
		int vlGnum = 0;
		string DtFechaCita = String.Empty;
		string DtFechaCitaOtroCentro = String.Empty;


		private void CBAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			try
			{
				string sqlEliminarCita = String.Empty;
				DataSet RrEliminarCita = null;
				string nombre = String.Empty;
                ECitasExtenas.RcAdmision.BeginTransScope();
                sqlEliminarCita = "select * from acontcex where " + 
				                  " ganoadme=" + viGano + 
				                  " and gnumadme=" + vlGnum.ToString();
				if (DtFechaCita != "")
				{
					sqlEliminarCita = sqlEliminarCita + " and fcitaext=" + Serrores.FormatFechaHMS(DtFechaCita);
				}
				sqlEliminarCita = sqlEliminarCita + " and fapuntes=" + Serrores.FormatFechaHMS(DtFechaCitaOtroCentro);

                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEliminarCita, ECitasExtenas.RcAdmision);
				RrEliminarCita = new DataSet();
				tempAdapter.Fill(RrEliminarCita);
				if (RrEliminarCita.Tables[0].Rows.Count == 1)
				{
					//     RrEliminarCita.Delete
					RrEliminarCita.Edit();
					RrEliminarCita.Tables[0].Rows[0]["gmanucit"] = cbbMotAnulacion.SelectedValue.ToString();
					RrEliminarCita.Tables[0].Rows[0]["gusuanul"] = ECitasExtenas.VstCodUsua;
					RrEliminarCita.Tables[0].Rows[0]["fanulaci"] = DateTime.Now;
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
					tempAdapter.Update(RrEliminarCita, RrEliminarCita.Tables[0].TableName);
				}
				
				RrEliminarCita.Close();
                ECitasExtenas.RcAdmision.CommitTransScope();
				CbCancelar_Click(CbCancelar, new EventArgs());
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "menu_citas:ProAnularcitas", ex);
                ECitasExtenas.RcAdmision.RollbackTransScope();
            }
		}

		private void Activar_Aceptar()
		{
			CBAceptar.Enabled = cbbMotAnulacion.SelectedIndex > -1;
		}

        private void cbbMotAnulacion_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
        {
			Activar_Aceptar();
		}

		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void Mot_anul_Load(Object eventSender, EventArgs eventArgs)
		{
            CargarComboMotivos();
		}
		public void proRecibir_Programacion(string FechaCita, string FechaCitaOtroCentro, int lNumeroiA˝oRegistro, int lNumeroRegistro)
		{
			//llamo a los procedimientos que rellenana el form
			viGano = lNumeroiA˝oRegistro.ToString();
			vlGnum = lNumeroRegistro;
			DtFechaCita = FechaCita;
			DtFechaCitaOtroCentro = FechaCitaOtroCentro;
		}

		public void CargarComboMotivos()
		{
            try
            {
                //cargamos el combo con los motivos de anulacion
                string Sql = "SELECT gmanucit, dmanucit FROM cmanucit WHERE fborrado is null ORDER BY dmanucit";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ECitasExtenas.RcAdmision);
                DataSet RrMotivosAnulacion = new DataSet();
                tempAdapter.Fill(RrMotivosAnulacion);

                foreach (DataRow iteration_row in RrMotivosAnulacion.Tables[0].Rows)
                {
                    cbbMotAnulacion.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dmanucit"]), Convert.ToInt32(iteration_row["gmanucit"])));
                }
            }
            catch(Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "Citas_externas:CargarComboMotivos", ex);
            }
		}
		private void Mot_anul_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}