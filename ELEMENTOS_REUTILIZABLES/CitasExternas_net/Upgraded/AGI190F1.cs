using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Microsoft.VisualBasic;

namespace ECitaExterna
{
	public partial class menu_citas
		: Telerik.WinControls.UI.RadForm
    {
        int NUMEROMAXIMOFILAS = 0;
		int NUMERODIAS = 0;
		const int NUMEROMAXIMOFILASENPANTALLA = 4;
		const int SS_SORT_BY_ROW = 0;
		const int SS_SORT_BY_COL = 1;
		const int SS_SORT_ORDER_ASCENDING = 1;
		const int SS_SORT_ORDER_DESCENDING = 2;

		const int COLOR_NOTIFICADOS = 0x8000; //vbGreen
		Color COLOR_REALIZADAS = Color.Blue;
		Color COLOR_ANULADAS = Color.Red;

		int ORDEN_PACIENTE = 0; // ORDENACION ASCENDENTE O DESCENDENTE  DE LA COLUMNA PACIENTE =1 o 2
		int ORDEN_CAMA = 0; // ORDENACION ASCENDENTE O DESCENDENTE  DE LA COLUMNA CAMA =1 o 2

		private SqlCommand _RqReservas = null;
        bool botonIzquierdo = false;
        bool flagDbClick = false;
        public menu_citas()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}


		SqlCommand RqReservas
		{
			get
			{
				if (_RqReservas == null)
				{
					_RqReservas = new SqlCommand();
				}
				return _RqReservas;
			}
			set
			{
				_RqReservas = value;
			}
		}
		 // establecer la query inicial para cargar todos
		DataSet RrCargarGrid = null;
		string sqlCargarGrid = String.Empty;
		bool bCargarMas = false;
		int iTope = 0;
		int UltFilaSeleccionada = 0;
		bool bRegistroSeleccionado = false;

		private ClaseCitasAdmision _ObjCitaAdmision = null;
		public ClaseCitasAdmision ObjCitaAdmision
		{
			get
			{
				if (_ObjCitaAdmision == null)
				{
					_ObjCitaAdmision = new ClaseCitasAdmision();
				}
				return _ObjCitaAdmision;
			}
			set
			{
				_ObjCitaAdmision = value;
			}
		}
		 //  OBJECTO REFERENTE A LA RESERVA ACTUAL
		object Clase_Filiacion = null;

		private OrderedDictionary _ColeAnulacionCodigo = null;
		OrderedDictionary ColeAnulacionCodigo
		{
			get
			{
				if (_ColeAnulacionCodigo == null)
				{
					_ColeAnulacionCodigo = new OrderedDictionary(System.StringComparer.OrdinalIgnoreCase);
				}
				return _ColeAnulacionCodigo;
			}
			set
			{
				_ColeAnulacionCodigo = value;
			}
		}

		private OrderedDictionary _ColeAnulacionDescri = null;
		OrderedDictionary ColeAnulacionDescri
		{
			get
			{
				if (_ColeAnulacionDescri == null)
				{
					_ColeAnulacionDescri = new OrderedDictionary(System.StringComparer.OrdinalIgnoreCase);
				}
				return _ColeAnulacionDescri;
			}
			set
			{
				_ColeAnulacionDescri = value;
			}
		}


		bool Cargados = false;

		public string VstOperacionCita = String.Empty;


		// recogida de datos de filiacion
		public void Recoger_datos(string nombre, string Apellido1, string Apellido2, string FechaNacto, string Sexo, string Historia, string Identificador)
		{

			if (Identificador.Trim() != "")
			{
				ObjCitaAdmision.IdentificadorPaciente = Identificador.Trim();
			}
			else
			{
				ObjCitaAdmision.IdentificadorPaciente = "";
			}
		}
        

		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			ECitasExtenas.VstGidenpac = "";
			this.Close();
		}

		private void cbBuscar_Click(Object eventSender, EventArgs eventArgs)
		{
			SprPacientes.MaxRows = 0;
			proEstablecerConsultaInicial();
			proCargarGrid();
			bRegistroSeleccionado = false;
			UltFilaSeleccionada = 0;
			SprPacientes.setSelModeIndex(0);
			HabilitarBotones();
            // COMPRUEBA QUE SI ALGUNO DE LOS BOTONES SE HA QUEDADO PINCHADO Y DISABLED PONERLO BIEN
            int tempForVar = radCommandBarStripElement1.Items.Count;
            for (int ibotones = 1; ibotones <= tempForVar; ibotones++)
			{
                /*INDRA jproche_TODO_5_5
                /if (((((ToolStripButton) Toolbar1.Items[ibotones - 1]).Checked) ? 1 : 0) == UpgradeStubs.MSComctlLib_ValueConstants.gettbrPressed())
                {
                	((ToolStripButton) Toolbar1.Items[ibotones - 1]).Checked = false;
                }
                */
            }
		}

		private void cbEstablecer_Click(Object eventSender, EventArgs eventArgs)
		{
			string Sql = "SELECT * FROM DPACIENT WHERE GIDENPAC= '" + ECitasExtenas.VstGidenpac + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ECitasExtenas.RcAdmision);
			DataSet rrpaciente = new DataSet();
			tempAdapter.Fill(rrpaciente);
			if (rrpaciente.Tables[0].Rows.Count != 0)
			{
				rrpaciente.Edit();
				if (optAcomp[0].IsChecked)
				{
					rrpaciente.Tables[0].Rows[0]["inecacom"] = "N";
				}
				else if (optAcomp[1].IsChecked)
				{ 
					rrpaciente.Tables[0].Rows[0]["inecacom"] = "C";
				}
				else if (optAcomp[2].IsChecked)
				{ 
					rrpaciente.Tables[0].Rows[0]["inecacom"] = "F";
				}
				else if (optAcomp[3].IsChecked)
				{ 
					rrpaciente.Tables[0].Rows[0]["inecacom"] = "E";
				}
				else
				{
					rrpaciente.Tables[0].Rows[0]["inecacom"] = DBNull.Value;
				}
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
				tempAdapter.Update(rrpaciente, rrpaciente.Tables[0].TableName);
			}
		}

		private void menu_citas_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				int n = 0;
				int posicion = 0;
				try
				{
					ObjCitaAdmision.usuario = ECitasExtenas.VstCodUsua;
					ObjCitaAdmision.IdentificadorPaciente = ECitasExtenas.VstGidenpac;
					ObjCitaAdmision.A�oRegistro = ECitasExtenas.viGanoadme.ToString();
					ObjCitaAdmision.NumeroRegistro = ECitasExtenas.viGnumadme.ToString();
					proRecolocaControles();

					Application.DoEvents();
					n = Application.OpenForms.Count;
					SprPacientes.MaxRows = 0;
					proEstablecerConsultaInicial();
					proCargarGrid();
					bRegistroSeleccionado = false;
					UltFilaSeleccionada = 0;

					SprPacientes.setSelModeIndex(0);
					HabilitarBotones();
                    // COMPRUEBA QUE SI ALGUNO DE LOS BOTONES SE HA QUEDADO PINCHADO Y DISABLED PONERLO BIEN
                    int tempForVar = radCommandBarStripElement1.Items.Count;
                    for (int ibotones = 1; ibotones <= tempForVar; ibotones++)
					{
                        /*INDRA jproche_TODO_5_5
						//UPGRADE_ISSUE: (2064) ComctlLib.ValueConstants property ValueConstants.tbrPressed was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						if (((((ToolStripButton) Toolbar1.Items[ibotones - 1]).Checked) ? 1 : 0) == UpgradeStubs.MSComctlLib_ValueConstants.gettbrPressed())
						{
							((ToolStripButton) Toolbar1.Items[ibotones - 1]).Checked = false;
						}
                        */
					}
					return;
				}
				catch(Exception ex)
				{
					Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "menu_citas:Form_activate", ex);
				}
			}
		}

		private void menu_citas_Load(Object eventSender, EventArgs eventArgs)
		{
            activarDobleClickRadioButton();
			Cargados = false;
			ECitasExtenas.ProCargaCabecera(this);
			proEstablecerConstantes();
		}


		private void proCargarGrid()
		{
			int iFilas = 0;
			Color Color = new Color();
			try
			{
				this.Cursor = Cursors.WaitCursor;
				string Sql1 = String.Empty;
				DataSet RrMotivosAnulacion = null;


				Frame2.Text = "Datos del " + Serrores.ObtenerLiteralPersona(ECitasExtenas.RcAdmision);
				Label28.Text = Serrores.ObtenerLiteralPersona(ECitasExtenas.RcAdmision);

				if (RrCargarGrid.Tables[0].Rows.Count != 0)
				{
					if (SprPacientes.MaxRows == 0)
					{
						iFilas = 1;
						SprPacientes.MaxRows = RrCargarGrid.Tables[0].Rows.Count + SprPacientes.MaxRows;
					}
					else
					{
						SprPacientes.MaxRows = RrCargarGrid.Tables[0].Rows.Count + SprPacientes.MaxRows - 1;
						iFilas = SprPacientes.MaxRows - RrCargarGrid.Tables[0].Rows.Count + 2;
					}
                    SprPacientes.BeginUpdate();
                    foreach (DataRow iteration_row in RrCargarGrid.Tables[0].Rows)
					{

						if (Information.IsDate(iteration_row["fcitaext"].ToString()) && Convert.ToDateTime(iteration_row["fcitaext"]) < DateTime.Now)
						{
							Color = COLOR_REALIZADAS;
						}
						else
						{
							if (!Convert.IsDBNull(iteration_row["FNOTIFAM"]))
							{
								Color = ColorTranslator.FromOle(COLOR_NOTIFICADOS);
							}
							else
							{
								Color = System.Drawing.Color.Black;
							}
						}
						if (Convert.ToString(iteration_row["gmanucit"]).Trim() != "" || !Convert.IsDBNull(iteration_row["gmanucit"]))
						{
							Color = COLOR_ANULADAS;
						}

						SprPacientes.Row = iFilas;
						SprPacientes.Col = 1;
						SprPacientes.Text = Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:mm");
						SprPacientes.Col = 2;
						SprPacientes.Text = iteration_row["fcitaext"].ToString() != string.Empty ? Convert.ToDateTime(iteration_row["fcitaext"]).ToString("dd/MM/yyyy HH:mm") : string.Empty;
						SprPacientes.Col = 3;
						SprPacientes.Text = Convert.ToString(iteration_row["dnomcent"]).Trim();
						SprPacientes.Col = 4;
						SprPacientes.Text = Convert.ToString(iteration_row["dprueba"]).Trim();
						SprPacientes.Col = 5; //motivo anulacion

						if (Convert.ToString(iteration_row["gmanucit"]) != "" && !Convert.IsDBNull(iteration_row["gmanucit"]))
						{
							Sql1 = "SELECT gmanucit, dmanucit FROM cmanucit where gmanucit = " + Convert.ToString(iteration_row["gmanucit"]) + "";
							SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql1, ECitasExtenas.RcAdmision);
							RrMotivosAnulacion = new DataSet();
							tempAdapter.Fill(RrMotivosAnulacion);
							if (RrMotivosAnulacion.Tables[0].Rows.Count != 0)
							{
								SprPacientes.Text = Convert.ToString(RrMotivosAnulacion.Tables[0].Rows[0]["dmanucit"]).Trim();
							}
							else
							{
								SprPacientes.Text = "";
							}
							RrMotivosAnulacion.Close();
						}
						else
						{
							SprPacientes.Text = "";
						}

                        SprPacientes.foreColor = Color;
						SprPacientes.Col = 6; // gidenpac oculto
						iFilas++;
					}
				}
				else
				{
					SprPacientes.MaxRows = 0;
				}
                SprPacientes.EndUpdate();
				RrCargarGrid.MoveFirst();
                this.Cursor = Cursors.Default;
			}
			catch(Exception ex)
			{
				this.Cursor = Cursors.Default;
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "menu_citas:ProCargaGrid", ex);
			}
		}
        
		private void proEstablecerConstantes()
		{
			string sqlConstantes = String.Empty;
			DataSet RrConstantes = null;
			try
			{
				sqlConstantes = "select nnumeri1 from sconsglo where gconsglo='MAXFILAS'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlConstantes, ECitasExtenas.RcAdmision);
				RrConstantes = new DataSet();
				tempAdapter.Fill(RrConstantes);
				if (RrConstantes.Tables[0].Rows.Count == 1)
				{
					NUMEROMAXIMOFILAS = Convert.ToInt32(RrConstantes.Tables[0].Rows[0][0]); // numero m�ximo de filas a cargar en el grid
				}
				RrConstantes.Close();

				//n�mero de dias a mostrar las citas anteriores
				sqlConstantes = "select nnumeri1 from sconsglo where gconsglo='FECCITEX'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlConstantes, ECitasExtenas.RcAdmision);
				RrConstantes = new DataSet();
				tempAdapter_2.Fill(RrConstantes);
				if (RrConstantes.Tables[0].Rows.Count != 0)
				{
					NUMERODIAS = (Convert.IsDBNull(RrConstantes.Tables[0].Rows[0]["nnumeri1"])) ? 0 : Convert.ToInt32(RrConstantes.Tables[0].Rows[0]["nnumeri1"]); // numero m�ximo de filas a cargar en el grid
				}
				else
				{
					NUMERODIAS = 0;
				}
				RrConstantes.Close();

				NUMERODIAS *= -1;

				SdcFechaDesde.MinDate = DateTime.Parse("01/01/1900");
				SdcFechaDesde.MaxDate = DateTime.Today;
				System.DateTime StFechaAMostrar = DateTime.FromOADate(0);
				StFechaAMostrar = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy")).AddDays(NUMERODIAS);
				SdcFechaDesde.Text = StFechaAMostrar.ToString("dd/MM/yyyy");
				SdcFechaDesde.Value = StFechaAMostrar;

				bRegistroSeleccionado = false;
				UltFilaSeleccionada = 0;
				//SprPacientes.OperationMode = OperationModeRead
				SprPacientes.MaxRows = 0;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "menu_citas:ProEstablecerConstantes", ex);
			}
		}

		//OBTIENE LA CONSULTA INICIAL PARA LUEGO IR OBTENIENDO DE 50 EN 50
		private void proEstablecerConsultaInicial()
		{
			string FechaMaxima = String.Empty;
			try
			{

				sqlCargarGrid = "select max(fcitaext) as maximafecha from acontcex where gnumadme=" + 
				                ObjCitaAdmision.NumeroRegistro + " and ganoadme=" + ObjCitaAdmision.A�oRegistro;


				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCargarGrid, ECitasExtenas.RcAdmision);
				RrCargarGrid = new DataSet();
				tempAdapter.Fill(RrCargarGrid);
				if (!Convert.IsDBNull(RrCargarGrid.Tables[0].Rows[0][0]))
				{
					FechaMaxima = Convert.ToString(RrCargarGrid.Tables[0].Rows[0][0]);
				}
				else
				{
					FechaMaxima = "";
				}
				RrCargarGrid.Close();
				sqlCargarGrid = "select acontcex.*";
				sqlCargarGrid = sqlCargarGrid + " from acontcex where (fcitaext <= @fecha ";
				sqlCargarGrid = sqlCargarGrid + " or fcitaext is null) ";
				sqlCargarGrid = sqlCargarGrid + " and ganoadme=" + ObjCitaAdmision.A�oRegistro;
				sqlCargarGrid = sqlCargarGrid + " and gnumadme=" + ObjCitaAdmision.NumeroRegistro;
				if (CheckTodos.CheckState == CheckState.Unchecked)
				{
					sqlCargarGrid = sqlCargarGrid + " and fcitaord > " + Serrores.FormatFecha(SdcFechaDesde.Text);
				}
				sqlCargarGrid = sqlCargarGrid + "  order by fcitaext";
				RqReservas.Connection = ECitasExtenas.RcAdmision;
				RqReservas.CommandText = sqlCargarGrid;

                if (RqReservas.Parameters.Count == 0)
                    RqReservas.Parameters.Add("@fecha", SqlDbType.DateTime);

                if (FechaMaxima.Trim() != "")
				{
					RqReservas.Parameters[0].Value = DateTime.Parse(FechaMaxima);
				}
				else
				{
					RqReservas.Parameters[0].Value = DateTime.Now;
				}
				/*INDRA jproche_TODO_5_5
				RqReservas.setMaxRows(NUMEROMAXIMOFILAS);
                */
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(RqReservas);
				RrCargarGrid = new DataSet();
				tempAdapter_2.Fill(RrCargarGrid, 0,  NUMEROMAXIMOFILAS, "Table1");
				if (RrCargarGrid.Tables[0].Rows.Count == NUMEROMAXIMOFILAS)
				{
					iTope = RrCargarGrid.Tables[0].Rows.Count - NUMEROMAXIMOFILASENPANTALLA; // cuatro es el maximo de filas que caben en la pantalla
					bCargarMas = true;
				}
				else
				{
					if (RrCargarGrid.Tables[0].Rows.Count < NUMEROMAXIMOFILAS)
					{
						bCargarMas = false;
					}
				}
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "menu_citas:ProEstablecerConsultaInicial", ex);
			}
		}

		private void menu_citas_Closed(Object eventSender, CancelEventArgs eventArgs)
		{
			//comprueba que no existe otro formulario de relacionado con las reservas
			foreach (Form Form in Application.OpenForms)
			{
				if (Form.Name.Trim().ToUpper() == "CITAS_EXTERNAS" || Form.Name.Trim().ToUpper() == "DOCU_CITAS")
				{ // si est� cargado la ventana de reservas deshabilitar seg�n
					// el tipo de operaci�n.
					RadMessageBox.Show("Imposible cerrar. Existe un formulario de CITAS abierto", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
					eventArgs.Cancel = (!true) ? false : true;
					return;
				}
			}

			ColeAnulacionCodigo = null;
			ColeAnulacionDescri = null;
            m_vb6FormDefInstance.Dispose();
        }

        private void optAcomp_CheckedChanged(Object eventSender, Telerik.WinControls.UI.StateChangedEventArgs eventArgs)
        {
            int Index = Array.IndexOf(this.optAcomp, eventSender);
            if (flagDbClick)
            {
                optAcomp[Index].IsChecked = false;                
                flagDbClick = false;
            }
        }

        private void optAcomp_DblClick(object sender, System.EventArgs e)
		{            
            int Index = Array.IndexOf(this.optAcomp, sender);
            optAcomp[Index].IsChecked = false;            
            flagDbClick = true;            
        }

        private void SprPacientes_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
        {

            if (botonIzquierdo)
			{
                if (SprPacientes.CurrentRow == null)
                    return;

                int Col = SprPacientes.CurrentColumn.Index + 1;
                int row = SprPacientes.CurrentRow.Index + 1;

                try
				{

					if (row == 0 || Col == 0)
					{
						return;
					}

					if (bRegistroSeleccionado)
					{ // si hay uno seleccionado
						if (UltFilaSeleccionada == row)
						{
							bRegistroSeleccionado = false;
                            SprPacientes.CurrentRow = null;
						}
						else
						{
							bRegistroSeleccionado = true;
                            proMemorizarCita();
						}
					}
					else
					{
						//SI NO HABIA REGISTRO SELECCIONADO
						bRegistroSeleccionado = true;
                        proMemorizarCita();
					}
					HabilitarBotones();
					UltFilaSeleccionada = row; //GUARDO LA ULTIMA FILA SELECCIONADA
					return;
				}
				catch(Exception ex)
				{
					Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "menu_citas:SprPacientes_click", ex);
				}
			}
		}

        public void SprPacientes_CellDoubleClick(object eventSender, GridViewCellEventArgs eventArgs)
            
		{
            if (SprPacientes.CurrentRow == null)
                return;

            int Col = SprPacientes.CurrentColumn.Index + 1;
            int row = SprPacientes.CurrentRow.Index + 1;

            if (Col == 0 || row == 0)
			{
				return;
			}
			//comprueba que no existe otro formulario de reservas y que se est� modificando tambien
			foreach (dynamic Form in Application.OpenForms)
			{
				if (Form.Name.Trim().ToUpper() == "CITAS_EXTERNAS")
				{ // si est� cargado la ventana de reservas deshabilitar seg�n
					// el tipo de operaci�n
					if (Convert.ToString(Form.VstOperacion) == "MODIFICAR_CITA")
					{
                        radCommandBarStripElement1.Items[0].Enabled = false;
                        RadMessageBox.Show("Ya existe una modificaci�n en curso", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
						return;
					}
				}
			}

			HabilitarBotones();
			proMemorizarCita();
			VstOperacionCita = "MODIFICAR_CITA";
			citas_externas tempLoadForm = citas_externas.DefInstance;
			citas_externas.DefInstance.ShowDialog();
            menu_citas_Activated(null, null);
        }

		private void SprPacientes_TopLeftChange(object eventSender, UpgradeHelpers.Spread.TopLeftChangeEventArgs eventArgs)
		{
			string stFecha = String.Empty;
			string stsegundos = String.Empty;
			if (bCargarMas)
			{
				if (eventArgs.NewTop >= iTope)
				{
					this.Cursor = Cursors.WaitCursor;
					SprPacientes.Row = SprPacientes.MaxRows;
					SprPacientes.Col = 2; // FECHA PREVISTA INGRESO
					stFecha = SprPacientes.Text;
					SprPacientes.Col = 12; // segundos
					RqReservas.Parameters[0].Value = DateTime.Parse(stFecha);
					RrCargarGrid.Reset();
					SqlDataAdapter tempAdapter = new SqlDataAdapter(RqReservas);
					tempAdapter.Fill(RrCargarGrid);
					if (RrCargarGrid.Tables[0].Rows.Count == NUMEROMAXIMOFILAS)
					{
						iTope = RrCargarGrid.Tables[0].Rows.Count + SprPacientes.MaxRows - NUMEROMAXIMOFILASENPANTALLA;
						bCargarMas = true;
					}
					else
					{
						if (RrCargarGrid.Tables[0].Rows.Count < NUMEROMAXIMOFILAS)
						{
							bCargarMas = false;
						}
					}
					if (RrCargarGrid.Tables[0].Rows.Count > 0)
					{
						RrCargarGrid.MoveNext();
						proCargarGrid();
					}
					this.Cursor = Cursors.Default;
				}
			}

		}

		public void HabilitarBotones()
		{
			bool fNuevaCita = false; // SI ENCUENTRA UN FORMULARIO QUE SE A�ADA UNA RESERVA -> TRUE
			bool fMdificacionCita = false; // SI ENCUENTRA UN FORMULARIO QUE SE MODIFIQUE UNA RESERVA -> TRUE
			bool fDocumentosCitas = false;
			// comprobar las ventanas que estan cargadas
			foreach (dynamic Form in Application.OpenForms)
			{
				if (Form.Name.Trim().ToUpper() == "CITAS_EXTERNAS")
				{ // si est� cargado la ventana de reservas deshabilitar seg�n
					// el tipo de operaci�n
					if (Convert.ToString(Form.VstOperacion) == "A�ADIR_CITA")
					{
						fNuevaCita = true;
					}
					if (Convert.ToString(Form.VstOperacion) == "MODIFICAR_CITA")
					{
						fMdificacionCita = true;
					}
				}
				if (Form.Name.Trim().ToUpper() == "DOCU_CITAS")
				{
					fDocumentosCitas = true;
				}
			}
			if (bRegistroSeleccionado)
			{
				// COMPROBAR LA FECHA DE LA CITA
				SprPacientes.Col = 2;
				if (!this.SprPacientes.foreColor.Equals(COLOR_ANULADAS))
				{
					if (SprPacientes.Text != "")
					{
                        radCommandBarStripElement1.Items[2].Enabled = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy HH:mm")) < DateTime.Parse(DateTime.Parse(SprPacientes.Text).ToString("dd/MM/yyyy HH:mm")); // anulacion de la cita
                    }
					else
					{
                        radCommandBarStripElement1.Items[2].Enabled = true; // anulacion de la cita
                    }
				}
				else
				{
                    radCommandBarStripElement1.Items[2].Enabled = false; ; // anulacion de la cita
                }
				if (fNuevaCita)
				{ // si se esta haciendo una nueva cita
                  radCommandBarStripElement1.Items[0].Enabled = false;
                } 
				else
				{
                    radCommandBarStripElement1.Items[0].Enabled = true;
                }
				if (fMdificacionCita)
				{
				}
				else
				{
				}
				if (!this.SprPacientes.foreColor.Equals(COLOR_ANULADAS))
				{
					if (fDocumentosCitas)
					{
                        radCommandBarStripElement1.Items[1].Enabled = false;
                    }
					else
					{
						SprPacientes.Col = 2;
						if (SprPacientes.Text != "")
						{
                            radCommandBarStripElement1.Items[1].Enabled  = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy HH:mm")) < DateTime.Parse(DateTime.Parse(SprPacientes.Text).ToString("dd/MM/yyyy HH:mm"));
                        }
						else
						{
                            radCommandBarStripElement1.Items[1].Enabled = false;
                        }
					}
				}
				else
				{
                    radCommandBarStripElement1.Items[1].Enabled = false;
                }
			}
			else
			{
				// si no hay un registro seleccionado
				if (fNuevaCita)
				{ // si se esta haciendo una nueva reserva
                    radCommandBarStripElement1.Items[0].Enabled = false;
				}
				else
				{
                    radCommandBarStripElement1.Items[0].Enabled = true;
                }
                
                radCommandBarStripElement1.Items[1].Enabled = false;
                radCommandBarStripElement1.Items[2].Enabled = false;
            }
			//si no tiene fecha de la cita no imprimir los documentos
			SprPacientes.Col = 2;
			if (SprPacientes.Text == "")
			{
                radCommandBarStripElement1.Items[1].Enabled = false;
            }
		}


		private void Toolbar1_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
			CommandBarButton Button = (CommandBarButton)eventSender;
            switch (radCommandBarStripElement1.Items.IndexOf(Button) + 1)
			{
				case 1 : 
					// LLAMA A LA DLL DE FILIACION PARA BUSCAR A UN PACIENTE Y REALIZAR SU RESERVA 
					ECitasExtenas.mnu_adm0113_Click(1); 
					break;
				case 2 : 
					ECitasExtenas.mnu_adm0113_Click(4); 
					break;
				case 3 : 
					ECitasExtenas.mnu_adm0113_Click(3); 
					break;
			}
            menu_citas_Activated(null, null);
        }


		public void proMemorizarCita()
		{
			string sqlCita = String.Empty;
			DataSet RrCita = null;
			try
			{
				// con el GIDENPAC SACO TODOS LOS DATOS DE LA cita DE LA TABLA
				sqlCita = "select * from acontcex where ganoadme=" + ObjCitaAdmision.A�oRegistro;
				sqlCita = sqlCita + " and gnumadme=" + ObjCitaAdmision.NumeroRegistro;
				sqlCita = sqlCita + " and fapuntes='";
				SprPacientes.Col = 1;
				SprPacientes.Row = SprPacientes.ActiveRowIndex;
				sqlCita = sqlCita + DateTime.Parse(SprPacientes.Text).ToString("MM/dd/yyyy HH:mm") + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCita, ECitasExtenas.RcAdmision);
				RrCita = new DataSet();
				tempAdapter.Fill(RrCita);
				ClaseCitasAdmision withVar = null;
				if (RrCita.Tables[0].Rows.Count == 1)
				{
					withVar = ObjCitaAdmision;
					withVar.Centro = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["dnomcent"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["dnomcent"]).Trim() : "";
					withVar.CodigoCausaAmb = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["gcauambu"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["gcauambu"]) : "";
					withVar.CodigoModoTrans = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["gmodtran"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["gmodtran"]) : "";
					withVar.CodPoblacion = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["gpoblaci"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["gpoblaci"]) : "";
					withVar.CodPostal = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["gcodipos"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["gcodipos"]) : "";
					withVar.CodProvincia = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["gprovinc"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["gprovinc"]) : "";
					withVar.DireccCentro = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["ddirecen"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["ddirecen"]).Trim() : "";
					withVar.Especialidad = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["dnomespe"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["dnomespe"]).Trim() : "";
					withVar.FechaCita = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["fcitaext"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["fcitaext"]) : "";
					withVar.FechaCitaOtroCentro = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["fapuntes"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["fapuntes"]) : "";
					withVar.FechaNotificadoFamilia = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["fnotifam"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["fnotifam"]) : "";
					withVar.medico = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["dmedico"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["dmedico"]).Trim() : "";
					withVar.NecesitaAmbulancia = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["inecambu"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["inecambu"]) : "";
					withVar.NotificadoFamiliar = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["inotifam"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["inotifam"]) : "";
					withVar.Prueba = (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["dprueba"])) ? Convert.ToString(RrCita.Tables[0].Rows[0]["dprueba"]).Trim() : "";
					if (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["inecacom"]))
					{
						withVar.NecesitaAcompa�ante = Convert.ToString(RrCita.Tables[0].Rows[0]["inecacom"]).Trim().ToUpper();
					}
					else
					{
						//(maplaza)(14/05/2007)El campo puede ser NULO, lo cual es distinto de "N".
						//.NecesitaAcompa�ante = "N"
						withVar.NecesitaAcompa�ante = "";
						//-----
					}
				}
				RrCita.Close();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "menu_citas:ProMemorizarCita", ex);
			}
		}


		public void ProAnularCita()
		{
			try
			{
				string nombre = String.Empty;
				int iresp = 0;
				int StAno = 0;
				int stNumero = 0;
				string FechaCita = String.Empty;
				string FechaCitaOtroCentro = String.Empty;
                
				SprPacientes.Row = SprPacientes.ActiveRowIndex;
				SprPacientes.Col = 2;
				FechaCita = SprPacientes.Text.Trim();
				SprPacientes.Col = 1;
				FechaCitaOtroCentro = SprPacientes.Text.Trim();

				StAno = Convert.ToInt32(Double.Parse(ObjCitaAdmision.A�oRegistro));
				stNumero = Convert.ToInt32(Double.Parse(ObjCitaAdmision.NumeroRegistro));

				Mot_anul tempLoadForm = Mot_anul.DefInstance;
				Mot_anul.DefInstance.proRecibir_Programacion(FechaCita, FechaCitaOtroCentro, StAno, stNumero);
				Mot_anul.DefInstance.ShowDialog();
                menu_citas_Activated(this, new EventArgs());
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "menu_citas:ProAnularcitas", ex);
                ECitasExtenas.RcAdmision.RollbackTransScope();
            }
		}

		private void proRecolocaControles()
		{
			this.Top = (int) 0;
			this.Left = (int) 0;

            this.Height = (int) (Screen.PrimaryScreen.Bounds.Height - 40);
			this.Width = (int) Screen.PrimaryScreen.Bounds.Width;
			Frame1.Width = (int) (this.ClientRectangle.Width - Frame4.Width - 67);
			Frame4.Left = (int) (Frame1.Width + 20);
			Frame2.Width = (int) (this.ClientRectangle.Width - 7);
			Frame2.Height = (int) (this.ClientRectangle.Height - 180);
			Frame3.Top = (int) (Frame2.Height + 117);
			SprPacientes.Width = (int) (Frame2.Width - 13);
			SprPacientes.Height = (int) (Frame2.Height - 20);
			cbCerrar.Top = (int) (Frame2.Height + 133);
			cbCerrar.Left = (int) (Frame2.Width - cbCerrar.Width);

            SprPacientes.Columns[2].Width = (SprPacientes.Width - 200) / 3;
            SprPacientes.Columns[3].Width = (SprPacientes.Width - 200) / 3;
            SprPacientes.Columns[4].Width = (SprPacientes.Width - 200) / 3;

        }
        private void SprPacientes_MouseDown(Object eventSender, MouseEventArgs eventArgs)
        {
            if (eventArgs.Button == MouseButtons.Left)
                botonIzquierdo = true;
            else
                botonIzquierdo = false;
        }

        /// <summary>
        /// INDRA jproche Necesario para activar el evento DoubleClick sobre el control RadioButton 
        /// </summary>
        private void activarDobleClickRadioButton()
        {
            MethodInfo m = typeof(RadioButton).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
            if (m != null)
            {
                m.Invoke(_optAcomp_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_optAcomp_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_optAcomp_2, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_optAcomp_3, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
            }

            _optAcomp_0.MouseDoubleClick += optAcomp_DblClick;
            _optAcomp_1.MouseDoubleClick += optAcomp_DblClick;
            _optAcomp_2.MouseDoubleClick += optAcomp_DblClick;
            _optAcomp_3.MouseDoubleClick += optAcomp_DblClick;
        }
    }
}