using Microsoft.VisualBasic;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using CrystalWrapper;

namespace ECitaExterna
{
	public partial class listado_citas_externas
		: Telerik.WinControls.UI.RadForm
    {

		public listado_citas_externas()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}


		private void listado_citas_externas_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		bool mSoloIngresados = false;

		private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (validadatos())
			{
				Imprimir();
			}
		}

		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void listado_citas_externas_Load(Object eventSender, EventArgs eventArgs)
		{
			tbHoraDesde.Text = "00:00";
			tbHoraHasta.Text = "23:59";

			SdcFechaDesde.Value = DateTime.Today;
			SdcFechaHasta.Value = DateTime.Today;

			SdcFechaDesde.MinDate = DateTime.Parse("01/01/1900");
			SdcFechaHasta.MinDate = DateTime.Parse("01/01/1900");

			if (mSoloIngresados)
			{
				chIngAlt[0].CheckState = CheckState.Checked;
				chIngAlt[1].CheckState = CheckState.Unchecked;
			}
			else
			{
				chIngAlt[0].CheckState = CheckState.Checked;
				chIngAlt[1].CheckState = CheckState.Checked;
			}

		}


		public void RecibeParametros(bool ParaSoloIngresados)
		{
			mSoloIngresados = ParaSoloIngresados;
		}

		private void Imprimir()
		{
			string Sql = String.Empty;
			string LitPersona = String.Empty;

			try
			{
				Sql = "select acontcex.fcitaext, dpacient.dnombpac, dpacient.dape1pac, dpacient.dape2pac, acontcex.dnomcent, acontcex.ddirecen,";
				Sql = Sql + " dprovinc.dnomprov, dpoblaci.dpoblaci, acontcex.gcodipos, acontcex.inecambu , acontcex.inotifam, acontcex.fnotifam,";
				Sql = Sql + " acontcex.DNOMESPE, acontcex.iNECACOM, acontcex.gmodtran, dunienfe.dunidenf ";
				Sql = Sql + " From acontcex ";
				Sql = Sql + "  INNER JOIN aepisadm ON acontcex.ganoadme = aepisadm.ganoadme and " + 
				      " acontcex.gnumadme = aepisadm.gnumadme and " + 
				      " acontcex.gidenpac = aepisadm.gidenpac ";
				if (chIngAlt[0].CheckState == CheckState.Checked && chIngAlt[1].CheckState == CheckState.Unchecked)
				{
					Sql = Sql + " and aepisadm.faltplan is null ";
				}
				if (chIngAlt[0].CheckState == CheckState.Unchecked && chIngAlt[1].CheckState == CheckState.Checked)
				{
					Sql = Sql + " and aepisadm.faltplan is not null ";
				}

				Sql = Sql + "  INNER JOIN dpacient ON acontcex.gidenpac = dpacient.gidenpac ";
				Sql = Sql + "  INNER JOIN amovimie ON AEPISADM.ganoadme = AMOVIMIE.ganoregi and " + 
				      " AEPISADM.gnumadme = AMOVIMIE.gnumregi ";
				if (chIngAlt[0].CheckState == CheckState.Checked && chIngAlt[1].CheckState == CheckState.Unchecked)
				{
					Sql = Sql + " and amovimie.ffinmovi is null ";
				}
				else if (chIngAlt[0].CheckState == CheckState.Unchecked && chIngAlt[1].CheckState == CheckState.Checked)
				{ 
					Sql = Sql + " and amovimie.ffinmovi = aepisadm.faltplan  ";
				}
				else
				{
					Sql = Sql + " and (amovimie.ffinmovi = aepisadm.faltplan or amovimie.ffinmovi is null) ";
				}
				Sql = Sql + " LEFT JOIN AMODTRAN ON acontcex.gmodtran = AMODTRAN.gmodtran ";
				Sql = Sql + " INNER JOIN dunienfe ON dunienfe.gunidenf = isnull(amovimie.gunenfde, amovimie.gunenfor) ";
				Sql = Sql + " LEFT JOIN dprovinc ON acontcex.gprovinc = dprovinc.gprovinc ";
				Sql = Sql + " LEFT JOIN dpoblaci ON acontcex.gpoblaci = dpoblaci.gpoblaci ";
				Sql = Sql + " WHERE acontcex.ganoadme IS NOT NULL and acontcex.fanulaci IS NULL";

				//(maplaza)(18/05/2007)
				if (ECitasExtenas.Modulo == "H")
				{
					//para el m�dulo de Hospitalizaci�n, hay que filtrar por la Unidad de Enfermer�a seleccionada
					//(maplaza)(11/01/2007)S�lo debe aparecer el registro si se trata de la unidad de enfermer�a en la que est�
					//actualmente ingresado.
					Sql = Sql + " and AMOVIMIE.gunenfor ='" + ECitasExtenas.UniEnf_ServMed + "'";
					//------------
				}
				else if ((ECitasExtenas.Modulo == "SM"))
				{ 
					//para el m�dulo de Servicios M�dicos, hay que filtrar por el Servicio M�dico seleccionado
					//(maplaza)(11/01/2007)S�lo debe aparecer el registro si se trata del servicio m�dico en el que est�
					//actualmente ingresado.
					Sql = Sql + " and AMOVIMIE.gservior =" + ECitasExtenas.UniEnf_ServMed;
					//------------
				}
				else if ((ECitasExtenas.Modulo == "OPA" || ECitasExtenas.Modulo == "AD"))
				{ 
					//para los m�dulos de OPA (Otros Profesionales Asistenciales) y Admisi�n, hay que agrupar por Unidad de Enfermer�a
				}
				//-----

				if (chkPendienteAsignacionFecha.CheckState == CheckState.Checked)
				{
					object tempRefParam = SdcFechaDesde.Value.ToShortDateString() + " " + tbHoraDesde.Text;
					object tempRefParam2 = SdcFechaHasta.Value.Date + " " + tbHoraHasta.Text;
					Sql = Sql + 
					      " and (acontcex.fcitaext is null or " + 
					      "     (acontcex.fcitaext >= " + Serrores.FormatFechaHMS(tempRefParam) + " and " + 
					      "      acontcex.fcitaext <=" + Serrores.FormatFechaHMS(tempRefParam2) + 
					      "     ))";
				}
				else
				{
					object tempRefParam3 = SdcFechaDesde.Value.ToShortDateString() + " " + tbHoraDesde.Text;
					object tempRefParam4 = SdcFechaHasta.Value.Date + " " + tbHoraHasta.Text;
					Sql = Sql + 
					      " and acontcex.fcitaext is not null and " + 
					      " acontcex.fcitaext >= " + Serrores.FormatFechaHMS(tempRefParam3) + " and " + 
					      " acontcex.fcitaext <=" + Serrores.FormatFechaHMS(tempRefParam4);
				}

				//O.Frias - 09/09/2009
				//Nuevas ordenaciones.

				if (optOrdena[0].IsChecked)
				{
					Sql = Sql + "\n" + "\r" + " Order By acontcex.fcitaord, dpacient.dape1pac, dpacient.dape2pac, dpacient.dnombpac ";
				}
				else if (optOrdena[1].IsChecked)
				{ 
					Sql = Sql + "\n" + "\r" + " Order By dpacient.dape1pac, dpacient.dape2pac, dpacient.dnombpac ";
				}
				else if (optOrdena[2].IsChecked)
				{ 

					//(maplaza)(18/05/2007)Para el caso de que haya que agrupar, se ordena en primer lugar por unidad de enfermer�a.
					//sql = sql & Chr(10) & Chr(13) & " Order By acontcex.fcitaext,dpacient.dape1pac, dpacient.dape2pac, dpacient.dnombpac "
					if (ECitasExtenas.Modulo == "H" || ECitasExtenas.Modulo == "SM")
					{
						Sql = Sql + "\n" + "\r" + " Order By acontcex.fcitaext,dpacient.dape1pac, dpacient.dape2pac, dpacient.dnombpac ";
					}
					else
					{
						//(Modulo = "OPA" Or Modulo = "AD")
						Sql = Sql + "\n" + "\r" + " Order By dunienfe.dunidenf, acontcex.fcitaord,dpacient.dape1pac, dpacient.dape2pac, dpacient.dnombpac ";
					}
					//-----

				}


				if (!ECitasExtenas.VstrCrystal.VfCabecera)
				{
					ECitasExtenas.proCabCrystal();
				}
                //ECitasExtenas.LISTADO_CRYSTAL.ReportFileName = ECitasExtenas.PathString + "\\AGI194R1.rpt";
                ECitasExtenas.LISTADO_CRYSTAL.ReportFileName = ECitasExtenas.PathString + "\\AGI194R1_CR11.rpt";
                //ECitasExtenas.LISTADO_CRYSTAL.Formulas["FORM1"] = "\"" + ECitasExtenas.VstrCrystal.VstGrupoHo + "\"";
                ECitasExtenas.LISTADO_CRYSTAL.Formulas["FORM1"] = "\"" + ECitasExtenas.VstrCrystal.VstGrupoHo + "\"";
				//ECitasExtenas.LISTADO_CRYSTAL.Formulas[1] = "FORM2=\"" + ECitasExtenas.VstrCrystal.VstNombreHo + "\"";
                ECitasExtenas.LISTADO_CRYSTAL.Formulas["FORM2"] = "\"" + ECitasExtenas.VstrCrystal.VstNombreHo + "\"";
                //ECitasExtenas.LISTADO_CRYSTAL.Formulas[2] = "FORM3=\"" + ECitasExtenas.VstrCrystal.VstDireccHo + "\"";
                ECitasExtenas.LISTADO_CRYSTAL.Formulas["FORM3"] = "\"" + ECitasExtenas.VstrCrystal.VstDireccHo + "\"";

                LitPersona = Serrores.ObtenerLiteralPersona(ECitasExtenas.RcAdmision);
				//ECitasExtenas.LISTADO_CRYSTAL.Formulas[3] = "LitPers=\"" + LitPersona + "\"";
                ECitasExtenas.LISTADO_CRYSTAL.Formulas["LitPers"] = "\"" + LitPersona + "\"";

                string sTitulo = String.Empty;
				sTitulo = "LISTADO DE " + LitPersona.ToUpper() + "S CON CITAS EXTERNAS";
				//ECitasExtenas.LISTADO_CRYSTAL.Formulas[4] = "TituloLitPers=\"" + sTitulo + "\"";
                ECitasExtenas.LISTADO_CRYSTAL.Formulas["TituloLitPers"] = "\"" + sTitulo + "\"";

                //ECitasExtenas.LISTADO_CRYSTAL.Formulas[5] = "Fecha_desde=\"" + DateTime.Parse(SdcFechaDesde.Value.Date).ToString("dd/MM/yyyy") + " " + tbHoraDesde.Text + "\"";
                ECitasExtenas.LISTADO_CRYSTAL.Formulas["Fecha_desde"] = "\"" + DateTime.Parse(SdcFechaDesde.Text).ToString("dd/MM/yyyy") + " " + tbHoraDesde.Text + "\"";
                //ECitasExtenas.LISTADO_CRYSTAL.Formulas[6] = "Fecha_hasta=\"" + DateTime.Parse(SdcFechaHasta.Value.Date).ToString("dd/MM/yyyy") + " " + tbHoraHasta.Text + "\"";
                ECitasExtenas.LISTADO_CRYSTAL.Formulas["Fecha_hasta"] = "\"" + DateTime.Parse(SdcFechaHasta.Text).ToString("dd/MM/yyyy") + " " + tbHoraHasta.Text + "\"";
                //(maplaza)(16/05/2007)En la cabecera del informe debe aparecer la unidad de enfermer�a o el servicio seleccionado

                if (ECitasExtenas.Modulo == "H")
				{ //si la llamada se ha realizado desde el m�dulo de Hospitalizaci�n
					//ECitasExtenas.LISTADO_CRYSTAL.Formulas[7] = "UniEnf_ServMed=\"" + ECitasExtenas.UnidadEnfermeria(ECitasExtenas.UniEnf_ServMed).ToUpper() + "\"";
                    ECitasExtenas.LISTADO_CRYSTAL.Formulas["UniEnf_ServMed"] = "\"" + ECitasExtenas.UnidadEnfermeria(ECitasExtenas.UniEnf_ServMed).ToUpper() + "\"";

                }
				else if ((ECitasExtenas.Modulo == "SM"))
				{  //si la llamada se ha realizado desde el m�dulo de Servicios M�dicos
					//ECitasExtenas.LISTADO_CRYSTAL.Formulas[7] = "UniEnf_ServMed=\"" + ECitasExtenas.Servicio(ECitasExtenas.UniEnf_ServMed).ToUpper() + "\"";
                    ECitasExtenas.LISTADO_CRYSTAL.Formulas["UniEnf_ServMed"] = "\"" + ECitasExtenas.Servicio(ECitasExtenas.UniEnf_ServMed).ToUpper() + "\"";

                }
				//-----

				//(maplaza)(18/05/2007)Si el m�dulo que llama es Admisi�n u OPA, se realizan agrupaciones en el report; en caso
				//contrario (Hospitalizaci�n o Servicios M�dicos), no se realizan agrupaciones, sino que se filtra por el
				//Servicio M�dico o por la Unidad de Enfermer�a.

				if (optOrdena[0].IsChecked || optOrdena[1].IsChecked)
				{
                    //ECitasExtenas.LISTADO_CRYSTAL.Formulas[8] = "Modulo=\"" + "H" + "\"";
                    ECitasExtenas.LISTADO_CRYSTAL.Formulas["Modulo"] = "\"" + "H" + "\"";

                }
				else
				{
                	//ECitasExtenas.LISTADO_CRYSTAL.Formulas[8] = "Modulo=\"" + ECitasExtenas.Modulo.ToUpper() + "\"";
                    ECitasExtenas.LISTADO_CRYSTAL.Formulas["Modulo"] = "\"" + ECitasExtenas.Modulo.ToUpper() + "\"";

                }
				//-----
                ECitasExtenas.LISTADO_CRYSTAL.Destination = (int) Crystal.DestinationConstants.crptToWindow;
				ECitasExtenas.LISTADO_CRYSTAL.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
				ECitasExtenas.LISTADO_CRYSTAL.WindowTitle = "Listado de pacientes con citas externas";
                //ECitasExtenas.LISTADO_CRYSTAL.Connect = "DSN=" + ECitasExtenas.VstCrystal + ";UID=" + ECitasExtenas.gUsuario + ";PWD=" + ECitasExtenas.gContrase�a + "";
                ECitasExtenas.LISTADO_CRYSTAL.Connect = ECitasExtenas.VstCrystal + ";" + ECitasExtenas.gUsuario + ";" + ECitasExtenas.gContrase�a + ";";
                ECitasExtenas.LISTADO_CRYSTAL.SQLQuery = Sql;
				ECitasExtenas.LISTADO_CRYSTAL.Action = 1;
				ECitasExtenas.LISTADO_CRYSTAL.Reset();
                
			}
			catch (System.Exception excep)
			{
                ECitasExtenas.LISTADO_CRYSTAL.Reset();
                
				if (excep.Source == "CRYSTALREPORT")
				{
					Serrores.GestionErrorCrystal(Information.Err().Number, ECitasExtenas.RcAdmision);
				}
				else
				{
					Serrores.AnalizaError("ECITAEXTERNA", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "listado_citas_externas:imprimir", excep);
				}
			}
		}

		private bool validadatos()
		{
			bool result = false;
			int IResume = 0;
			Mensajes.ClassMensajes obML = null;
			try
			{
				obML = null;
				int tiI = 0;
				obML = new Mensajes.ClassMensajes();

				result = true;

				//Hora desde
				if (tbHoraDesde.Text != "  :  ")
				{
					tiI = (tbHoraDesde.Text.IndexOf(':') + 1);
					if (Convert.ToInt32(Double.Parse(tbHoraDesde.Text.Substring(0, Math.Min(tiI - 1, tbHoraDesde.Text.Length)))) >= 24 || Convert.ToInt32(Double.Parse(tbHoraDesde.Text.Substring(tiI))) >= 60)
					{
						IResume = Convert.ToInt32(obML.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1150, ECitasExtenas.RcAdmision, Label19.Text));
						tbHoraDesde.Focus();
						return false;
					}
				}
				else
				{	IResume = Convert.ToInt32(obML.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1040, ECitasExtenas.RcAdmision, Label19.Text));
					tbHoraDesde.Focus();
					return false;
				}

				//Hora hasta
				if (tbHoraHasta.Text != "  :  ")
				{
					tiI = (tbHoraHasta.Text.IndexOf(':') + 1);
					if (Convert.ToInt32(Double.Parse(tbHoraHasta.Text.Substring(0, Math.Min(tiI - 1, tbHoraHasta.Text.Length)))) >= 24 || Convert.ToInt32(Double.Parse(tbHoraHasta.Text.Substring(tiI))) >= 60)
					{
						IResume = Convert.ToInt32(obML.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1150, ECitasExtenas.RcAdmision, Label2.Text));
						tbHoraHasta.Focus();
						return false;
					}
				}
				else
				{
					IResume = Convert.ToInt32(obML.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1040, ECitasExtenas.RcAdmision, Label2.Text));
					tbHoraHasta.Focus();
					return false;
				}


				if (DateTime.Parse(SdcFechaHasta.Value.Date + " " + tbHoraHasta.Text) < DateTime.Parse(SdcFechaDesde.Value.ToShortDateString() + " " + tbHoraDesde.Text))
				{
					IResume = Convert.ToInt32(obML.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1020, ECitasExtenas.RcAdmision, Label21.Text, "menor o igual", Label1.Text));
					SdcFechaHasta.Focus();
					return false;
				}

				obML = null;
			}
			catch
			{
			}
            
			if (Information.Err().Number == 13)
			{
				IResume = Convert.ToInt32(obML.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1150, ECitasExtenas.RcAdmision, Label19.Text));
				result = false;
			}
			return result;
		}
		private void listado_citas_externas_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}