using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using ElementosCompartidos;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using CrystalWrapper;
namespace ECitaExterna
{
	public partial class docu_citas
		: Telerik.WinControls.UI.RadForm
    {
        public docu_citas()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void docu_citas_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		ClaseCitasAdmision objCitasDocumentos = null;
		Crystal ListadoParticular = null; // CRYSTAL REPORT

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
			try
			{
				if (ChbNotificacion.CheckState == CheckState.Checked)
				{
					ListadoParticular = ECitasExtenas.LISTADO_CRYSTAL;
					this.Cursor = Cursors.WaitCursor;

					string tempRefParam = objCitasDocumentos.FechaCita;
					if (!ECitasExtenas.proLlamarDllImpresionNotificacion(Convert.ToInt32(Double.Parse(objCitasDocumentos.NumeroRegistro)), Convert.ToInt32(Double.Parse(objCitasDocumentos.A�oRegistro)), ref tempRefParam, objCitasDocumentos.IdentificadorPaciente, ref ListadoParticular))
					{
						ECitasExtenas.RcAdmision.BeginTransScope();
                        proActualizarCampoFechaNotificacionCita();
                        ECitasExtenas.RcAdmision.CommitTransScope();
					}

					ListadoParticular = null;
					this.ChbNotificacion.CheckState = CheckState.Unchecked;
					this.Cursor = Cursors.Default;
				}

				if (this.ChbVolanteA.CheckState == CheckState.Checked)
				{
					this.Cursor = Cursors.WaitCursor;
					ListadoParticular = ECitasExtenas.LISTADO_CRYSTAL;
					proImpresionVolanteAmbulancia();
					ListadoParticular = null;
					this.ChbVolanteA.CheckState = CheckState.Unchecked;
					this.Cursor = Cursors.Default;
				}

				proActivar_Imprimir();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "docu_citas:CbImprimir_Click", ex);
			}

		}

		private void ChbNotificacion_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Imprimir();
		}

		private void ChbVolanteA_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivar_Imprimir();
		}

		private void docu_citas_Load(Object eventSender, EventArgs eventArgs)
		{
			objCitasDocumentos = menu_citas.DefInstance.ObjCitaAdmision;
			string nombre = menu_citas.DefInstance.LbPaciente.Text;
			lbPaciente.Text = nombre;
			Frame3.Text = Serrores.ObtenerLiteralPersona(ECitasExtenas.RcAdmision);
            menu_citas.DefInstance.radCommandBarStripElement1.Items[0].Enabled = false;
            if (objCitasDocumentos.CodigoCausaAmb == "")
			{
				this.ChbVolanteA.Enabled = false;
			}
		}


		private void proActivar_Imprimir()
		{
			cbImprimir.Enabled = this.ChbNotificacion.CheckState == CheckState.Checked || this.ChbVolanteA.CheckState == CheckState.Checked;
		}

		private void proActualizarCampoFechaNotificacionCita()
		{
			DataSet RrFechaNotificado = null;
			string sqlFechaNotificado = String.Empty;
			try
			{
				sqlFechaNotificado = "select ganoadme, gnumadme, fapuntes, inotifam, fnotifam, gusuario from acontcex where ganoadme=" + 
				                     objCitasDocumentos.A�oRegistro + " and gnumadme=" + objCitasDocumentos.NumeroRegistro + 
				                     " and fcitaext='" + DateTime.Parse(objCitasDocumentos.FechaCita).ToString("MM/dd/yyyy HH:mm") + "' and gidenpac='" + 
				                     objCitasDocumentos.IdentificadorPaciente + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlFechaNotificado, ECitasExtenas.RcAdmision);
				RrFechaNotificado = new DataSet();
				tempAdapter.Fill(RrFechaNotificado);
				if (RrFechaNotificado.Tables[0].Rows.Count == 1)
				{ // actualizamos la tabla de citas
					RrFechaNotificado.Edit();
					RrFechaNotificado.Tables[0].Rows[0]["inotifam"] = "S";
					RrFechaNotificado.Tables[0].Rows[0]["fnotifam"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
					RrFechaNotificado.Tables[0].Rows[0]["gusuario"] = ECitasExtenas.VstCodUsua;
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
					tempAdapter.Update(RrFechaNotificado, RrFechaNotificado.Tables[0].TableName);
				}
				RrFechaNotificado.Close();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "docu_citas:proActualizarCamposFechaNotificacionCita", ex);
			}
		}

		private void docu_citas_Closed(Object eventSender, EventArgs eventArgs)
		{
			menu_citas.DefInstance.radCommandBarStripElement1.Items[0].Enabled = true;
		}

		private void proImpresionVolanteAmbulancia()
		{
			try
			{
				DataSet RrProvinciaPpal = null; // SACAMOS EL CODIGO DE PROVINCIA DE sconsglo
				string sqlCita = String.Empty;
				DataSet RrCita = null;
				string sqlSociedad = String.Empty;
				DataSet RrSociedad = null;
                Int32 indiceRrSociedad = 0;
                string sqlPoliza = String.Empty;
				DataSet RrPoliza = null;
				string tstNuevo = String.Empty;
				sqlCita = "SELECT " + " DPACIENT.DAPE1PAC,DPACIENT.DAPE2PAC,DPACIENT.DNOMBPAC,dpacient.gidenpac, " + " Dpacient.GSOCIEDA,DPACIENT.IFILPROV,DPACIENT.NAFILIAC, " + " AEPISADM.gnumadme,AEPISADM.ganoadme " + " FROM " + " AEPISADM " + "   INNER JOIN DPACIENT ON DPACIENT.GIDENPAC = AEPISADM.GIDENPAC " + "   LEFT JOIN DENTIPAC ON AEPISADM.GIDENPAC = DENTIPAC.GIDENPAC " + " WHERE " + "   DPACIENT.GIDENPAC= '" + objCitasDocumentos.IdentificadorPaciente + "' and " + "   ganoadme= " + objCitasDocumentos.A�oRegistro + " AND " + 
				          "   gnumadme=" + objCitasDocumentos.NumeroRegistro;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCita, ECitasExtenas.RcAdmision);
				RrCita = new DataSet();
				tempAdapter.Fill(RrCita);
				if (RrCita.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(RrCita.Tables[0].Rows[0]["nafiliac"]))
					{
                        //INDRA jproche_TODO_X_5 Contiene codigo Crystal
						//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						//ListadoParticular.Formulas[1] = "POLIZA=\"" + Convert.ToString(RrCita.Tables[0].Rows[0]["nafiliac"]).Trim() + "\"";
                        ListadoParticular.Formulas["POLIZA"] = "\"" + Convert.ToString(RrCita.Tables[0].Rows[0]["nafiliac"]).Trim() + "\"";

                    }

					
					sqlSociedad = "SELECT Fmovimie,gsocieda " + " FROM  AMOVIFIN " + " WHERE " + "  AMOVIFIN.GNUMREGI=" + Convert.ToString(RrCita.Tables[0].Rows[0]["GNUMADME"]) + " AND " + "  AMOVIFIN.GANOREGI= " + Convert.ToString(RrCita.Tables[0].Rows[0]["GANOADME"]) + " order by fmovimie";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlSociedad, ECitasExtenas.RcAdmision);
					RrSociedad = new DataSet();
					tempAdapter_2.Fill(RrSociedad);
					indiceRrSociedad = RrSociedad.MoveLast(null);
					tstNuevo = Convert.ToString(RrSociedad.Tables[0].Rows[indiceRrSociedad]["gsocieda"]);
					RrSociedad.Close();

					sqlSociedad = "SELECT nnumeri1,valfanu1 FROM  SCONSGLO WHERE gconsglo = 'SEGURSOC'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlSociedad, ECitasExtenas.RcAdmision);
					RrSociedad = new DataSet();
					tempAdapter_3.Fill(RrSociedad);
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToInt32(RrSociedad.Tables[0].Rows[indiceRrSociedad]["NNUMERI1"]) == StringsHelper.ToDoubleSafe(tstNuevo))
					{ //Seguridad Social
                        //INDRA jproche_TODO_X_5 Contiene codigo Crystal
						//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						//ListadoParticular.Formulas[0] = "FINANCIADORA='" + Convert.ToString(RrSociedad.Tables[0].Rows[indiceRrSociedad]["VALFANU1"]).Trim() + "'";
                        ListadoParticular.Formulas["FINANCIADORA"] = "'" + Convert.ToString(RrSociedad.Tables[0].Rows[indiceRrSociedad]["VALFANU1"]).Trim() + "'";

                    }
					else
					{
						sqlSociedad = "SELECT NNUMERI1,VALFANU1 FROM  SCONSGLO WHERE gconsglo = 'PRIVADO'";
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlSociedad, ECitasExtenas.RcAdmision);
						RrSociedad = new DataSet();
						tempAdapter_4.Fill(RrSociedad);
						if (Convert.ToInt32(RrSociedad.Tables[0].Rows[indiceRrSociedad]["NNUMERI1"]) == StringsHelper.ToDoubleSafe(tstNuevo))
						{ // Privado
                            //INDRA jproche_TODO_X_5 Contiene codigo Crystal
							//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							//ListadoParticular.Formulas[0] = "FINANCIADORA='" + Convert.ToString(RrSociedad.Tables[0].Rows[indiceRrSociedad]["VALFANU1"]).Trim() + "'";
                            ListadoParticular.Formulas["FINANCIADORA"] = "'" + Convert.ToString(RrSociedad.Tables[0].Rows[indiceRrSociedad]["VALFANU1"]).Trim() + "'";
                        }
                        else
						{
							sqlSociedad = "SELECT dsocieda FROM DSOCIEDA WHERE gSOCIEDA = " + tstNuevo;
							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sqlSociedad, ECitasExtenas.RcAdmision);
							RrSociedad = new DataSet();
							tempAdapter_5.Fill(RrSociedad);
                            //INDRA jproche_TODO_X_5 Contiene codigo Crystal
							//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							//ListadoParticular.Formulas[0] = "FINANCIADORA='" + Convert.ToString(RrSociedad.Tables[0].Rows[indiceRrSociedad]["DSOCIEDA"]).Trim() + "'";
                            ListadoParticular.Formulas["FINANCIADORA"] = "'" + Convert.ToString(RrSociedad.Tables[0].Rows[indiceRrSociedad]["DSOCIEDA"]).Trim() + "'";

                        }
					}
					RrSociedad.Close();

				}

				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter("select dnomprov from dprovinc,sconsglo " + 
				                               " WHERE gprovinc=substring(valfanu1,1,2) AND  gconsglo='CODPROVI'", ECitasExtenas.RcAdmision);
				RrProvinciaPpal = new DataSet();
				tempAdapter_6.Fill(RrProvinciaPpal);
				if (!Convert.IsDBNull(RrProvinciaPpal.Tables[0].Rows[0][0]))
				{
                   //INDRA jproche_TODO_X_5 Contiene codigo Crystal
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					//ListadoParticular.Formulas[2] = "PROVINCIAHospital=\"" + Convert.ToString(RrProvinciaPpal.Tables[0].Rows[0][0]).Trim() + ", " + "\"";
                    ListadoParticular.Formulas["PROVINCIAHospital"] = "\"" + Convert.ToString(RrProvinciaPpal.Tables[0].Rows[0][0]).Trim() + ", " + "\"";

                }
				RrProvinciaPpal.Close();

				if (!ECitasExtenas.VstrCrystal.VfCabecera)
				{
					ECitasExtenas.proCabCrystal();
				}
                //INDRA jproche_TODO_X_5 Contiene codigo Crystal
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				//ListadoParticular.Formulas[3] = "FORM1= \"" + ECitasExtenas.VstrCrystal.VstGrupoHo + "\"";
                ListadoParticular.Formulas["FORM1"] = "\"" + ECitasExtenas.VstrCrystal.VstGrupoHo + "\"";
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //ListadoParticular.Formulas[4] = "FORM2= \"" + ECitasExtenas.VstrCrystal.VstNombreHo + "\"";
                ListadoParticular.Formulas["FORM2"] = "\"" + ECitasExtenas.VstrCrystal.VstNombreHo + "\"";
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //ListadoParticular.Formulas[5] = "FORM3= \"" + ECitasExtenas.VstrCrystal.VstDireccHo + "\"";
                ListadoParticular.Formulas["FORM3"] = "\"" + ECitasExtenas.VstrCrystal.VstDireccHo + "\"";
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //ListadoParticular.Formulas[6] = "FORM4= 'VOLANTE DE AMBULANCIA'";
                ListadoParticular.Formulas["FORM4"] = "'VOLANTE DE AMBULANCIA'";
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //ListadoParticular.Formulas[7] = "LogoIDC = \"" + Serrores.ExisteLogo(ECitasExtenas.RcAdmision) + "\"";
                ListadoParticular.Formulas["LogoIDC"] = "LogoIDC = \"" + Serrores.ExisteLogo(ECitasExtenas.RcAdmision) + "\"";
				//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.ReportFileName = ECitasExtenas.PathString + "\\agi193r1_CR11.rpt";
				//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.Destination = Crystal.DestinationConstants.crptToPrinter;
				//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.Connect = "DSN=" + ECitasExtenas.VstCrystal + ";UID=" + ECitasExtenas.gUsuario + ";PWD=" + ECitasExtenas.gContrase�a;

				//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				object tempRefParam = objCitasDocumentos.FechaCita;
				ListadoParticular.SQLQuery = "select * from acontcex " + 
				                             "  INNER JOIN dpacient ON acontcex.gidenpac = dpacient.gidenpac " + 
				                             "  INNER JOIN dcausamb ON acontcex.gcauambu = dcausamb.gcauambu " + 
				                             "  LEFT JOIN AMODTRAN ON acontcex.gmodtran = AMODTRAN.gmodtran " + 
				                             "  LEFT JOIN dprovinc ON acontcex.gprovinc = dprovinc.gprovinc " + 
				                             "  LEFT JOIN dpoblaci ON acontcex.gpoblaci = dpoblaci.gpoblaci " + 
				                             " where " + 
				                             "  acontcex.ganoadme=" + objCitasDocumentos.A�oRegistro + " and " + 
				                             "  acontcex.gnumadme=" + objCitasDocumentos.NumeroRegistro + " and " + 
				                             "  acontcex.gidenpac='" + objCitasDocumentos.IdentificadorPaciente + "' and" + 
				                             "  acontcex.fcitaext=" + Serrores.FormatFechaHM(tempRefParam);

				//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.SubreportToChange = "LogotipoIDC";
				//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.Connect = "DSN=" + ECitasExtenas.VstCrystal + ";UID=" + ECitasExtenas.gUsuario + ";PWD=" + ECitasExtenas.gContrase�a + "";
				//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.SQLQuery = "select * from slogodoc";
				//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.SubreportToChange = "";

				//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.WindowTitle = "Volante de ambulancia";
				//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
				//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.Action = 1;
				//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				//ListadoParticular.PageCount();
				//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.Reset();
                
				ListadoParticular = null;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), ECitasExtenas.VstCodUsua, "docu_citas:proImpresionVolanteAmbulancia", ex);
                //INDRA jproche_TODO_X_5 Contiene codigo Crystal
				//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				ListadoParticular.Reset();
				ListadoParticular = null;
                
				this.Cursor = Cursors.Default;
			}

		}
	}
}