using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;

namespace ECitaExterna
{
	public class ClaseCitasAdmision
	{
		//PROPIEDADES DEL OBJECTO "reservas en Admision"

		string Identificador = String.Empty; // obligatorio
		string FechaCitaExterna = String.Empty;
		string FechaCitaOtroC = String.Empty; // obligatorio
		string NombreCentro = String.Empty; // obligatorio
		string DireccionCentro = String.Empty; // obligatorio
		string CodigoPostal = String.Empty;
		string CodigoProvincia = String.Empty;
		string CodigoPoblacion = String.Empty;
		string NombreEspecialidad = String.Empty;
		string DescripcionMedico = String.Empty;
		string DescripcionPrueba = String.Empty;
		string iNecesitaAmbulancia = String.Empty;
		string CodigoCausaAmbulancia = String.Empty;
		string iNotificadoFamiliar = String.Empty;
		string FechaNotificadoFamiliar = String.Empty;
		string vstusuario = String.Empty;
		string a�o = String.Empty;
		string Numero = String.Empty;
		string iNecesitaAcompa�ante = String.Empty;
		string CodigoModoTransporte = String.Empty;


		//LISTA DE FUNCIONES ASOCIADAS A LAS PROPIEDADES


		//***************************************************************************************
		//******************************PROPIEDADES A ESTABLECER*********************************
		//***************************************************************************************

		public string IdentificadorPaciente
		{
			get
			{
				return Identificador;
			}
			set
			{
				Identificador = value;
			}
		}



		public string FechaCita
		{
			get
			{
				return FechaCitaExterna;
			}
			set
			{
				FechaCitaExterna = value;
			}
		}

		public string FechaCitaOtroCentro
		{
			get
			{
				return FechaCitaOtroC;
			}
			set
			{
				FechaCitaOtroC = value;
			}
		}


		public string Centro
		{
			get
			{
				return NombreCentro;
			}
			set
			{
				NombreCentro = value;
			}
		}



		public string DireccCentro
		{
			get
			{
				return DireccionCentro;
			}
			set
			{
				DireccionCentro = value;
			}
		}

		public string CodPostal
		{
			get
			{
				return CodigoPostal;
			}
			set
			{
				CodigoPostal = value;
			}
		}

		public string CodProvincia
		{
			get
			{
				return CodigoProvincia;
			}
			set
			{
				CodigoProvincia = value;
			}
		}

		public string CodPoblacion
		{
			get
			{
				return CodigoPoblacion;
			}
			set
			{
				CodigoPoblacion = value;
			}
		}

		public string medico
		{
			get
			{
				return DescripcionMedico;
			}
			set
			{
				DescripcionMedico = value;
			}
		}

		public string Prueba
		{
			get
			{
				return DescripcionPrueba;
			}
			set
			{
				DescripcionPrueba = value;
			}
		}

		public string NecesitaAmbulancia
		{
			get
			{
				return iNecesitaAmbulancia;
			}
			set
			{
				iNecesitaAmbulancia = value;
			}
		}

		public string NecesitaAcompa�ante
		{
			get
			{
				return iNecesitaAcompa�ante;
			}
			set
			{
				iNecesitaAcompa�ante = value;
			}
		}

		public string CodigoCausaAmb
		{
			get
			{
				return CodigoCausaAmbulancia;
			}
			set
			{
				CodigoCausaAmbulancia = value;
			}
		}

		public string CodigoModoTrans
		{
			get
			{
				return CodigoModoTransporte;
			}
			set
			{
				CodigoModoTransporte = value;
			}
		}

		public string NotificadoFamiliar
		{
			get
			{
				return iNotificadoFamiliar;
			}
			set
			{
				iNotificadoFamiliar = value;
			}
		}

		public string FechaNotificadoFamilia
		{
			get
			{
				return FechaNotificadoFamiliar;
			}
			set
			{
				FechaNotificadoFamiliar = value;
			}
		}



		public string usuario
		{
			get
			{
				return vstusuario;
			}
			set
			{
				vstusuario = value;
			}
		}



		public string A�oRegistro
		{
			get
			{
				return a�o;
			}
			set
			{
				a�o = value;
			}
		}

		public string NumeroRegistro
		{
			get
			{
				return Numero;
			}
			set
			{
				Numero = value;
			}
		}



		public string Especialidad
		{
			get
			{
				return NombreEspecialidad;
			}
			set
			{
				NombreEspecialidad = value;
			}
		}



		//Public Function Actualizar(CodigoAccion As Long, RcReserva As rdoConnection)
		//    RealizarAccion CodigoAccion
		//End Function
		//
		// pone a todas a EMPTY
		public object Reset()
		{
			Identificador = "";
			FechaCitaExterna = "";
			FechaCitaOtroC = "";
			NombreCentro = "";
			DireccionCentro = "";
			CodigoPostal = "";
			CodigoProvincia = "";
			CodigoPoblacion = "";
			NombreEspecialidad = "";
			DescripcionMedico = "";
			DescripcionPrueba = "";
			iNecesitaAmbulancia = "";
			iNecesitaAcompa�ante = "";
			CodigoCausaAmbulancia = "";
			CodigoModoTransporte = "";
			iNotificadoFamiliar = "";
			FechaNotificadoFamiliar = "";
			vstusuario = "";
			a�o = "";
			Numero = "";
			return null;
		}

		// SE EDITA O A�ADE CON LOS DATOS QUE SE OBTIENEn de  ClaseCita
		// Cuando se edita necesito los datos anteriores para poder recuperar el registro

		public object EditarCita(SqlConnection RcReserva, ClaseCitasAdmision ClaseCita, System.DateTime FechaCitaAntigua, System.DateTime FechaCitaAntOtra, System.DateTime fechaOrden)
		{
			string sqlEditar = String.Empty;
			DataSet RrEditar = null;

			try
			{

				sqlEditar = "select * from acontcex where ganoadme=" + ClaseCita.A�oRegistro;
				sqlEditar = sqlEditar + " and gnumadme=" + ClaseCita.NumeroRegistro;
				//If FechaCitaAntigua <> "00:00:00" Then
				//    sqlEditar = sqlEditar & " and fcitaext='" & Format(FechaCitaAntigua, "mm/dd/yyyy hh:nn") & "'"
				//End If
				sqlEditar = sqlEditar + " and fapuntes='" + FechaCitaAntOtra.ToString("MM/dd/yyyy HH:mm") + "'";
				sqlEditar = sqlEditar + " and gidenpac='" + ClaseCita.IdentificadorPaciente + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEditar, RcReserva);
				RrEditar = new DataSet();
				tempAdapter.Fill(RrEditar);
				if (RrEditar.Tables[0].Rows.Count == 1)
				{
					RrEditar.Edit();
					//    RrEditar("gidenpac") =  ClaseCita .IdentificadorPaciente
					//O.Frias - 09/09/2009
					//Nuevo campo para ordenacion
					if (FechaCitaAntigua != DateTime.Parse("00:00:00"))
					{
						RrEditar.Tables[0].Rows[0]["fcitaext"] = DateTime.Parse(ClaseCita.FechaCita);
						RrEditar.Tables[0].Rows[0]["fcitaord"] = DateTime.Parse(ClaseCita.FechaCita);
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["fcitaext"] = DBNull.Value;
						RrEditar.Tables[0].Rows[0]["fcitaord"] = fechaOrden;
					}
					RrEditar.Tables[0].Rows[0]["fapuntes"] = DateTime.Parse(ClaseCita.FechaCitaOtroCentro);
					RrEditar.Tables[0].Rows[0]["dnomcent"] = ClaseCita.Centro.Trim(); // OBLIGATORIO
					if (ClaseCita.DireccCentro.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["ddirecen"] = ClaseCita.DireccCentro.Trim();
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["ddirecen"] = DBNull.Value;
					}
					if (ClaseCita.CodPostal.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["gcodipos"] = ClaseCita.CodPostal;
					}
					else
					{	
						RrEditar.Tables[0].Rows[0]["gcodipos"] = DBNull.Value;
					}
					if (ClaseCita.CodProvincia.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["gprovinc"] = ClaseCita.CodProvincia;
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["gprovinc"] = DBNull.Value;
					}
					if (ClaseCita.CodPoblacion.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["gpoblaci"] = ClaseCita.CodPoblacion;
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["gpoblaci"] = DBNull.Value;
					}
					if (ClaseCita.Especialidad.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["dnomespe"] = ClaseCita.Especialidad;
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["dnomespe"] = DBNull.Value;
					}
					if (ClaseCita.medico.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["dmedico"] = ClaseCita.medico;
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["dmedico"] = DBNull.Value;
					}
					RrEditar.Tables[0].Rows[0]["dprueba"] = ClaseCita.Prueba; //obligatorio
					RrEditar.Tables[0].Rows[0]["inecambu"] = ClaseCita.NecesitaAmbulancia;
					//(maplaza)(14/05/2007)Puede darse el caso de que el campo sea NULO.
					//RrEditar("inecacom") = ClaseCita.NecesitaAcompa�ante
					if (ClaseCita.NecesitaAcompa�ante.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["inecacom"] = ClaseCita.NecesitaAcompa�ante;
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["inecacom"] = DBNull.Value;
					}
					//-----
					if (ClaseCita.CodigoCausaAmb.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["gcauambu"] = Convert.ToInt32(Double.Parse(ClaseCita.CodigoCausaAmb));
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["gcauambu"] = DBNull.Value;
					}
					if (ClaseCita.CodigoModoTrans.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["gmodtran"] = Convert.ToInt32(Double.Parse(ClaseCita.CodigoModoTrans));
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["gmodtran"] = DBNull.Value;
					}

                    RrEditar.Tables[0].Rows[0]["inotifam"] = ClaseCita.NotificadoFamiliar;
					if (ClaseCita.FechaNotificadoFamilia.Trim() != "")
					{
						RrEditar.Tables[0].Rows[0]["fnotifam"] = ClaseCita.FechaNotificadoFamilia;
					}
					else
					{
						RrEditar.Tables[0].Rows[0]["fnotifam"] = DBNull.Value;
					}
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
					tempAdapter.Update(RrEditar, RrEditar.Tables[0].TableName);
				}
				
				RrEditar.Close();
			}
			catch (Exception e)
			{	
				if (Information.Err().Number == 40002)
				{
					RadMessageBox.Show("Ya existe un registro con la misma fecha de apunte", Application.ProductName);
                    return null;
				}
			}
			return null;
		}


		public object A�adirCita(SqlConnection RcReserva, ClaseCitasAdmision ClaseCita, System.DateTime fechaOrden)
		{
			string sqlA�adir = String.Empty;
			DataSet RrA�adir = null;

			try
			{

				sqlA�adir = "select * from acontcex where 1=2";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlA�adir, RcReserva);
				RrA�adir = new DataSet();
				tempAdapter.Fill(RrA�adir);
				RrA�adir.AddNew();
				RrA�adir.Tables[0].Rows[0]["gidenpac"] = ClaseCita.IdentificadorPaciente;
				RrA�adir.Tables[0].Rows[0]["ganoadme"] = ClaseCita.A�oRegistro;
				RrA�adir.Tables[0].Rows[0]["gnumadme"] = ClaseCita.NumeroRegistro;
				if (ClaseCita.FechaCita != "   :  ")
				{
					RrA�adir.Tables[0].Rows[0]["fcitaext"] = DateTime.Parse(ClaseCita.FechaCita);
					RrA�adir.Tables[0].Rows[0]["fcitaord"] = DateTime.Parse(ClaseCita.FechaCita);
				}
				else
				{
                    RrA�adir.Tables[0].Rows[0]["fcitaext"] = DBNull.Value;
					RrA�adir.Tables[0].Rows[0]["fcitaord"] = fechaOrden;
				}
				RrA�adir.Tables[0].Rows[0]["fapuntes"] = DateTime.Parse(ClaseCita.FechaCitaOtroCentro);
				RrA�adir.Tables[0].Rows[0]["dnomcent"] = ClaseCita.Centro.Trim(); // OBLIGATORIO
				if (ClaseCita.DireccCentro.Trim() != "")
				{
					RrA�adir.Tables[0].Rows[0]["ddirecen"] = ClaseCita.DireccCentro.Trim();
				}
				if (ClaseCita.CodPostal.Trim() != "")
				{
					RrA�adir.Tables[0].Rows[0]["gcodipos"] = ClaseCita.CodPostal;
				}
				if (ClaseCita.CodProvincia.Trim() != "")
				{
					RrA�adir.Tables[0].Rows[0]["gprovinc"] = ClaseCita.CodProvincia;
				}
				if (ClaseCita.CodPoblacion.Trim() != "")
				{
					RrA�adir.Tables[0].Rows[0]["gpoblaci"] = ClaseCita.CodPoblacion;
				}
				if (ClaseCita.Especialidad.Trim() != "")
				{
					RrA�adir.Tables[0].Rows[0]["dnomespe"] = ClaseCita.Especialidad;
				}
				if (ClaseCita.medico.Trim() != "")
				{
					RrA�adir.Tables[0].Rows[0]["dmedico"] = ClaseCita.medico;
				}

				RrA�adir.Tables[0].Rows[0]["dprueba"] = ClaseCita.Prueba; //obligatorio
				RrA�adir.Tables[0].Rows[0]["inecambu"] = ClaseCita.NecesitaAmbulancia;
				//(maplaza)(14/05/2007)Puede darse el caso de que el campo sea NULO.
				//RrA�adir("inecacom") = ClaseCita.NecesitaAcompa�ante
				if (ClaseCita.NecesitaAcompa�ante.Trim() != "")
				{
					RrA�adir.Tables[0].Rows[0]["inecacom"] = ClaseCita.NecesitaAcompa�ante;
				}
				else
				{
					RrA�adir.Tables[0].Rows[0]["inecacom"] = DBNull.Value;
				}
				//-----
				if (ClaseCita.CodigoCausaAmb.Trim() != "")
				{
					RrA�adir.Tables[0].Rows[0]["gcauambu"] = Convert.ToInt32(Double.Parse(ClaseCita.CodigoCausaAmb));
				}
				if (ClaseCita.CodigoModoTrans.Trim() != "")
				{
					RrA�adir.Tables[0].Rows[0]["gmodtran"] = Convert.ToInt32(Double.Parse(ClaseCita.CodigoModoTrans));
				}
				
				RrA�adir.Tables[0].Rows[0]["inotifam"] = "N"; // al a�adir poner la notificacion a "N"
				//        If Trim(ClaseCita.FechaNotificadoFamilia) <> "" Then
				//            RrA�adir("fnotifam") = ClaseCita.FechaNotificadoFamilia
				//        End If
				RrA�adir.Tables[0].Rows[0]["gusuario"] = ClaseCita.usuario;
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
				tempAdapter.Update(RrA�adir, RrA�adir.Tables[0].TableName);
				RrA�adir.Close();
			}
			catch (Exception e)
			{	
				if (Information.Err().Number == 40002)
				{
					RadMessageBox.Show("Ya existe un registro con la misma fecha de apunte", Application.ProductName);
                    return null;
				}
			}
			return null;
		}
	}
}