using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.DB.DAO;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using ElementosCompartidos;

namespace Listado_Genereal_Camas
{
	public partial class List_general_camas
		: RadForm
	{

		string stsql = String.Empty;
		string stsql1 = String.Empty;
		string stsql2 = String.Empty;
		string stsql3 = String.Empty;
		string stGrupoHo = String.Empty;
		string stNombreHo = String.Empty;
		string stDireccHo = String.Empty;
		string PathString = String.Empty;
		string stSeleccion = String.Empty;
		string stUnidad = String.Empty;
		bool VfCabecera = false;
		string vstEnfermeria = String.Empty;

		DataSet Rr1 = null;
		DataSet rr2 = null;
		DataSet Rr3 = null;

		SqlConnection rcAdmision = null;
		object CrystalReport1 = null;
		string VstUsuadns = String.Empty;
		string VstPassdns = String.Empty;

		string StBaseTemporal = String.Empty;
		//Usuario conectado en el modulo de enfermeria
		//Si viene, filtraremos las unidades de enfermeria a las que el usuario tiene acceso.
		//(que seran las mismas que en el menu "VER")
		//y si no viene usuario conectado, significa que la llamada se realiza desde el
		//modulo de admision por lo que mostraremos todas las unidades de enfermeria disponibles.
		string gUsuarioENF = String.Empty;

		//''OSCAR C: PARECE QUE ESTA FUNCION SE HA SUSTITUIDO POR RELLENATABLATEMPORAL
		//''Sub listado_query()
		//''
		//''    stSeleccion = ""
		//''    stUnidad = UCase(stUnidad)
		//''
		//''    'OSCAR C 25/10/2004
		//''    'seleccion de las unidades de enfermeria elegidas
		//''    Dim stWhereDUNIENFEVA As String
		//''    Dim ilista As Integer
		//''    If lsbUnidades.Items.Count <> 0 And lsbSeleccion.Items.Count <> 0 Then  ' si no es cero recorrer el de disponibles
		//''        stWhereDUNIENFEVA = " and ("
		//''        For ilista = 0 To lsbSeleccion.Items.Count - 1
		//''            If ilista = lsbSeleccion.Items.Count - 1 Then ' si es el ULTIMO
		//''                stWhereDUNIENFEVA = stWhereDUNIENFEVA & "DUNIENFEVA.GUNIDENF='" & lsbSeleccion.Column(1, ilista) & "')"
		//''            Else  ' si no es el ultimo
		//''                stWhereDUNIENFEVA = stWhereDUNIENFEVA & "DUNIENFEVA.GUNIDENF='" & lsbSeleccion.Column(1, ilista) & "' or "
		//''            End If
		//''        Next
		//''    End If
		//''    '-------
		//''
		//''
		//''    If rbUnienfe.Value = True Then
		//'''SQL para el caso en el que este seleccionado el boton de unidad de enfermeria
		//''         stSeleccion = "Unidad de Enfermerķa"
		//''
		//''         stsql = " SELECT " _
		//'''            & " DUNIENFEVA.dunidenf,DUNIENFEVA.gunidenf, " _
		//'''            & " DCAMASBOVA.gplantas, DCAMASBOVA.ghabitac," _
		//'''            & " DCAMASBOVA.gcamasbo, DCAMASBOVA.gcounien," _
		//'''            & " DCAMASBOVA.gtipocam , DCAMASBOVA.iestcama," _
		//'''            & " DCAMASBOVA.irestsex, DCAMASBOVA.igeneest," _
		//'''            & " DCAMASBOVA.irestsex, DCAMASBOVA.gmotinha,DCAMASBOVA.ivirtual," _
		//'''            & " AEPISADM.fprevalt, " _
		//'''            & " DTIPOCAMVA.dtipcama,DTIPOCAMVA.gtipocam, " _
		//'''            & " DMOTINHAVA.dinhabil, DMOTINHAVA.gmotinha " _
		//'''            & " FROM " _
		//'''            & " DCAMASBOVA, DTIPOCAMVA, DMOTINHAVA, DUNIENFEVA, AEPISADM " _
		//'''        & " WHERE " _
		//'''            & " DUNIENFEVA.itiposer = '" & stUnidad & "' AND " _
		//'''            & " DCAMASBOVA.gtipocam *= DTIPOCAMVA.gtipocam AND " _
		//'''            & " DUNIENFEVA.gunidenf = DCAMASBOVA.gcounien AND " _
		//'''            & " DCAMASBOVA.gmotinha *= DMOTINHAVA.gmotinha AND " _
		//'''            & " DCAMASBOVA.gidenpac *= AEPISADM.gidenpac AND " _
		//'''            & " AEPISADM.faltplan is null "
		//''
		//''        'Oscar C 25/10/2004
		//''        'If vstEnfermeria <> "" Then
		//''        '    StSql = StSql & "AND DUNIENFEVA.GUNIDENF= '" & vstEnfermeria & "'"
		//''        'End If
		//''        stsql = stsql & stWhereDUNIENFEVA
		//''        '------------------
		//''
		//'''        CrystalReport1.SortFields(0) = "+{DUNIENFEVA.dunidenf}"
		//'''        CrystalReport1.SortFields(1) = "+{DCAMASBOVA.gplantas}"
		//'''        CrystalReport1.SortFields(2) = "+{DCAMASBOVA.ghabitac}"
		//'''        CrystalReport1.SortFields(3) = "+{DCAMASBOVA.gcamasbo}"
		//''
		//''        stsql = stsql & Chr(13) & Chr(10) & " Order By " _
		//'''            & " DUNIENFEVA.dunidenf ASC, " _
		//'''            & " DCAMASBOVA.gplantas ASC, " _
		//'''            & " DCAMASBOVA.ghabitac ASC, " _
		//'''            & " DCAMASBOVA.gcamasbo ASC "
		//''    Else
		//'''SQL para el caso en el que este seleccionada el boton de estado
		//''        stSeleccion = "Estado"
		//''
		//''            stsql = " SELECT " _
		//'''                & " DCAMASBOVA.gplantas, DCAMASBOVA.ghabitac," _
		//'''                & " DCAMASBOVA.gcamasbo , DCAMASBOVA.gcounien," _
		//'''                & " DCAMASBOVA.irestsex , DCAMASBOVA.igeneest," _
		//'''                & " DCAMASBOVA.iestcama, DCAMASBOVA.gmotinha,DCAMASBOVA.ivirtual," _
		//'''                & " AEPISADM.fprevalt, " _
		//'''                & " DUNIENFEVA.dunidenf, DUNIENFEVA.gunidenf,DUNIENFEVA.itiposer, " _
		//'''                & " DTIPOCAMVA.dtipcama, " _
		//'''                & " DMOTINHAVA.dinhabil " _
		//'''            & " From " _
		//'''                & " DCAMASBOVA, DTIPOCAMVA, DMOTINHAVA, DUNIENFEVA, AEPISADM " _
		//'''            & " Where " _
		//'''                & " DUNIENFEVA.itiposer = '" & stUnidad & "' AND " _
		//'''                & " DCAMASBOVA.gtipocam *= DTIPOCAMVA.gtipocam AND " _
		//'''                & " DCAMASBOVA.gmotinha *= DMOTINHAVA.gmotinha AND " _
		//'''                & " DUNIENFEVA.gunidenf = DCAMASBOVA.gcounien AND " _
		//'''                & " DCAMASBOVA.gidenpac *= AEPISADM.gidenpac AND " _
		//'''                & " AEPISADM.faltplan is null "
		//''
		//''
		//''            'Oscar C 25/10/2004
		//''            'If vstEnfermeria <> "" Then
		//''            '   StSql = StSql & "AND DUNIENFEVA.GUNIDENF= '" & vstEnfermeria & "'"
		//''            'End If
		//''            stsql = stsql & stWhereDUNIENFEVA
		//''            '------------------
		//''
		//''            stsql = stsql & Chr(13) & Chr(10) & " Order By " _
		//'''                & " DCAMASBOVA.iestcama ASC, " _
		//'''                & " DCAMASBOVA.gplantas ASC, " _
		//'''                & " DCAMASBOVA.ghabitac ASC, " _
		//'''                & " DCAMASBOVA.gcamasbo ASC "
		//''    End If
		//''
		//''
		public List_general_camas()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}
        //''End Sub

        //DGMORENOG TODO_X_4 - USO CRISTAL REPORT
        //public void proimprimir(Crystal.DestinationConstants ParaSalida)
        public void proimprimir()
        {
            //DGMORENOG TODO_X_4 - USO CRISTAL REPORT - INICIO
            /*try
			{
				//listado_query
				StBaseTemporal = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + Module1.VstCrystal + ".mdb";
				RellenaTablaTemporal();
                
				//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.Destination = (int) ParaSalida;
				//CrystalReport1.SQLQuery = stsql
				//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.ReportFileName = "" + PathString + "\\..\\ElementosComunes\\rpt\\Agi330f1.rpt";

				//*********cabeceras
				if (!VfCabecera)
				{
					proCabCrystal();
				}
				//*******************

				for (int x = 0; x <= 4; x++)
				{
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					CrystalReport1.Formulas[x] = "";
				}

				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.Formulas["FORM1"] = "\"" + stGrupoHo + "\"";
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.Formulas["FORM2"] = "\"" + stNombreHo + "\"";
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.Formulas["FORM3"] = "\"" + stDireccHo + "\"";
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.Formulas["SELECCION"] = "\"" + stSeleccion + "\"";
				//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.WindowTitle = "Listado General de Camas";
				//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
				//UPGRADE_TODO: (1067) Member WindowShowPrintSetupBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.WindowShowPrintSetupBtn = true;
				//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				if (CrystalReport1.Destination == ((int) Crystal.DestinationConstants.crptToPrinter))
				{
					proDialogo_impre(CrystalReport1);
				}

				//CrystalReport1.Connect = "DSN=" & VstCrystal & ";UID=" & VstUsuadns & ";PWD=" & VstPassdns & ""
				//UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.DataFiles[0] = StBaseTemporal;

				//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.Action = 1;
				//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.PrinterStopPage = CrystalReport1.PageCount;
				for (int x = 0; x <= 4; x++)
				{
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					CrystalReport1.Formulas[x] = "";
					//UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					CrystalReport1.SortFields[x] = "";
				}
				//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CrystalReport1.Reset();
				if (vstEnfermeria == "")
				{
					cbImprimir.Enabled = false;
					cbPantalla.Enabled = false;
					rbEstado.Checked = false;
					rbUnienfe.Checked = false;
				}

				ProInicializarPantalla();

				File.Delete(StBaseTemporal);
			}
			catch (Exception e)
			{
				this.Cursor = Cursors.Default;
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 32755)
				{
					//CANCELADA IMPRESION
				}
				else
				{
					Serrores.AnalizaError("LISTADO_CAMAS", Path.GetDirectoryName(Application.ExecutablePath), "USUADESCONOCIDO", "LIST_GENERAL_CAMAS:PROIMPRIMIR", rcAdmision);
				}
			}*/
            //DGMORENOG TODO_X_4 - USO CRISTAL REPORT - FIN
        }

        private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			CrystalReport1 = null;
			this.Close();
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
            //DGMORENOG TODO_X_4 - USO CRISTAL REPORT
            //proimprimir(Crystal.DestinationConstants.crptToPrinter);            
        }

        private void cbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{
            //DGMORENOG TODO_X_4 - USO CRISTAL REPORT
            //proimprimir(Crystal.DestinationConstants.crptToWindow);
        }

        private void List_general_camas_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				//If vstEnfermeria <> "" Then
				//    rbEstado.Value = True
				//    rbUnienfe.Value = False
				//    rbEstado.Enabled = False
				//    rbUnienfe.Enabled = False
				//    cbImprimir.Enabled = True
				//    cbPantalla.Enabled = True
				//
				//Else
				cbImprimir.Enabled = false;
				cbPantalla.Enabled = false;
				//End If
			}
		}
        
        public void proDialogo_impre(object Pcrystal)
        {
            //DGMORENOG TODO_X_4 - USO CRISTAL REPORT - INICIO
            /*Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
            //UPGRADE_WARNING: (6022) The CommonDialog CancelError property is not supported in .NET. More Information: http://www.vbtonet.com/ewis/ewi6022.aspx
            Cuadro_Diag_impre.CancelError = true;
            //UPGRADE_ISSUE: (2064) MSComDlg.CommonDialog property Cuadro_Diag_impre.PrinterDefault was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            Cuadro_Diag_impre.setPrinterDefault(true);
			Cuadro_Diag_imprePrint.ShowDialog();
			//UPGRADE_TODO: (1067) Member CopiesToPrinter is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Pcrystal.CopiesToPrinter = Cuadro_Diag_imprePrint.PrinterSettings.Copies;*/
            //DGMORENOG TODO_X_4 - USO CRISTAL REPORT - FIN
        }

        private void List_general_camas_Load(Object eventSender, EventArgs eventArgs)
		{
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			ProInicializarPantalla();
		}

		private bool isInitializingComponent;

        private void rbEstado_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				//cbImprimir.Enabled = True
				//cbPantalla.Enabled = True
				proActivarAceptar();
			}
		}

		private void rbUnienfe_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				//cbImprimir.Enabled = True
				//cbPantalla.Enabled = True
				proActivarAceptar();
			}
		}

		public void proReferenciasF(mcList_Camas mcvar1, object mfvar2, SqlConnection srcvar3, string PathString_Recibido, string stUnidad_Recibida, string paraenfermeria, object listado_camas, string stUsuario, string stPassword, string ParaUsuarioENF)
		{
			PathString = PathString_Recibido;
			stUnidad = stUnidad_Recibida;
			rcAdmision = srcvar3;
			vstEnfermeria = paraenfermeria;
			VstUsuadns = stUsuario;
			VstPassdns = stPassword;
			CrystalReport1 = listado_camas;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref rcAdmision);
			gUsuarioENF = ParaUsuarioENF;
		}

		public void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************
			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, rcAdmision);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			
			stGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, rcAdmision);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			
			stNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, rcAdmision);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			
			stDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();			
			tRrCrystal.Close();
			VfCabecera = true;
		}

		//---------------- OSCAR C 25/10/2004 --------------------------------
		public void ProInicializarPantalla()
		{
			lsbUnidades.Items.Clear();
			lsbSeleccion.Items.Clear();
			proRellenarUnidades();
			cbMayorMayor.Enabled = true;
			cbMayor.Enabled = false;
			cbMenor.Enabled = false;
			cbMenorMenor.Enabled = false;
			proActivarAceptar();
		}

		private void proRellenarUnidades()
		{
			string sqlUnidades = String.Empty;
            
            /*lsbUnidades.ColumnCount = 2;
			lsbUnidades.ColumnWidths = "30;0";
			lsbSeleccion.ColumnCount = 2;
			lsbSeleccion.ColumnWidths = "30;0";*/

			//gUsuarioENF: Usuario conectado en el modulo de enfermeria
			//Si viene, filtraremos las unidades de enfermeria a las que el usuario tiene acceso.
			//(que seran las mismas que en el menu "VER")
			//y si no viene usuario conectado, significa que la llamada se realiza desde el
			//modulo de admision por lo que mostraremos todas las unidades de enfermeria disponibles.

			if (gUsuarioENF != "")
			{
				sqlUnidades = "select T1.dunidenf , T1.gunidenf FROM (" + 
				              "select DUNIENFEVA.dunidenf , DUNIENFEVA.gunidenf " + 
				              "From susuario , DUNIENFEVA Where " + 
				              "susuario.gusuario = '" + gUsuarioENF + "' and " + 
				              "DUNIENFEVA.gunidenf = susuario.gunidenf " + 
				              " UNION ALL " + 
				              "select DUNIENFEVA.dunidenf , DUNIENFEVA.gunidenf " + 
				              "From DUNIENFEVA , dunicont Where " + 
				              "dunicont.gusuario = '" + gUsuarioENF + "' and " + 
				              "DUNIENFEVA.gunidenf = dunicont.guniacce ) T1" + 
				              " group by T1.dunidenf, T1.gunidenf";
				sqlUnidades = sqlUnidades + " order by dunidenf, gunidenf ";
			}
			else
			{
				sqlUnidades = "select dunidenf,gunidenf from " + 
				              " DUNIENFEVA where DUNIENFEVA.itiposer = '" + stUnidad + "' order by dunidenf";
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlUnidades, rcAdmision);
			DataSet RrUnidades = new DataSet();
			tempAdapter.Fill(RrUnidades);
			int ilistas = 0;
			int ilistau = 0;
			foreach (DataRow iteration_row in RrUnidades.Tables[0].Rows)
			{
				if (vstEnfermeria != "")
				{
					//Si tiene unidad de enfermeria, es que viene directamente del modulo de enfermeria,
					//por lo que internamente se pondra en la lista de unidades seleccionadas, la unidad
					//de enfermeria que viniese.
					if (Convert.ToString(iteration_row["GUNIDENF"]) == vstEnfermeria)
					{                        
                        RadListDataItem item = new RadListDataItem(Convert.ToString(iteration_row["DUNIDENF"]).Trim(), Convert.ToString(iteration_row["GUNIDENF"]).Trim());
                        lsbSeleccion.Items.Add(item);
                        ilistas++;
					}
					else
					{
                        RadListDataItem item = new RadListDataItem(Convert.ToString(iteration_row["DUNIDENF"]).Trim(), Convert.ToString(iteration_row["GUNIDENF"]).Trim());
                        lsbUnidades.Items.Add(item);                        
						ilistau++;
					}
				}
				else
				{
                    RadListDataItem item = new RadListDataItem(Convert.ToString(iteration_row["DUNIDENF"]).Trim(), Convert.ToString(iteration_row["GUNIDENF"]).Trim());
                    lsbUnidades.Items.Add(item);
                    ilistau++;
                }

			}			
			RrUnidades.Close();
			cbMayorMayor.Enabled = true;
		}

		private void proActivarAceptar()
		{
			if (lsbSeleccion.Items.Count > 0 && (rbUnienfe.IsChecked || rbEstado.IsChecked))
			{
				cbImprimir.Enabled = true;
				cbPantalla.Enabled = true;
			}
			else
			{
				cbImprimir.Enabled = false;
				cbPantalla.Enabled = false;
			}
		}

		//UPGRADE_NOTE: (7001) The following declaration (cbSalir_Click) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void cbSalir_Click()
		//{
				//this.Close();
		//}

		private void cbMayor_Click(Object eventSender, EventArgs eventArgs)
		{
			proMandarIzquierdaDerechaSeleccionados();
			lsbUnidades_Change();
			lsbSeleccion_Change();
		}

		private void cbMayorMayor_Click(Object eventSender, EventArgs eventArgs)
		{
			proMandarIzquierdaDerechaTodos();
			lsbUnidades_Change();
			lsbSeleccion_Change();
		}

		private void cbMenor_Click(Object eventSender, EventArgs eventArgs)
		{
			proMandarDerechaIzquierdaSeleccionados();
			lsbUnidades_Change();
			lsbSeleccion_Change();
		}

		private void cbMenorMenor_Click(Object eventSender, EventArgs eventArgs)
		{
			proMandarDerechaIzquierdaTodos();
			lsbUnidades_Change();
			lsbSeleccion_Change();
		}

		private void proMandarDerechaIzquierdaSeleccionados()
		{
			if (!HayAlgunoSeleccionado(lsbSeleccion))
			{
				return;
			}
			for (int ilista = 0; ilista <= lsbSeleccion.Items.Count - 1; ilista++)
			{								
				if (lsbSeleccion.Items[ilista].Selected)
				{                    
                    lsbUnidades.Items.Add(lsbSeleccion.Items[ilista]);
				}				
			}			
		}

		private void proMandarIzquierdaDerechaSeleccionados()
		{
			if (!HayAlgunoSeleccionado(lsbUnidades))
			{
				return;
			}
			for (int ilista = 0; ilista <= lsbUnidades.Items.Count - 1; ilista++)
			{				
				if (lsbUnidades.Items[ilista].Selected)
				{                    
                    lsbSeleccion.Items.Add(lsbUnidades.Items[ilista]);
                }				
			}			
		}

		private void proMandarIzquierdaDerechaTodos()
		{
            RadListDataItem item;
            for (int ilista = 0; ilista <= lsbUnidades.Items.Count - 1; ilista++)
            {                
                item = new RadListDataItem();
                item.Value = lsbUnidades.Items[ilista].Value;
                item.Text = lsbUnidades.Items[ilista].Text;
                lsbSeleccion.Items.Add(item);                 
            }
            lsbUnidades.Items.Clear();
        }

		private void proMandarDerechaIzquierdaTodos()
		{
            RadListDataItem item;
            for (int ilista = 0; ilista <= lsbSeleccion.Items.Count - 1; ilista++)
            {                
                item = new RadListDataItem();
                item.Value = lsbSeleccion.Items[ilista].Value;
                item.Text = lsbSeleccion.Items[ilista].Text;
                lsbUnidades.Items.Add(item);
            }
            lsbSeleccion.Items.Clear();
        }

		private void lsbSeleccion_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			cbMenor_Click(cbMenor, new EventArgs());
		}

        private void lsbSeleccion_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            lsbSeleccion_Change();
        }

        private void lsbSeleccion_Change()
        {
			if (lsbSeleccion.Items.Count > 0)
			{
				cbMenorMenor.Enabled = true;
				cbMenor.Enabled = HayAlgunoSeleccionado(lsbSeleccion);
			}
			else
			{
				cbMenorMenor.Enabled = false;
				cbMenor.Enabled = false;
			}
			proActivarAceptar();

		}

        private void lsbUnidades_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            lsbUnidades_Change();
        }

        private void lsbUnidades_Change()
        {
			if (lsbUnidades.Items.Count > 0)
			{
				cbMayorMayor.Enabled = true;
				cbMayor.Enabled = HayAlgunoSeleccionado(lsbUnidades);
			}
			else
			{
				cbMayorMayor.Enabled = false;
				cbMayor.Enabled = false;
			}
			proActivarAceptar();
		}

		private void lsbUnidades_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			cbMayor_Click(cbMayor, new EventArgs());
		}

		private bool HayAlgunoSeleccionado(RadListControl lista)
		{
			for (int ilista = 0; ilista <= lista.Items.Count - 1; ilista++)
			{				
				if (lista.Items[ilista].Selected)                					
					return true;				
			}
			return false;
		}

		//----------- FIN OSCAR C 25/10/2004--------------------------------

		private void RellenaTablaTemporal()
		{
			DataSet rr2 = null;
			DAODatabaseHelper Dbt1 = null;

			System.DateTime fechasistema = DateTime.Today;
			stSeleccion = "";
			stUnidad = stUnidad.ToUpper();
			StringBuilder stWhereDUNIENFEVA = new StringBuilder();
			string stsql = "";

			if (lsbSeleccion.Items.Count != 0)
			{ // si no es cero recorrer el de disponibles
				stWhereDUNIENFEVA = new StringBuilder(" and (");
				for (int ilista = 0; ilista <= lsbSeleccion.Items.Count - 1; ilista++)
				{
					if (ilista == lsbSeleccion.Items.Count - 1)
					{ // si es el ULTIMO						
						stWhereDUNIENFEVA.Append("DUNIENFEVA.GUNIDENF='" + Convert.ToString(lsbSeleccion.Items[ilista].Value) + "')");						
					}
					else
					{						
						stWhereDUNIENFEVA.Append("DUNIENFEVA.GUNIDENF='" + Convert.ToString(lsbSeleccion.Items[ilista].Value) + "' or ");						
					}
				}
			}

			stsql = " SELECT " + " DUNIENFEVA.dunidenf, " + " DCAMASBOVA.gplantas, DCAMASBOVA.ghabitac, DCAMASBOVA.gcamasbo, " + " DTIPOCAMVA.dtipcama, DCAMASBOVA.iestcama," + " DCAMASBOVA.irestsex,  DCAMASBOVA.ivirtual, " + " DMOTINHAVA.dinhabil," + " AEPISADM.fprevalt " + " FROM " + " DCAMASBOVA " + " INNER JOIN DUNIENFEVA on DUNIENFEVA.gunidenf = DCAMASBOVA.gcounien " + " LEFT JOIN DTIPOCAMVA on DTIPOCAMVA.gtipocam =  DCAMASBOVA.gtipocam " + " LEFT JOIN DMOTINHAVA on DMOTINHAVA.gmotinha =  DCAMASBOVA.gmotinha " + " LEFT JOIN AEPISADM on AEPISADM.gidenpac = DCAMASBOVA.gidenpac " + " WHERE " + " DUNIENFEVA.itiposer = '" + stUnidad + "' AND " + " AEPISADM.faltplan is null ";


			stsql = stsql + stWhereDUNIENFEVA.ToString();

			if (rbUnienfe.IsChecked)
			{
				stSeleccion = "Unidad de Enfermerķa";
				stsql = stsql + " Order By " + " DUNIENFEVA.dunidenf ASC, " + " DCAMASBOVA.gplantas ASC, " + " DCAMASBOVA.ghabitac ASC, " + " DCAMASBOVA.gcamasbo ASC ";
			}
			else
			{
				stSeleccion = "Estado";
				stsql = stsql + " Order By " + " DCAMASBOVA.iestcama ASC, " + " DCAMASBOVA.gplantas ASC, " + " DCAMASBOVA.ghabitac ASC, " + " DCAMASBOVA.gcamasbo ASC ";
			}


			//SI NO EXISTE LA BASE DE DATOS CREARLA
			WorkspaceHelper Wk1 = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0];
			if (FileSystem.Dir(StBaseTemporal, FileAttribute.Normal) == "")
			{
                //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES
                //Dbt1 = Wk1.CreateDatabase(StBaseTemporal, DAO.LanguageConstants.dbLangSpanish, null);
			}
			else
			{
				//SI EXISTE LA TABLA BORRAR
				//UPGRADE_WARNING: (2065) DAO.Workspace method Wk1.OpenDatabase has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
				Dbt1 = Wk1.OpenDatabase("<Connection-String>");
				foreach (TableDefHelper dTabla in Dbt1.TableDefs)
				{
					if (dTabla.Name == "LISTADOGENERALCAMAS")
					{
						DbCommand TempCommand = Dbt1.Connection.CreateCommand();
						TempCommand.CommandText = "drop table LISTADOGENERALCAMAS";
						TempCommand.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(Dbt1.Connection);
						TempCommand.ExecuteNonQuery();
						break;
					}
				}
			}
			//crear tabla
			DbCommand TempCommand_2 = Dbt1.Connection.CreateCommand();
			TempCommand_2.CommandText = "CREATE TABLE LISTADOGENERALCAMAS (UNIDADENF TEXT, CAMA TEXT, TIPO TEXT, RES CHAR(1)," + 
			                            "    ESTADO TEXT, RESSEXO CHAR(2), GENEST CHAR(2)," + 
			                            "    VIRTUAL CHAR(2), INHABILITADA CHAR(2), MOTIVOINHA TEXT," + 
			                            "    FDISPONIBLE TEXT);";
			TempCommand_2.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(Dbt1.Connection);
			TempCommand_2.ExecuteNonQuery();

			DAORecordSetHelper rtemp = Dbt1.OpenRecordset("select * from LISTADOGENERALCAMAS where 2=1");
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, rcAdmision);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			foreach (DataRow iteration_row in rr.Tables[0].Rows)
			{
				rtemp.AddNew();
				rtemp["UNIDADENF"] = iteration_row["dunidenf"];
				rtemp["CAMA"] = StringsHelper.Format(iteration_row["gplantas"], "00") + StringsHelper.Format(iteration_row["ghabitac"], "00") + Convert.ToString(iteration_row["gcamasbo"]);
				rtemp["TIPO"] = Convert.ToString(iteration_row["dtipcama"]) + "";
				switch(Convert.ToString(iteration_row["iestcama"]))
				{
					case "L" :  
						rtemp["ESTADO"] = "LIBRE"; 
						stsql = " SELECT COUNT(*) FROM ARESINGR WHERE " + " gplantas=" + Convert.ToString(iteration_row["gplantas"]) + " AND " + " ghabitac=" + Convert.ToString(iteration_row["ghabitac"]) + " AND " + " gcamasbo='" + Convert.ToString(iteration_row["gcamasbo"]) + "' AND " + " ganoadme IS NULL AND fpreving >=" + Serrores.FormatFecha(fechasistema); 
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql, rcAdmision); 
						rr2 = new DataSet(); 
						tempAdapter_2.Fill(rr2); 
						if (Convert.ToDouble(rr2.Tables[0].Rows[0][0]) > 0)
						{
							rtemp["RES"] = "R";
						} 
						
						rr2.Close(); 
						break;
					case "I" :  
						rtemp["ESTADO"] = "INHABILITADA"; 
						break;
					case "O" :  
						rtemp["ESTADO"] = "OCUPADA"; 
						break;
					case "B" :  
						rtemp["ESTADO"] = "BLOQUEADA"; 
						break;
					default: 
						rtemp["ESTADO"] = ""; 
						break;
				}
				
				if (!Convert.IsDBNull(iteration_row["irestsex"]))
				{
					rtemp["RESSEXO"] = (Convert.ToString(iteration_row["irestsex"]) == "S") ? "SI" : "NO";
				}
				//''        If Not IsNull(rr!igeneest) Then rtemp("GENEST") = IIf(rr!igeneest = "S", "SI", "NO")
				
				if (!Convert.IsDBNull(iteration_row["ivirtual"]))
				{
					rtemp["VIRTUAL"] = (Convert.ToString(iteration_row["ivirtual"]) == "S") ? "SI" : "NO";
				}
				
				if (!Convert.IsDBNull(iteration_row["iestcama"]))
				{
					rtemp["INHABILITADA"] = (Convert.ToString(iteration_row["iestcama"]) == "I") ? "SI" : "NO";
				}
				rtemp["MOTIVOINHA"] = Convert.ToString(iteration_row["dinhabil"]) + "";
				rtemp["FDISPONIBLE"] = Convert.ToDateTime(iteration_row["fprevalt"]).ToString("dd/MM/yyyy HH:mm") + "";

				rtemp.Update();
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rr.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rr.Close();
			rtemp.Close();
			UpgradeHelpers.DB.TransactionManager.DeEnlist(Dbt1.Connection);
			Dbt1.Close();
			Wk1.Close();

		}

        private void List_general_camas_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}