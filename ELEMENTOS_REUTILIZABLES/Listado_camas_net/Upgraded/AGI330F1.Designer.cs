using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Listado_Genereal_Camas
{
	partial class List_general_camas
	{

		#region "Upgrade Support "
		private static List_general_camas m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static List_general_camas DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new List_general_camas();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbMenorMenor", "cbMenor", "cbMayorMayor", "cbMayor", "Cuadro_Diag_imprePrint", "rbEstado", "rbUnienfe", "Frame1", "cbImprimir", "cbCerrar", "cbPantalla", "Label3", "lsbSeleccion", "lsbUnidades", "Label2", "Label1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbMenorMenor;
		public Telerik.WinControls.UI.RadButton cbMenor;
		public Telerik.WinControls.UI.RadButton cbMayorMayor;
		public Telerik.WinControls.UI.RadButton cbMayor;
		public System.Windows.Forms.PrintDialog Cuadro_Diag_imprePrint;
		public Telerik.WinControls.UI.RadRadioButton rbEstado;
		public Telerik.WinControls.UI.RadRadioButton rbUnienfe;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadButton cbPantalla;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadListControl lsbSeleccion;
		public Telerik.WinControls.UI.RadListControl lsbUnidades;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.cbMenorMenor = new Telerik.WinControls.UI.RadButton();
            this.cbMenor = new Telerik.WinControls.UI.RadButton();
            this.cbMayorMayor = new Telerik.WinControls.UI.RadButton();
            this.cbMayor = new Telerik.WinControls.UI.RadButton();
            this.Cuadro_Diag_imprePrint = new System.Windows.Forms.PrintDialog();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.rbEstado = new Telerik.WinControls.UI.RadRadioButton();
            this.rbUnienfe = new Telerik.WinControls.UI.RadRadioButton();
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            this.cbPantalla = new Telerik.WinControls.UI.RadButton();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.lsbSeleccion = new Telerik.WinControls.UI.RadListControl();
            this.lsbUnidades = new Telerik.WinControls.UI.RadListControl();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenorMenor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayorMayor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbUnienfe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbSeleccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbUnidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbMenorMenor
            // 
            this.cbMenorMenor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMenorMenor.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbMenorMenor.Enabled = false;
            this.cbMenorMenor.Location = new System.Drawing.Point(291, 178);
            this.cbMenorMenor.Name = "cbMenorMenor";
            this.cbMenorMenor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMenorMenor.Size = new System.Drawing.Size(50, 33);
            this.cbMenorMenor.TabIndex = 9;
            this.cbMenorMenor.Text = "<<";
            this.cbMenorMenor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbMenorMenor.Click += new System.EventHandler(this.cbMenorMenor_Click);
            // 
            // cbMenor
            // 
            this.cbMenor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMenor.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbMenor.Enabled = false;
            this.cbMenor.Location = new System.Drawing.Point(291, 137);
            this.cbMenor.Name = "cbMenor";
            this.cbMenor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMenor.Size = new System.Drawing.Size(50, 33);
            this.cbMenor.TabIndex = 8;
            this.cbMenor.Text = "<";
            this.cbMenor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbMenor.Click += new System.EventHandler(this.cbMenor_Click);
            // 
            // cbMayorMayor
            // 
            this.cbMayorMayor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMayorMayor.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbMayorMayor.Enabled = false;
            this.cbMayorMayor.Location = new System.Drawing.Point(291, 95);
            this.cbMayorMayor.Name = "cbMayorMayor";
            this.cbMayorMayor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMayorMayor.Size = new System.Drawing.Size(50, 33);
            this.cbMayorMayor.TabIndex = 7;
            this.cbMayorMayor.Text = ">>";
            this.cbMayorMayor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbMayorMayor.Click += new System.EventHandler(this.cbMayorMayor_Click);
            // 
            // cbMayor
            // 
            this.cbMayor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMayor.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbMayor.Enabled = false;
            this.cbMayor.Location = new System.Drawing.Point(291, 54);
            this.cbMayor.Name = "cbMayor";
            this.cbMayor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMayor.Size = new System.Drawing.Size(50, 33);
            this.cbMayor.TabIndex = 6;
            this.cbMayor.Text = ">";
            this.cbMayor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbMayor.Click += new System.EventHandler(this.cbMayor_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.rbEstado);
            this.Frame1.Controls.Add(this.rbUnienfe);
            this.Frame1.HeaderText = "Seleccione ordenación del listado";
            this.Frame1.Location = new System.Drawing.Point(8, 220);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(615, 73);
            this.Frame1.TabIndex = 3;
            this.Frame1.Text = "Seleccione ordenación del listado";
            // 
            // rbEstado
            // 
            this.rbEstado.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbEstado.Location = new System.Drawing.Point(90, 48);
            this.rbEstado.Name = "rbEstado";
            this.rbEstado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbEstado.Size = new System.Drawing.Size(54, 18);
            this.rbEstado.TabIndex = 5;
            this.rbEstado.Text = "Estado";
            this.rbEstado.CheckStateChanged += new System.EventHandler(this.rbEstado_CheckedChanged);
            // 
            // rbUnienfe
            // 
            this.rbUnienfe.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbUnienfe.Location = new System.Drawing.Point(90, 24);
            this.rbUnienfe.Name = "rbUnienfe";
            this.rbUnienfe.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbUnienfe.Size = new System.Drawing.Size(252, 18);
            this.rbUnienfe.TabIndex = 4;
            this.rbUnienfe.Text = "Unidad de enfermería/Planta/habitación/cama";
            this.rbUnienfe.CheckStateChanged += new System.EventHandler(this.rbUnienfe_CheckedChanged);
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbImprimir.Location = new System.Drawing.Point(366, 302);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(81, 29);
            this.cbImprimir.TabIndex = 2;
            this.cbImprimir.Text = "&Impresora";
            this.cbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCerrar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCerrar.Location = new System.Drawing.Point(542, 302);
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(81, 29);
            this.cbCerrar.TabIndex = 1;
            this.cbCerrar.Text = "&Cerrar";
            this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // cbPantalla
            // 
            this.cbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbPantalla.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbPantalla.Location = new System.Drawing.Point(456, 302);
            this.cbPantalla.Name = "cbPantalla";
            this.cbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbPantalla.Size = new System.Drawing.Size(81, 29);
            this.cbPantalla.TabIndex = 0;
            this.cbPantalla.Text = "&Pantalla";
            this.cbPantalla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbPantalla.Click += new System.EventHandler(this.cbPantalla_Click);
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(10, 8);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(191, 18);
            this.Label3.TabIndex = 14;
            this.Label3.Text = "Selección de Unidades de Enfermería";
            // 
            // lsbSeleccion
            // 
            this.lsbSeleccion.Location = new System.Drawing.Point(346, 49);
            this.lsbSeleccion.Name = "lsbSeleccion";
            this.lsbSeleccion.Size = new System.Drawing.Size(275, 161);
            this.lsbSeleccion.TabIndex = 13;
            this.lsbSeleccion.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.lsbSeleccion_SelectedIndexChanged);
            this.lsbSeleccion.DoubleClick += new System.EventHandler(this.lsbSeleccion_DoubleClick);
            // 
            // lsbUnidades
            // 
            this.lsbUnidades.Location = new System.Drawing.Point(6, 49);
            this.lsbUnidades.Name = "lsbUnidades";
            this.lsbUnidades.Size = new System.Drawing.Size(277, 163);
            this.lsbUnidades.TabIndex = 12;
            this.lsbUnidades.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.lsbUnidades_SelectedIndexChanged);
            this.lsbUnidades.DoubleClick += new System.EventHandler(this.lsbUnidades_DoubleClick);
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(346, 31);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(53, 18);
            this.Label2.TabIndex = 11;
            this.Label2.Text = "Selección";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(6, 31);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(105, 18);
            this.Label1.TabIndex = 10;
            this.Label1.Text = "Unidades existentes";
            // 
            // List_general_camas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 336);
            this.Controls.Add(this.cbMenorMenor);
            this.Controls.Add(this.cbMenor);
            this.Controls.Add(this.cbMayorMayor);
            this.Controls.Add(this.cbMayor);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbImprimir);
            this.Controls.Add(this.cbCerrar);
            this.Controls.Add(this.cbPantalla);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.lsbSeleccion);
            this.Controls.Add(this.lsbUnidades);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "List_general_camas";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Listado general de camas - AGI330F1";
            this.Activated += new System.EventHandler(this.List_general_camas_Activated);
            this.Closed += new System.EventHandler(this.List_general_camas_Closed);
            this.Load += new System.EventHandler(this.List_general_camas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbMenorMenor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayorMayor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbUnienfe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbSeleccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbUnidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}