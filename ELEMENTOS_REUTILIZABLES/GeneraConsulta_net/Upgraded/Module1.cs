using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace GeneraConsulta
{
	internal static class Module1
	{

		public static SqlConnection cnConexion = null;
		public static bool bHacerTransac = false;

		public static string stfiliacion = String.Empty;
		static int lSS = 0;
		static int lPrivado = 0;
        
		private static void GrabarProtocolo(string Prestacion, ref string FechaPeticion, string PGidenpac, int Ano, int num, string tipo, System.DateTime fmecaniz)
		{
			string sql = String.Empty;
			DataSet danal = null;
			DataSet danal2 = null;
			string fechainicio = String.Empty;
			DataSet cursor = null;
			string fechainicio2 = String.Empty;
			string stSinComillas = String.Empty;

			try
			{				
				object tempRefParam = FechaPeticion;
				object tempRefParam2 = FechaPeticion;
				sql = "select * from dcodpres where gprestac='" + Prestacion + "' and finivali<=" + Serrores.FormatFecha(tempRefParam) + " and " + " (ffinvali>=" + Serrores.FormatFecha(tempRefParam2) + " or ffinvali is null)";
				FechaPeticion = Convert.ToString(tempRefParam2);
				FechaPeticion = Convert.ToString(tempRefParam);
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, cnConexion);
				danal = new DataSet();
				tempAdapter.Fill(danal);
				
				fechainicio = Convert.ToString(danal.Tables[0].Rows[0]["finivali"]);
				
				danal.Close();
				
				object tempRefParam3 = FechaPeticion;
				sql = "select * from danalext where gidenpac='" + PGidenpac + "' and gprestac='" + Prestacion + "' and " + "fpeticio=" + Serrores.FormatFechaHMS(tempRefParam3) + "  and isnull(iestfact,'')<>'F' ";
				FechaPeticion = Convert.ToString(tempRefParam3);
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, cnConexion);
				danal2 = new DataSet();
				tempAdapter_2.Fill(danal2);

				sql = "select * from danalsol where 1=2";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, cnConexion);
				danal = new DataSet();
				tempAdapter_3.Fill(danal);


				if (danal2.Tables[0].Rows.Count != 0)
				{					
					danal2.MoveFirst();
					foreach (DataRow iteration_row in danal2.Tables[0].Rows)
					{					
						
						object tempRefParam4 = FechaPeticion;
						object tempRefParam5 = FechaPeticion;
						sql = "select * from dcodpres where gprestac='" + Convert.ToString(iteration_row["gpruelab"]) + "' and finivali<=" + Serrores.FormatFecha(tempRefParam4) + " and " + " (ffinvali>=" + Serrores.FormatFecha(tempRefParam5) + " or ffinvali is null)";
						FechaPeticion = Convert.ToString(tempRefParam5);
						FechaPeticion = Convert.ToString(tempRefParam4);
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, cnConexion);
						cursor = new DataSet();
						tempAdapter_4.Fill(cursor);
						fechainicio2 = Convert.ToString(cursor.Tables[0].Rows[0]["finivali"]);
						
						cursor.Close();
						
						danal.AddNew();
						danal.Tables[0].Rows[0]["itiposer"] = "C";
						danal.Tables[0].Rows[0]["ganoregi"] = Ano;
						danal.Tables[0].Rows[0]["gnumregi"] = num;
						danal.Tables[0].Rows[0]["fpeticio"] = fmecaniz;
						danal.Tables[0].Rows[0]["gprestac"] = iteration_row["gprestac"];
						danal.Tables[0].Rows[0]["gpruelab"] = iteration_row["gpruelab"];
						danal.Tables[0].Rows[0]["ncantida"] = iteration_row["ncantida"];
						danal.Tables[0].Rows[0]["finivali"] = DateTime.Parse(fechainicio);
						danal.Tables[0].Rows[0]["finivalp"] = DateTime.Parse(fechainicio2);
						string tempRefParam6 = Convert.ToString(iteration_row["oresulta"]) + "";
						stSinComillas = ReemplazarComillaSimple(ref tempRefParam6);
                       
                        if (stSinComillas == "")
                        {
                            danal.Tables[0].Rows[0]["oresulta"] = DBNull.Value;

                        }
                        else
                        {
                            danal.Tables[0].Rows[0]["oresulta"] = stSinComillas;
                        }
						//danal.Tables[0].Rows[0]["oresulta"] = (stSinComillas == "") ? DBNull.Value : stSinComillas;						
						danal.Tables[0].Rows[0]["nresulta"] = (Convert.IsDBNull(iteration_row["nresulta"])) ? DBNull.Value : iteration_row["nresulta"];						
						danal.Tables[0].Rows[0]["maxresul"] = (Convert.IsDBNull(iteration_row["maxresul"])) ? DBNull.Value : iteration_row["maxresul"];						
						danal.Tables[0].Rows[0]["minresul"] = (Convert.IsDBNull(iteration_row["minresul"])) ? DBNull.Value : iteration_row["minresul"];						
						danal.Tables[0].Rows[0]["uniresul"] = (Convert.IsDBNull(iteration_row["uniresul"])) ? DBNull.Value : iteration_row["uniresul"];
						
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_4);
                        tempAdapter_4.Update(danal, danal.Tables[0].TableName);
					}

				}
				
				danal.Close();				
				danal2.Close();
			}
			catch
			{
			}
		}

		private static string ReemplazarComillaSimple(ref string stEntrada)
		{
			int i = (stEntrada.IndexOf('\'') + 1);

			while (i != 0)
			{
				stEntrada = stEntrada.Substring(0, Math.Min(i - 1, stEntrada.Length)) + "�" + stEntrada.Substring(i);
				i = (stEntrada.IndexOf('\'') + 1);
			}

			return stEntrada;

		}

		internal static bool ABRIRNUEVACONEXION()
		{
			bool result = false;
			string NOMBRE_DSN_SQLSERVER = String.Empty;
			string stServer2 = String.Empty;
			string vstbase = String.Empty;
			string stDriver2 = String.Empty;
			try
			{
				stServer2 = fnQueryRegistro("SERVIDOR");
				stDriver2 = fnQueryRegistro("DRIVER");
				vstbase = fnQueryRegistro("BASEDATOS");
				//**********************************
				//****Path y confirmaci�n de la existencia de
				// ****** conexion al servidor SQL_SERVER 6.5
				result = true;
				int tiDriver = 0;
				string pathstring = String.Empty;
				string tstconexion = String.Empty;
				NOMBRE_DSN_SQLSERVER = "GHOSP";
				tstconexion = "Description=" + "SQL Server en  SERVICURIA" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=" + stServer2 + "" + "\r" + "Database=" + vstbase + "";
				tiDriver = (stDriver2.IndexOf('{') + 1);
				stDriver2 = stDriver2.Substring(tiDriver);
				tiDriver = (stDriver2.IndexOf('}') + 1);
				stDriver2 = stDriver2.Substring(0, Math.Min(tiDriver - 1, stDriver2.Length));
				//camaya todo_x_5
				//UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(NOMBRE_DSN_SQLSERVER, stDriver2, true, tstconexion);


				Conexion.Obtener_conexion Instancia = null;
				//Dim RcConexion As rdoConnection
				bool VstError = false;
				string VstLetra = String.Empty;
				string vstUsuario_NT = String.Empty;
				string GUsuario = String.Empty;
				string GPassword = String.Empty;
				vstUsuario_NT = obtener_usuario();
				VstLetra = "C"; // Caso de urgencias
				Instancia = new Conexion.Obtener_conexion();
				VstError = true;
				string tempRefParam = String.Empty;
				string tempRefParam2 = String.Empty;
				string tempRefParam3 = String.Empty;
				string tempRefParam4 = String.Empty;
				string tempRefParam5 = String.Empty;
				string tempRefParam6 = String.Empty;
				string tempRefParam7 = String.Empty;
				string tempRefParam8 = String.Empty;
				string tempRefParam9 = String.Empty;
				Instancia.Conexion(ref cnConexion, VstLetra, vstUsuario_NT, ref VstError, ref GUsuario, ref GPassword, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, tempRefParam5, tempRefParam6, tempRefParam7, tempRefParam8, tempRefParam9);
				return VstError;
			}
			catch
			{
				return true;
			}
		}
		private static string obtener_usuario()
		{
			string stBuffer = new String(' ', 8);
			int lSize = stBuffer.Length;
			//Call GetUserName(stBuffer, lSize)
			if (lSize > 0)
			{
				return stBuffer.Substring(0, Math.Min(lSize, stBuffer.Length));
			}
			else
			{
				return "";
			}
		}

		internal static string fnQueryRegistro(string stSubKey)
		{
			//'Dim lresult As Long 'todo va bien si es cero
			//'Dim lValueType As Long  'tipo de dato
			//'Dim strBuf As String  'cadena que contiene el valor requerido
			//'Dim lDataBufSize As Long 'tama�o de la cadena en el registro
			//'Dim hKey As Long   'ubicaci�n de la clave en el registro
			//'Dim tstKeyName  As String 'clave donde estan los datos en el registro
			//'tstKeyName = "SOFTWARE\INDRA\GHOSP"
			//'lValueType = REG_SZ
			//'
			//'lresult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tstKeyName, 0, KEY_QUERY_VALUE, hKey)
			//'lresult = RegQueryValueEx(hKey, stSubKey, 0&, lValueType, ByVal 0&, lDataBufSize)
			//'    If lresult = ERROR_SUCCESS Then
			//'        If lValueType = REG_SZ Then
			//'            strBuf = String(lDataBufSize, " ")
			//'            lresult = RegQueryValueEx(hKey, stSubKey, 0&, 0&, ByVal strBuf, lDataBufSize)
			//'            If lresult = ERROR_SUCCESS Then
			//'                 fnQueryRegistro = Mid$(strBuf, 1, lDataBufSize - 1)
			//'            End If
			//'        End If
			//'    End If
			//'    If lresult <> ERROR_SUCCESS Then
			//'        'MsgBox "Pongase en contacto con inform�tica", vbCritical + vbOKOnly, "Error en conexi�n"
			//'        'End
			//'        fnQueryRegistro = ""
			//'    End If
			//'RegCloseKey (hKey)
			//'CURIAE -- EQUIPO DE MIGRACION: NUEVA FORMA DE OBTENR LOS DATOS DE CCONEXION
			return String.Empty;
		}

		internal static bool GrabarConsulta(string stGidenpac, ref string stFpeticio, string stPresPadre, string usuario, ref int CodigoError, ref string DescripcionError)
		{
			bool etiqueta2 = false;
			bool etiqueta = false;
			bool result = false;
			try
			{
				etiqueta = true;
				etiqueta2 = false;
				DataSet DOPENEXT = null;
				DataSet RrCCONSULT = null;
				DataSet RrCHUECCIT = null;
				DataSet RrCCITPROG = null;
				DataSet RrAEPISADM = null;
				DataSet RrUEPISURG = null;
				DataSet RrDCODPRES = null;
				DataSet RrEPRESALT = null;
				DataSet tRr = null;
				string stTemp = String.Empty;
				string stServPrest = String.Empty;
				int stGnumcons = 0;
				int stGanocons = 0;
				bool GraboDanalsol = false;
				DataSet rsPeticiones = null;
				string stIprimsuc = String.Empty;
				System.DateTime FechaHospitalizacion = DateTime.FromOADate(0);
				string stGnumadme = String.Empty;
				string stGanoadme = String.Empty;
				string sqlServicio = String.Empty;
				string stGprestac = String.Empty;
				DataSet RrServicio = null;
				int i = 0;
				string TipodeRelacion = String.Empty;
				string DondeestaHospitalizado = String.Empty;
                //camaya todo_x_5
				//FacturacionConsultas.clsFacturacion ClaseFacturacion = null;
				int lNumeroRegistro = 0;
				bool Hospitalizado = false;
				string PrestacionPrincipal = String.Empty;
				DialogResult respuesta = (DialogResult) 0;
				bool HospitalizadoYdadodeAlta = false;
				string NomTablaPrestaciones = String.Empty;
				string fechaPerfact = String.Empty;
				int SersoliPrestacion = 0;
				System.DateTime FechaLLegadaaHospita = DateTime.FromOADate(0);
				DataSet CursorDanalsol = null;
				System.DateTime fechaanal = DateTime.FromOADate(0);
				object ResultadoPrueba = null;
				object MotivoSolicitud = null;

				int VstAnoEpisodio = 0;
				int VstNumEpisodio = 0;
				string VstServicio = String.Empty;
				string VItiposer = String.Empty;
				string VFechaRea = String.Empty;

				int SerHijaOpenLab = 0;
				int SerPadreOpenLab = 0;

				string stSql = String.Empty;
				string sql = String.Empty;
				DataSet cursor = null;
				string stActuaFechRealiza = String.Empty;
				string sqltempo = String.Empty;

				sqlServicio = "select * from sconsglo where gconsglo='CEXRELHO'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlServicio, cnConexion);
				RrCCONSULT = new DataSet();
				tempAdapter.Fill(RrCCONSULT);
				if (RrCCONSULT.Tables[0].Rows.Count != 0)
				{			
					TipodeRelacion = Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["VALFANU1"]).Trim();
				}
			
				RrCCONSULT.Close();


				sqltempo = "select nnumeri1 from sconsglo where gconsglo = 'SEGURSOC'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqltempo, cnConexion);
				cursor = new DataSet();
				tempAdapter_2.Fill(cursor);				
				lSS = Convert.ToInt32(cursor.Tables[0].Rows[0]["nnumeri1"]);


				sqltempo = "select nnumeri1 from sconsglo where gconsglo = 'PRIVADO'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqltempo, cnConexion);
				cursor = new DataSet();
				tempAdapter_3.Fill(cursor);
				
				lPrivado = Convert.ToInt32(cursor.Tables[0].Rows[0]["nnumeri1"]);



				HospitalizadoYdadodeAlta = false;
				Hospitalizado = false;
				fechaPerfact = "";
                				
				object tempRefParam = stFpeticio;
				object tempRefParam2 = stFpeticio;
				stSql = " SELECT faltplan, ganoadme, gnumadme, gserulti, gperulti, fllegada " + " FROM AEPISADM " + " WHERE " + " AEPISADM.gidenpac='" + stGidenpac + "' AND " + " (AEPISADM.faltplan IS NULL or AEPISADM.faltplan>=" + Serrores.FormatFechaHMS(tempRefParam) + ") and AEPISADM.fllegada<=" + Serrores.FormatFechaHMS(tempRefParam2);
				stFpeticio = Convert.ToString(tempRefParam2);
				stFpeticio = Convert.ToString(tempRefParam);

				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, cnConexion);
				RrAEPISADM = new DataSet();
				tempAdapter_4.Fill(RrAEPISADM);

				// Vemos si el paciente esta hospitalizado, busco en Admision *********
				if (RrAEPISADM.Tables[0].Rows.Count != 0)
				{
					DondeestaHospitalizado = "Admision";
					if (TipodeRelacion == "S")
					{					
						if (!Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["faltplan"]))
						{
							HospitalizadoYdadodeAlta = true;
					
							fechaPerfact = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["FALTPLAN"]);
						}
					
						FechaLLegadaaHospita = Convert.ToDateTime(RrAEPISADM.Tables[0].Rows[0]["fllegada"]);
						Hospitalizado = true;
					}
					else
					{
						if (TipodeRelacion == "P")
						{
							
							FechaLLegadaaHospita = Convert.ToDateTime(RrAEPISADM.Tables[0].Rows[0]["fllegada"]);
							Hospitalizado = true;							
							
							if (!Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["faltplan"]))
							{
								HospitalizadoYdadodeAlta = true;								
								fechaPerfact = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["FALTPLAN"]);
							}
						}
					}
				}
				else
				{

					// Vemos si el paciente esta hospitalizado, busco en Urgencias *********					
					object tempRefParam3 = stFpeticio;
					object tempRefParam4 = stFpeticio;
					stSql = " SELECT faltaurg, ganourge, gnumurge, gservici, gpersona, fllegada " + " FROM UEPISURG " + " WHERE " + " UEPISURG.gidenpac='" + stGidenpac + "' AND " + " (UEPISURG.faltaurg IS NULL or UEPISURG.faltaurg>=" + Serrores.FormatFechaHMS(tempRefParam3) + ") and UEPISURG.fllegada<=" + Serrores.FormatFechaHMS(tempRefParam4);
					stFpeticio = Convert.ToString(tempRefParam4);
					stFpeticio = Convert.ToString(tempRefParam3);
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSql, cnConexion);
					RrUEPISURG = new DataSet();
					tempAdapter_5.Fill(RrUEPISURG);
					if (RrUEPISURG.Tables[0].Rows.Count != 0)
					{
						DondeestaHospitalizado = "Urgencias";
						if (TipodeRelacion == "S")
						{							
							FechaLLegadaaHospita = Convert.ToDateTime(RrUEPISURG.Tables[0].Rows[0]["fllegada"]);
							Hospitalizado = true;					
						
							if (!Convert.IsDBNull(RrUEPISURG.Tables[0].Rows[0]["faltaurg"]))
							{
								HospitalizadoYdadodeAlta = true;							
								fechaPerfact = Convert.ToString(RrUEPISURG.Tables[0].Rows[0]["faltaurg"]);
							}
						}
						else
						{
							if (TipodeRelacion == "P")
							{								
								FechaLLegadaaHospita = Convert.ToDateTime(RrUEPISURG.Tables[0].Rows[0]["fllegada"]);
								Hospitalizado = true;
																
								if (!Convert.IsDBNull(RrUEPISURG.Tables[0].Rows[0]["faltaurg"]))
								{
									HospitalizadoYdadodeAlta = true;
									
									fechaPerfact = Convert.ToString(RrUEPISURG.Tables[0].Rows[0]["faltaurg"]);
								}
							}
						}
					}
				}


				stGprestac = stPresPadre;

				//If UCase(vOpenLab) = UCase(stGprestac) Then
                
				stSql = "select * from dcodpres where gprestac='" + stPresPadre + " '";
				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSql, cnConexion);
				cursor = new DataSet();
				tempAdapter_6.Fill(cursor);
				if (cursor.Tables[0].Rows.Count != 0)
				{				
					SerPadreOpenLab = Convert.ToInt32(cursor.Tables[0].Rows[0]["gservici"]);
				}
				
				cursor.Close();

				//si las pruebas hijas son de distinto servicio que la padre se cambia en
				//danalext y dopenext la prestacion por la que aparezca en DOPENSER
				SerHijaOpenLab = SerPadreOpenLab;
				
				object tempRefParam5 = stFpeticio;
				stSql = "select * from danalext,dcodpres where danalext.fpeticio=" + Serrores.FormatFechaHMS(tempRefParam5) + " and dcodpres.gprestac=danalext.gpruelab and danalext.gidenpac='" + stGidenpac + "' " + " and danalext.gprestac='" + stPresPadre + "' and dcodpres.gservici <> " + SerPadreOpenLab.ToString();
				stFpeticio = Convert.ToString(tempRefParam5);
				SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSql, cnConexion);
				cursor = new DataSet();
				tempAdapter_7.Fill(cursor);
				if (cursor.Tables[0].Rows.Count != 0)
				{					
					SerHijaOpenLab = Convert.ToInt32(cursor.Tables[0].Rows[0]["gservici"]);
				}
				
				cursor.Close();
				if (SerPadreOpenLab != SerHijaOpenLab)
				{
					//buscar el servicio de la prueba hija modificaci�n a raiz de nuevos servicios de
					//laboratorio, se ha creado la tabla dopenser que relaciona prestaciones ficticias
					//con servicios de laboratorio.
					stSql = "select * from dopenser where gservici=" + SerHijaOpenLab.ToString();
					SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(stSql, cnConexion);
					cursor = new DataSet();
					tempAdapter_8.Fill(cursor);
					if (cursor.Tables[0].Rows.Count != 0)
					{						
						if ((Convert.ToString(cursor.Tables[0].Rows[0]["gprestac"]) + "").Trim() != "")
						{							
							stGprestac = Convert.ToString(cursor.Tables[0].Rows[0]["gprestac"]);
							
							object tempRefParam6 = stFpeticio;
							stSql = "update dopenext set dopenext.gprestac='" + stGprestac + "' where " + 
							        " dopenext.fpeticio=" + Serrores.FormatFechaHMS(tempRefParam6) + " and " + 
							        " dopenext.gidenpac='" + stGidenpac + "' and " + 
							        " dopenext.gprestac='" + stPresPadre + "'";
							stFpeticio = Convert.ToString(tempRefParam6);
							SqlCommand tempCommand = new SqlCommand(stSql, cnConexion);
							tempCommand.ExecuteNonQuery();
							
							object tempRefParam7 = stFpeticio;
							stSql = "update danalext set gprestac='" + stGprestac + "' where " + 
							        " fpeticio=" + Serrores.FormatFechaHMS(tempRefParam7) + " and " + 
							        " gidenpac='" + stGidenpac + "' and " + 
							        " gprestac='" + stPresPadre + "'";
							stFpeticio = Convert.ToString(tempRefParam7);
							SqlCommand tempCommand_2 = new SqlCommand(stSql, cnConexion);
							tempCommand_2.ExecuteNonQuery();
						}
					}
					
					cursor.Close();
				}
				//'DOPENEXT.Close
				//''        stSql = "select dopenext.*,dservici.*,dcodpres.*,dpacient.NAFILIAC,dpacient.gsocieda socpac from dopenext,dservici,dcodpres,dpacient where " & _
				//'''        " dopenext.fpeticio=" & FormatFechaHMS(stFpeticio) & " and " & _
				//'''        " dopenext.gidenpac='" & stGidenpac & "' and " & _
				//'''        " dopenext.gidenpac=dpacient.gidenpac and " & _
				//'''        " dcodpres.gservici=dservici.gservici and " & _
				//'''        " dopenext.gprestac=dcodpres.gprestac "
				//''
				//''        Set DOPENEXT = cnConexion.OpenResultset(stSql, 1, 2)
				//''        DOPENEXT.MoveFirst
				//''        stGprestac = DOPENEXT("GPRESTAC")
				//End If


				etiqueta2 = true;
				etiqueta = false;
				// leemos de dopenext
				
				object tempRefParam8 = stFpeticio;
				stSql = "select dopenext.*,dservici.*,dcodpres.*,dpacient.NAFILIAC,dpacient.gsocieda socpac from dopenext,dservici,dcodpres,dpacient where " + 
				        " dopenext.fpeticio=" + Serrores.FormatFechaHMS(tempRefParam8) + " and " + 
				        " dopenext.gidenpac='" + stGidenpac + "' and dopenext.gprestac = '" + stGprestac + "' and  " + 
				        " dopenext.gidenpac=dpacient.gidenpac and " + 
				        " dcodpres.gservici=dservici.gservici and " + 
				        " dopenext.gprestac=dcodpres.gprestac ";
				stFpeticio = Convert.ToString(tempRefParam8);

				SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql, cnConexion);
				DOPENEXT = new DataSet();
				tempAdapter_9.Fill(DOPENEXT);
				
				DOPENEXT.MoveFirst();
				if (!RegistroCorrecto(DOPENEXT, ref CodigoError, ref DescripcionError))
				{					
					DOPENEXT.Close();
					return true;
				}
				if (bHacerTransac)
				{						
                    cnConexion.BeginTransScope();                    
				}

				// INSERTAMOS UN REGISTRO EN LA TABLA CCONSULT ****************************
				stSql = " SELECT * " + " FROM CCONSULT " + " WHERE 2=1";

				SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(stSql, cnConexion);
				RrCCONSULT = new DataSet();
				tempAdapter_10.Fill(RrCCONSULT);			
				RrCCONSULT.AddNew();
				// Primero relleno los campos de la tabla NO NULOS			
				RrCCONSULT.Tables[0].Rows[0]["ganoregi"] = DateTime.Today.Year;			
				stGanocons = Convert.ToInt32(RrCCONSULT.Tables[0].Rows[0]["ganoregi"]);
				// Asignamos un numero de consulta llamando a la DLL de NUMERACION
				Numeracion.SIGUIENTE Numero1 = null;
				Numero1 = new Numeracion.SIGUIENTE();
				string tempRefParam9 = "NROCONS";
				lNumeroRegistro = Numero1.SIG_NUMERO(tempRefParam9,cnConexion);
				
				RrCCONSULT.Tables[0].Rows[0]["gnumregi"] = lNumeroRegistro;				
				stGnumcons = Convert.ToInt32(RrCCONSULT.Tables[0].Rows[0]["gnumregi"]);				
				RrCCONSULT.Tables[0].Rows[0]["gidenpac"] = stGidenpac;					
				RrCCONSULT.Tables[0].Rows[0]["ncitapac"] = DBNull.Value;
				stSql = "SELECT iprimsuc,gservici,gprestac, dprestac " + " FROM DCODPRES " + " WHERE " + " gprestac='" + stGprestac + "'"; // and finivali<=" & FormatFechaHMS(stFpeticio) & " and (ffinvali>=" & FormatFechaHMS(stFpeticio) & " or ffinvali is null)"
				SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(stSql, cnConexion);
				RrDCODPRES = new DataSet();
				tempAdapter_11.Fill(RrDCODPRES);

				if (RrDCODPRES.Tables[0].Rows.Count != 0)
				{					
					PrestacionPrincipal = Convert.ToString(RrDCODPRES.Tables[0].Rows[0]["gprestac"]);					
					RrCCONSULT.Tables[0].Rows[0]["gprestac"] = RrDCODPRES.Tables[0].Rows[0]["gprestac"];					
					
					if (!Convert.IsDBNull(RrDCODPRES.Tables[0].Rows[0]["iprimsuc"]))
					{						
						stIprimsuc = Convert.ToString(RrDCODPRES.Tables[0].Rows[0]["iprimsuc"]);
					}
					else
					{
						stIprimsuc = "P";
					}
					
					stServPrest = Convert.ToString(RrDCODPRES.Tables[0].Rows[0]["gservici"]);
				}

				
				RrDCODPRES.Close();
				//Si procede de una cita......
								
				if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["ganoprog"]))
				{					
					RrCCONSULT.Tables[0].Rows[0]["ffeccita"] = DOPENEXT.Tables[0].Rows[0]["fpeticio"];					
					RrCCONSULT.Tables[0].Rows[0]["fmecaniz"] = DOPENEXT.Tables[0].Rows[0]["frealiza"];
				}
				else
				{
					//Si procede de una consulta ambulante
					stActuaFechRealiza = "";
					sql = "select * from sconsglo where gconsglo='FECONAMB'";
					SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(sql, cnConexion);
					cursor = new DataSet();
					tempAdapter_12.Fill(cursor);
					if (cursor.Tables[0].Rows.Count != 0)
					{						
						stActuaFechRealiza = Convert.ToString(cursor.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() + "";
					}
					
					cursor.Close();
					if (stActuaFechRealiza == "R")
					{						
						RrCCONSULT.Tables[0].Rows[0]["ffeccita"] = DOPENEXT.Tables[0].Rows[0]["frealiza"];
					}
					else
					{						
						RrCCONSULT.Tables[0].Rows[0]["ffeccita"] = DOPENEXT.Tables[0].Rows[0]["fpeticio"];
					}
					
					RrCCONSULT.Tables[0].Rows[0]["fprhueco"] = RrCCONSULT.Tables[0].Rows[0]["ffeccita"];					
					RrCCONSULT.Tables[0].Rows[0]["fmecaniz"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
				}
				
				if (Convert.ToString(DOPENEXT.Tables[0].Rows[0]["IPRIORID"]) == "N")
				{					
					RrCCONSULT.Tables[0].Rows[0]["itipopac"] = "N";
				}
				else
				{					
					RrCCONSULT.Tables[0].Rows[0]["itipopac"] = "P";
				}
				
				RrCCONSULT.Tables[0].Rows[0]["itipcita"] = "S";				
				RrCCONSULT.Tables[0].Rows[0]["fsolicit"] = DOPENEXT.Tables[0].Rows[0]["FPETICIO"];
				RrCCONSULT.Tables[0].Rows[0]["fpropmed"] = DOPENEXT.Tables[0].Rows[0]["FPETICIO"];
                								
				if (Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["GSOCIEDA"]))
				{				
					RrCCONSULT.Tables[0].Rows[0]["gsocieda"] = DOPENEXT.Tables[0].Rows[0]["socpac"];
									
					if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["NAFILIAC"]))
					{						
						RrCCONSULT.Tables[0].Rows[0]["nafiliac"] = DOPENEXT.Tables[0].Rows[0]["NAFILIAC"];
					}
				}
				else
				{					
					RrCCONSULT.Tables[0].Rows[0]["gsocieda"] = DOPENEXT.Tables[0].Rows[0]["GSOCIEDA"];
					if (stfiliacion != "")
					{					
						RrCCONSULT.Tables[0].Rows[0]["nafiliac"] = stfiliacion;
					}
				}
				
				RrCCONSULT.Tables[0].Rows[0]["gusuario"] = usuario;				
				RrCCONSULT.Tables[0].Rows[0]["ipresent"] = "S";

				if (Hospitalizado)
				{
					if (DondeestaHospitalizado == "Admision")
					{
						
						RrCCONSULT.Tables[0].Rows[0]["ihoscons"] = "H";
						
						RrCCONSULT.Tables[0].Rows[0]["itiposer"] = "H";
						
						stGanoadme = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["ganoadme"]);
						
						stGnumadme = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gnumadme"]);
						
						RrCCONSULT.Tables[0].Rows[0]["ganoprin"] = Conversion.Val(stGanoadme);
						
						RrCCONSULT.Tables[0].Rows[0]["gnumprin"] = Conversion.Val(stGnumadme);						
						
						RrCCONSULT.Tables[0].Rows[0]["fultrevi"] = DBNull.Value;
					}
					else
					{
						
						RrCCONSULT.Tables[0].Rows[0]["ihoscons"] = "H";						
						RrCCONSULT.Tables[0].Rows[0]["itiposer"] = "U";						
						stGanoadme = Convert.ToString(RrUEPISURG.Tables[0].Rows[0]["ganourge"]);						
						stGnumadme = Convert.ToString(RrUEPISURG.Tables[0].Rows[0]["gnumurge"]);						
						RrCCONSULT.Tables[0].Rows[0]["ganoprin"] = Conversion.Val(stGanoadme);						
						RrCCONSULT.Tables[0].Rows[0]["gnumprin"] = Conversion.Val(stGnumadme);												
						RrCCONSULT.Tables[0].Rows[0]["fultrevi"] = DBNull.Value;
					}
				}
				else
				{				
					RrCCONSULT.Tables[0].Rows[0]["ihoscons"] = "C";				
					RrCCONSULT.Tables[0].Rows[0]["itiposer"] = "C";					
					RrCCONSULT.Tables[0].Rows[0]["ganoprin"] = DBNull.Value;				
					RrCCONSULT.Tables[0].Rows[0]["gnumprin"] = DBNull.Value;					
					RrCCONSULT.Tables[0].Rows[0]["fultrevi"] = DBNull.Value;
				}

				//     If Not IsNull(DOPENEXT("GINSPECC")) Then
				//        RrCCONSULT("ginspecc") = DOPENEXT("GINSPECC")
				//     Else
				//        If lSS = RrCCONSULT("gsocieda") Then
				//           sql = "select * from dentipac where gidenpac='" & stGidenpac & "' and gsocieda=" & lSS
				//           Set cursor = cnConexion.OpenResultset(sql, 1, 1)
				//              If Not IsNull(cursor("ginspecc")) Then
				//                 RrCCONSULT("ginspecc") = cursor("GINSPECC")
				//              End If
				//           cursor.Close
				//        End If
				//     End If
				//S�lo se graba la inspecci�n, si la sociedad es de SS
				
				if (lSS == Convert.ToDouble(RrCCONSULT.Tables[0].Rows[0]["gsocieda"]))
				{			
					
					if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["GINSPECC"]))
					{						
						RrCCONSULT.Tables[0].Rows[0]["ginspecc"] = DOPENEXT.Tables[0].Rows[0]["GINSPECC"];
					}
					else
					{
						sql = "select * from dentipac where gidenpac='" + stGidenpac + "' and gsocieda=" + lSS.ToString();
						SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(sql, cnConexion);
						cursor = new DataSet();
						tempAdapter_13.Fill(cursor);
										
						if (!Convert.IsDBNull(cursor.Tables[0].Rows[0]["ginspecc"]))
						{
							
							RrCCONSULT.Tables[0].Rows[0]["ginspecc"] = cursor.Tables[0].Rows[0]["GINSPECC"];
						}
						
						cursor.Close();
					}
				}				
				
				RrCCONSULT.Tables[0].Rows[0]["gdiagnos"] = DBNull.Value;				
				RrCCONSULT.Tables[0].Rows[0]["ddiagnos"] = DBNull.Value;				
				
				MotivoSolicitud = DOPENEXT.Tables[0].Rows[0]["OMOTISOL"];
				
				if (!Convert.IsDBNull(MotivoSolicitud))
				{				
					RrCCONSULT.Tables[0].Rows[0]["omotaten"] = MotivoSolicitud;
				}
				else
				{					
					RrCCONSULT.Tables[0].Rows[0]["omotaten"] = DBNull.Value;
				}				
				
				RrCCONSULT.Tables[0].Rows[0]["dnombmed"] = DBNull.Value;	
				
				RrCCONSULT.Tables[0].Rows[0]["nordcita"] = DBNull.Value;				
				
				if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["GSERSOLI"]))
				{				
					RrCCONSULT.Tables[0].Rows[0]["gsersoli"] = DOPENEXT.Tables[0].Rows[0]["GSERSOLI"];
				}
				else
				{				
					RrCCONSULT.Tables[0].Rows[0]["gsersoli"] = DBNull.Value;
				}				
				
				if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["GPERSOLI"]))
				{				
					RrCCONSULT.Tables[0].Rows[0]["gmedsoli"] = DOPENEXT.Tables[0].Rows[0]["GPERSOLI"];
				}
				else
				{					
					RrCCONSULT.Tables[0].Rows[0]["gmedsoli"] = DBNull.Value;
				}				
				
				RrCCONSULT.Tables[0].Rows[0]["npriorid"] = DBNull.Value;
				
				
				if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["ghospori"]))
				{					
					RrCCONSULT.Tables[0].Rows[0]["ghospori"] = DOPENEXT.Tables[0].Rows[0]["ghospori"];
				}
				else
				{					
					RrCCONSULT.Tables[0].Rows[0]["ghospori"] = DBNull.Value;
				}			
				
				RrCCONSULT.Tables[0].Rows[0]["dmediori"] = DBNull.Value;				
				RrCCONSULT.Tables[0].Rows[0]["ghospita"] = DBNull.Value;				
				RrCCONSULT.Tables[0].Rows[0]["dserdest"] = DBNull.Value;					
				RrCCONSULT.Tables[0].Rows[0]["gmottrac"] = DBNull.Value;				
				RrCCONSULT.Tables[0].Rows[0]["nhorainc"] = DBNull.Value;				
				RrCCONSULT.Tables[0].Rows[0]["ndiasinc"] = DBNull.Value;				
				RrCCONSULT.Tables[0].Rows[0]["gservici"] = Convert.ToString(DOPENEXT.Tables[0].Rows[0]["GSERVICI"]) + "";
                				
				if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["GPERREAL"]))
				{					
					RrCCONSULT.Tables[0].Rows[0]["gpersona"] = DOPENEXT.Tables[0].Rows[0]["GPERREAL"];
				}

				//Si es una petici�n de una cita...

				string stomotexMC = String.Empty;
				
				
				if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["ganoprog"]))
				{					
					stTemp = " SELECT * FROM ccitprog WHERE  ganoregi = " + Convert.ToString(DOPENEXT.Tables[0].Rows[0]["ganoprog"]) + " AND gnumregi =" + Convert.ToString(DOPENEXT.Tables[0].Rows[0]["gnumprog"]);

					SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(stTemp, cnConexion);
					tRr = new DataSet();
					tempAdapter_14.Fill(tRr);

					if (tRr.Tables[0].Rows.Count != 0)
					{
						
						RrCCONSULT.Tables[0].Rows[0]["gsalacon"] = Convert.ToString(tRr.Tables[0].Rows[0]["gsalacon"]).Trim();
						
						RrCCONSULT.Tables[0].Rows[0]["ganoprog"] = tRr.Tables[0].Rows[0]["ganoregi"];
						
						RrCCONSULT.Tables[0].Rows[0]["gnumprog"] = tRr.Tables[0].Rows[0]["gnumregi"];
						
						RrCCONSULT.Tables[0].Rows[0]["gagendas"] = Convert.ToString(tRr.Tables[0].Rows[0]["gagendas"]).Trim();
						
						RrCCONSULT.Tables[0].Rows[0]["gbloques"] = tRr.Tables[0].Rows[0]["gbloques"];
                        						
						RrCCONSULT.Tables[0].Rows[0]["gtipentr"] = tRr.Tables[0].Rows[0]["gtipentr"];
						
						RrCCONSULT.Tables[0].Rows[0]["gtipcita"] = tRr.Tables[0].Rows[0]["gtipcita"];
                        						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["fprhueco"]))
						{						
							RrCCONSULT.Tables[0].Rows[0]["fprhueco"] = tRr.Tables[0].Rows[0]["fprhueco"];
						}
												
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["fpropmed"]))
						{						
							RrCCONSULT.Tables[0].Rows[0]["fpropmed"] = tRr.Tables[0].Rows[0]["fpropmed"];
						}

						//OSCAR C Ocubre 2009
												
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["gtiporig"]))
						{						
							RrCCONSULT.Tables[0].Rows[0]["gtiporig"] = tRr.Tables[0].Rows[0]["gtiporig"];
						}					
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["idpicorigenMUL"]))
						{							
							RrCCONSULT.Tables[0].Rows[0]["idpicorigenMUL"] = tRr.Tables[0].Rows[0]["idpicorigenMUL"];
						}						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["idnumercitaMUL"]))
						{							
							RrCCONSULT.Tables[0].Rows[0]["idnumercitaMUL"] = tRr.Tables[0].Rows[0]["idnumercitaMUL"];
						}						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["codcenoranuMUL"]))
						{							
							RrCCONSULT.Tables[0].Rows[0]["codcenoranuMUL"] = tRr.Tables[0].Rows[0]["codcenoranuMUL"];
						}
												
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["visibilidadMUL"]))
						{						
							RrCCONSULT.Tables[0].Rows[0]["visibilidadMUL"] = tRr.Tables[0].Rows[0]["visibilidadMUL"];
						}					
					
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["ultespMUL"]))
						{					
							RrCCONSULT.Tables[0].Rows[0]["ultespMUL"] = tRr.Tables[0].Rows[0]["ultespMUL"];
						}
						//--------

						//OSCAR C Noviembre 2010
						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["ffecphlc"]))
						{
						
							RrCCONSULT.Tables[0].Rows[0]["ffecphlc"] = tRr.Tables[0].Rows[0]["ffecphlc"];
						}
						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["ffecphls"]))
						{
						
							RrCCONSULT.Tables[0].Rows[0]["ffecphls"] = tRr.Tables[0].Rows[0]["ffecphls"];
						}
						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["ffecphla"]))
						{
						
							RrCCONSULT.Tables[0].Rows[0]["ffecphla"] = tRr.Tables[0].Rows[0]["ffecphla"];
						}
						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["ffecmhlp"]))
						{
						
							RrCCONSULT.Tables[0].Rows[0]["ffecmhlp"] = tRr.Tables[0].Rows[0]["ffecmhlp"];
						}						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["ffecmhlc"]))
						{						
							RrCCONSULT.Tables[0].Rows[0]["ffecmhlc"] = tRr.Tables[0].Rows[0]["ffecmhlc"];
						}					
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["ilibrele"]))
						{						
							RrCCONSULT.Tables[0].Rows[0]["ilibrele"] = tRr.Tables[0].Rows[0]["ilibrele"];
						}

						//O.Frias - 30/05/2013
						//Nuevos valores de informacion clinica MultiCita
					
						stomotexMC = Convert.ToString(tRr.Tables[0].Rows[0]["omotexMC"]) + "";
						if (stomotexMC != "")
						{					
							RrCCONSULT.Tables[0].Rows[0]["omotexMC"] = stomotexMC;
						}
						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["dlaterMC"]))
						{						
							RrCCONSULT.Tables[0].Rows[0]["dlaterMC"] = tRr.Tables[0].Rows[0]["dlaterMC"];
						}
						//                If Not IsNull(tRr("omotexMC")) Then RrCCONSULT("omotexMC") = tRr("omotexMC")
												
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["dproyeMC"]))
						{						
							RrCCONSULT.Tables[0].Rows[0]["dproyeMC"] = tRr.Tables[0].Rows[0]["dproyeMC"];
						}						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["dtpetiMC"]))
						{							
							RrCCONSULT.Tables[0].Rows[0]["dtpetiMC"] = tRr.Tables[0].Rows[0]["dtpetiMC"];
						}

						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["gdiagnos"]))
						{							
							RrCCONSULT.Tables[0].Rows[0]["gdiagnos"] = tRr.Tables[0].Rows[0]["gdiagnos"];
						}
												
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["ddiagnos"]))
						{						
							RrCCONSULT.Tables[0].Rows[0]["ddiagnos"] = tRr.Tables[0].Rows[0]["ddiagnos"];
						}

						//O.Frias - 12/11/2015
						//Nuevo campo de fecha peticion origen												
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["fpetiori"]))
						{							
							RrCCONSULT.Tables[0].Rows[0]["fpetiori"] = tRr.Tables[0].Rows[0]["fpetiori"];
						}

						//--------
					}
				}
				else
				{					
					RrCCONSULT.Tables[0].Rows[0]["gsalacon"] = DBNull.Value;					
					RrCCONSULT.Tables[0].Rows[0]["ganoprog"] = DBNull.Value;					
					RrCCONSULT.Tables[0].Rows[0]["gnumprog"] = DBNull.Value;					
					RrCCONSULT.Tables[0].Rows[0]["gagendas"] = DBNull.Value;					
					RrCCONSULT.Tables[0].Rows[0]["gbloques"] = DBNull.Value;					
					RrCCONSULT.Tables[0].Rows[0]["fpetiori"] = Convert.ToDateTime(RrCCONSULT.Tables[0].Rows[0]["ffeccita"]).ToString("dd/MM/yyyy");
				}				
				
				RrCCONSULT.Tables[0].Rows[0]["hllegada"] = DBNull.Value;				
				RrCCONSULT.Tables[0].Rows[0]["hsalidac"] = DBNull.Value;				
				RrCCONSULT.Tables[0].Rows[0]["hatencio"] = DBNull.Value;

				// Grabamos datos del EPISODIO PRINCIPAL ******************************
				// En funcion del dato que encuentro en stIprimisuc
				if (!Hospitalizado && stIprimsuc != "P")
				{ // La cita no es la primera
					// Buscamos en la tabla CCONSULT los registros para ese paciente
					stTemp = " SELECT * " + " FROM CCONSULT " + " WHERE " + " gidenpac ='" + stGidenpac + "' AND " + " gservici =" + stServPrest + " " + " ORDER BY ffeccita asc ";
					SqlDataAdapter tempAdapter_15 = new SqlDataAdapter(stTemp, cnConexion);
					tRr = new DataSet();
					tempAdapter_15.Fill(tRr);
					if (tRr.Tables[0].Rows.Count != 0)
					{						
						tRr.MoveLast(null); //Me posiciono en el ultimo registro que encuentre
						// y cojo el ganoprin gnumprin						
						
						if (!Convert.IsDBNull(tRr.Tables[0].Rows[0]["ganoprin"]))
						{					
							RrCCONSULT.Tables[0].Rows[0]["ganoprin"] = tRr.Tables[0].Rows[0]["ganoprin"];					
							RrCCONSULT.Tables[0].Rows[0]["gnumprin"] = tRr.Tables[0].Rows[0]["gnumprin"];
						}
						else
						{					
							RrCCONSULT.Tables[0].Rows[0]["ganoprin"] = tRr.Tables[0].Rows[0]["ganoregi"];					
							RrCCONSULT.Tables[0].Rows[0]["gnumprin"] = tRr.Tables[0].Rows[0]["gnumregi"];
						}
					
						RrCCONSULT.Tables[0].Rows[0]["fultrevi"] = tRr.Tables[0].Rows[0]["ffeccita"];
					}
					
					tRr.Close();
				}

				
				RrCCONSULT.Tables[0].Rows[0]["ncantida"] = 1;					
				RrCCONSULT.Tables[0].Rows[0]["iautosoc"] = DBNull.Value;				
				RrCCONSULT.Tables[0].Rows[0]["iexiauto"] = DBNull.Value;

				// Si el paciente esta hospitalizado se graba un registro en la tabla
				// EPRESTAC con la prestacion realizada.
				if (Hospitalizado)
				{
					etiqueta2 = true;
					etiqueta = false;
					if (HospitalizadoYdadodeAlta)
					{
						NomTablaPrestaciones = "epresalt";
					}
					else
					{
						NomTablaPrestaciones = "eprestac";
					}
					
					string tempRefParam10 = Convert.ToDateTime(RrCCONSULT.Tables[0].Rows[0]["ffeccita"]).ToString("dd/MM/yyyy") + " 00:00";
					string tempRefParam11 = Convert.ToDateTime(RrCCONSULT.Tables[0].Rows[0]["ffeccita"]).ToString("dd/MM/yyyy") + " 23:59";
					stSql = "SELECT * " + 
					        "FROM " + NomTablaPrestaciones + 
					        " WHERE itiposer = '" + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["itiposer"]) + "' AND " + 
					        "ganoregi = " + stGanoadme + " AND " + 
					        "gnumregi = " + stGnumadme + " AND " + 
					        "gprestac = '" + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["gprestac"]) + "' AND " + 
					        "(iestsoli = 'P' OR iestsoli = 'A' OR (iestsoli = 'C' AND " + 
					        "fprevista >= " + Serrores.FormatFechaHM(tempRefParam10) + " AND " + 
					        "fprevista <= " + Serrores.FormatFechaHM(tempRefParam11) + "))" + 
					        "ORDER BY iestsoli, fpeticio";

					SqlDataAdapter tempAdapter_16 = new SqlDataAdapter(stSql, cnConexion);
					rsPeticiones = new DataSet();
					tempAdapter_16.Fill(rsPeticiones);
					if (rsPeticiones.Tables[0].Rows.Count == 0)
					{						
						rsPeticiones.AddNew();						
						rsPeticiones.Tables[0].Rows[0]["iTiposer"] = RrCCONSULT.Tables[0].Rows[0]["itiposer"];						
						rsPeticiones.Tables[0].Rows[0]["ganoregi"] = RrCCONSULT.Tables[0].Rows[0]["ganoprin"];						
						rsPeticiones.Tables[0].Rows[0]["gnumregi"] = RrCCONSULT.Tables[0].Rows[0]["gnumprin"];						
						rsPeticiones.Tables[0].Rows[0]["fpeticio"] = RrCCONSULT.Tables[0].Rows[0]["ffeccita"];						
						rsPeticiones.Tables[0].Rows[0]["gprestac"] = RrCCONSULT.Tables[0].Rows[0]["gprestac"];
					}
					else
					{						
						rsPeticiones.Edit();
					}
					
					rsPeticiones.Tables[0].Rows[0]["NMUESTRA"] = (Convert.ToString(DOPENEXT.Tables[0].Rows[0]["NMUESTRA"]) + "").Trim();

					
					if (Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["itipopac"]) == "P")
					{
						
						rsPeticiones.Tables[0].Rows[0]["ipriorid"] = "U";
					}
					else
					{						
						rsPeticiones.Tables[0].Rows[0]["ipriorid"] = RrCCONSULT.Tables[0].Rows[0]["itipopac"];
					}
					
					rsPeticiones.Tables[0].Rows[0]["iestsoli"] = "R";
										
					if (!Convert.IsDBNull(RrCCONSULT.Tables[0].Rows[0]["omotaten"]))
					{
						
						rsPeticiones.Tables[0].Rows[0]["omotisol"] = RrCCONSULT.Tables[0].Rows[0]["omotaten"];
					}
					
					rsPeticiones.Tables[0].Rows[0]["frealiza"] = RrCCONSULT.Tables[0].Rows[0]["ffeccita"];
					//Jes�s 16/01/2008 Actualizar la fecha de paso a facturaci�n
					if (HospitalizadoYdadodeAlta)
					{						
						rsPeticiones.Tables[0].Rows[0]["fpasfact"] = RrCCONSULT.Tables[0].Rows[0]["ffeccita"];
					}					
					
					if (!Convert.IsDBNull(RrCCONSULT.Tables[0].Rows[0]["gsersoli"]))
					{						
						rsPeticiones.Tables[0].Rows[0]["gsersoli"] = RrCCONSULT.Tables[0].Rows[0]["gsersoli"];
					}
					else
					{						
						if (Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["itiposer"]) == "H")
						{							
							rsPeticiones.Tables[0].Rows[0]["gsersoli"] = RrAEPISADM.Tables[0].Rows[0]["gserulti"];
						}
						else
						{					
							rsPeticiones.Tables[0].Rows[0]["gsersoli"] = RrUEPISURG.Tables[0].Rows[0]["gservici"];
						}
					}					
					
					if (!Convert.IsDBNull(RrCCONSULT.Tables[0].Rows[0]["gmedsoli"]))
					{						
						rsPeticiones.Tables[0].Rows[0]["gpersoli"] = RrCCONSULT.Tables[0].Rows[0]["gmedsoli"];
					}
					else
					{						
						if (Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["itiposer"]) == "H")
						{						
							rsPeticiones.Tables[0].Rows[0]["gpersoli"] = RrAEPISADM.Tables[0].Rows[0]["gperulti"];
						}
						else
						{				
							if (!Convert.IsDBNull(RrUEPISURG.Tables[0].Rows[0]["gpersona"]))
							{						
								rsPeticiones.Tables[0].Rows[0]["gpersoli"] = RrUEPISURG.Tables[0].Rows[0]["gpersona"];
							}
						}
					}
					
					RrCCONSULT.Tables[0].Rows[0]["gsersoli"] = rsPeticiones.Tables[0].Rows[0]["gsersoli"];					
					RrCCONSULT.Tables[0].Rows[0]["gmedsoli"] = rsPeticiones.Tables[0].Rows[0]["gpersoli"];					
					rsPeticiones.Tables[0].Rows[0]["GUsuario"] = RrCCONSULT.Tables[0].Rows[0]["gusuario"];					
					rsPeticiones.Tables[0].Rows[0]["itiprela"] = "C";					
					rsPeticiones.Tables[0].Rows[0]["ganorela"] = RrCCONSULT.Tables[0].Rows[0]["ganoregi"];					
					rsPeticiones.Tables[0].Rows[0]["gnumrela"] = RrCCONSULT.Tables[0].Rows[0]["gnumregi"];				
					
					if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["gtipmues"]))
					{						
						rsPeticiones.Tables[0].Rows[0]["gtipmues"] = DOPENEXT.Tables[0].Rows[0]["gtipmues"];
					}
					
					string tempQuery = rsPeticiones.Tables[0].TableName;
					SqlDataAdapter tempAdapter_17 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_17);
					tempAdapter_17.Update(rsPeticiones, rsPeticiones.Tables[0].TableName);

				}
                			
				GrabarProtocolo(PrestacionPrincipal, ref stFpeticio, stGidenpac, Convert.ToInt32(RrCCONSULT.Tables[0].Rows[0]["ganoregi"]), Convert.ToInt32(RrCCONSULT.Tables[0].Rows[0]["gnumregi"]), Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["itiposer"]), Convert.ToDateTime(RrCCONSULT.Tables[0].Rows[0]["fmecaniz"]));
                				
				ResultadoPrueba = DOPENEXT.Tables[0].Rows[0]["resultado"];

				//'     ' ********************************************************************
				//'     ' Si se han introducido OTRAS PRESTACIONES en la spread, las
				//'     ' grabo en la tabla EPRESALT
				//'     If DOPENEXT.EOF = False Then
				//'        DOPENEXT.MoveNext
				//'        While DOPENEXT.EOF = False
				//'
				//'            stsql = " SELECT * " _
				//''                & " FROM EPRESALT " _
				//''                & " WHERE 2=1"
				//'            Set RrEPRESALT = cnConexion.OpenResultset(stsql, rdOpenKeyset, rdConcurValues)
				//'            RrEPRESALT.AddNew
				//'            RrEPRESALT("itiposer") = "C"
				//'            RrEPRESALT("ganoregi") = RrCCONSULT("ganoregi")
				//'            RrEPRESALT("gnumregi") = RrCCONSULT("gnumregi")
				//'            RrEPRESALT("fpeticio") = RrCCONSULT("ffeccita")
				//'            RrEPRESALT("gprestac") = DOPENEXT("GPRESTAC")
				//'            RrEPRESALT("nmuestra") = DOPENEXT("NMUESTRA")
				//'            If UCase(DOPENEXT("IPRIORID")) = "N" Then
				//'                RrEPRESALT("ipriorid") = "N"
				//'            Else
				//'                RrEPRESALT("ipriorid") = "U"
				//'            End If
				//'            RrEPRESALT("iestsoli") = "R"
				//'            RrEPRESALT("omotisol") = Null
				//'            RrEPRESALT("NCANTIDA") = 1
				//'            RrEPRESALT("fprevista") = Null
				//'            RrEPRESALT("frealiza") = RrCCONSULT("ffeccita")
				//'            RrEPRESALT("fanulaci") = Null
				//'            If Not IsNull(RrCCONSULT("gsersoli")) Then
				//'               RrEPRESALT("gsersoli") = RrCCONSULT("gsersoli")
				//'            End If
				//'            If Not IsNull(RrCCONSULT("gmedsoli")) Then
				//'                RrEPRESALT("gpersoli") = RrCCONSULT("gmedsoli")
				//'            End If
				//'            RrEPRESALT("iautosoc") = Null
				//'            RrEPRESALT("iexiauto") = Null
				//'            RrEPRESALT("gusuario") = usuario
				//'
				//'            GrabarProtocolo RrEPRESALT("gprestac"), stFpeticio, stGidenpac, RrCCONSULT("ganoregi"), RrCCONSULT("gnumregi"), RrCCONSULT("itiposer"), RrCCONSULT("fmecaniz")
				//'
				//'            RrEPRESALT.Update
				//'            RrEPRESALT.Close
				//'            DOPENEXT.MoveNext
				//'        Wend
				//'     End If

				
				VItiposer = Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["itiposer"]);				
				VstAnoEpisodio = Convert.ToInt32(RrCCONSULT.Tables[0].Rows[0]["ganoregi"]);				
				VstNumEpisodio = Convert.ToInt32(RrCCONSULT.Tables[0].Rows[0]["gnumregi"]);				
				VstServicio = Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["gservici"]);				
				VFechaRea = Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["ffeccita"]);

				//RELLENA TABLAS DE COMUNICACION								
				object tempRefParam12 = stFpeticio;
				stSql = "UPDATE DOPENEXT SET ITIPOSER='" + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["ITIPOSER"]) + "',GANOREGI=" + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["GANOREGI"]) + 
				        ", GNUMREGI=" + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["GNUMREGI"]) + " WHERE GIDENPAC='" + stGidenpac + "' AND FPETICIO=" + Serrores.FormatFechaHMS(tempRefParam12) + " and GPRESTAC = '" + stGprestac + "'";
				stFpeticio = Convert.ToString(tempRefParam12);

				SqlCommand tempCommand_3 = new SqlCommand(stSql, cnConexion);
				tempCommand_3.ExecuteNonQuery();                			
				
				object tempRefParam13 = stFpeticio;
				stSql = "UPDATE DANALEXT SET ITIPOSER='" + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["ITIPOSER"]) + "',GANOREGI=" + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["GANOREGI"]) + 
				        ", GNUMREGI=" + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["GNUMREGI"]) + " WHERE GIDENPAC='" + stGidenpac + "' AND FPETICIO=" + Serrores.FormatFechaHMS(tempRefParam13) + " and GPRESTAC = '" + stGprestac + "'";
				stFpeticio = Convert.ToString(tempRefParam13);

				SqlCommand tempCommand_4 = new SqlCommand(stSql, cnConexion);
				tempCommand_4.ExecuteNonQuery();
				//Si procede de una cita...
				
				if ((Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["gnumprog"]) + "").Trim() != "" && (Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["ganoprog"]) + "").Trim() != "")
				{
					//Si la cita procede de lista de espera
					//stSql = "select aepisleq.ganoproc, aepisleq.gnumproc,'C'," & RrCCONSULT("ganoregi") & "," & RrCCONSULT("gnumregi") & ",'C',cconsult.ganoregi,cconsult.gnumregi,'INDRA' From apreoleq "
					stSql = "select aepisleq.ganoproc,aepisleq.gnumproc From apreoleq  inner join aepisleq on apreoleq.ganoregi = aepisleq.ganoregi and apreoleq.gnumregi = aepisleq.gnumregi Where ganocita = " + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["ganoprog"]) + " And gnumcita = " + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["gnumprog"]) + "";
					SqlDataAdapter tempAdapter_18 = new SqlDataAdapter(stSql, cnConexion);
					rsPeticiones = new DataSet();
					tempAdapter_18.Fill(rsPeticiones);
					if (rsPeticiones.Tables[0].Rows.Count != 0)
					{
						//Si no est� incluida, la consulta asociada a la cita, entre los episodios del proceso de lista de espera...
						
						stSql = "select * From depiproc Where ganoproc = " + Convert.ToString(rsPeticiones.Tables[0].Rows[0]["ganoproc"]) + " And gnumproc = " + Convert.ToString(rsPeticiones.Tables[0].Rows[0]["gnumproc"]) + " and itiposer='C' and ganoregi = " + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["ganoregi"]) + " And gnumregi = " + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["gnumregi"]);
						SqlDataAdapter tempAdapter_19 = new SqlDataAdapter(stSql, cnConexion);
						rsPeticiones = new DataSet();
						tempAdapter_19.Fill(rsPeticiones);
						if (rsPeticiones.Tables[0].Rows.Count == 0)
						{
							//La insertamos en el proceso de lista de espera
							
							stSql = "insert into depiproc (ganoproc, gnumproc, itiposer, ganoregi, gnumregi, itipprin, ganoprin, gnumprin, gusuario) " + 
							        "select aepisleq.ganoproc, aepisleq.gnumproc,'C'," + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["ganoregi"]) + "," + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["gnumregi"]) + ",'C'," + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["ganoregi"]) + "," + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["gnumregi"]) + ",'" + usuario + "' From apreoleq ";
							
							stSql = stSql + " inner join aepisleq on apreoleq.ganoregi = aepisleq.ganoregi and apreoleq.gnumregi = aepisleq.gnumregi Where ganocita = " + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["ganoprog"]) + " And gnumcita = " + Convert.ToString(RrCCONSULT.Tables[0].Rows[0]["gnumprog"]);
							SqlCommand tempCommand_5 = new SqlCommand(stSql, cnConexion);
							tempCommand_5.ExecuteNonQuery();
						}
					}
					
					rsPeticiones.Close();
				}				
				
				if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["gtipmues"]))
				{				
					RrCCONSULT.Tables[0].Rows[0]["gtipmues"] = DOPENEXT.Tables[0].Rows[0]["gtipmues"];
				}				
				
				SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(RrCCONSULT, RrCCONSULT.Tables[0].TableName);				
				RrCCONSULT.Close();


				// Si el paciente no esta hospitalizado se generan los movimientos de
				// facturacion
				dynamic retorno = null;
				//retorno = new Retorno.Class2();
				if (!Hospitalizado)
				{
                    //camaya todo_x_5
                    //ClaseFacturacion = new FacturacionConsultas.clsFacturacion();
					short tempRefParam14 = (short) DateTime.Today.Year;
					object tempRefPar5am15 = retorno;
					object tempRefParam16 = Type.Missing;
					object tempRefParam17 = Type.Missing;
					object tempRefParam18 = Type.Missing;
                    //camaya todo_x_5
                    //ClaseFacturacion.GenerarMovimientosFacturacion(ref tempRefParam14, ref lNumeroRegistro, ref cnConexion, ref tempRefParam15, ref tempRefParam16, ref tempRefParam17, ref tempRefParam18);
                    //retorno = (Retorno.Class2) tempRefParam15;
                    //ClaseFacturacion = null;
                }
                else
				{
					//si estuvo hospitalizado en la fecha de la consulta
					if (HospitalizadoYdadodeAlta)
					{
                        //camaya todo_x_5
                        //ClaseFacturacion = new FacturacionConsultas.clsFacturacion();						
                        //short tempRefParam19 = (short) DateTime.Today.Year;
                        //object tempRefParam20 = retorno;
                        //object tempRefParam21 = fechaPerfact;
                        //object tempRefParam22 = Type.Missing;
                        //object tempRefParam23 = Type.Missing;
                        //ClaseFacturacion.GenerarMovimientosFacturacion(ref tempRefParam19, ref lNumeroRegistro, ref cnConexion, ref tempRefParam20, ref tempRefParam21, ref tempRefParam22, ref tempRefParam23);
                        //fechaPerfact = Convert.ToString(tempRefParam21);
                        //retorno = (Retorno.Class2) tempRefParam20;
                        //ClaseFacturacion = null;
                    }
                }

				object valoracion = null;
				bool valoerror = false;
				if (retorno.RetonarError())
				{
					result = true;
					if (bHacerTransac)
					{							
                        cnConexion.RollbackTransScope();
					}
					CodigoError = 300;
					DescripcionError = "Error al enviar los datos a facturaci�n";
				}
				else
				{
					retorno = null;					
					if (!Convert.IsDBNull(ResultadoPrueba))
					{
                        //Generamos autom�ticamente el n�mero de historia
                        //grabar valoraci�n

                        //camaya todo_x_5
                        //valoracion = new ValoLaboracion.ValoLabo();					
                        //valoracion.CreaValoracionLaboratorio(cnConexion, "C", VstAnoEpisodio, VstNumEpisodio, Convert.ToInt32(Double.Parse(VstServicio)), usuario, ResultadoPrueba, VFechaRea, valoerror);

                        if (valoerror)
						{
							if (bHacerTransac)
							{
                                cnConexion.RollbackTransScope();								
							}
							result = true;
							CodigoError = 400;
							DescripcionError = "Error al generar la valoraci�n";
						}
						else
						{
							if (bHacerTransac)
							{							
                                cnConexion.CommitTransScope();
                            }

						}
						valoracion = null;
					}
					else
					{
						if (bHacerTransac)
						{
                            cnConexion.CommitTransScope();							
						}
					}
				}
				return result;
			}
			catch (Exception excep)
			{
				if (!etiqueta2 && !etiqueta)
				{
					throw excep;
				}
				if (etiqueta)
				{
					return true;
				}
				if (etiqueta2 || etiqueta)
				{

					if (bHacerTransac)
					{						
                        cnConexion.RollbackTransScope();
					}
					result = true;
					//'    If Trim(UCase(Err.Source)) = "MSRDO20.DLL" Then
					//'        If rdoErrors.Count > -1 Then
					//'            For i = 0 To rdoErrors.Count - 1
					//'                DescripcionError = DescripcionError & " - " & rdoErrors(i).Description
					//'            Next i
					//'            CodigoError = 200
					//'            rdoErrors.Clear
					//'        Else
					//'            DescripcionError = "Error no controlado"
					//'            CodigoError = 500
					//'        End If
					//'    Else
					//'
					//'    End If
					DescripcionError = "\\ElementosComunes\\ActualizarPrestacionExterna1.log";
					CodigoError = 200;
					Serrores.AnalizaError("ActualizarPrestacionExterna", Path.GetDirectoryName(Application.ExecutablePath), "", "Grabar Consulta", excep, true);

					return result;
				}
			}
			return false;
		}

		internal static bool RegistroCorrecto(DataSet RrComunicacion, ref int CodigoError, ref string DescripcionError)
		{
			bool result = false;
			string sqltempo = String.Empty;
			string sqltempo2 = String.Empty;
			DataSet cursor = null;
			DataSet cursor2 = null;

			try
			{
				stfiliacion = "";
				result = true;
				
				sqltempo = "select * from dcodpres where gprestac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gprestac"]).Trim() + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqltempo, cnConexion);
				cursor = new DataSet();
				tempAdapter.Fill(cursor);
				//comprobar la existencia de la prestaci�n padre
				if (cursor.Tables[0].Rows.Count == 0)
				{				
					RrComunicacion.Edit();				
					RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";				
					RrComunicacion.Tables[0].Rows[0]["coderror"] = 1;				
					RrComunicacion.Tables[0].Rows[0]["error"] = "La prestaci�n no existe o se ha dado de baja";				
					CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);				
					DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);				
					
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
					result = false;
				}
				
				cursor.Close();

				if (result)
				{
					//comprobar las existencia de las prestaciones hijas si es un protocolo				
					sqltempo = "select * from dprotlab where gprestac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gprestac"]).Trim() + "'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqltempo, cnConexion);
					cursor = new DataSet();
					tempAdapter_3.Fill(cursor);
					//Comprobamos si es un protocolo
					if (cursor.Tables[0].Rows.Count != 0)
					{
						//comprobamos si las pruebas hijas son todas de igual servicio.						
						object tempRefParam = RrComunicacion.Tables[0].Rows[0]["fpeticio"];
						sqltempo = "select distinct gservici from danalext inner join dcodpres on danalext.gpruelab = dcodpres.gprestac " + " where danalext.gprestac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gprestac"]).Trim() + "'" + " and gidenpac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gidenpac"]) + "'" + " and FPeticio =" + Serrores.FormatFechaHMS(tempRefParam);
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqltempo, cnConexion);
						cursor = new DataSet();
						tempAdapter_4.Fill(cursor);
						//y adem�s que no proceda de una cita. Para que no pete en el circuito interno.					
						cursor.MoveLast(null);						
						
						if (cursor.Tables[0].Rows.Count > 1 && !Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["GANOPROG"]))
						{						
							RrComunicacion.Edit();						
							RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";						
							RrComunicacion.Tables[0].Rows[0]["coderror"] = 13;						
							RrComunicacion.Tables[0].Rows[0]["error"] = "El perfil o protocolo tiene pruebas asociadas de servicios distintos";							
							CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);							
							DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);							
							
							SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
                            tempAdapter_4.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
							result = false;
						}
						else
						{
							//comprobamos si tiene pruebas hijas.							
							object tempRefParam2 = RrComunicacion.Tables[0].Rows[0]["fpeticio"];
							sqltempo = "select * from danalext where gprestac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gprestac"]).Trim() + "'" + " and gidenpac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gidenpac"]) + "'" + " and FPeticio =" + Serrores.FormatFechaHMS(tempRefParam2);
							SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sqltempo, cnConexion);
							cursor = new DataSet();
							tempAdapter_6.Fill(cursor);
							if (cursor.Tables[0].Rows.Count == 0)
							{								
								RrComunicacion.Edit();							
								RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";							
								RrComunicacion.Tables[0].Rows[0]["coderror"] = 6;							
								RrComunicacion.Tables[0].Rows[0]["error"] = "El perfil o protocolo no tiene pruebas asociadas";							
								CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);							
								DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);							
								
								SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_6);
                                tempAdapter_6.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
								result = false;
							}
							else
							{

								foreach (DataRow iteration_row in cursor.Tables[0].Rows)
								{
									//comprobar las existencia de las prestaciones hijas									
									object tempRefParam3 = RrComunicacion.Tables[0].Rows[0]["fpeticio"];
									object tempRefParam4 = RrComunicacion.Tables[0].Rows[0]["fpeticio"];
									sqltempo2 = "select * from dcodpres where gprestac='" + Convert.ToString(iteration_row["gpruelab"]).Trim() + "' and finivali<=" + Serrores.FormatFechaHMS(tempRefParam3) + " and (ffinvali is null or ffinvali>=" + Serrores.FormatFechaHMS(tempRefParam4) + ")";
									SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(sqltempo2, cnConexion);
									cursor2 = new DataSet();
									tempAdapter_8.Fill(cursor2);
									if (cursor2.Tables[0].Rows.Count == 0)
									{										
										RrComunicacion.Edit();
										RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";
										RrComunicacion.Tables[0].Rows[0]["coderror"] = 1;
										RrComunicacion.Tables[0].Rows[0]["error"] = "La prestaci�n no existe o se ha dado de baja";
										CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);
										DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);
										
										SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_8);
                                        tempAdapter_8.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
										result = false;
									}
								
									cursor2.Close();
									//''                    If RegistroCorrecto = False Then
									//''                        cursor.MoveLast
									//''                    End If
								}
							}
						}
					}
				
					cursor.Close();
				}


				if (result)
				{
					//comprobar existecia del paciente
					
					sqltempo = "select * from dpacient where gidenpac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gidenpac"]).Trim() + "'";
					SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(sqltempo, cnConexion);
					cursor = new DataSet();
					tempAdapter_10.Fill(cursor);
					if (cursor.Tables[0].Rows.Count == 0)
					{
						
						RrComunicacion.Edit();						
						RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";						
						RrComunicacion.Tables[0].Rows[0]["coderror"] = 2;						
						RrComunicacion.Tables[0].Rows[0]["error"] = "El paciente no existe";						
						CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);						
						DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);						
						
						SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter_10);
                        tempAdapter_10.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
						result = false;
					}
					
					cursor.Close();
				}

				
				if (result && !Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["gsersoli"]))
				{
					//comprobar la existecia del servicio solicitante					
					object tempRefParam5 = RrComunicacion.Tables[0].Rows[0]["fpeticio"];
					sqltempo = "select * from dservici where gservici=" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gsersoli"]) + " and (fborrado is null " + " or fborrado >=" + Serrores.FormatFechaHMS(tempRefParam5) + ")";
					SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(sqltempo, cnConexion);
					cursor = new DataSet();
					tempAdapter_12.Fill(cursor);
					if (cursor.Tables[0].Rows.Count == 0)
					{
					
						RrComunicacion.Edit();
					
						RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";					
						RrComunicacion.Tables[0].Rows[0]["coderror"] = 3;					
						RrComunicacion.Tables[0].Rows[0]["error"] = "El servicio solicitante no existe o est� de baja";
					
						CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);					
						DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);					
						
						SqlCommandBuilder tempCommandBuilder_6 = new SqlCommandBuilder(tempAdapter_12);
                        tempAdapter_12.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
						result = false;
					}
				
					cursor.Close();
				}

				
				if (result && !Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["gpersoli"]))
				{
					//comprobar la existencia de m�dico solicitante
					
					
					object tempRefParam6 = RrComunicacion.Tables[0].Rows[0]["fpeticio"];
					sqltempo = "select * from dpersona where gpersona=" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gpersoli"]) + " and (fborrado is null " + " or fborrado >=" + Serrores.FormatFechaHMS(tempRefParam6) + ")";
					SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(sqltempo, cnConexion);
					cursor = new DataSet();
					tempAdapter_14.Fill(cursor);
                    if (cursor.Tables[0].Rows.Count == 0)
                    {                       
                        RrComunicacion.Edit();                      
                        RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";                      
                        RrComunicacion.Tables[0].Rows[0]["coderror"] = 4;                      
                        RrComunicacion.Tables[0].Rows[0]["error"] = "El m�dico solicitante no existe o est� de baja";                      
                        CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);                      
                        DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);                     
                        SqlCommandBuilder tempCommandBuilder_7 = new SqlCommandBuilder(tempAdapter_14);
                        tempAdapter_14.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
						result = false;
					}
					
					cursor.Close();
				}

				if (result)
				{
					//comprobar la existencia de m�dico realizador
					
					
					if (!Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["gperreal"]) && Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gperreal"]) != "")
					{					
						object tempRefParam7 = RrComunicacion.Tables[0].Rows[0]["fpeticio"];
						sqltempo = "select * from dpersona where gpersona=" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gperreal"]) + " and (fborrado is null " + " or fborrado >=" + Serrores.FormatFechaHMS(tempRefParam7) + ")";
						SqlDataAdapter tempAdapter_16 = new SqlDataAdapter(sqltempo, cnConexion);
						cursor = new DataSet();
						tempAdapter_16.Fill(cursor);
						if (cursor.Tables[0].Rows.Count == 0)
						{							
							RrComunicacion.Edit();					
							RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";					
							RrComunicacion.Tables[0].Rows[0]["coderror"] = 4;					
							RrComunicacion.Tables[0].Rows[0]["error"] = "El m�dico realizador no existe o est� de baja";					
							CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);					
							DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);					
							
							SqlCommandBuilder tempCommandBuilder_8 = new SqlCommandBuilder(tempAdapter_16);
                            tempAdapter_16.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
							result = false;
						}
					
						cursor.Close();
					}
					else
					{
						//sqltempo = "select nnumeri1 from sconsglo where gconsglo ='presprod' "
						//La persona responsable de laboratorio debe salir de la tabla DOPENSER
					
						sqltempo = " select dopenser.gpersona  " + 
						           " From dopenser " + 
						           " inner join dcodpres on dopenser.gservici = dcodpres.gservici " + 
						           " where dcodpres.gprestac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gprestac"]) + "'";

						SqlDataAdapter tempAdapter_18 = new SqlDataAdapter(sqltempo, cnConexion);
						cursor = new DataSet();
						tempAdapter_18.Fill(cursor);
						if (cursor.Tables[0].Rows.Count != 0)
						{
							//sqltempo = "select * from dpersona where gpersona = " & IIf(IsNull(cursor("nnumeri1")), -1, cursor("nnumeri1"))				
							sqltempo = "select * from dpersona where gpersona = " + ((Convert.IsDBNull(cursor.Tables[0].Rows[0]["gpersona"])) ? "-1" : Convert.ToString(cursor.Tables[0].Rows[0]["gpersona"]));
							SqlDataAdapter tempAdapter_19 = new SqlDataAdapter(sqltempo, cnConexion);
							cursor = new DataSet();
							tempAdapter_19.Fill(cursor);
							if (cursor.Tables[0].Rows.Count == 0)
							{
					
								RrComunicacion.Edit();
					
								RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";
								//RrComunicacion("error") = "4 El m�dico realizador de la constante PRESPROD no existe o es null."
					
								RrComunicacion.Tables[0].Rows[0]["error"] = "4 El m�dico realizador de la tabla DOPENSER no existe o es null.";
					
								CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);
					
								DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);
					
								
								SqlCommandBuilder tempCommandBuilder_9 = new SqlCommandBuilder(tempAdapter_19);
                                tempAdapter_19.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
								result = false;
							}
						}
						else
						{							
							RrComunicacion.Edit();					
							RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";
							//RrComunicacion("error") = "4 La constante PRESPROD no existe."					
							RrComunicacion.Tables[0].Rows[0]["error"] = "4 El registro en DOPENSER no existe.";						
							DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);							
							
							SqlCommandBuilder tempCommandBuilder_10 = new SqlCommandBuilder(tempAdapter_18);
                            tempAdapter_18.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
							result = false;
						}
						
						cursor.Close();

					}
				}				
				
				if (result && !Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["gpersoli"]) && !Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["gsersoli"]))
				{
					//comprobar la relaci�n persona-servicio					
					sqltempo = "select * from dperserv where gpersona=" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gpersoli"]) + " and " + " gservici=" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gsersoli"]);
					SqlDataAdapter tempAdapter_22 = new SqlDataAdapter(sqltempo, cnConexion);
					cursor = new DataSet();
					tempAdapter_22.Fill(cursor);
					if (cursor.Tables[0].Rows.Count == 0)
					{						
						RrComunicacion.Edit();					
						RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";					
						RrComunicacion.Tables[0].Rows[0]["coderror"] = 5;					
						RrComunicacion.Tables[0].Rows[0]["error"] = "No existe la relaci�n servicio-m�dico solicitante";
					
						CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);					
						DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);
					
						
						SqlCommandBuilder tempCommandBuilder_11 = new SqlCommandBuilder(tempAdapter_22);
                        tempAdapter_22.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
						result = false;
					}
					
					cursor.Close();
				}
                				
				if (result && !Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["gsocieda"]))
				{
					//comprobar la existencia de la sociedad				
					sqltempo = "select * from dsociedava where gsocieda=" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gsocieda"]).Trim();
					SqlDataAdapter tempAdapter_24 = new SqlDataAdapter(sqltempo, cnConexion);
					cursor = new DataSet();
					tempAdapter_24.Fill(cursor);
					if (cursor.Tables[0].Rows.Count == 0)
					{
						
						RrComunicacion.Edit();						
						RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";						
						RrComunicacion.Tables[0].Rows[0]["coderror"] = 7;						
						RrComunicacion.Tables[0].Rows[0]["error"] = "No existe la sociedad garante";						
						CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);						
						DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);						
						
						SqlCommandBuilder tempCommandBuilder_12 = new SqlCommandBuilder(tempAdapter_24);
                        tempAdapter_24.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
						result = false;
					}
					
					cursor.Close();
				}				
				
				if (result && !Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["ginspecc"]))
				{
					//comprobar la existencia de la inspecci�n
				
					sqltempo = "select * from dinspeccva where ginspecc= '" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["ginspecc"]).Trim() + "'";
					SqlDataAdapter tempAdapter_26 = new SqlDataAdapter(sqltempo, cnConexion);
					cursor = new DataSet();
					tempAdapter_26.Fill(cursor);
					if (cursor.Tables[0].Rows.Count == 0)
					{						
						RrComunicacion.Edit();						
						RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";						
						RrComunicacion.Tables[0].Rows[0]["coderror"] = 8;						
						RrComunicacion.Tables[0].Rows[0]["error"] = "No existe la inspecci�n garante";						
						CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);						
						DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);					
						
						SqlCommandBuilder tempCommandBuilder_13 = new SqlCommandBuilder(tempAdapter_26);
                        tempAdapter_26.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
						result = false;
					}
					
					cursor.Close();
				}

				
				if (result && !Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["gsocieda"]))
				{
					//comprobar la existencia en DENTIPAC				
					int switchVar = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["gsocieda"]);
					if (switchVar == lSS)
					{				
						
						if (!Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["ginspecc"]))
						{						
							sqltempo = "select * from dentipac where gsocieda = " + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gsocieda"]) + " and ginspecc = '" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["ginspecc"]) + "' and gidenpac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gidenpac"]).Trim() + "'";
							SqlDataAdapter tempAdapter_28 = new SqlDataAdapter(sqltempo, cnConexion);
							cursor = new DataSet();
							tempAdapter_28.Fill(cursor);
							if (cursor.Tables[0].Rows.Count == 0)
							{
								//RrComunicacion.Edit
								//RrComunicacion("iestsoli") = "E"
								//RrComunicacion("coderror") = 9
								//RrComunicacion("error") = "No existe la inspecci�n en la tabla de entidades del paciente"
								//CodigoError = RrComunicacion("coderror")
								//DescripcionError = RrComunicacion("error")
								//RrComunicacion.Update
								//RegistroCorrecto = False
								
								sqltempo = "select * from dentipac where gsocieda = " + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gsocieda"]) + " and gidenpac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gidenpac"]).Trim() + "'";
								SqlDataAdapter tempAdapter_29 = new SqlDataAdapter(sqltempo, cnConexion);
								cursor = new DataSet();
								tempAdapter_29.Fill(cursor);
								if (cursor.Tables[0].Rows.Count == 0)
								{
								
									cursor.AddNew();
								
									cursor.Tables[0].Rows[0]["gidenpac"] = RrComunicacion.Tables[0].Rows[0]["gidenpac"];								
									cursor.Tables[0].Rows[0]["gsocieda"] = RrComunicacion.Tables[0].Rows[0]["gsocieda"];								
									cursor.Tables[0].Rows[0]["ginspecc"] = RrComunicacion.Tables[0].Rows[0]["ginspecc"];								
									cursor.Tables[0].Rows[0]["itituben"] = "T";								
									cursor.Tables[0].Rows[0]["fmovimie"] = DateTime.Now;								
									
									SqlCommandBuilder tempCommandBuilder_14 = new SqlCommandBuilder(tempAdapter_29);
                                    tempAdapter_29.Update(cursor, cursor.Tables[0].TableName);
								}
								else
								{
									
									cursor.Edit();
									
									cursor.Tables[0].Rows[0]["ginspecc"] = RrComunicacion.Tables[0].Rows[0]["ginspecc"];									
									
									SqlCommandBuilder tempCommandBuilder_15 = new SqlCommandBuilder(tempAdapter_29);
                                    tempAdapter_29.Update(cursor, cursor.Tables[0].TableName);
									
									stfiliacion = (Convert.ToString(cursor.Tables[0].Rows[0]["nafiliac"]) + "").Trim();
								}
							}
							else
							{
								
								stfiliacion = (Convert.ToString(cursor.Tables[0].Rows[0]["nafiliac"]) + "").Trim();
							}
							
							cursor.Close();
						}
						else
						{
							//                RrComunicacion.Edit
							//                RrComunicacion("iestsoli") = "E"
							//                RrComunicacion("coderror") = 10
							//                RrComunicacion("error") = "No viene inspecci�n en el registro."
							//                CodigoError = RrComunicacion("coderror")
							//                DescripcionError = RrComunicacion("error")
							//                RrComunicacion.Update
							//                RegistroCorrecto = False
							
							sqltempo = "select * from dentipac where gsocieda = " + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gsocieda"]) + " and gidenpac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gidenpac"]).Trim() + "'";
							SqlDataAdapter tempAdapter_32 = new SqlDataAdapter(sqltempo, cnConexion);
							cursor = new DataSet();
							tempAdapter_32.Fill(cursor);
							if (cursor.Tables[0].Rows.Count == 0)
							{								
								RrComunicacion.Edit();
								
								RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";								
								RrComunicacion.Tables[0].Rows[0]["coderror"] = 10;								
								RrComunicacion.Tables[0].Rows[0]["error"] = "No viene inspecci�n en el registro y no existe inspecci�n para ese paciente.";
								
								CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);								
								DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);								
								
								SqlCommandBuilder tempCommandBuilder_16 = new SqlCommandBuilder(tempAdapter_32);
                                tempAdapter_32.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
								result = false;
							}
						}

					}
					else if (switchVar == lPrivado)
					{ 
						
						sqltempo = "select * from dprivado where gidenpac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gidenpac"]).Trim() + "'";
						SqlDataAdapter tempAdapter_34 = new SqlDataAdapter(sqltempo, cnConexion);
						cursor = new DataSet();
						tempAdapter_34.Fill(cursor);
						if (cursor.Tables[0].Rows.Count == 0)
						{
							
							RrComunicacion.Edit();							
							RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";							
							RrComunicacion.Tables[0].Rows[0]["coderror"] = 11;							
							RrComunicacion.Tables[0].Rows[0]["error"] = "No existen datos del pagador privado para este paciente.";
							
							CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);							
							DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);						
							
							SqlCommandBuilder tempCommandBuilder_17 = new SqlCommandBuilder(tempAdapter_34);
                            tempAdapter_34.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
							result = false;
						}
						else
						{							
							stfiliacion = (Convert.ToString(cursor.Tables[0].Rows[0]["nnifpriv"]) + "").Trim();
						}
						
						cursor.Close();
					}
					else
					{						
						sqltempo = "select * from dentipac where gsocieda = " + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gsocieda"]) + " and gidenpac='" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["gidenpac"]).Trim() + "'";
						SqlDataAdapter tempAdapter_36 = new SqlDataAdapter(sqltempo, cnConexion);
						cursor = new DataSet();
						tempAdapter_36.Fill(cursor);
						if (cursor.Tables[0].Rows.Count == 0)
						{							
							RrComunicacion.Edit();							
							RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";							
							RrComunicacion.Tables[0].Rows[0]["coderror"] = 12;							
							RrComunicacion.Tables[0].Rows[0]["error"] = "No existen datos de la sociedad pagadora para este paciente.";
							
							CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);							
							DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);							
							
							SqlCommandBuilder tempCommandBuilder_18 = new SqlCommandBuilder(tempAdapter_36);
                            tempAdapter_36.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
							result = false;
						}
						else
						{							
							stfiliacion = (Convert.ToString(cursor.Tables[0].Rows[0]["nafiliac"]) + "").Trim();
						}
						
						cursor.Close();
					}
				}
                		
				
				if (result && !Convert.IsDBNull(RrComunicacion.Tables[0].Rows[0]["ghospori"]))
				{
					//comprobar la existencia de la inspecci�n
					
					sqltempo = "select * from dhospitava where ghospita=" + Convert.ToString(RrComunicacion.Tables[0].Rows[0]["ghospori"]);
					SqlDataAdapter tempAdapter_38 = new SqlDataAdapter(sqltempo, cnConexion);
					cursor = new DataSet();
					tempAdapter_38.Fill(cursor);
					if (cursor.Tables[0].Rows.Count == 0)
					{
					
						RrComunicacion.Edit();
					
						RrComunicacion.Tables[0].Rows[0]["iestsoli"] = "E";					
						RrComunicacion.Tables[0].Rows[0]["coderror"] = 9;					
						RrComunicacion.Tables[0].Rows[0]["error"] = "No existe el hospital de procedencia";					
						CodigoError = Convert.ToInt32(RrComunicacion.Tables[0].Rows[0]["coderror"]);
					
						DescripcionError = Convert.ToString(RrComunicacion.Tables[0].Rows[0]["error"]);					

						SqlCommandBuilder tempCommandBuilder_19 = new SqlCommandBuilder(tempAdapter_38);
                        tempAdapter_38.Update(RrComunicacion, RrComunicacion.Tables[0].TableName);
						result = false;
					}
					
					cursor.Close();
				}

				return result;
			}
			catch
			{
				return false;
			}
		}

		internal static object ProRecibirError(object var1, object var2)
		{
			return null;
		}
	}
}