using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace GeneraConsulta
{
	public class Class1
	{

		public object REGISTRACONSULTA(string Paciente, ref string FechaPeticion, string Prestacion, ref string usuario, ref bool ErrorDLL, ref int CodigoError, ref string DescripcionError, object nConnet_optional)
		{
			SqlConnection nConnet = (nConnet_optional == Type.Missing) ? null : nConnet_optional as SqlConnection;
			ErrorDLL = false;

			if (nConnet_optional == Type.Missing)
			{
				ErrorDLL = Module1.ABRIRNUEVACONEXION();
				Module1.bHacerTransac = true;
			}
			else
			{
				Module1.bHacerTransac = false;
				Module1.cnConexion = nConnet;
			}
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Module1.cnConexion);
			Serrores.AjustarRelojCliente(Module1.cnConexion);
			ErrorDLL = Module1.GrabarConsulta(Paciente, ref FechaPeticion, Prestacion, usuario, ref CodigoError, ref DescripcionError);
			if (Module1.bHacerTransac)
			{                   
                Module1.cnConexion.Close();          
			}
			return null;
		}

		public object REGISTRACONSULTA(string Paciente, ref string FechaPeticion, string Prestacion, ref string usuario, ref bool ErrorDLL, ref int CodigoError, ref string DescripcionError)
		{
			return REGISTRACONSULTA(Paciente, ref FechaPeticion, Prestacion, ref usuario, ref ErrorDLL, ref CodigoError, ref DescripcionError, Type.Missing);
		}
	}
}