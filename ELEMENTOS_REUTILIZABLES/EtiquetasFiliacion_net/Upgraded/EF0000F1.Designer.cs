using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace EtiquetasF
{
	partial class EF0000F1
	{

		#region "Upgrade Support "
		private static EF0000F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static EF0000F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new EF0000F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_optMedico_1", "_optMedico_0", "tbMedico", "cbMedico", "Frame1", "cbbServicio", "CrystalReport1", "cbbImpresoras", "tbCopias", "cbImprimir", "cbCancelar", "Label4", "lbPaciente", "Label1", "lbImpresora", "lbCopias", "optMedico"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		private Telerik.WinControls.UI.RadRadioButton _optMedico_1;
        private Telerik.WinControls.UI.RadRadioButton  _optMedico_0;
		public Telerik.WinControls.UI.RadTextBox tbMedico;
		public UpgradeHelpers.MSForms.MSCombobox cbMedico;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public UpgradeHelpers.MSForms.MSCombobox cbbServicio;
		//public AxCrystal.AxCrystalReport CrystalReport1;
		public UpgradeHelpers.MSForms.MSCombobox cbbImpresoras;
		public Telerik.WinControls.UI.RadTextBox tbCopias;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel lbImpresora;
		public Telerik.WinControls.UI.RadLabel lbCopias;
		public Telerik.WinControls.UI.RadRadioButton[] optMedico = new Telerik.WinControls.UI.RadRadioButton[2];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this._optMedico_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._optMedico_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbMedico = new Telerik.WinControls.UI.RadTextBox();
            this.cbMedico = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbServicio = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbImpresoras = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.tbCopias = new Telerik.WinControls.UI.RadTextBox();
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.lbImpresora = new Telerik.WinControls.UI.RadLabel();
            this.lbCopias = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._optMedico_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._optMedico_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMedico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMedico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbImpresoras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCopias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbImpresora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCopias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this._optMedico_1);
            this.Frame1.Controls.Add(this._optMedico_0);
            this.Frame1.Controls.Add(this.tbMedico);
            this.Frame1.Controls.Add(this.cbMedico);
            this.Frame1.HeaderText = "Responsable:";
            this.Frame1.Location = new System.Drawing.Point(4, 62);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(443, 67);
            this.Frame1.TabIndex = 10;
            this.Frame1.Text = "Responsable:";
            // 
            // _optMedico_1
            // 
            this._optMedico_1.CheckState = System.Windows.Forms.CheckState.Checked;
            this._optMedico_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._optMedico_1.Location = new System.Drawing.Point(28, 44);
            this._optMedico_1.Name = "_optMedico_1";
            this._optMedico_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._optMedico_1.Size = new System.Drawing.Size(58, 18);
            this._optMedico_1.TabIndex = 14;
            this._optMedico_1.Text = "Externo";
            this._optMedico_1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // _optMedico_0
            // 
            this._optMedico_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._optMedico_0.Location = new System.Drawing.Point(28, 18);
            this._optMedico_0.Name = "_optMedico_0";
            this._optMedico_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._optMedico_0.Size = new System.Drawing.Size(56, 18);
            this._optMedico_0.TabIndex = 13;
            this._optMedico_0.Text = "Interno";
            // 
            // tbMedico
            // 
            this.tbMedico.AcceptsReturn = true;
            this.tbMedico.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbMedico.Location = new System.Drawing.Point(106, 42);
            this.tbMedico.MaxLength = 60;
            this.tbMedico.Name = "tbMedico";
            this.tbMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbMedico.Size = new System.Drawing.Size(329, 20);
            this.tbMedico.TabIndex = 12;
            // 
            // cbMedico
            // 
            this.cbMedico.ColumnWidths = "";
            this.cbMedico.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMedico.Location = new System.Drawing.Point(106, 16);
            this.cbMedico.Name = "cbMedico";
            this.cbMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMedico.Size = new System.Drawing.Size(332, 20);
            this.cbMedico.TabIndex = 11;
            // 
            // cbbServicio
            // 
            this.cbbServicio.ColumnWidths = "";
            this.cbbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbServicio.Location = new System.Drawing.Point(111, 32);
            this.cbbServicio.Name = "cbbServicio";
            this.cbbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbServicio.Size = new System.Drawing.Size(336, 20);
            this.cbbServicio.TabIndex = 8;
            // 
            // cbbImpresoras
            // 
            this.cbbImpresoras.BackColor = System.Drawing.SystemColors.Window;
            this.cbbImpresoras.ColumnWidths = "";
            this.cbbImpresoras.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbImpresoras.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cbbImpresoras.Location = new System.Drawing.Point(112, 140);
            this.cbbImpresoras.Name = "cbbImpresoras";
            this.cbbImpresoras.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbImpresoras.Size = new System.Drawing.Size(336, 20);
            this.cbbImpresoras.TabIndex = 3;
            // 
            // tbCopias
            // 
            this.tbCopias.AcceptsReturn = true;
            this.tbCopias.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCopias.Location = new System.Drawing.Point(112, 167);
            this.tbCopias.MaxLength = 2;
            this.tbCopias.Name = "tbCopias";
            this.tbCopias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCopias.Size = new System.Drawing.Size(43, 20);
            this.tbCopias.TabIndex = 2;
            this.tbCopias.Enter += new System.EventHandler(this.tbCopias_Enter);
            this.tbCopias.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCopias_KeyPress);
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.Location = new System.Drawing.Point(264, 176);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(86, 29);
            this.cbImprimir.TabIndex = 1;
            this.cbImprimir.Text = "&Imprimir";
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(362, 176);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(86, 29);
            this.cbCancelar.TabIndex = 0;
            this.cbCancelar.Text = "&Cerrar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(4, 34);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(47, 18);
            this.Label4.TabIndex = 9;
            this.Label4.Text = "Servicio:";
            // 
            // lbPaciente
            // 
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Location = new System.Drawing.Point(110, 8);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(341, 20);
            this.lbPaciente.TabIndex = 7;
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(4, 8);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(51, 18);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "Paciente:";
            // 
            // lbImpresora
            // 
            this.lbImpresora.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbImpresora.Location = new System.Drawing.Point(6, 143);
            this.lbImpresora.Name = "lbImpresora";
            this.lbImpresora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbImpresora.Size = new System.Drawing.Size(59, 18);
            this.lbImpresora.TabIndex = 5;
            this.lbImpresora.Text = "Impresora:";
            // 
            // lbCopias
            // 
            this.lbCopias.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbCopias.Location = new System.Drawing.Point(6, 171);
            this.lbCopias.Name = "lbCopias";
            this.lbCopias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbCopias.Size = new System.Drawing.Size(115, 18);
            this.lbCopias.TabIndex = 4;
            this.lbCopias.Text = "N�mero de etiquetas:";
            // 
            // EF0000F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 213);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbbServicio);
            this.Controls.Add(this.cbbImpresoras);
            this.Controls.Add(this.tbCopias);
            this.Controls.Add(this.cbImprimir);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.lbPaciente);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.lbImpresora);
            this.Controls.Add(this.lbCopias);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EF0000F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Etiquetas de Filiaci�n - EF0000F2";
            this.Activated += new System.EventHandler(this.EF0000F1_Activated);
            this.Closed += new System.EventHandler(this.EF0000F1_Closed);
            this.Load += new System.EventHandler(this.EF0000F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._optMedico_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._optMedico_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMedico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMedico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbImpresoras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCopias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbImpresora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCopias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		void ReLoadForm(bool addEvents)
		{
			InitializeoptMedico();
		}
		void InitializeoptMedico()
		{
			this.optMedico = new Telerik.WinControls.UI.RadRadioButton[2];
			this.optMedico[1] = _optMedico_1;
			this.optMedico[0] = _optMedico_0;
		}
		#endregion
	}
}