using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace EtiquetasF
{
	partial class EF0000F2
	{

		#region "Upgrade Support "
		private static EF0000F2 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static EF0000F2 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new EF0000F2();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbbImpresoras", "tbCopias", "cbImprimir", "cbCancelar", "CrystalReport1", "LbLitetiquetas", "LbConsulta", "lbImpresora", "lbCopias"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.MSForms.MSCombobox cbbImpresoras;
		public Telerik.WinControls.UI.RadTextBox tbCopias;
		public Telerik.WinControls.UI.RadButton  cbImprimir;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		//public AxCrystal.AxCrystalReport CrystalReport1;
		public Telerik.WinControls.UI.RadLabel LbLitetiquetas;
		public Telerik.WinControls.UI.RadTextBox LbConsulta;
		public Telerik.WinControls.UI.RadLabel lbImpresora;
		public Telerik.WinControls.UI.RadLabel lbCopias;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cbbImpresoras = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.tbCopias = new Telerik.WinControls.UI.RadTextBox();
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.LbLitetiquetas = new Telerik.WinControls.UI.RadLabel();
            this.LbConsulta = new Telerik.WinControls.UI.RadTextBox();
            this.lbImpresora = new Telerik.WinControls.UI.RadLabel();
            this.lbCopias = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbbImpresoras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCopias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbLitetiquetas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbConsulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbImpresora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCopias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbbImpresoras
            // 
            this.cbbImpresoras.ColumnWidths = "";
            this.cbbImpresoras.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbImpresoras.Location = new System.Drawing.Point(12, 65);
            this.cbbImpresoras.Name = "cbbImpresoras";
            this.cbbImpresoras.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbImpresoras.Size = new System.Drawing.Size(264, 20);
            this.cbbImpresoras.TabIndex = 3;
            this.cbbImpresoras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbImpresoras_KeyPress);
            // 
            // tbCopias
            // 
            this.tbCopias.AcceptsReturn = true;
            this.tbCopias.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCopias.Location = new System.Drawing.Point(162, 90);
            this.tbCopias.MaxLength = 0;
            this.tbCopias.Name = "tbCopias";
            this.tbCopias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCopias.Size = new System.Drawing.Size(39, 20);
            this.tbCopias.TabIndex = 2;
            this.tbCopias.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbCopias_KeyDown);
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.Location = new System.Drawing.Point(133, 133);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(86, 29);
            this.cbImprimir.TabIndex = 1;
            this.cbImprimir.Text = "&Imprimir";
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(231, 133);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(86, 29);
            this.cbCancelar.TabIndex = 0;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // LbLitetiquetas
            // 
            this.LbLitetiquetas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbLitetiquetas.Location = new System.Drawing.Point(0, 17);
            this.LbLitetiquetas.Name = "LbLitetiquetas";
            this.LbLitetiquetas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbLitetiquetas.Size = new System.Drawing.Size(70, 18);
            this.LbLitetiquetas.TabIndex = 7;
            this.LbLitetiquetas.Text = "Etiquetas de:";
            // 
            // LbConsulta
            // 
            this.LbConsulta.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbConsulta.Location = new System.Drawing.Point(76, 12);
            this.LbConsulta.Name = "LbConsulta";
            this.LbConsulta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbConsulta.Size = new System.Drawing.Size(236, 20);
            this.LbConsulta.TabIndex = 6;
            // 
            // lbImpresora
            // 
            this.lbImpresora.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbImpresora.Location = new System.Drawing.Point(11, 46);
            this.lbImpresora.Name = "lbImpresora";
            this.lbImpresora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbImpresora.Size = new System.Drawing.Size(251, 18);
            this.lbImpresora.TabIndex = 5;
            this.lbImpresora.Text = "Seleccione Impresora para imprimir las etiquetas:";
            // 
            // lbCopias
            // 
            this.lbCopias.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbCopias.Location = new System.Drawing.Point(11, 90);
            this.lbCopias.Name = "lbCopias";
            this.lbCopias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbCopias.Size = new System.Drawing.Size(154, 18);
            this.lbCopias.TabIndex = 4;
            this.lbCopias.Text = "Seleccione n�mero de copias:";
            // 
            // EF0000F2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 171);
            this.Controls.Add(this.cbbImpresoras);
            this.Controls.Add(this.tbCopias);
            this.Controls.Add(this.cbImprimir);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.LbLitetiquetas);
            this.Controls.Add(this.LbConsulta);
            this.Controls.Add(this.lbImpresora);
            this.Controls.Add(this.lbCopias);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EF0000F2";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Impresi�n de etiqueta Simple de la cita - CCI400F1";
            this.Activated += new System.EventHandler(this.EF0000F2_Activated);
            this.Closed += new System.EventHandler(this.EF0000F2_Closed);
            this.Load += new System.EventHandler(this.EF0000F2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbbImpresoras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCopias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbLitetiquetas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbConsulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbImpresora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCopias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}