using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace EtiquetasF
{
	internal static class FuncEtiquetas
	{

		public static SqlConnection cnconexion = null;

		internal static bool ComprobarSiImprimeConLaser()
		{
			string sql = String.Empty;
			DataSet rr1 = null;
			bool btest = false;
			try
			{
				btest = false;
				//Comprobamos en SCONSGLO si tienen impresoras "LASER" para cambiar de nombre al informe
				sql = "select valfanu1 from sconsglo  where gconsglo='TIPOETIQ'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, cnconexion);
				rr1 = new DataSet();
				tempAdapter.Fill(rr1);
				if (rr1.Tables[0].Rows.Count != 0)
				{
					
					if (Convert.ToString(rr1.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "L")
					{
						btest = true;
					}
				}
				
				rr1.Close();
				return btest;
			}
			catch
			{
				return false;
			}
		}

		internal static string fnCodigoBarras(string tcString)
		{

			int lnAsc = 0;

			string lcStart = Strings.Chr(104 + 32).ToString();
			string lcStop = Strings.Chr(106 + 32).ToString();
			int lnCheckSum = Strings.Asc(lcStart[0]) - 32;
			string lcRet = tcString;
			int lnLong = lcRet.Length;
			for (int lnI = 1; lnI <= lnLong; lnI++)
			{
				lnAsc = Strings.Asc(lcRet.Substring(lnI - 1, Math.Min(1, lcRet.Length - (lnI - 1)))[0]) - 32;
				if (!(lnAsc >= 0) && lnAsc <= 99)
				{
					lnAsc = Strings.Asc(lcRet.Substring(lnI - 1, Math.Min(1, lcRet.Length - (lnI - 1)))[0]) - 32;
				}
				lnCheckSum += (lnAsc * lnI);
			}
			int aa = lnCheckSum % 103;
			aa += 32;
			string lcCheck = Strings.Chr(aa).ToString();
			lcRet = lcStart + lcRet + lcCheck + lcStop;
			StringBuilder tstCodigo = new StringBuilder();
			for (int tiFor = 1; tiFor <= lcRet.Length; tiFor++)
			{
				switch(Strings.Asc(lcRet.Substring(tiFor - 1, Math.Min(1, lcRet.Length - (tiFor - 1)))[0]))
				{
					case 32 :  //232 
						tstCodigo.Append(Strings.Chr(232).ToString()); 
						break;
					case 127 :  //192 
						tstCodigo.Append(Strings.Chr(192).ToString()); 
						break;
					case 128 :  //193 
						tstCodigo.Append(Strings.Chr(193).ToString()); 
						break;
					default:
						tstCodigo.Append(lcRet.Substring(tiFor - 1, Math.Min(1, lcRet.Length - (tiFor - 1)))); 
						break;
				}
			}

			return tstCodigo.ToString();
		}

		internal static string ObtenerEtiquetasDefecto()
		{
			string result = String.Empty;
			result = "0";
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select nnumeri1 from sconsglo where gconsglo='NROETIAD'", cnconexion);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				
				result = Convert.ToString(rr.Tables[0].Rows[0][0]);
			}
			return result;
		}
	}
}