using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.VB;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace EtiquetasF
{
	public partial class EF0000F2
         : Telerik.WinControls.UI.RadForm
    {


		string stImpresora2 = String.Empty;
		string stPortPrint2 = String.Empty;
		string stDriverPrint2 = String.Empty;
		string iOrientacion2 = String.Empty;
		int iA�oConsulta = 0;
		int iNumCopias = 0;
		string Mensaje = String.Empty;
		string stId = String.Empty;
		string stTipoEtiquetas = String.Empty;
		int lNumeroConsulta = 0;

// pario todo _x_5
  /*      AxCrystal.AxCrystalReport Listado_Etiquetas = null;
		PrinterHelper prImpre = null;
		//UPGRADE_ISSUE: (2068) Workspaces object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		Workspaces ws = null;
		//UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		Database bs = null;
		//UPGRADE_ISSUE: (2068) Recordset object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		Recordset Rs = null;*/

		string vstNombreMaquina = String.Empty;
		public EF0000F2()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{

			if (!ValidaDatos())
			{
				return;
			}

			iNumCopias = Convert.ToInt32(Double.Parse(tbCopias.Text));
			proEtiquetas();
			this.Close();

		}

		private void EF0000F2_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				tbCopias.Text = FuncEtiquetas.ObtenerEtiquetasDefecto();
			}
		}

        //UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
        private void EF0000F2_Load(Object eventSender, EventArgs eventArgs)
        {
            // PARIO TODO _X_5
            /*Listado_Etiquetas = this.CrystalReport1;*/

            Mensaje = "";

            // Relleno el combo de impresoras y cojo el nombre de la predeterminada

            stImpresora2 = PrinterHelper.Printer.DeviceName;
            stPortPrint2 = PrinterHelper.Printer.Port;
            stDriverPrint2 = PrinterHelper.Printer.DeviceName;
            iOrientacion2 = PrinterHelper.Printer.Orientation.ToString();

            foreach (PrinterHelper prImpre2 in PrinterHelper.Printers)
            {
                // PARIO TODO _X_5
                /*
				prImpre = prImpre2;
				cbbImpresoras.AddItem(PrinterHelper.Printer.DeviceName);
				prImpre = null;
			}




			Listado_Etiquetas = this.CrystalReport1;*/

                Conexion.Obtener_conexion instancia = new Conexion.Obtener_conexion();
                vstNombreMaquina = instancia.NonMaquina();
                instancia = null;

            }
        }

		public void establecevariables(SqlConnection paracn, string NombrePaciente, int A�ocita, int NumeroCita)
		{

			LbConsulta.Text = NombrePaciente;
			iA�oConsulta = A�ocita;
			lNumeroConsulta = NumeroCita;
			FuncEtiquetas.cnconexion = paracn;

		}

		public void proEtiquetas()
		{

			bool bImpresoraLaser = false;
			string stBaseDatos = String.Empty;

			string stPathR = String.Empty;
			string stPathB = String.Empty;

            //***
            try
            {
                //****

                this.Cursor = Cursors.WaitCursor;
                //PARIO TODO _X_5
                /*
                //Comprobamos el tipo de impresora por la que se imprimir�
                bImpresoraLaser = FuncEtiquetas.ComprobarSiImprimeConLaser();

                Listado_Etiquetas.PrinterName = stImpresora2;
                Listado_Etiquetas.PrinterPort = stPortPrint2;
                Listado_Etiquetas.PrinterDriver = stDriverPrint2;
                Listado_Etiquetas.Destination = Crystal.DestinationConstants.crptToPrinter;
                Listado_Etiquetas.DetailCopies = Convert.ToInt16(Conversion.Val(tbCopias.Text));


                stPathR = Path.GetDirectoryName(Application.ExecutablePath) + "\\rpt\\";
                stPathB = Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Consultas\\" + vstNombreMaquina + "Consulta.mdb";

                if (bImpresoraLaser)
                {
                    Listado_Etiquetas.ReportFileName = "" + stPathR + "CCI400r1L.rpt";
                }
                else
                {
                    Listado_Etiquetas.ReportFileName = "" + stPathR + "CCI400r1.rpt";
                }

                Listado_Etiquetas.WindowTitle = "Etiquetas";
                Listado_Etiquetas.WindowState = Crystal.WindowStateConstants.crptMaximized;



                stBaseDatos = stPathB;
                Listado_Etiquetas.set_DataFiles(0, stBaseDatos);


                proListadoEtiquetas(stBaseDatos);

                Listado_Etiquetas.SQLQuery = "select * from ETIQUETASCEX";

                Listado_Etiquetas.Action = 1;
                Listado_Etiquetas.PageCount();
                Listado_Etiquetas.PrinterStopPage = Listado_Etiquetas.PageCount();

                Listado_Etiquetas.SelectionFormula = "";
                Listado_Etiquetas.PrinterName = "";
                Listado_Etiquetas.PrinterPort = "";
                Listado_Etiquetas.PrinterDriver = "";
                Listado_Etiquetas.set_DataFiles(0, "");
                Listado_Etiquetas.DetailCopies = 1;
                Listado_Etiquetas.Reset();*/

                this.Cursor = Cursors.Default;
            }
            catch
            {

                //UPGRADE_WARNING: (1068) oClass.RespuestaMensaje() of type Variant is being forced to int. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                short tempRefParam = 1100;
                string[] tempRefParam2 = new string[] {"Etiquetas"};
				Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion,  Serrores.vAyuda, tempRefParam, FuncEtiquetas.cnconexion,  tempRefParam2));
               
			}

		}

		private void proListadoEtiquetas(string stNombreMDB)
		{
			object dbLangSpanish = null;
			object DBEngine = null;

			string sqlTemporal = String.Empty;
			bool vbBase = false;
			//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			DataSet RrDatos = null;

			string vstPathTemp = stNombreMDB;
			if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
			{
				vbBase = true;
			}

			if (!vbBase)
			{
                //UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                // PARIO TODO_X_5
                
                /*
                bs = (Database) DBEngine.Workspaces(0).CreateDatabase(vstPathTemp, dbLangSpanish);
				vbBase = true;
			}
			else
			{
				//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				bs = (Database) DBEngine.Workspaces(0).OpenDatabase(vstPathTemp);

				//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				foreach (TableDef tabla in bs.TableDefs)
				{
					//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					if (Convert.ToString(tabla.Name) == "ETIQUETASCEX")
					{
						//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						bs.Execute("DROP TABLE ETIQUETASCEX");
					}
				}
			}

			//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			bs.Execute("CREATE TABLE ETIQUETASCEX ( Historia varchar(15), HistoriaCB TEXT, " + 
			           "Episodio TEXT, Paciente TEXT, FechaNacimiento varchar(10), " + 
			           "Sexo varchar(20), CIP varchar(30), NAfiliacion varchar(20), CIAS varchar(20), Entidad varchar(50), " + 
			           "Telefono varchar(30), Servicio varchar(70), Agenda varchar(40)); ");

			try
			{

				//UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Rs = (Recordset) bs.OpenRecordset("select * from ETIQUETASCEX where 1=2");
				//'O.Frias - 09/06/2011
				//'En lugar de recuperar la inspecci�n del paciente la recupero de la cita.
				sqlTemporal = "SELECT ghistoria, dnombpac, dape1pac, dape2pac, " + 
				              "fnacipac, DPACIENT.itipsexo, DENTIPAC.nafiliac, gcodcias, dsocieda, " + 
				              "ntelefo1, ntelefo2, dnomserv, gagendas, " + 
				              "DINSPECC.dinspecc, DPACIENT.ntarjeta " + 
				              "from CCITPROG " + 
				              "INNER JOIN DSOCIEDA ON CCITPROG.gsocieda = DSOCIEDA.gsocieda " + 
				              "INNER JOIN DPACIENT ON CCITPROG.gidenpac = DPACIENT.gidenpac " + 
				              "LEFT JOIN DSERVICI ON CCITPROG.gservici = DSERVICI.gservici " + 
				              "LEFT JOIN HDOSSIER ON CCITPROG.gidenpac = HDOSSIER.gidenpac " + 
				              "LEFT JOIN DENTIPAC ON CCITPROG.gidenpac = DENTIPAC.gidenpac and CCITPROG.gsocieda = DENTIPAC.gsocieda " + 
				              "LEFT JOIN DINSPECC ON DINSPECC.ginspecc = CCITPROG.ginspecc " + 
				              "where CCITPROG.ganoregi = " + iA�oConsulta.ToString() + " and " + 
				              "CCITPROG.gnumregi = " + lNumeroConsulta.ToString();

				//'      "LEFT JOIN DINSPECC ON DINSPECC.ginspecc = DENTIPAC.ginspecc " & _
				//
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTemporal, FuncEtiquetas.cnconexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Rs.AddNew();

					// N�mero de historia

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ghistoria"]))
					{
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Historia"] = DBNull.Value;
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["HistoriaCB"] = DBNull.Value;
					}
					else
					{
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Historia). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Historia"] = (Recordset) StringsHelper.Format(RrDatos.Tables[0].Rows[0]["ghistoria"], "000000000");
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(HistoriaCB). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						//UPGRADE_WARNING: (1068) Rs() of type Recordset is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["HistoriaCB"] = (Recordset) FuncEtiquetas.fnCodigoBarras((string) Rs("Historia"));
					}

					// Episodio

					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Episodio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
					Rs["Episodio"] = (Recordset) (Conversion.Str(iA�oConsulta) + Conversion.Str(lNumeroConsulta));

					// Paciente

					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Paciente). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
					Rs["Paciente"] = (Recordset) (((Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dape1pac"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["dape1pac"]).Trim()) + " " + 
					                 ((Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dape2pac"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["dape2pac"]).Trim()) + ", " + 
					                 ((Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["dnombpac"]).Trim()));

					// Fecha de Nacimiento

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["fnacipac"]))
					{
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["FechaNacimiento"] = DBNull.Value;
					}
					else
					{
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(FechaNacimiento). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["FechaNacimiento"] = (Recordset) Convert.ToDateTime(RrDatos.Tables[0].Rows[0]["fnacipac"]).ToString("dd/MM/yyyy");
					}

					// Sexo

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToString(RrDatos.Tables[0].Rows[0]["itipsexo"]).Trim().ToUpper() == "I")
					{
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Sexo"] = (Recordset) "INDET.";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToString(RrDatos.Tables[0].Rows[0]["itipsexo"]).Trim().ToUpper() == "H")
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["Sexo"] = (Recordset) "HOMBRE";
						}
						else
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["Sexo"] = (Recordset) "MUJER";
						}
						//Rs("Sexo") = UCase(Trim(RrDatos("itipsexo")))
					}

					// CIP

					//OSCAR C 06/07/2005
					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(CIP). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
					Rs["CIP"] = (Recordset) (Convert.ToString(RrDatos.Tables[0].Rows[0]["ntarjeta"]).Trim().ToUpper() + "");
					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(NAfiliacion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
					Rs["NAfiliacion"] = (Recordset) (Convert.ToString(RrDatos.Tables[0].Rows[0]["nafiliac"]).Trim().ToUpper() + "");
					//--------


					// CIAS

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["gcodcias"]))
					{
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["CIAS"] = DBNull.Value;
					}
					else
					{
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(CIAS). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["CIAS"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["gcodcias"]).Trim().ToUpper();
					}

					// Entidad

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dsocieda"]))
					{
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Entidad"] = DBNull.Value;
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dinspecc"]))
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Entidad). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["Entidad"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["dsocieda"]).Trim().ToUpper();
						}
						else
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Entidad). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["Entidad"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["dinspecc"]).Trim().ToUpper();
						}
					}

					// Tel�fono/s

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ntelefo1"]))
					{
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Telefono"] = DBNull.Value;
					}
					else
					{
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Telefono). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Telefono"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["ntelefo1"]).Trim();
					}

					// Servicio

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dnomserv"]))
					{
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Servicio"] = DBNull.Value;
					}
					else
					{
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Servicio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Servicio"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["dnomserv"]).Trim();
					}

					// Agenda

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["gagendas"]))
					{
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Agenda"] = DBNull.Value;
					}
					else
					{
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Agenda). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Agenda"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["gagendas"]).Trim().ToUpper();
					}

					//UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Rs.Update();
				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();
				//UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Rs.Close();
				//UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				bs.Close();
				//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				DBEngine.Workspaces(0).Close();
				this.Cursor = Cursors.Default;
			}
			catch
			{

				//UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Rs.Close();
				//UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				bs.Close();
				//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				DBEngine.Workspaces(0).Close(); */
				this.Cursor = Cursors.Default;
			}

		}



		private void tbCopias_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;

			tbCopias.ReadOnly = !((KeyCode >= 96 && KeyCode <= 105) || (KeyCode >= 48 && KeyCode <= 57) || (KeyCode == 12) || (KeyCode == 46) || (KeyCode == 8)); //no lo son

		}


		private void cbbImpresoras_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{

			if (cbbImpresoras.SelectedIndex != -1 && cbbImpresoras.Text != "")
			{
				stImpresora2 = cbbImpresoras.GetItemData(cbbImpresoras.SelectedIndex);
				foreach (PrinterHelper prImpre2 in PrinterHelper.Printers)
				{
                    // PARIO TODO X_5
                 /*
                    prImpre = prImpre2;
					if (PrinterHelper.Printer.DeviceName == stImpresora2)
					{
						stPortPrint2 = PrinterHelper.Printer.Port;
						stDriverPrint2 = PrinterHelper.Printer.DeviceName;
						break;
					}
					prImpre = null;*/
				}

			}

		}


		private void cbbImpresoras_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private bool ValidaDatos()
		{
			bool result = false;
			result = true;
			// Comprobamos que se haya seleccionado alguna impresora

			if (cbbImpresoras.Text.Trim() == "")
			{
				short tempRefParam = 1040;
				string [] tempRefParam2 = new string []{"impresora"};
				Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam,  FuncEtiquetas.cnconexion,  tempRefParam2);
				cbbImpresoras.Focus();
				return false;
			}

            // Comprobamos que el n�mero de copias es correcto

            if (Conversion.Val(tbCopias.Text) == 0)
            {
                short tempRefParam3 = 1020;
                string[] tempRefParam4 = new string[] {lbCopias.Text, ">", "0"};
				Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3,  FuncEtiquetas.cnconexion,  tempRefParam4);
				tbCopias.Focus();
				return false;
			}
			return result;
		}
		private void EF0000F2_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}