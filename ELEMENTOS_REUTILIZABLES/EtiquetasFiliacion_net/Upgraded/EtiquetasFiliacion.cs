using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace EtiquetasF
{
	public class clsEtiquetasF
	{

		public object ProEtiquetaFiliacion(SqlConnection paraconexion, string stIdPaciente)
		{

			EF0000F1.DefInstance.establecevariables(paraconexion, stIdPaciente);

		
			EF0000F1.DefInstance.ShowDialog();

			return null;
		}

		public clsEtiquetasF()
		{
			Serrores.oClass = new Mensajes.ClassMensajes();
		}

		~clsEtiquetasF()
		{
			Serrores.oClass = null;
		}
        
		public void ProEtiquetaSimpleCita(SqlConnection paraconexion, string stNombrePaciente, int A�ocita, int NumeroCita)
		{

			EF0000F2.DefInstance.establecevariables(paraconexion, stNombrePaciente, A�ocita, NumeroCita);

			
			EF0000F2.DefInstance.ShowDialog();

		}
	}
}