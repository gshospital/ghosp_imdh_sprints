using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.VB;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace EtiquetasF
{
	public partial class EF0000F1
        : Telerik.WinControls.UI.RadForm
    {


		/*AxCrystal.AxCrystalReport Listado_Etiquetas = null;*/
		PrinterHelper prImpre = null;
		string stImpresora2 = String.Empty;
		string stPortPrint2 = String.Empty;
		string stDriverPrint2 = String.Empty;
		string iOrientacion2 = String.Empty;
		string Mensaje = String.Empty;
		string vstNombreMaquina = String.Empty;

		string stId = String.Empty;
		string stTipoEtiquetas = String.Empty;
        //UPGRADE_ISSUE: (2068) Workspaces object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
   // PARIO TODO _X_5
   /*
        Workspaces ws = null;
	
        //UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		Database bs = null;
		//UPGRADE_ISSUE: (2068) Recordset object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		Recordset Rs = null;*/
		public EF0000F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}





		private void cbbImpresoras_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{

			if (cbbImpresoras.SelectedIndex != -1 && cbbImpresoras.Text != "")
			{
				stImpresora2 = cbbImpresoras.GetItemData(cbbImpresoras.SelectedIndex);

              
				foreach (PrinterHelper prImpre2 in PrinterHelper.Printers)
				{
					prImpre = prImpre2;
					if (PrinterHelper.Printer.DeviceName == stImpresora2)
					{
						stPortPrint2 = PrinterHelper.Printer.Port;
						stDriverPrint2 = PrinterHelper.Printer.DeviceName;
						break;
					}
					prImpre = null;
				}

			}

		}

		private void cbbServicio_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			cbMedico.SelectedIndex = -1;

			if (cbbServicio.SelectedIndex != -1 && cbbServicio.Text != "")
			{
				CargarComboResponsable();
			}
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{

			this.Close();

		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{

			// Validamos los campos

			if (!ValidaDatos())
			{
				return;
			}


			proEtiquetas();
			this.Close();
		}


		private void EF0000F1_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				tbCopias.Text = FuncEtiquetas.ObtenerEtiquetasDefecto();
			}
		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void EF0000F1_Load(Object eventSender, EventArgs eventArgs)
		{

			// Cargamos las impresoras

			stImpresora2 = PrinterHelper.Printer.DeviceName;
			stPortPrint2 = PrinterHelper.Printer.Port;
			stDriverPrint2 = PrinterHelper.Printer.DeviceName;
			iOrientacion2 = PrinterHelper.Printer.Orientation.ToString();

			foreach (PrinterHelper prImpre2 in PrinterHelper.Printers)
			{
				prImpre = prImpre2;
				cbbImpresoras.Items.Add(PrinterHelper.Printer.DeviceName);
				prImpre = null;
			}


			CargarComboServicios();
            //PARIO TODO _x_5
			/*Listado_Etiquetas = this.CrystalReport1;*/

			string sql = "SELECT dnombpac, dape1pac, dape2pac, nafiliac FROM DPACIENT " + 
			             "WHERE GIDENPAC='" + stId + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, FuncEtiquetas.cnconexion);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			lbPaciente.Text = ((Convert.IsDBNull(rr.Tables[0].Rows[0]["dape1pac"])) ? "" : Convert.ToString(rr.Tables[0].Rows[0]["dape1pac"])) + " " + 
			                  ((Convert.IsDBNull(rr.Tables[0].Rows[0]["dape2pac"])) ? "" : Convert.ToString(rr.Tables[0].Rows[0]["dape2pac"])) + ", " + 
			                  ((Convert.IsDBNull(rr.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(rr.Tables[0].Rows[0]["dnombpac"]));
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rr.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rr.Close();

			Conexion.Obtener_conexion instancia = new Conexion.Obtener_conexion();
			vstNombreMaquina = instancia.NonMaquina();
			instancia = null;

		}




		private bool isInitializingComponent;
		private void optMedico_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.optMedico, eventSender);
				switch(Index)
				{
					case 0 : 
						tbMedico.Text = ""; 
						tbMedico.Enabled = false; 
						cbMedico.Enabled = true; 
						break;
					case 1 : 
						cbMedico.SelectedIndex = -1; 
						cbMedico.Enabled = false; 
						tbMedico.Enabled = true; 
						break;
				}
			}
		}

		private void tbCopias_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbCopias.SelectionStart = 0;
			tbCopias.SelectionLength = tbCopias.Text.Trim().Length;
		}

		private void tbCopias_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			if (!(KeyAscii == 8 || (KeyAscii >= 48 && KeyAscii <= 57)))
			{
				KeyAscii = 0;
			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private bool ValidaDatos()
		{
			bool result = false;
			result = true;
            // Comprobamos que se haya seleccionado alguna impresora

            if (cbbImpresoras.Text.Trim() == "")
            {
                short tempRefParam = 1040;
                string[] tempRefParam2 = new string[] {"impresora"};
				Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion, Serrores.vAyuda,  tempRefParam, FuncEtiquetas.cnconexion, tempRefParam2);
				cbbImpresoras.Focus();
				return false;
			}

			// Comprobamos que el n�mero de copias es correcto

			if (Conversion.Val(tbCopias.Text) == 0)
			{
				short tempRefParam3 = 1020;
				string [] tempRefParam4 = new string []{lbCopias.Text, ">", "0"};
				Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda, tempRefParam3,  FuncEtiquetas.cnconexion,  tempRefParam4);
				tbCopias.Focus();
				return false;
			}

			return true;

		}

		public void proEtiquetas()
		{

			bool bImpresoraLaser = false;
			string stBaseDatos = String.Empty;

			string stPathR = String.Empty;
			string stPathB = String.Empty;

            try
            {

                this.Cursor = Cursors.WaitCursor;

                //Comprobamos el tipo de impresora por la que se imprimir�
                bImpresoraLaser = FuncEtiquetas.ComprobarSiImprimeConLaser();

                stPathR = Path.GetDirectoryName(Application.ExecutablePath) + "\\rpt\\";
                stPathB = Path.GetDirectoryName(Application.ExecutablePath) + "\\..\\Consultas\\" + vstNombreMaquina + "Consulta.mdb";

                if (bImpresoraLaser)
                {

                    // PARIO TODO _X_5
                    /*	Listado_Etiquetas.ReportFileName = stPathR + "eti400r1L.rpt";
                    }
                    else
                    {
                        Listado_Etiquetas.ReportFileName = stPathR + "eti400r1.rpt";
                    }

                    Listado_Etiquetas.PrinterName = stImpresora2;
                    Listado_Etiquetas.PrinterPort = stPortPrint2;
                    Listado_Etiquetas.PrinterDriver = stDriverPrint2;
                    Listado_Etiquetas.Destination = Crystal.DestinationConstants.crptToPrinter;
                    Listado_Etiquetas.DetailCopies = Convert.ToInt16(Conversion.Val(tbCopias.Text));

                    Listado_Etiquetas.WindowTitle = "Etiquetas";
                    Listado_Etiquetas.WindowState = Crystal.WindowStateConstants.crptMaximized;

                    stBaseDatos = stPathB;
                    Listado_Etiquetas.set_DataFiles(0, stBaseDatos);

                    //MsgBox Listado_Etiquetas.ReportFileName & vbCrLf & _
                    //stBaseDatos
                    proListadoEtiquetas(stBaseDatos);

                    Listado_Etiquetas.set_Formulas(0, "Especialidad= \"" + cbbServicio.Text + "\"");
                    if (optMedico[0].IsChecked)
                    {
                        Listado_Etiquetas.set_Formulas(1, "Medico= \"" + cbMedico.Text + "\"");
                    }
                    else
                    {
                        Listado_Etiquetas.set_Formulas(1, "Medico= \"" + tbMedico.Text.Trim() + "\"");
                    }

                    Listado_Etiquetas.Action = 1;
                    Listado_Etiquetas.PageCount();
                    Listado_Etiquetas.PrinterStopPage = Listado_Etiquetas.PageCount();

                    Listado_Etiquetas.SelectionFormula = "";
                    Listado_Etiquetas.PrinterName = "";
                    Listado_Etiquetas.PrinterPort = "";
                    Listado_Etiquetas.PrinterDriver = "";
                    Listado_Etiquetas.set_DataFiles(0, "");
                    Listado_Etiquetas.DetailCopies = 1;
                    Listado_Etiquetas.Reset();*/

                    this.Cursor = Cursors.Default;
                }
            }
            catch (System.Exception excep)
            {

                //IResume = oClass.RespuestaMensaje(vNomAplicacion, vAyuda, 1100, cnconexion, "Etiquetas")
                RadMessageBox.Show(excep.Message, Application.ProductName);
           



            }

		}



		private void proListadoEtiquetas(string stNombreMDB)
		{
			object dbLangSpanish = null;
			object DBEngine = null;

			string sqlTemporal = String.Empty;
			bool vbBase = false;
			//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			DataSet RrDatos = null;

			string vstPathTemp = stNombreMDB;
			if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
			{
				vbBase = true;
			}

			if (!vbBase)
			{
                //pario TODO _X_5
                /*
				//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				bs = (Database) DBEngine.Workspaces(0).CreateDatabase(vstPathTemp, dbLangSpanish);
				vbBase = true;
			}
			else
			{
				//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				bs = (Database) DBEngine.Workspaces(0).OpenDatabase(vstPathTemp);

				//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				foreach (TableDef tabla in bs.TableDefs)
				{
					//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					if (Convert.ToString(tabla.Name) == "ETIQUETASCEX")
					{
						//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						bs.Execute("DROP TABLE ETIQUETASCEX");
						break;
					}
				}
			}

			//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			bs.Execute("CREATE TABLE ETIQUETASCEX ( Historia varchar(15), HistoriaCB TEXT, " + 
			           "Episodio TEXT, Paciente TEXT, FechaNacimiento varchar(10), " + 
			           "Sexo varchar(20), CIP varchar(30), NAfiliacion varchar(20), CIAS varchar(20), Entidad varchar(50), " + 
			           "Telefono varchar(30)); ");

			try
			{

                //UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
               

            Rs = (Recordset) bs.OpenRecordset("select * from ETIQUETASCEX where 1=2");

            sqlTemporal = "SELECT ghistoria, dnombpac, dape1pac, dape2pac, " + 
                          "fnacipac, DPACIENT.itipsexo, DENTIPAC.nafiliac, gcodcias, dsocieda, " + 
                          "ntelefo1, ntelefo2, " + 
                          "DINSPECC.dinspecc, DPACIENT.ntarjeta " + 
                          "from DPACIENT " + 
                          "INNER JOIN DSOCIEDA ON DPACIENT.gsocieda = DSOCIEDA.gsocieda " + 
                          "LEFT JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac " + 
                          "LEFT JOIN DENTIPAC ON DPACIENT.gidenpac = DENTIPAC.gidenpac and DPACIENT.gsocieda = DENTIPAC.gsocieda " + 
                          "LEFT JOIN DINSPECC ON DINSPECC.ginspecc = DENTIPAC.ginspecc " + 
                          "where DPACIENT.gidenpac ='" + stId + "'";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTemporal, FuncEtiquetas.cnconexion);
            RrDatos = new DataSet();
            tempAdapter.Fill(RrDatos);

            if (RrDatos.Tables[0].Rows.Count != 0)
            {


                //UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                Rs.AddNew();

                // N�mero de historia

                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ghistoria"]))
                {
                    //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["Historia"] = DBNull.Value;
                    //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["HistoriaCB"] = DBNull.Value;
                }
                else
                {
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Historia). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["Historia"] = (Recordset) StringsHelper.Format(RrDatos.Tables[0].Rows[0]["ghistoria"], "000000000");
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(HistoriaCB). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    //UPGRADE_WARNING: (1068) Rs() of type Recordset is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["HistoriaCB"] = (Recordset) FuncEtiquetas.fnCodigoBarras((string) Rs("Historia"));
                }


                // Paciente

                //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Paciente). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                Rs["Paciente"] = (Recordset) (((Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dape1pac"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["dape1pac"]).Trim()) + " " + 
                                 ((Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dape2pac"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["dape2pac"]).Trim()) + ", " + 
                                 ((Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["dnombpac"]).Trim()));

                // Fecha de Nacimiento

                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["fnacipac"]))
                {
                    //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["FechaNacimiento"] = DBNull.Value;
                }
                else
                {
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(FechaNacimiento). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["FechaNacimiento"] = (Recordset) Convert.ToDateTime(RrDatos.Tables[0].Rows[0]["fnacipac"]).ToString("dd/MM/yyyy");
                }

                // Sexo

                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                if (Convert.ToString(RrDatos.Tables[0].Rows[0]["itipsexo"]).Trim().ToUpper() == "I")
                {
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["Sexo"] = (Recordset) "INDET.";
                }
                else
                {
                    //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                    if (Convert.ToString(RrDatos.Tables[0].Rows[0]["itipsexo"]).Trim().ToUpper() == "H")
                    {
                        //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                        Rs["Sexo"] = (Recordset) "HOMBRE";
                    }
                    else
                    {
                        //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                        Rs["Sexo"] = (Recordset) "MUJER";
                    }
                    //Rs("Sexo") = UCase(Trim(RrDatos("itipsexo")))
                }

                //OSCAR C 06/07/2005
                //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(CIP). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                Rs["CIP"] = (Recordset) (Convert.ToString(RrDatos.Tables[0].Rows[0]["ntarjeta"]).Trim().ToUpper() + "");
                //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(NAfiliacion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                Rs["NAfiliacion"] = (Recordset) (Convert.ToString(RrDatos.Tables[0].Rows[0]["nafiliac"]).Trim().ToUpper() + "");
                //--------

                // CIAS

                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["gcodcias"]))
                {
                    //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["CIAS"] = DBNull.Value;
                }
                else
                {
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(CIAS). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["CIAS"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["gcodcias"]).Trim().ToUpper();
                }

                // Entidad

                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dsocieda"]))
                {
                    //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["Entidad"] = DBNull.Value;
                }
                else
                {
                    //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                    //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                    if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["dinspecc"]))
                    {
                        //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Entidad). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                        //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                        //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                        Rs["Entidad"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["dsocieda"]).Trim().ToUpper();
                    }
                    else
                    {
                        //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Entidad). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                        //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                        //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                        Rs["Entidad"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["dinspecc"]).Trim().ToUpper();
                    }
                }

                // Tel�fono/s

                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ntelefo1"]))
                {
                    //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["Telefono"] = DBNull.Value;
                }
                else
                {
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Telefono). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["Telefono"] = (Recordset) Convert.ToString(RrDatos.Tables[0].Rows[0]["ntelefo1"]).Trim();
                }


                //UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                Rs.Update();
            }

            //UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            RrDatos.Close();
            //UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            Rs.Close();
            //UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            bs.Close();
            //UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx

           DBEngine.Workspaces(0).Close();
            this.Cursor = Cursors.Default;
        }
        catch
        {

            //UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            Rs.Close();
            //UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            bs.Close();
            //UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            DBEngine.Workspaces(0).Close();*/
                this.Cursor = Cursors.Default;
			}

		}

		public object establecevariables(SqlConnection paracn, string stIdPaciente)
		{
			stId = stIdPaciente;
			FuncEtiquetas.cnconexion = paracn;
			return null;
		}

		private void CargarComboServicios()
		{
			string StSql = "SELECT DISTINCT gservici, dnomserv " + 
			               "FROM dserviciVA " + 
			               "WHERE iconsult='S' or icentral='S' " + 
			               "ORDER BY dnomserv";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, FuncEtiquetas.cnconexion);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			cbbServicio.Items.Clear();
			foreach (DataRow iteration_row in rr.Tables[0].Rows)
			{
				cbbServicio.Items.Add(Convert.ToString(iteration_row["dnomserv"]));
				cbbServicio.SetItemData(cbbServicio.GetNewIndex(), Convert.ToInt32(iteration_row["gservici"]));
			}
			
			rr.Close();
			cbbServicio.SelectedIndex = -1;
		}

		private void CargarComboResponsable()
		{
			string TxtMedico = String.Empty;
			cbMedico.Items.Clear();
			cbMedico.Items.Add("");
			cbMedico.SetItemData(cbMedico.GetNewIndex(), 0);

			string sql = "select D.gpersona, D.dnompers, D.dap1pers , D.dap2pers ";
			sql = sql + "From dperserv, dpersonaVA D, dservici ";
			sql = sql + "Where ";
			sql = sql + "dservici.gservici = dperserv.gservici and ";
			sql = sql + "dperserv.gpersona = D.gpersona and ";
			sql = sql + "dservici.gservici = " + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString() + " And ";
			sql = sql + "D.imedico = 'S' ";
			sql = sql + "order by D.dap1pers,D.dap2pers,D.dnompers";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, FuncEtiquetas.cnconexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
			{
				
				TxtMedico = Convert.ToString(iteration_row["dap1pers"]).Trim() + " " + 
				            ((Convert.IsDBNull(iteration_row["dap2pers"])) ? "" : Convert.ToString(iteration_row["dap2pers"]).Trim()) + " " + 
				            Convert.ToString(iteration_row["dnompers"]).Trim();
				cbMedico.Items.Add(TxtMedico.ToUpper());
				cbMedico.SetItemData(cbMedico.GetNewIndex(), Convert.ToInt32(iteration_row["gpersona"]));
			}
		
			RrSql.Close();

			cbMedico.SelectedIndex = 0;
		}
		private void EF0000F1_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}