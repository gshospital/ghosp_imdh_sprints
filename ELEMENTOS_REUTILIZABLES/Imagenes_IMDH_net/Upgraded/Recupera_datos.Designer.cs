using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ImagenesIMDH
{
	partial class Recupera_Dato
	{

		#region "Upgrade Support "
		private static Recupera_Dato m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Recupera_Dato DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Recupera_Dato();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbFiltro", "cbAyuda", "cbCancelar", "cbAceptar", "lsbDescripción", "lsbCódigo", "tbDescripción", "tbCódigo", "Neg_CURSIVA", "Tam_FUENTE", "lbAyuda", "lbDescripción", "lbCódigo", "lbSQL", "Label2", "Label1", "listBoxHelper1", "commandButtonHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
        public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbFiltro;
		public Telerik.WinControls.UI.RadButton cbAyuda;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadListControl lsbDescripción;
		public Telerik.WinControls.UI.RadListControl lsbCódigo;
		public Telerik.WinControls.UI.RadTextBoxControl tbDescripción;
		public Telerik.WinControls.UI.RadTextBoxControl tbCódigo;
		public Telerik.WinControls.UI.RadLabel Neg_CURSIVA;
		public Telerik.WinControls.UI.RadLabel Tam_FUENTE;
		public Telerik.WinControls.UI.RadLabel lbAyuda;
		public Telerik.WinControls.UI.RadLabel lbDescripción;
		public Telerik.WinControls.UI.RadLabel lbCódigo;
		public Telerik.WinControls.UI.RadLabel lbSQL;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		//private UpgradeHelpers.Gui.ListBoxHelper listBoxHelper1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Recupera_Dato));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.cbFiltro = new Telerik.WinControls.UI.RadButton();
			this.cbAyuda = new Telerik.WinControls.UI.RadButton();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.lsbDescripción = new Telerik.WinControls.UI.RadListControl();
			this.lsbCódigo = new Telerik.WinControls.UI.RadListControl();
			this.tbDescripción = new Telerik.WinControls.UI.RadTextBoxControl();
			this.tbCódigo = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Neg_CURSIVA = new Telerik.WinControls.UI.RadLabel();
			this.Tam_FUENTE = new Telerik.WinControls.UI.RadLabel();
			this.lbAyuda = new Telerik.WinControls.UI.RadLabel();
			this.lbDescripción = new Telerik.WinControls.UI.RadLabel();
			this.lbCódigo = new Telerik.WinControls.UI.RadLabel();
			this.lbSQL = new Telerik.WinControls.UI.RadLabel();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.SuspendLayout();
			// 
			// cbFiltro
			// 
			this.cbFiltro.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbFiltro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.cbFiltro.Image = (System.Drawing.Image) resources.GetObject("cbFiltro.Image");
            this.cbFiltro.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbFiltro.Location = new System.Drawing.Point(400, 32);
			this.cbFiltro.Name = "cbFiltro";
			this.cbFiltro.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbFiltro.Size = new System.Drawing.Size(25, 25);
			this.cbFiltro.TabIndex = 13;
			this.cbFiltro.Click += new System.EventHandler(this.cbFiltro_Click);
			// 
			// cbAyuda
			// 
			this.cbAyuda.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAyuda.Font = new System.Drawing.Font("Microsoft Sans Serif", 12f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.cbAyuda.Image = (System.Drawing.Image) resources.GetObject("cbAyuda.Image");
            this.cbAyuda.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.cbAyuda.Location = new System.Drawing.Point(400, 64);
			this.cbAyuda.Name = "cbAyuda";
			this.cbAyuda.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAyuda.Size = new System.Drawing.Size(25, 25);
			this.cbAyuda.TabIndex = 12;
			this.cbAyuda.Click += new System.EventHandler(this.cbAyuda_Click);
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.cbCancelar.Location = new System.Drawing.Point(352, 200);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 32);
			this.cbCancelar.TabIndex = 5;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.cbAceptar.Location = new System.Drawing.Point(264, 200);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 32);
			this.cbAceptar.TabIndex = 4;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// lsbDescripción
			// 
			this.lsbDescripción.SelectionMode = System.Windows.Forms.SelectionMode.One;
            this.lsbDescripción.Enabled = false;
			this.lsbDescripción.CausesValidation = true;
			//this.lsbDescripción.ColumnWidth = this.lsbDescripción.ClientSize.Width;
			this.lsbDescripción.Cursor = System.Windows.Forms.Cursors.Default;
			this.lsbDescripción.Enabled = true;
			this.lsbDescripción.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.lsbDescripción.Location = new System.Drawing.Point(168, 56);
			this.lsbDescripción.Name = "lsbDescripción";
			this.lsbDescripción.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lsbDescripción.Size = new System.Drawing.Size(217, 137);
            this.lsbDescripción.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
			this.lsbDescripción.TabIndex = 3;
			this.lsbDescripción.TabStop = true;
			this.lsbDescripción.Visible = true;
			this.lsbDescripción.DoubleClick += new System.EventHandler(this.lsbDescripción_DoubleClick);
			this.lsbDescripción.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lsbDescripción_KeyDown);
			this.lsbDescripción.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lsbDescripción_KeyPress);
			this.lsbDescripción.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lsbDescripción_MouseDown);
            //jproche
			//this.lsbDescripción.SelectedIndexChanged += new System.EventHandler(this.lsbDescripción_SelectedIndexChanged);
			// 
			// lsbCódigo
			// 
			this.lsbCódigo.SelectionMode = System.Windows.Forms.SelectionMode.One;
            this.lsbCódigo.CausesValidation = true;
            this.lsbCódigo.Cursor = System.Windows.Forms.Cursors.Default;
			this.lsbCódigo.Enabled = true;
			this.lsbCódigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.lsbCódigo.Location = new System.Drawing.Point(16, 56);
			this.lsbCódigo.Name = "lsbCódigo";
			this.lsbCódigo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lsbCódigo.Size = new System.Drawing.Size(137, 137);
			this.lsbCódigo.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
			this.lsbCódigo.TabIndex = 2;
			this.lsbCódigo.TabStop = true;
			this.lsbCódigo.Visible = true;
			this.lsbCódigo.DoubleClick += new System.EventHandler(this.lsbCódigo_DoubleClick);
			this.lsbCódigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lsbCódigo_KeyDown);
			this.lsbCódigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lsbCódigo_KeyPress);
			this.lsbCódigo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lsbCódigo_KeyUp);
			this.lsbCódigo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lsbCódigo_MouseDown);
            //jproche
            //this.lsbCódigo.SelectedIndexChanged += new System.EventHandler(this.lsbCódigo_SelectedIndexChanged);
			// 
			// tbDescripción
			// 
			this.tbDescripción.AcceptsReturn = true;
			this.tbDescripción.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbDescripción.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.tbDescripción.Location = new System.Drawing.Point(168, 32);
			this.tbDescripción.MaxLength = 0;
			this.tbDescripción.Name = "tbDescripción";
			this.tbDescripción.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbDescripción.Size = new System.Drawing.Size(217, 19);
			this.tbDescripción.TabIndex = 1;
			this.tbDescripción.Enter += new System.EventHandler(this.tbDescripción_Enter);
			this.tbDescripción.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDescripción_KeyPress);
			this.tbDescripción.TextChanged += new System.EventHandler(this.tbDescripción_TextChanged);
			// 
			// tbCódigo
			// 
			this.tbCódigo.AcceptsReturn = true;
			this.tbCódigo.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbCódigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.tbCódigo.Location = new System.Drawing.Point(16, 32);
			this.tbCódigo.MaxLength = 0;
			this.tbCódigo.Name = "tbCódigo";
			this.tbCódigo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbCódigo.Size = new System.Drawing.Size(137, 19);
			this.tbCódigo.TabIndex = 0;
			this.tbCódigo.Enter += new System.EventHandler(this.tbCódigo_Enter);
			this.tbCódigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCódigo_KeyPress);
			this.tbCódigo.TextChanged += new System.EventHandler(this.tbCódigo_TextChanged);
			// 
			// Neg_CURSIVA
			// 
			this.Neg_CURSIVA.Cursor = System.Windows.Forms.Cursors.Default;
			this.Neg_CURSIVA.Location = new System.Drawing.Point(80, 168);
			this.Neg_CURSIVA.Name = "Neg_CURSIVA";
			this.Neg_CURSIVA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Neg_CURSIVA.Size = new System.Drawing.Size(25, 17);
			this.Neg_CURSIVA.TabIndex = 15;
			this.Neg_CURSIVA.Text = "Label4";
			this.Neg_CURSIVA.Visible = false;
			// 
			// Tam_FUENTE
			// 
			this.Tam_FUENTE.Cursor = System.Windows.Forms.Cursors.Default;
			this.Tam_FUENTE.Location = new System.Drawing.Point(48, 168);
			this.Tam_FUENTE.Name = "Tam_FUENTE";
			this.Tam_FUENTE.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Tam_FUENTE.Size = new System.Drawing.Size(25, 17);
			this.Tam_FUENTE.TabIndex = 14;
			this.Tam_FUENTE.Text = "Label3";
			this.Tam_FUENTE.Visible = false;
			// 
			// lbAyuda
			// 
			this.lbAyuda.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbAyuda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.lbAyuda.Location = new System.Drawing.Point(400, 136);
			this.lbAyuda.Name = "lbAyuda";
			this.lbAyuda.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbAyuda.Size = new System.Drawing.Size(33, 9);
			this.lbAyuda.TabIndex = 11;
			this.lbAyuda.Text = "Label3";
			this.lbAyuda.Visible = false;
			// 
			// lbDescripción
			// 
			this.lbDescripción.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbDescripción.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.lbDescripción.Location = new System.Drawing.Point(400, 168);
			this.lbDescripción.Name = "lbDescripción";
			this.lbDescripción.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbDescripción.Size = new System.Drawing.Size(41, 9);
			this.lbDescripción.TabIndex = 10;
			this.lbDescripción.Text = "Label5";
			this.lbDescripción.Visible = false;
			// 
			// lbCódigo
			// 
			this.lbCódigo.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbCódigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.lbCódigo.Location = new System.Drawing.Point(400, 160);
			this.lbCódigo.Name = "lbCódigo";
			this.lbCódigo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbCódigo.Size = new System.Drawing.Size(33, 9);
			this.lbCódigo.TabIndex = 9;
			this.lbCódigo.Text = "Label4";
			this.lbCódigo.Visible = false;
			// 
			// lbSQL
			// 
			this.lbSQL.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbSQL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.lbSQL.Location = new System.Drawing.Point(400, 144);
			this.lbSQL.Name = "lbSQL";
			this.lbSQL.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbSQL.Size = new System.Drawing.Size(33, 17);
			this.lbSQL.TabIndex = 8;
			this.lbSQL.Text = "Label3";
			this.lbSQL.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.Label2.Location = new System.Drawing.Point(16, 16);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(105, 17);
			this.Label2.TabIndex = 7;
			this.Label2.Text = "Código";
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.Label1.Location = new System.Drawing.Point(168, 16);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(105, 17);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "Descripción";
			// 
			// Recupera_Dato
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(0, 0);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(440, 241);
			this.ControlBox = false;
			this.Controls.Add(this.cbFiltro);
			this.Controls.Add(this.cbAyuda);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.lsbDescripción);
			this.Controls.Add(this.lsbCódigo);
			this.Controls.Add(this.tbDescripción);
			this.Controls.Add(this.tbCódigo);
			this.Controls.Add(this.Neg_CURSIVA);
			this.Controls.Add(this.Tam_FUENTE);
			this.Controls.Add(this.lbAyuda);
			this.Controls.Add(this.lbDescripción);
			this.Controls.Add(this.lbCódigo);
			this.Controls.Add(this.lbSQL);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 15);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Recupera_Dato";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "                                                                                                             ";
            this.ToolTipMain.SetToolTip(this.cbFiltro, "Filtro combinado Código y Descripción");
			this.ToolTipMain.SetToolTip(this.cbAyuda, "Ayuda ");
			this.ToolTipMain.SetToolTip(this.cbCancelar, "Salir Sin Cambios");
			this.ToolTipMain.SetToolTip(this.cbAceptar, "Aceptar Código");
			this.ToolTipMain.SetToolTip(this.lsbDescripción, "Lista de Descripciones");
			this.ToolTipMain.SetToolTip(this.lsbCódigo, "Lista de Códigos ");
			this.Activated += new System.EventHandler(this.Recupera_Dato_Activated);
			this.Closed += new System.EventHandler(this.Recupera_Dato_Closed);
			this.Load += new System.EventHandler(this.Recupera_Dato_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.Recupera_Dato_Paint);
			this.ResumeLayout(false);
		}
		#endregion
	}
}