using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ImagenesIMDH
{
	internal static class Module1
	{

		public static int HAY_ESCANER = 0;
		public static string HAY_Clasificacion = String.Empty;
		public static object HAY_Imagen = null;
		public static SqlConnection BaseRDO = null;
		public static string Referen = String.Empty;

		//Variables para la llamada de IMDH
		public static string stTipoLlamada = String.Empty; //
		public static bool fModoLectura = false; //si es true se presenta la pantalla
		//en modo consulta
		public static string stIdImagen = String.Empty; //identificador de fichero
		public static string stFicheroTemporal = String.Empty; //fichero temporal donde se guarda la imagen
		public static string stVueltaIMDH = String.Empty; //variable para guardar la vuelta de la llamada de IMDH
		public static string vstNomAplicacion = String.Empty; //variable donde va el nombre de la aplicacion para los mensajes
		public static string gUsuario = String.Empty; //usuario conectado


		internal static string Archivo()
		{
			string result = String.Empty;
			try
			{
				string Nombre_Archivo = String.Empty;
				int N�mero = 0;

				Directory.CreateDirectory("c:\\Temp");
				Nombre_Archivo = "c:\\Temp\\001bprb";
				N�mero = 0;
				while (FileSystem.Dir(Nombre_Archivo + N�mero.ToString(), FileAttribute.Normal) != "")
				{
					N�mero++;
				}

				result = Nombre_Archivo + N�mero.ToString();
				FileSystem.FileOpen(1, result, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);
				FileSystem.FileClose(1);

				return result;
			}
			catch
			{
				return result;
			}
		}
		internal static string ObtenerNombreAplicacion()
		{

			string result = String.Empty;
			string sql = "select valfanu1 from sconsglo where gconsglo='NOMAPLIC'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BaseRDO);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim();
			}
			else
			{
				result = "";
			}
			RrSql.Close();
			return result;
		}
	}
}