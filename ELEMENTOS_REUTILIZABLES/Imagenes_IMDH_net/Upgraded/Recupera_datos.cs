using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using UpgradeHelpers.Gui;

namespace ImagenesIMDH
{
	internal partial class Recupera_Dato
		: Telerik.WinControls.UI.RadForm
    {

		int ControlCod = 0;
		int ControlDes = 0;
		int[] MATRIZ_INDICES = null;
		bool Contt = false;
		public Recupera_Dato()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void Rellenar_Listas(ref string cadena, ref string filtro_cod, ref string filtro_desc)
		{
			string cadena2 = String.Empty;
			string palabra = String.Empty;
			string Cadena_SQL = String.Empty;
			int INDICE = 0; // CON ESTE PARÁMETRO CONTROLARÉ LAS VECES QUE SE ME PRODUCEN ERRORES
            bool control = false;
            do
            {



                try
                {

                    DataSet tablardo = null;
                    int i_f = 0;
                    while (filtro_cod.IndexOf('*') >= 0)
                    {
                        filtro_cod = StringsHelper.MidAssignment(filtro_cod, filtro_cod.IndexOf('*') + 1, 1, "%");
                    }

                    while (filtro_desc.IndexOf('*') >= 0)
                    {
                        filtro_desc = StringsHelper.MidAssignment(filtro_desc, filtro_desc.IndexOf('*') + 1, 1, "%");
                    }
                    if (filtro_desc == "")
                    {
                        filtro_desc = "%";
                    }
                    if (filtro_cod == "")
                    {
                        filtro_cod = "%";
                    }

                    palabra = ((cadena.IndexOf("WHERE") + 1) == 0) ? " WHERE " : " AND ";
                    cadena2 = palabra + lbCódigo.Text + " LIKE '" + filtro_cod + "' AND " + lbDescripción.Text + " LIKE '" + filtro_desc + "' ORDER BY " + lbCódigo.Text;
                    Cadena_SQL = cadena + cadena2;
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(Cadena_SQL, Recupera_Datos_Módulo.Connexión_RDO_Recupera_Datos);
                    tablardo = new DataSet();
                    tempAdapter.Fill(tablardo);
                    INDICE = 0;
                    lsbCódigo.Items.Clear();
                    lsbDescripción.Items.Clear();
                    i_f = 0;
                    //RELLENAR LAS LISTAS
                    foreach (DataRow iteration_row in tablardo.Tables[0].Rows)
                    {

                        if (Convert.IsDBNull(iteration_row[0]))
                        {
                            lsbCódigo.Items.Add(new RadListDataItem(" ", i_f));
                        }
                        else
                        {
                            lsbCódigo.Items.Add(new RadListDataItem(Convert.ToString(iteration_row[0]).Trim(), i_f));
                        }

                        if (Convert.IsDBNull(iteration_row[1]))
                        {
                            lsbDescripción.Items.Add(new RadListDataItem(" ", i_f));
                        }
                        else
                        {
                            lsbDescripción.Items.Add(new RadListDataItem(Convert.ToString(iteration_row[1]).Trim(), i_f));

                        }
                        i_f++;

                    }
                    tablardo.Close();

                    // LA LISTA DE CODIGOS YA ESTÁ ORDENADA
                    // LA LISTA DE DESCRIPCIONES SE ORDENA AL ACABAR DE INTRODUCIR DATOS
                    // VOY A LLENAR LA MATRIZ CON LOS INDICES DE DESCRIPCIÓN QUE COINCIDAN CON CODIGO

                    if (lsbDescripción.Items.Count == 0)
                    {

                        if (MATRIZ_INDICES != null)
                        {
                            Array.Clear(MATRIZ_INDICES, 0, MATRIZ_INDICES.Length);
                        }
                        tbCódigo.Text = "";
                        tbDescripción.Text = "";
                        tbDescripción.Tag = "0";
                        return;
                    } //Me.Hide: Exit Sub
                    MATRIZ_INDICES = new int[lsbDescripción.Items.Count];
                    //EL INDICE DE LA MATRIZ CORRESPONDE CON LA POSICIÓN DE INDICE DE LA
                    //LISTA DE CODIGOS, EL VALOR QUE TIENE ES EL INDICE DE LAS DESCRIPCIONES
                    //EN SU LISTA
                    for (i_f = 0; i_f <= lsbDescripción.Items.Count - 1; i_f++)
                    {
                        MATRIZ_INDICES[Convert.ToInt32(lsbDescripción.Items[i_f])] = i_f;
                    }
                    // VAMOS A PONER LOS VALORES

                    tbCódigo.Text = lsbCódigo.Items[0].Text;
                    lsbCódigo.SelectedIndex = 0;
                    lsbDescripción.SelectedIndex = MATRIZ_INDICES[lsbCódigo.SelectedIndex];
                    tbDescripción.Text = lsbDescripción.Items[lsbDescripción.SelectedIndex].Text;
                    control = true;
                }
                catch
                {
                    // QUE VENGA AQUÍ SIGNIFICA UN ERROR DE BASE DE DATOS
                    if (Conversion.ErrorToString().IndexOf("Incorrect syntax near ','.") >= 0 || Conversion.ErrorToString().StartsWith("S0002"))
                    {
                        RadMessageBox.Show("Se ha producido un Error en las Clasificaciones", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                        if (MATRIZ_INDICES != null)
                        {
                            Array.Clear(MATRIZ_INDICES, 0, MATRIZ_INDICES.Length);
                        }
                        tbCódigo.Text = "";
                        tbDescripción.Text = "";
                        tbDescripción.Tag = "0";
                        this.Hide();
                        return;
                    }

                    if (Conversion.ErrorToString().IndexOf(" CONVERT ") >= 0)
                    {

                        // EN PRINCIPIO ORACLE HACE LAS CONVERSIONES AUTOMÁTICAMENTE
                        // EN CASO CONTRARIO HABRÁ QUE PONER UNA FUNCIÓN AQUÍ
                        INDICE++;
                        //en esta condición que sigue el valor de índice
                        //no debe superar el nº de casos del select
                        if (INDICE > 1)
                        {
                            RadMessageBox.Show("Se ha producido un Error en la Base de Datos", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                            return;
                        }
                        // ESTE ERROR SE PRODUCE POR UN ERROR DE CONVERSIÓN, HAY CÓDIGOS NUMÉRICOS EN SQL
                        // GENERO UNA NUEVA CADENA CON LA FUNCIÓN DE CONVERSIÓN
                        switch (INDICE)
                        {
                            case 1:  // CASO PARA SQL SERVER 
                                     //cadena2 = palabra + "CONVERT(VARCHAR," + lbCódigo.Caption + ") LIKE '" + filtro_cod + "' AND " + lbDescripción.Caption + " LIKE '" + filtro_desc + "' ORDER BY " & lbCódigo.Caption 
                                string tempRefParam = cadena + cadena2;
                                Cadena_SQL = Recupera_Datos_Módulo.CAMBIO_SQL(ref tempRefParam);
                                // Case Else    ' PARA OTROS TIPOS DE BASES 
                                break;
                        }                        
                    }
                    else
                    {
                        //CUALQUIER OTRO ERROR DE BASE DE DATOS CERRARÉ LA FUNCIÓN
                        //UPGRADE_NOTE: (1061) Erase was upgraded to System.Array.Clear. More Information: http://www.vbtonet.com/ewis/ewi1061.aspx
                        if (MATRIZ_INDICES != null)
                        {
                            Array.Clear(MATRIZ_INDICES, 0, MATRIZ_INDICES.Length);
                        }
                        tbCódigo.Text = "";
                        tbDescripción.Text = "";
                        tbDescripción.Tag = "0";
                        this.Hide();
                        return;
                    }
                }
            } while (!control);

		}
		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int i_f = 0;

			if (lsbCódigo.Items.Count != 0)
			{
				tbCódigo.Text =  lsbCódigo.Items[lsbCódigo.SelectedIndex].Text;
				tbDescripción.Text = lsbDescripción.Items[lsbDescripción.SelectedIndex].Text;
				tbDescripción.Tag = "1";
			}
			else
			{
				tbCódigo.Text = "";
				tbDescripción.Text = "";
				tbDescripción.Tag = "0";
			}
			Recupera_Dato.DefInstance.Hide();
		}


		private void cbAyuda_Click(Object eventSender, EventArgs eventArgs)
		{
			RadMessageBox.Show(lbAyuda.Text, "Ayuda", MessageBoxButtons.OK, RadMessageIcon.Question);
		}

		private void cbFiltro_Click(Object eventSender, EventArgs eventArgs)
		{

			string tempRefParam = lbSQL.Text.Trim();
			string tempRefParam2 = tbCódigo.Text.Trim();
			string tempRefParam3 = tbDescripción.Text.Trim();
			Rellenar_Listas(ref tempRefParam, ref tempRefParam2, ref tempRefParam3);

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			tbDescripción.Tag = "2";
			tbDescripción.Text = "";
			tbCódigo.Text = "";
			this.Hide();
		}


		private void Recupera_Dato_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				if (Contt)
				{
					return;
				}
				string tempRefParam = lbSQL.Text.Trim();
				string tempRefParam2 = Convert.ToString(tbCódigo.Tag).Trim();
				string tempRefParam3 = Convert.ToString(tbDescripción.Tag).Trim();
				Rellenar_Listas(ref tempRefParam, ref tempRefParam2, ref tempRefParam3);
				Contt = true;
			}
		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void Recupera_Dato_Load(Object eventSender, EventArgs eventArgs)
		{
			ControlCod = 0;
			ControlDes = 0;
			Contt = false;
		}




		private void Recupera_Dato_Paint(Object eventSender, PaintEventArgs eventArgs)
		{
			// COMPROBAR QUE SE HA HECHO ESTE EVENTO POR MODIFICAR EL CAPTION DEL FORMULARIO
			string fuente = Tam_FUENTE.Text;
			double TAMAÑO = Conversion.Val(Convert.ToString(Tam_FUENTE.Tag));
			string CURSIVA = Neg_CURSIVA.Text;
			string NEGRITA = Convert.ToString(Neg_CURSIVA.Tag);
			string TITULO = Recupera_Datos_Módulo.CARGAR_FUENTE_CAPTION(this.Text);
			//COMPROBAR SI SE HA CAMBIADO O NO
			if (fuente != Tam_FUENTE.Text || TAMAÑO != Conversion.Val(Convert.ToString(Tam_FUENTE.Tag)) || CURSIVA != Neg_CURSIVA.Text || NEGRITA != Convert.ToString(Neg_CURSIVA.Tag))
			{
				//SI HA CAMBIADO ALGO MODIFICO EL TÍTULO
				this.Text = TITULO;
			}
		}

		private void lsbCódigo_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			//si pulso
			if (ControlCod == 1 && ControlDes == 0)
			{
				ErrorHandlingHelper.ResumeNext(
					() => { tbCódigo.Text = lsbCódigo.Items[lsbCódigo.SelectedIndex].Text;}, 
					() => { tbDescripción.Text = lsbDescripción.Items[MATRIZ_INDICES[lsbCódigo.SelectedIndex]].Text;}, 
					() => {lsbDescripción.SelectedIndex = MATRIZ_INDICES[lsbCódigo.SelectedIndex];},
                    () => {lsbCódigo.Focus();});
			}
		}

		private void lsbCódigo_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			cbAceptar_Click(cbAceptar, new EventArgs());
		}

		private void lsbCódigo_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (KeyCode >= 33 && KeyCode <= 40)
			{
				ControlCod = 1;
				ControlDes = 0;
			}
		}



		private void lsbCódigo_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 13)
			{
				cbAceptar_Click(cbAceptar, new EventArgs());
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void lsbCódigo_KeyUp(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			//On Error Resume Next
			//si pulso
			//If ControlCod = 1 And ControlDes = 0 Then
			//    tbCódigo.Text = Trim(lsbCódigo.List(lsbCódigo.ListIndex))
			//    lsbDescripción.SetFocus
			//    tbDescripción.Text = Trim(lsbCódigo_Descripción.List(lsbCódigo.ItemData(lsbCódigo.ListIndex)))
			//    SendKeys Trim(lsbCódigo_Descripción.List(lsbCódigo.ItemData(lsbCódigo.ListIndex))), True
			//    lsbCódigo.SetFocus
			//End If

		}

		private void lsbCódigo_MouseDown(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float X = (float) (eventArgs.X * 15);
			float Y = (float) (eventArgs.Y * 15);
			ControlDes = 0;
			ControlCod = 1;
		}


		private void lsbDescripción_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{

			if (ControlDes == 1 && ControlCod == 0)
			{
				ErrorHandlingHelper.ResumeNext(
					() => {tbDescripción.Text = lsbDescripción.Items[lsbDescripción.SelectedIndex].Text;}, 
					() => {tbCódigo.Text = lsbCódigo.Items[lsbDescripción.SelectedIndex].Text;}, 
					() => {lsbCódigo.SelectedIndex = lsbDescripción.SelectedIndex;}, 
					() => {lsbDescripción.Focus();});
			}
		}

		private void lsbDescripción_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			// con el click esto no se ejecuta nunca
			cbAceptar_Click(cbAceptar, new EventArgs());
		}

		private void lsbDescripción_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (KeyCode >= 33 && KeyCode <= 40)
			{
				ControlCod = 0;
				ControlDes = 1;
			}
		}

		private void lsbDescripción_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 13)
			{
				cbAceptar_Click(cbAceptar, new EventArgs());
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}



		private void lsbDescripción_MouseDown(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float X = (float) (eventArgs.X * 15);
			float Y = (float) (eventArgs.Y * 15);
			ControlDes = 1;
			ControlCod = 0;
		}


		private void tbCódigo_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			string filtr2 = String.Empty;
			string xx = String.Empty;
			if (ControlCod == 2)
			{
				ControlDes = 0;

				if (lsbCódigo.Items.Count != 0)
				{
					filtr2 = tbCódigo.Text.Trim();
					while (filtr2.IndexOf('%') >= 0)
					{
						filtr2 = StringsHelper.MidAssignment(filtr2, filtr2.IndexOf('%') + 1, 1, "*");
					}
					xx = "NO";
					for (int f = 0; f <= lsbCódigo.Items.Count - 1; f++)
					{
						if (String.CompareOrdinal(filtr2.ToUpper(), lsbCódigo.Items[f].Text.Trim().Substring(0, Math.Min(filtr2.Length, lsbCódigo.Items[f].Text.Trim().Length)).ToUpper()) <= 0)
						{
							if (f == 0)
							{
								lsbCódigo.SelectedIndex = 0;
								xx = "SI";
								break;
							}
							else
							{
								lsbCódigo.SelectedIndex = f;
								xx = "SI";
								break;
							}
						}
					}
					if (xx == "NO")
					{
						lsbCódigo.SelectedIndex = lsbCódigo.Items.Count - 1;
					}
                    tbDescripción.Text = lsbDescripción.Items[MATRIZ_INDICES[lsbCódigo.SelectedIndex]].Text;
					lsbDescripción.SelectedIndex = MATRIZ_INDICES[lsbCódigo.SelectedIndex];
					tbCódigo.Focus();
				}

			}

		}

		private void tbCódigo_Enter(Object eventSender, EventArgs eventArgs)
		{

			if (ControlCod != 2)
			{
				tbCódigo.SelectionStart = 0;
				tbCódigo.SelectionLength = Strings.Len(tbCódigo.Text);
			}
		}

		private void tbCódigo_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 13)
			{
				//acepto lo elegido
				cbAceptar_Click(cbAceptar, new EventArgs());
			}
			else
			{

				ControlCod = 2;
				ControlDes = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDescripción_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			string filtr2 = String.Empty;
			string xx = String.Empty;
			if (ControlDes == 2)
			{
				ControlCod = 0;

				if (lsbDescripción.Items.Count != 0)
				{
					filtr2 = tbDescripción.Text.Trim();
					while (filtr2.IndexOf('%') >= 0)
					{
						filtr2 = StringsHelper.MidAssignment(filtr2, filtr2.IndexOf('%') + 1, 1, "*");
					}
					if (filtr2 == "")
					{
                        lsbDescripción.SelectedIndex = 0;
						return;
					}
					xx = "NO";
					for (int f = 0; f <= lsbDescripción.Items.Count - 1; f++)
					{
						if (String.CompareOrdinal(filtr2.ToUpper(), lsbDescripción.Items[f].Text.Trim().Substring(0, Math.Min(filtr2.Length, lsbDescripción.Items[f].Text.Trim().Length)).ToUpper()) <= 0)
						{
							if (f == 0)
							{
								lsbDescripción.SelectedIndex = 0;
								xx = "SI";
								break;
							}
							else
							{
								lsbDescripción.SelectedIndex = f;
								xx = "SI";
								break;
							}
						}
					}
					if (xx == "NO")
					{
                       lsbDescripción.SelectedIndex = lsbDescripción.Items.Count - 1;
					}
					tbCódigo.Text = lsbCódigo.Items[lsbDescripción.SelectedIndex].Text;
                    lsbCódigo.SelectedIndex = lsbDescripción.SelectedIndex;
					tbDescripción.Focus();
				}
			}

		}

		private void tbDescripción_Enter(Object eventSender, EventArgs eventArgs)
		{

			if (ControlDes != 2)
			{
				tbDescripción.SelectionStart = 0;
				tbDescripción.SelectionLength = Strings.Len(tbDescripción.Text);
			}
		}

		private void tbDescripción_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 13)
			{
				//acepto lo elegido
				cbAceptar_Click(cbAceptar, new EventArgs());
			}
			else
			{
				ControlDes = 2;
				ControlCod = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		private void Recupera_Dato_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}