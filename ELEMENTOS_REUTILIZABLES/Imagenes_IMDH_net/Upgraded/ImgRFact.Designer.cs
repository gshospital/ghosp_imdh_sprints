using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ImagenesIMDH
{
	partial class ImgRFact
	{

		#region "Upgrade Support "
		private static ImgRFact m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static ImgRFact DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new ImgRFact();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

        #endregion
        #region "Windows Form Designer generated code "
        //private string[] visualControls = new string[]{"components", "ToolTipMain", "_tbImagenes_Button1", "NUEVO", "ELIM", "_tbImagenes_Button4", "GRAB", "CANC", "_tbImagenes_Button7", "IMPR", "_tbImagenes_Button9", "CORTAR", "COPIAR", "PEGAR", "IMAGENNUEVA", "_tbImagenes_Button14", "PINTAR", "HERRAMIENTAS", "_tbImagenes_Button17", "SELECC", "ARRAS", "_tbImagenes_Button20", "ZOOMIN", "ZOOMOUT", "ACERC", "_tbImagenes_Button24", "IZQ", "DER", "_tbImagenes_Button27", "_tbImagenes_Button28", "VOLVER", "_tbImagenes_Button30", "SALIR", "_tbImagenes_Button32", "_tbImagenes_Button33", "tbImagenes", "_Toolbar2_Button1", "CANC", "_Toolbar2_Button3", "IMPR", "_Toolbar2_Button5", "ZOOMIN", "ZOOMOUT", "ACERC", "_Toolbar2_Button9", "IZQ", "DER", "_Toolbar2_Button12", "SALIR", "_Toolbar2_Button14", "_Toolbar2_Button15", "Toolbar2", "_Toolbar1_Button1", "GRAB", "CANC", "_Toolbar1_Button4", "ESCANER", "_Toolbar1_Button6", "ESCAN", "ABRIR", "BUSCAR", "_Toolbar1_Button10", "IZQ", "DER", "Toolbar1", "ImgScan1", "Image1", "CommonDialog1Open", "CommonDialog1Print", "scbPaginas", "iEImgFact", "lbNumeracion", "lbPaginas", "lbZoom", "frmImagenes", "Image2", "ilImagenes"};
        //private string[] visualControls = new string[] { "components", "ToolTipMain", "_tbImagenes_Button1", "NUEVO", "ELIM", "_tbImagenes_Button4", "GRAB", "CANC", "_tbImagenes_Button7", "IMPR", "_tbImagenes_Button9", "CORTAR", "COPIAR", "PEGAR", "IMAGENNUEVA", "_tbImagenes_Button14", "PINTAR", "HERRAMIENTAS", "_tbImagenes_Button17", "SELECC", "ARRAS", "_tbImagenes_Button20", "ZOOMIN", "ZOOMOUT", "ACERC", "_tbImagenes_Button24", "IZQ", "DER", "_tbImagenes_Button27", "_tbImagenes_Button28", "VOLVER", "_tbImagenes_Button30", "SALIR", "_tbImagenes_Button32", "_tbImagenes_Button33", "tbImagenes", "_Toolbar2_Button1", "_Toolbar2_Button3", "IMPR", "_Toolbar2_Button5", "ZOOMIN", "ZOOMOUT", "ACERC", "_Toolbar2_Button9", "IZQ", "DER", "_Toolbar2_Button12", "SALIR", "_Toolbar2_Button14", "_Toolbar2_Button15", "Toolbar2", "_Toolbar1_Button1", CANC", "_Toolbar1_Button4", "ESCANER", "_Toolbar1_Button6", "ESCAN", "ABRIR", "BUSCAR", "_Toolbar1_Button10", "IZQ", "DER", "Toolbar1", "ImgScan1", "Image1", "CommonDialog1Open", "CommonDialog1Print", "scbPaginas", "iEImgFact", "lbNumeracion", "lbPaginas", "lbZoom", "frmImagenes", "Image2", "ilImagenes" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button1;
		public System.Windows.Forms.ToolStripButton NUEVO;
		public System.Windows.Forms.ToolStripButton ELIM;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button4;
		public System.Windows.Forms.ToolStripButton GRAB;
		public System.Windows.Forms.ToolStripButton CANC;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button7;
		public System.Windows.Forms.ToolStripButton IMPR;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button9;
		public System.Windows.Forms.ToolStripButton CORTAR;
		public System.Windows.Forms.ToolStripButton COPIAR;
		public System.Windows.Forms.ToolStripButton PEGAR;
		public System.Windows.Forms.ToolStripButton IMAGENNUEVA;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button14;
		public System.Windows.Forms.ToolStripButton PINTAR;
		public System.Windows.Forms.ToolStripButton HERRAMIENTAS;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button17;
		public System.Windows.Forms.ToolStripButton SELECC;
		public System.Windows.Forms.ToolStripButton ARRAS;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button20;
		public System.Windows.Forms.ToolStripButton ZOOMIN;
		public System.Windows.Forms.ToolStripButton ZOOMOUT;
		public System.Windows.Forms.ToolStripButton ACERC;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button24;
		public System.Windows.Forms.ToolStripButton IZQ;
		public System.Windows.Forms.ToolStripButton DER;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button27;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button28;
		public System.Windows.Forms.ToolStripButton VOLVER;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button30;
		public System.Windows.Forms.ToolStripButton SALIR;
		private System.Windows.Forms.ToolStripSeparator _tbImagenes_Button32;
		private System.Windows.Forms.ToolStripButton _tbImagenes_Button33;
		public System.Windows.Forms.ToolStrip tbImagenes;
		private System.Windows.Forms.ToolStripSeparator _Toolbar2_Button1;
        /*INDRA jproche_TODO_5_5
        public System.Windows.Forms.ToolStripButton CANC;
        */
		private System.Windows.Forms.ToolStripSeparator _Toolbar2_Button3;
        /*INDRA jproche_TODO_5_5
		public System.Windows.Forms.ToolStripButton IMPR;
        */
		private System.Windows.Forms.ToolStripSeparator _Toolbar2_Button5;
        /*INDRA jproche_TODO_5_5
		public System.Windows.Forms.ToolStripButton ZOOMIN;
		public System.Windows.Forms.ToolStripButton ZOOMOUT;
		public System.Windows.Forms.ToolStripButton ACERC;
        */
		private System.Windows.Forms.ToolStripSeparator _Toolbar2_Button9;
        /*INDRA jproche_TODO_5_5
		public System.Windows.Forms.ToolStripButton IZQ;
		public System.Windows.Forms.ToolStripButton DER;
        */
		private System.Windows.Forms.ToolStripSeparator _Toolbar2_Button12;
        /*INDRA jproche_TODO_5_5
		public System.Windows.Forms.ToolStripButton SALIR;
        */
		private System.Windows.Forms.ToolStripSeparator _Toolbar2_Button14;
		private System.Windows.Forms.ToolStripButton _Toolbar2_Button15;
		public System.Windows.Forms.ToolStrip Toolbar2;
		private System.Windows.Forms.ToolStripSeparator _Toolbar1_Button1;
        /*INDRA jproche_TODO_5_5
		public System.Windows.Forms.ToolStripButton GRAB;
		public System.Windows.Forms.ToolStripButton CANC;
        */
		private System.Windows.Forms.ToolStripSeparator _Toolbar1_Button4;
		public System.Windows.Forms.ToolStripButton ESCANER;
		private System.Windows.Forms.ToolStripSeparator _Toolbar1_Button6;
		public System.Windows.Forms.ToolStripButton ESCAN;
		public System.Windows.Forms.ToolStripButton ABRIR;
		public System.Windows.Forms.ToolStripButton BUSCAR;
		private System.Windows.Forms.ToolStripSeparator _Toolbar1_Button10;
        /*INDRA jproche_TODO_5_5
		public System.Windows.Forms.ToolStripButton IZQ;
		public System.Windows.Forms.ToolStripButton DER;
        */
		public System.Windows.Forms.ToolStrip Toolbar1;
        /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
		public AxScanLibCtl.AxImgScan ImgScan1;
        */
		public System.Windows.Forms.PictureBox Image1;
		public System.Windows.Forms.OpenFileDialog CommonDialog1Open;
		public System.Windows.Forms.PrintDialog CommonDialog1Print;
		public System.Windows.Forms.HScrollBar scbPaginas;
        /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
		public AxImgeditLibCtl.AxImgEdit iEImgFact;
        */
        public System.Windows.Forms.Label lbNumeracion;
		public System.Windows.Forms.Label lbPaginas;
		public System.Windows.Forms.Label lbZoom;
		public System.Windows.Forms.GroupBox frmImagenes;
		public System.Windows.Forms.PictureBox Image2;
		public System.Windows.Forms.ImageList ilImagenes;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImgRFact));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.tbImagenes = new System.Windows.Forms.ToolStrip();
			this._tbImagenes_Button1 = new System.Windows.Forms.ToolStripSeparator();
			this.NUEVO = new System.Windows.Forms.ToolStripButton();
			this.ELIM = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button4 = new System.Windows.Forms.ToolStripSeparator();
			this.GRAB = new System.Windows.Forms.ToolStripButton();
			this.CANC = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button7 = new System.Windows.Forms.ToolStripSeparator();
			this.IMPR = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button9 = new System.Windows.Forms.ToolStripSeparator();
			this.CORTAR = new System.Windows.Forms.ToolStripButton();
			this.COPIAR = new System.Windows.Forms.ToolStripButton();
			this.PEGAR = new System.Windows.Forms.ToolStripButton();
			this.IMAGENNUEVA = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button14 = new System.Windows.Forms.ToolStripSeparator();
			this.PINTAR = new System.Windows.Forms.ToolStripButton();
			this.HERRAMIENTAS = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button17 = new System.Windows.Forms.ToolStripSeparator();
			this.SELECC = new System.Windows.Forms.ToolStripButton();
			this.ARRAS = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button20 = new System.Windows.Forms.ToolStripSeparator();
			this.ZOOMIN = new System.Windows.Forms.ToolStripButton();
			this.ZOOMOUT = new System.Windows.Forms.ToolStripButton();
			this.ACERC = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button24 = new System.Windows.Forms.ToolStripSeparator();
			this.IZQ = new System.Windows.Forms.ToolStripButton();
			this.DER = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button27 = new System.Windows.Forms.ToolStripSeparator();
			this._tbImagenes_Button28 = new System.Windows.Forms.ToolStripSeparator();
			this.VOLVER = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button30 = new System.Windows.Forms.ToolStripSeparator();
			this.SALIR = new System.Windows.Forms.ToolStripButton();
			this._tbImagenes_Button32 = new System.Windows.Forms.ToolStripSeparator();
			this._tbImagenes_Button33 = new System.Windows.Forms.ToolStripButton();
			this.Toolbar2 = new System.Windows.Forms.ToolStrip();
			this._Toolbar2_Button1 = new System.Windows.Forms.ToolStripSeparator();
             /*INDRA jproche_TODO_5_5
			this.CANC = new System.Windows.Forms.ToolStripButton();
            */
			this._Toolbar2_Button3 = new System.Windows.Forms.ToolStripSeparator();
             /*INDRA jproche_TODO_5_5
			this.IMPR = new System.Windows.Forms.ToolStripButton();
            */
			this._Toolbar2_Button5 = new System.Windows.Forms.ToolStripSeparator();
             /*INDRA jproche_TODO_5_5
			this.ZOOMIN = new System.Windows.Forms.ToolStripButton();
			this.ZOOMOUT = new System.Windows.Forms.ToolStripButton();
			this.ACERC = new System.Windows.Forms.ToolStripButton();
            */
			this._Toolbar2_Button9 = new System.Windows.Forms.ToolStripSeparator();
            /*INDRA jproche_TODO_5_5
           this.IZQ = new System.Windows.Forms.ToolStripButton();
           this.DER = new System.Windows.Forms.ToolStripButton();
           */
           this._Toolbar2_Button12 = new System.Windows.Forms.ToolStripSeparator();
             /*INDRA jproche_TODO_5_5
           this.SALIR = new System.Windows.Forms.ToolStripButton();
           */
           this._Toolbar2_Button14 = new System.Windows.Forms.ToolStripSeparator();
           this._Toolbar2_Button15 = new System.Windows.Forms.ToolStripButton();
           this.Toolbar1 = new System.Windows.Forms.ToolStrip();
           this._Toolbar1_Button1 = new System.Windows.Forms.ToolStripSeparator();
            /*INDRA jproche_TODO_5_5
           this.GRAB = new System.Windows.Forms.ToolStripButton();
           this.CANC = new System.Windows.Forms.ToolStripButton();
           */
            this._Toolbar1_Button4 = new System.Windows.Forms.ToolStripSeparator();
			this.ESCANER = new System.Windows.Forms.ToolStripButton();
			this._Toolbar1_Button6 = new System.Windows.Forms.ToolStripSeparator();
			this.ESCAN = new System.Windows.Forms.ToolStripButton();
			this.ABRIR = new System.Windows.Forms.ToolStripButton();
			this.BUSCAR = new System.Windows.Forms.ToolStripButton();
			this._Toolbar1_Button10 = new System.Windows.Forms.ToolStripSeparator();
            /*INDRA jproche_TODO_5_5
			this.IZQ = new System.Windows.Forms.ToolStripButton();
			this.DER = new System.Windows.Forms.ToolStripButton();
            INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
			this.ImgScan1 = new AxScanLibCtl.AxImgScan();
            */
			this.Image1 = new System.Windows.Forms.PictureBox();
			this.CommonDialog1Open = new System.Windows.Forms.OpenFileDialog();
			this.CommonDialog1Print = new System.Windows.Forms.PrintDialog();
			this.CommonDialog1Print.PrinterSettings = new System.Drawing.Printing.PrinterSettings();
			this.frmImagenes = new System.Windows.Forms.GroupBox();
			this.scbPaginas = new System.Windows.Forms.HScrollBar();
            /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
			this.iEImgFact = new AxImgeditLibCtl.AxImgEdit();
            */
			this.lbNumeracion = new System.Windows.Forms.Label();
			this.lbPaginas = new System.Windows.Forms.Label();
			this.lbZoom = new System.Windows.Forms.Label();
			this.Image2 = new System.Windows.Forms.PictureBox();
			this.ilImagenes = new System.Windows.Forms.ImageList();
            /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
			((System.ComponentModel.ISupportInitialize) this.ImgScan1).BeginInit();
			((System.ComponentModel.ISupportInitialize) this.iEImgFact).BeginInit();
            */
			this.tbImagenes.SuspendLayout();
			this.Toolbar2.SuspendLayout();
			this.Toolbar1.SuspendLayout();
			this.frmImagenes.SuspendLayout();
			this.SuspendLayout();
			// 
			// tbImagenes
			// 
			this.tbImagenes.Dock = System.Windows.Forms.DockStyle.Top;
			this.tbImagenes.ImageList = ilImagenes;
			this.tbImagenes.Location = new System.Drawing.Point(0, 0);
			this.tbImagenes.Name = "tbImagenes";
			this.tbImagenes.ShowItemToolTips = true;
			this.tbImagenes.Size = new System.Drawing.Size(758, 28);
			this.tbImagenes.TabIndex = 5;
			this.tbImagenes.Items.Add(this.NUEVO);
			this.tbImagenes.Items.Add(this.ELIM);
			this.tbImagenes.Items.Add(this.GRAB);
			this.tbImagenes.Items.Add(this.CANC);
			this.tbImagenes.Items.Add(this.IMPR);
			this.tbImagenes.Items.Add(this.CORTAR);
			this.tbImagenes.Items.Add(this.COPIAR);
			this.tbImagenes.Items.Add(this.PEGAR);
			this.tbImagenes.Items.Add(this.IMAGENNUEVA);
			this.tbImagenes.Items.Add(this.PINTAR);
			this.tbImagenes.Items.Add(this.HERRAMIENTAS);
			this.tbImagenes.Items.Add(this.SELECC);
			this.tbImagenes.Items.Add(this.ARRAS);
			this.tbImagenes.Items.Add(this.ZOOMIN);
			this.tbImagenes.Items.Add(this.ZOOMOUT);
			this.tbImagenes.Items.Add(this.ACERC);
			this.tbImagenes.Items.Add(this.IZQ);
			this.tbImagenes.Items.Add(this.DER);
			this.tbImagenes.Items.Add(this.VOLVER);
			this.tbImagenes.Items.Add(this.SALIR);
			this.tbImagenes.Items.Add(this._tbImagenes_Button33);
			// 
			// _tbImagenes_Button1
			// 
			this._tbImagenes_Button1.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button1.Tag = "";
			this._tbImagenes_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button1.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// NUEVO
			// 
			this.NUEVO.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.NUEVO.ImageIndex = 23;
			this.NUEVO.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.NUEVO.Name = "NUEVO";
			this.NUEVO.Size = new System.Drawing.Size(24, 22);
			this.NUEVO.Tag = "";
			this.NUEVO.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.NUEVO.ToolTipText = "Insertar Imagen";
			this.NUEVO.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// ELIM
			// 
			this.ELIM.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ELIM.ImageIndex = 14;
			this.ELIM.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ELIM.Name = "ELIM";
			this.ELIM.Size = new System.Drawing.Size(24, 22);
			this.ELIM.Tag = "";
			this.ELIM.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ELIM.ToolTipText = "Eliminar Imagen";
			this.ELIM.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button4
			// 
			this._tbImagenes_Button4.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button4.Tag = "";
			this._tbImagenes_Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button4.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// GRAB
			// 
			this.GRAB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.GRAB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.GRAB.Enabled = false;
			this.GRAB.Enabled = false;
			this.GRAB.ImageIndex = 16;
			this.GRAB.ImageIndex = 25;
			this.GRAB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.GRAB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.GRAB.Name = "GRAB";
			this.GRAB.Name = "GRAB";
			this.GRAB.Size = new System.Drawing.Size(24, 22);
			this.GRAB.Size = new System.Drawing.Size(24, 22);
			this.GRAB.Tag = "";
			this.GRAB.Tag = "";
			this.GRAB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.GRAB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.GRAB.ToolTipText = "Actualizar Cambios";
			this.GRAB.ToolTipText = "Actualizar";
			this.GRAB.Visible = false;
			this.GRAB.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			this.GRAB.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// CANC
			// 
			this.CANC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.CANC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.CANC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.CANC.Enabled = false;
			this.CANC.Enabled = false;
			this.CANC.ImageIndex = 17;
			this.CANC.ImageIndex = 17;
			this.CANC.ImageIndex = 17;
			this.CANC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.CANC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.CANC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.CANC.Name = "CANC";
			this.CANC.Name = "CANC";
			this.CANC.Name = "CANC";
			this.CANC.Size = new System.Drawing.Size(24, 22);
			this.CANC.Size = new System.Drawing.Size(24, 22);
			this.CANC.Size = new System.Drawing.Size(24, 22);
			this.CANC.Tag = "";
			this.CANC.Tag = "";
			this.CANC.Tag = "";
			this.CANC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.CANC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.CANC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.CANC.ToolTipText = "Cancelar Cambios";
			this.CANC.ToolTipText = "Cancelar Cambios";
			this.CANC.ToolTipText = "Cancelar";
			this.CANC.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			this.CANC.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			this.CANC.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// _tbImagenes_Button7
			// 
			this._tbImagenes_Button7.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button7.Tag = "";
			this._tbImagenes_Button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button7.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// IMPR
			// 
			this.IMPR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.IMPR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.IMPR.ImageIndex = 0;
			this.IMPR.ImageIndex = 0;
			this.IMPR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.IMPR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.IMPR.Name = "IMPR";
			this.IMPR.Name = "IMPR";
			this.IMPR.Size = new System.Drawing.Size(24, 22);
			this.IMPR.Size = new System.Drawing.Size(24, 22);
			this.IMPR.Tag = "";
			this.IMPR.Tag = "";
			this.IMPR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.IMPR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.IMPR.ToolTipText = "Imprimir";
			this.IMPR.ToolTipText = "Imprimir";
			this.IMPR.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			this.IMPR.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button9
			// 
			this._tbImagenes_Button9.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button9.Tag = "";
			this._tbImagenes_Button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button9.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// CORTAR
			// 
			this.CORTAR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.CORTAR.ImageIndex = 1;
			this.CORTAR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.CORTAR.Name = "CORTAR";
			this.CORTAR.Size = new System.Drawing.Size(24, 22);
			this.CORTAR.Tag = "";
			this.CORTAR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.CORTAR.ToolTipText = "Cortar";
			this.CORTAR.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// COPIAR
			// 
			this.COPIAR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.COPIAR.ImageIndex = 2;
			this.COPIAR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.COPIAR.Name = "COPIAR";
			this.COPIAR.Size = new System.Drawing.Size(24, 22);
			this.COPIAR.Tag = "";
			this.COPIAR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.COPIAR.ToolTipText = "Copiar";
			this.COPIAR.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// PEGAR
			// 
			this.PEGAR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.PEGAR.ImageIndex = 3;
			this.PEGAR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.PEGAR.Name = "PEGAR";
			this.PEGAR.Size = new System.Drawing.Size(24, 22);
			this.PEGAR.Tag = "";
			this.PEGAR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.PEGAR.ToolTipText = "Pegar";
			this.PEGAR.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// IMAGENNUEVA
			// 
			this.IMAGENNUEVA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.IMAGENNUEVA.ImageIndex = 26;
			this.IMAGENNUEVA.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.IMAGENNUEVA.Name = "IMAGENNUEVA";
			this.IMAGENNUEVA.Size = new System.Drawing.Size(24, 22);
			this.IMAGENNUEVA.Tag = "";
			this.IMAGENNUEVA.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.IMAGENNUEVA.ToolTipText = "Pegar Como Imagen Nueva";
			this.IMAGENNUEVA.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button14
			// 
			this._tbImagenes_Button14.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button14.Tag = "";
			this._tbImagenes_Button14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button14.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// PINTAR
			// 
			this.PINTAR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.PINTAR.ImageIndex = 28;
			this.PINTAR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.PINTAR.Name = "PINTAR";
			this.PINTAR.Size = new System.Drawing.Size(24, 22);
			this.PINTAR.Tag = "";
			this.PINTAR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.PINTAR.ToolTipText = "A�adir anotaciones";
			this.PINTAR.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// HERRAMIENTAS
			// 
			this.HERRAMIENTAS.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.HERRAMIENTAS.ImageIndex = 27;
			this.HERRAMIENTAS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.HERRAMIENTAS.Name = "HERRAMIENTAS";
			this.HERRAMIENTAS.Size = new System.Drawing.Size(24, 22);
			this.HERRAMIENTAS.Tag = "";
			this.HERRAMIENTAS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.HERRAMIENTAS.ToolTipText = "Configurar anotaciones";
			this.HERRAMIENTAS.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button17
			// 
			this._tbImagenes_Button17.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button17.Tag = "";
			this._tbImagenes_Button17.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button17.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// SELECC
			// 
			this.SELECC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.SELECC.ImageIndex = 4;
			this.SELECC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.SELECC.Name = "SELECC";
			this.SELECC.Size = new System.Drawing.Size(24, 22);
			this.SELECC.Tag = "";
			this.SELECC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.SELECC.ToolTipText = "Seleccionar �rea";
			this.SELECC.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// ARRAS
			// 
			this.ARRAS.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ARRAS.ImageIndex = 5;
			this.ARRAS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ARRAS.Name = "ARRAS";
			this.ARRAS.Size = new System.Drawing.Size(24, 22);
			this.ARRAS.Tag = "";
			this.ARRAS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ARRAS.ToolTipText = "Arrastrar";
			this.ARRAS.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button20
			// 
			this._tbImagenes_Button20.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button20.Tag = "";
			this._tbImagenes_Button20.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button20.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// ZOOMIN
			// 
			this.ZOOMIN.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ZOOMIN.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ZOOMIN.ImageIndex = 6;
			this.ZOOMIN.ImageIndex = 6;
			this.ZOOMIN.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ZOOMIN.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ZOOMIN.Name = "ZOOMIN";
			this.ZOOMIN.Name = "ZOOMIN";
			this.ZOOMIN.Size = new System.Drawing.Size(24, 22);
			this.ZOOMIN.Size = new System.Drawing.Size(24, 22);
			this.ZOOMIN.Tag = "";
			this.ZOOMIN.Tag = "";
			this.ZOOMIN.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ZOOMIN.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ZOOMIN.ToolTipText = "Acercar imagen";
			this.ZOOMIN.ToolTipText = "Acercar imagen";
			this.ZOOMIN.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			this.ZOOMIN.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// ZOOMOUT
			// 
			this.ZOOMOUT.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ZOOMOUT.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ZOOMOUT.ImageIndex = 7;
			this.ZOOMOUT.ImageIndex = 7;
			this.ZOOMOUT.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ZOOMOUT.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ZOOMOUT.Name = "ZOOMOUT";
			this.ZOOMOUT.Name = "ZOOMOUT";
			this.ZOOMOUT.Size = new System.Drawing.Size(24, 22);
			this.ZOOMOUT.Size = new System.Drawing.Size(24, 22);
			this.ZOOMOUT.Tag = "";
			this.ZOOMOUT.Tag = "";
			this.ZOOMOUT.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ZOOMOUT.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ZOOMOUT.ToolTipText = "Alejar imagen";
			this.ZOOMOUT.ToolTipText = "Alejar imagen";
			this.ZOOMOUT.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			this.ZOOMOUT.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// ACERC
			// 
			this.ACERC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ACERC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ACERC.ImageIndex = 8;
			this.ACERC.ImageIndex = 8;
			this.ACERC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ACERC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ACERC.Name = "ACERC";
			this.ACERC.Name = "ACERC";
			this.ACERC.Size = new System.Drawing.Size(24, 22);
			this.ACERC.Size = new System.Drawing.Size(24, 22);
			this.ACERC.Tag = "";
			this.ACERC.Tag = "";
			this.ACERC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ACERC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ACERC.ToolTipText = "Acercar un �rea";
			this.ACERC.ToolTipText = "Acercar un �rea";
			this.ACERC.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			this.ACERC.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button24
			// 
			this._tbImagenes_Button24.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button24.Tag = "";
			this._tbImagenes_Button24.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button24.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// IZQ
			// 
			this.IZQ.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.IZQ.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.IZQ.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.IZQ.ImageIndex = 21;
			this.IZQ.ImageIndex = 21;
			this.IZQ.ImageIndex = 21;
			this.IZQ.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.IZQ.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.IZQ.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.IZQ.Name = "IZQ";
			this.IZQ.Name = "IZQ";
			this.IZQ.Name = "IZQ";
			this.IZQ.Size = new System.Drawing.Size(24, 22);
			this.IZQ.Size = new System.Drawing.Size(24, 22);
			this.IZQ.Size = new System.Drawing.Size(24, 22);
			this.IZQ.Tag = "";
			this.IZQ.Tag = "";
			this.IZQ.Tag = "";
			this.IZQ.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.IZQ.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.IZQ.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.IZQ.ToolTipText = "Rotar a la izquierda";
			this.IZQ.ToolTipText = "Rotar a la izquierda";
			this.IZQ.ToolTipText = "Rotar a la izquierda";
			this.IZQ.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			this.IZQ.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			this.IZQ.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// DER
			// 
			this.DER.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.DER.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.DER.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.DER.ImageIndex = 20;
			this.DER.ImageIndex = 20;
			this.DER.ImageIndex = 20;
			this.DER.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.DER.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.DER.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.DER.Name = "DER";
			this.DER.Name = "DER";
			this.DER.Name = "DER";
			this.DER.Size = new System.Drawing.Size(24, 22);
			this.DER.Size = new System.Drawing.Size(24, 22);
			this.DER.Size = new System.Drawing.Size(24, 22);
			this.DER.Tag = "";
			this.DER.Tag = "";
			this.DER.Tag = "";
			this.DER.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.DER.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.DER.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.DER.ToolTipText = "Rotar a la derecha";
			this.DER.ToolTipText = "Rotar a la derecha";
			this.DER.ToolTipText = "Rotar a la derecha";
			this.DER.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			this.DER.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			this.DER.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// _tbImagenes_Button27
			// 
			this._tbImagenes_Button27.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button27.Tag = "";
			this._tbImagenes_Button27.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button27.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button28
			// 
			this._tbImagenes_Button28.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button28.Tag = "";
			this._tbImagenes_Button28.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button28.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// VOLVER
			// 
			this.VOLVER.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.VOLVER.ImageIndex = 25;
			this.VOLVER.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.VOLVER.Name = "VOLVER";
			this.VOLVER.Size = new System.Drawing.Size(24, 22);
			this.VOLVER.Tag = "";
			this.VOLVER.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.VOLVER.ToolTipText = "Cerrar con Cambios";
			this.VOLVER.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button30
			// 
			this._tbImagenes_Button30.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button30.Tag = "";
			this._tbImagenes_Button30.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button30.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// SALIR
			// 
			this.SALIR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.SALIR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.SALIR.ImageIndex = 11;
			this.SALIR.ImageIndex = 11;
			this.SALIR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.SALIR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.SALIR.Name = "SALIR";
			this.SALIR.Name = "SALIR";
			this.SALIR.Size = new System.Drawing.Size(24, 22);
			this.SALIR.Size = new System.Drawing.Size(24, 22);
			this.SALIR.Tag = "";
			this.SALIR.Tag = "";
			this.SALIR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.SALIR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.SALIR.ToolTipText = "Salir";
			this.SALIR.ToolTipText = "Salir";
			this.SALIR.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			this.SALIR.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button32
			// 
			this._tbImagenes_Button32.Size = new System.Drawing.Size(6, 22);
			this._tbImagenes_Button32.Tag = "";
			this._tbImagenes_Button32.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button32.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// _tbImagenes_Button33
			// 
			this._tbImagenes_Button33.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._tbImagenes_Button33.Enabled = false;
			this._tbImagenes_Button33.ImageIndex = 12;
			this._tbImagenes_Button33.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._tbImagenes_Button33.Size = new System.Drawing.Size(24, 22);
			this._tbImagenes_Button33.Tag = "";
			this._tbImagenes_Button33.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbImagenes_Button33.ToolTipText = "Ayuda";
			this._tbImagenes_Button33.Click += new System.EventHandler(this.tbImagenes_ButtonClick);
			// 
			// Toolbar2
			// 
			this.Toolbar2.Dock = System.Windows.Forms.DockStyle.Top;
			this.Toolbar2.ImageList = ilImagenes;
			this.Toolbar2.Location = new System.Drawing.Point(0, 25);
			this.Toolbar2.Name = "Toolbar2";
			this.Toolbar2.ShowItemToolTips = true;
			this.Toolbar2.Size = new System.Drawing.Size(758, 28);
			this.Toolbar2.TabIndex = 9;
			this.Toolbar2.Items.Add(this.CANC);
			this.Toolbar2.Items.Add(this.IMPR);
			this.Toolbar2.Items.Add(this.ZOOMIN);
			this.Toolbar2.Items.Add(this.ZOOMOUT);
			this.Toolbar2.Items.Add(this.ACERC);
			this.Toolbar2.Items.Add(this.IZQ);
			this.Toolbar2.Items.Add(this.DER);
			this.Toolbar2.Items.Add(this.SALIR);
			this.Toolbar2.Items.Add(this._Toolbar2_Button15);
			// 
			// _Toolbar2_Button1
			// 
			this._Toolbar2_Button1.Size = new System.Drawing.Size(6, 22);
			this._Toolbar2_Button1.Tag = "";
			this._Toolbar2_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar2_Button1.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// _Toolbar2_Button3
			// 
			this._Toolbar2_Button3.Size = new System.Drawing.Size(6, 22);
			this._Toolbar2_Button3.Tag = "";
			this._Toolbar2_Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar2_Button3.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// _Toolbar2_Button5
			// 
			this._Toolbar2_Button5.Size = new System.Drawing.Size(6, 22);
			this._Toolbar2_Button5.Tag = "";
			this._Toolbar2_Button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar2_Button5.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// _Toolbar2_Button9
			// 
			this._Toolbar2_Button9.Size = new System.Drawing.Size(6, 22);
			this._Toolbar2_Button9.Tag = "";
			this._Toolbar2_Button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar2_Button9.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// _Toolbar2_Button12
			// 
			this._Toolbar2_Button12.Size = new System.Drawing.Size(6, 22);
			this._Toolbar2_Button12.Tag = "";
			this._Toolbar2_Button12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar2_Button12.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// _Toolbar2_Button14
			// 
			this._Toolbar2_Button14.Size = new System.Drawing.Size(6, 22);
			this._Toolbar2_Button14.Tag = "";
			this._Toolbar2_Button14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar2_Button14.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// _Toolbar2_Button15
			// 
			this._Toolbar2_Button15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar2_Button15.Enabled = false;
			this._Toolbar2_Button15.ImageIndex = 12;
			this._Toolbar2_Button15.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._Toolbar2_Button15.Size = new System.Drawing.Size(24, 22);
			this._Toolbar2_Button15.Tag = "";
			this._Toolbar2_Button15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar2_Button15.ToolTipText = "Ayuda";
			this._Toolbar2_Button15.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// Toolbar1
			// 
			this.Toolbar1.Dock = System.Windows.Forms.DockStyle.Top;
			this.Toolbar1.ImageList = ilImagenes;
			this.Toolbar1.Location = new System.Drawing.Point(0, 56);
			this.Toolbar1.Name = "Toolbar1";
			this.Toolbar1.ShowItemToolTips = true;
			this.Toolbar1.Size = new System.Drawing.Size(758, 28);
			this.Toolbar1.TabIndex = 8;
			this.Toolbar1.Visible = false;
			this.Toolbar1.Items.Add(this.GRAB);
			this.Toolbar1.Items.Add(this.CANC);
			this.Toolbar1.Items.Add(this.ESCANER);
			this.Toolbar1.Items.Add(this.ESCAN);
			this.Toolbar1.Items.Add(this.ABRIR);
			this.Toolbar1.Items.Add(this.BUSCAR);
			this.Toolbar1.Items.Add(this.IZQ);
			this.Toolbar1.Items.Add(this.DER);
			// 
			// _Toolbar1_Button1
			// 
			this._Toolbar1_Button1.Size = new System.Drawing.Size(6, 22);
			this._Toolbar1_Button1.Tag = "";
			this._Toolbar1_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button1.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button4
			// 
			this._Toolbar1_Button4.Size = new System.Drawing.Size(6, 22);
			this._Toolbar1_Button4.Tag = "";
			this._Toolbar1_Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button4.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// ESCANER
			// 
			this.ESCANER.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ESCANER.ImageIndex = 18;
			this.ESCANER.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ESCANER.Name = "ESCANER";
			this.ESCANER.Size = new System.Drawing.Size(24, 22);
			this.ESCANER.Tag = "";
			this.ESCANER.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ESCANER.ToolTipText = "Esc�ners Disponibles";
			this.ESCANER.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button6
			// 
			this._Toolbar1_Button6.Size = new System.Drawing.Size(6, 22);
			this._Toolbar1_Button6.Tag = "";
			this._Toolbar1_Button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button6.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// ESCAN
			// 
			this.ESCAN.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ESCAN.ImageIndex = 15;
			this.ESCAN.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ESCAN.Name = "ESCAN";
			this.ESCAN.Size = new System.Drawing.Size(24, 22);
			this.ESCAN.Tag = "";
			this.ESCAN.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ESCAN.ToolTipText = "Adquirir Imagen";
			this.ESCAN.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// ABRIR
			// 
			this.ABRIR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.ABRIR.ImageIndex = 22;
			this.ABRIR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.ABRIR.Name = "ABRIR";
			this.ABRIR.Size = new System.Drawing.Size(24, 22);
			this.ABRIR.Tag = "";
			this.ABRIR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.ABRIR.ToolTipText = "Importar Imagen";
			this.ABRIR.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// BUSCAR
			// 
			this.BUSCAR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this.BUSCAR.ImageIndex = 24;
			this.BUSCAR.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.BUSCAR.Name = "BUSCAR";
			this.BUSCAR.Size = new System.Drawing.Size(24, 22);
			this.BUSCAR.Tag = "";
			this.BUSCAR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.BUSCAR.ToolTipText = "Imagen Guardada";
			this.BUSCAR.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button10
			// 
			this._Toolbar1_Button10.Size = new System.Drawing.Size(6, 22);
			this._Toolbar1_Button10.Tag = "";
			this._Toolbar1_Button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button10.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
			// 
			// ImgScan1
			// 
			this.ImgScan1.Location = new System.Drawing.Point(0, 408);
			this.ImgScan1.Name = "ImgScan1";
			this.ImgScan1.OcxState = (System.Windows.Forms.AxHost.State) resources.GetObject("ImgScan1.OcxState");
            */
			// 
			// Image1
			// 
			this.Image1.BackColor = System.Drawing.SystemColors.Window;
			this.Image1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Image1.CausesValidation = true;
			this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Image1.Dock = System.Windows.Forms.DockStyle.None;
			this.Image1.Enabled = true;
			this.Image1.Location = new System.Drawing.Point(0, 120);
			this.Image1.Name = "Image1";
			this.Image1.Size = new System.Drawing.Size(17, 33);
			this.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this.Image1.TabIndex = 6;
			this.Image1.TabStop = true;
			this.Image1.Visible = false;
			// 
			// frmImagenes
			// 
			this.frmImagenes.BackColor = System.Drawing.SystemColors.Control;
			this.frmImagenes.Controls.Add(this.scbPaginas);
            /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
			this.frmImagenes.Controls.Add(this.iEImgFact);
            */
			this.frmImagenes.Controls.Add(this.lbNumeracion);
			this.frmImagenes.Controls.Add(this.lbPaginas);
			this.frmImagenes.Controls.Add(this.lbZoom);
			this.frmImagenes.Enabled = true;
			this.frmImagenes.ForeColor = System.Drawing.SystemColors.ControlText;
			this.frmImagenes.Location = new System.Drawing.Point(10, 27);
			this.frmImagenes.Name = "frmImagenes";
			this.frmImagenes.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmImagenes.Size = new System.Drawing.Size(745, 432);
			this.frmImagenes.TabIndex = 1;
			this.frmImagenes.Visible = true;
			// 
			// scbPaginas
			// 
			this.scbPaginas.CausesValidation = true;
			this.scbPaginas.Cursor = System.Windows.Forms.Cursors.Default;
			this.scbPaginas.Enabled = true;
			this.scbPaginas.LargeChange = 1;
			this.scbPaginas.Location = new System.Drawing.Point(440, 10);
			this.scbPaginas.Maximum = 32767;
			this.scbPaginas.Minimum = 1;
			this.scbPaginas.Name = "scbPaginas";
			this.scbPaginas.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.scbPaginas.Size = new System.Drawing.Size(65, 17);
			this.scbPaginas.SmallChange = 1;
			this.scbPaginas.TabIndex = 7;
			this.scbPaginas.TabStop = true;
			this.scbPaginas.Value = 1;
			this.scbPaginas.Visible = false;
            /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
			// 
			// iEImgFact
			// 
            this.iEImgFact.Location = new System.Drawing.Point(10, 37);
			this.iEImgFact.Name = "iEImgFact";
			this.iEImgFact.OcxState = (System.Windows.Forms.AxHost.State) resources.GetObject("iEImgFact.OcxState");
			this.iEImgFact.Size = new System.Drawing.Size(728, 388);
			this.iEImgFact.TabIndex = 0;
			this.iEImgFact.TabStop = false;
			this.iEImgFact.MouseDownEvent += new AxImgeditLibCtl._DImgEditEvents_MouseDownEventHandler(this.iEImgFact_MouseDownEvent);
			this.iEImgFact.MouseMoveEvent += new AxImgeditLibCtl._DImgEditEvents_MouseMoveEventHandler(this.iEImgFact_MouseMoveEvent);
			this.iEImgFact.SelectionRectDrawn += new AxImgeditLibCtl._DImgEditEvents_SelectionRectDrawnEventHandler(this.iEImgFact_SelectionRectDrawn);
            */
			// 
			// lbNumeracion
			// 
			this.lbNumeracion.AutoSize = true;
			this.lbNumeracion.BackColor = System.Drawing.SystemColors.Control;
			this.lbNumeracion.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lbNumeracion.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbNumeracion.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbNumeracion.Location = new System.Drawing.Point(517, 11);
			this.lbNumeracion.Name = "lbNumeracion";
			this.lbNumeracion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbNumeracion.Size = new System.Drawing.Size(3, 13);
			this.lbNumeracion.TabIndex = 4;
			// 
			// lbPaginas
			// 
			this.lbPaginas.AutoSize = true;
			this.lbPaginas.BackColor = System.Drawing.SystemColors.Control;
			this.lbPaginas.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lbPaginas.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbPaginas.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbPaginas.Location = new System.Drawing.Point(374, 11);
			this.lbPaginas.Name = "lbPaginas";
			this.lbPaginas.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbPaginas.Size = new System.Drawing.Size(53, 13);
			this.lbPaginas.TabIndex = 3;
			this.lbPaginas.Text = "Referencia:";
			// 
			// lbZoom
			// 
			this.lbZoom.AutoSize = true;
			this.lbZoom.BackColor = System.Drawing.SystemColors.Control;
			this.lbZoom.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lbZoom.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbZoom.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbZoom.Location = new System.Drawing.Point(170, 10);
			this.lbZoom.Name = "lbZoom";
			this.lbZoom.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbZoom.Size = new System.Drawing.Size(33, 13);
			this.lbZoom.TabIndex = 2;
			this.lbZoom.Text = "Zoom: ";
			// 
			// Image2
			// 
			this.Image2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Image2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Image2.Enabled = true;
			this.Image2.Location = new System.Drawing.Point(0, 168);
			this.Image2.Name = "Image2";
			this.Image2.Size = new System.Drawing.Size(33, 33);
			this.Image2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this.Image2.Visible = false;
			// 
			// ilImagenes
			// 
			this.ilImagenes.ImageSize = new System.Drawing.Size(16, 16);
			this.ilImagenes.ImageStream = (System.Windows.Forms.ImageListStreamer) resources.GetObject("ilImagenes.ImageStream");
			this.ilImagenes.TransparentColor = System.Drawing.Color.Silver;
			this.ilImagenes.Images.SetKeyName(0, "");
			this.ilImagenes.Images.SetKeyName(1, "");
			this.ilImagenes.Images.SetKeyName(2, "");
			this.ilImagenes.Images.SetKeyName(3, "");
			this.ilImagenes.Images.SetKeyName(4, "");
			this.ilImagenes.Images.SetKeyName(5, "");
			this.ilImagenes.Images.SetKeyName(6, "");
			this.ilImagenes.Images.SetKeyName(7, "");
			this.ilImagenes.Images.SetKeyName(8, "");
			this.ilImagenes.Images.SetKeyName(9, "");
			this.ilImagenes.Images.SetKeyName(10, "");
			this.ilImagenes.Images.SetKeyName(11, "");
			this.ilImagenes.Images.SetKeyName(12, "");
			this.ilImagenes.Images.SetKeyName(13, "");
			this.ilImagenes.Images.SetKeyName(14, "");
			this.ilImagenes.Images.SetKeyName(15, "");
			this.ilImagenes.Images.SetKeyName(16, "");
			this.ilImagenes.Images.SetKeyName(17, "");
			this.ilImagenes.Images.SetKeyName(18, "");
			this.ilImagenes.Images.SetKeyName(19, "");
			this.ilImagenes.Images.SetKeyName(20, "");
			this.ilImagenes.Images.SetKeyName(21, "");
			this.ilImagenes.Images.SetKeyName(22, "");
			this.ilImagenes.Images.SetKeyName(23, "");
			this.ilImagenes.Images.SetKeyName(24, "");
			this.ilImagenes.Images.SetKeyName(25, "");
			this.ilImagenes.Images.SetKeyName(26, "");
			this.ilImagenes.Images.SetKeyName(27, "");
			this.ilImagenes.Images.SetKeyName(28, "");
			// 
			// ImgRFact
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
            /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
			((ToolStripButton) this.SELECC).Checked = true;
            */
			this.ClientSize = new System.Drawing.Size(758, 463);
             /*INDRA jproche_TODO_5_5
			this.Controls.Add(this.frmImagenes);
            */
			this.Controls.Add(this.tbImagenes);
			this.Controls.Add(this.Image2);
			this.Controls.Add(this.Image1);
            /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
			this.Controls.Add(this.ImgScan1);
            */
            this.Controls.Add(this.Toolbar1);
			this.Controls.Add(this.Toolbar2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = true;
			this.Name = "ImgRFact";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Gesti�n de Imagen";
			this.Closed += new System.EventHandler(this.ImgRFact_Closed);
			this.Load += new System.EventHandler(this.ImgRFact_Load);
            /*INDRA jproche_TODO_X_5  No se ha definido por cual dll se migrara 25/05/2016
			((System.ComponentModel.ISupportInitialize) this.ImgScan1).EndInit();
			((System.ComponentModel.ISupportInitialize) this.iEImgFact).EndInit();
            */
            this.tbImagenes.ResumeLayout(false);
			this.Toolbar2.ResumeLayout(false);
			this.Toolbar1.ResumeLayout(false);
			this.frmImagenes.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}