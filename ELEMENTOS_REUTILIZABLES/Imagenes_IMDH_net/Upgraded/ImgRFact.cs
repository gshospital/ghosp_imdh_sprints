using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;

namespace ImagenesIMDH
{
	internal partial class ImgRFact
		: Telerik.WinControls.UI.RadForm
    {

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", EntryPoint = "FindWindowExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int FindWindowEx(int hWnd1, int hWnd2, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpsz1, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpsz2);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int SendTBMessage(int hWnd, int wMsg, short wParam, System.IntPtr lParam);

		string stNumRegistro = String.Empty;
		bool NUEVO_ACTUALIZA = false; // SI PULSO NUEVO ES TRUE SINO FALSE
		//Coordenadas del cuadro de selecci�n
		int lIzda = 0;
		int lArriba = 0;
		int lAncho = 0;
		int lAlto = 0;

		//Coordenadas cuando Mousedown en la imagen
		float sXpulsada = 0;
		float sYpulsada = 0;

		string Nombre_Archivo = String.Empty;
		int N�mero = 0;
		public ImgRFact()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void CerrarImagen()
		{
            /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
			if (Module1.stTipoLlamada == "IMDH")
			{
				if (!iEImgFact.ImageDisplayed)
				{
					Module1.stVueltaIMDH = "VACIO";
				}
				else
				{
					iEImgFact.DeleteImageData(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight);
				}

			}
			else
			{
				if (iEImgFact.ImageDisplayed)
				{
					iEImgFact.DeleteImageData(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight);
				}
			}
            */
			if (FileSystem.Dir(Nombre_Archivo, FileAttribute.Normal) != "")
			{
				File.Delete(Nombre_Archivo);
			}

			this.Close();

		}

		private void ImprimirImagen()
		{
			try
			{
				//antes de imprimir la imagen vamos a probar a guardarla
				//para despues imprimirla

				if (Module1.stTipoLlamada == "IMDH")
				{
                    /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
                    //se tiene que presentar la pantalla para seleccionar una impresora
                    if (iEImgFact.ImageDisplayed)
					{
						//si existe imagen
						//iEImgFact.BurnInAnnotations 1, 2
						//las opciones de file type pueden ser 3.bmp, 6.jpg
						//para que salgan las anotaciones
						iEImgFact.SaveAs(Nombre_Archivo + "M", 3, null, null, null, null);
						iEImgFact.setImage(Nombre_Archivo + "M");
						iEImgFact.Display();

					}
                    
					//cuando es en modo de lectura/escritura no sale la pantalla
					//de dialogo de la mpresora
					//en modo consulta si que se muestra la pantalla de dialogo.
					//UPGRADE_WARNING: (6022) The CommonDialog CancelError property is not supported in .NET. More Information: http://www.vbtonet.com/ewis/ewi6022.aspx
					CommonDialog1.CancelError = true;
                    */
                    CommonDialog1Print.PrinterSettings.Copies = 1;

					CommonDialog1Print.ShowDialog();
				}

                //con 2 aparece la imagen adaptada a la hoja
                //con 1 aparece el tama�o real de la imagen
                //false 'no saca las anotaciones producidas
                //true saca las anotaciones que tenga
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
                iEImgFact.PrintImage(Type.Missing, Type.Missing, 2, false, null, null, null); //, , 1, False
                */
			}
			catch (Exception e)
			{	
				if (Information.Err().Number == 32755)
				{
					//error provocado al pulsar cancelar en la ventana se seleccion de impresoras
				}
				else
				{
					RadMessageBox.Show("Error al imprimir la imagen." + "\n" + "\r" + 
					                Conversion.ErrorToString() + ".", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
				}
			}
		}

		private void ZoomAcercar()
		{

			try
			{
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				iEImgFact.AutoRefresh = true;
				//Valor del Aumento del Zoom = el doble (/Disminuci�n)
				iEImgFact.Zoom *= 2;
				//iEImgFact.Display
				if (!tbImagenes.Items["ZOOMOUT"].Enabled)
				{
					tbImagenes.Items["ZOOMOUT"].Enabled = true;
				}
				lbZoom.Text = "Zoom: " + iEImgFact.Zoom.ToString();
                */
			}
			catch
			{
				if (Information.Err().Number == 380)
				{
					//Sobrepasado el valor del zoom
					tbImagenes.Items["ZOOMIN"].Enabled = false;
					return;
				}
			}
		}

		private void ZoomAlejar()
		{
			try
			{
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				iEImgFact.AutoRefresh = true;
				//Valor de la Disminuci�n del Zoom = la mitad
				iEImgFact.Zoom /= 2;
				//iEImgFact.Display
				if (!tbImagenes.Items["ZOOMIN"].Enabled)
				{
					tbImagenes.Items["ZOOMIN"].Enabled = true;
				}
				lbZoom.Text = "Zoom: " + iEImgFact.Zoom.ToString();
                */
			}
			catch
			{
				if (Information.Err().Number == 380)
				{
					//Sobrepasado el valor del zoom
					tbImagenes.Items["ZOOMOUT"].Enabled = false;
					return;
				}
			}
		}

		private void RotarImagenIzda()
		{
			try
			{
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				if (iEImgFact.ImageDisplayed)
				{
					iEImgFact.RotateLeft(null);
				}
				else
				{
					RadMessageBox.Show("No hay ninguna imagen cargada.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
				}
                */
			}
			catch
			{
				RadMessageBox.Show("Error al rotar la imagen." + "\n" + "\r" + 
				                Conversion.ErrorToString() + ".", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
			}

		}

		private void RotarImagenDcha()
		{

			try
			{
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				if (iEImgFact.ImageDisplayed)
				{
					iEImgFact.RotateRight(null);
				}
                
				else
				{
					RadMessageBox.Show("No hay ninguna imagen cargada.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
				}
                */
			}
			catch
			{

				RadMessageBox.Show("Error al rotar la imagen." + "\n" + "\r" + 
				                Conversion.ErrorToString() + ".", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
			}

		}

		private void ZoomArea()
		{

			try
			{
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				//iEImgFact.DrawSelectionRect 0, 30, 10, 40
				iEImgFact.ZoomToSelection();
                */
			}
			catch
			{

				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 1007)
				{
					RadMessageBox.Show("Debe seleccionar el �rea a ampliar.", "Error Zoom", MessageBoxButtons.OK, RadMessageIcon.Error);
					return;
				}				
			}
		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void ImgRFact_Load(Object eventSender, EventArgs eventArgs)
		{
			int HSRC = 0;
			object HDEST = null;
			int SUC = 0;
			//   Dim Tbh As Long
			//   Dim Styl As Long
			//   Tbh = FindWindowEx(tbImagenes.hWnd, 0&, "ToolbarWindow32", vbNullString)
			//   Styl = SendTBMessage(Tbh, (&H400 + 57), 0&, 0&)
			//   Styl = Styl Or &H800 Or &H8000 Or &H40
			//   Styl = SendTBMessage(Tbh, (&H400 + 56), 0&, Styl)
			//   tbImagenes.Refresh
			//   Tbh = FindWindowEx(Toolbar1.hWnd, 0&, "ToolbarWindow32", vbNullString)
			//   Styl = SendTBMessage(Tbh, (&H400 + 57), 0&, 0&)
			//   Styl = Styl Or &H800 Or &H8000 Or &H40
			//   Styl = SendTBMessage(Tbh, (&H400 + 56), 0&, Styl)
			//   Toolbar1.Refresh

			scbPaginas.Maximum = (0 + scbPaginas.LargeChange - 1);
			if (Module1.HAY_ESCANER == 0)
			{
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				Module1.HAY_ESCANER = (ImgRFact.DefInstance.ImgScan1.ScannerAvailable()) ? 1 : 2;
                */
			}
			try
			{

				bool tempBool = false;
				string auxVar = (Module1.stTipoLlamada == "IMDH").ToString().Trim();
				if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
				{
					CargarLLamadaIMDH();
					return;
				}
				else
				{
					//oculto el toolbar para el modo consulta 'imdh 01/2003
					Toolbar2.Visible = false;
					tbImagenes.Items["PINTAR"].Visible = false;
					tbImagenes.Items["HERRAMIENTAS"].Visible = false;

				}

				//DECLARAR ESTA VARIABLE COMO GLOBAL
				if (Module1.HAY_ESCANER == 1)
				{
					//tbImagenes.Buttons(21).Enabled = True   ' si existen escaner habilito el bot�n si no no
					Toolbar1.Items[4].Enabled = true; // selecci�n de escaner
					Toolbar1.Items[6].Enabled = true; // escanear
				}
				else
				{
					//tbImagenes.Buttons(21).Enabled = False
					Toolbar1.Items[4].Enabled = false; // selecci�n de escaner
					Toolbar1.Items[6].Enabled = false; // escanear
				}

				int iResult = 0;
				//vamos a ver como metemos esto en el control
				//estos controles son la mayor mierda jam�s realizada
				//probado lo de pasarlo a portapapeles y sacarlo en el imaedit y falla por todos lados
				//soluci�n:
				//sacar el picture en archivo e importarlo, luego, si se modifica o algo pues ... no se
				//de momento lo saco como archivo
				//voy a coger un nombre de archivo....
				Nombre_Archivo = Module1.Archivo();
				FileSystem.FileOpen(1, Nombre_Archivo, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);
				FileSystem.FileClose();

				int xSrc = 0;
				int ySrc = 0;
				int NNWIDTH = 0;
				int NNHEIGHT = 0;
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				if (Convert.ToDouble(Module1.HAY_Imagen.Picture) != 0)
				{
					// por si la imagen es de tipo raro, voy a pasarla a BMP
					//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Image1.Image = (Image) Module1.HAY_Imagen.Picture;
					//aqu� mantendr� la picture original
					//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Image2.Image = (Image) Module1.HAY_Imagen.Picture;

					Image1.Width = (int) Image2.Width;
					Image1.Height = (int) Image2.Height;
					HSRC = Image1.Handle.ToInt32();
					xSrc = 0;
					ySrc = 0;
					NNWIDTH = Convert.ToInt32((float) (Image2.Width * 15));
					NNHEIGHT = Convert.ToInt32((float) (Image2.Height * 15));
					//UPGRADE_WARNING: (7003) The Hdc should be released once it is used for safety More Information: http://www.vbtonet.com/ewis/ewi7003.aspx
					//UPGRADE_WARNING: (1037) Couldn't resolve default property of object HDEST. More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
					HDEST = Image1.CreateGraphics().GetHdc().ToInt32();

					//UPGRADE_WARNING: (1068) HDEST of type Variant is being forced to int. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
					SUC = Imagenes_IMDHSupport.PInvoke.SafeNative.gdi32.BitBlt(Convert.ToInt32(HDEST), 0, 0, NNWIDTH, NNHEIGHT, HSRC, xSrc, ySrc, 0xCC0020);

					//UPGRADE_ISSUE: (2064) PictureBox property Image1.Image was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					Image1.Image = Image1.getImage();
					//UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
					Image1.Image.Save(Nombre_Archivo);

					Image1.Image = null;
					//Set Image2.Picture = Nothing

					iEImgFact.setImage(Nombre_Archivo);
					iEImgFact.Display();
				}
                
				//N� de Registro
				//stNumRegistro = VstNumRef
				NUEVO_ACTUALIZA = false;
				lbZoom.Text = "Zoom: " + iEImgFact.Zoom.ToString();
				lbNumeracion.Text = Module1.Referen;
				//Inicializar coordenadas de selecci�n
				lIzda = 0;
				lArriba = 0;
				lAncho = 0;
				lAlto = 0;

				//Valores para seleccionar
				//Dibujar el recuadro de selecci�n
				iEImgFact.SelectionRectangle = true;
				iEImgFact.MousePointer = ImgeditLibCtl.MousePointerConstants.wiMPDefault;
				tbImagenes.Items["ACERC"].Enabled = false;
				tbImagenes.Items["CORTAR"].Enabled = false;
				tbImagenes.Items["COPIAR"].Enabled = false;
				tbImagenes.Items["PEGAR"].Enabled = false;
				tbImagenes.Items["IMAGENNUEVA"].Enabled = false;
				tbImagenes.Items["PINTAR"].Visible = false;
				tbImagenes.Items["HERRAMIENTAS"].Visible = false;
				//Optimizar la resoluci�n dependiendo del tipo de
				//imagen a visualizar
				iEImgFact.DisplayScaleAlgorithm = ImgeditLibCtl.DisplayScaleConstants.wiScaleOptimize;

				if (!iEImgFact.ImageDisplayed)
				{
					tbImagenes.Items["ELIM"].Enabled = false; // bot�n de eliminar
					tbImagenes.Items["GRAB"].Enabled = false; // actualizar
					tbImagenes.Items["CANC"].Enabled = false; // cancelar
					tbImagenes.Items["IMPR"].Enabled = false;
					tbImagenes.Items["CORTAR"].Enabled = false;
					tbImagenes.Items["COPIAR"].Enabled = false;
					tbImagenes.Items["IMAGENNUEVA"].Enabled = false;
					tbImagenes.Items["PEGAR"].Enabled = false;
					tbImagenes.Items["SELECC"].Enabled = false;
					tbImagenes.Items["ARRAS"].Enabled = false;
					tbImagenes.Items["ZOOMIN"].Enabled = false;
					tbImagenes.Items["ZOOMOUT"].Enabled = false;
					tbImagenes.Items["ACERC"].Enabled = false;
					tbImagenes.Items["IZQ"].Enabled = false;
					tbImagenes.Items["DER"].Enabled = false;
				}

				if (!iEImgFact.IsClipboardDataAvailable())
				{
					//No hay informaci�n en el Clipboard
					tbImagenes.Items["PEGAR"].Enabled = false;
					tbImagenes.Items["IMAGENNUEVA"].Enabled = false;
				}
				this.Cursor = Cursors.Default;
                */
			}
			catch
			{

				this.Cursor = Cursors.Default;
				RadMessageBox.Show("Error al cargar las im�genes de la factura." + "\n" + "\r" + 
				                Conversion.ErrorToString() + ".", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
			}

		}

		private void ImgRFact_Closed(Object eventSender, EventArgs eventArgs)
		{

			try
			{
				string DIRECTORIO = String.Empty;
				DIRECTORIO = (Path.GetDirectoryName(Application.ExecutablePath).Trim().Substring(Math.Max(Path.GetDirectoryName(Application.ExecutablePath).Trim().Length - 1, 0)) == "\\") ? Path.GetDirectoryName(Application.ExecutablePath) : Path.GetDirectoryName(Application.ExecutablePath) + "\\";
				//Borrar todos los ficheros temporales generados
				File.Delete(Nombre_Archivo);
			}
			catch
			{

				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 53)
				{
                    //Archivo no encontrado
                    //UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                    /*INDRA jproche_TODO_X_5 25/05/2016
					UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
                    */
				}
				MemoryHelper.ReleaseMemory();
			}

		}


		public int BuscarImagenes(string NumReg)
		{
			int result = 0;
			int HSRC = 0;
			object HDEST = null;
			int SUC = 0;
			//*************************************************************
			//Accede a la tabla FIMAGS04 para mostrar las im�genes asociadas
			//con el NumReg(S04_REF).
			//Devuelve 1 si la operaci�n se realiza con �xito
			//         Err en caso de error
			//*************************************************************

			// On Error GoTo Error_BuscarImagenes

			//Variables necesarias para recuperar 'por trozos'
			//el fichero de im�genes
			int iNumFich = 0;
			byte[] bContenidoDelTrozo = null;
			string stSQL = "SELECT S04_IMGLON,S04_IMAGEN " + 
			               "FROM FIMAGS04 " + 
			               "WHERE S04_REF = '" + NumReg + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQL, Module1.BaseRDO);
			DataSet RrImagenes = new DataSet();
			tempAdapter.Fill(RrImagenes);

			//RrImagenes.MoveFirst
			int ii = 0;
			int xSrc = 0;
			int ySrc = 0;
			int NNWIDTH = 0;
			int NNHEIGHT = 0;
			if (RrImagenes.Tables[0].Rows.Count == 0)
			{
				RrImagenes.Close();
				return 0;
			}
			else
			{
				//Crear un fichero intermedio en el que se
				//va recuperando por trozos la informaci�n
				//del campo de la B.D. de tipo imagen
				ii = Convert.ToInt32(RrImagenes.Tables[0].Rows[0]["s04_imglon"]);
				bContenidoDelTrozo = new byte[ii + 1];
				bContenidoDelTrozo = (byte[]) RrImagenes.Tables[0].Rows[0]["s04_imagen"];
				RrImagenes.Close();

				FileSystem.FileOpen(1, Nombre_Archivo + "a", OpenMode.Binary, OpenAccess.Write, OpenShare.Default, -1);
				FileSystem.FilePutObject(1, bContenidoDelTrozo, 0);
				FileSystem.FileClose(1);

				bContenidoDelTrozo = new byte[1];
				//primero tengo que pasarlo a un image control por si es un icono
				Image1.Image = Image.FromFile(Nombre_Archivo + "a");
				//ahora lo vuelvo a grabar como si fuera un bmp
				Image2.Image = Image1.Image;

				Image1.Width = (int) Image2.Width;
				Image1.Height = (int) Image2.Height;
				HSRC = Image1.Handle.ToInt32();
				xSrc = 0;
				ySrc = 0;
				NNWIDTH = Convert.ToInt32((float) (Image2.Width * 15));
				NNHEIGHT = Convert.ToInt32((float) (Image2.Height * 15));
				HDEST = Image1.CreateGraphics().GetHdc().ToInt32();

				//UPGRADE_WARNING: (1068) HDEST of type Variant is being forced to int. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
				SUC = UpgradeSupportHelper.PInvoke.SafeNative.gdi32.BitBlt(Convert.ToInt32(HDEST), 0, 0, NNWIDTH, NNHEIGHT, HSRC, xSrc, ySrc, 0xCC0020);

                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				//UPGRADE_ISSUE: (2064) PictureBox property Image1.Image was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Image1.Image = Image1.getImage();

				//UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				Image1.Image.Save(Nombre_Archivo + "a");
				Image1.Image = null;

				//Limpiar la imagen

				if (iEImgFact.ImageDisplayed)
				{
					iEImgFact.DeleteImageData(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight);
				}
				iEImgFact.ClearDisplay();
				iEImgFact.setImage("");
				iEImgFact.DisplayBlankImage(1, 1, null, null, null);
				iEImgFact.setImage(Nombre_Archivo + "a");
				// iEImgFact.Refresh
				//Ajustar la imagen al ancho de la ventana
				//iEImgFact.FitTo 1, True

				iEImgFact.Display();
				lbZoom.Text = "Zoom: " + iEImgFact.Zoom.ToString();
				//Kill Nombre_Archivo & "a"
                */
                }
                result = 1;
			tbImagenes.Items["GRAB"].Enabled = false; // actualizar 5
			tbImagenes.Items["CANC"].Enabled = false; // cancelar 6
			return result;


			RadMessageBox.Show("Error al comprobar las im�genes de las facturas registradas." + "\n" + "\r" + 
			                Conversion.ErrorToString() + ".", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
			FileSystem.FileClose(iNumFich);
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrImagenes.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrImagenes.Close();
			//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			return Information.Err().Number;

		}

        /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
		private void iEImgFact_SelectionRectDrawn(Object eventSender, AxImgeditLibCtl._DImgEditEvents_SelectionRectDrawnEvent eventArgs)
		{

			//Actualizar coordenadas de selecci�n
			lIzda = eventArgs.left;
			lArriba = eventArgs.top;
			lAncho = eventArgs.width;
			lAlto = eventArgs.height;

			tbImagenes.Items["ACERC"].Enabled = true;
			tbImagenes.Items["CORTAR"].Enabled = true;
			tbImagenes.Items["COPIAR"].Enabled = true;
            
			if (iEImgFact.IsClipboardDataAvailable())
			{
				//Hay informaci�n en el Clipboard
				tbImagenes.Items["PEGAR"].Enabled = true;
				tbImagenes.Items["IMAGENNUEVA"].Enabled = true;
			}
            
		}
        */



        private void tbImagenes_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
			ToolStripItem Button = (ToolStripItem) eventSender;
            //barra de modificacion de imagenes
            /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar

			try
			{

				object switchVar = Button.Name;
				if (Convert.ToString(switchVar) == "PINTAR")
				{
					//muestra la paleta de anotaciones

					iEImgFact.ShowAnnotationToolPalette(null, null, null, null);
					//actiba el boton de herramientas
					tbImagenes.Items["HERRAMIENTAS"].Enabled = true;
					tbImagenes.Items["CANC"].Enabled = true;
				}
				else if (Convert.ToString(switchVar) == "HERRAMIENTAS")
				{ 
					//?se podria controlar que opcion de la paleta hemos seleccionado
					//segun la anotacion seleccionada se muestra el dialogo correspondiente
					iEImgFact.ShowAttribsDialog(null);
				}
				else if (Convert.ToString(switchVar) == "IMAGENNUEVA")
				{ 
					if (Module1.stTipoLlamada == "IMDH")
					{
						if (iEImgFact.ImageDisplayed)
						{
							iEImgFact.DeleteImageData(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight);
						}
						iEImgFact.ClearDisplay();
						//UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
						Clipboard.GetData(null).Save(Nombre_Archivo + "M");
						iEImgFact.setImage(Nombre_Archivo + "M");
						iEImgFact.Display();
						if (iEImgFact.ImageDisplayed)
						{
							ActivarBotonesIMDH(true);
							tbImagenes.Items["CANC"].Enabled = true; // cancelar
						}
					}
					else
					{
						if (iEImgFact.ImageDisplayed)
						{
							iEImgFact.DeleteImageData(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight);
						}
						iEImgFact.ClearDisplay();
						Image2.Image = Clipboard.GetData(null);
						//UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
						Clipboard.GetData(null).Save(Nombre_Archivo);
						iEImgFact.setImage(Nombre_Archivo);
						iEImgFact.Display();
						File.Delete(Nombre_Archivo);
						if (iEImgFact.ImageDisplayed)
						{
							tbImagenes.Items["GRAB"].Enabled = true; // actualizar
							tbImagenes.Items["CANC"].Enabled = true; // cancelar
						}
						if (iEImgFact.ImageDisplayed)
						{
							tbImagenes.Items["ELIM"].Enabled = true; // bot�n de eliminar
							tbImagenes.Items["GRAB"].Enabled = true; // actualizar
							tbImagenes.Items["IMPR"].Enabled = true;
							tbImagenes.Items["CORTAR"].Enabled = true;
							tbImagenes.Items["COPIAR"].Enabled = true;
							tbImagenes.Items["PEGAR"].Enabled = true;
							tbImagenes.Items["IMAGENNUEVA"].Enabled = true;
							tbImagenes.Items["SELECC"].Enabled = true;
							tbImagenes.Items["ARRAS"].Enabled = true;
							tbImagenes.Items["ZOOMIN"].Enabled = true;
							tbImagenes.Items["ZOOMOUT"].Enabled = true;
							tbImagenes.Items["ACERC"].Enabled = true;
							tbImagenes.Items["IZQ"].Enabled = true;
							tbImagenes.Items["DER"].Enabled = true;
						}
						lbNumeracion.Tag = "0000000000";
						Module1.Referen = Convert.ToString(lbNumeracion.Tag);
						lbNumeracion.Text = Module1.Referen;
					}
				}
				else if (Convert.ToString(switchVar) == "VOLVER")
				{ 
					if (Module1.stTipoLlamada == "IMDH")
					{
						if (!Module1.fModoLectura)
						{ //se puede modificar la imagen
							if (iEImgFact.ImageDisplayed)
							{
								//si hay una imagen preguntamos si queremos guardar los cambios
								if (RadMessageBox.Show("�Desea guardar los cambios?", Module1.vstNomAplicacion, MessageBoxButtons.YesNo, RadMessageIcon.Exclamation, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
								{
									//si si queremos salvar los cambios
									iEImgFact.BurnInAnnotations(1, 2, null);
									//las opciones de file type pueden ser 3.bmp, 6.jpg
									iEImgFact.SaveAs(Nombre_Archivo, 3, null, null, null, null);
									iEImgFact.setImage(Nombre_Archivo);
									GuardarEnBD(Module1.stIdImagen);
								}
								else
								{
									//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
									Module1.HAY_Imagen.Picture = Image2.Image;
									Module1.stVueltaIMDH = "";
									return;
								}
							}
							else
							{
								//no hay imagen en la pantalla
								if (Module1.stFicheroTemporal.Trim() == "")
								{
									RadMessageBox.Show("no habia imagen y no la hay", Application.ProductName);
								}
								else
								{
									RadMessageBox.Show("habia imagen y no la hay", Application.ProductName);
								}
							}
						}
						else
						{
							//si es en modo consulta este boton no estara
						}
						//CerrarImagen
					}
					else
					{
						if (iEImgFact.ImageDisplayed)
						{
							if (iEImgFact.ImageModified)
							{
								iEImgFact.Zoom = 100;
								iEImgFact.ScrollImage(1, 999999999);
								iEImgFact.ScrollImage(3, 999999999);
								iEImgFact.ClipboardCopy(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight);
								//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
								Module1.HAY_Imagen.Picture = Clipboard.GetData(null);
							}
							else
							{
								//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
								Module1.HAY_Imagen.Picture = Image2.Image;
							}
							Module1.Referen = Convert.ToString(lbNumeracion.Tag);
						}
						else
						{
							//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Module1.HAY_Imagen.Picture = null;
							Module1.Referen = "NADA";
						}
						CerrarImagen();
					}
				}
				else if (Convert.ToString(switchVar) == "NUEVO")
				{  //Insertar imagen
					//LIMPIA Y DA ESPLEDOR, EL EDIT
					iEImgFact.ClearDisplay();
					//ME DESAPAREZCO Y ME APARECE EL
					//TOOLBAR 2
					NUEVO_ACTUALIZA = true;
					Toolbar1.Visible = true;
					tbImagenes.Visible = false;
					scbPaginas.Enabled = false;
					//             If stTipoLlamada = "IMDH" Then
					//                Toolbar1.Buttons("GRAB").Enabled = False
					//                Toolbar1.Buttons("CANC").Enabled = False
					//                Toolbar1.Buttons("BUSCAR").Visible = False
					//                Toolbar1.Buttons("IZQ").Enabled = False
					//                Toolbar1.Buttons("DER").Enabled = False
					//             End If
				}
				else if (Convert.ToString(switchVar) == "ELIM")
				{  // Eliminar Imagen
					if (Module1.stTipoLlamada == "IMDH")
					{
						//mensaje de imdh
						if (RadMessageBox.Show("La Imagen ser� eliminada. �Desea continuar?", Module1.vstNomAplicacion, MessageBoxButtons.YesNo, RadMessageIcon.Info, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
						{
							if (iEImgFact.ImageDisplayed)
							{
								iEImgFact.DeleteImageData(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight);
							}
							iEImgFact.ClearDisplay();
							//desactivamos los botones
							ActivarBotonesIMDH(false);
						}
						else
						{
							return;
						}
					}
					else
					{
						//lo pongo en la esquina superior-izqda
						if (RadMessageBox.Show("Se va a Borrar la Imagen. �Continuar?", "Borrar", MessageBoxButtons.YesNo, RadMessageIcon.Info) == System.Windows.Forms.DialogResult.No)
						{
							return;
						}

						if (iEImgFact.ImageDisplayed)
						{
							iEImgFact.DeleteImageData(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight);
						}
						iEImgFact.ClearDisplay();
						tbImagenes.Items["ELIM"].Enabled = false; // elimina
						tbImagenes.Items["GRAB"].Enabled = false; // actualiza
						tbImagenes.Items["CANC"].Enabled = true; // cancela
						tbImagenes.Items["IMPR"].Enabled = false;
						tbImagenes.Items["CORTAR"].Enabled = false;
						tbImagenes.Items["COPIAR"].Enabled = false;
						tbImagenes.Items["PEGAR"].Enabled = false;
						tbImagenes.Items["IMAGENNUEVA"].Enabled = false;
						tbImagenes.Items["SELECC"].Enabled = false;
						tbImagenes.Items["ARRAS"].Enabled = false;
						tbImagenes.Items["ACERC"].Enabled = false;
						tbImagenes.Items["ZOOMIN"].Enabled = false;
						tbImagenes.Items["ZOOMOUT"].Enabled = false;
						tbImagenes.Items["IZQ"].Enabled = false;
						tbImagenes.Items["DER"].Enabled = false;
						lbNumeracion.Tag = "0000000000";
					}
				}
				else if (Convert.ToString(switchVar) == "GRAB")
				{  //Actualizar Imagen
					//    If iEImgFact.ImageDisplayed Then
					//         NUEVO_ACTUALIZA = False
					//         Call SalvarenBD
					//         NUEVO_ACTUALIZA = True
					//         tbImagenes.Buttons("GRAB").Enabled = False ' actualizar
					//         tbImagenes.Buttons("CANC").Enabled = False ' cancelar
					//    End If
				}
				else if (Convert.ToString(switchVar) == "CANC")
				{  // cancelar cambios
					if (Module1.stTipoLlamada == "IMDH")
					{
						if (iEImgFact.ImageDisplayed)
						{
							iEImgFact.DeleteImageData(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight);
						}
						iEImgFact.ClearDisplay();
						iEImgFact.setImage(Nombre_Archivo);
						iEImgFact.Display();
						tbImagenes.Items["CANC"].Enabled = false;
					}
					else
					{

						iEImgFact.Display();
						tbImagenes.Items["GRAB"].Enabled = false; // actualizar
						tbImagenes.Items["CANC"].Enabled = false; // cancelar

						if (iEImgFact.ImageDisplayed)
						{
							tbImagenes.Items["ELIM"].Enabled = true; // bot�n de eliminar
							tbImagenes.Items["GRAB"].Enabled = true; // actualizar
							tbImagenes.Items["IMPR"].Enabled = true;
							tbImagenes.Items["CANC"].Enabled = false;
							tbImagenes.Items["CORTAR"].Enabled = true;
							tbImagenes.Items["COPIAR"].Enabled = true;
							if (iEImgFact.IsClipboardDataAvailable())
							{
								tbImagenes.Items["PEGAR"].Enabled = true;
								tbImagenes.Items["IMAGENNUEVA"].Enabled = true;
							}
							else
							{
								tbImagenes.Items["PEGAR"].Enabled = false;
								tbImagenes.Items["IMAGENNUEVA"].Enabled = false;
							}
							tbImagenes.Items["SELECC"].Enabled = true;
							tbImagenes.Items["ARRAS"].Enabled = true;
							tbImagenes.Items["ZOOMIN"].Enabled = true;
							tbImagenes.Items["ZOOMOUT"].Enabled = true;
							tbImagenes.Items["ACERC"].Enabled = true;
							tbImagenes.Items["IZQ"].Enabled = true;
							tbImagenes.Items["DER"].Enabled = true;
						}
						lbNumeracion.Tag = Module1.Referen;
					}
				}
				else if (Convert.ToString(switchVar) == "IMPR")
				{  //BHIM_IND_IMPRIMIR
					ImprimirImagen();

				}
				else if (Convert.ToString(switchVar) == "CORTAR")
				{  //BHIM_IND_CORTAR
					CortarImagen();
					if (iEImgFact.ImageDisplayed)
					{
						tbImagenes.Items["GRAB"].Enabled = true; // actualizar
						tbImagenes.Items["CANC"].Enabled = true; // cancelar
					}

				}
				else if (Convert.ToString(switchVar) == "COPIAR")
				{  //BHIM_IND_COPIAR
					CopiarImagen();

				}
				else if (Convert.ToString(switchVar) == "PEGAR")
				{  //BHIM_IND_PEGAR
					PegarImagen();
					if (iEImgFact.ImageDisplayed)
					{
						tbImagenes.Items["GRAB"].Enabled = true; // actualizar
						tbImagenes.Items["CANC"].Enabled = true; // cancelar
					}

				}
				else if (Convert.ToString(switchVar) == "SELECC")
				{  //BHIM_IND_SELECCIONAR
					//Dibujar el recuadro de selecci�n
					iEImgFact.SelectionRectangle = true;
					iEImgFact.MousePointer = ImgeditLibCtl.MousePointerConstants.wiMPDefault;

				}
				else if (Convert.ToString(switchVar) == "ARRAS")
				{  //BHIM_IND_ARRASTRAR
					//Quitar el cuadro de seleccion
					iEImgFact.DrawSelectionRect(0, 0, 0, 0);
					//No dibujar el recuadro de selecci�n
					iEImgFact.SelectionRectangle = false;
					iEImgFact.MousePointer = ImgeditLibCtl.MousePointerConstants.wiMPHand;
					tbImagenes.Items["ACERC"].Enabled = false;
					tbImagenes.Items["CORTAR"].Enabled = false;
					tbImagenes.Items["COPIAR"].Enabled = false;
					tbImagenes.Items["IMAGENNUEVA"].Enabled = false;
					tbImagenes.Items["PEGAR"].Enabled = false;

				}
				else if (Convert.ToString(switchVar) == "ZOOMIN")
				{  //BHIM_IND_ACERCAR
					ZoomAcercar();
				}
				else if (Convert.ToString(switchVar) == "ZOOMOUT")
				{  //BHIM_IND_ALEJAR
					ZoomAlejar();
				}
				else if (Convert.ToString(switchVar) == "ACERC")
				{  //BHIM_IND_ZOOMAREA
					ZoomArea();
				}
				else if (Convert.ToString(switchVar) == "IZQ")
				{  //BHIM_IND_ROTARIZDA
					RotarImagenIzda();
					if (iEImgFact.ImageDisplayed)
					{
						tbImagenes.Items["GRAB"].Enabled = true; // actualizar
						tbImagenes.Items["CANC"].Enabled = true; // cancelar
					}
					lbNumeracion.Tag = "0000000000";
				}
				else if (Convert.ToString(switchVar) == "DER")
				{  //BHIM_IND_ROTARDCHA
					RotarImagenDcha();
					if (iEImgFact.ImageDisplayed)
					{
						tbImagenes.Items["GRAB"].Enabled = true; // actualizar
						tbImagenes.Items["CANC"].Enabled = true; // cancelar
					}
					lbNumeracion.Tag = "0000000000";

				}
				else if (Convert.ToString(switchVar) == "SALIR")
				{  //BHIM_IND_SALIR
					CerrarImagen();

				}
				else if (Convert.ToDouble(switchVar) == 26)
				{  //BHIM_IND_AYUDA
				}
			}
			catch
			{
				RadMessageBox.Show(Conversion.ErrorToString() + ".", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
			}
            */

		}
		public void CortarImagen()
		{
			try
			{

				if (lAlto == 0 && lAncho == 0)
				{
					//No hay cuadro de selecci�n
					RadMessageBox.Show("Debe seleccionar el �rea a cortar.", "Error Edici�n", MessageBoxButtons.OK, RadMessageIcon.Error);
					return;
				}
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				iEImgFact.ClipboardCut(null, null, null, null);
				//Quitar el cuadro de seleccion
				iEImgFact.DrawSelectionRect(0, 0, 0, 0);
				if (iEImgFact.IsClipboardDataAvailable())
				{
					//Hay informaci�n en el Clipboard
					tbImagenes.Items["PEGAR"].Enabled = true;
					tbImagenes.Items["IMAGENNUEVA"].Enabled = true;
				}
                */
			}
			catch
			{

				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 1020)
				{
					RadMessageBox.Show("Debe seleccionar el �rea a cortar.", "Error Edici�n", MessageBoxButtons.OK, RadMessageIcon.Error);
					return;
				}
                /*INDRA jproche_TODO_X_5 25/05/2016
				//UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
				UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
                */
			}

		}
		public void CopiarImagen()
		{
            /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
			try
			{

				if (lAlto == 0 && lAncho == 0)
				{
					//No hay cuadro de selecci�n
					RadMessageBox.Show("Debe seleccionar el �rea a copiar.", "Error Edici�n", MessageBoxButtons.OK, RadMessageIcon.Error);
					return;
				}

				iEImgFact.ClipboardCopy(null, null, null, null);
				//Quitar el cuadro de seleccion
				iEImgFact.DrawSelectionRect(0, 0, 0, 0);
				if (iEImgFact.IsClipboardDataAvailable())
				{
					//Hay informaci�n en el Clipboard
					tbImagenes.Items["PEGAR"].Enabled = true;
					tbImagenes.Items["IMAGENNUEVA"].Enabled = true;
				}
			}
			catch
			{

				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 1020)
				{
					RadMessageBox.Show("Debe seleccionar el �rea a copiar.", "Error Edici�n", MessageBoxButtons.OK, RadMessageIcon.Error);
					return;
				}
				//UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
				UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");

			}
            */

            }

        public void PegarImagen()
		{

			try
			{
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				if (iEImgFact.IsClipboardDataAvailable())
				{
					if (lAlto == 0 && lAncho == 0)
					{
						//No hay cuadro de selecci�n
						RadMessageBox.Show("Debe seleccionar el �rea d�nde pegar.", "Error Edici�n", MessageBoxButtons.OK, RadMessageIcon.Error);
						return;
					}
					//Hay informaci�n en el Clipboard
					iEImgFact.ClipboardPaste(lIzda, lArriba);
					//iEImgFact.CompletePaste
					//Quitar el cuadro de seleccion
					//iEImgFact.DrawSelectionRect 0, 0, 0, 0

				}
                else
				{
					RadMessageBox.Show("No existe informaci�n para pegar.", "Error Edici�n", MessageBoxButtons.OK, RadMessageIcon.Error);
					return;
				}
                */
			}
			catch
			{

				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 1020)
				{
					RadMessageBox.Show("Debe seleccionar el �rea d�nde pegar.", "Error Edici�n", MessageBoxButtons.OK, RadMessageIcon.Error);
					return;
				}
                /*INDRA jproche_TODO_X_5 25/05/2016 
				//UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
				UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
                */
			}

		}
        /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
		private void iEImgFact_MouseDownEvent(Object eventSender, AxImgeditLibCtl._DImgEditEvents_MouseDownEvent eventArgs)
		{
			if (eventArgs.button == 1 && !iEImgFact.SelectionRectangle)
			{
				//Bot�n izquierdo y no est� seleccionando.
				//Guardar las coordenadas
				sXpulsada = eventArgs.x;
				sYpulsada = eventArgs.y;
			}

		}
        */

                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
            float sXmove_iEImgFact_MouseMoveEvent = 0;
            float sYmove_iEImgFact_MouseMoveEvent = 0;
            private void iEImgFact_MouseMoveEvent(Object eventSender, AxImgeditLibCtl._DImgEditEvents_MouseMoveEvent eventArgs)
            {

                //Para determinar si ha dejado de mover el rat�n
                //Direcci�n en la que se realiza el scroll de la imagen
                int iDireccion = 0;

                if (eventArgs.button == ((short) MouseButtons.Left) && !iEImgFact.SelectionRectangle)
                {
                    //Bot�n izquierdo presionado y no se est� seleccionando

                    if (sXmove_iEImgFact_MouseMoveEvent == eventArgs.x && sYmove_iEImgFact_MouseMoveEvent == eventArgs.y)
                    {
                        return;
                    } //Ha parado
                    //Movimiento de la coordenada X
                    if (sXpulsada - eventArgs.x < 0)
                    {
                        iDireccion = 3; //SCROLL_HACIA_IZQUIERDA
                    }
                    else
                    {
                        iDireccion = 2; //SCROLL_HACIA_DERECHA
                    }
                    iEImgFact.ScrollImage((short) iDireccion, Convert.ToInt32(Math.Abs(sXpulsada - eventArgs.x) / 72)); //DE_TWIPS_A_PIXELS
                    //Movimiento de la coordenada Y
                    if (sYpulsada - eventArgs.y < 0)
                    {
                        iDireccion = 1; //SCROLL_HACIA_ARRIBA
                    }
                    else
                    {
                        iDireccion = 0; //SCROLL_HACIA_ABAJO
                    }
                    iEImgFact.ScrollImage((short) iDireccion, Convert.ToInt32(Math.Abs(sYpulsada - eventArgs.y) / 72)); //DE_TWIPS_A_PIXELS
                    //Guardar las coordenadas anteriores
                    sXmove_iEImgFact_MouseMoveEvent = eventArgs.x;
                    sYmove_iEImgFact_MouseMoveEvent = eventArgs.y;
                }

            }
            */


        private void Toolbar1_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
            /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
			bool Error_Abrir = false;
			ToolStripItem Button = (ToolStripItem) eventSender;
			object AaA = null;
			int xxx = 0;
			int iTipoFich = 0;
			try
			{
				switch(Button.Name)
				{
					case "GRAB" :  // aceptar 
						//Call SalvarenBD 
						Toolbar1.Visible = false; 
						Toolbar1.Items["GRAB"].Enabled = false; 
						if (Module1.stTipoLlamada == "IMDH")
						{

							ActivarBotonesIMDH(iEImgFact.ImageDisplayed);
						}
						else
						{
							if (iEImgFact.ImageDisplayed)
							{
								tbImagenes.Items["ELIM"].Enabled = true; // bot�n de eliminar
								tbImagenes.Items["GRAB"].Enabled = true; // actualizar
								tbImagenes.Items["IMPR"].Enabled = true;
								tbImagenes.Items["CORTAR"].Enabled = true;
								tbImagenes.Items["COPIAR"].Enabled = true;
								tbImagenes.Items["PEGAR"].Enabled = true;
								tbImagenes.Items["IMAGENNUEVA"].Enabled = true;
								tbImagenes.Items["SELECC"].Enabled = true;
								tbImagenes.Items["ARRAS"].Enabled = true;
								tbImagenes.Items["ZOOMIN"].Enabled = true;
								tbImagenes.Items["ZOOMOUT"].Enabled = true;
								tbImagenes.Items["ACERC"].Enabled = true;
								tbImagenes.Items["IZQ"].Enabled = true;
								tbImagenes.Items["DER"].Enabled = true;
							}
							else
							{
								tbImagenes.Items["ELIM"].Enabled = false; // bot�n de eliminar
								tbImagenes.Items["GRAB"].Enabled = false; // actualizar
								tbImagenes.Items["CANC"].Enabled = false; // cancelar
								tbImagenes.Items["IMPR"].Enabled = false;
								tbImagenes.Items["CORTAR"].Enabled = false;
								tbImagenes.Items["COPIAR"].Enabled = false;
								tbImagenes.Items["PEGAR"].Enabled = false;
								tbImagenes.Items["IMAGENNUEVA"].Enabled = false;
								tbImagenes.Items["SELECC"].Enabled = false;
								tbImagenes.Items["ARRAS"].Enabled = false;
								tbImagenes.Items["ZOOMIN"].Enabled = false;
								tbImagenes.Items["ZOOMOUT"].Enabled = false;
								tbImagenes.Items["ACERC"].Enabled = false;
								tbImagenes.Items["IZQ"].Enabled = false;
								tbImagenes.Items["DER"].Enabled = false;
							}
						} 
						iEImgFact.ScrollImage(1, 999999999); 
						iEImgFact.ScrollImage(3, 999999999); 
						iEImgFact.ClipboardCopy(0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight); 
						iEImgFact.DrawSelectionRect(0, 0, 0, 0); 
						Image2.Image = Clipboard.GetData(null); 
						tbImagenes.Visible = true; 
						if (Module1.stTipoLlamada == "IMDH")
						{
							//Tenemos que guardar la imagen en el lugar adecuado
							//UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
							Clipboard.GetData(null).Save(Nombre_Archivo);
							iEImgFact.setImage(Nombre_Archivo);
							Clipboard.Clear();
							tbImagenes.Items["PEGAR"].Enabled = false;
							tbImagenes.Items["IMAGENNUEVA"].Enabled = false;
						}
						else
						{
							Module1.Referen = Convert.ToString(lbNumeracion.Tag);
							lbNumeracion.Text = Module1.Referen;
							File.Delete(Nombre_Archivo);
							if (FileSystem.Dir(Nombre_Archivo + "a", FileAttribute.Normal) != "")
							{
								FileSystem.Rename(Nombre_Archivo + "a", Nombre_Archivo);
							}
						} 
						break;
					case "CANC" :  // cancelar 
						//If scbPaginas.Max = 0 Then ' no hay datos 
						//    iEImgFact.ClearDisplay 
						//Else 
						//    If iEImgFact.ImageDisplayed Then 
						//        iEImgFact.DeleteImageData 0, 0, iEImgFact.ImageWidth, iEImgFact.ImageHeight 
						//    End If 
						//    Dim DIRECTORIO As String 
						//    If Right(Trim(App.Path), 1) = "\" Then DIRECTORIO = App.Path Else DIRECTORIO = App.Path & "\" 
						//   iEImgFact.Image = DIRECTORIO & "pictemp.tif" '& scbPaginas.value 
						//   iEImgFact.Display 
						//End If 
						//si es IMDH el boton no estara activo 
						if (FileSystem.Dir(Nombre_Archivo + "a", FileAttribute.Normal) != "")
						{
							File.Delete(Nombre_Archivo + "a");
						} 
						Toolbar1.Items["GRAB"].Enabled = false; 
						iEImgFact.setImage(Nombre_Archivo); 
						iEImgFact.Display(); 
						Toolbar1.Visible = false; 
						tbImagenes.Visible = true; 
						lbNumeracion.Tag = Module1.Referen; 
						//scbPaginas.Enabled = True 
						 
						break;
					case "ESCANER" :  // Escaner disponible 
						ImgScan1.ShowSelectScanner(); 
						break;
					case "BUSCAR" : 
						if (Module1.HAY_Clasificacion != "")
						{
							AaA = Recupera_Datos_M�dulo.Recupera_Datos(Module1.BaseRDO, Module1.HAY_Clasificacion);
						}
						else
						{
							AaA = Recupera_Datos_M�dulo.Recupera_Datos(Module1.BaseRDO, "IMAGEN");
						} 
						if (Convert.IsDBNull(AaA))
						{
							return;
						} 
						if (Convert.ToString(((Array) AaA).GetValue(0)) == "")
						{
							return;
						} 
						xxx = BuscarImagenes(Convert.ToString(((Array) AaA).GetValue(0))); 
						lbNumeracion.Tag = Convert.ToString(((Array) AaA).GetValue(0)); 
						Toolbar1.Items["GRAB"].Enabled = true; 
						//tbFiltro1_1.Text = var_dato(0) '(0) c�digo (1) 'descripcion 
						 
						break;
					case "ESCAN" :  // escanear 
						iEImgFact.ImageControl = "iEImgFact"; 
						ImgScan1.DestImageControl = "iEImgFact"; 
						ImgScan1.StartScan(); 
						Toolbar1.Items["GRAB"].Enabled = true; 
						lbNumeracion.Tag = "0000000000"; 
						break;
					case "ABRIR" : 
						Error_Abrir = true; 
						//Establecer el filtro de los archivos 
						//UPGRADE_WARNING: (2081) Filter has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx 
						CommonDialog1Open.Filter = "Todos los archivos de imagen|*.bmp;*.dcx;*.pcx;*.jpg;*.jpeg;*.tif;*.xif;*.gif" + 
						                           "|Documento TIFF (*.tif)|*.tif" + 
						                           "|Imagen de mapa de bits (*.bmp)|*.bmp" + 
						                           "|Archivo JPG (*.jpg)|*.jpg;*.jpeg" + 
						                           "|Documento PCX/DCX (*.pcx;*.dcx)|*.pcx;*.dcx" + 
						                           "|Documento XIF (*.xif)|*.xif" + 
						                           "|Todos los archivos (*.*)|*.*"; 
						 
						CommonDialog1Open.ShowDialog(); 
						iEImgFact.setImage(CommonDialog1Open.FileName); 
						iTipoFich = CommonDialog1Open.FilterIndex; 
						this.Activate(); 
						iEImgFact.Display(); 
						if (iEImgFact.ImageDisplayed)
						{
							Toolbar1.Items["GRAB"].Enabled = true;
							Toolbar1.Items["IZQ"].Enabled = true; //A�ADIDO IMDH
							Toolbar1.Items["DER"].Enabled = true; //A�ADIDO IMDH
						} 
						lbNumeracion.Tag = "0000000000"; 
						break;
					case "IZQ" :  //BHIM_IND_ROTARIZDA 
						RotarImagenIzda(); 
						if (iEImgFact.ImageDisplayed)
						{
							Toolbar1.Items["GRAB"].Enabled = true;
						} 
						lbNumeracion.Tag = "0000000000"; 
						break;
					case "DER" :  //BHIM_IND_ROTARDCHA 
						RotarImagenDcha(); 
						if (iEImgFact.ImageDisplayed)
						{
							Toolbar1.Items["GRAB"].Enabled = true;
						} 
						lbNumeracion.Tag = "0000000000"; 
						break;
				}
			}
			catch (Exception excep)
			{
				if (!Error_Abrir)
				{
					throw excep;
				}
				if (Error_Abrir)
				{	
					if (Information.Err().Number == 321)
					{
						RadMessageBox.Show("Archivo de imagen no v�lido.", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
						return;
					}
				}
			}
            */
		}

		private void GuardarEnBD(string NombreFichero)
		{
			//rutina IMDH para que guarde la imagen en la BD.
			string sql = String.Empty;
			DataSet Rrdatos = null;

			int DataFile = 0, Chunks = 0;
			int Fl = 0;
			int Fragment = 0;
			byte[] Chunk = null;

			try
			{

				const int ChunkSize = 16384;

				//hay que guardar la imagen en un fichero temporal
				//SavePicture Image2.Picture, Nombre_Archivo
				//SavePicture HAY_Imagen.Picture, Nombre_Archivo
				//buscamos la imagen en la tabla
				sql = "select * from DIMAGMED where gnombimg ='" + NombreFichero + "' ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Module1.BaseRDO);
				Rrdatos = new DataSet();
				tempAdapter.Fill(Rrdatos);
				if (Rrdatos.Tables[0].Rows.Count != 0)
				{
					//si esta select devuelve algun registro tiene que insertar en la tabla de modificaciones
					sql = "Insert into DIMAGMOD select getdate(),* from DIMAGMED where gnombimg = '" + NombreFichero + "'";

					SqlCommand tempCommand = new SqlCommand(sql, Module1.BaseRDO);
					tempCommand.ExecuteNonQuery();
					Rrdatos.Edit();
				}
				else
				{	
					Rrdatos.AddNew();
				}
				Rrdatos.Tables[0].Rows[0]["gnombimg"] = NombreFichero;
				Rrdatos.Tables[0].Rows[0]["gusuario"] = Module1.gUsuario;
				DataFile = 1;
				FileSystem.FileOpen(DataFile, Nombre_Archivo, OpenMode.Binary, OpenAccess.Read, OpenShare.Default, -1);
				Fl = (int) FileSystem.LOF(DataFile); // Longitud de los datos en el archivo
				if (Fl == 0)
				{
					FileSystem.FileClose(DataFile);
					return;
				}

				Chunks = Fl / ChunkSize;

				Fragment = Fl % ChunkSize;

				Chunk = new byte[Fragment + 1];

				Array TempArray = Array.CreateInstance(Chunk.GetType().GetElementType(), Chunk.Length);
				FileSystem.FileGet(DataFile, ref TempArray, -1, false, false);
				Array.Copy(TempArray, Chunk, TempArray.Length);

                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				//UPGRADE_ISSUE: (2064) RDO._rdoColumn method Rrdatos.AppendChunk was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Rrdatos.Tables[0].Rows[0]["oimgmedi"].AppendChunk(Chunk);
                */

				Chunk = new byte[ChunkSize + 1];

				for (int i = 1; i <= Chunks; i++)
				{
					//UPGRADE_WARNING: (2080) Get was upgraded to FileGet and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
					Array TempArray2 = Array.CreateInstance(Chunk.GetType().GetElementType(), Chunk.Length);
					FileSystem.FileGet(DataFile, ref TempArray2, -1, false, false);
					Array.Copy(TempArray2, Chunk, TempArray2.Length);
                    /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_ISSUE: (2064) RDO._rdoColumn method Rrdatos.AppendChunk was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					Rrdatos.Tables[0].Rows[0]["oimgmedi"].AppendChunk(Chunk);
                    */
				}
				FileSystem.FileClose(DataFile);

				string tempQuery = Rrdatos.Tables[0].TableName;
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
				tempAdapter_2.Update(Rrdatos, Rrdatos.Tables[0].TableName);

				Rrdatos.Close();
				Module1.stVueltaIMDH = "OK";
			}
			catch
			{	
				Module1.stVueltaIMDH = "Error" + Information.Err().Number.ToString();
				//borramos el fichero temporal para que no quede guarreria
				// Kill nombre_archivo
			}
		}


		private void CargarLLamadaIMDH()
		{
			int HSRC = 0;
			object HDEST = null;
			int SUC = 0;
			//funcion para cargar la pantalla segun IMDH

			try
			{
                /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
				scbPaginas.Maximum = (0 + scbPaginas.LargeChange - 1);
				scbPaginas.Visible = false;
				lbPaginas.Visible = false;
				lbNumeracion.Visible = false;
				Module1.Referen = Module1.stIdImagen;

				if (Module1.fModoLectura)
				{
					Toolbar1.Visible = false;
					tbImagenes.Visible = false;
					Toolbar2.Items["CANC"].Enabled = false;
					Toolbar2.Visible = true;

				}
				else
				{
					Toolbar2.Visible = false;
					tbImagenes.Visible = true;
					Toolbar1.Visible = false;
					//
					//    If HAY_Imagen.Picture <> 0 Then
					//        tbImagenes.Visible = True
					//        Toolbar1.Visible = False
					//    Else
					//        tbImagenes.Visible = False
					//        Toolbar1.Visible = True
					//    End If
				}

				if (Module1.stFicheroTemporal.Trim() == "")
				{
					//crea un fichero temporal
					Nombre_Archivo = Module1.Archivo();
					FileSystem.FileOpen(1, Nombre_Archivo, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);
					FileSystem.FileClose();
				}
				else
				{
					Nombre_Archivo = Module1.stFicheroTemporal.Trim();
				}

				int xSrc = 0;
				int ySrc = 0;
				int NNWIDTH = 0;
				int NNHEIGHT = 0;
				if (Convert.ToDouble(Module1.HAY_Imagen.Picture) != 0)
				{
					//si en el picture tenemos una imagen guardada
					//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Image1.Image = (Image) Module1.HAY_Imagen.Picture;
					//aqu� mantendr� la picture original
					//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Image2.Image = (Image) Module1.HAY_Imagen.Picture;

					Image1.Width = (int) Image2.Width;
					Image1.Height = (int) Image2.Height;
					HSRC = Image1.Handle.ToInt32();
					xSrc = 0;
					ySrc = 0;
					NNWIDTH = Convert.ToInt32((float) (Image2.Width * 15));
					NNHEIGHT = Convert.ToInt32((float) (Image2.Height * 15));
					HDEST = Image1.CreateGraphics().GetHdc().ToInt32();

					SUC = Imagenes_IMDHSupport.PInvoke.SafeNative.gdi32.BitBlt(Convert.ToInt32(HDEST), 0, 0, NNWIDTH, NNHEIGHT, HSRC, xSrc, ySrc, 0xCC0020);

					Image1.Image = Image1.getImage();
					Image1.Image.Save(Nombre_Archivo);

					Image1.Image = null;

					iEImgFact.setImage(Nombre_Archivo);
					iEImgFact.Display();
				}
				else
				{
					//en el picture no hay imagen guardada

				}

				NUEVO_ACTUALIZA = false;
				lbZoom.Text = "Zoom: " + iEImgFact.Zoom.ToString();
				//Inicializar coordenadas de selecci�n
				lIzda = 0;
				lArriba = 0;
				lAncho = 0;
				lAlto = 0;

				//Valores para seleccionar
				//Dibujar el recuadro de selecci�n
				iEImgFact.SelectionRectangle = true;
				iEImgFact.MousePointer = ImgeditLibCtl.MousePointerConstants.wiMPDefault;

				//hacemos desaparecer los botones que no queremos
				Toolbar1.Items["CANC"].Visible = false;
				Toolbar1.Items["BUSCAR"].Visible = false;

				if (Module1.HAY_ESCANER == 1)
				{
					// si existen escaner habilito el bot�n si no no
					Toolbar1.Items["ESCANER"].Enabled = true; // selecci�n de escaner
					Toolbar1.Items["ESCAN"].Enabled = true; // escanear
				}
				else
				{
					//tbImagenes.Buttons(21).Enabled = False
					Toolbar1.Items["ESCANER"].Enabled = false; // selecci�n de escaner
					Toolbar1.Items["ESCAN"].Enabled = false; // escanear
				}

				if (!iEImgFact.ImageDisplayed)
				{
					ActivarBotonesIMDH(false);
				}
				else
				{
					ActivarBotonesIMDH(true);
				}
				iEImgFact.DisplayScaleAlgorithm = ImgeditLibCtl.DisplayScaleConstants.wiScaleOptimize;

				this.Cursor = Cursors.Default;
                */
			}
			catch
			{

				this.Cursor = Cursors.Default;
				RadMessageBox.Show("Error al cargar la imagen." + "\n" + "\r" + 
				                Conversion.ErrorToString() + ".", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
				Module1.stVueltaIMDH = "";
			}

		}

		private void Toolbar2_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
			ToolStripItem Button = (ToolStripItem) eventSender;
			//barra de herramientas de IMDH

			try
			{

				switch(Button.Name)
				{
					case "CANC" :  // cancelar cambios producidos por el volteo 
                        /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
						iEImgFact.Display(); 
                        */
						Toolbar2.Items["CANC"].Enabled = false; 
						break;
					case "IMPR" :  //BHIM_IND_IMPRIMIR 
						ImprimirImagen(); 
						 
						break;
					case "ZOOMIN" :  //BHIM_IND_ACERCAR 
						ZoomAcercar(); 
						 
						break;
					case "ZOOMOUT" :  //BHIM_IND_ALEJAR 
						ZoomAlejar(); 
						 
						break;
					case "ACERC" :  //BHIM_IND_ZOOMAREA 
						ZoomArea(); 
						break;
					case "IZQ" :  //BHIM_IND_ROTARIZDA 
						RotarImagenIzda(); 
						Toolbar2.Items["CANC"].Enabled = true; 
						break;
					case "DER" :  //BHIM_IND_ROTARDCHA 
						RotarImagenDcha(); 
						Toolbar2.Items["CANC"].Enabled = true; 
						break;
					case "SALIR" :  //BHIM_IND_SALIR 
						Module1.stVueltaIMDH = ""; 
						CerrarImagen(); 
						break;
				}
			}
			catch
			{
                RadMessageBox.Show(Conversion.ErrorToString() + ".", this.Text, MessageBoxButtons.OK, RadMessageIcon.Error);
			}

		}

		private void ActivarBotonesIMDH(bool fAccion)
		{

			tbImagenes.Items["NUEVO"].Enabled = true;
			tbImagenes.Items["ELIM"].Enabled = fAccion;
			tbImagenes.Items["CANC"].Enabled = false;
			tbImagenes.Items["IMPR"].Enabled = fAccion;
			tbImagenes.Items["CORTAR"].Enabled = fAccion;
			tbImagenes.Items["COPIAR"].Enabled = fAccion;
			tbImagenes.Items["PEGAR"].Enabled = false;
			tbImagenes.Items["IMAGENNUEVA"].Enabled = false;
			tbImagenes.Items["PINTAR"].Enabled = fAccion;
			tbImagenes.Items["HERRAMIENTAS"].Enabled = false;
			tbImagenes.Items["SELECC"].Enabled = fAccion;
			tbImagenes.Items["ARRAS"].Enabled = fAccion;
			tbImagenes.Items["ZOOMIN"].Enabled = fAccion;
			tbImagenes.Items["ZOOMOUT"].Enabled = fAccion;
			tbImagenes.Items["ACERC"].Enabled = fAccion;
			tbImagenes.Items["IZQ"].Enabled = fAccion;
			tbImagenes.Items["DER"].Enabled = fAccion;
			tbImagenes.Items["VOLVER"].Enabled = fAccion;
			tbImagenes.Items["SALIR"].Enabled = true;
		}
	}
}
 
 
 
 