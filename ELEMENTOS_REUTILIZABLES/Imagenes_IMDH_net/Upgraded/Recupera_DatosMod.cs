using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;

namespace ImagenesIMDH
{
	internal static class Recupera_Datos_M�dulo
	{


		public static SqlConnection Connexi�n_RDO_Recupera_Datos = null; // VARIABLE PARA QUE PUEDA COGER LA BASE EL FORMULARIO
		
		internal static string CAMBIO_SQL(ref string DATOS)
		{
			string DAT2 = String.Empty;
			try
			{
				int inicio = 0;
				bool LETRA = false;
				DAT2 = DATOS;
				inicio = 1;
				// MODIFICAR TODOS LOS BETWEEN
				while (Strings.InStr(inicio, DATOS, "BETWEEN", CompareMethod.Binary) != 0)
				{
					LETRA = false;
					for (int f = Strings.InStr(inicio, DATOS, "BETWEEN", CompareMethod.Binary) - 1; f >= 1; f--)
					{
						if (DATOS.Substring(f - 1, Math.Min(1, DATOS.Length - (f - 1))) != " ")
						{
							LETRA = true;
						}
						if (DATOS.Substring(f - 1, Math.Min(1, DATOS.Length - (f - 1))) == " " && LETRA)
						{
							DATOS = DATOS.Substring(0, Math.Min(f, DATOS.Length)) + "CONVERT(VARCHAR," + DATOS.Substring(f, Math.Min(Strings.InStr(inicio, DATOS, "BETWEEN", CompareMethod.Binary) - 1, DATOS.Length - f)) + ")" + DATOS.Substring(Math.Max(DATOS.Length - (DATOS.Length - Strings.InStr(inicio, DATOS, "BETWEEN", CompareMethod.Binary) + 1), 0));
							// para que empiece en la e de bEtween
							inicio = Strings.InStr(inicio, DATOS, "BETWEEN", CompareMethod.Binary) + 1;
						}
					}
				}
				inicio = 1;
				// MODIFICAR TODOS LOS LIKE
				while (Strings.InStr(inicio, DATOS, "LIKE", CompareMethod.Binary) != 0)
				{
					LETRA = false;
					for (int f = Strings.InStr(inicio, DATOS, "LIKE", CompareMethod.Binary) - 1; f >= 1; f--)
					{
						if (DATOS.Substring(f - 1, Math.Min(1, DATOS.Length - (f - 1))) != " ")
						{
							LETRA = true;
						}
						if (DATOS.Substring(f - 1, Math.Min(1, DATOS.Length - (f - 1))) == " " && LETRA)
						{
							DATOS = DATOS.Substring(0, Math.Min(f, DATOS.Length)) + "CONVERT(VARCHAR," + DATOS.Substring(f, Math.Min(Strings.InStr(inicio, DATOS, "LIKE", CompareMethod.Binary) - f - 1, DATOS.Length - f)) + ") " + DATOS.Substring(Math.Max(DATOS.Length - (DATOS.Length - Strings.InStr(inicio, DATOS, "LIKE", CompareMethod.Binary) + 1), 0));
							//para que empiece en la i de lIke
							inicio = Strings.InStr(inicio, DATOS, "LIKE", CompareMethod.Binary) + 1;
							break;
						}
					}
				}
				return DATOS;
			}
			catch
			{
				return DAT2;
			}
		}

		internal static string CARGAR_FUENTE_CAPTION(string Titulo)
		{
			try
			{
				const int KEY_ALL_ACCESS = 0x3F;
				const int HKEY_CURRENT_USER = unchecked((int) 0x80000001);
				const int ERROR_SUCCESS = 0;
				int TAMA�O = 0;

				StringBuilder fuente = new StringBuilder();
				string LECTURA = String.Empty;
				string NEGRITA = String.Empty;
				string CURSIVA = String.Empty;
				int lresult = 0; //todo va bien si es cero
				string strBuf = String.Empty; //cadena que contiene el valor requerido
				int lDataBufSize = 0; //tama�o de la cadena en el registro
				int hKey = 0; //ubicaci�n de la clave en el registro
				string ruta = String.Empty;
				float V1 = 0;
				int V2 = 0;

				ruta = "Control Panel\\Desktop\\WindowMetrics";
                lresult = (UpgradeSupportHelper.PInvoke.SafeNative.advapi32.RegOpenKeyEx(HKEY_CURRENT_USER, ref ruta, 0, KEY_ALL_ACCESS, ref hKey) != ERROR_SUCCESS) ? -1 : 0;
               

                string tempRefParam = "CaptionFont";
				int tempRefParam2 = 3;
				lresult = UpgradeSupportHelper.PInvoke.SafeNative.advapi32.RegQueryValueEx(hKey, ref tempRefParam, 0, ref tempRefParam2, 0, ref lDataBufSize); // EL 3 ES TIPO DE DATOS BINARIOS
				if (lresult == ERROR_SUCCESS)
				{
					strBuf = new string(' ', lDataBufSize);
					string tempRefParam3 = "CaptionFont";
					int tempRefParam4 = 0;
					lresult = UpgradeSupportHelper.PInvoke.SafeNative.advapi32.RegQueryValueEx(hKey, ref tempRefParam3, 0, ref tempRefParam4, strBuf, ref lDataBufSize);
					if (lresult == ERROR_SUCCESS)
					{
						LECTURA = strBuf.Substring(0, Math.Min(lDataBufSize - 1, strBuf.Length));
					}
				}
				lresult = UpgradeSupportHelper.PInvoke.SafeNative.advapi32.RegCloseKey(hKey);

				if (lresult != ERROR_SUCCESS)
				{
					// EL T�TULO EN LA IZQUIERDA
					TAMA�O = 8;
					fuente = new StringBuilder("Ms Sans Serif");
					NEGRITA = "SI";
					CURSIVA = "NO";
				}
				else
				{
					// VER EL TAMA�O DEL FONT
					//       XX = (258 - Asc(Mid(LECTURA, 1, 1)))
					//       TAMA�O = XX - Int(XX / 4) - 2
					TAMA�O = Convert.ToInt32((258 - Strings.Asc(LECTURA.Substring(0, Math.Min(1, LECTURA.Length))[0])) - Math.Floor(Convert.ToDouble((258 - Strings.Asc(LECTURA.Substring(0, Math.Min(1, LECTURA.Length))[0])) / ((int) 4)) - 2));

					// VER SI EST� EN NEGRITA
					NEGRITA = (Strings.Asc(LECTURA.Substring(16, Math.Min(1, LECTURA.Length - 16))[0]) == 0xBC) ? "SI" : "NO";
					// VER SI EST� EN KURSIVA
					CURSIVA = (Strings.Asc(LECTURA.Substring(20, Math.Min(1, LECTURA.Length - 20))[0]) == 0x1) ? "SI" : "NO";
					// VER EL FONT
					fuente = new StringBuilder("");
					for (int f = 29; f <= LECTURA.Length; f += 2)
					{
						if (Strings.Asc(LECTURA.Substring(f - 1, Math.Min(1, LECTURA.Length - (f - 1)))[0]) == 0)
						{
							break;
						}
						else
						{
							fuente.Append(LECTURA.Substring(f - 1, Math.Min(1, LECTURA.Length - (f - 1))));
						}
					}
				}
				if (fuente.ToString().Trim() == "")
				{
					//SI NO SE PUDO CARGAR LA FUENTE SE PONE A LA IZQUIERDA
					Recupera_Dato.DefInstance.Tam_FUENTE.Text = "";
					return Titulo;
				}
				else
				{
					//CADENA DE CENTRADO DEL CAPTION
					Recupera_Dato.DefInstance.Tam_FUENTE.Text = fuente.ToString();
					Recupera_Dato.DefInstance.Tam_FUENTE.Tag = TAMA�O.ToString();
					Recupera_Dato.DefInstance.Neg_CURSIVA.Text = CURSIVA;
					Recupera_Dato.DefInstance.Neg_CURSIVA.Tag = NEGRITA;
					Recupera_Dato.DefInstance.Font = Recupera_Dato.DefInstance.Font.Change(name:fuente.ToString(), size:TAMA�O);
					Recupera_Dato.DefInstance.Font = (CURSIVA == "SI") ? Recupera_Dato.DefInstance.Font.Change(italic:true) : Recupera_Dato.DefInstance.Font.Change(italic:false);
					Recupera_Dato.DefInstance.Font = (NEGRITA == "SI") ? Recupera_Dato.DefInstance.Font.Change(bold:true) : Recupera_Dato.DefInstance.Font.Change(bold:false);
					V1 = (((float) (Recupera_Dato.DefInstance.Width * 15)) - Recupera_Dato.DefInstance.CreateGraphics().MeasureString("  ", Recupera_Dato.DefInstance.Font).Width * 15 - Recupera_Dato.DefInstance.CreateGraphics().MeasureString(("  " + Titulo).Trim(), Recupera_Dato.DefInstance.Font).Width * 15) / 2;
					V2 = Convert.ToInt32(Recupera_Dato.DefInstance.CreateGraphics().MeasureString("  ", Recupera_Dato.DefInstance.Font).Width * 15 - Recupera_Dato.DefInstance.CreateGraphics().MeasureString(" ", Recupera_Dato.DefInstance.Font).Width * 15);
					return new String(' ', Convert.ToInt32((float) Math.Floor(V1 / V2))) + Titulo.Trim();
				}
			}
			catch
			{
				Recupera_Dato.DefInstance.Tam_FUENTE.Text = "";
				return Titulo;
			}
		}


		// ESTA FUNCI�N DEVUELVE 3 DATOS
		//DATO 1 = C�DIGO o NADA
		//DATO 2 = DESCRIPCI�N o NADA
		//DATO 3 = 0 SIN DATOS
		//         1 SE HAN ACEPTADO DATOS
		//         2 SE HA PULSADO CANCELAR


		//UPGRADE_WARNING: (1003) ParamArray Filtros was changed from ByRef to ByVal. More Information: http://www.vbtonet.com/ewis/ewi1003.aspx
		internal static string[] Recupera_Datos(SqlConnection base_Renamed, string str_tag, params object[] Filtros)
		{


			string[] result = null;
			if (str_tag == "")
			{
				//UPGRADE_WARNING: (2081) Array has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				return new string[]{"", "", "0"};
			}
			// Esta funci�n lleva asociado el formulario RECUPERA_DATO.FRM

			try
			{
				DataSet tablardo = null;
				DataSet TABLA1rdo = null;
				int i_f = 0;
				object i_indice = null;
				object i_pos1 = null;
				object i_pos2 = null;
				object i_posactual = null;
				int i_i = 0;
				string st_Z = String.Empty;
				string st_cadena_sql = String.Empty;
				string st_Cadena_SQL_Fin = String.Empty;
				string Valor_Menor = String.Empty;
				string Valor_Mayor = String.Empty;
				string st_fil = String.Empty;
				string st_fil_F1 = String.Empty;

				// cargo la forma pero sin mostrarla para poner los datos
				Recupera_Dato tempLoadForm = Recupera_Dato.DefInstance;

				string b42_f0desde = String.Empty;
				string b42_f0hasta = String.Empty;
				string b42_f1desde = String.Empty;
				string b42_f1hasta = String.Empty;
				string b42_f2desde = String.Empty;
				string b42_f2hasta = String.Empty;
				string p32_filtro0 = String.Empty;
				string p32_filtro1 = String.Empty;
				string p32_filtro2 = String.Empty;
				string b42_mserr = String.Empty;
				string p32_cadescr = String.Empty;
				string p32_tabla = String.Empty;
				string b42_msayd = String.Empty;

				// campos principales, siempre deben estar rellenos

				i_i = Filtros.GetUpperBound(0);
				//cambio de todos los comodines * por comodines %
				for (i_f = 0; i_f <= i_i; i_f++)
				{
					while (Convert.ToString(Filtros[i_f]).IndexOf('*') >= 0)
					{
						Filtros[i_f] = StringsHelper.MidAssignment(Convert.ToString(Filtros[i_f]), Convert.ToString(Filtros[i_f]).IndexOf('*') + 1, 1, "%");
					}
				}
				st_Z = new string('Z', 80);

				st_cadena_sql = "SELECT b42_titulo, b42_f0desde, b42_f0hasta, b42_f1desde, b42_f1hasta, b42_f2desde, b42_f2hasta, b42_msayd, b42_mserr, p32_tabla, p32_filtro0, p32_filtro1, p32_filtro2, p32_cadescr ";
				st_cadena_sql = st_cadena_sql + "FROM FDATOB42, FPLANP32 WHERE fplanp32.p32_plan=fdatob42.b42_plan AND b42_dato = '" + str_tag + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(st_cadena_sql, base_Renamed);
				tablardo = new DataSet();
				tempAdapter.Fill(tablardo);

				// PONER T�TULO
				i_f = 0;
				foreach (DataRow iteration_row in tablardo.Tables[0].Rows)
				{
					b42_f0desde = (Convert.IsDBNull(iteration_row["b42_titulo"])) ? "" : Convert.ToString(iteration_row["b42_titulo"]).Trim();

					//Recupera_Dato.Caption = Space(Int((Recupera_Dato.Width - Recupera_Dato.TextWidth(b42_f0desde))) / 90) & b42_f0desde
					Recupera_Dato.DefInstance.Text = CARGAR_FUENTE_CAPTION(b42_f0desde);
					//MsgBox "Le�do las fuentes del T�tulo de formulario"
					b42_f0desde = (Convert.IsDBNull(iteration_row["b42_f0desde"])) ? "" : Convert.ToString(iteration_row["b42_f0desde"]).Trim();
					b42_f0hasta = (Convert.IsDBNull(iteration_row["b42_f0hasta"])) ? "" : Convert.ToString(iteration_row["b42_f0hasta"]).Trim();
					b42_f1desde = (Convert.IsDBNull(iteration_row["b42_f1desde"])) ? "" : Convert.ToString(iteration_row["b42_f1desde"]).Trim();
					b42_f1hasta = (Convert.IsDBNull(iteration_row["b42_f1hasta"])) ? "" : Convert.ToString(iteration_row["b42_f1hasta"]).Trim();
					b42_f2desde = (Convert.IsDBNull(iteration_row["b42_f2desde"])) ? "" : Convert.ToString(iteration_row["b42_f2desde"]).Trim();
					b42_f2hasta = (Convert.IsDBNull(iteration_row["b42_f2hasta"])) ? "" : Convert.ToString(iteration_row["b42_f2hasta"]).Trim();

					p32_filtro0 = (Convert.IsDBNull(iteration_row["p32_filtro0"])) ? "" : Convert.ToString(iteration_row["p32_filtro0"]).Trim();
					p32_filtro1 = (Convert.IsDBNull(iteration_row["p32_filtro1"])) ? "" : Convert.ToString(iteration_row["p32_filtro1"]).Trim();
					p32_filtro2 = (Convert.IsDBNull(iteration_row["p32_filtro2"])) ? "" : Convert.ToString(iteration_row["p32_filtro2"]).Trim();

					p32_cadescr = (Convert.IsDBNull(iteration_row["p32_cadescr"])) ? "" : Convert.ToString(iteration_row["p32_cadescr"]).Trim();
					p32_tabla = (Convert.IsDBNull(iteration_row["p32_tabla"])) ? "" : Convert.ToString(iteration_row["p32_tabla"]).Trim();
					b42_mserr = (Convert.IsDBNull(iteration_row["b42_mserr"])) ? "" : Convert.ToString(iteration_row["b42_mserr"]).Trim();
					b42_msayd = (Convert.IsDBNull(iteration_row["b42_msayd"])) ? "" : Convert.ToString(iteration_row["b42_msayd"]).Trim();

					i_f++;
				}
				tablardo.Close();

				if (i_f == 0)
				{
					Recupera_Dato.DefInstance.Close();
					return new string[]{"", "", "0"};
				}
				if (p32_cadescr.Trim() == "" || p32_filtro0.Trim() == "")
				{
					return new string[]{"", "", "0"};
				}

				// campos principales, siempre deben estar rellenos
				if (Filtros.GetUpperBound(0) == 1)
				{
					if (Convert.ToString(Filtros[1]).Trim() == "" && b42_f1desde == "" && b42_f1hasta == "" && p32_filtro1 != "")
					{
						Recupera_Dato.DefInstance.Close();
						return new string[]{"", "", "0"};
					}
				}
				if (Filtros.GetUpperBound(0) == 2)
				{
					if (Convert.ToString(Filtros[1]).Trim() == "" && b42_f1desde == "" && b42_f1hasta == "" && p32_filtro1 != "")
					{
						Recupera_Dato.DefInstance.Close();
						return new string[]{"", "", "0"};
					}
					if (Convert.ToString(Filtros[2]).Trim() == "" && b42_f2desde == "" && b42_f2hasta == "" && p32_filtro2 != "")
					{
						Recupera_Dato.DefInstance.Close();
						return new string[]{"", "", "0"};
					}
				}

				i_i = Filtros.GetUpperBound(0);


				if (i_i == -1)
				{
					st_fil = "%";
				}
				else
				{
					st_fil = Convert.ToString(Filtros[0]) + "%";
				}

				// vamos a generar la sql
				if (b42_f0desde == "")
				{
					Valor_Menor = " ";
				}
				else
				{
					Valor_Menor = b42_f0desde;
				}


				if (b42_f0hasta == "")
				{
					Valor_Mayor = st_Z;
				}
				else
				{
					Valor_Mayor = b42_f0hasta;
				}


				st_cadena_sql = "SELECT " + p32_filtro0 + ", " + p32_cadescr + " FROM " + p32_tabla;
				if (Valor_Mayor != st_Z || Valor_Menor != " ")
				{
					st_cadena_sql = st_cadena_sql + " WHERE " + p32_filtro0 + " BETWEEN '" + Valor_Menor + "' AND '" + Valor_Mayor + "' ";
				}
				// si existe el campo filtro1


				if (p32_filtro1.Trim() != "")
				{

					// a�adir el filtro1 a la consulta

					if (b42_f1desde == "")
					{
						Valor_Menor = " ";
					}
					else
					{
						Valor_Menor = b42_f1desde;
					}

					if (b42_f1hasta == "")
					{
						Valor_Mayor = st_Z;
					}
					else
					{
						Valor_Mayor = b42_f1hasta;
					}


					if ((st_cadena_sql.IndexOf("WHERE") + 1) == 0)
					{
						if (Valor_Mayor != st_Z || Valor_Menor != " ")
						{
							st_cadena_sql = st_cadena_sql + " WHERE " + p32_filtro1 + " BETWEEN '" + Valor_Menor + "' AND '" + Valor_Mayor + "' ";
						}
					}
					else
					{
						if (Valor_Mayor != st_Z || Valor_Menor != " ")
						{
							st_cadena_sql = st_cadena_sql + " AND " + p32_filtro1 + " BETWEEN '" + Valor_Menor + "' AND '" + Valor_Mayor + "' ";
						}
					}




					// comprobar si se pasan valores iniciales de filtro

					if (Valor_Mayor != Valor_Menor)
					{
						if (i_i <= 0)
						{
							st_fil_F1 = "%";
						}
						else
						{
							st_fil_F1 = Convert.ToString(Filtros[1]) + "%";
						}

						if (st_fil_F1 != "")
						{
							if ((st_cadena_sql.IndexOf("WHERE") + 1) == 0)
							{
								st_cadena_sql = st_cadena_sql + " WHERE " + p32_filtro1 + " LIKE'" + st_fil_F1 + "'";
							}
							else
							{
								st_cadena_sql = st_cadena_sql + " AND " + p32_filtro1 + " LIKE'" + st_fil_F1 + "'";
							}
						}
					}
				}


				// si existe el campo filtro2

				if (p32_filtro2 != "")
				{

					// a�adir el filtro1 a la consulta

					if (b42_f2desde == "")
					{
						Valor_Menor = " ";
					}
					else
					{
						Valor_Menor = b42_f2desde;
					}

					if (b42_f2hasta == "")
					{
						Valor_Mayor = st_Z;
					}
					else
					{
						Valor_Mayor = b42_f2hasta;
					}


					if ((st_cadena_sql.IndexOf("WHERE") + 1) == 0)
					{
						if (Valor_Mayor != st_Z || Valor_Menor != " ")
						{
							st_cadena_sql = st_cadena_sql + " WHERE " + p32_filtro2 + " BETWEEN '" + Valor_Menor + "' AND '" + Valor_Mayor + "' ";
						}
					}
					else
					{
						if (Valor_Mayor != st_Z || Valor_Menor != " ")
						{
							st_cadena_sql = st_cadena_sql + " AND " + p32_filtro2 + " BETWEEN '" + Valor_Menor + "' AND '" + Valor_Mayor + "' ";
						}
					}

                    
					// comprobar si se pasan valores iniciales de filtro
					if (Valor_Menor != Valor_Mayor)
					{
						if (i_i <= 1)
						{
							st_fil_F1 = "%";
						}
						else
						{
							st_fil_F1 = Convert.ToString(Filtros[2]) + "%";
						}

						if (st_fil_F1 != "")
						{
							if ((st_cadena_sql.IndexOf("WHERE") + 1) == 0)
							{
								st_cadena_sql = st_cadena_sql + " WHERE " + p32_filtro2 + " LIKE'" + st_fil_F1 + "'";
							}
							else
							{
								st_cadena_sql = st_cadena_sql + " AND " + p32_filtro2 + " LIKE'" + st_fil_F1 + "'";
							}
						}
					}


				}
				// carga de datos
				Recupera_Dato.DefInstance.tbC�digo.Tag = st_fil;
				Recupera_Dato.DefInstance.tbDescripci�n.Tag = "";
				Recupera_Dato.DefInstance.lbC�digo.Text = p32_filtro0;
				Recupera_Dato.DefInstance.lbDescripci�n.Text = p32_cadescr;
				Recupera_Dato.DefInstance.lbAyuda.Text = b42_msayd;
				Recupera_Dato.DefInstance.lbSQL.Text = st_cadena_sql;

				Connexi�n_RDO_Recupera_Datos = base_Renamed;
				// pongo el texto en el primer valor
				Recupera_Dato.DefInstance.ShowDialog();
				result = new string[]{Recupera_Dato.DefInstance.tbC�digo.Text, Recupera_Dato.DefInstance.tbDescripci�n.Text, Convert.ToString(Recupera_Dato.DefInstance.tbDescripci�n.Tag)};

				Recupera_Dato.DefInstance.Close();
				Connexi�n_RDO_Recupera_Datos = null;
				return result;
			}
			catch
			{
				if (Information.Err().Number == 94)
				{
                    //INDRA jproche  TODO_X_5 - USO BASE DATOS ACCES 23/05/2016
                    //UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                    //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
				} // uso inv�lido de null
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number > 40000 && Information.Err().Number < 40516)
				{
                    //INDRA jproche  TODO_X_5 - USO BASE DATOS ACCES 23/05/2016
                    //UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
                    //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
				} // error odbc
				RadMessageBox.Show(" Error en M�dulo " + Information.Err().Number.ToString() + Conversion.ErrorToString(), Application.ProductName);
				return new string[]{"", "", "0"};
				//Stop
			}
		}
	}
}