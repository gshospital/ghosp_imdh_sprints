using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ImagenesIMDH
{
	public class Imagenes_Dll
	{
		public int Escaner(int Valor = -1)
		{
			try
			{
				if (Valor < 0)
				{
                    /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
					Module1.HAY_ESCANER = (ImgRFact.DefInstance.ImgScan1.ScannerAvailable()) ? 1 : 2;
                    */
					return Module1.HAY_ESCANER;
				}
				else
				{
					Module1.HAY_ESCANER = Valor;
					return 0;
				}
			}
			catch
			{
				return Information.Err().Number;
			}
		}
		public int Clasificacion(string Valor)
		{
			try
			{
				Module1.HAY_Clasificacion = Valor;
				return 0;
			}
			catch
			{
				return Information.Err().Number;
			}
		}
		public int Cargar_Imagen(SqlConnection base_Renamed, object Imag, string Refe)
		{
			DataSet RrImagenes = null;
			string stSQL = String.Empty;
			string Nombre_Archivo = String.Empty;
			byte[] bContenidoDelTrozo = null;
			try
			{

				stSQL = "SELECT S04_IMGLON,S04_IMAGEN " + 
				        "FROM FIMAGS04 " + 
				        "WHERE S04_REF = '" + Refe + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQL, base_Renamed);
				RrImagenes = new DataSet();
				tempAdapter.Fill(RrImagenes);

				//RrImagenes.MoveFirst
				int ii = 0;
				if (RrImagenes.Tables[0].Rows.Count == 0)
				{
					RrImagenes.Close();
					RrImagenes = null;
					return -1;
				}
				else
				{
					//Crear un fichero intermedio en el que se
					//va recuperando por trozos la informaci�n
					//del campo de la B.D. de tipo imagen
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					ii = Convert.ToInt32(RrImagenes.Tables[0].Rows[0]["s04_imglon"]);
					if (ii > 0)
					{
						bContenidoDelTrozo = new byte[ii + 1];
						bContenidoDelTrozo = (byte[]) RrImagenes.Tables[0].Rows[0]["s04_imagen"];
						RrImagenes.Close();

						Nombre_Archivo = Module1.Archivo();
						FileSystem.FileOpen(1, Nombre_Archivo, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);
						FileSystem.FileClose();

						FileSystem.FileOpen(1, Nombre_Archivo, OpenMode.Binary, OpenAccess.Write, OpenShare.Default, -1);
						FileSystem.FilePutObject(1, bContenidoDelTrozo, 0);
						FileSystem.FileClose(1);
                        //primero tengo que pasarlo a un image control por si es un icono
                        /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
						//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						Imag.Picture = Image.FromFile(Nombre_Archivo);
                        */
						Application.DoEvents();
						File.Delete(Nombre_Archivo);
					}
					else
					{
                        /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
						//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						Imag.Picture = null;
                        */
					}
					return 0;
				}
			}
			catch
			{
				return Convert.ToInt32(Double.Parse(Information.Err().Number.ToString() + Conversion.ErrorToString()));
			}
		}
		public string Guardar_Imagen(SqlConnection base_Renamed, object Imag, ref string Refe, ref string Desc)
		{
            /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
			string guarda_imagen = String.Empty;
			//grabar
			DataSet Tal = null;
			byte[] xxx = null;
			string Nombre_Archivo = String.Empty;
			int ii = 0;
			string cad = String.Empty;
			try
			{
				Refe = Refe.Trim();
				Desc = Desc.Trim();
				if (Refe.Trim() == "NADA")
				{
					guarda_imagen = "";
					return System.String.Empty;
				}
				if (Conversion.Val(Refe) == 0)
				{
					//de momento saco el m�ximo
					cad = "SELECT max(s04_ref) from fimags04";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(cad, base_Renamed);
					Tal = new DataSet();
					tempAdapter.Fill(Tal);
					if (Tal.Tables[0].Rows.Count == 0)
					{
						Refe = "0000000001";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (("" + Convert.ToString(Tal.Tables[0].Rows[0][0])).Trim() == "")
						{
							Refe = "0000000001";
						}
						else
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							Refe = ("0000000000" + ((Conversion.Val(Convert.ToString(Tal.Tables[0].Rows[0][0])) + 1).ToString())).Substring(Math.Max(("0000000000" + ((Conversion.Val(Convert.ToString(Tal.Tables[0].Rows[0][0])) + 1).ToString())).Length - 10, 0));
						}
					}
					Tal.Close();
					Tal = null;

					//sacar un n� de referencia nuevo y:
					cad = "INSERT into Fimags04 (s04_REF,s04_DESCR,S04_imglon) values ('" + Refe + "','" + Desc + "',0)";
					SqlCommand tempCommand = new SqlCommand(cad, base_Renamed);
					tempCommand.ExecuteNonQuery();
					if (Imag != null)
					{

						//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						if (Imag.Picture != null)
						{
							cad = "SELECT S04_IMAGEN, S04_imglon from fimags04 where S04_ref = '" + Refe + "'";
							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(cad, base_Renamed);
							Tal = new DataSet();
							tempAdapter_2.Fill(Tal);

							Nombre_Archivo = Module1.Archivo();
							FileSystem.FileOpen(1, Nombre_Archivo, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);
							FileSystem.FileClose();

							//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							//UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
							((Image) Imag.Picture).Save(Nombre_Archivo);
							FileSystem.FileOpen(1, Nombre_Archivo, OpenMode.Binary, OpenAccess.Read, OpenShare.Default, -1);
							ii = (int) FileSystem.LOF(1);
							xxx = new byte[ii + 1];
							Array TempArray = Array.CreateInstance(xxx.GetType().GetElementType(), xxx.Length);
							FileSystem.FileGet(1, ref TempArray, -1, false, false);
							Array.Copy(TempArray, xxx, TempArray.Length);
							FileSystem.FileClose(1);
							Application.DoEvents();
							File.Delete(Nombre_Archivo);

							Tal.Edit();
							Tal.Tables[0].Rows[0]["s04_imagen"] = xxx;
							Tal.Tables[0].Rows[0]["s04_imglon"] = ii;
							string tempQuery = Tal.Tables[0].TableName;
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
							SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
							tempAdapter_3.Update(Tal, Tal.Tables[0].TableName);
							Tal.Close();
							Tal = null;
						}
					}
					return Refe;

				}
				else
				{
					//actualizo la foto y la descripci�n
					cad = "SELECT S04_descr, S04_IMAGEN, S04_imglon from fimags04 where S04_ref = '" + Refe + "'";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(cad, base_Renamed);
					Tal = new DataSet();
					tempAdapter_4.Fill(Tal);
					if (Imag != null)
					{
						//si existe el objeto
						//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						if (Imag.Picture == null)
						{
							//si no tiene imagen
							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Tal.Edit was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							Tal.Edit();
							if (Desc != "")
							{
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								Tal.Tables[0].Rows[0]["s04_descr"] = (Desc);
							}
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							Tal.Tables[0].Rows[0]["s04_imglon"] = 0;
						}
						else
						{
							//si tiene imagen
							Nombre_Archivo = Module1.Archivo();
							FileSystem.FileOpen(1, Nombre_Archivo, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);
							FileSystem.FileClose();

							//UPGRADE_TODO: (1067) Member Picture is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							//UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
							((Image) Imag.Picture).Save(Nombre_Archivo);
							FileSystem.FileOpen(1, Nombre_Archivo, OpenMode.Binary, OpenAccess.Read, OpenShare.Default, -1);
							ii = (int) FileSystem.LOF(1);
							xxx = new byte[ii + 1];
							//UPGRADE_WARNING: (2080) Get was upgraded to FileGet and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
							Array TempArray2 = Array.CreateInstance(xxx.GetType().GetElementType(), xxx.Length);
							FileSystem.FileGet(1, ref TempArray2, -1, false, false);
							Array.Copy(TempArray2, xxx, TempArray2.Length);
							FileSystem.FileClose(1);
							Application.DoEvents();
							File.Delete(Nombre_Archivo);

							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Tal.Edit was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							Tal.Edit();
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							Tal.Tables[0].Rows[0]["s04_imagen"] = xxx;
							if (Desc != "")
							{
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								Tal.Tables[0].Rows[0]["s04_descr"] = (Desc);
							}
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							Tal.Tables[0].Rows[0]["s04_imglon"] = ii;
						}
					}
					else
					{
						//no hay objeto y se modifica, en su caso la descripci�n
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Tal.Edit was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						Tal.Edit();
						if (Desc != "")
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							Tal.Tables[0].Rows[0]["s04_descr"] = (Desc);
						}
					}
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					string tempQuery_2 = Tal.Tables[0].TableName;
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery_2, "");
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_5);
					tempAdapter_5.Update(Tal, Tal.Tables[0].TableName);
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Tal.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					Tal.Close();
					Tal = null;
					return Refe;
				}
			}
			catch
			{
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				return ((Information.Err().Number * -1).ToString()) + Conversion.ErrorToString();
			}
            */
            return "";
		}

		public string Editar_Imagen(SqlConnection base_Renamed, object Imag, string Refe)
		{
			//�debo devolver un objeto o un ole_handle?
			// en Base ir� la conexion
			// en Imag ir� el control imagen, donde le pegar� lo que sea
			// en Refe ir� la referencia de la imagen
			//devolver� (nada si la imagen se borra)
			//          (la referencia de la imagen... la que sea, ya sea nueva o la misma)

			try
			{
				Module1.BaseRDO = base_Renamed;
				Module1.HAY_Imagen = Imag;
				Module1.Referen = Refe;
				//UPGRADE_WARNING: (7009) Multiple invocations to ShowDialog in Forms with ActiveX Controls might throw runtime exceptions More Information: http://www.vbtonet.com/ewis/ewi7009.aspx
				ImgRFact.DefInstance.ShowDialog();
				return Module1.Referen;
			}
			catch
			{
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				return ((Information.Err().Number * -1).ToString()) + Conversion.ErrorToString();
			}
		}
		public int Cargar_Memo(SqlConnection base_Renamed, object Imag, ref string Refe)
		{
			int result = 0;
			string Desc = String.Empty;
			DataSet Tal = null;
			string stSQL = String.Empty;
			try
			{
				Refe = Refe.Trim();
				Desc = Desc.Trim();

				stSQL = "SELECT S05_MEMO " + 
				        "FROM FMEMOS05 " + 
				        "WHERE S05_REF = '" + Refe + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQL, base_Renamed);
				Tal = new DataSet();
				tempAdapter.Fill(Tal);

				if (Tal.Tables[0].Rows.Count != 0)
				{
                    //UPGRADE_TODO: (1067) Member Text is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                    /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
					Imag.Text = "" + Convert.ToString(Tal.Tables[0].Rows[0][0]);
                    */
					result = 0;
				}
				else
				{
					result = -1;
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Tal.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Tal.Close();
				return result;
			}
			catch
			{
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				return Convert.ToInt32(Double.Parse(Information.Err().Number.ToString() + Conversion.ErrorToString()));
			}
		}
		public string Guardar_Memo(SqlConnection base_Renamed, object Imag, ref string Refe)
		{
			string Desc = String.Empty;
			DataSet Tal = null;
			string cad = String.Empty;
			try
			{
				Refe = Refe.Trim();
				Desc = Desc.Trim();

				if (Conversion.Val(Refe) == 0)
				{
					//de momento saco el m�ximo
					cad = "SELECT max(S05_ref) from FMEMOS05";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(cad, base_Renamed);
					Tal = new DataSet();
					tempAdapter.Fill(Tal);
					if (Tal.Tables[0].Rows.Count == 0)
					{
						Refe = "0000000001";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (("" + Convert.ToString(Tal.Tables[0].Rows[0][0])).Trim() == "")
						{
							Refe = "0000000001";
						}
						else
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							Refe = ("0000000000" + ((Conversion.Val(Convert.ToString(Tal.Tables[0].Rows[0][0])) + 1).ToString())).Substring(Math.Max(("0000000000" + ((Conversion.Val(Convert.ToString(Tal.Tables[0].Rows[0][0])) + 1).ToString())).Length - 10, 0));
						}
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Tal.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					Tal.Close();
					Tal = null;
					//sacar un n� de referencia nuevo y:
					cad = "INSERT into FMEMOS05 (S05_REF) values ('" + Refe + "')";
					SqlCommand tempCommand = new SqlCommand(cad, base_Renamed);
					tempCommand.ExecuteNonQuery();
					if (Imag != null)
					{
						cad = "SELECT S05_MEMO from fMEMOS05 where S05_ref = '" + Refe + "'";
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(cad, base_Renamed);
						Tal = new DataSet();
						tempAdapter_2.Fill(Tal);
						Tal.Edit();

                        /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
                        //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                        //UPGRADE_TODO: (1067) Member Text is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                        Tal.Tables[0].Rows[0]["s05_memo"] = Imag.Text;
                        */
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//string tempQuery = Tal.Tables[0].TableName;
						//SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
						tempAdapter_2.Update(Tal, Tal.Tables[0].TableName);
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Tal.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						Tal.Close();
						Tal = null;
					}
					return Refe;
				}
				else
				{
					//actualizo TEXT
					cad = "SELECT S05_MEMO  from fMEMOS05 where S05_ref = '" + Refe + "'";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(cad, base_Renamed);
					Tal = new DataSet();
					tempAdapter_4.Fill(Tal);
					if (Tal.Tables[0].Rows.Count == 0)
					{
						cad = "INSERT into FMEMOS05 (S05_REF) values ('" + Refe + "')";
						SqlCommand tempCommand_2 = new SqlCommand(cad, base_Renamed);
						tempCommand_2.ExecuteNonQuery();
						cad = "SELECT S05_MEMO from FMEMOS05 where S05_ref = '" + Refe + "'";
						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(cad, base_Renamed);
						Tal = new DataSet();
						tempAdapter_5.Fill(Tal);
					}
					if (Imag != null)
					{
						//si existe el objeto
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Tal.Edit was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						Tal.Edit();
                        /*INDRA jproche_TODO_X_5 25/05/2016 Aun no se define la ddl a usar
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_TODO: (1067) Member Text is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						Tal.Tables[0].Rows[0]["s05_memo"] = Imag.Text;
                        */
					}
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					string tempQuery_2 = Tal.Tables[0].TableName;
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(tempQuery_2, "");
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_6);
					tempAdapter_6.Update(Tal, Tal.Tables[0].TableName);
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Tal.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					Tal.Close();
					Tal = null;
					return Refe;
				}
			}
			catch
			{
				return ((Information.Err().Number * -1).ToString()) + Conversion.ErrorToString();
			}
		}

		public string LlamadaIMDH(SqlConnection base_Renamed, object Imag, string TipoLlamada, bool ModoLectura, string Titulo, string NombreImagen, string Usuario, object FicheroTemporal_optional)
		{
			string FicheroTemporal = (FicheroTemporal_optional == Type.Missing) ? String.Empty : FicheroTemporal_optional as string;
			//esta es la llamada desde IMDH
			try
			{

				Module1.BaseRDO = base_Renamed;
				Module1.HAY_Imagen = Imag;
				Module1.stTipoLlamada = TipoLlamada;
				Module1.fModoLectura = ModoLectura;

				Module1.stIdImagen = NombreImagen;

				if (FicheroTemporal_optional == Type.Missing)
				{
					Module1.stFicheroTemporal = "";
				}
				else
				{
					Module1.stFicheroTemporal = FicheroTemporal;
				}

				Module1.vstNomAplicacion = Module1.ObtenerNombreAplicacion();

				Module1.gUsuario = Usuario;

				ImgRFact.DefInstance.Text = Titulo;
				ImgRFact.DefInstance.ShowDialog();
				//hay que controlar se carga en esta variable para
				return Module1.stVueltaIMDH;
			}
			catch
			{
				return ((Information.Err().Number * -1).ToString()) + Conversion.ErrorToString();
			}
		}

		public string LlamadaIMDH(SqlConnection base_Renamed, object Imag, string TipoLlamada, bool ModoLectura, string Titulo, string NombreImagen, string Usuario)
		{
			return LlamadaIMDH(base_Renamed, Imag, TipoLlamada, ModoLectura, Titulo, NombreImagen, Usuario, Type.Missing);
		}
	}
}
 
 
 