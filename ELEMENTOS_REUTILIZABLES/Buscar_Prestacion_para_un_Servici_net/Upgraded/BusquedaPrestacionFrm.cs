using ElementosCompartidos;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BuscarPrestacion
{
	public partial class frmBusquedaPrestacion
		: RadForm
	{


		DataSet rsPrestaciones = null;
		dynamic frmFormularioDevolverDatos = null; //a qui�n devolver los datos (formulario que
		//   contenga el m�todo RecogerPrestacion).
		SqlConnection cnPrestaciones = null; //conexi�n sobre la que hacer consultas.
		string stCodigoServicioFiltrar = String.Empty; //servicio que filtra las prestaciones visualizables.
		string stCondicionFiltroLlamada = String.Empty; //se aplica en el Where que muestra prestaciones.
		bool fFechaPrestacion = false;
		//

		public frmBusquedaPrestacion()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		public void EstablecerVariables(dynamic frmFormularioRetorno, SqlConnection cnConexionLlamante, string stCodigoServicio, string stCondicionFiltro, bool fFecha)
		{

			frmFormularioDevolverDatos = frmFormularioRetorno;
			cnPrestaciones = cnConexionLlamante;
			stCodigoServicioFiltrar = stCodigoServicio;
			stCondicionFiltroLlamada = stCondicionFiltro;
			fFechaPrestacion = fFecha;
		}

		private void frmBusquedaPrestacion_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				txtContenga.Focus();
			}
		}

		private void frmBusquedaPrestacion_Load(Object eventSender, EventArgs eventArgs)
		{
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			sprPrestaciones.MaxRows = 0;
		}



        private void sprPrestaciones_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex;
			int Row = eventArgs.RowIndex;

			if (Row >= 0)
			{
				cbAceptar.Enabled = true;
			}

		}

		private void txtContenga_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = SustituirComilla();
			}
			txtEmpiecepor.Text = "";
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void txtEmpiecepor_TextChanged(Object eventSender, EventArgs eventArgs)
		{

			string stComp = String.Empty;
			string stBusqueda = String.Empty;
			string stSql = String.Empty;
			string fecha = String.Empty;

			string stCondicionesSql = String.Empty; //se ir� construyendo de derecha a izquierda.
			if (txtEmpiecepor.Text != "")
			{
				txtContenga.Text = "";
				stBusqueda = txtEmpiecepor.Text.Trim();
				stComp = "dprestac like '" + stBusqueda + "%'";

				// Construir la sentencia Sql de selecci�n de registros:
				//

				if (stCondicionFiltroLlamada.Length > 0)
				{
					stCondicionesSql = stCondicionFiltroLlamada + " And (" + stComp + ")";
				}
				else
				{
					stCondicionesSql = "(" + stComp + ")";
				}

				if (stCodigoServicioFiltrar.Length > 0)
				{
					stCondicionesSql = "(gservici= " + stCodigoServicioFiltrar + ") And " + stCondicionesSql;
				}

				stSql = "select gprestac, dprestac, finivali from dcodpresVa " + 
				        "Where " + stCondicionesSql + 
				        " Order By dprestac ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnPrestaciones);
				rsPrestaciones = new DataSet();
				tempAdapter.Fill(rsPrestaciones);

				if (rsPrestaciones.Tables[0].Rows.Count != 0)
				{
					sprPrestaciones.MaxRows = rsPrestaciones.Tables[0].Rows.Count;
					carga_grid();
				}
				else
				{
					sprPrestaciones.MaxRows = 0;
					txtEmpiecepor.SelectionStart = 0;
					txtEmpiecepor.SelectionLength = Strings.Len(txtEmpiecepor.Text);
					txtEmpiecepor.Focus();
					return;
				}
			}
			else
			{
				sprPrestaciones.MaxRows = 0;
			}


		}

		private void txtEmpiecepor_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = SustituirComilla();
			}
			txtContenga.Text = "";
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void cbBuscarCadena_Click(Object eventSender, EventArgs eventArgs)
		{

			string fecha = String.Empty;

			txtEmpiecepor.Text = "";

			StringBuilder busca = new StringBuilder();
			string cadena = txtContenga.Text.Trim();

			int iCorte = (cadena.IndexOf(',') + 1);
			if (iCorte == 0)
			{
				busca = new StringBuilder("dprestac like '%" + cadena + "%'");
			}
			else
			{
				while (iCorte != 0)
				{
					busca = new StringBuilder("dprestac like '%" + cadena.Substring(0, Math.Min(iCorte - 1, cadena.Length)).Trim() + "%'");
					cadena = cadena.Substring(iCorte, Math.Min(cadena.Length, cadena.Length - iCorte)).Trim();
					iCorte = (cadena.IndexOf(',') + 1);
					if (iCorte != 0)
					{
						if (rbY.IsChecked)
						{
							busca.Append(" and ");
						}
						else
						{
							busca.Append(" or ");
						}
					}
				}
				if (rbY.IsChecked)
				{
					busca.Append(" and dprestac like '%" + cadena + "%'");
				}
				else
				{
					busca.Append(" or dprestac like '%" + cadena + "%'");
				}
			}

			// Construir la sentencia Sql de selecci�n de registros:
			//
			string stCondicionesSql = String.Empty; //se ir� construyendo de derecha a izquierda.

			if (stCondicionFiltroLlamada.Length > 0)
			{
				stCondicionesSql = stCondicionFiltroLlamada + " And (" + busca.ToString() + ")";
			}
			else
			{
				stCondicionesSql = "(" + busca.ToString() + ")";
			}

			if (stCodigoServicioFiltrar.Length > 0)
			{
				stCondicionesSql = "(gservici= " + stCodigoServicioFiltrar + ") And " + stCondicionesSql;
			}

			string stSql = "select gprestac, dprestac, finivali from dcodpresVa " + 
			               "Where " + stCondicionesSql + 
			               " Order By dprestac ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnPrestaciones);
			rsPrestaciones = new DataSet();
			tempAdapter.Fill(rsPrestaciones);

			if (rsPrestaciones.Tables[0].Rows.Count != 0)
			{
				sprPrestaciones.MaxRows = rsPrestaciones.Tables[0].Rows.Count;
				carga_grid();
			}
			else
			{
				sprPrestaciones.MaxRows = 0;
				txtContenga.SelectionStart = 0;
				txtContenga.SelectionLength = Strings.Len(txtContenga.Text);
				txtContenga.Focus();
				return;
			}

		}

		private void carga_grid()
		{

			sprPrestaciones.Row = 0;
			rsPrestaciones.MoveFirst();

			foreach (DataRow iteration_row in rsPrestaciones.Tables[0].Rows)
			{

				sprPrestaciones.Row++;

				sprPrestaciones.Col = 1;
				sprPrestaciones.Text = Convert.ToString(iteration_row["gprestac"]).Trim();

				sprPrestaciones.Col = 2;
				sprPrestaciones.Text = Convert.ToString(iteration_row["dprestac"]).Trim();

				sprPrestaciones.Col = 3;
				sprPrestaciones.Text = Convert.ToDateTime(iteration_row["finivali"]).ToString("MM/dd/yyyy HH:mm:ss");


			}

		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{


			if (sprPrestaciones.ActiveRowIndex <= 0)
			{
				cbAceptar.Enabled = false;
				return;
			}

			sprPrestaciones.Row = sprPrestaciones.ActiveRowIndex;

			sprPrestaciones.Col = 1;
			string stCodigoPrestacion = sprPrestaciones.Text.Trim();
			sprPrestaciones.Col = 2;
			string stDescripcionPrestacion = sprPrestaciones.Text.Trim();

			sprPrestaciones.Col = 3;
			string stFechaPrestacion = sprPrestaciones.Text.Trim(); //va en formato "MM/DD/YYYY HH:NN:SS"
			if (fFechaPrestacion)
			{
				frmFormularioDevolverDatos.RecogerFechaPrestacion(stCodigoPrestacion, stDescripcionPrestacion, stFechaPrestacion);
			}
			else
			{
				frmFormularioDevolverDatos.RecogerPrestacion(stCodigoPrestacion, stDescripcionPrestacion);
			}
			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (fFechaPrestacion)
			{
				frmFormularioDevolverDatos.RecogerFechaPrestacion("", "", "");
			}
			else
			{
				frmFormularioDevolverDatos.RecogerPrestacion("", ""); //?? innecesario??
			}
			this.Close();
		}

		private void frmBusquedaPrestacion_FormClosing(Object eventSender, FormClosingEventArgs eventArgs)
		{
			int Cancel = (eventArgs.Cancel) ? 1 : 0;
			int UnloadMode = (int) eventArgs.CloseReason;

            if (UnloadMode == ((int)CloseReason.UserClosing))
            { //el usuario pinch� el bot�n de cerrar ventana.
				if (fFechaPrestacion)
				{
					frmFormularioDevolverDatos.RecogerFechaPrestacion("", "", "");
				}
				else
				{
					frmFormularioDevolverDatos.RecogerPrestacion("", ""); //?? innecesario??
				}
			}

			eventArgs.Cancel = Cancel != 0;
		}

		private void frmBusquedaPrestacion_Closed(Object eventSender, EventArgs eventArgs)
		{
			cnPrestaciones = null;
		}
		public int SustituirComilla()
		{
			return 180;
		}
	}
}