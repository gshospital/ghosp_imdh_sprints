using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BuscarPrestacion
{
	public class clsBusquedaPrestacion
	{


		frmBusquedaPrestacion frmVentanaBusqueda = null;
		//

		public void Ejecutar(Form frmFormularioDevolverDatos, SqlConnection cnConexion, string stCodigoServicio, ref object stCondicionFiltro_optional, ref bool fFecha)
		{
			string stCondicionFiltro = (stCondicionFiltro_optional == Type.Missing) ? String.Empty : stCondicionFiltro_optional as string;
			try
			{ //stCondicionFiltro As String
				//   no, pues no funcionar�a IsMissing

				//frmFormularioDevolverDatos debe contener un m�todo RecogerPrestacion; este
				//     m�todo ser� ejecutado cuando se descargue el formulario de esta Dll.
				//cnConexion es la conexi�n que se utilizar� para consultar la tabla o vista
				//     DcodPreVa (adem�s de otras tablas que se pueden indicar en subselects
				//     de stCondicionFiltro).
				//stCodigoServicio es el servicio por el cual filtrar, si es cadena
				//     vac�a (""), no se filtrar� por ninguno.
				//stCondicionFiltro tiene el formato de una cl�usula Where de Sql (pero
				//     NO incluir� la palabra 'Where').
				//le a�ado un parametro opcional 18/08/2000 para que mande finivali
				//si es true le mando la fecha ,si no es true sigo como hasta ahora

				// Pod�a haberse pasado el entorno, y aqu� dentro abrir una conexi�n independiente:
				//Set cnConexion = envEntorno.OpenConnection("GHOSP", , False, "")
				//   pero ya tendr�a que recibir estos par�metros.




				// se da a la condici�n de filtrado el formato apropiado para un futuro Where:
				//
				if (stCondicionFiltro_optional == Type.Missing)
				{
					stCondicionFiltro = "";
				}
				else if (!String.IsNullOrEmpty(stCondicionFiltro))
				{ 
					stCondicionFiltro = "(" + stCondicionFiltro + ")";
					//Else   stcondicionfiltro=""    'superfluo:
					//    stCondicionFiltro = ""
				}
				if (false)
				{
					fFecha = false;
				}
				else
				{
					fFecha = fFecha;
				}
				// se inicializa y lanza la ejecuci�n de una pantalla de b�squeda:
				//

				frmVentanaBusqueda = new frmBusquedaPrestacion();
				frmVentanaBusqueda.EstablecerVariables(frmFormularioDevolverDatos, cnConexion, stCodigoServicio, stCondicionFiltro, fFecha);
				frmVentanaBusqueda.ShowDialog();
			}
			finally
			{
				stCondicionFiltro_optional = stCondicionFiltro;
			}

		}

		public void Ejecutar(Form frmFormularioDevolverDatos, SqlConnection cnConexion, string stCodigoServicio, ref object stCondicionFiltro_optional)
		{
			bool tempRefParam = false;
			Ejecutar(frmFormularioDevolverDatos, cnConexion, stCodigoServicio, ref stCondicionFiltro_optional, ref tempRefParam);
		}

		public void Ejecutar(Form frmFormularioDevolverDatos, SqlConnection cnConexion, string stCodigoServicio)
		{
			object tempRefParam2 = Type.Missing;
			bool tempRefParam3 = false;
			Ejecutar(frmFormularioDevolverDatos, cnConexion, stCodigoServicio, ref tempRefParam2, ref tempRefParam3);
		}
	}
}