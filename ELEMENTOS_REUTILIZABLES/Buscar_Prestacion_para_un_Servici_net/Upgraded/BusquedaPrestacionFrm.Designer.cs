using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BuscarPrestacion
{
	partial class frmBusquedaPrestacion
	{

		#region "Upgrade Support "
		private static frmBusquedaPrestacion m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmBusquedaPrestacion DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmBusquedaPrestacion();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprPrestaciones", "txtEmpiecepor", "Label1", "Frame1", "cbAceptar", "cbCancelar", "cbBuscarCadena", "rbO", "rbY", "FrmCriterio", "txtContenga", "LbBusqueda", "Frmbuscar", "commandButtonHelper1", "sprPrestaciones_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread sprPrestaciones;
        public Telerik.WinControls.UI.RadTextBoxControl txtEmpiecepor;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbBuscarCadena;
		public Telerik.WinControls.UI.RadRadioButton rbO;
		public Telerik.WinControls.UI.RadRadioButton rbY;
		public Telerik.WinControls.UI.RadGroupBox FrmCriterio;
		public Telerik.WinControls.UI.RadTextBoxControl txtContenga;
		public Telerik.WinControls.UI.RadLabel LbBusqueda;
		public Telerik.WinControls.UI.RadGroupBox Frmbuscar;
		private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
		
        //private FarPoint.Win.Spread.SheetView sprPrestaciones_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBusquedaPrestacion));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cbBuscarCadena = new Telerik.WinControls.UI.RadButton();
            this.sprPrestaciones = new UpgradeHelpers.Spread.FpSpread();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.txtEmpiecepor = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.Frmbuscar = new Telerik.WinControls.UI.RadGroupBox();
            this.FrmCriterio = new Telerik.WinControls.UI.RadGroupBox();
            this.rbO = new Telerik.WinControls.UI.RadRadioButton();
            this.rbY = new Telerik.WinControls.UI.RadRadioButton();
            this.txtContenga = new Telerik.WinControls.UI.RadTextBoxControl();
            this.LbBusqueda = new Telerik.WinControls.UI.RadLabel();
            this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscarCadena)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprPrestaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprPrestaciones.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpiecepor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frmbuscar)).BeginInit();
            this.Frmbuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrmCriterio)).BeginInit();
            this.FrmCriterio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContenga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbBusqueda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbBuscarCadena
            // 
            this.cbBuscarCadena.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscarCadena.Image = ((System.Drawing.Image)(resources.GetObject("cbBuscarCadena.Image")));
            this.cbBuscarCadena.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbBuscarCadena.Location = new System.Drawing.Point(248, 76);
            this.cbBuscarCadena.Name = "cbBuscarCadena";
            this.cbBuscarCadena.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbBuscarCadena.Size = new System.Drawing.Size(37, 33);
            this.cbBuscarCadena.TabIndex = 4;
            this.ToolTipMain.SetToolTip(this.cbBuscarCadena, "Llevar a cabo la b�squeda en base de datos");
            this.cbBuscarCadena.Click += new System.EventHandler(this.cbBuscarCadena_Click);
            // 
            // sprPrestaciones
            // 
            this.sprPrestaciones.Location = new System.Drawing.Point(4, 154);
            // 
            // 
            // 
            gridViewTextBoxColumn1.HeaderText = "C�digo";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 60;
            gridViewTextBoxColumn2.HeaderText = "Descripci�n";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 200;
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.IsVisible = false;
            this.sprPrestaciones.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.sprPrestaciones.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.sprPrestaciones.Name = "sprPrestaciones";
            this.sprPrestaciones.Size = new System.Drawing.Size(502, 177);
            this.sprPrestaciones.TabIndex = 7;
            this.sprPrestaciones.ReadOnly = true;
            this.sprPrestaciones.EnableSorting = false;
            this.sprPrestaciones.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprPrestaciones_CellClick);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.txtEmpiecepor);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.HeaderText = "B�squeda por caracteres iniciales";
            this.Frame1.Location = new System.Drawing.Point(312, 44);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(193, 77);
            this.Frame1.TabIndex = 11;
            this.Frame1.Text = "B�squeda por caracteres iniciales";
            // 
            // txtEmpiecepor
            // 
            this.txtEmpiecepor.AcceptsReturn = true;
            this.txtEmpiecepor.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEmpiecepor.Location = new System.Drawing.Point(12, 44);
            this.txtEmpiecepor.MaxLength = 0;
            this.txtEmpiecepor.Name = "txtEmpiecepor";
            this.txtEmpiecepor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtEmpiecepor.Size = new System.Drawing.Size(169, 21);
            this.txtEmpiecepor.TabIndex = 5;
            this.txtEmpiecepor.TextChanged += new System.EventHandler(this.txtEmpiecepor_TextChanged);
            this.txtEmpiecepor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmpiecepor_KeyPress);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(12, 24);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(79, 18);
            this.Label1.TabIndex = 12;
            this.Label1.Text = "Comienzo por:";
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(336, 336);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 29);
            this.cbAceptar.TabIndex = 8;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(424, 336);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 29);
            this.cbCancelar.TabIndex = 9;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // Frmbuscar
            // 
            this.Frmbuscar.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frmbuscar.Controls.Add(this.cbBuscarCadena);
            this.Frmbuscar.Controls.Add(this.FrmCriterio);
            this.Frmbuscar.Controls.Add(this.txtContenga);
            this.Frmbuscar.Controls.Add(this.LbBusqueda);
            this.Frmbuscar.HeaderText = "B�squeda por contenido";
            this.Frmbuscar.Location = new System.Drawing.Point(4, 4);
            this.Frmbuscar.Name = "Frmbuscar";
            this.Frmbuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frmbuscar.Size = new System.Drawing.Size(293, 117);
            this.Frmbuscar.TabIndex = 6;
            this.Frmbuscar.Text = "B�squeda por contenido";
            // 
            // FrmCriterio
            // 
            this.FrmCriterio.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmCriterio.Controls.Add(this.rbO);
            this.FrmCriterio.Controls.Add(this.rbY);
            this.FrmCriterio.HeaderText = "Criterio de selecci�n";
            this.FrmCriterio.Location = new System.Drawing.Point(12, 68);
            this.FrmCriterio.Name = "FrmCriterio";
            this.FrmCriterio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmCriterio.Size = new System.Drawing.Size(158, 42);
            this.FrmCriterio.TabIndex = 1;
            this.FrmCriterio.Text = "Criterio de selecci�n";
            // 
            // rbO
            // 
            this.rbO.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbO.Location = new System.Drawing.Point(92, 19);
            this.rbO.Name = "rbO";
            this.rbO.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbO.Size = new System.Drawing.Size(47, 18);
            this.rbO.TabIndex = 3;
            this.rbO.Text = "A � B";
            // 
            // rbY
            // 
            this.rbY.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbY.Location = new System.Drawing.Point(20, 19);
            this.rbY.Name = "rbY";
            this.rbY.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbY.Size = new System.Drawing.Size(46, 18);
            this.rbY.TabIndex = 2;
            this.rbY.Text = "A y B";
            // 
            // txtContenga
            // 
            this.txtContenga.AcceptsReturn = true;
            this.txtContenga.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtContenga.Location = new System.Drawing.Point(12, 36);
            this.txtContenga.MaxLength = 0;
            this.txtContenga.Name = "txtContenga";
            this.txtContenga.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtContenga.Size = new System.Drawing.Size(273, 21);
            this.txtContenga.TabIndex = 0;
            this.txtContenga.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContenga_KeyPress);
            // 
            // LbBusqueda
            // 
            this.LbBusqueda.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbBusqueda.Location = new System.Drawing.Point(12, 17);
            this.LbBusqueda.Name = "LbBusqueda";
            this.LbBusqueda.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbBusqueda.Size = new System.Drawing.Size(60, 18);
            this.LbBusqueda.TabIndex = 10;
            this.LbBusqueda.Text = "Contenga :";
            // 
            // frmBusquedaPrestacion
            // 
            this.AcceptButton = this.cbBuscarCadena;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 370);
            this.Controls.Add(this.sprPrestaciones);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.Frmbuscar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBusquedaPrestacion";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "B�squeda de prestaciones";
            this.Activated += new System.EventHandler(this.frmBusquedaPrestacion_Activated);
            this.Closed += new System.EventHandler(this.frmBusquedaPrestacion_Closed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBusquedaPrestacion_FormClosing);
            this.Load += new System.EventHandler(this.frmBusquedaPrestacion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscarCadena)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprPrestaciones.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprPrestaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmpiecepor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frmbuscar)).EndInit();
            this.Frmbuscar.ResumeLayout(false);
            this.Frmbuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrmCriterio)).EndInit();
            this.FrmCriterio.ResumeLayout(false);
            this.FrmCriterio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContenga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbBusqueda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion
    }
}