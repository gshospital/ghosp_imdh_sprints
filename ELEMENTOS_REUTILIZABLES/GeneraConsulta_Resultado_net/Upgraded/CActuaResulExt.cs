using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace PasoResulExternos
{
	public class CActuaResulExt
	{

		public object ActualizaResultadoExt(string Paciente, ref string FechaPeticion, string Prestacion, string usuario, ref bool ErrorDLL, int CodigoError, string DescripcionError, object nConnet_optional)
		{
			SqlConnection nConnet = (nConnet_optional == Type.Missing) ? null : nConnet_optional as SqlConnection;

            ErrorDLL = false;

			if (nConnet_optional == Type.Missing)
			{
				ErrorDLL = MActuaResulExt.ABRIRNUEVACONEXION();
			}
			else
			{
				MActuaResulExt.cnConexion = nConnet;
			}
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref MActuaResulExt.cnConexion);
			Serrores.AjustarRelojCliente(MActuaResulExt.cnConexion);
			ErrorDLL = MActuaResulExt.GrabarResultado(Paciente, ref FechaPeticion, Prestacion, usuario, CodigoError, DescripcionError);

            return null;
		}

		public object ActualizaResultadoExt(string Paciente, ref string FechaPeticion, string Prestacion, string usuario, ref bool ErrorDLL, int CodigoError, string DescripcionError)
		{
			return ActualizaResultadoExt(Paciente, ref FechaPeticion, Prestacion, usuario, ref ErrorDLL, CodigoError, DescripcionError, Type.Missing);
		}
	}
}