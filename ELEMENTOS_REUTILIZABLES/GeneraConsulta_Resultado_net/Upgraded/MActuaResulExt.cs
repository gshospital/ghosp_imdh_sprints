using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;


namespace PasoResulExternos
{
	internal static class MActuaResulExt
	{

		public static SqlConnection cnConexion = null;








		internal static bool ABRIRNUEVACONEXION()
		{
			bool result = false;
			string NOMBRE_DSN_SQLSERVER = String.Empty;
			string stServer2 = String.Empty;
			string vstbase = String.Empty;
			string stDriver2 = String.Empty;
			try
			{
				stServer2 = fnQueryRegistro("SERVIDOR");
				stDriver2 = fnQueryRegistro("DRIVER");
				vstbase = fnQueryRegistro("BASEDATOS");
				//**********************************
				//****Path y confirmaci�n de la existencia de
				// ****** conexion al servidor SQL_SERVER 6.5
				result = true;
				int tiDriver = 0;
				string pathstring = String.Empty;
				string tstconexion = String.Empty;
				NOMBRE_DSN_SQLSERVER = "GHOSP";
				tstconexion = "Description=" + "SQL Server en  SERVICURIA" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=" + stServer2 + "" + "\r" + "Database=" + vstbase + "";
				tiDriver = (stDriver2.IndexOf('{') + 1);
				stDriver2 = stDriver2.Substring(tiDriver);
				tiDriver = (stDriver2.IndexOf('}') + 1);
				stDriver2 = stDriver2.Substring(0, Math.Min(tiDriver - 1, stDriver2.Length));
               
                
                 // PARIO TODO_X_5
                 /* UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(NOMBRE_DSN_SQLSERVER, stDriver2, true, tstconexion);*/


				Conexion.Obtener_conexion Instancia = null;
				//Dim RcConexion As rdoConnection
				bool VstError = false;
				string VstLetra = String.Empty;
				string vstUsuario_NT = String.Empty;
				string gUsuario = String.Empty;
				string GPassword = String.Empty;
				vstUsuario_NT = obtener_usuario();
				VstLetra = "C"; // Caso de urgencias
				Instancia = new Conexion.Obtener_conexion();
				VstError = true;
				string tempRefParam = String.Empty;
				string tempRefParam2 = String.Empty;
				string tempRefParam3 = String.Empty;
				string tempRefParam4 = String.Empty;
				string tempRefParam5 = String.Empty;
				string tempRefParam6 = String.Empty;
				string tempRefParam7 = String.Empty;
				string tempRefParam8 = String.Empty;
				string tempRefParam9 = String.Empty;
				Instancia.Conexion(ref cnConexion,  VstLetra,  vstUsuario_NT, ref VstError, ref gUsuario, ref GPassword,  tempRefParam, tempRefParam2,  tempRefParam3,  tempRefParam4,  tempRefParam5,  tempRefParam6,  tempRefParam7,  tempRefParam8, tempRefParam9);
				return VstError;
			}
			catch
			{
				return true;
			}
		}
		private static string obtener_usuario()
		{
			string stBuffer = new String(' ', 8);
			int lSize = stBuffer.Length;
			//Call GetUserName(stBuffer, lSize)
			if (lSize > 0)
			{
				return stBuffer.Substring(0, Math.Min(lSize, stBuffer.Length));
			}
			else
			{
				return "";
			}
		}


		internal static string fnQueryRegistro(string stSubKey)
		{
			//'Dim lresult As Long 'todo va bien si es cero
			//'Dim lValueType As Long  'tipo de dato
			//'Dim strBuf As String  'cadena que contiene el valor requerido
			//'Dim lDataBufSize As Long 'tama�o de la cadena en el registro
			//'Dim hKey As Long   'ubicaci�n de la clave en el registro
			//'Dim tstKeyName  As String 'clave donde estan los datos en el registro
			//'tstKeyName = "SOFTWARE\INDRA\GHOSP"
			//'lValueType = REG_SZ
			//'
			//'lresult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tstKeyName, 0, KEY_QUERY_VALUE, hKey)
			//'lresult = RegQueryValueEx(hKey, stSubKey, 0&, lValueType, ByVal 0&, lDataBufSize)
			//'    If lresult = ERROR_SUCCESS Then
			//'        If lValueType = REG_SZ Then
			//'            strBuf = String(lDataBufSize, " ")
			//'            lresult = RegQueryValueEx(hKey, stSubKey, 0&, 0&, ByVal strBuf, lDataBufSize)
			//'            If lresult = ERROR_SUCCESS Then
			//'                 fnQueryRegistro = Mid$(strBuf, 1, lDataBufSize - 1)
			//'            End If
			//'        End If
			//'    End If
			//'    If lresult <> ERROR_SUCCESS Then
			//'        'MsgBox "Pongase en contacto con inform�tica", vbCritical + vbOKOnly, "Error en conexi�n"
			//'        'End
			//'        fnQueryRegistro = ""
			//'    End If
			//'RegCloseKey (hKey)
			//'CURIA: EQUIPO DE MIGRACION - NUEVA FORMA DE OBTENER LOS DATOS DE CONEXION
			return String.Empty;
		}

		internal static bool GrabarResultado(string stGidenpac, ref string stFpeticio, string Prestacion, string usuario, int CodigoError, string DescripcionError)
		{
			try
			{
				DataSet DOPENEXT = null;
				DataSet cursor = null;
				string StSql = String.Empty;
				ValoLaboracion.ValoLabo valoracion = null;
				bool baloerror = false;
				int CodigoConcepto = 0;
				string TipoValoracion = String.Empty;
				string TipodeHoja = String.Empty;
				string stCodDoc = String.Empty;
				string stServicioRealizador = String.Empty;
				string stTextoResultado = String.Empty;
				string stFValora = String.Empty;

				baloerror = false;
				// leemos de dopenext
				//UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
				object tempRefParam = stFpeticio;
				StSql = "select * from dopenext where " + 
				        " dopenext.fpeticio=" + Serrores.FormatFechaHMS( tempRefParam) + " and " + 
				        " dopenext.gidenpac='" + stGidenpac + "' and dopenext.gprestac = '" + Prestacion.Trim() + "'";
				stFpeticio = Convert.ToString(tempRefParam);

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, cnConexion);
				DOPENEXT = new DataSet();
				tempAdapter.Fill(DOPENEXT);

				if (DOPENEXT.Tables[0].Rows.Count != 0)
				{
					//grabar valoraci�n
					
					if (!Convert.IsDBNull(DOPENEXT.Tables[0].Rows[0]["itiposer"]))
					{
						//Comprobamos si la consulta existe. Puede ser que la hayan anulado.
					
						StSql = "select * from cconsult where ganoregi = " + Convert.ToString(DOPENEXT.Tables[0].Rows[0]["ganoregi"]) + " and gnumregi = " + Convert.ToString(DOPENEXT.Tables[0].Rows[0]["gnumregi"]);
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, cnConexion);
						cursor = new DataSet();
						tempAdapter_2.Fill(cursor);
						if (cursor.Tables[0].Rows.Count != 0)
						{
							stFValora = Convert.ToString(cursor.Tables[0].Rows[0]["ffeccita"]);
							//Obtemos de SCONSGLO los datod de la hoja de la valoraci�n de laboratorio
							StSql = "select * from sconsglo where gconsglo='VALOLABO'";
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, cnConexion);
							cursor = new DataSet();
							tempAdapter_3.Fill(cursor);
							if (cursor.Tables[0].Rows.Count != 0)
							{
								
								CodigoConcepto = Convert.ToInt32(cursor.Tables[0].Rows[0]["nnumeri1"]);
								TipoValoracion = Convert.ToString(cursor.Tables[0].Rows[0]["valfanu1"]);
								TipodeHoja = Convert.ToString(cursor.Tables[0].Rows[0]["valfanu2"]);
							}
							//Buscamos el documento
							System.DateTime TempDate2 = DateTime.FromOADate(0);
							System.DateTime TempDate = DateTime.FromOADate(0);
							
							stCodDoc = "C" + Conversion.Str(DOPENEXT.Tables[0].Rows[0]["ganoregi"]).Trim() + Conversion.Str(DOPENEXT.Tables[0].Rows[0]["gnumregi"]).Trim() + ((DateTime.TryParse(stFValora, out TempDate)) ? TempDate.ToString("ddMMyyyy") : stFValora) + ((DateTime.TryParse(stFValora, out TempDate2)) ? TempDate2.ToString("HHmmss") : stFValora) + TipoValoracion.Trim() + TipodeHoja.Trim() + CodigoConcepto.ToString().Trim();
							string tempRefParam2 = Convert.ToString(DOPENEXT.Tables[0].Rows[0]["RESULTADO"]).Trim();
							stTextoResultado = ReemplazarComillaSimple(ref tempRefParam2);
							StSql = "select * from document where gnomdocu = '" + stCodDoc + "'";
							SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSql, cnConexion);
							cursor = new DataSet();
							tempAdapter_4.Fill(cursor);
							if (cursor.Tables[0].Rows.Count != 0)
							{
								//Si existe, lo modificamos
								SqlCommand tempCommand = new SqlCommand("update document set contdocu = '" + stTextoResultado + "' where gnomdocu = '" + stCodDoc + "'", cnConexion);
								tempCommand.ExecuteNonQuery();
							}
							else
							{
								//Si no lo encontramos,.....
								stServicioRealizador = "";
								//Buscamos el servicio de la prestaci�n.
								
								StSql = "SELECT gservici FROM DCODPRES WHERE " + 
								        " gprestac='" + Convert.ToString(DOPENEXT.Tables[0].Rows[0]["GPRESTAC"]).Trim() + "'"; //'and finivali<= " & FormatFechaHMS(DOPENEXT("frealiza")) & |                           ''' " and (ffinvali>=" & FormatFechaHMS(DOPENEXT("frealiza")) & " or ffinvali is null)"
								SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSql, cnConexion);
								cursor = new DataSet();
								tempAdapter_5.Fill(cursor);
								if (cursor.Tables[0].Rows.Count != 0)
								{
								
									stServicioRealizador = Convert.ToString(cursor.Tables[0].Rows[0]["gservici"]);
								}
								else
								{
									//StSql = "select nnumeri1 from sconsglo where gconsglo ='ISERLABO' "

									StSql = "SELECT gservici FROM DCODPRES WHERE gprestac='" + Convert.ToString(DOPENEXT.Tables[0].Rows[0]["GPRESTAC"]).Trim() + "'";
									SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(StSql, cnConexion);
									cursor = new DataSet();
									tempAdapter_6.Fill(cursor);
									if (cursor.Tables[0].Rows.Count != 0)
									{
									
										stServicioRealizador = Convert.ToString(cursor.Tables[0].Rows[0]["nnumeri1"]);
									}
								}
								//Lo a�adimos
								valoracion = new ValoLaboracion.ValoLabo();
							
								string tempRefParam3 = "C";
								short tempRefParam4 = (short) DOPENEXT.Tables[0].Rows[0]["ganoregi"];
								int tempRefParam5 = Convert.ToInt32(DOPENEXT.Tables[0].Rows[0]["gnumregi"]);
								int tempRefParam6 = Convert.ToInt32(Double.Parse(stServicioRealizador));
								string tempRefParam7 = "";
							    string tempRefParam8 = stTextoResultado;
								string tempRefParam9 = Conversion.Str(DOPENEXT.Tables[0].Rows[0]["fpeticio"]).Trim();
								valoracion.CreaValoracionLaboratorio( cnConexion, tempRefParam3,  tempRefParam4,tempRefParam5,  tempRefParam6,  tempRefParam7,  tempRefParam8,  tempRefParam9,  baloerror);
								stTextoResultado = Convert.ToString(tempRefParam8);
								valoracion = null;
							}
							//Actualizar DANALSOL
							
							object tempRefParam10 = DOPENEXT.Tables[0].Rows[0]["fpeticio"];
							SqlCommand tempCommand_2 = new SqlCommand("update danalsol set danalsol.oresulta = c.oresulta, danalsol.nresulta = c.nresulta, " + 
							                           "danalsol.maxresul = c.maxresul,danalsol.minresul = c.minresul,danalsol.uniresul = c.uniresul  from danalext c " + 
							                           "inner join dopenext a on c.ganoregi = a.ganoregi and c.gnumregi = a.gnumregi " + 
							                           "inner join cconsult d on d.ganoregi = a.ganoregi and d.gnumregi = a.gnumregi " + 
							                           "inner join danalsol b on 'C' = b.itiposer  and a.ganoregi = b.ganoregi  and " + 
							                           "a.gnumregi = b.gnumregi And b.fpeticio = d.fmecaniz And c.gpruelab = b.gpruelab where " + 
							                           "c.itiposer='" + Convert.ToString(DOPENEXT.Tables[0].Rows[0]["itiposer"]) + "' and c.ganoregi=" + Convert.ToString(DOPENEXT.Tables[0].Rows[0]["ganoregi"]) + " and " + 
							                           "c.gnumregi=" + Convert.ToString(DOPENEXT.Tables[0].Rows[0]["gnumregi"]) + " and c.fpeticio=" + Serrores.FormatFechaHMS( tempRefParam10), cnConexion);
							tempCommand_2.ExecuteNonQuery();

						}
						
						cursor.Close();
					}
				}
				
				DOPENEXT.Close();
				return baloerror;
			}
			catch
			{
				return true;
			}
		}


		private static string ReemplazarComillaSimple(ref string stEntrada)
		{


			int I = (stEntrada.IndexOf('\'') + 1);

			while (I != 0)
			{
				stEntrada = stEntrada.Substring(0, Math.Min(I - 1, stEntrada.Length)) + "�" + stEntrada.Substring(I);
				I = (stEntrada.IndexOf('\'') + 1);
			}

			return stEntrada;

		}





		internal static object ProRecibirError(object var1, object var2)
		{

			return null;
		}
	}
}