using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace CambioServicio
{
	partial class Cambio_se_me_en
	{

		#region "Upgrade Support "
		private static Cambio_se_me_en m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Cambio_se_me_en DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Cambio_se_me_en();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "tbHoraCambio", "SdcFecha", "Label19", "Label21", "Frame3", "CbbServicioA", "TbServicioDe", "Label2", "Label1", "Frame1", "CbbMedicoA", "TbMedicoDe", "Label4", "Label3", "Frame4", "SprCambios", "cbNuevo", "cbCambio", "cbBorrar", "Frame5", "CbImprimir", "CbCancelar", "CbAceptar", "LbPaciente", "Label28", "Frame2", "SprCambios_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraCambio;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFecha;
		public Telerik.WinControls.UI.RadLabel Label19;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadDropDownList CbbServicioA;
		public Telerik.WinControls.UI.RadTextBox TbServicioDe;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadDropDownList CbbMedicoA;
		public Telerik.WinControls.UI.RadTextBox TbMedicoDe;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		public UpgradeHelpers.Spread.FpSpread SprCambios;
		public Telerik.WinControls.UI.RadButton cbNuevo;
		public Telerik.WinControls.UI.RadButton cbCambio;
		public Telerik.WinControls.UI.RadButton cbBorrar;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		public Telerik.WinControls.UI.RadButton CbImprimir;
		public Telerik.WinControls.UI.RadButton CbCancelar;
		public Telerik.WinControls.UI.RadButton CbAceptar;
		public Telerik.WinControls.UI.RadTextBox LbPaciente;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		
        //private FarPoint.Win.Spread.SheetView SprCambios_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cambio_se_me_en));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this.tbHoraCambio = new Telerik.WinControls.UI.RadMaskedEditBox();
			this.SdcFecha = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Label19 = new Telerik.WinControls.UI.RadLabel();
			this.Label21 = new Telerik.WinControls.UI.RadLabel();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.CbbServicioA = new Telerik.WinControls.UI.RadDropDownList();
			this.TbServicioDe = new Telerik.WinControls.UI.RadTextBox();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
			this.CbbMedicoA = new Telerik.WinControls.UI.RadDropDownList();
			this.TbMedicoDe = new Telerik.WinControls.UI.RadTextBox();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
			this.SprCambios = new UpgradeHelpers.Spread.FpSpread();
			this.cbNuevo = new Telerik.WinControls.UI.RadButton();
			this.cbCambio = new Telerik.WinControls.UI.RadButton();
			this.cbBorrar = new Telerik.WinControls.UI.RadButton();
			this.CbImprimir = new Telerik.WinControls.UI.RadButton();
			this.CbCancelar = new Telerik.WinControls.UI.RadButton();
			this.CbAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.LbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.Frame3.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.Frame4.SuspendLayout();
			this.Frame5.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
			// 
			// Frame3
			// 
			//this.Frame3.BackColor = System.Drawing.SystemColors.Control;
			this.Frame3.Controls.Add(this.tbHoraCambio);
			this.Frame3.Controls.Add(this.SdcFecha);
			this.Frame3.Controls.Add(this.Label19);
			this.Frame3.Controls.Add(this.Label21);
			this.Frame3.Enabled = true;
			//this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame3.Location = new System.Drawing.Point(8, 164);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(567, 36);
			this.Frame3.TabIndex = 21;
			this.Frame3.Visible = true;
			// 
			// tbHoraCambio
			// 
			this.tbHoraCambio.AllowPromptAsInput = false;
			this.tbHoraCambio.Location = new System.Drawing.Point(316, 12);
			this.tbHoraCambio.Mask = "99:99";
			this.tbHoraCambio.Name = "tbHoraCambio";
			this.tbHoraCambio.PromptChar = ' ';
			this.tbHoraCambio.Size = new System.Drawing.Size(45, 20);
			this.tbHoraCambio.TabIndex = 22;
			this.tbHoraCambio.Enter += new System.EventHandler(this.tbHoraCambio_Enter);
			this.tbHoraCambio.Leave += new System.EventHandler(this.tbHoraCambio_Leave);
			this.tbHoraCambio.TextChanged += new System.EventHandler(this.tbHoraCambio_TextChanged);
			// 
			// SdcFecha
			// 
			//this.SdcFecha.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.SdcFecha.CustomFormat = "dd/MM/yyyy";
			this.SdcFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFecha.Location = new System.Drawing.Point(114, 12);
			this.SdcFecha.Name = "SdcFecha";
			this.SdcFecha.Size = new System.Drawing.Size(129, 17);
			this.SdcFecha.TabIndex = 23;
			// 
			// Label19
			// 
			//this.Label19.BackColor = System.Drawing.SystemColors.Control;
			//this.Label19.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label19.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label19.Location = new System.Drawing.Point(268, 12);
			this.Label19.Name = "Label19";
			this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label19.Size = new System.Drawing.Size(33, 17);
			this.Label19.TabIndex = 25;
			this.Label19.Text = "Hora:";
			// 
			// Label21
			// 
			//this.Label21.BackColor = System.Drawing.SystemColors.Control;
			//this.Label21.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label21.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label21.Location = new System.Drawing.Point(14, 12);
			this.Label21.Name = "Label21";
			this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label21.Size = new System.Drawing.Size(89, 17);
			this.Label21.TabIndex = 24;
			this.Label21.Text = "Fecha cambio:";
			// 
			// Frame1
			// 
			//this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			this.Frame1.Controls.Add(this.CbbServicioA);
			this.Frame1.Controls.Add(this.TbServicioDe);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Enabled = true;
			//this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(8, 200);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(567, 41);
			this.Frame1.TabIndex = 12;
			this.Frame1.Text = "Cambio de Servicio";
			this.Frame1.Visible = true;
			// 
			// CbbServicioA
			// 
			//this.CbbServicioA.BackColor = System.Drawing.SystemColors.Window;
			this.CbbServicioA.CausesValidation = true;
			this.CbbServicioA.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbbServicioA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
			this.CbbServicioA.Enabled = true;
			//this.CbbServicioA.ForeColor = System.Drawing.SystemColors.WindowText;
			//this.CbbServicioA.IntegralHeight = true;
			this.CbbServicioA.Location = new System.Drawing.Point(292, 14);
			this.CbbServicioA.Name = "CbbServicioA";
			this.CbbServicioA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbbServicioA.Size = new System.Drawing.Size(269, 21);
			//this.CbbServicioA.Sorted = false;
			this.CbbServicioA.TabIndex = 13;
			this.CbbServicioA.TabStop = true;
			this.CbbServicioA.Visible = true;
			this.CbbServicioA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbServicioA_KeyPress);
			this.CbbServicioA.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbServicioA_SelectionChangeCommitted);
			this.CbbServicioA.TextChanged += new System.EventHandler(this.CbbServicioA_TextChanged);
			// 
			// TbServicioDe
			// 
			//this.TbServicioDe.BackColor = System.Drawing.SystemColors.Control;
			//this.TbServicioDe.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.TbServicioDe.Cursor = System.Windows.Forms.Cursors.Default;
			//this.TbServicioDe.ForeColor = System.Drawing.SystemColors.ControlText;
			this.TbServicioDe.Location = new System.Drawing.Point(32, 14);
			this.TbServicioDe.Name = "TbServicioDe";
			this.TbServicioDe.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TbServicioDe.Size = new System.Drawing.Size(235, 20);
			this.TbServicioDe.TabIndex = 16;
            this.TbServicioDe.Enabled = false;
			// 
			// Label2
			// 
			//this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(277, 15);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(17, 17);
			this.Label2.TabIndex = 15;
			this.Label2.Text = "A:";
			// 
			// Label1
			// 
			//this.Label1.BackColor = System.Drawing.SystemColors.Control;
			//this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(8, 15);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(17, 17);
			this.Label1.TabIndex = 14;
			this.Label1.Text = "De:";
			// 
			// Frame4
			// 
			//this.Frame4.BackColor = System.Drawing.SystemColors.Control;
			this.Frame4.Controls.Add(this.CbbMedicoA);
			this.Frame4.Controls.Add(this.TbMedicoDe);
			this.Frame4.Controls.Add(this.Label4);
			this.Frame4.Controls.Add(this.Label3);
			this.Frame4.Enabled = true;
			//this.Frame4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame4.Location = new System.Drawing.Point(8, 242);
			this.Frame4.Name = "Frame4";
			this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame4.Size = new System.Drawing.Size(567, 41);
			this.Frame4.TabIndex = 8;
			this.Frame4.Text = "Cambio de M�dico";
			this.Frame4.Visible = true;
			// 
			// CbbMedicoA
			// 
			//this.CbbMedicoA.BackColor = System.Drawing.SystemColors.Window;
			this.CbbMedicoA.CausesValidation = true;
			this.CbbMedicoA.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbbMedicoA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
			this.CbbMedicoA.Enabled = true;
			//this.CbbMedicoA.ForeColor = System.Drawing.SystemColors.WindowText;
			//this.CbbMedicoA.IntegralHeight = true;
			this.CbbMedicoA.Location = new System.Drawing.Point(293, 14);
			this.CbbMedicoA.Name = "CbbMedicoA";
			this.CbbMedicoA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbbMedicoA.Size = new System.Drawing.Size(267, 21);
			//this.CbbMedicoA.Sorted = false;
			this.CbbMedicoA.TabIndex = 9;
			this.CbbMedicoA.TabStop = true;
			this.CbbMedicoA.Visible = true;
			this.CbbMedicoA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbMedicoA_KeyPress);
			this.CbbMedicoA.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbMedicoA_SelectionChangeCommitted);
			this.CbbMedicoA.TextChanged += new System.EventHandler(this.CbbMedicoA_TextChanged);
			// 
			// TbMedicoDe
			// 
			//this.TbMedicoDe.BackColor = System.Drawing.SystemColors.Control;
			//this.TbMedicoDe.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.TbMedicoDe.Cursor = System.Windows.Forms.Cursors.Default;
			//this.TbMedicoDe.ForeColor = System.Drawing.SystemColors.ControlText;
			this.TbMedicoDe.Location = new System.Drawing.Point(32, 14);
			this.TbMedicoDe.Name = "TbMedicoDe";
			this.TbMedicoDe.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TbMedicoDe.Size = new System.Drawing.Size(234, 19);
			this.TbMedicoDe.TabIndex = 17;
            this.TbMedicoDe.Enabled = false;
			// 
			// Label4
			// 
			//this.Label4.BackColor = System.Drawing.SystemColors.Control;
			//this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label4.Location = new System.Drawing.Point(277, 15);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(17, 17);
			this.Label4.TabIndex = 11;
			this.Label4.Text = "A:";
			// 
			// Label3
			// 
			//this.Label3.BackColor = System.Drawing.SystemColors.Control;
			//this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label3.Location = new System.Drawing.Point(8, 15);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(17, 17);
			this.Label3.TabIndex = 10;
			this.Label3.Text = "De:";
			// 
			// Frame5
			// 
			//this.Frame5.BackColor = System.Drawing.SystemColors.Control;
			this.Frame5.Controls.Add(this.SprCambios);
			this.Frame5.Controls.Add(this.cbNuevo);
			this.Frame5.Controls.Add(this.cbCambio);
			this.Frame5.Controls.Add(this.cbBorrar);
			this.Frame5.Enabled = true;
			//this.Frame5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame5.Location = new System.Drawing.Point(8, 42);
			this.Frame5.Name = "Frame5";
			this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame5.Size = new System.Drawing.Size(567, 121);
			this.Frame5.TabIndex = 6;
			this.Frame5.Visible = true;
			// 
			// SprCambios
			// 
			this.SprCambios.Location = new System.Drawing.Point(10, 16);
			this.SprCambios.Name = "SprCambios";
			this.SprCambios.Size = new System.Drawing.Size(464, 96);
			this.SprCambios.TabIndex = 7;
			this.SprCambios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprCambios_CellClick);
            this.SprCambios.AllowAutoSizeColumns = false;
            this.SprCambios.AutoSizeRows = true;
            this.SprCambios.AutoScroll = true;
            this.SprCambios.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.SprCambios.AllowColumnResize = true;

            // 
            // cbNuevo
            // 
            //this.cbNuevo.BackColor = System.Drawing.SystemColors.Control;
            this.cbNuevo.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbNuevo.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbNuevo.Location = new System.Drawing.Point(482, 16);
			this.cbNuevo.Name = "cbNuevo";
			this.cbNuevo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbNuevo.Size = new System.Drawing.Size(77, 27);
			this.cbNuevo.TabIndex = 20;
			this.cbNuevo.Text = "&Nuevo";
			this.cbNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbNuevo.UseVisualStyleBackColor = false;
			this.cbNuevo.Click += new System.EventHandler(this.cbNuevo_Click);
            this.cbNuevo.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cbCambio
            // 
            //this.cbCambio.BackColor = System.Drawing.SystemColors.Control;
            this.cbCambio.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbCambio.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCambio.Location = new System.Drawing.Point(482, 84);
			this.cbCambio.Name = "cbCambio";
			this.cbCambio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCambio.Size = new System.Drawing.Size(77, 27);
			this.cbCambio.TabIndex = 19;
			this.cbCambio.Text = "&Cambio";
			this.cbCambio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbCambio.UseVisualStyleBackColor = false;
			this.cbCambio.Click += new System.EventHandler(this.cbCambio_Click);
            this.cbCambio.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cbBorrar
            // 
            //this.cbBorrar.BackColor = System.Drawing.SystemColors.Control;
            this.cbBorrar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbBorrar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbBorrar.Location = new System.Drawing.Point(482, 50);
			this.cbBorrar.Name = "cbBorrar";
			this.cbBorrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbBorrar.Size = new System.Drawing.Size(77, 27);
			this.cbBorrar.TabIndex = 18;
			this.cbBorrar.Text = "&Eliminar";
			this.cbBorrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbBorrar.UseVisualStyleBackColor = false;
			this.cbBorrar.Click += new System.EventHandler(this.cbBorrar_Click);
            this.cbBorrar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CbImprimir
            // 
            //this.CbImprimir.BackColor = System.Drawing.SystemColors.Control;
            this.CbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbImprimir.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CbImprimir.Location = new System.Drawing.Point(318, 290);
			this.CbImprimir.Name = "CbImprimir";
			this.CbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbImprimir.Size = new System.Drawing.Size(81, 29);
			this.CbImprimir.TabIndex = 5;
			this.CbImprimir.Text = "&Imprimir Movimientos";
			this.CbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.CbImprimir.UseVisualStyleBackColor = false;
			this.CbImprimir.Visible = false;
			this.CbImprimir.Click += new System.EventHandler(this.CbImprimir_Click);
            this.CbImprimir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CbCancelar
            // 
            //this.CbCancelar.BackColor = System.Drawing.SystemColors.Control;
            this.CbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CbCancelar.Location = new System.Drawing.Point(494, 290);
			this.CbCancelar.Name = "CbCancelar";
			this.CbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCancelar.Size = new System.Drawing.Size(81, 29);
			this.CbCancelar.TabIndex = 0;
			this.CbCancelar.Text = "&Cerrar";
			this.CbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.CbCancelar.UseVisualStyleBackColor = false;
			this.CbCancelar.Click += new System.EventHandler(this.CbCancelar_Click);
            this.CbCancelar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CbAceptar
            // 
            //this.CbAceptar.BackColor = System.Drawing.SystemColors.Control;
            this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CbAceptar.Location = new System.Drawing.Point(406, 290);
			this.CbAceptar.Name = "CbAceptar";
			this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbAceptar.Size = new System.Drawing.Size(81, 29);
			this.CbAceptar.TabIndex = 1;
			this.CbAceptar.Text = "&Aceptar";
			this.CbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.CbAceptar.UseVisualStyleBackColor = false;
			this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
            this.CbAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame2
            // 
            //this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            this.Frame2.Controls.Add(this.LbPaciente);
			this.Frame2.Controls.Add(this.Label28);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(8, 2);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(568, 39);
			this.Frame2.TabIndex = 2;
			this.Frame2.Visible = true;
			// 
			// LbPaciente
			// 
			//this.LbPaciente.BackColor = System.Drawing.SystemColors.Control;
			//this.LbPaciente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			//this.LbPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LbPaciente.Location = new System.Drawing.Point(78, 12);
			this.LbPaciente.Name = "LbPaciente";
			this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbPaciente.Size = new System.Drawing.Size(478, 19);
			this.LbPaciente.TabIndex = 4;
            this.LbPaciente.Enabled = false;
			// 
			// Label28
			// 
			//this.Label28.BackColor = System.Drawing.SystemColors.Control;
			//this.Label28.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label28.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label28.Location = new System.Drawing.Point(14, 14);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(51, 17);
			this.Label28.TabIndex = 3;
			this.Label28.Text = "Paciente:";
			// 
			// Cambio_se_me_en
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(586, 326);
			this.Controls.Add(this.Frame3);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.Frame4);
			this.Controls.Add(this.Frame5);
			this.Controls.Add(this.CbImprimir);
			this.Controls.Add(this.CbCancelar);
			this.Controls.Add(this.CbAceptar);
			this.Controls.Add(this.Frame2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Cambio_se_me_en";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Cambio de Servicio/M�dico -  AGI120F1";
			this.Activated += new System.EventHandler(this.Cambio_se_me_en_Activated);
			this.Closed += new System.EventHandler(this.Cambio_se_me_en_Closed);
			this.Load += new System.EventHandler(this.Cambio_se_me_en_Load);
			this.Frame3.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.Frame4.ResumeLayout(false);
			this.Frame5.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}