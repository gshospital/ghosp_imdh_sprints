using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace CambioServicio
{
	internal static class bCambioServicio
	{

		public static SqlConnection RcAdmision = null;
		public static object CrystalReport1 = null;
		public static string PathString = String.Empty;
		public static Mensajes.ClassMensajes objError = null;
		public static string VstCodUsua = String.Empty;

		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public bool VfCabecera;
			public string VstTelefoHo;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					result.VstTelefoHo = String.Empty;
					return result;
			}
		}
		public static bCambioServicio.CabCrystal VstrCrystal = bCambioServicio.CabCrystal.CreateInstance();

		public static string VstCrystal = String.Empty;
		public static string gUsuario = String.Empty;
		public static string gContrase�a = String.Empty;

		internal static void ActualizarServicioConsumos(int A�o, int Numero, int vCodSerOld, string vCodSerNew, ref string FechaNuevo, bool fNuevo, ref string FechaFinMovimiento)
		{

			//************************************************************************
			//***** ACTUALIZAMOS LOS APUNTES GENERADOS EN FCONSPAC (CONSUMOS) ********
			//************************************************************************
			string stSqlFact = "Update FCONSPAC set gserreal = '" + vCodSerNew + "'";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + A�o.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + Numero.ToString() + " and ";
			stSqlFact = stSqlFact + " gserreal='" + vCodSerOld.ToString() + "'";

			if (fNuevo)
			{ // si es el �ltimo apunte o el primero

				stSqlFact = stSqlFact + " and foper>=" + Serrores.FormatFechaHM(FechaNuevo) + "";

			}
			else
			{
				if (FechaFinMovimiento == "00:00:00")
				{

					stSqlFact = stSqlFact + " and foper>=" + Serrores.FormatFechaHM(FechaNuevo) + "";

				}
				else
				{

					stSqlFact = stSqlFact + " and foper>=" + Serrores.FormatFechaHM(FechaNuevo) + "";
					stSqlFact = stSqlFact + " and foper<=" + Serrores.FormatFechaHM(FechaFinMovimiento) + "";

				}
			}

			SqlCommand tempCommand = new SqlCommand(stSqlFact, RcAdmision);
			tempCommand.ExecuteNonQuery();

		}

		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************


			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RcAdmision);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			VstrCrystal.VstGrupoHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1, valfanu2 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, RcAdmision);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			VstrCrystal.VstNombreHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			VstrCrystal.VstTelefoHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU2"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU2"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, RcAdmision);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			VstrCrystal.VstDireccHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tRrCrystal.Close();

		}
	}
}