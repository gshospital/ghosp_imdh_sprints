using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace CambioServicio
{
	public partial class Cambio_se_me_en
		: RadForm
	{


		string stFechaMaxMov = String.Empty;

		bool fMedico = false; //Cambia Médico
		bool fServicio = false; //Cambia servicio

		int viCodMedico = 0; //código médico de
		int viServiciDe = 0; //código servicio de
		string vstSegundos = String.Empty; // segundos del sistema
		int sthorasystem = 0; // horas del sistema
		int stminutossystem = 0; //minutos del sistema
		string stSeleccion = String.Empty; //se utiliza para el report
		//
		string vstfecha_hora_sys = String.Empty;
		string vstfecha = String.Empty;
		string vsthora = String.Empty;

		//Dim CrystalReport1 As Crystal.CrystalReport

		string vstAccion = String.Empty; //se guarda la accion que se ha seleccionado
		string stFechaIngreso = String.Empty; //lleva la fecha de llegada de AEPISADM

		int lRegistroSeleccionado = 0; //registro marcado en el spread
		bool fFilaSeleccionada = false; //guarda si hay una fila marcada o no

		string stCodpac = String.Empty; //Gidenpac del paciente
		int iAñoRegistro = 0; //ganoregi del paciente
		int iNumRegistro = 0; //gnumregi del paciente
		//datos origen
		string stUnidadOr = String.Empty;
		int iServiciOr = 0;
		int iMedicoOr = 0;
		int iPlantaOr = 0;
		int iHabitaOr = 0;
		string stCamaOr = String.Empty;

		//datos destino
		string stFfinmovi = String.Empty; //ffinmovi de la fila borrada
		string stUnidadDes = String.Empty;
		int iPlantaDes = 0;
		int iHabitaDes = 0;
		string stCamaDes = String.Empty;

		int viCodMedicoDes = 0; //código médico de
		int viServicioDeS = 0; //código servicio de

		string stTipotra = String.Empty;
		int iMotitras = 0;
		string stIrotaci = String.Empty;
		public Cambio_se_me_en()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		public void proRecPaciente(string stCodPaciente, string stNombPaciente)
		{
			//por donde entra desde el menu de ingresos
			//antes de salir de la pantalla del menu entra en este procedimiento
			string stSqCambio = String.Empty;
			DataSet RrMaximo = null;

			try
			{

				LbPaciente.Text = stNombPaciente;
				stCodpac = stCodPaciente;

				stSqCambio = "SELECT AEPISADM.ganoadme,AEPISADM.gnumadme,AEPISADM.fllegada,AMOVIMIE.fmovimie  " + " FROM AEPISADM, AMOVIMIE " + " WHERE " + " AEPISADM.GNUMADME = AMOVIMIE.GNUMREGI AND " + " AEPISADM.GANOADME = AMOVIMIE.GANOREGI AND " + " AEPISADM.GIDENPAC = '" + stCodpac + "' and faltplan is null " + " order by AMOVIMIE.fmovimie ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqCambio, bCambioServicio.RcAdmision);
				RrMaximo = new DataSet();
				tempAdapter.Fill(RrMaximo);
				if (RrMaximo.Tables[0].Rows.Count != 0)
				{
					//nos posicionamos en el último para recoger la ultima fecha
					RrMaximo.MoveLast(null);
					//dMaxMov = RrMaximo("fmovimie")
					stFechaMaxMov = Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss");
					stFechaIngreso = Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:mm:ss");
					//en el mindate ponemos la fecha de llegada del paciente
					SdcFecha.MinDate = DateTime.Parse(Convert.ToDateTime(RrMaximo.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy"));
					iAñoRegistro = Convert.ToInt32(RrMaximo.Tables[0].Rows[0]["ganoadme"]);
					iNumRegistro = Convert.ToInt32(RrMaximo.Tables[0].Rows[0]["gnumadme"]);
				}
				else
				{
                    RadMessageBox.Show("no hay movimientos del paciente ", Application.ProductName);

                }
				RrMaximo.Close();

				//limpiamos los campos
				TbServicioDe.Text = "";
				TbMedicoDe.Text = "";

				CbbServicioA.Items.Clear();
				CbbMedicoA.Items.Clear();
			}
			catch (Exception Excep)
			{
				//**********
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_se_me_en:proRecPaciente", Excep);
			}

		}

		public void rellena_grid()
		{

            //SDSALAZAR
            int longi = 0;
            int longi2 = 0;
            longi = 0;
            longi2 = 300;
            SprCambios.MaxCols = 6;
            SprCambios.MaxRows = 0;
            SprCambios.Row = 0;
            SprCambios.Col = 1;
            SprCambios.Text = "Fecha del cambio";
            //SprCambios.SetColWidth(1, CreateGraphics().MeasureString(SprCambios.Text, Font).Width * 15 + longi + 100);
                SprCambios.SetColWidth(1, 120);
            SprCambios.Col = 2;
            SprCambios.Text = "Servicio";
            //SprCambios.SetColWidth(2, CreateGraphics().MeasureString(SprCambios.Text, Font).Width * 15 + longi + 2200);
            SprCambios.SetColWidth(2, 180);
            SprCambios.Col = 3;
            SprCambios.Text = "Médico";
            //SprCambios.SetColWidth(3, CreateGraphics().MeasureString(SprCambios.Text, Font).Width * 15 + longi + 2225);
            SprCambios.SetColWidth(3,  210);
            SprCambios.Col = 4; //fecha/hora completa
            SprCambios.SetColHidden(SprCambios.Col, true);
            SprCambios.Col = 5; //codigo servicio
            SprCambios.SetColHidden(SprCambios.Col, true);
            SprCambios.Col = 6; //codigo medico
            SprCambios.SetColHidden(SprCambios.Col, true);

            int lcol = 0;
			int lfil = 0;

			int iServicioAux = -1;
			int lMedicoaux = -1;
			SprCambios.MaxRows = 0;

			string StSqlPlanta = "select DSERVICI.dnomserv,DPERSONA.dap1pers,DPERSONA.dap2pers,DPERSONA.dnomperS,AMOVIMIE.* " + "from AMOVIMIE,DSERVICI,DPERSONA Where AMOVIMIE.itiposer = 'H' " + "and AMOVIMIE.ganoregi = " + iAñoRegistro.ToString() + " And AMOVIMIE.gnumregi = " + iNumRegistro.ToString() + " " + "and AMOVIMIE.gservior = DSERVICI.gservici AND AMOVIMIE.gmedicor = DPERSONA.gpersona ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSqlPlanta, bCambioServicio.RcAdmision);
			DataSet RrPlanta = new DataSet();
			tempAdapter.Fill(RrPlanta);
			if (RrPlanta.Tables[0].Rows.Count != 0)
			{
				RrPlanta.MoveFirst();
				foreach (DataRow iteration_row in RrPlanta.Tables[0].Rows)
				{
					//el primer registro tiene que salir
					//guardamos el servicio y el medico del primero
					if (iServicioAux != Convert.ToDouble(iteration_row["gservior"]) || lMedicoaux != Convert.ToDouble(iteration_row["gmedicor"]))
					{
						SprCambios.MaxRows ++;
						iServicioAux = Convert.ToInt32(iteration_row["gservior"]);
						lMedicoaux = Convert.ToInt32(iteration_row["gmedicor"]);
						lfil ++;
						SprCambios.Row = lfil + 1;
                        //lcol ++;
                        //SprCambios.Col = lcol;
                        SprCambios.Col = 1;

                        SprCambios.Text = Convert.ToDateTime(iteration_row["fmovimie"]).ToString("dd/MM/yyyy HH:mm");
                        //lcol ++;
                        //SprCambios.Col = lcol;
                        SprCambios.Col = 2;
                        SprCambios.Text = Convert.ToString(iteration_row["dnomserv"]).Trim();
                        //lcol ++;
                        //SprCambios.Col = lcol;
                        SprCambios.Col = 3;
                        SprCambios.Text = Convert.ToString(iteration_row["dap1pers"]).Trim() + " " + Convert.ToString(iteration_row["dap2pers"]).Trim() + "," + Convert.ToString(iteration_row["dnompers"]).Trim();
                        //columnas ocultas
                        //lcol ++;
                        ////hora completa
                        //SprCambios.Col = lcol;
                        SprCambios.Col = 4;
                        SprCambios.Text = Convert.ToDateTime(iteration_row["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss");
                        //lcol ++;
                        ////cod servicio
                        //SprCambios.Col = lcol;
                        SprCambios.Col = 5;
                        SprCambios.Text = Convert.ToString(iteration_row["gservior"]);
                        //lcol ++;
                        ////cod medico
                        //SprCambios.Col = lcol;
                        SprCambios.Col = 6;
                        SprCambios.Text = Convert.ToString(iteration_row["gmedicor"]);
					}
					lcol = 0;
				}
			}
			else
			{
				//no se recupera ningun movimiento
			}

			RrPlanta.Close();
		}

		private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//para select auxiliares
			string stTemp = String.Empty;
			DataSet RrTemp = null;
			//para las select de la transaccion
			string StSqAceptar = String.Empty;
			DataSet RrAceptar = null;
			string stFechaMovimiento = String.Empty;
			string stFechaSiguiente = String.Empty;

			string sEnf = String.Empty, sCama = String.Empty;
			int iPlan = 0, iSer = 0, ihab = 0;
			int iSerQueCambia = 0; // para actualizar los consumos
			int iMed = 0;

			int ServicioCambio = 0;
			int MedicoCambio = 0;

			int ServicioAnterior = 0;
			int MedicoAnterior = 0;

			int ServicioPosterior = 0;
			int MedicoPosterior = 0;



			//****************************
			try
			{
				//*******************************
				this.Cursor = Cursors.WaitCursor;


				//la hora tiene que estar rellena
				if (tbHoraCambio.Text == "  :  ")
				{
					this.Cursor = Cursors.Default;
					short tempRefParam = 1040;
					string [] tempRefParam2 = new string[] {Label19.Text};
					Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, bCambioServicio.RcAdmision, tempRefParam2));
					tbHoraCambio.Focus();
					return;
				}
				//VALIDAR QUE LA FECHA SEA POSTERIOR AL ÚLTIMO MOVIMIENTO
				//NO PERMITIMOS CAMBIAR LA FECHA A CUALQUIER DIA POR QUE
				//SE DESCABALA EL ORDEN CRONOLOGICO DE LOS APUNTES CERRANDO UN APUNTE
				//CON FECHA ANTERIOR A LA DEL INICIO
				if (vstAccion == "NUEVO")
				{
					//se valida que la fecha/hora sea posterior a el último movimiento
					stTemp = " select * from amovimie,aepisadm where gidenpac='" + stCodpac + "'" + " and ganoadme=ganoregi and gnumadme=gnumregi and faltplan is null " + " and ffinmovi is null";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(stTemp, bCambioServicio.RcAdmision);
					RrTemp = new DataSet();
					tempAdapter.Fill(RrTemp);
					if (RrTemp.Tables[0].Rows.Count != 0)
					{
						stFechaMaxMov = Convert.ToDateTime(RrTemp.Tables[0].Rows[0]["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss");
						if (Convert.ToDateTime(RrTemp.Tables[0].Rows[0]["fmovimie"]) > DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos))
						{
							this.Cursor = Cursors.Default;
							short tempRefParam3 = 1020;
							string [] tempRefParam4 = new string [] { "la fecha del cambio", "mayor", "la fecha del último cambio " + Convert.ToDateTime(RrTemp.Tables[0].Rows[0]["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss") };
                            Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, bCambioServicio.RcAdmision, tempRefParam4));
							RrTemp.Close();
							SdcFecha.Focus();
							return;
						}
					}
					RrTemp.Close();
					//la fecha no puede ser menor que la fecha del ingreso
					if (DateTime.Parse(stFechaIngreso) > DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos))
					{
						this.Cursor = Cursors.Default;
						System.DateTime TempDate = DateTime.FromOADate(0);
						short tempRefParam5 = 1020;
						string [] tempRefParam6 = new string [] {"la fecha del movimiento", "mayor", "la fecha de ingreso " + ((DateTime.TryParse(stFechaIngreso, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : stFechaIngreso)};
						Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, bCambioServicio.RcAdmision, tempRefParam6));
						SdcFecha.Focus();
						return;
					}
				}
				else
				{
					//en el caso del cambio no se permite cambiar la fecha/hora
					//cuando se trata de un cambio hay que validar que el cambio sea diferente al movimiento anterior y
					//al movimiento posterior al seleccionado
					if (CbbServicioA.Text.Trim() == "")
					{
						SprCambios.Row = lRegistroSeleccionado;
						SprCambios.Col = 5;
						ServicioCambio = Convert.ToInt32(Double.Parse(SprCambios.Text));
					}
					else
					{
						ServicioCambio = Convert.ToInt32(CbbServicioA.SelectedValue);
					}

					if (CbbMedicoA.Text.Trim() == "")
					{
						SprCambios.Row = lRegistroSeleccionado;
						SprCambios.Col = 6;
						MedicoCambio = Convert.ToInt32(Double.Parse(SprCambios.Text));
					}
					else
					{
						MedicoCambio = Convert.ToInt32(CbbMedicoA.SelectedValue); 
					}

					if (SprCambios.MaxRows > 1)
					{
						//si hay mas de un registro en el spread
						if (lRegistroSeleccionado == 1)
						{
							//si es el primer registro el seleccionado
							//se compara con el siguiente
							SprCambios.Row = lRegistroSeleccionado + 1;
							SprCambios.Col = 5;
							ServicioPosterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
							SprCambios.Col = 6;
							MedicoPosterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
							if (ServicioCambio == ServicioPosterior && MedicoCambio == MedicoPosterior)
							{
								short tempRefParam7 = 1125;
								string [] tempRefParam8 = new string[] {"El servicio y el médico coinciden con el cambio posterior ", "modificar el registro seleccionado"};
								Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, bCambioServicio.RcAdmision, tempRefParam8));
								if (Serrores.iresume == 7)
								{
									//se tiene que salir de la rutina
									this.Cursor = Cursors.Default;
									return;
								}
							}
						}
						else
						{
							if (lRegistroSeleccionado == SprCambios.MaxRows)
							{
								//si se trata del ultimo registro
								//se compara con el anterior
								SprCambios.Row = lRegistroSeleccionado - 1;
								SprCambios.Col = 5;
								ServicioAnterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
								SprCambios.Col = 6;
								MedicoAnterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
								if (ServicioCambio == ServicioAnterior && MedicoCambio == MedicoAnterior)
								{
									short tempRefParam9 = 1125;
									string[] tempRefParam10 = new string[] {"El servicio y el médico coinciden con el cambio anterior ", "modificar el registro seleccionado"};
									Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, bCambioServicio.RcAdmision, tempRefParam10));
									if (Serrores.iresume == 7)
									{
										this.Cursor = Cursors.Default;
										return;
									}
								}
							}
							else
							{
								//si es uno intermedio se tiene que comparar con el anterior
								//y con el posterior
								SprCambios.Row = lRegistroSeleccionado + 1;
								SprCambios.Col = 5;
								ServicioPosterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
								SprCambios.Col = 6;
								MedicoPosterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
								if (ServicioCambio == ServicioPosterior && MedicoCambio == MedicoPosterior)
								{
									short tempRefParam11 = 1125;
									string[] tempRefParam12 = new string[] {"El servicio y el médico coinciden con el cambio posterior ", "modificar el registro seleccionado"};
									Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, bCambioServicio.RcAdmision, tempRefParam12));
									if (Serrores.iresume == 7)
									{
										this.Cursor = Cursors.Default;
										return;
									}
								}
								SprCambios.Row = lRegistroSeleccionado - 1;
								SprCambios.Col = 5;
								ServicioAnterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
								SprCambios.Col = 6;
								MedicoAnterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
								if (ServicioCambio == ServicioAnterior && MedicoCambio == MedicoAnterior)
								{
									short tempRefParam13 = 1125;
									string[] tempRefParam14 = new string[] {"El servicio y el médico coinciden con el cambio anterior ", "modificar el registro seleccionado"};
									Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, bCambioServicio.RcAdmision, tempRefParam14));
									if (Serrores.iresume == 7)
									{
										this.Cursor = Cursors.Default;
										return;
									}
								}
							}
						}
					}
				}
				//cuidado habria que validar de otra forma el medico y el servicio

				if (fServicio && !fMedico)
				{
					stTemp = "SELECT * " + " FROM DPERSONAva,DPERSERV " + " WHERE GSERVICI=" + CbbServicioA.Items[CbbServicioA.SelectedIndex].Value.ToString() + " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA " + " and dpersonava.gpersona =" + viCodMedico.ToString();
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stTemp, bCambioServicio.RcAdmision);
					RrTemp = new DataSet();
					tempAdapter_2.Fill(RrTemp);
					if (RrTemp.Tables[0].Rows.Count == 0)
					{ //DEBE CAMBIAR EL MÉDICO POR UNO QUE CORRESPONDA AL SERVICIO
						this.Cursor = Cursors.Default;
						short tempRefParam15 = 1040;
						string[] tempRefParam16 = new string[]{"Médico"};
						Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, bCambioServicio.RcAdmision, tempRefParam16));
						RrTemp.Close();
						CbbMedicoA.Focus();
						return;
					}
				}

				switch(vstAccion)
				{
					case "NUEVO" : 
						//SqlTransaction transaction = bCambioServicio.RcAdmision.BeginTrans();
                        bCambioServicio.RcAdmision.BeginTransScope();
                        object tempRefParam17 = stFechaMaxMov; 
						StSqAceptar = "SELECT * from AMOVIMIE where itiposer = 'H' and " + " GNUMREGI = " + iNumRegistro.ToString() + " AND GANOREGI = " + iAñoRegistro.ToString() + " and " + " FMOVIMIE =" + Serrores.FormatFechaHMS(tempRefParam17); 
						stFechaMaxMov = Convert.ToString(tempRefParam17); 
						 
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSqAceptar, bCambioServicio.RcAdmision); 
						RrAceptar = new DataSet(); 
						tempAdapter_3.Fill(RrAceptar); 
						if (RrAceptar.Tables[0].Rows.Count != 0)
						{
							//completo el último registro de amovimie
							RrAceptar.Edit();

							RrAceptar.Tables[0].Rows[0]["ffinmovi"] = DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos);
							RrAceptar.Tables[0].Rows[0]["GUNENFDE"] = RrAceptar.Tables[0].Rows[0]["GUNENFOR"];

							if (fServicio)
							{
								//RrAceptar("GSERVIDE") = CbbServicioA.ItemData(CbbServicioA.ListIndex)
								RrAceptar.Tables[0].Rows[0]["GSERVIDE"] = viServicioDeS;
							}
							else
							{
								RrAceptar.Tables[0].Rows[0]["GSERVIDE"] = RrAceptar.Tables[0].Rows[0]["gSERVIOR"];
								viServicioDeS = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["GSERVIOR"]);
							}
							if (fMedico)
							{
								//RrAceptar("GMEDICDE") = CbbMedicoA.ItemData(CbbMedicoA.ListIndex)
								RrAceptar.Tables[0].Rows[0]["GMEDICDE"] = viCodMedicoDes;
							}
							else
							{
								RrAceptar.Tables[0].Rows[0]["GMEDICDE"] = RrAceptar.Tables[0].Rows[0]["GMEDICOR"];
								viCodMedicoDes = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["GMEDICOR"]);
							}
							RrAceptar.Tables[0].Rows[0]["GPLANTDE"] = RrAceptar.Tables[0].Rows[0]["GPLANTOR"];
							RrAceptar.Tables[0].Rows[0]["GHABITDE"] = RrAceptar.Tables[0].Rows[0]["GHABITOR"];
							RrAceptar.Tables[0].Rows[0]["GCAMADES"] = RrAceptar.Tables[0].Rows[0]["GCAMAORI"];

							iSerQueCambia = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["gSERVIOR"]);

							string tempQuery = RrAceptar.Tables[0].TableName;
							SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
                            tempAdapter_3.Update(RrAceptar, RrAceptar.Tables[0].TableName);
							//*guardo los datos en variables para grabarlos en el nuevo registro
							sEnf = Convert.ToString(RrAceptar.Tables[0].Rows[0]["GUNENFOR"]);
							iSer = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["GSERVIDE"]);
							iPlan = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["GPLANTDE"]);
							ihab = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["GHABITDE"]);
							sCama = Convert.ToString(RrAceptar.Tables[0].Rows[0]["GCAMADES"]);
							iMed = Convert.ToInt32(RrAceptar.Tables[0].Rows[0]["gmedicor"]);

							RrAceptar.Close();
							//**********grabo el nuevo registro en amovimie
							StSqAceptar = "SELECT * " + " FROM  AMOVIMIE where 2=1 ";

							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSqAceptar, bCambioServicio.RcAdmision);
							RrAceptar = new DataSet();
							tempAdapter_5.Fill(RrAceptar);

							RrAceptar.AddNew();

							RrAceptar.Tables[0].Rows[0]["FMOVIMIE"] = DateTime.Parse(SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos);
							RrAceptar.Tables[0].Rows[0]["GANOREGI"] = iAñoRegistro;
							RrAceptar.Tables[0].Rows[0]["GNUMREGI"] = iNumRegistro;
							RrAceptar.Tables[0].Rows[0]["ITIPOSER"] = "H";
							RrAceptar.Tables[0].Rows[0]["GUNENFOR"] = sEnf;
							RrAceptar.Tables[0].Rows[0]["GSERVIOR"] = iSer;
							viServiciDe = iSer;
							RrAceptar.Tables[0].Rows[0]["GPLANTOR"] = iPlan;
							RrAceptar.Tables[0].Rows[0]["GHABITOR"] = ihab;
							RrAceptar.Tables[0].Rows[0]["GCAMAORI"] = sCama;
							RrAceptar.Tables[0].Rows[0]["GUSUARIO"] = bCambioServicio.VstCodUsua;
							RrAceptar.Tables[0].Rows[0]["FSISTEMA"] = vstfecha_hora_sys;


							if (CbbMedicoA.SelectedIndex != -1)
							{
								//RrAceptar("GMEDICOR") = CbbMedicoA.ItemData(CbbMedicoA.ListIndex)
								RrAceptar.Tables[0].Rows[0]["GMEDICOR"] = viCodMedicoDes;
								//viCodMedico = CbbMedicoA.ItemData(CbbMedicoA.ListIndex)
								viCodMedico = viCodMedicoDes;
							}
							else
							{
								RrAceptar.Tables[0].Rows[0]["gmedicor"] = iMed;
								viCodMedico = iMed;
							}
							string tempQuery_2 = RrAceptar.Tables[0].TableName;
							SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_5);
                            tempAdapter_5.Update(RrAceptar, RrAceptar.Tables[0].TableName);
							RrAceptar.Close();

							ActualizarAEPISADM(-1, -1, viServicioDeS, viCodMedicoDes);

							//Nuevo Requerimiento
							if (!ActualizarDMOVFACT_nuevos(iAñoRegistro, iNumRegistro))
							{
                                bCambioServicio.RcAdmision.RollbackTransScope();
                                this.Cursor = Cursors.Default;
								return;
							}

							if (fServicio)
							{
								string tempRefParam18 = SdcFecha.Text + " " + tbHoraCambio.Text + ":" + vstSegundos;
								string tempRefParam19 = "00:00:00";
								bCambioServicio.ActualizarServicioConsumos(iAñoRegistro, iNumRegistro, iSerQueCambia, viServicioDeS.ToString(), ref tempRefParam18, true, ref tempRefParam19);
							}

                            bCambioServicio.RcAdmision.CommitTransScope();

                        } 
						//fin de caso nuevo 
						break;
					case "CAMBIO" : 
						//tiene que existir un registro seleccionado 
						SprCambios.Row = lRegistroSeleccionado; 
						SprCambios.Col = 4; 
						stFechaMovimiento = SprCambios.Text; 
						 
						//buscar si hay movimientos en dmovfact 
						object tempRefParam20 = stFechaMovimiento; 
						StSqAceptar = "select * from DMOVFACT where itiposer = 'H' and " + " ganoregi=" + iAñoRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " and " + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam20) + " and ffactura is not null"; 
						stFechaMovimiento = Convert.ToString(tempRefParam20); 
						 
						if (Serrores.proExisteTabla("DMOVFACH", bCambioServicio.RcAdmision))
						{
							string tempRefParam21 = "DMOVFACT";
							StSqAceptar = StSqAceptar + " UNION ALL " + Serrores.proReplace(ref StSqAceptar, tempRefParam21, "DMOVFACH");
						} 
						 
						SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(StSqAceptar, bCambioServicio.RcAdmision); 
						RrAceptar = new DataSet(); 
						tempAdapter_7.Fill(RrAceptar); 
						if (RrAceptar.Tables[0].Rows.Count != 0)
						{
							//si hay filas en DMOVFACT no se le permite modificar los registros
							RrAceptar.Close();
							this.Cursor = Cursors.Default;
							short tempRefParam22 = 1940;
							string[] tempRefParam23 = new string[]{"modificar", "ya ha sido facturado"};
							Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam22, bCambioServicio.RcAdmision, tempRefParam23));
							return;
						}
						else
						{
							SprCambios.Col = 5; // código de servicio
							iSerQueCambia = Convert.ToInt32(Double.Parse(SprCambios.Text));

							//no hay filas en dmovfat
							//viServiciDe = CbbServicioA.ItemData(CbbServicioA.ListIndex)
							//viCodMedico = CbbMedicoA.ItemData(CbbMedicoA.ListIndex)
							viServiciDe = viServicioDeS;
							viCodMedico = viCodMedicoDes;

							if (lRegistroSeleccionado == 1 && SprCambios.MaxRows == 1)
							{
								//es la primera y la unica fila de spread
								//pueden existir movimientos en AMOVIMIE por tralados de camas antes y despues
							    bCambioServicio.RcAdmision.BeginTransScope();
                                //modificar en AEPISADM gservici,gpersona,gserulti,gperulti
                                ActualizarAEPISADM(viServiciDe, viCodMedico, viServiciDe, viCodMedico);
								//camposOrigen
								object tempRefParam24 = stFechaMovimiento;
								ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam24) + " ", "ORIGEN", viServiciDe, viCodMedico);
								stFechaMovimiento = Convert.ToString(tempRefParam24);
								//campos destino
								object tempRefParam25 = stFechaMovimiento;
								ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam25) + " and ffinmovi is not null ", "DESTINO", viServiciDe, viCodMedico);
								stFechaMovimiento = Convert.ToString(tempRefParam25);
								//se actualiza DMOVFACT
								string tempRefParam26 = DateTimeHelper.ToString(DateTime.Now);
								ActualizarDMOVFACT(ref stFechaMovimiento, ref tempRefParam26, viServiciDe, viCodMedico);

								if (iSerQueCambia != viServicioDeS)
								{ // si ha cambiado el servicio

									string tempRefParam27 = SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos;
									string tempRefParam28 = "00:00:00";
									bCambioServicio.ActualizarServicioConsumos(iAñoRegistro, iNumRegistro, iSerQueCambia, viServicioDeS.ToString(), ref tempRefParam27, true, ref tempRefParam28);

								}

							    bCambioServicio.RcAdmision.CommitTransScope();
                            }
							else
							{
								//guardamos los datos de la fila siguiente al seleccionado
								SprCambios.Row = lRegistroSeleccionado + 1;
								SprCambios.Col = 4;
								stFechaSiguiente = SprCambios.Text;
								if (lRegistroSeleccionado == 1 && SprCambios.MaxRows > 1)
								{
                                    bCambioServicio.RcAdmision.BeginTransScope();
                                    //es la primera fila pero no la ultima
                                    //se actualiza en AEPISADM gservici,gpersona
                                    ActualizarAEPISADM(viServiciDe, viCodMedico);
									//camposOrigen
									object tempRefParam29 = stFechaMovimiento;
									object tempRefParam30 = stFechaSiguiente;
									ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam29) + " and fmovimie < " + Serrores.FormatFechaHMS(tempRefParam30) + " ", "ORIGEN", viServiciDe, viCodMedico);
									stFechaSiguiente = Convert.ToString(tempRefParam30);
									stFechaMovimiento = Convert.ToString(tempRefParam29);
									//camposdestino
									object tempRefParam31 = stFechaMovimiento;
									object tempRefParam32 = stFechaSiguiente;
									ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam31) + " and ffinmovi < " + Serrores.FormatFechaHMS(tempRefParam32) + " ", "DESTINO", viServiciDe, viCodMedico);
									stFechaSiguiente = Convert.ToString(tempRefParam32);
									stFechaMovimiento = Convert.ToString(tempRefParam31);
									//se actualiza DMOVFACT
									ActualizarDMOVFACT(ref stFechaMovimiento, ref stFechaSiguiente, viServiciDe, viCodMedico);

									if (iSerQueCambia != viServicioDeS)
									{ // si ha cambiado el servicio

										string tempRefParam33 = SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos;
										string tempRefParam34 = DateTime.Parse(SprCambios.Text).ToString("dd/MM/yyyy HH:mm:ss");
										bCambioServicio.ActualizarServicioConsumos(iAñoRegistro, iNumRegistro, iSerQueCambia, viServicioDeS.ToString(), ref tempRefParam33, false, ref tempRefParam34);

									}

									bCambioServicio.RcAdmision.CommitTransScope();
                                }
								else
								{
									//si es la ultima fila del spread
									if (lRegistroSeleccionado == SprCambios.MaxRows)
									{
                                        bCambioServicio.RcAdmision.BeginTransScope();
                                        //se actualiza en AEPISADM gserulti,gperulti
                                        ActualizarAEPISADM(-1, -1, viServiciDe, viCodMedico);
										//actualizar los registros de AMOVIMIE
										//campos origen
										object tempRefParam35 = stFechaMovimiento;
										ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam35) + " ", "ORIGEN", viServiciDe, viCodMedico);
										stFechaMovimiento = Convert.ToString(tempRefParam35);
										//campos destino
										object tempRefParam36 = stFechaMovimiento;
										ActualizarAMOVIMIE("ffinmovi >= " + Serrores.FormatFechaHMS(tempRefParam36) + " and ffinmovi is not null", "DESTINO", viServiciDe, viCodMedico);
										stFechaMovimiento = Convert.ToString(tempRefParam36);
										//se actualiza DMOVFACT
										string tempRefParam37 = DateTimeHelper.ToString(DateTime.Now);
										ActualizarDMOVFACT(ref stFechaMovimiento, ref tempRefParam37, viServiciDe, viCodMedico);

										if (iSerQueCambia != viServicioDeS)
										{ // si ha cambiado el servicio

											string tempRefParam38 = SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos;
											string tempRefParam39 = "00:00:00";
											bCambioServicio.ActualizarServicioConsumos(iAñoRegistro, iNumRegistro, iSerQueCambia, viServicioDeS.ToString(), ref tempRefParam38, true, ref tempRefParam39);

										}

										bCambioServicio.RcAdmision.CommitTransScope();
                                    }
									else
									{
										//si no es la ultima ni la primera fila
										bCambioServicio.RcAdmision.BeginTransScope();
                                        //campos origen
                                        object tempRefParam40 = stFechaMovimiento;
										object tempRefParam41 = stFechaSiguiente;
										ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam40) + " and fmovimie < " + Serrores.FormatFechaHMS(tempRefParam41) + " ", "ORIGEN", viServiciDe, viCodMedico);
										stFechaSiguiente = Convert.ToString(tempRefParam41);
										stFechaMovimiento = Convert.ToString(tempRefParam40);
										//campos destino
										object tempRefParam42 = stFechaMovimiento;
										object tempRefParam43 = stFechaSiguiente;
										ActualizarAMOVIMIE("ffinmovi >= " + Serrores.FormatFechaHMS(tempRefParam42) + " and ffinmovi < " + Serrores.FormatFechaHMS(tempRefParam43) + " ", "DESTINO", viServiciDe, viCodMedico);
										stFechaSiguiente = Convert.ToString(tempRefParam43);
										stFechaMovimiento = Convert.ToString(tempRefParam42);
										//se actualiza DMOVFACT
										ActualizarDMOVFACT(ref stFechaMovimiento, ref stFechaSiguiente, viServiciDe, viCodMedico);

										if (iSerQueCambia != viServicioDeS)
										{ // si ha cambiado el servicio

											string tempRefParam44 = SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos;
											string tempRefParam45 = DateTime.Parse(SprCambios.Text).ToString("dd/MM/yyyy HH:mm:ss");
											bCambioServicio.ActualizarServicioConsumos(iAñoRegistro, iNumRegistro, iSerQueCambia, viServicioDeS.ToString(), ref tempRefParam44, false, ref tempRefParam45);

										}

										bCambioServicio.RcAdmision.CommitTransScope();
                                    }
								}
							}
						} 
						break;
				}

				IniciarPantalla();
				rellena_grid();

				this.Cursor = Cursors.Default;
			}
			catch (Exception Excep)
			{

				//********************************
				bCambioServicio.RcAdmision.RollbackTransScope();
                this.Cursor = Cursors.Default;
				//deberia haber un select case que diferencie casos de update o de lectura
				Serrores.GestionErrorUpdate(Information.Err().Number, bCambioServicio.RcAdmision);
			}
		}

		private bool isInitializingComponent;
		private void CbbMedicoA_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (CbbMedicoA.Text == "")
			{
				fMedico = false;
				if (!fServicio)
				{
					CbAceptar.Enabled = false;
				}
			}
		}

		private void CbbMedicoA_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{

			try
			{
				//**************************
				if (CbbMedicoA.SelectedIndex != -1)
				{
					CbAceptar.Enabled = true;
					viCodMedicoDes = Convert.ToInt32(CbbMedicoA.SelectedValue);
					fMedico = true;
				}
			}
			catch
			{
				//*************************
			}
		}


		private void CbbMedicoA_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void cbBorrar_Click(Object eventSender, EventArgs eventArgs)
		{
			//solo esta activo si se selecciona el ultimo movimiento y no es el primero
			//Hay que buscar en dmovfact para comprobar si hay movimientos facturados
			string stFechaMovimiento = String.Empty; //fecha del movimiento seleccionado
			string stSql = String.Empty;
			DataSet RrSQL = null;

			try
			{
				this.Cursor = Cursors.WaitCursor;

				if (lRegistroSeleccionado > 0)
				{
					if (lRegistroSeleccionado == SprCambios.MaxRows && lRegistroSeleccionado > 1)
					{ //si estamos en el ultimo registro
						SprCambios.Row = lRegistroSeleccionado;
						SprCambios.Col = 4;
						stFechaMovimiento = SprCambios.Text;
						//buscar si hay movimientos en dmovfact
						object tempRefParam = stFechaMovimiento;
						stSql = "select * from DMOVFACT where itiposer = 'H' and " + " ganoregi=" + iAñoRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " and " + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam) + " and ffactura is not null";
						stFechaMovimiento = Convert.ToString(tempRefParam);

						if (Serrores.proExisteTabla("DMOVFACH", bCambioServicio.RcAdmision))
						{
							string tempRefParam2 = "DMOVFACT";
							stSql = stSql + " UNION ALL " + Serrores.proReplace(ref stSql, tempRefParam2, "DMOVFACH");
						}

						SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, bCambioServicio.RcAdmision);
						RrSQL = new DataSet();
						tempAdapter.Fill(RrSQL);
						if (RrSQL.Tables[0].Rows.Count != 0)
						{
							//si hay filas en DMOVFACT no se le permite eliminar los registros
							this.Cursor = Cursors.Default;
							RrSQL.Close();
							short tempRefParam3 = 1940;
							string[] tempRefParam4 = new string[]{"eliminar", "ya ha sido facturado"};
							Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, bCambioServicio.RcAdmision, tempRefParam4));
						}
						else
						{
							this.Cursor = Cursors.Default;
							short tempRefParam5 = 1490;
							string[] tempRefParam6 = new string[]{"los datos seleccionados"};
							Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, bCambioServicio.RcAdmision, tempRefParam6));
							if (Serrores.iresume == 7)
							{
								//si no se quiere eliminar
								RrSQL.Close();
							}
							else
							{
								this.Cursor = Cursors.WaitCursor;
								//no hay filas en dmovfact se puede borrar
								//Buscamos en amovimie si hay movimientos posteriores al seleccionado
								object tempRefParam7 = stFechaMovimiento;
								if (BusquedaAMOVIMIE("fmovimie > " + Serrores.FormatFechaHMS(tempRefParam7) + " "))
								{
									stFechaMovimiento = Convert.ToString(tempRefParam7);
									//si hay filas posteriores por traslados de camas
									//guardar los datos del destino de la fila a borrar
									object tempRefParam8 = stFechaMovimiento;
									GuardarDatos("DESTINO", "fmovimie = " + Serrores.FormatFechaHMS(tempRefParam8) + "");
									stFechaMovimiento = Convert.ToString(tempRefParam8);
									//guardamos los datos del origen de la fila anterior a la borrada
									object tempRefParam9 = stFechaMovimiento;
									GuardarDatos("ORIGEN", "ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam9) + "");
									stFechaMovimiento = Convert.ToString(tempRefParam9);
								    bCambioServicio.RcAdmision.BeginTransScope();
                                    //se borra la fila seleccionada
                                    object tempRefParam10 = stFechaMovimiento;
									stSql = "SELECT * FROM AMOVIMIE where itiposer = 'H' and " + " ganoregi=" + iAñoRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " and " + " fmovimie = " + Serrores.FormatFechaHMS(tempRefParam10) + "";
									stFechaMovimiento = Convert.ToString(tempRefParam10);

									SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, bCambioServicio.RcAdmision);
									RrSQL = new DataSet();
									tempAdapter_2.Fill(RrSQL);
									if (RrSQL.Tables[0].Rows.Count == 1)
									{
										RrSQL.Delete(tempAdapter_2);
									}
									RrSQL.Close();
									//se actualiza el destino en la fila anterior a la borrada
									object tempRefParam11 = stFechaMovimiento;
									ActualizarAMOVIMIE("ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam11) + " ", "BDESTINO");
									stFechaMovimiento = Convert.ToString(tempRefParam11);
									//actualizamos el origen y el destino de las filas posteriores a la borrada
									object tempRefParam12 = stFechaMovimiento;
									ActualizarAMOVIMIE("fmovimie > " + Serrores.FormatFechaHMS(tempRefParam12) + " ", "BORIGENDES", iServiciOr, iMedicoOr);
									stFechaMovimiento = Convert.ToString(tempRefParam12);
									//se actualiza gperulti y gserulti en aepisadm
									ActualizarAEPISADM(-1, -1, iServiciOr, iMedicoOr);


									// habra que  actualizar las filas de FCONSPAC con la última servicio que quede.

									SqlCommand tempCommand = new SqlCommand(" Update FCONSPAC set gserreal='" + iServiciOr.ToString() + "'" + 
									                         " where itiposer='H' and ganoregi=" + iAñoRegistro.ToString() + 
									                         " and gnumregi=" + iNumRegistro.ToString() + " and foper>=" + 
									                         Serrores.FormatFechaHM(stFechaMovimiento) + "", bCambioServicio.RcAdmision);
									tempCommand.ExecuteNonQuery();

									//OSCAR C
									//ACTUALIZAMOS DMOVFACT
									ActualizarDMOVFACT_borrados(ref stFechaMovimiento, iServiciOr, iMedicoOr);

                                    bCambioServicio.RcAdmision.CommitTransScope();
                                }
								else
								{
									stFechaMovimiento = Convert.ToString(tempRefParam7);
									//no hay filas posteriores no hay traslados
									//guardo medico y servicio de la fila anterior a la borrada para actualizar AEPISADM
									object tempRefParam13 = stFechaMovimiento;
									GuardarDatos("ORIGEN", "ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam13) + "");
									stFechaMovimiento = Convert.ToString(tempRefParam13);
									bCambioServicio.RcAdmision.BeginTransScope();
                                    //borrar la fila seleccionada
                                    object tempRefParam14 = stFechaMovimiento;
									stSql = "SELECT * FROM AMOVIMIE where itiposer = 'H' and " + " ganoregi=" + iAñoRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " and " + " fmovimie = " + Serrores.FormatFechaHMS(tempRefParam14);
									stFechaMovimiento = Convert.ToString(tempRefParam14);

									SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, bCambioServicio.RcAdmision);
									RrSQL = new DataSet();
									tempAdapter_3.Fill(RrSQL);
									if (RrSQL.Tables[0].Rows.Count == 1)
									{
										RrSQL.Delete(tempAdapter_3);
									}
									RrSQL.Close();
									//se actualizan los datos del ultimo movimiento a nulos
									//en el que ffinmovi es igual que fmovimie del que he borrado
									object tempRefParam15 = stFechaMovimiento;
									ActualizarAMOVIMIE("ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam15) + "", "BNULOS");
									stFechaMovimiento = Convert.ToString(tempRefParam15);
									//se actualiza gperulti y gserulti en aepisadm
									ActualizarAEPISADM(-1, -1, iServiciOr, iMedicoOr);

									// habra que  actualizar las filas de FCONSPAC con la última servicio que quede.

									SqlCommand tempCommand_2 = new SqlCommand(" Update FCONSPAC set gserreal='" + iServiciOr.ToString() + "'" + 
									                           " where itiposer='H' and ganoregi=" + iAñoRegistro.ToString() + 
									                           " and gnumregi=" + iNumRegistro.ToString() + " and foper>=" + 
									                           Serrores.FormatFechaHM(stFechaMovimiento) + "", bCambioServicio.RcAdmision);
									tempCommand_2.ExecuteNonQuery();

									//OSCAR C
									//ACTUALIZAMOS DMOVFACT
									ActualizarDMOVFACT_borrados(ref stFechaMovimiento, iServiciOr, iMedicoOr);

									bCambioServicio.RcAdmision.CommitTransScope();
                                }
								//se refresca el grid
							}
							IniciarPantalla();
							rellena_grid();
						}
					}
					else
					{
						//si es una fila diferente a la ultima
					}
				}
				else
				{
					//si no hay ninguno seleccionado
				}

				this.Cursor = Cursors.Default;
			}
			catch (Exception Excep)
			{

				bCambioServicio.RcAdmision.RollbackTransScope();
                this.Cursor = Cursors.Default;
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "CambioSerMedico:cbBorrar", Excep);
			}

		}

		private void CbbServicioA_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (CbbServicioA.Text == "")
			{
				fServicio = false;
				proMedicoA(viServiciDe);
				fMedico = false;
				CbAceptar.Enabled = false;
			}
		}

		private void CbbServicioA_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{
			string tstCom = String.Empty;
			DataSet tRrCom = null;
			try
			{
				//*********************
				if (CbbServicioA.SelectedIndex != -1)
				{
					fServicio = true;
					proMedicoA(Convert.ToInt32(CbbServicioA.SelectedValue));
					viServicioDeS = Convert.ToInt32(CbbServicioA.SelectedValue); ;
					if (!ValidacionEdadSexo(Convert.ToInt32(CbbServicioA.SelectedValue)))
					{
		                RadMessageBox.Show("El Servicio no es adecuado para el paciente", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);

                    }
					if (fServicio && !fMedico)
					{
						tstCom = "SELECT * " + " FROM DPERSONAva,DPERSERV " + " WHERE GSERVICI=" + CbbServicioA.Items[CbbServicioA.SelectedIndex].Value.ToString() + " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA " + " and dpersonava.gpersona =" + viCodMedico.ToString();
						SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCom, bCambioServicio.RcAdmision);
						tRrCom = new DataSet();
						tempAdapter.Fill(tRrCom);
						if (tRrCom.Tables[0].Rows.Count == 0)
						{ //DEBE CAMBIAR EL MÉDICO POR UNO QUE CORRESPONDA AL SERVICIO
							CbbMedicoA.Focus();
							CbAceptar.Enabled = false;
						}
						else
						{
							CbAceptar.Enabled = true;
						}
					}
				}
				else
				{
					fServicio = false;
					CbAceptar.Enabled = false;
				}
			}
			catch
			{
				//*********************
			}
		}


		private void CbbServicioA_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void cbCambio_Click(Object eventSender, EventArgs eventArgs)
		{

			try
			{

				//modifica el registro seleccionado
				if (lRegistroSeleccionado > 0)
				{
					vstAccion = "CAMBIO";
					//hay que activar
					Frame1.Enabled = true; //servicio
					Frame3.Enabled = false; //fecha/hora del cambio
					Label21.Enabled = false;
					SdcFecha.Enabled = false;
					tbHoraCambio.Enabled = false;
					Label19.Enabled = false;
					Frame4.Enabled = true; //medico
				}
				else
				{
					//MsgBox "debe seleccionar un registro para modificar"
					short tempRefParam = 1270;
					string[] tempRefParam2 = new string[]{"un registro"};
					Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, bCambioServicio.RcAdmision, tempRefParam2));
					Frame1.Enabled = false; //servicio
					Frame3.Enabled = false; //fecha/hora del cambio
					Label21.Enabled = false;
					SdcFecha.Enabled = false;
					tbHoraCambio.Enabled = false;
					Label19.Enabled = false;
					Frame4.Enabled = false; //medico
				}
			}
			catch (Exception Excep)
            {

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "CambioSerMedico:cbCambio", Excep);
			}

		}

		private void CbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}


		private void CbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
			string LitPersona = String.Empty;
			string LitPersona1 = String.Empty;

			try
			{
				//    Set CrystalReport1 = LISTADO_CRYSTAL
				this.Cursor = Cursors.WaitCursor;
				pro_query();

				//*********cabeceras
				bCambioServicio.proCabCrystal();
                //*******************
                //INIC SDSALAZAR SPRINT X_4
                //            //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //            bCambioServicio.CrystalReport1.Formulas["FORM1"] = "\"" + bCambioServicio.VstrCrystal.VstGrupoHo + "\"";
                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.Formulas["FORM2"] = "\"" + bCambioServicio.VstrCrystal.VstNombreHo + "\"";
                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.Formulas["FORM3"] = "\"" + bCambioServicio.VstrCrystal.VstDireccHo + "\"";

                //LitPersona = Serrores.ObtenerLiteralPersona(bCambioServicio.RcAdmision);
                //LitPersona = LitPersona.ToUpper();
                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.Formulas["Litpers"] = "\"" + LitPersona + "\"";

                //LitPersona1 = "CAMBIOS DE SERVICIO Y MÉDICO DE " + LitPersona.ToUpper() + "S";
                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.Formulas["LitpersTitulo"] = "\"" + LitPersona1 + "\"";

                ////UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.ReportFileName = "" + bCambioServicio.PathString + "\\rpt\\agi415r1.rpt";
                ////UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.WindowTitle = "Listado de Movimientos";
                ////UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.WindowState = 2; //crptmaximized
                ////UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.Destination = 0; //crptToWindow

                ////UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.Connect = "DSN=" + bCambioServicio.VstCrystal + ";UID=" + bCambioServicio.gUsuario + ";PWD=" + bCambioServicio.gContraseña + "";
                ////UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.Action = 1;
                //this.Cursor = Cursors.Default;

                ////vacia_formulas CrystalReport1, 5
                ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.Reset();
                //FIN SDSALAZAR SPRINT X_4
                stSeleccion = "";
			}
			catch (System.Exception excep)
			{
		        RadMessageBox.Show("Error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
                this.Cursor = Cursors.Default;
			}
		}

		private void cbNuevo_Click(Object eventSender, EventArgs eventArgs)
		{
			//se añade un nuevo registro al spread
			try
			{

				vstAccion = "NUEVO";
                //si hay alguna fila seleccionada se tendra que deseleccionar
                //SprCambios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly;
               
                SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                fFilaSeleccionada = false;
				lRegistroSeleccionado = 0;
				//se tendran que activar los campos de entrada de datos
				Frame1.Enabled = true;
				Frame3.Enabled = true;
				Label21.Enabled = true;
				SdcFecha.Enabled = true;
				tbHoraCambio.Enabled = true;
				Label19.Enabled = true;
				Frame4.Enabled = true;

				SdcFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
				tbHoraCambio.Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
				vstSegundos = Conversion.Str(DateTime.Now.ToString("ss"));

				//cargo en medico y servicio gserulti y gperulti de AEPISADM
				proMedicoDe(); //Pongo el médico
				proServicio(); //Lleno el  Servicio
			}
			catch (Exception Excep)
            {

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "CambioSerMedico:cbNuevo", Excep);
			}

		}

		private void Cambio_se_me_en_Activated(Object eventSender, EventArgs eventArgs)
		{
        
            if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				Frame1.Refresh();
				Frame2.Refresh();
				Frame3.Refresh();
				Frame4.Refresh();
				Frame5.Refresh();

				rellena_grid();
				if (SprCambios.MaxRows > 0)
				{
					IniciarPantalla();
				}
				else
				{
					this.Close();
				}

				//Frame1.Enabled = False
				//Frame3.Enabled = False
				//Frame4.Enabled = False
				//cbBorrar.Enabled = False
				//cbCambio.Enabled = False
				//cbNuevo.Enabled = True

			}
		}
		public void pro_query2()
		{

			//    StSqlPanta = " SELECT amovimie.fmovimie as fechaini,dservici.dnomserv as serviori,a.dnomserv as servides, " _
			//'        & " dpersona.dap1pers as ape1ori,dpersona.dap2pers as ape2ori,dpersona.dnomperS as nombori," _
			//'        & " B.dap1pers as ape1DES,B.dap2pers as ape2DES,B.dnomperS as nombDES," _
			//'        & " amovimie.ffinmovi as fechacam,amovimie.ganoregi,amovimie.gnumregi " _
			//'        & " FROM AMOVIMIE, AEPISADM, DSERVICI, DPERSONA, " _
			//'        & " DSERVICI AS A, DPERSONA AS B " _
			//'        & " WHERE AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " _
			//'        & " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " _
			//'        & " AEPISADM.faltplan IS NULL AND " _
			//'        & " (AMOVIMIE.gservior <> AMOVIMIE.gservide or " _
			//'        & " AMOVIMIE.gmedicor <> AMOVIMIE.gmedicde) " _
			//'        & " AND " _
			//'        & " AMOVIMIE.gservior = DSERVICI.gservici AND " _
			//'        & " AMOVIMIE.gservide = A.gservici AND " _
			//'        & " AMOVIMIE.gmedicor = DPERSONA.gpersona AND " _
			//'        & " AMOVIMIE.gmedicde = B.gpersona AND " _
			//'        & " AEPISADM.GIDENPAC = '" & stCodpac & "' and AEPISADM.faltplan is null"

			//Exit Sub
			//Etiqueta:
			//     AnalizaError "admision", App.Path, VstCodUsua, "Cambio_se_me_en:pro_query2", RcAdmision
		}

		public void pro_query()
		{
			string stSql = String.Empty;

			try
			{

				stSeleccion = "Movimientos del paciente";
				stSql = " SELECT * " + " FROM AMOVIMIE, AEPISADM, DPACIENT, DSERVICI, DPERSONA, " + " DSERVICI AS A, DPERSONA AS B " + " WHERE AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " + " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " + " AEPISADM.faltplan IS NULL AND " + " AEPISADM.gidenpac = DPACIENT.gidenpac AND " + " (AMOVIMIE.gservior <> AMOVIMIE.gservide or " + " AMOVIMIE.gmedicor <> AMOVIMIE.gmedicde) " + " AND " + " AMOVIMIE.gservior = DSERVICI.gservici AND " + " AMOVIMIE.gservide = A.gservici AND " + " AMOVIMIE.gmedicor = DPERSONA.gpersona AND " + " AMOVIMIE.gmedicde = B.gpersona AND " + " AEPISADM.GIDENPAC = '" + stCodpac + "' and AEPISADM.faltplan is null";

                //INIC SDSALAZAR SPRINT X_4
                ////& " ORDER BY DAPE1PAC,DAPE2PAC,DNOMBPAC,ffinmovi ASC "
                ////UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.SortFields[0] = "+{DPACIENT.DAPE1PAC}";
                ////UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.SortFields[1] = "+{DPACIENT.DAPE2PAC}";
                ////UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.SortFields[2] = "+{DPACIENT.DNOMBPAC}";
                ////UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.SortFields[3] = "+{AMOVIMIE.FFINMOVI}";

                ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.Formulas["SELECCION"] = "\"" + stSeleccion + "\"";
                ////UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //bCambioServicio.CrystalReport1.SQLQuery = stSql;
                //FIN SDSALAZAR SPRINT X_4
            }
            catch (Exception Excep)
            {
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_se_me_en:pro_query", Excep);
			}
		}


		private void Cambio_se_me_en_Load(Object eventSender, EventArgs eventArgs)
		{
            Application.DoEvents();
			int n = Application.OpenForms.Count;
			//Me.Top = 10
			//Me.Left = 0
			//Me.Height = 4215
			//Me.Width = 6540
			tbHoraCambio.Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
			vstSegundos = Conversion.Str(DateTime.Now.ToString("ss"));
			CbAceptar.Enabled = false;
			SdcFecha.MaxDate = DateTime.Now;

			string LitPersona = Serrores.ObtenerLiteralPersona(bCambioServicio.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			Label28.Text = LitPersona;

		}

		public void proServicio()
		{
			string stSqCambio = String.Empty;
			DataSet rRcambio = null;
			string stSqServicio = String.Empty;
			DataSet RrServicio = null;
			string LitPersona = String.Empty;

			try
			{

				stSqCambio = "SELECT DNOMSERV,gserulti ,DSERVICI.GSERVICI" + " FROM AEPISADM, DSERVICI " + " WHERE " + " AEPISADM.GIDENPAC = '" + stCodpac + "' AND " + " DSERVICI.GSERVICI = AEPISADM.GSERULTI and aepisadm.faltplan is null";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqCambio, bCambioServicio.RcAdmision);
				rRcambio = new DataSet();
				tempAdapter.Fill(rRcambio);

				//Compruebo que hay un servicio
				if (rRcambio.Tables[0].Rows.Count == 1)
				{
					TbServicioDe.Text = Convert.ToString(rRcambio.Tables[0].Rows[0]["dnomserv"]);
					viServiciDe = Convert.ToInt32(rRcambio.Tables[0].Rows[0]["gserulti"]);
				}


				//oscar 12/01/2004
				//No se podran efectuar traslados de pacientes ingresados en plaza de dia
				//(episodios menores a 1900) a un servicio que no pueda ser de hospital de dia.
				//ademas, se debera restringir mas el servicio destino a un servicio de hospital de dia
				//que tenga el mismo indicador IMEDQUIR del servicio origen, para que un paciente ingresado
				//en servicios de HOSPITAL DE DIA QUIRURGICO (IMEDQUIR='Q' y ICENTDIA='S') solo puedan trasladarse
				// a servicios de HOSPITAL DE DIA QUIRURGICO.

				//Lleno el combo de servicio
				//stSqServicio = "SELECT GSERVICI, DNOMSERV " _
				//& " FROM DSERVICIva " _
				//& " WHERE IHOSPITA='S' and " _
				//& " gservici<>" & rRcambio("gservici") _
				//& " order by dnomserv"

				stSqServicio = "SELECT GSERVICI, DNOMSERV " + " FROM DSERVICIva " + " WHERE IHOSPITA='S' and " + " gservici<>" + Convert.ToString(rRcambio.Tables[0].Rows[0]["gservici"]);
				if (iAñoRegistro < 1900)
				{
					stSqServicio = stSqServicio + 
					               " AND ICENTDIA='S'" + 
					               " AND IMEDQUIR = (SELECT IMEDQUIR FROM DSERVICIVA WHERE GSERVICI=" + Convert.ToString(rRcambio.Tables[0].Rows[0]["gservici"]) + ")";
				}

				stSqServicio = stSqServicio + " order by dnomserv";
				//------------------------------------------------------------------------------------

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqServicio, bCambioServicio.RcAdmision);
				RrServicio = new DataSet();
				tempAdapter_2.Fill(RrServicio);
				CbbServicioA.Items.Clear();
				foreach (DataRow iteration_row in RrServicio.Tables[0].Rows)
				{
                    CbbServicioA.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["DNOMSERV"]), Convert.ToInt32(iteration_row["GSERVICI"])));
                }

                RrServicio.Close();
				if (rRcambio.Tables[0].Rows.Count == 1)
				{
					proMedicoA(Convert.ToInt32(rRcambio.Tables[0].Rows[0]["gservici"]));
				}
				else
				{
					rRcambio.Close();
				}

				LitPersona = Serrores.ObtenerLiteralPersona(bCambioServicio.RcAdmision);
				LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1) + ":";
				Label28.Text = LitPersona;
			}
			catch (Exception Excep)
            {

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_se_me_en:proServicio", Excep);
			}

		}

		public void proMedicoA(int iServicio)
		{
			string stSqServicio = String.Empty;
			DataSet RrServicio = null;

			//*********************
			try
			{
				//*****************
				CbbMedicoA.Items.Clear();
				stSqServicio = "SELECT DNOMPERS,DAP1PERS, DAP2PERS,DPERSONAva.GPERSONA " + " FROM DPERSONAva,DPERSERV " + " WHERE GSERVICI=" + iServicio.ToString() + " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA ";

				if (CbbServicioA.SelectedIndex == -1)
				{
					stSqServicio = stSqServicio + " and dpersonava.gpersona <>" + viCodMedico.ToString();
				}

				//oscar 25/11/2003
				stSqServicio = stSqServicio + " AND DPERSONAVA.IMEDICO='S' ";
				//------

				stSqServicio = stSqServicio + " order by dap1pers,dap2pers,dnompers";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqServicio, bCambioServicio.RcAdmision);
				RrServicio = new DataSet();
				tempAdapter.Fill(RrServicio);
				foreach (DataRow iteration_row in RrServicio.Tables[0].Rows)
				{
                    CbbMedicoA.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["DAP1PERS"]).Trim() + " " + Convert.ToString(iteration_row["DAP2PERS"]).Trim() + " " + Convert.ToString(iteration_row["DNOMPERS"]).Trim(), Convert.ToString(iteration_row["GPERSONA"]).Trim()));
   				}

                RrServicio.Close();
			}
			catch (Exception Excep)
            {
				//******************
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_se_me_en:proMedicoA", Excep);
			}
		}

		//Private Sub Form_Unload(Cancel As Integer)
		//menu_ingreso.proQuitarMatriz stCodpac, Me
		//End Sub

		public void proMedicoDe()
		{
			string stSqCambio = String.Empty;
			DataSet rRcambio = null;

			//***********
			try
			{
				//*******************
				stSqCambio = "SELECT DNOMPERS,DAP1PERS, DAP2PERS,dpersona.gpersona " + " FROM AEPISADM, DPERSONA, DPERSERV " + " WHERE " + " AEPISADM.GIDENPAC = '" + stCodpac + "' AND " + " DPERSONA.GPERSONA = AEPISADM.GPERULTI AND " + " DPERSONA.GPERSONA = DPERSERV.GPERSONA AND " + " DPERSERV.GSERVICI=AEPISADM.GSERULTI and aepisadm.faltplan is null ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqCambio, bCambioServicio.RcAdmision);
				rRcambio = new DataSet();
				tempAdapter.Fill(rRcambio);

				//Compruebo que hay un Medico
				if (rRcambio.Tables[0].Rows.Count == 1)
				{
					if (Convert.ToString(rRcambio.Tables[0].Rows[0]["DAP1PERS"]) != "")
					{
						TbMedicoDe.Text = Convert.ToString(rRcambio.Tables[0].Rows[0]["DAP1PERS"]).Trim();
					}
					if (Convert.ToString(rRcambio.Tables[0].Rows[0]["DAP2PERS"]) != "")
					{
						TbMedicoDe.Text = TbMedicoDe.Text + " " + Convert.ToString(rRcambio.Tables[0].Rows[0]["DAP2PERS"]).Trim();
					}
					if (Convert.ToString(rRcambio.Tables[0].Rows[0]["DNOMPERS"]) != "")
					{
						TbMedicoDe.Text = TbMedicoDe.Text + " " + Convert.ToString(rRcambio.Tables[0].Rows[0]["DNOMPERS"]).Trim();
					}
					viCodMedico = Convert.ToInt32(rRcambio.Tables[0].Rows[0]["gpersona"]);
				}
				rRcambio.Close();
			}
			catch (Exception Excep)
            {
				//***************
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_se_me_en:proMedicoDe", Excep);
			}
		}

            private void SprCambios_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {

            int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			//cuando se selecciona un registro tendriamos que activar un boton u otro
			//dependiendo del registro seleccionado
			SprCambios.Row = Row;
			//se se selecciona el registro de cabecera o no se selecciona ninguno
			if (SprCambios.Row <= 0)
			{
				return;
			}

			if (lRegistroSeleccionado == Row)
			{
				//si el seleccionado coincide con el seleccionado anterior
				if (fFilaSeleccionada)
				{
                    //SprCambios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly; //Se desmarca la fila (Por la Banda)
                    //SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
                    SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
                    fFilaSeleccionada = false;
				}
				else
				{
					//SprCambios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; //Se marca la fila
                    //SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;
                    fFilaSeleccionada = true;
				}
			}
			else
			{
				//SprCambios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; //Se marca la fila
                //SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode;
                fFilaSeleccionada = true;
			}

			if (fFilaSeleccionada)
			{
				lRegistroSeleccionado = Row;
				//habria que cargar los datos del registro seleccionado en la pantalla
				//se pone la fecha del registro seleccionado
				SprCambios.Row = lRegistroSeleccionado;
				SprCambios.Col = 4;
				SdcFecha.Text = DateTime.Parse(SprCambios.Text).ToString("dd/MM/yyyy");
				tbHoraCambio.Text = DateTime.Parse(SprCambios.Text).ToString("HH:mm");
				vstSegundos = DateTime.Parse(SprCambios.Text).ToString("ss");
				//se cargan los datos del seleccionado

				SprCambios.Col = 5; //servicio  de origen
				viServicioDeS = Convert.ToInt32(Double.Parse(SprCambios.Text));
				viServiciDe = Convert.ToInt32(Double.Parse(SprCambios.Text));
				BuscarServicio(viServiciDe);

				SprCambios.Col = 6; //medico de origen
				viCodMedico = Convert.ToInt32(Double.Parse(SprCambios.Text));
				viCodMedicoDes = Convert.ToInt32(Double.Parse(SprCambios.Text));
				BuscarMedico(viCodMedico);

				//se protegen los campos de la pantalla
				Frame3.Enabled = false;
				Label21.Enabled = false;
				SdcFecha.Enabled = false;
				tbHoraCambio.Enabled = false;
				Label19.Enabled = false;
				Frame1.Enabled = false;
				Frame4.Enabled = false;
				CbAceptar.Enabled = false; //(maplaza)(31/05/2006)

				if (lRegistroSeleccionado == SprCambios.MaxRows && lRegistroSeleccionado > 1)
				{
					//si se trata del ultimo registro se puede eliminar siempre que haya mas de uno
					cbBorrar.Enabled = true;
				}
				else
				{
					cbBorrar.Enabled = false;
				}
				cbCambio.Enabled = true;
				cbNuevo.Enabled = false;
			}
			else
			{
				//se trata de la cabecera
				lRegistroSeleccionado = 0;
				//hay que limpiar los campos de la pantalla
				IniciarPantalla();
				cbBorrar.Enabled = false;
				cbCambio.Enabled = false;
				cbNuevo.Enabled = true;
			}

		}

		private void tbHoraCambio_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			sthorasystem = Convert.ToInt32(Double.Parse(DateTime.Now.ToString("HH")));
			stminutossystem = Convert.ToInt32(Double.Parse(DateTime.Now.ToString("mm")));

		}

		private void tbHoraCambio_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbHoraCambio.SelectionStart = 0;
			tbHoraCambio.SelectionLength = Strings.Len(tbHoraCambio.Text);
		}

		private void tbHoraCambio_Leave(Object eventSender, EventArgs eventArgs)
		{
			int tiI = 0;
			int sthora = 0;
			int stminutos = 0;
			string stfecha = String.Empty;
			//*****************
			try
			{
				//******************
				tiI = (tbHoraCambio.Text.IndexOf(':') + 1);

                if (tbHoraCambio.Text.Length == 0)
                {
                    tbHoraCambio.Text = "  :  ";
                }

                if (tbHoraCambio.Text != "  :  ")
				{
                   
					if (Convert.ToInt32(Double.Parse(tbHoraCambio.Text.Substring(0, Math.Min(tiI - 1, tbHoraCambio.Text.Length)))) > 24 || Convert.ToInt32(Double.Parse(tbHoraCambio.Text.Substring(tiI))) > 60)
					{
						//MsgBox "Hora Incorrecta", vbOKOnly + vbInformation, "Admisión"
						short tempRefParam = 1150;
						string[] tempRefParam2 = new string[]{"Hora"};
						Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, bCambioServicio.RcAdmision, tempRefParam2));
						tbHoraCambio.Focus();
						return;
					}
					sthora = Convert.ToInt32(Double.Parse(DateTime.Parse(tbHoraCambio.Text).ToString("HH")));
					stminutos = Convert.ToInt32(Double.Parse(DateTime.Parse(tbHoraCambio.Text).ToString("mm")));
					stfecha = DateTime.Now.ToString("dd/MM/yyyy");
					if (stfecha == SdcFecha.Text)
					{
						if (sthora == sthorasystem)
						{
							if (stminutos > stminutossystem)
							{
								//ERROR MINUTOS MAYOR QUE LOS DEL SYSTEMA
								short tempRefParam3 = 1020;
								string[] tempRefParam4 = new string[]{"La hora no", "mayor", "la del sistema"};
								Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, bCambioServicio.RcAdmision, tempRefParam4));
								tbHoraCambio.Focus();
								return;
							}
						}
						else
						{
							if (sthora > sthorasystem)
							{
								//ERROR HORA MAYOR QUE LA DEL SYSTEMA
								short tempRefParam5 = 1020;
								string[] tempRefParam6 = new string[] {"La hora no", "mayor", "la del sistema"};
								Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, bCambioServicio.RcAdmision, tempRefParam6));
								tbHoraCambio.Focus();
								return;
							}
						}
					}
					CbAceptar.Enabled = true;
					tbHoraCambio.Text = DateTime.Parse(tbHoraCambio.Text).ToString("HH") + ":" + DateTime.Parse(tbHoraCambio.Text).ToString("mm");

				}

				vstSegundos = Conversion.Str(DateTime.Now.ToString("ss"));
			}
			//*******************
			catch (InvalidCastException e)
			{
				short tempRefParam7 = 1150;
				string[] tempRefParam8 = new string[]{"Hora"};
				Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, bCambioServicio.RcAdmision, tempRefParam8));
				tbHoraCambio.Focus();
				return;
			}
			//ojo error 13 si deja en blanco
		}


		private bool BusquedaAMOVIMIE(string stCondicionBusqueda)
		{
			//funcion que busca en AMOVIMIE si hay filas ono que cumplan la condicion
			bool result = false;
			string sqlCondicion = String.Empty;
			DataSet RrCondicion = null;

			try
			{


				sqlCondicion = "select * from AMOVIMIE where itiposer = 'H' and " + " ganoregi=" + iAñoRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " " + " and " + stCondicionBusqueda;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCondicion, bCambioServicio.RcAdmision);
				RrCondicion = new DataSet();
				tempAdapter.Fill(RrCondicion);

				if (RrCondicion.Tables[0].Rows.Count != 0)
				{
					result = true;
				}
				RrCondicion.Close();

				return result;
			}
			catch (Exception Excep)
            {

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_se_me_en:BusquedaAMOVIMIE", Excep);
				return result;
			}
		}

		private void GuardarDatos(string stDATOS, string stCondicionBusqueda)
		{
			string sqlCondicion = String.Empty;
			DataSet RrCondicion = null;

			try
			{

				sqlCondicion = "select * from AMOVIMIE where itiposer = 'H' and " + " ganoregi=" + iAñoRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " " + " and " + stCondicionBusqueda;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCondicion, bCambioServicio.RcAdmision);
				RrCondicion = new DataSet();
				tempAdapter.Fill(RrCondicion);

				if (RrCondicion.Tables[0].Rows.Count != 0)
				{
					if (stDATOS == "DESTINO")
					{
						if (!Convert.IsDBNull(RrCondicion.Tables[0].Rows[0]["ffinmovi"]))
						{
							stFfinmovi = Convert.ToString(RrCondicion.Tables[0].Rows[0]["ffinmovi"]);
							stUnidadDes = Convert.ToString(RrCondicion.Tables[0].Rows[0]["gunenfde"]);
							iPlantaDes = Convert.ToInt32(RrCondicion.Tables[0].Rows[0]["gplantde"]);
							iHabitaDes = Convert.ToInt32(RrCondicion.Tables[0].Rows[0]["ghabitde"]);
							stCamaDes = Convert.ToString(RrCondicion.Tables[0].Rows[0]["gcamades"]);
							stTipotra = (Convert.IsDBNull(RrCondicion.Tables[0].Rows[0]["itipotra"])) ? "" : Convert.ToString(RrCondicion.Tables[0].Rows[0]["itipotra"]).Trim();
							iMotitras = (Convert.IsDBNull(RrCondicion.Tables[0].Rows[0]["gmottras"])) ? -1 : Convert.ToInt32(RrCondicion.Tables[0].Rows[0]["gmottras"]);
							stIrotaci = (Convert.IsDBNull(RrCondicion.Tables[0].Rows[0]["irotacio"])) ? "" : Convert.ToString(RrCondicion.Tables[0].Rows[0]["irotacio"]).Trim();
						}
					}
					if (stDATOS == "ORIGEN")
					{
						iServiciOr = Convert.ToInt32(RrCondicion.Tables[0].Rows[0]["gservior"]);
						iMedicoOr = Convert.ToInt32(RrCondicion.Tables[0].Rows[0]["gmedicor"]);
					}
				}
				RrCondicion.Close();
			}
			catch (Exception Excep)
            {

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "CambioSerMed:DatosOrigen", Excep);
			}

		}

		private void ActualizarAEPISADM(int iServInicial = -1, int lMedInicial = -1, int iServUltimo = -1, int lMedUltimo = -1)
		{
			//rutina que actualiza AEPISADM con los datos que le mandemos

			string sql = "SELECT * FROM AEPISADM WHERE " + " AEPISADM.ganoadme = " + iAñoRegistro.ToString() + " AND AEPISADM.gnumadme =  " + iNumRegistro.ToString() + " AND " + " AEPISADM.faltplan is null and AEPISADM.gidenpac = '" + stCodpac + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, bCambioServicio.RcAdmision);
			DataSet RrActualizar = new DataSet();
			tempAdapter.Fill(RrActualizar);
			if (RrActualizar.Tables[0].Rows.Count != 0)
			{
				RrActualizar.Edit();
				if (iServInicial != -1 && lMedInicial != -1)
				{
					RrActualizar.Tables[0].Rows[0]["GSERVICI"] = iServInicial;
					RrActualizar.Tables[0].Rows[0]["GPERSONA"] = lMedInicial;
				}
				if (iServUltimo != -1 && lMedUltimo != -1)
				{
					RrActualizar.Tables[0].Rows[0]["GSERULTI"] = iServUltimo;
					RrActualizar.Tables[0].Rows[0]["GPERULTI"] = lMedUltimo;
				}
				string tempQuery = RrActualizar.Tables[0].TableName;
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(RrActualizar, tempQuery);
			}
			RrActualizar.Close();
		}

		private void ActualizarAMOVIMIE(string stCondicion, string stDATOS, int iServicio = 0, int lMedico = 0)
		{
			//Actualiza AMOVIMIE los campos de origen o destino

			string sql = "select * from AMOVIMIE where itiposer = 'H' and " + " ganoregi=" + iAñoRegistro.ToString() + " and gnumregi=" + iNumRegistro.ToString() + " " + " and " + stCondicion;

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, bCambioServicio.RcAdmision);
			DataSet RrActualizar = new DataSet();
			tempAdapter.Fill(RrActualizar);

			if (RrActualizar.Tables[0].Rows.Count != 0)
			{
				RrActualizar.MoveFirst();
				foreach (DataRow iteration_row in RrActualizar.Tables[0].Rows)
				{
					RrActualizar.Edit();
					switch(stDATOS)
					{
						case "ORIGEN" : 
							iteration_row["gservior"] = iServicio; 
							iteration_row["gmedicor"] = lMedico; 
							iteration_row["gusuario"] = bCambioServicio.VstCodUsua; 
							iteration_row["fsistema"] = vstfecha_hora_sys; 
							break;
						case "DESTINO" : 
							iteration_row["gservide"] = iServicio; 
							iteration_row["gmedicde"] = lMedico; 
							break;
						case "BORIGENDES" : 
							iteration_row["gservior"] = iServicio; 
							iteration_row["gmedicor"] = lMedico; 
							if (!Convert.IsDBNull(iteration_row["ffinmovi"]))
							{
								iteration_row["gservide"] = iServicio;
								iteration_row["gmedicde"] = lMedico;
							} 
							break;
						case "BDESTINO" : 
							iteration_row["ffinmovi"] = DateTime.Parse(stFfinmovi); 
							iteration_row["gunenfde"] = stUnidadDes; 
							iteration_row["gservide"] = iServiciOr; 
							iteration_row["gmedicde"] = iMedicoOr; 
							iteration_row["gplantde"] = iPlantaDes; 
							iteration_row["ghabitde"] = iHabitaDes; 
							iteration_row["gcamades"] = stCamaDes; 
							if (stTipotra.Trim() == "")
							{
								iteration_row["itipotra"] = DBNull.Value;
							}
							else
							{
								iteration_row["itipotra"] = stTipotra;
							} 
							if (iMotitras == -1)
							{
								iteration_row["gmottras"] = DBNull.Value;
							}
							else
							{
								iteration_row["gmottras"] = iMotitras;
							} 
							if (stIrotaci.Trim() == "")
							{
								iteration_row["irotacio"] = DBNull.Value;
							}
							else
							{
								iteration_row["irotacio"] = stIrotaci;
							} 
							break;
						case "BNULOS" : 
							iteration_row["ffinmovi"] = DBNull.Value; 
							iteration_row["gunenfde"] = DBNull.Value; 
							iteration_row["gservide"] = DBNull.Value; 
							iteration_row["gmedicde"] = DBNull.Value; 
							iteration_row["gplantde"] = DBNull.Value; 
							iteration_row["ghabitde"] = DBNull.Value; 
							iteration_row["gcamades"] = DBNull.Value; 
							iteration_row["itipotra"] = DBNull.Value; 
							iteration_row["gmottras"] = DBNull.Value; 
							iteration_row["irotacio"] = DBNull.Value; 
							break;
					}


					string tempQuery = RrActualizar.Tables[0].TableName;
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrActualizar, tempQuery);
				}
			}
			RrActualizar.Close();

		}

		private void BuscarServicio(int iServicio)
		{
            try
            {
                string stSqCambio = "SELECT dnomserv,gservici " + " FROM DSERVICI " + " WHERE " + " DSERVICI.gservici = " + iServicio.ToString() + " "; //String Sql

                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqCambio, bCambioServicio.RcAdmision);
                DataSet rRcambio = new DataSet();
                tempAdapter.Fill(rRcambio);

                //Compruebo que hay un servicio
                if (rRcambio.Tables[0].Rows.Count == 1)
                {
                    TbServicioDe.Text = Convert.ToString(rRcambio.Tables[0].Rows[0]["dnomserv"]);
                }


                //oscar 12/01/2004
                //No se podran efectuar traslados de pacientes ingresados en plaza de dia
                //(episodios menores a 1900) a un servicio que no pueda ser de hospital de dia.
                //ademas, se debera restringir mas el servicio destino a un servicio de hospital de dia
                //que tenga el mismo indicador IMEDQUIR del servicio origen, para que un paciente ingresado
                //en servicios de HOSPITAL DE DIA QUIRURGICO (IMEDQUIR='Q' y ICENTDIA='S') solo puedan trasladarse
                // a servicios de HOSPITAL DE DIA QUIRURGICO.

                //Lleno el combo de servicio
                //stSqServicio = "SELECT GSERVICI, DNOMSERV " _
                //& " FROM DSERVICIva " _
                //& " WHERE IHOSPITA='S' " _
                //& " and gservici<>" & rRcambio("gservici") _
                //& " order by dnomserv"

                string stSqServicio = "SELECT GSERVICI, DNOMSERV " + " FROM DSERVICIva " + " WHERE IHOSPITA='S' and " + " gservici<>" + Convert.ToString(rRcambio.Tables[0].Rows[0]["gservici"]);
                if (iAñoRegistro < 1900)
                {
                    stSqServicio = stSqServicio +
                                   " AND ICENTDIA ='S' " +
                                   " AND IMEDQUIR = (SELECT IMEDQUIR FROM DSERVICIVA WHERE GSERVICI=" + Convert.ToString(rRcambio.Tables[0].Rows[0]["gservici"]) + ")";
                }

                stSqServicio = stSqServicio + " order by dnomserv";
                //-------------

                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqServicio, bCambioServicio.RcAdmision);
                DataSet RrServicio = new DataSet();
                tempAdapter_2.Fill(RrServicio);
                CbbServicioA.Items.Clear();
                foreach (DataRow iteration_row in RrServicio.Tables[0].Rows)
                {
                    CbbServicioA.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["DNOMSERV"]), Convert.ToInt32(iteration_row["GSERVICI"])));
                }
                RrServicio.Close();

                if (rRcambio.Tables[0].Rows.Count == 1)
                {
                    proMedicoA(Convert.ToInt32(rRcambio.Tables[0].Rows[0]["gservici"]));
                }
                else
                {
                    rRcambio.Close();
                }
                //return;
            }
            catch (Exception Exep)
            {
                //****************

                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_se_me_en:proServicio", Exep);
            }
			

		}

		private void BuscarMedico(int lMedico)
		{
			string stSqServicio = String.Empty;
			DataSet RrServicio = null;

			//*********************
			try
			{
				//*****************
				stSqServicio = "SELECT DNOMPERS,DAP1PERS,DAP2PERS " + " FROM DPERSONAVA " + " WHERE " + " dpersonava.gpersona = " + lMedico.ToString();

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqServicio, bCambioServicio.RcAdmision);
				RrServicio = new DataSet();
				tempAdapter.Fill(RrServicio);

				//Compruebo que hay un medico
				if (RrServicio.Tables[0].Rows.Count == 1)
				{
					TbMedicoDe.Text = Convert.ToString(RrServicio.Tables[0].Rows[0]["DAP1PERS"]).Trim() + " " + Convert.ToString(RrServicio.Tables[0].Rows[0]["DAP2PERS"]).Trim() + "," + Convert.ToString(RrServicio.Tables[0].Rows[0]["DNOMPERS"]).Trim();
				}

				CbbMedicoA.Items.Clear();
				stSqServicio = "SELECT DNOMPERS,DAP1PERS,DAP2PERS,DPERSONAva.GPERSONA " + " FROM DPERSONAva,DPERSERV " + " WHERE GSERVICI=" + viServiciDe.ToString() + " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA " + " and dpersonava.gpersona <>" + lMedico.ToString();

				//oscar 25/11/2003
				stSqServicio = stSqServicio + " AND DPERSONAVA.IMEDICO='S' ";
				//------

				stSqServicio = stSqServicio + " order by dap1pers,dap2pers,dnompers";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqServicio, bCambioServicio.RcAdmision);
				RrServicio = new DataSet();
				tempAdapter_2.Fill(RrServicio);
				foreach (DataRow iteration_row in RrServicio.Tables[0].Rows)
				{
                    CbbMedicoA.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["DAP1PERS"]).Trim() + " " + Convert.ToString(iteration_row["DAP2PERS"]).Trim() + " " + Convert.ToString(iteration_row["DNOMPERS"]).Trim(), Convert.ToString(iteration_row["GPERSONA"]).Trim()));
				}
				RrServicio.Close();
			}
			catch (Exception Excep)
            {
				//******************
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_se_me_en:proMedicoA", Excep);
			}
		}

		private void IniciarPantalla()
		{

			//se pone la fecha actual
			SdcFecha.MaxDate = DateTime.Now;
			//el mindate tiene que ser la fecha del ingreso
			System.DateTime TempDate = DateTime.FromOADate(0);
			SdcFecha.MinDate = DateTime.Parse((DateTime.TryParse(stFechaIngreso, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : stFechaIngreso);
			//
			vstfecha = DateTimeHelper.ToString(DateTime.Today);
			//
			SdcFecha.Text = Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy"));
            tbHoraCambio.Text = Convert.ToString(DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm"));
			vstSegundos = Conversion.Str(DateTime.Now.ToString("ss"));
			//
			vsthora = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm") + ":" + (DateTime.Now.ToString("ss"));
			vstfecha_hora_sys = vstfecha + " " + vsthora;

			//se limpian los campos de la pantalla
			CbbMedicoA.Text = "";
			CbbServicioA.Text = "";
			TbServicioDe.Text = "";
			TbMedicoDe.Text = "";

			//se protegen los campos de la pantalla
			Frame3.Enabled = false;
			Label21.Enabled = false;
			SdcFecha.Enabled = false;
			tbHoraCambio.Enabled = false;
			Label19.Enabled = false;
			Frame1.Enabled = false;
			Frame4.Enabled = false;

			cbBorrar.Enabled = false;
			cbCambio.Enabled = false;
			cbNuevo.Enabled = true;
			CbAceptar.Enabled = false;

			fServicio = false;
			fMedico = false;
			vstAccion = "";

			//se desmarca el spread
			//SprCambios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly;
            SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
            fFilaSeleccionada = false;

		}
		private bool ValidacionEdadSexo(int iCodigoServicio)
		{
			//Funcion que devuelve un true si el servicio esta acorde con el paciente
			bool result = false;
			string stSexo = String.Empty;
			int viEdad = 0;
			bool fEdad = false;

			string stFechaValidacion = SdcFecha.Text + " " + tbHoraCambio.Text;

			string sql = "select itipsexo,fnacipac,* from DPACIENT where gidenpac = '" + stCodpac + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, bCambioServicio.RcAdmision);
			DataSet RrSQL = new DataSet();
			tempAdapter.Fill(RrSQL);
			if (RrSQL.Tables[0].Rows.Count != 0)
			{
				stSexo = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["itipsexo"])) ? "I" : Convert.ToString(RrSQL.Tables[0].Rows[0]["itipsexo"]);
				if (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["fnacipac"]))
				{
					fEdad = false;
				}
				else
				{
					viEdad = (int) DateAndTime.DateDiff("m", Convert.ToDateTime(RrSQL.Tables[0].Rows[0]["fnacipac"]), DateTime.Parse(stFechaValidacion), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
					fEdad = true;
				}
			}
			else
			{

			}
			//buscamos la edad y sexo del servicio
			sql = "select * from DSERVICI ";
			sql = sql + "where gservici = " + iCodigoServicio.ToString() + " ";
			sql = sql + "and (itipsexo = '" + stSexo + "' or itipsexo is null) ";
			sql = sql + "and (cedadmin <=" + viEdad.ToString() + " or cedadmin is null) ";
			sql = sql + "and (cedadmax >=" + viEdad.ToString() + " or cedadmax is null) ";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, bCambioServicio.RcAdmision);
			RrSQL = new DataSet();
			tempAdapter_2.Fill(RrSQL);
			result = RrSQL.Tables[0].Rows.Count != 0;
			RrSQL.Close();

			return result;
		}


		public void ActualizarDMOVFACT(ref string stFechaInicio, ref string stFechafin, int viServicioDestino, int viMedicoDestino)
		{


			string stSqlFact = "update dmovfact set gserreal = " + viServicioDestino.ToString() + ", gpersona = " + viMedicoDestino.ToString() + " ";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + iAñoRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + iNumRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " (gcodeven= 'ES' or gcodeven= 'AU') and ";
			object tempRefParam = stFechaInicio;
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam) + "";
			stFechaInicio = Convert.ToString(tempRefParam);
			object tempRefParam2 = stFechafin;
			stSqlFact = stSqlFact + " and ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam2) + "";
			stFechafin = Convert.ToString(tempRefParam2);

			SqlCommand tempCommand = new SqlCommand(stSqlFact, bCambioServicio.RcAdmision);
			tempCommand.ExecuteNonQuery();

			stSqlFact = "update dmovfact set gsersoli = " + viServicioDestino.ToString() + " ";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + iAñoRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + iNumRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gcodeven= 'DI' and ";
			object tempRefParam3 = stFechaInicio;
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam3) + "";
			stFechaInicio = Convert.ToString(tempRefParam3);
			object tempRefParam4 = stFechafin;
			stSqlFact = stSqlFact + " and ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam4) + "";
			stFechafin = Convert.ToString(tempRefParam4);

			SqlCommand tempCommand_2 = new SqlCommand(stSqlFact, bCambioServicio.RcAdmision);
			tempCommand_2.ExecuteNonQuery();

			stSqlFact = "update dmovfact set gsersoli = " + viServicioDestino.ToString() + ", gserresp = " + viServicioDestino.ToString() + " ";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + iAñoRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + iNumRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gcodeven= 'FA' and ";
			object tempRefParam5 = stFechaInicio;
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam5) + "";
			stFechaInicio = Convert.ToString(tempRefParam5);
			object tempRefParam6 = stFechafin;
			stSqlFact = stSqlFact + " and ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam6) + "";
			stFechafin = Convert.ToString(tempRefParam6);

			SqlCommand tempCommand_3 = new SqlCommand(stSqlFact, bCambioServicio.RcAdmision);
			tempCommand_3.ExecuteNonQuery();
		}

		//oscar 14/03/03
		private bool ActualizarDMOVFACT_nuevos(int PAnoregi, int PNumregi)
		{
			bool result = false;
			DataSet RrEditarAñadir = null;
			double Cantidad = 0;
			string stFarmaco = String.Empty;
			double nequival = 0;
			int i = 0;


			//En el caso de que existan movimientos en DMOVFACT con el campo FFECHFIN mayor que la fecha del apunte,
			//se actuara de diferente forma segun el campo GCODEVEN
			object tempRefParam = SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos;
			object tempRefParam2 = SdcFecha.Value.ToShortDateString() + " " + tbHoraCambio.Text + ":" + vstSegundos;
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT * FROM DMOVFACT WHERE" + " itiposer = 'H' and " + " ganoregi=" + PAnoregi.ToString() + " and " + " gnumregi=" + PNumregi.ToString() + " and " + " (ffechfin >" + Serrores.FormatFechaHMS(tempRefParam) + " or ffinicio >" + Serrores.FormatFechaHMS(tempRefParam2) + ") ");

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql.ToString(), bCambioServicio.RcAdmision);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in rr.Tables[0].Rows)
				{

					//'            Case "AL", "AN", "DQ", "PR", "AY", "HM", "IN"
					//'
					//'              En estos casos no se hace nada para el cambio de servicio / médico (si
					//'              se hacía en el cambio de sociedad

					switch(Convert.ToString(iteration_row["GCODEVEN"]).Trim().ToUpper())
					{
						case "AU" : case "ES" : 
							 
							//En el caso que la fecha de inicio sea menor que la fecha del apunte, se modificaran las lineas correspondentes de 
							//DMOVFACT poniendo en FFECHFIN la fecha del apunte. Por cada linea modificada en DMOVFACT, se grabara una linea nueva 
							//con los campos GSERVICI, GPERSONA correspondientes, poniendo como FFINICIO la fecha 
							//del apunte y como FFECHFIN la fecha de fin de movimiento que tuviera antes de ser modificada. 
							 
							System.DateTime TempDate = DateTime.FromOADate(0); 
							if (DateTime.Parse(Convert.ToDateTime(iteration_row["ffinicio"]).ToString("dd/MM/yyyy HH:NN:SS")) < DateTime.Parse((DateTime.TryParse(SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:NN:SS") : SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos))
							{

								sql = new StringBuilder("SELECT * FROM DMOVFACT WHERE 2=1");
								SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql.ToString(), bCambioServicio.RcAdmision);
								RrEditarAñadir = new DataSet();
								tempAdapter_2.Fill(RrEditarAñadir);
								RrEditarAñadir.AddNew();

								i = 0;

								//while(RrEditarAñadir.Tables[0].Rows[0].getCount() != i)
                                  while (RrEditarAñadir.Tables[0].Rows.Count != i)
                                    {
									//Debug.Print RrEditarAñadir.rdoColumns(i).Name
									switch(RrEditarAñadir.Tables[0].Columns[i].ColumnName.ToUpper())
									{
										case "FILA_ID" : 
											break;
										case "GSERREAL" : 
											RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = viServicioDeS; 
											break;
										case "GPERSONA" : 
											RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = viCodMedicoDes; 
											break;
										case "FFINICIO" : 
											System.DateTime TempDate2 = DateTime.FromOADate(0); 
											RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = DateTime.Parse((DateTime.TryParse(SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:NN:SS") : SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos); 
											break;
										default:
											if (!Convert.IsDBNull(iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName]))
											{
												RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName];
											}
											else
											{
												RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = DBNull.Value;
											} 
											break;
									}
									i++;
								};

								string tempQuery = RrEditarAñadir.Tables[0].TableName;
								SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
								SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
								tempAdapter_3.Update(RrEditarAñadir, RrEditarAñadir.Tables[0].TableName);

								object tempRefParam3 = SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
								sql = new StringBuilder("UPDATE DMOVFACT SET " + 
								      "FFECHFIN=" + Serrores.FormatFechaHMS(tempRefParam3) + 
								      " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
								SqlCommand tempCommand = new SqlCommand(sql.ToString(), bCambioServicio.RcAdmision);
								tempCommand.ExecuteNonQuery();

								RrEditarAñadir.Close();


							}
							else
							{

								sql = new StringBuilder("UPDATE DMOVFACT SET " + 
								      "GSERREAL = " + viServicioDeS.ToString() + "," + 
								      "GPERSONA = " + viCodMedicoDes.ToString());
								sql.Append(
								           " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
								SqlCommand tempCommand_2 = new SqlCommand(sql.ToString(), bCambioServicio.RcAdmision);
								tempCommand_2.ExecuteNonQuery();
								//''''''''''''''''

							} 
							 
							break;
						case "FA" : 
							 
							//En el caso que la fecha de inicio sea menor que la fecha del apunte, se realizara el calculo 
							//de farmacos administrados en el periodo. Una vez calculado, se actualizara el campo NCANTIDA de 
							//DMOVFACT y se generara otra linea para el periodo desde fecha de cambio hasta fecha fin con los 
							//mismos valores que el anterior, pero con los nuevos valores para GSOCIEDA, NAFILIAC y GINSPECC 
							//correspondientes a las tomas de dicho periodo. 
							 
							System.DateTime TempDate3 = DateTime.FromOADate(0); 
							if (DateTime.Parse(Convert.ToDateTime(iteration_row["ffinicio"]).ToString("dd/MM/yyyy HH:NN:SS")) < DateTime.Parse((DateTime.TryParse(SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:NN:SS") : SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos))
							{

								object tempRefParam4 = SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
								object tempRefParam5 = SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
								object tempRefParam6 = SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
								sql = new StringBuilder("SELECT GFARMACO,SUM(CDOSISTO) CANTIDAD " + 
								      "FROM ETOMASFA E INNER JOIN DMOVFACT D ON " + 
								      "E.ITIPOSER = D.ITIPOSER AND " + 
								      "E.GANOREGI = D.GANOREGI AND " + 
								      "E.GNUMREGI = D.GNUMREGI AND " + 
								      "E.FPASFACT = D.FPERFACT AND " + 
								      "E.GFARMACO = D.GCONCFAC " + 
								      "WHERE GCODEVEN='FA' AND " + 
								      "D.FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]) + " AND " + 
								      "FFINICIO<=" + Serrores.FormatFechaHMS(tempRefParam4) + " AND " + 
								      "FFECHFIN>=" + Serrores.FormatFechaHMS(tempRefParam5) + " and " + 
								      "FTOMASFA>= FFINICIO and " + 
								      "FTOMASFA<" + Serrores.FormatFechaHMS(tempRefParam6) + " " + 
								      "GROUP BY GFARMACO ");
								//************** O.Frias (SQL-2005)**************
								sql.Append("Order BY GFARMACO ");
								//************** O.Frias (SQL-2005)**************
								SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql.ToString(), bCambioServicio.RcAdmision);
								RrEditarAñadir = new DataSet();
								tempAdapter_4.Fill(RrEditarAñadir);
								if (RrEditarAñadir.Tables[0].Rows.Count != 0)
								{
									stFarmaco = Convert.ToString(RrEditarAñadir.Tables[0].Rows[0]["GFARMACO"]);
									string tempRefParam7 = Convert.ToString(RrEditarAñadir.Tables[0].Rows[0]["CANTIDAD"]);
									Cantidad = (Convert.IsDBNull(RrEditarAñadir.Tables[0].Rows[0]["CANTIDAD"])) ? 0 : NumeroConvertido(ref tempRefParam7);
									//RrEditarAñadir.Close

									sql = new StringBuilder("SELECT nequival FROM DCODFARMVA WHERE gfarmaco='" + stFarmaco + "'");
									SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql.ToString(), bCambioServicio.RcAdmision);
									RrEditarAñadir = new DataSet();
									tempAdapter_5.Fill(RrEditarAñadir);
									if (RrEditarAñadir.Tables[0].Rows.Count != 0)
									{
										string tempRefParam8 = Convert.ToString(RrEditarAñadir.Tables[0].Rows[0][0]);
										nequival = (Convert.IsDBNull(RrEditarAñadir.Tables[0].Rows[0][0])) ? 0 : NumeroConvertido(ref tempRefParam8);
									}
									//RrEditarAñadir.Close

									if (nequival == 0)
									{
                                        RadMessageBox.Show("ERROR al obtener la Unidad de equivalencia de algún(os) de los Farmaco(s) implicados.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
										//MessageBox.Show("ERROR al obtener la Unidad de equivalencia de algún(os) de los Farmaco(s) implicados.", Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
										return result;
									}

									sql = new StringBuilder("SELECT * FROM DMOVFACT WHERE 2=1");
									SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql.ToString(), bCambioServicio.RcAdmision);
									RrEditarAñadir = new DataSet();
									tempAdapter_6.Fill(RrEditarAñadir);
									RrEditarAñadir.AddNew();

									i = 0;

									//while(RrEditarAñadir.Tables[0].Rows[0].getCount() != i)
                                        while (RrEditarAñadir.Tables[0].Rows.Count != i)
                                        {
										//Debug.Print RrEditarAñadir.rdoColumns(i).Name
										switch(RrEditarAñadir.Tables[0].Columns[i].ColumnName.ToUpper())
										{
											case "FILA_ID" : 
												break;
											case "GSERSOLI" : case "GSERRESP" : 
												RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = viServicioDeS; 
												break;
											case "FFINICIO" : 
												System.DateTime TempDate4 = DateTime.FromOADate(0); 
												RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = DateTime.Parse((DateTime.TryParse(SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:NN:SS") : SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos); 
												break;
											case "NCANTIDA" : 
												string tempRefParam10 = Convert.ToString(iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName]); 
												string tempRefParam9 = (NumeroConvertido(ref tempRefParam10) - (Cantidad / nequival)).ToString(); 
												string tempRefParam11 = Serrores.ObtenerSeparadorDecimalBD(bCambioServicio.RcAdmision); 
												RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = Serrores.ConvertirDecimales(ref tempRefParam9, ref tempRefParam11, 2); 
												break;
											default:
												if (!Convert.IsDBNull(iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName]))
												{
													RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName];
												}
												else
												{
													RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = DBNull.Value;
												} 
												break;
										}
										i++;
									};
									string tempQuery_2 = RrEditarAñadir.Tables[0].TableName;
									SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(tempQuery_2, "");
									SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_7);
									tempAdapter_7.Update(RrEditarAñadir, RrEditarAñadir.Tables[0].TableName);

									string tempRefParam12 = (Cantidad / nequival).ToString();
									string tempRefParam13 = Serrores.ObtenerSeparadorDecimalBD(bCambioServicio.RcAdmision);
									object tempRefParam14 = SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
									sql = new StringBuilder("UPDATE DMOVFACT SET " + 
									      "NCANTIDA=" + Serrores.ConvertirDecimales(ref tempRefParam12, ref tempRefParam13, 2) + 
									      ",FFECHFIN=" + Serrores.FormatFechaHMS(tempRefParam14) + 
									      " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
									SqlCommand tempCommand_3 = new SqlCommand(sql.ToString(), bCambioServicio.RcAdmision);
									tempCommand_3.ExecuteNonQuery();
								}
								else
								{
									//se actualiza el servicio cuando hay tomas en el periodo entre la fecha de inicio del movimiento y la fecha de fin del movimiento
									//pero no hay tomas entre FFINICIO y Fecha de Cambio
									object tempRefParam15 = SdcFecha.Value.Date + " " + tbHoraCambio.Text + ":" + vstSegundos;
									sql = new StringBuilder("UPDATE DMOVFACT SET " + 
									      "GSERSOLI = " + viServicioDeS.ToString() + "," + 
									      "GSERRESP = " + viServicioDeS.ToString() + "," + 
									      "FFINICIO=" + Serrores.FormatFechaHMS(tempRefParam15));
									sql.Append(
									           " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
									SqlCommand tempCommand_4 = new SqlCommand(sql.ToString(), bCambioServicio.RcAdmision);
									tempCommand_4.ExecuteNonQuery();
								}
								RrEditarAñadir.Close();

							}
							else
							{

								//cuando la fecha de inicio sea mayor que la fecha de cambio,
								//se actualiza la sociedad.
								sql = new StringBuilder("UPDATE DMOVFACT SET " + 
								      "GSERSOLI = " + viServicioDeS.ToString() + "," + 
								      "GSERRESP = " + viServicioDeS.ToString());
								sql.Append(
								           " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
								SqlCommand tempCommand_5 = new SqlCommand(sql.ToString(), bCambioServicio.RcAdmision);
								tempCommand_5.ExecuteNonQuery();
								//''''''''''''''''

							} 
							 
							break;
						case "DI" : 
							// no se cambian 
							break;
					}
				}
			}
			rr.Close();

			return true;

		}

		public void ActualizarDMOVFACT_borrados(ref string stFechaInicio, int viServicioDestino, int viMedicoDestino)
		{

			string stSqlFact = "update dmovfact set gserreal = " + viServicioDestino.ToString() + ", gpersona = " + viMedicoDestino.ToString() + " ";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + iAñoRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + iNumRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " (gcodeven= 'ES' or gcodeven= 'AU') and ";
			object tempRefParam = stFechaInicio;
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam);
			stFechaInicio = Convert.ToString(tempRefParam);

			SqlCommand tempCommand = new SqlCommand(stSqlFact, bCambioServicio.RcAdmision);
			tempCommand.ExecuteNonQuery();

			stSqlFact = "update dmovfact set gsersoli = " + viServicioDestino.ToString() + " ";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + iAñoRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + iNumRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gcodeven= 'DI' and ";
			object tempRefParam2 = stFechaInicio;
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam2);
			stFechaInicio = Convert.ToString(tempRefParam2);

			SqlCommand tempCommand_2 = new SqlCommand(stSqlFact, bCambioServicio.RcAdmision);
			tempCommand_2.ExecuteNonQuery();

			stSqlFact = "update dmovfact set gsersoli = " + viServicioDestino.ToString() + ", gserresp = " + viServicioDestino.ToString() + " ";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + iAñoRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + iNumRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gcodeven= 'FA' and ";
			object tempRefParam3 = stFechaInicio;
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam3);
			stFechaInicio = Convert.ToString(tempRefParam3);

			SqlCommand tempCommand_3 = new SqlCommand(stSqlFact, bCambioServicio.RcAdmision);
			tempCommand_3.ExecuteNonQuery();
		}

		private double NumeroConvertido(ref string PNumero)
		{
			double result = 0;
			int CuantosDec = 0;
			int Constante = 0;

			if (PNumero == "")
			{
				return result;
			}

			int posdec = (PNumero.IndexOf('.') + 1);
			if (posdec == 0)
			{
				posdec = (PNumero.IndexOf(',') + 1);
			}
			if (posdec == 0)
			{
				//No hay decimales en el numero
				return Double.Parse(PNumero);
			}
			else
			{
				//Tratamiento de numero en dotacion decimal
				//Ej: 3,33 = (333 / 100)
				//     3.3 = (33  / 10)
				CuantosDec = PNumero.Substring(posdec).Length;
				Constante = Convert.ToInt32(Double.Parse("1" + new string('0', CuantosDec)));
				return Double.Parse(PNumero.Substring(0, Math.Min(posdec - 1, PNumero.Length)) + PNumero.Substring(posdec)) / Constante;
			}
			return result;
		}
		private void Cambio_se_me_en_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}
	}
}