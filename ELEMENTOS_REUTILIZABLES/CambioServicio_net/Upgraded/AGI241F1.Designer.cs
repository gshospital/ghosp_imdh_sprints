using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace CambioServicio
{
	partial class CAMBIO_SERMED_ALTA
	{

		#region "Upgrade Support "
		private static CAMBIO_SERMED_ALTA m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static CAMBIO_SERMED_ALTA DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new CAMBIO_SERMED_ALTA();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprCambios", "Label6", "lbPaciente", "Frame5", "CbbMedicoA", "Label3", "Label4", "TbMedicoDe", "Frame4", "CbbServicioA", "Label1", "Label2", "TbServicioDe", "Frame2", "tbHoraCambio", "SdcFecha", "Label21", "Label19", "Frame3", "CbCambio", "cbAceptar", "cbCancelar", "cbSalir", "SprEpisodios", "Frame1", "SprEpisodios_Sheet1", "SprCambios_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprCambios;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		public Telerik.WinControls.UI.RadDropDownList CbbMedicoA;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadTextBox TbMedicoDe;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
        public Telerik.WinControls.UI.RadDropDownList CbbServicioA;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadTextBox TbServicioDe;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadMaskedEditBox tbHoraCambio;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFecha;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadLabel Label19;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadButton CbCambio;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbSalir;
		public UpgradeHelpers.Spread.FpSpread SprEpisodios;
		public Telerik.WinControls.UI.RadGroupBox Frame1;

        //private FarPoint.Win.Spread.SheetView SprEpisodios_Sheet1 = null;
        //private FarPoint.Win.Spread.SheetView SprCambios_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CAMBIO_SERMED_ALTA));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.SprCambios = new UpgradeHelpers.Spread.FpSpread();
            this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.CbbMedicoA = new Telerik.WinControls.UI.RadDropDownList();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.TbMedicoDe = new Telerik.WinControls.UI.RadTextBox();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.CbbServicioA = new Telerik.WinControls.UI.RadDropDownList();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.TbServicioDe = new Telerik.WinControls.UI.RadTextBox();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbHoraCambio = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.SdcFecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label21 = new Telerik.WinControls.UI.RadLabel();
            this.Label19 = new Telerik.WinControls.UI.RadLabel();
            this.CbCambio = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbSalir = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.SprEpisodios = new UpgradeHelpers.Spread.FpSpread();
            this.Frame5.SuspendLayout();
            this.Frame4.SuspendLayout();
            this.Frame2.SuspendLayout();
            this.Frame3.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SprCambios
            // 
            this.SprCambios.Location = new System.Drawing.Point(4, 134);
            this.SprCambios.Name = "SprCambios";
            this.SprCambios.Size = new System.Drawing.Size(564, 88);
            this.SprCambios.TabIndex = 2;
            //this.SprCambios.CellClick += new UpgradeHelpers.FpSpread.CellClickEventHandler(this.SprCambios_CellClick);
            this.SprCambios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprCambios_CellClick);
            // 
            // Frame5
            // 
            //this.Frame5.BackColor = System.Drawing.SystemColors.Control;
            this.Frame5.Controls.Add(this.Label6);
            this.Frame5.Controls.Add(this.lbPaciente);
            this.Frame5.Enabled = true;
            //this.Frame5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame5.Location = new System.Drawing.Point(4, -2);
            this.Frame5.Name = "Frame5";
            this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame5.Size = new System.Drawing.Size(566, 39);
            this.Frame5.TabIndex = 22;
            this.Frame5.Visible = true;
            // 
            // Label6
            // 
            //this.Label6.BackColor = System.Drawing.SystemColors.Control;
            //this.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            //this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label6.Location = new System.Drawing.Point(14, 14);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(51, 17);
            this.Label6.TabIndex = 24;
            this.Label6.Text = "Paciente:";
            // 
            // lbPaciente
            // 
            //this.lbPaciente.BackColor = System.Drawing.SystemColors.Control;
            //this.lbPaciente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbPaciente.Location = new System.Drawing.Point(78, 12);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(478, 19);
            this.lbPaciente.TabIndex = 23;
            this.lbPaciente.Enabled = false;
            // 
            // Frame4
            // 
            //this.Frame4.BackColor = System.Drawing.SystemColors.Control;
            this.Frame4.Controls.Add(this.CbbMedicoA);
            this.Frame4.Controls.Add(this.Label3);
            this.Frame4.Controls.Add(this.Label4);
            this.Frame4.Controls.Add(this.TbMedicoDe);
            this.Frame4.Enabled = true;
            //this.Frame4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame4.Location = new System.Drawing.Point(2, 300);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(567, 41);
            this.Frame4.TabIndex = 17;
            this.Frame4.Text = "Cambio de M�dico";
            this.Frame4.Visible = true;
            // 
            // CbbMedicoA
            // 
            //this.CbbMedicoA.BackColor = System.Drawing.SystemColors.Window;
            this.CbbMedicoA.CausesValidation = true;
            this.CbbMedicoA.Cursor = System.Windows.Forms.Cursors.Default;
            //this.CbbMedicoA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.CbbMedicoA.Enabled = true;
            this.CbbMedicoA.ForeColor = System.Drawing.SystemColors.WindowText;
            //this.CbbMedicoA.IntegralHeight = true;
            this.CbbMedicoA.Location = new System.Drawing.Point(293, 14);
            this.CbbMedicoA.Name = "CbbMedicoA";
            this.CbbMedicoA.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbbMedicoA.Size = new System.Drawing.Size(267, 21);
            //this.CbbMedicoA.Sorted = false;
			this.CbbMedicoA.TabIndex = 18;
			this.CbbMedicoA.TabStop = true;
			this.CbbMedicoA.Visible = true;
			this.CbbMedicoA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbMedicoA_KeyPress);
			this.CbbMedicoA.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbMedicoA_SelectionChangeCommitted);
			this.CbbMedicoA.TextChanged += new System.EventHandler(this.CbbMedicoA_TextChanged);
			// 
			// Label3
			// 
			//this.Label3.BackColor = System.Drawing.SystemColors.Control;
			//this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label3.Location = new System.Drawing.Point(8, 15);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(17, 17);
			this.Label3.TabIndex = 21;
			this.Label3.Text = "De:";
			// 
			// Label4
			// 
			//this.Label4.BackColor = System.Drawing.SystemColors.Control;
			//this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label4.Location = new System.Drawing.Point(277, 15);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(17, 17);
			this.Label4.TabIndex = 20;
			this.Label4.Text = "A:";
			// 
			// TbMedicoDe
			// 
			//this.TbMedicoDe.BackColor = System.Drawing.SystemColors.Control;
			//this.TbMedicoDe.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.TbMedicoDe.Cursor = System.Windows.Forms.Cursors.Default;
			//this.TbMedicoDe.ForeColor = System.Drawing.SystemColors.ControlText;
			this.TbMedicoDe.Location = new System.Drawing.Point(32, 16);
			this.TbMedicoDe.Name = "TbMedicoDe";
			this.TbMedicoDe.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TbMedicoDe.Size = new System.Drawing.Size(234, 19);
			this.TbMedicoDe.TabIndex = 19;
            this.TbMedicoDe.Enabled = false;
			// 
			// Frame2
			// 
			//this.Frame2.BackColor = System.Drawing.SystemColors.Control;
			this.Frame2.Controls.Add(this.CbbServicioA);
			this.Frame2.Controls.Add(this.Label1);
			this.Frame2.Controls.Add(this.Label2);
			this.Frame2.Controls.Add(this.TbServicioDe);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(2, 258);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(567, 41);
			this.Frame2.TabIndex = 12;
			this.Frame2.Text = "Cambio de Servicio";
			this.Frame2.Visible = true;
			// 
			// CbbServicioA
			// 
			//this.CbbServicioA.BackColor = System.Drawing.SystemColors.Window;
			this.CbbServicioA.CausesValidation = true;
			this.CbbServicioA.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CbbServicioA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
			this.CbbServicioA.Enabled = true;
			//this.CbbServicioA.ForeColor = System.Drawing.SystemColors.WindowText;
			//this.CbbServicioA.IntegralHeight = true;
			this.CbbServicioA.Location = new System.Drawing.Point(292, 14);
			this.CbbServicioA.Name = "CbbServicioA";
			this.CbbServicioA.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbbServicioA.Size = new System.Drawing.Size(269, 21);
			//this.CbbServicioA.Sorted = false;
			this.CbbServicioA.TabIndex = 13;
			this.CbbServicioA.TabStop = true;
			this.CbbServicioA.Visible = true;
			this.CbbServicioA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbServicioA_KeyPress);
			this.CbbServicioA.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbServicioA_SelectionChangeCommitted);
			this.CbbServicioA.TextChanged += new System.EventHandler(this.CbbServicioA_TextChanged);
			// 
			// Label1
			// 
			//this.Label1.BackColor = System.Drawing.SystemColors.Control;
			//this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(8, 15);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(17, 17);
			this.Label1.TabIndex = 16;
			this.Label1.Text = "De:";
			// 
			// Label2
			// 
			//this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(277, 15);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(17, 17);
			this.Label2.TabIndex = 15;
			this.Label2.Text = "A:";
			// 
			// TbServicioDe
			// 
			//this.TbServicioDe.BackColor = System.Drawing.SystemColors.Control;
			//this.TbServicioDe.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.TbServicioDe.Cursor = System.Windows.Forms.Cursors.Default;
			//this.TbServicioDe.ForeColor = System.Drawing.SystemColors.ControlText;
			this.TbServicioDe.Location = new System.Drawing.Point(32, 14);
			this.TbServicioDe.Name = "TbServicioDe";
			this.TbServicioDe.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TbServicioDe.Size = new System.Drawing.Size(235, 20);
			this.TbServicioDe.TabIndex = 14;
            this.TbServicioDe.Enabled = false;
			// 
			// Frame3
			// 
			//this.Frame3.BackColor = System.Drawing.SystemColors.Control;
			this.Frame3.Controls.Add(this.tbHoraCambio);
			this.Frame3.Controls.Add(this.SdcFecha);
			this.Frame3.Controls.Add(this.Label21);
			this.Frame3.Controls.Add(this.Label19);
			this.Frame3.Enabled = true;
			//this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame3.Location = new System.Drawing.Point(2, 222);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(567, 36);
			this.Frame3.TabIndex = 7;
			this.Frame3.Visible = true;
			// 
			// tbHoraCambio
			// 
			this.tbHoraCambio.AllowPromptAsInput = false;
			this.tbHoraCambio.Location = new System.Drawing.Point(316, 12);
			this.tbHoraCambio.Mask = "99:99";
			this.tbHoraCambio.Name = "tbHoraCambio";
			this.tbHoraCambio.PromptChar = ' ';
			this.tbHoraCambio.Size = new System.Drawing.Size(45, 20);
			this.tbHoraCambio.TabIndex = 8;
			// 
			// SdcFecha
			// 
			//this.SdcFecha.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.SdcFecha.CustomFormat = "dd/MM/yyyy";
			this.SdcFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SdcFecha.Location = new System.Drawing.Point(114, 12);
			this.SdcFecha.Name = "SdcFecha";
			this.SdcFecha.Size = new System.Drawing.Size(129, 19);
			this.SdcFecha.TabIndex = 9;
            this.SdcFecha.MinDate = System.DateTime.Parse("0001/01/01");
			// 
			// Label21
			// 
			//this.Label21.BackColor = System.Drawing.SystemColors.Control;
			//this.Label21.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label21.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label21.Location = new System.Drawing.Point(14, 12);
			this.Label21.Name = "Label21";
			this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label21.Size = new System.Drawing.Size(89, 17);
			this.Label21.TabIndex = 11;
			this.Label21.Text = "Fecha cambio:";
			// 
			// Label19
			// 
			//this.Label19.BackColor = System.Drawing.SystemColors.Control;
			//this.Label19.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label19.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label19.Location = new System.Drawing.Point(268, 12);
			this.Label19.Name = "Label19";
			this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label19.Size = new System.Drawing.Size(33, 17);
			this.Label19.TabIndex = 10;
			this.Label19.Text = "Hora:";
			// 
			// CbCambio
			// 
			//this.CbCambio.BackColor = System.Drawing.SystemColors.Control;
			this.CbCambio.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCambio.Enabled = false;
			//this.CbCambio.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CbCambio.Location = new System.Drawing.Point(234, 346);
			this.CbCambio.Name = "CbCambio";
			this.CbCambio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCambio.Size = new System.Drawing.Size(79, 29);
			this.CbCambio.TabIndex = 6;
			this.CbCambio.Text = "Ca&mbio";
			this.CbCambio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.CbCambio.Click += new System.EventHandler(this.CbCambio_Click);
            this.CbCambio.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cbAceptar
            // 
            //this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Enabled = false;
			//this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAceptar.Location = new System.Drawing.Point(318, 346);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(79, 29);
			this.cbAceptar.TabIndex = 5;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            this.cbAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;

            // 
            // cbCancelar
            // 
            //this.cbCancelar.BackColor = System.Drawing.SystemColors.Control;
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Enabled = false;
			//this.cbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCancelar.Location = new System.Drawing.Point(404, 346);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(79, 29);
			this.cbCancelar.TabIndex = 4;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            this.cbCancelar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cbSalir
            // 
            //this.cbSalir.BackColor = System.Drawing.SystemColors.Control;
            this.cbSalir.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbSalir.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbSalir.Location = new System.Drawing.Point(488, 346);
			this.cbSalir.Name = "cbSalir";
			this.cbSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbSalir.Size = new System.Drawing.Size(79, 29);
			this.cbSalir.TabIndex = 3;
			this.cbSalir.Text = "&Salir";
			this.cbSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbSalir.UseVisualStyleBackColor = false;
			this.cbSalir.Click += new System.EventHandler(this.cbSalir_Click);
            this.cbSalir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame1
            // 
            //this.Frame1.BackColor = System.Drawing.SystemColors.Control;
            this.Frame1.Controls.Add(this.SprEpisodios);
			this.Frame1.Enabled = true;
			//this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(4, 38);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(565, 96);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Episodios";
			this.Frame1.Visible = true;
			// 
			// SprEpisodios
			// 
			this.SprEpisodios.Location = new System.Drawing.Point(46, 16);
			this.SprEpisodios.Name = "SprEpisodios";
			this.SprEpisodios.Size = new System.Drawing.Size(486, 72);
			this.SprEpisodios.TabIndex = 1;
			this.SprEpisodios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprEpisodios_CellClick);
			// 
			// CAMBIO_SERMED_ALTA
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(572, 381);
			this.Controls.Add(this.SprCambios);
			this.Controls.Add(this.Frame5);
			this.Controls.Add(this.Frame4);
			this.Controls.Add(this.Frame2);
			this.Controls.Add(this.Frame3);
			this.Controls.Add(this.CbCambio);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbSalir);
			this.Controls.Add(this.Frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CAMBIO_SERMED_ALTA";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Cambio de Servicio/M�dico de pacientes dados de alta - AGI241F1";
			this.Activated += new System.EventHandler(this.CAMBIO_SERMED_ALTA_Activated);
			this.Closed += new System.EventHandler(this.CAMBIO_SERMED_ALTA_Closed);
			this.Load += new System.EventHandler(this.CAMBIO_SERMED_ALTA_Load);
			this.Frame5.ResumeLayout(false);
			this.Frame4.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}