using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using UpgradeHelpers.Helpers;
using UpgradeSupportHelper;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;

namespace CambioServicio
{
	public partial class CAMBIO_SERMED_ALTA
        : RadForm
    {

		// guarda identificador del paciente recibido via tag de la pantalla
		string vstGidenpac = String.Empty;

		// episodio seleccionado en la spread
		int iA�oRegistro = 0;
		int iNumeroRegistro = 0;
		string stFechaAltaEpisodio = String.Empty;

		int lRegistroSeleccionado = 0; //registro marcado en el spread
		bool fFilaSeleccionada = false; //guarda si hay una fila marcada o no

		int viServicioOrigen = 0; //servicio seleccionado
		int viMedicoOrigen = 0; //medico seleccionado
		int viServicioDestino = 0; //servicio seleccionado
		int viMedicoDestino = 0; //medico seleccionado

		//booleanas para controlar cuando se cambia el medico
		bool fMedico = false; //Cambia M�dico
		bool fServicio = false; //Cambia servicio
		public CAMBIO_SERMED_ALTA()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		public void proInicDetalle()
		{
			//rutina para inicializar el detalle del cambio
			//se limpian los campos de la pantalla
			SdcFecha.Value = DateTime.Parse("0001/01/01");
			tbHoraCambio.Text = "  :  ";

			CbbMedicoA.Text = "";
			CbbServicioA.Text = "";
			TbServicioDe.Text = "";
			TbMedicoDe.Text = "";

			//se protegen los campos de la pantalla
			Frame3.Enabled = false;
			Label21.Enabled = false;
			SdcFecha.Enabled = false;
			Label19.Enabled = false;
			tbHoraCambio.Enabled = false;

			Frame1.Enabled = true;
			Frame2.Enabled = false;
			Frame4.Enabled = false;

			CbCambio.Enabled = false;
			cbAceptar.Enabled = false;
			cbCancelar.Enabled = false;

		}

		public void proRellena_Grid()
		{
			string sqlEpisodios = String.Empty;
			DataSet rREpisodios = null;
			int longi = 0;
			int longi2 = 0;

			try
			{

				//se inicializa las cabeceras del 2� spread
				// inicializo cabeceras 2� spread
				longi = 500;
				longi2 = 300;
				SprCambios.MaxCols = 6;
				SprCambios.MaxRows = 0;
				SprCambios.Row = 0;
				SprCambios.Col = 1;
				SprCambios.Text = "Fecha del cambio";
				SprCambios.SetColWidth(1, CreateGraphics().MeasureString(SprCambios.Text, Font).Width * 15 + longi + 100);
				SprCambios.Col = 2;
				SprCambios.Text = "Servicio";
				SprCambios.SetColWidth(2, CreateGraphics().MeasureString(SprCambios.Text, Font).Width * 15 + longi + 2200);
				SprCambios.Col = 3;
				SprCambios.Text = "M�dico";
				SprCambios.SetColWidth(3, CreateGraphics().MeasureString(SprCambios.Text, Font).Width * 15 + longi + 2225);
				SprCambios.Col = 4; //fecha/hora completa
				SprCambios.SetColHidden(SprCambios.Col, true);
				SprCambios.Col = 5; //codigo servicio
				SprCambios.SetColHidden(SprCambios.Col, true);
				SprCambios.Col = 6; //codigo medico
				SprCambios.SetColHidden(SprCambios.Col, true);

				// inicializo cabeceras 1� spread
				longi = 500;
				longi2 = 300;

				SprEpisodios.MaxCols = 5;
				SprEpisodios.MaxRows = 1;
				SprEpisodios.Row = 0;
				SprEpisodios.Col = 1;
				SprEpisodios.Text = "A�o";
				SprEpisodios.SetColWidth(1, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 15 + longi + 600);
				SprEpisodios.Col = 2;
				SprEpisodios.Text = "N�mero";
				SprEpisodios.SetColWidth(2, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 15 + longi + 100);
				SprEpisodios.Col = 3;
				SprEpisodios.Text = "Fecha ingreso";
				SprEpisodios.SetColWidth(3, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 15 + longi + 800);
				SprEpisodios.Col = 4;
				SprEpisodios.Text = "Fecha alta";
				SprEpisodios.SetColWidth(4, CreateGraphics().MeasureString(SprEpisodios.Text, Font).Width * 15 + longi + 900);

				SprEpisodios.Col = 5; //columna oculta con los segundos de alta
				SprEpisodios.SetColHidden(SprEpisodios.Col, true);

				//se hace la select para llenar el primer spread con los episodios del paciente
				//con fecha de alta en planta distinta de nulos

				sqlEpisodios = "SELECT GNUMADME, GANOADME, FLLEGADA, FALTPLAN " + " FROM AEPISADM " + " WHERE " + " AEPISADM.GIDENPAC = '" + vstGidenpac + "' and" + " FALTPLAN is not null " + " order by fllegada desc";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEpisodios, bCambioServicio.RcAdmision);
				rREpisodios = new DataSet();
				tempAdapter.Fill(rREpisodios);
				if (rREpisodios.Tables[0].Rows.Count != 0)
				{
					SprEpisodios.MaxRows = rREpisodios.Tables[0].Rows.Count;

                    rREpisodios.MoveFirst();
					longi = 1;
					cbAceptar.Enabled = false;

					foreach (DataRow iteration_row in rREpisodios.Tables[0].Rows)
					{
						//SprEpisodios.SetRowHeight(longi, longi2);
						SprEpisodios.Row = longi;
						SprEpisodios.Col = 1;
						SprEpisodios.Text = Convert.ToString(iteration_row["ganoadme"]);
						SprEpisodios.Col = 2;
						SprEpisodios.Text = Convert.ToString(iteration_row["gnumadme"]);
						SprEpisodios.Col = 3;
						SprEpisodios.Text = Convert.ToDateTime(iteration_row["fllegada"]).ToString("dd/MM/yyyy HH:mm");
						SprEpisodios.Col = 4;
						SprEpisodios.Text = Convert.ToDateTime(iteration_row["faltplan"]).ToString("dd/MM/yyyy HH:mm");
						SprEpisodios.Col = 5;
						SprEpisodios.Text = Convert.ToDateTime(iteration_row["faltplan"]).ToString("ss");
						longi++;
					}

                    //SprEpisodios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; // single selection
                    SprEpisodios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                    SprEpisodios.setSelModeIndex(0);
					// inicializo detalle para que al cambiar de episodio no se quede
					proInicDetalle();
				}
				else
				{
					// si no hay ning�n episodio, mando mensaje y descargo la ventana
					short tempRefParam = 1520;
					string[] tempRefParam2 = new string[] {"episodios", lbPaciente.Text.Trim()};
					Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, bCambioServicio.RcAdmision, tempRefParam2));
					this.Close();
				}

                rREpisodios.Close();
			}
			catch (Exception Excep)
            {

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_sermed_alta:proRellenaGrid", Excep);
			}

		}
		public void proRellena_Grid2()
		{
			string sqlCambios = String.Empty;
			DataSet rrCambios = null;
			//Dim longi As Integer
			//Dim longi2 As Integer
			//para guardar el medico y el servicio que se va cargando en el spread
			int iServicioAux = 0;
			int lMedicoaux = 0;
			//columna y fila del spread para ir rellenando
			int lcol = 0;
			int lfil = 0;

			try
			{

				//relleno los datos del episodio
				//SprEpisodios.Row = SprEpisodios.ActiveSheet.ActiveRowIndex;
                SprEpisodios.Row = SprEpisodios.ActiveRowIndex;
                SprEpisodios.Col = 1;
				iA�oRegistro = Convert.ToInt32(Double.Parse(SprEpisodios.Text));
				SprEpisodios.Col = 2;
				iNumeroRegistro = Convert.ToInt32(Double.Parse(SprEpisodios.Text));

				//cargo en una variable la fecha de alta en planta del episodio
				SprEpisodios.Col = 4; //fecha de alta en planta
				stFechaAltaEpisodio = SprEpisodios.Text.Trim();
				SprEpisodios.Col = 5; //fecha de alta en planta
				//hay que concatenar los segundos a la fecha de alta del episodio
				stFechaAltaEpisodio = stFechaAltaEpisodio + ":" + SprEpisodios.Text.Trim();
				//select para rellenar el spread con
				//los movimientos de amovimie del episodio seleccionado

				iServicioAux = -1;
				lMedicoaux = -1;
				SprCambios.MaxRows = 0;
				lcol = 0;
				lfil = 0;

				sqlCambios = "select DSERVICI.dnomserv,DPERSONA.dap1pers,DPERSONA.dap2pers,DPERSONA.dnomperS,AMOVIMIE.* " + "from AMOVIMIE,DSERVICI,DPERSONA Where AMOVIMIE.itiposer = 'H' " + "and AMOVIMIE.ganoregi = " + iA�oRegistro.ToString() + " And AMOVIMIE.gnumregi = " + iNumeroRegistro.ToString() + " " + "and AMOVIMIE.gservior = DSERVICI.gservici AND AMOVIMIE.gmedicor = DPERSONA.gpersona ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlCambios, bCambioServicio.RcAdmision);
				rrCambios = new DataSet();
				tempAdapter.Fill(rrCambios);

				if (rrCambios.Tables[0].Rows.Count != 0)
				{
					rrCambios.MoveFirst();

					foreach (DataRow iteration_row in rrCambios.Tables[0].Rows)
					{
						//   sprCambios.RowHeight(longi) = longi2
						//   sprCambios.Row = longi
						//el primer registro tiene que salir
						//guardamos el servicio y el medico del primero
						if (iServicioAux != Convert.ToDouble(iteration_row["gservior"]) || lMedicoaux != Convert.ToDouble(iteration_row["gmedicor"]))
						{
							SprCambios.MaxRows++;
							iServicioAux = Convert.ToInt32(iteration_row["gservior"]);
							lMedicoaux = Convert.ToInt32(iteration_row["gmedicor"]);
							lfil++;
							SprCambios.Row = lfil;
							lcol++;
							SprCambios.Col = lcol;
							SprCambios.Text = Convert.ToDateTime(iteration_row["fmovimie"]).ToString("dd/MM/yyyy HH:mm");
							lcol++;
							SprCambios.Col = lcol;
							SprCambios.Text = Convert.ToString(iteration_row["dnomserv"]).Trim();
							lcol++;
							SprCambios.Col = lcol;
							SprCambios.Text = Convert.ToString(iteration_row["dap1pers"]).Trim() + " " + Convert.ToString(iteration_row["dap2pers"]).Trim() + "," + Convert.ToString(iteration_row["dnompers"]).Trim();
							//columnas ocultas
							lcol++;
							//hora completa
							SprCambios.Col = lcol;
							SprCambios.Text = Convert.ToDateTime(iteration_row["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss");
							lcol++;
							//cod servicio
							SprCambios.Col = lcol;
							SprCambios.Text = Convert.ToString(iteration_row["gservior"]);
							lcol++;
							//cod medico
							SprCambios.Col = lcol;
							SprCambios.Text = Convert.ToString(iteration_row["gmedicor"]);
						}
						//    longi = longi + 1
						lcol = 0;
					}
					//SprCambios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; // single selection
                    SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;

                    SprCambios.setSelModeIndex(0);
				}
				else
				{
					//no se recupera ningun movimiento
				}

                rrCambios.Close();
			}
			catch (Exception Excep)
            {

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_sermed_alta:proRellenaGrid2", Excep);
			}

		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int ServicioCambio = 0;
			int MedicoCambio = 0;

			int ServicioAnterior = 0;
			int MedicoAnterior = 0;
			int ServicioPosterior = 0;
			int MedicoPosterior = 0;

			string stSql = String.Empty;
			DataSet RrAceptar = null;
			string stFechaMovimiento = String.Empty;
			string stFechaSiguiente = String.Empty;
			int iSerQueCambia = 0;

			try
			{



				//*****************
				//no se permite nunca cambiar la fecha/hora
				//solo se permite modificar el medico y el servicio y no en todos los casos
				//solo cuando exista en DMOVFACT con ffactura = null
				//cuando se trata de un cambio hay que validar que el cambio sea diferente al movimiento anterior y
				//al movimiento posterior al seleccionado

				//guardamos el codigo del ServicioDestino
				if (CbbServicioA.Text.Trim() == "")
				{
					SprCambios.Row = lRegistroSeleccionado;
					SprCambios.Col = 5;
					ServicioCambio = Convert.ToInt32(Double.Parse(SprCambios.Text));
					viServicioDestino = Convert.ToInt32(Double.Parse(SprCambios.Text));
				}
				else
				{
                    ServicioCambio = Convert.ToInt32(CbbServicioA.SelectedValue);
                }


				//guardamos el codigo del MedicoDestino
				if (CbbMedicoA.Text.Trim() == "")
				{
					SprCambios.Row = lRegistroSeleccionado;
					SprCambios.Col = 6;
					MedicoCambio = Convert.ToInt32(Double.Parse(SprCambios.Text));
					viMedicoDestino = Convert.ToInt32(Double.Parse(SprCambios.Text));
				}
				else
				{
                    MedicoCambio = Convert.ToInt32(CbbMedicoA.SelectedValue);
                }

				//hacemos las comparaciones pertinentes
				if (SprCambios.MaxRows > 1)
				{
					//si hay mas de un registro en el spread
					if (lRegistroSeleccionado == 1)
					{
						//si es el primer registro el seleccionado
						//se compara con el siguiente
						SprCambios.Row = lRegistroSeleccionado + 1;
						SprCambios.Col = 5;
						ServicioPosterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
						SprCambios.Col = 6;
						MedicoPosterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
						if (ServicioCambio == ServicioPosterior && MedicoCambio == MedicoPosterior)
						{
							short tempRefParam = 1125;
							string[] tempRefParam2 = new string[] {"El servicio y el m�dico coinciden con el cambio posterior ", "modificar el registro seleccionado"};
							Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, bCambioServicio.RcAdmision, tempRefParam2));
							if (Serrores.iresume == 7)
							{
								//se tiene que salir de la rutina
								this.Cursor = Cursors.Default;
								return;
							}
						}
					}
					else
					{
						if (lRegistroSeleccionado == SprCambios.MaxRows)
						{
							//si se trata del ultimo registro
							//se compara con el anterior
							SprCambios.Row = lRegistroSeleccionado - 1;
							SprCambios.Col = 5;
							ServicioAnterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
							SprCambios.Col = 6;
							MedicoAnterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
							if (ServicioCambio == ServicioAnterior && MedicoCambio == MedicoAnterior)
							{
								short tempRefParam3 = 1125;
								string[] tempRefParam4 = new string[]{"El servicio y el m�dico coinciden con el cambio anterior ", "modificar el registro seleccionado"};
								Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, bCambioServicio.RcAdmision, tempRefParam4));
								if (Serrores.iresume == 7)
								{
									this.Cursor = Cursors.Default;
									return;
								}
							}
						}
						else
						{
							//si es uno intermedio se tiene que comparar con el anterior
							//y con el posterior
							SprCambios.Row = lRegistroSeleccionado + 1;
							SprCambios.Col = 5;
							ServicioPosterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
							SprCambios.Col = 6;
							MedicoPosterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
							if (ServicioCambio == ServicioPosterior && MedicoCambio == MedicoPosterior)
							{
								short tempRefParam5 = 1125;
								string[] tempRefParam6 = new string[] {"El servicio y el m�dico coinciden con el cambio posterior ", "modificar el registro seleccionado"};
								Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, bCambioServicio.RcAdmision, tempRefParam6));
								if (Serrores.iresume == 7)
								{
									this.Cursor = Cursors.Default;
									return;
								}
							}
							SprCambios.Row = lRegistroSeleccionado - 1;
							SprCambios.Col = 5;
							ServicioAnterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
							SprCambios.Col = 6;
							MedicoAnterior = Convert.ToInt32(Double.Parse(SprCambios.Text));
							if (ServicioCambio == ServicioAnterior && MedicoCambio == MedicoAnterior)
							{
								short tempRefParam7 = 1125;
								string[] tempRefParam8 = new string[] {"El servicio y el m�dico coinciden con el cambio anterior ", "modificar el registro seleccionado"};
								Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, bCambioServicio.RcAdmision, tempRefParam8));
								if (Serrores.iresume == 7)
								{
									this.Cursor = Cursors.Default;
									return;
								}
							}
						}
					}
				}
				//validacion de que el medico seleccionado coincida con el servicio del cambio
				if (fServicio && !fMedico)
				{
					stSql = "SELECT * FROM DPERSONAva,DPERSERV " + " WHERE GSERVICI=" + CbbServicioA.Items[CbbServicioA.SelectedIndex].Value + " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA " + " and dpersonava.gpersona =" + MedicoCambio.ToString();

					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, bCambioServicio.RcAdmision);
					RrAceptar = new DataSet();
					tempAdapter.Fill(RrAceptar);
					if (RrAceptar.Tables[0].Rows.Count == 0)
					{ //DEBE CAMBIAR EL M�DICO POR UNO QUE CORRESPONDA AL SERVICIO
						this.Cursor = Cursors.Default;
						short tempRefParam9 = 1040;
						string[] tempRefParam10 = new string[] {"M�dico"};
						Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, bCambioServicio.RcAdmision, tempRefParam10));
						RrAceptar.Close();
						CbbMedicoA.Focus();
						return;
					}
				}
				//una vez que cumple las comparaciones pertinentes
				//buscamos que no exista en dmovfact

				SprCambios.Row = lRegistroSeleccionado;
				SprCambios.Col = 4;
				stFechaMovimiento = SprCambios.Text;

				//no se puede modificar si los movimientos estan facturados en dmovfact
				object tempRefParam11 = stFechaMovimiento;
				stSql = "select * from DMOVFACT where itiposer = 'H' and " + " ganoregi=" + iA�oRegistro.ToString() + " and gnumregi=" + iNumeroRegistro.ToString() + " and " + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam11) + " and " + " ffactura is not null ";
				stFechaMovimiento = Convert.ToString(tempRefParam11);

				if (Serrores.proExisteTabla("DMOVFACH", bCambioServicio.RcAdmision))
				{
					string tempRefParam12 = "DMOVFACT";
					stSql = stSql + " UNION ALL " + Serrores.proReplace(ref stSql, tempRefParam12, "DMOVFACH");
				}

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, bCambioServicio.RcAdmision);
				RrAceptar = new DataSet();
				tempAdapter_2.Fill(RrAceptar);
				if (RrAceptar.Tables[0].Rows.Count != 0)
				{
					//si hay filas en DMOVFACT con ffactura = null no se le permite modificar los registros
					RrAceptar.Close();
					this.Cursor = Cursors.Default;
					short tempRefParam13 = 1940;
					string[] tempRefParam14 = new string[]{"modificar", "ya ha sido facturado"};
					Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, bCambioServicio.RcAdmision, tempRefParam14));
					return;
				}
				else
				{
					//no hay filas en dmovfact que tengan ffactura
					//si es el primero o el ultimo registro hay que actualizar AEPISADM
					//Actualizar AMOVIMIE
					//Actualizar DMOVFACT
					SprCambios.Col = 5; // c�digo de servicio
					iSerQueCambia = Convert.ToInt32(Double.Parse(SprCambios.Text));

					if (lRegistroSeleccionado == 1 && SprCambios.MaxRows == 1)
					{
						//es la primera y la unica fila de spread
						bCambioServicio.RcAdmision.BeginTransScope();
                        //modificar en AEPISADM gservici,gpersona,gserulti,gperulti
                        ActualizarAEPISADM(viServicioDestino, viMedicoDestino, viServicioDestino, viMedicoDestino);
						//camposOrigen
						object tempRefParam15 = stFechaMovimiento;
						ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam15) + " ", "ORIGEN", viServicioDestino, viMedicoDestino);
						stFechaMovimiento = Convert.ToString(tempRefParam15);
						//campos destino
						object tempRefParam16 = stFechaMovimiento;
						ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam16) + " and ffinmovi is not null ", "DESTINO", viServicioDestino, viMedicoDestino);
						stFechaMovimiento = Convert.ToString(tempRefParam16);
						//se actualiza DMOVFACT
						ActualizarDMOVFACT(ref stFechaMovimiento, ref stFechaAltaEpisodio);

						if (iSerQueCambia != viServicioDestino)
						{ // si ha cambiado el servicio

							string tempRefParam17 = "00:00:00";
							bCambioServicio.ActualizarServicioConsumos(iA�oRegistro, iNumeroRegistro, iSerQueCambia, viServicioDestino.ToString(), ref stFechaMovimiento, true, ref tempRefParam17);

						}

						bCambioServicio.RcAdmision.CommitTransScope();

                    }
					else
					{
						//guardamos los datos de la fila siguiente al seleccionado
						SprCambios.Row = lRegistroSeleccionado + 1;
						SprCambios.Col = 4;
						stFechaSiguiente = SprCambios.Text;
						if (lRegistroSeleccionado == 1 && SprCambios.MaxRows > 1)
						{
							bCambioServicio.RcAdmision.BeginTransScope();
                            //es la primera fila pero no la ultima
                            //se actualiza en AEPISADM gservici,gpersona
                            ActualizarAEPISADM(viServicioDestino, viMedicoDestino);
							//camposOrigen
							object tempRefParam18 = stFechaMovimiento;
							object tempRefParam19 = stFechaSiguiente;
							ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam18) + " and fmovimie < " + Serrores.FormatFechaHMS(tempRefParam19) + " ", "ORIGEN", viServicioDestino, viMedicoDestino);
							stFechaSiguiente = Convert.ToString(tempRefParam19);
							stFechaMovimiento = Convert.ToString(tempRefParam18);
							//camposdestino
							object tempRefParam20 = stFechaMovimiento;
							object tempRefParam21 = stFechaSiguiente;
							ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam20) + " and ffinmovi < " + Serrores.FormatFechaHMS(tempRefParam21) + " ", "DESTINO", viServicioDestino, viMedicoDestino);
							stFechaSiguiente = Convert.ToString(tempRefParam21);
							stFechaMovimiento = Convert.ToString(tempRefParam20);
							ActualizarDMOVFACT(ref stFechaMovimiento, ref stFechaSiguiente);

							if (iSerQueCambia != viServicioDestino)
							{ // si ha cambiado el servicio

								bCambioServicio.ActualizarServicioConsumos(iA�oRegistro, iNumeroRegistro, iSerQueCambia, viServicioDestino.ToString(), ref stFechaMovimiento, false, ref stFechaSiguiente);

							}

                            bCambioServicio.RcAdmision.CommitTransScope();
                        }
						else
						{
							//si es la ultima fila del spread
							if (lRegistroSeleccionado == SprCambios.MaxRows)
							{
								bCambioServicio.RcAdmision.BeginTransScope();
                                //se actualiza en AEPISADM gserulti,gperulti
                                ActualizarAEPISADM(-1, -1, viServicioDestino, viMedicoDestino);
								//actualizar los registros de AMOVIMIE
								//campos origen
								object tempRefParam22 = stFechaMovimiento;
								ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam22) + " ", "ORIGEN", viServicioDestino, viMedicoDestino);
								stFechaMovimiento = Convert.ToString(tempRefParam22);
								//campos destino
								object tempRefParam23 = stFechaMovimiento;
								ActualizarAMOVIMIE("ffinmovi >= " + Serrores.FormatFechaHMS(tempRefParam23) + " and ffinmovi is not null", "DESTINO", viServicioDestino, viMedicoDestino);
								stFechaMovimiento = Convert.ToString(tempRefParam23);
								ActualizarDMOVFACT(ref stFechaMovimiento, ref stFechaAltaEpisodio);

								if (iSerQueCambia != viServicioDestino)
								{ // si ha cambiado el servicio

									string tempRefParam24 = "00:00:00";
									bCambioServicio.ActualizarServicioConsumos(iA�oRegistro, iNumeroRegistro, iSerQueCambia, viServicioDestino.ToString(), ref stFechaMovimiento, true, ref tempRefParam24);

								}

                                bCambioServicio.RcAdmision.CommitTransScope();
                            }
							else
							{
								//si no es la ultima ni la primera fila
								bCambioServicio.RcAdmision.BeginTransScope();
                                //campos origen
                                object tempRefParam25 = stFechaMovimiento;
								object tempRefParam26 = stFechaSiguiente;
								ActualizarAMOVIMIE("fmovimie >= " + Serrores.FormatFechaHMS(tempRefParam25) + " and fmovimie < " + Serrores.FormatFechaHMS(tempRefParam26) + " ", "ORIGEN", viServicioDestino, viMedicoDestino);
								stFechaSiguiente = Convert.ToString(tempRefParam26);
								stFechaMovimiento = Convert.ToString(tempRefParam25);
								//campos destino
								object tempRefParam27 = stFechaMovimiento;
								object tempRefParam28 = stFechaSiguiente;
								ActualizarAMOVIMIE("ffinmovi >= " + Serrores.FormatFechaHMS(tempRefParam27) + " and ffinmovi < " + Serrores.FormatFechaHMS(tempRefParam28) + " ", "DESTINO", viServicioDestino, viMedicoDestino);
								stFechaSiguiente = Convert.ToString(tempRefParam28);
								stFechaMovimiento = Convert.ToString(tempRefParam27);
								ActualizarDMOVFACT(ref stFechaMovimiento, ref stFechaSiguiente);

								if (iSerQueCambia != viServicioDestino)
								{ // si ha cambiado el servicio

									bCambioServicio.ActualizarServicioConsumos(iA�oRegistro, iNumeroRegistro, iSerQueCambia, viServicioDestino.ToString(), ref stFechaMovimiento, false, ref stFechaSiguiente);

								}

                                bCambioServicio.RcAdmision.CommitTransScope();
                            }
						}
					}
				}

				proRellena_Grid2();
				proInicDetalle();

				this.Cursor = Cursors.Default;
			}
			catch (Exception Excep)
            {
                bCambioServicio.RcAdmision.RollbackTransScope();

                this.Cursor = Cursors.Default;
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_sermed_alta:cbAceptar", Excep);
			}
		}

		public void ActualizarDMOVFACT(ref string stFechaInicio, ref string stFechafin)
		{


			string stSqlFact = "update dmovfact set gserreal = " + viServicioDestino.ToString() + ", gpersona = " + viMedicoDestino.ToString() + " ";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + iA�oRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + iNumeroRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " (gcodeven= 'ES' or gcodeven= 'AU') and ";
			object tempRefParam = stFechaInicio;
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam) + "";
			stFechaInicio = Convert.ToString(tempRefParam);
			object tempRefParam2 = stFechafin;
			stSqlFact = stSqlFact + " and ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam2) + "";
			stFechafin = Convert.ToString(tempRefParam2);

			SqlCommand tempCommand = new SqlCommand(stSqlFact, bCambioServicio.RcAdmision);
			tempCommand.ExecuteNonQuery();

			stSqlFact = "update dmovfact set gsersoli = " + viServicioDestino.ToString() + " ";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + iA�oRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + iNumeroRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gcodeven= 'DI' and ";
			object tempRefParam3 = stFechaInicio;
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam3) + "";
			stFechaInicio = Convert.ToString(tempRefParam3);
			object tempRefParam4 = stFechafin;
			stSqlFact = stSqlFact + " and ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam4) + "";
			stFechafin = Convert.ToString(tempRefParam4);

			SqlCommand tempCommand_2 = new SqlCommand(stSqlFact, bCambioServicio.RcAdmision);
			tempCommand_2.ExecuteNonQuery();

			stSqlFact = "update dmovfact set gsersoli = " + viServicioDestino.ToString() + ", gserresp = " + viServicioDestino.ToString() + " ";
			stSqlFact = stSqlFact + " where itiposer= 'H' and ";
			stSqlFact = stSqlFact + " ganoregi=" + iA�oRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gnumregi=" + iNumeroRegistro.ToString() + " and ";
			stSqlFact = stSqlFact + " gcodeven= 'FA' and ";
			object tempRefParam5 = stFechaInicio;
			stSqlFact = stSqlFact + " ffinicio >=" + Serrores.FormatFechaHMS(tempRefParam5) + "";
			stFechaInicio = Convert.ToString(tempRefParam5);
			object tempRefParam6 = stFechafin;
			stSqlFact = stSqlFact + " and ffechfin<=" + Serrores.FormatFechaHMS(tempRefParam6) + "";
			stFechafin = Convert.ToString(tempRefParam6);

			SqlCommand tempCommand_3 = new SqlCommand(stSqlFact, bCambioServicio.RcAdmision);
			tempCommand_3.ExecuteNonQuery();

		}

		private void ActualizarAEPISADM(int iServInicial = -1, int lMedInicial = -1, int iServUltimo = -1, int lMedUltimo = -1)
		{
			//rutina que actualiza AEPISADM con los datos que le mandemos

			string sql = "SELECT * FROM AEPISADM WHERE " + " AEPISADM.ganoadme = " + iA�oRegistro.ToString() + " AND AEPISADM.gnumadme =  " + iNumeroRegistro.ToString() + " AND " + " AEPISADM.faltplan is not null and AEPISADM.gidenpac = '" + vstGidenpac + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, bCambioServicio.RcAdmision);
			DataSet RrActualizar = new DataSet();
			tempAdapter.Fill(RrActualizar);
            if (RrActualizar.Tables[0].Rows.Count != 0)
            {
                RrActualizar.Edit();
                if (iServInicial != -1 && lMedInicial != -1)
                {
                    RrActualizar.Tables[0].Rows[0]["GSERVICI"] = iServInicial;
                    RrActualizar.Tables[0].Rows[0]["GPERSONA"] = lMedInicial;
                }
                if (iServUltimo != -1 && lMedUltimo != -1)
                {
                    RrActualizar.Tables[0].Rows[0]["GSERULTI"] = iServUltimo;
                    RrActualizar.Tables[0].Rows[0]["GPERULTI"] = lMedUltimo;
                }
                string tempQuery = RrActualizar.Tables[0].TableName;
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(RrActualizar, tempQuery);
			}
			RrActualizar.Close();
		}

		private bool isInitializingComponent;
		private void CbbMedicoA_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (CbbMedicoA.Text == "")
			{
				fMedico = false;
				if (!fServicio)
				{
					cbAceptar.Enabled = false;
				}
			}
		}

		private void CbbMedicoA_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{

			if (CbbMedicoA.SelectedIndex != -1)
			{
				cbAceptar.Enabled = true;
				viMedicoDestino = Convert.ToInt32(CbbMedicoA.SelectedValue);
				fMedico = true;
			}
		}

		private void CbbMedicoA_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void CbbServicioA_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (CbbServicioA.Text == "")
			{
				fServicio = false;
				proMedicoA(viServicioDestino);
				fMedico = false;
				cbAceptar.Enabled = false;
			}
		}

		private void CbbServicioA_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{
			string tstCom = String.Empty;
			DataSet tRrCom = null;

			try
			{

				//cuando seleccionamos un servicio
				if (CbbServicioA.SelectedIndex != -1)
				{
					fServicio = true;
					viServicioDestino = Convert.ToInt32(CbbServicioA.SelectedValue);
					//se tienen que cargar los medicos del servicio seleccionado
					proMedicoA(Convert.ToInt32(CbbServicioA.SelectedValue));

					if (fServicio && !fMedico)
					{
						tstCom = "SELECT * " + " FROM DPERSONAva,DPERSERV " + " WHERE GSERVICI=" + CbbServicioA.Items[CbbServicioA.SelectedIndex].Value.ToString() + " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA " + " and dpersonava.gpersona =" + viMedicoDestino.ToString();
						SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCom, bCambioServicio.RcAdmision);
						tRrCom = new DataSet();
						tempAdapter.Fill(tRrCom);
						if (tRrCom.Tables[0].Rows.Count == 0)
						{ //DEBE CAMBIAR EL M�DICO POR UNO QUE CORRESPONDA AL SERVICIO
							CbbMedicoA.Focus();
							cbAceptar.Enabled = false;
						}
						else
						{
							cbAceptar.Enabled = true;
						}
					}
				}
				else
				{
					fServicio = false;
					cbAceptar.Enabled = false;
				}
			}
			catch
			{
			}

		}
		private void proMedicoA(int iServicio)
		{

			CbbMedicoA.Items.Clear();

			string stSqServicio = "SELECT DNOMPERS,DAP1PERS, DAP2PERS,DPERSONAva.GPERSONA " + " FROM DPERSONAva,DPERSERV " + " WHERE GSERVICI=" + iServicio.ToString() + " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA ";

			if (CbbServicioA.SelectedIndex == -1)
			{
				stSqServicio = stSqServicio + "and dpersonava.gpersona <>" + viMedicoOrigen.ToString();
			}

			//oscar 25/11/2003
			stSqServicio = stSqServicio + " AND DPERSONAVA.IMEDICO='S' ";
			//------

			stSqServicio = stSqServicio + " order by dap1pers,dap2pers,dnompers";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqServicio, bCambioServicio.RcAdmision);
			DataSet RrServicio = new DataSet();
			tempAdapter.Fill(RrServicio);
			foreach (DataRow iteration_row in RrServicio.Tables[0].Rows)
			{
                CbbMedicoA.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["DAP1PERS"]).Trim() + " " + Convert.ToString(iteration_row["DAP2PERS"]).Trim() + " " + Convert.ToString(iteration_row["DNOMPERS"]).Trim(), Convert.ToString(iteration_row["GPERSONA"]).Trim()));
        	}
			RrServicio.Close();

		}

		private void CbbServicioA_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void CbCambio_Click(Object eventSender, EventArgs eventArgs)
		{
			//tiene que haber un registro seleccionado
			try
			{

				if (lRegistroSeleccionado > 0)
				{
					//hay que activar
					Frame3.Enabled = false; //fecha/hora del cambio
					Label21.Enabled = false;
					SdcFecha.Enabled = false;
					tbHoraCambio.Enabled = false;
					Label19.Enabled = false;

					Frame2.Enabled = true; //servicio
					Frame4.Enabled = true; //medico
				}
				else
				{
					//MsgBox "debe seleccionar un registro para modificar"
					short tempRefParam = 1270;
					string[] tempRefParam2 = new string[]{"un registro"};
					Serrores.iresume = Convert.ToInt32(bCambioServicio.objError.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, bCambioServicio.RcAdmision, tempRefParam2));
					Frame3.Enabled = false; //fecha/hora del cambio
					Label21.Enabled = false;
					SdcFecha.Enabled = false;
					tbHoraCambio.Enabled = false;
					Label19.Enabled = false;
					Frame2.Enabled = false; //servicio
					Frame4.Enabled = false; //medico
				}
			}
			catch (Exception Excep)
            {
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_sermed_alta:cbCambio", Excep);
			}

		}

		private void BuscarServicio(int iServicio)
		{

			string stSqCambio = "SELECT dnomserv,gservici " + " FROM DSERVICI " + " WHERE " + " DSERVICI.gservici = " + iServicio.ToString() + " "; //String Sql

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqCambio, bCambioServicio.RcAdmision);
			DataSet rRcambio = new DataSet();
			tempAdapter.Fill(rRcambio);

			//Compruebo que hay un servicio
			//se carga el nombre del servicio y se carga el combo de servicios
			if (rRcambio.Tables[0].Rows.Count == 1)
			{
				TbServicioDe.Text = Convert.ToString(rRcambio.Tables[0].Rows[0]["dnomserv"]);
			}

			CbbServicioA.Items.Clear();

			//oscar 12/01/2004
			//No se podran efectuar traslados de pacientes ingresados en plaza de dia dados de alta
			//(episodios menores a 1900) a un servicio que no pueda ser de hospital de dia.
			//ademas, se debera restringir mas el servicio destino a un servicio de hospital de dia
			//que tenga el mismo indicador IMEDQUIR del servicio origen, para que un paciente ingresado
			//en servicios de HOSPITAL DE DIA QUIRURGICO (IMEDQUIR='Q' y ICENTDIA='S') solo puedan trasladarse
			// a servicios de HOSPITAL DE DIA QUIRURGICO.

			//Lleno el combo de servicio
			//stSqServicio = "SELECT GSERVICI, DNOMSERV " _
			//& " FROM DSERVICIva " _
			//& " WHERE IHOSPITA='S' " _
			//& "and gservici<>" & rRcambio("gservici") _
			//& " order by dnomserv"

			string stSqServicio = "SELECT GSERVICI, DNOMSERV " + " FROM DSERVICIva " + " WHERE IHOSPITA='S' and " + " gservici<>" + Convert.ToString(rRcambio.Tables[0].Rows[0]["gservici"]);
			if (iA�oRegistro < 1900)
			{
				stSqServicio = stSqServicio + 
				               " AND ICENTDIA='S'" + 
				               " AND IMEDQUIR = (SELECT IMEDQUIR FROM DSERVICIVA WHERE GSERVICI=" + Convert.ToString(rRcambio.Tables[0].Rows[0]["gservici"]) + ")";
			}

			stSqServicio = stSqServicio + " order by dnomserv";
			//---------------------------------------------------------------------------------

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqServicio, bCambioServicio.RcAdmision);
			DataSet RrServicio = new DataSet();
			tempAdapter_2.Fill(RrServicio);
			foreach (DataRow iteration_row in RrServicio.Tables[0].Rows)
			{
                CbbServicioA.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["DNOMSERV"]), Convert.ToInt32(iteration_row["GSERVICI"])));
        	}
			RrServicio.Close();

			//If rRcambio.RowCount = 1 Then
			//    proMedicoA rRcambio("gservici")
			//Else
			//    rRcambio.Close
			//End If

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			try
			{
				proInicDetalle();
			}
			catch (Exception Excep)
            {
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_sermed_alta:cbCancelar", Excep);
			}
		}

		private void cbSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void CAMBIO_SERMED_ALTA_Activated(Object eventSender, EventArgs eventArgs)
		{

			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				//Top = 0
				//Left = 0
				// relleno el grid de episodios
				vstGidenpac = Convert.ToString(this.Tag).Trim();

				string LitPersona = Serrores.ObtenerLiteralPersona(bCambioServicio.RcAdmision);
				LitPersona = LitPersona.ToLower() + "s";

				CAMBIO_SERMED_ALTA.DefInstance.Text = "Cambio de Servicio/M�dico de " + LitPersona + " dados de alta - AGI241F1";
				proRellena_Grid();
			}
		}

		private void CAMBIO_SERMED_ALTA_Load(Object eventSender, EventArgs eventArgs)
		{

			string LitPersona = Serrores.ObtenerLiteralPersona(bCambioServicio.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + ":";
			Label6.Text = LitPersona;
           
            // recibe en el tag el gidenpac del paciente seleccionado
        }

		private void SprCambios_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			//cuando se selecciona un registro tendriamos que activar un boton u otro
			//dependiendo del registro seleccionado

			try
			{

				SprCambios.Row = Row;
				//se se selecciona el registro de cabecera o no se selecciona ninguno
				if (SprCambios.Row <= 0)
				{
					return;
				}

				if (lRegistroSeleccionado == Row)
				{
					//si el seleccionado coincide con el seleccionado anterior
					if (fFilaSeleccionada)
					{
                        //SprCambios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly; //Se desmarca la fila (Por la Banda)
                        //SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                        fFilaSeleccionada = false;
					}
					else
					{
                        //SprCambios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; //Se marca la fila
                        //SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;  //Se desmarca la fila (Por la Banda)

                        fFilaSeleccionada = true;
					}
				}
				else
				{
					//SprCambios.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; //Se marca la fila
                   // SprCambios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect; //Se marca la fila
                    fFilaSeleccionada = true;
				}

				if (fFilaSeleccionada)
				{
					lRegistroSeleccionado = Row;
					//Se cargan los datos del registro seleccionado en la pantalla
					//se pone la fecha del registro seleccionado
					SprCambios.Row = lRegistroSeleccionado;
					SprCambios.Col = 4;
					SdcFecha.Text = DateTime.Parse(SprCambios.Text).ToString("dd/MM/yyyy");
					tbHoraCambio.Text = DateTime.Parse(SprCambios.Text).ToString("HH:mm");
					//se cargan los datos del seleccionado

					SprCambios.Col = 5; //servicio de origen
					viServicioOrigen = Convert.ToInt32(Double.Parse(SprCambios.Text.Trim()));
					//viServiciDe = SprCambios.Text
					BuscarServicio(viServicioOrigen);
					//BuscarServicio viServiciDestino

					SprCambios.Col = 6; //medico de origen
					viMedicoOrigen = Convert.ToInt32(Double.Parse(SprCambios.Text.Trim()));
					//viCodMedicoDes = SprCambios.Text
					BuscarMedico(viMedicoOrigen);
					//BuscarMedico viCodMedico

					//se protegen los campos de la pantalla
					Frame3.Enabled = false;
					Label21.Enabled = false;
					SdcFecha.Enabled = false;
					tbHoraCambio.Enabled = false;
					Label19.Enabled = false;

					Frame1.Enabled = false;
					Frame4.Enabled = false;

					CbCambio.Enabled = true;
					cbCancelar.Enabled = true;
				}
				else
				{
					//se trata de la cabecera
					lRegistroSeleccionado = 0;
					//hay que limpiar los campos de la pantalla
					//IniciarPantalla
					proInicDetalle();
					CbCambio.Enabled = false;
					cbCancelar.Enabled = false;
				}
			}
			catch (Exception Excep)
			{

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_sermed_alta:SprCambios_click", Excep);
			}
		}

		private void BuscarMedico(int lMedico)
		{

			string stSqServicio = "SELECT DNOMPERS,DAP1PERS,DAP2PERS " + " FROM DPERSONAVA " + " WHERE " + " dpersonava.gpersona = " + lMedico.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqServicio, bCambioServicio.RcAdmision);
			DataSet RrServicio = new DataSet();
			tempAdapter.Fill(RrServicio);

			//Compruebo que hay un medico
			//se llena el combo de medicos con los asociados a ese servicio
			if (RrServicio.Tables[0].Rows.Count == 1)
			{
				TbMedicoDe.Text = Convert.ToString(RrServicio.Tables[0].Rows[0]["DAP1PERS"]).Trim() + " " + Convert.ToString(RrServicio.Tables[0].Rows[0]["DAP2PERS"]).Trim() + "," + Convert.ToString(RrServicio.Tables[0].Rows[0]["DNOMPERS"]).Trim();
			}

            CbbMedicoA.Items.Clear();

            stSqServicio = "SELECT DNOMPERS,DAP1PERS,DAP2PERS,DPERSONAva.GPERSONA " + " FROM DPERSONAva,DPERSERV " + " WHERE GSERVICI=" + viServicioOrigen.ToString() + " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA " + " and dpersonava.gpersona <>" + lMedico.ToString();

			//oscar 25/11/2003
			stSqServicio = stSqServicio + " AND DPERSONAVA.IMEDICO='S' ";
			//------

			stSqServicio = stSqServicio + " order by dap1pers,dap2pers,dnompers";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqServicio, bCambioServicio.RcAdmision);
			RrServicio = new DataSet();
			tempAdapter_2.Fill(RrServicio);
			foreach (DataRow iteration_row in RrServicio.Tables[0].Rows)
			{
                CbbMedicoA.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["DAP1PERS"]).Trim() + " " + Convert.ToString(iteration_row["DAP2PERS"]).Trim() + " " + Convert.ToString(iteration_row["DNOMPERS"]).Trim(), Convert.ToString(iteration_row["GPERSONA"]).Trim()));
        	}
			RrServicio.Close();

		}

		private void SprEpisodios_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;

			try
			{

				SprEpisodios.Row = Row;
				//se se selecciona el registro de cabecera o no se selecciona ninguno
				if (SprEpisodios.Row <= 0)
				{
					return;
				}
				else
				{
					// si ha seleccionado una fila se rellena el spread de cambios con
					// los cambios de servicio/medico para el episodio seleccionado
					proRellena_Grid2();
					proInicDetalle();
				}
			}
			catch (Exception Excep)
            {

				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), bCambioServicio.VstCodUsua, "Cambio_sermed_alta:sprEpisodios_click", Excep);
			}
		}
		private void ActualizarAMOVIMIE(string stCondicion, string stDATOS, int iServicio = 0, int lMedico = 0)
		{
			//Actualiza AMOVIMIE los campos de origen o destino

			string sql = "select * from AMOVIMIE where itiposer = 'H' and " + " ganoregi=" + iA�oRegistro.ToString() + " and gnumregi=" + iNumeroRegistro.ToString() + " " + " and " + stCondicion;

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, bCambioServicio.RcAdmision);
			DataSet RrActualizar = new DataSet();
			tempAdapter.Fill(RrActualizar);

			if (RrActualizar.Tables[0].Rows.Count != 0)
			{
				RrActualizar.MoveFirst();
				foreach (DataRow iteration_row in RrActualizar.Tables[0].Rows)
				{
					RrActualizar.Edit();
					switch(stDATOS)
					{
						case "ORIGEN" : 
							iteration_row["gservior"] = iServicio; 
							iteration_row["gmedicor"] = lMedico; 
							iteration_row["gusuario"] = bCambioServicio.VstCodUsua; 
							 
							break;
						case "DESTINO" : 
							iteration_row["gservide"] = iServicio; 
							iteration_row["gmedicde"] = lMedico; 
							break;
					}
					string tempQuery = RrActualizar.Tables[0].TableName;
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrActualizar, tempQuery);
				}
			}
			RrActualizar.Close();

		}
		private void CAMBIO_SERMED_ALTA_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}
	}
}