using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace CambioServicio
{
	public class clsCambioServicio
	{


		public void proRecPacienteIngreso(SqlConnection ParaConexion, object ParaListado, object ParaGidenpac, string ParaNombPaciente, string paraUsuario, string paraDsnCrystal, string paraUsuarioCrystal, string ParaContraseñaCrystal)
		{

			bCambioServicio.RcAdmision = ParaConexion;
			bCambioServicio.CrystalReport1 = ParaListado;
			Serrores.AjustarRelojCliente(bCambioServicio.RcAdmision);

			Serrores.Obtener_TipoBD_FormatoFechaBD(ref bCambioServicio.RcAdmision);

			bCambioServicio.PathString = Path.GetDirectoryName(Application.ExecutablePath);
			bCambioServicio.VstCodUsua = paraUsuario;

			bCambioServicio.VstCrystal = paraDsnCrystal;
			bCambioServicio.gUsuario = paraUsuarioCrystal;
			bCambioServicio.gContraseña = ParaContraseñaCrystal;

			Cambio_se_me_en.DefInstance.Tag = Convert.ToString(ParaGidenpac);
			Cambio_se_me_en.DefInstance.proRecPaciente(Convert.ToString(ParaGidenpac), ParaNombPaciente);
			Cambio_se_me_en.DefInstance.ShowDialog();

		}

		public void proRecPacienteAlta(SqlConnection ParaConexion, object ParaGidenpac, string ParaNombPaciente, string paraUsuario)
		{

			bCambioServicio.RcAdmision = ParaConexion;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref bCambioServicio.RcAdmision);
			Serrores.AjustarRelojCliente(bCambioServicio.RcAdmision);
			bCambioServicio.VstCodUsua = paraUsuario;

			CAMBIO_SERMED_ALTA.DefInstance.lbPaciente.Text = ParaNombPaciente;
			CAMBIO_SERMED_ALTA.DefInstance.Tag = Convert.ToString(ParaGidenpac);
			CAMBIO_SERMED_ALTA.DefInstance.ShowDialog();

		}

		public clsCambioServicio()
		{
			bCambioServicio.objError = new Mensajes.ClassMensajes();
		}

		~clsCambioServicio()
		{
			bCambioServicio.objError = null;
		}
	}
}