using System;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace INCAPACIDAD
{
	internal static class MINCAPACIDAD
	{

		public struct datospaciente
		{
			public string paciente;
			public string direccion;
			public string tDNIcedula;
			public string DNIcedula;
			public string nafiliac;
			public string stfealta;
			public string sthoalta;
			public string ndiasinc;
			public string nhorainc;
			public string stncoleg;
			public string stmedico;
			public static datospaciente CreateInstance()
			{
					datospaciente result = new datospaciente();
					result.paciente = String.Empty;
					result.direccion = String.Empty;
					result.tDNIcedula = String.Empty;
					result.DNIcedula = String.Empty;
					result.nafiliac = String.Empty;
					result.stfealta = String.Empty;
					result.sthoalta = String.Empty;
					result.ndiasinc = String.Empty;
					result.nhorainc = String.Empty;
					result.stncoleg = String.Empty;
					result.stmedico = String.Empty;
					return result;
			}
		}
		public static string STCODSS = String.Empty;
	}
}