using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace INCAPACIDAD
{
    public class MCINCAPACIDAD
    {

        SqlConnection rc = null;
        string GRUPO_HOSPITAL = String.Empty; //Nombre y direcci�n Hospital
        string NOMBRE_HOSPITAL = String.Empty;
        string DIRECCION_HOSPITAL = String.Empty;
        object oLISTADO_GENERAL = null;
        string stNOMBRE_DSN_SQLSERVER = String.Empty;
        string stGUsuario_Crystal = String.Empty;
        string stGPassword_Crystal = String.Empty;
        string VSTPATHINFORMES = String.Empty;
        object oVentanaImpresion = null;
        int lNumeroCopias = 0;
        private MINCAPACIDAD.datospaciente paciente = MINCAPACIDAD.datospaciente.CreateInstance();
        string gUsuario_C = String.Empty;
        string VstCrystal_C = String.Empty;
        string gContrase�a_C = String.Empty;
        //oscar
        string NroDias = String.Empty;
        string lItiposer = String.Empty;
        object Cuadro_Diag_impre = null;
        //----


        public void datos(string itiposer, int ganoregi, int gnumregi, object LISTADO_GENERAL, string PATHINFORMES, SqlConnection Conexion, int NumeroCopias, string VstCrystal, string gUsuario, string gContrase�a, object PCuadro_Diag_impre)
        {
            //"H", paciente.a_urgencia, paciente.n_urgencia, oLISTADO_GENERAL, VSTPATHINFORMES, Rc
            string sql = String.Empty;
            rc = Conexion;
            string stGidenpac = String.Empty;

            oLISTADO_GENERAL = LISTADO_GENERAL;
            //oscar
            Cuadro_Diag_impre = PCuadro_Diag_impre;
            //-----
            VSTPATHINFORMES = PATHINFORMES;
            lNumeroCopias = NumeroCopias;
            VstCrystal_C = VstCrystal;
            gUsuario_C = gUsuario;
            gContrase�a_C = gContrase�a;

            //oscar
            lItiposer = itiposer.Trim().ToUpper();
            //-----

            //obtenemos los datos del paciente
            SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT * FROM SCONSGLO WHERE GCONSGLO ='IDENPERS'", rc);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);
            paciente.tDNIcedula = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim();
            RrSql.Close();
            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter("SELECT nnumeri1 FROM SCONSGLO WHERE GCONSGLO ='SEGURSOC'", rc);
            RrSql = new DataSet();
            tempAdapter_2.Fill(RrSql);
            MINCAPACIDAD.STCODSS = Convert.ToString(RrSql.Tables[0].Rows[0]["nnumeri1"]).Trim();
            RrSql.Close();

            switch (itiposer.Trim().ToUpper())
            {
                case "U":
                    sql = "select gidenpac from uepisurg where ganourge = " + ganoregi.ToString() + " and gnumurge = " + gnumregi.ToString() + " ";
                    break;
                case "C":
                    sql = "select gidenpac from cconsult where ganoregi = " + ganoregi.ToString() + " and gnumregi = " + gnumregi.ToString() + " ";
                    break;
                case "H":
                    sql = "select gidenpac from aepisadm where ganoadme = " + ganoregi.ToString() + " and gnumadme = " + gnumregi.ToString() + " ";
                    break;
            }
            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, rc);
            RrSql = new DataSet();
            tempAdapter_3.Fill(RrSql);
            if (RrSql.Tables[0].Rows.Count != 0)
            {
                stGidenpac = Convert.ToString(RrSql.Tables[0].Rows[0]["gidenpac"]);
            }
            RrSql.Close();
            Serrores.Obtener_Multilenguaje(rc, stGidenpac);
            Serrores.ObtenerTextosRPT(rc, "INCAPACIDAD", "SMT230R1");

            rellenar_constantes();
            switch (itiposer.Trim().ToUpper())
            {
                case "U":
                    sql = "select dpacient.gidenpac, dpacient.dnombpac, dpacient.dape1pac, dpacient.dape2pac, " +
                          " dpacient.gpoblaci, dpacient.dpoblaci POBLACION, dpacient.ddirepac, dpacient.ndninifp, " +
                          " uepisurg.faltaurg, uepisurg.ndiasinc, uepisurg.nhorainc, " +
                          " dpersona.ncolegia, dpersona.dnompers, dpersona.dap1pers, dpersona.dap2pers, dpoblaci.dpoblaci, " +
                          " dpoblaci.dpoblacii1,dpoblaci.dpoblacii2,dpaisres.dpaisres,dpaisres.dpaisresi1,dpaisres.dpaisresi2 " +
                          "        From " +
                          " uepisurg " +
                          " inner join dpacient on uepisurg.gidenpac =  dpacient.gidenpac " +
                          " left  join dpersona on uepisurg.gpersona = dpersona.gpersona " +
                          " left  join dpoblaci on dpacient.gpoblaci = dpoblaci.gpoblaci " +
                          " left  join dpaisres on dpacient.gpaisres = dpaisres.gpaisres " +
                          " where ganourge = " + ganoregi.ToString() + " and  " +
                          " gnumurge = " + gnumregi.ToString() + " ";
                    break;
                case "C":
                    sql = "select dpacient.gidenpac, dpacient.dnombpac, dpacient.dape1pac, dpacient.dape2pac, " +
                          " dpacient.gpoblaci, dpacient.dpoblaci POBLACION, dpacient.ddirepac, dpacient.ndninifp, " +
                          " cconsult.ffeccita as faltaurg, cconsult.ndiasinc, " +
                          " cconsult.nhorainc, DPERSONA.ncolegia, dpersona.dnompers, dpersona.dap1pers, dpersona.dap2pers, " +
                          " dpoblaci.dpoblaci,dpoblaci.dpoblacii1,dpoblaci.dpoblacii2,dpaisres.dpaisres,dpaisres.dpaisresi1,dpaisres.dpaisresi2 " +
                          " From " +
                          " cconsult " +
                          " inner join dpacient on cconsult.gidenpac =  dpacient.gidenpac " +
                          " left  join dpersona on cconsult.gpersona = dpersona.gpersona " +
                          " left  join dpoblaci on dpacient.gpoblaci = dpoblaci.gpoblaci " +
                          " left  join dpaisres on dpacient.gpaisres = dpaisres.gpaisres " +
                          " where ganoregi = " + ganoregi.ToString() + " and " +
                          " gnumregi = " + gnumregi.ToString() + " ";
                    break;
                case "H":
                    sql = "select " +
                          " datediff(day, aepisadm.fllegada, aepisadm.faltplan)+1 as Ndias," +
                          " dpacient.gidenpac, dpacient.dnombpac, dpacient.dape1pac, dpacient.dape2pac, " +
                          " dpacient.gpoblaci, dpacient.dpoblaci POBLACION, dpacient.ddirepac, dpacient.ndninifp, " +
                          " aepisadm.faltplan as faltaurg, aepisadm.diasabse as ndiasinc, " +
                          " aepisadm.horaabse as nhorainc, DPERSONA.ncolegia, dpersona.dnompers, dpersona.dap1pers, dpersona.dap2pers, " +
                          " dpoblaci.dpoblaci,dpoblaci.dpoblacii1,dpoblaci.dpoblacii2,dpaisres.dpaisres,dpaisres.dpaisresi1,dpaisres.dpaisresi2 " +
                          " From " +
                          " aepisadm " +
                          " inner join dpacient on aepisadm.gidenpac =  dpacient.gidenpac " +
                          " left  join dpersona on aepisadm.gpersona = dpersona.gpersona " +
                          " left  join dpoblaci on dpacient.gpoblaci = dpoblaci.gpoblaci " +
                          " left  join dpaisres on dpacient.gpaisres = dpaisres.gpaisres " +
                          " where ganoadme = " + ganoregi.ToString() + " and " +
                          " gnumadme = " + gnumregi.ToString() + " ";

                    break;
            }
            SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, rc);
            RrSql = new DataSet();
            tempAdapter_4.Fill(RrSql);
            string PaisResi = String.Empty;
            if (RrSql.Tables[0].Rows.Count != 0)
            {

                //oscar
                if (lItiposer == "H")
                {
                    if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["Ndias"]))
                    {
                        //           NroDias = "ha permanecido ingresado en este centro " & CStr(Rrsql("Ndias")) & " d�as,"
                        NroDias = Serrores.VarRPT[1] + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["Ndias"]) + " " + Serrores.VarRPT[2] + ",";
                    }
                    else
                    {
                        NroDias = "";
                    }
                }
                //-----

                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnombpac"]))
                {
                    paciente.paciente = Convert.ToString(RrSql.Tables[0].Rows[0]["dnombpac"]).Trim().ToUpper();
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape1pac"]))
                {
                    paciente.paciente = paciente.paciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape1pac"]).Trim().ToUpper();
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape2pac"]))
                {
                    paciente.paciente = paciente.paciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape2pac"]).Trim().ToUpper();
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ndninifp"]))
                {
                    paciente.DNIcedula = Convert.ToString(RrSql.Tables[0].Rows[0]["ndninifp"]).Trim().ToUpper();
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ddirepac"]))
                {
                    paciente.direccion = Convert.ToString(RrSql.Tables[0].Rows[0]["ddirepac"]).Trim().ToUpper();
                }
                //''    If Not IsNull(rrsql("dpoblaci")) Then paciente.direccion = paciente.direccion & " (" & Trim(UCase(rrsql("dpoblaci"))) & ")"
                switch (Serrores.CampoIdioma)
                {
                    case "IDIOMA0":
                        PaisResi = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dpaisres"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dpaisres"]).Trim();
                        break;
                    case "IDIOMA1":
                        PaisResi = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dpaisresi1"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dpaisresi1"]).Trim();
                        break;
                    case "IDIOMA2":
                        PaisResi = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dpaisresi2"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dpaisresi2"]).Trim();
                        break;
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gpoblaci"]))
                {
                    paciente.direccion = paciente.direccion + " (" + BuscarPoblacion(Convert.ToString(RrSql.Tables[0].Rows[0]["Gidenpac"])) + ")";
                }
                else
                {
                    if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["POBLACION"]))
                    {
                        paciente.direccion = paciente.direccion + " (" + Convert.ToString(RrSql.Tables[0].Rows[0]["POBLACION"]).Trim().ToUpper() + ")" + " - " + PaisResi;
                    }
                    else
                    {
                        if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dpaisres"]) && paciente.direccion != "")
                        {
                            paciente.direccion = paciente.direccion + " - " + PaisResi;
                        }
                        else
                        {
                            if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dpaisres"]))
                            {
                                paciente.direccion = PaisResi;
                            }
                            else
                            {
                                paciente.direccion = paciente.direccion;

                            }
                        }
                    }
                }
                //    If Not IsNull(rrsql("dpoblaci")) Then paciente.direccion = paciente.direccion & " (" & BuscarPoblacion(rrsql("Gidenpac")) & ")"
                paciente.nafiliac = ObtenerNSS(Convert.ToString(RrSql.Tables[0].Rows[0]["gidenpac"]));
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["faltaurg"]))
                {
                    paciente.stfealta = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["faltaurg"]).ToString("dd/MM/yyyy");
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["faltaurg"]))
                {
                    paciente.sthoalta = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["faltaurg"]).ToString("HH:mm");
                }
                //    If Not IsNull(Rrsql("ndiasinc")) Then paciente.ndiasinc = Rrsql("ndiasinc") & " d�as "
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ndiasinc"]))
                {
                    paciente.ndiasinc = Convert.ToString(RrSql.Tables[0].Rows[0]["ndiasinc"]) + " " + Serrores.VarRPT[2];
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["nhorainc"]))
                {
                    //        If Not IsNull(Rrsql("ndiasinc")) Then paciente.ndiasinc = paciente.ndiasinc & " y "
                    //        paciente.ndiasinc = paciente.ndiasinc & Rrsql("nhorainc") & " horas"
                    if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ndiasinc"]))
                    {
                        paciente.ndiasinc = paciente.ndiasinc + " " + Serrores.VarRPT[3];
                    }
                    paciente.ndiasinc = paciente.ndiasinc + Convert.ToString(RrSql.Tables[0].Rows[0]["nhorainc"]) + " " + Serrores.VarRPT[4];
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnompers"]))
                {
                    paciente.stmedico = Convert.ToString(RrSql.Tables[0].Rows[0]["dnompers"]).Trim().ToUpper();
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dap1pers"]))
                {
                    paciente.stmedico = paciente.stmedico + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dap1pers"]).Trim().ToUpper();
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dap2pers"]))
                {
                    paciente.stmedico = paciente.stmedico + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dap2pers"]).Trim().ToUpper();
                }
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ncolegia"]))
                {
                    paciente.stncoleg = Convert.ToString(RrSql.Tables[0].Rows[0]["ncolegia"]);
                }
            }
            RrSql.Close();
            ImprimirInforme();
        }
        private void ImprimirInforme()
        {
            /*INDRA JPROCHE TODO_X_4 Reportes 03/05/2016
            //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["GRUPO_HOSPITAL"] = "\"" + GRUPO_HOSPITAL + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["NOMBRE_HOSPITAL"] = "\"" + NOMBRE_HOSPITAL + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["DIRECCION_HOSPITAL"] = "\"" + DIRECCION_HOSPITAL + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["paciente"] = "\"" + paciente.paciente + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["direccion"] = "\"" + paciente.direccion + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["DNIcedula"] = "\"" + paciente.DNIcedula + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["nafiliac"] = "\"" + paciente.nafiliac + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["stfealta"] = "\"" + paciente.stfealta + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["sthoalta"] = "\"" + paciente.sthoalta + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["ndiasinc"] = "\"" + paciente.ndiasinc + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["nhorainc"] = "\"" + paciente.nhorainc + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["stmedico"] = "\"" + paciente.stmedico + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["stColegiado"] = "\"" + paciente.stncoleg + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["tDNIcedula"] = "\"" + paciente.tDNIcedula + "\"";



			//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.ReportFileName = VSTPATHINFORMES + "\\smt230r1.RPT";


			//oscar
			if (lItiposer == "C")
			{
				//        oLISTADO_GENERAL.Formulas(18) = "Accion='atendido'"
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				oLISTADO_GENERAL.Formulas["Accion"] = "\"" + Serrores.VarRPT[5] + "\"";
			}
			else
			{
				//        oLISTADO_GENERAL.Formulas(18) = "Accion='dado de alta'"
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				oLISTADO_GENERAL.Formulas["Accion"] = "\"" + Serrores.VarRPT[6] + "\"";
			}
			//------

			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Formulas["LogoIDC"] = "\"" + Serrores.ExisteLogo(rc) + "\"";
			//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.SubreportToChange = "LogotipoIDC";
			//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Connect = "DSN=" + VstCrystal_C + ";UID=" + gUsuario_C + ";PWD=" + gContrase�a_C + "";
			//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.SubreportToChange = "";

			//UPGRADE_TODO: (1067) Member SQLQUery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.SQLQUery = "select * from SLOGODOC";
			//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.Destination = 1; //crptToPrinter
			//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			oLISTADO_GENERAL.WindowState = 2; //crptMaximized

			//oscar
			//UPGRADE_TODO: (1067) Member CancelError is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Cuadro_Diag_impre.CancelError = true;
			//UPGRADE_TODO: (1067) Member Copies is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Cuadro_Diag_impre.Copies = 1;
			//UPGRADE_TODO: (1067) Member ShowPrinter is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			try
			{
				Cuadro_Diag_impre.ShowPrinter();
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			}
			catch
			{
			}
			if (ErrorHandlingHelper.ResumeNextExpr<bool>(() => {return Information.Err().Number == 32755;}))
				{
					try
					{ //Se ha pulsado cancelar
						return;
					}
					catch
					{
					}
				}
				ErrorHandlingHelper.ResumeNext(
						//UPGRADE_TODO: (1067) Member CopiesToPrinter is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						//UPGRADE_TODO: (1067) Member Copies is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					() => {oLISTADO_GENERAL.CopiesToPrinter = Cuadro_Diag_impre.Copies;}, 
						//--------
					() => {Serrores.LLenarFormulas(rc, oLISTADO_GENERAL, "SMT230R1", "SMT230R1");}, 
						//oLISTADO_GENERAL.CopiesToPrinter = lNumeroCopias
						//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					() => {oLISTADO_GENERAL.Action = 1;}, 
						//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					() => {oLISTADO_GENERAL.PageCount();}, 
						//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					() => {oLISTADO_GENERAL.PrinterStopPage = oLISTADO_GENERAL.PageCount;}, 
						//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					() => {oLISTADO_GENERAL.Reset();}, 
						//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					() => {oLISTADO_GENERAL.PrinterStopPage = 0;}, 
						//UPGRADE_TODO: (1067) Member CopiesToPrinter is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					() => {oLISTADO_GENERAL.CopiesToPrinter = 1;}, 
					() => {proFormulas(25);});
				oLISTADO_GENERAL = null;
                */
        }
        private void rellenar_constantes()
        {
            string sqlconstantes = "select valfanu1 from sconsglo where gconsglo = 'GrupoHo'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlconstantes, rc);
            DataSet RR = new DataSet();
            tempAdapter.Fill(RR);
            GRUPO_HOSPITAL = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim();
            RR.Close();
            sqlconstantes = "select valfanu1 from sconsglo where gconsglo = 'NOMBREHO'";
            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlconstantes, rc);
            RR = new DataSet();
            tempAdapter_2.Fill(RR);
            NOMBRE_HOSPITAL = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim();
            RR.Close();
            sqlconstantes = "select valfanu1 from sconsglo where gconsglo = 'direccho'";
            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlconstantes, rc);
            RR = new DataSet();
            tempAdapter_3.Fill(RR);
            DIRECCION_HOSPITAL = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim();
            RR.Close();
        }
        private void proFormulas(int idForm)
        {
            /*INDRA JPROCHE TODO_X_4 Reportes 03/05/2016
            for (int tiForm = 0; tiForm <= idForm; tiForm++)
            {
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                oLISTADO_GENERAL.Formulas[tiForm] = "";
            }
            */
        }
        private string ObtenerNSS(string stGidenpac)
        {
            string result = String.Empty;
            result = "";
            string sqlSS = "select nafiliac from dentipac where gidenpac = '" + stGidenpac + "' and gsocieda = " + MINCAPACIDAD.STCODSS + "";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlSS, rc);
            DataSet RrSS = new DataSet();
            tempAdapter.Fill(RrSS);
            if (RrSS.Tables[0].Rows.Count != 0)
            {
                result = (Convert.IsDBNull(RrSS.Tables[0].Rows[0]["nafiliac"])) ? "" : Convert.ToString(RrSS.Tables[0].Rows[0]["nafiliac"]).Trim();
            }
            RrSS.Close();
            return result;
        }

        public string BuscarPoblacion(string gidenpac)
        {
            string result = String.Empty;
            string sqlPro = String.Empty;
            DataSet RrPro = null;
            string CodPobla = String.Empty;
            result = "";

            string sqlPob = "select DPOBLACI.dpoblaci,DPOBLACI.dpoblacii1,DPOBLACI.dpoblacii2,DPOBLACI.gpoblaci from dpacient left join dpoblaci on dpacient.gpoblaci = dpoblaci.gpoblaci where dpacient.gidenpac ='" + gidenpac + "'";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlPob, rc);
            DataSet RrPob = new DataSet();
            tempAdapter.Fill(RrPob);
            if (RrPob.Tables[0].Rows.Count != 0)
            {
                if (!Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaci"]))
                {
                    switch (Serrores.CampoIdioma)
                    {
                        case "IDIOMA0":
                            result = (Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaci"])) ? "" : Convert.ToString(RrPob.Tables[0].Rows[0]["dpoblaci"]).Trim();
                            break;
                        case "IDIOMA1":
                            result = (Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaciI1"])) ? "" : Convert.ToString(RrPob.Tables[0].Rows[0]["dpoblaciI1"]).Trim();
                            break;
                        case "IDIOMA2":
                            result = (Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaciI2"])) ? "" : Convert.ToString(RrPob.Tables[0].Rows[0]["dpoblaciI2"]).Trim();
                            break;
                    }
                    //        BuscarPoblacion = Trim(RrPob("dpoblaci"))
                    CodPobla = Convert.ToString(RrPob.Tables[0].Rows[0]["gpoblaci"]);
                    sqlPro = "select * from DPROVINC where gprovinc = '" + CodPobla.Substring(0, Math.Min(2, CodPobla.Length)) + "' ";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlPro, rc);
                    RrPro = new DataSet();
                    tempAdapter_2.Fill(RrPro);
                    if (RrPro.Tables[0].Rows.Count != 0)
                    {
                        switch (Serrores.CampoIdioma)
                        {
                            case "IDIOMA0":
                                result = result + " - " + ((Convert.IsDBNull(RrPro.Tables[0].Rows[0]["dnomprov"])) ? "" : Convert.ToString(RrPro.Tables[0].Rows[0]["dnomprov"]).Trim());
                                break;
                            case "IDIOMA1":
                                result = result + " - " + ((Convert.IsDBNull(RrPro.Tables[0].Rows[0]["dnomprovI1"])) ? "" : Convert.ToString(RrPro.Tables[0].Rows[0]["dnomprovI1"]).Trim());
                                break;
                            case "IDIOMA2":
                                result = result + " - " + ((Convert.IsDBNull(RrPro.Tables[0].Rows[0]["dnomprovI2"])) ? "" : Convert.ToString(RrPro.Tables[0].Rows[0]["dnomprovI2"]).Trim());
                                break;
                        }
                        //            BuscarPoblacion = BuscarPoblacion & " - " & Trim(RrPro("dnomprov"))
                    }
                    RrPro.Close();
                }
            }
            RrPob.Close();
            return result;
        }
    }
}