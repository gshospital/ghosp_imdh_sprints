using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace Anulacion
{
	public partial class anu_alta
		: RadForm
	{

		string stIdpaciente = String.Empty;
		string stcadena_temp = String.Empty;
		string stNanoadms = String.Empty;
		string stNnumadms = String.Empty;
		string stGanoadme = String.Empty;
		string stGnumadme = String.Empty;
		string stFfinmovi = String.Empty;
		string stFmovimie = String.Empty;
		string stFingplan = String.Empty;
		string stItipsexo = String.Empty;
		string stIndicador = String.Empty;
		string stServicio = String.Empty;
		int iGservici = 0;
		int iGpersona = 0;
		string VstCodUsua = String.Empty;
		SqlConnection RcAdmision = null;
		DataSet RrAEPISADM = null;
		DataSet RrUEPISURG = null;
		DataSet RrAMOVIMIE = null;
		DataSet RrDCAMASBO = null;
		DataSet RrDPACIENT = null;
		DataSet RrDEPIANUL = null;
		DataSet RrAMOVIFIN = null;
		DataSet RrTemp = null;
		string StSql = String.Empty;
		string ttemp = String.Empty;
		//Dim stFecha_Llegada As String
		dynamic FrFormulario = null;
		string VstArea = String.Empty;
		string VstTag = String.Empty;
		string VstSociedad = String.Empty;
		string VstInspecc = String.Empty;

		bool bErroProcesoComprobar = false;
		string stDescripcionProcesoComprobar = String.Empty;
		bool ResumenCMDB = false;
        SqlDataAdapter tempAdapter_AMOVIMIE = null;

        //oscar 30/04/04
        bool bGestionProd = false;


		//Procedimiento que recoge las incidencias de la Dll de ProcesoComprobar.
		public anu_alta()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		//Hay que definir bErroAsociarProcesor y stDescripcionAsociarProceso en el general.
		public void DevolverAsociarProceso(string stError, string stDescripcionError)
		{
			if (stError.Trim() != "")
			{
				bErroProcesoComprobar = true;
				stDescripcionProcesoComprobar = stDescripcionError;
			}
		}


		public bool fnValida_factu(string ParaPaciente)
		{
			bool etinespisneo = false;
			bool etiqueta = false;
			//comprobamos facturaci�n
			string tstFact = String.Empty;
			DataSet tRrfact = null;
			DataSet tRrfact2 = null;
			string tstFaltplan = String.Empty;
			string sql = String.Empty;
			DataSet RrTemp = null;
			bool bVuelta = false;
			bool ErrorTabla = false;
            SqlDataAdapter tempAdapter_4 = null;
            SqlDataAdapter adapterDelete = null;
            try
            {
                etiqueta = true;
                etinespisneo = false;
                bVuelta = true;
                //obtengo el episodio
                tstFact = "select faltplan,ganoadme,gnumadme from aepisadm " + " where gidenpac='" + ParaPaciente + "' and " + " faltplan is not null and" + " faltplan=(select max(faltplan) from aepisadm " + " where gidenpac='" + ParaPaciente + "')";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(tstFact, RcAdmision);
                tRrfact = new DataSet();
                tempAdapter.Fill(tRrfact);
                if (tRrfact.Tables[0].Rows.Count == 0)
                {
                    tRrfact.Close();
                    short tempRefParam = 1520;
                    string[] tempRefParam2 = new string[] { "el alta", "este paciente" };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcAdmision, tempRefParam2));
                    bVuelta = false;
                }
                else
                {
                    //verificar si tiene apuntes pasados a facturaci�n en nepiseno
                    stGanoadme = Convert.ToString(tRrfact.Tables[0].Rows[0]["ganoadme"]);
                    stGnumadme = Convert.ToString(tRrfact.Tables[0].Rows[0]["gnumadme"]);
                    tstFaltplan = Convert.ToString(tRrfact.Tables[0].Rows[0]["Faltplan"]);
                    tRrfact.Close();
                    ErrorTabla = false;
                    etinespisneo = true;
                    etiqueta = false;
                    try
                    {
                        sql = "select * from nepisneo where ganoadme=" + stGanoadme + " and gnumadme=" + stGnumadme + " and fpasfact is not null";
                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RcAdmision);
                        RrTemp = new DataSet();
                        tempAdapter_2.Fill(RrTemp);
                    }
                    catch
                    {
                        ErrorTabla = true;
                    }
                    if (!ErrorTabla)
                    {
                        if (RrTemp.Tables[0].Rows.Count != 0)
                        {
                            RadMessageBox.Show("Existen apuntes de Nido facturados. Imposible anular el alta.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
                            bVuelta = false;
                            return false;
                        }
                        RrTemp.Close();
                    }
                    etiqueta = true;
                    etinespisneo = false;

                    //busco en dmovfact
                    System.DateTime TempDate2 = DateTime.FromOADate(0);
                    System.DateTime TempDate = DateTime.FromOADate(0);
                    object tempRefParam3 = ((DateTime.TryParse(tstFaltplan, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : tstFaltplan) + ":00";
                    object tempRefParam4 = ((DateTime.TryParse(tstFaltplan, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm") : tstFaltplan) + ":59";
                    tstFact = "select * from dmovfact " + " where ganoregi=" + stGanoadme + " and " + " gnumregi=" + stGnumadme + " and " + " fperfact >=" + Serrores.FormatFechaHMS(tempRefParam3) + " and fperfact <= " + Serrores.FormatFechaHMS(tempRefParam4) + " and itiposer= 'H' ";

                    if (Serrores.proExisteTabla("DMOVFACH", RcAdmision))
                    {
                        string tempRefParam5 = "DMOVFACT";
                        tstFact = tstFact + " UNION ALL " + Serrores.proReplace(ref tstFact, tempRefParam5, "DMOVFACH");
                    }

                    tempAdapter_4 = new SqlDataAdapter(tstFact, RcAdmision);
                    tRrfact = new DataSet();
                    tempAdapter_4.Fill(tRrfact);
                    //    tRrfact.MoveLast
                    // TIENE APUNTES PASADOS A FACTURACION AL ALTA
                    if (tRrfact.Tables[0].Rows.Count != 0)
                    {
                        //comprobar si todos los apuntes estan facturados
                        tRrfact.MoveFirst();
                        foreach (DataRow iteration_row in tRrfact.Tables[0].Rows)
                        {
                            if (!Convert.IsDBNull(iteration_row["ffactura"]))
                            {
                                tRrfact.Close();
                                this.Cursor = Cursors.Default;
                                short tempRefParam6 = 1130;
                                string[] tempRefParam7 = new string[] { "anular el alta existen", "apuntes facturados" };
                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam6, RcAdmision, tempRefParam7));
                                bVuelta = false;
                                break;
                            }
                        }

                        if (bVuelta)
                        {
                            // Lo dejamos como estaba para borrar las filas de DMOVFACT

                            System.DateTime TempDate4 = DateTime.FromOADate(0);
                            System.DateTime TempDate3 = DateTime.FromOADate(0);
                            object tempRefParam8 = ((DateTime.TryParse(tstFaltplan, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:mm") : tstFaltplan) + ":00";
                            object tempRefParam9 = ((DateTime.TryParse(tstFaltplan, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:mm") : tstFaltplan) + ":59";
                            tstFact = "select * from dmovfact " + " where ganoregi=" + stGanoadme + " and " + " gnumregi=" + stGnumadme + " and " + " fperfact >=" + Serrores.FormatFechaHMS(tempRefParam8) + " and fperfact <= " + Serrores.FormatFechaHMS(tempRefParam9) + " and itiposer= 'H' ";

                            tempAdapter_4 = new SqlDataAdapter(tstFact, RcAdmision);
                            tRrfact = new DataSet();
                            tempAdapter_4.Fill(tRrfact);
                        }
                    }
                    else
                    {
                        // NO TIENE APUNTES FACTURADOS AL ALTA COMPROBAR QUE ES MENSUAL Y QUE YA SE HA PASADO
                        // LA MENSUAL
                        tstFact = " select * from dmovfact " + " where ganoregi=" + stGanoadme + " and " + " gnumregi=" + stGnumadme + " and itiposer= 'H' and " + " (gcodeven='ES' or gcodeven='AU') and datepart(month,fperfact)=" + DateAndTime.DatePart("m", DateTime.Parse(tstFaltplan), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1).ToString() + " and datepart(year,fperfact)=" + DateAndTime.DatePart("yyyy", DateTime.Parse(tstFaltplan), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1).ToString();

                        if (Serrores.proExisteTabla("DMOVFACH", RcAdmision))
                        {
                            string tempRefParam10 = "DMOVFACT";
                            tstFact = tstFact + " UNION ALL " + Serrores.proReplace(ref tstFact, tempRefParam10, "DMOVFACH");
                        }

                        tempAdapter_4 = new SqlDataAdapter(tstFact, RcAdmision);
                        tRrfact2 = new DataSet();
                        tempAdapter_4.Fill(tRrfact2);

                        //If tRrfact2("FACTURADOS") > 0 Then
                        if (tRrfact2.Tables[0].Rows.Count != 0)
                        {
                            //1200     V1 ya ha sido V2.
                            tRrfact2.Close();
                            this.Cursor = Cursors.Default;
                            short tempRefParam11 = 1200;
                            string[] tempRefParam12 = new string[] { "Operaci�n denegada. El paciente", "facturado mensualmente" };
                            Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, RcAdmision, tempRefParam12);
                            bVuelta = false;
                        }
                    }

                    if (bVuelta)
                    {
                        if (tRrfact.Tables[0].Rows.Count > 0)
                        {
                            tRrfact.MoveFirst();
                        }
                        //borro los registros de dmovfact
                        tRrfact.Delete(tempAdapter_4);
                        //        tRrfact.Update
                        tRrfact.Close();
                        tempAdapter_4.Dispose();
                    }
                }
                return bVuelta;
			}
			catch (Exception excep)
			{
				if (!etinespisneo && !etiqueta)
				{
					throw excep;
				}
				if (etiqueta)
				{
					RadMessageBox.Show("Error comprobando la facturaci�n", Application.ProductName);
					return false;
				}
			}
			return false;
		}


		public void BorrarDocumento(string stDocumento)
		{

			string StSql = "SELECT * FROM document where gnomdocu ='" + stDocumento + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, RcAdmision);
			DataSet RsDoc = new DataSet();
			tempAdapter.Fill(RsDoc);
			if (RsDoc.Tables[0].Rows.Count > 0)
			{
                RsDoc.Delete(tempAdapter);
			}
			RsDoc.Close();
		}

		public bool fnValida_Cama(string ParaAno, string ParaNum)
		{
			string stMensaje1 = String.Empty;
			string SqlT = String.Empty;
			DataSet RrT = null;
			string stFecha = String.Empty;
			string stRegAloj = String.Empty;
			string stTempRes = String.Empty;
			string stTempSex = String.Empty;
			int iresume = 0;
			bool bVuelta = true;
			try
			{
				StSql = " SELECT MAX(fmovimie) as fmovimie " + " FROM AMOVIMIE " + " WHERE " + " itiposer = 'H' AND " + " ganoregi = " + ParaAno + " AND " + " gnumregi = " + ParaNum;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, RcAdmision);
				RrAMOVIMIE = new DataSet();
				tempAdapter.Fill(RrAMOVIMIE);
				//StFecha = RrAMOVIMIE("fmovimie")
				stFecha = Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["fmovimie"]);
				RrAMOVIMIE.Close();
				object tempRefParam = stFecha;
				StSql = " SELECT * FROM AMOVIMIE inner join DCAMASBOVA on gplantor = gplantas AND " + " ghabitor = ghabitac AND gcamaori = gcamasbo  WHERE  itiposer = 'H' AND " + " ganoregi = " + ParaAno + " AND  gnumregi = " + ParaNum + " AND " + " fmovimie = " + Serrores.FormatFechaHMS(tempRefParam) + "";
				stFecha = Convert.ToString(tempRefParam);

                tempAdapter_AMOVIMIE = new SqlDataAdapter(StSql, RcAdmision);
				RrAMOVIMIE = new DataSet();
                tempAdapter_AMOVIMIE.Fill(RrAMOVIMIE);

				string sexo_temp = String.Empty;
				bool pasa_uno = false;
				if (RrAMOVIMIE.Tables[0].Rows.Count == 0)
				{
					short tempRefParam2 = 1100;
                    string[] tempRefParam3 = new string[] { "No se puede anular el alta. Cama"};
					iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam2, RcAdmision, tempRefParam3));
					bVuelta = false;
				}
				else
				{
					iGservici = Convert.ToInt32(RrAMOVIMIE.Tables[0].Rows[0]["gservior"]);
					iGpersona = Convert.ToInt32(RrAMOVIMIE.Tables[0].Rows[0]["gmedicor"]);

					//si hay movimientos comprobamos el estado de la cama
					switch(Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["iestcama"]))
					{
						case "O" : 
							//MsgBox ("La cama que tenia el paciente actualmente est� Ocupada o Bloqueada") 
							stMensaje1 = StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gcamasbo"], "00").Trim() + " Ocupada"; 
							short tempRefParam4 = 1320;
                            string[] tempRefParam5 = new string[] { stMensaje1}; 
							iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, RcAdmision, tempRefParam5)); 
							bVuelta = false; 
							break;
						case "I" : 
							SqlT = "SELECT * FROM SCONSGLO WHERE gconsglo ='INHABALT'"; 
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SqlT, RcAdmision); 
							RrT = new DataSet(); 
							tempAdapter_3.Fill(RrT); 
							if (!Convert.IsDBNull(RrT.Tables[0].Rows[0]["nnumeri1"]))
							{
								if (Conversion.Val(Convert.ToString(RrT.Tables[0].Rows[0]["nnumeri1"])) == Convert.ToDouble(RrAMOVIMIE.Tables[0].Rows[0]["gmotinha"]))
								{
									//dejar la cama libre
									RrAMOVIMIE.Edit();
                                    //RrAMOVIMIE.Tables[0].Rows[0]["iestcama"] = "L";
                                    var sqltemp = "select * from DCAMASBOVA where gplantas = "+ RrAMOVIMIE.Tables[0].Rows[0]["gplantor"].ToString() + " and ghabitac = "+ RrAMOVIMIE.Tables[0].Rows[0]["ghabitor"].ToString() + " and gcamasbo = '" + RrAMOVIMIE.Tables[0].Rows[0]["gcamaori"].ToString()+"'";
                                    var dataAdapter = new SqlDataAdapter(sqltemp, RcAdmision);
                                    var dataSet = new DataSet();
                                    dataAdapter.Fill(dataSet);
                                    dataSet.Tables[0].Rows[0]["iestcama"] = "L";
                                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(dataAdapter);
                                    dataAdapter.Update(dataSet, dataSet.Tables[0].TableName);
									RrT.Close();
									bVuelta = true;
								}
								else
								{
									//MsgBox ("La cama que ten�a el paciente actualmente est� Inhabilitada, debe primero desinhabilitarla")
									stMensaje1 = StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gcamasbo"], "00").Trim() + " Inhabilitada";
									short tempRefParam6 = 1320;
                                    string[] tempRefParam7 = new string[] { stMensaje1};
									iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam6, RcAdmision, tempRefParam7));
									bVuelta = false;
								}
							}
							else
							{
								//MsgBox ("La cama que ten�a el paciente actualmente est� Inhabilitada, debe primero desinhabilitarla")
								stMensaje1 = StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gcamasbo"], "00").Trim() + " Inhabilitada";
								short tempRefParam8 = 1320;
                                string[] tempRefParam9 = new string[] { stMensaje1};
								iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam8, RcAdmision, tempRefParam9));
								bVuelta = false;
							} 
							break;
						case "B" : 
							//MsgBox ("La cama que ten�a el paciente actualmente est� Bloqueada, debe primero desinhabilitarla") 
							stMensaje1 = StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gcamasbo"], "00").Trim() + " Bloqueada"; 
							short tempRefParam10 = 1320;
                            string[] tempRefParam11 = new string[] { stMensaje1}; 
							iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam10, RcAdmision, tempRefParam11)); 
							bVuelta = false; 
							break;
						default:
							bVuelta = true; 
							break;
					}
					//****************
					if (bVuelta)
					{
						//buscar �ltimo regimen de alojamiento para ese episodio
						SqlT = " SELECT iregaloj " + " FROM AMOVREGA " + " WHERE " + " itiposer = 'H' AND " + " ganoregi = " + ParaAno + " AND " + " gnumregi = " + ParaNum + " and " + " fmovimie in (select max(fmovimie) from amovrega where " + " itiposer = 'H' AND " + " ganoregi = " + ParaAno + " AND " + " gnumregi = " + ParaNum + ")";

						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SqlT, RcAdmision);
						RrT = new DataSet();
						tempAdapter_5.Fill(RrT);
						if (RrT.Tables[0].Rows.Count == 0)
						{
							bVuelta = false;
						}
						else
						{

							//valida regimen de alojamiento de la habitaci�n
							stRegAloj = Convert.ToString(RrT.Tables[0].Rows[0]["iregaloj"]); //ULTIMO REGIMEN DE ALOJAMIENTO DEL PACIENTE
							RrT.Close();
							SqlT = "select iregaloj from dcamasbova inner join aepisadm on " + " aepisadm.gidenpac=dcamasbova.gidenpac " + " where  gplantas=" + Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"]) + " and " + " ghabitac=" + Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"]) + " and " + " faltplan is null and (iestcama='O' or iestcama='B')";

							SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SqlT, RcAdmision);
							RrT = new DataSet();
							tempAdapter_6.Fill(RrT);
							// TODOS LOS REGIMEN DE LOS PACIENTES DE LA MISMA HABITACION
							if (RrT.Tables[0].Rows.Count > 0)
							{
								//Comprobamos Restricciones por Alojamiento
								if (stRegAloj == "I")
								{
									short tempRefParam12 = 1650;
                                    string[] tempRefParam13 = new string[] { };
									iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam12, RcAdmision, tempRefParam13));
									if (iresume == 1)
									{
										bVuelta = true;
									}
									else if (iresume == 2)
									{ 
										bVuelta = false;
									}
								}
							}
						}

						RrT.Close();
						if (bVuelta)
						{

							//validar restriccion de sexo
							SqlT = "select itipsexo,irestsex from dcamasbova where " + " gplantas=" + Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"]) + " and " + " ghabitac=" + Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"]);
							SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(SqlT, RcAdmision);
							RrT = new DataSet();
							tempAdapter_7.Fill(RrT);
							if (RrT.Tables[0].Rows.Count != 0)
							{
								RrT.MoveFirst();
								stTempRes = "N";
								stTempSex = "P";
								pasa_uno = false;
								sexo_temp = "P";
								foreach (DataRow iteration_row in RrT.Tables[0].Rows)
								{
									if (!Convert.IsDBNull(iteration_row["itipsexo"]))
									{
										stTempSex = Convert.ToString(iteration_row["itipsexo"]);
										if (!pasa_uno)
										{
											pasa_uno = true;
											sexo_temp = stTempSex;
										}
										if (sexo_temp != stTempSex)
										{
											sexo_temp = "L";
										}
									}
									if (!Convert.IsDBNull(iteration_row["irestsex"]))
									{
										if (Convert.ToString(iteration_row["irestsex"]) == "S")
										{
											stTempRes = Convert.ToString(iteration_row["irestsex"]);
										}
									}
								}
								RrT.Close();
								if (sexo_temp != "P")
								{
									//busco el sexo del paciente
									SqlT = "select itipsexo from dpacient where " + " dpacient.gidenpac='" + stIdpaciente + "'";
									SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SqlT, RcAdmision);
									RrT = new DataSet();
									tempAdapter_8.Fill(RrT);
									if ((stTempRes == "S" && stTempSex != Convert.ToString(RrT.Tables[0].Rows[0]["itipsexo"])) || (stTempRes == "S" && sexo_temp == "L"))
									{
										//no cumple las restricciones de sexo
										short tempRefParam14 = 1350;
                                        string[] tempRefParam15 = new string[] { };
										iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam14, RcAdmision, tempRefParam15));
										bVuelta = false;
									}
									RrT.Close();
								}
							}
							//RrT.Close
						}
						if (bVuelta)
						{
							switch(Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["iestcama"]))
							{
								case "O" : 
									//MsgBox ("La cama que tenia el paciente actualmente est� Ocupada o Bloqueada") 
									stMensaje1 = StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gcamasbo"], "00").Trim() + " Ocupada"; 
									short tempRefParam16 = 1320;
                                    string[] tempRefParam17 = new string[] { stMensaje1}; 
									iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam16, RcAdmision, tempRefParam17)); 
									bVuelta = false; 
									break;
								case "I" : 
									SqlT = "SELECT * FROM SCONSGLO WHERE gconsglo ='INHABALT'"; 
									SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(SqlT, RcAdmision); 
									RrT = new DataSet(); 
									tempAdapter_9.Fill(RrT); 
									if (Conversion.Val(Convert.ToString(RrT.Tables[0].Rows[0]["nnumeri1"])) == Convert.ToDouble(RrAMOVIMIE.Tables[0].Rows[0]["gmotinha"]))
									{
                                        //dejar la cama libre
                                        //RrAMOVIMIE.Edit();
                                        //RrAMOVIMIE.Tables[0].Rows[0]["iestcama"] = "L";
                                        //SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_AMOVIMIE);
                                        //                              tempAdapter_AMOVIMIE.Update(RrAMOVIMIE, RrAMOVIMIE.Tables[0].TableName);

                                        var sqltemp = "select * from DCAMASBOVA where gplantas = " + RrAMOVIMIE.Tables[0].Rows[0]["gplantor"].ToString() + " and ghabitac = " + RrAMOVIMIE.Tables[0].Rows[0]["ghabitor"].ToString() + " and gcamasbo = '" + RrAMOVIMIE.Tables[0].Rows[0]["gcamaori"].ToString() + "'";
                                        var dataAdapter = new SqlDataAdapter(sqltemp, RcAdmision);
                                        var dataSet = new DataSet();
                                        dataAdapter.Fill(dataSet);
                                        dataSet.Tables[0].Rows[0]["iestcama"] = "L";
                                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(dataAdapter);
                                        dataAdapter.Update(dataSet, dataSet.Tables[0].TableName);

                                        RrT.Close();
										bVuelta = true;
									}
									else
									{
										//MsgBox ("La cama que ten�a el paciente actualmente est� Inhabilitada, debe primero desinhabilitarla")
										stMensaje1 = StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gcamasbo"], "00").Trim() + " Inhabilitada";
										short tempRefParam18 = 1320;
                                        string[] tempRefParam19 = new string[] { stMensaje1};
										iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam18, RcAdmision, tempRefParam19));
										bVuelta = false;
									} 
									break;
								case "B" : 
									//MsgBox ("La cama que ten�a el paciente actualmente est� Bloqueada, debe primero desinhabilitarla") 
									stMensaje1 = StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"], "00").Trim() + StringsHelper.Format(RrAMOVIMIE.Tables[0].Rows[0]["gcamasbo"], "00").Trim() + " Bloqueada"; 
									short tempRefParam20 = 1320;
                                    string[] tempRefParam21 = new string[] { stMensaje1}; 
									iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam20, RcAdmision, tempRefParam21)); 
									bVuelta = false; 
									break;
								default:
									bVuelta = true; 
									break;
							}
						}
					}
				}
				return bVuelta;
			}
			catch(Exception ex)
			{
				RadMessageBox.Show("Error validando cama", Application.ProductName);
				return false;
			}
		}

		private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string sqlmasivos = String.Empty;
			string stMensaje1 = String.Empty;
			string stMensaje2 = String.Empty;

			string SqlMasivo = String.Empty;
			SqlCommand RqMasivos = null;
			string SqlConsglo = String.Empty;
			DataSet RrConsglo = null;
			string stFecha_Llegada = String.Empty;
			System.DateTime dtFechaAltaAdmision = DateTime.FromOADate(0);
			ProcesoComprobar.ClComprobar ClaseProceso = null;

            try
			{
                RcAdmision.BeginTransScope();
                this.Cursor = Cursors.WaitCursor;

				// Recogemos el GIDENPAC del paciente enviado por FILIACION
				int tempRefParam = 5;
				string tempRefParam2 = ";";
				stIdpaciente = fnGetToken(stcadena_temp, tempRefParam, tempRefParam2);


				//Consultas previas a la modificacion de datos *******************************

				// BUSCA SI EL PACIENTE ESTA EN URGENCIAS ************************************
				StSql = " SELECT * " + " FROM UEPISURG " + " WHERE gidenpac = '" + stIdpaciente + "' AND " + " faltaurg IS NULL ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, RcAdmision);
				RrUEPISURG = new DataSet();
				tempAdapter.Fill(RrUEPISURG);
				if (RrUEPISURG.Tables[0].Rows.Count != 0)
				{
					// Encuentra registros con lo cual el paciente esta en urgencias
					//MsgBox ("El paciente seleccionado se encuentra en Urgencias")
					stMensaje1 = "Anular el Alta";
					stMensaje2 = "en Urgencias";
					short tempRefParam3 = 1310;
                    string[] tempRefParam4 = new string[] { stMensaje1, stMensaje2};
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RcAdmision, tempRefParam4));
					RrUEPISURG.Close();
					this.Cursor = Cursors.Default;
                    RcAdmision.RollbackTransScope();

                    this.Close();
					if (VstArea == "H")
					{
						FrFormulario.Show();
					}
					return;
				}
				RrUEPISURG.Close();

				//valida si tiene apuntes facturados
				if (!fnValida_factu(stIdpaciente))
				{
					this.Cursor = Cursors.Default;
                    RcAdmision.RollbackTransScope();

                    this.Close();
					if (VstArea == "H")
					{
						FrFormulario.Show();
					}
					return;

				}


				//*******************************************************************************************
				//Comprobamos que se puede anular el alta de un episodio si pertenece a alg�n proceso
				//El episodio se obtiene en la funci�n fnValida_factu
				bErroProcesoComprobar = false;
				stDescripcionProcesoComprobar = "";
				ClaseProceso = new ProcesoComprobar.ClComprobar();
				object tempRefParam5 = this;
				string tempRefParam6 = "H";
				short tempRefParam7 = (short) Convert.ToInt32(Double.Parse(stGanoadme));
				int tempRefParam8 = Convert.ToInt32(Double.Parse(stGnumadme));
				object tempRefParam9 = null;
				object tempRefParam10 = "V";
				ClaseProceso.Iniciar(RcAdmision, tempRefParam5, tempRefParam6, tempRefParam7, tempRefParam8, tempRefParam9, tempRefParam10);
				ClaseProceso = null;
				//************************************************************************************
				if (bErroProcesoComprobar)
				{
					RadMessageBox.Show(stDescripcionProcesoComprobar, Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
					this.Cursor = Cursors.Default;
                    RcAdmision.RollbackTransScope();

                    this.Close();
					if (VstArea == "H")
					{
						FrFormulario.Show();
					}
					return;
				}
				//************************************************************************************



				ttemp = " SELECT * " + " FROM AEPISADM " + " WHERE " + " AEPISADM.gidenpac ='" + stIdpaciente + "' AND " + " AEPISADM.faltplan IS NULL ";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(ttemp, RcAdmision);
				RrTemp = new DataSet();
				tempAdapter_2.Fill(RrTemp);
				if (RrTemp.Tables[0].Rows.Count != 0)
				{
					// Encuentra registros, EL PACIENTE ESTA INGRESADO
					//MsgBox ("El paciente seleccionado no est� dado de Alta")
					stMensaje1 = "Anular el Alta";
					stMensaje2 = "Ingresado";
					short tempRefParam11 = 1310;
                    string[] tempRefParam12 = new string[] { stMensaje1, stMensaje2};
					Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, RcAdmision, tempRefParam12));
					RrAEPISADM.Close();
					RrTemp.Close();
                    RcAdmision.RollbackTransScope();

                }
				else
				{
					RrTemp.Close();
					// BUSCA AL PACIENTE POR LA MAYOR FECHA DE LLEGADA A ADMISION*****************
					StSql = " SELECT MAX(fllegada) as fllegada " + " FROM AEPISADM " + " WHERE gidenpac = '" + stIdpaciente + "' AND " + " faltplan IS NOT NULL ";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, RcAdmision);
					RrAEPISADM = new DataSet();
					tempAdapter_3.Fill(RrAEPISADM);
					// ENCUENTRA al paciente que ha sido dado de alta en planta
					if (Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["fllegada"]))
					{
						// Puede ocurrir que las tablas de pacientes esten mal y un paciente
						// se le intenta anular el alta y no tiene fecha de llegada
						// salta un error
						stMensaje1 = "Anular el Alta";
						stMensaje2 = "sin registrar en Admision";
						short tempRefParam13 = 1310;
                        string[] tempRefParam14 = new string[] { stMensaje1, stMensaje2};
						Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, RcAdmision, tempRefParam14));
                        RcAdmision.RollbackTransScope();

                    }
					else
					{
						stFecha_Llegada = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["fllegada"]);
						RrAEPISADM.Close();
						object tempRefParam15 = stFecha_Llegada;
						StSql = " SELECT * " + " FROM AEPISADM " + " WHERE gidenpac = '" + stIdpaciente + "' AND " + " faltplan IS NOT NULL AND " + " fllegada =" + Serrores.FormatFechaHMS(tempRefParam15) + "";
						stFecha_Llegada = Convert.ToString(tempRefParam15);
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSql, RcAdmision);
						RrAEPISADM = new DataSet();
						tempAdapter_4.Fill(RrAEPISADM);
						// Cogemos los datos del paciente seleccionado
						stGanoadme = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["Ganoadme"]);
						stGnumadme = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gnumadme"]);
						stNanoadms = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["Nanoadms"]);
						stNnumadms = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["nnumadms"]);
						stServicio = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["gserulti"]);
						stFfinmovi = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["faltplan"]);
						dtFechaAltaAdmision = Convert.ToDateTime(RrAEPISADM.Tables[0].Rows[0]["faltplan"]);
						if (Convert.IsDBNull(RrAEPISADM.Tables[0].Rows[0]["fingplan"]))
						{
							// No deber�a entrar porque debe tener fecha de ingreso en
							// planta ya que tiene fecha de alta en planta
							stFingplan = "";
							stIndicador = "AA";
						}
						else
						{
							stFingplan = Convert.ToString(RrAEPISADM.Tables[0].Rows[0]["fingplan"]);
							stIndicador = "AAP"; // Anularemos el Alta solo de la Planta
						}
						RrAEPISADM.Close();
						StSql = " SELECT * " + " FROM DPACIENT " + " WHERE gidenpac = '" + stIdpaciente + "'";
						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSql, RcAdmision);
						RrDPACIENT = new DataSet();
						tempAdapter_5.Fill(RrDPACIENT);
						stItipsexo = Convert.ToString(RrDPACIENT.Tables[0].Rows[0]["itipsexo"]);
						RrDPACIENT.Close();
						// Consulta para obtener la cama que ten�a asignada y ver su estado
						if (!fnValida_Cama(stGanoadme, stGnumadme))
						{
							RrAMOVIMIE.Close();
                            RcAdmision.RollbackTransScope();
                        }
						else
						{
							//si llega aqui miramos si existe nido y si tiene algun episodio relacionado
							if (fnExiste_Nido())
							{
								// Llamada al procedimiento de anulaci�n
								if (proAnular())
								{
									// replico todas las tablas de enfermeria para que sea coherentes en admision
									// fin de replicacion de las tablas de enfermeria

									// anular movimientos masivos de alta en planta
									sqlmasivos = "MASIVOSENFERMERIA";
									RqMasivos = RcAdmision.CreateQuery("Masivos", sqlmasivos);
                                    RqMasivos.CommandType = CommandType.StoredProcedure;
                                    RqMasivos.Parameters.Add("@itiposer", SqlDbType.Char);
                                    RqMasivos.Parameters.Add("@anoreg", SqlDbType.SmallInt);
                                    RqMasivos.Parameters.Add("@numreg", SqlDbType.Int);
                                    RqMasivos.Parameters.Add("@sentido", SqlDbType.Int);

                                    RqMasivos.Parameters[0].Direction = ParameterDirection.Input;
									RqMasivos.Parameters[1].Direction = ParameterDirection.Input;
									RqMasivos.Parameters[2].Direction = ParameterDirection.Input;
									RqMasivos.Parameters[3].Direction = ParameterDirection.Input;
									RqMasivos.Parameters[0].Value = "H";
									RqMasivos.Parameters[1].Value = stGanoadme;
									RqMasivos.Parameters[2].Value = stGnumadme;
									RqMasivos.Parameters[3].Value = 1;
									RqMasivos.ExecuteNonQuery();
                                    RqMasivos.Close();

									sqlmasivos = "MASIVOS_ADMISION";
									RqMasivos = RcAdmision.CreateQuery("Masivos", sqlmasivos);
                                    RqMasivos.CommandType = CommandType.StoredProcedure;
                                    RqMasivos.Parameters.Add("@TIPOSERVI", SqlDbType.Char);
                                    RqMasivos.Parameters.Add("@ANOADME", SqlDbType.SmallInt);
                                    RqMasivos.Parameters.Add("@NUMADME", SqlDbType.Int);
                                    RqMasivos.Parameters.Add("@TIPO", SqlDbType.Int);


                                    RqMasivos.Parameters[0].Direction = ParameterDirection.Input;
									RqMasivos.Parameters[1].Direction = ParameterDirection.Input;
									RqMasivos.Parameters[2].Direction = ParameterDirection.Input;
									RqMasivos.Parameters[3].Direction = ParameterDirection.Input;
									RqMasivos.Parameters[0].Value = "H";
									RqMasivos.Parameters[1].Value = stGanoadme;
									RqMasivos.Parameters[2].Value = stGnumadme;
									RqMasivos.Parameters[3].Value = 1;
									RqMasivos.ExecuteNonQuery();
                                    RqMasivos.Close();

									//BORRAR REGISTRO DEL LISTADO AL ALTA SI EXISTE
									//SI TODO BIEN BORRAR EL FICHERO
									//No tiene sentido al anular el alta borrar las observaciones del informe de alta
									//cuando se da el alta de nuevo al paciente se modificaran las observaciones
									if (proMovimientosVarios(DateTimeHelper.ToString(dtFechaAltaAdmision)))
									{ // realiza varios procesos como borrar apuntes de facturacion
										// que tengan eventos FA,IN,PR, poner fpasfact a null en determinados sitios
										if (proMoverDePasivoAActivo())
										{ // quiere decir que hay error
                                            RcAdmision.RollbackTransScope();
                                            RadMessageBox.Show("Error moviendo de pasivo a activo", Application.ProductName);
										}
										else
										{
                                            RcAdmision.CommitTransScope();
                                        }
									}
									else
									{
                                        RcAdmision.RollbackTransScope();
                                    }
								}
								else
								{
                                    RcAdmision.RollbackTransScope();
                                    RadMessageBox.Show("Error moviendo de pasivo a activo", Application.ProductName);
								}
							}
							else
							{
                                RcAdmision.RollbackTransScope();
                                RadMessageBox.Show("Error comprobando NIDO", Application.ProductName);
							}
						}
					}
				}



				this.Cursor = Cursors.Default;
				this.Close();
				if (VstArea == "H")
				{
                    FrFormulario.refresco();
				}
			}
			catch (System.Exception excep)
			{
                RcAdmision.RollbackTransScope();
                //RqMasivos.Close
                this.Cursor = Cursors.Default;
				RadMessageBox.Show("error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
			}
		}

		private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
			if (VstArea == "H")
			{
                FrFormulario.refresco();
			}
		}

		private void anu_alta_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				string stCodSociedad = String.Empty;
				DataSet RrDSOCIEDA = null;
				DataSet RrSCONSGLO = null;
				string stsql1 = String.Empty;
				string stsql2 = String.Empty;
				string stsql3 = String.Empty;
				int lCodSS = 0;
				int lCodPV = 0;
				string stTexSS = String.Empty;
				string stTexPV = String.Empty;
				string sqlTemp = String.Empty;
				DataSet RrTemp = null;
				string LitPersona = String.Empty;

				try
				{
					Serrores.oClass = new Mensajes.ClassMensajes();


                    CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

					this.Height = 139;
					this.Width = 516;
					this.Top = 67;

					LitPersona = Serrores.ObtenerLiteralPersona(RcAdmision);
					LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
					Label28.Text = LitPersona + ":";

					stcadena_temp = VstTag;
					Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(RcAdmision);

					int tempRefParam = 1;
					string tempRefParam2 = ";";
					LbNombrePaciente.Text = " " + fnGetToken(stcadena_temp, tempRefParam, tempRefParam2); //& " " & fnGetToken(stcadena_temp, 2, ";") & " " & fnGetToken(stcadena_temp, 3, ";")
					int tempRefParam3 = 2;
					string tempRefParam4 = ";";
					LbAsegurado.Text = " " + fnGetToken(stcadena_temp, tempRefParam3, tempRefParam4);
					int tempRefParam5 = 4;
					string tempRefParam6 = ";";
					LbHistoria.Text = " " + fnGetToken(stcadena_temp, tempRefParam5, tempRefParam6);

					int tempRefParam7 = 3;
					string tempRefParam8 = ";";
					stCodSociedad = fnGetToken(stcadena_temp, tempRefParam7, tempRefParam8);
					if (stcadena_temp == ";;;;")
					{
						return;
					}

					stsql1 = " SELECT dsocieda" + " FROM DSOCIEDA " + " WHERE gsocieda =" + stCodSociedad + "";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql1, RcAdmision);
					RrDSOCIEDA = new DataSet();
					tempAdapter.Fill(RrDSOCIEDA);
					//*****recojo el codigo de la sociedad********
					VstSociedad = stCodSociedad;

					stsql2 = " select valfanu1,nnumeri1 from sconsglo where gconsglo= 'segursoc' ";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql2, RcAdmision);
					RrSCONSGLO = new DataSet();
					tempAdapter_2.Fill(RrSCONSGLO);
					lCodSS = Convert.ToInt32(Double.Parse(Convert.ToString(RrSCONSGLO.Tables[0].Rows[0]["nnumeri1"]).Trim()));
					stTexSS = Convert.ToString(RrSCONSGLO.Tables[0].Rows[0]["valfanu1"]).Trim();
					RrSCONSGLO.Close();

					stsql3 = " select valfanu1,nnumeri1 from sconsglo where gconsglo= 'privado' ";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stsql3, RcAdmision);
					RrSCONSGLO = new DataSet();
					tempAdapter_3.Fill(RrSCONSGLO);
					lCodPV = Convert.ToInt32(Double.Parse(Convert.ToString(RrSCONSGLO.Tables[0].Rows[0]["nnumeri1"]).Trim()));
					stTexPV = Convert.ToString(RrSCONSGLO.Tables[0].Rows[0]["valfanu1"]).Trim();
					RrSCONSGLO.Close();



					//comprobar que sigue de alta
					int tempRefParam9 = 5;
					string tempRefParam10 = ";";
					stIdpaciente = fnGetToken(stcadena_temp, tempRefParam9, tempRefParam10);
					sqlTemp = "select * from aepisadm where gidenpac='" + stIdpaciente + "' and faltplan  is not null";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlTemp, RcAdmision);
					RrTemp = new DataSet();
					tempAdapter_4.Fill(RrTemp);
					if (RrTemp.Tables[0].Rows.Count == 0)
					{
						short tempRefParam11 = 1310;
                        string[] tempRefParam12 = new string[] { "anular el alta", "ingresado"};
						Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, RcAdmision, tempRefParam12));
						this.Close();
						return;
					}


					if (StringsHelper.ToDoubleSafe(stCodSociedad) == lCodSS)
					{
						LbFinanciadora.Text = " " + stTexSS;
						object tempRefParam13 = stFfinmovi;
						stsql1 = " SELECT ginspecc FROM AMOVIFIN " + " WHERE " + " itiposer = 'H' AND " + " ganoregi = " + Convert.ToString(RrTemp.Tables[0].Rows[0]["ganoadme"]) + " AND " + " gnumregi = " + Convert.ToString(RrTemp.Tables[0].Rows[0]["gnumadme"]) + " AND " + " ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam13) + " ";
						stFfinmovi = Convert.ToString(tempRefParam13);
						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stsql1, RcAdmision);
						RrTemp = new DataSet();
						tempAdapter_5.Fill(RrTemp);
						if (RrTemp.Tables[0].Rows.Count != 0)
						{
							VstInspecc = Convert.ToString(RrTemp.Tables[0].Rows[0]["ginspecc"]) + "";
						}
					}
					else
					{
						if (StringsHelper.ToDoubleSafe(stCodSociedad) == lCodPV)
						{
							LbFinanciadora.Text = " " + stTexPV;
						}
						else
						{
							LbFinanciadora.Text = Convert.ToString(RrDSOCIEDA.Tables[0].Rows[0]["dsocieda"]);
						}
					}
					RrTemp.Close();
					RrDSOCIEDA.Close();

					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter("SELECT * FROM SCONSGLO WHERE GCONSGLO ='BDBCONEX'", RcAdmision);
					RrTemp = new DataSet();
					tempAdapter_6.Fill(RrTemp);
					ResumenCMDB = false;
					if (RrTemp.Tables[0].Rows.Count != 0)
					{
						if (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
						{
							ResumenCMDB = true;
						}
					}
					RrTemp.Close();

					return;
				}
				catch (System.Exception excep)
				{
					RadMessageBox.Show("error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
				}
			}
		}



		private string fnGetToken(string cCadena, int nPosBus, string cCarSep)
		{

			//*****************************************************************************
			//Extrae un "token" (fragmento) de una cadena
			//Par�metros........: - Cadena donde buscar
			//                    - N� de ocurrencia a extraer
			//                    - Caracter de separaci�n de token's, ";" por defecto.
			//*****************************************************************************

			string stCarsep1 = String.Empty;
			int iPosEnc = 0;
			int iPossig = 0;


			string stCadFin = "";
			if (String.Compare(cCadena, "") != 0 && nPosBus > 0)
			{
				if (false)
				{
					cCarSep = ";";
				}
				else
				{
					stCarsep1 = cCarSep;
				}
				if (nPosBus <= fnNumToken(cCadena, stCarsep1))
				{
					int tempRefParam = nPosBus - 1;
					iPosEnc = fnAtNum(stCarsep1, cCadena, tempRefParam) + 1;
					iPossig = fnAtNum(stCarsep1, cCadena, nPosBus) - 1;
					if (iPossig < 0)
					{
						iPossig = cCadena.Length;
					}
					stCadFin = cCadena.Substring(iPosEnc - 1, Math.Min(iPossig - iPosEnc + 1, cCadena.Length - (iPosEnc - 1)));
				}
			}
			return stCadFin;
		}

		private string fnGetToken(string cCadena, int nPosBus)
		{
			string tempRefParam = String.Empty;
			return fnGetToken(cCadena, nPosBus, tempRefParam);
		}

		private int fnAtNum(string cCaracter, string cCadena, int nPosBus, int? nComparar_optional)
		{
			int nComparar = (nComparar_optional == null) ? 0 : nComparar_optional.Value;
			try
			{
				CompareMethod cComparar = (CompareMethod) 0;

				//********************************************************************************
				//Busca la N ocurrencia de un caracter en una cadena
				//Par�metros.......: - Cadena a buscar
				//                   - Cadena donde buscar
				//                   - N� de ocurrencia buscada (Opcional)
				//                   - Tipo de comparaci�n:- 0 = Comparaci�n binaria (por defecto)
				//                                         - 1 = Comparaci�n textual.
				//********************************************************************************

				int icont = 0;
				int iPosEnc = 0;
				int iPossig = 0;

				if (false)
				{
					nPosBus = 1;
				}
				if (nPosBus > 0)
				{
					if (nComparar_optional == null)
					{
						nComparar = 0;
					}
					iPosEnc = 1;
					iPossig = 1;
					icont = 1;

					while(icont <= nPosBus && iPosEnc > 0)
					{
						iPosEnc = Strings.InStr(iPossig, cCadena, cCaracter, cComparar);
						icont++;
						iPossig = iPosEnc + 1;
					};
					return iPosEnc;
				}
				else
				{
					return 0;
				}
			}
			finally
			{
				nComparar_optional = nComparar;
			}
			return 0;
		}

		private int fnAtNum(string cCaracter, string cCadena, int nPosBus)
		{
			int? tempRefParam2 = null;
			return fnAtNum(cCaracter, cCadena, nPosBus, tempRefParam2);
		}

		private int fnAtNum(string cCaracter, string cCadena)
		{
			int tempRefParam3 = 0;
			int? tempRefParam4 = null;
			return fnAtNum(cCaracter, cCadena, tempRefParam3, tempRefParam4);
		}

		private int fnNumToken(string cCadena, string cCarSep)
		{

			//*****************************************************************************
			//Devuelve el n�mero de "token's" en una cadena
			//Par�metros......:- Cadena donde buscar
			//                 - Caracter de separaci�n de "token's", ";" por defecto.
			//*****************************************************************************


			int icont = 0;
			if (false || cCarSep.Trim() == "")
			{
				cCarSep = ";";
			}
			for (int iBucle = 1; iBucle <= cCadena.Length; iBucle++)
			{
				if (String.Compare(cCadena.Substring(iBucle - 1, Math.Min(1, cCadena.Length - (iBucle - 1))), cCarSep) == 0)
				{
					icont++;
				}
			}
			if (icont < 1 && cCadena.Length > 0)
			{
				icont = 1;
			}
			else
			{
				icont++;
			}
			return icont;
		}

		private int fnNumToken(string cCadena)
		{
			string tempRefParam5 = String.Empty;
			return fnNumToken(cCadena, tempRefParam5);
		}



		public bool proAnular()
		{
			string istGperulti = null;
			string fAprovechanum = String.Empty;
            RcAdmision.BeginTransScope();
            string tsSqReut = "select valfanu1 from sconsglo where gconsglo='iaproanu'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tsSqReut, RcAdmision);
			DataSet tRrreut = new DataSet();
			tempAdapter.Fill(tRrreut);
			if (Convert.ToString(tRrreut.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
			{
				fAprovechanum = "S";
			}
			else
			{
				fAprovechanum = "N";
			}
			tRrreut.Close();

			bool bConservarDatos = Serrores.ObternerValor_CTEGLOBAL(RcAdmision, "ANUVALAL", "VALFANU1") == "S";

			try
			{

				// La consulta RrAMOVIMIE la tengo abierta todavia
				StSql = " SELECT * " + " FROM DCAMASBO " + " WHERE " + " iestcama = 'L' AND " + " gplantas =" + Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["gplantas"]) + " AND " + " ghabitac =" + Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["ghabitac"]) + " AND " + " gcamasbo ='" + Convert.ToString(RrAMOVIMIE.Tables[0].Rows[0]["gcamasbo"]) + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, RcAdmision);
				RrDCAMASBO = new DataSet();
				tempAdapter_2.Fill(RrDCAMASBO);

				// Actualizamos la cama, ocupandola
				RrDCAMASBO.Edit();
				RrDCAMASBO.Tables[0].Rows[0]["gidenpac"] = stIdpaciente;
				RrDCAMASBO.Tables[0].Rows[0]["iestcama"] = "O";
				RrDCAMASBO.Tables[0].Rows[0]["itipsexo"] = stItipsexo;
				RrDCAMASBO.Tables[0].Rows[0]["gmotinha"] = DBNull.Value;
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                tempAdapter_2.Update(RrDCAMASBO, RrDCAMASBO.Tables[0].TableName);
				RrDCAMASBO.Close();

				// Actualizamos la tabla de movimientos AMOVIMIE (que aun tengo abierta)
				// anulando la fecha de ultimo movimiento
				RrAMOVIMIE.Edit();

                var sqltemp = " SELECT * FROM AMOVIMIE WHERE  itiposer = 'H' AND " + " ganoregi = " + RrAMOVIMIE.Tables[0].Rows[0]["ganoregi"].ToString() + " AND  gnumregi = " + RrAMOVIMIE.Tables[0].Rows[0]["gnumregi"].ToString() + " AND " + " fmovimie = " + Serrores.FormatFechaHMS(RrAMOVIMIE.Tables[0].Rows[0]["fmovimie"]) + "";
                var dataAdapter = new SqlDataAdapter(sqltemp, RcAdmision);
                var dataSet = new DataSet();
                dataAdapter.Fill(dataSet);

                dataSet.Tables[0].Rows[0]["ffinmovi"] = DBNull.Value;
                dataSet.Tables[0].Rows[0]["gunenfde"] = DBNull.Value;
                dataSet.Tables[0].Rows[0]["gservide"] = DBNull.Value;
                dataSet.Tables[0].Rows[0]["gmedicde"] = DBNull.Value;
                dataSet.Tables[0].Rows[0]["gplantde"] = DBNull.Value;
                dataSet.Tables[0].Rows[0]["ghabitde"] = DBNull.Value;
                dataSet.Tables[0].Rows[0]["gcamades"] = DBNull.Value;

                SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(dataAdapter);
                dataAdapter.Update(dataSet, dataSet.Tables[0].TableName);
                RrAMOVIMIE.Close();

				// Consulta para Actualizar la tabla de AMOVIFIN, ponemos a null
				// el campo de ffinmovi
				object tempRefParam = stFfinmovi;
				StSql = " SELECT * " + " FROM AMOVIFIN " + " WHERE " + " itiposer = 'H' AND " + " ganoregi = " + stGanoadme + " AND " + " gnumregi = " + stGnumadme + " AND " + " ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam) + " ";
				stFfinmovi = Convert.ToString(tempRefParam);
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSql, RcAdmision);
				RrAMOVIFIN = new DataSet();
				tempAdapter_5.Fill(RrAMOVIFIN);
				RrAMOVIFIN.Edit();
				RrAMOVIFIN.Tables[0].Rows[0]["ffinmovi"] = DBNull.Value;
				SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_5);
                tempAdapter_5.Update(RrAMOVIFIN, RrAMOVIFIN.Tables[0].TableName);
				RrAMOVIFIN.Close();
				// Consulta para Actualizar la tabla de AMOVREGA, ponemos a null
				// el campo de ffinmovi
				object tempRefParam2 = stFfinmovi;
				StSql = " SELECT * " + " FROM AMOVREGA " + " WHERE " + " itiposer = 'H' AND " + " ganoregi = " + stGanoadme + " AND " + " gnumregi = " + stGnumadme + " AND " + " ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam2) + " ";
				stFfinmovi = Convert.ToString(tempRefParam2);
				SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(StSql, RcAdmision);
				RrAMOVIFIN = new DataSet();
				tempAdapter_7.Fill(RrAMOVIFIN);
				RrAMOVIFIN.Edit();
				RrAMOVIFIN.Tables[0].Rows[0]["ffinmovi"] = DBNull.Value;
				SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_7);
                tempAdapter_7.Update(RrAMOVIFIN, RrAMOVIFIN.Tables[0].TableName);
				RrAMOVIFIN.Close();
				// CONSULTA PARA ACTUALIZAR LA TABLA AREGIMEN
				object tempRefParam3 = stFfinmovi;
				StSql = " SELECT * " + " FROM AREGIMEN " + " WHERE " + " ganoadme = " + stGanoadme + " AND " + " gnumadme = " + stGnumadme + " AND " + " ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam3) + " ";
				stFfinmovi = Convert.ToString(tempRefParam3);
				SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(StSql, RcAdmision);
				RrAMOVIFIN = new DataSet();
				tempAdapter_9.Fill(RrAMOVIFIN);
				RrAMOVIFIN.Edit();
				RrAMOVIFIN.Tables[0].Rows[0]["ffinmovi"] = DBNull.Value;
				SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter_9);
                tempAdapter_9.Update(RrAMOVIFIN, RrAMOVIFIN.Tables[0].TableName);
				RrAMOVIFIN.Close();


				//oscar 30/04/04
				// CONSULTA PARA ACTUALIZAR LA TABLA AMOVPROD si tiene gestion de producto
				if (bGestionProd)
				{
					object tempRefParam4 = stFfinmovi;
					StSql = " SELECT * " + " FROM AMOVPROD " + " WHERE " + " ganoregi = " + stGanoadme + " AND " + " gnumregi = " + stGnumadme + " AND " + " ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam4) + " " + " AND itiposer='H'";
					stFfinmovi = Convert.ToString(tempRefParam4);
					SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(StSql, RcAdmision);
					RrAMOVIFIN = new DataSet();
					tempAdapter_11.Fill(RrAMOVIFIN);
                    if (RrAMOVIFIN.Tables[0].Rows.Count > 0)
                    {
                        RrAMOVIFIN.Edit();
                        RrAMOVIFIN.Tables[0].Rows[0]["ffinmovi"] = DBNull.Value;
                        SqlCommandBuilder tempCommandBuilder_6 = new SqlCommandBuilder(tempAdapter_11);
                        tempAdapter_11.Update(RrAMOVIFIN, RrAMOVIFIN.Tables[0].TableName);
                    }
					RrAMOVIFIN.Close();
				}
				//-----------------------


				// *******************************************************************
				// Anulamos el alta en ADMISION, se modifican los registros de AEPISADM
				StSql = " SELECT * " + " FROM AEPISADM " + " WHERE " + " ganoadme = " + stGanoadme + " AND " + " gnumadme = " + stGnumadme + " ";
				SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(StSql, RcAdmision);
				RrAEPISADM = new DataSet();
				tempAdapter_13.Fill(RrAEPISADM);
				RrAEPISADM.Edit();
				RrAEPISADM.Tables[0].Rows[0]["faltaadm"] = DBNull.Value;
				RrAEPISADM.Tables[0].Rows[0]["faltplan"] = DBNull.Value;
				RrAEPISADM.Tables[0].Rows[0]["nanoadms"] = DBNull.Value;
				RrAEPISADM.Tables[0].Rows[0]["nnumadms"] = DBNull.Value;
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["gsitalta"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["gmotalta"] = DBNull.Value;
				}
				//oscar 23/03/04
				//SI SE ANULA EL ALTA HABRA QUE ELIMINAR LAS CAUSAS DE EXITUS
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["ocinmexi"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["ocpriexi"] = DBNull.Value;
				}
				//-------

				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["ghospita"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["gdiagalt"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["odiagalt"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["gdiasec1"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["odiasec1"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["gdiasec2"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["odiasec2"] = DBNull.Value;
				}
				RrAEPISADM.Tables[0].Rows[0]["gusualta"] = DBNull.Value;
				RrAEPISADM.Tables[0].Rows[0]["gserulti"] = iGservici;
                RrAEPISADM.Tables[0].Rows[0]["gperulti"] = iGpersona;

				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["dserdest"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["gmottrac"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["iestudio"] = DBNull.Value;
				}
				if (!bConservarDatos)
				{
					RrAEPISADM.Tables[0].Rows[0]["gcoeconc"] = DBNull.Value;
				}



				SqlCommandBuilder tempCommandBuilder_7 = new SqlCommandBuilder(tempAdapter_13);
				tempAdapter_13.Update(RrAEPISADM, RrAEPISADM.Tables[0].TableName);
				RrAEPISADM.Close();
				// Generamos un registro en la tabla de episodios anulados DEPIANUL
				StSql = " SELECT * " + " FROM DEPIANUL ";
				SqlDataAdapter tempAdapter_15 = new SqlDataAdapter(StSql, RcAdmision);
				RrDEPIANUL = new DataSet();
				tempAdapter_15.Fill(RrDEPIANUL);
				RrDEPIANUL.AddNew();
				RrDEPIANUL.Tables[0].Rows[0]["itiposer"] = "H";
				RrDEPIANUL.Tables[0].Rows[0]["ganoregi"] = stNanoadms;
				RrDEPIANUL.Tables[0].Rows[0]["gnumregi"] = stNnumadms;
				RrDEPIANUL.Tables[0].Rows[0]["fanulaci"] = DateTime.Now;
				RrDEPIANUL.Tables[0].Rows[0]["ialtaing"] = "A";
				RrDEPIANUL.Tables[0].Rows[0]["gusuario"] = VstCodUsua;
				RrDEPIANUL.Tables[0].Rows[0]["gservici"] = Conversion.Val(stServicio);
				RrDEPIANUL.Tables[0].Rows[0]["ireutili"] = fAprovechanum;
				RrDEPIANUL.Tables[0].Rows[0]["gidenpac"] = stIdpaciente;
				RrDEPIANUL.Tables[0].Rows[0]["gsocieda"] = VstSociedad;
				SqlCommandBuilder tempCommandBuilder_8 = new SqlCommandBuilder(tempAdapter_15);
				tempAdapter_15.Update(RrDEPIANUL, RrDEPIANUL.Tables[0].TableName);
				RrDEPIANUL.Close();


				StSql = " DELETE FROM HDIAGGLO WHERE itiposer='H' AND nanoregi = " + stGanoadme + " AND nnumregi = " + stGnumadme;
				SqlCommand tempCommand = new SqlCommand(StSql, RcAdmision);
				tempCommand.ExecuteNonQuery();

				StSql = " DELETE HDIAGPAR WHERE itiposer='H' AND nanoregi = " + stGanoadme + " AND nnumregi = " + stGnumadme;
				SqlCommand tempCommand_2 = new SqlCommand(StSql, RcAdmision);
				tempCommand_2.ExecuteNonQuery();

				StSql = " DELETE HPROCINC WHERE itiposer='H' AND ganoregi = " + stGanoadme + " AND gnumregi = " + stGnumadme;
				SqlCommand tempCommand_3 = new SqlCommand(StSql, RcAdmision);
				tempCommand_3.ExecuteNonQuery();

				StSql = " DELETE HPROCNOI WHERE itiposer='H' AND ganoregi = " + stGanoadme + " AND gnumregi = " + stGnumadme;
				SqlCommand tempCommand_4 = new SqlCommand(StSql, RcAdmision);
				tempCommand_4.ExecuteNonQuery();




				return true;
			}
			catch (System.Exception excep)
			{
                RcAdmision.RollbackTransScope();
                this.Cursor = Cursors.Default;
				RadMessageBox.Show("Error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
				return false;
			}
		}

		public void recoge_paciente(dynamic PaFormulario, SqlConnection PaConexion, string PaNombre, string PaNumAse, string PaSocieda, string PaHistoria, string PaGidenpac, string PaUsuario, string PaArea)
		{
			FrFormulario = PaFormulario;
			RcAdmision = PaConexion;
			VstCodUsua = PaUsuario;
			VstArea = PaArea;
			VstTag = PaNombre + ";" + PaNumAse + ";" + PaSocieda + ";" + PaHistoria + ";" + PaGidenpac;
		}


		// devuelve true si se ha producido un error en el volcado de las tablas
		// Mueve tablas del pasivo de urgencias a Enfermeria en caso de que
		// tenga episodio relacionado y tenga apuntes
		private bool proMoverDePasivoAActivo()
		{
			bool result = false;
			MtoEnferUrgAdm.MvtoEnferUrgAdm ClaseReplicacion = null;

			// de anulacion de alta. Para ello lo controlo con una variable
			// booleana

			bool ControlError = false; // Si hay un error no se va a disparar el controlador de errores

			string sqlEpisodioUrg = "select ganrelur,gnurelur from aepisadm where ganoadme =" + stGanoadme + 
			                        " and gnumadme = " + stGnumadme;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEpisodioUrg, RcAdmision);
			DataSet RrEpisodioUrg = new DataSet();
			tempAdapter.Fill(RrEpisodioUrg);
			if (RrEpisodioUrg.Tables[0].Rows.Count == 1)
			{
				if (!Convert.IsDBNull(RrEpisodioUrg.Tables[0].Rows[0]["ganrelur"]))
				{ //tiene episodio relacionado
					ClaseReplicacion = new MtoEnferUrgAdm.MvtoEnferUrgAdm();
					string tempRefParam = Convert.ToString(RrEpisodioUrg.Tables[0].Rows[0]["ganrelur"]);
					string tempRefParam2 = Convert.ToString(RrEpisodioUrg.Tables[0].Rows[0]["gnurelur"]);
					string tempRefParam3 = stGanoadme;
					string tempRefParam4 = stGnumadme;
					int tempRefParam5 = 2;
					ClaseReplicacion.ReplicarTablasEnferUrgAdm(tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, tempRefParam5, RcAdmision, ControlError);
					ClaseReplicacion = null;
				}
			}
			result = ControlError;
			RrEpisodioUrg.Close();
			return result;
		}




		private bool proMovimientosVarios(string FechaAlta)
		{
			bool result = false;
			string sqlVarios = String.Empty;
			DataSet RrVarios = null;
			object DtFechaMenorInicioFacturacion = null; // se guarda para actualizar FPASFACT DE edietvia
			string stFechaAlta = String.Empty;
			string sqlDietas = String.Empty;
			DataSet RrDietas = null;

			try
			{

				result = true;

				// obtengo todos los apuntes de enfermeria que est�n en DMOVFACT para borrarlos
				// y los relacionados de quir�fanos y citas
				System.DateTime TempDate = DateTime.FromOADate(0);
				stFechaAlta = (DateTime.TryParse(FechaAlta, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : FechaAlta;
				object tempRefParam = stFechaAlta + ":00";
				object tempRefParam2 = stFechaAlta + ":59";
				sqlVarios = " select dmovfact.gidenpac from dmovfact where dmovfact.gnumregi=" + stGnumadme + 
				            " and dmovfact.ganoregi = " + stGanoadme + " and dmovfact.fperfact >= " + Serrores.FormatFechaHMS(tempRefParam) + 
				            " and dmovfact.fperfact <= " + Serrores.FormatFechaHMS(tempRefParam2) + 
				            " and dmovfact.gcodeven <> 'ES' " + 
				            " and itiposer ='H'"; // Tiposer ?
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlVarios, RcAdmision);
				RrVarios = new DataSet();
				tempAdapter.Fill(RrVarios);
				// borramos
                RrVarios.Delete(tempAdapter);
				RrVarios.Close();

				// ACTUALIZAR FPASFACT DE ETOMASFA,EINTERCO Y EPRESTAC A LA VEZ MEDIANTE UNA UNION
				// NO ME DEJA ACTUALIZAR LA UNION CON LO CUAL VOY A REALIZAR UN 3 EXECUTE

				object tempRefParam3 = FechaAlta;
				sqlVarios = " select itiposer, ganoregi, gnumregi, fapuntes, gfarmaco, fprevist, etomasfa.fpasfact from etomasfa where etomasfa.itiposer ='H'" + 
				            " and etomasfa.ganoregi =" + stGanoadme + " and etomasfa.gnumregi = " + 
				            stGnumadme + " and etomasfa.fpasfact=" + Serrores.FormatFechaHMS(tempRefParam3) + "";
				FechaAlta = Convert.ToString(tempRefParam3);
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlVarios, RcAdmision);
				RrVarios = new DataSet();
				tempAdapter_2.Fill(RrVarios);
				foreach (DataRow iteration_row_2 in RrVarios.Tables[0].Rows)
				{
					RrVarios.Edit();
					iteration_row_2["fpasfact"] = DBNull.Value;
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
					tempAdapter_2.Update(RrVarios, RrVarios.Tables[0].TableName);
				}
				RrVarios.Close();

				object tempRefParam4 = FechaAlta;
				sqlVarios = "select itiposer,ganoregi,gnumregi,fpeticio,EINTERCO.fpasfact from EINTERCO where EINTERCO.itiposer ='H'" + 
				            " and EINTERCO.ganoregi =" + stGanoadme + " and EINTERCO.gnumregi = " + 
				            stGnumadme + " and EINTERCO.fpasfact=" + Serrores.FormatFechaHMS(tempRefParam4) + "";
				FechaAlta = Convert.ToString(tempRefParam4);
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlVarios, RcAdmision);
				RrVarios = new DataSet();
				tempAdapter_4.Fill(RrVarios);
				foreach (DataRow iteration_row_3 in RrVarios.Tables[0].Rows)
				{
					RrVarios.Edit();
					iteration_row_3["fpasfact"] = DBNull.Value;
					string tempQuery_2 = RrVarios.Tables[0].TableName;
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery_2, "");
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
					tempAdapter_4.Update(RrVarios, RrVarios.Tables[0].TableName);
				}
				RrVarios.Close();

				object tempRefParam5 = FechaAlta;
				sqlVarios = "select itiposer, ganoregi, gnumregi, fpeticio, gprestac, EPRESTAC.fpasfact from EPRESTAC where EPRESTAC.itiposer ='H'" + 
				            " and EPRESTAC.ganoregi =" + stGanoadme + " and EPRESTAC.gnumregi = " + 
				            stGnumadme + " and EPRESTAC.fpasfact=" + Serrores.FormatFechaHMS(tempRefParam5) + "";
				FechaAlta = Convert.ToString(tempRefParam5);
				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sqlVarios, RcAdmision);
				RrVarios = new DataSet();
				tempAdapter_6.Fill(RrVarios);
				foreach (DataRow iteration_row_4 in RrVarios.Tables[0].Rows)
				{
					RrVarios.Edit();
					iteration_row_4["fpasfact"] = DBNull.Value;
					SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_6);
                    tempAdapter_6.Update(RrVarios, RrVarios.Tables[0].TableName);
				}
				RrVarios.Close();


				// OBTENEMOS LOS APUNTES PASADOS A FACTURACION CON EVENTO -> DI (DIETAS)
				object tempRefParam6 = FechaAlta;
				sqlVarios = "select ffinicio from dmovfact where dmovfact.gnumregi=" + stGnumadme + 
				            " and dmovfact.ganoregi = " + stGanoadme + " and dmovfact.fsistema = " + Serrores.FormatFechaHMS(tempRefParam6) + 
				            " and dmovfact.gcodeven in ('DI')" + 
				            " and itiposer ='H' order by ffinicio desc";
				FechaAlta = Convert.ToString(tempRefParam6); // Tiposer ?
				SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(sqlVarios, RcAdmision);
				RrVarios = new DataSet();
				tempAdapter_8.Fill(RrVarios);
				//inicializo la fecha menor de facturacion para saber despues si tenia algun registro
				DtFechaMenorInicioFacturacion = null;
				int tempForVar = RrVarios.Tables[0].Rows.Count;
				for (int iBucleRow = 1; iBucleRow <= tempForVar; iBucleRow++)
				{
					if (iBucleRow == RrVarios.Tables[0].Rows.Count)
					{ // si es la ultima fila quiere decir que ffinicio es la menor
						// porque esta ordenado descendentemente.
						DtFechaMenorInicioFacturacion = RrVarios.Tables[0].Rows[iBucleRow]["ffinicio"];
					}
                    RrVarios.Delete(tempAdapter_8, iBucleRow);
					RrVarios.MoveNext();
				}
				RrVarios.Close();

				if (!Object.Equals(DtFechaMenorInicioFacturacion, null))
				{ // si se ha inicializado actualizar EDIETVIA
					object tempRefParam7 = FechaAlta;
					sqlDietas = " select itiposer,ganoregi,gnumregi,fapuntes,ipaciaco, edietvia.fpasfact from edietvia where edietvia.itiposer = 'H' and " + 
					            " edietvia.gnumregi = " + stGnumadme + " and edietvia.ganoregi = " + stGanoadme + 
					            " and edietvia.fpasfact = " + Serrores.FormatFechaHMS(tempRefParam7) + "";
					FechaAlta = Convert.ToString(tempRefParam7);
					SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(sqlDietas, RcAdmision);
					RrDietas = new DataSet();
					tempAdapter_9.Fill(RrDietas);
					foreach (DataRow iteration_row_5 in RrDietas.Tables[0].Rows)
					{
						RrDietas.Edit();
						iteration_row_5["fpasfact"] = DtFechaMenorInicioFacturacion;
						SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_9);
						tempAdapter_9.Update(RrDietas, RrDietas.Tables[0].TableName);
					}
					RrDietas.Close();
				}
				return result;
			}
			catch
			{
				return false;
			}
		}

		private bool fnExiste_Nido()
		{
			//si existe la tabLA nepisneo
			DataSet tRrN = null;
			string sqlTemp = String.Empty;

			try
			{

				SqlDataAdapter tempAdapter = new SqlDataAdapter("select valfanu1 from sconsglo where gconsglo= 'CODOBSTE'", RcAdmision);
				tRrN = new DataSet();
				tempAdapter.Fill(tRrN);
				if (tRrN.Tables[0].Rows.Count != 0)
				{
					if (Convert.ToString(tRrN.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
					{

						//no tiene por que tener el episodio

						sqlTemp = "Update NEPISNEO Set FALTNIDO = fllegada , " + " NEPISNEO.GANOADNE=aepisadm.ganoadme," + " NEPISNEO.Gnumadne=aepisadm.gnumadme" + " FROM AEPISADM INNER JOIN NEPISNEO ON " + " AEPISADM.Gidenpac=NEPISNEO.Gidenpac " + " WHERE  " + " AEPISADM.GANOADME=" + Conversion.Val(stGanoadme).ToString() + " AND AEPISADM.GNUMADME=" + Conversion.Val(stGnumadme).ToString();
						SqlCommand tempCommand = new SqlCommand(sqlTemp, RcAdmision);
						tempCommand.ExecuteNonQuery();
					}
				}
				tRrN.Close();

				return true;
			}
			catch
			{
				return false;
			}
		}

		//oscar 30/04/04
		private void anu_alta_Load(Object eventSender, EventArgs eventArgs)
		{
			bGestionProd = false;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("Select valfanu1 from sConsGlo where gConsGlo = 'GPRODUCT'", RcAdmision);
			DataSet rrglobal = new DataSet();
			tempAdapter.Fill(rrglobal);
			if (rrglobal.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(rrglobal.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					bGestionProd = true;
				}
			}
			rrglobal.Close();
		}
		//-------
		private void anu_alta_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}