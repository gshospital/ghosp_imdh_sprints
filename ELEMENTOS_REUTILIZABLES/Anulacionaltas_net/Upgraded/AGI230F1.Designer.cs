using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Anulacion
{
	partial class anu_alta
	{

		#region "Upgrade Support "
		private static anu_alta m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static anu_alta DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new anu_alta();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CbAceptar", "CbCerrar", "LbAsegurado", "LbFinanciadora", "LbHistoria", "Label5", "LbNombrePaciente", "Label3", "Label1", "Label28", "Frame2"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton CbAceptar;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadTextBox LbAsegurado;
		public Telerik.WinControls.UI.RadTextBox LbFinanciadora;
		public Telerik.WinControls.UI.RadTextBox LbHistoria;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadTextBox LbNombrePaciente;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(anu_alta));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.CbAceptar = new Telerik.WinControls.UI.RadButton();
			this.CbCerrar = new Telerik.WinControls.UI.RadButton();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.LbAsegurado = new Telerik.WinControls.UI.RadTextBox();
			this.LbFinanciadora = new Telerik.WinControls.UI.RadTextBox();
			this.LbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.LbNombrePaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
			// 
			// CbAceptar
			// 
			this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbAceptar.Location = new System.Drawing.Point(336, 79);
			this.CbAceptar.Name = "CbAceptar";
			this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbAceptar.Size = new System.Drawing.Size(81, 29);
			this.CbAceptar.TabIndex = 10;
			this.CbAceptar.Text = "&Aceptar";
			this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
			// 
			// CbCerrar
			// 
			this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCerrar.Location = new System.Drawing.Point(424, 79);
			this.CbCerrar.Name = "CbCerrar";
			this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCerrar.Size = new System.Drawing.Size(81, 29);
			this.CbCerrar.TabIndex = 9;
			this.CbCerrar.Text = "&Cancelar";
			this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.LbAsegurado);
			this.Frame2.Controls.Add(this.LbFinanciadora);
			this.Frame2.Controls.Add(this.LbHistoria);
			this.Frame2.Controls.Add(this.Label5);
			this.Frame2.Controls.Add(this.LbNombrePaciente);
			this.Frame2.Controls.Add(this.Label3);
			this.Frame2.Controls.Add(this.Label1);
			this.Frame2.Controls.Add(this.Label28);
			this.Frame2.Enabled = true;
			this.Frame2.Location = new System.Drawing.Point(0, 0);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(505, 73);
			this.Frame2.TabIndex = 0;
			this.Frame2.Visible = true;
			// 
			// LbAsegurado
			// 
			this.LbAsegurado.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbAsegurado.Location = new System.Drawing.Point(368, 40);
			this.LbAsegurado.Name = "LbAsegurado";
			this.LbAsegurado.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbAsegurado.Size = new System.Drawing.Size(129, 17);
			this.LbAsegurado.TabIndex = 8;
            this.LbAsegurado.Enabled = false;
            // 
            // LbFinanciadora
            // 
			this.LbFinanciadora.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbFinanciadora.Location = new System.Drawing.Point(104, 40);
			this.LbFinanciadora.Name = "LbFinanciadora";
			this.LbFinanciadora.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbFinanciadora.Size = new System.Drawing.Size(177, 17);
			this.LbFinanciadora.TabIndex = 7;
            this.LbFinanciadora.Enabled = false;
            // 
            // LbHistoria
            // 
			this.LbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbHistoria.Location = new System.Drawing.Point(400, 16);
			this.LbHistoria.Name = "LbHistoria";
			this.LbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbHistoria.Size = new System.Drawing.Size(97, 17);
			this.LbHistoria.TabIndex = 6;
            this.LbHistoria.Enabled = false;
            // 
            // Label5
            // 
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label5.Location = new System.Drawing.Point(320, 16);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(75, 17);
			this.Label5.TabIndex = 5;
			this.Label5.Text = "Historia cl�nica:";
			// 
			// LbNombrePaciente
			// 
			this.LbNombrePaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbNombrePaciente.Location = new System.Drawing.Point(64, 16);
			this.LbNombrePaciente.Name = "LbNombrePaciente";
			this.LbNombrePaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbNombrePaciente.Size = new System.Drawing.Size(249, 17);
			this.LbNombrePaciente.TabIndex = 4;
            this.LbNombrePaciente.Enabled = false;
            // 
            // Label3
            // 
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.Location = new System.Drawing.Point(288, 40);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(75, 17);
			this.Label3.TabIndex = 3;
			this.Label3.Text = "N� asegurado:";
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(8, 40);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(91, 17);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "Ent. Financiadora:";
			// 
			// Label28
			// 
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label28.Location = new System.Drawing.Point(8, 16);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(61, 17);
			this.Label28.TabIndex = 1;
			this.Label28.Text = "Paciente:";
			// 
			// anu_alta
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(508, 112);
			this.Controls.Add(this.CbAceptar);
			this.Controls.Add(this.CbCerrar);
			this.Controls.Add(this.Frame2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "anu_alta";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Anulaci�n del alta - AGI230F1";
			this.Activated += new System.EventHandler(this.anu_alta_Activated);
			this.Closed += new System.EventHandler(this.anu_alta_Closed);
			this.Load += new System.EventHandler(this.anu_alta_Load);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}