using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
namespace Anulacion
{
	public class Alta
	{


		public void recibe_paciente(Anulacion.Alta ParaClase, dynamic ParaFormulario, ref SqlConnection ParaConexion, string ParaNombre, string ParaNumase, string ParaSociedad, string ParaHistoria, string ParaGidenpac, string Parausuario, string ParaArea)
		{

			Anulacion.anu_alta instancia = new Anulacion.anu_alta();
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref ParaConexion);
			Serrores.AjustarRelojCliente(ParaConexion);
			instancia.recoge_paciente(ParaFormulario, ParaConexion, ParaNombre, ParaNumase, ParaSociedad, ParaHistoria, ParaGidenpac, Parausuario, ParaArea);
			instancia.ShowDialog();
		}
	}
}