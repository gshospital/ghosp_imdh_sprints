using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Microsoft.CSharp;
using ElementosCompartidos;

namespace GRAFICADLL
{
	internal static class MGRAFICA
	{
        //INIC SDSALAZAR SPRIN 5_5
        ////UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        //public static Database bs = null;
        ////UPGRADE_ISSUE: (2068) Recordset object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        //public static Recordset Rs = null;
        //INIC SDSALAZAR SPRIN 5_5
        public static SqlConnection RcEnfermeria = null;
		public static string vstA�oAdmi = String.Empty;
		public static string vstNumAdmi = String.Empty;
		public static string stTipSer = String.Empty;
		public static object oCrystal = null;
		public static string stNombreBD = String.Empty;
		public static string PathReport = String.Empty;
		public static string stDSN_ACCESS = String.Empty;
		public static string stPathAplicacion = String.Empty;
		public static string gUsuario = String.Empty;
		public static string TOrigen = String.Empty;
		public static string stGidenpac = String.Empty;
		public struct DEVMODE
		{
			public short dmOrientation;
		}

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", EntryPoint = "MapVirtualKeyA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int MapVirtualKey(int wCode, int wMapType);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static void Sleep(int dwMilliseconds);

		public const int DMORIENT_LANDSCAPE = 2;
		public const int VK_MENU = 0x12;
		public const int VK_SNAPSHOT = 0x2C;
		public const int KEYEVENTF_KEYUP = 0x2;
		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public string VstPoblaciHo;
			public bool VfCabecera;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					result.VstPoblaciHo = String.Empty;
					return result;
			}
		}
		public static MGRAFICA.CabCrystal VstrCrystal = MGRAFICA.CabCrystal.CreateInstance();
		public static Mensajes.ClassMensajes clase_mensaje = null;

		public static string stFechaMinimaIntervalo = String.Empty; //fecha/hora inicio intervalo
		public static string stFechaMaximaIntervalo = String.Empty; //fecha/hora fin intervalo

		//oscar 11/05/04
		//variables para poder obtener las graficas de todos los pacientes
		public static string stUnidadEnf = String.Empty;
		public static bool bMasivo = false;
		public static string intdestino = String.Empty;
		public static Form objFormulario = null;
		//-------

		//OSCAR C 28/07/2005
		public static int vminTramo1 = 0;
		public static int vmaxTramo1 = 0;
		public static int vminTramo2 = 0;
		public static int vmaxTramo2 = 0;
		public static int vminTramo3 = 0;
		public static int vmaxTramo3 = 0;
		public static int vminTramo4 = 0;
		public static int vmaxTramo4 = 0;
		public static int vminTramo5 = 0;
		public static int vmaxTramo5 = 0;
		public static int vminTramo6 = 0;
		public static int vmaxTramo6 = 0;
		//------------


		public static string strAlfa1 = String.Empty;
		public static string strAlfa2 = String.Empty;
		public static string strAlfa3 = String.Empty;
		public static int intNume1 = 0;
		public static int intNume2 = 0;





		internal static void vacia_formulas(dynamic listado, int Numero)
		{
			for (int tiForm = 0; tiForm <= Numero; tiForm++)
			{
				listado.Formulas[tiForm] = "";
			}
		}
		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************

			//On Error GoTo error
			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RcEnfermeria);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			VstrCrystal.VstGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, RcEnfermeria);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			VstrCrystal.VstNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, RcEnfermeria);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			VstrCrystal.VstDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu2 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tstCab, RcEnfermeria);
			tRrCrystal = new DataSet();
			tempAdapter_4.Fill(tRrCrystal);
			VstrCrystal.VstPoblaciHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU2"]).Trim();
			tRrCrystal.Close();

		}
		internal static string proObtenerPaciente(string stAno, string stNumero)
		{
			// On Error GoTo error
			string result = String.Empty;
			string tstCab = "SELECT dape1pac, dape2pac, dnombpac FROM dpacient, " + TOrigen + " A WHERE  ";
			if (stTipSer == "H")
			{
				tstCab = tstCab + " A.ganoadme = " + stAno + " and A.gnumadme = " + stNumero + " and";
			}
			else if (stTipSer == "U")
			{ 
				tstCab = tstCab + " A.ganourge = " + stAno + " and A.gnumurge = " + stNumero + " and";
			}
			else if (stTipSer == "Q")
			{ 
				tstCab = tstCab + " A.ganoregi = " + stAno + " and A.gnumregi = " + stNumero + " and";
			}
			tstCab = tstCab + " A.gidenpac = dpacient.gidenpac";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RcEnfermeria);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			if (tRrCrystal.Tables[0].Rows.Count != 0)
			{
				result = (Convert.ToString(tRrCrystal.Tables[0].Rows[0]["dape1pac"]).Trim() != "") ? Convert.ToString(tRrCrystal.Tables[0].Rows[0]["dape1pac"]).Trim() : "";
				result = (!Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["dape2pac"])) ? result + " " + Convert.ToString(tRrCrystal.Tables[0].Rows[0]["dape2pac"]).Trim() : result;
				return (!Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["dnombpac"])) ? result + ", " + Convert.ToString(tRrCrystal.Tables[0].Rows[0]["dnombpac"]).Trim() : result;
			}
			else
			{
				return "";
			}
			return result;
		}
		internal static string proObtenerCama(string stAno, string stNumero)
		{
			string tstCab = String.Empty;
			if (stTipSer == "H")
			{
				tstCab = "select gplantas, ghabitac, gcamasbo" + " from " + TOrigen + " A, dcamasbo Where  " + " A.ganoadme = " + stAno + " and " + " A.gnumadme = " + stNumero + " and " + " A.gidenpac = dcamasbo.gidenpac AND " + " dcamasbo.iestcama = 'O'";
			}
			else if (stTipSer == "U")
			{ 
				tstCab = "select gplantas, ghabitac, gcamasbo" + " from " + TOrigen + " A Where  ";
				tstCab = tstCab + " A.ganourge = " + stAno + " and " + " A.gnumurge = " + stNumero + "";
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RcEnfermeria);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			if (tRrCrystal.Tables[0].Rows.Count != 0)
			{
				return ((!Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["gplantas"])) ? StringsHelper.Format(tRrCrystal.Tables[0].Rows[0]["gplantas"], "00") : "") + ((!Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["ghabitac"])) ? StringsHelper.Format(tRrCrystal.Tables[0].Rows[0]["ghabitac"], "00") : "") + ((!Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["gcamasbo"])) ? Convert.ToString(tRrCrystal.Tables[0].Rows[0]["gcamasbo"]).Trim() : "");
			}
			else
			{
				return "";
			}
		}
		internal static string proObtenerGidenpac(string stAno, string stNumero)
		{
			string tstCab = String.Empty;
			if (stTipSer == "H")
			{
				tstCab = "SELECT gidenpac FROM " + TOrigen + " A WHERE  A.ganoadme = " + stAno + " and A.gnumadme = " + stNumero + " ";
			}
			else if (stTipSer == "U")
			{ 
				tstCab = "SELECT gidenpac FROM " + TOrigen + " A WHERE  A.ganourge = " + stAno + " and A.gnumurge = " + stNumero + " ";
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RcEnfermeria);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			if (tRrCrystal.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				return Convert.ToString(tRrCrystal.Tables[0].Rows[0]["gidenpac"]).Trim();
			}
			else
			{
				return "";
			}
		}

		//oscar 04/02/04
		//Funcion que rellena el spread del formulario que recibe.
		//Puede rellenarlo diariamente ("D") u Horariamente ("H")
		//Datos mostrados:
		//   -Glucosa : Ultimo apunte realizado del dia / hora
		//   -Peso / Talla: Ultimo apunte realizado de peso y ultimo apunte realizado de talla del dia / hora
		//   -Diuresis: Sumatorio de diuresis del dia / hora
		//10/05/04
		//   -Drenaje: Sumatorio de drenajes del dia / hora
		//   -Deposiciones: Sumatorio de deposiciones del dia / hora
		//----
		//   -Balance: sumatorio de balance del dia / hora (ganancias - perdidas)

		//************************************************************************************************************
		//*                                                                                                          *
		//*  Modificaciones:                                                                                         *
		//*                                                                                                          *
		//*      O.Frias 26/03/2007 El valor de glucosa es parametrizable ( IGLUCEMI )                               *
		//*                                                                                                          *
		//*          1. Si no hay constante o el valfanu1 = N, la operativa no cambia.                               *
		//*          2. Si valfanu1 = S y valfanu2 = S, la primera glucosa del d�a                                   *
		//*          3. Si valfanu1 = S y valfanu2 = S y valfanu3 esta relleno, el caption de la columna de glucosa  *
		//*             ser� la descripci�n del perfil.
		//*          4. Si valfanu1 = S y valfanu2 = S y valfanu3 esta vacio, el primer apunte de glucosa del d�a en *
		//*             Intervalo de horas definido en nnumeri1 y nnumeri2                                           *
		//*          5. Si valfanu1 = S y el paciente no tiene apunte que cumpla las condiciones no se mostrar� nada *
		//************************************************************************************************************

		internal static void rellenaSpread(dynamic Formulario, string tipo)
		{
			StringBuilder sql = new StringBuilder();
			int col = 0;
			DataSet Rr = null;
			string stPeso = String.Empty;
			string stTalla = String.Empty;


			//***************** O.Frias - 26/03/2007 *****************
			CargaValores();
			//***************** O.Frias - 26/03/2007 *****************

			string vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(RcEnfermeria);

			//********************* O.Frias - 26/03/2007 *********************
			//lo comento para que siempre salga glucosa, para saber que perfil esta por constante global
			//If strAlfa3 <> "" And strAlfa1 = "S" And strAlfa2 = "S" Then
			//    Formulario.vsDatos.col = 0
			//    Formulario.vsDatos.Row = 1
			//    Formulario.vsDatos.Text = fPerfil(strAlfa3)
			//End If
			//********************* O.Frias - 26/03/2007 *********************

			for (col = 1; col <= Convert.ToInt32(Formulario.vsDatos.MaxCols); col++)
			{
				for (int i = 1; i <= Convert.ToInt32(Formulario.vsDatos.MaxRows); i++)
				{
					Formulario.vsDatos.col = col;
					Formulario.vsDatos.Row = i;
					Formulario.vsDatos.Text = "";
				}
			}

			col = 2;
			string Hora = String.Empty;
			string FechaIni = String.Empty;
			string FechaFin = String.Empty;
			for (int i = 30; i <= 36; i++)
			{

				if (Convert.ToString(Formulario.Label4(i).Caption) == "")
				{
					return;
				}

				//CLUCOSA
				sql = new StringBuilder("SELECT cglucemi FROM EGLUCEMI " + 
				      "WHERE itiposer = '" + stTipSer + "' AND " + 
				      "ganoregi=" + Convert.ToString(Formulario.ganoadme) + " AND " + 
				      "gnumregi=" + Convert.ToString(Formulario.gnumadme) + " AND " + 
				      "cglucemi is not null ");

				switch(tipo)
				{
					case "H" : 
						sql.Append("   and fapuntes >= '" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":00:00' " + 
						           "   and fapuntes <='" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":59:59' "); 
						break;
					case "D" : 
						//********************* O.Frias - 26/03/2007 ********************* 
						if (strAlfa3 != "" && strAlfa1 == "S" && strAlfa2 == "S")
						{
							sql.Append(" AND itipocom = '" + strAlfa3 + "'");
						} 
						 
						//********************* O.Frias - 26/03/2007 ********************* 
						if (strAlfa1 == "S" && strAlfa2 == "S" && strAlfa3 == "" && intNume1 >= 0 && intNume2 >= 0)
						{

							sql.Append(" and (fapuntes >= '" + Convert.ToDateTime(Formulario.Label4(i).Caption).ToString("MM/dd/yyyy") + " 00:00' " + 
							           " and fapuntes <='" + Convert.ToDateTime(Formulario.Label4(i).Caption).ToString("MM/dd/yyyy") + " 23:59:59' ) And " + 
							           "(CONVERT (VARCHAR (5),hcontglu,114) >= '" + StringsHelper.Format(intNume1, "00") + ":00' And " + 
							           " CONVERT (VARCHAR (5),hcontglu,114) <= '" + StringsHelper.Format(intNume2, "00") + ":59' )");

						}
						else
						{
							sql.Append("   and fapuntes >= '" + Convert.ToDateTime(Formulario.Label4(i).Caption).ToString("MM/dd/yyyy") + " 00:00' " + 
							           "   and fapuntes <='" + Convert.ToDateTime(Formulario.Label4(i).Caption).ToString("MM/dd/yyyy") + " 23:59:59' ");
						} 

						 
						break;
				}
				sql.Append(" order by fapuntes");

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql.ToString(), RcEnfermeria);
				Rr = new DataSet();
				tempAdapter.Fill(Rr);
				if (Rr.Tables[0].Rows.Count != 0)
				{
					//********************* O.Frias - 26/03/2007 *********************
					if (strAlfa1 == "S" && strAlfa2 == "S")
					{
						Rr.MoveFirst();
					}
					else
					{
						Rr.MoveLast(null);
					}

					Formulario.vsDatos.col = col;
					Formulario.vsDatos.Row = 1;
					Formulario.vsDatos.Text = Rr.Tables[0].Rows[0][0];
				}
				Rr.Close();

				//PESO Y TALLA
				stPeso = "";
				stTalla = "";
				sql = new StringBuilder("select cpesopac from emedidas " + 
				      " where itiposer = '" + stTipSer + "' " + 
				      "   and ganoregi=" + Convert.ToString(Formulario.ganoadme) + " " + 
				      "   and gnumregi=" + Convert.ToString(Formulario.gnumadme) + " " + 
				      "   and cpesopac is not null ");
				switch(tipo)
				{
					case "D" : 
						sql.Append("   and fapuntes >= '" + Convert.ToDateTime(Formulario.Label4(i).Caption).ToString("MM/dd/yyyy") + " 00:00' " + 
						           "   and fapuntes <='" + Convert.ToDateTime(Formulario.Label4(i).Caption).ToString("MM/dd/yyyy") + " 23:59:59' "); 
						break;
					case "H" : 
						sql.Append("   and fapuntes >= '" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":00:00' " + 
						           "   and fapuntes <='" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":59:59' "); 
						break;
				}
				sql.Append(" order by fapuntes");
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql.ToString(), RcEnfermeria);
				Rr = new DataSet();
				tempAdapter_2.Fill(Rr);
				if (Rr.Tables[0].Rows.Count != 0)
				{
					Rr.MoveLast(null);
					string tempRefParam = (Convert.ToDouble(Rr.Tables[0].Rows[0]["cpesopac"]) / 1000).ToString();
					stPeso = Serrores.ConvertirDecimales(ref tempRefParam, ref vstSeparadorDecimal, 2);
				}
				Rr.Close();

				sql = new StringBuilder("select ctallapac from emedidas " + 
				      " where itiposer = '" + stTipSer + "' " + 
				      "   and ganoregi=" + Convert.ToString(Formulario.ganoadme) + " " + 
				      "   and gnumregi=" + Convert.ToString(Formulario.gnumadme) + " " + 
				      "   and ctallapac is not null ");
				switch(tipo)
				{
					case "D" : 
						sql.Append("   and fapuntes >= '" + Convert.ToDateTime(Formulario.Label4(i).Caption).ToString("MM/dd/yyyy") + " 00:00' " + 
						           "   and fapuntes <='" + Convert.ToDateTime(Formulario.Label4(i).Caption).ToString("MM/dd/yyyy") + " 23:59:59' "); 
						break;
					case "H" : 
						sql.Append("   and fapuntes >= '" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":00:00' " + 
						           "   and fapuntes <='" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":59:59' "); 
						break;
				}
				sql.Append(" order by fapuntes");
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql.ToString(), RcEnfermeria);
				Rr = new DataSet();
				tempAdapter_3.Fill(Rr);
				if (Rr.Tables[0].Rows.Count != 0)
				{
					Rr.MoveLast(null);
					string tempRefParam2 = Convert.ToString(Rr.Tables[0].Rows[0]["ctallapac"]);
					stTalla = Serrores.ConvertirDecimales(ref tempRefParam2, ref vstSeparadorDecimal, 1);
				}
				Rr.Close();

				Formulario.vsDatos.col = col;
				Formulario.vsDatos.Row = 2;
				Formulario.vsDatos.Text = (stPeso + "/" + stTalla == "/") ? "" : stPeso + "/" + stTalla;


				//MODIFICACION OSCAR C 16/11/2005
				//Ahora para la grafica DIARIA, el calculo de la DIURESIS, DRENAJES, DEPOSICIONES y
				//el calculo del BALANCE DIARIO, dependera de la constante global HBALANCE
				//VALFANU1: Indica Hora de ruptura para el calculo
				//Ej.
				//Si tiene 07:00 Indica que el Balance Diario ... sera
				//el sumatorio de apuntes de balance entre fecha grafica >= 7:00 y el dia posterior  < 7:00
				if (tipo == "D")
				{
					Hora = " 00:00";
					sql = new StringBuilder("SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='HBALANCE'");
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql.ToString(), RcEnfermeria);
					Rr = new DataSet();
					tempAdapter_4.Fill(Rr);
					if (Rr.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(Rr.Tables[0].Rows[0]["valfanu1"]))
						{
							if (Information.IsDate(Convert.ToString(Rr.Tables[0].Rows[0]["valfanu1"]).Trim()))
							{
								Hora = " " + Convert.ToString(Rr.Tables[0].Rows[0]["valfanu1"]).Trim() + ":00";
							}
						}
					}
					FechaIni = Convert.ToString(Formulario.Label4(i).Caption) + Hora;
					FechaFin = DateTimeHelper.ToString(DateTime.Parse(FechaIni).AddDays(1));
					Rr.Close();
				}
				//------------------

				//DIURESIS (sumatorio de las diuresis del rango (1 dia o 1 h)
				//DRENAJES (Sumatorio de drenajes del dia / hora)
				//DEPOSICIONES (Sumatorio de deposiciones del dia / hora)
				sql = new StringBuilder("select ISNULL(SUM(cperdiur),0) diuresis," + 
				      "       ISNULL(SUM(cdrenaje),0) drenajes," + 
				      "       ISNULL(SUM(nvechece),0) deposiciones" + 
				      " from ebalance " + 
				      " where itiposer = '" + stTipSer + "' " + 
				      "   and ganoregi=" + Convert.ToString(Formulario.ganoadme) + " " + 
				      "   and gnumregi=" + Convert.ToString(Formulario.gnumadme) + " ");
				switch(tipo)
				{
					case "D" : 
						//sql = sql & "   and fapuntes >= '" & Format(Formulario.Label4(i).Caption, "mm/dd/yyyy") & " 00:00' " & _
						//"   and fapuntes <='" & Format(Formulario.Label4(i).Caption, "mm/dd/yyyy") & " 23:59:59' " 
						System.DateTime TempDate2 = DateTime.FromOADate(0); 
						System.DateTime TempDate = DateTime.FromOADate(0); 
						sql.Append("   and fapuntes >= '" + ((DateTime.TryParse(FechaIni, out TempDate)) ? TempDate.ToString("MM/dd/yyyy HH:mm:ss") : FechaIni) + "' " + 
						           "   and fapuntes < '" + ((DateTime.TryParse(FechaFin, out TempDate2)) ? TempDate2.ToString("MM/dd/yyyy HH:mm:ss") : FechaFin) + "' "); 
						break;
					case "H" : 
						sql.Append("   and fapuntes >= '" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":00:00' " + 
						           "   and fapuntes <='" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":59:59' "); 
						break;
				}
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql.ToString(), RcEnfermeria);
				Rr = new DataSet();
				tempAdapter_5.Fill(Rr);
				if (Rr.Tables[0].Rows.Count != 0)
				{
					Formulario.vsDatos.col = col;
					Formulario.vsDatos.Row = 3;
					Formulario.vsDatos.Text = (Convert.ToDouble(Rr.Tables[0].Rows[0]["diuresis"]) == 0) ? "" : Rr.Tables[0].Rows[0]["diuresis"];
					//10/05/04
					Formulario.vsDatos.Row = 4;
					Formulario.vsDatos.Text = (Convert.ToDouble(Rr.Tables[0].Rows[0]["drenajes"]) == 0) ? "" : Rr.Tables[0].Rows[0]["drenajes"];
					Formulario.vsDatos.Row = 5;
					Formulario.vsDatos.Text = (Convert.ToDouble(Rr.Tables[0].Rows[0]["deposiciones"]) == 0) ? "" : Rr.Tables[0].Rows[0]["deposiciones"];
					//--------
				}
				Rr.Close();


				//BALANCE DIARIO (sumatorio de las ganancias - sumatorio de las perdidas del dia o de la hora)
				sql = new StringBuilder(" select " + 
				      " ISNULL(sum(isnull(cganoral,0)+isnull(cganendo,0)+isnull(cotragan,0)+isnull(cgantran,0))- " + 
				      "        Sum(IsNull(cperdiur,0)+IsNull(cperaspi,0)+IsNull(cpervomi,0)+isNull(cperhece,0)+IsNull(cdrenaje,0)+IsNull(cotraper,0)), 0) Balance" + 
				      " from EBALANCE where " + 
				      " itiposer = '" + stTipSer + "'" + 
				      " and ganoregi = " + Convert.ToString(Formulario.ganoadme) + 
				      " and gnumregi = " + Convert.ToString(Formulario.gnumadme));
				switch(tipo)
				{
					case "D" : 
						System.DateTime TempDate4 = DateTime.FromOADate(0); 
						System.DateTime TempDate3 = DateTime.FromOADate(0); 
						sql.Append("   and fapuntes >= '" + ((DateTime.TryParse(FechaIni, out TempDate3)) ? TempDate3.ToString("MM/dd/yyyy HH:mm:ss") : FechaIni) + "' " + 
						           "   and fapuntes < '" + ((DateTime.TryParse(FechaFin, out TempDate4)) ? TempDate4.ToString("MM/dd/yyyy HH:mm:ss") : FechaFin) + "' "); 
						break;
					case "H" : 
						sql.Append("   and fapuntes >= '" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":00:00' " + 
						           "   and fapuntes <='" + Convert.ToDateTime(Formulario.LbFechaGrafica.Caption).ToString("MM/dd/yyyy") + " " + Convert.ToString(Formulario.Label4(i).Caption).Substring(0, Math.Min(2, Convert.ToString(Formulario.Label4(i).Caption).Length)) + ":59:59' "); 
						break;
				}
				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql.ToString(), RcEnfermeria);
				Rr = new DataSet();
				tempAdapter_6.Fill(Rr);
				if (Rr.Tables[0].Rows.Count != 0)
				{

					Formulario.vsDatos.col = col;

					Formulario.vsDatos.Row = 6; //4
					string tempRefParam3 = Convert.ToString(Rr.Tables[0].Rows[0]["Balance"]);
					Formulario.vsDatos.Text = (Convert.ToDouble(Rr.Tables[0].Rows[0]["Balance"]) == 0) ? "" : Serrores.ConvertirDecimales(ref tempRefParam3, ref vstSeparadorDecimal, 2);

					if (Convert.ToDouble(Rr.Tables[0].Rows[0]["Balance"]) < 0)
					{
						Formulario.vsDatos.ForeColor = 0xFF; //rojo
					}
					else
					{
						Formulario.vsDatos.ForeColor = 0x80000012; //negro
					}

				}
				Rr.Close();

				col++;

			}
		}

		//********************************************************************************************
		//*  Procedimiento: oculta                                                                   *
		//*                                                                                          *
		//*  Objeto: En el caso de no estar selecionada grafica oculta sus labels correspondientes   *
		//*                                                                                          *
		//*  Modificaciones:                                                                         *
		//*                                                                                          *
		//*      28/09/2006 - O.Frias                                                                *
		//*                                                                                          *
		//*          * Se incorpora la posible ocultaci�n de los datos de Dolor (EVA(                *
		//*                                                                                          *
		//********************************************************************************************


		internal static void oculta(dynamic Formulario)
		{
			//Frecuenca Respiratoria
			Formulario.Label1(3).Visible = HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked;
			Formulario.Label1(31).Visible = HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked;
			Formulario.Label1(35).Visible = HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked;
			Formulario.Label1(4).Visible = HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked;
			Formulario.Label1(8).Visible = HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked;
			Formulario.Label1(12).Visible = HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked;
			Formulario.Label1(16).Visible = HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked;
			Formulario.Label1(20).Visible = HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked;
			Formulario.Label1(24).Visible = HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked;

			//Tension Arterial
			Formulario.Label1(2).Visible = HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked;
			Formulario.Label1(30).Visible = HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked;
			Formulario.Label1(34).Visible = HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked;
			Formulario.Label1(5).Visible = HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked;
			Formulario.Label1(9).Visible = HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked;
			Formulario.Label1(13).Visible = HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked;
			Formulario.Label1(17).Visible = HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked;
			Formulario.Label1(21).Visible = HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked;
			Formulario.Label1(25).Visible = HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked;

			//Pulso
			Formulario.Label1(1).Visible = HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked;
			Formulario.Label1(29).Visible = HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked;
			Formulario.Label1(33).Visible = HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked;
			Formulario.Label1(6).Visible = HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked;
			Formulario.Label1(10).Visible = HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked;
			Formulario.Label1(14).Visible = HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked;
			Formulario.Label1(18).Visible = HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked;
			Formulario.Label1(22).Visible = HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked;
			Formulario.Label1(26).Visible = HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked;

			//Temperatura
			Formulario.Label1(0).Visible = HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked;
			Formulario.Label1(28).Visible = HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked;
			Formulario.Label1(32).Visible = HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked;
			Formulario.Label1(7).Visible = HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked;
			Formulario.Label1(11).Visible = HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked;
			Formulario.Label1(15).Visible = HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked;
			Formulario.Label1(19).Visible = HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked;
			Formulario.Label1(23).Visible = HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked;
			Formulario.Label1(27).Visible = HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked;

			//Balance
			Formulario.Label1(36).Visible = HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked;
			Formulario.Label1(37).Visible = HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked;
			Formulario.Label1(38).Visible = HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked;
			Formulario.Label1(39).Visible = HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked;
			Formulario.Label1(40).Visible = HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked;
			Formulario.Label1(41).Visible = HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked;
			Formulario.Label1(42).Visible = HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked;
			Formulario.Label1(43).Visible = HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked;
			Formulario.Label1(44).Visible = HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked;

			//************* O.Frias -- 28/09/2006 *********************
			//Dolor Eva
			Formulario.Label1(45).Visible = HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked;
			Formulario.Label1(47).Visible = HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked;
			Formulario.Label1(48).Visible = HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked;
			Formulario.Label1(49).Visible = HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked;
			Formulario.Label1(50).Visible = HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked;
			Formulario.Label1(51).Visible = HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked;
			Formulario.Label1(52).Visible = HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked;
			Formulario.Label1(53).Visible = HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked;
			Formulario.Picture1.Visible = HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked;
			//************* O.Frias -- 28/09/2006 *********************
		}
		//-----


		//UPGRADE_NOTE: (7001) The following declaration (fPerfil) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string fPerfil(string strPerfil)
		//{
				//
				//string result = String.Empty;
				//result = "";
				//
				//string strSql = "SELECT  * FROM DTIPOCOM " + 
				//                "WHERE fborrado is null AND " + 
				//                "itipocom = '" + strPerfil + "'";
				//
				//SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, RcEnfermeria);
				//DataSet rdoTemp = new DataSet();
				//tempAdapter.Fill(rdoTemp);
				//if (rdoTemp.Tables[0].Rows.Count != 0)
				//{
					////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//result = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dtipocom"]).Trim();
				//}
				////UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//rdoTemp.Close();
				//return result;
		//}

		private static void CargaValores()
		{

			string strSql = "SELECT * FROM SCONSGLO WHERE gconsglo = 'IGLUCEMI'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, RcEnfermeria);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				strAlfa1 = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["valfanu1"])) ? "N" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]).Trim();
				strAlfa2 = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["valfanu2"])) ? "N" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu2"]).Trim();
				strAlfa3 = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["valfanu3"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu3"]).Trim();
				intNume1 = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["nnumeri1"])) ? 0 : Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["nnumeri1"]);
				intNume2 = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["nnumeri2"])) ? 0 : Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["nnumeri2"]);

				if (strAlfa1 != "S" && strAlfa1 != "N")
				{
					strAlfa1 = "N";
				}
				if (strAlfa2 != "S" && strAlfa2 != "N")
				{
					strAlfa2 = "N";
				}

				if (intNume1 < 0 || intNume1 > 23)
				{
					strAlfa1 = "N";
					intNume1 = 0;
				}
				if (intNume2 < 0 || intNume2 > 23)
				{
					strAlfa1 = "N";
					intNume2 = 0;
				}
				if (intNume1 > intNume2)
				{
					strAlfa1 = "N";
					intNume1 = 0;
					intNume2 = 0;
				}

			}
			else
			{
				strAlfa1 = "N";
				strAlfa2 = "N";
				strAlfa3 = "";
				intNume1 = 0;
				intNume2 = 0;
			}
			rdoTemp.Close();

		}
	}
}