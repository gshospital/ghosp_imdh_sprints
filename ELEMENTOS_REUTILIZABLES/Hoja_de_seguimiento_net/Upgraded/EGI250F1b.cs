using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace GRAFICADLL
{
	public partial class GHORARIA_SH
		: RadForm
	{

		string stsql = String.Empty;
		DataSet rsmagnitud = null;
		int p = 0;
		System.DateTime fecha_epi = DateTime.FromOADate(0);
		string hora_epi = String.Empty;
		string minutos_epi = String.Empty;
		float x1 = 0;
		float y1 = 0;
		float x2 = 0;
		float y2 = 0;
		int i = 0;
		int D = 0;
		int j = 0;
		MGRAFICA.DEVMODE dmout = new MGRAFICA.DEVMODE();
		int IsWindows95 = 0;

		string stInicioIntervalo = String.Empty; //fecha/hora inicio intervalo
		string stFinIntervalo = String.Empty; //fecha/hora fin intervalo
		string stHoraIni = String.Empty; //hora inicio
		string stHoraFin = String.Empty; //hora fin
		double AuGraf = 0;
		bool LinRef = false;

		//Valgris es una variable que se utiliza para colocar el punto de la grafica en el lugar exacto.
		//Por ejemplo: en el caso de la frecuencia de respiracion (R) la grafica aumenta en tramos de 10 en 10
		//(diez valores enteros en el tramo) y sabemos que en cada tramo caben 10 lineas horizontales,
		//por lo que en este caso valgris vale 10/10=1
		//En el caso del pulso la grafica aumenta de 20 en 20 (veinte valores enteros en el tramo) y sabemos
		//que en cada tramo existen 10 lineas horizontales, por lo que en este caso valgris valdra 20/10=2
		//En el caso de la temperatura la grafica aumenta de 10 en 10 (centigrados) y sabemos que en cada tramo existen 10
		//lineas horizontales, por lo que en este caso valgris valdra 10/10=1
		//...y asi con los demas datos de la grafica
		double valgris = 0;

		//la identificacion del episodio se traslada a nivel de formulario desde el mgrafica.bas
		//para poder tener graficas en pantallas individuales de distintos episodios.
		public int ganoadme = 0;
		public int gnumadme = 0;

		DataSet RRhorasApintar = null;
		int intPaginasAPintar = 0; //caben 7 columnas por pagina
		int intPaginaActual = 0; //Indicador de pagina
		int intContadorLeidosXPagina = 0;
		string sqlWhereHoras = String.Empty;


		//****************************************************************************************
		//*  Procedimiento: Command1_Click                                                       *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias (17/10/2006)                                                              *
		//*                                                                                      *
		//*      - Realizo la llamada a DolorEva para realizar la carga de la grafica al ir a    *
		//*        la pagina anterior                                                            *
		//*                                                                                      *
		public GHORARIA_SH()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}


		//****************************************************************************************
		private void Command1_Click(Object eventSender, EventArgs eventArgs)
		{

			intPaginaActual--;

			for (i = 30; i <= 36; i++)
			{
				Label4[i].Text = "";
			}

			//UPGRADE_WARNING: (2080) QBCOlor(15) was upgraded to System.Drawing.Color.White and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
			graficaenfer.BackColor = Color.White;
			lineas();

			for (i = 0; i <= intContadorLeidosXPagina + 7; i++)
			{
				RRhorasApintar.MovePrevious();
				if (RRhorasApintar.Tables[0].Rows.Count != 0)
				{
					RRhorasApintar.MoveFirst();
					break;
				}
			}

			i = 0;
			intContadorLeidosXPagina = 0;

			foreach (DataRow iteration_row in RRhorasApintar.Tables[0].Rows)
			{
				if (i <= 6)
				{
					Label4[30 + i].Text = StringsHelper.Format(iteration_row[0], "00") + ":00";
					intContadorLeidosXPagina++;
					i++;
				}
				else
				{
					break;
				}
			}

			sqlWhereHoras = "";
			System.DateTime TempDate = DateTime.FromOADate(0);
			stInicioIntervalo = ((DateTime.TryParse(MGRAFICA.stFechaMinimaIntervalo, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : MGRAFICA.stFechaMinimaIntervalo) + " " + Label4[30].Text + ":00";
			for (i = 30; i <= 36; i++)
			{
				if (Label4[i].Text != "")
				{
					System.DateTime TempDate2 = DateTime.FromOADate(0);
					stFinIntervalo = ((DateTime.TryParse(MGRAFICA.stFechaMinimaIntervalo, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy") : MGRAFICA.stFechaMinimaIntervalo) + " " + Label4[i].Text.Substring(0, Math.Min(2, Label4[i].Text.Length)) + ":59:59";
					sqlWhereHoras = sqlWhereHoras + "'" + Label4[i].Text.Substring(0, Math.Min(2, Label4[i].Text.Length)) + "',";
				}
			}

			sqlWhereHoras = sqlWhereHoras.Substring(0, Math.Min(sqlWhereHoras.Length - 1, sqlWhereHoras.Length));

			//graficaenfer.BackColor = QBColor(15)
			//lineas

			if (HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked)
			{
				frecuen();
			}
			if (HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked)
			{
				Tension();
			}
			if (HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked)
			{
				Pulso();
			}
			if (HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked)
			{
				Temperatura();
			}
			if (HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked)
			{
				balance();
			}

			//************** O.Frias - 17/10/2006 - Inicio ****************
			if (HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked)
			{
				DolorEva();
			}
			//************** O.Frias - 17/10/2006 - Fin ******************

			MGRAFICA.rellenaSpread(this, "H");

			this.Command2.Enabled = true;
			if (intPaginaActual == 1)
			{
				this.Command1.Enabled = false;
			}
			this.Command2.Enabled = intPaginaActual < intPaginasAPintar;

		}


		//****************************************************************************************
		//*  Procedimiento: Command2_Click                                                       *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias (17/10/2006)                                                              *
		//*                                                                                      *
		//*      - Realizo la llamada a DolorEva para realizar la carga de la grafica al ir a    *
		//*        la siguiente pagina                                                           *
		//*                                                                                      *
		//****************************************************************************************
		private void Command2_Click(Object eventSender, EventArgs eventArgs)
		{

			intPaginaActual++;

			for (i = 30; i <= 36; i++)
			{
				Label4[i].Text = "";
			}

			i = 0;
			intContadorLeidosXPagina = 0;

			//RRhorasApintar.MoveNext
			//If RRhorasApintar.EOF Then Exit Sub
			//UPGRADE_WARNING: (2080) QBCOlor(15) was upgraded to System.Drawing.Color.White and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
			graficaenfer.BackColor = Color.White;
			lineas();

			foreach (DataRow iteration_row in RRhorasApintar.Tables[0].Rows)
			{
				if (i <= 6)
				{
					Label4[30 + i].Text = StringsHelper.Format(iteration_row[0], "00") + ":00";
					intContadorLeidosXPagina++;
					i++;
				}
				else
				{
					break;
				}
			}

			sqlWhereHoras = "";
			System.DateTime TempDate = DateTime.FromOADate(0);
			stInicioIntervalo = ((DateTime.TryParse(MGRAFICA.stFechaMinimaIntervalo, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : MGRAFICA.stFechaMinimaIntervalo) + " " + Label4[30].Text + ":00";
			for (i = 30; i <= 36; i++)
			{
				if (Label4[i].Text != "")
				{
					System.DateTime TempDate2 = DateTime.FromOADate(0);
					stFinIntervalo = ((DateTime.TryParse(MGRAFICA.stFechaMinimaIntervalo, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy") : MGRAFICA.stFechaMinimaIntervalo) + " " + Label4[i].Text.Substring(0, Math.Min(2, Label4[i].Text.Length)) + ":59:59";
					sqlWhereHoras = sqlWhereHoras + "'" + Label4[i].Text.Substring(0, Math.Min(2, Label4[i].Text.Length)) + "',";
				}
			}

			sqlWhereHoras = sqlWhereHoras.Substring(0, Math.Min(sqlWhereHoras.Length - 1, sqlWhereHoras.Length));


			//graficaenfer.BackColor = QBColor(15)
			//lineas

			if (HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked)
			{
				frecuen();
			}
			if (HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked)
			{
				Tension();
			}
			if (HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked)
			{
				Pulso();
			}
			if (HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked)
			{
				Temperatura();
			}
			if (HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked)
			{
				balance();
			}


			//************** O.Frias - 17/10/2006 - Inicio ****************
			if (HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked)
			{
				DolorEva();
			}
			//************** O.Frias - 17/10/2006 - Fin ******************


			MGRAFICA.rellenaSpread(this, "H");


			this.Command1.Enabled = true;
			this.Command2.Enabled = intPaginaActual < intPaginasAPintar;

		}


		//****************************************************************************************
		//*  Procedimiento: Command3_Click                                                       *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias (17/10/2006)                                                              *
		//*                                                                                      *
		//*      - Actualizo el color de los nuevos controles para la impresi�n y posteriormente *
		//*        restauro el original.                                                         *
		//*                                                                                      *
		//****************************************************************************************
		public void Command3_Click(Object eventSender, EventArgs eventArgs)
		{

			proImprimirGraficaCompleta();

		}


		//****************************************************************************************
		//*  Procedimiento: Command4_Click                                                       *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias (17/10/2006)                                                              *
		//*                                                                                      *
		//*      - Actualizo el color de los nuevos controles para la impresi�n y posteriormente *
		//*        restauro el original.                                                         *
		//*                                                                                      *
		//****************************************************************************************
		public void Command4_Click(Object eventSender, EventArgs eventArgs)
		{

			proImprimirGraficaActiva();

		}

		private void Command5_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		//****************************************************************************************
		//*  Procedimiento: Form_Load                                                            *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias (16/10/2006)                                                              *
		//*                                                                                      *
		//*      - Dibujo el cuadro en la etiqueta de dolor(EVA)                                 *
		//*      - Si el tipo servicio es Q pongo check(6)-Dolor(EVA) marcado.                   *
		//*      - Compruebo si el valor de check(6) del dolor(EVA) esta marcado.                *
		//*      - Se incorpora la nueva opci�n de dolor (EVA) en la select.                     *
		//*      - Se realiza la llamada al proceso de calculo de la grafica del Dolor(EVA)      *
		//*                                                                                      *
		//****************************************************************************************
		private void GHORARIA_SH_Load(Object eventSender, EventArgs eventArgs)
		{
			bool bprimero = false;

			this.Height = (int) Screen.PrimaryScreen.Bounds.Height;
			this.Width = (int) Screen.PrimaryScreen.Bounds.Width;

			//************* O.Frias - 16/10/2006 - Inicio ****************
			float CX = ((float) (Picture1.ClientRectangle.Width * 15)) / 2;
			int CY = Convert.ToInt32(((float) (Picture1.ClientRectangle.Height * 15)) / 2);
			using (Graphics g = Picture1.CreateGraphics())
				{
                //INIC SDSALAZAR TODO SPRIN 5_5
                //g.FillRectangle(new SolidBrush(Color.Black), new Point(Convert.ToInt32(CX - 50), CY - 50), new Point(Convert.ToInt32(CX + 50), CY + 50));
                //INIC SDSALAZAR TODO SPRIN 5_5
            }
            //************** O.Frias - 16/10/2006 - Fin ******************


            AuGraf = 0.5d;
			LinRef = false;

			graficaenfer.Height = (int) (graficaenfer.Height + (graficaenfer.Height * AuGraf));
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

            intContadorLeidosXPagina = 0;

			//OSCAR C 28/07/2005
			Label1[25].Visible = Serrores.ObternerValor_CTEGLOBAL(MGRAFICA.RcEnfermeria, "GRAESTEN", "VALFANU1") == "1";
			Label1[25].Text = MGRAFICA.vminTramo1.ToString();
			Label1[21].Text = MGRAFICA.vmaxTramo1.ToString();
			Label1[17].Text = MGRAFICA.vmaxTramo2.ToString();
			Label1[13].Text = MGRAFICA.vmaxTramo3.ToString();
			Label1[9].Text = MGRAFICA.vmaxTramo4.ToString();
			Label1[5].Text = MGRAFICA.vmaxTramo5.ToString();
			Label1[34].Text = MGRAFICA.vmaxTramo6.ToString();
			//------------------

			//Se saca la hora hasta seleccionada en la anterior pantalla
			if (MGRAFICA.stTipSer == "Q")
			{
				//si es de quirofanos tenemos que encontrar la fecha de inicio y de fin del intervalo
				HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState = CheckState.Checked;
				HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState = CheckState.Checked;
				HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState = CheckState.Checked;
				HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState = CheckState.Checked;
				HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState = CheckState.Checked;

				//************* O.Frias - 16/10/2006 - Inicio ****************
				HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState = CheckState.Checked;
				//************** O.Frias - 16/10/2006 - Fin ******************
			}
			else
			{
				MGRAFICA.stFechaMinimaIntervalo = DateTime.Parse(HOJA_SEGUIMIENTO.DefInstance.sdcfechadesde.Text).ToString("dd/MM/yyyy") + " 00:00:00";
				MGRAFICA.stFechaMaximaIntervalo = DateTime.Parse(HOJA_SEGUIMIENTO.DefInstance.sdcfechadesde.Text).ToString("dd/MM/yyyy") + " 23:59:59";

				Label3.Text = MGRAFICA.proObtenerCama(ganoadme.ToString(), gnumadme.ToString());
			}

			System.DateTime TempDate = DateTime.FromOADate(0);
			LbFechaGrafica.Text = (DateTime.TryParse(MGRAFICA.stFechaMinimaIntervalo, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : MGRAFICA.stFechaMinimaIntervalo;

			Label2.Text = MGRAFICA.proObtenerPaciente(ganoadme.ToString(), gnumadme.ToString());

			if (MGRAFICA.bMasivo)
			{
				this.Text = "Gr�fica de Enfermer�a de " + Label2.Text + " (" + Label3.Text + ")";
			}

			//color del fondo de la grafica a blanco
			//UPGRADE_WARNING: (2080) QBCOlor(15) was upgraded to System.Drawing.Color.White and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
			graficaenfer.BackColor = Color.White;
			//rutina para dibujar las l�neas de coordenadas del gr�fico
			lineas();

			MGRAFICA.oculta(this);

			for (i = 30; i <= 36; i++)
			{
				Label4[i].Text = "";
			}

			string Lsql = " SELECT DISTINCT CONVERT(CHAR(2),T1.FAPUNTES,108) FROM (";

			//************* O.Frias - 16/10/2006 - Inicio ****************
			if (HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked)
			{
				//************** O.Frias - 16/10/2006 - Fin ******************

				bprimero = false;

				object tempRefParam = MGRAFICA.stFechaMinimaIntervalo;
				object tempRefParam2 = MGRAFICA.stFechaMaximaIntervalo;
				Lsql = Lsql + 
				       " SELECT fapuntes from ECONSVIT " + 
				       " Where itiposer = '" + MGRAFICA.stTipSer + "' and" + 
				       "       ganoregi=" + ganoadme.ToString() + " and " + 
				       "       gnumregi=" + gnumadme.ToString() + " and " + 
				       "       fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam) + " and " + 
				       "       fapuntes <=" + Serrores.FormatFechaHMS(tempRefParam2) + " " + 
				       " AND (";
				MGRAFICA.stFechaMaximaIntervalo = Convert.ToString(tempRefParam2);
				MGRAFICA.stFechaMinimaIntervalo = Convert.ToString(tempRefParam);
				//Frecuencia
				if (HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked)
				{
					Lsql = Lsql + " nfrecres is not null ";
					bprimero = true;
				}
				//Tension
				if (HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked)
				{
					if (!bprimero)
					{
						Lsql = Lsql + " ntensmax is not null ";
						bprimero = true;
					}
					else
					{
						Lsql = Lsql + " or ntensmax is not null ";
					}
				}
				//Pulso
				if (HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked)
				{
					if (!bprimero)
					{
						Lsql = Lsql + " nfreccar is not null ";
						bprimero = true;
					}
					else
					{
						Lsql = Lsql + " or nfreccar is not null ";
					}
				}
				//Temperatura
				if (HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked)
				{
					if (!bprimero)
					{
						Lsql = Lsql + " ntempera is not null ";
						bprimero = true;
					}
					else
					{
						Lsql = Lsql + " or ntempera is not null ";
					}
				}

				//************* O.Frias - 16/10/2006 - Inicio ****************
				if (HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked)
				{
					if (!bprimero)
					{
						Lsql = Lsql + " ndoloeva is not null ";
						bprimero = true;
					}
					else
					{
						Lsql = Lsql + " or ndoloeva is not null ";
					}
				}
				//************** O.Frias - 16/10/2006 - Fin ******************

				Lsql = Lsql + " ) ";

			}

			//************* O.Frias - 16/10/2006 - Inicio ****************
			if (HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked)
			{
				if (HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked)
				{
					Lsql = Lsql + " UNION ALL ";
				}
				//************** O.Frias - 16/10/2006 - Fin ******************

				//Balance
				object tempRefParam3 = MGRAFICA.stFechaMinimaIntervalo;
				object tempRefParam4 = MGRAFICA.stFechaMaximaIntervalo;
				Lsql = Lsql + 
				       "select FAPUNTES " + 
				       "from ebalance " + 
				       "Where itiposer = '" + MGRAFICA.stTipSer + "' " + 
				       "and ganoregi= " + ganoadme.ToString() + " " + 
				       "and gnumregi= " + gnumadme.ToString() + " " + 
				       "and FAPUNTES >= " + Serrores.FormatFechaHMS(tempRefParam3) + " " + 
				       "and FAPUNTES <=" + Serrores.FormatFechaHMS(tempRefParam4) + " ";
				MGRAFICA.stFechaMaximaIntervalo = Convert.ToString(tempRefParam4);
				MGRAFICA.stFechaMinimaIntervalo = Convert.ToString(tempRefParam3);
			}

			//ADEMAS, DEBEMOS MIRAR LAS POSIBLES HORAS QUE SE PINTAN EN EL CUADRO RESUMEN
			//YA QUE AUNQUE NO SE PINTE NINGUN VALOR EN LA GRAFICA, SE DEBERA MOSTRAR LAS HORAS
			//EN LAS QUE SE HAN PUESTO APUNTES CORRESPONDIENTES AL CUADRO RESUMEN

			//************* O.Frias - 16/10/2006 - Inicio ****************
			if (HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked || HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked)
			{
				Lsql = Lsql + " UNION ALL ";
			}
			//************** O.Frias - 16/10/2006 - Fin ******************

			//CLUCOSA
			object tempRefParam5 = MGRAFICA.stFechaMinimaIntervalo;
			object tempRefParam6 = MGRAFICA.stFechaMaximaIntervalo;
			Lsql = Lsql + " select fapuntes from eglucemi " + 
			       " where itiposer = '" + MGRAFICA.stTipSer + "' " + 
			       "   and ganoregi=" + ganoadme.ToString() + " " + 
			       "   and gnumregi=" + gnumadme.ToString() + " " + 
			       "   and fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam5) + " " + 
			       "   and fapuntes <=" + Serrores.FormatFechaHMS(tempRefParam6) + " " + 
			       "   and cglucemi is not null ";
			MGRAFICA.stFechaMaximaIntervalo = Convert.ToString(tempRefParam6);
			MGRAFICA.stFechaMinimaIntervalo = Convert.ToString(tempRefParam5);
			//PESO Y TALLA
			Lsql = Lsql + " UNION ALL ";

            object tempRefParam7 = MGRAFICA.stFechaMinimaIntervalo;
			object tempRefParam8 = MGRAFICA.stFechaMaximaIntervalo;
			Lsql = Lsql + "select fapuntes from emedidas " + 
			       " where itiposer = '" + MGRAFICA.stTipSer + "' " + 
			       "   and ganoregi=" + ganoadme.ToString() + " " + 
			       "   and gnumregi=" + gnumadme.ToString() + " " + 
			       "   and fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam7) + " " + 
			       "   and fapuntes <=" + Serrores.FormatFechaHMS(tempRefParam8) + " " + 
			       "   and (cpesopac is not null or ctallapac is not null) ";
			MGRAFICA.stFechaMaximaIntervalo = Convert.ToString(tempRefParam8);
			MGRAFICA.stFechaMinimaIntervalo = Convert.ToString(tempRefParam7);
			//DIURESIS / DRENAJES / N DEPOSCIONES
			Lsql = Lsql + " UNION ALL ";

            object tempRefParam9 = MGRAFICA.stFechaMinimaIntervalo;
			object tempRefParam10 = MGRAFICA.stFechaMaximaIntervalo;
			Lsql = Lsql + "select FAPUNTES  from ebalance " + 
			       " where itiposer = '" + MGRAFICA.stTipSer + "' " + 
			       "   and ganoregi=" + ganoadme.ToString() + " " + 
			       "   and gnumregi=" + gnumadme.ToString() + " " + 
			       "   and FAPUNTES >= " + Serrores.FormatFechaHMS(tempRefParam9) + " " + 
			       "   and FAPUNTES <=" + Serrores.FormatFechaHMS(tempRefParam10) + " " + 
			       "   and (cperdiur is not null or cdrenaje is not null or nvechece is not null)";
			MGRAFICA.stFechaMaximaIntervalo = Convert.ToString(tempRefParam10);
			MGRAFICA.stFechaMinimaIntervalo = Convert.ToString(tempRefParam9);
			//BALANCE DIARIO
			Lsql = Lsql + " UNION ALL ";

            object tempRefParam11 = MGRAFICA.stFechaMinimaIntervalo;
			object tempRefParam12 = MGRAFICA.stFechaMaximaIntervalo;
			Lsql = Lsql + "select FAPUNTES from ebalance " + 
			       " where itiposer = '" + MGRAFICA.stTipSer + "' " + 
			       "   and ganoregi=" + ganoadme.ToString() + " " + 
			       "   and gnumregi=" + gnumadme.ToString() + " " + 
			       "   and FAPUNTES >= " + Serrores.FormatFechaHMS(tempRefParam11) + " " + 
			       "   and FAPUNTES <=" + Serrores.FormatFechaHMS(tempRefParam12) + " " + 
			       "   and (cganoral is not null or " + 
			       "        cganendo is not null or " + 
			       "        cotragan is not null or " + 
			       "        cgantran is not null or " + 
			       "        cperdiur is not null or " + 
			       "        cperaspi is not null or " + 
			       "        cpervomi is not null or " + 
			       "        cperhece is not null or " + 
			       "        cdrenaje is not null or " + 
			       "        cotraper is not null)";
			MGRAFICA.stFechaMaximaIntervalo = Convert.ToString(tempRefParam12);
			MGRAFICA.stFechaMinimaIntervalo = Convert.ToString(tempRefParam11);
			//--------------------------


			Lsql = Lsql + " ) T1 " + 
			       " ORDER BY CONVERT(CHAR(2),T1.FAPUNTES,108) ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(Lsql, MGRAFICA.RcEnfermeria);
			RRhorasApintar = new DataSet();
			tempAdapter.Fill(RRhorasApintar);
			if (RRhorasApintar.Tables[0].Rows.Count != 0)
			{
				RRhorasApintar.MoveLast(null);
				RRhorasApintar.MoveFirst();


				if ((RRhorasApintar.Tables[0].Rows.Count % 7) == 0)
				{
					intPaginasAPintar = RRhorasApintar.Tables[0].Rows.Count / ((int) 7);
				}
				else
				{
                    //INIC SDSALAZAR TODO SPRIN 5_5
                    //intPaginasAPintar = Convert.ToInt32(Math.Floor(RRhorasApintar.Tables[0].Rows.Count / ((int) 7)) + 1); //7 columnas por pagina
                    //INIC SDSALAZAR TODO SPRIN 5_5
                }

                intPaginaActual = 1;
				i = 0;
				foreach (DataRow iteration_row in RRhorasApintar.Tables[0].Rows)
				{
					if (i <= 6)
					{
						Label4[30 + i].Text = StringsHelper.Format(iteration_row[0], "00") + ":00";
						intContadorLeidosXPagina++;
						i++;
					}
					else
					{
						break;
					}
				}

				sqlWhereHoras = "";
				System.DateTime TempDate2 = DateTime.FromOADate(0);
				stInicioIntervalo = ((DateTime.TryParse(MGRAFICA.stFechaMinimaIntervalo, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy") : MGRAFICA.stFechaMinimaIntervalo) + " " + Label4[30].Text + ":00";
				for (i = 30; i <= 36; i++)
				{
					if (Label4[i].Text != "")
					{
						System.DateTime TempDate3 = DateTime.FromOADate(0);
						stFinIntervalo = ((DateTime.TryParse(MGRAFICA.stFechaMinimaIntervalo, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : MGRAFICA.stFechaMinimaIntervalo) + " " + Label4[i].Text.Substring(0, Math.Min(2, Label4[i].Text.Length)) + ":59:59";
						sqlWhereHoras = sqlWhereHoras + "'" + Label4[i].Text.Substring(0, Math.Min(2, Label4[i].Text.Length)) + "',";
					}
				}

				sqlWhereHoras = sqlWhereHoras.Substring(0, Math.Min(sqlWhereHoras.Length - 1, sqlWhereHoras.Length));

				if (HOJA_SEGUIMIENTO.DefInstance.Check1.CheckState == CheckState.Checked)
				{
					frecuen();
				}
				if (HOJA_SEGUIMIENTO.DefInstance.Check2.CheckState == CheckState.Checked)
				{
					Tension();
				}
				if (HOJA_SEGUIMIENTO.DefInstance.Check3.CheckState == CheckState.Checked)
				{
					Pulso();
				}
				if (HOJA_SEGUIMIENTO.DefInstance.Check4.CheckState == CheckState.Checked)
				{
					Temperatura();
				}
				if (HOJA_SEGUIMIENTO.DefInstance.Check5.CheckState == CheckState.Checked)
				{
					balance();
				}

				//************* O.Frias - 16/10/2006 - Inicio ****************
				if (HOJA_SEGUIMIENTO.DefInstance.Check6.CheckState == CheckState.Checked)
				{
					DolorEva();
				}
				//************** O.Frias - 16/10/2006 - Fin ******************


				MGRAFICA.rellenaSpread(this, "H");

				this.Command1.Enabled = false;

				this.Command2.Enabled = intPaginaActual < intPaginasAPintar;

			}
			else
			{
				Command1.Enabled = false;
				Command2.Enabled = false;
				Command3.Enabled = false;
				Command4.Enabled = false;
			}

			//Si se puede avanzar por la grafica, pulsamos >> hasta que se deshabilite (por haber llegado)
			//a la ultima p�gina a visualizar.
			if (Command2.Enabled)
			{

				while(Command2.Enabled)
				{
					Command2_Click(Command2, new EventArgs());
				};
			}
		}
		private void Temperatura()
		{
			//CALCULO DE LA TEMPERATURA
			//*************************
			try
			{

				//10 valores en cada tramo / 10 lineas horizontales por tramo = 1
				valgris = 1;

				object tempRefParam = stInicioIntervalo;
				object tempRefParam2 = stFinIntervalo;
				stsql = "select E.ntempera ,E.fapuntes  " + 
				        "from econsvit E " + 
				        "Where itiposer = '" + MGRAFICA.stTipSer + "' " + 
				        "and E.ganoregi= " + ganoadme.ToString() + " " + 
				        "and E.gnumregi= " + gnumadme.ToString() + " " + 
				        "and E.ntempera is not null " + 
				        "and E.fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam) + " " + 
				        "and E.fapuntes <=" + Serrores.FormatFechaHMS(tempRefParam2) + " ";
				stFinIntervalo = Convert.ToString(tempRefParam2);
				stInicioIntervalo = Convert.ToString(tempRefParam);

				stsql = stsql + 
				        " and CONVERT(CHAR(2),E.FAPUNTES,108) in (" + sqlWhereHoras + ")" + 
				        " order by fapuntes";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MGRAFICA.RcEnfermeria);
				rsmagnitud = new DataSet();
				tempAdapter.Fill(rsmagnitud);

                //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //INIC SDSALAZAR TODO SPRIN 5_5
                //graficaenfer.setForeColor(Label1[0].BackColor);
                //INIC SDSALAZAR TODO SPRIN 5_5
                x1 = 80;
				x2 = 300;
				y1 = -80;
				y2 = -80;
				y2 = (float) (y2 + (y2 * AuGraf));
				y1 = (float) (y1 + (y1 * AuGraf));
				p = 30;
				D = 0;
				bool paso = false;
				bool primera = false;
				primera = true;

				if (rsmagnitud.Tables[0].Rows.Count > 0)
				{
					rsmagnitud.MoveFirst();
					foreach (DataRow iteration_row in rsmagnitud.Tables[0].Rows)
					{
						fecha_epi = DateTime.Parse(Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("dd/MM/yyyy"));
						hora_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH");
						minutos_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("mm");
						//ponerlo cuando tenga un paciente con datos en constantes_vitales
						//While Label4(p).Caption <> Format(rsmagnitud!fapuntes, "dd/mm/yyyy")
						while (Label4[p].Text != Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH") + ":00")
						{
							p++;
							D++;
							x2 = (1100 * D) + 300;
							paso = true;
							if (p == 37)
							{
								break;
							}
						}

						if (paso)
						{
							paso = false;
						}
						else
						{
							x2 = (1100 * D) + 300;
						}

						if (String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "35.0") >= 0 && String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "36.0") < 0)
						{
							y2 = 4120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntempera"]) - Double.Parse("35.0")) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_x(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_x(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;

						}

						if (String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "36.0") >= 0 && String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "37.0") < 0)
						{
							y2 = 3520;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntempera"]) - Double.Parse("36.0")) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_x(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_x(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "37.0") >= 0 && String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "38.0") < 0)
						{
							y2 = 2920;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntempera"]) - Double.Parse("37.0")) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_x(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_x(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "38.0") >= 0 && String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "39.0") < 0)
						{
							y2 = 2320;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntempera"]) - Double.Parse("38.0")) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_x(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_x(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "39.0") >= 0 && String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "40.0") < 0)
						{
							y2 = 1720;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntempera"]) - Double.Parse("39.0")) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_x(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_x(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "40.0") >= 0 && String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "41.0") < 0)
						{
							y2 = 1120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntempera"]) - Double.Parse("40.0")) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_x(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_x(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (String.CompareOrdinal(Convert.ToString(iteration_row["ntempera"]), "41.0") >= 0)
						{
							y2 = 520;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntempera"]) - Double.Parse("41.0")) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_x(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_x(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

					}
                    rsmagnitud.Close();
				}
			}
			catch(SqlException SqlExep)
			{
				Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "temperatura:GRAFICA", SqlExep);
				rsmagnitud = null;
			}
            catch(Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }
		}
		private void balance()
		{
			//CALCULO DE LA TEMPERATURA
			//*************************
			try
			{

				object tempRefParam = stInicioIntervalo;
				object tempRefParam2 = stFinIntervalo;
				stsql = "select * " + 
				        "from ebalance E " + 
				        "Where itiposer = '" + MGRAFICA.stTipSer + "' " + 
				        "and E.ganoregi= " + ganoadme.ToString() + " " + 
				        "and E.gnumregi= " + gnumadme.ToString() + " " + 
				        "and E.FAPUNTES >= " + Serrores.FormatFechaHMS(tempRefParam) + " " + 
				        "and E.FAPUNTES <=" + Serrores.FormatFechaHMS(tempRefParam2) + " ";
				stFinIntervalo = Convert.ToString(tempRefParam2);
				stInicioIntervalo = Convert.ToString(tempRefParam);

				stsql = stsql + 
				        " and CONVERT(CHAR(2),E.FAPUNTES,108) in (" + sqlWhereHoras + ")" + 
				        " order by FAPUNTES";



				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MGRAFICA.RcEnfermeria);
				rsmagnitud = new DataSet();
				tempAdapter.Fill(rsmagnitud);
                //INIC SDSALAZAR TODO SPRIN 5_5
                //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //graficaenfer.setForeColor(Label1[36].BackColor);
                //INIC SDSALAZAR TODO SPRIN 5_5
                x1 = 80;
				x2 = 300;
				y1 = -80;
				y2 = -80;
				p = 30;
				D = 0;
				bool paso = false;
				bool primera = false;
				double dGanancias = 0;
				double dPerdidas = 0;
				double dBalance = 0;


				primera = true;

				if (rsmagnitud.Tables[0].Rows.Count > 0)
				{
					rsmagnitud.MoveFirst();
					foreach (DataRow iteration_row in rsmagnitud.Tables[0].Rows)
					{
						dGanancias = ((Convert.IsDBNull(iteration_row["cganoral"])) ? 0 : Convert.ToDouble(iteration_row["cganoral"])) + ((Convert.IsDBNull(iteration_row["cganendo"])) ? 0 : Convert.ToDouble(iteration_row["cganendo"])) + ((Convert.IsDBNull(iteration_row["cotragan"])) ? 0 : Convert.ToDouble(iteration_row["cotragan"])) + ((Convert.IsDBNull(iteration_row["cgantran"])) ? 0 : Convert.ToDouble(iteration_row["cgantran"]));
						dPerdidas = ((Convert.IsDBNull(iteration_row["cperdiur"])) ? 0 : Convert.ToDouble(iteration_row["cperdiur"])) + ((Convert.IsDBNull(iteration_row["cperaspi"])) ? 0 : Convert.ToDouble(iteration_row["cperaspi"])) + ((Convert.IsDBNull(iteration_row["cpervomi"])) ? 0 : Convert.ToDouble(iteration_row["cpervomi"])) + ((Convert.IsDBNull(iteration_row["cperhece"])) ? 0 : Convert.ToDouble(iteration_row["cperhece"])) + ((Convert.IsDBNull(iteration_row["cdrenaje"])) ? 0 : Convert.ToDouble(iteration_row["cdrenaje"])) + ((Convert.IsDBNull(iteration_row["cotraper"])) ? 0 : Convert.ToDouble(iteration_row["cotraper"]));
						dBalance = dGanancias - dPerdidas;

						fecha_epi = DateTime.Parse(Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("dd/MM/yyyy"));
						hora_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH");
						minutos_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("mm");
						//ponerlo cuando tenga un paciente con datos en constantes_vitales
						while (Label4[p].Text != Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH") + ":00")
						{
							p++;
							D++;
							x2 = (1100 * D) + 300;
							paso = true;
							if (p == 37)
							{
								break;
							}
						}

						if (paso)
						{
							paso = false;
						}
						else
						{
							x2 = (1100 * D) + 300;
						}

						if (dBalance >= -3000 && dBalance < -2000)
						{
							y2 = 4120;
							y2 = (float) (y2 - (60 * ((dBalance - (-3000)) / 100)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi)); // la he sacado del if
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (dBalance >= -2000 && dBalance < -1000)
						{
							y2 = 3520;
							y2 = (float) (y2 - (60 * ((dBalance - (-2000)) / 100)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (dBalance >= -1000 && dBalance < 0)
						{
							y2 = 2920;
							y2 = (float) (y2 - (60 * ((dBalance - (-1000)) / 100)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (dBalance >= 0 && dBalance < 1000)
						{
							y2 = 2320;
							y2 = (float) (y2 - (60 * ((dBalance) / 100)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (dBalance >= 1000 && dBalance < 2000)
						{
							y2 = 1720;
							y2 = (float) (y2 - (60 * ((dBalance - 1000) / 100)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (dBalance >= 2000 && dBalance < 3000)
						{
							y2 = 1120;
							y2 = (float) (y2 - (60 * ((dBalance - 2000) / 100)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (dBalance >= 3000)
						{
							y2 = 520;
							y2 = (float) (y2 - (60 * ((dBalance - 3000) / 100)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

					}
                    rsmagnitud.Close();
				}
			}
			catch(SqlException SqlExep)
			{
				Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "Balance:GRAFICA", SqlExep);
				rsmagnitud = null;
			}
            catch (Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }
        }

		private void frecuen()
		{
			//CALCULO DE LA FRECUENCIA RESPIRATORIA
			//**************************************
			try
			{

				//10 valores en cada tramo / 10 lineas horizontales por tramo = 1
				valgris = 1;

                //INIC SDSALAZAR TODO SPRIN 5_5
                //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //graficaenfer.setForeColor(Label1[3].BackColor);
                //INIC SDSALAZAR TODO SPRIN 5_5
                object tempRefParam = stInicioIntervalo;
				object tempRefParam2 = stFinIntervalo;
				stsql = "select E.nfrecres ,E.fapuntes  " + 
				        "from econsvit E " + 
				        "Where itiposer = '" + MGRAFICA.stTipSer + "' " + 
				        "and E.ganoregi=" + ganoadme.ToString() + " " + 
				        "and E.gnumregi=" + gnumadme.ToString() + " " + 
				        "and E.nfrecres is not null " + 
				        "and E.fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam) + " " + 
				        "and E.fapuntes <=" + Serrores.FormatFechaHMS(tempRefParam2) + " ";
				stFinIntervalo = Convert.ToString(tempRefParam2);
				stInicioIntervalo = Convert.ToString(tempRefParam);

				stsql = stsql + 
				        " and CONVERT(CHAR(2),E.FAPUNTES,108) in (" + sqlWhereHoras + ")" + 
				        " order by fapuntes";


				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MGRAFICA.RcEnfermeria);
				rsmagnitud = new DataSet();
				tempAdapter.Fill(rsmagnitud);
                //UPGRADE_ISSUE: (2070) Constant vbPixels was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
                //UPGRADE_ISSUE: (2038) Form property .ScaleMode is not supported. More Information: http://www.vbtonet.com/ewis/ewi2038.aspx

                //SDSALAZAR SPRINT 5_5
    //            this.setScaleMode(UpgradeStubs.VBRUN_ScaleModeConstants.getvbPixels()); //Selecciono la escala
				////UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.AutoRedraw was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//graficaenfer.setAutoRedraw(true);
                //SDSALAZAR SPRINT 5_5

                x1 = 80;
				x2 = 300;
				y1 = -80;
				y2 = -80;
				p = 30;
				D = 0;
				bool paso = false;
				bool primera = false;
				primera = true;
				y2 = (float) (y2 + (y2 * AuGraf));
				y1 = (float) (y1 + (y1 * AuGraf));
				if (rsmagnitud.Tables[0].Rows.Count > 0)
				{
					rsmagnitud.MoveFirst();
					foreach (DataRow iteration_row in rsmagnitud.Tables[0].Rows)
					{
						fecha_epi = DateTime.Parse(Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("dd/MM/yyyy"));
						hora_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH");
						minutos_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("mm");
						//ponerlo cuando tenga un paciente con datos en constantes_vitales
						while (Label4[p].Text != Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH") + ":00")
						{
							p++;
							D++;
							x2 = (1100 * D) + 300; // Desplazamiento
							paso = true;
							if (p == 37)
							{
								break;
							}
						}

						if (paso)
						{
							paso = false;
						}
						else
						{
							x2 = (1100 * D) + 300;
						}

						if (Convert.ToDouble(iteration_row["nfrecres"]) >= 10 && Convert.ToDouble(iteration_row["nfrecres"]) < 20)
						{

							y2 = 4120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfrecres"]) - 10) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;

						}

						if (Convert.ToDouble(iteration_row["nfrecres"]) >= 20 && Convert.ToDouble(iteration_row["nfrecres"]) < 30)
						{
							y2 = 3520;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfrecres"]) - 20) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["nfrecres"]) >= 30 && Convert.ToDouble(iteration_row["nfrecres"]) < 40)
						{
							y2 = 2920;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfrecres"]) - 30) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}

							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["nfrecres"]) >= 40 && Convert.ToDouble(iteration_row["nfrecres"]) < 50)
						{
							y2 = 2320;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfrecres"]) - 40) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["nfrecres"]) >= 50 && Convert.ToDouble(iteration_row["nfrecres"]) < 60)
						{
							y2 = 1720;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfrecres"]) - 50) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["nfrecres"]) >= 60 && Convert.ToDouble(iteration_row["nfrecres"]) <= 70)
						{
							y2 = 1120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfrecres"]) - 60) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								pinta_circulo(x2, y2);
								primera = false;
							}
							else
							{
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_circulo(x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

					}
                    rsmagnitud.Close();
				}
			}
			catch(SqlException SqlExep)
			{
				Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "frecuem:GRAFICA", SqlExep);
				rsmagnitud = null;
			}
            catch (Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }
        }
		private void Pulso()
		{
			//CALCULO DEl PULSO
			//*****************
			try
			{

				//20 valores en cada tramo / 10 lineas horizontales por tramo = 2
				valgris = 2;

                //INIC SDSALAZAR TODO SPRIN 5_5
                //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //graficaenfer.setForeColor(Label1[1].BackColor);
                //INIC SDSALAZAR TODO SPRIN 5_5
                object tempRefParam = stInicioIntervalo;
				object tempRefParam2 = stFinIntervalo;
				stsql = "select E.nfreccar ,E.fapuntes  " + 
				        "from econsvit E " + 
				        "Where itiposer = '" + MGRAFICA.stTipSer + "' " + 
				        "and E.ganoregi=" + ganoadme.ToString() + " " + 
				        "and E.gnumregi=" + gnumadme.ToString() + " " + 
				        "and E.nfreccar is not null " + 
				        "and E.fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam) + " " + 
				        "and E.fapuntes <=" + Serrores.FormatFechaHMS(tempRefParam2) + " ";
				stFinIntervalo = Convert.ToString(tempRefParam2);
				stInicioIntervalo = Convert.ToString(tempRefParam);

				stsql = stsql + 
				        " and CONVERT(CHAR(2),E.FAPUNTES,108) in (" + sqlWhereHoras + ")" + 
				        " order by fapuntes";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MGRAFICA.RcEnfermeria);
				rsmagnitud = new DataSet();
				tempAdapter.Fill(rsmagnitud);

                //SDSALAZAR SPRINT 5_5
    //            //UPGRADE_ISSUE: (2070) Constant vbPixels was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
    //            //UPGRADE_ISSUE: (2038) Form property .ScaleMode is not supported. More Information: http://www.vbtonet.com/ewis/ewi2038.aspx
    //            this.setScaleMode(UpgradeStubs.VBRUN_ScaleModeConstants.getvbPixels()); //Selecciono la escala
				////UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.AutoRedraw was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//graficaenfer.setAutoRedraw(true);
                //SDSALAZAR SPRINT 5_5
                x1 = 80;
				x2 = 300;
				y1 = -80;
				y2 = -80;
				y2 = (float) (y2 + (y2 * AuGraf));
				y1 = (float) (y1 + (y1 * AuGraf));
				p = 30;
				D = 0;
				bool paso = false;
				bool primera = false;
				primera = true;

				if (rsmagnitud.Tables[0].Rows.Count > 0)
				{
					rsmagnitud.MoveFirst();
					foreach (DataRow iteration_row in rsmagnitud.Tables[0].Rows)
					{
						fecha_epi = DateTime.Parse(Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("dd/MM/yyyy"));
						hora_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH");
						minutos_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("mm");

						//ponerlo cuando tenga un paciente con datos en constantes_vitales
						while (Label4[p].Text != Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH") + ":00")
						{
							p++;
							D++;
							x2 = (1100 * D) + 300; // Desplazamiento
							paso = true;
							if (p == 37)
							{
								break;
							}
						}

						if (paso)
						{
							paso = false;
						}
						else
						{
							x2 = (1100 * D) + 300;
						}

						if (Convert.ToDouble(iteration_row["nfreccar"]) >= 40 && Convert.ToDouble(iteration_row["nfreccar"]) < 60)
						{
							y2 = 4120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfreccar"]) - 40) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_v(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_v(x1, y1, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["nfreccar"]) >= 60 && Convert.ToDouble(iteration_row["nfreccar"]) < 80)
						{
							y2 = 3520;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfreccar"]) - 60) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_v(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_v(x1, y1, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["nfreccar"]) >= 80 && Convert.ToDouble(iteration_row["nfreccar"]) < 100)
						{
							y2 = 2920;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfreccar"]) - 80) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_v(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_v(x1, y1, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}


						if (Convert.ToDouble(iteration_row["nfreccar"]) >= 100 && Convert.ToDouble(iteration_row["nfreccar"]) < 120)
						{
							y2 = 2320;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfreccar"]) - 100) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_v(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_v(x1, y1, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["nfreccar"]) >= 120 && Convert.ToDouble(iteration_row["nfreccar"]) < 140)
						{
							y2 = 1720;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfreccar"]) - 120) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_v(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_v(x1, y1, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["nfreccar"]) >= 140 && Convert.ToDouble(iteration_row["nfreccar"]) < 160)
						{
							y2 = 1120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfreccar"]) - 140) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_v(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_v(x1, y1, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["nfreccar"]) >= 160 && Convert.ToDouble(iteration_row["nfreccar"]) <= 180)
						{
							y2 = 520;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["nfreccar"]) - 160) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (primera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_v(x2, y2, x2, y2);
								primera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_v(x1, y1, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

					}
					rsmagnitud.Close();
				}
			}
			catch(SqlException SqlExep)
			{
				Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "pulso:GRAFICA", SqlExep);
				rsmagnitud = null;
			}
            catch (Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }
        }


		//****************************************************************************************
		//*  Procedimiento: DolorEva (16/10/2006)                                                *
		//*                                                                                      *
		//*  Observaciones: Realiza los calculos necesarios para la grafica.                     *
		//*                                                                                      *
		//****************************************************************************************
		private void DolorEva()
		{
			bool error = false;

			try
			{
				error = true;

				//oscar 04/02/04
				//2 valores en cada tramo / 10 lineas horizontales por tramo = 1
				string tempRefParam = "0.2";
				string tempRefParam2 = Serrores.ObtenerSeparadorDecimal(MGRAFICA.RcEnfermeria);
				valgris = Double.Parse(Serrores.ConvertirDecimales(ref tempRefParam, ref tempRefParam2, 1));
				//------------


				object tempRefParam3 = stInicioIntervalo;
				object tempRefParam4 = stFinIntervalo;
				stsql = "select E.ndoloeva, E.fapuntes " + 
				        "FROM ECONSVIT E " + 
				        "WHERE E.itiposer = '" + MGRAFICA.stTipSer + "' AND " + 
				        "E.ganoregi= " + ganoadme.ToString() + " AND " + 
				        "E.gnumregi= " + gnumadme.ToString() + " AND " + 
				        "E.ndoloeva is not null AND " + 
				        "E.fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam3) + " AND " + 
				        "E.fapuntes <=" + Serrores.FormatFechaHMS(tempRefParam4) + " AND " + 
				        "CONVERT(CHAR(2),E.FAPUNTES,108) in (" + sqlWhereHoras + ") " + 
				        "ORDER BY fapuntes";
				stFinIntervalo = Convert.ToString(tempRefParam4);
				stInicioIntervalo = Convert.ToString(tempRefParam3);


				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MGRAFICA.RcEnfermeria);
				rsmagnitud = new DataSet();
				tempAdapter.Fill(rsmagnitud);

                //INIC SDSALAZAR TODO SPRIN 5_5
                //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //graficaenfer.setForeColor(Label1[45].BackColor);
                //INIC SDSALAZAR TODO SPRIN 5_5

                x1 = 80;
				x2 = 300;
				y1 = -80;
				y2 = -80;
				p = 30;
				D = 0;
				y2 = (float) (y2 + (y2 * AuGraf));
				y1 = (float) (y1 + (y1 * AuGraf));

				bool blnPaso = false;
				bool blnPrimera = false;

				blnPrimera = true;

				if (rsmagnitud.Tables[0].Rows.Count > 0)
				{
					rsmagnitud.MoveFirst();
					foreach (DataRow iteration_row in rsmagnitud.Tables[0].Rows)
					{
						fecha_epi = DateTime.Parse(Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("dd/MM/yyyy"));
						hora_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH");
						minutos_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("mm");

						//ponerlo cuando tenga un paciente con datos en constantes_vitales
						while (Label4[p].Text != Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH") + ":00")
						{
							p++;
							D++;
							x2 = (1100 * D) + 300;
							blnPaso = true;
							if (p == 37)
							{
								break;
							}
						}

						if (blnPaso)
						{
							blnPaso = false;
						}
						else
						{
							x2 = (1100 * D) + 300;
						}


						if (Convert.ToDouble(iteration_row["ndoloeva"]) >= 0 && Convert.ToDouble(iteration_row["ndoloeva"]) < 2)
						{
							y2 = 4120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ndoloeva"]) - 0) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));

							if (blnPrimera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_cuadro(x2, y2, x2, y2);
								blnPrimera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_cuadro(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["ndoloeva"]) >= 2 && Convert.ToDouble(iteration_row["ndoloeva"]) < 4)
						{
							y2 = 3520;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ndoloeva"]) - 2) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (blnPrimera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_cuadro(x2, y2, x2, y2);
								blnPrimera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_cuadro(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["ndoloeva"]) >= 4 && Convert.ToDouble(iteration_row["ndoloeva"]) < 6)
						{
							y2 = 2920;

							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ndoloeva"]) - 4) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (blnPrimera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_cuadro(x2, y2, x2, y2);
								blnPrimera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_cuadro(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["ndoloeva"]) >= 6 && Convert.ToDouble(iteration_row["ndoloeva"]) < 8)
						{
							y2 = 2320;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ndoloeva"]) - 6) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (blnPrimera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_cuadro(x2, y2, x2, y2);
								blnPrimera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_cuadro(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}


						if (Convert.ToDouble(iteration_row["ndoloeva"]) >= 8 && Convert.ToDouble(iteration_row["ndoloeva"]) < 10)
						{
							y2 = 1720;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ndoloeva"]) - 8) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (blnPrimera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_cuadro(x2, y2, x2, y2);
								blnPrimera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_cuadro(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

						if (Convert.ToDouble(iteration_row["ndoloeva"]) >= 10)
						{
							y2 = 1120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ndoloeva"]) - 10) / valgris)));
							y2 = (float) (y2 + (y2 * AuGraf));
							if (blnPrimera)
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								pinta_cuadro(x2, y2, x2, y2);
								blnPrimera = false;
							}
							else
							{
								x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
								using (Graphics g = graficaenfer.CreateGraphics())
									{

										g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
									}
								pinta_cuadro(x2, y2, x2, y2);
							}
							x1 = x2;
							y1 = y2;
						}

					}
                    rsmagnitud.Close();
				}

				error = false;
			}
			catch (Exception excep)
			{
				if (!error)
				{
					throw excep;
				}

				if (error)
				{

					Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "Dolor(EVA):GRAFICA", excep);
					rsmagnitud = null;

				}

			}
		}

		//****************************************************************************************
		//*  Procedimiento: pinta_cuadro (16/10/2006)                                            *
		//*                                                                                      *
		//*  Observaciones:                                                                      *
		//*                                                                                      *
		//*    - Pinto el cuadro de posicion en la grafica.                                      *
		//*                                                                                      *
		//****************************************************************************************
		private void pinta_cuadro(float x1, float y1, float x2, float y2)
		{

			using (Graphics g = graficaenfer.CreateGraphics())
				{

                //INIC SDSALAZAR TODO SPRIN 5_5
                //g.FillRectangle(new SolidBrush(Color.Black), new Point(Convert.ToInt32(x2 - 45), Convert.ToInt32(y2 - 45)), new Point(Convert.ToInt32(x2 + 45), Convert.ToInt32(y2 + 45)));
                //INIC SDSALAZAR TODO SPRIN 5_5
            }

        }


		//UPGRADE_NOTE: (7001) The following declaration (pinta_cuadrado) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void pinta_cuadrado(float x1, float y1, float x2, float y2)
		//{
				//using (Graphics g = graficaenfer.CreateGraphics())
					//{
					//
						//g.FillRectangle(new SolidBrush(Color.Black), new Point(Convert.ToInt32(x2 - 20), Convert.ToInt32(y2 - 20)), new Point(Convert.ToInt32(x2 + 20), Convert.ToInt32(y2 + 20)));
					//}
		//}

		//pinta el punto como c�rculo de la frecuencia respiratoria

		private void pinta_circulo(float x2, float y2)
		{
            //graficaenfer.ForeColor = QBColor(0)
            //graficaenfer.ForeColor = QBColor(13)
            //SDSALAZAR SPRINT 5_5
   //         //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
   //         //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.FillColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
   //         graficaenfer.setFillColor(graficaenfer.getForeColor());
			////UPGRADE_WARNING: (1068) (x2)-(y2) of type Point is being forced to float. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
			////UPGRADE_ISSUE: (2064) PictureBox method graficaenfer.Circle was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//graficaenfer.Circle(x2, y2, 30, 0, 0, 0, 0);
            //SDSALAZAR SPRINT 5_5

        }

        //pinta el punto de la temperatura

        private void pinta_x(float x1, float y1, float x2, float y2)
		{
			//graficaenfer.ForeColor = QBColor(0)
			using (Graphics g = graficaenfer.CreateGraphics())
				{

					g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2 - 20), Convert.ToInt32(y2 + 70)), new Point(Convert.ToInt32(x2 + 70), Convert.ToInt32(y2 - 70)));
				}
			using (Graphics g = graficaenfer.CreateGraphics())
				{

					g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2 + 20), Convert.ToInt32(y2 - 70)), new Point(Convert.ToInt32(x2 - 70), Convert.ToInt32(y2 + 70)));
				}
			//graficaenfer.ForeColor = QBColor(12)
		}

		//pinta el punto de la frecuencia cardiaca

		private void pinta_v(float x1, float y1, float x2, float y2)
		{
			//graficaenfer.ForeColor = QBColor(0)
			using (Graphics g = graficaenfer.CreateGraphics())
				{

					g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2 - 20), Convert.ToInt32(y2 + 100)), new Point(Convert.ToInt32(x2 - 20), Convert.ToInt32(y2 - 100)));
				}
			//graficaenfer.Line (x2 + 20, y2 - 100)-(x2 + 20, y2 + 100)
			//graficaenfer.Line (x2 + 20, y2)-(x2 + 20, y2)
			//graficaenfer.ForeColor = QBColor(10)
		}

		// pinta el punto de la tensi�n m�xima

		private void pinta_tensmax(float x1, float y1, float x2, float y2)
		{
			//graficaenfer.ForeColor = QBColor(0)
			using (Graphics g = graficaenfer.CreateGraphics())
				{

					g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2 - 50), Convert.ToInt32(y2 - 100)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
				}
			using (Graphics g = graficaenfer.CreateGraphics())
				{

					g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2 + 50), Convert.ToInt32(y2 - 100)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
				}
			//graficaenfer.ForeColor = QBColor(9)
		}

		// pinta el punto de la tensi�n m�nima

		private void pinta_tensmin(float x1, float y1, float x2, float y2)
		{
			//graficaenfer.ForeColor = QBColor(0)
			using (Graphics g = graficaenfer.CreateGraphics())
				{

					g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2 - 50), Convert.ToInt32(y2 + 100)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
				}
			using (Graphics g = graficaenfer.CreateGraphics())
				{

					g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2 + 50), Convert.ToInt32(y2 + 100)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
				}
			//graficaenfer.ForeColor = QBColor(9)
		}

		private void Tension()
		{
			//CALCULO DE LA TENSION
			//**************************************
			try
			{


				//oscar 04/02/04
				//50 valores en cada tramo / 10 lineas horizontales por tramo = 5
				valgris = 5;
                //------------

                //INIC SDSALAZAR TODO SPRIN 5_5
                //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //graficaenfer.setForeColor(Label1[2].BackColor);
                //INIC SDSALAZAR TODO SPRIN 5_5

                object tempRefParam = stInicioIntervalo;
				object tempRefParam2 = stFinIntervalo;
				stsql = "select E.ntensmax,E.ntensmin ,E.fapuntes  " + 
				        "from econsvit E " + 
				        "Where itiposer = '" + MGRAFICA.stTipSer + "' " + 
				        "and E.ganoregi=" + ganoadme.ToString() + " " + 
				        "and E.gnumregi=" + gnumadme.ToString() + " " + 
				        "and E.ntensmax is not null " + 
				        "and E.fapuntes >= " + Serrores.FormatFechaHMS(tempRefParam) + " " + 
				        "and E.fapuntes <=" + Serrores.FormatFechaHMS(tempRefParam2) + " ";
				stFinIntervalo = Convert.ToString(tempRefParam2);
				stInicioIntervalo = Convert.ToString(tempRefParam);

				stsql = stsql + 
				        " and CONVERT(CHAR(2),E.FAPUNTES,108) in (" + sqlWhereHoras + ")" + 
				        " order by fapuntes";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MGRAFICA.RcEnfermeria);
				rsmagnitud = new DataSet();
				tempAdapter.Fill(rsmagnitud);

                //SDSALAZAR SPRINT 5_5
    //            //UPGRADE_ISSUE: (2070) Constant vbPixels was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
    //            //UPGRADE_ISSUE: (2038) Form property .ScaleMode is not supported. More Information: http://www.vbtonet.com/ewis/ewi2038.aspx
    //            this.setScaleMode(UpgradeStubs.VBRUN_ScaleModeConstants.getvbPixels()); //Selecciono la escala
				////UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.AutoRedraw was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//graficaenfer.setAutoRedraw(true);
                //SDSALAZAR SPRINT 5_5
                x1 = 80;
				x2 = 300;
				y1 = -80;
				y2 = -80;
				p = 30;
				D = 0;
				y2 = (float) (y2 + (y2 * AuGraf));
				y1 = (float) (y1 + (y1 * AuGraf));
				bool paso = false;
				if (rsmagnitud.Tables[0].Rows.Count > 0)
				{
					rsmagnitud.MoveFirst();
					foreach (DataRow iteration_row in rsmagnitud.Tables[0].Rows)
					{
						fecha_epi = DateTime.Parse(Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("dd/MM/yyyy"));
						hora_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH");
						minutos_epi = Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("mm");
						//ponerlo cuando tenga un paciente con datos en constantes_vitales
						while (Label4[p].Text != Convert.ToDateTime(iteration_row["FAPUNTES"]).ToString("HH") + ":00")
						{
							p++;
							D++;
							x2 = (1100 * D) + 300;
							paso = true;
							if (p == 37)
							{
								break;
							}
						}
						if (paso)
						{
							paso = false;
						}
						else
						{
							x2 = (1100 * D) + 300;
						}


						if (Convert.ToDouble(iteration_row["ntensmax"]) >= MGRAFICA.vminTramo1 && Convert.ToDouble(iteration_row["ntensmax"]) < MGRAFICA.vmaxTramo1)
						{
							y2 = 4120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntensmax"]) - MGRAFICA.vminTramo1) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							pinta_tensmax(x2, y2, x2, y2);
							y1 = y2;
							tension_minima(x1, y1, x2);
							x1 = x2;
						}

						if (Convert.ToDouble(iteration_row["ntensmax"]) >= MGRAFICA.vminTramo2 && Convert.ToDouble(iteration_row["ntensmax"]) < MGRAFICA.vmaxTramo2)
						{
							y2 = 3520;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntensmax"]) - MGRAFICA.vminTramo2) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							pinta_tensmax(x2, y2, x2, y2);
							y1 = y2;
							tension_minima(x1, y1, x2);
							x1 = x2;
						}

						if (Convert.ToDouble(iteration_row["ntensmax"]) >= MGRAFICA.vminTramo3 && Convert.ToDouble(iteration_row["ntensmax"]) < MGRAFICA.vmaxTramo3)
						{
							y2 = 2920;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntensmax"]) - MGRAFICA.vminTramo3) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							pinta_tensmax(x2, y2, x2, y2);
							y1 = y2;
							tension_minima(x1, y1, x2);
							x1 = x2;
						}

						if (Convert.ToDouble(iteration_row["ntensmax"]) >= MGRAFICA.vminTramo4 && Convert.ToDouble(iteration_row["ntensmax"]) < MGRAFICA.vmaxTramo4)
						{
							y2 = 2320;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntensmax"]) - MGRAFICA.vminTramo4) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							pinta_tensmax(x2, y2, x2, y2);
							y1 = y2;
							tension_minima(x1, y1, x2);
							x1 = x2;
						}

						if (Convert.ToDouble(iteration_row["ntensmax"]) >= MGRAFICA.vminTramo5 && Convert.ToDouble(iteration_row["ntensmax"]) < MGRAFICA.vmaxTramo5)
						{
							y2 = 1720;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntensmax"]) - MGRAFICA.vminTramo5) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							pinta_tensmax(x2, y2, x2, y2);
							y1 = y2;
							tension_minima(x1, y1, x2);
							x1 = x2;
						}

						if (Convert.ToDouble(iteration_row["ntensmax"]) >= MGRAFICA.vminTramo6 && Convert.ToDouble(iteration_row["ntensmax"]) <= MGRAFICA.vmaxTramo6)
						{
							y2 = 1120;
							y2 = (float) (y2 - (60 * ((Convert.ToDouble(iteration_row["ntensmax"]) - MGRAFICA.vminTramo6) / valgris)));
							x2 = (float) (x2 + (1100 / ((int) 60)) * Double.Parse(minutos_epi));
							y2 = (float) (y2 + (y2 * AuGraf));
							pinta_tensmax(x2, y2, x2, y2);
							y1 = y2;
							tension_minima(x1, y1, x2);
							x1 = x2;
						}

					}
                    rsmagnitud.Close();
                }
			}
			catch(SqlException SqlExep)
			{
				Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "tesion:GRAFICA", SqlExep);
				rsmagnitud = null;
			}
            catch (Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }
        }
		private void tension_minima(float x1, float y1, float x2)
		{

			//oscar 04/02/04
			//50 valores en cada tramo / 10 lineas horizontales por tramo = 5
			valgris = 5;
			//------------


			if (Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) >= MGRAFICA.vminTramo1 && Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) < MGRAFICA.vmaxTramo1)
			{
				y2 = 4120;
				y2 = (float) (y2 - (60 * ((Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) - MGRAFICA.vminTramo1) / valgris)));
				y2 = (float) (y2 + (y2 * AuGraf));
				using (Graphics g = graficaenfer.CreateGraphics())
					{

						g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
					}
				pinta_tensmin(x1, y1, x2, y2);
			}


			if (Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) >= MGRAFICA.vminTramo2 && Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) < MGRAFICA.vmaxTramo2)
			{
				y2 = 3520;
				y2 = (float) (y2 - (60 * ((Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) - MGRAFICA.vminTramo2) / valgris)));
				y2 = (float) (y2 + (y2 * AuGraf));
				using (Graphics g = graficaenfer.CreateGraphics())
					{

						g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
					}
				pinta_tensmin(x1, y1, x2, y2);
			}

			if (Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) >= MGRAFICA.vminTramo3 && Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) < MGRAFICA.vmaxTramo3)
			{
				y2 = 2920;
				y2 = (float) (y2 - (60 * ((Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) - MGRAFICA.vminTramo3) / valgris)));
				y2 = (float) (y2 + (y2 * AuGraf));
				using (Graphics g = graficaenfer.CreateGraphics())
					{

						g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
					}
				pinta_tensmin(x1, y1, x2, y2);
			}

			if (Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) >= MGRAFICA.vminTramo4 && Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) < MGRAFICA.vmaxTramo4)
			{
				y2 = 2320;
				y2 = (float) (y2 - (60 * ((Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) - MGRAFICA.vminTramo4) / valgris)));
				y2 = (float) (y2 + (y2 * AuGraf));
				using (Graphics g = graficaenfer.CreateGraphics())
					{

						g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
					}
				pinta_tensmin(x1, y1, x2, y2);
			}

			if (Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) >= MGRAFICA.vminTramo5 && Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) < MGRAFICA.vmaxTramo5)
			{
				y2 = 1720;
				y2 = (float) (y2 - (60 * ((Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) - MGRAFICA.vminTramo5) / valgris)));
				y2 = (float) (y2 + (y2 * AuGraf));
				using (Graphics g = graficaenfer.CreateGraphics())
					{

						g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
					}
				pinta_tensmin(x1, y1, x2, y2);
			}

			if (Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) >= MGRAFICA.vminTramo6 && Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) <= MGRAFICA.vmaxTramo6)
			{
				y2 = 1120;
				y2 = (float) (y2 - (60 * ((Convert.ToDouble(rsmagnitud.Tables[0].Rows[0]["ntensmin"]) - MGRAFICA.vminTramo6) / valgris)));
				y2 = (float) (y2 + (y2 * AuGraf));
				using (Graphics g = graficaenfer.CreateGraphics())
					{

						g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
					}
				pinta_tensmin(x1, y1, x2, y2);
			}

		}

		//****************************************************************************************
		//*  Procedimiento: lineas                                                               *
		//*                                                                                      *
		//*  Objeto: Posicionar los contoles en pantalla                                         *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias.                                                                          *
		//*                                                                                      *
		//*    16/10/2006 -  - Se incorpora la colocaci�n de los nuevo Labels de Dolor (EVA)     *
		//*                                                                                      *
		//****************************************************************************************
		private void lineas()
		{
            //SDSALAZAR SPRINT 5_5
   //         //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
   //         graficaenfer.setForeColor(Color.Black);
			////UPGRADE_ISSUE: (2070) Constant vbPixels was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
			////UPGRADE_ISSUE: (2038) Form property .ScaleMode is not supported. More Information: http://www.vbtonet.com/ewis/ewi2038.aspx
			//this.setScaleMode(UpgradeStubs.VBRUN_ScaleModeConstants.getvbPixels()); //Selecciono la escala
			////UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.AutoRedraw was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//graficaenfer.setAutoRedraw(true);
            //SDSALAZAR SPRINT 5_5
            //Crea las lineas de intervalo peque�o
            x1 = 30;
			x1 = 20;
			y1 = -80;
			x2 = 10000;
			y2 = -80;
			y2 = (float) (y2 + (y2 * AuGraf));
			y1 = (float) (y1 + (y1 * AuGraf));
			j = 0;
			int nlineagris = 0;
			for (i = 70; i >= 0; i--)
			{
                //UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.DrawWidth was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                //INIC SDSALAZAR TODO SPRIN 5_5
                //graficaenfer.setDrawWidth(1);
				//if (j == 10)
				//{
				//	//UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//	graficaenfer.setForeColor(Color.Black);
				//	j = 1;
				//	if (nlineagris == 46)
				//	{
				//		//UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.DrawWidth was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//		graficaenfer.setDrawWidth(2);
				//		//UPGRADE_WARNING: (2080) QBCOlor(12) was upgraded to System.Drawing.Color.Red and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				//		//UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//		graficaenfer.setForeColor(Color.Red);
				//	}
				//}
				//else
				//{
				//	if (i == 60)
				//	{
				//		//UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//		graficaenfer.setForeColor(Color.Black);
				//	}
				//	else
				//	{
				//		nlineagris++;
				//		if (nlineagris == 33)
				//		{
				//			//Como se ha bajado la temp correspondiente a 37�, la linea 33 que antes se
				//			//pintaba de rojo, se pinta de blanco ya que no coincidia con ninguna linea gris
				//			//graficaenfer.ForeColor = QBColor(12)
				//			//graficaenfer.DrawWidth = 1.4
				//			//UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//			graficaenfer.setForeColor(Color.White);
				//		}
				//		else
				//		{
				//			//UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//			graficaenfer.setForeColor(Color.Gray);
				//		}
				//		j++;
				//	}
				//}
				//if (j % 2 == 0)
				//{
				//	//UPGRADE_ISSUE: (2064) PictureBox property graficaenfer.ForeColor was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//	graficaenfer.setForeColor(Color.White);
				//}
                //INIC SDSALAZAR TODO SPRIN 5_5
                using (Graphics g = graficaenfer.CreateGraphics())
					{

						g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
					}

				y1 = (float) (y1 + (60 + (60 * AuGraf)));
				y2 = (float) (y2 + (60 + (60 * AuGraf)));
			}

			// Crea las lineas verticales
			y1 = 1;
			y2 = 5000;
			x1 = 300;
			x2 = 300;
			y2 = (float) (y2 + (y2 * AuGraf));
			for (i = 8; i >= 1; i--)
			{
				using (Graphics g = graficaenfer.CreateGraphics())
					{

						g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2), Convert.ToInt32(y2)));
					}
				x1 += 1100;
				x2 += 1100;
			}

			using (Graphics g = graficaenfer.CreateGraphics())
				{

					g.DrawLine(new Pen(default(Color)), new Point(Convert.ToInt32(x1 + 50), Convert.ToInt32(y1)), new Point(Convert.ToInt32(x2 + 50), Convert.ToInt32(y2)));
				}
			if (!LinRef)
			{
				Line1.Y1 = (int) (Line1.Y1 + (Line1.Y1 * AuGraf) - 17.5d / 15);
				Line1.Y2 = (int) (Line1.Y2 + (Line1.Y2 * AuGraf) - 17.5d / 15);

				Line2.Y1 = (int) (Line2.Y1 + (Line2.Y1 * AuGraf) - 17.5d / 15);
				Line2.Y2 = (int) (Line2.Y2 + (Line2.Y2 * AuGraf) - 17.5d / 15);

				Line3.Y1 = (int) (Line3.Y1 + (Line3.Y1 * AuGraf) - 17.5d / 15);
				Line3.Y2 = (int) (Line3.Y2 + (Line3.Y2 * AuGraf) - 17.5d / 15);

				Line4.Y1 = (int) (Line4.Y1 + (Line4.Y1 * AuGraf) - 17.5d / 15);
				Line4.Y2 = (int) (Line4.Y2 + (Line4.Y2 * AuGraf) - 17.5d / 15);

				Line5.Y1 = (int) (Line5.Y1 + (Line5.Y1 * AuGraf) - 17.5d / 15);
				Line5.Y2 = (int) (Line5.Y2 + (Line5.Y2 * AuGraf) - 17.5d / 15);

				Line6.Y1 = (int) (Line6.Y1 + (Line6.Y1 * AuGraf) - 17.5d / 15);
				Line6.Y2 = (int) (Line6.Y2 + (Line6.Y2 * AuGraf) - 17.5d / 15);

				Line7.Y1 = (int) (Line7.Y1 + (Line7.Y1 * AuGraf) - 17.5d / 15);
				Line7.Y2 = (int) (Line7.Y2 + (Line7.Y2 * AuGraf) - 17.5d / 15);

				for (i = 4; i <= 27; i++)
				{
					Label1[i].Top = (int) (Label1[i].Top + (Label1[i].Top * AuGraf) - 1);
				}
				for (i = 32; i <= 35; i++)
				{
					Label1[i].Top = (int) (Label1[i].Top + (Label1[i].Top * AuGraf) - 1);
				}
				for (i = 38; i <= 44; i++)
				{
					Label1[i].Top = (int) (Label1[i].Top + (Label1[i].Top * AuGraf) - 1);
				}
				for (i = 30; i <= 36; i++)
				{
					Label4[i].Top = (int) (Label4[i].Top + (Label4[i].Top * AuGraf) - 1); //15
				}

				//************* O.Frias - 16/10/2006 - Inicio ****************
				for (i = 38; i <= 44; i++)
				{
					Label1[i + 9].Top = (int) (Label1[i].Top - 1.5d / 15);
				}
				//************** O.Frias - 16/10/2006 - Fin ******************



				Command1.Top = (int) (Command1.Top + (Command1.Top * AuGraf) - 1);
				Command2.Top = (int) (Command2.Top + (Command2.Top * AuGraf) - 1);

				LinRef = true;

				vsDatos.Top = (int) (vsDatos.Top + (vsDatos.Top * AuGraf) - 2);

			}
			y1 = 1;
			y2 = 5000;
			x1 = 400;
			x2 = 400;
		}
        //UPGRADE_WARNING: (206) Untranslated statement in Ir_a_TABLA_temporal. Please check source code. More Information: http://www.vbtonet.com/ewis/ewi206.aspx
        private void Ir_a_TABLA_temporal(string Nomfichero)
        {
            object dbLangSpanish = null;
            object DBEngine = null;
            bool vbBase = false;
            //UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            string sqltemporal = String.Empty;
            string vstPathTemp = String.Empty;

            int DataFile = 0;
            int Fl = 0;
            int Chunks = 0;
            int Fragment = 0;
            byte[] Chunk = null;
            const int ChunkSize = 16384;

            try
            {
                //Se crea una base de datos temporal para realizar el informe
                vstPathTemp = MGRAFICA.stPathAplicacion + "\\" + MGRAFICA.stNombreBD;
                if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
                {
                    vbBase = true;
                }

                if (!vbBase)
                { // SI est� dado
                    //SDSALAZAR_SPRINT_X_5
                  ////UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                  //  MGRAFICA.bs = (Database)DBEngine.Workspaces(0).CreateDatabase(vstPathTemp, dbLangSpanish);
                    vbBase = true;
                }
                else
                {
                    //SDSALAZAR_SPRINT_X_5
                    ////UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //MGRAFICA.bs = (Database)DBEngine.Workspaces(0).OpenDatabase(vstPathTemp);
                    ////UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //foreach (TableDef TABLA in MGRAFICA.bs.TableDefs)
                    //{
                    //    //UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //    if (Convert.ToString(TABLA.Name) == "Gr�fica")
                    //    {
                    //        //UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //        MGRAFICA.bs.Execute("DROP TABLE Gr�fica");
                    //    }
                    //}
                }

                //crear la tabla temporal
                //SDSALAZAR_SPRINT_X_5
                ////UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MGRAFICA.bs.Execute("CREATE TABLE Gr�fica (Dibujo LongBinary)");

                //llenar la tabla temporal

                sqltemporal = "select * from Gr�fica  where 1=2";

                //SDSALAZAR_SPRINT_X_5
                ////UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MGRAFICA.Rs = (Recordset)MGRAFICA.bs.OpenRecordset(sqltemporal);
                //if (Graphics.FromImage(Image1.Image).GetHdc().ToInt32() != 0)
                //{
                //    //UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //    MGRAFICA.Rs.AddNew();
                //    DataFile = 1;
                //    FileSystem.FileOpen(DataFile, Nomfichero, OpenMode.Binary, OpenAccess.Read, OpenShare.Default, -1);

                //    Fl = (int)FileSystem.LOF(DataFile); // Longitud de los datos en el archivo
                //    if (Fl == 0)
                //    {
                //        FileSystem.FileClose(DataFile);
                //        return;
                //    }

                //    Chunks = Fl / ChunkSize;

                //    Fragment = Fl % ChunkSize;

                //    Chunk = new byte[Fragment + 1];

                //    //UPGRADE_WARNING: (2080) Get was upgraded to FileGet and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
                //    Array TempArray = Array.CreateInstance(Chunk.GetType().GetElementType(), Chunk.Length);
                //    FileSystem.FileGet(DataFile, ref TempArray, -1, false, false);
                //    Array.Copy(TempArray, Chunk, TempArray.Length);

                //    //UPGRADE_TODO: (1067) Member AppendChunk is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //    MGRAFICA.Rs("Dibujo").AppendChunk(Chunk);

                //    Chunk = new byte[ChunkSize + 1];

                //    for (int i = 1; i <= Chunks; i++)
                //    {
                //        //UPGRADE_WARNING: (2080) Get was upgraded to FileGet and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
                //        Array TempArray2 = Array.CreateInstance(Chunk.GetType().GetElementType(), Chunk.Length);
                //        FileSystem.FileGet(DataFile, ref TempArray2, -1, false, false);
                //        Array.Copy(TempArray2, Chunk, TempArray2.Length);
                //        //UPGRADE_TODO: (1067) Member AppendChunk is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //        MGRAFICA.Rs("Dibujo").AppendChunk(Chunk);
                //    }
                //    FileSystem.FileClose(DataFile);

                //    //UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //    MGRAFICA.Rs.Update();
                //}

                ////UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MGRAFICA.bs.Close();
            }
            catch(Exception Excep)
            {
                //SDSALAZAR_SPRINT_X_5
                //MGRAFICA.Rs = null; //se cierra el registro que escribe en tratamientos
                //MGRAFICA.bs = null; //se cierra la base de datos
                Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "Ir_a_TABLA_temporal", Excep);
            }
        }
        private void GHORARIA_SH_Closed(Object eventSender, EventArgs eventArgs)
		{
			RRhorasApintar = null;
		}


		private void proImprimirGraficaActiva()
		{

			int PAGINA = 0;

			string Nomfichero = Path.GetDirectoryName(Application.ExecutablePath) + "\\grafico.bmp";

			//para imprimir el gr�fico actual en pantalla

			object cryInformes = MGRAFICA.oCrystal;
			int Color_Original = ColorTranslator.ToOle(Frame1.BackColor);
			this.BackColor = Color.White;

			//Me.WindowState = vbMaximized
			//Ajustamos el tama�o del formulario al tama�o minimo que ocupa la grafica
			this.Width = 700;
			this.Height = 617;

			this.Cursor = Cursors.WaitCursor;
			this.Command1.Visible = false;
			this.Command2.Visible = false;
			this.Command3.Visible = false;
			this.Command4.Visible = false;
			this.Command5.Visible = false;

			for (i = 4; i <= 27; i++)
			{
				Label1[i].BackColor = Color.White;
			}
			for (i = 32; i <= 35; i++)
			{
				Label1[i].BackColor = Color.White;
			}

			for (i = 38; i <= 44; i++)
			{
				Label1[i].BackColor = Color.White;
			}
			for (i = 30; i <= 36; i++)
			{
				Label4[i].BackColor = Color.White;
			}
			for (i = 47; i <= 53; i++)
			{
				Label1[i].BackColor = Color.White;
			}

			Frame1.BackColor = Color.White;
			Label2.BackColor = Color.White;
			Label3.BackColor = Color.White;

			Label6.BackColor = Color.White;
			LbFechaGrafica.BackColor = Color.White;

			this.Activate();

			Application.DoEvents();
			// borra el portapapeles
			Clipboard.Clear();
			Application.DoEvents();

			// Manda la pulsaci�n de teclas para capturar la imagen de la pantalla
			UpgradeSupportHelper.PInvoke.SafeNative.user32.keybd_event(44, 1, 0, 0);

			Application.DoEvents();

			// Si el formato del clipboard es un bitmap
			if (Clipboard.ContainsData(DataFormats.Bitmap))
			{
                //Guardamos la imagen en disco
                //SavePicture Clipboard.GetData(vbCFBitmap), Path
                //MsgBox " Captura generada en: " & Path, vbInformation

                //INIC SDSALAZAR TODO SPRIN 5_5
                //Image1.Image = Clipboard.GetData(DataFormats.Bitmap);
				//UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				Image1.Image.Save(Nomfichero);
				Ir_a_TABLA_temporal(Nomfichero);
                //INIC SDSALAZAR TODO SPRIN 5_5

                File.Delete(Nomfichero);
			}
			else
			{
                RadMessageBox.Show(" Error Capturando la Imagen del portapapeles", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
				return;
			}


			PAGINA++;
			int ViForm = 3;
			MGRAFICA.vacia_formulas(cryInformes, ViForm); //limpio las formulas

			if (!MGRAFICA.VstrCrystal.VfCabecera)
			{
				MGRAFICA.proCabCrystal();
			}
            // INIC SDSALAZAR TODO SPRINT X_5
            //         //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //         cryInformes.Formulas["FORM1"] = "\"" + MGRAFICA.VstrCrystal.VstGrupoHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Formulas["FORM2"] = "\"" + MGRAFICA.VstrCrystal.VstNombreHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Formulas["FORM3"] = "\"" + MGRAFICA.VstrCrystal.VstDireccHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Formulas["NPAGINA"] = "\"" + PAGINA.ToString() + "\"";
            ////UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.ReportFileName = MGRAFICA.PathReport + "\\EGI240R1.rpt";
            ////UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.SubreportToChange = "GRAFICA";
            ////UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.DataFiles[0] = MGRAFICA.stPathAplicacion + "\\" + MGRAFICA.stNombreBD;
            ////UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.WindowState = 2; //crptMaximized
            ////UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.WindowTitle = "Gr�fica de enfermeria";
            ////UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Destination = 1;
            ////UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Action = 1;
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.PageCount();
            ////UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.PrinterStopPage = cryInformes.PageCount;
            ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Reset();
            // INIC SDSALAZAR TODO SPRINT X_5
            ViForm = 3;
			MGRAFICA.vacia_formulas(cryInformes, ViForm); //limpio las formulas


			this.BackColor = ColorTranslator.FromOle(Color_Original);
			for (i = 4; i <= 27; i++)
			{
				Label1[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}
			for (i = 32; i <= 35; i++)
			{
				Label1[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}

			for (i = 38; i <= 44; i++)
			{
				Label1[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}
			for (i = 30; i <= 36; i++)
			{
				Label4[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}
			for (i = 47; i <= 53; i++)
			{
				Label1[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}

			Frame1.BackColor = ColorTranslator.FromOle(Color_Original);
			Label2.BackColor = ColorTranslator.FromOle(Color_Original);
			Label3.BackColor = ColorTranslator.FromOle(Color_Original);
			Label6.BackColor = ColorTranslator.FromOle(Color_Original);
			LbFechaGrafica.BackColor = ColorTranslator.FromOle(Color_Original);
			this.Command1.Visible = true;
			this.Command2.Visible = true;
			this.Command3.Visible = true;
			this.Command4.Visible = true;
			this.Command5.Visible = true;

			cryInformes = null;
			this.Cursor = Cursors.Default;
			Clipboard.Clear();

			//Volvemos a dejar la pantalla maximizada
			this.Height = (int) Screen.PrimaryScreen.Bounds.Height;
			this.Width = (int) Screen.PrimaryScreen.Bounds.Width;
		}

		private void proImprimirGraficaCompleta()
		{

			int PAGINA = 0;
			int ViForm = 0;

			object cryInformes = MGRAFICA.oCrystal;
			string Nomfichero = Path.GetDirectoryName(Application.ExecutablePath) + "\\grafico.bmp";

			//Ajustamos el tama�o del formulario al tama�o minimo que ocupa la grafica
			this.Width = 700;
			this.Height = 617;


			this.Cursor = Cursors.WaitCursor;
			this.Command1.Visible = false;
			this.Command2.Visible = false;
			this.Command3.Visible = false;
			this.Command4.Visible = false;
			this.Command5.Visible = false;

			int Color_Original = ColorTranslator.ToOle(Frame1.BackColor);

			this.BackColor = Color.White;

			for (i = 4; i <= 35; i++)
			{
				Label1[i].BackColor = Color.White;
			}
			for (i = 38; i <= 44; i++)
			{
				Label1[i].BackColor = Color.White;
			}
			for (i = 30; i <= 36; i++)
			{
				Label4[i].BackColor = Color.White;
			}


			for (i = 47; i <= 53; i++)
			{
				Label1[i].BackColor = Color.White;
			}

			Frame1.BackColor = Color.White;
			Label2.BackColor = Color.White;
			Label3.BackColor = Color.White;
			Label6.BackColor = Color.White;
			LbFechaGrafica.BackColor = Color.White;


			if (Command2.Enabled || Command1.Enabled)
			{


				while(Command1.Enabled)
				{
					Command1_Click(Command1, new EventArgs());
				};

				while(Command2.Enabled)
				{
					Application.DoEvents();
					// borra el portapapeles
					Clipboard.Clear();
					Application.DoEvents();
					// Manda la pulsaci�n de teclas para capturar la imagen de la pantalla
					UpgradeSupportHelper.PInvoke.SafeNative.user32.keybd_event(44, 1, 0, 0);
					Application.DoEvents();
					// Si el formato del clipboard es un bitmap
					if (Clipboard.ContainsData(DataFormats.Bitmap))
					{
                        //Guardamos la imagen en disco
                        //SavePicture Clipboard.GetData(vbCFBitmap), Path
                        //MsgBox " Captura generada en: " & Path, vbInformation

                        //INIC SDSALAZAR TODO SPRIN 5_5
                        //Image1.Image = Clipboard.GetData(DataFormats.Bitmap);
                        //UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
                        //INIC SDSALAZAR TODO SPRIN 5_5
                        Image1.Image.Save(Nomfichero);
						Ir_a_TABLA_temporal(Nomfichero);
                       

                        File.Delete(Nomfichero);
					}
					else
					{
						MessageBox.Show(" Error Capturando la Imagen del portapapeles", Serrores.vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
						return;
					}



					PAGINA++;
					ViForm = 3;
					MGRAFICA.vacia_formulas(cryInformes, ViForm); //limpio las formulas
					if (!MGRAFICA.VstrCrystal.VfCabecera)
					{
						MGRAFICA.proCabCrystal();
					}
                    // INIC SDSALAZAR TODO SPRINT X_5
                    //               //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //               cryInformes.Formulas["FORM1"] = "\"" + MGRAFICA.VstrCrystal.VstGrupoHo + "\"";
                    ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.Formulas["FORM2"] = "\"" + MGRAFICA.VstrCrystal.VstNombreHo + "\"";
                    ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.Formulas["FORM3"] = "\"" + MGRAFICA.VstrCrystal.VstDireccHo + "\"";
                    ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.Formulas["NPAGINA"] = "\"" + PAGINA.ToString() + "\"";
                    ////UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.ReportFileName = MGRAFICA.PathReport + "\\EGI240R1.rpt";
                    ////UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.SubreportToChange = "GRAFICA";
                    ////UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.DataFiles[0] = MGRAFICA.stPathAplicacion + "\\" + MGRAFICA.stNombreBD;
                    ////UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.WindowState = 2; //crptMaximized
                    ////UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.Destination = 1;
                    ////UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.WindowTitle = "Gr�fica de enfermer�a (Hoja " + PAGINA.ToString() + ")";
                    ////UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.Action = 1;
                    ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.PageCount();
                    ////UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.PrinterStopPage = cryInformes.PageCount;
                    ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //cryInformes.Reset();
                    // INIC SDSALAZAR TODO SPRINT X_5
                    ViForm = 3;
					MGRAFICA.vacia_formulas(cryInformes, ViForm); //limpio las formulas

					Command2_Click(Command2, new EventArgs());
					if (Command2.Enabled)
					{
						this.Activate();
					}
				};
			}

			//para imprimir el ultimo gr�fico
			this.Activate();
			Application.DoEvents();

			// borra el portapapeles
			Clipboard.Clear();
			Application.DoEvents();

			// Manda la pulsaci�n de teclas para capturar la imagen de la pantalla (formulario activo)
			UpgradeSupportHelper.PInvoke.SafeNative.user32.keybd_event(44, 1, 0, 0);
			Application.DoEvents();

			// Si el formato del clipboard es un bitmap
			if (Clipboard.ContainsData(DataFormats.Bitmap))
			{
                //Guardamos la imagen en disco
                //SavePicture Clipboard.GetData(vbCFBitmap), Path
                //MsgBox " Captura generada en: " & Path, vbInformation

                //INIC SDSALAZAR TODO SPRIN 5_5
                //Image1.Image = Clipboard.GetData(DataFormats.Bitmap);
				//UPGRADE_WARNING: (2080) SavePicture was upgraded to System.Drawing.Image.Save and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				Image1.Image.Save(Nomfichero);
				Ir_a_TABLA_temporal(Nomfichero);
                //INIC SDSALAZAR TODO SPRIN 5_5

                File.Delete(Nomfichero);
			}
			else
			{
                RadMessageBox.Show(" Error Capturando la Imagen del portapapeles", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
				return;
			}



			PAGINA++;
			ViForm = 3;
			MGRAFICA.vacia_formulas(cryInformes, ViForm); //limpio las formulas

			if (!MGRAFICA.VstrCrystal.VfCabecera)
			{
				MGRAFICA.proCabCrystal();
			}
            // INIC SDSALAZAR TODO SPRINT X_5
            //         //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //         cryInformes.Formulas["FORM1"] = "\"" + MGRAFICA.VstrCrystal.VstGrupoHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Formulas["FORM2"] = "\"" + MGRAFICA.VstrCrystal.VstNombreHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Formulas["FORM3"] = "\"" + MGRAFICA.VstrCrystal.VstDireccHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Formulas["NPAGINA"] = "\"" + PAGINA.ToString() + "\"";
            ////UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.ReportFileName = MGRAFICA.PathReport + "\\EGI240R1.rpt";
            ////UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.SubreportToChange = "GRAFICA";
            ////UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.DataFiles[0] = MGRAFICA.stPathAplicacion + "\\" + MGRAFICA.stNombreBD;
            ////UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.WindowState = 2; //crptMaximized
            ////UPGRADE_TODO: (1067) Member ProgressDialog is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.ProgressDialog = false;
            ////UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Destination = 1;
            ////UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.WindowTitle = "Gr�fica de enfermer�a (Hoja " + PAGINA.ToString() + ")";
            ////UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Action = 1;
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.PageCount();
            ////UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.PrinterStopPage = cryInformes.PageCount;
            ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //cryInformes.Reset();
            // INIC SDSALAZAR TODO SPRINT X_5
            ViForm = 3;
			MGRAFICA.vacia_formulas(cryInformes, ViForm); //limpio las formulas

			Clipboard.Clear();
			Application.DoEvents();
			this.BackColor = ColorTranslator.FromOle(Color_Original);
			for (i = 4; i <= 27; i++)
			{
				Label1[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}
			for (i = 32; i <= 35; i++)
			{
				Label1[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}

			for (i = 38; i <= 44; i++)
			{
				Label1[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}
			for (i = 30; i <= 36; i++)
			{
				Label4[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}
			for (i = 47; i <= 53; i++)
			{
				Label1[i].BackColor = ColorTranslator.FromOle(Color_Original);
			}

			Frame1.BackColor = ColorTranslator.FromOle(Color_Original);
			Label2.BackColor = ColorTranslator.FromOle(Color_Original);
			Label3.BackColor = ColorTranslator.FromOle(Color_Original);
			Label6.BackColor = ColorTranslator.FromOle(Color_Original);
			LbFechaGrafica.BackColor = ColorTranslator.FromOle(Color_Original);

			this.Command1.Visible = true;
			this.Command2.Visible = true;
			this.Command3.Visible = true;
			this.Command4.Visible = true;
			this.Command5.Visible = true;

			cryInformes = null;
			this.Cursor = Cursors.Default;
			Clipboard.Clear();

			//Volvemos a dejar la pantalla maximizada
			this.Height = (int) Screen.PrimaryScreen.Bounds.Height;
			this.Width = (int) Screen.PrimaryScreen.Bounds.Width;
		}
	}
}