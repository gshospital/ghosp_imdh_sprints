using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace GRAFICADLL
{
	partial class GHORARIA_SH
	{

		#region "Upgrade Support "
		private static GHORARIA_SH m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static GHORARIA_SH DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new GHORARIA_SH();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "vsDatos", "Picture1", "graficaenfer", "Command1", "Command2", "Command3", "Command4", "Label2", "Label3", "Frame1", "Command5", "_Label1_47", "_Label1_48", "_Label1_49", "_Label1_50", "_Label1_51", "_Label1_52", "_Label1_53", "_Label1_45", "_Label1_0", "_Label1_1", "_Label1_2", "_Label1_3", "Line1", "Line2", "Line3", "Line4", "Line5", "Line6", "Line7", "_Label1_4", "_Label1_5", "_Label1_6", "_Label1_7", "_Label1_8", "_Label1_9", "_Label1_10", "_Label1_11", "_Label1_12", "_Label1_13", "_Label1_14", "_Label1_15", "_Label1_16", "_Label1_17", "_Label1_18", "_Label1_19", "_Label1_20", "_Label1_21", "_Label1_22", "_Label1_23", "_Label1_24", "_Label1_25", "_Label1_26", "_Label1_27", "_Label4_36", "_Label4_35", "_Label4_34", "_Label4_33", "_Label4_32", "_Label4_31", "_Label4_30", "_Label1_28", "_Label1_29", "_Label1_30", "_Label1_31", "Image1", "_Label1_32", "_Label1_33", "_Label1_34", "_Label1_35", "LbFechaGrafica", "Label6", "_Label1_36", "_Label1_37", "_Label1_38", "_Label1_39", "_Label1_40", "_Label1_41", "_Label1_42", "_Label1_43", "_Label1_44", "Label1", "Label4", "ShapeContainer2", "vsDatos_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread vsDatos;
		public System.Windows.Forms.PictureBox Picture1;
		public System.Windows.Forms.PictureBox graficaenfer;
		public Telerik.WinControls.UI.RadButton Command1;
		public Telerik.WinControls.UI.RadButton Command2;
		public Telerik.WinControls.UI.RadButton Command3;
		public Telerik.WinControls.UI.RadButton Command4;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton Command5;
		private Telerik.WinControls.UI.RadLabel _Label1_47;
		private Telerik.WinControls.UI.RadLabel _Label1_48;
		private Telerik.WinControls.UI.RadLabel _Label1_49;
		private Telerik.WinControls.UI.RadLabel _Label1_50;
		private Telerik.WinControls.UI.RadLabel _Label1_51;
		private Telerik.WinControls.UI.RadLabel _Label1_52;
		private Telerik.WinControls.UI.RadLabel _Label1_53;
		private Telerik.WinControls.UI.RadLabel _Label1_45;
		private Telerik.WinControls.UI.RadLabel _Label1_0;
		private Telerik.WinControls.UI.RadLabel _Label1_1;
		private Telerik.WinControls.UI.RadLabel _Label1_2;
		private Telerik.WinControls.UI.RadLabel _Label1_3;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line2;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line3;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line4;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line5;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line6;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line7;
		private Telerik.WinControls.UI.RadLabel _Label1_4;
		private Telerik.WinControls.UI.RadLabel _Label1_5;
		private Telerik.WinControls.UI.RadLabel _Label1_6;
		private Telerik.WinControls.UI.RadLabel _Label1_7;
		private Telerik.WinControls.UI.RadLabel _Label1_8;
		private Telerik.WinControls.UI.RadLabel _Label1_9;
		private Telerik.WinControls.UI.RadLabel _Label1_10;
		private Telerik.WinControls.UI.RadLabel _Label1_11;
		private Telerik.WinControls.UI.RadLabel _Label1_12;
		private Telerik.WinControls.UI.RadLabel _Label1_13;
		private Telerik.WinControls.UI.RadLabel _Label1_14;
		private Telerik.WinControls.UI.RadLabel _Label1_15;
		private Telerik.WinControls.UI.RadLabel _Label1_16;
		private Telerik.WinControls.UI.RadLabel _Label1_17;
		private Telerik.WinControls.UI.RadLabel _Label1_18;
		private Telerik.WinControls.UI.RadLabel _Label1_19;
		private Telerik.WinControls.UI.RadLabel _Label1_20;
		private Telerik.WinControls.UI.RadLabel _Label1_21;
		private Telerik.WinControls.UI.RadLabel _Label1_22;
		private Telerik.WinControls.UI.RadLabel _Label1_23;
		private Telerik.WinControls.UI.RadLabel _Label1_24;
		private Telerik.WinControls.UI.RadLabel _Label1_25;
		private Telerik.WinControls.UI.RadLabel _Label1_26;
		private Telerik.WinControls.UI.RadLabel _Label1_27;
		private Telerik.WinControls.UI.RadLabel _Label4_36;
		private Telerik.WinControls.UI.RadLabel _Label4_35;
		private Telerik.WinControls.UI.RadLabel _Label4_34;
		private Telerik.WinControls.UI.RadLabel _Label4_33;
		private Telerik.WinControls.UI.RadLabel _Label4_32;
		private Telerik.WinControls.UI.RadLabel _Label4_31;
		private Telerik.WinControls.UI.RadLabel _Label4_30;
		private Telerik.WinControls.UI.RadLabel _Label1_28;
		private Telerik.WinControls.UI.RadLabel _Label1_29;
		private Telerik.WinControls.UI.RadLabel _Label1_30;
		private Telerik.WinControls.UI.RadLabel _Label1_31;
		public System.Windows.Forms.PictureBox Image1;
		private Telerik.WinControls.UI.RadLabel _Label1_32;
		private Telerik.WinControls.UI.RadLabel _Label1_33;
		private Telerik.WinControls.UI.RadLabel _Label1_34;
		private Telerik.WinControls.UI.RadLabel _Label1_35;
		public Telerik.WinControls.UI.RadLabel LbFechaGrafica;
		public Telerik.WinControls.UI.RadLabel Label6;
		private Telerik.WinControls.UI.RadLabel _Label1_36;
		private Telerik.WinControls.UI.RadLabel _Label1_37;
		private Telerik.WinControls.UI.RadLabel _Label1_38;
		private Telerik.WinControls.UI.RadLabel _Label1_39;
		private Telerik.WinControls.UI.RadLabel _Label1_40;
		private Telerik.WinControls.UI.RadLabel _Label1_41;
		private Telerik.WinControls.UI.RadLabel _Label1_42;
		private Telerik.WinControls.UI.RadLabel _Label1_43;
		private Telerik.WinControls.UI.RadLabel _Label1_44;
		public Telerik.WinControls.UI.RadLabel[] Label1 = new Telerik.WinControls.UI.RadLabel[54];
		public Telerik.WinControls.UI.RadLabel[] Label4 = new Telerik.WinControls.UI.RadLabel[37];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
		//private FarPoint.Win.Spread.SheetView vsDatos_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GHORARIA_SH));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.vsDatos = new UpgradeHelpers.Spread.FpSpread();
			this.Picture1 = new System.Windows.Forms.PictureBox();
			this.graficaenfer = new System.Windows.Forms.PictureBox();
			this.Command1 = new Telerik.WinControls.UI.RadButton();
			this.Command2 = new Telerik.WinControls.UI.RadButton();
			this.Command3 = new Telerik.WinControls.UI.RadButton();
			this.Command4 = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.Command5 = new Telerik.WinControls.UI.RadButton();
			this._Label1_47 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_48 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_49 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_50 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_51 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_52 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_53 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_45 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_0 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_1 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_2 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_3 = new Telerik.WinControls.UI.RadLabel();
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this._Label1_4 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_5 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_6 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_7 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_8 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_9 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_10 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_11 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_12 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_13 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_14 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_15 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_16 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_17 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_18 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_19 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_20 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_21 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_22 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_23 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_24 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_25 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_26 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_27 = new Telerik.WinControls.UI.RadLabel();
			this._Label4_36 = new Telerik.WinControls.UI.RadLabel();
			this._Label4_35 = new Telerik.WinControls.UI.RadLabel();
			this._Label4_34 = new Telerik.WinControls.UI.RadLabel();
			this._Label4_33 = new Telerik.WinControls.UI.RadLabel();
			this._Label4_32 = new Telerik.WinControls.UI.RadLabel();
			this._Label4_31 = new Telerik.WinControls.UI.RadLabel();
			this._Label4_30 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_28 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_29 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_30 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_31 = new Telerik.WinControls.UI.RadLabel();
			this.Image1 = new System.Windows.Forms.PictureBox();
			this._Label1_32 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_33 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_34 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_35 = new Telerik.WinControls.UI.RadLabel();
			this.LbFechaGrafica = new Telerik.WinControls.UI.RadLabel();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_36 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_37 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_38 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_39 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_40 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_41 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_42 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_43 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_44 = new Telerik.WinControls.UI.RadLabel();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// ShapeContainer2
			// 
			this.ShapeContainer2.Location = new System.Drawing.Point(0, 0);
			this.ShapeContainer2.Size = new System.Drawing.Size(774, 433);
			this.ShapeContainer2.Shapes.Add(Line1);
			this.ShapeContainer2.Shapes.Add(Line2);
			this.ShapeContainer2.Shapes.Add(Line3);
			this.ShapeContainer2.Shapes.Add(Line4);
			this.ShapeContainer2.Shapes.Add(Line5);
			this.ShapeContainer2.Shapes.Add(Line6);
			this.ShapeContainer2.Shapes.Add(Line7);
			// 
			// vsDatos
			// 
			this.vsDatos.Location = new System.Drawing.Point(28, 333);
			this.vsDatos.Name = "vsDatos";
			this.vsDatos.Size = new System.Drawing.Size(659, 97);
			this.vsDatos.TabIndex = 0;
			// 
			// Picture1
			// 
			this.Picture1.BackColor = System.Drawing.SystemColors.Window;
			this.Picture1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Picture1.CausesValidation = true;
			this.Picture1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Picture1.Dock = System.Windows.Forms.DockStyle.None;
			this.Picture1.Enabled = true;
			this.Picture1.Location = new System.Drawing.Point(112, 13);
			this.Picture1.Name = "Picture1";
			this.Picture1.Size = new System.Drawing.Size(25, 17);
			this.Picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this.Picture1.TabIndex = 72;
			this.Picture1.TabStop = true;
			this.Picture1.Visible = true;
			// 
			// graficaenfer
			// 
			this.graficaenfer.BackColor = System.Drawing.Color.Silver;
			this.graficaenfer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.graficaenfer.CausesValidation = true;
			this.graficaenfer.Cursor = System.Windows.Forms.Cursors.Default;
			this.graficaenfer.Dock = System.Windows.Forms.DockStyle.None;
			this.graficaenfer.Enabled = true;
			this.graficaenfer.Location = new System.Drawing.Point(142, 31);
			this.graficaenfer.Name = "graficaenfer";
			this.graficaenfer.Size = new System.Drawing.Size(545, 285);
			this.graficaenfer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this.graficaenfer.TabIndex = 9;
			this.graficaenfer.TabStop = true;
			this.graficaenfer.Visible = true;
			// 
			// Command1
			// 
			//this.Command1.BackColor = System.Drawing.SystemColors.Control;
			this.Command1.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Command1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Command1.Location = new System.Drawing.Point(694, 282);
			this.Command1.Name = "Command1";
			this.Command1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Command1.Size = new System.Drawing.Size(33, 33);
			this.Command1.TabIndex = 8;
			this.Command1.Text = "<<";
			this.Command1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.Command1.UseVisualStyleBackColor = false;
			this.Command1.Click += new System.EventHandler(this.Command1_Click);
            this.Command1.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Command2
            // 
            //this.Command2.BackColor = System.Drawing.SystemColors.Control;
            this.Command2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Command2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Command2.Location = new System.Drawing.Point(728, 282);
			this.Command2.Name = "Command2";
			this.Command2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Command2.Size = new System.Drawing.Size(33, 33);
			this.Command2.TabIndex = 7;
			this.Command2.Text = ">>";
			this.Command2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.Command2.UseVisualStyleBackColor = false;
			this.Command2.Click += new System.EventHandler(this.Command2_Click);
            this.Command2.TextAlignment = System.Drawing.ContentAlignment.TopCenter;

            // 
            // Command3
            // 
            //this.Command3.BackColor = System.Drawing.SystemColors.Control;
            this.Command3.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Command3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Command3.Location = new System.Drawing.Point(692, 68);
			this.Command3.Name = "Command3";
			this.Command3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Command3.Size = new System.Drawing.Size(81, 33);
			this.Command3.TabIndex = 6;
			this.Command3.Text = "Imprimir gr�fica completa";
			this.Command3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.Command3.UseVisualStyleBackColor = false;
			this.Command3.Click += new System.EventHandler(this.Command3_Click);
            this.Command3.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Command4
            // 
            //this.Command4.BackColor = System.Drawing.SystemColors.Control;
            this.Command4.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Command4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Command4.Location = new System.Drawing.Point(692, 31);
			this.Command4.Name = "Command4";
			this.Command4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Command4.Size = new System.Drawing.Size(81, 33);
			this.Command4.TabIndex = 5;
			this.Command4.Text = "Imprimir gr�fica activa";
			this.Command4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.Command4.UseVisualStyleBackColor = false;
			this.Command4.Click += new System.EventHandler(this.Command4_Click);
            this.Command3.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame1
            // 
            //this.Frame1.BackColor = System.Drawing.SystemColors.Control;
            this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Enabled = true;
			this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(138, -8);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(393, 36);
			this.Frame1.TabIndex = 2;
			this.Frame1.Visible = true;
			// 
			// Label2
			// 
			//this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(8, 13);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(305, 17);
			this.Label2.TabIndex = 4;
			// 
			// Label3
			// 
			//this.Label3.BackColor = System.Drawing.SystemColors.Control;
			//this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label3.Location = new System.Drawing.Point(314, 13);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(73, 17);
			this.Label3.TabIndex = 3;
			// 
			// Command5
			// 
			//this.Command5.BackColor = System.Drawing.SystemColors.Control;
			this.Command5.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Command5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Command5.Location = new System.Drawing.Point(692, 105);
			this.Command5.Name = "Command5";
			this.Command5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Command5.Size = new System.Drawing.Size(81, 33);
			this.Command5.TabIndex = 1;
			this.Command5.Text = "Cancelar";
			this.Command5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.Command5.UseVisualStyleBackColor = false;
			this.Command5.Click += new System.EventHandler(this.Command5_Click);
            this.Command5.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _Label1_47
            // 
            //this._Label1_47.BackColor = System.Drawing.SystemColors.Control;
            //this._Label1_47.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._Label1_47.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_47.ForeColor = System.Drawing.Color.FromArgb(255, 128, 0);
			this._Label1_47.Location = new System.Drawing.Point(116, 54);
			this._Label1_47.Name = "_Label1_47";
			this._Label1_47.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_47.Size = new System.Drawing.Size(21, 12);
			this._Label1_47.TabIndex = 71;
			this._Label1_47.TextAlignment = System.Drawing.ContentAlignment.TopRight;
			// 
			// _Label1_48
			// 
			//this._Label1_48.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_48.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_48.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_48.ForeColor = System.Drawing.Color.FromArgb(255, 128, 0);
			this._Label1_48.Location = new System.Drawing.Point(116, 93);
			this._Label1_48.Name = "_Label1_48";
			this._Label1_48.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_48.Size = new System.Drawing.Size(21, 15);
			this._Label1_48.TabIndex = 70;
			this._Label1_48.Text = "10";
			this._Label1_48.TextAlignment = System.Drawing.ContentAlignment.TopRight;
			// 
			// _Label1_49
			// 
			//this._Label1_49.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_49.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_49.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_49.ForeColor = System.Drawing.Color.FromArgb(255, 128, 0);
			this._Label1_49.Location = new System.Drawing.Point(116, 133);
			this._Label1_49.Name = "_Label1_49";
			this._Label1_49.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_49.Size = new System.Drawing.Size(21, 15);
			this._Label1_49.TabIndex = 69;
			this._Label1_49.Text = "8";
			this._Label1_49.TextAlignment = System.Drawing.ContentAlignment.TopRight;
			// 
			// _Label1_50
			// 
			//this._Label1_50.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_50.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_50.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_50.ForeColor = System.Drawing.Color.FromArgb(255, 128, 0);
			this._Label1_50.Location = new System.Drawing.Point(116, 173);
			this._Label1_50.Name = "_Label1_50";
			this._Label1_50.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_50.Size = new System.Drawing.Size(21, 15);
			this._Label1_50.TabIndex = 68;
			this._Label1_50.Text = "6";
			this._Label1_50.TextAlignment = System.Drawing.ContentAlignment.TopRight;
			// 
			// _Label1_51
			// 
			//this._Label1_51.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_51.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_51.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_51.ForeColor = System.Drawing.Color.FromArgb(255, 128, 0);
			this._Label1_51.Location = new System.Drawing.Point(116, 213);
			this._Label1_51.Name = "_Label1_51";
			this._Label1_51.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_51.Size = new System.Drawing.Size(21, 15);
			this._Label1_51.TabIndex = 67;
			this._Label1_51.Text = "4";
			this._Label1_51.TextAlignment = System.Drawing.ContentAlignment.TopRight;
			// 
			// _Label1_52
			// 
			//this._Label1_52.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_52.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_52.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_52.ForeColor = System.Drawing.Color.FromArgb(255, 128, 0);
			this._Label1_52.Location = new System.Drawing.Point(116, 253);
			this._Label1_52.Name = "_Label1_52";
			this._Label1_52.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_52.Size = new System.Drawing.Size(21, 15);
			this._Label1_52.TabIndex = 66;
			this._Label1_52.Text = "2";
			this._Label1_52.TextAlignment = System.Drawing.ContentAlignment.TopRight;
			// 
			// _Label1_53
			// 
			//this._Label1_53.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_53.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_53.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_53.ForeColor = System.Drawing.Color.FromArgb(255, 128, 0);
			this._Label1_53.Location = new System.Drawing.Point(116, 293);
			this._Label1_53.Name = "_Label1_53";
			this._Label1_53.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_53.Size = new System.Drawing.Size(21, 15);
			this._Label1_53.TabIndex = 65;
			this._Label1_53.Text = "0";
			this._Label1_53.TextAlignment = System.Drawing.ContentAlignment.TopRight;
			// 
			// _Label1_45
			// 
			this._Label1_45.BackColor = System.Drawing.Color.FromArgb(255, 192, 128);
			//this._Label1_45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_45.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_45.ForeColor = System.Drawing.Color.White;
			this._Label1_45.Location = new System.Drawing.Point(112, -3);
			this._Label1_45.Name = "_Label1_45";
			this._Label1_45.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_45.Size = new System.Drawing.Size(25, 17);
			this._Label1_45.TabIndex = 64;
			this._Label1_45.Text = "D";
			this._Label1_45.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_0
			// 
			this._Label1_0.BackColor = System.Drawing.Color.Red;
			//this._Label1_0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_0.ForeColor = System.Drawing.Color.White;
			this._Label1_0.Location = new System.Drawing.Point(59, -3);
			this._Label1_0.Name = "_Label1_0";
			this._Label1_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_0.Size = new System.Drawing.Size(25, 17);
			this._Label1_0.TabIndex = 63;
			this._Label1_0.Text = " T ";
			this._Label1_0.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_1
			// 
			this._Label1_1.BackColor = System.Drawing.Color.Navy;
			//this._Label1_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_1.ForeColor = System.Drawing.Color.White;
			this._Label1_1.Location = new System.Drawing.Point(39, -3);
			this._Label1_1.Name = "_Label1_1";
			this._Label1_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_1.Size = new System.Drawing.Size(25, 17);
			this._Label1_1.TabIndex = 62;
			this._Label1_1.Text = " P ";
			this._Label1_1.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_2
			// 
			this._Label1_2.BackColor = System.Drawing.Color.Green;
			//this._Label1_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_2.ForeColor = System.Drawing.Color.White;
			this._Label1_2.Location = new System.Drawing.Point(14, -3);
			this._Label1_2.Name = "_Label1_2";
			this._Label1_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_2.Size = new System.Drawing.Size(25, 17);
			this._Label1_2.TabIndex = 61;
			this._Label1_2.Text = "T.A.";
			// 
			// _Label1_3
			// 
			this._Label1_3.BackColor = System.Drawing.Color.Gray;
			//this._Label1_3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_3.ForeColor = System.Drawing.Color.White;
			this._Label1_3.Location = new System.Drawing.Point(-2, -3);
			this._Label1_3.Name = "_Label1_3";
			this._Label1_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_3.Size = new System.Drawing.Size(17, 17);
			this._Label1_3.TabIndex = 60;
			this._Label1_3.Text = "R";
			this._Label1_3.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Line1
			// 
			this.Line1.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line1.BorderWidth = 1;
			this.Line1.Enabled = false;
			this.Line1.Name = "Line1";
			this.Line1.Visible = true;
			this.Line1.X1 = (int) 0;
			this.Line1.X2 = (int) 138;
			this.Line1.Y1 = (int) 308;
			this.Line1.Y2 = (int) 308;
			// 
			// Line2
			// 
			this.Line2.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line2.BorderWidth = 1;
			this.Line2.Enabled = false;
			this.Line2.Name = "Line2";
			this.Line2.Visible = true;
			this.Line2.X1 = (int) 0;
			this.Line2.X2 = (int) 138;
			this.Line2.Y1 = (int) 268;
			this.Line2.Y2 = (int) 268;
			// 
			// Line3
			// 
			this.Line3.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line3.BorderWidth = 1;
			this.Line3.Enabled = false;
			this.Line3.Name = "Line3";
			this.Line3.Visible = true;
			this.Line3.X1 = (int) 0;
			this.Line3.X2 = (int) 138;
			this.Line3.Y1 = (int) 228;
			this.Line3.Y2 = (int) 228;
			// 
			// Line4
			// 
			this.Line4.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line4.BorderWidth = 1;
			this.Line4.Enabled = false;
			this.Line4.Name = "Line4";
			this.Line4.Visible = true;
			this.Line4.X1 = (int) 0;
			this.Line4.X2 = (int) 138;
			this.Line4.Y1 = (int) 188;
			this.Line4.Y2 = (int) 188;
			// 
			// Line5
			// 
			this.Line5.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line5.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line5.BorderWidth = 1;
			this.Line5.Enabled = false;
			this.Line5.Name = "Line5";
			this.Line5.Visible = true;
			this.Line5.X1 = (int) 0;
			this.Line5.X2 = (int) 138;
			this.Line5.Y1 = (int) 148;
			this.Line5.Y2 = (int) 148;
			// 
			// Line6
			// 
			this.Line6.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line6.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line6.BorderWidth = 1;
			this.Line6.Enabled = false;
			this.Line6.Name = "Line6";
			this.Line6.Visible = true;
			this.Line6.X1 = (int) 0;
			this.Line6.X2 = (int) 136;
			this.Line6.Y1 = (int) 108;
			this.Line6.Y2 = (int) 108;
			// 
			// Line7
			// 
			this.Line7.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line7.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line7.BorderWidth = 1;
			this.Line7.Enabled = false;
			this.Line7.Name = "Line7";
			this.Line7.Visible = true;
			this.Line7.X1 = (int) 0;
			this.Line7.X2 = (int) 138;
			this.Line7.Y1 = (int) 68;
			this.Line7.Y2 = (int) 68;
			// 
			// _Label1_4
			// 
			//this._Label1_4.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_4.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_4.ForeColor = System.Drawing.Color.Gray;
			this._Label1_4.Location = new System.Drawing.Point(-8, 93);
			this._Label1_4.Name = "_Label1_4";
			this._Label1_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_4.Size = new System.Drawing.Size(25, 15);
			this._Label1_4.TabIndex = 59;
			this._Label1_4.Text = "  60";
			this._Label1_4.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_5
			// 
			//this._Label1_5.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_5.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_5.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_5.ForeColor = System.Drawing.Color.Green;
			this._Label1_5.Location = new System.Drawing.Point(16, 93);
			this._Label1_5.Name = "_Label1_5";
			this._Label1_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_5.Size = new System.Drawing.Size(25, 15);
			this._Label1_5.TabIndex = 58;
			this._Label1_5.Text = "200";
			this._Label1_5.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_6
			// 
			//this._Label1_6.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_6.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_6.ForeColor = System.Drawing.Color.Navy;
			this._Label1_6.Location = new System.Drawing.Point(40, 93);
			this._Label1_6.Name = "_Label1_6";
			this._Label1_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_6.Size = new System.Drawing.Size(25, 15);
			this._Label1_6.TabIndex = 57;
			this._Label1_6.Text = "140";
			this._Label1_6.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_7
			// 
			//this._Label1_7.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_7.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_7.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_7.ForeColor = System.Drawing.Color.Red;
			this._Label1_7.Location = new System.Drawing.Point(64, 93);
			this._Label1_7.Name = "_Label1_7";
			this._Label1_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_7.Size = new System.Drawing.Size(25, 15);
			this._Label1_7.TabIndex = 56;
			this._Label1_7.Text = "40";
			this._Label1_7.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_8
			// 
			//this._Label1_8.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_8.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_8.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_8.ForeColor = System.Drawing.Color.Gray;
			this._Label1_8.Location = new System.Drawing.Point(-8, 133);
			this._Label1_8.Name = "_Label1_8";
			this._Label1_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_8.Size = new System.Drawing.Size(25, 15);
			this._Label1_8.TabIndex = 55;
			this._Label1_8.Text = "  50";
			this._Label1_8.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_9
			// 
			//this._Label1_9.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_9.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_9.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_9.ForeColor = System.Drawing.Color.Green;
			this._Label1_9.Location = new System.Drawing.Point(16, 133);
			this._Label1_9.Name = "_Label1_9";
			this._Label1_9.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_9.Size = new System.Drawing.Size(25, 15);
			this._Label1_9.TabIndex = 54;
			this._Label1_9.Text = "150";
			this._Label1_9.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_10
			// 
			//this._Label1_10.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_10.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_10.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_10.ForeColor = System.Drawing.Color.Navy;
			this._Label1_10.Location = new System.Drawing.Point(40, 133);
			this._Label1_10.Name = "_Label1_10";
			this._Label1_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_10.Size = new System.Drawing.Size(25, 15);
			this._Label1_10.TabIndex = 53;
			this._Label1_10.Text = "120";
			this._Label1_10.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_11
			// 
			//this._Label1_11.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_11.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_11.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_11.ForeColor = System.Drawing.Color.Red;
			this._Label1_11.Location = new System.Drawing.Point(64, 133);
			this._Label1_11.Name = "_Label1_11";
			this._Label1_11.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_11.Size = new System.Drawing.Size(25, 15);
			this._Label1_11.TabIndex = 52;
			this._Label1_11.Text = "39";
			this._Label1_11.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_12
			// 
			//this._Label1_12.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_12.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_12.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_12.ForeColor = System.Drawing.Color.Gray;
			this._Label1_12.Location = new System.Drawing.Point(-8, 173);
			this._Label1_12.Name = "_Label1_12";
			this._Label1_12.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_12.Size = new System.Drawing.Size(25, 15);
			this._Label1_12.TabIndex = 51;
			this._Label1_12.Text = "  40";
			this._Label1_12.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_13
			// 
			//this._Label1_13.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_13.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_13.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_13.ForeColor = System.Drawing.Color.Green;
			this._Label1_13.Location = new System.Drawing.Point(16, 173);
			this._Label1_13.Name = "_Label1_13";
			this._Label1_13.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_13.Size = new System.Drawing.Size(25, 15);
			this._Label1_13.TabIndex = 50;
			this._Label1_13.Text = "100";
			this._Label1_13.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_14
			// 
			//this._Label1_14.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_14.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_14.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_14.ForeColor = System.Drawing.Color.Navy;
			this._Label1_14.Location = new System.Drawing.Point(40, 173);
			this._Label1_14.Name = "_Label1_14";
			this._Label1_14.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_14.Size = new System.Drawing.Size(25, 15);
			this._Label1_14.TabIndex = 49;
			this._Label1_14.Text = "100";
			this._Label1_14.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_15
			// 
			//this._Label1_15.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_15.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_15.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_15.ForeColor = System.Drawing.Color.Red;
			this._Label1_15.Location = new System.Drawing.Point(64, 173);
			this._Label1_15.Name = "_Label1_15";
			this._Label1_15.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_15.Size = new System.Drawing.Size(25, 15);
			this._Label1_15.TabIndex = 48;
			this._Label1_15.Text = "38";
			this._Label1_15.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_16
			// 
			//this._Label1_16.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_16.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_16.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_16.ForeColor = System.Drawing.Color.Gray;
			this._Label1_16.Location = new System.Drawing.Point(-8, 213);
			this._Label1_16.Name = "_Label1_16";
			this._Label1_16.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_16.Size = new System.Drawing.Size(25, 15);
			this._Label1_16.TabIndex = 47;
			this._Label1_16.Text = "  30";
			this._Label1_16.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_17
			// 
			//this._Label1_17.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_17.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_17.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_17.ForeColor = System.Drawing.Color.Green;
			this._Label1_17.Location = new System.Drawing.Point(16, 213);
			this._Label1_17.Name = "_Label1_17";
			this._Label1_17.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_17.Size = new System.Drawing.Size(25, 15);
			this._Label1_17.TabIndex = 46;
			this._Label1_17.Text = "50";
			this._Label1_17.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_18
			// 
			//this._Label1_18.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_18.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_18.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_18.ForeColor = System.Drawing.Color.Navy;
			this._Label1_18.Location = new System.Drawing.Point(40, 213);
			this._Label1_18.Name = "_Label1_18";
			this._Label1_18.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_18.Size = new System.Drawing.Size(25, 15);
			this._Label1_18.TabIndex = 45;
			this._Label1_18.Text = "80";
			this._Label1_18.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_19
			// 
			//this._Label1_19.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_19.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_19.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_19.ForeColor = System.Drawing.Color.Red;
			this._Label1_19.Location = new System.Drawing.Point(64, 213);
			this._Label1_19.Name = "_Label1_19";
			this._Label1_19.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_19.Size = new System.Drawing.Size(25, 15);
			this._Label1_19.TabIndex = 44;
			this._Label1_19.Text = "37";
			this._Label1_19.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_20
			// 
			//this._Label1_20.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_20.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_20.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_20.ForeColor = System.Drawing.Color.Gray;
			this._Label1_20.Location = new System.Drawing.Point(-8, 253);
			this._Label1_20.Name = "_Label1_20";
			this._Label1_20.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_20.Size = new System.Drawing.Size(25, 15);
			this._Label1_20.TabIndex = 43;
			this._Label1_20.Text = "  20";
			this._Label1_20.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_21
			// 
			//this._Label1_21.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_21.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_21.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_21.ForeColor = System.Drawing.Color.Green;
			this._Label1_21.Location = new System.Drawing.Point(16, 253);
			this._Label1_21.Name = "_Label1_21";
			this._Label1_21.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_21.Size = new System.Drawing.Size(25, 15);
			this._Label1_21.TabIndex = 42;
			this._Label1_21.Text = "0";
			this._Label1_21.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_22
			// 
			//this._Label1_22.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_22.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_22.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_22.ForeColor = System.Drawing.Color.Navy;
			this._Label1_22.Location = new System.Drawing.Point(40, 253);
			this._Label1_22.Name = "_Label1_22";
			this._Label1_22.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_22.Size = new System.Drawing.Size(25, 15);
			this._Label1_22.TabIndex = 41;
			this._Label1_22.Text = "60";
			this._Label1_22.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_23
			// 
			//this._Label1_23.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_23.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_23.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_23.ForeColor = System.Drawing.Color.Red;
			this._Label1_23.Location = new System.Drawing.Point(64, 253);
			this._Label1_23.Name = "_Label1_23";
			this._Label1_23.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_23.Size = new System.Drawing.Size(25, 15);
			this._Label1_23.TabIndex = 40;
			this._Label1_23.Text = "36";
			this._Label1_23.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_24
			// 
			//this._Label1_24.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_24.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_24.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_24.ForeColor = System.Drawing.Color.Gray;
			this._Label1_24.Location = new System.Drawing.Point(-8, 293);
			this._Label1_24.Name = "_Label1_24";
			this._Label1_24.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_24.Size = new System.Drawing.Size(25, 15);
			this._Label1_24.TabIndex = 39;
			this._Label1_24.Text = "  10";
			this._Label1_24.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_25
			// 
			//this._Label1_25.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_25.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_25.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_25.ForeColor = System.Drawing.Color.Green;
			this._Label1_25.Location = new System.Drawing.Point(16, 293);
			this._Label1_25.Name = "_Label1_25";
			this._Label1_25.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_25.Size = new System.Drawing.Size(25, 15);
			this._Label1_25.TabIndex = 38;
			this._Label1_25.Text = "-50";
			this._Label1_25.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			this._Label1_25.Visible = false;
			// 
			// _Label1_26
			// 
			//this._Label1_26.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_26.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_26.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_26.ForeColor = System.Drawing.Color.Navy;
			this._Label1_26.Location = new System.Drawing.Point(40, 293);
			this._Label1_26.Name = "_Label1_26";
			this._Label1_26.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_26.Size = new System.Drawing.Size(25, 15);
			this._Label1_26.TabIndex = 37;
			this._Label1_26.Text = "40";
			this._Label1_26.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_27
			// 
			//this._Label1_27.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_27.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_27.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_27.ForeColor = System.Drawing.Color.Red;
			this._Label1_27.Location = new System.Drawing.Point(64, 293);
			this._Label1_27.Name = "_Label1_27";
			this._Label1_27.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_27.Size = new System.Drawing.Size(25, 15);
			this._Label1_27.TabIndex = 36;
			this._Label1_27.Text = " 35";
			this._Label1_27.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label4_36
			// 
			//this._Label4_36.BackColor = System.Drawing.SystemColors.Control;
			//this._Label4_36.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label4_36.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label4_36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._Label4_36.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label4_36.Location = new System.Drawing.Point(606, 320);
			this._Label4_36.Name = "_Label4_36";
			this._Label4_36.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label4_36.Size = new System.Drawing.Size(73, 13);
			this._Label4_36.TabIndex = 35;
			this._Label4_36.Text = "Label4";
			this._Label4_36.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label4_35
			// 
			//this._Label4_35.BackColor = System.Drawing.SystemColors.Control;
			//this._Label4_35.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label4_35.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label4_35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._Label4_35.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label4_35.Location = new System.Drawing.Point(532, 320);
			this._Label4_35.Name = "_Label4_35";
			this._Label4_35.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label4_35.Size = new System.Drawing.Size(73, 13);
			this._Label4_35.TabIndex = 34;
			this._Label4_35.Text = "Label4";
			this._Label4_35.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label4_34
			// 
			//this._Label4_34.BackColor = System.Drawing.SystemColors.Control;
			//this._Label4_34.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label4_34.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label4_34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._Label4_34.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label4_34.Location = new System.Drawing.Point(460, 320);
			this._Label4_34.Name = "_Label4_34";
			this._Label4_34.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label4_34.Size = new System.Drawing.Size(71, 13);
			this._Label4_34.TabIndex = 33;
			this._Label4_34.Text = "Label4";
			this._Label4_34.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label4_33
			// 
			//this._Label4_33.BackColor = System.Drawing.SystemColors.Control;
			//this._Label4_33.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label4_33.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label4_33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._Label4_33.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label4_33.Location = new System.Drawing.Point(386, 320);
			this._Label4_33.Name = "_Label4_33";
			this._Label4_33.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label4_33.Size = new System.Drawing.Size(73, 13);
			this._Label4_33.TabIndex = 32;
			this._Label4_33.Text = "Label4";
			this._Label4_33.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label4_32
			// 
			//this._Label4_32.BackColor = System.Drawing.SystemColors.Control;
			//this._Label4_32.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label4_32.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label4_32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._Label4_32.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label4_32.Location = new System.Drawing.Point(314, 320);
			this._Label4_32.Name = "_Label4_32";
			this._Label4_32.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label4_32.Size = new System.Drawing.Size(71, 13);
			this._Label4_32.TabIndex = 31;
			this._Label4_32.Text = "Label4";
			this._Label4_32.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label4_31
			// 
			//this._Label4_31.BackColor = System.Drawing.SystemColors.Control;
			//this._Label4_31.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label4_31.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label4_31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._Label4_31.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label4_31.Location = new System.Drawing.Point(240, 320);
			this._Label4_31.Name = "_Label4_31";
			this._Label4_31.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label4_31.Size = new System.Drawing.Size(73, 13);
			this._Label4_31.TabIndex = 30;
			this._Label4_31.Text = "Label4";
			this._Label4_31.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label4_30
			// 
			//this._Label4_30.BackColor = System.Drawing.SystemColors.Control;
			//this._Label4_30.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label4_30.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label4_30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._Label4_30.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label4_30.Location = new System.Drawing.Point(168, 320);
			this._Label4_30.Name = "_Label4_30";
			this._Label4_30.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label4_30.Size = new System.Drawing.Size(71, 13);
			this._Label4_30.TabIndex = 29;
			this._Label4_30.Text = "Label4";
			this._Label4_30.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_28
			// 
			this._Label1_28.BackColor = System.Drawing.Color.White;
			//this._Label1_28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_28.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_28.ForeColor = System.Drawing.Color.Black;
			this._Label1_28.Location = new System.Drawing.Point(62, 13);
			this._Label1_28.Name = "_Label1_28";
			this._Label1_28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_28.Size = new System.Drawing.Size(25, 17);
			this._Label1_28.TabIndex = 28;
			this._Label1_28.Text = "//";
			this._Label1_28.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_29
			// 
			this._Label1_29.BackColor = System.Drawing.Color.White;
			//this._Label1_29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_29.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_29.ForeColor = System.Drawing.Color.Black;
			this._Label1_29.Location = new System.Drawing.Point(38, 13);
			this._Label1_29.Name = "_Label1_29";
			this._Label1_29.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_29.Size = new System.Drawing.Size(25, 17);
			this._Label1_29.TabIndex = 27;
			this._Label1_29.Text = "|";
			this._Label1_29.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_30
			// 
			this._Label1_30.BackColor = System.Drawing.Color.White;
			//this._Label1_30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_30.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_30.ForeColor = System.Drawing.Color.Black;
			this._Label1_30.Location = new System.Drawing.Point(14, 13);
			this._Label1_30.Name = "_Label1_30";
			this._Label1_30.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_30.Size = new System.Drawing.Size(25, 17);
			this._Label1_30.TabIndex = 26;
			this._Label1_30.Text = "  >";
			// 
			// _Label1_31
			// 
			this._Label1_31.BackColor = System.Drawing.Color.White;
			//this._Label1_31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_31.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_31.ForeColor = System.Drawing.Color.Black;
			this._Label1_31.Location = new System.Drawing.Point(-2, 13);
			this._Label1_31.Name = "_Label1_31";
			this._Label1_31.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_31.Size = new System.Drawing.Size(17, 17);
			this._Label1_31.TabIndex = 25;
			this._Label1_31.Text = "o";
			this._Label1_31.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Image1
			// 
			this.Image1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Image1.Enabled = true;
			this.Image1.Location = new System.Drawing.Point(700, 233);
			this.Image1.Name = "Image1";
			this.Image1.Size = new System.Drawing.Size(49, 33);
			this.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this.Image1.Visible = false;
			// 
			// _Label1_32
			// 
			//this._Label1_32.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_32.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_32.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_32.ForeColor = System.Drawing.Color.Red;
			this._Label1_32.Location = new System.Drawing.Point(64, 54);
			this._Label1_32.Name = "_Label1_32";
			this._Label1_32.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_32.Size = new System.Drawing.Size(25, 12);
			this._Label1_32.TabIndex = 24;
			this._Label1_32.Text = "41";
			this._Label1_32.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_33
			// 
			//this._Label1_33.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_33.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_33.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_33.ForeColor = System.Drawing.Color.Navy;
			this._Label1_33.Location = new System.Drawing.Point(40, 54);
			this._Label1_33.Name = "_Label1_33";
			this._Label1_33.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_33.Size = new System.Drawing.Size(25, 12);
			this._Label1_33.TabIndex = 23;
			this._Label1_33.Text = "160";
			this._Label1_33.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_34
			// 
			//this._Label1_34.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_34.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_34.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_34.ForeColor = System.Drawing.Color.Green;
			this._Label1_34.Location = new System.Drawing.Point(16, 54);
			this._Label1_34.Name = "_Label1_34";
			this._Label1_34.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_34.Size = new System.Drawing.Size(25, 12);
			this._Label1_34.TabIndex = 22;
			this._Label1_34.Text = "250";
			this._Label1_34.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_35
			// 
			//this._Label1_35.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_35.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_35.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_35.ForeColor = System.Drawing.Color.Gray;
			this._Label1_35.Location = new System.Drawing.Point(-6, 54);
			this._Label1_35.Name = "_Label1_35";
			this._Label1_35.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_35.Size = new System.Drawing.Size(25, 12);
			this._Label1_35.TabIndex = 21;
			this._Label1_35.Text = " 70";
			this._Label1_35.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// LbFechaGrafica
			// 
			//this.LbFechaGrafica.BackColor = System.Drawing.SystemColors.Control;
			//this.LbFechaGrafica.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.LbFechaGrafica.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbFechaGrafica.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LbFechaGrafica.Location = new System.Drawing.Point(578, 5);
			this.LbFechaGrafica.Name = "LbFechaGrafica";
			this.LbFechaGrafica.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbFechaGrafica.Size = new System.Drawing.Size(109, 17);
			this.LbFechaGrafica.TabIndex = 20;
			// 
			// Label6
			// 
			//this.Label6.BackColor = System.Drawing.SystemColors.Control;
			//this.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label6.Location = new System.Drawing.Point(536, 7);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(43, 15);
			this.Label6.TabIndex = 19;
			this.Label6.Text = "Fecha:";
			// 
			// _Label1_36
			// 
			this._Label1_36.BackColor = System.Drawing.Color.FromArgb(128, 64, 64);
			//this._Label1_36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_36.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_36.ForeColor = System.Drawing.Color.White;
			this._Label1_36.Location = new System.Drawing.Point(88, -3);
			this._Label1_36.Name = "_Label1_36";
			this._Label1_36.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_36.Size = new System.Drawing.Size(25, 17);
			this._Label1_36.TabIndex = 18;
			this._Label1_36.Text = "B";
			this._Label1_36.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_37
			// 
			this._Label1_37.BackColor = System.Drawing.Color.White;
			//this._Label1_37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._Label1_37.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_37.ForeColor = System.Drawing.Color.Black;
			this._Label1_37.Location = new System.Drawing.Point(88, 13);
			this._Label1_37.Name = "_Label1_37";
			this._Label1_37.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_37.Size = new System.Drawing.Size(25, 17);
			this._Label1_37.TabIndex = 17;
			this._Label1_37.Text = "o";
			this._Label1_37.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_38
			// 
			//this._Label1_38.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_38.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_38.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_38.ForeColor = System.Drawing.Color.FromArgb(128, 64, 64);
			this._Label1_38.Location = new System.Drawing.Point(90, 54);
			this._Label1_38.Name = "_Label1_38";
			this._Label1_38.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_38.Size = new System.Drawing.Size(27, 12);
			this._Label1_38.TabIndex = 16;
			this._Label1_38.Text = "3000";
			this._Label1_38.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_39
			// 
			//this._Label1_39.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_39.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_39.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_39.ForeColor = System.Drawing.Color.FromArgb(128, 64, 64);
			this._Label1_39.Location = new System.Drawing.Point(90, 93);
			this._Label1_39.Name = "_Label1_39";
			this._Label1_39.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_39.Size = new System.Drawing.Size(27, 15);
			this._Label1_39.TabIndex = 15;
			this._Label1_39.Text = "2000";
			this._Label1_39.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_40
			// 
			//this._Label1_40.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_40.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_40.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_40.ForeColor = System.Drawing.Color.FromArgb(128, 64, 64);
			this._Label1_40.Location = new System.Drawing.Point(90, 133);
			this._Label1_40.Name = "_Label1_40";
			this._Label1_40.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_40.Size = new System.Drawing.Size(27, 15);
			this._Label1_40.TabIndex = 14;
			this._Label1_40.Text = "1000";
			this._Label1_40.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_41
			// 
			//this._Label1_41.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_41.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_41.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_41.ForeColor = System.Drawing.Color.FromArgb(128, 64, 64);
			this._Label1_41.Location = new System.Drawing.Point(90, 173);
			this._Label1_41.Name = "_Label1_41";
			this._Label1_41.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_41.Size = new System.Drawing.Size(27, 15);
			this._Label1_41.TabIndex = 13;
			this._Label1_41.Text = "0";
			this._Label1_41.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_42
			// 
			//this._Label1_42.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_42.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_42.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_42.ForeColor = System.Drawing.Color.FromArgb(128, 64, 64);
			this._Label1_42.Location = new System.Drawing.Point(88, 213);
			this._Label1_42.Name = "_Label1_42";
			this._Label1_42.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_42.Size = new System.Drawing.Size(29, 15);
			this._Label1_42.TabIndex = 12;
			this._Label1_42.Text = "-1000";
			this._Label1_42.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_43
			// 
			//this._Label1_43.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_43.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_43.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_43.ForeColor = System.Drawing.Color.FromArgb(128, 64, 64);
			this._Label1_43.Location = new System.Drawing.Point(88, 253);
			this._Label1_43.Name = "_Label1_43";
			this._Label1_43.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_43.Size = new System.Drawing.Size(29, 15);
			this._Label1_43.TabIndex = 11;
			this._Label1_43.Text = "-2000";
			this._Label1_43.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// _Label1_44
			// 
			//this._Label1_44.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_44.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_44.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_44.ForeColor = System.Drawing.Color.FromArgb(128, 64, 64);
			this._Label1_44.Location = new System.Drawing.Point(88, 293);
			this._Label1_44.Name = "_Label1_44";
			this._Label1_44.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_44.Size = new System.Drawing.Size(29, 15);
			this._Label1_44.TabIndex = 10;
			this._Label1_44.Text = "-3000";
			this._Label1_44.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// GHORARIA_SH
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(774, 433);
			this.Controls.Add(this.vsDatos);
			this.Controls.Add(this.Picture1);
			this.Controls.Add(this.graficaenfer);
			this.Controls.Add(this.Command1);
			this.Controls.Add(this.Command2);
			this.Controls.Add(this.Command3);
			this.Controls.Add(this.Command4);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.Command5);
			this.Controls.Add(this._Label1_47);
			this.Controls.Add(this._Label1_48);
			this.Controls.Add(this._Label1_49);
			this.Controls.Add(this._Label1_50);
			this.Controls.Add(this._Label1_51);
			this.Controls.Add(this._Label1_52);
			this.Controls.Add(this._Label1_53);
			this.Controls.Add(this._Label1_45);
			this.Controls.Add(this._Label1_0);
			this.Controls.Add(this._Label1_1);
			this.Controls.Add(this._Label1_2);
			this.Controls.Add(this._Label1_3);
			this.Controls.Add(this._Label1_4);
			this.Controls.Add(this._Label1_5);
			this.Controls.Add(this._Label1_6);
			this.Controls.Add(this._Label1_7);
			this.Controls.Add(this._Label1_8);
			this.Controls.Add(this._Label1_9);
			this.Controls.Add(this._Label1_10);
			this.Controls.Add(this._Label1_11);
			this.Controls.Add(this._Label1_12);
			this.Controls.Add(this._Label1_13);
			this.Controls.Add(this._Label1_14);
			this.Controls.Add(this._Label1_15);
			this.Controls.Add(this._Label1_16);
			this.Controls.Add(this._Label1_17);
			this.Controls.Add(this._Label1_18);
			this.Controls.Add(this._Label1_19);
			this.Controls.Add(this._Label1_20);
			this.Controls.Add(this._Label1_21);
			this.Controls.Add(this._Label1_22);
			this.Controls.Add(this._Label1_23);
			this.Controls.Add(this._Label1_24);
			this.Controls.Add(this._Label1_25);
			this.Controls.Add(this._Label1_26);
			this.Controls.Add(this._Label1_27);
			this.Controls.Add(this._Label4_36);
			this.Controls.Add(this._Label4_35);
			this.Controls.Add(this._Label4_34);
			this.Controls.Add(this._Label4_33);
			this.Controls.Add(this._Label4_32);
			this.Controls.Add(this._Label4_31);
			this.Controls.Add(this._Label4_30);
			this.Controls.Add(this._Label1_28);
			this.Controls.Add(this._Label1_29);
			this.Controls.Add(this._Label1_30);
			this.Controls.Add(this._Label1_31);
			this.Controls.Add(this.Image1);
			this.Controls.Add(this._Label1_32);
			this.Controls.Add(this._Label1_33);
			this.Controls.Add(this._Label1_34);
			this.Controls.Add(this._Label1_35);
			this.Controls.Add(this.LbFechaGrafica);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this._Label1_36);
			this.Controls.Add(this._Label1_37);
			this.Controls.Add(this._Label1_38);
			this.Controls.Add(this._Label1_39);
			this.Controls.Add(this._Label1_40);
			this.Controls.Add(this._Label1_41);
			this.Controls.Add(this._Label1_42);
			this.Controls.Add(this._Label1_43);
			this.Controls.Add(this._Label1_44);
			this.Controls.Add(ShapeContainer2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location = new System.Drawing.Point(4, 23);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "GHORARIA_SH";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Gr�fica horaria de enfermer�a";
			this.ToolTipMain.SetToolTip(this.Command1, "Retroceso de  p�gina");
			this.ToolTipMain.SetToolTip(this.Command2, "Avance de p�gina");
			this.ToolTipMain.SetToolTip(this.Label2, "Nombre");
			this.ToolTipMain.SetToolTip(this.Label3, "Cama");
			this.ToolTipMain.SetToolTip(this.LbFechaGrafica, "Cama");
			this.Closed += new System.EventHandler(this.GHORARIA_SH_Closed);
			this.Load += new System.EventHandler(this.GHORARIA_SH_Load);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeLabel4();
			InitializeLabel1();
		}
		void InitializeLabel4()
		{
			this.Label4 = new Telerik.WinControls.UI.RadLabel[37];
			this.Label4[36] = _Label4_36;
			this.Label4[35] = _Label4_35;
			this.Label4[34] = _Label4_34;
			this.Label4[33] = _Label4_33;
			this.Label4[32] = _Label4_32;
			this.Label4[31] = _Label4_31;
			this.Label4[30] = _Label4_30;
		}
		void InitializeLabel1()
		{
			this.Label1 = new Telerik.WinControls.UI.RadLabel[54];
			this.Label1[47] = _Label1_47;
			this.Label1[48] = _Label1_48;
			this.Label1[49] = _Label1_49;
			this.Label1[50] = _Label1_50;
			this.Label1[51] = _Label1_51;
			this.Label1[52] = _Label1_52;
			this.Label1[53] = _Label1_53;
			this.Label1[45] = _Label1_45;
			this.Label1[0] = _Label1_0;
			this.Label1[1] = _Label1_1;
			this.Label1[2] = _Label1_2;
			this.Label1[3] = _Label1_3;
			this.Label1[4] = _Label1_4;
			this.Label1[5] = _Label1_5;
			this.Label1[6] = _Label1_6;
			this.Label1[7] = _Label1_7;
			this.Label1[8] = _Label1_8;
			this.Label1[9] = _Label1_9;
			this.Label1[10] = _Label1_10;
			this.Label1[11] = _Label1_11;
			this.Label1[12] = _Label1_12;
			this.Label1[13] = _Label1_13;
			this.Label1[14] = _Label1_14;
			this.Label1[15] = _Label1_15;
			this.Label1[16] = _Label1_16;
			this.Label1[17] = _Label1_17;
			this.Label1[18] = _Label1_18;
			this.Label1[19] = _Label1_19;
			this.Label1[20] = _Label1_20;
			this.Label1[21] = _Label1_21;
			this.Label1[22] = _Label1_22;
			this.Label1[23] = _Label1_23;
			this.Label1[24] = _Label1_24;
			this.Label1[25] = _Label1_25;
			this.Label1[26] = _Label1_26;
			this.Label1[27] = _Label1_27;
			this.Label1[28] = _Label1_28;
			this.Label1[29] = _Label1_29;
			this.Label1[30] = _Label1_30;
			this.Label1[31] = _Label1_31;
			this.Label1[32] = _Label1_32;
			this.Label1[33] = _Label1_33;
			this.Label1[34] = _Label1_34;
			this.Label1[35] = _Label1_35;
			this.Label1[36] = _Label1_36;
			this.Label1[37] = _Label1_37;
			this.Label1[38] = _Label1_38;
			this.Label1[39] = _Label1_39;
			this.Label1[40] = _Label1_40;
			this.Label1[41] = _Label1_41;
			this.Label1[42] = _Label1_42;
			this.Label1[43] = _Label1_43;
			this.Label1[44] = _Label1_44;
		}
		#endregion
	}
}