using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace GRAFICADLL
{
	partial class HOJA_SEGUIMIENTO
	{

		#region "Upgrade Support "
		private static HOJA_SEGUIMIENTO m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static HOJA_SEGUIMIENTO DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new HOJA_SEGUIMIENTO();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbImprimir", "_OptOrdenado_1", "_OptOrdenado_0", "Frame5", "cbHasta", "cbDesde", "Label6", "Label5", "_Label1_1", "FraCamas", "_rbTipoGrafica_1", "_rbTipoGrafica_0", "Frame4", "_optmedidas_3", "_optmedidas_2", "_optmedidas_1", "_optmedidas_0", "Frame3", "sdcfechahasta", "sdcfechadesde", "Label2", "_Label1_0", "Frame2", "cmdSalir", "cbAceptar", "Check6", "Check4", "Check3", "Check2", "Check1", "Check5", "Option1", "Option2", "Line1", "Line2", "Frame1", "Label4", "Label3", "Label1", "OptOrdenado", "optmedidas", "rbTipoGrafica", "ShapeContainer1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		private Telerik.WinControls.UI.RadRadioButton _OptOrdenado_1;
		private Telerik.WinControls.UI.RadRadioButton _OptOrdenado_0;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		public UpgradeHelpers.MSForms.MSCombobox cbHasta;
		public UpgradeHelpers.MSForms.MSCombobox cbDesde;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel Label5;
		private Telerik.WinControls.UI.RadLabel _Label1_1;
		public Telerik.WinControls.UI.RadGroupBox FraCamas;
		private Telerik.WinControls.UI.RadRadioButton _rbTipoGrafica_1;
		private Telerik.WinControls.UI.RadRadioButton _rbTipoGrafica_0;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		private Telerik.WinControls.UI.RadRadioButton _optmedidas_3;
		private Telerik.WinControls.UI.RadRadioButton _optmedidas_2;
		private Telerik.WinControls.UI.RadRadioButton _optmedidas_1;
		private Telerik.WinControls.UI.RadRadioButton _optmedidas_0;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadDateTimePicker sdcfechahasta;
		public Telerik.WinControls.UI.RadDateTimePicker sdcfechadesde;
		public Telerik.WinControls.UI.RadLabel Label2;
		private Telerik.WinControls.UI.RadLabel _Label1_0;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton cmdSalir;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadCheckBox Check6;
		public Telerik.WinControls.UI.RadCheckBox Check4;
		public Telerik.WinControls.UI.RadCheckBox Check3;
		public Telerik.WinControls.UI.RadCheckBox Check2;
		public Telerik.WinControls.UI.RadCheckBox Check1;
		public Telerik.WinControls.UI.RadCheckBox Check5;
		public Telerik.WinControls.UI.RadRadioButton Option1;
		public Telerik.WinControls.UI.RadRadioButton Option2;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line2;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel[] Label1 = new Telerik.WinControls.UI.RadLabel[2];
		public Telerik.WinControls.UI.RadRadioButton[] OptOrdenado = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] optmedidas = new Telerik.WinControls.UI.RadRadioButton[4];
		public Telerik.WinControls.UI.RadRadioButton[] rbTipoGrafica = new Telerik.WinControls.UI.RadRadioButton[2];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HOJA_SEGUIMIENTO));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.cbImprimir = new Telerik.WinControls.UI.RadButton();
			this.FraCamas = new Telerik.WinControls.UI.RadGroupBox();
			this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
			this._OptOrdenado_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._OptOrdenado_0 = new Telerik.WinControls.UI.RadRadioButton();
			this.cbHasta = new UpgradeHelpers.MSForms.MSCombobox();
			this.cbDesde = new UpgradeHelpers.MSForms.MSCombobox();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_1 = new Telerik.WinControls.UI.RadLabel();
			this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
			this._rbTipoGrafica_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._rbTipoGrafica_0 = new Telerik.WinControls.UI.RadRadioButton();
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this._optmedidas_3 = new Telerik.WinControls.UI.RadRadioButton();
			this._optmedidas_2 = new Telerik.WinControls.UI.RadRadioButton();
			this._optmedidas_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._optmedidas_0 = new Telerik.WinControls.UI.RadRadioButton();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.sdcfechahasta = new Telerik.WinControls.UI.RadDateTimePicker();
			this.sdcfechadesde = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this._Label1_0 = new Telerik.WinControls.UI.RadLabel();
			this.cmdSalir = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.Check6 = new Telerik.WinControls.UI.RadCheckBox();
			this.Check4 = new Telerik.WinControls.UI.RadCheckBox();
			this.Check3 = new Telerik.WinControls.UI.RadCheckBox();
			this.Check2 = new Telerik.WinControls.UI.RadCheckBox();
			this.Check1 = new Telerik.WinControls.UI.RadCheckBox();
			this.Check5 = new Telerik.WinControls.UI.RadCheckBox();
			this.Option1 = new Telerik.WinControls.UI.RadRadioButton();
			this.Option2 = new Telerik.WinControls.UI.RadRadioButton();
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.FraCamas.SuspendLayout();
			this.Frame5.SuspendLayout();
			this.Frame4.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// ShapeContainer1
			// 
			this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
			this.ShapeContainer1.Size = new System.Drawing.Size(193, 183);
			this.ShapeContainer1.Shapes.Add(Line1);
			this.ShapeContainer1.Shapes.Add(Line2);
			// 
			// cbImprimir
			// 
			//this.cbImprimir.BackColor = System.Drawing.SystemColors.Control;
			this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbImprimir.Enabled = false;
			//this.cbImprimir.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbImprimir.Location = new System.Drawing.Point(144, 318);
			this.cbImprimir.Name = "cbImprimir";
			this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbImprimir.Size = new System.Drawing.Size(81, 29);
			this.cbImprimir.TabIndex = 25;
			this.cbImprimir.Text = "&Imprimir";
			this.cbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            this.cbImprimir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FraCamas
            // 
            //this.FraCamas.BackColor = System.Drawing.SystemColors.Control;
			this.FraCamas.Controls.Add(this.Frame5);
			this.FraCamas.Controls.Add(this.cbHasta);
			this.FraCamas.Controls.Add(this.cbDesde);
			this.FraCamas.Controls.Add(this.Label6);
			this.FraCamas.Controls.Add(this.Label5);
			this.FraCamas.Controls.Add(this._Label1_1);
			this.FraCamas.Enabled = true;
			//this.FraCamas.ForeColor = System.Drawing.SystemColors.ControlText;
			this.FraCamas.Location = new System.Drawing.Point(212, 36);
			this.FraCamas.Name = "FraCamas";
			this.FraCamas.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FraCamas.Size = new System.Drawing.Size(191, 177);
			this.FraCamas.TabIndex = 20;
			this.FraCamas.Text = "Rango de camas";
			this.FraCamas.Visible = true;
			// 
			// Frame5
			// 
			//this.Frame5.BackColor = System.Drawing.SystemColors.Control;
			this.Frame5.Controls.Add(this._OptOrdenado_1);
			this.Frame5.Controls.Add(this._OptOrdenado_0);
			this.Frame5.Enabled = true;
			//this.Frame5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame5.Location = new System.Drawing.Point(16, 92);
			this.Frame5.Name = "Frame5";
			this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame5.Size = new System.Drawing.Size(161, 75);
			this.Frame5.TabIndex = 27;
			this.Frame5.Text = "Ordenadas por";
			this.Frame5.Visible = true;
			// 
			// _OptOrdenado_1
			// 
			//this._OptOrdenado_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._OptOrdenado_1.BackColor = System.Drawing.SystemColors.Control;
			this._OptOrdenado_1.CausesValidation = true;
			//this._OptOrdenado_1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._OptOrdenado_1.IsChecked = false;
			this._OptOrdenado_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._OptOrdenado_1.Enabled = true;
			this._OptOrdenado_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._OptOrdenado_1.Location = new System.Drawing.Point(14, 48);
			this._OptOrdenado_1.Name = "_OptOrdenado_1";
			this._OptOrdenado_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._OptOrdenado_1.Size = new System.Drawing.Size(63, 17);
			this._OptOrdenado_1.TabIndex = 29;
			this._OptOrdenado_1.TabStop = true;
			this._OptOrdenado_1.Text = "Paciente";
			//this._OptOrdenado_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._OptOrdenado_1.Visible = true;
			// 
			// _OptOrdenado_0
			// 
			//this._OptOrdenado_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._OptOrdenado_0.BackColor = System.Drawing.SystemColors.Control;
			this._OptOrdenado_0.CausesValidation = true;
			//this._OptOrdenado_0.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._OptOrdenado_0.IsChecked = true;
			this._OptOrdenado_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._OptOrdenado_0.Enabled = true;
			//this._OptOrdenado_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._OptOrdenado_0.Location = new System.Drawing.Point(14, 26);
			this._OptOrdenado_0.Name = "_OptOrdenado_0";
			this._OptOrdenado_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._OptOrdenado_0.Size = new System.Drawing.Size(47, 17);
			this._OptOrdenado_0.TabIndex = 28;
			this._OptOrdenado_0.TabStop = true;
			this._OptOrdenado_0.Text = "Cama";
			//this._OptOrdenado_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._OptOrdenado_0.Visible = true;
			// 
			// cbHasta
			// 
			this.cbHasta.BackColor = System.Drawing.SystemColors.Window;
			this.cbHasta.CausesValidation = true;
			this.cbHasta.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbHasta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbHasta.Enabled = false;
			this.cbHasta.ForeColor = System.Drawing.SystemColors.WindowText;
			//this.cbHasta.IntegralHeight = true;
			this.cbHasta.Location = new System.Drawing.Point(80, 56);
			this.cbHasta.Name = "cbHasta";
			this.cbHasta.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbHasta.Size = new System.Drawing.Size(96, 21);
			//this.cbHasta.Sorted = false;
			this.cbHasta.TabIndex = 22;
			this.cbHasta.TabStop = true;
			this.cbHasta.Visible = true;
			this.cbHasta.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbHasta_SelectedIndexChanged);
			// 
			// cbDesde
			// 
			this.cbDesde.BackColor = System.Drawing.SystemColors.Window;
			this.cbDesde.CausesValidation = true;
			this.cbDesde.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbDesde.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDesde.Enabled = true;
			this.cbDesde.ForeColor = System.Drawing.SystemColors.WindowText;
			//this.cbDesde.IntegralHeight = true;
			this.cbDesde.Location = new System.Drawing.Point(80, 28);
			this.cbDesde.Name = "cbDesde";
			this.cbDesde.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbDesde.Size = new System.Drawing.Size(96, 21);
			//this.cbDesde.Sorted = false;
			this.cbDesde.TabIndex = 21;
			this.cbDesde.TabStop = true;
			this.cbDesde.Visible = true;
			this.cbDesde.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbDesde_SelectedIndexChanged);
			// 
			// Label6
			// 
			//this.Label6.BackColor = System.Drawing.SystemColors.Control;
			//this.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label6.Location = new System.Drawing.Point(8, 122);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(79, 17);
			this.Label6.TabIndex = 26;
			// 
			// Label5
			// 
			//this.Label5.BackColor = System.Drawing.SystemColors.Control;
			//this.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label5.Location = new System.Drawing.Point(19, 60);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(33, 17);
			this.Label5.TabIndex = 24;
			this.Label5.Text = "Hasta:";
			// 
			// _Label1_1
			// 
			//this._Label1_1.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_1.Cursor = System.Windows.Forms.Cursors.Default;
			//this._Label1_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_1.Location = new System.Drawing.Point(18, 32);
			this._Label1_1.Name = "_Label1_1";
			this._Label1_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_1.Size = new System.Drawing.Size(57, 17);
			this._Label1_1.TabIndex = 23;
			this._Label1_1.Text = "Desde:";
			// 
			// Frame4
			// 
			//this.Frame4.BackColor = System.Drawing.SystemColors.Control;
			this.Frame4.Controls.Add(this._rbTipoGrafica_1);
			this.Frame4.Controls.Add(this._rbTipoGrafica_0);
			this.Frame4.Enabled = true;
			//this.Frame4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame4.Location = new System.Drawing.Point(6, 218);
			this.Frame4.Name = "Frame4";
			this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame4.Size = new System.Drawing.Size(397, 43);
			this.Frame4.TabIndex = 17;
			this.Frame4.Text = "Tipo de gr�fica";
			this.Frame4.Visible = true;
			// 
			// _rbTipoGrafica_1
			// 
			//this._rbTipoGrafica_1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._rbTipoGrafica_1.BackColor = System.Drawing.SystemColors.Control;
			this._rbTipoGrafica_1.CausesValidation = true;
			//this._rbTipoGrafica_1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._rbTipoGrafica_1.IsChecked = false;
			this._rbTipoGrafica_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._rbTipoGrafica_1.Enabled = true;
			//this._rbTipoGrafica_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._rbTipoGrafica_1.Location = new System.Drawing.Point(216, 18);
			this._rbTipoGrafica_1.Name = "_rbTipoGrafica_1";
			this._rbTipoGrafica_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._rbTipoGrafica_1.Size = new System.Drawing.Size(65, 13);
			this._rbTipoGrafica_1.TabIndex = 19;
			this._rbTipoGrafica_1.TabStop = true;
			this._rbTipoGrafica_1.Text = "Horaria";
			//this._rbTipoGrafica_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._rbTipoGrafica_1.Visible = true;
			this._rbTipoGrafica_1.CheckStateChanged += new System.EventHandler(this.rbTipoGrafica_CheckedChanged);
			// 
			// _rbTipoGrafica_0
			// 
			//this._rbTipoGrafica_0.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._rbTipoGrafica_0.BackColor = System.Drawing.SystemColors.Control;
			this._rbTipoGrafica_0.CausesValidation = true;
			//this._rbTipoGrafica_0.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._rbTipoGrafica_0.IsChecked = false;
			this._rbTipoGrafica_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._rbTipoGrafica_0.Enabled = true;
			//this._rbTipoGrafica_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._rbTipoGrafica_0.Location = new System.Drawing.Point(62, 18);
			this._rbTipoGrafica_0.Name = "_rbTipoGrafica_0";
			this._rbTipoGrafica_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._rbTipoGrafica_0.Size = new System.Drawing.Size(65, 13);
			this._rbTipoGrafica_0.TabIndex = 18;
			this._rbTipoGrafica_0.TabStop = true;
			this._rbTipoGrafica_0.Text = "Diaria";
			//this._rbTipoGrafica_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._rbTipoGrafica_0.Visible = true;
			this._rbTipoGrafica_0.CheckStateChanged += new System.EventHandler(this.rbTipoGrafica_CheckedChanged);
			// 
			// Frame3
			// 
			//this.Frame3.BackColor = System.Drawing.SystemColors.Control;
			this.Frame3.Controls.Add(this._optmedidas_3);
			this.Frame3.Controls.Add(this._optmedidas_2);
			this.Frame3.Controls.Add(this._optmedidas_1);
			this.Frame3.Controls.Add(this._optmedidas_0);
			this.Frame3.Enabled = true;
			//this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame3.Location = new System.Drawing.Point(204, 36);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(193, 161);
			this.Frame3.TabIndex = 10;
			this.Frame3.Text = "Medidas";
			this.Frame3.Visible = true;
			// 
			// _optmedidas_3
			// 
			//this._optmedidas_3.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optmedidas_3.BackColor = System.Drawing.SystemColors.Control;
			this._optmedidas_3.CausesValidation = true;
			//this._optmedidas_3.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._optmedidas_3.IsChecked = false;
			this._optmedidas_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._optmedidas_3.Enabled = true;
			//this._optmedidas_3.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optmedidas_3.Location = new System.Drawing.Point(24, 120);
			this._optmedidas_3.Name = "_optmedidas_3";
			this._optmedidas_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optmedidas_3.Size = new System.Drawing.Size(145, 25);
			this._optmedidas_3.TabIndex = 14;
			this._optmedidas_3.TabStop = true;
			this._optmedidas_3.Text = "Per�metro abdominal";
			//this._optmedidas_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._optmedidas_3.Visible = true;
			this._optmedidas_3.CheckStateChanged += new System.EventHandler(this.optmedidas_CheckedChanged);
			// 
			// _optmedidas_2
			// 
			//this._optmedidas_2.Appearance = System.Windows.Forms.Appearance.Normal;
			//this._optmedidas_2.BackColor = System.Drawing.SystemColors.Control;
			this._optmedidas_2.CausesValidation = true;
			//this._optmedidas_2.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._optmedidas_2.IsChecked = false;
			this._optmedidas_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._optmedidas_2.Enabled = true;
			//this._optmedidas_2.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optmedidas_2.Location = new System.Drawing.Point(24, 88);
			this._optmedidas_2.Name = "_optmedidas_2";
			this._optmedidas_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optmedidas_2.Size = new System.Drawing.Size(113, 25);
			this._optmedidas_2.TabIndex = 13;
			this._optmedidas_2.TabStop = true;
			this._optmedidas_2.Text = "Per�metro Craneal";
			//this._optmedidas_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._optmedidas_2.Visible = true;
			this._optmedidas_2.CheckStateChanged += new System.EventHandler(this.optmedidas_CheckedChanged);
			// 
			// _optmedidas_1
			// 
			//this._optmedidas_1.Appearance = System.Windows.Forms.Appearance.Normal;
			this._optmedidas_1.BackColor = System.Drawing.SystemColors.Control;
			this._optmedidas_1.CausesValidation = true;
			//this._optmedidas_1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._optmedidas_1.IsChecked = false;
			this._optmedidas_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._optmedidas_1.Enabled = true;
			//this._optmedidas_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optmedidas_1.Location = new System.Drawing.Point(24, 56);
			this._optmedidas_1.Name = "_optmedidas_1";
			this._optmedidas_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optmedidas_1.Size = new System.Drawing.Size(113, 25);
			this._optmedidas_1.TabIndex = 12;
			this._optmedidas_1.TabStop = true;
			this._optmedidas_1.Text = "Talla";
			//this._optmedidas_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._optmedidas_1.Visible = true;
			this._optmedidas_1.CheckStateChanged += new System.EventHandler(this.optmedidas_CheckedChanged);
			// 
			// _optmedidas_0
			// 
			//this._optmedidas_0.Appearance = System.Windows.Forms.Appearance.Normal;
			this._optmedidas_0.BackColor = System.Drawing.SystemColors.Control;
			this._optmedidas_0.CausesValidation = true;
			//this._optmedidas_0.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._optmedidas_0.IsChecked = false;
			this._optmedidas_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._optmedidas_0.Enabled = true;
			//this._optmedidas_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._optmedidas_0.Location = new System.Drawing.Point(24, 24);
			this._optmedidas_0.Name = "_optmedidas_0";
			this._optmedidas_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._optmedidas_0.Size = new System.Drawing.Size(113, 25);
			this._optmedidas_0.TabIndex = 11;
			this._optmedidas_0.TabStop = true;
			this._optmedidas_0.Text = "Peso";
			//this._optmedidas_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this._optmedidas_0.Visible = true;
			this._optmedidas_0.CheckStateChanged += new System.EventHandler(this.optmedidas_CheckedChanged);
			// 
			// Frame2
			// 
			//this.Frame2.BackColor = System.Drawing.SystemColors.Control;
			this.Frame2.Controls.Add(this.sdcfechahasta);
			this.Frame2.Controls.Add(this.sdcfechadesde);
			this.Frame2.Controls.Add(this.Label2);
			this.Frame2.Controls.Add(this._Label1_0);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(6, 264);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(397, 49);
			this.Frame2.TabIndex = 5;
			this.Frame2.Text = "Fechas de Impresi�n";
			this.Frame2.Visible = true;
			// 
			// sdcfechahasta
			// 
			//this.sdcfechahasta.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.sdcfechahasta.CustomFormat = "dd/MM/yy";
			this.sdcfechahasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.sdcfechahasta.Location = new System.Drawing.Point(276, 20);
			this.sdcfechahasta.Name = "sdcfechahasta";
			this.sdcfechahasta.Size = new System.Drawing.Size(106, 17);
			this.sdcfechahasta.TabIndex = 6;
			// 
			// sdcfechadesde
			// 
			//this.sdcfechadesde.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.sdcfechadesde.CustomFormat = "dd/MM/yy";
			this.sdcfechadesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.sdcfechadesde.Location = new System.Drawing.Point(72, 20);
			this.sdcfechadesde.Name = "sdcfechadesde";
			this.sdcfechadesde.Size = new System.Drawing.Size(106, 17);
			this.sdcfechadesde.TabIndex = 7;
			// 
			// Label2
			// 
			//this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(212, 20);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(49, 17);
			this.Label2.TabIndex = 9;
			this.Label2.Text = "Hasta";
			// 
			// _Label1_0
			// 
			//this._Label1_0.BackColor = System.Drawing.SystemColors.Control;
			//this._Label1_0.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._Label1_0.Cursor = System.Windows.Forms.Cursors.Default;
			//this._Label1_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_0.Location = new System.Drawing.Point(8, 20);
			this._Label1_0.Name = "_Label1_0";
			this._Label1_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label1_0.Size = new System.Drawing.Size(57, 17);
			this._Label1_0.TabIndex = 8;
			this._Label1_0.Text = "Desde";
			// 
			// cmdSalir
			// 
			//this.cmdSalir.BackColor = System.Drawing.SystemColors.Control;
			this.cmdSalir.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cmdSalir.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdSalir.Location = new System.Drawing.Point(324, 318);
			this.cmdSalir.Name = "cmdSalir";
			this.cmdSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdSalir.Size = new System.Drawing.Size(81, 29);
			this.cmdSalir.TabIndex = 4;
			this.cmdSalir.Text = "&Cancelar";
			this.cmdSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cmdSalir.UseVisualStyleBackColor = false;
			this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            this.cmdSalir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cbAceptar
            // 
            //this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Enabled = false;
			//this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAceptar.Location = new System.Drawing.Point(230, 318);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 3;
			this.cbAceptar.Text = "&Gr�fica";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            this.cbAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame1
            // 
            //this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			this.Frame1.Controls.Add(this.Check6);
			this.Frame1.Controls.Add(this.Check4);
			this.Frame1.Controls.Add(this.Check3);
			this.Frame1.Controls.Add(this.Check2);
			this.Frame1.Controls.Add(this.Check1);
			this.Frame1.Controls.Add(this.Check5);
			this.Frame1.Controls.Add(this.Option1);
			this.Frame1.Controls.Add(this.Option2);
			this.Frame1.Enabled = true;
			//this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(6, 32);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(193, 183);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Seleccione documento a imprimir";
			this.Frame1.Visible = true;
			// 
			// Check6
			// 
			//this.Check6.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.Check6.BackColor = System.Drawing.SystemColors.Control;
			this.Check6.CausesValidation = true;
			//this.Check6.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Check6.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.Check6.Cursor = System.Windows.Forms.Cursors.Default;
			this.Check6.Enabled = true;
			//this.Check6.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Check6.Location = new System.Drawing.Point(10, 122);
			this.Check6.Name = "Check6";
			this.Check6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Check6.Size = new System.Drawing.Size(131, 17);
			this.Check6.TabIndex = 35;
			this.Check6.TabStop = true;
			this.Check6.Text = "Dolor (EVA)";
			this.Check6.Visible = true;
			this.Check6.CheckStateChanged += new System.EventHandler(this.Check6_CheckStateChanged);
			// 
			// Check4
			// 
			//this.Check4.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.Check4.BackColor = System.Drawing.SystemColors.Control;
			this.Check4.CausesValidation = true;
			//this.Check4.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Check4.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.Check4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Check4.Enabled = true;
			//this.Check4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Check4.Location = new System.Drawing.Point(10, 80);
			this.Check4.Name = "Check4";
			this.Check4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Check4.Size = new System.Drawing.Size(131, 17);
			this.Check4.TabIndex = 34;
			this.Check4.TabStop = true;
			this.Check4.Text = "Temperatura";
			this.Check4.Visible = true;
			this.Check4.CheckStateChanged += new System.EventHandler(this.Check4_CheckStateChanged);
			// 
			// Check3
			// 
			//this.Check3.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.Check3.BackColor = System.Drawing.SystemColors.Control;
			this.Check3.CausesValidation = true;
			//this.Check3.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Check3.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.Check3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Check3.Enabled = true;
			//this.Check3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Check3.Location = new System.Drawing.Point(10, 60);
			this.Check3.Name = "Check3";
			this.Check3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Check3.Size = new System.Drawing.Size(131, 17);
			this.Check3.TabIndex = 33;
			this.Check3.TabStop = true;
			this.Check3.Text = "Frecuencia cardiaca";
			this.Check3.Visible = true;
			this.Check3.CheckStateChanged += new System.EventHandler(this.Check3_CheckStateChanged);
			// 
			// Check2
			// 
			//this.Check2.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.Check2.BackColor = System.Drawing.SystemColors.Control;
			this.Check2.CausesValidation = true;
			//this.Check2.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Check2.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.Check2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Check2.Enabled = true;
			//this.Check2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Check2.Location = new System.Drawing.Point(10, 39);
			this.Check2.Name = "Check2";
			this.Check2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Check2.Size = new System.Drawing.Size(131, 17);
			this.Check2.TabIndex = 32;
			this.Check2.TabStop = true;
			this.Check2.Text = "Tensi�n arterial";
			this.Check2.Visible = true;
			this.Check2.CheckStateChanged += new System.EventHandler(this.Check2_CheckStateChanged);
			// 
			// Check1
			// 
			//this.Check1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.Check1.BackColor = System.Drawing.SystemColors.Control;
			this.Check1.CausesValidation = true;
			//this.Check1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Check1.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.Check1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Check1.Enabled = true;
			//this.Check1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Check1.Location = new System.Drawing.Point(10, 17);
			this.Check1.Name = "Check1";
			this.Check1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Check1.Size = new System.Drawing.Size(131, 17);
			this.Check1.TabIndex = 31;
			this.Check1.TabStop = true;
			this.Check1.Text = "Frecuencia respiratoria";
			this.Check1.Visible = true;
			this.Check1.CheckStateChanged += new System.EventHandler(this.Check1_CheckStateChanged);
			// 
			// Check5
			// 
			//this.Check5.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.Check5.BackColor = System.Drawing.SystemColors.Control;
			this.Check5.CausesValidation = true;
			//this.Check5.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Check5.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.Check5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Check5.Enabled = true;
			//this.Check5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Check5.Location = new System.Drawing.Point(10, 100);
			this.Check5.Name = "Check5";
			this.Check5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Check5.Size = new System.Drawing.Size(131, 17);
			this.Check5.TabIndex = 30;
			this.Check5.TabStop = true;
			this.Check5.Text = "Balance";
			this.Check5.Visible = true;
			this.Check5.CheckStateChanged += new System.EventHandler(this.Check5_CheckStateChanged);
			// 
			// Option1
			// 
			//this.Option1.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.Option1.BackColor = System.Drawing.SystemColors.Control;
			this.Option1.CausesValidation = true;
			//this.Option1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Option1.IsChecked = false;
			this.Option1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Option1.Enabled = true;
			//this.Option1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Option1.Location = new System.Drawing.Point(16, 156);
			this.Option1.Name = "Option1";
			this.Option1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Option1.Size = new System.Drawing.Size(65, 13);
			this.Option1.TabIndex = 2;
			this.Option1.TabStop = true;
			this.Option1.Text = "Todos";
			//this.Option1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Option1.Visible = true;
			this.Option1.CheckStateChanged += new System.EventHandler(this.Option1_CheckedChanged);
			// 
			// Option2
			// 
			//this.Option2.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.Option2.BackColor = System.Drawing.SystemColors.Control;
			this.Option2.CausesValidation = true;
			//this.Option2.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Option2.IsChecked = false;
			this.Option2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Option2.Enabled = true;
			//this.Option2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Option2.Location = new System.Drawing.Point(96, 156);
			this.Option2.Name = "Option2";
			this.Option2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Option2.Size = new System.Drawing.Size(73, 13);
			this.Option2.TabIndex = 1;
			this.Option2.TabStop = true;
			this.Option2.Text = "Ninguno";
			//this.Option2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Option2.Visible = true;
			this.Option2.CheckStateChanged += new System.EventHandler(this.Option2_CheckedChanged);
			// 
			// Line1
			// 
			this.Line1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line1.BorderWidth = 1;
			this.Line1.Enabled = false;
			this.Line1.Name = "Line1";
			this.Line1.Visible = true;
			this.Line1.X1 = (int) 0;
			this.Line1.X2 = (int) 292;
			this.Line1.Y1 = (int) 135;
			this.Line1.Y2 = (int) 135;
			// 
			// Line2
			// 
			this.Line2.BorderColor = System.Drawing.SystemColors.Window;
			this.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line2.BorderWidth = 1;
			this.Line2.Enabled = false;
			this.Line2.Name = "Line2";
			this.Line2.Visible = true;
			this.Line2.X1 = (int) 1;
			this.Line2.X2 = (int) 293;
			this.Line2.Y1 = (int) 185;
			this.Line2.Y2 = (int) 185;
			// 
			// Label4
			// 
			//this.Label4.BackColor = System.Drawing.SystemColors.Control;
			//this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label4.Location = new System.Drawing.Point(10, 8);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(53, 17);
			this.Label4.TabIndex = 16;
			this.Label4.Text = "Paciente:";
			// 
			// Label3
			// 
			//this.Label3.BackColor = System.Drawing.SystemColors.Control;
			//this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label3.Location = new System.Drawing.Point(64, 8);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(341, 17);
			this.Label3.TabIndex = 15;
			// 
			// HOJA_SEGUIMIENTO
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(406, 352);
			this.ControlBox = false;
			this.Controls.Add(this.cbImprimir);
			this.Controls.Add(this.FraCamas);
			this.Controls.Add(this.Frame4);
			this.Controls.Add(this.Frame3);
			this.Controls.Add(this.Frame2);
			this.Controls.Add(this.cmdSalir);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(ShapeContainer1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "HOJA_SEGUIMIENTO";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Hoja de seguimiento cl�nico - EGI240F1";
			this.Closed += new System.EventHandler(this.HOJA_SEGUIMIENTO_Closed);
			this.Load += new System.EventHandler(this.HOJA_SEGUIMIENTO_Load);
			this.FraCamas.ResumeLayout(false);
			this.Frame5.ResumeLayout(false);
			this.Frame4.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializerbTipoGrafica();
			Initializeoptmedidas();
			InitializeOptOrdenado();
			InitializeLabel1();
		}
		void InitializerbTipoGrafica()
		{
			this.rbTipoGrafica = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbTipoGrafica[1] = _rbTipoGrafica_1;
			this.rbTipoGrafica[0] = _rbTipoGrafica_0;
		}
		void Initializeoptmedidas()
		{
			this.optmedidas = new Telerik.WinControls.UI.RadRadioButton[4];
			this.optmedidas[3] = _optmedidas_3;
			this.optmedidas[2] = _optmedidas_2;
			this.optmedidas[1] = _optmedidas_1;
			this.optmedidas[0] = _optmedidas_0;
		}
		void InitializeOptOrdenado()
		{
			this.OptOrdenado = new Telerik.WinControls.UI.RadRadioButton[2];
			this.OptOrdenado[1] = _OptOrdenado_1;
			this.OptOrdenado[0] = _OptOrdenado_0;
		}
		void InitializeLabel1()
		{
			this.Label1 = new Telerik.WinControls.UI.RadLabel[2];
			this.Label1[1] = _Label1_1;
			this.Label1[0] = _Label1_0;
		}
		#endregion
	}
}