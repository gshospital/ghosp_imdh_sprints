using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
namespace GRAFICADLL
{
	public class MCGRAFICA
	{

		//Public Sub load(rc As rdoConnection, Fdesde As Date, _
		//'    Fhasta As Date, a�o As String, Num As String, frecuencia As Boolean, _
		//'    Tension As Boolean, Pulso As Boolean, Temperatura As Boolean, _
		//'    Servicio As String, ObjetoCrystal As Object, _
		//'    NombreBaseDatos As String, PathInformes As String, _
		//'    DSN_ACCESS As String, PathAplicacion As String, Usuario As String)
		public void load(SqlConnection rc, string a�o, string num, string Servicio, object ObjetoCrystal, string NombreBaseDatos, string PathInformes, string DSN_ACCESS, string PathAplicacion, string usuario)
		{


			MGRAFICA.RcEnfermeria = rc;
			Serrores.AjustarRelojCliente(MGRAFICA.RcEnfermeria);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref MGRAFICA.RcEnfermeria);

			MGRAFICA.vstA�oAdmi = a�o;
			MGRAFICA.vstNumAdmi = num;

			MGRAFICA.stTipSer = Servicio;
			MGRAFICA.oCrystal = ObjetoCrystal;
			MGRAFICA.stNombreBD = NombreBaseDatos;
			MGRAFICA.PathReport = PathInformes;
			MGRAFICA.stDSN_ACCESS = DSN_ACCESS;
			MGRAFICA.stPathAplicacion = PathAplicacion;
			MGRAFICA.gUsuario = usuario;

			//oscar 11/05/04
			MGRAFICA.bMasivo = false;
			//----------------

			if (MGRAFICA.stTipSer == "H")
			{
				MGRAFICA.TOrigen = "Aepisadm";
			}
			else if (MGRAFICA.stTipSer == "U")
			{ 
				MGRAFICA.TOrigen = "Uepisurg";
			}
			else if (MGRAFICA.stTipSer == "Q")
			{ 
				MGRAFICA.TOrigen = "Qintquir";
			}
			if (MGRAFICA.stTipSer == "Q")
			{
				//si es de quirofanos tenemos que encontrar la fecha de inicio y de fin del intervalo
				RangoFechas();
				if (MGRAFICA.stFechaMinimaIntervalo.Trim() == "" || MGRAFICA.stFechaMaximaIntervalo.Trim() == "")
				{
					short tempRefParam = 1520;
					string[] tempRefParam2 = new string[]{"Constantes", "realizar la gr�fica"};
					MGRAFICA.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, MGRAFICA.RcEnfermeria, tempRefParam2);
					//MsgBox "no hay datos para la gr�fica"
				}
				else
				{
					//oscar 11/05/04
					GHORARIA_SH.DefInstance.ganoadme = Convert.ToInt32(Double.Parse(MGRAFICA.vstA�oAdmi));
					GHORARIA_SH.DefInstance.gnumadme = Convert.ToInt32(Double.Parse(MGRAFICA.vstNumAdmi));
					//----------
					GHORARIA_SH.DefInstance.ShowDialog();
				}
			}
			else
			{
				MGRAFICA.stGidenpac = MGRAFICA.proObtenerGidenpac(MGRAFICA.vstA�oAdmi, MGRAFICA.vstNumAdmi);
				HOJA_SEGUIMIENTO.DefInstance.ShowDialog();
				//GRAFICA.Show 1
			}
		}

		public MCGRAFICA()
		{
			MGRAFICA.clase_mensaje = new Mensajes.ClassMensajes();
		}
		private void RangoFechas()
		{

			string sql = "select min(fapuntes) FechaMinima ,max(fapuntes) FechaMaxima   " + 
			             "from econsvit " + 
			             "Where itiposer = '" + MGRAFICA.stTipSer + "' " + 
			             "and ganoregi= " + MGRAFICA.vstA�oAdmi + " " + 
			             "and gnumregi= " + MGRAFICA.vstNumAdmi + " ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MGRAFICA.RcEnfermeria);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["FechaMinima"]))
				{
					MGRAFICA.stFechaMinimaIntervalo = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["FechaMinima"]).ToString("dd/MM/yyyy HH:NN:SS");
				}
				else
				{
					MGRAFICA.stFechaMinimaIntervalo = "";
				}
				if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["FechaMaxima"]))
				{
					MGRAFICA.stFechaMaximaIntervalo = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["FechaMaxima"]).ToString("dd/MM/yyyy HH:NN:SS");
				}
				else
				{
					MGRAFICA.stFechaMaximaIntervalo = "";
				}
			}
			RrSql.Close();

		}

		//oscar 11/05/04
		public void load_Masivo(Form Formulario, SqlConnection rc, object ObjetoCrystal, string NombreBaseDatos, string PathInformes, string DSN_ACCESS, string PathAplicacion, string usuario, string unidad)
		{

			MGRAFICA.RcEnfermeria = rc;
			MGRAFICA.oCrystal = ObjetoCrystal;
			MGRAFICA.objFormulario = Formulario;

			MGRAFICA.stNombreBD = NombreBaseDatos;
			MGRAFICA.PathReport = PathInformes;
			MGRAFICA.stDSN_ACCESS = DSN_ACCESS;
			MGRAFICA.stPathAplicacion = PathAplicacion;
			MGRAFICA.gUsuario = usuario;
			Serrores.AjustarRelojCliente(MGRAFICA.RcEnfermeria);

			MGRAFICA.bMasivo = true;
			MGRAFICA.stUnidadEnf = unidad;

			MGRAFICA.stTipSer = "H";
			MGRAFICA.TOrigen = "Aepisadm";

			HOJA_SEGUIMIENTO.DefInstance.Show(Formulario);

		}
		//-------------------
	}
}