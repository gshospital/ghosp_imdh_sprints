using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace GRAFICADLL
{
	public partial class HOJA_SEGUIMIENTO
		: RadForm
	{

		object cryInformes = null;
		int tiForm = 0;
		string campo = String.Empty;
		public HOJA_SEGUIMIENTO()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}





		private void Check1_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (Check1.CheckState == CheckState.Unchecked)
			{
				Option1.IsChecked = false;
			}
			else
			{
				Option2.IsChecked = false;
				for (int i = 0; i <= 3; i++)
				{
					optmedidas[i].IsChecked = false;
				}
				rbTipoGrafica[1].IsChecked = false;
				rbTipoGrafica[1].Enabled = true;
			}
			activar_aceptar();
		}

		private void Check2_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (Check2.CheckState == CheckState.Unchecked)
			{
				Option1.IsChecked = false;
			}
			else
			{
				Option2.IsChecked = false;
				for (int i = 0; i <= 3; i++)
				{
					optmedidas[i].IsChecked = false;
				}
				rbTipoGrafica[1].IsChecked = false;
				rbTipoGrafica[1].Enabled = true;
			}
			activar_aceptar();
		}

		private void Check3_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (Check3.CheckState == CheckState.Unchecked)
			{
				Option1.IsChecked = false;
			}
			else
			{
				Option2.IsChecked = false;
				for (int i = 0; i <= 3; i++)
				{
					optmedidas[i].IsChecked = false;
				}
				rbTipoGrafica[1].IsChecked = false;
				rbTipoGrafica[1].Enabled = true;
			}
			activar_aceptar();
		}

		private void Check4_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (Check4.CheckState == CheckState.Unchecked)
			{
				Option1.IsChecked = false;
			}
			else
			{
				Option2.IsChecked = false;
				for (int i = 0; i <= 3; i++)
				{
					optmedidas[i].IsChecked = false;
				}
				rbTipoGrafica[1].IsChecked = false;
				rbTipoGrafica[1].Enabled = true;
			}
			activar_aceptar();
		}



		private void Check5_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (Check4.CheckState == CheckState.Unchecked)
			{
				Option1.IsChecked = false;
			}
			else
			{
				Option2.IsChecked = false;
				for (int i = 0; i <= 3; i++)
				{
					optmedidas[i].IsChecked = false;
				}
				rbTipoGrafica[1].IsChecked = false;
				rbTipoGrafica[1].Enabled = true;
			}
			activar_aceptar();
		}





		private void cmdSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		//****************************************************************************************
		//*  Procedimiento: CbAceptar_Click                                                      *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias (16/19/2006)                                                              *
		//*                                                                                      *
		//*      - Compruebo el valor de check(6) para activar el boton de aceptar o no          *
		//*                                                                                      *
		//****************************************************************************************
		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (!rbTipoGrafica[0].IsChecked && !rbTipoGrafica[1].IsChecked)
			{
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{"Tipo de gr�fica"};
				MGRAFICA.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, MGRAFICA.RcEnfermeria, tempRefParam2);
				rbTipoGrafica[0].Focus();
				return;
			}

			if ((DateTime.Parse(sdcfechahasta.Text) < DateTime.Parse(sdcfechadesde.Text)) && rbTipoGrafica[0].IsChecked)
			{
				cbAceptar.Enabled = false;
				short tempRefParam3 = 1020;
				string[] tempRefParam4 = new string[]{"Fecha hasta", ">", "fecha desde"};
				MGRAFICA.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, MGRAFICA.RcEnfermeria, tempRefParam4);
				sdcfechahasta.Focus();
			}
			else
			{
				cbAceptar.Enabled = true;

				//************* O.Frias - 28/09/2006 - Inicio ****************
				if (Check1.CheckState == CheckState.Checked || Check2.CheckState == CheckState.Checked || Check3.CheckState == CheckState.Checked || Check4.CheckState == CheckState.Checked || Check5.CheckState == CheckState.Checked || Check6.CheckState == CheckState.Checked)
				{
					//************** O.Frias - 28/09/2006 - Fin ******************

					//oscar 11/05/04
					//If rbTipoGrafica(0).Value = True Then
					//       GRAFICA.Show 1
					//Else
					//       GHORARIA.Show 1
					//End If
					if (MGRAFICA.bMasivo)
					{
						proRepetirGraficas("P");
					}
					else
					{
						if (rbTipoGrafica[0].IsChecked)
						{
							GRAFICA_SH.DefInstance.ganoadme = Convert.ToInt32(Double.Parse(MGRAFICA.vstA�oAdmi));
							GRAFICA_SH.DefInstance.gnumadme = Convert.ToInt32(Double.Parse(MGRAFICA.vstNumAdmi));
							GRAFICA_SH.DefInstance.ShowDialog();
						}
						else
						{
							GHORARIA_SH.DefInstance.ganoadme = Convert.ToInt32(Double.Parse(MGRAFICA.vstA�oAdmi));
							GHORARIA_SH.DefInstance.gnumadme = Convert.ToInt32(Double.Parse(MGRAFICA.vstNumAdmi));
							GHORARIA_SH.DefInstance.ShowDialog();
						}
					}
					//-------

				}
				else
				{
					if (optmedidas[0].IsChecked)
					{
						campo = "CPESOPAC";
						REALIZAR_Grafica();
					}
					if (optmedidas[1].IsChecked)
					{
						campo = "ctallapac";
						REALIZAR_Grafica();
					}
					if (optmedidas[2].IsChecked)
					{
						campo = "cpercran";
						REALIZAR_Grafica();
					}
					if (optmedidas[3].IsChecked)
					{
						campo = "cperabdo";
						REALIZAR_Grafica();
					}
				}
			}
		}





		private void Check6_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (Check6.CheckState == CheckState.Unchecked)
			{
				Option1.IsChecked = false;
			}
			else
			{
				Option2.IsChecked = false;
				for (int i = 0; i <= 3; i++)
				{
					optmedidas[i].IsChecked = false;
				}
				rbTipoGrafica[1].IsChecked = false;
				rbTipoGrafica[1].Enabled = true;
			}
			activar_aceptar();
		}

		private void HOJA_SEGUIMIENTO_Load(Object eventSender, EventArgs eventArgs)
		{
			string sql = String.Empty;
			DataSet RrSql = null;
			try
			{
				this.Top = (int) 0;
				this.Left = (int) 0;
				CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
                // Ahora no se filtra por la fecha de ingreso en planta para que pueda
                // sacar la informacion de los apuntes de urgencias
                //sql = "select fingplan from aepisadm " & _
                //'" Where " & _
                //'" ganoadme = " & vstA�oAdmi & " and " & _
                //'" gnumadme = " & vstNumAdmi '& " and " & _
                //" faltplan is null "
                //Set Rrsql = RcEnfermeria.OpenResultset(sql, rdOpenKeyset)
                //If Rrsql.RowCount <> 0 Then
                //    sdcfechadesde.MinDate = Format(Rrsql("fingplan"), "DD/mm/yyyy")
                //    sdcfechadesde.MaxDate = Format(Now, "dd/mm/yyyy")
                //    sdcfechadesde.Date = Format(Rrsql("fingplan"), "DD/mm/yyyy")
                //End If
                //Rrsql.Close
                sdcfechahasta.MaxDate = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				sdcfechahasta.Value = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));

				//oscar 11/05/04
				//A�adimos la posibilidad de obtener la grafica de forma masiva por camas de la
				//unidad de enfermeria seleccionada
				//Label3.Caption = proObtenerPaciente(vstA�oAdmi, vstNumAdmi)
				if (MGRAFICA.bMasivo)
				{
					Cargar_Camas_Ocupadas();
					FraCamas.Visible = true;
					Frame3.Visible = false;
					Label4.Text = "U. Enf:";
					Label3.Text = proObtenerUnidadEnf(MGRAFICA.stUnidadEnf);
					this.Text = "Hoja de seguimiento cl�nico Masivo - EGI240F1";
					cbDesde.SelectedIndex = -1;
					cbHasta.SelectedIndex = -1;
					cbAceptar.Text = "Pantalla";
					cbImprimir.Visible = true;
					MGRAFICA.objFormulario.Enabled = false;
					//this.setHelpContextID(15241);
				}
				else
				{
					Label3.Text = MGRAFICA.proObtenerPaciente(MGRAFICA.vstA�oAdmi, MGRAFICA.vstNumAdmi);
					FraCamas.Visible = false;
					Frame3.Visible = true;
					cbImprimir.Visible = false;
					//this.setHelpContextID(15240);
				}
				//--------------

				//OSCAR C 28/07/2005
				//La FJD quiere que el valor 100 de la tesion coincida con el valor 37 de la temperatura,
				//por lo que la escala debe estar como estaba antes: de 0 a 300 en lugar de de -50 a 250.
				if (Serrores.ObternerValor_CTEGLOBAL(MGRAFICA.RcEnfermeria, "GRAESTEN", "VALFANU1") == "1")
				{
					MGRAFICA.vminTramo1 = 0;
					MGRAFICA.vmaxTramo1 = 50;
					MGRAFICA.vminTramo2 = 50;
					MGRAFICA.vmaxTramo2 = 100;
					MGRAFICA.vminTramo3 = 100;
					MGRAFICA.vmaxTramo3 = 150;
					MGRAFICA.vminTramo4 = 150;
					MGRAFICA.vmaxTramo4 = 200;
					MGRAFICA.vminTramo5 = 200;
					MGRAFICA.vmaxTramo5 = 250;
					MGRAFICA.vminTramo6 = 250;
					MGRAFICA.vmaxTramo6 = 300;
				}
				else
				{
					MGRAFICA.vminTramo1 = -50;
					MGRAFICA.vmaxTramo1 = 0;
					MGRAFICA.vminTramo2 = 0;
					MGRAFICA.vmaxTramo2 = 50;
					MGRAFICA.vminTramo3 = 50;
					MGRAFICA.vmaxTramo3 = 100;
					MGRAFICA.vminTramo4 = 100;
					MGRAFICA.vmaxTramo4 = 150;
					MGRAFICA.vminTramo5 = 150;
					MGRAFICA.vmaxTramo5 = 200;
					MGRAFICA.vminTramo6 = 200;
					MGRAFICA.vmaxTramo6 = 250;
				}
				//-----------
			}
			catch(Exception Exep)
			{
				Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "Form_Load:HOJA_SEGUIMIENTO", Exep);
				RrSql = null;
			}

		}

		//****************************************************************************************
		//*  Procedimiento: Option1_Click                                                        *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias (16/19/2006)                                                              *
		//*                                                                                      *
		//*      - Incorporo el control de dolor(EVA)                                            *
		//*                                                                                      *
		//****************************************************************************************
		private bool isInitializingComponent;
		private void Option1_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				Check1.CheckState = CheckState.Checked;
				Check2.CheckState = CheckState.Checked;
				Check3.CheckState = CheckState.Checked;
				Check4.CheckState = CheckState.Checked;
				Check5.CheckState = CheckState.Checked;

				//************* O.Frias - 16/10/2006 - Inicio ****************
				Check6.CheckState = CheckState.Checked;
				//************* O.Frias - 16/10/2006 - Fin *******************

			}
		}

		//****************************************************************************************
		//*  Procedimiento: Option2_Click                                                        *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias (16/19/2006)                                                              *
		//*                                                                                      *
		//*      - Incorporo el control de dolor(EVA)                                            *
		//*                                                                                      *
		//****************************************************************************************
		private void Option2_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				Check1.CheckState = CheckState.Unchecked;
				Check2.CheckState = CheckState.Unchecked;
				Check3.CheckState = CheckState.Unchecked;
				Check4.CheckState = CheckState.Unchecked;
				Check5.CheckState = CheckState.Unchecked;

				//************* O.Frias - 16/10/2006 - Inicio ****************
				Check6.CheckState = CheckState.Unchecked;
				//************* O.Frias - 16/10/2006 - Fin *******************

			}
		}

		//****************************************************************************************
		//*  Procedimiento: optmedidas_Click                                                     *
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*    O.Frias (16/19/2006)                                                              *
		//*                                                                                      *
		//*      - Incorporo el control de dolor(EVA)                                            *
		//*                                                                                      *
		//****************************************************************************************

		private void optmedidas_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				Check1.CheckState = CheckState.Unchecked;
				Check2.CheckState = CheckState.Unchecked;
				Check3.CheckState = CheckState.Unchecked;
				Check4.CheckState = CheckState.Unchecked;
				Check5.CheckState = CheckState.Unchecked;

				//************* O.Frias - 16/10/2006 - Inicio ****************
				Check6.CheckState = CheckState.Unchecked;
				//************* O.Frias - 16/10/2006 - Fin *******************

				//cuando se pulsa alguna medida se tiene que proteger la opcion de horaria
				rbTipoGrafica[1].IsChecked = false;
				rbTipoGrafica[1].Enabled = false;
				//y desproteger el hasta de la grafica
				Label2.Enabled = true;
				sdcfechahasta.Enabled = true;
				activar_aceptar();
			}
		}

		private void crear_base_de_datos_temporal()
		{
			object dbLangSpanish = null;
			object DBEngine = null;
			string sqltemporal = String.Empty;
			string stPathTemp = String.Empty;
			bool bBase = false;
			//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			try
			{
                //INIC SDSALAZAR TODO SPRIN 5_5
                //stPathTemp = MGRAFICA.stPathAplicacion + "\\" + MGRAFICA.stNombreBD;
                //bBase = false;
                //if (FileSystem.Dir(stPathTemp, FileAttribute.Normal) != "")
                //{
                //	bBase = true;
                //}
                //if (!bBase)
                //{ // SI est� dado
                //	//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //	MGRAFICA.bs = (Database) DBEngine.Workspaces(0).CreateDatabase(stPathTemp, dbLangSpanish);
                //}
                //else
                //{
                //	//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //	MGRAFICA.bs = (Database) DBEngine.Workspaces(0).OpenDatabase(stPathTemp);
                //	//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //	foreach (TableDef TABLA in MGRAFICA.bs.TableDefs)
                //	{
                //		//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //		if (Convert.ToString(TABLA.Name) == "graficas")
                //		{
                //			//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //			MGRAFICA.bs.Execute("DROP TABLE graficas");
                //			break;
                //		}
                //	}
                //}
                ////UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MGRAFICA.bs.Execute("CREATE TABLE graficas (intervalo datetime, dato double)");
                //sqltemporal = "select * from graficas  where 1=2";
                ////UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MGRAFICA.Rs = (Recordset) MGRAFICA.bs.OpenRecordset(sqltemporal);
                //INIC SDSALAZAR TODO SPRIN 5_5
            }
            catch (Exception Exep)
			{
				Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "crear_base_de_datos_temporal:HOJA_SEGUIMIENTO", Exep);
                //INIC SDSALAZAR TODO SPRIN 5_5
    //            MGRAFICA.bs = null;
				//MGRAFICA.Rs = null;
                //INIC SDSALAZAR TODO SPRIN 5_5
            }
        }


		private void imprimir_grafica()
		{
            //INIC SDSALAZAR SPRINT X_5
            //try
            //{
            //	//Set cryInformes = Listado_general
            //	cryInformes = MGRAFICA.oCrystal;

            //	//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.ReportFileName = MGRAFICA.PathReport + "\\EGI240R2.rpt";
            //	//**************Comprueba si tiene datos sobre las cabeceras******************
            //	if (!MGRAFICA.VstrCrystal.VfCabecera)
            //	{
            //		MGRAFICA.proCabCrystal();
            //	}
            //	//****************************************************************************
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Formulas["FORM1"] = "\"" + MGRAFICA.VstrCrystal.VstGrupoHo + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Formulas["FORM2"] = "\"" + MGRAFICA.VstrCrystal.VstNombreHo + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Formulas["FORM3"] = "\"" + MGRAFICA.VstrCrystal.VstDireccHo + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Formulas["fecha_desde"] = "\"" + this.sdcfechadesde.Text + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Formulas["fecha_hasta"] = "\"" + this.sdcfechahasta.Text + "\"";
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Formulas["paciente"] = "\"" + MGRAFICA.proObtenerPaciente(MGRAFICA.vstA�oAdmi, MGRAFICA.vstNumAdmi) + "\"";
            //	switch(campo)
            //	{
            //		case "CPESOPAC" : 
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.Formulas["TITULO"] = "\"" + "GR�FICA DE PESO" + "\""; 
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.Formulas["NRPT"] = "\"" + "EGI240R2" + "\""; 
            //			//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.WindowTitle = "Gr�fica de peso"; 
            //			break;
            //		case "ctallapac" : 
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.Formulas["TITULO"] = "\"" + "GR�FICA DE TALLA" + "\""; 
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.Formulas["NRPT"] = "\"" + "EGI240R3" + "\""; 
            //			//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.WindowTitle = "Gr�fica de talla"; 
            //			break;
            //		case "cpercran" : 
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.Formulas["TITULO"] = "\"" + "GR�FICA DE PERIMETRO CRANEAL" + "\""; 
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.Formulas["NRPT"] = "\"" + "EGI240R4" + "\""; 
            //			//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.WindowTitle = "Gr�fica de perimetro craneal"; 
            //			break;
            //		case "cperabdo" : 
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.Formulas["TITULO"] = "\"" + "GR�FICA DE PERIMETRO ABDOMINAL" + "\""; 
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.Formulas["NRPT"] = "\"" + "EGI240R5" + "\""; 
            //			//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx 
            //			cryInformes.WindowTitle = "Gr�fica de perimetro abdominal"; 
            //			break;
            //	}
            //	//UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.DataFiles[0] = MGRAFICA.stPathAplicacion + "\\" + MGRAFICA.stNombreBD;
            //	//cryInformes.Connect = "DSN=" & stDSN_ACCESS & ""
            //	//cryInformes.SQLQuery = " SELECT * From graficas"
            //	//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Destination = 0; //1 'crptToWindow
            //	//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.WindowState = (int) FormWindowState.Maximized;
            //	//RegistrarDsnAccess (stNombreBD)
            //	//cryInformes.DataFiles(0) = stPathAplicacion & "\" & stNombreBD
            //	//cryInformes.Connect = "DSN=" & stDSN_ACCESS & ""
            //	//cryInformes.ReportFileName = PathReport & "\EGI240R2.rpt"
            //	//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Action = 1;
            //	//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.PageCount();
            //	//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.PrinterStopPage = cryInformes.PageCount;
            //	tiForm = 7;
            //	MGRAFICA.vacia_formulas(cryInformes, tiForm); //limpio las formulas
            //	//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Reset();
            //	cryInformes = null;
            //}
            //catch
            //{
            //	Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "crear_base_de_datos_temporal:HOJA_SEGUIMIENTO", MGRAFICA.RcEnfermeria);
            //	//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.PageCount();
            //	//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.PrinterStopPage = cryInformes.PageCount;
            //	tiForm = 7;
            //	MGRAFICA.vacia_formulas(cryInformes, tiForm); //limpio las formulas
            //	//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	cryInformes.Reset();
            //	cryInformes = null;
            //}
            //INIC SDSALAZAR SPRINT X_5
        }


        //****************************************************************************************
        //*  Procedimiento: Form_Load                                                            *
        //*                                                                                      *
        //*  Modificaciones:                                                                     *
        //*                                                                                      *
        //*    O.Frias (16/10/2006)                                                              *
        //*                                                                                      *
        //*      - Compruebo el valor de check(6) para activar el boton de aceptar o no          *
        //*                                                                                      *
        //****************************************************************************************
        private void activar_aceptar()
		{

			//************* O.Frias - 16/10/2006 - Inicio ****************
			if (optmedidas[0].IsChecked || optmedidas[1].IsChecked || optmedidas[2].IsChecked || optmedidas[3].IsChecked || Check1.CheckState == CheckState.Checked || Check2.CheckState == CheckState.Checked || Check3.CheckState == CheckState.Checked || Check4.CheckState == CheckState.Checked || Check5.CheckState == CheckState.Checked || Check6.CheckState == CheckState.Checked)
			{
				//************** O.Frias - 16/10/2006 - Fin ******************
				if (sdcfechadesde.Text != "" && sdcfechahasta.Text != "")
				{

					//oscar 11/05/04
					//cbAceptar.Enabled = True
					if (MGRAFICA.bMasivo)
					{
						if (cbDesde.SelectedIndex == -1 || cbHasta.SelectedIndex == -1)
						{
							cbAceptar.Enabled = false;
							cbImprimir.Enabled = false;
						}
						else
						{
							cbAceptar.Enabled = true;
							cbImprimir.Enabled = true;
						}
					}
					else
					{
						cbAceptar.Enabled = true;
					}
					//--------

				}
				else
				{
					cbAceptar.Enabled = false;
					cbImprimir.Enabled = false;
				}
			}
			else
			{
				cbAceptar.Enabled = false;
				cbImprimir.Enabled = false;
			}
		}
		private void REALIZAR_Grafica()
		{
			object DBEngine = null;
			string sql = String.Empty;
			DataSet RrSql = null;
			try
			{

				//crear_base_de_datos_temporal
				System.DateTime TempDate = DateTime.FromOADate(0);
				sql = "select A.fapuntes, " + campo + " from emedidas A " + 
				      " Where A.itiposer = '" + MGRAFICA.stTipSer + "' and " + 
				      " A.ganoregi = " + MGRAFICA.vstA�oAdmi + " and A.gnumregi = " + MGRAFICA.vstNumAdmi + " and " + campo + " IS NOT NULL AND " + 
				      " A.fapuntes >=  '" + DateTime.Parse(sdcfechadesde.Text).ToString("MM/dd/yyyy") + "' and " + 
				      " A.fapuntes <= ' " + ((DateTime.TryParse(sdcfechahasta.Text + " 23:59:59", out TempDate)) ? TempDate.ToString("MM/dd/yyyy HH:mm:ss") : sdcfechahasta.Text + " 23:59:59") + "' and " + 
				      " A.fapuntes in " + 
				      " (select max(emedidas.fapuntes) from emedidas where " + 
				      " convert (char(11), A.fapuntes) = convert (char(11), emedidas.fapuntes) and " + 
				      " A.itiposer = emedidas.itiposer and " + 
				      " A.ganoregi = emedidas.ganoregi and " + 
				      " A.gnumregi = emedidas.gnumregi and " + 
				      " emedidas." + campo + " is not null) " + 
				      " Order By A.fapuntes ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MGRAFICA.RcEnfermeria);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				int veces_a_imprimir = 0;
				int registros_a_imprimir = 0;
				int ultimos_registros_a_imprimir = 0;
				string v1 = String.Empty;
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					if (RrSql.Tables[0].Rows.Count > 25)
					{
						veces_a_imprimir = 1;

						while(true)
						{
							if (Conversion.Val((RrSql.Tables[0].Rows.Count / ((int) veces_a_imprimir)).ToString()) < 25)
							{
								break;
							}
							veces_a_imprimir++;
						};
						registros_a_imprimir = Convert.ToInt32(Conversion.Val((RrSql.Tables[0].Rows.Count / ((int) veces_a_imprimir)).ToString()));
						ultimos_registros_a_imprimir = (RrSql.Tables[0].Rows.Count % veces_a_imprimir) + registros_a_imprimir;

						for (int NUMERO_DE_INFORMES = 1; NUMERO_DE_INFORMES <= veces_a_imprimir; NUMERO_DE_INFORMES++)
						{
							if (NUMERO_DE_INFORMES == veces_a_imprimir)
							{
								crear_base_de_datos_temporal();
								for (int i = 1; i <= ultimos_registros_a_imprimir; i++)
								{
                                    //INIC SDSALAZAR TODO SPRIN 5_5
         //                           //UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
         //                           MGRAFICA.Rs.AddNew();
									////UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(intervalo). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									////UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//MGRAFICA.Rs["intervalo"] = (Recordset) RrSql.Tables[0].Rows[0]["fapuntes"];
									//if (campo == "CPESOPAC")
									//{
									//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dato). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//	//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//	MGRAFICA.Rs["dato"] = (Recordset) StringsHelper.Format(Convert.ToDouble(RrSql.Tables[0].Rows[0]["" + campo + ""]) / 1000, ".00");
									//}
									//else
									//{
									//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dato). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//	//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
									//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//	MGRAFICA.Rs["dato"] = (Recordset) RrSql.Tables[0].Rows[0]["" + campo + ""];
									//}
									////UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
									//MGRAFICA.Rs.Update();
									////UPGRADE_ISSUE: (1040) MoveNext function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
									//RrSql.MoveNext();
                                    //INIC SDSALAZAR TODO SPRIN 5_5
                                }
                            }
							else
							{
								crear_base_de_datos_temporal();
								for (int i = 1; i <= registros_a_imprimir; i++)
								{
                                    //INIC SDSALAZAR TODO SPRIN 5_5
                                    //                           //UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                                    //                           MGRAFICA.Rs.AddNew();
                                    ////UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(intervalo). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                                    ////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                                    ////UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                                    //MGRAFICA.Rs["intervalo"] = (Recordset) RrSql.Tables[0].Rows[0]["fapuntes"];
                                    //if (campo == "CPESOPAC")
                                    //{
                                    //	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dato). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                                    //	//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                                    //	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                                    //	MGRAFICA.Rs["dato"] = (Recordset) StringsHelper.Format(Convert.ToDouble(RrSql.Tables[0].Rows[0]["" + campo + ""]) / 1000, ".00");
                                    //}
                                    //else
                                    //{
                                    //	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dato). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                                    //	//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                                    //	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                                    //	MGRAFICA.Rs["dato"] = (Recordset) RrSql.Tables[0].Rows[0]["" + campo + ""];
                                    //}
                                    ////UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                                    //MGRAFICA.Rs.Update();
                                    ////UPGRADE_ISSUE: (1040) MoveNext function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
                                    //RrSql.MoveNext();
                                    //INIC SDSALAZAR TODO SPRIN 5_5
                                }
                            }
							imprimir_grafica();
						}
					}
					else
					{
						crear_base_de_datos_temporal();
						foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
						{
                            //INIC SDSALAZAR TODO SPRIN 5_5
       //                     //UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
       //                     MGRAFICA.Rs.AddNew();
							////UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(intervalo). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							////UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//MGRAFICA.Rs["intervalo"] = (Recordset) iteration_row["fapuntes"];
							//if (campo == "CPESOPAC")
							//{
							//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dato). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//	MGRAFICA.Rs["dato"] = (Recordset) StringsHelper.Format(Convert.ToDouble(iteration_row["" + campo + ""]) / 1000, ".00");
							//}
							//else
							//{
							//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dato). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//	//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//	MGRAFICA.Rs["dato"] = (Recordset) iteration_row["" + campo + ""];
							//}
							////UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							//MGRAFICA.Rs.Update();
                            //INIC SDSALAZAR TODO SPRIN 5_5
                        }
                        imprimir_grafica();
					}
                    RrSql.Close();

                    //INIC SDSALAZAR TODO SPRIN 5_5
                    //UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //MGRAFICA.Rs.Close();
					//UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					//MGRAFICA.bs.Close();
                    //UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    
                    //DBEngine.Workspaces(0).Close();
                    //INIC SDSALAZAR TODO SPRIN 5_5
                }
                else
				{
					for (int i = 0; i <= 3; i++)
					{
						if (optmedidas[i].IsChecked)
						{
							v1 = optmedidas[i].Text;
						}
					}
					short tempRefParam = 1520;
					string[] tempRefParam2 = new string[]{v1, "el " + Label4.Text};
					MGRAFICA.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, MGRAFICA.RcEnfermeria, tempRefParam2);
					//MsgBox "El paciente no tiene datos que mostrar"
				}
			}
			catch(Exception Exep)
			{
				Serrores.AnalizaError("Enfermeria", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "crear_base_de_datos_temporal:HOJA_SEGUIMIENTO", Exep);
                //INIC SDSALAZAR TODO SPRIN 5_5
    //            MGRAFICA.Rs = null;
				//MGRAFICA.bs = null;
                //INIC SDSALAZAR TODO SPRIN 5_5
            }
        }

		private void rbTipoGrafica_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.rbTipoGrafica, eventSender);
				switch(Index)
				{
					case 0 : 
						if (rbTipoGrafica[0].IsChecked)
						{
							sdcfechadesde.Enabled = true;
							Label2.Enabled = true;
							sdcfechahasta.Enabled = true;
							sdcfechadesde.Value = DateTime.Today.AddDays(-6);
						} 
						break;
					case 1 : 
						if (rbTipoGrafica[1].IsChecked)
						{
							Label2.Enabled = false;
							sdcfechahasta.Enabled = false;
							sdcfechadesde.Enabled = true;
							sdcfechadesde.Value = DateTime.Today;
						} 
						break;
				}

			}
		}

		//oscar 11/05/04
		private void Cargar_Camas_Ocupadas()
		{
			string tstsql = String.Empty;
			string tstCama = String.Empty;
			DataSet tRsCamas = null; //Valor del estado de cama ocupada
			try
			{
				cbDesde.Items.Clear();
				cbHasta.Items.Clear();

				//Obtener las camas ocupadas, asociadas a la unidad de enfermer�a
				tstsql = "Select DCAMASBO.gplantas, DCAMASBO.ghabitac, DCAMASBO.gcamasbo ";
				tstsql = tstsql + "From DCAMASBO, AEPISADM Where ";
				tstsql = tstsql + "AEPISADM.gidenpac = DCAMASBO.gidenpac AND ";
				tstsql = tstsql + "AEPISADM.fllegada Is Not Null AND ";
				tstsql = tstsql + "AEPISADM.faltplan is NULL and ";
				tstsql = tstsql + "AEPISADM.fingplan Is Not Null AND ";
				tstsql = tstsql + "DCAMASBO.iestcama = 'O' AND ";
				tstsql = tstsql + "DCAMASBO.gcounien = '" + MGRAFICA.stUnidadEnf + "' ";
				tstsql = tstsql + "ORDER BY DCAMASBO.gplantas, DCAMASBO.ghabitac, DCAMASBO.gcamasbo";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstsql, MGRAFICA.RcEnfermeria);
				tRsCamas = new DataSet();
				tempAdapter.Fill(tRsCamas);
				if (tRsCamas.Tables[0].Rows.Count != 0)
				{
					tRsCamas.MoveFirst();
					int tempForVar = tRsCamas.Tables[0].Rows.Count;
					for (int i = 1; i <= tempForVar; i++)
					{
						tstCama = StringsHelper.Format(tRsCamas.Tables[0].Rows[0]["gplantas"], "00");
						tstCama = tstCama + StringsHelper.Format(tRsCamas.Tables[0].Rows[0]["ghabitac"], "00");
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE-WARNING: It was not possible to determine if this addition expression should use an addition operator (+) or a concatenation operator (&)
						tstCama = Convert.ToString(Double.Parse(tstCama) + Convert.ToDouble(tRsCamas.Tables[0].Rows[0]["gcamasbo"]));
						cbDesde.Items.Add(tstCama);
						tRsCamas.MoveNext();
					}
				}
                tRsCamas.Close();
			}
			catch(Exception Exep)
			{
				Serrores.AnalizaError("GRAFICADLL", Path.GetDirectoryName(Application.ExecutablePath), MGRAFICA.gUsuario, "Cargar_Camas_Ocupadas:PROGRAMACION_HORARIA", Exep);
			}
		}

		private string proObtenerUnidadEnf(string unidad)
		{
			string result = String.Empty;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select dunidenf from dunienfe where gunidenf='" + MGRAFICA.stUnidadEnf + "'", MGRAFICA.RcEnfermeria);
			DataSet r = new DataSet();
			tempAdapter.Fill(r);
			result = "";
			if (r.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(r.Tables[0].Rows[0][0]).Trim().ToUpper();
			}
			return result;
		}

		private void proRepetirGraficas(string Pdestino)
		{
			GRAFICA_SH formuGD = null;
			GHORARIA_SH formuGH = null;
			//En teoria:
			//    - Abrir un recordset con todos los pacientes ingresados en las camas especificadas
			//    - Ir modificando el episodio
			//    - Invocar a las pantalla de graficas (horaria / diaria)
			//    - Invocar el boton "imprimir grafica activa" / "imprimir grafica completa"
			//    - descargar la pantalla correspondiente

			this.Cursor = Cursors.WaitCursor;
			//Misma Select que proObtenerQueryAdmisionUnidad de Estado Situacion de pacientes
			//pero restringida a que las camas deberan estar en el rango de camas seleccionado.

			string sqldatos = "SELECT";
			sqldatos = sqldatos + " AEPISADM.ganoadme, AEPISADM.gnumadme";
			sqldatos = sqldatos + " From";
			sqldatos = sqldatos + " AEPISADM , DCAMASBO, DPACIENT";
			sqldatos = sqldatos + " Where";
			sqldatos = sqldatos + " AEPISADM.gidenpac = DCAMASBO.gidenpac AND";
			sqldatos = sqldatos + " AEPISADM.gidenpac = DPACIENT.gidenpac AND";
			sqldatos = sqldatos + " AEPISADM.fllegada Is Not Null AND";
			sqldatos = sqldatos + " AEPISADM.faltplan is NULL and";
			sqldatos = sqldatos + " AEPISADM.fingplan is not NULL and";
			sqldatos = sqldatos + " DCAMASBO.gcounien = '" + MGRAFICA.stUnidadEnf + "' AND";
			sqldatos = sqldatos + " DCAMASBO.iestcama ='O' AND";
			sqldatos = sqldatos + " REPLACE(STR(GPLANTAS,2),' ' ,'0')+" + 
			           " REPLACE(STR(GHABITAC,2),' ' ,'0')+ GCAMASBO between " + 
			           " '" + cbDesde.GetListItem(cbDesde.SelectedIndex) + "' AND '" + cbHasta.GetListItem(cbHasta.SelectedIndex) + "'";
			if (OptOrdenado[0].IsChecked)
			{
				sqldatos = sqldatos + " order by DCAMASBO.gplantas, DCAMASBO.ghabitac, DCAMASBO.gcamasbo";
			}
			if (OptOrdenado[1].IsChecked)
			{
				sqldatos = sqldatos + " order by DPACIENT.DAPE1PAC,DPACIENT.DAPE2PAC,DPACIENT.dnombpac";
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqldatos, MGRAFICA.RcEnfermeria);
			DataSet RGRafica = new DataSet();
			tempAdapter.Fill(RGRafica);
			if (RGRafica.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in RGRafica.Tables[0].Rows)
				{

					if (rbTipoGrafica[0].IsChecked)
					{
						if (Pdestino == "I")
						{
							GRAFICA_SH.DefInstance.ganoadme = Convert.ToInt32(iteration_row["ganoadme"]);
							GRAFICA_SH.DefInstance.gnumadme = Convert.ToInt32(iteration_row["gnumadme"]);
							GRAFICA_SH.DefInstance.Show();
							//Se imprime la grafica completa
							GRAFICA_SH.DefInstance.Command3_Click(GRAFICA_SH.DefInstance.Command3, new EventArgs());
							GRAFICA_SH.DefInstance.Close();
						}
						else
						{
							formuGD = new GRAFICA_SH();
							formuGD.ganoadme = Convert.ToInt32(iteration_row["ganoadme"]);
							formuGD.gnumadme = Convert.ToInt32(iteration_row["gnumadme"]);
							formuGD.Show();
						}
					}
					else
					{
						if (Pdestino == "I")
						{
							GHORARIA_SH.DefInstance.ganoadme = Convert.ToInt32(iteration_row["ganoadme"]);
							GHORARIA_SH.DefInstance.gnumadme = Convert.ToInt32(iteration_row["gnumadme"]);
							GHORARIA_SH.DefInstance.Show();
							//Se imprime la grafica completa
							GHORARIA_SH.DefInstance.Command3_Click(GHORARIA_SH.DefInstance.Command3, new EventArgs());
							GHORARIA_SH.DefInstance.Close();
						}
						else
						{
							formuGH = new GHORARIA_SH();
							formuGH.ganoadme = Convert.ToInt32(iteration_row["ganoadme"]);
							formuGH.gnumadme = Convert.ToInt32(iteration_row["gnumadme"]);
							formuGH.Show();
						}
					}

				}
			}
			this.Cursor = Cursors.Arrow;
		}

		private void cbDesde_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			string tstCama = String.Empty;

			cbHasta.Items.Clear();
			cbHasta.SelectedIndex = -1;
			for (int i = cbDesde.SelectedIndex; i <= cbDesde.Items.Count - 1; i++)
			{
				cbHasta.Items.Add(cbDesde.GetListItem(i));
			}
			cbHasta.Enabled = true;
			activar_aceptar();
		}

		private void cbHasta_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			activar_aceptar();
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
			if (!rbTipoGrafica[0].IsChecked && !rbTipoGrafica[1].IsChecked)
			{
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{"Tipo de gr�fica"};
				MGRAFICA.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, MGRAFICA.RcEnfermeria, tempRefParam2);
				rbTipoGrafica[0].Focus();
				return;
			}

			if ((DateTime.Parse(sdcfechahasta.Text) < DateTime.Parse(sdcfechadesde.Text)) && rbTipoGrafica[0].IsChecked)
			{
				cbAceptar.Enabled = false;
				short tempRefParam3 = 1020;
				string[] tempRefParam4 = new string[]{"Fecha hasta", ">", "fecha desde"};
				MGRAFICA.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, MGRAFICA.RcEnfermeria, tempRefParam4);
				sdcfechahasta.Focus();
			}
			else
			{
				proRepetirGraficas("I");
			}

		}

		//UPGRADE_NOTE: (7001) The following declaration (chkGraficaActiva_Click) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void chkGraficaActiva_Click()
		//{
				//activar_aceptar();
		//}

		//UPGRADE_NOTE: (7001) The following declaration (chkGraficaCompleta_Click) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void chkGraficaCompleta_Click()
		//{
				//activar_aceptar();
		//}

		private void HOJA_SEGUIMIENTO_Closed(Object eventSender, EventArgs eventArgs)
		{
			//Dim intgh As Integer
			//Dim intgd As Integer
			//Dim respuesta As Long
			if (MGRAFICA.bMasivo)
			{
				//    For Each FPantalla In Forms
				//        Select Case UCase(FPantalla.Name)
				//            Case "GRAFICA": intgd = intgd + 1
				//            Case "GHORARIA": intgh = intgh + 1
				//        End Select
				//    Next
				//    If intgh + intgd > 0 Then
				//        respuesta = MsgBox("Existen " & CStr(intgd) & " graficas diarias y " & CStr(intgh) & _
				//'                         " graficas horarias mostradas en la barra de tareas." & vbCrLf & vbCrLf & _
				//'                         " � Desea cerrar todas las ventanas de gr�ficas ?", vbYesNo + vbQuestion, vNomAplicacion)
				//        If respuesta = vbYes Then
				foreach (Form FPantalla in Application.OpenForms)
				{
					if (FPantalla.Name.ToUpper() == "GRAFICA_SH" || FPantalla.Name.ToUpper() == "GHORARIA_SH")
					{
						FPantalla.Close();
					}
				}
				//        End If
				//    End If

				MGRAFICA.objFormulario.Enabled = true;
				MGRAFICA.objFormulario.Show();
				//UPGRADE_NOTE: (1029) Object objFormulario may not be destroyed until it is garbage collected. More Information: http://www.vbtonet.com/ewis/ewi1029.aspx
				MGRAFICA.objFormulario = null;
			}
		}
		//-----------------
	}
}