using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace LlamarOperacionesFMS
{
	public class clsOperFMS
	{



		const string vstTipoBD = "SQLSERVER";
		const string StProgAccion = "P";
		const string stNoper = "";

		// TipoOperacion = "CONSUMOS"
		//                  "PEDIDOS"
		//                  "FACTURA"
		//                  "FACTURASOMBRA"
		//                  "DEPOSITO"
		//                  "CONSUMOSCONSULTAS"
		//                  "CONSUMOSARCHIVO"
		//                  "CONTRATO" 
		//                  "DEUDASPACIENTE"
		public void LlamarOperacionesFMS(string CodUsuarioAplicacion, SqlConnection Conexion, object Formulario, string TipoOperacion, string IdPaciente = "", string Ape1Pac = "", object Ape2Pac = null, string Itiposer = "", string AnoEpisodio = "", string NumEpisodio = "", string SerEpisodio = "", object UnidadEnfermeria = null, string stGsocieda = "", object nTran = null)
		{
			//CURIAE: A REVISAR POR EL EQUIPO DE MIGRACION --> NUEVA FORMA DE CONECTARSE A LAS OPERACIONES DE IFMS

			//'Dim aAtri() As String
			//'Dim avalores() As Variant
			//'Dim ClaseOperacion As Object
			//'Dim stOperacion As String
			//'Dim CodEmpresa  As String
			//'Dim CodDpto As String
			//'Dim NumeroValfanu As Long
			//'Dim sqlOperacion As String
			//'Dim RrOperacion As rdoResultset
			//'Dim stSubOperacion As String   ' PARA OBTENER EL CODIGO DE DEPARTAMENTO Y DE EMPRESA
			//'Dim stTipoaccion As String
			//'Dim bEsModal As Boolean
			//'Dim LlamdaFapry As String
			//'Dim MandarEmpreDpto As Boolean
			//'
			//'
			//'stTipoaccion = ""
			//'bEsModal = False
			//'Set ClaseOperacion = CreateObject("Proyecto1.Classexegenerador")
			//'Select Case UCase(TipoOperacion)
			//'Case "PEDIDOS"
			//'
			//'        ReDim aAtri(1)
			//'        ReDim avalores(1, 1)
			//'        aAtri(1) = "IDENTIPAC"
			//'        avalores(1, 1) = ""
			//'        stOperacion = "OPERALMA"
			//'        NumeroValfanu = 1
			//'        stSubOperacion = "FPEDPL"
			//'
			//'Case "PEDIDOS2"
			//'
			//'        ReDim aAtri(1)
			//'        ReDim avalores(1, 1)
			//'        aAtri(1) = "IDENTIPAC"
			//'        avalores(1, 1) = ""
			//'        stOperacion = "OPERALMA"
			//'        NumeroValfanu = 2
			//'        stSubOperacion = "FPEDPL"
			//'
			//'Case "FACTURA"
			//'
			//'        ReDim aAtri(1 To 8)
			//'        ReDim avalores(1, 8)
			//'        aAtri(1) = "DAPE1PAC"
			//'        aAtri(2) = "ID"
			//'
			//'        aAtri(3) = "ITIPOSER"
			//'        aAtri(4) = "GANOREGI"
			//'        aAtri(5) = "GNUMREGI"
			//'        aAtri(6) = "GIDENPAC"
			//'        aAtri(7) = "GSERVICI"
			//'        aAtri(8) = "IMDH"
			//'
			//'        avalores(1, 1) = Ape1Pac
			//'        avalores(1, 2) = IdPaciente
			//'
			//'        avalores(1, 3) = Itiposer
			//'        avalores(1, 4) = AnoEpisodio
			//'        avalores(1, 5) = NumEpisodio
			//'        avalores(1, 6) = IdPaciente
			//'        avalores(1, 7) = SerEpisodio
			//'        avalores(1, 8) = "S"
			//'
			//'        stOperacion = "OPERFACT"
			//'        stSubOperacion = "FFACTPRI"
			//'        NumeroValfanu = 1
			//'
			//'Case "FACTURASOMBRA"
			//'
			//'        ReDim aAtri(1 To 6)
			//'        ReDim avalores(1, 6)
			//'        stOperacion = "OPERFACT"
			//'        aAtri(1) = "ID"
			//'
			//'        aAtri(2) = "ITIPOSER"
			//'        aAtri(3) = "GANOREGI"
			//'        aAtri(4) = "GNUMREGI"
			//'        aAtri(5) = "GIDENPAC"
			//'        aAtri(6) = "GSERVICI"
			//'
			//'        avalores(1, 1) = IdPaciente
			//'        avalores(1, 2) = Itiposer
			//'        avalores(1, 3) = AnoEpisodio
			//'        avalores(1, 4) = NumEpisodio
			//'        avalores(1, 5) = IdPaciente
			//'        avalores(1, 6) = SerEpisodio
			//'
			//'
			//'        stSubOperacion = "FFACTPRI"
			//'        NumeroValfanu = 2
			//'
			//'Case "DEPOSITO"
			//'
			//'        ReDim aAtri(1 To 7)
			//'        ReDim avalores(1, 7)
			//'        aAtri(1) = "PA"
			//'        aAtri(2) = "EXPACIENTE"
			//'        aAtri(3) = "ITIPOSER"
			//'        aAtri(4) = "GANOREGI"
			//'        aAtri(5) = "GNUMREGI"
			//'        aAtri(6) = "GIDENPAC"
			//'        aAtri(7) = "GSERVICI"
			//'
			//'        avalores(1, 1) = Ape1Pac
			//'        avalores(1, 2) = IdPaciente
			//'        avalores(1, 3) = Itiposer
			//'        avalores(1, 4) = AnoEpisodio
			//'        avalores(1, 5) = NumEpisodio
			//'        avalores(1, 6) = IdPaciente
			//'        avalores(1, 7) = SerEpisodio
			//'
			//'        If Not IsMissing(nTran) Then
			//'            ReDim Preserve aAtri(1 To 8)
			//'            ReDim Preserve avalores(1, 8)
			//'            aAtri(8) = "NTRAN"
			//'            avalores(1, 8) = nTran
			//'        End If
			//'
			//'        stOperacion = "OPERFACT"
			//'        stSubOperacion = "FFACTPRI"
			//'        NumeroValfanu = 3
			//'        bEsModal = True
			//'
			//'Case "CONSUMOS"
			//'
			//'        ReDim aAtri(1)
			//'        ReDim avalores(1, 1)
			//'        aAtri(1) = "CE"
			//'        avalores(1, 1) = UnidadEnfermeria
			//'        stOperacion = "OPERALMA"
			//'        NumeroValfanu = 2
			//'
			//'Case "PRIVADOBENEFICA"
			//'        ReDim aAtri(1 To 2)
			//'        ReDim avalores(1, 2)
			//'        aAtri(1) = "DAPE1PAC"
			//'        aAtri(2) = "ID"
			//'        avalores(1, 1) = Ape1Pac
			//'        avalores(1, 2) = IdPaciente
			//'        stOperacion = "OPERBENE"
			//'        stSubOperacion = "FFACTPRI"
			//'        NumeroValfanu = 1
			//'
			//'Case "CONSUMOSCONSULTAS"
			//'
			//'        ReDim aAtri(1)
			//'        ReDim avalores(1, 1)
			//'        aAtri(1) = "CE"
			//'        avalores(1, 1) = ""
			//'        stOperacion = "OPERCOAR"
			//'        NumeroValfanu = 1
			//'
			//'Case "CONSUMOSARCHIVO"
			//'
			//'        ReDim aAtri(1)
			//'        ReDim avalores(1, 1)
			//'        aAtri(1) = "CE"
			//'        avalores(1, 1) = ""
			//'        stOperacion = "OPERCOAR"
			//'        NumeroValfanu = 2
			//'
			//'Case "CONTRATO"
			//'        ReDim aAtri(4)
			//'        ReDim avalores(1, 4)
			//'        aAtri(1) = "DAPE1PAC"
			//'        aAtri(2) = "ID"
			//'        aAtri(3) = "GIDENPAC"
			//'        aAtri(4) = "DAPE2PAC"
			//'        avalores(1, 1) = Ape1Pac
			//'        avalores(1, 2) = IdPaciente
			//'        avalores(1, 3) = IdPaciente
			//'        avalores(1, 4) = Ape2Pac
			//'        stOperacion = "CONTRATO"
			//'        NumeroValfanu = 2
			//'
			//'        If IsNull(ObtenerCodigoSConsglo(stOperacion, "valfanu3", Conexion)) Then
			//'            stTipoaccion = ""
			//'        Else
			//'            stTipoaccion = ObtenerCodigoSConsglo(stOperacion, "valfanu3", Conexion)
			//'        End If
			//'
			//''oscar C 28/10/04
			//'Case "DEUDASPACIENTE"
			//'        ReDim aAtri(1)
			//'        ReDim avalores(1, 1)
			//'        aAtri(1) = "GIDENPAC"
			//'
			//'        avalores(1, 1) = IdPaciente
			//'
			//'        stOperacion = "CANTBLOQ"
			//'        NumeroValfanu = 2
			//'        stSubOperacion = "FFACTPRI"
			//'
			//''---------------------
			//'Case "PRESUDEP"
			//'
			//'        ReDim aAtri(6)
			//'        ReDim avalores(1, 6)
			//'        Dim stInspeccion As String
			//'
			//'        stInspeccion = ObtenerInspeccion(IdPaciente, stGsocieda, Conexion)
			//'
			//'        aAtri(1) = "GIDENPAC"
			//'        aAtri(2) = "ID"
			//'        aAtri(3) = "GSOCIEDA"
			//'        aAtri(4) = "DAPE1PAC"
			//'        aAtri(5) = "DAPE2PAC"
			//'        aAtri(6) = "GINSPECC"
			//'
			//'        avalores(1, 1) = IdPaciente
			//'        avalores(1, 2) = IdPaciente
			//'        avalores(1, 3) = stGsocieda
			//'        avalores(1, 4) = Ape1Pac
			//'        avalores(1, 5) = Ape2Pac
			//'        avalores(1, 6) = stInspeccion
			//'
			//'        stOperacion = "PRESUDEP"
			//'        NumeroValfanu = 1
			//'
			//'        stSubOperacion = "FFACTPRI"
			//'        bEsModal = True
			//''---------------------
			//'End Select
			//'
			//'If stSubOperacion <> "" Then
			//'    CodEmpresa = ObtenerCodigoSConsglo(stSubOperacion, "valfanu1", Conexion)
			//'    CodDpto = ObtenerCodigoSConsglo(stSubOperacion, "valfanu2", Conexion)
			//'End If
			//'
			//'stOperacion = ObtenerCodigoSConsglo(stOperacion, "valfanu" & NumeroValfanu, Conexion)
			//'
			//'
			//''si estamos accediendo a la factura de privados y la constante FFACTPRI tiene valor 1 alfanumérico 1 a S
			//'If UCase(TipoOperacion) = "FACTURA" And stSubOperacion = "FFACTPRI" Then
			//'    LlamdaFapry = ObtenerCodigoSConsglo(stSubOperacion, "valfanu1i1", Conexion)
			//'    If LlamdaFapry = "S" Then
			//'        sql = "select top 1 b21_emp, b21_depto from FUSUAB21 where b21_usuar = '" & CodUsuarioAplicacion & "'"
			//'        Set RrOperacion = Conexion.OpenResultset(sql, rdOpenKeyset)
			//'        If Not RrOperacion.EOF Then
			//'            CodEmpresa = RrOperacion("b21_emp")
			//'            CodDpto = RrOperacion("b21_depto")
			//'        End If
			//'        RrOperacion.Close
			//'    End If
			//'End If
			//'
			//'If bEsModal = True Then
			//'    ClaseOperacion.Ejecutar_Operacion Formulario, Trim(stOperacion), aAtri, avalores, Conexion, _
			//''                    vstTipoBD, StProgAccion, stNoper, CodEmpresa, CodDpto, CodUsuarioAplicacion, stTipoaccion, "", , , True
			//'
			//'Else
			//'    ClaseOperacion.Ejecutar_Operacion Formulario, Trim(stOperacion), aAtri, avalores, Conexion, _
			//''                    vstTipoBD, StProgAccion, stNoper, CodEmpresa, CodDpto, CodUsuarioAplicacion, stTipoaccion, ""
			//'End If
		}


		public string ObtenerCodigoSConsglo(string Codigo, string CampoObtener, SqlConnection nConnet)
		{
			string result = String.Empty;
			string sql = "select " + CampoObtener + " from sconsglo where gconsglo='" + Codigo + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, nConnet);
			DataSet RrConstantes = new DataSet();
			tempAdapter.Fill(RrConstantes);
			if (RrConstantes.Tables[0].Rows.Count == 1)
			{
				result = (Convert.ToString(RrConstantes.Tables[0].Rows[0][0]) + "").Trim();
			}
			RrConstantes.Close();
			return result;
		}

		public string ObtenerInspeccion(string stPaciente, object Sociedad, SqlConnection nConnet)
		{

			string result = String.Empty;
			result = "";
			string sql = "select ginspecc from dentipac where gidenpac = '" + stPaciente.Trim() + "' and gsocieda = " + Convert.ToInt32(Sociedad).ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, nConnet);
			DataSet RrInspeccion = new DataSet();
			tempAdapter.Fill(RrInspeccion);
			if (RrInspeccion.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(RrInspeccion.Tables[0].Rows[0]["ginspecc"]) + "";
			}
			RrInspeccion.Close();

			return result;
		}
	}
}