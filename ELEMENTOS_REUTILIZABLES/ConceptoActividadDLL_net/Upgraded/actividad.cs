using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using ElementosCompartidos;
using Microsoft.CSharp;

namespace ConceptoActividad
{
	public partial class BUSCAR_ACTIVIDAD
		: RadForm
	{

		int Fila = 0;
		SqlConnection RcActividad = null;
		private SqlCommand _RqActiv = null;
		public BUSCAR_ACTIVIDAD()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		SqlCommand RqActiv
		{
			get
			{
				if (_RqActiv == null)
				{
					_RqActiv = new SqlCommand();
				}
				return _RqActiv;
			}
			set
			{
				_RqActiv = value;
			}
		}


		string sqlActividad = String.Empty;
		DataSet RrActividad = null; //diagnosticos

		string vstNomBusco = String.Empty;
		string vstCodBusco = String.Empty;
		string vstCondiBusco = String.Empty;
		string vstEconc = String.Empty;
		dynamic nFormulario = null;
		string CodActividad = String.Empty;
		string DescripActividad = String.Empty;
		int vlTope = 0; //maximo n� de filas
		bool vfActi = false; //si es la primera query o posteriores
		bool vfRr = false; //para saber si el Resulset esta abierto
		public void proReferencias(string TipAccion, dynamic nForm, SqlConnection nConnet)
		{
			RqActiv.Connection = nConnet;
			RcActividad = nConnet;
			nFormulario = nForm;
			vstCondiBusco = TipAccion;
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int AltoFila = 0;

			SprActividad.Col = 3;
			DescripActividad = SprActividad.Text;
			SprActividad.Col = 1;
			CodActividad = SprActividad.Text;

			if (vfRr)
			{				
				RrActividad.Close();
			}
			
			nFormulario.RecogerResultadoFinal(CodActividad, DescripActividad);

			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (vfRr)
			{				
				RrActividad.Close();
			}
			this.Close();
		}
		
		private void BUSCAR_ACTIVIDAD_Load(Object eventSender, EventArgs eventArgs)
		{
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			SprActividad.Col = 2;
			SprActividad.SetColHidden(SprActividad.Col, true);

			vstNomBusco = "dconc";
			vstCodBusco = "econc";
			vfRr = false;
		}

		private void SprActividad_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
			int Col = eventArgs.ColumnIndex;
			int Row = eventArgs.RowIndex;
			if (Row > 0)
			{
				SprActividad.Row = Row;
				cbAceptar.Enabled = true;
			}

		}

		/*private void SprActividad_TopLeftChange(object eventSender, UpgradeHelpers.Spread.FpSpread.TopLeftChangeEventArgs eventArgs)
		{
			try
			{
				SprActividad.Row = SprActividad.MaxRows;
				RqActiv.Parameters[0].Value = SprActividad.Text;
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				string tempQuery = RrActividad.Tables[0].TableName;
				RrActividad.Reset();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tempQuery, "");
				tempAdapter.Fill(RrActividad, tempQuery);
			}
			catch (System.Exception excep)
			{
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				MessageBox.Show("error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
			}
		}*/



		private void TbActividad_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				//UPGRADE_ISSUE: (1058) Assignment not supported: KeyAscii to a non-positive constant More Information: http://www.vbtonet.com/ewis/ewi1058.aspx
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

        private void tbBuscar_ButtonClick(Object eventSender, EventArgs eventArgs)
        {
            try
            {
                ToolStripItem Button = (ToolStripItem)eventSender;
                this.Cursor = Cursors.WaitCursor;
                tbEmpiecepor.Text = "";
                string cadena = String.Empty;
                StringBuilder busca = new StringBuilder();
                int corte = 0;
                if (TbActividad.Text != "")
                {
                    busca = new StringBuilder("");
                    cadena = TbActividad.Text;
                    if (vfRr)
                    {
                        //UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrActividad.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        RrActividad.Close();
                    }
                    if (cadena != "")
                    {
                        corte = (cadena.IndexOf(',') + 1);
                        if (corte == 0)
                        {
                            busca = new StringBuilder(vstNomBusco + " like '%" + cadena + "%'");
                        }
                        else
                        {
                            while (corte != 0)
                            {
                                busca = new StringBuilder(vstNomBusco + " like '%" + cadena.Substring(0, Math.Min(corte - 1, cadena.Length)) + "%'");
                                cadena = cadena.Substring(corte, Math.Min(cadena.Length, cadena.Length - corte));
                                corte = (cadena.IndexOf(',') + 1);
                                if (corte != 0)
                                {
                                    if (Rby.IsChecked)
                                    {
                                        busca.Append(" and ");
                                    }
                                    else
                                    {
                                        busca.Append(" or ");
                                    }
                                }
                            }
                            if (Rby.IsChecked)
                            {
                                busca.Append(" and " + vstNomBusco + " like '%" + cadena + "%'");
                            }
                            else
                            {
                                busca.Append(" or " + vstNomBusco + " like '%" + cadena + "%'");
                            }
                        }
                        sqlActividad = "select * from DCOECONCVA" + " where ( " + busca.ToString() + " ) " + " and rconc = '" + vstCondiBusco + "' " + " and dconc>=? ";

                        RqActiv.CommandText = sqlActividad;
                        RqActiv.Parameters[0].Value = "";
                        SqlDataAdapter tempAdapter = new SqlDataAdapter(RqActiv);
                        RrActividad = new DataSet();
                        tempAdapter.Fill(RrActividad);
                        if (RrActividad.Tables[0].Rows.Count != 0)
                        {
                            vfRr = true;
                            SprActividad.MaxRows = RrActividad.Tables[0].Rows.Count;
                            vlTope = RrActividad.Tables[0].Rows.Count;
                            int tempRefParam = 1;
                            carga_grid(ref tempRefParam);
                        }
                        else
                        {
                            vfRr = false;
                            cbAceptar.Enabled = false;
                            //UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrActividad.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                            RrActividad.Close();
                            SprActividad.MaxRows = 0;
                            TbActividad.SelectionStart = 0;
                            TbActividad.SelectionLength = Strings.Len(TbActividad.Text);
                            TbActividad.Focus();
                            this.Cursor = Cursors.Default;
                            return;
                        }
                    }
                }
                this.Cursor = Cursors.Default;
                return;
            }
            catch (SqlException ex)
            {                 
                Serrores.GestionErrorBaseDatos(ex, RcActividad);
            }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
		public void carga_grid(ref int filas)
		{
			try
			{

				if (vfActi)
				{
					vlTope += RrActividad.Tables[0].Rows.Count;
				}
				SprActividad.MaxRows = vlTope;
				SprActividad.Row = filas;
				//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
				RrActividad.MoveFirst();

				foreach (DataRow iteration_row in RrActividad.Tables[0].Rows)
				{

					SprActividad.Row = filas;
					SprActividad.Col = 1;
					SprActividad.Text = Convert.ToString(iteration_row["econc"]);

					SprActividad.Col = 2;
					SprActividad.Text = Convert.ToString(iteration_row["rconc"]);

					SprActividad.Col = 3;
					SprActividad.Text = Convert.ToString(iteration_row["dconc"]);


					filas++;
				}
			}
			catch (SqlException ex)
			{				
				Serrores.GestionErrorBaseDatos(ex, RcActividad);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		private void tbEmpiecepor_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			string stComp = String.Empty;
			string stBusqueda = String.Empty;
			string stSqldiag = String.Empty;
			string tstNomBusco = String.Empty;
			try
			{
				this.Cursor = Cursors.WaitCursor;

				//***
				if (tbEmpiecepor.Text != "")
				{
					TbActividad.Text = "";
					vlTope = 0;
					if (vfRr)
					{
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrActividad.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrActividad.Close();
						SprActividad.MaxRows = 0;
					}
					//dependiendo del valor del radiobuton busca por c�digo o por descripci�n
					if (opCodigo.IsChecked)
					{
						tstNomBusco = vstCodBusco;
					}
					else if (opDescripcion.IsChecked)
					{ 
						tstNomBusco = vstNomBusco;
					}

					if (tbEmpiecepor.Text != "")
					{
						stBusqueda = tbEmpiecepor.Text.Trim();
						stComp = tstNomBusco + " like '" + stBusqueda + "%'";

						sqlActividad = "select * from DCOECONCVA" + " where ( " + stComp + " ) " + " and rconc = '" + vstCondiBusco + "' " + " and dconc>=? ";


						//dependiendo del radio buton ordenara por codigo o codigo,descripcion
						if (opCodigo.IsChecked)
						{
							sqlActividad = sqlActividad + " order by econc ";
						}
						else
						{
							sqlActividad = sqlActividad + " order by dconc ";
						}
						RqActiv.CommandText = sqlActividad;
						RqActiv.Parameters[0].Value = "";
						SqlDataAdapter tempAdapter = new SqlDataAdapter(RqActiv);
						RrActividad = new DataSet();
						tempAdapter.Fill(RrActividad);
						if (RrActividad.Tables[0].Rows.Count != 0)
						{
							SprActividad.MaxRows = RrActividad.Tables[0].Rows.Count;
							vlTope = RrActividad.Tables[0].Rows.Count;
							vfRr = true;
							int tempRefParam = 1;
							carga_grid(ref tempRefParam);
						}
						else
						{
							vfRr = false;
							cbAceptar.Enabled = false;
							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrActividad.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							RrActividad.Close();
							SprActividad.MaxRows = 0;
							tbEmpiecepor.Focus();
							this.Cursor = Cursors.Default;
							return;
						}
					}
					else
					{
						SprActividad.MaxRows = 0;
					}
				}
				this.Cursor = Cursors.Default;
			}
			catch (SqlException ex)
			{
				//****				
				Serrores.GestionErrorBaseDatos(ex, RcActividad);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		private void tbEmpiecepor_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				//UPGRADE_ISSUE: (1058) Assignment not supported: KeyAscii to a non-positive constant More Information: http://www.vbtonet.com/ewis/ewi1058.aspx
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		private void BUSCAR_ACTIVIDAD_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}