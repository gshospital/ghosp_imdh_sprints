using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace ConceptoActividad
{
	public class ConcepActiv
	{

		BUSCAR_ACTIVIDAD nFActiv = null;

		public void proLoad(object nForm, ConcepActiv nConcepActiv, ref SqlConnection nConnet, string nCaso)
		{

			Serrores.oClass = new Mensajes.ClassMensajes();
			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(nConnet);
			Serrores.vAyuda = Serrores.ObtenerFicheroAyuda();
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref nConnet);

			nFActiv = new BUSCAR_ACTIVIDAD();
			nFActiv.proReferencias(nCaso, nForm, nConnet);

			nFActiv.ShowDialog();
		}
	}
}