using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ConceptoActividad
{
	partial class BUSCAR_ACTIVIDAD
	{

		#region "Upgrade Support "
		private static BUSCAR_ACTIVIDAD m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static BUSCAR_ACTIVIDAD DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new BUSCAR_ACTIVIDAD();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprActividad", "_tbBuscar_Button1", "tbBuscar", "TbActividad", "Rby", "RbO", "FrmCriterio", "LbBusqueda", "Frmbuscar", "opDescripcion", "opCodigo", "tbEmpiecepor", "Label1", "Frame1", "cbAceptar", "cbCancelar", "Ilbuscar", "SprActividad_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprActividad;
		private System.Windows.Forms.ToolStripButton _tbBuscar_Button1;
		public System.Windows.Forms.ToolStrip tbBuscar;
		public Telerik.WinControls.UI.RadTextBoxControl TbActividad;
		public Telerik.WinControls.UI.RadRadioButton Rby;
		public Telerik.WinControls.UI.RadRadioButton RbO;
		public Telerik.WinControls.UI.RadGroupBox FrmCriterio;
		public Telerik.WinControls.UI.RadLabel LbBusqueda;
		public Telerik.WinControls.UI.RadGroupBox Frmbuscar;
		public Telerik.WinControls.UI.RadRadioButton opDescripcion;
		public Telerik.WinControls.UI.RadRadioButton opCodigo;
		public Telerik.WinControls.UI.RadTextBoxControl tbEmpiecepor;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public System.Windows.Forms.ImageList Ilbuscar;
		//private FarPoint.Win.Spread.SheetView SprActividad_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BUSCAR_ACTIVIDAD));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.SprActividad = new UpgradeHelpers.Spread.FpSpread();
			this.Frmbuscar = new Telerik.WinControls.UI.RadGroupBox();
			this.tbBuscar = new System.Windows.Forms.ToolStrip();
			this._tbBuscar_Button1 = new System.Windows.Forms.ToolStripButton();
			this.TbActividad = new Telerik.WinControls.UI.RadTextBoxControl();
			this.FrmCriterio = new Telerik.WinControls.UI.RadGroupBox();
			this.Rby = new Telerik.WinControls.UI.RadRadioButton();
			this.RbO = new Telerik.WinControls.UI.RadRadioButton();
			this.LbBusqueda = new Telerik.WinControls.UI.RadLabel();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.opDescripcion = new Telerik.WinControls.UI.RadRadioButton();
			this.opCodigo = new Telerik.WinControls.UI.RadRadioButton();
			this.tbEmpiecepor = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.Ilbuscar = new System.Windows.Forms.ImageList();
			this.Frmbuscar.SuspendLayout();
			this.tbBuscar.SuspendLayout();
			this.FrmCriterio.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// SprActividad
			// 
			this.SprActividad.Location = new System.Drawing.Point(8, 121);
			this.SprActividad.Name = "SprActividad";
			this.SprActividad.Size = new System.Drawing.Size(605, 190);
			this.SprActividad.TabIndex = 0;
			this.SprActividad.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprActividad_CellClick);
			//this.SprActividad.TopLeftChange += new UpgradeHelpers.Spread.FpSpread.TopLeftChangeEventHandler(this.SprActividad_TopLeftChange);
			// 
			// Frmbuscar
			// 
			//this.Frmbuscar.BackColor = System.Drawing.SystemColors.Control;
			this.Frmbuscar.Controls.Add(this.tbBuscar);
			this.Frmbuscar.Controls.Add(this.TbActividad);
			this.Frmbuscar.Controls.Add(this.FrmCriterio);
			this.Frmbuscar.Controls.Add(this.LbBusqueda);
			this.Frmbuscar.Enabled = true;
			//this.Frmbuscar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frmbuscar.Location = new System.Drawing.Point(6, 0);
			this.Frmbuscar.Name = "Frmbuscar";
			this.Frmbuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frmbuscar.Size = new System.Drawing.Size(606, 73);
			this.Frmbuscar.TabIndex = 8;
			this.Frmbuscar.Visible = true;
			// 
			// tbBuscar
			// 
			this.tbBuscar.CanOverflow = false;
			this.tbBuscar.Dock = System.Windows.Forms.DockStyle.None;
			this.tbBuscar.ImageList = Ilbuscar;
			this.tbBuscar.Location = new System.Drawing.Point(572, 38);
			this.tbBuscar.Name = "tbBuscar";
			this.tbBuscar.ShowItemToolTips = true;
			this.tbBuscar.Size = new System.Drawing.Size(26, 26);
			this.tbBuscar.TabIndex = 9;
			this.tbBuscar.Items.Add(this._tbBuscar_Button1);
			// 
			// _tbBuscar_Button1
			// 
			this._tbBuscar_Button1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._tbBuscar_Button1.ImageIndex = 0;
			this._tbBuscar_Button1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._tbBuscar_Button1.Name = "";
			this._tbBuscar_Button1.Size = new System.Drawing.Size(24, 22);
			this._tbBuscar_Button1.Tag = "";
			this._tbBuscar_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._tbBuscar_Button1.ToolTipText = "Buscar";
			this._tbBuscar_Button1.Click += new System.EventHandler(this.tbBuscar_ButtonClick);
			// 
			// TbActividad
			// 
			this.TbActividad.AcceptsReturn = true;
			//this.TbActividad.BackColor = System.Drawing.SystemColors.Window;
			//this.TbActividad.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.TbActividad.Cursor = System.Windows.Forms.Cursors.IBeam;
			//this.TbActividad.ForeColor = System.Drawing.SystemColors.WindowText;
			this.TbActividad.Location = new System.Drawing.Point(70, 12);
			this.TbActividad.MaxLength = 0;
			this.TbActividad.Name = "TbActividad";
			this.TbActividad.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TbActividad.Size = new System.Drawing.Size(527, 19);
			this.TbActividad.TabIndex = 13;
			this.TbActividad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbActividad_KeyPress);
			// 
			// FrmCriterio
			// 
			//this.FrmCriterio.BackColor = System.Drawing.SystemColors.Control;
			this.FrmCriterio.Controls.Add(this.Rby);
			this.FrmCriterio.Controls.Add(this.RbO);
			this.FrmCriterio.Enabled = true;
			//this.FrmCriterio.ForeColor = System.Drawing.SystemColors.ControlText;
			this.FrmCriterio.Location = new System.Drawing.Point(9, 32);
			this.FrmCriterio.Name = "FrmCriterio";
			this.FrmCriterio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmCriterio.Size = new System.Drawing.Size(130, 38);
			this.FrmCriterio.TabIndex = 10;
			this.FrmCriterio.Text = "Criterio de selecci�n";
			this.FrmCriterio.Visible = true;
			// 
			// Rby
			// 
			//this.Rby.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.Rby.BackColor = System.Drawing.SystemColors.Control;
			this.Rby.CausesValidation = true;
			this.Rby.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.Rby.IsChecked = false;
			this.Rby.Cursor = System.Windows.Forms.Cursors.Default;
			this.Rby.Enabled = true;
			//this.Rby.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Rby.Location = new System.Drawing.Point(14, 19);
			this.Rby.Name = "Rby";
			this.Rby.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Rby.Size = new System.Drawing.Size(46, 14);
			this.Rby.TabIndex = 12;
			this.Rby.TabStop = true;
			this.Rby.Text = "A y B";
			this.Rby.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.Rby.Visible = true;
			// 
			// RbO
			// 
			//this.RbO.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.RbO.BackColor = System.Drawing.SystemColors.Control;
			this.RbO.CausesValidation = true;
			this.RbO.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.RbO.IsChecked = false;
			this.RbO.Cursor = System.Windows.Forms.Cursors.Default;
			this.RbO.Enabled = true;
			//this.RbO.ForeColor = System.Drawing.SystemColors.ControlText;
			this.RbO.Location = new System.Drawing.Point(75, 19);
			this.RbO.Name = "RbO";
			this.RbO.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.RbO.Size = new System.Drawing.Size(47, 14);
			this.RbO.TabIndex = 11;
			this.RbO.TabStop = true;
			this.RbO.Text = "A � B";
			this.RbO.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.RbO.Visible = true;
			// 
			// LbBusqueda
			// 
			//this.LbBusqueda.BackColor = System.Drawing.SystemColors.Control;
			//this.LbBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.LbBusqueda.Cursor = System.Windows.Forms.Cursors.Default;
			//this.LbBusqueda.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LbBusqueda.Location = new System.Drawing.Point(8, 13);
			this.LbBusqueda.Name = "LbBusqueda";
			this.LbBusqueda.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbBusqueda.Size = new System.Drawing.Size(58, 13);
			this.LbBusqueda.TabIndex = 14;
			this.LbBusqueda.Text = "Contenga :";
			// 
			// Frame1
			// 
			//this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			this.Frame1.Controls.Add(this.opDescripcion);
			this.Frame1.Controls.Add(this.opCodigo);
			this.Frame1.Controls.Add(this.tbEmpiecepor);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Enabled = true;
			//this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(8, 72);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(604, 44);
			this.Frame1.TabIndex = 3;
			this.Frame1.Visible = true;
			// 
			// opDescripcion
			// 
			//this.opDescripcion.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.opDescripcion.BackColor = System.Drawing.SystemColors.Control;
			this.opDescripcion.CausesValidation = true;
			this.opDescripcion.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.opDescripcion.IsChecked = true;
			this.opDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
			this.opDescripcion.Enabled = true;
			//this.opDescripcion.ForeColor = System.Drawing.SystemColors.ControlText;
			this.opDescripcion.Location = new System.Drawing.Point(498, 16);
			this.opDescripcion.Name = "opDescripcion";
			this.opDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.opDescripcion.Size = new System.Drawing.Size(98, 17);
			this.opDescripcion.TabIndex = 6;
			this.opDescripcion.TabStop = true;
			this.opDescripcion.Text = "Por descripci�n";
			this.opDescripcion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.opDescripcion.Visible = true;
			// 
			// opCodigo
			// 
			//this.opCodigo.Appearance = System.Windows.Forms.Appearance.Normal;
			//this.opCodigo.BackColor = System.Drawing.SystemColors.Control;
			this.opCodigo.CausesValidation = true;
			this.opCodigo.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.opCodigo.IsChecked = false;
			this.opCodigo.Cursor = System.Windows.Forms.Cursors.Default;
			this.opCodigo.Enabled = true;
			//this.opCodigo.ForeColor = System.Drawing.SystemColors.ControlText;
			this.opCodigo.Location = new System.Drawing.Point(414, 16);
			this.opCodigo.Name = "opCodigo";
			this.opCodigo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.opCodigo.Size = new System.Drawing.Size(77, 17);
			this.opCodigo.TabIndex = 5;
			this.opCodigo.TabStop = true;
			this.opCodigo.Text = "Por c�digo";
			this.opCodigo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.opCodigo.Visible = true;
			// 
			// tbEmpiecepor
			// 
			this.tbEmpiecepor.AcceptsReturn = true;
			//this.tbEmpiecepor.BackColor = System.Drawing.SystemColors.Window;
			//this.tbEmpiecepor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.tbEmpiecepor.Cursor = System.Windows.Forms.Cursors.IBeam;
			//this.tbEmpiecepor.ForeColor = System.Drawing.SystemColors.WindowText;
			this.tbEmpiecepor.Location = new System.Drawing.Point(89, 14);
			this.tbEmpiecepor.MaxLength = 0;
			this.tbEmpiecepor.Name = "tbEmpiecepor";
			this.tbEmpiecepor.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbEmpiecepor.Size = new System.Drawing.Size(309, 22);
			this.tbEmpiecepor.TabIndex = 4;
			this.tbEmpiecepor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbEmpiecepor_KeyPress);
			this.tbEmpiecepor.TextChanged += new System.EventHandler(this.tbEmpiecepor_TextChanged);
			// 
			// Label1
			// 
			//this.Label1.BackColor = System.Drawing.SystemColors.Control;
			//this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(15, 18);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(70, 16);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "Empiece por :";
			// 
			// cbAceptar
			// 
			//this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Enabled = false;
			//this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAceptar.Location = new System.Drawing.Point(438, 318);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbAceptar.TabIndex = 2;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// cbCancelar
			// 
			//this.cbCancelar.BackColor = System.Drawing.SystemColors.Control;
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCancelar.Location = new System.Drawing.Point(533, 318);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCancelar.TabIndex = 1;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbCancelar.UseVisualStyleBackColor = false;
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// Ilbuscar
			// 
			this.Ilbuscar.ImageSize = new System.Drawing.Size(16, 16);
			this.Ilbuscar.ImageStream = (System.Windows.Forms.ImageListStreamer) resources.GetObject("Ilbuscar.ImageStream");
			this.Ilbuscar.TransparentColor = System.Drawing.Color.Silver;
			this.Ilbuscar.Images.SetKeyName(0, "");
			// 
			// BUSCAR_ACTIVIDAD
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(622, 352);
			this.Controls.Add(this.SprActividad);
			this.Controls.Add(this.Frmbuscar);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCancelar);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "BUSCAR_ACTIVIDAD";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Busqueda de Conceptos de Actividad";
			this.Closed += new System.EventHandler(this.BUSCAR_ACTIVIDAD_Closed);
			this.Load += new System.EventHandler(this.BUSCAR_ACTIVIDAD_Load);
			this.Frmbuscar.ResumeLayout(false);
			this.tbBuscar.ResumeLayout(false);
			this.FrmCriterio.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}