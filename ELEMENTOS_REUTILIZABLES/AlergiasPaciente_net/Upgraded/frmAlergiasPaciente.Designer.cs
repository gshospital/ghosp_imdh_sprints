using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace AlergiasPaciente
{
	partial class frmAlergiasPaciente
	{

		#region "Upgrade Support "
		private static frmAlergiasPaciente m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmAlergiasPaciente DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmAlergiasPaciente();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprTerapeutico", "_cbActivar_1", "_cbEliminar_1", "_cbNuevo_1", "Frame2", "SprPrincipios", "_cbNuevo_0", "_cbEliminar_0", "_cbActivar_0", "Frame1", "cbCancelar", "cbAceptar", "Label1", "lbPaciente", "tbPaciente", "FrmPaciente", "LbActivos", "lnActivas", "LbCerrados", "lnEliminadas", "cbActivar", "cbEliminar", "cbNuevo", "ShapeContainer1", "SprPrincipios_Sheet1", "sprTerapeutico_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprTerapeutico;
		private Telerik.WinControls.UI.RadButton _cbActivar_1;
		private Telerik.WinControls.UI.RadButton _cbEliminar_1;
		private Telerik.WinControls.UI.RadButton _cbNuevo_1;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public UpgradeHelpers.Spread.FpSpread SprPrincipios;
		private Telerik.WinControls.UI.RadButton _cbNuevo_0;
		private Telerik.WinControls.UI.RadButton _cbEliminar_0;
		private Telerik.WinControls.UI.RadButton _cbActivar_0;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel lbPaciente;
		public Telerik.WinControls.UI.RadTextBox tbPaciente;
		public Telerik.WinControls.UI.RadGroupBox FrmPaciente;
		public Telerik.WinControls.UI.RadLabel LbActivos;
		public Microsoft.VisualBasic.PowerPacks.LineShape lnActivas;
		public Telerik.WinControls.UI.RadLabel LbCerrados;
		public Microsoft.VisualBasic.PowerPacks.LineShape lnEliminadas;
		public Telerik.WinControls.UI.RadButton[] cbActivar = new Telerik.WinControls.UI.RadButton[2];
		public Telerik.WinControls.UI.RadButton[] cbEliminar = new Telerik.WinControls.UI.RadButton[2];
		public Telerik.WinControls.UI.RadButton[] cbNuevo = new Telerik.WinControls.UI.RadButton[2];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lnActivas = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lnEliminadas = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.sprTerapeutico = new UpgradeHelpers.Spread.FpSpread();
            this._cbActivar_1 = new Telerik.WinControls.UI.RadButton();
            this._cbEliminar_1 = new Telerik.WinControls.UI.RadButton();
            this._cbNuevo_1 = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.SprPrincipios = new UpgradeHelpers.Spread.FpSpread();
            this._cbNuevo_0 = new Telerik.WinControls.UI.RadButton();
            this._cbEliminar_0 = new Telerik.WinControls.UI.RadButton();
            this._cbActivar_0 = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.FrmPaciente = new Telerik.WinControls.UI.RadGroupBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.lbPaciente = new Telerik.WinControls.UI.RadLabel();
            this.tbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.LbActivos = new Telerik.WinControls.UI.RadLabel();
            this.LbCerrados = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sprTerapeutico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprTerapeutico.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbActivar_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbEliminar_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbNuevo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SprPrincipios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SprPrincipios.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbNuevo_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbEliminar_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbActivar_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmPaciente)).BeginInit();
            this.FrmPaciente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbActivos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCerrados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lnActivas,
            this.lnEliminadas});
            this.ShapeContainer1.Size = new System.Drawing.Size(824, 522);
            this.ShapeContainer1.TabIndex = 14;
            this.ShapeContainer1.TabStop = false;
            // 
            // lnActivas
            // 
            this.lnActivas.BorderWidth = 2;
            this.lnActivas.Enabled = false;
            this.lnActivas.Name = "lnActivas";
            this.lnActivas.X1 = 8;
            this.lnActivas.X2 = 71;
            this.lnActivas.Y1 = 482;
            this.lnActivas.Y2 = 482;
            // 
            // lnEliminadas
            // 
            this.lnEliminadas.BorderWidth = 2;
            this.lnEliminadas.Enabled = false;
            this.lnEliminadas.Name = "lnEliminadas";
            this.lnEliminadas.X1 = 8;
            this.lnEliminadas.X2 = 71;
            this.lnEliminadas.Y1 = 503;
            this.lnEliminadas.Y2 = 503;
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.sprTerapeutico);
            this.Frame2.Controls.Add(this._cbActivar_1);
            this.Frame2.Controls.Add(this._cbEliminar_1);
            this.Frame2.Controls.Add(this._cbNuevo_1);
            this.Frame2.HeaderText = "Grupo Terap�utico";
            this.Frame2.Location = new System.Drawing.Point(8, 266);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(807, 197);
            this.Frame2.TabIndex = 13;
            this.Frame2.Text = "Grupo Terap�utico";
            // 
            // sprTerapeutico
            // 
            this.sprTerapeutico.AutoScroll = true;
            this.sprTerapeutico.Location = new System.Drawing.Point(10, 22);
            // 
            // 
            // 
            this.sprTerapeutico.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.sprTerapeutico.Name = "sprTerapeutico";
            this.sprTerapeutico.Size = new System.Drawing.Size(705, 165);
            this.sprTerapeutico.TabIndex = 14;
            this.sprTerapeutico.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprTerapeutico_CellClick);
            // 
            // _cbActivar_1
            // 
            this._cbActivar_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._cbActivar_1.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this._cbActivar_1.Enabled = false;
            this._cbActivar_1.Location = new System.Drawing.Point(722, 151);
            this._cbActivar_1.Name = "_cbActivar_1";
            this._cbActivar_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cbActivar_1.Size = new System.Drawing.Size(77, 36);
            this._cbActivar_1.TabIndex = 17;
            this._cbActivar_1.Text = "Activar";
            this._cbActivar_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._cbActivar_1.Click += new System.EventHandler(this.cbActivar_Click);
            // 
            // _cbEliminar_1
            // 
            this._cbEliminar_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._cbEliminar_1.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this._cbEliminar_1.Enabled = false;
            this._cbEliminar_1.Location = new System.Drawing.Point(722, 109);
            this._cbEliminar_1.Name = "_cbEliminar_1";
            this._cbEliminar_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cbEliminar_1.Size = new System.Drawing.Size(77, 36);
            this._cbEliminar_1.TabIndex = 16;
            this._cbEliminar_1.Text = "Eliminar";
            this._cbEliminar_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._cbEliminar_1.Click += new System.EventHandler(this.cbEliminar_Click);
            // 
            // _cbNuevo_1
            // 
            this._cbNuevo_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._cbNuevo_1.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this._cbNuevo_1.Location = new System.Drawing.Point(722, 66);
            this._cbNuevo_1.Name = "_cbNuevo_1";
            this._cbNuevo_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cbNuevo_1.Size = new System.Drawing.Size(77, 36);
            this._cbNuevo_1.TabIndex = 15;
            this._cbNuevo_1.Text = "A�adir";
            this._cbNuevo_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._cbNuevo_1.Click += new System.EventHandler(this.cbNuevo_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.SprPrincipios);
            this.Frame1.Controls.Add(this._cbNuevo_0);
            this.Frame1.Controls.Add(this._cbEliminar_0);
            this.Frame1.Controls.Add(this._cbActivar_0);
            this.Frame1.HeaderText = "Principio Activo";
            this.Frame1.Location = new System.Drawing.Point(6, 64);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(807, 197);
            this.Frame1.TabIndex = 8;
            this.Frame1.Text = "Principio Activo";
            // 
            // SprPrincipios
            // 
            this.SprPrincipios.AutoScroll = true;
            this.SprPrincipios.Location = new System.Drawing.Point(10, 20);
            // 
            // 
            // 
            this.SprPrincipios.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.SprPrincipios.Name = "SprPrincipios";
            this.SprPrincipios.Size = new System.Drawing.Size(705, 167);
            this.SprPrincipios.TabIndex = 9;
            this.SprPrincipios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPrincipios_CellClick);
            // 
            // _cbNuevo_0
            // 
            this._cbNuevo_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._cbNuevo_0.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this._cbNuevo_0.Location = new System.Drawing.Point(722, 66);
            this._cbNuevo_0.Name = "_cbNuevo_0";
            this._cbNuevo_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cbNuevo_0.Size = new System.Drawing.Size(77, 36);
            this._cbNuevo_0.TabIndex = 12;
            this._cbNuevo_0.Text = "A�adir";
            this._cbNuevo_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._cbNuevo_0.Click += new System.EventHandler(this.cbNuevo_Click);
            // 
            // _cbEliminar_0
            // 
            this._cbEliminar_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._cbEliminar_0.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this._cbEliminar_0.Enabled = false;
            this._cbEliminar_0.Location = new System.Drawing.Point(722, 109);
            this._cbEliminar_0.Name = "_cbEliminar_0";
            this._cbEliminar_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cbEliminar_0.Size = new System.Drawing.Size(77, 36);
            this._cbEliminar_0.TabIndex = 11;
            this._cbEliminar_0.Text = "Eliminar";
            this._cbEliminar_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._cbEliminar_0.Click += new System.EventHandler(this.cbEliminar_Click);
            // 
            // _cbActivar_0
            // 
            this._cbActivar_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._cbActivar_0.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this._cbActivar_0.Enabled = false;
            this._cbActivar_0.Location = new System.Drawing.Point(722, 151);
            this._cbActivar_0.Name = "_cbActivar_0";
            this._cbActivar_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cbActivar_0.Size = new System.Drawing.Size(77, 36);
            this._cbActivar_0.TabIndex = 10;
            this._cbActivar_0.Text = "Activar";
            this._cbActivar_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._cbActivar_0.Click += new System.EventHandler(this.cbActivar_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCancelar.Location = new System.Drawing.Point(646, 476);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 36);
            this.cbCancelar.TabIndex = 4;
            this.cbCancelar.Text = "Cerrar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbAceptar.Enabled = false;
            this.cbAceptar.Location = new System.Drawing.Point(736, 476);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 36);
            this.cbAceptar.TabIndex = 3;
            this.cbAceptar.Text = "Aceptar";
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // FrmPaciente
            // 
            this.FrmPaciente.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmPaciente.Controls.Add(this.Label1);
            this.FrmPaciente.Controls.Add(this.lbPaciente);
            this.FrmPaciente.Controls.Add(this.tbPaciente);
            this.FrmPaciente.HeaderText = "";
            this.FrmPaciente.Location = new System.Drawing.Point(5, 0);
            this.FrmPaciente.Name = "FrmPaciente";
            this.FrmPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmPaciente.Size = new System.Drawing.Size(808, 61);
            this.FrmPaciente.TabIndex = 0;
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(13, 38);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(2, 2);
            this.Label1.TabIndex = 7;
            // 
            // lbPaciente
            // 
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Location = new System.Drawing.Point(13, 16);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(51, 18);
            this.lbPaciente.TabIndex = 2;
            this.lbPaciente.Text = "Paciente:";
            // 
            // tbPaciente
            // 
            this.tbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.tbPaciente.Location = new System.Drawing.Point(73, 16);
            this.tbPaciente.Name = "tbPaciente";
            this.tbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPaciente.Size = new System.Drawing.Size(724, 20);
            this.tbPaciente.TabIndex = 1;
            // 
            // LbActivos
            // 
            this.LbActivos.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbActivos.Location = new System.Drawing.Point(83, 476);
            this.LbActivos.Name = "LbActivos";
            this.LbActivos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbActivos.Size = new System.Drawing.Size(84, 18);
            this.LbActivos.TabIndex = 6;
            this.LbActivos.Text = "Alergias Activas";
            // 
            // LbCerrados
            // 
            this.LbCerrados.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbCerrados.Location = new System.Drawing.Point(83, 497);
            this.LbCerrados.Name = "LbCerrados";
            this.LbCerrados.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbCerrados.Size = new System.Drawing.Size(102, 18);
            this.LbCerrados.TabIndex = 5;
            this.LbCerrados.Text = "Alergias Eliminadas";
            // 
            // frmAlergiasPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 522);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.FrmPaciente);
            this.Controls.Add(this.LbActivos);
            this.Controls.Add(this.LbCerrados);
            this.Controls.Add(this.ShapeContainer1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAlergiasPaciente";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alergias de paciente por Principio Activo y/o por Grupo Terap�utico.";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmAlergiasPaciente_Closed);
            this.Load += new System.EventHandler(this.frmAlergiasPaciente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sprTerapeutico.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprTerapeutico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbActivar_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbEliminar_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbNuevo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SprPrincipios.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SprPrincipios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbNuevo_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbEliminar_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cbActivar_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmPaciente)).EndInit();
            this.FrmPaciente.ResumeLayout(false);
            this.FrmPaciente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbActivos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCerrados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		void ReLoadForm(bool addEvents)
		{
			InitializecbNuevo();
			InitializecbEliminar();
			InitializecbActivar();
		}
		void InitializecbNuevo()
		{
			this.cbNuevo = new Telerik.WinControls.UI.RadButton[2];
			this.cbNuevo[1] = _cbNuevo_1;
			this.cbNuevo[0] = _cbNuevo_0;
		}
		void InitializecbEliminar()
		{
			this.cbEliminar = new Telerik.WinControls.UI.RadButton[2];
			this.cbEliminar[1] = _cbEliminar_1;
			this.cbEliminar[0] = _cbEliminar_0;
		}
		void InitializecbActivar()
		{
			this.cbActivar = new Telerik.WinControls.UI.RadButton[2];
			this.cbActivar[1] = _cbActivar_1;
			this.cbActivar[0] = _cbActivar_0;
		}
		#endregion
	}
}