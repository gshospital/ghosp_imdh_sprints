using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace AlergiasPaciente
{
	partial class frmInteracciones
	{

		#region "Upgrade Support "
		private static frmInteracciones m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmInteracciones DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmInteracciones();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbContinuar", "tbMensaje", "frmMensaje", "cbCerrar"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbContinuar;
		public Telerik.WinControls.UI.RadTextBoxControl tbMensaje;
		public Telerik.WinControls.UI.RadGroupBox frmMensaje;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.cbContinuar = new Telerik.WinControls.UI.RadButton();
            this.frmMensaje = new Telerik.WinControls.UI.RadGroupBox();
            this.tbMensaje = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.cbContinuar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmMensaje)).BeginInit();
            this.frmMensaje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbMensaje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbContinuar
            // 
            this.cbContinuar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbContinuar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbContinuar.Location = new System.Drawing.Point(200, 200);
            this.cbContinuar.Name = "cbContinuar";
            this.cbContinuar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbContinuar.Size = new System.Drawing.Size(81, 28);
            this.cbContinuar.TabIndex = 3;
            this.cbContinuar.Text = "C&ontinuar";
            this.cbContinuar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbContinuar.Click += new System.EventHandler(this.cbContinuar_Click);
            // 
            // frmMensaje
            // 
            this.frmMensaje.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmMensaje.Controls.Add(this.tbMensaje);
            this.frmMensaje.HeaderText = "";
            this.frmMensaje.Location = new System.Drawing.Point(4, 0);
            this.frmMensaje.Name = "frmMensaje";
            this.frmMensaje.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmMensaje.Size = new System.Drawing.Size(653, 193);
            this.frmMensaje.TabIndex = 1;
            // 
            // tbMensaje
            // 
            this.tbMensaje.AcceptsReturn = true;
            this.tbMensaje.AutoScroll = true;
            this.tbMensaje.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbMensaje.IsReadOnly = true;
            this.tbMensaje.Location = new System.Drawing.Point(4, 14);
            this.tbMensaje.MaxLength = 0;
            this.tbMensaje.Multiline = true;
            this.tbMensaje.Name = "tbMensaje";
            this.tbMensaje.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbMensaje.Size = new System.Drawing.Size(644, 169);
            this.tbMensaje.TabIndex = 2;
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCerrar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCerrar.Location = new System.Drawing.Point(385, 200);
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(81, 28);
            this.cbCerrar.TabIndex = 0;
            this.cbCerrar.Text = "&Cancelar";
            this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // frmInteracciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 232);
            this.Controls.Add(this.cbContinuar);
            this.Controls.Add(this.frmMensaje);
            this.Controls.Add(this.cbCerrar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInteracciones";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Interacciones / Incompatibilidades detectadas";
            this.Closed += new System.EventHandler(this.frmInteracciones_Closed);
            this.Load += new System.EventHandler(this.frmInteracciones_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbContinuar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmMensaje)).EndInit();
            this.frmMensaje.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbMensaje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}