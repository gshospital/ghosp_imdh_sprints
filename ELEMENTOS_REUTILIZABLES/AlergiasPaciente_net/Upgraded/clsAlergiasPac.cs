using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using Telerik.WinControls;

using ElementosCompartidos;

namespace AlergiasPaciente
{
	public class clsAlergiasPac
	{


		public clsAlergiasPac()
		{

			try
			{

				basAlergiasPac.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{

				RadMessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}

		}

		~clsAlergiasPac()
		{

			basAlergiasPac.clase_mensaje = null;

		}

		public void Llamada(SqlConnection nConexion, string stGidenpac, string stUsuario, bool bSoloLectura = true)
		{

			basAlergiasPac.GstPaciente = stGidenpac;
			basAlergiasPac.GstUsuario = stUsuario;
			basAlergiasPac.GSoloLectura = bSoloLectura;
			Serrores.AjustarRelojCliente(nConexion);
			basAlergiasPac.rcConexion = nConexion;

			Serrores.ObtenerNombreAplicacion(basAlergiasPac.rcConexion);

			basAlergiasPac.ObtenerNombreUsuario();

			basAlergiasPac.nFormAlergias = new frmAlergiasPaciente();

			frmAlergiasPaciente tempLoadForm = basAlergiasPac.nFormAlergias;

			basAlergiasPac.nFormAlergias.ShowDialog();

		}

		public bool EsAlergico(SqlConnection nConexion, string stGidenpac, string stFarmaco)
		{

			bool result = false;
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;
				Serrores.AjustarRelojCliente(nConexion);
				stSql = "SELECT * FROM DPACALPA, FCOMPFAR WHERE " + 
				        "FCOMPFAR.gfarmaco = '" + stFarmaco + "' AND " + 
				        "DPACALPA.gpactivo = FCOMPFAR.gpactivo AND " + 
				        "DPACALPA.gidenpac = '" + stGidenpac + "' AND " + 
				        "DPACALPA.fborrado IS NULL";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, nConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				result = RrDatos.Tables[0].Rows.Count != 0;

				if (!result)
				{
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrDatos.Close();
					stSql = "SELECT * FROM DPACALGT " + 
					        "INNER JOIN DCODFARM ON DPACALGT.ggrutera = DCODFARM.gterap " + 
					        "WHERE DCODFARM.articulo = '" + stFarmaco + "' AND " + 
					        "DPACALGT.gidenpac = '" + stGidenpac + "' AND DPACALGT.fborrado IS NULL";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, nConexion);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);

					result = RrDatos.Tables[0].Rows.Count != 0;
				}


				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				return result;
			}
			catch
			{

				return false;
			}
		}

		public bool HayIncompatibilidad(SqlConnection nConexion, string ParamItiposer, int ParamGanoregi, int ParamGnumregi, string ParamFarmaco = "", bool ParamPlantilla = false)
		{

			bool result = false;
			string stConPrincipios = String.Empty;
			string stConFarmacos = String.Empty;
			string stMensaje = String.Empty;
			string stBotPlus = String.Empty;

			try
			{
				Serrores.AjustarRelojCliente(nConexion);
				// Guardamos la conexi�n

				basAlergiasPac.rcConexion = nConexion;

				// Ponemos los valores por defecto


				// Si el f�rmaco viene sin comillas simples es que es un �nico f�rmaco

				if (ParamFarmaco.Trim() != "" && (ParamFarmaco.IndexOf('\'') + 1) == 0)
				{

					// Le a�adimos las comillas simples

					ParamFarmaco = "'" + ParamFarmaco.Trim() + "'";

				}

				// Recorremos cada f�rmaco activo para ver si presenta incompatibilidad

				stConPrincipios = basAlergiasPac.BuscaInteraccionesPrincipio(ParamItiposer, ParamGanoregi, ParamGnumregi, ParamFarmaco, ParamPlantilla);

				// Buscamos interacciones de F�rmacos con este f�rmaco

				stConFarmacos = basAlergiasPac.BuscaInteraccionesFarmaco(ParamItiposer, ParamGanoregi, ParamGnumregi, ParamFarmaco, ParamPlantilla);

				//' O.Frias - 28/04/2014
				//' Se buscan interacciones en el Bot-Plus
				stBotPlus = basAlergiasPac.buscaInteracionesBotPlus(ParamItiposer, ParamGanoregi, ParamGnumregi, ref ParamFarmaco, "", ParamPlantilla);

				// Si se encontraron incompatibilidades, preguntamos si queremos mostrarlas por pantalla

				if ((stConPrincipios.Trim() != "") || (stConFarmacos.Trim() != "" || stBotPlus.Trim() != ""))
				{

					// Creamos el objeto frmInteracciones

					basAlergiasPac.nFormInteracciones = new frmInteracciones();

					// Obtenemos el mensaje

					if (stConPrincipios.Trim() != "")
					{

						stMensaje = "------------ INTERACCIONES / INCOMPATIBILIDADES DE PRINCIPIOS ACTIVOS ------------" + 
						            Environment.NewLine + Environment.NewLine + stConPrincipios.Trim() + 
						            ((stConFarmacos.Trim() != "" || stBotPlus.Trim() != "") ? Environment.NewLine + Environment.NewLine : "");

					}

					if (stConFarmacos.Trim() != "")
					{

						stMensaje = stMensaje + 
						            "------------ INTERACCIONES / INCOMPATIBILIDADES DE F�RMACOS ------------" + 
						            Environment.NewLine + Environment.NewLine + stConFarmacos.Trim() + 
						            ((stBotPlus.Trim() != "") ? Environment.NewLine + Environment.NewLine : "");

					}


					if (stBotPlus.Trim() != "")
					{

						stMensaje = stMensaje + 
						            "------------ INTERACCIONES INDICADAS EN BOT-PLUS ------------" + 
						            Environment.NewLine + Environment.NewLine + stBotPlus.Trim();

					}

					// Se lo pasamos al formulario

					basAlergiasPac.nFormInteracciones.proTexto(stMensaje);

					basAlergiasPac.nFormInteracciones.ShowDialog();

					// Vemos qu� respuesta se dio

					result = !basAlergiasPac.nFormInteracciones.seContinuo();

					// Cerramos el objeto

					basAlergiasPac.nFormInteracciones = null;

				}

				return result;
			}
			catch
			{

				return false;
			}
		}

		public bool HayIncompatibilidadREX(SqlConnection nConexion, string ParamIdPaciente, string ParamFarmaco)
		{

			bool result = false;
			string stConPrincipios = String.Empty;
			string stConFarmacos = String.Empty;
			string stMensaje = String.Empty;
			string stBotPlus = String.Empty;

			try
			{
				Serrores.AjustarRelojCliente(nConexion);
				// Guardamos la conexi�n

				basAlergiasPac.rcConexion = nConexion;

				// Ponemos los valores por defecto


				// Si el f�rmaco viene sin comillas simples es que es un �nico f�rmaco

				if (ParamFarmaco.Trim() != "" && (ParamFarmaco.IndexOf('\'') + 1) == 0)
				{

					// Le a�adimos las comillas simples

					ParamFarmaco = "'" + ParamFarmaco.Trim() + "'";

				}

				// Recorremos cada f�rmaco activo para ver si presenta incompatibilidad

				stConPrincipios = basAlergiasPac.BuscaInteraccionesPrincipioREX(ParamIdPaciente, ParamFarmaco);

				// Buscamos interacciones de F�rmacos con este f�rmaco

				stConFarmacos = basAlergiasPac.BuscaInteraccionesFarmacoREX(ParamIdPaciente, ParamFarmaco);


				//' O.Frias - 28/04/2014
				//' Se buscan interacciones en el Bot-Plus
				stBotPlus = basAlergiasPac.buscaInteracionesBotPlusREX("", 0, 0, ParamFarmaco, ParamIdPaciente);

				// Si se encontraron incompatibilidades, preguntamos si queremos mostrarlas por pantalla

				if ((stConPrincipios.Trim() != "") || (stConFarmacos.Trim() != "" || stBotPlus.Trim() != ""))
				{

					// Creamos el objeto frmInteracciones

					basAlergiasPac.nFormInteracciones = new frmInteracciones();

					// Obtenemos el mensaje

					if (stConPrincipios.Trim() != "")
					{

						stMensaje = "------------ INTERACCIONES / INCOMPATIBILIDADES DE PRINCIPIOS ACTIVOS ------------" + 
						            Environment.NewLine + Environment.NewLine + stConPrincipios.Trim() + 
						            ((stConFarmacos.Trim() != "" || stBotPlus.Trim() != "") ? Environment.NewLine + Environment.NewLine : "");

					}

					if (stConFarmacos.Trim() != "")
					{

						stMensaje = stMensaje + 
						            "------------ INTERACCIONES / INCOMPATIBILIDADES DE F�RMACOS ------------" + 
						            Environment.NewLine + Environment.NewLine + stConFarmacos.Trim() + 
						            ((stBotPlus.Trim() != "") ? Environment.NewLine + Environment.NewLine : "");
					}

					if (stBotPlus.Trim() != "")
					{

						stMensaje = stMensaje + 
						            "------------ INTERACCIONES INDICADAS EN BOT-PLUS ------------" + 
						            Environment.NewLine + Environment.NewLine + stBotPlus.Trim();

					}
					// Se lo pasamos al formulario

					basAlergiasPac.nFormInteracciones.proTexto(stMensaje);

					basAlergiasPac.nFormInteracciones.ShowDialog();

					// Vemos qu� respuesta se dio

					result = !basAlergiasPac.nFormInteracciones.seContinuo();

					// Cerramos el objeto

					basAlergiasPac.nFormInteracciones = null;

				}

				return result;
			}
			catch
			{

				return false;
			}
		}

		public bool HayIncompatibilidadPF(SqlConnection nConexion, int ParamGanoregi, int ParamGnumregi, string ParamFarmaco)
		{


			bool result = false;
			string stConPrincipios = String.Empty;
			string stConFarmacos = String.Empty;
			string stMensaje = String.Empty;
			string stBotPlus = String.Empty;

			try
			{
				Serrores.AjustarRelojCliente(nConexion);
				// Guardamos la conexi�n

				basAlergiasPac.rcConexion = nConexion;

				// Ponemos los valores por defecto


				// Si el f�rmaco viene sin comillas simples es que es un �nico f�rmaco

				if (ParamFarmaco.Trim() != "" && (ParamFarmaco.IndexOf('\'') + 1) == 0)
				{

					// Le a�adimos las comillas simples

					ParamFarmaco = "'" + ParamFarmaco.Trim() + "'";

				}

				// Recorremos cada f�rmaco activo para ver si presenta incompatibilidad

				stConPrincipios = basAlergiasPac.BuscaInteraccionesPrincipioPF(ParamGanoregi, ParamGnumregi, ParamFarmaco);

				// Buscamos interacciones de F�rmacos con este f�rmaco

				stConFarmacos = basAlergiasPac.BuscaInteraccionesFarmacoPF(ParamGanoregi, ParamGnumregi, ParamFarmaco);

				//' O.Frias - 28/04/2014
				//' Se buscan interacciones en el Bot-Plus
				stBotPlus = basAlergiasPac.buscaInteracionesBotPlus("PF", ParamGanoregi, ParamGnumregi, ref ParamFarmaco);

				// Si se encontraron incompatibilidades, preguntamos si queremos mostrarlas por pantalla

				if ((stConPrincipios.Trim() != "") || (stConFarmacos.Trim() != "" || stBotPlus.Trim() != ""))
				{

					// Creamos el objeto frmInteracciones

					basAlergiasPac.nFormInteracciones = new frmInteracciones();

					// Obtenemos el mensaje

					if (stConPrincipios.Trim() != "")
					{

						stMensaje = "------------ INTERACCIONES / INCOMPATIBILIDADES DE PRINCIPIOS ACTIVOS ------------" + 
						            Environment.NewLine + Environment.NewLine + stConPrincipios.Trim() + 
						            ((stConFarmacos.Trim() != "" || stBotPlus.Trim() != "") ? Environment.NewLine + Environment.NewLine : "");

					}

					if (stConFarmacos.Trim() != "")
					{

						stMensaje = stMensaje + 
						            "------------ INTERACCIONES / INCOMPATIBILIDADES DE F�RMACOS ------------" + 
						            Environment.NewLine + Environment.NewLine + stConFarmacos.Trim() + 
						            ((stBotPlus.Trim() != "") ? Environment.NewLine + Environment.NewLine : "");

					}


					if (stBotPlus.Trim() != "")
					{

						stMensaje = stMensaje + 
						            "------------ INTERACCIONES INDICADAS EN BOT-PLUS ------------" + 
						            Environment.NewLine + Environment.NewLine + stBotPlus.Trim();

					}

					// Se lo pasamos al formulario

					basAlergiasPac.nFormInteracciones.proTexto(stMensaje);

					basAlergiasPac.nFormInteracciones.ShowDialog();

					// Vemos qu� respuesta se dio

					result = !basAlergiasPac.nFormInteracciones.seContinuo();

					// Cerramos el objeto

					basAlergiasPac.nFormInteracciones = null;

				}

				return result;
			}
			catch
			{

				return false;
			}
		}
	}
}