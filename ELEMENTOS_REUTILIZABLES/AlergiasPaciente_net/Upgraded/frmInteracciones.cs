using System;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI; 

namespace AlergiasPaciente
{
	public partial class frmInteracciones
		: RadForm
	{   


		private string stTexto = String.Empty;
		private bool bContinuar = false;
		public frmInteracciones()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{

			bContinuar = false;
			this.Close();

		}

		private void cbContinuar_Click(Object eventSender, EventArgs eventArgs)
		{

			bContinuar = true;
			this.Close();

		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void frmInteracciones_Load(Object eventSender, EventArgs eventArgs)
		{

			// Ajustamos el tama�o del formulario al de la pantalla

			this.Width = (int) (Screen.PrimaryScreen.Bounds.Width / 2);
			this.Height = (int) (Screen.PrimaryScreen.Bounds.Height / 2);

			// Colocamos los controles en funci�n del nuevo tama�o

			frmMensaje.Width = (int) (this.Width - 13);
			frmMensaje.Left = 4;

			frmMensaje.Height = (int) (this.Height - 47 - cbCerrar.Height);

			tbMensaje.Width = (int) (frmMensaje.Width - 7);
			tbMensaje.Left = 3;

			tbMensaje.Height = (int) (frmMensaje.Height - 20);

			cbContinuar.Top = (int) (frmMensaje.Height + 7);

			cbCerrar.Top = (int) (frmMensaje.Height + 7);

			cbCerrar.Left = (int) (this.Width - cbCerrar.Width - 13);
			cbContinuar.Left = (int) (cbCerrar.Left - cbContinuar.Width - 13);

			// Cargamos los campos

			tbMensaje.Text = stTexto;
			bContinuar = false;

		}

		public void proTexto(string ParamTexto)
		{

			stTexto = ParamTexto.Trim();

		}

		public bool seContinuo()
		{

			return bContinuar;

		}
		private void frmInteracciones_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}