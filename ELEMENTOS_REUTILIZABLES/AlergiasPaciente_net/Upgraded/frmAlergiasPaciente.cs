using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using ElementosCompartidos;

namespace AlergiasPaciente
{
	public partial class frmAlergiasPaciente
		: RadForm
	{


		private const int COL_CODIGO = 1;
		private const int COL_DESCRIPCION = 2;
		private const int COL_USUARIO = 3;
		private const int COL_FBORRADO = 4;
		public frmAlergiasPaciente()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}



		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{

			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;

				// Marcamos como borrados los que est�n en el grid con marca de borrado

				int tempForVar = SprPrincipios.MaxRows;
				for (int I = 1; I <= tempForVar; I++)
				{

					SprPrincipios.Row = I;

					if (ColorTranslator.ToOle(SprPrincipios.ForeColor) == ColorTranslator.ToOle(lnEliminadas.BorderColor))
					{

						// Si est� borrado

						SprPrincipios.Col = COL_FBORRADO;

						if (SprPrincipios.Text.Trim() == "")
						{

							// S�lo marcamos como borrados aquellos que a�n no lo estuvieran

							SprPrincipios.Col = COL_CODIGO;

							stSql = "UPDATE DPACALPA SET " + 
							        "fborrado = getDate(), " + 
							        "gusuario = '" + basAlergiasPac.GstUsuario + "' WHERE " + 
							        "gidenpac = '" + basAlergiasPac.GstPaciente + "' AND " + 
							        "gpactivo = '" + SprPrincipios.Text.Trim() + "' AND " + 
							        "fborrado IS NULL";

							SqlCommand tempCommand = new SqlCommand(stSql, basAlergiasPac.rcConexion);
							tempCommand.ExecuteNonQuery();

						}

					}
					else
					{

						// Si est� activo, vemos si lo incluimos o lo reactivamos

						SprPrincipios.Col = COL_CODIGO;

						stSql = "SELECT * FROM DPACALPA WHERE " + 
						        "gidenpac = '" + basAlergiasPac.GstPaciente + "' AND " + 
						        "gpactivo = '" + SprPrincipios.Text.Trim() + "'";

						SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, basAlergiasPac.rcConexion);
						RrDatos = new DataSet();
						tempAdapter.Fill(RrDatos);

						if (RrDatos.Tables[0].Rows.Count == 0)
						{

							// A�adimos el nuevo

							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.AddNew was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							RrDatos.AddNew();

							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							RrDatos.Tables[0].Rows[0]["gidenpac"] = basAlergiasPac.GstPaciente;
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							RrDatos.Tables[0].Rows[0]["gpactivo"] = SprPrincipios.Text.Trim();
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							RrDatos.Tables[0].Rows[0]["gusuario"] = basAlergiasPac.GstUsuario;

							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							string tempQuery = RrDatos.Tables[0].TableName;
							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
							SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
							tempAdapter_2.Update(RrDatos, RrDatos.Tables[0].TableName);

						}
						else
						{

							// Si estaba borrado, lo reactivamos. Si no, lo ignoramos porque ya estaba introducido

							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["fborrado"]))
							{

								//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Edit was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
								RrDatos.Edit();

								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
								RrDatos.Tables[0].Rows[0]["fborrado"] = DBNull.Value;
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								RrDatos.Tables[0].Rows[0]["fdetecci"] = DateTime.Now;
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								RrDatos.Tables[0].Rows[0]["gusuario"] = basAlergiasPac.GstUsuario;

								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								string tempQuery_2 = RrDatos.Tables[0].TableName;
								SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery_2, "");
								SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);
								tempAdapter_3.Update(RrDatos, RrDatos.Tables[0].TableName);

							}

						}

						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrDatos.Close();

					}

				}


				int tempForVar2 = sprTerapeutico.MaxRows;
				for (int I = 1; I <= tempForVar2; I++)
				{
					sprTerapeutico.Row = I;
					if (ColorTranslator.ToOle(sprTerapeutico.ForeColor) == ColorTranslator.ToOle(lnEliminadas.BorderColor))
					{
						// Si est� borrado
						sprTerapeutico.Col = COL_FBORRADO;
						if (sprTerapeutico.Text.Trim() == "")
						{
							// S�lo marcamos como borrados aquellos que a�n no lo estuvieran
							sprTerapeutico.Col = COL_CODIGO;
							stSql = "UPDATE DPACALGT SET " + 
							        "fborrado = getDate(), " + 
							        "gusuario = '" + basAlergiasPac.GstUsuario + "' WHERE " + 
							        "gidenpac = '" + basAlergiasPac.GstPaciente + "' AND " + 
							        "ggrutera = '" + sprTerapeutico.Text.Trim() + "' AND " + 
							        "fborrado IS NULL";
							SqlCommand tempCommand_2 = new SqlCommand(stSql, basAlergiasPac.rcConexion);
							tempCommand_2.ExecuteNonQuery();
						}
					}
					else
					{
						// Si est� activo, vemos si lo incluimos o lo reactivamos
						sprTerapeutico.Col = COL_CODIGO;
						stSql = "SELECT * FROM DPACALGT WHERE " + 
						        "gidenpac = '" + basAlergiasPac.GstPaciente + "' AND " + 
						        "ggrutera = '" + sprTerapeutico.Text.Trim() + "'";

						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, basAlergiasPac.rcConexion);
						RrDatos = new DataSet();
						tempAdapter_4.Fill(RrDatos);
						if (RrDatos.Tables[0].Rows.Count == 0)
						{
							// A�adimos el nuevo
							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.AddNew was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							RrDatos.AddNew();
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							RrDatos.Tables[0].Rows[0]["gidenpac"] = basAlergiasPac.GstPaciente;
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							RrDatos.Tables[0].Rows[0]["ggrutera"] = sprTerapeutico.Text.Trim();
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							RrDatos.Tables[0].Rows[0]["gusuario"] = basAlergiasPac.GstUsuario;
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							string tempQuery_3 = RrDatos.Tables[0].TableName;
							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery_3, "");
							SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_5);
							tempAdapter_5.Update(RrDatos, RrDatos.Tables[0].TableName);
						}
						else
						{
							// Si estaba borrado, lo reactivamos. Si no, lo ignoramos porque ya estaba introducido
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["fborrado"]))
							{
								//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Edit was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
								RrDatos.Edit();
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
								RrDatos.Tables[0].Rows[0]["fborrado"] = DBNull.Value;
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								RrDatos.Tables[0].Rows[0]["fdetecci"] = DateTime.Now;
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								RrDatos.Tables[0].Rows[0]["gusuario"] = basAlergiasPac.GstUsuario;
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								string tempQuery_4 = RrDatos.Tables[0].TableName;
								SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(tempQuery_4, "");
								SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_6);
								tempAdapter_6.Update(RrDatos, RrDatos.Tables[0].TableName);
							}
						}
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrDatos.Close();
					}

				}

				cbAceptar.Enabled = false;


				//Oscar C Octubre 2011
				//Si tiene alguna prescripci�n actual , en Urgencias u Hospitalizaci�n
				//con alg�n f�rmaco que tenga alguno de los principios activos a los que es al�rgico el paciente,
				//que de un mensaje al usuario para que revise la prescripci�n farmacol�gica.
				bool bPrescripcionFarmacosPrincipiosActivosAlergicoPaciente = false;
				bPrescripcionFarmacosPrincipiosActivosAlergicoPaciente = false;
				stSql = "SELECT count(*) " + 
				        "FROM AEPISADM " + 
				        " INNER JOIN EFARMACO on EFARMACO.itiposer='H' and " + 
				        "                        EFARMACO.ganoregi = AEPISADM.ganoadme AND EFARMACO.gnumregi = AEPISADM.gnumadme AND " + 
				        "                        (EFARMACO.iesttrat = 'I' or EFARMACO.iesttrat = 'S') AND " + 
				        "                        EFARMACO.ffinmovi is null " + 
				        " INNER JOIN DPACALPA on AEPISADM.gidenpac = DPACALPA.gidenpac AND DPACALPA.fborrado is null " + 
				        " INNER JOIN FCOMPFAR on FCOMPFAR.gfarmaco = EFARMACO.gfarmaco AND FCOMPFAR.gpactivo = DPACALPA.gpactivo " + 
				        " where " + 
				        " AEPISADM.gidenpac ='" + basAlergiasPac.GstPaciente + "' And AEPISADM.FALTPLAN Is Null ";
				SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSql, basAlergiasPac.rcConexion);
				RrDatos = new DataSet();
				tempAdapter_7.Fill(RrDatos);
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToDouble(RrDatos.Tables[0].Rows[0][0]) > 0)
				{
					bPrescripcionFarmacosPrincipiosActivosAlergicoPaciente = true;
				}
				else
				{
					stSql = "SELECT count(*) " + 
					        "FROM UEPISURG " + 
					        " INNER JOIN EFARMACO on EFARMACO.itiposer='U' and " + 
					        "                        EFARMACO.ganoregi = UEPISURG.ganourge AND EFARMACO.gnumregi = UEPISURG.gnumurge AND " + 
					        "                        (EFARMACO.iesttrat = 'I' or EFARMACO.iesttrat = 'S') AND " + 
					        "                        EFARMACO.ffinmovi is null " + 
					        " INNER JOIN DPACALPA on UEPISURG.gidenpac = DPACALPA.gidenpac AND DPACALPA.fborrado is null " + 
					        " INNER JOIN FCOMPFAR on FCOMPFAR.gfarmaco = EFARMACO.gfarmaco AND FCOMPFAR.gpactivo = DPACALPA.gpactivo " + 
					        " where " + 
					        " UEPISURG.gidenpac ='" + basAlergiasPac.GstPaciente + "' And UEPISURG.FALTAURG Is Null ";
					SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(stSql, basAlergiasPac.rcConexion);
					RrDatos = new DataSet();
					tempAdapter_8.Fill(RrDatos);
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToDouble(RrDatos.Tables[0].Rows[0][0]) > 0)
					{
						bPrescripcionFarmacosPrincipiosActivosAlergicoPaciente = true;
					}
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				if (!bPrescripcionFarmacosPrincipiosActivosAlergicoPaciente)
				{
					stSql = "SELECT count(*) " + 
					        "FROM AEPISADM " + 
					        " INNER JOIN EFARMACO on EFARMACO.itiposer='H' and " + 
					        "                        EFARMACO.ganoregi = AEPISADM.ganoadme AND EFARMACO.gnumregi = AEPISADM.gnumadme AND " + 
					        "                        (EFARMACO.iesttrat = 'I' or EFARMACO.iesttrat = 'S') AND " + 
					        "                        EFARMACO.ffinmovi is null " + 
					        " INNER JOIN DPACALGT on AEPISADM.gidenpac = DPACALGT.gidenpac AND DPACALGT.fborrado is null " + 
					        " INNER JOIN DCODFARM on DCODFARM.articulo = EFARMACO.gfarmaco AND DCODFARM.Gterap = DPACALGT.ggrutera " + 
					        " where " + 
					        " AEPISADM.gidenpac ='" + basAlergiasPac.GstPaciente + "' And AEPISADM.FALTPLAN Is Null ";
					SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql, basAlergiasPac.rcConexion);
					RrDatos = new DataSet();
					tempAdapter_9.Fill(RrDatos);
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToDouble(RrDatos.Tables[0].Rows[0][0]) > 0)
					{
						bPrescripcionFarmacosPrincipiosActivosAlergicoPaciente = true;
					}
					else
					{
						stSql = "SELECT count(*) " + 
						        "FROM UEPISURG " + 
						        " INNER JOIN EFARMACO on EFARMACO.itiposer='U' and " + 
						        "                        EFARMACO.ganoregi = UEPISURG.ganourge AND EFARMACO.gnumregi = UEPISURG.gnumurge AND " + 
						        "                        (EFARMACO.iesttrat = 'I' or EFARMACO.iesttrat = 'S') AND " + 
						        "                        EFARMACO.ffinmovi is null " + 
						        " INNER JOIN DPACALGT on UEPISURG.gidenpac = DPACALGT.gidenpac AND DPACALGT.fborrado is null " + 
						        " INNER JOIN DCODFARM on DCODFARM.articulo = EFARMACO.gfarmaco AND DCODFARM.Gterap = DPACALGT.ggrutera " + 
						        " where " + 
						        " UEPISURG.gidenpac ='" + basAlergiasPac.GstPaciente + "' And UEPISURG.FALTAURG Is Null ";
						SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(stSql, basAlergiasPac.rcConexion);
						RrDatos = new DataSet();
						tempAdapter_10.Fill(RrDatos);
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToDouble(RrDatos.Tables[0].Rows[0][0]) > 0)
						{
							bPrescripcionFarmacosPrincipiosActivosAlergicoPaciente = true;
						}
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrDatos.Close();
				}
				if (bPrescripcionFarmacosPrincipiosActivosAlergicoPaciente)
				{
					short tempRefParam = 5040;
					string[] tempRefParam2 = new string[] { };
					basAlergiasPac.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, basAlergiasPac.rcConexion, tempRefParam2);
				}
				//-----------------------------------------------------


				this.Close();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("AlergiasPaciente", Path.GetDirectoryName(Application.ExecutablePath), basAlergiasPac.GstUsuario, "AlergiasPaciente:cbAceptar", ex);
			}

		}

		private void cbActivar_Click(Object eventSender, EventArgs eventArgs)
		{
			int index = Array.IndexOf(this.cbActivar, eventSender);

			if (index == 0)
			{
				SprPrincipios.Col = COL_USUARIO;
				SprPrincipios.Text = basAlergiasPac.GstNombreUsuario;

				SprPrincipios.Col = COL_FBORRADO;
				SprPrincipios.Text = "";
				proCambiaColorFila(SprPrincipios.Row, lnActivas.BorderColor, index);
			}
			else
			{
				sprTerapeutico.Col = COL_USUARIO;
				sprTerapeutico.Text = basAlergiasPac.GstNombreUsuario;

				sprTerapeutico.Col = COL_FBORRADO;
				sprTerapeutico.Text = "";
				proCambiaColorFila(sprTerapeutico.Row, lnActivas.BorderColor, index);
			}


			proActivaBotones(index);

			cbAceptar.Enabled = true;

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{

			this.Close();

		}

		private void cbEliminar_Click(Object eventSender, EventArgs eventArgs)
		{
			int index = Array.IndexOf(this.cbEliminar, eventSender);

			if (index == 0)
			{
				SprPrincipios.Col = COL_USUARIO;
				SprPrincipios.Text = basAlergiasPac.GstNombreUsuario;

				SprPrincipios.Col = COL_FBORRADO;
				SprPrincipios.Text = ""; //Format(Now, "DD/MM/YYYY HH:NN:SS")

				proCambiaColorFila(SprPrincipios.Row, lnEliminadas.BorderColor, index);

			}
			else
			{
				sprTerapeutico.Col = COL_USUARIO;
				sprTerapeutico.Text = basAlergiasPac.GstNombreUsuario;

				sprTerapeutico.Col = COL_FBORRADO;
				sprTerapeutico.Text = ""; //Format(Now, "DD/MM/YYYY HH:NN:SS")

				proCambiaColorFila(sprTerapeutico.Row, lnEliminadas.BorderColor, index);
			}
			proActivaBotones(index);

			cbAceptar.Enabled = true;

		}

		private void proOrdena(int index)
		{

			if (index == 0)
			{
				SprPrincipios.Row = 1;
				SprPrincipios.Col = COL_CODIGO;
				SprPrincipios.Row2 = SprPrincipios.MaxRows;
				SprPrincipios.Col2 = SprPrincipios.MaxCols;
				SprPrincipios.SortBy = 0;
				// Ordenamos por la descripci�n
				SprPrincipios.SetSortKey(1, COL_DESCRIPCION);
				SprPrincipios.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants) 1);
				//SprPrincipios.Action = 25;
			}
			else
			{
				sprTerapeutico.Row = 1;
				sprTerapeutico.Col = COL_CODIGO;
				sprTerapeutico.Row2 = sprTerapeutico.MaxRows;
				sprTerapeutico.Col2 = sprTerapeutico.MaxCols;
				sprTerapeutico.SortBy = 0;
				// Ordenamos por la descripci�n
				sprTerapeutico.SetSortKey(1, COL_DESCRIPCION);
				sprTerapeutico.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants) 1);
				//sprTerapeutico.Action = 25;
			}
		}

		public void Recoger_datosPrincipio(string stCodigo, string stDescripcion)
		{


			if (stCodigo.Trim() == "")
			{
				return;
			}

			// Comprobamos si el principio ya existe

			SprPrincipios.Col = COL_CODIGO;

			int tempForVar = SprPrincipios.MaxRows;
			for (int I = 1; I <= tempForVar; I++)
			{
				SprPrincipios.Row = I;

				if (SprPrincipios.Text.Trim().ToUpper() == stCodigo.Trim().ToUpper())
				{

					// Si existe y estaba inactivo, lo activamos

					SprPrincipios.Col = COL_USUARIO;

					if (ColorTranslator.ToOle(SprPrincipios.ForeColor) == ColorTranslator.ToOle(lnEliminadas.BorderColor))
					{

						SprPrincipios.Text = basAlergiasPac.GstNombreUsuario;

						SprPrincipios.Col = COL_FBORRADO;
						SprPrincipios.Text = "";

						proCambiaColorFila(SprPrincipios.Row, lnActivas.BorderColor, 0);

						cbAceptar.Enabled = true;

					}

					return;

				}

			}

			// En este punto, el principio no existe. Lo a�adimos

			SprPrincipios.MaxRows++;

			SprPrincipios.Row = SprPrincipios.MaxRows;
			SprPrincipios.Col = COL_DESCRIPCION;
			SprPrincipios.Text = stDescripcion;
			SprPrincipios.Col = COL_CODIGO;
			SprPrincipios.Text = stCodigo;
			SprPrincipios.Col = COL_USUARIO;
			SprPrincipios.Text = basAlergiasPac.GstNombreUsuario;

			proOrdena(0);

			cbAceptar.Enabled = true;

		}


		public void Recoger_datosGrupoTerapeutico(string stCodigo, string stDescripcion)
		{


			if (stCodigo.Trim() == "")
			{
				return;
			}

			// Comprobamos si el principio ya existe

			sprTerapeutico.Col = COL_CODIGO;

			int tempForVar = sprTerapeutico.MaxRows;
			for (int I = 1; I <= tempForVar; I++)
			{
				sprTerapeutico.Row = I;

				if (sprTerapeutico.Text.Trim().ToUpper() == stCodigo.Trim().ToUpper())
				{

					// Si existe y estaba inactivo, lo activamos

					sprTerapeutico.Col = COL_USUARIO;

					if (ColorTranslator.ToOle(sprTerapeutico.ForeColor) == ColorTranslator.ToOle(lnEliminadas.BorderColor))
					{

						sprTerapeutico.Text = basAlergiasPac.GstNombreUsuario;

						sprTerapeutico.Col = COL_FBORRADO;
						sprTerapeutico.Text = "";

						proCambiaColorFila(sprTerapeutico.Row, lnActivas.BorderColor, 0);

						cbAceptar.Enabled = true;

					}

					return;

				}

			}

			// En este punto, el principio no existe. Lo a�adimos

			sprTerapeutico.MaxRows++;

			sprTerapeutico.Row = sprTerapeutico.MaxRows;
			sprTerapeutico.Col = COL_DESCRIPCION;
			sprTerapeutico.Text = stDescripcion;
			sprTerapeutico.Col = COL_CODIGO;
			sprTerapeutico.Text = stCodigo;
			sprTerapeutico.Col = COL_USUARIO;
			sprTerapeutico.Text = basAlergiasPac.GstNombreUsuario;

			proOrdena(0);

			cbAceptar.Enabled = true;

		}



		private void cbNuevo_Click(Object eventSender, EventArgs eventArgs)
		{
			int index = Array.IndexOf(this.cbNuevo, eventSender);

			BuscarCie.clsBuscarCie objBuscar = null;

			if (index == 0)
			{
				SprPrincipios.Row = SprPrincipios.ActiveRowIndex;
				objBuscar = new BuscarCie.clsBuscarCie();
				object tempRefParam2 = null;
				bool tempRefParam3 = true;
				string tempRefParam4 = String.Empty;
				objBuscar.Ejecutar(this, basAlergiasPac.rcConexion, ref tempRefParam2, tempRefParam3, tempRefParam4);
			}
			else
			{
				sprTerapeutico.Row = sprTerapeutico.ActiveRowIndex;
				objBuscar = new BuscarCie.clsBuscarCie();
				object tempRefParam5 = this;
				object tempRefParam6 = null;
				bool tempRefParam7 = true;
				objBuscar.grupoTerapeutico(this,basAlergiasPac.rcConexion,ref tempRefParam6, tempRefParam7);
			}

			objBuscar = null;
			proActivaBotones(index);

		}

		private void proNombrePaciente()
		{


			//' O.Frias - 18/10/2013
			//' Se recupera el valor del campo DPACIENT.oalermed, para mostrarlo en pantalla

			string stSql = "SELECT rTrim(rTrim(ISNULL(dape1pac, '')) + ' ' + rTrim(ISNULL(dape2pac, ''))) + ', ' + " + 
			               "rTrim(ISNULL(dnombpac, '')) AS PACIENTE, DPACIENT.oalermed FROM DPACIENT WHERE GIDENPAC = '" + basAlergiasPac.GstPaciente + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, basAlergiasPac.rcConexion);
			DataSet RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);

			if (RrDatos.Tables[0].Rows.Count == 0)
			{
				tbPaciente.Text = "";
				Label1.Text = "";
			}
			else
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				tbPaciente.Text = Convert.ToString(RrDatos.Tables[0].Rows[0]["PACIENTE"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				Label1.Text = ("Al�rgico a: " + (Convert.ToString(RrDatos.Tables[0].Rows[0]["oalermed"]) + "").Trim()).ToUpper();
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrDatos.Close();

		}

		private void proCambiaColorFila(int fila, Color colorFila, int index)
		{

			if (index == 0)
			{
				SprPrincipios.Row = fila;
				int tempForVar = SprPrincipios.MaxCols;
				for (int I = 1; I <= tempForVar; I++)
				{
					SprPrincipios.Col = I;
					SprPrincipios.ForeColor = colorFila;
				}
			}
			else
			{
				sprTerapeutico.Row = fila;
				int tempForVar2 = sprTerapeutico.MaxCols;
				for (int I = 1; I <= tempForVar2; I++)
				{
					sprTerapeutico.Col = I;
					sprTerapeutico.ForeColor = colorFila;
				}
			}
		}

		private void proCargaAlergias()
		{


			// Vaciamos el grid

			SprPrincipios.MaxRows = 0;

			string stSql = "SELECT DPACTIVO.gpactivo, DPACTIVO.dpactivo, DPACALPA.gusuario, SUSUARIO.dusuario, DPACALPA.fborrado " + 
			               "FROM DPACALPA, DPACTIVO, SUSUARIO WHERE " + 
			               "DPACALPA.gidenpac = '" + basAlergiasPac.GstPaciente + "' AND " + 
			               "DPACTIVO.gpactivo = DPACALPA.gpactivo AND " + 
			               "SUSUARIO.gusuario = DPACALPA.gusuario";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, basAlergiasPac.rcConexion);
			DataSet RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);

			foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
			{

				SprPrincipios.MaxRows++;

				SprPrincipios.Row = SprPrincipios.MaxRows;
				SprPrincipios.Col = COL_DESCRIPCION;
				SprPrincipios.Text = Convert.ToString(iteration_row["dpactivo"]).Trim();
				SprPrincipios.Col = COL_CODIGO;
				SprPrincipios.Text = Convert.ToString(iteration_row["gpactivo"]).Trim();
				SprPrincipios.Col = COL_USUARIO;
				SprPrincipios.Text = Convert.ToString(iteration_row["dusuario"]).Trim();
				SprPrincipios.Col = COL_FBORRADO;

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (Convert.IsDBNull(iteration_row["fborrado"]))
				{
					SprPrincipios.Text = "";
				}
				else
				{
					SprPrincipios.Text = Convert.ToDateTime(iteration_row["fborrado"]).ToString("dd/MM/yyyy HH:NN:SS");
				}

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				proCambiaColorFila(SprPrincipios.Row, ColorTranslator.FromOle((Convert.IsDBNull(iteration_row["fborrado"])) ? ColorTranslator.ToOle(lnActivas.BorderColor) : ColorTranslator.ToOle(lnEliminadas.BorderColor)), 0);


			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrDatos.Close();

			proOrdena(0);

			if (SprPrincipios.MaxRows > 0)
			{
				SprPrincipios.Row = SprPrincipios.ActiveRowIndex;

				proActivaBotones(0);
			}
			else
			{
				cbNuevo[0].Enabled = !basAlergiasPac.GSoloLectura;
				cbEliminar[0].Enabled = false;
				cbActivar[0].Enabled = false;
			}

		}


		private void proCargaTerapeutico()
		{


			// Vaciamos el grid

			sprTerapeutico.MaxRows = 0;

			string stSql = "Select DPACALGT.ggrutera, IFMSAL_GTERAP.tgterap, DPACALGT.gusuario, SUSUARIO.dusuario, DPACALGT.fborrado from DPACALGT " + 
			               "INNER JOIN IFMSAL_GTERAP ON DPACALGT.ggrutera = IFMSAL_GTERAP.gterap " + 
			               "INNER JOIN SUSUARIO ON DPACALGT.gusuario = SUSUARIO.gusuario " + 
			               "WHERE DPACALGT.gidenpac = '" + basAlergiasPac.GstPaciente + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, basAlergiasPac.rcConexion);
			DataSet RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);

			foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
			{

				sprTerapeutico.MaxRows++;

				sprTerapeutico.Row = sprTerapeutico.MaxRows;
				sprTerapeutico.Col = COL_DESCRIPCION;
				sprTerapeutico.Text = Convert.ToString(iteration_row["tgterap"]).Trim();
				sprTerapeutico.Col = COL_CODIGO;
				sprTerapeutico.Text = Convert.ToString(iteration_row["ggrutera"]).Trim();
				sprTerapeutico.Col = COL_USUARIO;
				sprTerapeutico.Text = Convert.ToString(iteration_row["dusuario"]).Trim();
				sprTerapeutico.Col = COL_FBORRADO;

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (Convert.IsDBNull(iteration_row["fborrado"]))
				{
					sprTerapeutico.Text = "";
				}
				else
				{
					sprTerapeutico.Text = Convert.ToDateTime(iteration_row["fborrado"]).ToString("dd/MM/yyyy HH:NN:SS");
				}

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				proCambiaColorFila(sprTerapeutico.Row, ColorTranslator.FromOle((Convert.IsDBNull(iteration_row["fborrado"])) ? ColorTranslator.ToOle(lnActivas.BorderColor) : ColorTranslator.ToOle(lnEliminadas.BorderColor)), 1);


			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrDatos.Close();

			proOrdena(1);

			if (sprTerapeutico.MaxRows > 0)
			{
				sprTerapeutico.Row = sprTerapeutico.ActiveRowIndex;

				proActivaBotones(1);
			}
			else
			{
				cbNuevo[1].Enabled = !basAlergiasPac.GSoloLectura;
				cbEliminar[1].Enabled = false;
				cbActivar[1].Enabled = false;
			}

		}


		private void proActivaBotones(int indice)
		{

			bool activaBotones = false;

			if (indice == 0)
			{
				SprPrincipios.Col = COL_CODIGO;
				activaBotones = (ColorTranslator.ToOle(SprPrincipios.ForeColor) == ColorTranslator.ToOle(lnActivas.BorderColor));
			}
			else
			{
				sprTerapeutico.Col = COL_CODIGO;
				activaBotones = (ColorTranslator.ToOle(sprTerapeutico.ForeColor) == ColorTranslator.ToOle(lnActivas.BorderColor));
			}

			if (activaBotones)
			{
				cbNuevo[indice].Enabled = !basAlergiasPac.GSoloLectura;
				cbEliminar[indice].Enabled = !basAlergiasPac.GSoloLectura;
				cbActivar[indice].Enabled = false;
			}
			else
			{
				cbNuevo[indice].Enabled = !basAlergiasPac.GSoloLectura;
				cbEliminar[indice].Enabled = false;
				cbActivar[indice].Enabled = !basAlergiasPac.GSoloLectura;
			}

		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void frmAlergiasPaciente_Load(Object eventSender, EventArgs eventArgs)
		{

			// Tomamos el nombre del paciente

			proNombrePaciente();

			// La rellenamos con las alergias que tenga

			proCargaAlergias();


			//' O.Frias - 21/10/2013
			// Grupo Terap�utico.
			proCargaTerapeutico();

			// Si es modo lectura, desactivamos los botones que no vayamos a usar

			if (basAlergiasPac.GSoloLectura)
			{

				cbNuevo[0].Enabled = false;
				cbEliminar[0].Enabled = false;
				cbActivar[0].Enabled = false;
				cbNuevo[1].Enabled = false;
				cbEliminar[1].Enabled = false;
				cbActivar[1].Enabled = false;
				cbAceptar.Enabled = false;
				SprPrincipios.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
				sprTerapeutico.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;

			}
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);			
		}

		private void frmAlergiasPaciente_Closed(Object eventSender, CancelEventArgs eventArgs)
		{

			DialogResult respuesta = (DialogResult) 0;

            if (cbAceptar.Enabled)
            {

                // Hay cambios sin aplicar. Preguntamos si queremos salir sin guardar los cambios

                //UPGRADE_WARNING: (6021) Casting 'Variant' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
                //UPGRADE_WARNING: (1068) clase_mensaje.RespuestaMensaje() of type Variant is being forced to int. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                //UPGRADE_WARNING: (1068) clase_mensaje.RespuestaMensaje() of type Variant is being forced to VBA.VbMsgBoxResult. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                short tempRefParam = 1125;
                string[] tempRefParam2 = new string[] { "Hay cambios realizados", "salir sin guardar"};
				respuesta = (DialogResult) Convert.ToInt32(basAlergiasPac.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, basAlergiasPac.rcConexion, tempRefParam2));

				if (respuesta != System.Windows.Forms.DialogResult.Yes)
				{
					eventArgs.Cancel = true;
				}

			}

		}

		private void SprPrincipios_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
			int Col = eventArgs.ColumnIndex;
			int Row = eventArgs.RowIndex;

			if (Row <= 0)
			{
				return;
			}

			SprPrincipios.Row = Row;

			proActivaBotones(0);

		}

		private void sprTerapeutico_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{

            int Col = eventArgs.ColumnIndex;
			int Row = eventArgs.RowIndex;

			if (Row <= 0)
			{
				return;
			}

			sprTerapeutico.Row = Row;

			proActivaBotones(1);

		}
	}
}