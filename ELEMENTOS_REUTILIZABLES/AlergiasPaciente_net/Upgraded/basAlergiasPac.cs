using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace AlergiasPaciente
{
	internal static class basAlergiasPac
	{


		public const int iEspacios = 200;

		public static SqlConnection rcConexion = null; // Objeto de Conexi�n
		public static frmAlergiasPaciente nFormAlergias = null; // Formulario de alergias
		public static frmInteracciones nFormInteracciones = null; // Formulario para las interacciones

		public static string GstPaciente = String.Empty; // Gidenpac del paciente
		public static string GstUsuario = String.Empty; // Usuario conectado
		public static string GstNombreUsuario = String.Empty; // Nombre del usuario conectado
		public static bool GSoloLectura = false; // Indicador de modo s�lo lectura

		public static Mensajes.ClassMensajes clase_mensaje = null;

		internal static void ObtenerNombreUsuario()
		{


			string stSql = "SELECT dusuario FROM SUSUARIO WHERE gusuario = '" + GstUsuario.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
			DataSet RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);

			if (RrDatos.Tables[0].Rows.Count != 0)
			{

				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				GstNombreUsuario = Convert.ToString(RrDatos.Tables[0].Rows[0]["dusuario"]).Trim();

			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrDatos.Close();

		}

		internal static string BuscaInteraccionesPrincipio(string ParamItiposer, int ParamGanoregi, int ParamGnumregi, string ParamFarmaco, bool ParamPlantilla = false)
		{

			StringBuilder result = new StringBuilder();
			string stSql = String.Empty;
			DataSet RrDatos = null;
			string[] Paquetes = null;
			string stPaquete1 = String.Empty;
			string stPaquete2 = String.Empty;
			int iPaquete = 0;
			bool bEncontrado = false;

			try
			{

				// Vaciamos las interacciones iniciales

				result = new StringBuilder("");
				iPaquete = 0;

				// Comprobamos si hay interacciones entre los principios activos (sin contar los del propio)

				stSql = "SELECT distinct B.gfarmaco CodFarm1, D.dnomfarm DesFarm1, B.gpactivo CodPpio1, F.dpactivo DesPpio1, " + 
				        "C.gfarmaco CodFarm2, E.dnomfarm DesFarm2, C.gpactivo CodPpio2, G.dpactivo DesPpio2, " + 
				        "A.ctipintinc CodInteraccion, H.dtipintinc DesInteraccion " + 
				        "FROM IFMSFM_INTERINPA A, FCOMPFAR B, FCOMPFAR C, DCODFARMVT D, DCODFARMVT E, DPACTIVO F, DPACTIVO G, " + 
				        "IFMSFM_TIPOINTINC H " + 
				        "WHERE ";

				if (ParamFarmaco.Trim() == "")
				{

					// Comprobamos los f�rmacos del tratamiento entre si

					stSql = stSql + 
					        "B.gfarmaco IN (SELECT distinct gfarmaco FROM EFARMACO WHERE " + 
					        "itiposer = '" + ParamItiposer + "' AND " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "iesttrat IN ('I') AND " + 
					        "ffinmovi IS NULL) AND " + 
					        "C.gfarmaco IN (SELECT distinct gfarmaco FROM EFARMACO WHERE " + 
					        "itiposer = '" + ParamItiposer + "' AND " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "iesttrat IN ('I') AND " + 
					        "ffinmovi IS NULL) AND ";

				}
				else
				{

					// Comprobamos con el f�rmaco en cuesti�n

					stSql = stSql + 
					        "B.gfarmaco IN ('', " + ParamFarmaco + ") AND ";

					if (!ParamPlantilla)
					{
						stSql = stSql + 
						        "(C.gfarmaco IN (SELECT distinct gfarmaco FROM EFARMACO WHERE " + 
						        "itiposer = '" + ParamItiposer + "' AND " + 
						        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
						        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
						        "iesttrat IN ('I') AND " + 
						        "ffinmovi IS NULL) OR " + 
						        "C.gfarmaco IN ('', " + ParamFarmaco + ")) AND ";
					}
					else
					{
						stSql = stSql + 
						        "C.gfarmaco IN (SELECT distinct gfarmaco FROM DPLANFAR WHERE " + 
						        "gplanfar = " + ParamItiposer + " AND gfarmaco <> " + ParamFarmaco + ") AND ";
					}

				}

				stSql = stSql + 
				        "( (A.pactivoa = B.gpactivo AND A.pactivob = C.gpactivo) OR " + 
				        "  (A.pactivob = B.gpactivo AND A.pactivoa = C.gpactivo) ) AND " + 
				        "D.gfarmaco = B.gfarmaco AND " + 
				        "E.gfarmaco = C.gfarmaco AND " + 
				        "F.gpactivo = B.gpactivo AND " + 
				        "G.gpactivo = C.gpactivo AND " + 
				        "H.ctipintinc = A.ctipintinc AND " + 
				        "A.iborrado <> 'S' " + 
				        "ORDER BY F.dpactivo, E.dnomfarm, G.dpactivo";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				// Recorremos las interacciones y las apuntamos

				foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
				{

					// Si no hay f�rmaco, comprobamos

					stPaquete1 = Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodPpio1"]) + "#-#" + Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodPpio2"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					stPaquete2 = Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodPpio2"]) + "#-#" + Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodPpio1"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					bEncontrado = false;

					// Buscamos si el paquete est� en la lista de los ya usados

					for (int I = 0; I <= iPaquete - 1; I++)
					{
						if (Paquetes[I] == stPaquete1 || Paquetes[I] == stPaquete2)
						{
							bEncontrado = true;
							break;
						}
					}

					// Si no est� entre los usados, a�adimos los dos al grupo

					if (!bEncontrado)
					{

						iPaquete += 2;
						Paquetes = ArraysHelper.RedimPreserve(Paquetes, new int[]{iPaquete});
						Paquetes[iPaquete - 2] = stPaquete1;
						Paquetes[iPaquete - 1] = stPaquete2;

					}

					// Si hay ya datos, incluimos un salto de l�nea para hacer m�s clara la informaci�n

					if (!bEncontrado)
					{

						result.Append(
						              ((result.ToString() != "") ? Environment.NewLine + Environment.NewLine : "") + 
						              "El principio activo '" + Convert.ToString(iteration_row["DesPpio1"]).Trim() + 
						              "' del f�rmaco '" + Convert.ToString(iteration_row["DesFarm1"]).Trim() + 
						              "' tiene la interacci�n/incompatibilidad '" + Convert.ToString(iteration_row["DesInteraccion"]).Trim() + 
						              "' con el principio activo '" + Convert.ToString(iteration_row["desppio2"]).Trim() + 
						              "' del f�rmaco '" + Convert.ToString(iteration_row["DesFarm2"]).Trim() + 
						              "' prescrito.");

					}


				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				return result.ToString();
			}
			catch
			{

				return "";
			}
		}

		internal static string BuscaInteraccionesFarmaco(string ParamItiposer, int ParamGanoregi, int ParamGnumregi, string ParamFarmaco, bool ParamPlantilla = false)
		{

			StringBuilder result = new StringBuilder();
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;
				string[] Paquetes = null;
				string stPaquete1 = String.Empty;
				string stPaquete2 = String.Empty;
				int iPaquete = 0;
				bool bEncontrado = false;

				// Vaciamos las interacciones iniciales

				result = new StringBuilder("");
				iPaquete = 0;

				// Comprobamos si hay interacciones entre los f�rmacos activos (sin contar el propio)

				stSql = "SELECT distinct B.gfarmaco CodFarm1, B.dnomfarm DesFarm1, " + 
				        "C.gfarmaco CodFarm2, C.dnomfarm DesFarm2, " + 
				        "A.ctipintinc CodInteraccion, D.dtipintinc DesInteraccion " + 
				        "FROM IFMSFM_INTERINCOM A, DCODFARMVT B, DCODFARMVT C, IFMSFM_TIPOINTINC D " + 
				        "WHERE ";

				if (ParamFarmaco.Trim() == "")
				{

					// Comprobamos los f�rmacos del tratamiento entre si

					stSql = stSql + 
					        "B.gfarmaco IN (SELECT distinct gfarmaco FROM EFARMACO WHERE " + 
					        "itiposer = '" + ParamItiposer + "' AND " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "iesttrat IN ('I') AND " + 
					        "ffinmovi IS NULL) AND " + 
					        "C.gfarmaco IN (SELECT distinct gfarmaco FROM EFARMACO WHERE " + 
					        "itiposer = '" + ParamItiposer + "' AND " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "iesttrat IN ('I') AND " + 
					        "ffinmovi IS NULL) AND ";

				}
				else
				{

					// Comprobamos con el f�rmaco en cuesti�n

					stSql = stSql + 
					        "B.gfarmaco IN ('', " + ParamFarmaco + ") AND ";

					if (!ParamPlantilla)
					{
						stSql = stSql + 
						        "(C.gfarmaco IN (SELECT distinct gfarmaco FROM EFARMACO WHERE " + 
						        "itiposer = '" + ParamItiposer + "' AND " + 
						        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
						        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
						        "iesttrat IN ('I') AND " + 
						        "ffinmovi IS NULL) OR " + 
						        "C.gfarmaco IN ('', " + ParamFarmaco + ")) AND ";
					}
					else
					{
						stSql = stSql + 
						        "C.gfarmaco IN (SELECT distinct gfarmaco FROM DPLANFAR WHERE " + 
						        "gplanfar = " + ParamItiposer + " AND gfarmaco <> " + ParamFarmaco + ") AND ";
					}

				}

				stSql = stSql + 
				        " ( (A.articula = B.gfarmaco AND A.articulb = C.gfarmaco) OR " + 
				        "   (A.articulb = B.gfarmaco AND A.articula = C.gfarmaco) ) AND " + 
				        "D.ctipintinc = A.ctipintinc AND " + 
				        "A.iborrado <> 'S' " + 
				        "ORDER BY C.dnomfarm";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				// Recorremos las interacciones y las apuntamos

				foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
				{

					// Si no hay f�rmaco, comprobamos

					stPaquete1 = Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					stPaquete2 = Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					bEncontrado = false;

					// Buscamos si el paquete est� en la lista de los ya usados

					for (int I = 0; I <= iPaquete - 1; I++)
					{
						if (Paquetes[I] == stPaquete1 || Paquetes[I] == stPaquete2)
						{
							bEncontrado = true;
							break;
						}
					}

					// Si no est� entre los usados, a�adimos los dos al grupo

					if (!bEncontrado)
					{

						iPaquete += 2;
						Paquetes = ArraysHelper.RedimPreserve(Paquetes, new int[]{iPaquete});
						Paquetes[iPaquete - 2] = stPaquete1;
						Paquetes[iPaquete - 1] = stPaquete2;

					}

					// Si hay ya datos, incluimos un salto de l�nea para hacer m�s clara la informaci�n

					if (!bEncontrado)
					{

						result.Append(
						              ((result.ToString() != "") ? Environment.NewLine + Environment.NewLine : "") + 
						              "El f�rmaco '" + Convert.ToString(iteration_row["DesFarm1"]).Trim() + 
						              "' tiene la interacci�n/incompatibilidad '" + Convert.ToString(iteration_row["DesInteraccion"]).Trim() + 
						              "' con el f�rmaco '" + Convert.ToString(iteration_row["DesFarm2"]).Trim() + 
						              "' prescrito.");

					}


				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				return result.ToString();
			}
			catch
			{

				return "";
			}
		}


		internal static string BuscaInteraccionesPrincipioREX(string ParamIdPaciente, string ParamFarmaco)
		{

			StringBuilder result = new StringBuilder();
			string stSql = String.Empty;
			DataSet RrDatos = null;
			string[] Paquetes = null;
			string stPaquete1 = String.Empty;
			string stPaquete2 = String.Empty;
			int iPaquete = 0;
			bool bEncontrado = false;

			try
			{

				// Vaciamos las interacciones iniciales

				result = new StringBuilder("");
				iPaquete = 0;

				// Comprobamos si hay interacciones entre los principios activos (sin contar los del propio)

				stSql = "SELECT distinct B.gfarmaco CodFarm1, D.dnomfarm DesFarm1, B.gpactivo CodPpio1, F.dpactivo DesPpio1, " + 
				        "C.gfarmaco CodFarm2, E.dnomfarm DesFarm2, C.gpactivo CodPpio2, G.dpactivo DesPpio2, " + 
				        "A.ctipintinc CodInteraccion, H.dtipintinc DesInteraccion " + 
				        "FROM IFMSFM_INTERINPA A, FCOMPFAR B, FCOMPFAR C, DCODFARMVB D, DCODFARMVB E, DPACTIVO F, DPACTIVO G, " + 
				        "IFMSFM_TIPOINTINC H " + 
				        "WHERE ";

				if (ParamFarmaco.Trim() == "")
				{

					// Comprobamos los f�rmacos del tratamiento entre si

					stSql = stSql + 
					        "B.gfarmaco IN (SELECT distinct gfarmaco FROM MFARMACO WHERE gidenpac = '" + ParamIdPaciente + "' AND iesttrat IN ('I')) AND " + 
					        "C.gfarmaco IN (SELECT distinct gfarmaco FROM MFARMACO WHERE gidenpac = '" + ParamIdPaciente + "' AND iesttrat IN ('I')) AND ";

				}
				else
				{

					// Comprobamos con el f�rmaco en cuesti�n

					stSql = stSql + 
					        "B.gfarmaco IN ('', " + ParamFarmaco + ") AND ";

					stSql = stSql + 
					        "(C.gfarmaco IN (SELECT distinct gfarmaco FROM MFARMACO WHERE " + 
					        "gidenpac = '" + ParamIdPaciente + "' AND " + 
					        "iesttrat IN ('I')) OR " + 
					        "C.gfarmaco IN ('', " + ParamFarmaco + ")) AND ";


				}

				stSql = stSql + 
				        "( (A.pactivoa = B.gpactivo AND A.pactivob = C.gpactivo) OR " + 
				        "  (A.pactivob = B.gpactivo AND A.pactivoa = C.gpactivo) ) AND " + 
				        "D.gfarmaco = B.gfarmaco AND " + 
				        "E.gfarmaco = C.gfarmaco AND " + 
				        "F.gpactivo = B.gpactivo AND " + 
				        "G.gpactivo = C.gpactivo AND " + 
				        "H.ctipintinc = A.ctipintinc AND " + 
				        "A.iborrado <> 'S' " + 
				        "ORDER BY F.dpactivo, E.dnomfarm, G.dpactivo";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				// Recorremos las interacciones y las apuntamos

				foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
				{

					// Si no hay f�rmaco, comprobamos

					stPaquete1 = Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodPpio1"]) + "#-#" + Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodPpio2"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					stPaquete2 = Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodPpio2"]) + "#-#" + Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodPpio1"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					bEncontrado = false;

					// Buscamos si el paquete est� en la lista de los ya usados

					for (int I = 0; I <= iPaquete - 1; I++)
					{
						if (Paquetes[I] == stPaquete1 || Paquetes[I] == stPaquete2)
						{
							bEncontrado = true;
							break;
						}
					}

					// Si no est� entre los usados, a�adimos los dos al grupo

					if (!bEncontrado)
					{

						iPaquete += 2;
						Paquetes = ArraysHelper.RedimPreserve(Paquetes, new int[]{iPaquete});
						Paquetes[iPaquete - 2] = stPaquete1;
						Paquetes[iPaquete - 1] = stPaquete2;

					}

					// Si hay ya datos, incluimos un salto de l�nea para hacer m�s clara la informaci�n

					if (!bEncontrado)
					{

						result.Append(
						              ((result.ToString() != "") ? Environment.NewLine + Environment.NewLine : "") + 
						              "El principio activo '" + Convert.ToString(iteration_row["DesPpio1"]).Trim() + 
						              "' del f�rmaco '" + Convert.ToString(iteration_row["DesFarm1"]).Trim() + 
						              "' tiene la interacci�n/incompatibilidad '" + Convert.ToString(iteration_row["DesInteraccion"]).Trim() + 
						              "' con el principio activo '" + Convert.ToString(iteration_row["desppio2"]).Trim() + 
						              "' del f�rmaco '" + Convert.ToString(iteration_row["DesFarm2"]).Trim() + 
						              "' prescrito.");

					}


				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				return result.ToString();
			}
			catch
			{

				return "";
			}
		}


		internal static string BuscaInteraccionesFarmacoREX(string ParamIdPaciente, string ParamFarmaco)
		{

			StringBuilder result = new StringBuilder();
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;
				string[] Paquetes = null;
				string stPaquete1 = String.Empty;
				string stPaquete2 = String.Empty;
				int iPaquete = 0;
				bool bEncontrado = false;

				// Vaciamos las interacciones iniciales

				result = new StringBuilder("");
				iPaquete = 0;

				// Comprobamos si hay interacciones entre los f�rmacos activos (sin contar el propio)

				stSql = "SELECT distinct B.gfarmaco CodFarm1, B.dnomfarm DesFarm1, " + 
				        "C.gfarmaco CodFarm2, C.dnomfarm DesFarm2, " + 
				        "A.ctipintinc CodInteraccion, D.dtipintinc DesInteraccion " + 
				        "FROM IFMSFM_INTERINCOM A, DCODFARMVB B, DCODFARMVB C, IFMSFM_TIPOINTINC D " + 
				        "WHERE ";

				if (ParamFarmaco.Trim() == "")
				{

					// Comprobamos los f�rmacos del tratamiento entre si

					stSql = stSql + 
					        "B.gfarmaco IN (SELECT distinct gfarmaco FROM MFARMACO WHERE " + 
					        "gidenpac = '" + ParamIdPaciente + "' AND " + 
					        "iesttrat IN ('I')) AND " + 
					        "C.gfarmaco IN (SELECT distinct gfarmaco FROM MFARMACO WHERE " + 
					        "gidenpac = '" + ParamIdPaciente + "' AND " + 
					        "iesttrat IN ('I')) AND ";

				}
				else
				{

					// Comprobamos con el f�rmaco en cuesti�n

					stSql = stSql + 
					        "B.gfarmaco IN ('', " + ParamFarmaco + ") AND ";


					stSql = stSql + 
					        "(C.gfarmaco IN (SELECT distinct gfarmaco FROM MFARMACO WHERE " + 
					        "gidenpac = '" + ParamIdPaciente + "' AND " + 
					        "iesttrat IN ('I')) OR " + 
					        "C.gfarmaco IN ('', " + ParamFarmaco + ")) AND ";


				}

				stSql = stSql + 
				        " ( (A.articula = B.gfarmaco AND A.articulb = C.gfarmaco) OR " + 
				        "   (A.articulb = B.gfarmaco AND A.articula = C.gfarmaco) ) AND " + 
				        "D.ctipintinc = A.ctipintinc AND " + 
				        "A.iborrado <> 'S' " + 
				        "ORDER BY C.dnomfarm";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				// Recorremos las interacciones y las apuntamos

				foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
				{

					// Si no hay f�rmaco, comprobamos

					stPaquete1 = Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					stPaquete2 = Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					bEncontrado = false;

					// Buscamos si el paquete est� en la lista de los ya usados

					for (int I = 0; I <= iPaquete - 1; I++)
					{
						if (Paquetes[I] == stPaquete1 || Paquetes[I] == stPaquete2)
						{
							bEncontrado = true;
							break;
						}
					}

					// Si no est� entre los usados, a�adimos los dos al grupo

					if (!bEncontrado)
					{

						iPaquete += 2;
						Paquetes = ArraysHelper.RedimPreserve(Paquetes, new int[]{iPaquete});
						Paquetes[iPaquete - 2] = stPaquete1;
						Paquetes[iPaquete - 1] = stPaquete2;

					}

					// Si hay ya datos, incluimos un salto de l�nea para hacer m�s clara la informaci�n

					if (!bEncontrado)
					{

						result.Append(
						              ((result.ToString() != "") ? Environment.NewLine + Environment.NewLine : "") + 
						              "El f�rmaco '" + Convert.ToString(iteration_row["DesFarm1"]).Trim() + 
						              "' tiene la interacci�n/incompatibilidad '" + Convert.ToString(iteration_row["DesInteraccion"]).Trim() + 
						              "' con el f�rmaco '" + Convert.ToString(iteration_row["DesFarm2"]).Trim() + 
						              "' prescrito.");

					}


				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				return result.ToString();
			}
			catch
			{

				return "";
			}
		}


		//PIdpaciente SOLAMENTE viene relleno cuando se accede buscar las interacciones del BOTPlus
		//desde la llamada HayIncompatibilidadREX que se utiliza desde la prescripcion de farmacos desde receta externa
		//Trampeamos el parametro tipoServicio a "PF" cuando se accede dede Programas Farmacologicos
		internal static string buscaInteracionesBotPlus(string tipoServicio, int A�oRegistro, int numRegistro, ref string ParamFarmaco, string PIdpaciente = "", bool bPlantilla = false)
		{
			StringBuilder result = new StringBuilder();
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;
				string nivelGravedad = String.Empty;
				string[] Paquetes = null;
				string stPaquete1 = String.Empty;
				string stPaquete2 = String.Empty;
				int iPaquete = 0;
				bool bEncontrado = false;
				int I = 0;
				bool blnLeve = false;
				bool blnMedio = false;
				bool blnGrave = false;
				bool cabeceraLeve = false;
				bool cabeceraMedio = false;
				bool cabeceraGrave = false;

				StringBuilder ParamFarmaco2 = new StringBuilder();
				ParamFarmaco2 = new StringBuilder(ParamFarmaco);

				result = new StringBuilder("");

				stSql = "SELECT ISNULL(valfanu1, 'N') valfanu1, ISNULL(valfanu2, '') valfanu2 FROM SCONSGLO  where gconsglo  = 'IVALBOTP'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S" && Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper() != "")
					{

						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						blnLeve = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).IndexOf('L') >= 0);
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						blnMedio = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).IndexOf('M') >= 0);
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						blnGrave = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).IndexOf('G') >= 0);

						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrDatos.Close();
						if (PIdpaciente.Trim() != "")
						{
							stSql = " SELECT gfarmaco from mfarmaco where " + 
							        "  iesttrat ='I' AND gidenpac = '" + PIdpaciente + "'";
							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, rcConexion);
							RrDatos = new DataSet();
							tempAdapter_2.Fill(RrDatos);
							if (RrDatos.Tables[0].Rows.Count != 0)
							{
								foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
								{
									ParamFarmaco2.Append(",'" + Convert.ToString(iteration_row["gfarmaco"]).Trim() + "'");
								}
							}
							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							RrDatos.Close();
						}
						else if (bPlantilla)
						{ 
							stSql = " SELECT gfarmaco from dplanfar where " + 
							        "  gplanfar = " + Convert.ToInt32(Double.Parse(tipoServicio)).ToString();
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, rcConexion);
							RrDatos = new DataSet();
							tempAdapter_3.Fill(RrDatos);
							if (RrDatos.Tables[0].Rows.Count != 0)
							{
								foreach (DataRow iteration_row_2 in RrDatos.Tables[0].Rows)
								{
									ParamFarmaco2.Append(",'" + Convert.ToString(iteration_row_2["gfarmaco"]).Trim() + "'");
								}
							}
							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							RrDatos.Close();
						}
						else if (A�oRegistro != 0 && numRegistro != 0 && ParamFarmaco.IndexOf(',') >= 0)
						{ 
							stSql = " SELECT gfarmaco from efarmaco where " + 
							        "  ffinmovi is null and iesttrat <> 'F' and iesttrat <> 'S' AND itiposer = '" + tipoServicio + "' AND " + 
							        " ganoregi = " + A�oRegistro.ToString() + " And gnumregi = " + numRegistro.ToString();
							SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, rcConexion);
							RrDatos = new DataSet();
							tempAdapter_4.Fill(RrDatos);
							if (RrDatos.Tables[0].Rows.Count != 0)
							{
								foreach (DataRow iteration_row_3 in RrDatos.Tables[0].Rows)
								{
									ParamFarmaco2.Append(",'" + Convert.ToString(iteration_row_3["gfarmaco"]).Trim() + "'");
								}
							}
							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							RrDatos.Close();
						}
						else if (A�oRegistro != 0 && numRegistro != 0 && tipoServicio == "PF")
						{ 
							stSql = "select gfarmaco from DPRFPFAR where ganoregi = " + A�oRegistro.ToString() + " and gnumregi = " + numRegistro.ToString() + " ";
							stSql = stSql + " and (ffinfarm is null or ffinfarm >= getdate()) ";
							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSql, rcConexion);
							RrDatos = new DataSet();
							tempAdapter_5.Fill(RrDatos);
							if (RrDatos.Tables[0].Rows.Count != 0)
							{
								foreach (DataRow iteration_row_4 in RrDatos.Tables[0].Rows)
								{
									ParamFarmaco2.Append(",'" + Convert.ToString(iteration_row_4["gfarmaco"]).Trim() + "'");
								}
							}
							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							RrDatos.Close();
						}

						if (A�oRegistro != 0 && numRegistro != 0 && (ParamFarmaco.IndexOf(',') + 1) == 0 && tipoServicio != "PF")
						{
							stSql = "SELECT C.dnomfarm desFarmaco1, F.tpactivo desPActivo1, D.dnomfarm desFarmaco2, G.tpactivo desPActivo2, a.gfarmaco farmaco1, " + 
							        "b.gfarmaco farmaco2, cod1a pActivo1, cod2a pActivo2, INTSIG , intmed " + 
							        "FROM efarmaco a " + 
							        "INNER JOIN DCODFARMVT C ON a.gfarmaco = C.gfarmaco " + 
							        "INNER JOIN efarmaco b ON b.gfarmaco= " + ParamFarmaco + " AND b.ffinmovi is null AND b.iesttrat <> 'F'  AND b.iesttrat <> 'S' " + 
							        "INNER JOIN DCODFARMVT D ON b.gfarmaco = D.gfarmaco " + 
							        "INNER JOIN ifmsfm_interasfar ON (art1 = a.gfarmaco or art2 = a.gfarmaco )and (art1 = " + ParamFarmaco + " or art2 = " + ParamFarmaco + " ) " + 
							        "INNER JOIN IFMSAL_PACTIVO F ON cast(cod1a as char(10)) = F.PACTIVO " + 
							        "INNER JOIN IFMSAL_PACTIVO G ON cast(cod2a as char(10)) = G.PACTIVO " + 
							        "WHERE   A.gfarmaco <> " + ParamFarmaco + " and a.ffinmovi is null AND a.iesttrat <> 'F'  AND a.iesttrat <> 'S' AND a.itiposer = '" + tipoServicio + "' AND " + 
							        "a.ganoregi = " + A�oRegistro.ToString() + " And a.gnumregi = " + numRegistro.ToString() + "  And cod1a Is Not Null And cod2a Is Not Null " + 
							        "GROUP BY C.dnomfarm, F.tpactivo, D.dnomfarm, G.tpactivo, a.gfarmaco, b.gfarmaco, cod1a, cod2a, INTSIG, intmed " + 
							        "ORDER BY a.gfarmaco, cod1a ";
						}
						else
						{

							stSql = "select DISTINCT B.dnomfarm desFarmaco1,  D.tpactivo desPActivo1, C.dnomfarm desFarmaco2,  E.tpactivo desPActivo2, " + 
							        "A.art1 farmaco1,  A.art2 farmaco2,  A.cod1a pActivo1, A.cod2a pActivo2, INTSIG , intmed " + 
							        "from ifmsfm_interasfar A " + 
							        "INNER JOIN DCODFARMVT B ON A.art1 = B.gfarmaco " + 
							        "INNER JOIN DCODFARMVT C ON A.art2 = C.gfarmaco " + 
							        "INNER JOIN IFMSAL_PACTIVO D ON cast(A.cod1a as char(10)) = D.PACTIVO " + 
							        "INNER JOIN IFMSAL_PACTIVO E ON cast(A.cod2a as char(10)) = E.PACTIVO " + 
							        "WHERE A.cod1a Is Not Null And A.cod2a Is Not Null AND (";

							stSql = stSql + "(art1 in (" + ParamFarmaco + ") and art2 in (" + ParamFarmaco2.ToString() + ")) or ";
							stSql = stSql + "(art2 in (" + ParamFarmaco + ") and art1 in (" + ParamFarmaco2.ToString() + ")) ";

							stSql = stSql + " ) " + 
							        "  GROUP BY B.dnomfarm, D.tpactivo, C.dnomfarm,  E.tpactivo,  A.art1,  A.art2,  A.cod1a, A.cod2a, INTSIG , intmed " + 
							        "ORDER BY farmaco1, cod1a ";


						}



						SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSql, rcConexion);
						RrDatos = new DataSet();
						tempAdapter_6.Fill(RrDatos);
						foreach (DataRow iteration_row_5 in RrDatos.Tables[0].Rows)
						{
							if (blnLeve)
							{
								if (Convert.ToDouble(iteration_row_5["intmed"]) == 2 || Convert.ToDouble(iteration_row_5["intmed"]) == 3 || (Convert.ToDouble(iteration_row_5["INTSIG"]) > 6 && Convert.ToDouble(iteration_row_5["intmed"]) != 2 && Convert.ToDouble(iteration_row_5["intmed"]) != 3))
								{
									//                        If Not cabeceraLeve Then
									if (result.ToString() != "")
									{
										result.Append(Environment.NewLine);
									}
									result.Append("Nivel de gravedad (LEVE) " + Environment.NewLine);
									cabeceraLeve = true;
									//                        End If
									result.Append(Environment.NewLine + (Convert.ToString(iteration_row_5["desFarmaco1"]) + "").Trim() + " #-# " + (Convert.ToString(iteration_row_5["desFarmaco2"]) + "").Trim() + ":");
									result.Append(Environment.NewLine + obtieneDetalleInteraccion(Convert.ToString(iteration_row_5["pActivo1"]), Convert.ToString(iteration_row_5["pActivo2"])));
								}
							}
							if (blnGrave)
							{
								if (Convert.ToDouble(iteration_row_5["INTSIG"]) == 1 && Convert.ToDouble(iteration_row_5["intmed"]) != 2 && Convert.ToDouble(iteration_row_5["intmed"]) != 3)
								{
									//                         If Not cabeceraGrave Then
									if (result.ToString() != "")
									{
										result.Append(Environment.NewLine);
									}
									result.Append("Nivel de gravedad (GRAVE) " + Environment.NewLine);
									cabeceraGrave = true;
									//                        End If

									result.Append(Environment.NewLine + (Convert.ToString(iteration_row_5["desFarmaco1"]) + "").Trim() + " #-# " + (Convert.ToString(iteration_row_5["desFarmaco2"]) + "").Trim() + ":");
									result.Append(Environment.NewLine + obtieneDetalleInteraccion(Convert.ToString(iteration_row_5["pActivo1"]), Convert.ToString(iteration_row_5["pActivo2"])));
								}
							}
							if (blnMedio)
							{

								if (Convert.ToDouble(iteration_row_5["INTSIG"]) >= 2 && Convert.ToDouble(iteration_row_5["INTSIG"]) <= 6 && Convert.ToDouble(iteration_row_5["intmed"]) != 2 && Convert.ToDouble(iteration_row_5["intmed"]) != 3)
								{

									if (result.ToString() != "")
									{
										result.Append(Environment.NewLine);
									}
									result.Append("Nivel de gravedad (MEDIO) " + Environment.NewLine);
									result.Append(Environment.NewLine + (Convert.ToString(iteration_row_5["desFarmaco1"]) + "").Trim() + " #-# " + (Convert.ToString(iteration_row_5["desFarmaco2"]) + "").Trim() + ":");
									result.Append(Environment.NewLine + obtieneDetalleInteraccion(Convert.ToString(iteration_row_5["pActivo1"]), Convert.ToString(iteration_row_5["pActivo2"])));
								}
							}
						}
					}
				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();
				RrDatos = null;

				return result.ToString();
			}
			catch
			{

				return "";
			}
		}

		//Para cuando se llame desde Receta Externas
		internal static string buscaInteracionesBotPlusREX(string tipoServicio, int A�oRegistro, int numRegistro, string ParamFarmaco, string PIdpaciente = "", bool bPlantilla = false)
		{
			StringBuilder result = new StringBuilder();
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;
				DataSet RrDatosFarmaco = null;
				DataSet RrDatosFarmacosPrescritos = null;
				DataSet RrDatosInteraciones = null;
				string nivelGravedad = String.Empty;
				int I = 0;
				bool blnLeve = false;
				bool blnMedio = false;
				bool blnGrave = false;
				bool cabeceraLeve = false;
				bool cabeceraMedio = false;
				bool cabeceraGrave = false;

				StringBuilder ParamFarmaco2 = new StringBuilder();
				ParamFarmaco2 = new StringBuilder(ParamFarmaco);

				result = new StringBuilder("");

				stSql = "SELECT ISNULL(valfanu1, 'N') valfanu1, ISNULL(valfanu2, '') valfanu2 FROM SCONSGLO  where gconsglo  = 'IVALBOTP'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S" && Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper() != "")
					{

						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						blnLeve = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).IndexOf('L') >= 0);
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						blnMedio = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).IndexOf('M') >= 0);
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						blnGrave = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).IndexOf('G') >= 0);

						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrDatos.Close();
						stSql = " SELECT gfarmaco from mfarmaco where " + 
						        "  iesttrat ='I' AND gidenpac = '" + PIdpaciente + "'";
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, rcConexion);
						RrDatos = new DataSet();
						tempAdapter_2.Fill(RrDatos);
						if (RrDatos.Tables[0].Rows.Count != 0)
						{
							foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
							{
								if (ParamFarmaco2.ToString().Trim() != "")
								{
									ParamFarmaco2.Append(",'" + Convert.ToString(iteration_row["gfarmaco"]).Trim() + "'");
								}
								else
								{
									ParamFarmaco2 = new StringBuilder("'" + Convert.ToString(iteration_row["gfarmaco"]).Trim() + "'");
								}
							}
						}

						stSql = "select fcompfar.*,ifmsal_pactivo.tpactivo,dcodfarmvb.dnomfarm " + 
						        "from fcompfar inner join dcodfarmvb on fcompfar.gfarmaco = dcodfarmvb.gfarmaco " + 
						        "inner join ifmsal_pactivo on fcompfar.gpactivo = ifmsal_pactivo.pactivo " + 
						        "where fcompfar.gfarmaco in (" + ParamFarmaco + ")";
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, rcConexion);
						RrDatosFarmaco = new DataSet();
						tempAdapter_3.Fill(RrDatosFarmaco);
						if (RrDatosFarmaco.Tables[0].Rows.Count != 0)
						{
							stSql = "select fcompfar.*,ifmsal_pactivo.tpactivo,dcodfarmvb.dnomfarm " + 
							        "from fcompfar inner join dcodfarmvb on fcompfar.gfarmaco = dcodfarmvb.gfarmaco " + 
							        "inner join ifmsal_pactivo on fcompfar.gpactivo = ifmsal_pactivo.pactivo " + 
							        "where fcompfar.gfarmaco in (" + ParamFarmaco2.ToString() + ")";
							SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, rcConexion);
							RrDatosFarmacosPrescritos = new DataSet();
							tempAdapter_4.Fill(RrDatosFarmacosPrescritos);
							if (RrDatosFarmacosPrescritos.Tables[0].Rows.Count != 0)
							{
								//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
								RrDatosFarmaco.MoveFirst();
								//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
								RrDatosFarmacosPrescritos.MoveFirst();
								foreach (DataRow iteration_row_2 in RrDatosFarmaco.Tables[0].Rows)
								{
									foreach (DataRow iteration_row_3 in RrDatosFarmacosPrescritos.Tables[0].Rows)
									{
										stSql = "select cod1A,cod2A,intsig,intmed from ifmsfm_interas where (cod1A = " + Convert.ToString(iteration_row_2["gpactivo"]) + " and cod2A = " + Convert.ToString(iteration_row_3["gpactivo"]) + ") or " + 
										        " (cod2A = " + Convert.ToString(iteration_row_2["gpactivo"]) + " and cod1A = " + Convert.ToString(iteration_row_3["gpactivo"]) + ")";

										SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSql, rcConexion);
										RrDatosInteraciones = new DataSet();
										tempAdapter_5.Fill(RrDatosInteraciones);
										if (RrDatosInteraciones.Tables[0].Rows.Count != 0)
										{
											if (blnLeve)
											{
												if (Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["intmed"]) == 2 || Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["intmed"]) == 3 || (Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["INTSIG"]) > 6 && Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["intmed"]) != 2 && Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["intmed"]) != 3))
												{
													if (result.ToString() != "")
													{
														result.Append(Environment.NewLine);
													}
													result.Append("Nivel de gravedad (LEVE) " + Environment.NewLine);
													cabeceraLeve = true;
													result.Append(Environment.NewLine + (Convert.ToString(iteration_row_2["dnomfarm"]) + "").Trim() + " #-# " + (Convert.ToString(iteration_row_3["dnomfarm"]) + "").Trim() + ":");
													result.Append(Environment.NewLine + obtieneDetalleInteraccion(Convert.ToString(RrDatosInteraciones.Tables[0].Rows[0]["cod1A"]), Convert.ToString(RrDatosInteraciones.Tables[0].Rows[0]["cod2A"])));
												}
											}
											if (blnGrave)
											{
												if (Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["INTSIG"]) == 1 && Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["intmed"]) != 2 && Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["intmed"]) != 3)
												{
													if (result.ToString() != "")
													{
														result.Append(Environment.NewLine);
													}
													result.Append("Nivel de gravedad (GRAVE) " + Environment.NewLine);
													cabeceraGrave = true;
													result.Append(Environment.NewLine + (Convert.ToString(iteration_row_2["dnomfarm"]) + "").Trim() + " #-# " + (Convert.ToString(iteration_row_3["dnomfarm"]) + "").Trim() + ":");
													result.Append(Environment.NewLine + obtieneDetalleInteraccion(Convert.ToString(RrDatosInteraciones.Tables[0].Rows[0]["cod1A"]), Convert.ToString(RrDatosInteraciones.Tables[0].Rows[0]["cod2A"])));
												}
											}
											if (blnMedio)
											{
												if (Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["INTSIG"]) >= 2 && Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["INTSIG"]) <= 6 && Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["intmed"]) != 2 && Convert.ToDouble(RrDatosInteraciones.Tables[0].Rows[0]["intmed"]) != 3)
												{
													if (result.ToString() != "")
													{
														result.Append(Environment.NewLine);
													}
													result.Append("Nivel de gravedad (MEDIO) " + Environment.NewLine);
													result.Append(Environment.NewLine + (Convert.ToString(iteration_row_2["dnomfarm"]) + "").Trim() + " #-# " + (Convert.ToString(iteration_row_3["dnomfarm"]) + "").Trim() + ":");
													result.Append(Environment.NewLine + obtieneDetalleInteraccion(Convert.ToString(RrDatosInteraciones.Tables[0].Rows[0]["cod1A"]), Convert.ToString(RrDatosInteraciones.Tables[0].Rows[0]["cod2A"])));
												}
											}
										}
										//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatosInteraciones.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										RrDatosInteraciones.Close();
									}
									//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
									RrDatosFarmacosPrescritos.MoveFirst();
								}
							}
							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatosFarmacosPrescritos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							RrDatosFarmacosPrescritos.Close();
						}
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatosFarmaco.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrDatosFarmaco.Close();
					}
				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();
				RrDatos = null;

				return result.ToString();
			}
			catch
			{

				return "";
			}
		}


		//UPGRADE_NOTE: (7001) The following declaration (arrayVacio) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private bool arrayVacio(object arrayRecibido)
		//{
				//
				//bool result = false;
				//try
				//{
					//if (((Array) arrayRecibido).GetUpperBound(0) >= 0)
					//{
						//result = false;
					//}
					//
					//return result;
				//}
				//catch
				//{
					//return true;
				//}
		//}


		private static string obtieneDetalleInteraccion(string pAcivo1, string pActivo2)
		{
			string result = String.Empty;
			string stEfecto = String.Empty;
			StringBuilder cadena = new StringBuilder();

			string stSql = "Select ISNULL(IFMSFM_INTSEN.nombre, '') sentido, ISNULL(IFMSFM_INTSEN.nombre, '') significacion, ISNULL(IFMSFM_INTNAT.nombre, '') naturaleza, " + 
			               "ISNULL(IFMSFM_INTMED.nombre, '') medidas, ISNULL(titulo, '') titulo, ISNULL(A.texto_com, '') efectos, ISNULL(B.texto_com, '') improtancia, " + 
			               "ISNULL(C.texto_com, '') mecanismo, ISNULL(D.texto_com, '') evidencias, ISNULL(E.texto_com, '') referencias " + 
			               "From ifmsfm_interas " + 
			               "LEFT JOIN IFMSFM_INTSEN ON ifmsfm_interas.INTSEN = IFMSFM_INTSEN.INTSEN " + 
			               "LEFT JOIN IFMSFM_INTSIG ON ifmsfm_interas.intsig = IFMSFM_INTSIG.intsig " + 
			               "LEFT JOIN IFMSFM_INTNAT ON ifmsfm_interas.INTNAT = IFMSFM_INTNAT.INTNAT " + 
			               "LEFT JOIN IFMSFM_INTMED ON ifmsfm_interas.INTMED = IFMSFM_INTMED.INTMED " + 
			               "LEFT JOIN IFMSFM_TEXTO A ON ifmsfm_interas.EFECTO = A.coditext " + 
			               "LEFT JOIN IFMSFM_TEXTO B ON ifmsfm_interas.importancia  = B.coditext " + 
			               "LEFT JOIN IFMSFM_TEXTO C ON ifmsfm_interas.MECANISMO  = C.coditext " + 
			               "LEFT JOIN IFMSFM_TEXTO D ON ifmsfm_interas.EVIDENCIAS  = D.coditext " + 
			               "LEFT JOIN IFMSFM_TEXTO E ON ifmsfm_interas.REFERENCIAs  = E.coditext " + 
			               "WHERE cod1A = '" + pAcivo1 + "' and cod2A='" + pActivo2 + "'";


			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["titulo"]).Trim() != "")
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					result = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["titulo"]) + "").Trim();
				}
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				stEfecto = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["efectos"]) + "").Trim();
				if (stEfecto.Trim() != "")
				{
					stEfecto = stEfecto.Trim();
					cadena = new StringBuilder("");
					for (int contador = 1; contador <= stEfecto.Length; contador++)
					{
						if (Strings.Asc(stEfecto.Substring(contador - 1, Math.Min(1, stEfecto.Length - (contador - 1)))[0]) != 13 && Strings.Asc(stEfecto.Substring(contador - 1, Math.Min(1, stEfecto.Length - (contador - 1)))[0]) != 10)
						{
							cadena.Append(stEfecto.Substring(contador - 1, Math.Min(1, stEfecto.Length - (contador - 1))));
						}
						else
						{
							cadena.Append(" ");
						}
					}

					result = result + " ( " + cadena.ToString().Trim() + " ) " + Environment.NewLine;
				}
			}


			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rdoTemp.Close();
			return result;
		}

		internal static string BuscaInteraccionesPrincipioPF(int ParamGanoregi, int ParamGnumregi, string ParamFarmaco)
		{

			StringBuilder result = new StringBuilder();
			string stSql = String.Empty;
			DataSet RrDatos = null;
			string[] Paquetes = null;
			string stPaquete1 = String.Empty;
			string stPaquete2 = String.Empty;
			int iPaquete = 0;
			bool bEncontrado = false;

			try
			{

				// Vaciamos las interacciones iniciales

				result = new StringBuilder("");
				iPaquete = 0;

				// Comprobamos si hay interacciones entre los principios activos (sin contar los del propio)

				stSql = "SELECT distinct B.gfarmaco CodFarm1, D.dnomfarm DesFarm1, B.gpactivo CodPpio1, F.dpactivo DesPpio1, " + 
				        "C.gfarmaco CodFarm2, E.dnomfarm DesFarm2, C.gpactivo CodPpio2, G.dpactivo DesPpio2, " + 
				        "A.ctipintinc CodInteraccion, H.dtipintinc DesInteraccion " + 
				        "FROM IFMSFM_INTERINPA A, FCOMPFAR B, FCOMPFAR C, DCODFARMVT D, DCODFARMVT E, DPACTIVO F, DPACTIVO G, " + 
				        "IFMSFM_TIPOINTINC H " + 
				        "WHERE ";

				if (ParamFarmaco.Trim() == "")
				{

					// Comprobamos los f�rmacos del tratamiento entre si
					stSql = stSql + 
					        "B.gfarmaco IN (SELECT distinct gfarmaco FROM DPRFPFAR WHERE " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "(ffinfarm is null or ffinfarm >=getdate())) AND " + 
					        "C.gfarmaco IN (SELECT distinct gfarmaco FROM DPRFPFAR WHERE " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "(ffinfarm is null or ffinfarm >=getdate())) AND ";

				}
				else
				{

					// Comprobamos con el f�rmaco en cuesti�n

					stSql = stSql + 
					        "B.gfarmaco IN ('', " + ParamFarmaco + ") AND ";

					stSql = stSql + 
					        "(C.gfarmaco IN (SELECT distinct gfarmaco FROM DPRFPFAR WHERE " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "(ffinfarm is null or ffinfarm >=getdate())) OR " + 
					        "C.gfarmaco IN ('', " + ParamFarmaco + ")) AND ";

				}

				stSql = stSql + 
				        "( (A.pactivoa = B.gpactivo AND A.pactivob = C.gpactivo) OR " + 
				        "  (A.pactivob = B.gpactivo AND A.pactivoa = C.gpactivo) ) AND " + 
				        "D.gfarmaco = B.gfarmaco AND " + 
				        "E.gfarmaco = C.gfarmaco AND " + 
				        "F.gpactivo = B.gpactivo AND " + 
				        "G.gpactivo = C.gpactivo AND " + 
				        "H.ctipintinc = A.ctipintinc AND " + 
				        "A.iborrado <> 'S' " + 
				        "ORDER BY F.dpactivo, E.dnomfarm, G.dpactivo";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				// Recorremos las interacciones y las apuntamos

				foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
				{

					// Si no hay f�rmaco, comprobamos

					stPaquete1 = Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodPpio1"]) + "#-#" + Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodPpio2"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					stPaquete2 = Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodPpio2"]) + "#-#" + Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodPpio1"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					bEncontrado = false;

					// Buscamos si el paquete est� en la lista de los ya usados

					for (int I = 0; I <= iPaquete - 1; I++)
					{
						if (Paquetes[I] == stPaquete1 || Paquetes[I] == stPaquete2)
						{
							bEncontrado = true;
							break;
						}
					}

					// Si no est� entre los usados, a�adimos los dos al grupo

					if (!bEncontrado)
					{

						iPaquete += 2;
						Paquetes = ArraysHelper.RedimPreserve(Paquetes, new int[]{iPaquete});
						Paquetes[iPaquete - 2] = stPaquete1;
						Paquetes[iPaquete - 1] = stPaquete2;

					}

					// Si hay ya datos, incluimos un salto de l�nea para hacer m�s clara la informaci�n

					if (!bEncontrado)
					{

						result.Append(
						              ((result.ToString() != "") ? Environment.NewLine + Environment.NewLine : "") + 
						              "El principio activo '" + Convert.ToString(iteration_row["DesPpio1"]).Trim() + 
						              "' del f�rmaco '" + Convert.ToString(iteration_row["DesFarm1"]).Trim() + 
						              "' tiene la interacci�n/incompatibilidad '" + Convert.ToString(iteration_row["DesInteraccion"]).Trim() + 
						              "' con el principio activo '" + Convert.ToString(iteration_row["desppio2"]).Trim() + 
						              "' del f�rmaco '" + Convert.ToString(iteration_row["DesFarm2"]).Trim() + 
						              "' prescrito.");

					}


				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				return result.ToString();
			}
			catch
			{

				return "";
			}
		}

		internal static string BuscaInteraccionesFarmacoPF(int ParamGanoregi, int ParamGnumregi, string ParamFarmaco)
		{

			StringBuilder result = new StringBuilder();
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;
				string[] Paquetes = null;
				string stPaquete1 = String.Empty;
				string stPaquete2 = String.Empty;
				int iPaquete = 0;
				bool bEncontrado = false;

				// Vaciamos las interacciones iniciales

				result = new StringBuilder("");
				iPaquete = 0;

				// Comprobamos si hay interacciones entre los f�rmacos activos (sin contar el propio)

				stSql = "SELECT distinct B.gfarmaco CodFarm1, B.dnomfarm DesFarm1, " + 
				        "C.gfarmaco CodFarm2, C.dnomfarm DesFarm2, " + 
				        "A.ctipintinc CodInteraccion, D.dtipintinc DesInteraccion " + 
				        "FROM IFMSFM_INTERINCOM A, DCODFARMVT B, DCODFARMVT C, IFMSFM_TIPOINTINC D " + 
				        "WHERE ";

				if (ParamFarmaco.Trim() == "")
				{

					// Comprobamos los f�rmacos del tratamiento entre si
					stSql = stSql + 
					        "B.gfarmaco IN (SELECT distinct gfarmaco FROM DPRFPFAR WHERE " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "(ffinfarm is null or ffinfarm >=getdate())) AND " + 
					        "C.gfarmaco IN (SELECT distinct gfarmaco FROM DPRFPFAR WHERE " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "(ffinfarm is null or ffinfarm >=getdate())) AND ";

				}
				else
				{

					// Comprobamos con el f�rmaco en cuesti�n

					stSql = stSql + 
					        "B.gfarmaco IN ('', " + ParamFarmaco + ") AND ";

					stSql = stSql + 
					        "(C.gfarmaco IN (SELECT distinct gfarmaco FROM DPRFPFAR WHERE " + 
					        "ganoregi = " + ParamGanoregi.ToString() + " AND " + 
					        "gnumregi = " + ParamGnumregi.ToString() + " AND " + 
					        "(ffinfarm is null or ffinfarm >=getdate())) OR " + 
					        "C.gfarmaco IN ('', " + ParamFarmaco + ")) AND ";

				}

				stSql = stSql + 
				        " ( (A.articula = B.gfarmaco AND A.articulb = C.gfarmaco) OR " + 
				        "   (A.articulb = B.gfarmaco AND A.articula = C.gfarmaco) ) AND " + 
				        "D.ctipintinc = A.ctipintinc AND " + 
				        "A.iborrado <> 'S' " + 
				        "ORDER BY C.dnomfarm";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				// Recorremos las interacciones y las apuntamos

				foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
				{

					// Si no hay f�rmaco, comprobamos

					stPaquete1 = Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					stPaquete2 = Convert.ToString(iteration_row["CodFarm2"]) + "#-#" + Convert.ToString(iteration_row["CodFarm1"]) + "#-#" + Convert.ToString(iteration_row["CodInteraccion"]);
					bEncontrado = false;

					// Buscamos si el paquete est� en la lista de los ya usados

					for (int I = 0; I <= iPaquete - 1; I++)
					{
						if (Paquetes[I] == stPaquete1 || Paquetes[I] == stPaquete2)
						{
							bEncontrado = true;
							break;
						}
					}

					// Si no est� entre los usados, a�adimos los dos al grupo

					if (!bEncontrado)
					{

						iPaquete += 2;
						Paquetes = ArraysHelper.RedimPreserve(Paquetes, new int[]{iPaquete});
						Paquetes[iPaquete - 2] = stPaquete1;
						Paquetes[iPaquete - 1] = stPaquete2;

					}

					// Si hay ya datos, incluimos un salto de l�nea para hacer m�s clara la informaci�n

					if (!bEncontrado)
					{

						result.Append(
						              ((result.ToString() != "") ? Environment.NewLine + Environment.NewLine : "") + 
						              "El f�rmaco '" + Convert.ToString(iteration_row["DesFarm1"]).Trim() + 
						              "' tiene la interacci�n/incompatibilidad '" + Convert.ToString(iteration_row["DesInteraccion"]).Trim() + 
						              "' con el f�rmaco '" + Convert.ToString(iteration_row["DesFarm2"]).Trim() + 
						              "' prescrito.");

					}


				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				return result.ToString();
			}
			catch
			{

				return "";
			}
		}
	}
}