using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ProcesoIMDHOpen
{
	partial class Opcion
	{

		#region "Upgrade Support "
		private static Opcion m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Opcion DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Opcion();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private void Ctx_mnu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			System.Collections.Generic.List<System.Windows.Forms.ToolStripItem> list = new System.Collections.Generic.List<System.Windows.Forms.ToolStripItem>();
			Ctx_mnu.Items.Clear();
			//We are moving the submenus from original menu to the context menu before displaying it
			foreach (System.Windows.Forms.ToolStripItem item in mnu.DropDownItems)
			{
				list.Add(item);
			}
			foreach (System.Windows.Forms.ToolStripItem item in list)
			{
				Ctx_mnu.Items.Add(item);
			}
			e.Cancel = false;
		}
		private void Ctx_mnu_Closing(object sender, System.Windows.Forms.ToolStripDropDownClosingEventArgs e)
		{
			System.Collections.Generic.List<System.Windows.Forms.ToolStripItem> list = new System.Collections.Generic.List<System.Windows.Forms.ToolStripItem>();
			//We are moving the submenus the context menu back to the original menu after displaying
			foreach (System.Windows.Forms.ToolStripItem item in Ctx_mnu.Items)
			{
				list.Add(item);
			}
			foreach (System.Windows.Forms.ToolStripItem item in list)
			{
				mnu.DropDownItems.Add(item);
			}
		}
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_SubMnu_0", "_SubMnu_1", "_SubMnu_2", "_SubMnu_3", "_SubMnu_4", "_SubMnu_5", "_SubMnu_6", "_SubMnu_7", "_SubMnu_8", "_SubMnu_9", "_SubMnu_10", "_SubMnu_11", "_SubMnu_12", "_SubMnu_13", "_SubMnu_14", "_SubMnu_15", "_SubMnu_16", "_SubMnu_17", "_SubMnu_18", "_SubMnu_19", "_SubMnu_20", "_SubMnu_21", "_SubMnu_22", "_SubMnu_23", "_SubMnu_24", "_SubMnu_25", "_SubMnu_26", "_SubMnu_27", "_SubMnu_28", "_SubMnu_29", "_SubMnu_30", "_SubMnu_31", "_SubMnu_32", "_SubMnu_33", "_SubMnu_34", "_SubMnu_35", "_SubMnu_36", "_SubMnu_37", "_SubMnu_38", "_SubMnu_39", "_SubMnu_40", "_SubMnu_41", "_SubMnu_42", "_SubMnu_43", "_SubMnu_44", "_SubMnu_45", "_SubMnu_46", "_SubMnu_47", "_SubMnu_48", "_SubMnu_49", "mnu", "MainMenu1", "SubMnu", "Ctx_mnu"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_0;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_1;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_2;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_3;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_4;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_5;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_6;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_7;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_8;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_9;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_10;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_11;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_12;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_13;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_14;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_15;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_16;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_17;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_18;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_19;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_20;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_21;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_22;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_23;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_24;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_25;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_26;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_27;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_28;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_29;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_30;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_31;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_32;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_33;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_34;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_35;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_36;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_37;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_38;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_39;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_40;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_41;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_42;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_43;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_44;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_45;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_46;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_47;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_48;
		private System.Windows.Forms.ToolStripMenuItem _SubMnu_49;
		public System.Windows.Forms.ToolStripMenuItem mnu;
		public System.Windows.Forms.MenuStrip MainMenu1;
		public System.Windows.Forms.ToolStripItem[] SubMnu = new System.Windows.Forms.ToolStripItem[50];
		public System.Windows.Forms.ContextMenuStrip Ctx_mnu;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Opcion));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.MainMenu1 = new System.Windows.Forms.MenuStrip();
			this.mnu = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_0 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_1 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_2 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_3 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_4 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_5 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_6 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_7 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_8 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_9 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_10 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_11 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_12 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_13 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_14 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_15 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_16 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_17 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_18 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_19 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_20 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_21 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_22 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_23 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_24 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_25 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_26 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_27 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_28 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_29 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_30 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_31 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_32 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_33 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_34 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_35 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_36 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_37 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_38 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_39 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_40 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_41 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_42 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_43 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_44 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_45 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_46 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_47 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_48 = new System.Windows.Forms.ToolStripMenuItem();
			this._SubMnu_49 = new System.Windows.Forms.ToolStripMenuItem();
			this.SuspendLayout();
			//Ctx_mnu
			this.Ctx_mnu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.Ctx_mnu.Size = new System.Drawing.Size(153, 26);
			this.Ctx_mnu.Opening += new System.ComponentModel.CancelEventHandler(this.Ctx_mnu_Opening);
			this.Ctx_mnu.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.Ctx_mnu_Closing);
			// 
			// MainMenu1
			// 
			this.MainMenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[]{this.mnu});
			// 
			// mnu
			// 
			this.mnu.Available = false;
			this.mnu.Checked = false;
			this.mnu.Enabled = true;
			this.mnu.Name = "mnu";
			this.mnu.Text = "";
			this.mnu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[]{this._SubMnu_0, this._SubMnu_1, this._SubMnu_2, this._SubMnu_3, this._SubMnu_4, this._SubMnu_5, this._SubMnu_6, this._SubMnu_7, this._SubMnu_8, this._SubMnu_9, this._SubMnu_10, this._SubMnu_11, this._SubMnu_12, this._SubMnu_13, this._SubMnu_14, this._SubMnu_15, this._SubMnu_16, this._SubMnu_17, this._SubMnu_18, this._SubMnu_19, this._SubMnu_20, this._SubMnu_21, this._SubMnu_22, this._SubMnu_23, this._SubMnu_24, this._SubMnu_25, this._SubMnu_26, this._SubMnu_27, this._SubMnu_28, this._SubMnu_29, this._SubMnu_30, this._SubMnu_31, this._SubMnu_32, this._SubMnu_33, this._SubMnu_34, this._SubMnu_35, this._SubMnu_36, this._SubMnu_37, this._SubMnu_38, this._SubMnu_39, this._SubMnu_40, this._SubMnu_41, this._SubMnu_42, this._SubMnu_43, this._SubMnu_44, this._SubMnu_45, this._SubMnu_46, this._SubMnu_47, this._SubMnu_48, this._SubMnu_49});
			// 
			// _SubMnu_0
			// 
			this._SubMnu_0.Available = true;
			this._SubMnu_0.Checked = false;
			this._SubMnu_0.Enabled = true;
			this._SubMnu_0.Name = "_SubMnu_0";
			this._SubMnu_0.Text = "";
			this._SubMnu_0.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_1
			// 
			this._SubMnu_1.Available = true;
			this._SubMnu_1.Checked = false;
			this._SubMnu_1.Enabled = true;
			this._SubMnu_1.Name = "_SubMnu_1";
			this._SubMnu_1.Text = "";
			this._SubMnu_1.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_2
			// 
			this._SubMnu_2.Available = true;
			this._SubMnu_2.Checked = false;
			this._SubMnu_2.Enabled = true;
			this._SubMnu_2.Name = "_SubMnu_2";
			this._SubMnu_2.Text = "";
			this._SubMnu_2.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_3
			// 
			this._SubMnu_3.Available = true;
			this._SubMnu_3.Checked = false;
			this._SubMnu_3.Enabled = true;
			this._SubMnu_3.Name = "_SubMnu_3";
			this._SubMnu_3.Text = "";
			this._SubMnu_3.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_4
			// 
			this._SubMnu_4.Available = true;
			this._SubMnu_4.Checked = false;
			this._SubMnu_4.Enabled = true;
			this._SubMnu_4.Name = "_SubMnu_4";
			this._SubMnu_4.Text = "";
			this._SubMnu_4.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_5
			// 
			this._SubMnu_5.Available = true;
			this._SubMnu_5.Checked = false;
			this._SubMnu_5.Enabled = true;
			this._SubMnu_5.Name = "_SubMnu_5";
			this._SubMnu_5.Text = "";
			this._SubMnu_5.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_6
			// 
			this._SubMnu_6.Available = true;
			this._SubMnu_6.Checked = false;
			this._SubMnu_6.Enabled = true;
			this._SubMnu_6.Name = "_SubMnu_6";
			this._SubMnu_6.Text = "";
			this._SubMnu_6.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_7
			// 
			this._SubMnu_7.Available = true;
			this._SubMnu_7.Checked = false;
			this._SubMnu_7.Enabled = true;
			this._SubMnu_7.Name = "_SubMnu_7";
			this._SubMnu_7.Text = "";
			this._SubMnu_7.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_8
			// 
			this._SubMnu_8.Available = true;
			this._SubMnu_8.Checked = false;
			this._SubMnu_8.Enabled = true;
			this._SubMnu_8.Name = "_SubMnu_8";
			this._SubMnu_8.Text = "";
			this._SubMnu_8.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_9
			// 
			this._SubMnu_9.Available = true;
			this._SubMnu_9.Checked = false;
			this._SubMnu_9.Enabled = true;
			this._SubMnu_9.Name = "_SubMnu_9";
			this._SubMnu_9.Text = "";
			this._SubMnu_9.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_10
			// 
			this._SubMnu_10.Available = true;
			this._SubMnu_10.Checked = false;
			this._SubMnu_10.Enabled = true;
			this._SubMnu_10.Name = "_SubMnu_10";
			this._SubMnu_10.Text = "";
			this._SubMnu_10.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_11
			// 
			this._SubMnu_11.Available = true;
			this._SubMnu_11.Checked = false;
			this._SubMnu_11.Enabled = true;
			this._SubMnu_11.Name = "_SubMnu_11";
			this._SubMnu_11.Text = "";
			this._SubMnu_11.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_12
			// 
			this._SubMnu_12.Available = true;
			this._SubMnu_12.Checked = false;
			this._SubMnu_12.Enabled = true;
			this._SubMnu_12.Name = "_SubMnu_12";
			this._SubMnu_12.Text = "";
			this._SubMnu_12.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_13
			// 
			this._SubMnu_13.Available = true;
			this._SubMnu_13.Checked = false;
			this._SubMnu_13.Enabled = true;
			this._SubMnu_13.Name = "_SubMnu_13";
			this._SubMnu_13.Text = "";
			this._SubMnu_13.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_14
			// 
			this._SubMnu_14.Available = true;
			this._SubMnu_14.Checked = false;
			this._SubMnu_14.Enabled = true;
			this._SubMnu_14.Name = "_SubMnu_14";
			this._SubMnu_14.Text = "";
			this._SubMnu_14.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_15
			// 
			this._SubMnu_15.Available = true;
			this._SubMnu_15.Checked = false;
			this._SubMnu_15.Enabled = true;
			this._SubMnu_15.Name = "_SubMnu_15";
			this._SubMnu_15.Text = "";
			this._SubMnu_15.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_16
			// 
			this._SubMnu_16.Available = true;
			this._SubMnu_16.Checked = false;
			this._SubMnu_16.Enabled = true;
			this._SubMnu_16.Name = "_SubMnu_16";
			this._SubMnu_16.Text = "";
			this._SubMnu_16.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_17
			// 
			this._SubMnu_17.Available = true;
			this._SubMnu_17.Checked = false;
			this._SubMnu_17.Enabled = true;
			this._SubMnu_17.Name = "_SubMnu_17";
			this._SubMnu_17.Text = "";
			this._SubMnu_17.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_18
			// 
			this._SubMnu_18.Available = true;
			this._SubMnu_18.Checked = false;
			this._SubMnu_18.Enabled = true;
			this._SubMnu_18.Name = "_SubMnu_18";
			this._SubMnu_18.Text = "";
			this._SubMnu_18.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_19
			// 
			this._SubMnu_19.Available = true;
			this._SubMnu_19.Checked = false;
			this._SubMnu_19.Enabled = true;
			this._SubMnu_19.Name = "_SubMnu_19";
			this._SubMnu_19.Text = "";
			this._SubMnu_19.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_20
			// 
			this._SubMnu_20.Available = true;
			this._SubMnu_20.Checked = false;
			this._SubMnu_20.Enabled = true;
			this._SubMnu_20.Name = "_SubMnu_20";
			this._SubMnu_20.Text = "";
			this._SubMnu_20.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_21
			// 
			this._SubMnu_21.Available = true;
			this._SubMnu_21.Checked = false;
			this._SubMnu_21.Enabled = true;
			this._SubMnu_21.Name = "_SubMnu_21";
			this._SubMnu_21.Text = "";
			this._SubMnu_21.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_22
			// 
			this._SubMnu_22.Available = true;
			this._SubMnu_22.Checked = false;
			this._SubMnu_22.Enabled = true;
			this._SubMnu_22.Name = "_SubMnu_22";
			this._SubMnu_22.Text = "";
			this._SubMnu_22.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_23
			// 
			this._SubMnu_23.Available = true;
			this._SubMnu_23.Checked = false;
			this._SubMnu_23.Enabled = true;
			this._SubMnu_23.Name = "_SubMnu_23";
			this._SubMnu_23.Text = "";
			this._SubMnu_23.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_24
			// 
			this._SubMnu_24.Available = true;
			this._SubMnu_24.Checked = false;
			this._SubMnu_24.Enabled = true;
			this._SubMnu_24.Name = "_SubMnu_24";
			this._SubMnu_24.Text = "";
			this._SubMnu_24.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_25
			// 
			this._SubMnu_25.Available = true;
			this._SubMnu_25.Checked = false;
			this._SubMnu_25.Enabled = true;
			this._SubMnu_25.Name = "_SubMnu_25";
			this._SubMnu_25.Text = "";
			this._SubMnu_25.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_26
			// 
			this._SubMnu_26.Available = true;
			this._SubMnu_26.Checked = false;
			this._SubMnu_26.Enabled = true;
			this._SubMnu_26.Name = "_SubMnu_26";
			this._SubMnu_26.Text = "";
			this._SubMnu_26.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_27
			// 
			this._SubMnu_27.Available = true;
			this._SubMnu_27.Checked = false;
			this._SubMnu_27.Enabled = true;
			this._SubMnu_27.Name = "_SubMnu_27";
			this._SubMnu_27.Text = "";
			this._SubMnu_27.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_28
			// 
			this._SubMnu_28.Available = true;
			this._SubMnu_28.Checked = false;
			this._SubMnu_28.Enabled = true;
			this._SubMnu_28.Name = "_SubMnu_28";
			this._SubMnu_28.Text = "";
			this._SubMnu_28.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_29
			// 
			this._SubMnu_29.Available = true;
			this._SubMnu_29.Checked = false;
			this._SubMnu_29.Enabled = true;
			this._SubMnu_29.Name = "_SubMnu_29";
			this._SubMnu_29.Text = "";
			this._SubMnu_29.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_30
			// 
			this._SubMnu_30.Available = true;
			this._SubMnu_30.Checked = false;
			this._SubMnu_30.Enabled = true;
			this._SubMnu_30.Name = "_SubMnu_30";
			this._SubMnu_30.Text = "";
			this._SubMnu_30.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_31
			// 
			this._SubMnu_31.Available = true;
			this._SubMnu_31.Checked = false;
			this._SubMnu_31.Enabled = true;
			this._SubMnu_31.Name = "_SubMnu_31";
			this._SubMnu_31.Text = "";
			this._SubMnu_31.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_32
			// 
			this._SubMnu_32.Available = true;
			this._SubMnu_32.Checked = false;
			this._SubMnu_32.Enabled = true;
			this._SubMnu_32.Name = "_SubMnu_32";
			this._SubMnu_32.Text = "";
			this._SubMnu_32.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_33
			// 
			this._SubMnu_33.Available = true;
			this._SubMnu_33.Checked = false;
			this._SubMnu_33.Enabled = true;
			this._SubMnu_33.Name = "_SubMnu_33";
			this._SubMnu_33.Text = "";
			this._SubMnu_33.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_34
			// 
			this._SubMnu_34.Available = true;
			this._SubMnu_34.Checked = false;
			this._SubMnu_34.Enabled = true;
			this._SubMnu_34.Name = "_SubMnu_34";
			this._SubMnu_34.Text = "";
			this._SubMnu_34.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_35
			// 
			this._SubMnu_35.Available = true;
			this._SubMnu_35.Checked = false;
			this._SubMnu_35.Enabled = true;
			this._SubMnu_35.Name = "_SubMnu_35";
			this._SubMnu_35.Text = "";
			this._SubMnu_35.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_36
			// 
			this._SubMnu_36.Available = true;
			this._SubMnu_36.Checked = false;
			this._SubMnu_36.Enabled = true;
			this._SubMnu_36.Name = "_SubMnu_36";
			this._SubMnu_36.Text = "";
			this._SubMnu_36.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_37
			// 
			this._SubMnu_37.Available = true;
			this._SubMnu_37.Checked = false;
			this._SubMnu_37.Enabled = true;
			this._SubMnu_37.Name = "_SubMnu_37";
			this._SubMnu_37.Text = "";
			this._SubMnu_37.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_38
			// 
			this._SubMnu_38.Available = true;
			this._SubMnu_38.Checked = false;
			this._SubMnu_38.Enabled = true;
			this._SubMnu_38.Name = "_SubMnu_38";
			this._SubMnu_38.Text = "";
			this._SubMnu_38.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_39
			// 
			this._SubMnu_39.Available = true;
			this._SubMnu_39.Checked = false;
			this._SubMnu_39.Enabled = true;
			this._SubMnu_39.Name = "_SubMnu_39";
			this._SubMnu_39.Text = "";
			this._SubMnu_39.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_40
			// 
			this._SubMnu_40.Available = true;
			this._SubMnu_40.Checked = false;
			this._SubMnu_40.Enabled = true;
			this._SubMnu_40.Name = "_SubMnu_40";
			this._SubMnu_40.Text = "";
			this._SubMnu_40.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_41
			// 
			this._SubMnu_41.Available = true;
			this._SubMnu_41.Checked = false;
			this._SubMnu_41.Enabled = true;
			this._SubMnu_41.Name = "_SubMnu_41";
			this._SubMnu_41.Text = "";
			this._SubMnu_41.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_42
			// 
			this._SubMnu_42.Available = true;
			this._SubMnu_42.Checked = false;
			this._SubMnu_42.Enabled = true;
			this._SubMnu_42.Name = "_SubMnu_42";
			this._SubMnu_42.Text = "";
			this._SubMnu_42.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_43
			// 
			this._SubMnu_43.Available = true;
			this._SubMnu_43.Checked = false;
			this._SubMnu_43.Enabled = true;
			this._SubMnu_43.Name = "_SubMnu_43";
			this._SubMnu_43.Text = "";
			this._SubMnu_43.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_44
			// 
			this._SubMnu_44.Available = true;
			this._SubMnu_44.Checked = false;
			this._SubMnu_44.Enabled = true;
			this._SubMnu_44.Name = "_SubMnu_44";
			this._SubMnu_44.Text = "";
			this._SubMnu_44.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_45
			// 
			this._SubMnu_45.Available = true;
			this._SubMnu_45.Checked = false;
			this._SubMnu_45.Enabled = true;
			this._SubMnu_45.Name = "_SubMnu_45";
			this._SubMnu_45.Text = "";
			this._SubMnu_45.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_46
			// 
			this._SubMnu_46.Available = true;
			this._SubMnu_46.Checked = false;
			this._SubMnu_46.Enabled = true;
			this._SubMnu_46.Name = "_SubMnu_46";
			this._SubMnu_46.Text = "";
			this._SubMnu_46.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_47
			// 
			this._SubMnu_47.Available = true;
			this._SubMnu_47.Checked = false;
			this._SubMnu_47.Enabled = true;
			this._SubMnu_47.Name = "_SubMnu_47";
			this._SubMnu_47.Text = "";
			this._SubMnu_47.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_48
			// 
			this._SubMnu_48.Available = true;
			this._SubMnu_48.Checked = false;
			this._SubMnu_48.Enabled = true;
			this._SubMnu_48.Name = "_SubMnu_48";
			this._SubMnu_48.Text = "";
			this._SubMnu_48.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// _SubMnu_49
			// 
			this._SubMnu_49.Available = true;
			this._SubMnu_49.Checked = false;
			this._SubMnu_49.Enabled = true;
			this._SubMnu_49.Name = "_SubMnu_49";
			this._SubMnu_49.Text = "";
			this._SubMnu_49.Click += new System.EventHandler(this.SubMnu_Click);
			// 
			// Opcion
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
			this.ClientSize = new System.Drawing.Size(6, 30);
			this.ControlBox = false;
			this.Controls.Add(MainMenu1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Location = new System.Drawing.Point(1, 18);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Opcion";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ShowInTaskbar = false;
			this.Visible = false;
			this.Activated += new System.EventHandler(this.Opcion_Activated);
			this.Closed += new System.EventHandler(this.Opcion_Closed);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeSubMnu();
		}
		void InitializeSubMnu()
		{
			this.SubMnu = new System.Windows.Forms.ToolStripItem[50];
			this.SubMnu[0] = _SubMnu_0;
			this.SubMnu[1] = _SubMnu_1;
			this.SubMnu[2] = _SubMnu_2;
			this.SubMnu[3] = _SubMnu_3;
			this.SubMnu[4] = _SubMnu_4;
			this.SubMnu[5] = _SubMnu_5;
			this.SubMnu[6] = _SubMnu_6;
			this.SubMnu[7] = _SubMnu_7;
			this.SubMnu[8] = _SubMnu_8;
			this.SubMnu[9] = _SubMnu_9;
			this.SubMnu[10] = _SubMnu_10;
			this.SubMnu[11] = _SubMnu_11;
			this.SubMnu[12] = _SubMnu_12;
			this.SubMnu[13] = _SubMnu_13;
			this.SubMnu[14] = _SubMnu_14;
			this.SubMnu[15] = _SubMnu_15;
			this.SubMnu[16] = _SubMnu_16;
			this.SubMnu[17] = _SubMnu_17;
			this.SubMnu[18] = _SubMnu_18;
			this.SubMnu[19] = _SubMnu_19;
			this.SubMnu[20] = _SubMnu_20;
			this.SubMnu[21] = _SubMnu_21;
			this.SubMnu[22] = _SubMnu_22;
			this.SubMnu[23] = _SubMnu_23;
			this.SubMnu[24] = _SubMnu_24;
			this.SubMnu[25] = _SubMnu_25;
			this.SubMnu[26] = _SubMnu_26;
			this.SubMnu[27] = _SubMnu_27;
			this.SubMnu[28] = _SubMnu_28;
			this.SubMnu[29] = _SubMnu_29;
			this.SubMnu[30] = _SubMnu_30;
			this.SubMnu[31] = _SubMnu_31;
			this.SubMnu[32] = _SubMnu_32;
			this.SubMnu[33] = _SubMnu_33;
			this.SubMnu[34] = _SubMnu_34;
			this.SubMnu[35] = _SubMnu_35;
			this.SubMnu[36] = _SubMnu_36;
			this.SubMnu[37] = _SubMnu_37;
			this.SubMnu[38] = _SubMnu_38;
			this.SubMnu[39] = _SubMnu_39;
			this.SubMnu[40] = _SubMnu_40;
			this.SubMnu[41] = _SubMnu_41;
			this.SubMnu[42] = _SubMnu_42;
			this.SubMnu[43] = _SubMnu_43;
			this.SubMnu[44] = _SubMnu_44;
			this.SubMnu[45] = _SubMnu_45;
			this.SubMnu[46] = _SubMnu_46;
			this.SubMnu[47] = _SubMnu_47;
			this.SubMnu[48] = _SubMnu_48;
			this.SubMnu[49] = _SubMnu_49;
		}
		#endregion
	}
}