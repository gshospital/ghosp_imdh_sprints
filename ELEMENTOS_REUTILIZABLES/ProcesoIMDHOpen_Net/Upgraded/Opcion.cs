using Microsoft.VisualBasic;
using System;
using System.Diagnostics;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ProcesoIMDHOpen
{
	public partial class Opcion
		: System.Windows.Forms.Form
	{

		public Opcion()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}




		private void Opcion_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				float maxX = 0;
				float maxY = 0;
				try
				{
					//Me.Width = 500
					//Me.Height = 700

					maxX = Screen.PrimaryScreen.Bounds.Width * 15 - ((float) (this.Width * 15));
					maxY = Screen.PrimaryScreen.Bounds.Height * 15 - ((float) (this.Height * 15));

					//Opcion.ScaleMode = 1

					//ScreenToClient Opcion.hWnd, PosicionMouse

					//Opcion.Move Min(maxX, Screen.TwipsPerPixelX * PosicionMouse.X), Min(Screen.TwipsPerPixelX * PosicionMouse.Y, maxY)

					for (int i = 0; i <= 49; i++)
					{
						if (i <= modIMDHOpen.Contador)
						{
							// Lista.AddItem llamadas(i, 0), i
							SubMnu[i].Text = modIMDHOpen.llamadas[i, 0];
							SubMnu[i].Available = true;
							SubMnu[i].Enabled = true;

						}
						else
						{
							SubMnu[i].Available = false;
							SubMnu[i].Enabled = false;
						}
					}
					this.Visible = false;
					Ctx_mnu.Show(this, (int) PointToClient(Cursor.Position).X, (int) PointToClient(Cursor.Position).Y);
					return;
				}
				catch (System.Exception excep)
				{
					MessageBox.Show(excep.Message, "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		//UPGRADE_NOTE: (7001) The following declaration (Min) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private float Min(float uno, float dos)
		//{
				//if (uno > dos)
				//{
					//return dos;
				//}
				//else
				//{
					//return uno;
				//}
		//}

		public void SubMnu_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.SubMnu, eventSender);
			int Lugar = Index;
			try
			{
				if (modIMDHOpen.llamadas[Lugar, 1] == "U")
				{
					string tempRefParam = null;
					string tempRefParam2 = null;
					string tempRefParam3 = null;
					UpgradeSupportHelper.PInvoke.SafeNative.shell32.ShellExecute(IntPtr.Zero, ref tempRefParam, ref modIMDHOpen.llamadas[Lugar, 2], ref tempRefParam2, ref tempRefParam3, (int) ProcessWindowStyle.Normal);
				}

				if (modIMDHOpen.llamadas[Lugar, 1] == "E")
				{
					//UPGRADE_TODO: (7005) parameters (if any) must be set using the Arguments property of ProcessStartInfo More Information: http://www.vbtonet.com/ewis/ewi7005.aspx
					ProcessStartInfo startInfo = new ProcessStartInfo(modIMDHOpen.llamadas[Lugar, 2]);
					startInfo.WindowStyle = ProcessWindowStyle.Normal;
					Process.Start(startInfo);
				}
				this.Close();
				return;
				this.Close();
			}
			catch
			{
			}

			MessageBox.Show(Information.Err().Description, "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Information);
			this.Close();
		}
		private void Opcion_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}