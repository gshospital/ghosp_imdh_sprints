using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;
using Telerik.WinControls;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ProcesoIMDHOpen
{
	public class clsIMDHOpen
	{



		private struct POINTAPI
		{
			public int X;
			public int Y;
		}
		private const string NombreFormulario = "ULTI";


		public clsIMDHOpen()
		{

			//On Error GoTo inicia
			//Set clase_mensaje = CreateObject("Mensajes.ClassMensajes")
			//Exit Sub
			//inicia:
			//MsgBox "Smensajes.dll no encontrada"

		}

		~clsIMDHOpen()
		{
			//Set clase_mensaje = Nothing
		}

		public void EstableceUltimoForm(string Nombre, string Usuario)
		{
			Interaction.SaveSetting("ULTIMOFORM", "GHOSP", NombreFormulario + Usuario.Trim().ToUpper(), Nombre);
		}

		public string LeerUltimoForm(string Usuario)
		{
			string cadena = "";
			cadena = Interaction.GetSetting("ULTIMOFORM", "GHOSP", NombreFormulario + Usuario.Trim().ToUpper(), String.Empty);
			return cadena;

		}



		public int TeclaFuncionOpen(SqlConnection cnConexion)
		{
			int result = 0;
			DataSet rdoTemp = null;
			string sql = String.Empty;
			try
			{
				sql = "select ISNULL(nnumeri1,0) as nnumeri1 from sconsglo where gconsglo='IMDHOPEN' and valfanu1='S'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, cnConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					return Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["nnumeri1"]);
				}
				else
				{
					return 0;
				}
			}
			catch (System.Exception excep)
			{
				MessageBox.Show(excep.Message, "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return result;
			}
		}

		public bool TeclaFuncionBotonVisible(SqlConnection cnConexion)
		{
			bool result = false;
			DataSet rdoTemp = null;
			string sql = String.Empty;
			string aux = String.Empty;
			try
			{
				sql = "select ISNULL(valfanu1,'N') as valfanu1 from sconsglo where gconsglo='IMDHOPEN' and valfanu1='S'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, cnConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					aux = Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]);
				}


				return aux.Trim() == "S";
			}
			catch (System.Exception excep)
			{
				MessageBox.Show(excep.Message, "Atenci�n", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return result;
			}
		}


		public object LlamadaSP(SqlConnection cnConexion, string stUsuario, string stFormulario, string StControl, string stGidenpac, string stEpisodio, bool blTeclaFuncion, int PosicionX, int PosicionY)
		{
			string strSql = String.Empty;
			SqlCommand rdqLlamada = null;
			string StError = String.Empty;
			string stDevolucion = String.Empty;
			rdqLlamada = null;
			modIMDHOpen.PosicionMouse.X = PosicionX;
			modIMDHOpen.PosicionMouse.Y = PosicionY;
            try
            {

                rdqLlamada = null;

                string sql = "SELECT ISNULL(valfanu2,'') as valfanu2 FROM SCONSGLO WHERE gconsglo ='IMDHOPEN' AND valfanu1 = 'S'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, cnConexion);
                DataSet rdoTemp = new DataSet();
                tempAdapter.Fill(rdoTemp);

                string stProcedimiento = "";
                if (rdoTemp.Tables[0].Rows.Count != 0)
                {
                    stProcedimiento = Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu2"]).Trim();
                }

                rdoTemp.Close();

                if (stProcedimiento != "")
                {
                    //If Trim(stGidenpac) <> "" Then
                    //If Not (Len(Trim(stEpisodio)) > 0 And IsNumeric(Mid(Trim(stEpisodio), 1, 1))) Then
                    //On Local Error GoTo ERR

                    // Llamada
                    rdqLlamada = cnConexion.CreateQuery("Llamada", stProcedimiento);
                    rdqLlamada.CommandType = CommandType.StoredProcedure;

                    rdqLlamada.Parameters.Add("@stUsuario", SqlDbType.VarChar).Size = 20;
                    rdqLlamada.Parameters.Add("@stFormulario", SqlDbType.VarChar).Size = 20;
                    rdqLlamada.Parameters.Add("@stControl", SqlDbType.VarChar).Size = 20;
                    rdqLlamada.Parameters.Add("@stGidenPac", SqlDbType.VarChar).Size = 20;
                    rdqLlamada.Parameters.Add("@stEpisodio", SqlDbType.VarChar).Size = 20;
                    rdqLlamada.Parameters.Add("@blTeclaFuncion", SqlDbType.Bit);
                    rdqLlamada.Parameters.Add("@stError", SqlDbType.VarChar).Size = 20;
                    rdqLlamada.Parameters.Add("@resultado", SqlDbType.VarChar).Size = 2000;

                    for (int intContador = 0; intContador <= 5; intContador++)
                    {
                        rdqLlamada.Parameters[intContador].Direction = ParameterDirection.Input;

                    }
                    rdqLlamada.Parameters[6].Direction = ParameterDirection.Output;
                    rdqLlamada.Parameters[7].Direction = ParameterDirection.Output;

                    rdqLlamada.Parameters[0].Value = stUsuario.Trim();
                    rdqLlamada.Parameters[1].Value = stFormulario.Trim();

                    rdqLlamada.Parameters[2].Value = StControl.Trim();
                    rdqLlamada.Parameters[3].Value = stGidenpac.Trim();

                    rdqLlamada.Parameters[4].Value = stEpisodio.Trim();

                    if (blTeclaFuncion)
                    {
                        rdqLlamada.Parameters[5].Value = true;
                    }
                    else
                    {
                        rdqLlamada.Parameters[5].Value = false;
                    }

                    rdqLlamada.ExecuteNonQuery();
                    StError = Convert.ToString(rdqLlamada.Parameters[6].Value);
                    stDevolucion = Convert.ToString(rdqLlamada.Parameters[7].Value) + "";
                    if (StError.Trim() == "")
                    {
                        ProcesaLLamadas(stDevolucion, blTeclaFuncion);
                    }
                    else
                    {
                        RadMessageBox.Show(StError, Application.ProductName);
                    }
                    rdqLlamada = null;

                    //Opcion.Show 1

                    //Else
                    //    MsgBox "No se ha elegido ningun episodio.", vbCritical, "Error de configuaci�n"
                    //End If
                    //Else
                    //    MsgBox "No se ha pasado ningun identificador de paciente.", vbCritical, "Error de configuaci�n"
                    //End If
                    //Else
                    //   MsgBox "El nombre valor de la constante IMDHOPEN no est� declarado o est� desactivado.", vbCritical, "Error de configuaci�n"
                }
                return null;
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(ex.Message, Application.ProductName);
                return null;
            }
		}

		private void ProcesaLLamadas(string stDevolucion, bool teclafuncion)
		{
			int Contador2 = 0;
			int NumeroTotal = 0;

			string TresUltimas = String.Empty;
            IList<string> partes = new List<string>();
			int posicion = 0;
			modIMDHOpen.Contador = 0;
			bool Fin = false;
			string cadena = stDevolucion;
			int i = 0;
			try
			{
				while (!Fin)
				{

					posicion = (cadena.IndexOf("$$$") + 1);
					if (posicion == 0)
					{
						partes.Add(cadena);
						Fin = true;
					}
					else
					{
						//partes = ArraysHelper.RedimPreserve(partes, new int[]{modIMDHOpen.Contador + 2});
						partes.Add(cadena.Substring(0, Math.Min(posicion - 1, cadena.Length)));
						cadena = cadena.Substring(posicion + 2, Math.Min(cadena.Length, cadena.Length - (posicion + 2)));
						modIMDHOpen.Contador++;
					}

				}
				NumeroTotal = modIMDHOpen.Contador;
                //Const Constante = Contador

                modIMDHOpen.llamadas = ArraysHelper.InitializeArray<string[, ]>(new int[]{modIMDHOpen.Contador + 2, 4}, new int[]{0, 0});

				Fin = false;

				TresUltimas = "";
				posicion = 0;

				//ReDim Preserve llamadas(0, 3)
				Fin = false;
				cadena = stDevolucion;

				i = 0;

				for (i = 0; i <= modIMDHOpen.Contador; i++)
				{
					if (partes[i] != null && partes[i] != "")
					{

						cadena = partes[i];
						Contador2 = 0;
						Fin = false;
						while (!Fin)
						{
							posicion = (cadena.IndexOf("###") + 1);
							if (posicion == 0)
							{
								modIMDHOpen.llamadas[i, Contador2] = cadena;
								Fin = true;
							}
							else
							{
								// ReDim Preserve llamadas(i + 1, 3)
								modIMDHOpen.llamadas[i, Contador2] = cadena.Substring(0, Math.Min(posicion - 1, cadena.Length));
								cadena = cadena.Substring(posicion + 2, Math.Min(cadena.Length, cadena.Length - (posicion + 2)));
								Contador2++;
							}
						}
					}
				}
				NumeroTotal = modIMDHOpen.Contador;
				if (!teclafuncion && modIMDHOpen.Contador > 0)
				{
                    Form1.DefInstance.Show();
					//''    Opcion.Show 1
				}
				else
				{

					if (modIMDHOpen.llamadas[0, 1] == "U")
					{
						string tempRefParam = null;
						string tempRefParam2 = null;
						string tempRefParam3 = null;
						UpgradeSupportHelper.PInvoke.SafeNative.shell32.ShellExecute(IntPtr.Zero, ref tempRefParam, ref modIMDHOpen.llamadas[0, 2], ref tempRefParam2, ref tempRefParam3, (int) ProcessWindowStyle.Normal);
					}

					if (modIMDHOpen.llamadas[0, 1] == "E")
					{
						ProcessStartInfo startInfo = new ProcessStartInfo(modIMDHOpen.llamadas[0, 2]);
						startInfo.WindowStyle = ProcessWindowStyle.Normal;
						Process.Start(startInfo);
					}

				}
			}
			catch (System.Exception excep)
			{
				RadMessageBox.Show(excep.Message, "Atenci�n", MessageBoxButtons.OK, RadMessageIcon.Info);
			}
		}
	}
}