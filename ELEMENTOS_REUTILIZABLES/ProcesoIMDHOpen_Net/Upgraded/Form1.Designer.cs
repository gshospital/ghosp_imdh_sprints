using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ProcesoIMDHOpen
{
	partial class Form1
	{

		#region "Upgrade Support "
		private static Form1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Form1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Form1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private void Ctx_mnu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			System.Collections.Generic.List<Telerik.WinControls.RadItem> list = new System.Collections.Generic.List<Telerik.WinControls.RadItem>();
			Ctx_mnu.Items.Clear();
			//We are moving the submenus from original menu to the context menu before displaying it
			foreach (Telerik.WinControls.RadItem item in mnu.Items)
			{
				list.Add(item);
			}
			foreach (Telerik.WinControls.RadItem item in list)
			{
				Ctx_mnu.Items.Add(item);
			}
			e.Cancel = false;
		}
		private void Ctx_mnu_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//System.Collections.Generic.List<Telerik.WinControls.RadItem> list = new System.Collections.Generic.List<Telerik.WinControls.RadItem>();
			////We are moving the submenus the context menu back to the original menu after displaying
			//foreach (Telerik.WinControls.RadItem item in Ctx_mnu.Items)
			//{
			//	list.Add(item);
			//}
			//foreach (Telerik.WinControls.RadItem item in list)
			//{
			//	mnu.Items.Add(item);
			//}
            this.Close();
		}
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_SubMnu_0", "_SubMnu_1", "_SubMnu_2", "_SubMnu_3", "_SubMnu_4", "_SubMnu_5", "_SubMnu_6", "_SubMnu_7", "_SubMnu_8", "_SubMnu_9", "_SubMnu_10", "_SubMnu_11", "_SubMnu_12", "_SubMnu_13", "_SubMnu_14", "_SubMnu_15", "_SubMnu_16", "_SubMnu_17", "_SubMnu_18", "_SubMnu_19", "_SubMnu_20", "_SubMnu_21", "_SubMnu_22", "_SubMnu_23", "_SubMnu_24", "_SubMnu_25", "_SubMnu_26", "_SubMnu_27", "_SubMnu_28", "_SubMnu_29", "_SubMnu_30", "_SubMnu_31", "_SubMnu_32", "_SubMnu_33", "_SubMnu_34", "_SubMnu_35", "_SubMnu_36", "_SubMnu_37", "_SubMnu_38", "_SubMnu_39", "_SubMnu_40", "_SubMnu_41", "_SubMnu_42", "_SubMnu_43", "_SubMnu_44", "_SubMnu_45", "_SubMnu_46", "_SubMnu_47", "_SubMnu_48", "_SubMnu_49", "mnu", "MainMenu1", "SubMnu", "Ctx_mnu"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_0;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_1;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_2;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_3;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_4;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_5;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_6;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_7;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_8;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_9;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_10;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_11;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_12;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_13;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_14;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_15;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_16;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_17;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_18;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_19;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_20;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_21;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_22;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_23;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_24;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_25;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_26;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_27;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_28;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_29;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_30;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_31;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_32;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_33;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_34;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_35;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_36;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_37;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_38;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_39;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_40;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_41;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_42;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_43;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_44;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_45;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_46;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_47;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_48;
		private Telerik.WinControls.UI.RadMenuItem _SubMnu_49;
		public Telerik.WinControls.UI.RadMenuItem mnu;
		public Telerik.WinControls.UI.RadMenu MainMenu1;
		public Telerik.WinControls.RadItem[] SubMnu = new Telerik.WinControls.RadItem[50];
		public Telerik.WinControls.UI.RadContextMenu Ctx_mnu;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.MainMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.mnu = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_0 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_1 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_2 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_3 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_4 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_5 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_6 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_7 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_8 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_9 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_10 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_11 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_12 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_13 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_14 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_15 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_16 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_17 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_18 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_19 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_20 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_21 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_22 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_23 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_24 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_25 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_26 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_27 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_28 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_29 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_30 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_31 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_32 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_33 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_34 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_35 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_36 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_37 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_38 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_39 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_40 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_41 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_42 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_43 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_44 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_45 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_46 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_47 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_48 = new Telerik.WinControls.UI.RadMenuItem();
            this._SubMnu_49 = new Telerik.WinControls.UI.RadMenuItem();
            this.Ctx_mnu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MainMenu1
            // 
            this.MainMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnu});
            this.MainMenu1.Location = new System.Drawing.Point(0, 0);
            this.MainMenu1.Name = "MainMenu1";
            this.MainMenu1.Size = new System.Drawing.Size(334, 20);
            this.MainMenu1.TabIndex = 0;
            // 
            // mnu
            // 
            this.mnu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this._SubMnu_0,
            this._SubMnu_1,
            this._SubMnu_2,
            this._SubMnu_3,
            this._SubMnu_4,
            this._SubMnu_5,
            this._SubMnu_6,
            this._SubMnu_7,
            this._SubMnu_8,
            this._SubMnu_9,
            this._SubMnu_10,
            this._SubMnu_11,
            this._SubMnu_12,
            this._SubMnu_13,
            this._SubMnu_14,
            this._SubMnu_15,
            this._SubMnu_16,
            this._SubMnu_17,
            this._SubMnu_18,
            this._SubMnu_19,
            this._SubMnu_20,
            this._SubMnu_21,
            this._SubMnu_22,
            this._SubMnu_23,
            this._SubMnu_24,
            this._SubMnu_25,
            this._SubMnu_26,
            this._SubMnu_27,
            this._SubMnu_28,
            this._SubMnu_29,
            this._SubMnu_30,
            this._SubMnu_31,
            this._SubMnu_32,
            this._SubMnu_33,
            this._SubMnu_34,
            this._SubMnu_35,
            this._SubMnu_36,
            this._SubMnu_37,
            this._SubMnu_38,
            this._SubMnu_39,
            this._SubMnu_40,
            this._SubMnu_41,
            this._SubMnu_42,
            this._SubMnu_43,
            this._SubMnu_44,
            this._SubMnu_45,
            this._SubMnu_46,
            this._SubMnu_47,
            this._SubMnu_48,
            this._SubMnu_49});
            this.mnu.Name = "mnu";
            this.mnu.Text = "";
            this.mnu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // _SubMnu_0
            // 
            this._SubMnu_0.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_0.Name = "_SubMnu_0";
            this._SubMnu_0.Text = "";
            this._SubMnu_0.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_1
            // 
            this._SubMnu_1.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_1.Name = "_SubMnu_1";
            this._SubMnu_1.Text = "";
            this._SubMnu_1.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_2
            // 
            this._SubMnu_2.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_2.Name = "_SubMnu_2";
            this._SubMnu_2.Text = "";
            this._SubMnu_2.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_3
            // 
            this._SubMnu_3.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_3.Name = "_SubMnu_3";
            this._SubMnu_3.Text = "";
            this._SubMnu_3.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_4
            // 
            this._SubMnu_4.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_4.Name = "_SubMnu_4";
            this._SubMnu_4.Text = "";
            this._SubMnu_4.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_5
            // 
            this._SubMnu_5.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_5.Name = "_SubMnu_5";
            this._SubMnu_5.Text = "";
            this._SubMnu_5.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_6
            // 
            this._SubMnu_6.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_6.Name = "_SubMnu_6";
            this._SubMnu_6.Text = "";
            this._SubMnu_6.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_7
            // 
            this._SubMnu_7.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_7.Name = "_SubMnu_7";
            this._SubMnu_7.Text = "";
            this._SubMnu_7.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_8
            // 
            this._SubMnu_8.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_8.Name = "_SubMnu_8";
            this._SubMnu_8.Text = "";
            this._SubMnu_8.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_9
            // 
            this._SubMnu_9.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_9.Name = "_SubMnu_9";
            this._SubMnu_9.Text = "";
            this._SubMnu_9.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_10
            // 
            this._SubMnu_10.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_10.Name = "_SubMnu_10";
            this._SubMnu_10.Text = "";
            this._SubMnu_10.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_11
            // 
            this._SubMnu_11.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_11.Name = "_SubMnu_11";
            this._SubMnu_11.Text = "";
            this._SubMnu_11.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_12
            // 
            this._SubMnu_12.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_12.Name = "_SubMnu_12";
            this._SubMnu_12.Text = "";
            this._SubMnu_12.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_13
            // 
            this._SubMnu_13.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_13.Name = "_SubMnu_13";
            this._SubMnu_13.Text = "";
            this._SubMnu_13.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_14
            // 
            this._SubMnu_14.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_14.Name = "_SubMnu_14";
            this._SubMnu_14.Text = "";
            this._SubMnu_14.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_15
            // 
            this._SubMnu_15.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_15.Name = "_SubMnu_15";
            this._SubMnu_15.Text = "";
            this._SubMnu_15.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_16
            // 
            this._SubMnu_16.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_16.Name = "_SubMnu_16";
            this._SubMnu_16.Text = "";
            this._SubMnu_16.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_17
            // 
            this._SubMnu_17.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_17.Name = "_SubMnu_17";
            this._SubMnu_17.Text = "";
            this._SubMnu_17.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_18
            // 
            this._SubMnu_18.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_18.Name = "_SubMnu_18";
            this._SubMnu_18.Text = "";
            this._SubMnu_18.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_19
            // 
            this._SubMnu_19.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_19.Name = "_SubMnu_19";
            this._SubMnu_19.Text = "";
            this._SubMnu_19.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_20
            // 
            this._SubMnu_20.ClickMode = Telerik.WinControls.ClickMode.Press;
            this._SubMnu_20.Name = "_SubMnu_20";
            this._SubMnu_20.Text = "";
            this._SubMnu_20.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_21
            // 
            this._SubMnu_21.Name = "_SubMnu_21";
            this._SubMnu_21.Text = "";
            this._SubMnu_21.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_22
            // 
            this._SubMnu_22.Name = "_SubMnu_22";
            this._SubMnu_22.Text = "";
            this._SubMnu_22.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_23
            // 
            this._SubMnu_23.Name = "_SubMnu_23";
            this._SubMnu_23.Text = "";
            this._SubMnu_23.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_24
            // 
            this._SubMnu_24.Name = "_SubMnu_24";
            this._SubMnu_24.Text = "";
            this._SubMnu_24.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_25
            // 
            this._SubMnu_25.Name = "_SubMnu_25";
            this._SubMnu_25.Text = "";
            this._SubMnu_25.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_26
            // 
            this._SubMnu_26.Name = "_SubMnu_26";
            this._SubMnu_26.Text = "";
            this._SubMnu_26.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_27
            // 
            this._SubMnu_27.Name = "_SubMnu_27";
            this._SubMnu_27.Text = "";
            this._SubMnu_27.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_28
            // 
            this._SubMnu_28.Name = "_SubMnu_28";
            this._SubMnu_28.Text = "";
            this._SubMnu_28.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_29
            // 
            this._SubMnu_29.Name = "_SubMnu_29";
            this._SubMnu_29.Text = "";
            this._SubMnu_29.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_30
            // 
            this._SubMnu_30.Name = "_SubMnu_30";
            this._SubMnu_30.Text = "";
            this._SubMnu_30.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_31
            // 
            this._SubMnu_31.Name = "_SubMnu_31";
            this._SubMnu_31.Text = "";
            this._SubMnu_31.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_32
            // 
            this._SubMnu_32.Name = "_SubMnu_32";
            this._SubMnu_32.Text = "";
            this._SubMnu_32.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_33
            // 
            this._SubMnu_33.Name = "_SubMnu_33";
            this._SubMnu_33.Text = "";
            this._SubMnu_33.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_34
            // 
            this._SubMnu_34.Name = "_SubMnu_34";
            this._SubMnu_34.Text = "";
            this._SubMnu_34.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_35
            // 
            this._SubMnu_35.Name = "_SubMnu_35";
            this._SubMnu_35.Text = "";
            this._SubMnu_35.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_36
            // 
            this._SubMnu_36.Name = "_SubMnu_36";
            this._SubMnu_36.Text = "";
            this._SubMnu_36.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_37
            // 
            this._SubMnu_37.Name = "_SubMnu_37";
            this._SubMnu_37.Text = "";
            this._SubMnu_37.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_38
            // 
            this._SubMnu_38.Name = "_SubMnu_38";
            this._SubMnu_38.Text = "";
            this._SubMnu_38.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_39
            // 
            this._SubMnu_39.Name = "_SubMnu_39";
            this._SubMnu_39.Text = "";
            this._SubMnu_39.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_40
            // 
            this._SubMnu_40.Name = "_SubMnu_40";
            this._SubMnu_40.Text = "";
            this._SubMnu_40.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_41
            // 
            this._SubMnu_41.Name = "_SubMnu_41";
            this._SubMnu_41.Text = "";
            this._SubMnu_41.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_42
            // 
            this._SubMnu_42.Name = "_SubMnu_42";
            this._SubMnu_42.Text = "";
            this._SubMnu_42.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_43
            // 
            this._SubMnu_43.Name = "_SubMnu_43";
            this._SubMnu_43.Text = "";
            this._SubMnu_43.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_44
            // 
            this._SubMnu_44.Name = "_SubMnu_44";
            this._SubMnu_44.Text = "";
            this._SubMnu_44.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_45
            // 
            this._SubMnu_45.Name = "_SubMnu_45";
            this._SubMnu_45.Text = "";
            this._SubMnu_45.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_46
            // 
            this._SubMnu_46.Name = "_SubMnu_46";
            this._SubMnu_46.Text = "";
            this._SubMnu_46.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_47
            // 
            this._SubMnu_47.Name = "_SubMnu_47";
            this._SubMnu_47.Text = "";
            this._SubMnu_47.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_48
            // 
            this._SubMnu_48.Name = "_SubMnu_48";
            this._SubMnu_48.Text = "";
            this._SubMnu_48.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // _SubMnu_49
            // 
            this._SubMnu_49.Name = "_SubMnu_49";
            this._SubMnu_49.Text = "";
            this._SubMnu_49.Click += new System.EventHandler(this.SubMnu_Click);
            // 
            // Ctx_mnu
            // 
            this.Ctx_mnu.DropDownOpening += new System.ComponentModel.CancelEventHandler(this.Ctx_mnu_Opening);
            this.Ctx_mnu.DropDownClosing += new System.ComponentModel.CancelEventHandler(this.Ctx_mnu_Closing);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(334, 101);
            this.ControlBox = false;
            this.Controls.Add(this.MainMenu1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.Closed += new System.EventHandler(this.Form1_Closed);
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        private void Ctx_mnu_DropDownClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        void ReLoadForm(bool addEvents)
		{
			InitializeSubMnu();
		}
		void InitializeSubMnu()
		{
			this.SubMnu = new Telerik.WinControls.RadItem[50];
			this.SubMnu[0] = _SubMnu_0;
			this.SubMnu[1] = _SubMnu_1;
			this.SubMnu[2] = _SubMnu_2;
			this.SubMnu[3] = _SubMnu_3;
			this.SubMnu[4] = _SubMnu_4;
			this.SubMnu[5] = _SubMnu_5;
			this.SubMnu[6] = _SubMnu_6;
			this.SubMnu[7] = _SubMnu_7;
			this.SubMnu[8] = _SubMnu_8;
			this.SubMnu[9] = _SubMnu_9;
			this.SubMnu[10] = _SubMnu_10;
			this.SubMnu[11] = _SubMnu_11;
			this.SubMnu[12] = _SubMnu_12;
			this.SubMnu[13] = _SubMnu_13;
			this.SubMnu[14] = _SubMnu_14;
			this.SubMnu[15] = _SubMnu_15;
			this.SubMnu[16] = _SubMnu_16;
			this.SubMnu[17] = _SubMnu_17;
			this.SubMnu[18] = _SubMnu_18;
			this.SubMnu[19] = _SubMnu_19;
			this.SubMnu[20] = _SubMnu_20;
			this.SubMnu[21] = _SubMnu_21;
			this.SubMnu[22] = _SubMnu_22;
			this.SubMnu[23] = _SubMnu_23;
			this.SubMnu[24] = _SubMnu_24;
			this.SubMnu[25] = _SubMnu_25;
			this.SubMnu[26] = _SubMnu_26;
			this.SubMnu[27] = _SubMnu_27;
			this.SubMnu[28] = _SubMnu_28;
			this.SubMnu[29] = _SubMnu_29;
			this.SubMnu[30] = _SubMnu_30;
			this.SubMnu[31] = _SubMnu_31;
			this.SubMnu[32] = _SubMnu_32;
			this.SubMnu[33] = _SubMnu_33;
			this.SubMnu[34] = _SubMnu_34;
			this.SubMnu[35] = _SubMnu_35;
			this.SubMnu[36] = _SubMnu_36;
			this.SubMnu[37] = _SubMnu_37;
			this.SubMnu[38] = _SubMnu_38;
			this.SubMnu[39] = _SubMnu_39;
			this.SubMnu[40] = _SubMnu_40;
			this.SubMnu[41] = _SubMnu_41;
			this.SubMnu[42] = _SubMnu_42;
			this.SubMnu[43] = _SubMnu_43;
			this.SubMnu[44] = _SubMnu_44;
			this.SubMnu[45] = _SubMnu_45;
			this.SubMnu[46] = _SubMnu_46;
			this.SubMnu[47] = _SubMnu_47;
			this.SubMnu[48] = _SubMnu_48;
			this.SubMnu[49] = _SubMnu_49;
		}
		#endregion
	}
}