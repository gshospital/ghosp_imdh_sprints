using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Mensajeria
{
    partial class MENU_MENSAJES
    {

        #region "Upgrade Support "
        private static MENU_MENSAJES m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static MENU_MENSAJES DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new MENU_MENSAJES();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "_Toolbar1_Button1", "_Toolbar1_Button2", "_Toolbar1_Button3", "_Toolbar1_Button4", "_Toolbar1_Button5", "_Toolbar1_Button6", "_Toolbar1_Button7", "_Toolbar1_Button8", "_Toolbar1_Button9", "_Toolbar1_Button10", "Toolbar1", "SprMensajes", "Timer2", "cbCerrar", "cbRefrescar", "Timer1", "txtMensaje", "ImageList1", "Label1", "SprMensajes_Sheet1" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public Telerik.WinControls.RadToolTip ToolTipMain;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button1;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button2;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button3;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button4;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button5;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button6;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button7;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button8;
        private Telerik.WinControls.UI.CommandBarSeparator _Toolbar1_Button9;
        private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button10;
        public Telerik.WinControls.UI.RadCommandBar Toolbar1;
        private Telerik.WinControls.UI.CommandBarRowElement radCommandBarLineElement1;
        private Telerik.WinControls.UI.CommandBarStripElement radCommandBarStripElement1;
        public UpgradeHelpers.Spread.FpSpread SprMensajes;
        public System.Windows.Forms.Timer Timer2;
        public Telerik.WinControls.UI.RadButton cbCerrar;
        public Telerik.WinControls.UI.RadButton cbRefrescar;
        public System.Windows.Forms.Timer Timer1;
        public Telerik.WinControls.UI.RadTextBoxControl txtMensaje;
        public System.Windows.Forms.ImageList ImageList1;
        public Telerik.WinControls.UI.RadLabel Label1;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MENU_MENSAJES));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.Toolbar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.radCommandBarLineElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this._Toolbar1_Button1 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button2 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button3 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button4 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button5 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button6 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button7 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button8 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button9 = new Telerik.WinControls.UI.CommandBarSeparator();
            this._Toolbar1_Button10 = new Telerik.WinControls.UI.CommandBarButton();
            this.SprMensajes = new UpgradeHelpers.Spread.FpSpread();
            this.Timer2 = new System.Windows.Forms.Timer(components);
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            this.cbRefrescar = new Telerik.WinControls.UI.RadButton();
            this.Timer1 = new System.Windows.Forms.Timer(components);
            this.txtMensaje = new Telerik.WinControls.UI.RadTextBoxControl();
            this.ImageList1 = new System.Windows.Forms.ImageList();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Toolbar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Toolbar1
            // 
            this.Toolbar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Toolbar1.ImageList = ImageList1;
            this.Toolbar1.Location = new System.Drawing.Point(0, 0);
            this.Toolbar1.Name = "Toolbar1";
            this.Toolbar1.ShowItemToolTips = true;
            this.Toolbar1.Size = new System.Drawing.Size(862, 28);
            this.Toolbar1.TabIndex = 0;
            this.Toolbar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.radCommandBarLineElement1});
            // 
            // radCommandBarLineElement1
            // 
            this.radCommandBarLineElement1.BorderLeftShadowColor = System.Drawing.Color.Empty;
            this.radCommandBarLineElement1.DisplayName = null;
            this.radCommandBarLineElement1.MinSize = new System.Drawing.Size(25, 25);
            this.radCommandBarLineElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.radCommandBarStripElement1});
            this.radCommandBarLineElement1.Text = "";
            // 
            // radCommandBarStripElement1
            // 
            this.radCommandBarStripElement1.DisplayName = "Commands Strip";
            this.radCommandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this._Toolbar1_Button1,
            this._Toolbar1_Button2,
            this._Toolbar1_Button3,
            this._Toolbar1_Button4,
            this._Toolbar1_Button5,
            this._Toolbar1_Button6,
            this._Toolbar1_Button7,
            this._Toolbar1_Button8,
            this._Toolbar1_Button9,
            this._Toolbar1_Button10});
            this.radCommandBarStripElement1.Name = "radCommandBarStripElement1";
            this.radCommandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.radCommandBarStripElement1.StretchHorizontally = true;
            this.radCommandBarStripElement1.StretchVertically = true;
            this.radCommandBarStripElement1.Text = "";
            // 
            // _Toolbar1_Button1
            // 
            this._Toolbar1_Button1.ImageIndex = 6;
            this._Toolbar1_Button1.Name = "";
            this._Toolbar1_Button1.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button1.Tag = "";
            this._Toolbar1_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button1.ToolTipText = "Elementos Recibidos";
            this._Toolbar1_Button1.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button2
            // 
            this._Toolbar1_Button2.ImageIndex = 5;
            this._Toolbar1_Button2.Name = "";
            this._Toolbar1_Button2.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button2.Tag = "";
            this._Toolbar1_Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button2.ToolTipText = "Elementos Enviados";
            this._Toolbar1_Button2.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button3
            // 
            this._Toolbar1_Button3.ImageIndex = 8;
            this._Toolbar1_Button3.Name = "";
            this._Toolbar1_Button3.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button3.Tag = "";
            this._Toolbar1_Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button3.ToolTipText = "Elementos Eliminados";
            this._Toolbar1_Button3.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button4
            // 
            this._Toolbar1_Button4.ImageIndex = 4;
            this._Toolbar1_Button4.Name = "";
            this._Toolbar1_Button4.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button4.Tag = "";
            this._Toolbar1_Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button4.ToolTipText = "Nuevo";
            this._Toolbar1_Button4.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button5
            // 
            this._Toolbar1_Button5.ImageIndex = 7;
            this._Toolbar1_Button5.Name = "";
            this._Toolbar1_Button5.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button5.Tag = "";
            this._Toolbar1_Button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button5.ToolTipText = "Eliminar";
            this._Toolbar1_Button5.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button6
            // 
            this._Toolbar1_Button6.ImageIndex = 10;
            this._Toolbar1_Button6.Name = "";
            this._Toolbar1_Button6.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button6.Tag = "";
            this._Toolbar1_Button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button6.ToolTipText = "Responder";
            this._Toolbar1_Button6.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button7
            // 
            this._Toolbar1_Button7.ImageIndex = 11;
            this._Toolbar1_Button7.Name = "";
            this._Toolbar1_Button7.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button7.Tag = "";
            this._Toolbar1_Button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button7.ToolTipText = "Responder a todos";
            this._Toolbar1_Button7.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button8
            // 
            this._Toolbar1_Button8.ImageIndex = 9;
            this._Toolbar1_Button8.Name = "";
            this._Toolbar1_Button8.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button8.Tag = "";
            this._Toolbar1_Button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button8.ToolTipText = "Reenviar";
            this._Toolbar1_Button8.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button9
            // 
            this._Toolbar1_Button9.Name = "";
            this._Toolbar1_Button9.Size = new System.Drawing.Size(6, 22);
            this._Toolbar1_Button9.Tag = "";
            this._Toolbar1_Button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button9.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // _Toolbar1_Button10
            // 
            this._Toolbar1_Button10.ImageIndex = 12;
            this._Toolbar1_Button10.Name = "";
            this._Toolbar1_Button10.Size = new System.Drawing.Size(24, 22);
            this._Toolbar1_Button10.Tag = "";
            this._Toolbar1_Button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._Toolbar1_Button10.ToolTipText = "Salir";
            this._Toolbar1_Button10.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
            // 
            // SprMensajes
            // 
            this.SprMensajes.Location = new System.Drawing.Point(8, 60);
            this.SprMensajes.Name = "SprMensajes";
            this.SprMensajes.Size = new System.Drawing.Size(847, 201);
            this.SprMensajes.TabIndex = 5;
            this.SprMensajes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(SprMensajes_CellClick);
            this.SprMensajes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SprMensajes_KeyDown);
            // 
            // Timer2
            // 
            this.Timer2.Enabled = true;
            this.Timer2.Interval = 1000;
            this.Timer2.Tick += new System.EventHandler(this.Timer2_Tick);
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCerrar.Location = new System.Drawing.Point(772, 428);
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(80, 34);
            this.cbCerrar.TabIndex = 4;
            this.cbCerrar.Text = "Cerrar";
            this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // cbRefrescar
            // 
            this.cbRefrescar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbRefrescar.Location = new System.Drawing.Point(772, 388);
            this.cbRefrescar.Name = "cbRefrescar";
            this.cbRefrescar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbRefrescar.Size = new System.Drawing.Size(80, 34);
            this.cbRefrescar.TabIndex = 3;
            this.cbRefrescar.Text = "Refrescar";
            this.cbRefrescar.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.cbRefrescar.Click += new System.EventHandler(this.cbRefrescar_Click);
            // 
            // Timer1
            // 
            this.Timer1.Enabled = true;
            this.Timer1.Interval = 1000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // txtMensaje
            // 
            this.txtMensaje.AcceptsReturn = true;
            this.txtMensaje.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtMensaje.Location = new System.Drawing.Point(4, 268);
            this.txtMensaje.MaxLength = 0;
            this.txtMensaje.Multiline = true;
            this.txtMensaje.Name = "txtMensaje";
            this.txtMensaje.IsReadOnly = true;
            this.txtMensaje.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMensaje.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            this.txtMensaje.Size = new System.Drawing.Size(757, 193);
            this.txtMensaje.TabIndex = 2;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.ImageList1.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ImageList1.ImageStream");
            this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "");
            this.ImageList1.Images.SetKeyName(5, "");
            this.ImageList1.Images.SetKeyName(6, "");
            this.ImageList1.Images.SetKeyName(7, "");
            this.ImageList1.Images.SetKeyName(8, "");
            this.ImageList1.Images.SetKeyName(9, "");
            this.ImageList1.Images.SetKeyName(10, "");
            this.ImageList1.Images.SetKeyName(11, "");
            this.ImageList1.Images.SetKeyName(12, "");
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.Label1.Location = new System.Drawing.Point(8, 32);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(199, 24);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "Elementos Recibidos";
            // 
            // MENU_MENSAJES
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 467);
            this.Controls.Add(this.Toolbar1);
            this.Controls.Add(this.SprMensajes);
            this.Controls.Add(this.cbCerrar);
            this.Controls.Add(this.cbRefrescar);
            this.Controls.Add(this.txtMensaje);
            this.Controls.Add(this.Label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = (System.Drawing.Icon)resources.GetObject("MENU_MENSAJES.Icon");
            this.Location = new System.Drawing.Point(11, 64);
            this.MaximizeBox = false;
            this.MinimizeBox = true;
            this.Name = "MENU_MENSAJES";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Gesti�n de Mensajer�a - MEN001F1";
            this.Closed += new System.EventHandler(this.MENU_MENSAJES_Closed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MENU_MENSAJES_FormClosing);
            this.Load += new System.EventHandler(this.MENU_MENSAJES_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MENU_MENSAJES_MouseMove);
            this.Toolbar1.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        #endregion
    }
}