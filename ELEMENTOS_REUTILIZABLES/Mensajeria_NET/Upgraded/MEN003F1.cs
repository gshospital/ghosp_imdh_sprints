using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;

namespace Mensajeria
{
	public partial class SEL_USUARIO
        : Telerik.WinControls.UI.RadForm
	{

		public SEL_USUARIO()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();

            lsbUsuarios.Rows.Clear();
            lsbUsuarios.Columns.Clear();
            lsbUsuarios.ColumnCount = 3;
            lsbUsuarios.ColumnWidths = "0;240;240";
            lsbUsuarios.Columns[1].HeaderText = "Usuario";
            lsbUsuarios.Columns[2].HeaderText = "Persona de Contacto";
            lsbUsuarios.MultiSelect = true;


            lsbSeleccionUsuarios.Rows.Clear();
            lsbSeleccionUsuarios.Columns.Clear();
            lsbSeleccionUsuarios.ColumnCount = 3;
            lsbSeleccionUsuarios.ColumnWidths = "0;240;240";
            lsbSeleccionUsuarios.Columns[1].HeaderText = "Usuario";
            lsbSeleccionUsuarios.Columns[2].HeaderText = "Persona de Contacto";
            lsbSeleccionUsuarios.MultiSelect = true;
		}
        
		private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
            DETALLE_MENSAJE.DefInstance.txtDestinatarios.Text = "";
            if (lsbSeleccionUsuarios.ListCount > 0)
            {
                for (int i = 0; i <= lsbSeleccionUsuarios.ListCount - 1; i++)
                {
                    object tempRefParam2 = 1;
                    object tempRefParam4 = 2;
                    DETALLE_MENSAJE.DefInstance.txtDestinatarios.Text = DETALLE_MENSAJE.DefInstance.txtDestinatarios.Text +
                                                                        Convert.ToString(lsbSeleccionUsuarios.get_List(i, tempRefParam2)) +
                                                                        " " + Convert.ToString(lsbSeleccionUsuarios.get_List(i, tempRefParam4)) + "; ";
                   
                }
                DETALLE_MENSAJE.DefInstance.txtDestinatarios.Text = DETALLE_MENSAJE.DefInstance.txtDestinatarios.Text.Substring(0, Math.Min(Strings.Len(DETALLE_MENSAJE.DefInstance.txtDestinatarios.Text) - 2, DETALLE_MENSAJE.DefInstance.txtDestinatarios.Text.Length));
            }
            this.Hide();
		}

        private void cbbPerfilesAplicacion_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
		{
            if (Convert.ToDouble(cbbPerfilesAplicacion.get_ListIndex()) > 0)
            {
                proCargaUsuarios("A");
            }
		}

        private void cbbPerfilesMensajeria_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
		{
			if (cbbPerfilesMensajeria.SelectedIndex > 0)
			{
				proCargaUsuarios("M");
			}
		}

		private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Hide();
		}

		private void SEL_USUARIO_Load(Object eventSender, EventArgs eventArgs)
		{
            cbbPerfilesAplicacion.Enabled = false;
            cbbPerfilesMensajeria.Enabled = false;
            opPerfilesA.Visible = true;
            opPerfilesM.Visible = true;
            cbbPerfilesAplicacion.Visible = true;
            cbbPerfilesMensajeria.Visible = true;
            switch (BMensajeria.ObtenerValor_CTEGLOBAL(BMensajeria.rcConexion, "MENSAPRI", "VALFANU2"))
            {
                case "A":
                    opPerfilesM.Visible = false;
                    cbbPerfilesMensajeria.Visible = false;
                    opPerfilesA.IsChecked = true;
                    break;
                case "M":
                    opPerfilesA.Visible = false;
                    cbbPerfilesAplicacion.Visible = false;
                    opPerfilesM.IsChecked = true;
                    break;
            }
            proCargaListas();
		}

		private void proCargaListas()
		{
            int i = 1;
            
            cbbPerfilesAplicacion.Clear();
            
            object tempRefParam = "";
            object tempRefParam2 = Type.Missing;
            cbbPerfilesAplicacion.AddItem(tempRefParam, tempRefParam2);
            
            string sql = "select ggrupusuario, dgrupusuario from sgrupusu order by dgrupusuario";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BMensajeria.rcConexion);
            DataSet rr = new DataSet();
            tempAdapter.Fill(rr);
            foreach (DataRow iteration_row in rr.Tables[0].Rows)
            {
                object tempRefParam3 = (Convert.ToString(iteration_row["dgrupusuario"]) + "").Trim().ToUpper();
                object tempRefParam4 = Type.Missing;
                
                cbbPerfilesAplicacion.AddItem(tempRefParam3, tempRefParam4);
                cbbPerfilesAplicacion.set_Column(1, i, (Convert.ToString(iteration_row["ggrupusuario"]) + "").Trim().ToUpper());
                i++;
            }
            rr.Close();

            cbbPerfilesMensajeria.Items.Clear();
            cbbPerfilesMensajeria.Items.Add("");
            sql = "select gperfmen, dperfmen from SPERFMEN order by dperfmen";
            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, BMensajeria.rcConexion);
            rr = new DataSet();
            tempAdapter_2.Fill(rr);
            /*
             * INDRA 09/03/2016 jproche 
             * Se reemplaza SetItemData de combobox por funcionalidad .net que realiza la carga de un dropdown list
             */
            RadListDataItem item = new RadListDataItem();
            foreach (DataRow iteration_row_2 in rr.Tables[0].Rows)
            {
                item.Text = (Convert.ToString(iteration_row_2["dperfmen"]) + "").Trim().ToUpper();
                item.Value = Convert.ToInt32(iteration_row_2["gperfmen"]);
                cbbPerfilesMensajeria.Items.Add(item);
                item = new RadListDataItem();
            }
            //jproche fin
            rr.Close();
		}
        
		private void proCargaUsuarios(string TipoPerfil)
		{
            int ilistau = 0;
            string stUsuarios = String.Empty;

            //    stUsuarios = ""
            //    If lsbUsuarios.ListCount > 0 Then
            //        For ilistau = 0 To lsbUsuarios.ListCount - 1
            //            stUsuarios = stUsuarios & "'" & lsbUsuarios.List(ilistau, 0) & "',"
            //        Next
            //        stUsuarios = Mid(stUsuarios, 1, Len(stUsuarios) - 1)
            //    End If

            StringBuilder stUsuariosSel = new StringBuilder();
            if (lsbSeleccionUsuarios.ListCount > 0)
            {
                for (ilistau = 0; ilistau <= lsbSeleccionUsuarios.ListCount - 1; ilistau++)
                {
                    int tempRefParam2 = 0;
                    stUsuariosSel.Append("'" + Convert.ToString(lsbSeleccionUsuarios.get_List(ilistau, tempRefParam2)) + "',");
                    ilistau = Convert.ToInt32(ilistau);
                }
                stUsuariosSel = new StringBuilder(stUsuariosSel.ToString().Substring(0, Math.Min(stUsuariosSel.ToString().Length - 1, stUsuariosSel.ToString().Length)));
            }

            string sql = "select dpersona.dnompers, dpersona.dap1pers, dpersona.dap2pers, susuario.dusuario, susuario.gusuario " +
                         " from dpersona" +
                         " inner join susuario on susuario.gpersona = dpersona.gpersona ";
            switch (TipoPerfil)
            {
                case "A":
                    int tempRefParam3 = cbbPerfilesAplicacion.get_ListIndex();
                    int tempRefParam4 = 1;
                    sql = sql +
                          " inner join susugusu on susuario.gusuario = susugusu.gusuario " +
                          " and ggrupusuario ='" + Convert.ToString(cbbPerfilesAplicacion.get_List(tempRefParam3, tempRefParam4)) + "'";
                    break;
                case "M":
                    sql = sql +
                          " inner join SPERFUSU on susuario.gusuario = SPERFUSU.gusuario " +
                          " and gperfmen = " + cbbPerfilesMensajeria.SelectedIndex.ToString();
                    //" and gperfmen = " + cbbPerfilesMensajeria.GetItemData(cbbPerfilesMensajeria.SelectedIndex).ToString();
                    break;
            }
            sql = sql + " where 1=1 ";
            if (stUsuarios != "")
            {
                sql = sql + " and susuario.gusuario not in (" + stUsuarios + ")";
            }
            if (stUsuariosSel.ToString() != "")
            {
                sql = sql + " and susuario.gusuario not in (" + stUsuariosSel.ToString() + ")";
            }
            sql = sql +
                  " and (dpersona.fborrado is null or dpersona.fborrado >= getdate()) and susuario.fborrado is null" +
                  " order by dap1pers, dap2pers, dnompers ";
            
            lsbUsuarios.Clear();
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BMensajeria.rcConexion);
            DataSet rr = new DataSet();
            tempAdapter.Fill(rr);
            ilistau = 0;
            foreach (DataRow iteration_row in rr.Tables[0].Rows)
            {
                object tempRefParam5 = (Convert.ToString(iteration_row["gusuario"]) + "").Trim().ToUpper();
                object tempRefParam6 = Type.Missing;
                lsbUsuarios.AddItem(tempRefParam5, tempRefParam6); // columna 0
                lsbUsuarios.set_Column(1, ilistau, (Convert.ToString(iteration_row["dusuario"]) + "").Trim().ToUpper()); // columna 1
                lsbUsuarios.set_Column(2, ilistau, "( " + (Convert.ToString(iteration_row["dap1pers"]) + "").Trim().ToUpper() + " " + (Convert.ToString(iteration_row["dap2pers"]) + "").Trim().ToUpper() + ", " + (Convert.ToString(iteration_row["dnompers"]) + "").Trim().ToUpper() + " )"); // columna 2
                ilistau++;
            }
            rr.Close();
            cbMayorMayor.Enabled = lsbUsuarios.ListCount > 0;
            cbMayor.Enabled = false;
            cbMenor.Enabled = false;
            cbMenorMenor.Enabled = false;
		}

		private void cbMayor_Click(Object eventSender, EventArgs eventArgs)
		{
            proMandarIzquierdaDerechaSeleccionados(lsbUsuarios, lsbSeleccionUsuarios);
            lsbUsuarios_RowsChanging(null, null);
            lsbSeleccionUsuarios_RowsChanging(null, null);
		}

		private void cbMayorMayor_Click(Object eventSender, EventArgs eventArgs)
		{
            proMandarIzquierdaDerechaTodos(lsbUsuarios, lsbSeleccionUsuarios);
            lsbUsuarios_RowsChanging(null, null);
            lsbSeleccionUsuarios_RowsChanging(null, null);
		}

		private void cbMenor_Click(Object eventSender, EventArgs eventArgs)
		{
            proMandarDerechaIzquierdaSeleccionados(lsbUsuarios, lsbSeleccionUsuarios);
            lsbUsuarios_RowsChanging(null, null);
            lsbSeleccionUsuarios_RowsChanging(null, null);
		}

		private void cbMenorMenor_Click(Object eventSender, EventArgs eventArgs)
		{
            proMandarDerechaIzquierdaTodos(lsbUsuarios, lsbSeleccionUsuarios);
            lsbUsuarios_RowsChanging(null, null);
            lsbSeleccionUsuarios_RowsChanging(null, null);
		}

        private void proMandarDerechaIzquierdaSeleccionados(UpgradeHelpers.MSForms.MSListBox lsbListaOrigen, UpgradeHelpers.MSForms.MSListBox  lsbListaDestino)
        {
            if (!HayAlgunoSeleccionado(lsbListaDestino))
            {
                return;
            }
            for (int ilista = 0; ilista <= lsbListaDestino.ListCount - 1; ilista++)
            {
                if (lsbListaDestino.get_Selected(ilista))
                {
                    object tempRefParam4 = Type.Missing;
                    object tempRefParam2 = (lsbListaDestino.get_List(ilista, tempRefParam4));
                    object tempRefParam5 = Type.Missing;
                    lsbListaOrigen.AddItem(tempRefParam2, tempRefParam5);
                    object tempRefParam7 = 1;
                    lsbListaOrigen.set_List(lsbListaOrigen.ListCount - 1, 1, lsbListaDestino.get_List(ilista, tempRefParam7));
                    object tempRefParam8 = ilista;
                    object tempRefParam9 = 2;
                    lsbListaOrigen.set_List(lsbListaOrigen.ListCount - 1, 2, lsbListaDestino.get_List(ilista, tempRefParam9));
                }                
            }
            GridViewDataRowInfo[] filasSeleccionadas = new GridViewDataRowInfo[lsbListaDestino.SelectedRows.Count];
            lsbListaDestino.SelectedRows.CopyTo(filasSeleccionadas, 0);
            for (int i = 0; i < filasSeleccionadas.Length; i++)
            {
                filasSeleccionadas[i].Delete();
            }
            lsbListaDestino.CurrentRow = null;
        }

        private void proMandarIzquierdaDerechaSeleccionados(UpgradeHelpers.MSForms.MSListBox lsbListaOrigen, UpgradeHelpers.MSForms.MSListBox lsbListaDestino)
        {
            if (!HayAlgunoSeleccionado(lsbListaOrigen))
            {
                return;
            }
            for (int ilista = 0; ilista <= lsbListaOrigen.ListCount - 1; ilista++)
            {
                if (lsbListaOrigen.get_Selected(ilista))
                {
                    object tempRefParam4 = Type.Missing;
                    object tempRefParam2 = (lsbListaOrigen.get_List(ilista, tempRefParam4));
                    object tempRefParam5 = Type.Missing;
                    lsbListaDestino.AddItem(tempRefParam2, tempRefParam5);
                    object tempRefParam7 = 1;
                    lsbListaDestino.set_List(lsbListaDestino.ListCount - 1, 1, lsbListaOrigen.get_List(ilista, tempRefParam7));
                    object tempRefParam9 = 2;
                    lsbListaDestino.set_List(lsbListaDestino.ListCount - 1, 2, lsbListaOrigen.get_List(ilista,  tempRefParam9));
                }
            }

            GridViewDataRowInfo[] filasSeleccionadas = new GridViewDataRowInfo[lsbListaOrigen.SelectedRows.Count];
            lsbListaOrigen.SelectedRows.CopyTo(filasSeleccionadas, 0);
            for (int i = 0; i < filasSeleccionadas.Length; i++)
            {
                filasSeleccionadas[i].Delete();
            }
            lsbListaOrigen.CurrentRow = null;
        }

        private void proMandarIzquierdaDerechaTodos(UpgradeHelpers.MSForms.MSListBox lsbListaOrigen, UpgradeHelpers.MSForms.MSListBox lsbListaDestino)
        {
            for (int ilista = 0; ilista <= lsbListaOrigen.ListCount - 1; ilista++)
            {
                object tempRefParam3 = Type.Missing;
                object tempRefParam = (lsbListaOrigen.get_List(ilista, tempRefParam3));
                object tempRefParam4 = Type.Missing;
                lsbListaDestino.AddItem(tempRefParam, tempRefParam4);
                
                object tempRefParam6 = 1;
                lsbListaDestino.set_List(lsbListaDestino.ListCount - 1, 1, lsbListaOrigen.get_List(ilista, tempRefParam6));
                object tempRefParam8 = 2;
                lsbListaDestino.set_List(lsbListaDestino.ListCount - 1, 2, lsbListaOrigen.get_List(ilista, tempRefParam8));
            }
            lsbListaOrigen.Clear();
        }

        private void proMandarDerechaIzquierdaTodos(UpgradeHelpers.MSForms.MSListBox lsbListaOrigen, UpgradeHelpers.MSForms.MSListBox lsbListaDestino)
        {
            for (int ilista = 0; ilista <= lsbListaDestino.ListCount - 1; ilista++)
            {
                object tempRefParam3 = Type.Missing;
                object tempRefParam = (lsbListaDestino.get_List(ilista, tempRefParam3));
                object tempRefParam4 = Type.Missing;
                lsbListaOrigen.AddItem(tempRefParam, tempRefParam4);
                ilista = Convert.ToInt32(ilista);
                
                object tempRefParam6 = 1;
                lsbListaOrigen.set_List(lsbListaOrigen.ListCount - 1, 1, lsbListaDestino.get_List(ilista, tempRefParam6));
                object tempRefParam8 = 2;
                lsbListaOrigen.set_List(lsbListaOrigen.ListCount - 1, 2, lsbListaDestino.get_List(ilista, tempRefParam8));
            }
            lsbListaDestino.Clear();
        }

		private void lsbUsuarios_CellDoubleClick(object sender, GridViewCellEventArgs e)
		{
			cbMayor_Click(cbMayor, new EventArgs());
		}

        private void lsbSeleccionUsuarios_CellDoubleClick(object sender, GridViewCellEventArgs e)
		{
			cbMenor_Click(cbMenor, new EventArgs());
		}
        		
        private void lsbUsuarios_RowsChanging(object sender, GridViewCollectionChangingEventArgs e)
		{
            if (lsbUsuarios.ListCount > 0)
            {
                cbMayorMayor.Enabled = true;
                cbMayor.Enabled = HayAlgunoSeleccionado(lsbUsuarios);
            }
            else
            {
                cbMayorMayor.Enabled = false;
                cbMayor.Enabled = false;
            }
		}

        private void lsbSeleccionUsuarios_RowsChanging(object sender, GridViewCollectionChangingEventArgs e)
		{
            if (lsbSeleccionUsuarios.ListCount > 0)
            {
                cbMenorMenor.Enabled = true;
                cbMenor.Enabled = HayAlgunoSeleccionado(lsbSeleccionUsuarios);
            }
            else
            {
                cbMenorMenor.Enabled = false;
                cbMenor.Enabled = false;
            }
		}

        private bool HayAlgunoSeleccionado(UpgradeHelpers.MSForms.MSListBox lista)
        {
            for (int ilista = 0; ilista <= lista.ListCount - 1; ilista++)
            {
                if (lista.get_Selected(ilista))
                {
                    return true;
                }                
            }
            return false;
        }

		private bool isInitializingComponent;
        private void opPerfilesA_ToggleStateChanged(object sender, StateChangedEventArgs args)
		{
            if (((RadRadioButton)sender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                cbbPerfilesAplicacion.Enabled = true;
                cbbPerfilesMensajeria.Enabled = false;
                cbbPerfilesMensajeria.SelectedIndex = -1;
            }
		}

		private void opPerfilesM_ToggleStateChanged(object sender, StateChangedEventArgs args)
		{
            if (((RadRadioButton)sender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                cbbPerfilesAplicacion.Enabled = false;
                cbbPerfilesMensajeria.Enabled = true;
                cbbPerfilesAplicacion.SelectedIndex = -1;
            }
		}

		private void opUsuarios_ToggleStateChanged(object sender, StateChangedEventArgs args)
		{
            if (((RadRadioButton)sender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                cbbPerfilesAplicacion.Enabled = false;
                cbbPerfilesMensajeria.Enabled = false;
                cbbPerfilesAplicacion.SelectedIndex = -1;
                cbbPerfilesMensajeria.SelectedIndex = -1;
                proCargaUsuarios("U");
            }
		}

		private void txtBuscar_TextChanged(Object eventSender, EventArgs eventArgs)
		{
            try
            {
                for (int i = 0; i <= lsbUsuarios.ListCount - 1; i++)
                {
                    lsbUsuarios.set_Selected(i, false);
                }
                for (int i = 0; i <= lsbUsuarios.ListCount - 1; i++)
                {
                    if ((Convert.ToString(lsbUsuarios.get_Column(2, i)).IndexOf(txtBuscar.Text.ToUpper(), StringComparison.CurrentCultureIgnoreCase) + 1) != 0)
                    {
                        lsbUsuarios.set_ListIndex(i);
                        lsbUsuarios.set_Selected(i, true);
                        break;
                    }
                    else
                        lsbUsuarios.CurrentRow = null;
                }
            }
            catch
            {
                Information.Err().Clear();
            }            
		}

		private void SEL_USUARIO_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}

        private void lsbUsuarios_Click(object sender, EventArgs e)
        {
            if (lsbUsuarios.CurrentRow != null)
                cbMayor.Enabled = true;
        }

        private void lsbSeleccionUsuarios_Click(object sender, EventArgs e)
        {
            if (lsbSeleccionUsuarios.CurrentRow != null)
                cbMenor.Enabled = true;
        }
	}
}