using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Mensajeria
{
	partial class DETALLE_AVISO
	{

		#region "Upgrade Support "
		private static DETALLE_AVISO m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DETALLE_AVISO DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DETALLE_AVISO();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "Timer1", "cmdSI", "cmdNO", "Label1", "Label2", "Image1", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public System.Windows.Forms.Timer Timer1;
		public Telerik.WinControls.UI.RadButton cmdSI;
		public Telerik.WinControls.UI.RadButton cmdNO;
        public Telerik.WinControls.UI.RadLabel Label1;
        public Telerik.WinControls.UI.RadLabel Label2;
		public System.Windows.Forms.PictureBox Image1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DETALLE_AVISO));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cmdSI = new Telerik.WinControls.UI.RadButton();
            this.cmdNO = new Telerik.WinControls.UI.RadButton();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Image1 = new System.Windows.Forms.PictureBox();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.cmdSI);
            this.Frame1.Controls.Add(this.cmdNO);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Image1);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(4, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(389, 109);
            this.Frame1.TabIndex = 0;
            this.Frame1.TabStop = false;
            // 
            // cmdSI
            // 
            this.cmdSI.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSI.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cmdSI.Location = new System.Drawing.Point(128, 72);
            this.cmdSI.Name = "cmdSI";
            this.cmdSI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSI.Size = new System.Drawing.Size(65, 33);
            this.cmdSI.TabIndex = 2;
            this.cmdSI.Text = "Si";
            this.cmdSI.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdSI.Click += new System.EventHandler(this.cmdSI_Click);
            // 
            // cmdNO
            // 
            this.cmdNO.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdNO.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cmdNO.Location = new System.Drawing.Point(204, 72);
            this.cmdNO.Name = "cmdNO";
            this.cmdNO.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdNO.Size = new System.Drawing.Size(65, 33);
            this.cmdNO.TabIndex = 1;
            this.cmdNO.Text = "No";
            this.cmdNO.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdNO.Click += new System.EventHandler(this.cmdNO_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = false;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(48, 12);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(337, 37);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "Tiene Mensajes sin leer en la Bandeja de Elementos Recibidos";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(48, 48);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(280, 17);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "�Desea abrir la Gesti�n de Mensajer�a Privada?";
            // 
            // Image1
            // 
            this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(4, 8);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(40, 44);
            this.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Image1.TabIndex = 5;
            this.Image1.TabStop = false;
            // 
            // Timer1
            // 
            this.Timer1.Interval = 1;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // DETALLE_AVISO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 115);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DETALLE_AVISO";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Closed += new System.EventHandler(this.DETALLE_AVISO_Closed);
            this.Load += new System.EventHandler(this.DETALLE_AVISO_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}