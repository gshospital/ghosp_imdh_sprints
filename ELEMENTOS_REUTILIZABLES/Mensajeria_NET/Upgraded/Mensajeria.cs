using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Mensajeria
{
	public class MCMensajeria
	{


		public void proLoadMensajeria(string stServer, string stDriver, string stBasedatos, string stUsuarioBD, string stPasswordBD, string ParaUsuario)
		{
			bool fError = false;
			Conexion.Obtener_conexion instancia = new Conexion.Obtener_conexion();
			string tempRefParam = "P";
			string tempRefParam2 = "";
			string tempRefParam3 = "";
			string tempRefParam4 = "";
			string tempRefParam5 = "0";
			string tempRefParam6 = "0";
			string tempRefParam7 = "PRINCIPAL";
           instancia.Conexion(ref BMensajeria.rcConexion, tempRefParam, tempRefParam2, ref fError, ref tempRefParam3, ref tempRefParam4, stServer, stDriver, stBasedatos, stUsuarioBD, stPasswordBD, tempRefParam5, tempRefParam6, tempRefParam7, ParaUsuario);
			instancia = null;

			BMensajeria.gUsuario = ParaUsuario;
			BMensajeria.ObtenerNombreAplicacion(BMensajeria.rcConexion);
			BMensajeria.Obtener_TipoBD_FormatoFechaBD(ref BMensajeria.rcConexion);
			MENU_MENSAJES.DefInstance.Hide();
		}

		public void proVerMenuMensajeria()
		{
			MENU_MENSAJES.DefInstance.Show();
		}

		public MCMensajeria()
		{
			//UPGRADE_ISSUE: (2070) Constant App was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
			//UPGRADE_ISSUE: (2064) App property app.HelpFile was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\AYUDA_DLLS.hlp");
			BMensajeria.clase_mensaje = new Mensajes.ClassMensajes();
		}

		~MCMensajeria()
		{
			BMensajeria.clase_mensaje = null;
		}

		public void proCompruebaMensajeria()
		{
			string stCuantos = String.Empty;
			if (BMensajeria.fMensajesNoLeidos(ref stCuantos))
			{
				DETALLE_AVISO.DefInstance.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - DETALLE_AVISO.DefInstance.Height) / 2);
				DETALLE_AVISO.DefInstance.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - DETALLE_AVISO.DefInstance.Width) / 2);
				DETALLE_AVISO.DefInstance.Label1.Text = "Tiene " + stCuantos + " Mensaje" + ((stCuantos == "1") ? "" : "s") + " sin leer en la Bandeja de Elementos Recibidos";
				DETALLE_AVISO.DefInstance.ShowDialog();
				if (BMensajeria.iRespuesta == System.Windows.Forms.DialogResult.Yes)
				{
					MENU_MENSAJES.DefInstance.Show();
				}
			}
		}
	}
}