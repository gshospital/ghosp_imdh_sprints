using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Mensajeria
{
    partial class SEL_USUARIO
    {

        #region "Upgrade Support "
        private static SEL_USUARIO m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static SEL_USUARIO DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new SEL_USUARIO();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "opUsuarios", "txtBuscar", "opPerfilesM", "opPerfilesA", "cbMenorMenor", "cbMenor", "cbMayorMayor", "cbMayor", "Label6", "Label5", "Label3", "Label2", "Label4", "lsbSeleccionUsuarios", "lsbUsuarios", "frmUsuarios", "CbCerrar", "CbAceptar", "cbbPerfilesMensajeria", "Image1", "Label1", "cbbPerfilesAplicacion" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;
        public Telerik.WinControls.UI.RadRadioButton opUsuarios;
        public Telerik.WinControls.UI.RadTextBoxControl txtBuscar;
        public Telerik.WinControls.UI.RadRadioButton opPerfilesM;
        public Telerik.WinControls.UI.RadRadioButton opPerfilesA;
        public Telerik.WinControls.UI.RadButton cbMenorMenor;
        public Telerik.WinControls.UI.RadButton cbMenor;
        public Telerik.WinControls.UI.RadButton cbMayorMayor;
        public Telerik.WinControls.UI.RadButton cbMayor;
        public Telerik.WinControls.UI.RadLabel Label4;

        public UpgradeHelpers.MSForms.MSListBox lsbSeleccionUsuarios;
        public UpgradeHelpers.MSForms.MSListBox lsbUsuarios;
        public Telerik.WinControls.UI.RadGroupBox frmUsuarios;
        public Telerik.WinControls.UI.RadButton CbCerrar;
        public Telerik.WinControls.UI.RadButton CbAceptar;
        public Telerik.WinControls.UI.RadDropDownList cbbPerfilesMensajeria;
        public System.Windows.Forms.PictureBox Image1;
        public Telerik.WinControls.UI.RadLabel Label1;
        public UpgradeHelpers.MSForms.MSCombobox cbbPerfilesAplicacion;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SEL_USUARIO));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.opUsuarios = new Telerik.WinControls.UI.RadRadioButton();
            this.txtBuscar = new Telerik.WinControls.UI.RadTextBoxControl();
            this.opPerfilesM = new Telerik.WinControls.UI.RadRadioButton();
            this.opPerfilesA = new Telerik.WinControls.UI.RadRadioButton();
            this.frmUsuarios = new Telerik.WinControls.UI.RadGroupBox();
            this.cbMenorMenor = new Telerik.WinControls.UI.RadButton();
            this.cbMenor = new Telerik.WinControls.UI.RadButton();
            this.cbMayorMayor = new Telerik.WinControls.UI.RadButton();
            this.cbMayor = new Telerik.WinControls.UI.RadButton();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.lsbSeleccionUsuarios = new UpgradeHelpers.MSForms.MSListBox(this.components);
            this.lsbUsuarios = new UpgradeHelpers.MSForms.MSListBox(this.components);
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.CbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbbPerfilesMensajeria = new Telerik.WinControls.UI.RadDropDownList();
            this.Image1 = new System.Windows.Forms.PictureBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.cbbPerfilesAplicacion = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.opUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opPerfilesM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opPerfilesA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmUsuarios)).BeginInit();
            this.frmUsuarios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenorMenor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayorMayor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbSeleccionUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbSeleccionUsuarios.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbUsuarios.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPerfilesMensajeria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPerfilesAplicacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // opUsuarios
            // 
            this.opUsuarios.Cursor = System.Windows.Forms.Cursors.Default;
            this.opUsuarios.Location = new System.Drawing.Point(16, 60);
            this.opUsuarios.Name = "opUsuarios";
            this.opUsuarios.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.opUsuarios.Size = new System.Drawing.Size(102, 18);
            this.opUsuarios.TabIndex = 4;
            this.opUsuarios.Text = "Usuarios Activos";
            this.opUsuarios.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.opUsuarios_ToggleStateChanged);
            // 
            // txtBuscar
            // 
            this.txtBuscar.AcceptsReturn = true;
            this.txtBuscar.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBuscar.Location = new System.Drawing.Point(68, 96);
            this.txtBuscar.MaxLength = 0;
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBuscar.Size = new System.Drawing.Size(393, 19);
            this.txtBuscar.TabIndex = 5;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // opPerfilesM
            // 
            this.opPerfilesM.Cursor = System.Windows.Forms.Cursors.Default;
            this.opPerfilesM.Location = new System.Drawing.Point(16, 32);
            this.opPerfilesM.Name = "opPerfilesM";
            this.opPerfilesM.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.opPerfilesM.Size = new System.Drawing.Size(141, 18);
            this.opPerfilesM.TabIndex = 2;
            this.opPerfilesM.Text = "Perfiles de la Mensajer�a";
            this.opPerfilesM.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.opPerfilesM_ToggleStateChanged);
            // 
            // opPerfilesA
            // 
            this.opPerfilesA.Cursor = System.Windows.Forms.Cursors.Default;
            this.opPerfilesA.Location = new System.Drawing.Point(16, 4);
            this.opPerfilesA.Name = "opPerfilesA";
            this.opPerfilesA.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.opPerfilesA.Size = new System.Drawing.Size(138, 18);
            this.opPerfilesA.TabIndex = 0;
            this.opPerfilesA.Text = "Perfiles de la Aplicaci�n";
            this.opPerfilesA.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.opPerfilesA_ToggleStateChanged);
            // 
            // frmUsuarios
            // 
            this.frmUsuarios.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmUsuarios.Controls.Add(this.cbMenorMenor);
            this.frmUsuarios.Controls.Add(this.cbMenor);
            this.frmUsuarios.Controls.Add(this.cbMayorMayor);
            this.frmUsuarios.Controls.Add(this.cbMayor);
            this.frmUsuarios.Controls.Add(this.Label4);
            this.frmUsuarios.Controls.Add(this.lsbSeleccionUsuarios);
            this.frmUsuarios.Controls.Add(this.lsbUsuarios);
            this.frmUsuarios.HeaderText = "Selecci�n de Usuarios: ";
            this.frmUsuarios.Location = new System.Drawing.Point(12, 124);
            this.frmUsuarios.Name = "frmUsuarios";
            this.frmUsuarios.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmUsuarios.Size = new System.Drawing.Size(571, 331);
            this.frmUsuarios.TabIndex = 14;
            this.frmUsuarios.Text = "Selecci�n de Usuarios: ";
            // 
            // cbMenorMenor
            // 
            this.cbMenorMenor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMenorMenor.Enabled = false;
            this.cbMenorMenor.Location = new System.Drawing.Point(24, 248);
            this.cbMenorMenor.Name = "cbMenorMenor";
            this.cbMenorMenor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMenorMenor.Size = new System.Drawing.Size(49, 41);
            this.cbMenorMenor.TabIndex = 10;
            this.cbMenorMenor.Text = "<<";
            this.cbMenorMenor.Click += new System.EventHandler(this.cbMenorMenor_Click);
            // 
            // cbMenor
            // 
            this.cbMenor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMenor.Enabled = false;
            this.cbMenor.Location = new System.Drawing.Point(24, 204);
            this.cbMenor.Name = "cbMenor";
            this.cbMenor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMenor.Size = new System.Drawing.Size(49, 41);
            this.cbMenor.TabIndex = 9;
            this.cbMenor.Text = "<";
            this.cbMenor.Click += new System.EventHandler(this.cbMenor_Click);
            // 
            // cbMayorMayor
            // 
            this.cbMayorMayor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMayorMayor.Enabled = false;
            this.cbMayorMayor.Location = new System.Drawing.Point(500, 80);
            this.cbMayorMayor.Name = "cbMayorMayor";
            this.cbMayorMayor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMayorMayor.Size = new System.Drawing.Size(49, 41);
            this.cbMayorMayor.TabIndex = 8;
            this.cbMayorMayor.Text = ">>";
            this.cbMayorMayor.Click += new System.EventHandler(this.cbMayorMayor_Click);
            // 
            // cbMayor
            // 
            this.cbMayor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMayor.Enabled = false;
            this.cbMayor.Location = new System.Drawing.Point(500, 36);
            this.cbMayor.Name = "cbMayor";
            this.cbMayor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMayor.Size = new System.Drawing.Size(49, 41);
            this.cbMayor.TabIndex = 7;
            this.cbMayor.Text = ">";
            this.cbMayor.Click += new System.EventHandler(this.cbMayor_Click);
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(24, 176);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(35, 18);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "Para...";
            // 
            // lsbSeleccionUsuarios
            // 
            this.lsbSeleccionUsuarios.Location = new System.Drawing.Point(80, 192);
            this.lsbSeleccionUsuarios.MasterTemplate.AllowAddNewRow = false;
            this.lsbSeleccionUsuarios.MasterTemplate.ShowRowHeaderColumn = false;
            this.lsbSeleccionUsuarios.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.lsbSeleccionUsuarios.Name = "lsbSeleccionUsuarios";
            this.lsbSeleccionUsuarios.ReadOnly = true;
            this.lsbSeleccionUsuarios.ShowGroupPanel = false;
            this.lsbSeleccionUsuarios.Size = new System.Drawing.Size(480, 122);
            this.lsbSeleccionUsuarios.TabIndex = 11;
            this.lsbSeleccionUsuarios.RowsChanging += new Telerik.WinControls.UI.GridViewCollectionChangingEventHandler(this.lsbSeleccionUsuarios_RowsChanging);
            this.lsbSeleccionUsuarios.Click += new System.EventHandler(this.lsbSeleccionUsuarios_Click);
            this.lsbSeleccionUsuarios.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.lsbSeleccionUsuarios_CellDoubleClick);
            // 
            // lsbUsuarios
            // 
            this.lsbUsuarios.Location = new System.Drawing.Point(12, 36);
            this.lsbUsuarios.MasterTemplate.AllowAddNewRow = false;
            this.lsbUsuarios.MasterTemplate.ShowRowHeaderColumn = false;
            this.lsbUsuarios.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.lsbUsuarios.Name = "lsbUsuarios";
            this.lsbUsuarios.ReadOnly = true;
            this.lsbUsuarios.ShowGroupPanel = false;
            this.lsbUsuarios.Size = new System.Drawing.Size(480, 122);
            this.lsbUsuarios.TabIndex = 6;
            this.lsbUsuarios.RowsChanging += new Telerik.WinControls.UI.GridViewCollectionChangingEventHandler(this.lsbUsuarios_RowsChanging);
            this.lsbUsuarios.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.lsbUsuarios_CellDoubleClick);
            this.lsbUsuarios.Click += new System.EventHandler(this.lsbUsuarios_Click);
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.Location = new System.Drawing.Point(500, 460);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(81, 29);
            this.CbCerrar.TabIndex = 13;
            this.CbCerrar.Text = "&Cerrar";
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // CbAceptar
            // 
            this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbAceptar.Location = new System.Drawing.Point(412, 460);
            this.CbAceptar.Name = "CbAceptar";
            this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbAceptar.Size = new System.Drawing.Size(81, 29);
            this.CbAceptar.TabIndex = 12;
            this.CbAceptar.Text = "&Aceptar";
            this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
            // 
            // cbbPerfilesMensajeria
            // 
            this.cbbPerfilesMensajeria.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbPerfilesMensajeria.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbPerfilesMensajeria.Location = new System.Drawing.Point(172, 36);
            this.cbbPerfilesMensajeria.Name = "cbbPerfilesMensajeria";
            this.cbbPerfilesMensajeria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbPerfilesMensajeria.Size = new System.Drawing.Size(289, 20);
            this.cbbPerfilesMensajeria.TabIndex = 3;
            this.cbbPerfilesMensajeria.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbPerfilesMensajeria_SelectedIndexChanged);
            // 
            // Image1
            // 
            this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(532, 8);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(44, 48);
            this.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Image1.TabIndex = 15;
            this.Image1.TabStop = false;
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(16, 96);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(46, 18);
            this.Label1.TabIndex = 16;
            this.Label1.Text = "Buscar...";
            // 
            // cbbPerfilesAplicacion
            // 
            this.cbbPerfilesAplicacion.ColumnWidths = "";
            this.cbbPerfilesAplicacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbPerfilesAplicacion.Location = new System.Drawing.Point(172, 8);
            this.cbbPerfilesAplicacion.Name = "cbbPerfilesAplicacion";
            this.cbbPerfilesAplicacion.Size = new System.Drawing.Size(289, 20);
            this.cbbPerfilesAplicacion.TabIndex = 1;
            this.cbbPerfilesAplicacion.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbPerfilesAplicacion_SelectedIndexChanged);
            // 
            // SEL_USUARIO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 492);
            this.Controls.Add(this.opUsuarios);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.opPerfilesM);
            this.Controls.Add(this.opPerfilesA);
            this.Controls.Add(this.frmUsuarios);
            this.Controls.Add(this.CbCerrar);
            this.Controls.Add(this.CbAceptar);
            this.Controls.Add(this.cbbPerfilesMensajeria);
            this.Controls.Add(this.Image1);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.cbbPerfilesAplicacion);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SEL_USUARIO";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selecci�n de Usuarios - MEN003F1";
            this.Closed += new System.EventHandler(this.SEL_USUARIO_Closed);
            this.Load += new System.EventHandler(this.SEL_USUARIO_Load);
            ((System.ComponentModel.ISupportInitialize)(this.opUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opPerfilesM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opPerfilesA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmUsuarios)).EndInit();
            this.frmUsuarios.ResumeLayout(false);
            this.frmUsuarios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenorMenor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayorMayor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbSeleccionUsuarios.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbSeleccionUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbUsuarios.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPerfilesMensajeria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPerfilesAplicacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
    }
}