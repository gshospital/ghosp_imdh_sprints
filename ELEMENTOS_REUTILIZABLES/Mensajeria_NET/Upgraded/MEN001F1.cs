using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;

namespace Mensajeria
{
	public partial class MENU_MENSAJES
        : Telerik.WinControls.UI.RadForm
	{

        int Col;
        int Row;

		string mModo = String.Empty;
		//E-Modo Bandeja de Entrada
		//S-Modo Bandeja de Salida
		//B-Modo Elementos Eliminados


		int irow_ultima = 0; //guarda la ultima fila donde se hizo click
		bool bRegistroSeleccionado = false;
		string idMensaje = String.Empty;

		int OrdenAnteriorDe = 0;
		int OrdenAnteriorPara = 0;
		int OrdenAnteriorAsunto = 0;
		int OrdenAnteriorFechaEnvio = 0;
		int OrdenAnteriorFechaLectura = 0;
		int OrdenAnteriorFechaBorrado = 0;

		//constantes relacionas con el raton
		private const int WM_RBUTTONUP = 0x205;
		private const int WM_LBUTTONDBLCLK = 0x203;
		private const int WM_MOUSEMOVE = 0x200;
		//constantes de lo que queremos que muestre el icono
		private const int NIF_ICON = 0x2; // queremos que muestre un icono
		private const int NIF_MESSAGE = 0x1; // queremos que nos envie un mensaje
		private const int NIF_TIP = 0x4; // queremos que muestre un texto al posicionarnos encima
		//constantes para a�adir, borrar o modificar el icono
		private const int NIM_ADD = 0x0; // a�adirlo a la barra de tareas
		private const int NIM_DELETE = 0x2; // borrarlo de la barra de tareas
		private const int NIM_MODIFY = 0x1; // modificarlo
		
		public MENU_MENSAJES()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            SprMensajes.SelectionMode = GridViewSelectionMode.FullRowSelect;
		}




		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Hide();
		}

		public void cbRefrescar_Click(Object eventSender, EventArgs eventArgs)
		{
			switch(mModo)
			{
				case "E" :  //Bandeja de Entrada 
					proHabilitaDeshabilitaBotones(false); 
					proConfiguraSpread(); 
					proCargarElementosRecibidos(); 
					break;
				case "S" :  //Bandeja de Salida 
					proHabilitaDeshabilitaBotones(false); 
					proConfiguraSpread(); 
					proCargarElementosEnviados(); 
					break;
				case "B" :  //Elementos eliminados 
					proHabilitaDeshabilitaBotones(false); 
					proConfiguraSpread(); 
					proCargarElementosEliminados(); 
					break;
			}
			proSeleccionaMensaje();
		}

		private void MENU_MENSAJES_Load(Object eventSender, EventArgs eventArgs)
		{
			//UPGRADE_ISSUE: (2064) Form property MENU_MENSAJES.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//this.setHelpContextID(225001);
			//    lFilaSeleccionada = 0
			//    VstUltimaFila = 0
			bRegistroSeleccionado = false;
			Timer1.Enabled = false;
			Timer2.Enabled = false;
			string sql = "select gconsglo,nnumeri1,nnumeri2 from sconsglo where gconsglo='MENSAPRI'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BMensajeria.rcConexion);
			DataSet rraux = new DataSet();
			tempAdapter.Fill(rraux);
			if (rraux.Tables[0].Rows.Count != 0)
			{
				
				if (Conversion.Val(Convert.ToString(rraux.Tables[0].Rows[0]["nnumeri1"]) + "") != 0)
				{
					if (Convert.ToInt32(Convert.ToDouble(rraux.Tables[0].Rows[0]["nnumeri1"]) * 1000) == 0)
					{
						Timer1.Enabled = false;
					}
					else
					{
						Timer1.Interval = Convert.ToInt32(Convert.ToDouble(rraux.Tables[0].Rows[0]["nnumeri1"]) * 1000);
						Timer1.Enabled = true;
					}
					Timer1.Enabled = true;
				}
				if (Conversion.Val(Convert.ToString(rraux.Tables[0].Rows[0]["nnumeri2"]) + "") != 0)
				{
					if (Convert.ToInt32(Convert.ToDouble(rraux.Tables[0].Rows[0]["nnumeri2"]) * 1000) == 0)
					{
						Timer2.Enabled = false;
					}
					else
					{
						Timer2.Interval = Convert.ToInt32(Convert.ToDouble(rraux.Tables[0].Rows[0]["nnumeri2"]) * 1000);
						Timer2.Enabled = true;
					}
					Timer2.Enabled = true;
				}
			}
			rraux.Close();
			proRecolocaControles();
            _Toolbar1_Button1.PerformClick();
		}

		private void proHabilitaDeshabilitaBotones(bool Para)
		{
            _Toolbar1_Button4.Enabled = Para; //Eliminar
            _Toolbar1_Button5.Enabled = Para; //Responder
            _Toolbar1_Button6.Enabled = Para; //Responder a todos
            _Toolbar1_Button7.Enabled = Para; //Reenviar
		}


		private void MENU_MENSAJES_FormClosing(Object eventSender, FormClosingEventArgs eventArgs)
		{
			int Cancel = (eventArgs.Cancel) ? 1 : 0;
			int UnloadMode = (int) eventArgs.CloseReason;
			Cancel = -1;
			cbCerrar_Click(cbCerrar, new EventArgs());
			eventArgs.Cancel = Cancel != 0;
		}

		private void SprMensajes_KeyDown(Object eventSender, KeyEventArgs eventArgs)
        {   int KeyCode = (int)eventArgs.KeyCode;
            int Shift = (eventArgs.Shift) ? 1 : 0;
            if (KeyCode == ((int)Keys.Up))
            {
                
                if (SprMensajes.CurrentRow.Index > 1)
                {
                    SprMensajes_CellClick(SprMensajes, null);
                }
                else
                {
                    KeyCode = 0;
                }
            }
            else if (KeyCode == ((int)Keys.Down))
            {
                if (SprMensajes.CurrentRow.Index > 1  && SprMensajes.CurrentRow.Index < SprMensajes.MaxRows)
                {
                    SprMensajes_CellClick(SprMensajes, null);
                }
                else
                {
                    KeyCode = 0;
                }
            }
            else
            {
                KeyCode = 0;
            }
		}

        private void SprMensajes_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
        {
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            
            string stFechaLectura = String.Empty;            
            if (Row == 0)
            {
                idMensaje = "";
                bRegistroSeleccionado = false;
                txtMensaje.Text = "";
                _Toolbar1_Button4.Enabled = false; //Eliminar
                _Toolbar1_Button5.Enabled = false; //Responder
                _Toolbar1_Button6.Enabled = false; //Responder a todos
                _Toolbar1_Button7.Enabled = false; //Reenviar
                SprMensajes.ReadOnly = true;
                SprMensajes.Row = 1;
                SprMensajes.Col = BMensajeria.icol_De;
                SprMensajes.Row2 = SprMensajes.MaxRows;
                SprMensajes.Col2 = SprMensajes.MaxCols;
                SprMensajes.SortBy = UpgradeHelpers.Spread.SortByConstants.SortByRow; //Ordeno por columna
                switch (Col)
                {
                    case BMensajeria.icol_De:
                        SprMensajes.SetSortKey(1, BMensajeria.icol_De);
                        SprMensajes.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants)OrdenAnteriorDe);
                        OrdenAnteriorDe = (OrdenAnteriorDe == 1) ? 2 : 1;
                        break;
                    case BMensajeria.icol_Para:
                        SprMensajes.SetSortKey(1, BMensajeria.icol_Para);
                        SprMensajes.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants)OrdenAnteriorPara);
                        OrdenAnteriorPara = (OrdenAnteriorPara == 1) ? 2 : 1;
                        break;
                    case BMensajeria.icol_Asunto:
                        SprMensajes.SetSortKey(1, BMensajeria.icol_Asunto);
                        SprMensajes.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants)OrdenAnteriorAsunto);
                        OrdenAnteriorAsunto = (OrdenAnteriorAsunto == 1) ? 2 : 1;
                        break;
                    case BMensajeria.icol_FechaEnvio:
                        SprMensajes.SetSortKey(1, BMensajeria.icol_FechaEnvio_ORD);
                        SprMensajes.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants)OrdenAnteriorFechaEnvio);
                        OrdenAnteriorFechaEnvio = (OrdenAnteriorFechaEnvio == 1) ? 2 : 1;
                        break;
                    case BMensajeria.icol_FechaLectura:
                        SprMensajes.SetSortKey(1, BMensajeria.icol_FechaLectura_ORD);
                        SprMensajes.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants)OrdenAnteriorFechaLectura);
                        OrdenAnteriorFechaLectura = (OrdenAnteriorFechaLectura == 1) ? 2 : 1;
                        break;
                    case BMensajeria.icol_FechaBorrado:
                        SprMensajes.SetSortKey(1, BMensajeria.icol_FechaBorrado_ORD);
                        SprMensajes.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants)OrdenAnteriorFechaBorrado);
                        OrdenAnteriorFechaBorrado = (OrdenAnteriorFechaBorrado == 1) ? 2 : 1;
                        break;
                }
                SprMensajes.Action = UpgradeHelpers.Spread.ActionConstants.ActionSort;
                SprMensajes.CurrentRow = null;
            }
            else
            {
                SprMensajes.Row = Row;
                SprMensajes.Col = Col;
                if (bRegistroSeleccionado)
                {
                    if (irow_ultima == Row)
                    {
                        bRegistroSeleccionado = false;
                        SprMensajes.ReadOnly = true;
                    }
                    else
                    {
                        bRegistroSeleccionado = true;
                        SprMensajes.ReadOnly = true;
                    }
                }
                else
                {
                    bRegistroSeleccionado = true;
                    SprMensajes.ReadOnly = true;
                }

                if (bRegistroSeleccionado)
                {
                    SprMensajes.Col = BMensajeria.icol_IdMensaje;
                    idMensaje = SprMensajes.Text;
                    txtMensaje.Text = BMensajeria.fCargaDetalleMensaje(idMensaje);
                    switch (mModo)
                    {
                        case "E":
                        case "S":
                        case "B":
                            _Toolbar1_Button4.Enabled = true;  //Eliminar 
                            _Toolbar1_Button5.Enabled = true;  //Responder 
                            _Toolbar1_Button6.Enabled = true;  //Responder a todos 
                            _Toolbar1_Button7.Enabled = true;  //Reenviar 
                            break;
                    }
                    if (mModo == "E")
                    {
                        SprMensajes.Col = BMensajeria.icol_FechaLectura;
                        if (SprMensajes.Text.Trim() == "")
                        {
                            stFechaLectura = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                            SprMensajes.Col = BMensajeria.icol_FechaLectura;
                            SprMensajes.Text = stFechaLectura;
                            int tempForVar = SprMensajes.MaxCols;
                            for (int i = 1; i <= tempForVar; i++)
                            {
                                SprMensajes.Col = i;                               
                                SprMensajes.foreColor = Color.Black;
                            }
                            proActualizaFechaLectura(idMensaje, stFechaLectura);
                        }
                    }
                }
                else
                {
                    txtMensaje.Text = "";
                    _Toolbar1_Button4.Enabled = false; //Eliminar
                    _Toolbar1_Button5.Enabled = false; //Responder
                    _Toolbar1_Button6.Enabled = false; //Responder a todos
                    _Toolbar1_Button7.Enabled = false; //Reenviar
                }
                irow_ultima = Row;
            }
        }

		private void proBuscaMensaje(string idMensaje)
        {
            int tempForVar = SprMensajes.MaxRows;
            for (int i = 1; i <= tempForVar; i++)
            {
                SprMensajes.Row = i;
                SprMensajes.Col = BMensajeria.icol_IdMensaje;
                if (SprMensajes.Text.Trim() == idMensaje.Trim())
                {   
                    SprMensajes_CellClick(SprMensajes, null);
                    break;
                }
            }

        }

		private void proActualizaFechaLectura(string idMensaje, string stFechaLectura)
		{
			//La fecha de lectura solamente se debe actualizar la primera vez y para el usuario conectado
			int iPos = (idMensaje.IndexOf('/') + 1);
			int Amensaje = Convert.ToInt32(Double.Parse(idMensaje.Substring(0, Math.Min(iPos - 1, idMensaje.Length))));
			int Nmensaje = Convert.ToInt32(Double.Parse(idMensaje.Substring(iPos)));
			object tempRefParam = stFechaLectura;
			string sql = "UPDATE smenpusu set flectume = " + BMensajeria.FormatFechaHMS(tempRefParam) + 
			             " WHERE amensaje = " + Amensaje.ToString() + 
			             "   and nmensaje = " + Nmensaje.ToString() + 
			             "   and gusuario = '" + BMensajeria.gUsuario + "'" + 
			             "   and flectume is null ";
			stFechaLectura = Convert.ToString(tempRefParam);
			SqlCommand tempCommand = new SqlCommand(sql, BMensajeria.rcConexion);
			tempCommand.ExecuteNonQuery();
		}

		private void Timer1_Tick(Object eventSender, EventArgs eventArgs)
		{
			for (int i = 0; i <= Application.OpenForms.Count - 1; i++)
			{
				if (Application.OpenForms[i].Name.Trim().ToUpper() == "MENU_MENSAJES")
				{
					cbRefrescar_Click(cbRefrescar, new EventArgs());
					break;
				}
			}
		}

		private void Timer2_Tick(Object eventSender, EventArgs eventArgs)
		{
			string stCuantos = String.Empty;
			if (this.Visible)
			{
				return;
			}
			if (BMensajeria.fMensajesNoLeidos(ref stCuantos))
			{
				DETALLE_AVISO.DefInstance.Label1.Text = "Tiene " + stCuantos + " Mensaje" + ((stCuantos == "1") ? "" : "s") + " sin leer en la Bandeja de Elementos Recibidos";
				DETALLE_AVISO.DefInstance.EstableceNotificacion();
				DETALLE_AVISO.DefInstance.Timer1.Interval = 5000;
				DETALLE_AVISO.DefInstance.Timer1.Enabled = true;
				DETALLE_AVISO.DefInstance.Timer1.Enabled = true;
			}
		}

		private void Toolbar1_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
            Telerik.WinControls.UI.CommandBarButton Button = (Telerik.WinControls.UI.CommandBarButton)eventSender;
            switch (radCommandBarStripElement1.Items.IndexOf(Button) + 1)
            {
				case 1 :  //Bandeja de Entrada 
					mModo = "E"; 
					Label1.Text = "Mensajes recibidos"; 
					proHabilitaDeshabilitaBotones(false); 
					proConfiguraSpread(); 
					proCargarElementosRecibidos();
                    SprMensajes.CurrentRow = null;
					break;
				case 2 :  //Bandeja de Salida 
					mModo = "S"; 
					Label1.Text = "Mensajes enviados"; 
					proHabilitaDeshabilitaBotones(false); 
					proConfiguraSpread(); 
					proCargarElementosEnviados();
                    SprMensajes.CurrentRow = null;
					break;
				case 3 :  //Elementos eliminados 
					mModo = "B"; 
					Label1.Text = "Mensajes eliminados"; 
					proHabilitaDeshabilitaBotones(false); 
					proConfiguraSpread(); 
					proCargarElementosEliminados();
                    SprMensajes.CurrentRow = null;
					break;
				case 4 :  //Nuevo 
					proNuevoMensaje(); 
					break;
				case 5 :  //Eliminar 
					proEliminaMensaje(); 
					break;
				case 6 :  //Responder 
					proResponder("U"); 
					break;
				case 7 :  //Responder a todos 
					proResponder("T"); 
					break;
				case 8 :  //Reenviar 
					proReenviar(); 
					break;
                case 10: cbCerrar_Click(cbCerrar, new EventArgs()); 
					break;
			}            
		}

		private void proNuevoMensaje()
		{
			DETALLE_MENSAJE.DefInstance.Text = "Nuevo - MEN002F1";
			DETALLE_MENSAJE.DefInstance.ShowDialog();
		}

		private void proEliminaMensaje()
		{
            string sql = String.Empty;
            string stTabla = String.Empty;
            SprMensajes.Row = SprMensajes.ActiveRowIndex;
            SprMensajes.Col = BMensajeria.icol_IdMensaje;
            int iPos = (SprMensajes.Text.IndexOf('/') + 1);
            int Amensaje = Convert.ToInt32(Double.Parse(SprMensajes.Text.Substring(0, Math.Min(iPos - 1, SprMensajes.Text.Length))));
            int Nmensaje = Convert.ToInt32(Double.Parse(SprMensajes.Text.Substring(iPos)));
            switch(mModo)
            {
                case "E" :  //Bandeja de Entrada 
                    stTabla = "SMENPUSU"; 
                    object tempRefParam = DateTime.Now; 
                    sql = "UPDATE " + stTabla + " set fborrado = " + BMensajeria.FormatFechaHMS(tempRefParam) + 
                          " WHERE amensaje = " + Amensaje.ToString() + 
                          "   and nmensaje = " + Nmensaje.ToString() + 
                          "   and gusuario = '" + BMensajeria.gUsuario + "'" + 
                          "   and fborrado is null "; 
                    break;
                case "S" :  //Elementos Enviados 
                    stTabla = "STEXMENP"; 
                    object tempRefParam2 = DateTime.Now; 
                    sql = "UPDATE " + stTabla + " set fborrado = " + BMensajeria.FormatFechaHMS(tempRefParam2) + 
                          " WHERE amensaje = " + Amensaje.ToString() + 
                          "   and nmensaje = " + Nmensaje.ToString() + 
                          "   and gusuario = '" + BMensajeria.gUsuario + "'" + 
                          "   and fborrado is null "; 
                    break;
                case "B" :  //Elementos Eliminados 
                    SprMensajes.Col = BMensajeria.icol_iTipoMensaje; 
                    switch(SprMensajes.Text)
                    {
                        case "R" : 
                            stTabla = "SMENPUSU"; 
                            break;
                        case "E" : 
                            stTabla = "STEXMENP"; 
                            break;
                    } 
                    object tempRefParam3 = DateTime.Now; 
                    sql = "UPDATE " + stTabla + " set fborrdef = " + BMensajeria.FormatFechaHMS(tempRefParam3) + 
                          " WHERE amensaje = " + Amensaje.ToString() + 
                          "   and nmensaje = " + Nmensaje.ToString() + 
                          "   and gusuario = '" + BMensajeria.gUsuario + "'" + 
                          "   and fborrdef is null "; 
                    break;
            }

            SqlCommand tempCommand = new SqlCommand(sql, BMensajeria.rcConexion);
            tempCommand.ExecuteNonQuery();
            cbRefrescar_Click(cbRefrescar, new EventArgs());
		}

		private void proResponder(string AQuien)
		{
            SprMensajes.Row = SprMensajes.ActiveRowIndex;
            SprMensajes.Col = BMensajeria.icol_IdMensaje;
            string idMensaje = SprMensajes.Text.Trim();
            SprMensajes.Col = BMensajeria.icol_Asunto;
            string stAsunto = "RE: " + SprMensajes.Text.Trim();
            DETALLE_MENSAJE.DefInstance.Text = "Responder - MEN002F1";
            DETALLE_MENSAJE.DefInstance.txtAsunto.Text = stAsunto;
            DETALLE_MENSAJE.DefInstance.txtMensaje.Text = Environment.NewLine + txtMensaje.Text.Trim();
            switch (AQuien)
            {
                case "U":
                    DETALLE_MENSAJE.DefInstance.proResponderUsuario(idMensaje);
                    break;
                case "T":
                    DETALLE_MENSAJE.DefInstance.proResponderTodos(idMensaje);
                    break;
            }
            DETALLE_MENSAJE.DefInstance.ShowDialog();
		}

		private void proReenviar()
		{
            
            SprMensajes.Row = SprMensajes.ActiveRowIndex;
            SprMensajes.Col = BMensajeria.icol_Asunto;
            string stAsunto = "RV: " + SprMensajes.Text.Trim();
            DETALLE_MENSAJE.DefInstance.Text = "Reenviar - MEN002F1";
            DETALLE_MENSAJE.DefInstance.txtAsunto.Text = stAsunto;
            DETALLE_MENSAJE.DefInstance.txtMensaje.Text = Environment.NewLine + txtMensaje.Text.Trim();
            DETALLE_MENSAJE.DefInstance.ShowDialog();
		}

		private void proRecolocaControles()
		{
            this.Top = (int) 0;
            this.Left = (int) 0;
            this.Height = (int) (Screen.PrimaryScreen.Bounds.Height - 37);
            this.Width = (int) Screen.PrimaryScreen.Bounds.Width;
            SprMensajes.Width = (int) (this.Width - 13);
            SprMensajes.Height = (int) ((this.Height) / 2);
            txtMensaje.Top = (int) (SprMensajes.Height + 60);
            txtMensaje.Width = (int) (SprMensajes.Width - 100);
            txtMensaje.Height = (int) (SprMensajes.Height - 100);
            cbCerrar.Top = (int) (this.Height - (cbCerrar.Height * 2));
            cbRefrescar.Top = (int) (this.Height - (cbRefrescar.Height * 3));
            cbRefrescar.Left = (int) (this.Width - cbRefrescar.Width - 13);
            cbCerrar.Left = (int) cbRefrescar.Left;
		}

		private void proConfiguraSpread()
		{
            txtMensaje.Text = "";
            SprMensajes.ReadOnly = true;
            SprMensajes.MaxRows = 0;
            SprMensajes.MaxCols = 11;
            SprMensajes.Row = 0;
            SprMensajes.Col = BMensajeria.icol_De;
            SprMensajes.Text = "De...";
            SprMensajes.Col = BMensajeria.icol_Para;
            SprMensajes.Text = "Para...";
            SprMensajes.Col = BMensajeria.icol_FechaEnvio;
            SprMensajes.Text = "Fecha Env�o";
            SprMensajes.Col = BMensajeria.icol_FechaLectura;
            SprMensajes.Text = "Fecha Lectura";
            SprMensajes.Col = BMensajeria.icol_FechaBorrado;
            SprMensajes.Text = "Fecha Borrado";
            SprMensajes.Col = BMensajeria.icol_Asunto;
            SprMensajes.Text = "Asunto";
            SprMensajes.Col = BMensajeria.icol_IdMensaje;
            SprMensajes.Text = "Id Mensaje";
            SprMensajes.Col = BMensajeria.icol_FechaEnvio_ORD;
            SprMensajes.Text = "Fecha Env�o Ord";
            SprMensajes.SetColHidden(SprMensajes.Col, true);
            SprMensajes.Col = BMensajeria.icol_FechaLectura_ORD;
            SprMensajes.Text = "Fecha Borrado Ord";
            SprMensajes.SetColHidden(SprMensajes.Col, true);
            SprMensajes.Col = BMensajeria.icol_FechaBorrado_ORD;
            SprMensajes.Text = "Fecha Borrado Ord";
            SprMensajes.SetColHidden(SprMensajes.Col, true);
            SprMensajes.Col = BMensajeria.icol_iTipoMensaje;
            SprMensajes.SetColHidden(SprMensajes.Col, true);

            switch(mModo)
            {
                case "E" :  //Modo Bandeja de Entrada 
                    SprMensajes.Col = BMensajeria.icol_De; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.Col = BMensajeria.icol_Para; 
                    SprMensajes.SetColHidden(SprMensajes.Col, true); 
                    SprMensajes.Col = BMensajeria.icol_FechaEnvio; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.Col = BMensajeria.icol_FechaLectura; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.Col = BMensajeria.icol_FechaBorrado; 
                    SprMensajes.SetColHidden(SprMensajes.Col, true); 
                    SprMensajes.SetColWidth(BMensajeria.icol_De, (float) (((float) (SprMensajes.Width * 15)) / 5.1d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_FechaEnvio, (float) (((float) (SprMensajes.Width * 15)) / 5.1d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_FechaLectura, (float) (((float) (SprMensajes.Width * 15)) / 5.1d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_Asunto, (float) (((float) (SprMensajes.Width * 15)) / 5.1d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_IdMensaje, (float) (((float) (SprMensajes.Width * 15)) / 5.1d)); 
                    break;
                case "S" :  //Modo Bandeja de Salida 
                    SprMensajes.Col = BMensajeria.icol_De; 
                    SprMensajes.SetColHidden(SprMensajes.Col, true); 
                    SprMensajes.Col = BMensajeria.icol_Para; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.Col = BMensajeria.icol_FechaEnvio; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.Col = BMensajeria.icol_FechaLectura; 
                    SprMensajes.SetColHidden(SprMensajes.Col, true); 
                    SprMensajes.Col = BMensajeria.icol_FechaBorrado; 
                    SprMensajes.SetColHidden(SprMensajes.Col, true); 
                    SprMensajes.SetColWidth(BMensajeria.icol_Para, (float) (((float) (SprMensajes.Width * 15)) / 4.1d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_FechaEnvio, (float) (((float) (SprMensajes.Width * 15)) / 4.1d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_Asunto, (float) (((float) (SprMensajes.Width * 15)) / 4.1d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_IdMensaje, (float) (((float) (SprMensajes.Width * 15)) / 4.1d)); 
                    break;
                case "B" :  //Modo Elementos Eliminados 
                    SprMensajes.Col = BMensajeria.icol_De; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.Col = BMensajeria.icol_Para; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.Col = BMensajeria.icol_FechaEnvio; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.Col = BMensajeria.icol_FechaLectura; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.Col = BMensajeria.icol_FechaBorrado; 
                    SprMensajes.SetColHidden(SprMensajes.Col, false); 
                    SprMensajes.SetColWidth(BMensajeria.icol_De, (float) (((float) (SprMensajes.Width * 15)) / 7.2d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_Para, (float) (((float) (SprMensajes.Width * 15)) / 7.2d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_FechaEnvio, (float) (((float) (SprMensajes.Width * 15)) / 7.2d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_FechaLectura, (float) (((float) (SprMensajes.Width * 15)) / 7.2d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_Asunto, (float) (((float) (SprMensajes.Width * 15)) / 7.2d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_FechaBorrado, (float) (((float) (SprMensajes.Width * 15)) / 7.2d)); 
                    SprMensajes.SetColWidth(BMensajeria.icol_IdMensaje, (float) (((float) (SprMensajes.Width * 15)) / 7.2d)); 
                    break;
            }
		}

		private void proCargarElementosEnviados()
		{
            string stDestinatarios = String.Empty;
            OrdenAnteriorDe = 1;
            OrdenAnteriorPara = 1;
            OrdenAnteriorAsunto = 1;
            OrdenAnteriorFechaLectura = 1;
            OrdenAnteriorFechaBorrado = 1;
            string sql = "" + 
                         " SELECT STEXMENP.fechamen,  STEXMENP.asuntome, STEXMENP.amensaje, STEXMENP.nmensaje" + 
                         " From STEXMENP" + 
                         " WHERE " + 
                         " STEXMENP.gusuario = '" + BMensajeria.gUsuario + "' and " + 
                         " STEXMENP.fborrado Is Null" + 
                         " ORDER BY STEXMENP.fechamen desc ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BMensajeria.rcConexion);
            DataSet rrDatos = new DataSet();
            tempAdapter.Fill(rrDatos);
            if (rrDatos.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow iteration_row in rrDatos.Tables[0].Rows)
                {
                    stDestinatarios = BMensajeria.fObtenerDestinatarios(Convert.ToInt32(iteration_row["amensaje"]), Convert.ToInt32(iteration_row["nmensaje"]));
                    SprMensajes.MaxRows++;
                    SprMensajes.Row = SprMensajes.MaxRows;
                    //SprMensajes.SetRowHeight(SprMensajes.Row, 250);
                    SprMensajes.Col = BMensajeria.icol_FechaEnvio;
                    SprMensajes.Text = Convert.ToDateTime(iteration_row["fechamen"]).ToString("dd/MM/yyyy HH:mm");
                    SprMensajes.Col = BMensajeria.icol_FechaEnvio_ORD;
                    SprMensajes.Text = Convert.ToDateTime(iteration_row["fechamen"]).ToString("yyyyMMddHHmmss");
                    SprMensajes.Col = BMensajeria.icol_Asunto;
                    SprMensajes.Text = Convert.ToString(iteration_row["asuntome"]) + "";
                    SprMensajes.Col = BMensajeria.icol_IdMensaje;
                    SprMensajes.Text = Convert.ToString(iteration_row["amensaje"]) + "/" + Convert.ToString(iteration_row["nmensaje"]);
                    SprMensajes.Col = BMensajeria.icol_Para;
                    SprMensajes.Text = stDestinatarios;
                    SprMensajes.Col = BMensajeria.icol_iTipoMensaje;
                    SprMensajes.Text = "E";
                }
            }
            rrDatos.Close();
		}

		private void proCargarElementosRecibidos()
        {   
            string stEmisor = String.Empty;
            Color lcolor = new Color();
            OrdenAnteriorDe = 1;
            OrdenAnteriorPara = 1;
            OrdenAnteriorAsunto = 1;
            OrdenAnteriorFechaLectura = 1;
            OrdenAnteriorFechaBorrado = 1;
            string sql = "" +
                         " SELECT SMENPUSU.fenviome, SMENPUSU.flectume, STEXMENP.asuntome, STEXMENP.amensaje, STEXMENP.nmensaje" +
                         " From STEXMENP" +
                         " INNER JOIN SMENPUSU on STEXMENP.amensaje = SMENPUSU.amensaje and STEXMENP.nmensaje = SMENPUSU.nmensaje" +
                         " WHERE " +
                         " SMENPUSU.gusuario = '" + BMensajeria.gUsuario + "' and " +
                         " SMENPUSU.fborrado Is Null" +
                         " ORDER BY SMENPUSU.fenviome desc ";
            //" and STEXMENP.fborrado Is Null "
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BMensajeria.rcConexion);
            DataSet rrDatos = new DataSet();
            tempAdapter.Fill(rrDatos);
            if (rrDatos.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow iteration_row in rrDatos.Tables[0].Rows)
                {
                    stEmisor = BMensajeria.fObtenerEmisor(Convert.ToInt32(iteration_row["amensaje"]), Convert.ToInt32(iteration_row["nmensaje"]));
                    SprMensajes.MaxRows++;
                    SprMensajes.Row = SprMensajes.MaxRows;
                    //SprMensajes.SetRowHeight(SprMensajes.Row, 250);
                    if (!Convert.IsDBNull(iteration_row["flectume"]))
                    {
                        lcolor = Color.Black;
                        SprMensajes.Col = BMensajeria.icol_FechaLectura;
                        SprMensajes.Text = Convert.ToDateTime(iteration_row["flectume"]).ToString("dd/MM/yyyy HH:mm");
                        SprMensajes.foreColor = lcolor;
                        SprMensajes.Col = BMensajeria.icol_FechaLectura_ORD;
                        SprMensajes.SetColHidden(SprMensajes.Col, true);
                        SprMensajes.Text = Convert.ToDateTime(iteration_row["flectume"]).ToString("yyyyMMddHHmmss");
                    }
                    else
                    {
                        lcolor = Color.Red;
                    }
                    SprMensajes.Col = BMensajeria.icol_De;
                    SprMensajes.Text = stEmisor;
                    SprMensajes.foreColor=lcolor;
                    SprMensajes.Col = BMensajeria.icol_FechaEnvio;
                    SprMensajes.Text = Convert.ToDateTime(iteration_row["fenviome"]).ToString("dd/MM/yyyy HH:mm");
                    SprMensajes.foreColor=lcolor;
                    SprMensajes.Col = BMensajeria.icol_FechaEnvio_ORD;
                    SprMensajes.Text = Convert.ToDateTime(iteration_row["fenviome"]).ToString("yyyyMMddHHmmss");
                    SprMensajes.foreColor=lcolor;
                    SprMensajes.Col = BMensajeria.icol_Asunto;
                    SprMensajes.Text = Convert.ToString(iteration_row["asuntome"]) + "";
                    SprMensajes.foreColor=lcolor;
                    SprMensajes.Col = BMensajeria.icol_IdMensaje;
                    SprMensajes.Text = Convert.ToString(iteration_row["amensaje"]) + "/" + Convert.ToString(iteration_row["nmensaje"]);
                    SprMensajes.foreColor=lcolor;
                    SprMensajes.Col = BMensajeria.icol_iTipoMensaje;
                    SprMensajes.Text = "R";
                    SprMensajes.foreColor=lcolor;
                }
            }
            rrDatos.Close();
		}

		private void proCargarElementosEliminados()
		{
            string stEmisor = String.Empty;
            string stDestinatarios = String.Empty;
            OrdenAnteriorDe = 1;
            OrdenAnteriorPara = 1;
            OrdenAnteriorAsunto = 1;
            OrdenAnteriorFechaLectura = 1;
            OrdenAnteriorFechaBorrado = 1;
            string sql = "" +
                         " SELECT 'E' as TipoMensaje, STEXMENP.fechamen,  NULL flectume,  STEXMENP.asuntome, STEXMENP.amensaje, STEXMENP.nmensaje, STEXMENP.fborrado" +
                         " From STEXMENP" +
                         " WHERE " +
                         " STEXMENP.gusuario = '" + BMensajeria.gUsuario + "' and " +
                         " STEXMENP.fborrado Is not Null and STEXMENP.fborrdef is null" +
                         " UNION ALL " +
                         " SELECT 'R' as TipoMensaje, SMENPUSU.fenviome as fechamen, SMENPUSU.flectume, STEXMENP.asuntome, STEXMENP.amensaje, STEXMENP.nmensaje, SMENPUSU.fborrado" +
                         " From STEXMENP" +
                         " INNER JOIN SMENPUSU on STEXMENP.amensaje = SMENPUSU.amensaje and STEXMENP.nmensaje = SMENPUSU.nmensaje" +
                         " WHERE " +
                         " SMENPUSU.gusuario = '" + BMensajeria.gUsuario + "' and " +
                         " SMENPUSU.fborrado Is not Null and SMENPUSU.fborrdef is null" +
                         " order by fechamen ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BMensajeria.rcConexion);
            DataSet rrDatos = new DataSet();
            tempAdapter.Fill(rrDatos);
            if (rrDatos.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow iteration_row in rrDatos.Tables[0].Rows)
                {
                    stEmisor = BMensajeria.fObtenerEmisor(Convert.ToInt32(iteration_row["amensaje"]), Convert.ToInt32(iteration_row["nmensaje"]));
                    stDestinatarios = BMensajeria.fObtenerDestinatarios(Convert.ToInt32(iteration_row["amensaje"]), Convert.ToInt32(iteration_row["nmensaje"]));
                    SprMensajes.MaxRows++;
                    SprMensajes.Row = SprMensajes.MaxRows;
                    //SprMensajes.SetRowHeight(SprMensajes.Row, 250);
                    SprMensajes.Col = BMensajeria.icol_De;
                    SprMensajes.Text = stEmisor;
                    SprMensajes.Col = BMensajeria.icol_Para;
                    SprMensajes.Text = stDestinatarios;
                    SprMensajes.Col = BMensajeria.icol_FechaEnvio;
                    SprMensajes.Text = Convert.ToDateTime(iteration_row["fechamen"]).ToString("dd/MM/yyyy HH:mm");
                    SprMensajes.Col = BMensajeria.icol_FechaEnvio_ORD;
                    SprMensajes.Text = Convert.ToDateTime(iteration_row["fechamen"]).ToString("yyyyMMddHHmmss");
                    
                    if (!Convert.IsDBNull(iteration_row["flectume"]))
                    {
                        SprMensajes.Col = BMensajeria.icol_FechaLectura;
                        SprMensajes.Text = Convert.ToDateTime(iteration_row["flectume"]).ToString("dd/MM/yyyy HH:mm");
                        SprMensajes.Col = BMensajeria.icol_FechaLectura_ORD;
                        SprMensajes.SetColHidden(SprMensajes.Col, true);
                        SprMensajes.Text = Convert.ToDateTime(iteration_row["flectume"]).ToString("yyyyMMddHHmmss");
                    }
                    SprMensajes.Col = BMensajeria.icol_Asunto;
                    SprMensajes.Text = Convert.ToString(iteration_row["asuntome"]) + "";
                    
                    if (!Convert.IsDBNull(iteration_row["fborrado"]))
                    {
                        SprMensajes.Col = BMensajeria.icol_FechaBorrado;
                        SprMensajes.Text = Convert.ToDateTime(iteration_row["fborrado"]).ToString("dd/MM/yyyy HH:mm");
                        SprMensajes.Col = BMensajeria.icol_FechaBorrado_ORD;
                        SprMensajes.SetColHidden(SprMensajes.Col, true);
                        SprMensajes.Text = Convert.ToDateTime(iteration_row["fborrado"]).ToString("yyyyMMddHHmmss");
                    }
                    SprMensajes.Col = BMensajeria.icol_IdMensaje;
                    SprMensajes.Text = Convert.ToString(iteration_row["amensaje"]) + "/" + Convert.ToString(iteration_row["nmensaje"]);
                    SprMensajes.Col = BMensajeria.icol_iTipoMensaje;
                    SprMensajes.Text = Convert.ToString(iteration_row["TipoMensaje"]);
                }
            }
            rrDatos.Close();
		}


		bool rec_MENU_MENSAJES_MouseMove = false;
		int MSG_MENU_MENSAJES_MouseMove = 0;
		private void MENU_MENSAJES_MouseMove(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float x = (float) (eventArgs.X * 15);
			float Y = (float) (eventArgs.Y * 15);
			MSG_MENU_MENSAJES_MouseMove = Convert.ToInt32(x / 15); // forma facil de obtener el lwparam para usarlo en vb (es que soy programador de C)
			if (!rec_MENU_MENSAJES_MouseMove)
			{ // use la variable rec para saber si ya se esta mostrando el menu y que no aparezca dos veces
				rec_MENU_MENSAJES_MouseMove = true; // activo la variable para saber que ya voy a mostrar el menu
				switch(MSG_MENU_MENSAJES_MouseMove)
				{
					case WM_LBUTTONDBLCLK :  // doble click con el boton izquierdo del raton 
						DETALLE_AVISO.DefInstance.ShowDialog();  // mostramos el form principal 
						//frm_Principal.Show 
						break;
					case WM_RBUTTONUP : 
						//                Me.PopupMenu Menu ' click con el boton secundario, mostramos el menu correspondiente 
						break;
				}
				rec_MENU_MENSAJES_MouseMove = false;
			}
		}

		private void proSeleccionaMensaje()
		{
            if (idMensaje != "" && bRegistroSeleccionado)
            {
                SprMensajes.Col = BMensajeria.icol_IdMensaje;
                int tempForVar = SprMensajes.MaxRows;
                for (int i = 1; i <= tempForVar; i++)
                {
                    SprMensajes.Row = i;
                    if (idMensaje.Trim() == SprMensajes.Text.Trim())
                    {
                        bRegistroSeleccionado = false;
                        SprMensajes_CellClick(SprMensajes, null);                         
                        SprMensajes.setSelModeIndex(i);
                        return;
                    }
                }
            }
		}
		private void MENU_MENSAJES_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}