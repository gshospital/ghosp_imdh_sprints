using Microsoft.VisualBasic;
using System;
using System.Runtime.InteropServices;
using UpgradeHelpers.Helpers;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Mensajeria
{
	public partial class DETALLE_AVISO
        : Telerik.WinControls.UI.RadForm
	{
        private const int WM_MOUSEMOVE = 0x200;
		private const int WM_RBUTTONDBLCLK = 0x206;
		private const int WM_RBUTTONDOWN = 0x204;
		private const int WM_RBUTTONUP = 0x205;
		private const int WM_LBUTTONDBLCLK = 0x203;
		private const int WM_LBUTTONDOWN = 0x201;
		private const int WM_LBUTTONUP = 0x202;
		private const int WM_MBUTTONDBLCLK = 0x209;
		private const int WM_MBUTTONDOWN = 0x207;
		private const int WM_MBUTTONUP = 0x208;

		private const int NIS_HIDDEN = 0x1;
		private const int NIS_SHAREDICON = 0x2;
		private const int NIM_ADD = 0x0;
		private const int NIM_MODIFY = 0x1;
		private const int NIM_DELETE = 0x2;
		private const int NIF_MESSAGE = 0x1;
		private const int NIF_ICON = 0x2;
		private const int NIF_TIP = 0x4;
		private const int NIF_STATE = 0x8;
		private const int NIF_INFO = 0x10;

		private UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.NOTIFYICONDATA nid = UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.NOTIFYICONDATA.CreateInstance();

		private const int vbNone = 0;
		private const int vbInformation = 1;
		private const int vbExclamation = 2;
		private const int vbCritical = 3;
		public DETALLE_AVISO()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void cmdNO_Click(Object eventSender, EventArgs eventArgs)
		{
			BMensajeria.iRespuesta = System.Windows.Forms.DialogResult.No;
			this.Close();
		}

		private void cmdSI_Click(Object eventSender, EventArgs eventArgs)
		{
			BMensajeria.iRespuesta = System.Windows.Forms.DialogResult.Yes;
			this.Close();
		}

		private void DETALLE_AVISO_Load(Object eventSender, EventArgs eventArgs)
		{
			this.Text = BMensajeria.vNomAplicacion;
		}

		public void EstableceNotificacion()
		{
            nid.cbSize = Marshal.SizeOf(nid);
            nid.hwnd = this.Handle.ToInt32();
            nid.uID = (int)VariantType.Null;
            nid.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE | NIF_INFO;
            nid.uCallbackMessage = WM_MOUSEMOVE;
            nid.szTip = Label1.Text + "\0";

            UpgradeSupportHelper.PInvoke.SafeNative.shell32.Shell_NotifyIcon(NIM_ADD, ref nid);
            nid.uTimeout = 3000;
            nid.dwInfoFlags = vbInformation;
            nid.szInfoTitle = "Mensajeria" + "\0";
            nid.szInfo = Label1.Text + "\0";
            UpgradeSupportHelper.PInvoke.SafeNative.shell32.Shell_NotifyIcon(NIM_MODIFY, ref nid);
		}


		private void Timer1_Tick(Object eventSender, EventArgs eventArgs)
		{
            UpgradeSupportHelper.PInvoke.SafeNative.shell32.Shell_NotifyIcon(NIM_DELETE, ref nid);
            Timer1.Enabled = false;
		}
		private void DETALLE_AVISO_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}