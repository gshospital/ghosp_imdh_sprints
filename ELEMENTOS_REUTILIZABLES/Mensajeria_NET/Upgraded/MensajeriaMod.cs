using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Mensajeria
{
	internal static class BMensajeria
	{
		public static Mensajes.ClassMensajes clase_mensaje = null;
		public static SqlConnection rcConexion = null;
		public static string gUsuario = String.Empty;

		public const int icol_De = 1;
		public const int icol_Para = 2;
		public const int icol_Asunto = 3;
		public const int icol_FechaEnvio = 4;
		public const int icol_FechaLectura = 5;
		public const int icol_FechaBorrado = 6;
		public const int icol_IdMensaje = 7;
		public const int icol_FechaEnvio_ORD = 8;
		public const int icol_FechaLectura_ORD = 9;
		public const int icol_FechaBorrado_ORD = 10;
		public const int icol_iTipoMensaje = 11;

		public static string vNomAplicacion = String.Empty;
		public static string vstFormatoSOLOFechaBD = String.Empty; //formato de solo la fecha en la base de datos sin hora
		public static string vstFormatoFechaHoraMinBD = String.Empty; //formato de la fecha en la base de datos con hh:nn
		public static string vstFormatoFechaHoraMinSegBD = String.Empty; //formato de la fecha en la base de datos con hh:nn:ss
		public static string VstTipoBD = String.Empty; //Tipo de base de datos con la que se trabaja

		public static DialogResult iRespuesta = (DialogResult) 0;

		internal static string FormatFecha(ref object Fecha)
		{
			Conexion.Obtener_conexion objConexion = new Conexion.Obtener_conexion();
            return objConexion.FormatearFecha(VstTipoBD, Fecha, vstFormatoSOLOFechaBD);
		}
		internal static string FormatFechaHM(object Fecha)
		{
			Conexion.Obtener_conexion objConexion = new Conexion.Obtener_conexion();
			return objConexion.FormatearFecha(VstTipoBD, Fecha, vstFormatoFechaHoraMinBD);
		}
		internal static string FormatFechaHMS(object Fecha)
		{
			Conexion.Obtener_conexion objConexion = new Conexion.Obtener_conexion();
			return objConexion.FormatearFecha(VstTipoBD, Fecha, vstFormatoFechaHoraMinSegBD);
		}

		internal static string ObtenerNombreAplicacion(SqlConnection rcConexion)
		{
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select valfanu1 from sconsglo where gconsglo='NOMAPLIC'", rcConexion);
			DataSet rrtitulo = new DataSet();
			tempAdapter.Fill(rrtitulo);
			vNomAplicacion = Convert.ToString(rrtitulo.Tables[0].Rows[0]["valfanu1"]);
			rrtitulo.Close();
			return vNomAplicacion.Trim();
		}

		internal static object Obtener_TipoBD_FormatoFechaBD(ref SqlConnection rc)
		{
			// JavierC   19/07/2000
			// Funcion para obtener el formato de fecha de la base de datos
			// y el tipo de base de datos con la que estamos trabajando
			Conexion.Obtener_conexion objConexion = new Conexion.Obtener_conexion();
            vstFormatoSOLOFechaBD = objConexion.FormatoSoloFechaBD(rc);
            vstFormatoFechaHoraMinBD = objConexion.FormatoFechaHoraMinBD(rc);
            vstFormatoFechaHoraMinSegBD = objConexion.FormatoFechaHoraMinSegBD(rc);

			//OSCAR 09/10/2002
			VstTipoBD = objConexion.ObtenerTipoBD();
			//Ahora el tipo de BD depende de una variable global y no del registro
			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO ='GTIPOBD'", rc);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{	
                if (rr.Tables[0].Rows[0][0] != null)
				{
					VstTipoBD = Convert.ToString(rr.Tables[0].Rows[0][0]).Trim().ToUpper();
				}
			}
			rr.Close();

			return null;
		}

		internal static int SustituirComilla()
		{
			return 180;
		}


		internal static string fCargaDetalleMensaje(string idMensaje)
		{
			string stCuerpoMensaje = String.Empty;
			string stDetalleMensaje = "";
			int iPos = (idMensaje.IndexOf('/') + 1);
			int Amensaje = Convert.ToInt32(Double.Parse(idMensaje.Substring(0, Math.Min(iPos - 1, idMensaje.Length))));
			int Nmensaje = Convert.ToInt32(Double.Parse(idMensaje.Substring(iPos)));
			string sql = " SELECT STEXMENP.fechamen,  STEXMENP.asuntome, STEXMENP.textomen, STEXMENP.amensaje, STEXMENP.nmensaje" + 
			             " From STEXMENP" + 
			             " WHERE " + 
			             " STEXMENP.amensaje = " + Amensaje.ToString() + " and STEXMENP.nmensaje = " + Nmensaje.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
			DataSet rrDatos = new DataSet();
			tempAdapter.Fill(rrDatos);
			if (rrDatos.Tables[0].Rows.Count != 0)
			{
				stCuerpoMensaje = (Convert.ToString(rrDatos.Tables[0].Rows[0]["textomen"]) + "").Trim();
				stDetalleMensaje = stDetalleMensaje + "=========================================================================";
				stDetalleMensaje = stDetalleMensaje + Environment.NewLine;
				stDetalleMensaje = stDetalleMensaje + "ASUNTO: " + (Convert.ToString(rrDatos.Tables[0].Rows[0]["asuntome"]) + "").Trim();
				stDetalleMensaje = stDetalleMensaje + Environment.NewLine;
				stDetalleMensaje = stDetalleMensaje + "ENVIADO POR : " + fObtenerEmisor(Convert.ToInt32(rrDatos.Tables[0].Rows[0]["amensaje"]), Convert.ToInt32(rrDatos.Tables[0].Rows[0]["nmensaje"]));
				stDetalleMensaje = stDetalleMensaje + Environment.NewLine;
				stDetalleMensaje = stDetalleMensaje + "PARA: " + fObtenerDestinatarios(Convert.ToInt32(rrDatos.Tables[0].Rows[0]["amensaje"]), Convert.ToInt32(rrDatos.Tables[0].Rows[0]["nmensaje"]));
				stDetalleMensaje = stDetalleMensaje + Environment.NewLine;
				stDetalleMensaje = stDetalleMensaje + "EL: " + Convert.ToDateTime(rrDatos.Tables[0].Rows[0]["fechamen"]).ToString("dd/MM/yyyy HH:mm");
				stDetalleMensaje = stDetalleMensaje + Environment.NewLine;
				stDetalleMensaje = stDetalleMensaje + "=========================================================================";
				stDetalleMensaje = stDetalleMensaje + Environment.NewLine;
				stDetalleMensaje = stDetalleMensaje + stCuerpoMensaje;
			}
            
            rrDatos.Close();
			return stDetalleMensaje;
		}

		internal static string fObtenerDestinatarios(int Amensaje, int Nmensaje)
		{
			StringBuilder stDestinatarios = new StringBuilder();
			string sql = "" + 
			             "select SUSUARIO.dusuario, DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers" + 
			             " FROM SMENPUSU " + 
			             " INNER JOIN SUSUARIO on SMENPUSU.gusuario = SUSUARIO.gusuario" + 
			             " INNER JOIN DPERSONA on SUSUARIO.gpersona = DPERSONA.gpersona" + 
			             " WHERE SMENPUSU.amensaje = " + Amensaje.ToString() + 
			             "   and SMENPUSU.nmensaje = " + Nmensaje.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
			DataSet rrDatos = new DataSet();
			tempAdapter.Fill(rrDatos);
			if (rrDatos.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in rrDatos.Tables[0].Rows)
				{
					stDestinatarios.Append(
					                       (Convert.ToString(iteration_row["dusuario"]) + "").Trim().ToUpper() + 
					                       " (" + Convert.ToString(iteration_row["dap1pers"]).Trim() + " " + Convert.ToString(iteration_row["dap2pers"]).Trim() + ", " + Convert.ToString(iteration_row["dnompers"]).Trim() + "); ");
				}
				stDestinatarios = new StringBuilder(stDestinatarios.ToString().Substring(0, Math.Min(stDestinatarios.ToString().Length - 2, stDestinatarios.ToString().Length)));
			}
			rrDatos.Close();
			return stDestinatarios.ToString();
		}

		internal static string fObtenerEmisor(int Amensaje, int Nmensaje)
		{
			string stEmisor = "";
			string sql = "" + 
			             " SELECT SUSUARIO.dusuario, DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers " + 
			             " From STEXMENP" + 
			             " INNER JOIN SUSUARIO on STEXMENP.gusuario = SUSUARIO.gusuario" + 
			             " INNER JOIN DPERSONA on SUSUARIO.gpersona = DPERSONA.gpersona" + 
			             " WHERE STEXMENP.amensaje = " + Amensaje.ToString() + " and STEXMENP.nmensaje = " + Nmensaje.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
			DataSet rrDatos = new DataSet();
			tempAdapter.Fill(rrDatos);
			if (rrDatos.Tables[0].Rows.Count != 0)
			{
				stEmisor = stEmisor + 
				           (Convert.ToString(rrDatos.Tables[0].Rows[0]["dusuario"]) + "").Trim().ToUpper() + 
				           " (" + Convert.ToString(rrDatos.Tables[0].Rows[0]["dap1pers"]).Trim() + " " + Convert.ToString(rrDatos.Tables[0].Rows[0]["dap2pers"]).Trim() + ", " + Convert.ToString(rrDatos.Tables[0].Rows[0]["dnompers"]).Trim() + ")";
			}
			rrDatos.Close();
			return stEmisor;
		}

		internal static bool fMensajesNoLeidos(ref string Cuantos)
		{
			bool result = false;
			string sql = "SELECT count(*) " + 
			             " From STEXMENP" + 
			             " INNER JOIN SMENPUSU on STEXMENP.amensaje = SMENPUSU.amensaje and STEXMENP.nmensaje = SMENPUSU.nmensaje" + 
			             " WHERE " + 
			             " SMENPUSU.gusuario = '" + gUsuario + "' and " + 
			             " SMENPUSU.fborrado Is Null and" + 
			             " SMENPUSU.flectume is null";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
			DataSet rrDatos = new DataSet();
			tempAdapter.Fill(rrDatos);
			
			if (Convert.ToDouble(rrDatos.Tables[0].Rows[0][0]) > 0)
			{
				result = true;
				Cuantos = Convert.ToString(rrDatos.Tables[0].Rows[0][0]);
			}
			rrDatos.Close();
			return result;
		}

		internal static string ObtenerValor_CTEGLOBAL(SqlConnection ParaConexion, string ParaCTE, string ParaValor)
		{
			string result = String.Empty;
			result = "";
			string sql = "SELECT " + ParaValor + " FROM SCONSGLO WHERE GCONSGLO='" + ParaCTE + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				result = (Convert.ToString(rr.Tables[0].Rows[0][0]) + "").Trim().ToUpper();
			}
			rr.Close();
			return result;
		}
	}
}