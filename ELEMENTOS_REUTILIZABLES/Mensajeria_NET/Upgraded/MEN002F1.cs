using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Telerik.WinControls;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Mensajeria
{
	public partial class DETALLE_MENSAJE
        : Telerik.WinControls.UI.RadForm
	{


		bool bMensajeEnviado = false;
		public DETALLE_MENSAJE()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		public void proResponderUsuario(string idMensaje)
		{
			int iPos = (idMensaje.IndexOf('/') + 1);
			int Amensaje = Convert.ToInt32(Double.Parse(idMensaje.Substring(0, Math.Min(iPos - 1, idMensaje.Length))));
			int Nmensaje = Convert.ToInt32(Double.Parse(idMensaje.Substring(iPos)));
			string sql = "" + 
			             " SELECT SUSUARIO.dusuario, SUSUARIO.gusuario, DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers " + 
			             " From STEXMENP" + 
			             " INNER JOIN SUSUARIO on STEXMENP.gusuario = SUSUARIO.gusuario and SUSUARIO.fborrado is null" + 
			             " INNER JOIN DPERSONA on SUSUARIO.gpersona = DPERSONA.gpersona and DPERSONA.fborrado is null" + 
			             " WHERE " + 
			             " STEXMENP.amensaje = " + Amensaje.ToString() + " and STEXMENP.nmensaje = " + Nmensaje.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BMensajeria.rcConexion);
			DataSet rrDatos = new DataSet();
			tempAdapter.Fill(rrDatos);
			if (rrDatos.Tables[0].Rows.Count != 0)
			{
				txtDestinatarios.Text = txtDestinatarios.Text + 
				                        (Convert.ToString(rrDatos.Tables[0].Rows[0]["dusuario"]) + "").Trim().ToUpper() + 
				                        " (" + Convert.ToString(rrDatos.Tables[0].Rows[0]["dap1pers"]).Trim() + " " + Convert.ToString(rrDatos.Tables[0].Rows[0]["dap2pers"]).Trim() + ", " + Convert.ToString(rrDatos.Tables[0].Rows[0]["dnompers"]).Trim() + ")";
				
                object tempRefParam = (Convert.ToString(rrDatos.Tables[0].Rows[0]["gusuario"]) + "").Trim().ToUpper();
                object tempRefParam2 = Type.Missing;
                SEL_USUARIO.DefInstance.lsbSeleccionUsuarios.AddItem(tempRefParam, tempRefParam2); // columna 0
                SEL_USUARIO.DefInstance.lsbSeleccionUsuarios.set_Column(1, 0, (Convert.ToString(rrDatos.Tables[0].Rows[0]["dusuario"]) + "").Trim().ToUpper()); // columna 1
                SEL_USUARIO.DefInstance.lsbSeleccionUsuarios.set_Column(2, 0, "( " + (Convert.ToString(rrDatos.Tables[0].Rows[0]["dap1pers"]) + "").Trim().ToUpper() + " " + (Convert.ToString(rrDatos.Tables[0].Rows[0]["dap2pers"]) + "").Trim().ToUpper() + ", " + (Convert.ToString(rrDatos.Tables[0].Rows[0]["dnompers"]) + "").Trim().ToUpper() + " )"); // columna 2
			}
			rrDatos.Close();
		}

		public void proResponderTodos(string idMensaje)
		{
			int ilistau = 0;
			int iPos = (idMensaje.IndexOf('/') + 1);
			int Amensaje = Convert.ToInt32(Double.Parse(idMensaje.Substring(0, Math.Min(iPos - 1, idMensaje.Length))));
			int Nmensaje = Convert.ToInt32(Double.Parse(idMensaje.Substring(iPos)));
			string sql = "" + 
			             " SELECT SUSUARIO.dusuario, SUSUARIO.gusuario, DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers " + 
			             " From STEXMENP" + 
			             " INNER JOIN SUSUARIO on STEXMENP.gusuario = SUSUARIO.gusuario and SUSUARIO.fborrado is null" + 
			             " INNER JOIN DPERSONA on SUSUARIO.gpersona = DPERSONA.gpersona and DPERSONA.fborrado is null" + 
			             " WHERE " + 
			             " STEXMENP.amensaje = " + Amensaje.ToString() + " and STEXMENP.nmensaje = " + Nmensaje.ToString();
			sql = sql + 
			      " UNION " + 
			      " select SUSUARIO.dusuario, SUSUARIO.gusuario, DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers" + 
			      " FROM SMENPUSU " + 
			      " INNER JOIN SUSUARIO on SMENPUSU.gusuario = SUSUARIO.gusuario" + 
			      " INNER JOIN DPERSONA on SUSUARIO.gpersona = DPERSONA.gpersona" + 
			      " WHERE SMENPUSU.amensaje = " + Amensaje.ToString() + 
			      "   and SMENPUSU.nmensaje = " + Nmensaje.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BMensajeria.rcConexion);
			DataSet rrDatos = new DataSet();
			tempAdapter.Fill(rrDatos);
			if (rrDatos.Tables[0].Rows.Count != 0)
			{
				ilistau = 0;
				foreach (DataRow iteration_row in rrDatos.Tables[0].Rows)
				{
					txtDestinatarios.Text = txtDestinatarios.Text + 
					                        (Convert.ToString(iteration_row["dusuario"]) + "").Trim().ToUpper() + 
					                        " (" + Convert.ToString(iteration_row["dap1pers"]).Trim() + " " + Convert.ToString(iteration_row["dap2pers"]).Trim() + ", " + Convert.ToString(iteration_row["dnompers"]).Trim() + "); ";
					object tempRefParam = (Convert.ToString(iteration_row["gusuario"]) + "").Trim().ToUpper();
					object tempRefParam2 = Type.Missing;

                    SEL_USUARIO.DefInstance.lsbSeleccionUsuarios.AddItem(tempRefParam, tempRefParam2); // columna 0
                    SEL_USUARIO.DefInstance.lsbSeleccionUsuarios.set_Column(1, ilistau, (Convert.ToString(iteration_row["dusuario"]) + "").Trim().ToUpper()); // columna 1
                    SEL_USUARIO.DefInstance.lsbSeleccionUsuarios.set_Column(2, ilistau, "( " + (Convert.ToString(iteration_row["dap1pers"]) + "").Trim().ToUpper() + " " + (Convert.ToString(iteration_row["dap2pers"]) + "").Trim().ToUpper() + ", " + (Convert.ToString(iteration_row["dnompers"]) + "").Trim().ToUpper() + " )"); // columna 2
                    ilistau++;
				}
				txtDestinatarios.Text = txtDestinatarios.Text.Substring(0, Math.Min(Strings.Len(txtDestinatarios.Text) - 2, txtDestinatarios.Text.Length));
			}
			
			rrDatos.Close();
		}

		private void cmdEnviar_Click(Object eventSender, EventArgs eventArgs)
		{
            SqlTransaction transaction = BMensajeria.rcConexion.BeginTrans();

			try
			{
				string sql = String.Empty;
				DataSet rrTemp = null;
				int Nmensaje = 0;
				int Amensaje = 0;
				string stFechaSistema = String.Empty;
				stFechaSistema = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

				if (txtDestinatarios.Text.Trim() == "")
				{
                    RadMessageBox.Show("Debe especificar destinatario/s del mensaje", BMensajeria.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
					cmdSelUsuario.Focus();
					return;
				}
				if (txtAsunto.Text.Trim() == "")
				{
					RadMessageBox.Show("Debe especificar asunto del mensaje", BMensajeria.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
					txtAsunto.Focus();
					return;
				}
				if (txtMensaje.Text.Trim() == "")
				{
					RadMessageBox.Show("Debe especificar cuerpo del mensaje", BMensajeria.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
					txtMensaje.Focus();
					return;
				}
				Amensaje = Convert.ToInt32(Double.Parse(DateTime.Today.ToString("yyyy")));

				sql = "select max(nmensaje) as maximo from STEXMENP where amensaje=" + Amensaje.ToString();
                var command = new SqlCommand(sql, BMensajeria.rcConexion, transaction);
                SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
				rrTemp = new DataSet();
				tempAdapter.Fill(rrTemp);
				
				if (Convert.IsDBNull(rrTemp.Tables[0].Rows[0]["maximo"]))
				{
					Nmensaje = 0;
				}
				else
				{					
					Nmensaje = Convert.ToInt32(rrTemp.Tables[0].Rows[0]["maximo"]);
				}
				Nmensaje++;
				rrTemp.Close();

                if (SEL_USUARIO.DefInstance.lsbSeleccionUsuarios.ListCount > 0)
                {
                     object tempRefParam = stFechaSistema;
                    sql = "INSERT INTO STEXMENP (amensaje , nMensaje, asuntome, textomen, fechamen, gUsuario) " + 
                          " VALUES (" + Amensaje.ToString() + ", " + Nmensaje.ToString() + ", '" + txtAsunto.Text.Trim() + "', '" + txtMensaje.Text.Trim() + "', " + 
                          BMensajeria.FormatFechaHMS(tempRefParam) + ", '" + BMensajeria.gUsuario + "')";
                    stFechaSistema = Convert.ToString(tempRefParam);
                    SqlCommand tempCommand = new SqlCommand(sql, BMensajeria.rcConexion, transaction);
                    tempCommand.ExecuteNonQuery();

                    for (int i = 0; i <= SEL_USUARIO.DefInstance.lsbSeleccionUsuarios.ListCount - 1; i++)
                    {
                        object tempRefParam2 = i;
                        object tempRefParam3 = 0;
                        object tempRefParam4 = stFechaSistema;
                        sql = "INSERT INTO SMENPUSU (amensaje, nMensaje, gUsuario, fenviome)" + 
                              " VALUES (" + Amensaje.ToString() + ", " + Nmensaje.ToString() + ", '" + Convert.ToString(SEL_USUARIO.DefInstance.lsbSeleccionUsuarios.get_List(tempRefParam2, tempRefParam3)) + "'," + BMensajeria.FormatFechaHMS(tempRefParam4) + ")";
                        stFechaSistema = Convert.ToString(tempRefParam4);
                        i = Convert.ToInt32(tempRefParam2);
                        SqlCommand tempCommand_2 = new SqlCommand(sql, BMensajeria.rcConexion, transaction);
                        tempCommand_2.ExecuteNonQuery();
                    }
                }
                BMensajeria.rcConexion.CommitTrans(transaction);
				bMensajeEnviado = true;
				MENU_MENSAJES.DefInstance.cbRefrescar_Click(MENU_MENSAJES.DefInstance.cbRefrescar, new EventArgs());
				this.Close();
			}
			catch (System.Exception excep)
			{
				bMensajeEnviado = false;
                BMensajeria.rcConexion.RollbackTrans(transaction);
                RadMessageBox.Show("Error al enviar el mensaje. " + Environment.NewLine + excep.Message, BMensajeria.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
			}
            finally
            {
                transaction.Dispose();
            }
		}

		private void cmdSelUsuario_Click(Object eventSender, EventArgs eventArgs)
		{
			SEL_USUARIO.DefInstance.ShowDialog();
		}

		private void DETALLE_MENSAJE_Load(Object eventSender, EventArgs eventArgs)
		{
			//UPGRADE_ISSUE: (2064) Form property DETALLE_MENSAJE.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
         	//this.setHelpContextID(225002);			
			bMensajeEnviado = false;
			proRecolocaControles();
		}

		private void DETALLE_MENSAJE_FormClosing(Object eventSender, FormClosingEventArgs eventArgs)
		{
			int Cancel = (eventArgs.Cancel) ? 1 : 0;
			int UnloadMode = (int) eventArgs.CloseReason;
			DialogResult iRespuesta = (DialogResult) 0;
			if (!bMensajeEnviado)
			{
				if (txtDestinatarios.Text.Trim() != "" || txtAsunto.Text.Trim() != "" || txtMensaje.Text.Trim() != "")
				{
					iRespuesta = RadMessageBox.Show("�Desea salir sin enviar el mensaje?", BMensajeria.vNomAplicacion, MessageBoxButtons.YesNo, RadMessageIcon.Question);
					if (iRespuesta == System.Windows.Forms.DialogResult.No)
					{
						Cancel = -1;
					}
				}
			}
			eventArgs.Cancel = Cancel != 0;
		}

		private void DETALLE_MENSAJE_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
            SEL_USUARIO.DefInstance.Dispose();
		}

		private void proRecolocaControles()
		{
			this.Width = (int) (Screen.PrimaryScreen.Bounds.Width / 1.5d);
			this.Height = (int) (Screen.PrimaryScreen.Bounds.Height / 1.5d);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			cmdEnviar.Left = (int) (this.Width - cmdEnviar.Width - 13);
			txtDestinatarios.Width = (int) (this.Width - 147);
			txtAsunto.Width = (int) txtDestinatarios.Width;
			txtMensaje.Width = (int) (this.Width - 13);
			txtMensaje.Height = (int) (this.Height - 147);
		}

        private void txtAsunto_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = BMensajeria.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void txtAsunto_Leave(Object eventSender, EventArgs eventArgs)
		{
			txtAsunto.Text = txtAsunto.Text.ToUpper();
		}

		private void txtMensaje_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = BMensajeria.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
	}
}