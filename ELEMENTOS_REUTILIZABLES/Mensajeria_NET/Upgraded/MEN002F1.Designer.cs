using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Mensajeria
{
    partial class DETALLE_MENSAJE
    {

        #region "Upgrade Support "
        private static DETALLE_MENSAJE m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static DETALLE_MENSAJE DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new DETALLE_MENSAJE();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "cmdEnviar", "cmdSelUsuario", "txtDestinatarios", "txtAsunto", "txtMensaje", "Label1", "commandButtonHelper1" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;
        public Telerik.WinControls.UI.RadButton cmdEnviar;
        public Telerik.WinControls.UI.RadButton cmdSelUsuario;
        public Telerik.WinControls.UI.RadTextBoxControl txtDestinatarios;
        public Telerik.WinControls.UI.RadTextBoxControl txtAsunto;
        public Telerik.WinControls.UI.RadTextBoxControl txtMensaje;
        public Telerik.WinControls.UI.RadLabel Label1;
        private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DETALLE_MENSAJE));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.cmdEnviar = new Telerik.WinControls.UI.RadButton();
            this.cmdSelUsuario = new Telerik.WinControls.UI.RadButton();
            this.txtDestinatarios = new Telerik.WinControls.UI.RadTextBoxControl();
            this.txtAsunto = new Telerik.WinControls.UI.RadTextBoxControl();
            this.txtMensaje = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.cmdEnviar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDestinatarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsunto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMensaje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdEnviar
            // 
            this.cmdEnviar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdEnviar.Image = ((System.Drawing.Image)(resources.GetObject("cmdEnviar.Image")));
            this.cmdEnviar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdEnviar.Location = new System.Drawing.Point(616, 12);
            this.cmdEnviar.Name = "cmdEnviar";
            this.cmdEnviar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdEnviar.Size = new System.Drawing.Size(53, 49);
            this.cmdEnviar.TabIndex = 2;
            this.cmdEnviar.Text = "Enviar...";
            this.cmdEnviar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdEnviar.Click += new System.EventHandler(this.cmdEnviar_Click);
            // 
            // cmdSelUsuario
            // 
            this.cmdSelUsuario.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSelUsuario.Image = ((System.Drawing.Image)(resources.GetObject("cmdSelUsuario.Image")));
            this.cmdSelUsuario.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdSelUsuario.Location = new System.Drawing.Point(8, 16);
            this.cmdSelUsuario.Name = "cmdSelUsuario";
            this.cmdSelUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSelUsuario.Size = new System.Drawing.Size(53, 49);
            this.cmdSelUsuario.TabIndex = 0;
            this.cmdSelUsuario.Text = "Para...";
            this.cmdSelUsuario.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdSelUsuario.Click += new System.EventHandler(this.cmdSelUsuario_Click);
            // 
            // txtDestinatarios
            // 
            this.txtDestinatarios.AcceptsReturn = true;
            this.txtDestinatarios.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDestinatarios.IsReadOnly = true;
            this.txtDestinatarios.Location = new System.Drawing.Point(72, 12);
            this.txtDestinatarios.MaxLength = 0;
            this.txtDestinatarios.Multiline = true;
            this.txtDestinatarios.Name = "txtDestinatarios";
            this.txtDestinatarios.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDestinatarios.Size = new System.Drawing.Size(537, 57);
            this.txtDestinatarios.TabIndex = 1;
            this.txtDestinatarios.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            // 
            // txtAsunto
            // 
            this.txtAsunto.AcceptsReturn = true;
            this.txtAsunto.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtAsunto.Location = new System.Drawing.Point(72, 76);
            this.txtAsunto.MaxLength = 255;
            this.txtAsunto.Name = "txtAsunto";
            this.txtAsunto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAsunto.Size = new System.Drawing.Size(597, 25);
            this.txtAsunto.TabIndex = 3;
            this.txtAsunto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAsunto_KeyPress);
            this.txtAsunto.Leave += new System.EventHandler(this.txtAsunto_Leave);
            // 
            // txtMensaje
            // 
            this.txtMensaje.AcceptsReturn = true;
            this.txtMensaje.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtMensaje.Location = new System.Drawing.Point(4, 104);
            this.txtMensaje.MaxLength = 0;
            this.txtMensaje.Multiline = true;
            this.txtMensaje.Name = "txtMensaje";
            this.txtMensaje.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMensaje.Size = new System.Drawing.Size(665, 357);
            this.txtMensaje.TabIndex = 4;
            this.txtMensaje.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            this.txtMensaje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMensaje_KeyPress);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(8, 80);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(44, 18);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "Asunto:";
            // 
            // DETALLE_MENSAJE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 465);
            this.Controls.Add(this.cmdEnviar);
            this.Controls.Add(this.cmdSelUsuario);
            this.Controls.Add(this.txtDestinatarios);
            this.Controls.Add(this.txtAsunto);
            this.Controls.Add(this.txtMensaje);
            this.Controls.Add(this.Label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DETALLE_MENSAJE";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Nuevo Mensaje - MEN002F1";
            this.Closed += new System.EventHandler(this.DETALLE_MENSAJE_Closed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DETALLE_MENSAJE_FormClosing);
            this.Load += new System.EventHandler(this.DETALLE_MENSAJE_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmdEnviar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDestinatarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsunto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMensaje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
    }
}