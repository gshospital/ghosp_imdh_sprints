using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BuscaSelecPrestacionesDLL
{
	partial class HCE310F3
	{

		#region "Upgrade Support "
		private static HCE310F3 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static HCE310F3 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new HCE310F3();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbbServicio", "CB_cancelar", "CB_Aceptar", "sprPetitorio", "_Option1_5", "_Option1_4", "FRM_Petitotio", "SprProtocolo", "cmdA�adirPruebasPlan", "_Option1_1", "_Option1_0", "FRM_protocolo", "SprPlantillasLab", "FRM_PlantillasLab", "sprPrestacionesLab", "_Option1_3", "_Option1_2", "cmdA�adirPruebas", "FRM_PrestacionLab", "Label1", "Option1", "commandButtonHelper1", "sprPrestacionesLab_Sheet1", "SprPlantillasLab_Sheet1", "SprProtocolo_Sheet1", "sprPetitorio_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public UpgradeHelpers.MSForms.MSCombobox cbbServicio;
		public Telerik.WinControls.UI.RadButton CB_cancelar;
		public Telerik.WinControls.UI.RadButton CB_Aceptar;
		public UpgradeHelpers.Spread.FpSpread sprPetitorio;
		private Telerik.WinControls.UI.RadRadioButton _Option1_5;
		private Telerik.WinControls.UI.RadRadioButton _Option1_4;
		public Telerik.WinControls.UI.RadGroupBox FRM_Petitotio;
		public UpgradeHelpers.Spread.FpSpread SprProtocolo;
		public Telerik.WinControls.UI.RadButton cmdA�adirPruebasPlan;
		private Telerik.WinControls.UI.RadRadioButton _Option1_1;
		private Telerik.WinControls.UI.RadRadioButton _Option1_0;
		public Telerik.WinControls.UI.RadGroupBox FRM_protocolo;
		public UpgradeHelpers.Spread.FpSpread SprPlantillasLab;
		public Telerik.WinControls.UI.RadGroupBox FRM_PlantillasLab;
		public UpgradeHelpers.Spread.FpSpread sprPrestacionesLab;
		private Telerik.WinControls.UI.RadRadioButton _Option1_3;
		private Telerik.WinControls.UI.RadRadioButton _Option1_2;
		public Telerik.WinControls.UI.RadButton cmdA�adirPruebas;
		public Telerik.WinControls.UI.RadGroupBox FRM_PrestacionLab;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadRadioButton[] Option1 = new Telerik.WinControls.UI.RadRadioButton[6];
		private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
		//private FarPoint.Win.Spread.SheetView sprPrestacionesLab_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprPlantillasLab_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprProtocolo_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView sprPetitorio_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HCE310F3));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.cbbServicio = new UpgradeHelpers.MSForms.MSCombobox();
            this.CB_cancelar = new Telerik.WinControls.UI.RadButton();
			this.CB_Aceptar = new Telerik.WinControls.UI.RadButton();
			this.FRM_Petitotio = new Telerik.WinControls.UI.RadGroupBox();
			this.sprPetitorio = new UpgradeHelpers.Spread.FpSpread();
			this._Option1_5 = new Telerik.WinControls.UI.RadRadioButton();
			this._Option1_4 = new Telerik.WinControls.UI.RadRadioButton();
			this.FRM_protocolo = new Telerik.WinControls.UI.RadGroupBox();
			this.SprProtocolo = new UpgradeHelpers.Spread.FpSpread();
			this.cmdA�adirPruebasPlan = new Telerik.WinControls.UI.RadButton();
			this._Option1_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._Option1_0 = new Telerik.WinControls.UI.RadRadioButton();
			this.FRM_PlantillasLab = new Telerik.WinControls.UI.RadGroupBox();
			this.SprPlantillasLab = new UpgradeHelpers.Spread.FpSpread();
			this.FRM_PrestacionLab = new Telerik.WinControls.UI.RadGroupBox();
			this.sprPrestacionesLab = new UpgradeHelpers.Spread.FpSpread();
			this._Option1_3 = new Telerik.WinControls.UI.RadRadioButton();
			this._Option1_2 = new Telerik.WinControls.UI.RadRadioButton();
			this.cmdA�adirPruebas = new Telerik.WinControls.UI.RadButton();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.FRM_Petitotio.SuspendLayout();
			this.FRM_protocolo.SuspendLayout();
			this.FRM_PlantillasLab.SuspendLayout();
			this.FRM_PrestacionLab.SuspendLayout();
			this.SuspendLayout();
			this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
			// 
			// cbbServicio
			// 			
			this.cbbServicio.CausesValidation = true;
			this.cbbServicio.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbbServicio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
			this.cbbServicio.Enabled = true;			
			this.cbbServicio.Location = new System.Drawing.Point(4, 20);
			this.cbbServicio.Name = "cbbServicio";
			this.cbbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbbServicio.Size = new System.Drawing.Size(365, 21);			
			this.cbbServicio.TabIndex = 18;
			this.cbbServicio.TabStop = true;
			this.cbbServicio.Visible = true;
			this.cbbServicio.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbServicio_SelectedIndexChanged);
			// 
			// CB_cancelar
			// 			
			this.CB_cancelar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.CB_cancelar.Location = new System.Drawing.Point(669, 484);
			this.CB_cancelar.Name = "CB_cancelar";
			this.CB_cancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CB_cancelar.Size = new System.Drawing.Size(81, 29);
			this.CB_cancelar.TabIndex = 8;
			this.CB_cancelar.Text = "&Cancelar";			
			this.CB_cancelar.Click += new System.EventHandler(this.CB_cancelar_Click);
			// 
			// CB_Aceptar
			// 			
			this.CB_Aceptar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.CB_Aceptar.Location = new System.Drawing.Point(578, 484);
			this.CB_Aceptar.Name = "CB_Aceptar";
			this.CB_Aceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CB_Aceptar.Size = new System.Drawing.Size(81, 29);
			this.CB_Aceptar.TabIndex = 7;
			this.CB_Aceptar.Text = "&Aceptar";			
			this.CB_Aceptar.Click += new System.EventHandler(this.CB_Aceptar_Click);
			// 
			// FRM_Petitotio
			//		
			this.FRM_Petitotio.Controls.Add(this.sprPetitorio);
			this.FRM_Petitotio.Controls.Add(this._Option1_5);
			this.FRM_Petitotio.Controls.Add(this._Option1_4);
			this.FRM_Petitotio.Enabled = true;
			this.FRM_Petitotio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);			
			this.FRM_Petitotio.Location = new System.Drawing.Point(374, 262);
			this.FRM_Petitotio.Name = "FRM_Petitotio";
			this.FRM_Petitotio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FRM_Petitotio.Size = new System.Drawing.Size(378, 217);
			this.FRM_Petitotio.TabIndex = 6;
			this.FRM_Petitotio.Text = "Petitorio";
			this.FRM_Petitotio.Visible = true;
			// 
			// sprPetitorio
			// 
			this.sprPetitorio.Location = new System.Drawing.Point(6, 16);
			this.sprPetitorio.Name = "sprPetitorio";
			this.sprPetitorio.Size = new System.Drawing.Size(364, 165);
			this.sprPetitorio.TabIndex = 16;
			// 
			// _Option1_5
			//			
			this._Option1_5.CausesValidation = true;			
			this._Option1_5.IsChecked = false;
			this._Option1_5.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_5.Enabled = true;
			this._Option1_5.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option1_5.Location = new System.Drawing.Point(78, 190);
			this._Option1_5.Name = "_Option1_5";
			this._Option1_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_5.Size = new System.Drawing.Size(63, 15);
			this._Option1_5.TabIndex = 15;
			this._Option1_5.TabStop = true;
			this._Option1_5.Text = "Ninguno";
			this._Option1_5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_5.Visible = true;
			this._Option1_5.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Option1_CheckedChanged);
			// 
			// _Option1_4
			// 			
			this._Option1_4.CausesValidation = true;			
			this._Option1_4.IsChecked = false;
			this._Option1_4.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_4.Enabled = true;			
			this._Option1_4.Location = new System.Drawing.Point(10, 190);
			this._Option1_4.Name = "_Option1_4";
			this._Option1_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_4.Size = new System.Drawing.Size(63, 15);
			this._Option1_4.TabIndex = 14;
			this._Option1_4.TabStop = true;
			this._Option1_4.Text = "Todos";
			this._Option1_4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_4.Visible = true;
			this._Option1_4.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Option1_CheckedChanged);
			// 
			// FRM_protocolo
			// 			
			this.FRM_protocolo.Controls.Add(this.SprProtocolo);
			this.FRM_protocolo.Controls.Add(this.cmdA�adirPruebasPlan);
			this.FRM_protocolo.Controls.Add(this._Option1_1);
			this.FRM_protocolo.Controls.Add(this._Option1_0);
			this.FRM_protocolo.Enabled = true;
			this.FRM_protocolo.ForeColor = System.Drawing.SystemColors.ControlText;
			this.FRM_protocolo.Location = new System.Drawing.Point(6, 264);
			this.FRM_protocolo.Name = "FRM_protocolo";
			this.FRM_protocolo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FRM_protocolo.Size = new System.Drawing.Size(364, 247);
			this.FRM_protocolo.TabIndex = 3;
			this.FRM_protocolo.Text = "Prestaciones asociadas a la plantilla";
			this.FRM_protocolo.Visible = true;
			// 
			// SprProtocolo
			// 
			this.SprProtocolo.Location = new System.Drawing.Point(6, 18);
			this.SprProtocolo.Name = "SprProtocolo";
			this.SprProtocolo.Size = new System.Drawing.Size(352, 195);
			this.SprProtocolo.TabIndex = 17;
			// 
			// cmdA�adirPruebasPlan
			//			
			this.cmdA�adirPruebasPlan.Cursor = System.Windows.Forms.Cursors.Default;
			this.cmdA�adirPruebasPlan.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdA�adirPruebasPlan.Image = (System.Drawing.Image) resources.GetObject("cmdA�adirPruebasPlan.Image");
			this.cmdA�adirPruebasPlan.Location = new System.Drawing.Point(324, 218);
			this.cmdA�adirPruebasPlan.Name = "cmdA�adirPruebasPlan";
			this.cmdA�adirPruebasPlan.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdA�adirPruebasPlan.Size = new System.Drawing.Size(33, 25);
			this.cmdA�adirPruebasPlan.TabIndex = 9;
			this.cmdA�adirPruebasPlan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;			
			this.cmdA�adirPruebasPlan.Click += new System.EventHandler(this.cmdA�adirPruebasPlan_Click);
			// 
			// _Option1_1
			// 			
			this._Option1_1.CausesValidation = true;			
			this._Option1_1.IsChecked = false;
			this._Option1_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_1.Enabled = true;
			this._Option1_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option1_1.Location = new System.Drawing.Point(76, 222);
			this._Option1_1.Name = "_Option1_1";
			this._Option1_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_1.Size = new System.Drawing.Size(63, 15);
			this._Option1_1.TabIndex = 5;
			this._Option1_1.TabStop = true;
			this._Option1_1.Text = "Ninguno";			
			this._Option1_1.Visible = true;
			this._Option1_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Option1_CheckedChanged);
			// 
			// _Option1_0
			//			
			this._Option1_0.CausesValidation = true;			
			this._Option1_0.IsChecked = false;
			this._Option1_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_0.Enabled = true;
			this._Option1_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option1_0.Location = new System.Drawing.Point(8, 222);
			this._Option1_0.Name = "_Option1_0";
			this._Option1_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_0.Size = new System.Drawing.Size(63, 15);
			this._Option1_0.TabIndex = 4;
			this._Option1_0.TabStop = true;
			this._Option1_0.Text = "Todos";
			this._Option1_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_0.Visible = true;
			this._Option1_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Option1_CheckedChanged);
			// 
			// FRM_PlantillasLab
			// 			
			this.FRM_PlantillasLab.Controls.Add(this.SprPlantillasLab);
			this.FRM_PlantillasLab.Enabled = true;
			this.FRM_PlantillasLab.ForeColor = System.Drawing.SystemColors.ControlText;
			this.FRM_PlantillasLab.Location = new System.Drawing.Point(4, 44);
			this.FRM_PlantillasLab.Name = "FRM_PlantillasLab";
			this.FRM_PlantillasLab.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FRM_PlantillasLab.Size = new System.Drawing.Size(365, 216);
			this.FRM_PlantillasLab.TabIndex = 1;
			this.FRM_PlantillasLab.Text = "Plantillas";
			this.FRM_PlantillasLab.Visible = true;
			// 
			// SprPlantillasLab
			// 
			this.SprPlantillasLab.Location = new System.Drawing.Point(6, 14);
			this.SprPlantillasLab.Name = "SprPlantillasLab";
			this.SprPlantillasLab.Size = new System.Drawing.Size(353, 197);
			this.SprPlantillasLab.TabIndex = 2;
			this.SprPlantillasLab.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPlantillasLab_CellClick);
			this.SprPlantillasLab.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SprPlantillasLab_KeyDown);
			// 
			// FRM_PrestacionLab
			// 			
			this.FRM_PrestacionLab.Controls.Add(this.sprPrestacionesLab);
			this.FRM_PrestacionLab.Controls.Add(this._Option1_3);
			this.FRM_PrestacionLab.Controls.Add(this._Option1_2);
			this.FRM_PrestacionLab.Controls.Add(this.cmdA�adirPruebas);
			this.FRM_PrestacionLab.Enabled = true;
			this.FRM_PrestacionLab.ForeColor = System.Drawing.SystemColors.ControlText;
			this.FRM_PrestacionLab.Location = new System.Drawing.Point(374, 2);
			this.FRM_PrestacionLab.Name = "FRM_PrestacionLab";
			this.FRM_PrestacionLab.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FRM_PrestacionLab.Size = new System.Drawing.Size(379, 258);
			this.FRM_PrestacionLab.TabIndex = 0;
			this.FRM_PrestacionLab.Text = "Prestaciones de Laboratorio";
			this.FRM_PrestacionLab.Visible = true;
			// 
			// sprPrestacionesLab
			// 
			this.sprPrestacionesLab.Location = new System.Drawing.Point(6, 14);
			this.sprPrestacionesLab.Name = "sprPrestacionesLab";
			this.sprPrestacionesLab.Size = new System.Drawing.Size(368, 211);
			this.sprPrestacionesLab.TabIndex = 11;
			// 
			// _Option1_3
			// 			
			this._Option1_3.CausesValidation = true;			
			this._Option1_3.IsChecked = false;
			this._Option1_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_3.Enabled = true;
			this._Option1_3.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option1_3.Location = new System.Drawing.Point(80, 234);
			this._Option1_3.Name = "_Option1_3";
			this._Option1_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_3.Size = new System.Drawing.Size(63, 15);
			this._Option1_3.TabIndex = 13;
			this._Option1_3.TabStop = true;
			this._Option1_3.Text = "Ninguno";
			this._Option1_3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_3.Visible = true;
			this._Option1_3.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Option1_CheckedChanged);
			// 
			// _Option1_2
			// 			
			this._Option1_2.CausesValidation = true;			
			this._Option1_2.IsChecked = false;
			this._Option1_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_2.Enabled = true;
			this._Option1_2.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option1_2.Location = new System.Drawing.Point(12, 234);
			this._Option1_2.Name = "_Option1_2";
			this._Option1_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_2.Size = new System.Drawing.Size(63, 15);
			this._Option1_2.TabIndex = 12;
			this._Option1_2.TabStop = true;
			this._Option1_2.Text = "Todos";
			this._Option1_2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_2.Visible = true;
			this._Option1_2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Option1_CheckedChanged);
			// 
			// cmdA�adirPruebas
			// 			
			this.cmdA�adirPruebas.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cmdA�adirPruebas.Image = (System.Drawing.Image) resources.GetObject("cmdA�adirPruebas.Image");
			this.cmdA�adirPruebas.Location = new System.Drawing.Point(350, 226);
			this.cmdA�adirPruebas.Name = "cmdA�adirPruebas";
			this.cmdA�adirPruebas.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdA�adirPruebas.Size = new System.Drawing.Size(25, 29);
			this.cmdA�adirPruebas.TabIndex = 10;
			this.cmdA�adirPruebas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;			
			this.cmdA�adirPruebas.Click += new System.EventHandler(this.cmdA�adirPruebas_Click);
			// 
			// Label1
			//			
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(6, 0);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(134, 17);
			this.Label1.TabIndex = 19;
			this.Label1.Text = "Servicio de Laboratorio:";
			// 
			// HCE310F3
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;			
			this.ClientSize = new System.Drawing.Size(753, 515);
			this.Controls.Add(this.cbbServicio);
			this.Controls.Add(this.CB_cancelar);
			this.Controls.Add(this.CB_Aceptar);
			this.Controls.Add(this.FRM_Petitotio);
			this.Controls.Add(this.FRM_protocolo);
			this.Controls.Add(this.FRM_PlantillasLab);
			this.Controls.Add(this.FRM_PrestacionLab);
			this.Controls.Add(this.Label1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "HCE310F3";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Petitiorio de Laboratorio - HCE310F3";
			//commandButtonHelper1.SetStyle(this.cmdA�adirPruebasPlan, 1);
			//commandButtonHelper1.SetStyle(this.cmdA�adirPruebas, 1);
			this.ToolTipMain.SetToolTip(this.cmdA�adirPruebasPlan, "A�adir al Petitorio");
			this.ToolTipMain.SetToolTip(this.cmdA�adirPruebas, "A�adir al Petitorio");
			this.Closed += new System.EventHandler(this.HCE310F3_Closed);
			this.Load += new System.EventHandler(this.HCE310F3_Load);
			this.FRM_Petitotio.ResumeLayout(false);
			this.FRM_protocolo.ResumeLayout(false);
			this.FRM_PlantillasLab.ResumeLayout(false);
			this.FRM_PrestacionLab.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeOption1();
		}
		void InitializeOption1()
		{
			this.Option1 = new Telerik.WinControls.UI.RadRadioButton[6];
			this.Option1[5] = _Option1_5;
			this.Option1[4] = _Option1_4;
			this.Option1[1] = _Option1_1;
			this.Option1[0] = _Option1_0;
			this.Option1[3] = _Option1_3;
			this.Option1[2] = _Option1_2;
		}
		#endregion
	}
}