using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
namespace BuscaSelecPrestacionesDLL
{
	public class ClassHCE310F2
	{

		private dynamic RefFormulario = null;
		private ClassHCE310F2 RefClase = null;

		//oscar 18/02/2004
		public void Load(ClassHCE310F2 VClass, dynamic vForm, SqlConnection rc, string IndicadorTodas, object VarFECHA_optional, object Multiseleccion, string ElServicio, object stTipoSer, bool OcultarPruebas)
		{
			string VarFECHA = (VarFECHA_optional == Type.Missing) ? String.Empty : VarFECHA_optional as string;
			//------------

			Module1.bSeHaAceptadoPetitorio = false; //OSCAR C

			RefFormulario = vForm;
			RefClase = VClass;
			string stTipo = String.Empty;

			Module1.RefRc = rc;
			Serrores.AjustarRelojCliente(rc);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Module1.RefRc);
			if (VarFECHA_optional != Type.Missing)
			{
				Module1.StFecha = VarFECHA;
			}
			else
			{
				Module1.StFecha = "";
			}

			if (Multiseleccion != Type.Missing)
			{
				Module1.fMultiseleccion = Convert.ToBoolean(Multiseleccion);
			}

			if (stTipoSer != Type.Missing)
			{
				stTipo = stTipoSer.ToString();
			}

			//oscar 18/02/2004
			//Si no viene este parametro (false por defecto) debera actuar de forma normal,
			//o sea mostrando las opciones "todos/ninguno" y los checks de las pruebas de protocolos.
			Module1.fSeleccionPruebas = !OcultarPruebas;
			//-----------

			//David 28/02/2008
			proMostrarAnatomia();
			proMostrarBS();
			//-----------

			Serrores.ObtenerFicheroAyuda();
			Serrores.ObtenerNombreAplicacion(Module1.RefRc);
			Module1.ClassMensaje = new Mensajes.ClassMensajes();

			//IndicadorTodas podr� tomar los valores S o N e indica si
			//queremos todas las prestaciones o solamente las activas
			Module1.Todas = IndicadorTodas;
			Module1.CerrarConsulta = false;
			Module1.CodElServicio = "";
			if (ElServicio != "")
			{
				Module1.CodElServicio = ElServicio;
			}
			Module1.FrForm = new HCE310F2();
			Module1.FrForm.Referencias(RefClase, RefFormulario, stTipo);
			//FrForm.Visible = True
			Module1.FrForm.ShowDialog();


			//OSCAR C
			//De momento, la funcionalidad del petitorio solo funciona desde la pantalla de prestacionesP
			//UPGRADE_TODO: (1067) Member Name is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			if (Convert.ToString(vForm.Name) == "PRESTACIONES")
			{
				if (Module1.bSeHaAceptadoPetitorio)
				{
					//UPGRADE_TODO: (1067) Member Recoger_Multiseleccion is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					vForm.Recoger_Multiseleccion(Module1.ArrayPrestacionPetitorio, Module1.ArrayDatosPruebasPetitorio);
				}
				//UPGRADE_TODO: (1067) Member SehaRecogidoPetitorio is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				vForm.SehaRecogidoPetitorio = Module1.bSeHaAceptadoPetitorio;
			}
			//----------

		}

		public void Load(ClassHCE310F2 VClass, object vForm, SqlConnection rc, string IndicadorTodas, object VarFECHA_optional, bool Multiseleccion, string ElServicio, object stTipoSer)
		{
			Load(VClass, vForm, rc, IndicadorTodas, VarFECHA_optional, Multiseleccion, ElServicio, stTipoSer, false);
		}

		public void Load(ClassHCE310F2 VClass, object vForm, SqlConnection rc, string IndicadorTodas, object VarFECHA_optional, bool Multiseleccion, string ElServicio)
		{
			Load(VClass, vForm, rc, IndicadorTodas, VarFECHA_optional, Multiseleccion, ElServicio, Type.Missing, false);
		}

		public void Load(ClassHCE310F2 VClass, object vForm, SqlConnection rc, string IndicadorTodas, object VarFECHA_optional, bool Multiseleccion)
		{
			Load(VClass, vForm, rc, IndicadorTodas, VarFECHA_optional, Multiseleccion, String.Empty, Type.Missing, false);
		}

		public void Load(ClassHCE310F2 VClass, object vForm, SqlConnection rc, string IndicadorTodas, object VarFECHA_optional)
		{
			Load(VClass, vForm, rc, IndicadorTodas, VarFECHA_optional, false, String.Empty, Type.Missing, false);
		}

		public void Load(ClassHCE310F2 VClass, object vForm, SqlConnection rc, string IndicadorTodas)
		{
			Load(VClass, vForm, rc, IndicadorTodas, Type.Missing, false, String.Empty, Type.Missing, false);
		}

		private void proMostrarBS()
		{

			DataSet rrDatos = null;
			string stSql = String.Empty;

			Module1.iMostrarBS = -1;

			//UPGRADE_TODO: (1067) Member Tag is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			if (Convert.ToString(RefFormulario.Tag) == "BUZONPRESTACIONES")
			{

				//UPGRADE_TODO: (1067) Member stHistoria is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				if (Convert.ToString(RefFormulario.stHistoria).Trim() == "")
				{

					//     Si venimos del buz�n de prestaciones, no se podr�n solicitar pruebas
					// de Banco de Sangre si el paciente no tiene historia.

					stSql = "select isNull(nnumeri1, -1) BS from SCONSGLO " + 
					        "where gconsglo = 'ISERVBS'";

					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Module1.RefRc);
					rrDatos = new DataSet();
					tempAdapter.Fill(rrDatos);

					if (rrDatos.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						Module1.iMostrarBS = Convert.ToInt32(rrDatos.Tables[0].Rows[0]["BS"]);
					}

					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					rrDatos.Close();

				}

			}

		}

		private void proMostrarAnatomia()
		{

			DataSet rrDatos = null;
			string stSql = String.Empty;

			Module1.iMostrarAnatomia = -1;

			//UPGRADE_TODO: (1067) Member Tag is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			if (Convert.ToString(RefFormulario.Tag) == "BUZONPRESTACIONES")
			{

				//     Si venimos del buz�n de prestaciones, no se podr�n solicitar prestaciones
				// de anatom�a patol�gica si Vitro est� activado, para prevenir problemas a la
				// hora de registrar el env�o de las pruebas a su BD.
				//     Por tanto, tomaremos el servicio de Anatom�a Patol�gica para evitarlo en el
				// listado, pero s�lo lo haremos cuando Vitro est� activado.

				stSql = "select isNull(A.nnumeri1, -1) Anatomia from SCONSGLO A " + 
				        "inner join SCONSGLO B on B.gconsglo = 'DVITROP' and " + 
				        "rTrim(isNull(B.valfanu1,'N')) = 'S' where " + 
				        "A.gconsglo = 'ISERANAT'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Module1.RefRc);
				rrDatos = new DataSet();
				tempAdapter.Fill(rrDatos);

				if (rrDatos.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					Module1.iMostrarAnatomia = Convert.ToInt32(rrDatos.Tables[0].Rows[0]["Anatomia"]);
				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rrDatos.Close();

			}

		}


		//Oscar C
		public void LoadPetitorio(dynamic vForm, SqlConnection rc, string IndicadorTodas, object VarFECHA_optional, string stTipoSer)
		{
			string VarFECHA = (VarFECHA_optional == Type.Missing) ? String.Empty : VarFECHA_optional as string;


			Module1.bSeHaAceptadoPetitorio = false;
			Module1.stItiposer = stTipoSer;

			Module1.RefRc = rc;
			Serrores.AjustarRelojCliente(rc);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Module1.RefRc);

			if (VarFECHA_optional != Type.Missing)
			{
				Module1.StFecha = VarFECHA;
			}
			else
			{
				Module1.StFecha = "";
			}

			Serrores.ObtenerFicheroAyuda();
			Serrores.ObtenerNombreAplicacion(Module1.RefRc);
			Module1.ClassMensaje = new Mensajes.ClassMensajes();

			//IndicadorTodas podr� tomar los valores S o N e indica si
			//queremos todas las prestaciones o solamente las activas
			Module1.Todas = IndicadorTodas;
			Module1.FrFormP = new HCE310F3();
			Module1.FrFormP.ShowDialog();

			if (Module1.bSeHaAceptadoPetitorio)
			{
				//UPGRADE_TODO: (1067) Member Recoger_Multiseleccion is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				vForm.Recoger_Multiseleccion(Module1.ArrayPrestacionPetitorio, Module1.ArrayDatosPruebasPetitorio);
			}

			//UPGRADE_TODO: (1067) Member SehaRecogidoPetitorio is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			vForm.SehaRecogidoPetitorio = Module1.bSeHaAceptadoPetitorio;

		}

		public void LoadPetitorio(object vForm, SqlConnection rc, string IndicadorTodas, object VarFECHA_optional)
		{
			LoadPetitorio(vForm, rc, IndicadorTodas, VarFECHA_optional, String.Empty);
		}

		public void LoadPetitorio(object vForm, SqlConnection rc, string IndicadorTodas)
		{
			LoadPetitorio(vForm, rc, IndicadorTodas, Type.Missing, String.Empty);
		}
		//----------------------
	}
}