using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace BuscaSelecPrestacionesDLL
{
    public partial class HCE310F2
        : Telerik.WinControls.UI.RadForm
    {

        bool noMarcarSubpruebas = false; //DIEGO 01-06-2006
        ClassHCE310F2 VClass = null;
        dynamic vForm = null;
		string comando = String.Empty;
		string strValorCTE = String.Empty;
		public HCE310F2()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}

		public void ObtenerMaxFilas()
		{
			comando = "Select NNUMERI1 from SCONSGLO where GCONSGLO='MAXFILAS'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(comando, Module1.RefRc);
			Module1.Rrs = new DataSet();
			tempAdapter.Fill(Module1.Rrs);			
			Module1.MAXFILAS = Convert.ToInt32(Module1.Rrs.Tables[0].Rows[0][0]);			
			Module1.Rrs.Close();
		}

		private void CB_Aceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			Mensajes.ClassMensajes objMensaje = null;
			string strServicio = String.Empty;
			string StDiagnostico = String.Empty;
			int iFilaInicial = 0;
			int iFilaFinal = 0;
			int iIndice = 0;
			string[] ArrayPrestaciones = null;
			string[] ArrayTmuePrestac = null;
			StringBuilder stCadenaAux = new StringBuilder();
			bool protocolosseleccionado = false;
			string[] ArrayDatosPruebas = null;
			string strPrimerServicio = String.Empty;


			if (String.Equals(strValorCTE.Trim(), "S", StringComparison.OrdinalIgnoreCase) && !String.Equals(Module1.stItiposer, "", StringComparison.OrdinalIgnoreCase))
			{ //nuevo tratamiento de las prestaciones

				if (sprSeleccionados.MaxRows > 0)
				{ //cargo todos los datos del grid de prestaciones seleccionadas

					ArrayDatosPruebas = ArraysHelper.InitializeArray<string>(sprSeleccionados.MaxRows);
					//obtenemos las pruebas seleccionadas
					int tempForVar = sprSeleccionados.MaxRows;
					for (int i = 1; i <= tempForVar; i++)
					{
                        sprSeleccionados.Col = 4;
						sprSeleccionados.Row = i;
						ArrayDatosPruebas[i - 1] = sprSeleccionados.Text;
					}


					ArrayPrestaciones = ArraysHelper.InitializeArray<string>(sprSeleccionados.MaxRows);
					ArrayTmuePrestac = ArraysHelper.InitializeArray<string>(sprSeleccionados.MaxRows);

					//obtenemos las prestaciones seleccionadas
					//Se mantiene el orden, y as� los 2 arrays est�n ordenados, de manera que coincidan las celdas con la prestaci�n tratada y sus pruebas seleccionadas
					int tempForVar2 = sprSeleccionados.MaxRows;
					for (int i = 1; i <= tempForVar2; i++)
					{
						sprSeleccionados.Col = 3; //columna con la clave de la prestaci�n. Si est� vac�o, es que es una prueba, y no una prestaci�n.
						sprSeleccionados.Row = i;
						stCadenaAux = new StringBuilder(sprSeleccionados.Text.Trim()); //clave col 3

						sprSeleccionados.Col = 1; //texto
						stCadenaAux.Append("@" + sprSeleccionados.Text.Trim());
						ArrayPrestaciones[i - 1] = stCadenaAux.ToString();
						sprSeleccionados.Col = 6;
						if (!String.Equals(sprSeleccionados.Text, "", StringComparison.OrdinalIgnoreCase) && !String.Equals(sprSeleccionados.Text, "-1", StringComparison.OrdinalIgnoreCase))
						{
							ArrayTmuePrestac[i - 1] = sprSeleccionados.Text;
						}
					}
					
					vForm.Recoger_Multiseleccion(ArrayPrestaciones, ArrayDatosPruebas, ArrayTmuePrestac);

					Module1.FrForm.Close();

				}

			}
			else
			{
				//funciona como hasta ahora

				//If SprPrestaciones.SelModeSelCount >= 1 Then 'hay una o m�s prestaciones seleccionadas

				iFilaInicial = proPrimeraFilaSeleccionada();
				iFilaFinal = proUltimaFilaSeleccionada();

				for (int ibucle = iFilaInicial; ibucle <= iFilaFinal; ibucle++)
				{
					SprPrestaciones.Row = ibucle;
					SprPrestaciones.Col = 5;
					strServicio = SprPrestaciones.Text;

					//si se intenta pasar una selecci�n m�ltiple de prestaciones reviso los servicios para que no sean distintos
					if (SprPrestaciones.SelModeSelCount > 1)
					{
						if (String.Equals(strPrimerServicio, "", StringComparison.OrdinalIgnoreCase))
						{
							strPrimerServicio = strServicio; //primera iteracci�n
						}
						else
						{
							if (!String.Equals(strServicio, strPrimerServicio, StringComparison.OrdinalIgnoreCase))
							{ //en cuanto haya un servicio distinto, me salgo
								RadMessageBox.Show("No puede seleccionar prestaciones de distinto servicio.", "Selecci�n de pruebas", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
								return;
							}
						}
						SprPrestaciones.Col = 6;
						if (!String.Equals(SprPrestaciones.Text, "", StringComparison.OrdinalIgnoreCase))
						{
							RadMessageBox.Show("Las pruebas con tipo de muestra se han de hacer de forma individual", "Selecci�n de pruebas", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
							return;
						}
					}

					if (FrmTipoMuestra.Visible && cbbTipoMuestra.Enabled)
					{
						if (cbbTipoMuestra.SelectedIndex == -1)
						{
							SprPrestaciones.Col = 6;
							if (!String.Equals(SprPrestaciones.Text, "", StringComparison.OrdinalIgnoreCase))
							{
								objMensaje = new Mensajes.ClassMensajes();
								short tempRefParam = 1040;
								string[] tempRefParam2 = new string[] { "Tipo de muestra"};
								objMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Module1.RefRc, tempRefParam2);
								objMensaje = null;
								cbbTipoMuestra.Focus();
								return;
							}
						}
					}

				} //cuando sale del bucle, es que todo es correcto, si no, terminar�a el procedimiento

				//oscar 18/02/2004
				ArrayDatosPruebas = new string[]{String.Empty};
				int tempForVar3 = SprProtocolo.MaxRows;
				string auxVar = String.Empty;
				for (int i = 1; i <= tempForVar3; i++)
				{
					SprProtocolo.Col = 1;
					SprProtocolo.Row = i;
					bool tempBool = false;
					auxVar = Convert.ToString(SprProtocolo.Value);
					if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
					{
						SprProtocolo.Col = 2;
						ArrayDatosPruebas = ArraysHelper.RedimPreserve(ArrayDatosPruebas, new int[]{ArrayDatosPruebas.GetUpperBound(0) + 3});
						ArrayDatosPruebas[ArrayDatosPruebas.GetUpperBound(0)] = SprProtocolo.Text;
					}
				}
				//--------------------------

				if (Module1.fMultiseleccion)
				{
					if (SprPrestaciones.SelModeSelCount > 0)
					{
						iFilaInicial = proPrimeraFilaSeleccionada();
						iFilaFinal = proUltimaFilaSeleccionada();

						//oscar 18/02/2004
						//solo se podra seleccionar un protocolo (unica seleccion) en el caso de que exista
						//multiseleccion de prestaciones y se seleccionen varias filas (entre ellas algun protocolo)
						if (!Module1.fSeleccionPruebas)
						{
						}
						else
						{
							protocolosseleccionado = false;
							for (int ibucle = iFilaInicial; ibucle <= iFilaFinal; ibucle++)
							{
								SprPrestaciones.Row = ibucle;
								if (SprPrestaciones.SelModeSelected)
								{
									SprPrestaciones.Col = 4;
									if (SoyProtocolo(SprPrestaciones.Text))
									{
										protocolosseleccionado = true;
										break;
									}
								}
							}

							if (SprPrestaciones.SelModeSelCount > 1 && protocolosseleccionado)
							{
								RadMessageBox.Show("Atenci�n: " + Environment.NewLine + 
								                "S�lo se permite la selecci�n de un �nico perfil." + Environment.NewLine + 
								                "Seleccione el perfil deseado con sus pruebas correspondientes." + Environment.NewLine + 
								                "(la multiselecci�n solo se permite para prestaciones independientes)", "Selecci�n de perfiles", MessageBoxButtons.OK, RadMessageIcon.Error);
								return;
							}

							if (ArrayDatosPruebas.GetUpperBound(0) + 1 == 0 && protocolosseleccionado)
							{
								RadMessageBox.Show("Debe seleccionar al menos una prueba del perfil.", "Selecci�n de pruebas", MessageBoxButtons.OK, RadMessageIcon.Error);
								return;
							}
						}
						//---------------

						ArrayPrestaciones = ArraysHelper.InitializeArray<string>(SprPrestaciones.SelModeSelCount);
						ArrayTmuePrestac = ArraysHelper.InitializeArray<string>(SprPrestaciones.SelModeSelCount);
						iIndice = 0;
						stCadenaAux = new StringBuilder("");
						for (int ibucle = iFilaInicial; ibucle <= iFilaFinal; ibucle++)
						{
							SprPrestaciones.Row = ibucle;
							//si el registro esta seleccionado
							if (SprPrestaciones.SelModeSelected)
							{
								iIndice++;
								//se cargan los datos en la estructura
								SprPrestaciones.Col = 4;
								stCadenaAux = new StringBuilder(SprPrestaciones.Text.Trim());
								SprPrestaciones.Col = 1;
								stCadenaAux.Append("@" + SprPrestaciones.Text.Trim());
								ArrayPrestaciones[iIndice - 1] = stCadenaAux.ToString();
								if (FrmTipoMuestra.Visible && cbbTipoMuestra.SelectedIndex != -1)
								{
									ArrayTmuePrestac[iIndice - 1] = cbbTipoMuestra.GetItemData(cbbTipoMuestra.SelectedIndex).ToString();
								}
								else
								{
									ArrayTmuePrestac[iIndice - 1] = "";
								}
							}
						}
						//oscar 18/02/2004
						//una vez que se carga la estructura lo mandamoa a la pantalla
						//VForm.Recoger_Multiseleccion ArrayPrestaciones
						//Ahora debemos tener en cuenta que solo devolvera como maximo un
						//protocolo de labortatorio en la multiseleccion (siempre que �ste haya
						//sido el ultimo en seleccionarse) con sus pruebas seleccionadas.
						//UPGRADE_TODO: (1067) Member Name is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						if (String.Equals(Convert.ToString(vForm.Name), "PRESTACIONES", StringComparison.OrdinalIgnoreCase) || String.Equals(Convert.ToString(vForm.Name), "TI0004F1", StringComparison.OrdinalIgnoreCase) || String.Equals(Convert.ToString(vForm.Name), "DetallePrestacion", StringComparison.OrdinalIgnoreCase))
						{							
							vForm.Recoger_Multiseleccion(ArrayPrestaciones, ArrayDatosPruebas, ArrayTmuePrestac);
						}
						else
						{							
							vForm.Recoger_Multiseleccion(ArrayPrestaciones, ArrayDatosPruebas);
						}
						//----------------
						Module1.FrForm.Close();
					}
					else
					{
					}
				}
				else
				{
					if (!String.Equals(TB_CodPrestacion.Text, "", StringComparison.OrdinalIgnoreCase) && !String.Equals(TB_DescPrestacion.Text, "", StringComparison.OrdinalIgnoreCase))
					{
						SprPrestaciones.Col = 4;
						if (String.Equals(TB_CodPrestacion.Text.Trim(), SprPrestaciones.Text.Trim(), StringComparison.OrdinalIgnoreCase))
						{

							//oscar 18/02/2004
							protocolosseleccionado = false;
							if (SoyProtocolo(SprPrestaciones.Text))
							{
								protocolosseleccionado = true;
							}
							if (ArrayDatosPruebas.GetUpperBound(0) + 1 == 0 && protocolosseleccionado)
							{
								RadMessageBox.Show("Debe seleccionar al menos una prueba del perfil.", "Selecci�n de perfiles", MessageBoxButtons.OK, RadMessageIcon.Error);
								return;
							}
							//---------------

							SprPrestaciones.Col = 1;
							StDiagnostico = SprPrestaciones.Text.Trim();
							SprPrestaciones.Col = 2;
							StDiagnostico = StDiagnostico.Trim() + SprPrestaciones.Text.Trim();
							SprPrestaciones.Col = 3;
							StDiagnostico = StDiagnostico.Trim() + SprPrestaciones.Text.Trim();
							if (String.Equals(TB_DescPrestacion.Text.Trim(), StDiagnostico.Trim(), StringComparison.OrdinalIgnoreCase))
							{
								Module1.ClassMensaje = null;
								SprServicios.Row = SprServicios.ActiveRowIndex;
								SprServicios.Col = 2;
								
								if ((String.Equals(Convert.ToString(vForm.Name), "PRESTACIONES", StringComparison.OrdinalIgnoreCase) || String.Equals(Convert.ToString(vForm.Name), "TI0004F1", StringComparison.OrdinalIgnoreCase) || String.Equals(Convert.ToString(vForm.Name), "DetallePrestacion", StringComparison.OrdinalIgnoreCase)) && cbbTipoMuestra.SelectedIndex != -1)
								{									
									vForm.Recoger_Datos(TB_CodPrestacion.Text, TB_DescPrestacion.Text, SprServicios.Text.Trim(), TB_DescServicio.Text.Trim(), ArrayDatosPruebas, cbbTipoMuestra.GetItemData(cbbTipoMuestra.SelectedIndex).ToString().Trim() + " - " + cbbTipoMuestra.Text.Trim());
								}
								else
								{									
									vForm.Recoger_Datos(TB_CodPrestacion.Text, TB_DescPrestacion.Text, SprServicios.Text.Trim(), TB_DescServicio.Text.Trim(), ArrayDatosPruebas);
								}
								Module1.FrForm.Close();
							}
							else
							{                                								
								Module1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1100, Module1.RefRc, FRM_Prestacion.Text.Trim());
							}
						}
						else
						{							
							Module1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1100, Module1.RefRc, FRM_Prestacion.Text.Trim());
						}
					}
					else
					{						
						Module1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1040, Module1.RefRc, FRM_Prestacion.Text.Trim());
					}

				}

			}
		}

		private void CB_cancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			Module1.FrForm.Close();
		}

		public void Referencias(ClassHCE310F2 VClass2, object VForm2, string stTipo)
		{
			VClass = VClass2;
			vForm = VForm2;
			Module1.stItiposer = stTipo;
		}

		private bool PrestacionSeleccionada(string strPrestacionBuscada)
		{
			//busca en el grid de seleccionados, si ya est� la prestaci�n
            bool bEncontrado = false;
			sprSeleccionados.Col = 3; //columna de la clave

			int tempForVar = sprSeleccionados.MaxRows;
			for (int i = 1; i <= tempForVar; i++)
			{
				sprSeleccionados.Row = i;
				if (String.Equals(sprSeleccionados.Text, strPrestacionBuscada, StringComparison.OrdinalIgnoreCase))
				{
					bEncontrado = true;
					break;
				}
			}
			return bEncontrado;
		}

		private void cmdAnadir_Click(Object eventSender, EventArgs eventArgs)
		{
			bool bGridServiciosBloqueado = false;
			int iFilaFinal = 0;
			int iFilaInicial = 0;
			//DIEGO 30/10/2006 - A�ade las pruebas/prestaciones seleccionadas al nuevo grid
			string strCadenaClavesPruebas = String.Empty;
			string strCadenaNombresPruebas = String.Empty;
			string strPrestacionSeleccionada = String.Empty;
			DataSet rrProtocolo = null;
			StringBuilder strSQL = new StringBuilder();
			Mensajes.ClassMensajes objMensaje = null;

			string strPrimerServicio = "";
			string strServicio = "";
			string strServicioSeleccion = "";
			string strSeparador = "@";
			 //si est� marcada, concateno nombres y claves
			if (SprPrestaciones.SelModeSelCount >= 1)
			{ //hay una o m�s prestaciones seleccionadas

				iFilaInicial = proPrimeraFilaSeleccionada();
				iFilaFinal = proUltimaFilaSeleccionada();

				for (int ibucle = iFilaInicial; ibucle <= iFilaFinal; ibucle++)
				{
					SprPrestaciones.Row = ibucle;
					SprPrestaciones.Col = 5;
					strServicio = SprPrestaciones.Text;

					if (SprPrestaciones.SelModeSelCount > 1)
					{ //si se intenta pasar una selecci�n m�ltiple de prestaciones

						if (String.Equals(strPrimerServicio, "", StringComparison.OrdinalIgnoreCase))
						{
							strPrimerServicio = strServicio; //primera iteracci�n
						}
						else
						{
							if (!String.Equals(strServicio, strPrimerServicio, StringComparison.OrdinalIgnoreCase))
							{ //en cuanto haya un servicio distinto, me salgo
								RadMessageBox.Show("No puede seleccionar prestaciones de distinto servicio.", "Selecci�n de pruebas", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
								return;
							}
						}
						if (FrmTipoMuestra.Visible)
						{
							SprPrestaciones.Col = 6;
							if (!String.Equals(SprPrestaciones.Text, "", StringComparison.OrdinalIgnoreCase))
							{
								RadMessageBox.Show("Las pruebas con tipo de muestra se han de hacer de forma individual", "Selecci�n de pruebas", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
								return;
							}
						}
					}
					else
					{
						//si se intenta pasar una selecci�n simple de prestaciones

						int tempForVar = sprSeleccionados.MaxRows;
						for (int i = 1; i <= tempForVar; i++)
						{ //recorro el grid de seleccionados buscando el servicio de la prestaci�n que voy a a�adir

							sprSeleccionados.Row = i;
							sprSeleccionados.Col = 5;
							strServicioSeleccion = sprSeleccionados.Text;

							if (!String.Equals(strServicioSeleccion, strServicio, StringComparison.OrdinalIgnoreCase))
							{ //en cuanto haya un servicio distinto, me salgo
								RadMessageBox.Show("No puede seleccionar prestaciones de distinto servicio.", "Selecci�n de pruebas", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
								return;
							}

						}
						if (FrmTipoMuestra.Visible)
						{
							SprPrestaciones.Col = 6;
							if (!String.Equals(SprPrestaciones.Text, "", StringComparison.OrdinalIgnoreCase) && cbbTipoMuestra.SelectedIndex == -1 && cbbTipoMuestra.Enabled)
							{
								objMensaje = new Mensajes.ClassMensajes();
								short tempRefParam = 1040;
                                string[] tempRefParam2 = new string[] { "Tipo de muestra"};
								objMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Module1.RefRc, tempRefParam2);
								objMensaje = null;
								cbbTipoMuestra.Focus();
								return;
							}
						}

					}

				}

				string auxVar = String.Empty;
				for (int ibucle = iFilaInicial; ibucle <= iFilaFinal; ibucle++)
				{

					strCadenaClavesPruebas = "";
					strCadenaNombresPruebas = "";

					SprPrestaciones.Row = ibucle;

					if (SprPrestaciones.SelModeSelected)
					{ //si la fila est� seleccionada...

						SprPrestaciones.Col = 4; //clave prestaci�n
						strPrestacionSeleccionada = SprPrestaciones.Text;

						if (!PrestacionSeleccionada(strPrestacionSeleccionada))
						{

							if (!SoyProtocolo(strPrestacionSeleccionada))
							{
								//no es protocolo, a�ado la prestaci�n sin pruebas
								sprSeleccionados.MaxRows++;
								sprSeleccionados.Row = sprSeleccionados.MaxRows;
								sprSeleccionados.Col = 3;
								sprSeleccionados.Text = strPrestacionSeleccionada;
								sprSeleccionados.Col = 1;
								SprPrestaciones.Col = 1;
								sprSeleccionados.Text = SprPrestaciones.Text;
								sprSeleccionados.Col = 5; //clave servicio
								SprServicios.Row = SprServicios.ActiveRowIndex;
								SprServicios.Col = 2;
								sprSeleccionados.Text = SprServicios.Text;
								if (FrmTipoMuestra.Visible && cbbTipoMuestra.SelectedIndex != -1)
								{
									sprSeleccionados.Col = 6;
									sprSeleccionados.Text = cbbTipoMuestra.SelectedIndex.ToString();
								}
								//marcamos la �ltima a�adida
								sprSeleccionados.Col = 1;
								sprSeleccionados.Row = sprSeleccionados.MaxRows;
								sprSeleccionados.Action = 0; //activate cell
								//bloqueamos el grid de pruebas
								SprProtocolo.Enabled = false;

							}
							else
							{

								if (SprPrestaciones.SelModeSelCount > 1)
								{

									//es protocolo, a�ado la prestaci�n con TODAS sus pruebas
									strSQL = new StringBuilder("select dprotlab.gpruelab,dcodpres.dprestac " + 
									         " from dprotlab, dcodpres " + 
									         " where dprotlab.gprestac = '" + strPrestacionSeleccionada + "' AND " + 
									         " dprotlab.gpruelab = dcodpres.gprestac ");

									//Al cargar las pruebas del protocolo, no se cargan las que se haya dado de baja
									if (!String.Equals(Module1.StFecha.Trim(), "", StringComparison.OrdinalIgnoreCase))
									{
										string tempRefParam3 = Module1.StFecha.Trim();
										string tempRefParam4 = Module1.StFecha.Trim();
										strSQL.Append(
										              " and dcodpres.FINIVALI<=" + Serrores.FormatFecha(tempRefParam3) + 
										              " and (dcodpres.FFINVALI>=" + Serrores.FormatFecha(tempRefParam4) + " OR dcodpres.FFINVALI is null) ");
									}
									else if (String.Equals(Module1.Todas.Trim().ToUpper(), "N", StringComparison.OrdinalIgnoreCase))
									{ 
										strSQL.Append(" and dcodpres.FFINVALI is null ");
									}

									strSQL.Append(" order by dcodpres.dprestac");
									SqlDataAdapter tempAdapter = new SqlDataAdapter(strSQL.ToString(), Module1.RefRc);
									rrProtocolo = new DataSet();
									tempAdapter.Fill(rrProtocolo);

									foreach (DataRow iteration_row in rrProtocolo.Tables[0].Rows)
									{
										if (String.Equals(strCadenaClavesPruebas, "", StringComparison.OrdinalIgnoreCase))
										{
											strCadenaClavesPruebas = Convert.ToString(iteration_row["gpruelab"]);
										}
										else
										{
											strCadenaClavesPruebas = strCadenaClavesPruebas + strSeparador + Convert.ToString(iteration_row["gpruelab"]);
										}

										if (String.Equals(strCadenaNombresPruebas, "", StringComparison.OrdinalIgnoreCase))
										{
											strCadenaNombresPruebas = Convert.ToString(iteration_row["dprestac"]);
										}
										else
										{
											strCadenaNombresPruebas = strCadenaNombresPruebas + strSeparador + Convert.ToString(iteration_row["dprestac"]);
										}
									}

								}
								else if (SprPrestaciones.SelModeSelCount == 1)
								{  //SprPrestaciones.SelModeSelCount = 1 --> s�lo 1 fila seleccionada

									int tempForVar2 = SprProtocolo.MaxRows;
									for (int i = 1; i <= tempForVar2; i++)
									{
										SprProtocolo.Col = 1;
										SprProtocolo.Row = i;

										bool tempBool = false;
										auxVar = Convert.ToString(SprProtocolo.Value);
										if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
										{

											//concateno la clave
											SprProtocolo.Col = 2;
											//sprSeleccionados.Col = 4
											//sprSeleccionados.Text = SprProtocolo.Text
											if (String.Equals(strCadenaClavesPruebas, "", StringComparison.OrdinalIgnoreCase))
											{
												strCadenaClavesPruebas = SprProtocolo.Text;
											}
											else
											{
												strCadenaClavesPruebas = strCadenaClavesPruebas + strSeparador + SprProtocolo.Text;
											}

											//concateno los nombres
											SprProtocolo.Col = 3;
											//sprSeleccionados.Col = 2
											//sprSeleccionados.Text = SprProtocolo.Text
											if (String.Equals(strCadenaNombresPruebas, "", StringComparison.OrdinalIgnoreCase))
											{
												strCadenaNombresPruebas = SprProtocolo.Text;
											}
											else
											{
												strCadenaNombresPruebas = strCadenaNombresPruebas + strSeparador + SprProtocolo.Text;
											}

										}

									}

								} //If SprPrestaciones.SelModeSelCount > 1 Then...

								if (!String.Equals(strCadenaClavesPruebas.Trim(), "", StringComparison.OrdinalIgnoreCase))
								{

									//relleno los datos de la prestaci�n a la que pertenecen las pruebas seleccionadas
									//SprPrestaciones.Row = SprPrestaciones.ActiveRow
									sprSeleccionados.MaxRows++;
									sprSeleccionados.Row = sprSeleccionados.MaxRows;
									SprPrestaciones.Col = 1; //nombre prestaci�n
									sprSeleccionados.Col = 1;
									sprSeleccionados.Text = SprPrestaciones.Text;
									SprPrestaciones.Col = 4; //clave prestaci�n
									sprSeleccionados.Col = 3;
									sprSeleccionados.Text = SprPrestaciones.Text;
									sprSeleccionados.Col = 2; //nombres pruebas
									sprSeleccionados.Text = strCadenaNombresPruebas;
									sprSeleccionados.Col = 4; //claves pruebas
									sprSeleccionados.Text = strCadenaClavesPruebas;
									sprSeleccionados.Col = 5; //clave servicio
									SprServicios.Row = SprServicios.ActiveRowIndex;
									SprServicios.Col = 2;
									sprSeleccionados.Text = SprServicios.Text;
									sprSeleccionados.Col = 6;
									if (FrmTipoMuestra.Visible && cbbTipoMuestra.SelectedIndex != -1)
									{
										sprSeleccionados.Text = cbbTipoMuestra.GetItemData(cbbTipoMuestra.SelectedIndex).ToString();
									}
									else
									{
										sprSeleccionados.Text = "-1";
									}
									//marcamos la �ltima a�adida
									sprSeleccionados.Col = 1;
									sprSeleccionados.Row = sprSeleccionados.MaxRows;
									sprSeleccionados.Action = 0; //activate cell
									//bloqueamos el grid de pruebas
									SprProtocolo.Enabled = false;
									Option1[0].Enabled = false;
									Option1[1].Enabled = false;

								}
								else
								{
									//no se han marcado pruebas
									RadMessageBox.Show("Debe seleccionar al menos una prueba del perfil.", "Selecci�n de pruebas", MessageBoxButtons.OK, RadMessageIcon.Error);
									return;
								}


							} //If Not SoyProtocolo(strPrestacionSeleccionada) Then....

						}
						else
						{

							RadMessageBox.Show("La prestaci�n ya est� seleccionada.", "Selecci�n de pruebas", MessageBoxButtons.OK, RadMessageIcon.Exclamation);

						} //If Not PrestacionSeleccionada(strPrestacionSeleccionada) Then...

					} //If SprPrestaciones.SelModeSelected Then....

				} //For ibucle = iFilaInicial To iFilaFinal

			} //If SprPrestaciones.SelModeSelCount >= 1 Then

			//para bloquear el grid de servicios (s�lo se permite un �nico servicio)
			if (sprSeleccionados.MaxRows > 0)
			{
				SprServicios.Enabled = false;
				bGridServiciosBloqueado = true;
				CB_Aceptar.Enabled = true;
				//deshabilito la b�squeda de servicios
				TB_DescServicio.Enabled = false;
				Toolbar2.Enabled = false;
				//s�lo se podr�n buscar prestaciones en el mismo servicio de la a�adida
				chkServicioSeleccionado.CheckState = CheckState.Checked; //marco
				chkServicioSeleccionado.Enabled = false;
			}
			else
			{
				SprServicios.Enabled = true;
				bGridServiciosBloqueado = false;
				CB_Aceptar.Enabled = false;
				//habilito la b�squeda de servicios
				TB_DescServicio.Enabled = true;
				Toolbar2.Enabled = true;
				//s�lo se podr�n buscar prestaciones en el mismo servicio de la a�adida
				chkServicioSeleccionado.CheckState = CheckState.Checked; //marco
				chkServicioSeleccionado.Enabled = true;
			}
		}

		private void cmdEliminar_Click(Object eventSender, EventArgs eventArgs)
		{
			bool bGridServiciosBloqueado = false;

			Mensajes.ClassMensajes objMensaje = new Mensajes.ClassMensajes();

			if (sprSeleccionados.MaxRows > 0)
			{
				if (sprSeleccionados.ActiveRowIndex > 0)
				{ //si hay una fila seleccionada...
					//...la borramos
					sprSeleccionados.Row = sprSeleccionados.ActiveRowIndex;
                    //camaya todo_x_5
                    //sprSeleccionados.Action = 5;
                    sprSeleccionados.MaxRows--;
					//vac�o el grid de las pruebas hijas
					SprProtocolo.MaxRows = 0;
					//desactivo la fila seleccionada en SprPrestaciones ya que no hay pruebas en la otra lista
					SprPrestaciones.SelModeSelected = false;
				}
				else
				{
					short tempRefParam = 1271;
                    string[] tempRefParam2 = new string[] { };
					objMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Module1.RefRc, tempRefParam2);
				}
			}
			else
			{
				short tempRefParam3 = 1271;
				string[] tempRefParam4 = new string[] { };
				objMensaje.RespuestaMensaje( Serrores.vNomAplicacion, Serrores.vAyuda,  tempRefParam3, Module1.RefRc, tempRefParam4);
			}

			//para bloquear el grid de servicios (s�lo se permite un �nico servicio)
			if (sprSeleccionados.MaxRows > 0)
			{
				SprServicios.Enabled = false;
				bGridServiciosBloqueado = true;
				CB_Aceptar.Enabled = true;
				//deshabilito la b�squeda de servicios
				TB_DescServicio.Enabled = false;
				Toolbar2.Enabled = false;
				//s�lo se podr�n buscar prestaciones en el mismo servicio de la a�adida
				chkServicioSeleccionado.CheckState = CheckState.Checked; //marco
				chkServicioSeleccionado.Enabled = false;
			}
			else
			{
				SprServicios.Enabled = true;
				bGridServiciosBloqueado = false;
				CB_Aceptar.Enabled = false;
				//habilito la b�squeda de servicios
				TB_DescServicio.Enabled = true;
				Toolbar2.Enabled = true;
				//s�lo se podr�n buscar prestaciones en el mismo servicio de la a�adida
				chkServicioSeleccionado.CheckState = CheckState.Checked; //marco
				chkServicioSeleccionado.Enabled = true;

			}

		}
		//OSCAR C (PETITORIO LABORATORIO)
		private void cmdPetitorio_Click(Object eventSender, EventArgs eventArgs)
		{
			Module1.FrFormP = new HCE310F3();
			Module1.FrFormP.ShowDialog();

			if (Module1.bSeHaAceptadoPetitorio)
			{
				this.Close();
			}

		}
		//-------------------

		private void HCE310F2_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				//I.S.M. 23-Mayo-2.000. A�adido para que haga bien el Refresh
				//As� tiene el foco la primera vez. (En Form_Load no se puede usar Setfocus)
				if (TB_DescServicio.TabIndex == 0)
				{
					TB_DescServicio.TabIndex = 4;
				} //Y vuelve a su orden

			}
		}

		
		private void HCE310F2_Load(Object eventSender, EventArgs eventArgs)
		{
			Module1.stTipoMues = Serrores.ObternerValor_CTEGLOBAL(Module1.RefRc, "TIPOMUES", "valfanu1").Trim().ToUpper();
			//si la pantalla que llama a la dll de buscar prestaciones no es la de prestaciones, nunca aparecer� la selecci�n del tipo de muestra.
			
			if (!String.Equals(Convert.ToString(vForm.Name), "PRESTACIONES", StringComparison.OrdinalIgnoreCase) && !String.Equals(Convert.ToString(vForm.Name), "TI0004F1", StringComparison.OrdinalIgnoreCase) && !String.Equals(Convert.ToString(vForm.Name), "DetallePrestacion", StringComparison.OrdinalIgnoreCase))
			{
				Module1.stTipoMues = "N";
			}
			//Rub�n 07-10-2009
			strValorCTE = Serrores.ObternerValor_CTEGLOBAL(Module1.RefRc, "NTPRESTA", "valfanu1");

			//diego 30/11/06 - recoloco los controles si se est� utilizando la nueva versi�n de la pantalla de prestaciones
			if (!String.Equals(Module1.stItiposer, "", StringComparison.OrdinalIgnoreCase) && String.Equals(strValorCTE.Trim(), "S", StringComparison.OrdinalIgnoreCase))
			{ //nueva pantalla
				//Rub�n 07-10-2009
				if (String.Equals(Module1.stTipoMues, "N", StringComparison.OrdinalIgnoreCase))
				{
					cbbTipoMuestra.Items.Clear();
					cbbTipoMuestra.SelectedIndex = -1;
					FrmTipoMuestra.Enabled = false;
					FrmTipoMuestra.Visible = false;
					FRM_Prestacion.Height = (int) VB6.ToPixelsUserHeight(430.146d, 622, 564);
					SprPrestaciones.Height = 285;
					FrmTipoMuestra.Top = 260;
					Frame3.Top = 300;
					Frame3.Visible = true;
					Frame2.Visible = true;
					Module1.FILAS_VISIBLES = 15;
				}
				else
				{
					cbbTipoMuestra.Items.Clear();
					cbbTipoMuestra.SelectedIndex = -1;
					FrmTipoMuestra.Enabled = false;
					FrmTipoMuestra.Visible = true;
					FRM_Prestacion.Height = (int) VB6.ToPixelsUserHeight(430.146d, 622, 564);
					SprPrestaciones.Height = 243;
					FrmTipoMuestra.Top = 260;
					Frame3.Top = 300;
					Frame3.Visible = true;
					Frame2.Visible = true;
					Module1.FILAS_VISIBLES = 14;
				}
				//Rub�n 07-10-2009
			}
			else
			{
				//como hasta ahora

				//Rub�n 07-10-2009
				if (String.Equals(Module1.stTipoMues, "N", StringComparison.OrdinalIgnoreCase))
				{
					cbbTipoMuestra.Items.Clear();
					cbbTipoMuestra.SelectedIndex = -1;
					FrmTipoMuestra.Enabled = false;
					FrmTipoMuestra.Visible = false;
					FRM_Prestacion.Height = (int) VB6.ToPixelsUserHeight(585.661d, 622, 564);
					SprPrestaciones.Height = 430;
					FrmTipoMuestra.Top = 407;
					Frame3.Top = 441;
					Frame3.Visible = true;
					Frame2.Visible = false;
					Module1.FILAS_VISIBLES = 26;
				}
				else
				{
					cbbTipoMuestra.Items.Clear();
					cbbTipoMuestra.SelectedIndex = -1;
					FrmTipoMuestra.Enabled = false;
					FrmTipoMuestra.Visible = true;
					FRM_Prestacion.Height = (int) VB6.ToPixelsUserHeight(585.661d, 622, 564);
					SprPrestaciones.Height = 394;
					FrmTipoMuestra.Top = 407;
					Frame3.Top = 441;
					Frame3.Visible = true;
					Frame2.Visible = false;
					Module1.FILAS_VISIBLES = 24;
				}
				//Rub�n 07-10-2009

			}            			
						
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
            this.Top = (int) 0;
			this.Left = (int) 0;
			//I.S.M. 23-Mayo-2.000. A�adido para que haga bien el Refresh
			//Si SprServicios o SprPrestaciones tienen el foco, no aparecen en la pantalla
			TB_DescServicio.TabIndex = 0;

			SprServicios.MaxRows = 0;
			SprServicios.Col = 2;
			SprServicios.SetColHidden(SprServicios.Col, true);
			SprPrestaciones.MaxRows = 0;
			SprPrestaciones.Col = 2;
			SprPrestaciones.SetColHidden(SprPrestaciones.Col, true);
			SprPrestaciones.Col = 3;
			SprPrestaciones.SetColHidden(SprPrestaciones.Col, true);
			SprPrestaciones.Col = 4;
			SprPrestaciones.SetColHidden(SprPrestaciones.Col, true);
			SprPrestaciones.Col = 5;
			SprPrestaciones.SetColHidden(SprPrestaciones.Col, true);
			SprPrestaciones.Col = 6;
			SprPrestaciones.SetColHidden(SprPrestaciones.Col, true);
			Module1.NuevaConsulta = false;
			Module1.DesPrestacion = "";

			ObtenerMaxFilas();
			Module1.LlenarDatos(vForm, Module1.stItiposer);

			if (Module1.fMultiseleccion)
			{
                //camaya todo_x_5				
				//SprPrestaciones.setAllowCellOverflow = true;
				//SprPrestaciones.ActiveSheet.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.MultiRange;
				//SprPrestaciones.OperationMode = FarPoint.Win.Spread.OperationMode.ExtendedSelect;
			}
			else
			{
                //camaya todo_x_5                
                //SprPrestaciones.setAllowCellOverflow(false);
                //SprPrestaciones.ActiveSheet.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.Single;
                SprPrestaciones.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
			}

			//I.S.M. 22-Mayo-2.000. A�adido por defecto Servicio seleccionado
			chkServicioSeleccionado.CheckState = CheckState.Checked;


			FRM_Servicio.Refresh();
			FRM_Prestacion.Refresh();
			//this.Icon =  PIC_Prestaciones.Image;
			this.Icon = System.Drawing.Icon.FromHandle(((Bitmap)PIC_Prestaciones.Image).GetHicon());
            SprProtocolo.MaxRows = 0;


			//OSCAR C (PETITORIO LABORATORIO)
			//De momento, la funcionalidad del petitorio solo funciona desde la pantalla de prestacionesP			
			cmdPetitorio.Visible = Module1.fPetitorioLaboratorio() && String.Equals(Convert.ToString(vForm.Name), "PRESTACIONES", StringComparison.OrdinalIgnoreCase);
			//--------------

			//DIEGO 01-06-2006

			noMarcarSubpruebas = false;

			if (String.Equals(Serrores.ObternerValor_CTEGLOBAL(Module1.RefRc, "MAPRULAB", "valfanu1"), "N", StringComparison.OrdinalIgnoreCase))
			{
				noMarcarSubpruebas = true;
			}
			else
			{
				//si es S, o no existe, o cualuier otro valos, ponemos por defecto que las marque.
				noMarcarSubpruebas = false;
			}
			///DIEGO 01-06-2006

			sprSeleccionados.Col = 2;
			sprSeleccionados.SetColHidden(sprSeleccionados.Col, true);
			sprSeleccionados.Col = 3;
			sprSeleccionados.SetColHidden(sprSeleccionados.Col, true);
			sprSeleccionados.Col = 4;
			sprSeleccionados.SetColHidden(sprSeleccionados.Col, true);
			sprSeleccionados.Col = 5;
			sprSeleccionados.SetColHidden(sprSeleccionados.Col, true);
		}

		private void HCE310F2_Closed(Object eventSender, EventArgs eventArgs)
		{
            Module1.Rq2.Close();
			if (Module1.NuevaConsulta)
			{
				
				Module1.Rrs.Close();
			}
			if (Module1.CerrarConsulta)
			{
				//29-11-2006 - DIEGO si se ha cerrado previamente no lo cierro
				//camaya todo_x_5
				//if (Module1.Rrs2.getActiveConnection() != null)
				//{					
				//	Module1.Rrs2.Close();
				//}
				//29-11-2006 - DIEGO si se ha cerrado previamente no lo cierro
				if (Module1.Rq2.Connection != null)
				{					
					Module1.Rq2.Close();
				}
				//    Rrs2.Close
				//    Rq2.Close
			}
			//RefRc.Close
			//Re.Close
		}

		//oscar 18/02/2004
		private bool isInitializingComponent;
		private void Option1_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.Option1, eventSender);
				bool marcado = false;
				switch(Index)
				{
					case 0 : 
						marcado = true; 
						break;
					case 1 : 
						marcado = false; 
						break;
				}
				SprProtocolo.Col = 1;
				SprProtocolo.Row = -1;
				SprProtocolo.Value = marcado;
			}
		}
		//-------------------

		private void SprPrestaciones_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = 0;
			int Row = 0;
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = SprPrestaciones.Col;
                Row = SprPrestaciones.Row;
            }
            if (Module1.fMultiseleccion)
			{
				// If SprPrestaciones.SelModeSelected = True Then
				//     SprPrestaciones.SelModeSelected = False
				// Else
				//     SprPrestaciones.SelModeSelected = True
				// End If

				//I.S.M. 23-Mayo-2.000. A�adido para actualizar el c�digo y la descripci�n de la Prestaci�n
				if (SprPrestaciones.MaxRows != 0)
				{ //Hay filas
					//If SprPrestaciones.SelBlockRow = SprPrestaciones.SelBlockRow2 Then 'S�lo una seleccionada (No siempre, puede haber m�s de un bloque)
					if (SprPrestaciones.SelModeSelCount == 1)
					{ //Una seleccionada
						Module1.CambiaFilaSeleccionada2(SprPrestaciones.ActiveRowIndex);
					}
					else
					{
						//Hay m�s de una fila seleccionada
						Module1.FrForm.TB_CodPrestacion.Text = "";
						Module1.FrForm.TB_DescPrestacion.Text = "";
					}
				}

			}
			else
			{
				if (SprPrestaciones.MaxRows != 0)
				{
					Module1.CambiaFilaSeleccionada2(SprPrestaciones.ActiveRowIndex);
				}
			}
			if (Row != 0)
			{
				CargarProtocolo(Row);
			}

			if (Row != 0 && FrmTipoMuestra.Visible)
			{
				CargarTiposMuestra(Row);
			}


			if (String.Equals(strValorCTE, "N", StringComparison.OrdinalIgnoreCase) || String.Equals(Module1.stItiposer, "", StringComparison.OrdinalIgnoreCase))
			{
				CB_Aceptar.Enabled = true;
			}

			if (String.Equals(strValorCTE, "S", StringComparison.OrdinalIgnoreCase))
			{
				SprProtocolo.Enabled = true;
			}

			//oscar 18/02/2004
			if (!Module1.fSeleccionPruebas)
			{
				Option1[0].Visible = false;
				Option1[1].Visible = false;
				SprProtocolo.Col = 1;
				SprProtocolo.SetColHidden(SprProtocolo.Col, true);

			}
			//-------
		}
		private void CargarTiposMuestra(int fila)
		{

			Module1.FrForm.SprPrestaciones.Col = 4;
			Module1.FrForm.SprPrestaciones.Row = fila;

			cbbTipoMuestra.Items.Clear();

			Module1.FrForm.SprPrestaciones.Col = 6;
			Module1.FrForm.SprPrestaciones.Row = fila;
			string stSql = "SELECT gtipmues,dtipmues from DTIPMUES where gagrumue = '" + Module1.FrForm.SprPrestaciones.Text + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Module1.RefRc);
			DataSet rrDatos = new DataSet();
			tempAdapter.Fill(rrDatos);
			if (rrDatos.Tables[0].Rows.Count != 0)
			{				
				rrDatos.MoveFirst();
				for (int iCont = 0; iCont <= rrDatos.Tables[0].Rows.Count - 1; iCont++)
				{					
					cbbTipoMuestra.AddItem(Convert.ToString(rrDatos.Tables[0].Rows[iCont]["dtipmues"]), iCont);					
					cbbTipoMuestra.SetItemData(iCont, Convert.ToInt32(rrDatos.Tables[0].Rows[iCont]["gtipmues"]));					
					rrDatos.MoveNext();
				}
				if (rrDatos.Tables[0].Rows.Count == 1)
				{
					cbbTipoMuestra.SelectedIndex = 0;
				}
				else
				{
					cbbTipoMuestra.SelectedIndex = -1;
				}
			}
			
			rrDatos.Close();
			//    If FrForm.SprPrestaciones.SelModeSelCount > 1 Then
			//
			//    End If
			FrmTipoMuestra.Enabled = (cbbTipoMuestra.Items.Count != 0); //Or (FrForm.SprPrestaciones.SelModeSelCount > 1 And FrmTipoMuestra.Enabled)
			cbbTipoMuestra.Enabled = (cbbTipoMuestra.Items.Count != 0); //Or (FrForm.SprPrestaciones.SelModeSelCount > 1 And FrmTipoMuestra.Enabled)
		}
		private void CargarProtocolo(int fila)
		{
			bool bProtocoloCerrado = false;

			Module1.FrForm.SprPrestaciones.Col = 4;
			Module1.FrForm.SprPrestaciones.Row = fila;
			string Codpresta = Module1.FrForm.SprPrestaciones.Text;

			string sqlDCODPRES = "SELECT iprotcer from DCODPRES where gprestac = '" + Codpresta + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlDCODPRES, Module1.RefRc);
			DataSet rsDCODPRES = new DataSet();
			tempAdapter.Fill(rsDCODPRES);
			
			bProtocoloCerrado = (String.Equals(Convert.ToString(rsDCODPRES.Tables[0].Rows[0]["iprotcer"]), "C", StringComparison.OrdinalIgnoreCase)); //iprotcer vale lo mismo para todos los registros devueltos

			string sqlprotocolo = "select dprotlab.gpruelab,dcodpres.dprestac " + 
			                      " from dprotlab, dcodpres " + 
			                      " where dprotlab.gprestac = '" + Codpresta + "' AND " + 
			                      "       dprotlab.gpruelab = dcodpres.gprestac ";

			//Oscar 02/07/2004
			//Al cargar las pruebas del protocolo, no se cargan las que se haya dado de baja
			if (!String.Equals(Module1.StFecha.Trim(), "", StringComparison.OrdinalIgnoreCase))
			{
				string tempRefParam = Module1.StFecha.Trim();
				string tempRefParam2 = Module1.StFecha.Trim();
				sqlprotocolo = sqlprotocolo + 
				               " and dcodpres.FINIVALI<=" + Serrores.FormatFecha(tempRefParam) + 
				               " and (dcodpres.FFINVALI>=" + Serrores.FormatFecha(tempRefParam2) + " OR dcodpres.FFINVALI is null) ";
			}
			else if (String.Equals(Module1.Todas.Trim().ToUpper(), "N", StringComparison.OrdinalIgnoreCase))
			{ 
				sqlprotocolo = sqlprotocolo + " and dcodpres.FFINVALI is null ";
			}
			//---------------

			sqlprotocolo = sqlprotocolo + " order by dcodpres.dprestac";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlprotocolo, Module1.RefRc);
			DataSet rrProtocolo = new DataSet();
			tempAdapter_2.Fill(rrProtocolo);
			SprProtocolo.MaxRows = 0;
			if (rrProtocolo.Tables[0].Rows.Count != 0)
			{				
				rrProtocolo.MoveFirst();
				foreach (DataRow iteration_row in rrProtocolo.Tables[0].Rows)
				{
					SprProtocolo.MaxRows++;
					SprProtocolo.Col = 1;
					//oscar 18/02/2004
					//SprProtocolo.Text = rrprotocolo("dprestac")
					SprProtocolo.CellType = 10;
					SprProtocolo.Col = 2;
					SprProtocolo.Row = SprProtocolo.MaxRows;
					SprProtocolo.Text = Convert.ToString(iteration_row["gpruelab"]);
					SprProtocolo.Col = 3;
					SprProtocolo.CellType = 5;
					SprProtocolo.Text = Convert.ToString(iteration_row["dprestac"]);
					//------------------
				}
				//oscar 18/02/2004
				if (String.Equals(Serrores.ObternerValor_CTEGLOBAL(Module1.RefRc, "MAPRULAB", "valfanu2").ToUpper(), "S", StringComparison.OrdinalIgnoreCase))
				{
					Option1[0].Visible = true;
					Option1[1].Visible = true;
				}
				else
				{
					Option1[0].Visible = false;
					Option1[1].Visible = false;
				}
			}
			else
			{
				Option1[0].Visible = false;
				Option1[1].Visible = false;
				//-------

			}			
			rrProtocolo.Close();

			//DIEGO 19-10-2006
			if (bProtocoloCerrado)
			{ //si el protocolo es cerrado, marco todas las pruebas y no dejo desmarcarlas
				Option1[0].IsChecked = true; //selecciono todas
				Option1[0].Enabled = false;
				Option1[1].Enabled = false; //deshabilito la posibilidad de desmarcar
				SprProtocolo.Col = 1; //me situo en la columna de los checkboxes
				SprProtocolo.Row = -1;
				SprProtocolo.Lock = true;

			}
			else
			{
				//si el protocolo no es cerrado, uso con normalidad el marcar pruebas o no dependiendo de la cte. global, como hasta ahora

				SprProtocolo.Col = 1; //me situo en la columna de los checkboxes
				SprProtocolo.Row = -1;
				SprProtocolo.Lock = false;
				Option1[0].Enabled = true; //habilito la posibilidad de desmarcar
				Option1[1].Enabled = true; //habilito la posibilidad de desmarcar

				//DIEGO 01-06-2006
				if (noMarcarSubpruebas)
				{
					Option1[1].IsChecked = true;
					SprProtocolo.Col = 1;
					SprProtocolo.Row = -1;
					SprProtocolo.Value = false;
				}
				else
				{
					Option1[0].IsChecked = true; //oscar 18/02/2004
				}
			}
		}

		private void SprPrestaciones_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;

			if (SprPrestaciones.MaxRows != 0 && Shift == 0)
			{

				if (KeyCode == ((int) Keys.Up) && SprPrestaciones.ActiveRowIndex != 1)
				{
					if (SprPrestaciones.ActiveRowIndex > 1)
					{
						Module1.CambiaFilaSeleccionada2(SprPrestaciones.ActiveRowIndex - 1);
						if (SprPrestaciones.Row != 0)
						{
							CargarProtocolo(SprPrestaciones.ActiveRowIndex - 1);
						}

						if (SprPrestaciones.Row != 0 && FrmTipoMuestra.Visible)
						{
							CargarTiposMuestra(SprPrestaciones.ActiveRowIndex - 1);
						}
						if (String.Equals(strValorCTE, "N", StringComparison.OrdinalIgnoreCase) || String.Equals(Module1.stItiposer, "", StringComparison.OrdinalIgnoreCase))
						{
							CB_Aceptar.Enabled = true;
						}

						if (String.Equals(strValorCTE, "S", StringComparison.OrdinalIgnoreCase))
						{
							SprProtocolo.Enabled = true;
						}
						if (!Module1.fSeleccionPruebas)
						{
							Option1[0].Visible = false;
							Option1[1].Visible = false;
							SprProtocolo.Col = 1;
							SprProtocolo.SetColHidden(SprProtocolo.Col, true);

						}
					}
				}
				else if (KeyCode == ((int) Keys.Down) && SprPrestaciones.ActiveRowIndex != SprPrestaciones.MaxRows)
				{ 
					if (SprPrestaciones.ActiveRowIndex < SprPrestaciones.Rows.Count)//NonEmptyItemFlag.Data))
					{
						Module1.CambiaFilaSeleccionada2(SprPrestaciones.ActiveRowIndex + 1);
						if (SprPrestaciones.Row != 0)
						{
							CargarProtocolo(SprPrestaciones.ActiveRowIndex + 1);
						}

						if (SprPrestaciones.Row != 0 && FrmTipoMuestra.Visible)
						{
							CargarTiposMuestra(SprPrestaciones.ActiveRowIndex + 1);
						}
						if (String.Equals(strValorCTE, "N", StringComparison.OrdinalIgnoreCase) || String.Equals(Module1.stItiposer, "", StringComparison.OrdinalIgnoreCase))
						{
							CB_Aceptar.Enabled = true;
						}

						if (String.Equals(strValorCTE, "S", StringComparison.OrdinalIgnoreCase))
						{
							SprProtocolo.Enabled = true;
						}

						if (!Module1.fSeleccionPruebas)
						{
							Option1[0].Visible = false;
							Option1[1].Visible = false;
							SprProtocolo.Col = 1;
							SprProtocolo.SetColHidden(SprProtocolo.Col, true);

						}
					}
				}

			}

		}

        private void SprPrestaciones_TopLeftChange(object eventSender, UpgradeHelpers.Spread.TopLeftChangeEventArgs eventArgs)
        {
            if (!Module1.Pulsado)
            {
                if (Module1.CargarMas)
                {
                    if (eventArgs.NewTop >= Module1.Tope)
                    {
                        SprPrestaciones.Row = SprPrestaciones.MaxRows;
                        SprPrestaciones.Col = 1;
                        Module1.Diagnostico = SprPrestaciones.Text.Trim();
                        SprPrestaciones.Col = 2;
                        Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
                        SprPrestaciones.Col = 3;
                        Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
                        Module1.Rq.Parameters[0].Value = Module1.Diagnostico;
                        Module1.Rrs.Reset();
                        SqlDataAdapter tempAdapter = new SqlDataAdapter(Module1.Rq);
                        tempAdapter.Fill(Module1.Rrs);
                        if (Module1.Rrs.Tables[0].Rows.Count < Module1.MAXFILAS)
                        {
                            Module1.CargarMas = false;
                        }
                        Module1.RecorrerRegistros();
                        Module1.Tope = SprPrestaciones.MaxRows - Module1.FILAS_VISIBLES;
                    }
                    FRM_Servicio.Refresh();
                    FRM_Prestacion.Refresh();
                }
            }
            else
            {
                if (Module1.CargarMas2)
                {
                    if (eventArgs.NewTop >= Module1.Tope2)
                    {
                        SprPrestaciones.Row = SprPrestaciones.MaxRows;
                        SprPrestaciones.Col = 1;
                        Module1.Diagnostico = SprPrestaciones.Text.Trim();
                        SprPrestaciones.Col = 2;
                        Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
                        SprPrestaciones.Col = 3;
                        Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
                        Module1.Rq2.Parameters[0].Value = Module1.Diagnostico;

                        Module1.Rrs2.Reset();
                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Module1.Rq2);
                        tempAdapter_2.Fill(Module1.Rrs2);
                        if (Module1.Rrs2.Tables[0].Rows.Count < Module1.MAXFILAS)
                        {
                            Module1.CargarMas2 = false;
                        }
                        Module1.RecorrerRegistros2();
                        Module1.Tope2 = SprPrestaciones.MaxRows - Module1.FILAS_VISIBLES;
                    }
                    FRM_Servicio.Refresh();
                    FRM_Prestacion.Refresh();
                }
            }
        }

        private void sprSeleccionados_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = 0;
			int Row = 0;
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = sprSeleccionados.Col;
                Row = sprSeleccionados.Row;
            }

            string strClaveServicio = String.Empty;
			string strClavePrestacion = String.Empty;
			string strClavesPruebas = String.Empty;
			string strClavesTodasPruebas = String.Empty;
			if (String.Equals(strValorCTE.Trim(), "S", StringComparison.OrdinalIgnoreCase))
			{


				if (Row > 0)
				{
					sprSeleccionados.Row = Row;
					sprSeleccionados.Col = 5; //clave servicio
					strClaveServicio = sprSeleccionados.Text;
					sprSeleccionados.Col = 3; //clave prestaci�n
					strClavePrestacion = sprSeleccionados.Text;
					TB_CodPrestacion.Text = strClavePrestacion;
					sprSeleccionados.Col = 1; //descripci�n prestaci�n
					TB_DescPrestacion.Text = sprSeleccionados.Text;
					sprSeleccionados.Col = 4; //cadena con todas las claves de las pruebas
					strClavesTodasPruebas = sprSeleccionados.Text;
					FijarServicio(strClaveServicio);
					FijarPrestacion(strClavePrestacion);
					FijarPruebas(ref strClavesTodasPruebas);

					TB_CodPrestacion.Text = strClavePrestacion;
					sprSeleccionados.Col = 1; //descripci�n prestaci�n
					TB_DescPrestacion.Text = sprSeleccionados.Text;

				}

			}

		}

		private void FijarPruebas(ref string strClavesTodasPruebas)
		{

			int numeroFilas = 0;

			numeroFilas = SprProtocolo.MaxRows;

			for (int i = 1; i <= numeroFilas; i++)
			{
				SprProtocolo.Row = i;
				SprProtocolo.Col = 2;
				if ((strClavesTodasPruebas.IndexOf(SprProtocolo.Text, StringComparison.CurrentCultureIgnoreCase) + 1) != 0)
				{
					SprProtocolo.Col = 1;
					SprProtocolo.Value = true;
				}
				else
				{
					SprProtocolo.Col = 1;
					SprProtocolo.Value = false;
				}
			}
			SprProtocolo.Col = 1;
			SprProtocolo.Row = 1;
			SprProtocolo.Action = 0; //activate cell
			SprProtocolo.BlockMode = true;
			SprProtocolo.Enabled = false; //desactivo el grid para que no lo puedan modificar
			SprProtocolo.BlockMode = false;
			Option1[0].Enabled = false;
			Option1[1].Enabled = false;

		}

		private void FijarServicio(string strClaveServicio)
		{

			int numeroFilas = 0;

			numeroFilas = SprServicios.MaxRows;
			SprServicios.Col = 2;

			for (int i = 1; i <= numeroFilas; i++)
			{
				SprServicios.Row = i;
				if (String.Equals(SprServicios.Text, strClaveServicio, StringComparison.OrdinalIgnoreCase))
				{
					SprServicios.Col = 1;
					SprServicios.Action = 0; //activate cell
					break;
				}
			}
			SprServicios_CellClick(SprServicios, null);

		}

		private void FijarPrestacion(string strClavePrestacion)
		{

			int numeroFilas = 0;

			numeroFilas = SprPrestaciones.MaxRows;
			SprPrestaciones.Col = 4;

			for (int i = 1; i <= numeroFilas; i++)
			{
				SprPrestaciones.Row = i;
				if (String.Equals(SprPrestaciones.Text, strClavePrestacion, StringComparison.OrdinalIgnoreCase))
				{
					SprPrestaciones.Col = 1;
					SprPrestaciones.Action = 0; //activate cell
					SprPrestaciones.SelModeSelected = true;
					break;
				}
			}
			SprPrestaciones_CellClick(SprPrestaciones, null);

		}


		private void SprServicios_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = 0;
			int Row = 0;
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = SprServicios.Col;
                Row = SprServicios.Row;
            }
            if (SprServicios.MaxRows != 0)
			{

				//I.S.M. 23-Mayo-2.000. A�adido para que no recargue en la fila 0 (Cabecera)
				if (Row > 0)
				{
					SprProtocolo.MaxRows = 0;
					if (FrmTipoMuestra.Visible)
					{
						FrmTipoMuestra.Enabled = false;
						cbbTipoMuestra.Enabled = false;
						cbbTipoMuestra.Items.Clear();
					}
					Option1[0].Visible = false;
					Option1[1].Visible = false;

					Module1.CambiaFilaSeleccionada(SprServicios.ActiveRowIndex);

				} //I.S.M. 23-Mayo-2.000. A�adido. Fin del If ... Then

			}
		}

		private void SprServicios_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (SprServicios.MaxRows != 0)
			{
				if (KeyCode == ((int) Keys.Up) && SprServicios.ActiveRowIndex != 1)
				{
					SprServicios.Row = SprServicios.ActiveRowIndex - 1;
					Module1.CambiaFilaSeleccionada(SprServicios.ActiveRowIndex - 1);
				}
				else if (KeyCode == ((int) Keys.Down) && SprServicios.ActiveRowIndex != SprServicios.MaxRows)
				{ 
					SprServicios.Row = SprServicios.ActiveRowIndex + 1;
					Module1.CambiaFilaSeleccionada(SprServicios.ActiveRowIndex + 1);
				}
			}
		}


		private void TB_CodPrestacion_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void TB_DescPrestacion_Enter(Object eventSender, EventArgs eventArgs)
		{
			TB_DescPrestacion.SelectionStart = 0;
			TB_DescPrestacion.SelectionLength = Strings.Len(TB_DescPrestacion.Text);
		}

		private void TB_DescPrestacion_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 13)
			{
				Toolbar4_ButtonClick((ToolStripItem) Toolbar4.Items[0], new EventArgs());
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void TB_DescServicio_Enter(Object eventSender, EventArgs eventArgs)
		{
			TB_DescServicio.SelectionStart = 0;
			TB_DescServicio.SelectionLength = Strings.Len(TB_DescServicio.Text);
		}

		private void TB_DescServicio_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 13)
			{
				Toolbar2_ButtonClick((ToolStripItem) Toolbar2.Items[0], new EventArgs());
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void Toolbar2_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
			ToolStripItem Button = (ToolStripItem) eventSender;
			bool Encontrado = false;
			if (SprServicios.MaxRows != 0)
			{
				int tempForVar = SprServicios.MaxRows;
				for (int Row = 1; Row <= tempForVar; Row++)
				{
					SprServicios.Row = Row;
					SprServicios.Col = 1;
					if (StringsHelper.Like(SprServicios.Text.Trim(), TB_DescServicio.Text.Trim() + "*"))
					{
						SprServicios.Col = 1;
						TB_DescServicio.Text = SprServicios.Text.Trim();
						SprServicios.Action = 0; //Activate a cell
						SprPrestaciones.MaxRows = 0;
						Module1.BuscarPrestaciones();
						Encontrado = true;
					}
				}
				if (!Encontrado)
				{
					//        Call MsgBox("No existe ning�n servicio que empiece por " & TB_DescServicio.Text)					
					Module1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1100, Module1.RefRc, FRM_Servicio.Text.Trim());
					TB_DescServicio.Text = "";
				}
			}
		}

		private void Toolbar3_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
			ToolStripItem Button = (ToolStripItem) eventSender;
			int Row = 0;
			if (!String.Equals(TB_CodPrestacion.Text, "", StringComparison.OrdinalIgnoreCase))
			{
				comando = "Select DPRESTAC,GPRESTAC,GSERVICI, GAGRUMUE from DCODPRES where GPRESTAC=";
				comando = comando + "'" + TB_CodPrestacion.Text.Trim() + "'";
				//*******************************************************
				if (!String.Equals(Module1.StFecha.Trim(), "", StringComparison.OrdinalIgnoreCase))
				{
					string tempRefParam = Module1.StFecha.Trim();
					comando = comando + " and FINIVALI<=" + "" + Serrores.FormatFecha (tempRefParam) + "";
					string tempRefParam2 = Module1.StFecha.Trim();
					comando = comando + " and (FFINVALI>=" + "" + Serrores.FormatFecha(tempRefParam2) + "";
					comando = comando + " OR FFINVALI is null) ";
				}
				else if (String.Equals(Module1.Todas.Trim().ToUpper(), "N", StringComparison.OrdinalIgnoreCase))
				{ 
					comando = comando + " and FFINVALI is null ";
				}
				if (chkServicioSeleccionado.CheckState != CheckState.Unchecked)
				{
					SprServicios.Row = SprServicios.ActiveRowIndex; //o SprServicios.SelModeIndex
					SprServicios.Col = 2;
					comando = comando + " and GSERVICI=" + SprServicios.Text.Trim();
				}
				else
				{
					comando = comando + " and ( ";
					int tempForVar = Module1.FrForm.SprServicios.MaxRows;
					for (int iCont = 1; iCont <= tempForVar; iCont++)
					{
						Module1.FrForm.SprServicios.Row = iCont;
						Module1.FrForm.SprServicios.Col = 2;
						comando = comando + " gservici = " + Module1.FrForm.SprServicios.Text + " or ";
					}
					comando = comando.Substring(0, Math.Min(comando.Length - 4, comando.Length)) + " ) ";
				}

				//DIEGO 29/11/2006
				//Se muestran s�lo las prestaciones disponibles para el tipo de servicio dado
				switch(Module1.stItiposer)
				{
					case "H" : 
						comando = comando + " and iareahos = 'S'"; 
						break;
					case "C" : 
						comando = comando + " and iareacon = 'S'"; 
						break;
					case "Q" : 
						comando = comando + " and iareaqui = 'S'"; 
						break;
					case "U" : 
						comando = comando + " and iareaurg = 'S'"; 
						break;
				}

				//*******************************************************
				SqlDataAdapter tempAdapter = new SqlDataAdapter(comando, Module1.RefRc);
				Module1.Rrs2 = new DataSet();
				tempAdapter.Fill(Module1.Rrs2);
				if (Module1.Rrs2.Tables[0].Rows.Count != 0)
				{
					SprPrestaciones.MaxRows = 1;
					SprPrestaciones.Row = SprPrestaciones.MaxRows;
					SprPrestaciones.Col = 1;
					//SprPrestaciones.Text = StrConv(Trim(Rrs2("DPRESTAC")), vbProperCase)					
					SprPrestaciones.Text = Convert.ToString(Module1.Rrs2.Tables[0].Rows[0]["DPRESTAC"]).Trim();
					Module1.Diagnostico = SprPrestaciones.Text.Trim();
					SprPrestaciones.Col = 2;
					Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
					SprPrestaciones.Col = 3;
					Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
					TB_DescPrestacion.Text = Module1.Diagnostico.Trim();
					SprPrestaciones.Col = 4;					
					SprPrestaciones.Text = Convert.ToString(Module1.Rrs2.Tables[0].Rows[0]["GPRESTAC"]).Trim();
					TB_CodPrestacion.Text = SprPrestaciones.Text.Trim();
					SprPrestaciones.Col = 5;					
					SprPrestaciones.Text = Convert.ToString(Module1.Rrs2.Tables[0].Rows[0]["GSERVICI"]);
					SprPrestaciones.Col = 6;					
					SprPrestaciones.Text = Convert.ToString(Module1.Rrs2.Tables[0].Rows[0]["GAGRUMUE"]) + "";

					SprServicios.Col = 2;
					int tempForVar2 = SprServicios.MaxRows;
					for (Row = 1; Row <= tempForVar2; Row++)
					{
						SprServicios.Row = Row;
						if (String.Equals(SprServicios.Text.Trim(), SprPrestaciones.Text.Trim(), StringComparison.OrdinalIgnoreCase))
						{
							SprServicios.Col = 1;
							SprServicios.Action = 0; //Activate a cell
							TB_DescServicio.Text = SprServicios.Text.Trim();
						}
					}
					SprPrestaciones.Row = 1;
					SprPrestaciones.Col = 1;
					SprPrestaciones.Action = 0; //Activate a cell
					Module1.Diagnostico = SprPrestaciones.Text.Trim();
					SprPrestaciones.Col = 2;
					Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
					SprPrestaciones.Col = 3;
					Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
					TB_DescPrestacion.Text = Module1.Diagnostico.Trim();
				}
				else
				{
					//        Call MsgBox("El c�digo de prestacion " & TB_CodPrestacion.Text & " no existe")					
					Module1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1100, Module1.RefRc, FRM_Prestacion.Text.Trim());
					TB_CodPrestacion.Text = "";
					TB_DescPrestacion.Text = "";
				}
				
				Module1.Rrs2.Close();
				FRM_Servicio.Refresh();
				FRM_Prestacion.Refresh();
			}

		}
		private void Toolbar4_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
			ToolStripItem Button = (ToolStripItem) eventSender;
			int Row = 0;
			if (!String.Equals(TB_DescPrestacion.Text, "", StringComparison.OrdinalIgnoreCase))
			{
				Module1.Pulsado = true;
				comando = "Select DPRESTAC,GPRESTAC, GSERVICI, GAGRUMUE from DCODPRES ";
				comando = comando + "where DPRESTAC LIKE ";
				comando = comando + "'%" + TB_DescPrestacion.Text.Trim() + "%' ";

				//I.S.M. 22-Mayo-2.000. A�adido para buscar por Servicio seleccionado
				if (chkServicioSeleccionado.CheckState != CheckState.Unchecked)
				{
					SprServicios.Row = SprServicios.ActiveRowIndex; //o SprServicios.SelModeIndex
					SprServicios.Col = 2;
					comando = comando + " and GSERVICI=" + SprServicios.Text.Trim();
				}
				else
				{
					comando = comando + " and ( ";
					int tempForVar = Module1.FrForm.SprServicios.MaxRows;
					for (int iCont = 1; iCont <= tempForVar; iCont++)
					{
						Module1.FrForm.SprServicios.Row = iCont;
						Module1.FrForm.SprServicios.Col = 2;
						comando = comando + " gservici = " + Module1.FrForm.SprServicios.Text + " or ";
					}
					comando = comando.Substring(0, Math.Min(comando.Length - 4, comando.Length)) + " ) ";
				}

				comando = comando + " and DPRESTAC > @paramStr ";
				//*******************************************************
				if (!String.Equals(Module1.StFecha.Trim(), "", StringComparison.OrdinalIgnoreCase))
				{
					string tempRefParam = Module1.StFecha.Trim();
					comando = comando + " and FINIVALI<=" + "" + Serrores.FormatFecha(tempRefParam) + "";
					string tempRefParam2 = Module1.StFecha.Trim();
					comando = comando + " and (FFINVALI>=" + "" + Serrores.FormatFecha(tempRefParam2) + "";
					comando = comando + " OR FFINVALI is null) ";
				}
				else if (String.Equals(Module1.Todas.Trim().ToUpper(), "N", StringComparison.OrdinalIgnoreCase))
				{ 
					comando = comando + " and FFINVALI is null ";
				}
				//*******************************************************
				//DIEGO 29/11/2006
				//Se muestran s�lo las prestaciones disponibles para el tipo de servicio dado
				switch(Module1.stItiposer)
				{
					case "H" : 
						comando = comando + " and iareahos = 'S'"; 
						break;
					case "C" : 
						comando = comando + " and iareacon = 'S'"; 
						break;
					case "Q" : 
						comando = comando + " and iareaqui = 'S'"; 
						break;
					case "U" : 
						comando = comando + " and iareaurg = 'S'"; 
						break;
				}
				comando = comando + " order by DPRESTAC";
				if (Module1.CerrarConsulta)
				{
					//        Rrs2.Close
				}
				Module1.CerrarConsulta = true;				
				Module1.Rq2 = UpgradeStubs.System_Data_SqlClient_SqlConnection.CreateQuery(Module1.RefRc,"", comando);

				//   Set Rq2.ActiveConnection = RefRc
				Module1.Rq2.CommandText = comando;
                //camaya todo_X_5
				//Module1.Rq2.setMaxRows(Module1.MAXFILAS);
				SprPrestaciones.MaxRows = 0;

                Module1.Rq2.Parameters.Add("@paramStr", SqlDbType.VarChar);

				if (SprPrestaciones.MaxRows == 0)
				{
					Module1.Rq2.Parameters[0].Value = "";
				}
				else
				{
					SprPrestaciones.Row = SprPrestaciones.MaxRows;
					SprPrestaciones.Col = 1;
					Module1.Diagnostico = SprPrestaciones.Text.Trim();
					SprPrestaciones.Col = 2;
					Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
					SprPrestaciones.Col = 3;
					Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
					Module1.Rq2.Parameters[0].Value = Module1.Diagnostico;
				}
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Module1.Rq2);
				Module1.Rrs2 = new DataSet();
				tempAdapter.Fill(Module1.Rrs2, 0, Module1.MAXFILAS, "Table1");
				if (Module1.Rrs2.Tables[0].Rows.Count == Module1.MAXFILAS)
				{
					Module1.CargarMas2 = true;
					Module1.Tope2 = Module1.Rrs2.Tables[0].Rows.Count - Module1.FILAS_VISIBLES;
				}
				else if (Module1.Rrs2.Tables[0].Rows.Count < Module1.MAXFILAS)
				{ 
					Module1.CargarMas2 = false;
				}
				Module1.RecorrerRegistros2();
				if (Module1.Rrs2.Tables[0].Rows.Count != 0)
				{
					SprPrestaciones.Row = 1;
					SprPrestaciones.Col = 5;
					SprServicios.Col = 2;
					int tempForVar2 = SprServicios.MaxRows;
					for (Row = 1; Row <= tempForVar2; Row++)
					{
						SprServicios.Row = Row;
						if (String.Equals(SprServicios.Text.Trim(), SprPrestaciones.Text.Trim(), StringComparison.OrdinalIgnoreCase))
						{
							SprServicios.Col = 1;
							SprServicios.Action = 0; //Activate a cell
							TB_DescServicio.Text = SprServicios.Text.Trim();
						}
					}
					SprPrestaciones.Col = 1;
					SprPrestaciones.Action = 0; //Activate a cell
					Module1.Diagnostico = SprPrestaciones.Text.Trim();
					SprPrestaciones.Col = 2;
					Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
					SprPrestaciones.Col = 3;
					Module1.Diagnostico = Module1.Diagnostico + SprPrestaciones.Text.Trim();
					TB_DescPrestacion.Text = Module1.Diagnostico.Trim();
					SprPrestaciones.Col = 4;
					TB_CodPrestacion.Text = SprPrestaciones.Text;
				}
				FRM_Servicio.Refresh();
				FRM_Prestacion.Refresh();
			}
		}
		private int proPrimeraFilaSeleccionada()
		{
			int result = 0;
			int tempForVar = SprPrestaciones.MaxRows;
			for (int ibucle = 1; ibucle <= tempForVar; ibucle++)
			{
				SprPrestaciones.Row = ibucle;
				if (SprPrestaciones.SelModeSelected)
				{
					result = ibucle;
					break;
				}
			}
			return result;
		}

		private int proUltimaFilaSeleccionada()
		{
			int result = 0;
			for (int ibucle2 = SprPrestaciones.MaxRows; ibucle2 >= 1; ibucle2--)
			{ // empieza a mirar a partir de la siguiente fila
				SprPrestaciones.Row = ibucle2;
				if (SprPrestaciones.SelModeSelected)
				{ // si est� seleccionada miro la siguiente por si lo est� tambien
					result = ibucle2;
					break;
				}
			}
			return result;
		}

		//oscar 18/02/2004
		private bool SoyProtocolo(string Codpresta)
		{
			bool result = false;
			string sqlprotocolo = "select * from dprotlab where dprotlab.gprestac='" + Codpresta + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlprotocolo, Module1.RefRc);
			DataSet rrProtocolo = new DataSet();
			tempAdapter.Fill(rrProtocolo);
			if (rrProtocolo.Tables[0].Rows.Count != 0)
			{
				result = true;
			}			
			rrProtocolo.Close();
			return result;
		}
		//------------------
	}
}