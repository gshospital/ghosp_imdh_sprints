using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace BuscaSelecPrestacionesDLL
{
	public partial class HCE310F3
		: Telerik.WinControls.UI.RadForm
	{

		public HCE310F3()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}

		private void HCE310F3_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		string comando = String.Empty;
		DataSet rr = null;
		bool noMarcarSubpruebas = false;

		//Dim intServLabo As Long
		//Dim DescServLabo As String

		private void CB_Aceptar_Click(Object eventSender, EventArgs eventArgs)
		{

			if (sprPetitorio.MaxRows == 0)
			{
				MessageBox.Show("El petitorio debe estar compuesto por al menos una prueba.", "Selecci�n de pruebas en el petitorio", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			//Cogemos la prestacion generica De PETICIONES DE LABORATORIO
			//comando = "select VALFANU1 from sconsglo where gconsglo = 'PRESPROD'"
			comando = "select gprestac from DOPENSER where gservici = " + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(comando, Module1.RefRc);
			rr = new DataSet();
			tempAdapter.Fill(rr);
		
			string stPrestacionGenerica = Convert.ToString(rr.Tables[0].Rows[0][0]).Trim().ToUpper();
			
			rr.Close();

			//Cogemos la descripcion de la prestacion generica De PETICIONES DE LABORATORIO
			comando = "select dprestac from dcodpres where gprestac='" + stPrestacionGenerica + "'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(comando, Module1.RefRc);
			rr = new DataSet();
			tempAdapter_2.Fill(rr);
			
			string stDescPrestacionGenerica = Convert.ToString(rr.Tables[0].Rows[0][0]).Trim().ToUpper();
		
			rr.Close();

            string[]  ArrayPrestacionPetitorio = new string[]{String.Empty};
			Module1.ArrayPrestacionPetitorio[0] = stPrestacionGenerica + "@" + stDescPrestacionGenerica.Trim();

            string[] ArrayDatosPruebasPetitorio = new string[]{String.Empty};
			int tempForVar = sprPetitorio.MaxRows;
			for (int i = 1; i <= tempForVar; i++)
			{
				sprPetitorio.Col = 1;
				sprPetitorio.Row = i;
				bool tempBool = false;
                string auxVar = String.Empty;
                auxVar = Convert.ToString(sprPetitorio.Value);				
				if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
				{
					sprPetitorio.Col = 3;
					ArrayDatosPruebasPetitorio = ArraysHelper.RedimPreserve(ArrayDatosPruebasPetitorio, new int[]{Module1.ArrayDatosPruebasPetitorio.GetUpperBound(0) + 2});
					Module1.ArrayDatosPruebasPetitorio[Module1.ArrayDatosPruebasPetitorio.GetUpperBound(0)] = sprPetitorio.Text;
				}
			}

			Module1.bSeHaAceptadoPetitorio = true;

			this.Close();

		}

		private void CB_cancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbbServicio_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			this.Text = "Petitiorio de Laboratorio - HCE310F3: " + cbbServicio.Text + " (" + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString() + ")";

			LLenarPlantillas();
			LlenarPrestacionesLab();

			SprPlantillasLab_CellClick(SprPlantillasLab, null);

			SprProtocolo.MaxRows = 0;
			sprPetitorio.MaxRows = 0;
		}

		private void cmdA�adirPruebas_Click(Object eventSender, EventArgs eventArgs)
		{
			A�adirAlPetitorio(sprPrestacionesLab);
		}

		private void cmdA�adirPruebasPlan_Click(Object eventSender, EventArgs eventArgs)
		{
			A�adirAlPetitorio(SprProtocolo);
		}
		
		private void HCE310F3_Load(Object eventSender, EventArgs eventArgs)
		{
			LLenarServiciosLaboratorio();

			//DIEGO 21-05-2006

			noMarcarSubpruebas = false;

			if (Serrores.ObternerValor_CTEGLOBAL(Module1.RefRc, "MAPRULAB", "valfanu1") == "N")
			{
				noMarcarSubpruebas = true;
			}
			else
			{
				//si es S, o no existe, o cualuier otro valos, ponemos por defecto que las marque.
				noMarcarSubpruebas = false;
			}


		}
		private void LLenarServiciosLaboratorio()
		{
			cbbServicio.Items.Clear();
			comando = "SELECT gservici, dnomserv FROM dserviciva WHERE iservlab = 'S' ORDER BY dnomserv";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(comando, Module1.RefRc);
			rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{				
				rr.MoveFirst();
				for (int i = 0; i <= rr.Tables[0].Rows.Count - 1; i++)
				{					
					cbbServicio.AddItem(Convert.ToString(rr.Tables[0].Rows[i][1]), i);					
					cbbServicio.SetItemData(i, Convert.ToInt32(rr.Tables[0].Rows[i]["gservici"]));					
					rr.MoveNext();
				}
			}
			if (rr.Tables[0].Rows.Count == 1)
			{
				cbbServicio.SelectedIndex = 0;
			}
			else
			{
				cbbServicio.SelectedIndex = -1;
			}

		}

		private void LlenarPrestacionesLab()
		{
			//Las prestaciones de laboratorio se llenaran con las prestaciones de laboratorio de analisis clinicos
			//que sean "padres" o sea que no formen un protocolo de laboratorio.
			sprPrestacionesLab.MaxRows = 0;
			comando = "Select DPRESTAC, GPRESTAC from DCODPRES where GSERVICI=" + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString();

			if (Module1.StFecha.Trim() != "")
			{
				string tempRefParam = Module1.StFecha.Trim();
				comando = comando + " and FINIVALI<=" + Serrores.FormatFecha(tempRefParam) + "";
				string tempRefParam2 = Module1.StFecha.Trim();
				comando = comando + " and (FFINVALI>=" + Serrores.FormatFecha(tempRefParam2) + " OR FFINVALI is null) ";
			}
			else if (Module1.Todas.Trim().ToUpper() == "N")
			{ 
				comando = comando + " and FFINVALI is null ";
			}

			comando = comando + " and gprestac NOT IN (select gprestac FROM DPROTLAB)";

			//DIEGO 21/05/2007
			//Se muestran s�lo las prestaciones disponibles para el tipo de servicio dado
			switch(Module1.stItiposer)
			{
				case "H" : 
					comando = comando + " and iareahos = 'S'"; 
					break;
				case "C" : 
					comando = comando + " and iareacon = 'S'"; 
					break;
				case "Q" : 
					comando = comando + " and iareaqui = 'S'"; 
					break;
				case "U" : 
					comando = comando + " and iareaurg = 'S'"; 
					break;
			}

			comando = comando + " order by DPRESTAC";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(comando, Module1.RefRc);
			rr = new DataSet();
			tempAdapter.Fill(rr);

			foreach (DataRow iteration_row in rr.Tables[0].Rows)
			{
				sprPrestacionesLab.MaxRows++;
				sprPrestacionesLab.Row = sprPrestacionesLab.MaxRows;

                sprPrestacionesLab.Col = 1;               
				sprPrestacionesLab.CellType = 10;

				sprPrestacionesLab.Col = 2;
				
				sprPrestacionesLab.setTypeEditLen(200);
				sprPrestacionesLab.Text = Convert.ToString(iteration_row[0]).Trim().ToUpper();
				sprPrestacionesLab.Lock = true;

				sprPrestacionesLab.Col = 3;
				sprPrestacionesLab.Text = Convert.ToString(iteration_row[1]).Trim().ToUpper();

			}
			
			rr.Close();

		}

		private void LLenarPlantillas()
		{
			//Las plantillas se llenaran con los protocolos de laboratorio
			SprPlantillasLab.MaxRows = 0;
			comando = " select distinct dcodpres.DPRESTAC, dprotlab.GPRESTAC a " + 
			          " from dprotlab " + 
			          " inner join dcodpres on dprotlab.gprestac=dcodpres.gprestac " + 
			          " where dcodpres.GSERVICI=" + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString();

			if (Module1.StFecha.Trim() != "")
			{
				string tempRefParam = Module1.StFecha.Trim();
				comando = comando + " and dcodpres.FINIVALI<=" + Serrores.FormatFecha(tempRefParam) + "";
				string tempRefParam2 = Module1.StFecha.Trim();
				comando = comando + " and (dcodpres.FFINVALI>=" + Serrores.FormatFecha(tempRefParam2) + " OR dcodpres.FFINVALI is null) ";
			}
			else if (Module1.Todas.Trim().ToUpper() == "N")
			{ 
				comando = comando + " and dcodpres.FFINVALI is null ";
			}

			//DIEGO 21/05/2007
			//Se muestran s�lo las prestaciones disponibles para el tipo de servicio dado
			switch(Module1.stItiposer)
			{
				case "H" : 
					comando = comando + " and iareahos = 'S'"; 
					break;
				case "C" : 
					comando = comando + " and iareacon = 'S'"; 
					break;
				case "Q" : 
					comando = comando + " and iareaqui = 'S'"; 
					break;
				case "U" : 
					comando = comando + " and iareaurg = 'S'"; 
					break;
			}

			comando = comando + " order by dcodpres.DPRESTAC";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(comando, Module1.RefRc);
			rr = new DataSet();
			tempAdapter.Fill(rr);

			foreach (DataRow iteration_row in rr.Tables[0].Rows)
			{
				SprPlantillasLab.MaxRows++;
				SprPlantillasLab.Row = SprPlantillasLab.MaxRows;

				SprPlantillasLab.Col = 1;
				//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprPlantillasLab.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				SprPlantillasLab.setTypeEditLen(200);
				SprPlantillasLab.Text = Convert.ToString(iteration_row[0]).Trim().ToUpper();

				SprPlantillasLab.Col = 2;
				SprPlantillasLab.Text = Convert.ToString(iteration_row[1]).Trim().ToUpper();

			}			
			rr.Close();

		}

		private void LlenarProtocolos(string Codpresta)
		{

			//DIEGO 21/05/2007 - SE A�ADE EL TEMA DE LOS PROTOCOLOS ABIERTOS Y CERRADOS

			bool bProtocoloCerrado = false;

			string sqlDCODPRES = "SELECT iprotcer from DCODPRES where gprestac = '" + Codpresta + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlDCODPRES, Module1.RefRc);
			DataSet rsDCODPRES = new DataSet();
			tempAdapter.Fill(rsDCODPRES);
			if (rsDCODPRES.Tables[0].Rows.Count != 0)
			{				
				bProtocoloCerrado = (Convert.ToString(rsDCODPRES.Tables[0].Rows[0]["iprotcer"]) == "C"); //iprotcer vale lo mismo para todos los registros devueltos
			}

			comando = "select dcodpres.dprestac,dprotlab.gpruelab " + 
			          " from dprotlab, dcodpres " + 
			          " where dprotlab.gprestac = '" + Codpresta + "' AND " + 
			          "       dprotlab.gpruelab = dcodpres.gprestac AND " + 
			          "       dcodpres.GSERVICI=" + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString();

			if (Module1.StFecha.Trim() != "")
			{
				string tempRefParam = Module1.StFecha.Trim();
				string tempRefParam2 = Module1.StFecha.Trim();
				comando = comando + 
				          " and dcodpres.FINIVALI<=" + Serrores.FormatFecha(tempRefParam) + 
				          " and (dcodpres.FFINVALI>=" + Serrores.FormatFecha(tempRefParam2) + " OR dcodpres.FFINVALI is null) ";
			}
			else if (Module1.Todas.Trim().ToUpper() == "N")
			{ 
				comando = comando + " and dcodpres.FFINVALI is null ";
			}

			//DIEGO 21/05/2007
			//Se muestran s�lo las prestaciones disponibles para el tipo de servicio dado
			switch(Module1.stItiposer)
			{
				case "H" : 
					comando = comando + " and iareahos = 'S'"; 
					break;
				case "C" : 
					comando = comando + " and iareacon = 'S'"; 
					break;
				case "Q" : 
					comando = comando + " and iareaqui = 'S'"; 
					break;
				case "U" : 
					comando = comando + " and iareaurg = 'S'"; 
					break;
			}

			comando = comando + " order by dcodpres.dprestac";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(comando, Module1.RefRc);
			rr = new DataSet();
			tempAdapter_2.Fill(rr);
			SprProtocolo.MaxRows = 0;

			if (rr.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in rr.Tables[0].Rows)
				{
					SprProtocolo.MaxRows++;
					SprProtocolo.Row = SprProtocolo.MaxRows;

					SprProtocolo.Col = 1;                    
                    SprProtocolo.CellType = 10;
					SprProtocolo.Col = 2;
					
					SprProtocolo.setTypeEditLen(200);
					SprProtocolo.Text = Convert.ToString(iteration_row[0]).Trim().ToUpper();

					SprProtocolo.Col = 3;
					SprProtocolo.Text = Convert.ToString(iteration_row[1]).Trim().ToUpper();

				}
				
				rr.Close();

				//DIEGO 21-05-2007
				if (bProtocoloCerrado)
				{ //si el protocolo es cerrado, marco todas las pruebas y no dejo desmarcarlas
					Option1[0].IsChecked = true; //selecciono todas
					Option1[0].Enabled = false;
					Option1[1].Enabled = false; //deshabilito la posibilidad de desmarcar
					SprProtocolo.Col = 1; //me situo en la columna de los checkboxes
					SprProtocolo.Row = -1;
					SprProtocolo.Lock = true;

				}
				else
				{
					//si el protocolo no es cerrado, uso con normalidad el marcar pruebas o no dependiendo de la cte. global, como hasta ahora

					SprProtocolo.Col = 1; //me situo en la columna de los checkboxes
					SprProtocolo.Row = -1;
					SprProtocolo.Lock = false;
					Option1[0].Enabled = true; //habilito la posibilidad de desmarcar
					Option1[1].Enabled = true; //habilito la posibilidad de desmarcar

					//DIEGO 01-06-2006
					if (noMarcarSubpruebas)
					{
						Option1[1].IsChecked = true;
						SprProtocolo.Col = 1;
						SprProtocolo.Row = -1;
						SprProtocolo.Value = false;
					}
					else
					{
						Option1[0].IsChecked = true; //oscar 18/02/2004
					}
				}

				//Option1(0).Value = True 'DIEGO 21-05-2007

				Option1[0].Visible = true;
				Option1[1].Visible = true;

			}
			else
			{
				Option1[0].Visible = false;
				Option1[1].Visible = false;
			}

		}

		private bool isInitializingComponent;
		private void Option1_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.Option1, eventSender);
				int i = 0;
				bool marcado = false;
				switch(Index)
				{
					case 0 : case 2 : case 4 :  
						marcado = true; 
						break;
					case 1 : case 3 : case 5 :  
						marcado = false; 
						break;
				}
				switch(Index)
				{
					case 0 : case 1 : 
						SprProtocolo.Col = 1; 
						SprProtocolo.Row = -1; 
						SprProtocolo.Value = marcado; 
						break;
					case 2 : case 3 : 
						sprPrestacionesLab.Col = 1; 
						sprPrestacionesLab.Row = -1; 
						sprPrestacionesLab.Value = marcado; 
						break;
					case 4 : case 5 : 
						sprPetitorio.Col = 1; 
						sprPetitorio.Row = -1; 
						sprPetitorio.Value = marcado; 
						break;
				}
			}
		}


		private void SprPlantillasLab_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = 0;
			int Row = 0;
			SprPlantillasLab.Col = 2;
			SprPlantillasLab.Row = Row;
			if (SprPlantillasLab.Text != "")
			{
				LlenarProtocolos(SprPlantillasLab.Text);
			}
		}


		private void A�adirAlPetitorio(UpgradeHelpers.Spread.FpSpread SpreadAA�adir)
		{
			bool bA�adir = false;
			string Desc = String.Empty;
			string codigo = String.Empty;

			int tempForVar = SpreadAA�adir.MaxRows;
			for (int j = 1; j <= tempForVar; j++)
			{
				SpreadAA�adir.Row = j;
				SpreadAA�adir.Col = 1;
				bool tempBool = false;
                string auxVar = String.Empty;
                auxVar = Convert.ToString(SpreadAA�adir.Value);				
				if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
				{
					bA�adir = true;
					SpreadAA�adir.Col = 2;
					Desc = SpreadAA�adir.Text;
					SpreadAA�adir.Col = 3;
					codigo = SpreadAA�adir.Text;

					//Primero buscamos si la prueba ya se ha a�adido al petitorio
					int tempForVar2 = sprPetitorio.MaxRows;
					for (int i = 1; i <= tempForVar2; i++)
					{
						sprPetitorio.Row = i;
						sprPetitorio.Col = 3;
						if (sprPetitorio.Text.Trim().ToUpper() == codigo.Trim().ToUpper())
						{
							bA�adir = false;
							break;
						}
					}
					//Si no se ha a�adido, se a�ade al final y se reordena
					if (bA�adir)
					{
						sprPetitorio.MaxRows++;
						sprPetitorio.Row = sprPetitorio.MaxRows;

						sprPetitorio.Col = 1;                        
                        sprPetitorio.CellType = 10;
						sprPetitorio.Value = true;

						sprPetitorio.Col = 2;
						
						sprPetitorio.setTypeEditLen(200);
						sprPetitorio.Lock = true;
						sprPetitorio.Text = Desc.Trim().ToUpper();

						sprPetitorio.Col = 3;
						sprPetitorio.Text = codigo.Trim().ToUpper();
					}
				}
			}

			//Una vez a�adido al petitorio, lo ordenamos
			sprPetitorio.Row = 1;
			sprPetitorio.Col = 1;
			sprPetitorio.Row2 = sprPetitorio.MaxRows;
			sprPetitorio.Col2 = sprPetitorio.MaxCols;
			sprPetitorio.SortBy = 0;
			sprPetitorio.SetSortKey(1, 2);
			sprPetitorio.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants) 1);
			//sprPetitorio.Action = 25;

		}

		// DIEGO - 24/05/2007 - Se incluye esto para que recarge las prestaciones asociadas a una plantilla
		// cuando navegamos por el listado de plantillas usando el teclado
		private void SprPlantillasLab_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (SprPlantillasLab.MaxRows != 0)
			{
				if (KeyCode == ((int) Keys.Up) && SprPlantillasLab.ActiveRowIndex != 1)
				{
					SprPlantillasLab.Row = SprPlantillasLab.ActiveRowIndex - 1;
					SprPlantillasLab.Col = 2;
					LlenarProtocolos(SprPlantillasLab.Text);
				}
				else if (KeyCode == ((int) Keys.Down) && SprPlantillasLab.ActiveRowIndex != SprPlantillasLab.MaxRows)
				{ 
					SprPlantillasLab.Row = SprPlantillasLab.ActiveRowIndex + 1;
					SprPlantillasLab.Col = 2;
					LlenarProtocolos(SprPlantillasLab.Text);
				}
			}
		}
		private void HCE310F3_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}