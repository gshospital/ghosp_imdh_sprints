using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Microsoft.CSharp;

namespace BuscaSelecPrestacionesDLL
{
	internal static class Module1
	{        		
		//public static UpgradeStubs.RDO_rdoEnvironment Re = null;
		public static SqlConnection RefRc = null;
		public static DataSet Rrs = null;
		public static DataSet Rrs2 = null;
		public static SqlCommand Rq = null;
		public static SqlCommand Rq2 = null;
		public static string Todas = String.Empty;
		public static int MAXFILAS = 0;
		public static string Diagnostico = String.Empty;
		public static int Tope = 0;
		public static int Tope2 = 0;
		public static bool NuevaConsulta = false;
		public static bool CerrarConsulta = false;
		public static bool CargarMas = false;
		public static bool CargarMas2 = false;
		public static bool Pulsado = false;
		public static HCE310F2 FrForm = null;
		public static HCE310F3 FrFormP = null;
		public static string DesPrestacion = String.Empty;
		public static Mensajes.ClassMensajes ClassMensaje = null;
		public static string StFecha = String.Empty;
		public static bool fMultiseleccion = false;
		public static string CodElServicio = String.Empty;
		//DIEGO 27/10/2006 - LO QUITO DEL FORM Y LO METO AQU� PARA TENERLO DISPONIBLE SIEMPRE
		public static string stItiposer = String.Empty;

		//oscar 18/02/2004
		public static bool fSeleccionPruebas = false;
		//----

		//David 28/02/2008
		public static int iMostrarAnatomia = 0;
		public static int iMostrarBS = 0;
		//----

		//OSCAR C: PETITORIO
		public static bool bSeHaAceptadoPetitorio = false;
		public static string[] ArrayDatosPruebasPetitorio = null;
		public static string[] ArrayPrestacionPetitorio = null;
		//----------

		//DIEGO 15/11/2006
		//Public Const FILAS_VISIBLES = 25 'Filas visibles del grid, para la paginaci�n
		public static int FILAS_VISIBLES = 0;
		public static string stTipoMues = String.Empty;
		//Rub�n 07-10-2009


		internal static void BuscarPrestaciones()
		{
			if (NuevaConsulta)
			{				
				Rrs.Close();
				NuevaConsulta = false;
			}
			//FrForm.SprServicios.Row = FrForm.SprServicios.ActiveRow
			FrForm.SprServicios.Col = 2;
			string comando = "Select DPRESTAC,GPRESTAC,GSERVICI,GAGRUMUE from DCODPRES where GSERVICI=";
			comando = comando + FrForm.SprServicios.Text.Trim();
			//*******************************************************
			if (StFecha.Trim() != "")
			{
				string tempRefParam = StFecha.Trim();
				comando = comando + " and FINIVALI<=" + "" + Serrores.FormatFecha(tempRefParam) + "";
				string tempRefParam2 = StFecha.Trim();
				comando = comando + " and (FFINVALI>=" + "" + Serrores.FormatFecha(tempRefParam2) + "";
				comando = comando + " OR FFINVALI is null) ";
			}
			else if (Todas.Trim().ToUpper() == "N")
			{ 
				comando = comando + " and FFINVALI is null ";
			}
			//*******************************************************
			comando = comando + " and DPRESTAC > @paramStr";


			//DIEGO 27/10/2006
			//Se muestran s�lo las prestaciones disponibles para el tipo de servicio dado
			switch(stItiposer)
			{
				case "H" : 
					comando = comando + " and iareahos = 'S'"; 
					break;
				case "C" : 
					comando = comando + " and iareacon = 'S'"; 
					break;
				case "Q" : 
					comando = comando + " and iareaqui = 'S'"; 
					break;
				case "U" : 
					comando = comando + " and iareaurg = 'S'"; 
					break;
			}

			comando = comando + " order by DPRESTAC";
            //camaya todo_x_5			
			//Rq = UpgradeStubs.System_Data_SqlClient_SqlConnection.CreateQuery("", comando);

			//Set Rq.ActiveConnection = Nothing
			//Set Rq.ActiveConnection = RefRc
			Rq.CommandText = comando;
            Rq.Parameters.Add("@paramStr");
            //camaya todo_x_5
			//UPGRADE_ISSUE: (2064) RDO.rdoQuery property Rq.MaxRows was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//Rq.setMaxRows(MAXFILAS);
			if (FrForm.SprPrestaciones.MaxRows == 0)
			{
				Rq.Parameters[0].Value = "";
			}
			else
			{
				FrForm.SprPrestaciones.Row = FrForm.SprPrestaciones.MaxRows;
				FrForm.SprPrestaciones.Col = 1;
				Diagnostico = FrForm.SprPrestaciones.Text.Trim();
				FrForm.SprPrestaciones.Col = 2;
				Diagnostico = Diagnostico + FrForm.SprPrestaciones.Text.Trim();
				FrForm.SprPrestaciones.Col = 3;
				Diagnostico = Diagnostico + FrForm.SprPrestaciones.Text.Trim();
				Rq.Parameters[0].Value = Diagnostico;
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(Rq);
			Rrs = new DataSet();
			tempAdapter.Fill(Rrs);
			if (Rrs.Tables[0].Rows.Count == MAXFILAS)
			{
				CargarMas = true;
				Tope = Rrs.Tables[0].Rows.Count - FILAS_VISIBLES;
			}
			else if (Rrs.Tables[0].Rows.Count < MAXFILAS)
			{ 
				CargarMas = false;
			}
			RecorrerRegistros();
			//Rrs.Close
			FrForm.SprPrestaciones.Row = FrForm.SprPrestaciones.ActiveRowIndex;
			FrForm.SprPrestaciones.Col = 4;
			FrForm.TB_CodPrestacion.Text = FrForm.SprPrestaciones.Text.Trim();
			FrForm.SprPrestaciones.Col = 1;
			Diagnostico = FrForm.SprPrestaciones.Text.Trim();
			FrForm.SprPrestaciones.Col = 2;
			Diagnostico = Diagnostico + FrForm.SprPrestaciones.Text.Trim();
			FrForm.SprPrestaciones.Col = 3;
			Diagnostico = Diagnostico + FrForm.SprPrestaciones.Text.Trim();
			FrForm.TB_DescPrestacion.Text = Diagnostico.Trim();
			FrForm.FRM_Servicio.Refresh();
			FrForm.FRM_Prestacion.Refresh();
		}
		internal static void LlenarDatos(dynamic vForm, string stTipo)
		{
			string comando = "Select DISTINCT(A.DNOMSERV),B.GSERVICI,A.ICENTRAL from DSERVICI A,DCODPRES B ";
			//Si se trata de una operaci�n ambulante o de una consulta externa, cruzamos con DPERSERV para que s�lo salgan los servicios con personal asociados.
			if (stTipo.ToUpper() == "C" || stTipo.ToUpper() == "Q")
			{
				comando = comando + ", dperserv C ";
			}
			comando = comando + "where FBORRADO is null ";
			comando = comando + " and A.GSERVICI=B.GSERVICI ";

			//'O.Frias - 18/05/2009
			//'Incorporo DCODPRES.ffinvali IS NULL y el filtro segun el tipo de servicio
			comando = comando + " AND B.ffinvali IS NULL ";
			if (stTipo == "U")
			{
				comando = comando + " AND B.iareaurg = 'S' ";
			}
			else if (stTipo == "H")
			{ 
				comando = comando + " AND B.iareahos = 'S' ";
			}
			else if (stTipo == "C")
			{ 
				comando = comando + " AND B.iareacon = 'S' ";
			}
			else if (stTipo == "Q")
			{ 
				comando = comando + " AND B.iareaqui = 'S' ";
			}

			
			if (Convert.ToString(vForm.Name).ToUpper() == "QAQ330F1")
			{
				SqlDataAdapter tempAdapter = new SqlDataAdapter("select valfanu1 from sconsglo where gconsglo='QOTRPRES'", RefRc);
				Rrs = new DataSet();
				tempAdapter.Fill(Rrs);
				if (Rrs.Tables[0].Rows.Count != 0)
				{					
					if (Convert.ToString(Rrs.Tables[0].Rows[0]["valfanu1"]).Trim() == "N")
					{
						comando = comando + " and a.iquirofa = 'S' ";
					}
				}
				else
				{
					comando = comando + " and a.iquirofa = 'S' ";
				}
				
				Rrs.Close();
			}
			else if (Convert.ToString(vForm.Name).ToUpper() == "CAC520F1" || Convert.ToString(vForm.Name).ToUpper() == "CAC510F1")
			{ 
				//caso consultas, todos los de icentral='s'
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter("select valfanu2 from sconsglo where gconsglo='QOTRPRES'", RefRc);
				Rrs = new DataSet();
				tempAdapter_2.Fill(Rrs);
				if (Rrs.Tables[0].Rows.Count != 0)
				{					
					if (Convert.ToString(Rrs.Tables[0].Rows[0]["valfanu2"]).Trim() == "S")
					{
						if (CodElServicio != "")
						{
							comando = comando + " and (a.Icentral = 'S' or a.gservici=" + CodElServicio + ")";
						}
					}
					else
					{
						if (CodElServicio != "")
						{
							comando = comando + " and a.gservici=" + CodElServicio;
						}
					}
				}
				else
				{
					if (CodElServicio != "")
					{
						comando = comando + " and a.gservici=" + CodElServicio;
					}
				}
				
				Rrs.Close();
			}
			else if (Convert.ToString(vForm.Name).ToUpper() == "CAC520F1DLL")
			{  //MODIFICACION  CONSULTA DESDE RAYOS
				//caso consultas, todos los de icentral='s'
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter("select valfanu2 from sconsglo where gconsglo='QOTRPRES'", RefRc);
				Rrs = new DataSet();
				tempAdapter_3.Fill(Rrs);
				if (Rrs.Tables[0].Rows.Count != 0)
				{
					
					if (Convert.ToString(Rrs.Tables[0].Rows[0]["valfanu2"]).Trim() == "S")
					{
						if (CodElServicio != "")
						{
							comando = comando + " and (a.Icentral = 'S' or a.gservici=" + CodElServicio + ")";
						}
					}
					else
					{
						if (CodElServicio != "")
						{
							comando = comando + " and a.gservici=" + CodElServicio;
						}
					}
				}
				else
				{
					if (CodElServicio != "")
					{
						comando = comando + " and a.gservici=" + CodElServicio;
					}
				}
				
				Rrs.Close();
				comando = comando + " and a.iservlab<>'S' ";
			}
			else
			{
				if (CodElServicio != "")
				{
					comando = comando + " and a.gservici=" + CodElServicio;
				}
			}
			if (stTipo.ToUpper() == "C" || stTipo.ToUpper() == "Q")
			{
				comando = comando + " and A.GSERVICI=C.GSERVICI  ";
			}

			//OSCAR C (PETITORIO LABORATORIO)			
			if (fPetitorioLaboratorio() && Convert.ToString(vForm.Name) == "PRESTACIONES")
			{
				comando = comando + " AND A.iservlab<>'S'";
			}
			//--------------

			if (iMostrarAnatomia > -1)
			{
				comando = comando + " and A.gservici <> " + iMostrarAnatomia.ToString() + " ";
			}

			if (iMostrarBS > -1)
			{
				comando = comando + " and A.gservici <> " + iMostrarBS.ToString() + " ";
			}

			comando = comando + " order by a.icentral desc, a.dnomserv ";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(comando, RefRc);
			Rrs = new DataSet();
			tempAdapter_4.Fill(Rrs);
			foreach (DataRow iteration_row in Rrs.Tables[0].Rows)
			{
				FrForm.SprServicios.MaxRows++;
				FrForm.SprServicios.Row = FrForm.SprServicios.MaxRows;
				FrForm.SprServicios.Col = 1;
				FrForm.SprServicios.Text = Strings.StrConv(Convert.ToString(iteration_row[0]).Trim(), VbStrConv.ProperCase, 0);
				FrForm.SprServicios.Col = 2;
				FrForm.SprServicios.Text = Convert.ToString(iteration_row[1]).Trim();
			}
			
			Rrs.Close();
			FrForm.SprServicios.Row = FrForm.SprServicios.ActiveRowIndex;
			FrForm.SprServicios.Col = 1;
			FrForm.TB_DescServicio.Text = FrForm.SprServicios.Text.Trim();
			BuscarPrestaciones();
		}
		internal static void CambiaFilaSeleccionada(int Row)
		{
			FrForm.SprServicios.Row = Row;
			FrForm.SprServicios.Col = 1;
			FrForm.TB_DescServicio.Text = FrForm.SprServicios.Text.Trim();
			NuevaConsulta = true;
			CargarMas = false;
			FrForm.SprPrestaciones.MaxRows = 0;
			Pulsado = false;
			CerrarConsulta = false;
			BuscarPrestaciones();
		}
		internal static void CambiaFilaSeleccionada2(int Row)
		{
			FrForm.SprPrestaciones.Row = Row;
			FrForm.SprPrestaciones.Col = 4;
			FrForm.TB_CodPrestacion.Text = FrForm.SprPrestaciones.Text.Trim();
			FrForm.SprPrestaciones.Col = 1;
			Diagnostico = FrForm.SprPrestaciones.Text.Trim();
			FrForm.SprPrestaciones.Col = 2;
			Diagnostico = Diagnostico + FrForm.SprPrestaciones.Text.Trim();
			FrForm.SprPrestaciones.Col = 3;
			Diagnostico = Diagnostico + FrForm.SprPrestaciones.Text.Trim();
			FrForm.TB_DescPrestacion.Text = Diagnostico.Trim();
			FrForm.SprPrestaciones.Col = 5;
			FrForm.SprServicios.Col = 2;
			int tempForVar = FrForm.SprServicios.MaxRows;
			for (Row = 1; Row <= tempForVar; Row++)
			{
				FrForm.SprServicios.Row = Row;
				if (FrForm.SprServicios.Text.Trim() == FrForm.SprPrestaciones.Text.Trim())
				{
					FrForm.SprServicios.Col = 1;
					FrForm.SprServicios.Action = 0; //Activate a cell
					FrForm.TB_DescServicio.Text = FrForm.SprServicios.Text.Trim();
				}
			}
		}
		internal static void RecorrerRegistros2()
		{
			if (Rrs2.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in Rrs2.Tables[0].Rows)
				{
					FrForm.SprPrestaciones.MaxRows++;
					FrForm.SprPrestaciones.Row = FrForm.SprPrestaciones.MaxRows;
					FrForm.SprPrestaciones.Col = 1;
					//FrForm.SprPrestaciones.Text = IIf(StrComp(UCase(Trim(Rrs2("DPRESTAC"))), Trim(Rrs2("DPRESTAC"))) = 0, StrConv(Trim(Rrs2("DPRESTAC")), vbProperCase), Trim(Rrs2("DPRESTAC")))
					FrForm.SprPrestaciones.Text = Convert.ToString(iteration_row["DPRESTAC"]).Trim();
					FrForm.SprPrestaciones.Col = 4;
					FrForm.SprPrestaciones.Text = Convert.ToString(iteration_row["GPRESTAC"]).Trim();
					FrForm.SprPrestaciones.Col = 5;
					FrForm.SprPrestaciones.Text = Convert.ToString(iteration_row["GSERVICI"]).Trim();
					if (FrForm.FrmTipoMuestra.Visible)
					{
						FrForm.SprPrestaciones.Col = 6;
						FrForm.SprPrestaciones.Text = (Convert.ToString(iteration_row["GAGRUMUE"]) + "").Trim();
					}
				}
			}
			else
			{
				//    Call MsgBox("No existe ninguna prestaci�n que empiece por " & FrForm.TB_DescPrestacion.Text)				
				ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1100, RefRc, FrForm.FRM_Prestacion.Text.Trim());
				FrForm.TB_DescPrestacion.Text = "";
				FrForm.TB_CodPrestacion.Text = "";
			}
		}

		internal static void RecorrerRegistros()
		{
			foreach (DataRow iteration_row in Rrs.Tables[0].Rows)
			{
				FrForm.SprPrestaciones.MaxRows++;
				FrForm.SprPrestaciones.Row = FrForm.SprPrestaciones.MaxRows;
				FrForm.SprPrestaciones.Col = 1;
				//FrForm.SprPrestaciones.Text = IIf(StrComp(UCase(Trim(Rrs("DPRESTAC"))), Trim(Rrs("DPRESTAC"))) = 0, StrConv(Trim(Rrs("DPRESTAC")), vbProperCase), Trim(Rrs("DPRESTAC")))
				FrForm.SprPrestaciones.Text = Convert.ToString(iteration_row["DPRESTAC"]).Trim();
				FrForm.SprPrestaciones.Col = 4;
				FrForm.SprPrestaciones.Text = Convert.ToString(iteration_row["GPRESTAC"]).Trim();
				FrForm.SprPrestaciones.Col = 5;
				FrForm.SprPrestaciones.Text = Convert.ToString(iteration_row["GSERVICI"]).Trim();
				if (stTipoMues == "S")
				{
					FrForm.SprPrestaciones.Col = 6;
					FrForm.SprPrestaciones.Text = (Convert.ToString(iteration_row["GAGRUMUE"]) + "").Trim();
				}
			}
			FrForm.FRM_Servicio.Refresh();
			FrForm.FRM_Prestacion.Refresh();
		}

		//OSCAR C (PETITORIO LABORATORIO)
		internal static bool fPetitorioLaboratorio()
		{
			bool result = false;
			//Cogemos el valor de la constante PETILABO (PETITORIO LABORATORIO)
			string sql = "select VALFANU1 from sconsglo where gconsglo='PETILABO'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RefRc);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{				
				if (!Convert.IsDBNull(rr.Tables[0].Rows[0][0]))
				{					
					if (Convert.ToString(rr.Tables[0].Rows[0][0]).Trim().ToUpper() == "S")
					{
						result = true;
					}
				}
			}
			
			rr.Close();
			return result;
		}
		//---------------------
	}
}