using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BuscaSelecPrestacionesDLL
{
	partial class HCE310F2
	{

		#region "Upgrade Support "
		private static HCE310F2 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static HCE310F2 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new HCE310F2();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprProtocolo", "_Option1_0", "_Option1_1", "Frame1", "cmdPetitorio", "CB_Aceptar", "CB_cancelar", "SprServicios", "_Toolbar2_Button1", "Toolbar2", "TB_DescServicio", "Label2", "Frame4", "FRM_Servicio", "PIC_Prestaciones", "SprPrestaciones", "cbbTipoMuestra", "FrmTipoMuestra", "_Toolbar4_Button1", "Toolbar4", "_Toolbar3_Button1", "Toolbar3", "chkServicioSeleccionado", "TB_DescPrestacion", "TB_CodPrestacion", "Label4", "Label3", "Frame3", "FRM_Prestacion", "sprSeleccionados", "cmdAnadir", "cmdEliminar", "Frame2", "ImageList1", "Option1", "sprSeleccionados_Sheet1", "SprPrestaciones_Sheet1", "SprServicios_Sheet1", "SprProtocolo_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprProtocolo;
		private Telerik.WinControls.UI.RadRadioButton _Option1_0;
		private Telerik.WinControls.UI.RadRadioButton _Option1_1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cmdPetitorio;
		public Telerik.WinControls.UI.RadButton CB_Aceptar;
		public Telerik.WinControls.UI.RadButton CB_cancelar;
		public UpgradeHelpers.Spread.FpSpread SprServicios;
		private System.Windows.Forms.ToolStripButton _Toolbar2_Button1;
		public System.Windows.Forms.ToolStrip Toolbar2;
		public Telerik.WinControls.UI.RadTextBoxControl TB_DescServicio;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		public Telerik.WinControls.UI.RadGroupBox FRM_Servicio;
		public System.Windows.Forms.PictureBox PIC_Prestaciones;
		public UpgradeHelpers.Spread.FpSpread SprPrestaciones;
		public UpgradeHelpers.MSForms.MSCombobox cbbTipoMuestra;
		public Telerik.WinControls.UI.RadGroupBox FrmTipoMuestra;
		private System.Windows.Forms.ToolStripButton _Toolbar4_Button1;
		public System.Windows.Forms.ToolStrip Toolbar4;
		private System.Windows.Forms.ToolStripButton _Toolbar3_Button1;
		public System.Windows.Forms.ToolStrip Toolbar3;
		public Telerik.WinControls.UI.RadCheckBox chkServicioSeleccionado;
		public Telerik.WinControls.UI.RadTextBoxControl TB_DescPrestacion;
		public Telerik.WinControls.UI.RadTextBoxControl TB_CodPrestacion;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadGroupBox FRM_Prestacion;
		public UpgradeHelpers.Spread.FpSpread sprSeleccionados;
		public Telerik.WinControls.UI.RadButton cmdAnadir;
		public Telerik.WinControls.UI.RadButton cmdEliminar;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public System.Windows.Forms.ImageList ImageList1;
		public Telerik.WinControls.UI.RadRadioButton[] Option1 = new Telerik.WinControls.UI.RadRadioButton[2];
		//private FarPoint.Win.Spread.SheetView sprSeleccionados_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprPrestaciones_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprServicios_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SprProtocolo_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HCE310F2));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.SprProtocolo = new UpgradeHelpers.Spread.FpSpread();
			this._Option1_0 = new Telerik.WinControls.UI.RadRadioButton();
			this._Option1_1 = new Telerik.WinControls.UI.RadRadioButton();
			this.cmdPetitorio = new Telerik.WinControls.UI.RadButton();
			this.CB_Aceptar = new Telerik.WinControls.UI.RadButton();
			this.CB_cancelar = new Telerik.WinControls.UI.RadButton();
			this.FRM_Servicio = new Telerik.WinControls.UI.RadGroupBox();
			this.SprServicios = new UpgradeHelpers.Spread.FpSpread();
			this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
			this.Toolbar2 = new System.Windows.Forms.ToolStrip();
			this._Toolbar2_Button1 = new System.Windows.Forms.ToolStripButton();
			this.TB_DescServicio = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.PIC_Prestaciones = new System.Windows.Forms.PictureBox();
			this.FRM_Prestacion = new Telerik.WinControls.UI.RadGroupBox();
			this.SprPrestaciones = new UpgradeHelpers.Spread.FpSpread();
			this.FrmTipoMuestra = new Telerik.WinControls.UI.RadGroupBox();
			this.cbbTipoMuestra = new UpgradeHelpers.MSForms.MSCombobox();
			this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
			this.Toolbar4 = new System.Windows.Forms.ToolStrip();
			this._Toolbar4_Button1 = new System.Windows.Forms.ToolStripButton();
			this.Toolbar3 = new System.Windows.Forms.ToolStrip();
			this._Toolbar3_Button1 = new System.Windows.Forms.ToolStripButton();
			this.chkServicioSeleccionado = new Telerik.WinControls.UI.RadCheckBox();
			this.TB_DescPrestacion = new Telerik.WinControls.UI.RadTextBoxControl();
			this.TB_CodPrestacion = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.sprSeleccionados = new UpgradeHelpers.Spread.FpSpread();
			this.cmdAnadir = new Telerik.WinControls.UI.RadButton();
			this.cmdEliminar = new Telerik.WinControls.UI.RadButton();
			this.ImageList1 = new System.Windows.Forms.ImageList();
			this.Frame1.SuspendLayout();
			this.FRM_Servicio.SuspendLayout();
			this.Frame4.SuspendLayout();
			this.Toolbar2.SuspendLayout();
			this.FRM_Prestacion.SuspendLayout();
			this.FrmTipoMuestra.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.Toolbar4.SuspendLayout();
			this.Toolbar3.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
			// 
			// Frame1
			// 			
			this.Frame1.Controls.Add(this.SprProtocolo);
			this.Frame1.Controls.Add(this._Option1_0);
			this.Frame1.Controls.Add(this._Option1_1);
			this.Frame1.Enabled = true;			
			this.Frame1.Location = new System.Drawing.Point(4, 315);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(338, 243);
			this.Frame1.TabIndex = 19;
			this.Frame1.Visible = true;
			// 
			// SprProtocolo
			// 
			this.SprProtocolo.Location = new System.Drawing.Point(3, 12);
			this.SprProtocolo.Name = "SprProtocolo";
			this.SprProtocolo.Size = new System.Drawing.Size(331, 203);
			this.SprProtocolo.TabIndex = 20;
			// 
			// _Option1_0
			//			
			this._Option1_0.CausesValidation = true;			
			this._Option1_0.IsChecked = false;
			this._Option1_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_0.Enabled = true;
			this._Option1_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Option1_0.Location = new System.Drawing.Point(8, 221);
			this._Option1_0.Name = "_Option1_0";
			this._Option1_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_0.Size = new System.Drawing.Size(63, 15);
			this._Option1_0.TabIndex = 22;
			this._Option1_0.TabStop = true;
			this._Option1_0.Text = "Todos";
			this._Option1_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_0.Visible = true;
			this._Option1_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Option1_CheckedChanged);
			// 
			// _Option1_1
			// 			
			this._Option1_1.CausesValidation = true;			
			this._Option1_1.IsChecked = false;
			this._Option1_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._Option1_1.Enabled = true;			
			this._Option1_1.Location = new System.Drawing.Point(72, 221);
			this._Option1_1.Name = "_Option1_1";
			this._Option1_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Option1_1.Size = new System.Drawing.Size(63, 15);
			this._Option1_1.TabIndex = 21;
			this._Option1_1.TabStop = true;
			this._Option1_1.Text = "Ninguno";
			this._Option1_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._Option1_1.Visible = true;
			this._Option1_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Option1_CheckedChanged);
			// 
			// cmdPetitorio
			// 			
			this.cmdPetitorio.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cmdPetitorio.Location = new System.Drawing.Point(349, 540);
			this.cmdPetitorio.Name = "cmdPetitorio";
			this.cmdPetitorio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdPetitorio.Size = new System.Drawing.Size(129, 21);
			this.cmdPetitorio.TabIndex = 23;
			this.cmdPetitorio.Text = "Petitorio de Laboratorio";
			this.cmdPetitorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			
			this.cmdPetitorio.Visible = false;
			this.cmdPetitorio.Click += new System.EventHandler(this.cmdPetitorio_Click);
			// 
			// CB_Aceptar
			// 
			
			this.CB_Aceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CB_Aceptar.Enabled = false;			
			this.CB_Aceptar.Location = new System.Drawing.Point(605, 540);
			this.CB_Aceptar.Name = "CB_Aceptar";
			this.CB_Aceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CB_Aceptar.Size = new System.Drawing.Size(81, 21);
			this.CB_Aceptar.TabIndex = 17;
			this.CB_Aceptar.Text = "&Aceptar";
			this.CB_Aceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.CB_Aceptar.Click += new System.EventHandler(this.CB_Aceptar_Click);
			// 
			// CB_cancelar
			//			
			this.CB_cancelar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.CB_cancelar.Location = new System.Drawing.Point(696, 540);
			this.CB_cancelar.Name = "CB_cancelar";
			this.CB_cancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CB_cancelar.Size = new System.Drawing.Size(81, 21);
			this.CB_cancelar.TabIndex = 18;
			this.CB_cancelar.Text = "&Cancelar";
			this.CB_cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;			
			this.CB_cancelar.Click += new System.EventHandler(this.CB_cancelar_Click);
			// 
			// FRM_Servicio
			// 			
			this.FRM_Servicio.Controls.Add(this.SprServicios);
			this.FRM_Servicio.Controls.Add(this.Frame4);
			this.FRM_Servicio.Enabled = true;			
			this.FRM_Servicio.Location = new System.Drawing.Point(4, 6);
			this.FRM_Servicio.Name = "FRM_Servicio";
			this.FRM_Servicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FRM_Servicio.Size = new System.Drawing.Size(339, 309);
			this.FRM_Servicio.TabIndex = 1;
			this.FRM_Servicio.Text = "Servicio realizador";
			this.FRM_Servicio.Visible = true;
			// 
			// SprServicios
			// 
			this.SprServicios.Location = new System.Drawing.Point(8, 14);
			this.SprServicios.Name = "SprServicios";
			this.SprServicios.Size = new System.Drawing.Size(322, 242);
			this.SprServicios.TabIndex = 2;
			this.SprServicios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprServicios_CellClick);
			this.SprServicios.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SprServicios_KeyDown);
			// 
			// Frame4
			// 
			//this.Frame4.BackColor = System.Drawing.SystemColors.Control;
			this.Frame4.Controls.Add(this.Toolbar2);
			this.Frame4.Controls.Add(this.TB_DescServicio);
			this.Frame4.Controls.Add(this.Label2);
			this.Frame4.Enabled = true;
			this.Frame4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame4.Location = new System.Drawing.Point(7, 254);
			this.Frame4.Name = "Frame4";
			this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame4.Size = new System.Drawing.Size(325, 49);
			this.Frame4.TabIndex = 3;
			this.Frame4.Visible = true;
			// 
			// Toolbar2
			// 
			this.Toolbar2.Dock = System.Windows.Forms.DockStyle.None;
			this.Toolbar2.ImageList = ImageList1;
			this.Toolbar2.Location = new System.Drawing.Point(292, 14);
			this.Toolbar2.Name = "Toolbar2";
			this.Toolbar2.ShowItemToolTips = true;
			this.Toolbar2.Size = new System.Drawing.Size(25, 26);
			this.Toolbar2.TabIndex = 5;
			this.Toolbar2.Items.Add(this._Toolbar2_Button1);
			// 
			// _Toolbar2_Button1
			// 
			this._Toolbar2_Button1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar2_Button1.ImageIndex = 0;
			this._Toolbar2_Button1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._Toolbar2_Button1.Name = "";
			this._Toolbar2_Button1.Size = new System.Drawing.Size(24, 22);
			this._Toolbar2_Button1.Tag = "";
			this._Toolbar2_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar2_Button1.ToolTipText = "Buscar";
			this._Toolbar2_Button1.Click += new System.EventHandler(this.Toolbar2_ButtonClick);
			// 
			// TB_DescServicio
			// 
			this.TB_DescServicio.AcceptsReturn = true;
			//this.TB_DescServicio.BackColor = System.Drawing.SystemColors.Window;
			
			this.TB_DescServicio.Cursor = System.Windows.Forms.Cursors.IBeam;
			
			this.TB_DescServicio.Location = new System.Drawing.Point(67, 17);
			this.TB_DescServicio.MaxLength = 30;
			this.TB_DescServicio.Name = "TB_DescServicio";
			this.TB_DescServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TB_DescServicio.Size = new System.Drawing.Size(223, 21);
			this.TB_DescServicio.TabIndex = 0;
			this.TB_DescServicio.Enter += new System.EventHandler(this.TB_DescServicio_Enter);
			this.TB_DescServicio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TB_DescServicio_KeyPress);
			// 
			// Label2
			//			
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(3, 21);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(65, 17);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "Descripci�n:";
			// 
			// PIC_Prestaciones
			// 			
			this.PIC_Prestaciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.PIC_Prestaciones.CausesValidation = true;
			this.PIC_Prestaciones.Cursor = System.Windows.Forms.Cursors.Default;
			this.PIC_Prestaciones.Dock = System.Windows.Forms.DockStyle.None;
			this.PIC_Prestaciones.Enabled = true;
			this.PIC_Prestaciones.Image = (System.Drawing.Image) resources.GetObject("PIC_Prestaciones.Image");
			this.PIC_Prestaciones.Location = new System.Drawing.Point(314, 464);
			this.PIC_Prestaciones.Name = "PIC_Prestaciones";
			this.PIC_Prestaciones.Size = new System.Drawing.Size(38, 38);
			this.PIC_Prestaciones.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this.PIC_Prestaciones.TabIndex = 16;
			this.PIC_Prestaciones.TabStop = true;
			this.PIC_Prestaciones.Visible = false;
			// 
			// FRM_Prestacion
			// 			
			this.FRM_Prestacion.Controls.Add(this.SprPrestaciones);
			this.FRM_Prestacion.Controls.Add(this.FrmTipoMuestra);
			this.FRM_Prestacion.Controls.Add(this.Frame3);
			this.FRM_Prestacion.Enabled = true;			
			this.FRM_Prestacion.Location = new System.Drawing.Point(349, 6);
			this.FRM_Prestacion.Name = "FRM_Prestacion";
			this.FRM_Prestacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FRM_Prestacion.Size = new System.Drawing.Size(429, 531);
			this.FRM_Prestacion.TabIndex = 6;
			this.FRM_Prestacion.Text = "Prestaciones por servicio";
			this.FRM_Prestacion.Visible = true;
			// 
			// SprPrestaciones
			// 
			this.SprPrestaciones.Location = new System.Drawing.Point(4, 14);
			this.SprPrestaciones.Name = "SprPrestaciones";
			this.SprPrestaciones.Size = new System.Drawing.Size(421, 394);
			this.SprPrestaciones.TabIndex = 7;
			this.SprPrestaciones.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPrestaciones_CellClick);
			this.SprPrestaciones.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SprPrestaciones_KeyDown);
			this.SprPrestaciones.TopLeftChange += new UpgradeHelpers.Spread.TopLeftChangeEventHandler(this.SprPrestaciones_TopLeftChange);
			// 
			// FrmTipoMuestra
			//			
			this.FrmTipoMuestra.Controls.Add(this.cbbTipoMuestra);
			this.FrmTipoMuestra.Enabled = true;
			this.FrmTipoMuestra.ForeColor = System.Drawing.SystemColors.ControlText;
			this.FrmTipoMuestra.Location = new System.Drawing.Point(4, 407);
			this.FrmTipoMuestra.Name = "FrmTipoMuestra";
			this.FrmTipoMuestra.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmTipoMuestra.Size = new System.Drawing.Size(421, 38);
			this.FrmTipoMuestra.TabIndex = 24;
			this.FrmTipoMuestra.Text = "Tipos de Muestras";
			this.FrmTipoMuestra.Visible = true;
			// 
			// cbbTipoMuestra
			// 			
			this.cbbTipoMuestra.CausesValidation = true;
			this.cbbTipoMuestra.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbbTipoMuestra.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
			this.cbbTipoMuestra.Enabled = true;
			this.cbbTipoMuestra.ForeColor = System.Drawing.SystemColors.WindowText;			
			this.cbbTipoMuestra.Location = new System.Drawing.Point(7, 12);
			this.cbbTipoMuestra.Name = "cbbTipoMuestra";
			this.cbbTipoMuestra.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbbTipoMuestra.Size = new System.Drawing.Size(409, 21);			
			this.cbbTipoMuestra.TabIndex = 25;
			this.cbbTipoMuestra.TabStop = true;
			this.cbbTipoMuestra.Visible = true;
			// 
			// Frame3
			//			
			this.Frame3.Controls.Add(this.Toolbar4);
			this.Frame3.Controls.Add(this.Label4);
			this.Frame3.Controls.Add(this.TB_CodPrestacion);
			this.Frame3.Controls.Add(this.TB_DescPrestacion);
			this.Frame3.Controls.Add(this.chkServicioSeleccionado);
			this.Frame3.Controls.Add(this.Toolbar3);
			this.Frame3.Controls.Add(this.Label3);
			this.Frame3.Enabled = true;
			this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame3.Location = new System.Drawing.Point(4, 441);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(421, 86);
			this.Frame3.TabIndex = 8;
			this.Frame3.Visible = true;
			// 
			// Toolbar4
			// 
			this.Toolbar4.Dock = System.Windows.Forms.DockStyle.None;
			this.Toolbar4.ImageList = ImageList1;
			this.Toolbar4.Location = new System.Drawing.Point(377, 38);
			this.Toolbar4.Name = "Toolbar4";
			this.Toolbar4.ShowItemToolTips = true;
			this.Toolbar4.Size = new System.Drawing.Size(25, 26);
			this.Toolbar4.TabIndex = 14;
			this.Toolbar4.Items.Add(this._Toolbar4_Button1);
			// 
			// _Toolbar4_Button1
			// 
			this._Toolbar4_Button1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar4_Button1.ImageIndex = 0;
			this._Toolbar4_Button1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._Toolbar4_Button1.Name = "";
			this._Toolbar4_Button1.Size = new System.Drawing.Size(24, 22);
			this._Toolbar4_Button1.Tag = "";
			this._Toolbar4_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar4_Button1.ToolTipText = "Buscar";
			this._Toolbar4_Button1.Click += new System.EventHandler(this.Toolbar4_ButtonClick);
			// 
			// Toolbar3
			// 
			this.Toolbar3.Dock = System.Windows.Forms.DockStyle.None;
			this.Toolbar3.ImageList = ImageList1;
			this.Toolbar3.Location = new System.Drawing.Point(128, 12);
			this.Toolbar3.Name = "Toolbar3";
			this.Toolbar3.ShowItemToolTips = true;
			this.Toolbar3.Size = new System.Drawing.Size(25, 26);
			this.Toolbar3.TabIndex = 11;
			this.Toolbar3.Items.Add(this._Toolbar3_Button1);
			// 
			// _Toolbar3_Button1
			// 
			this._Toolbar3_Button1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar3_Button1.ImageIndex = 0;
			this._Toolbar3_Button1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._Toolbar3_Button1.Name = "";
			this._Toolbar3_Button1.Size = new System.Drawing.Size(24, 22);
			this._Toolbar3_Button1.Tag = "";
			this._Toolbar3_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar3_Button1.ToolTipText = "Buscar";
			this._Toolbar3_Button1.Click += new System.EventHandler(this.Toolbar3_ButtonClick);
			// 
			// chkServicioSeleccionado
			//			
			this.chkServicioSeleccionado.CausesValidation = true;			
			this.chkServicioSeleccionado.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.chkServicioSeleccionado.Cursor = System.Windows.Forms.Cursors.Default;
			this.chkServicioSeleccionado.Enabled = true;
			this.chkServicioSeleccionado.ForeColor = System.Drawing.SystemColors.ControlText;
			this.chkServicioSeleccionado.Location = new System.Drawing.Point(68, 64);
			this.chkServicioSeleccionado.Name = "chkServicioSeleccionado";
			this.chkServicioSeleccionado.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.chkServicioSeleccionado.Size = new System.Drawing.Size(140, 20);
			this.chkServicioSeleccionado.TabIndex = 15;
			this.chkServicioSeleccionado.TabStop = true;
			this.chkServicioSeleccionado.Text = "Servicio Seleccionado";
			this.chkServicioSeleccionado.Visible = true;
			// 
			// TB_DescPrestacion
			// 
			this.TB_DescPrestacion.AcceptsReturn = true;			
			this.TB_DescPrestacion.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.TB_DescPrestacion.ForeColor = System.Drawing.SystemColors.WindowText;
			this.TB_DescPrestacion.Location = new System.Drawing.Point(68, 40);
			this.TB_DescPrestacion.MaxLength = 120;
			this.TB_DescPrestacion.Name = "TB_DescPrestacion";
			this.TB_DescPrestacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TB_DescPrestacion.Size = new System.Drawing.Size(306, 21);
			this.TB_DescPrestacion.TabIndex = 13;
			this.TB_DescPrestacion.Enter += new System.EventHandler(this.TB_DescPrestacion_Enter);
			this.TB_DescPrestacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TB_DescPrestacion_KeyPress);
			// 
			// TB_CodPrestacion
			// 
			this.TB_CodPrestacion.AcceptsReturn = true;			
			this.TB_CodPrestacion.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.TB_CodPrestacion.ForeColor = System.Drawing.SystemColors.WindowText;
			this.TB_CodPrestacion.Location = new System.Drawing.Point(68, 12);
			this.TB_CodPrestacion.MaxLength = 7;
			this.TB_CodPrestacion.Name = "TB_CodPrestacion";
			this.TB_CodPrestacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TB_CodPrestacion.Size = new System.Drawing.Size(57, 21);
			this.TB_CodPrestacion.TabIndex = 10;
			this.TB_CodPrestacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TB_CodPrestacion_KeyPress);
			// 
			// Label4
			// 			
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label4.Location = new System.Drawing.Point(7, 47);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(65, 17);
			this.Label4.TabIndex = 12;
			this.Label4.Text = "Descripci�n:";
			// 
			// Label3
			//			
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label3.Location = new System.Drawing.Point(31, 19);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(3, 17);
			this.Label3.TabIndex = 9;
			this.Label3.Text = "C�digo:";
			// 
			// Frame2
			// 			
			this.Frame2.Controls.Add(this.sprSeleccionados);
			this.Frame2.Controls.Add(this.cmdAnadir);
			this.Frame2.Controls.Add(this.cmdEliminar);
			this.Frame2.Enabled = true;			
			this.Frame2.Location = new System.Drawing.Point(349, 397);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(429, 140);
			this.Frame2.TabIndex = 26;
			this.Frame2.Text = "Pruebas seleccionadas";
			this.Frame2.Visible = false;
			// 
			// sprSeleccionados
			// 
			this.sprSeleccionados.Location = new System.Drawing.Point(6, 13);
			this.sprSeleccionados.Name = "sprSeleccionados";
			this.sprSeleccionados.Size = new System.Drawing.Size(415, 100);
			this.sprSeleccionados.TabIndex = 27;
			this.sprSeleccionados.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprSeleccionados_CellClick);
			// 
			// cmdAnadir
			// 			
			this.cmdAnadir.Cursor = System.Windows.Forms.Cursors.Default;
			this.cmdAnadir.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdAnadir.Location = new System.Drawing.Point(333, 116);
			this.cmdAnadir.Name = "cmdAnadir";
			this.cmdAnadir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdAnadir.Size = new System.Drawing.Size(87, 20);
			this.cmdAnadir.TabIndex = 29;
			this.cmdAnadir.Text = "A�adir";
			this.cmdAnadir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;			
			this.cmdAnadir.Click += new System.EventHandler(this.cmdAnadir_Click);
			// 
			// cmdEliminar
			// 			
			this.cmdEliminar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cmdEliminar.Location = new System.Drawing.Point(240, 116);
			this.cmdEliminar.Name = "cmdEliminar";
			this.cmdEliminar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdEliminar.Size = new System.Drawing.Size(87, 20);
			this.cmdEliminar.TabIndex = 28;
			this.cmdEliminar.Text = "Eliminar";
			this.cmdEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cmdEliminar.Click += new System.EventHandler(this.cmdEliminar_Click);
			// 
			// ImageList1
			// 
			this.ImageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.ImageList1.ImageStream = (System.Windows.Forms.ImageListStreamer) resources.GetObject("ImageList1.ImageStream");
			this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
			this.ImageList1.Images.SetKeyName(0, "");
			// 
			// HCE310F2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;			
			this.ClientSize = new System.Drawing.Size(786, 564);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.cmdPetitorio);
			this.Controls.Add(this.CB_Aceptar);
			this.Controls.Add(this.CB_cancelar);
			this.Controls.Add(this.FRM_Servicio);
			this.Controls.Add(this.PIC_Prestaciones);
			this.Controls.Add(this.FRM_Prestacion);
			this.Controls.Add(this.Frame2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = (System.Drawing.Icon) resources.GetObject("HCE310F2.Icon");
			this.Location = new System.Drawing.Point(11, 79);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "HCE310F2";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Buscar y Seleccionar Prestaciones - HCE310F2";
			this.Activated += new System.EventHandler(this.HCE310F2_Activated);
			this.Closed += new System.EventHandler(this.HCE310F2_Closed);
			this.Load += new System.EventHandler(this.HCE310F2_Load);
			this.Frame1.ResumeLayout(false);
			this.FRM_Servicio.ResumeLayout(false);
			this.Frame4.ResumeLayout(false);
			this.Toolbar2.ResumeLayout(false);
			this.FRM_Prestacion.ResumeLayout(false);
			this.FrmTipoMuestra.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.Toolbar4.ResumeLayout(false);
			this.Toolbar3.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeOption1();
		}
		void InitializeOption1()
		{
			this.Option1 = new Telerik.WinControls.UI.RadRadioButton[2];
			this.Option1[0] = _Option1_0;
			this.Option1[1] = _Option1_1;
		}
		#endregion
	}
}