using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;
using Telerik.WinControls;

namespace ProcesoAsociar
{
	public partial class Asociar
         : Telerik.WinControls.UI.RadForm
    {
    
		SqlConnection rc = null;
		Mensajes.ClassMensajes oClass = null;
		string stIdentificador = String.Empty;
		string itiposer = String.Empty;
		int ganoregi = 0;
		int gnumregi = 0;
		string stUsuario = String.Empty;
		string stFechaLlegada = String.Empty;
		string stFormatoBD = String.Empty;
		string stSepFechBD = String.Empty;
		string stSepHoraBD = String.Empty;
		string AsocioProceso = String.Empty;
		string stIdProcesoSeleccionado = String.Empty;

		public Asociar()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		// OBTIENE VALORES DE SEPARADORES, Y FORMATO DE LA BASE DE DATOS
		public void ObtenerValoresTipoBD()
		{
			DataSet tRr = null;
			string tstQuery = String.Empty;
			string formato = String.Empty;
			int I = 0;
			try
			{
				tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, rc);
				tRr = new DataSet();
				tempAdapter.Fill(tRr);
			
				switch(Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
				{
					case "SQL SERVER 6.5" : 
					
						stFormatoBD = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();  //MM/DD/YYYY HH:MI 
						
						formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
						for (I = 1; I <= formato.Length; I++)
						{
							if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && stSepFechBD == "")
							{
								stSepFechBD = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
							}
							if (stSepFechBD != "")
							{
								break;
							}
						} 
						formato = formato.Substring(Strings.InStr(I + 1, formato, stSepFechBD, CompareMethod.Binary)); 
						for (I = 1; I <= formato.Length; I++)
						{
							if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && stSepHoraBD == "")
							{
								stSepHoraBD = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
							}
							if (stSepHoraBD != "")
							{
								break;
							}
						} 
						break;
				}
			
				tRr.Close();
			}
			catch (System.Exception excep)
			{
				RadMessageBox.Show("error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
			}
		}

		public void RecogerDatos(SqlConnection Conexion, string stUsua, string itipo, int gano, int gnum, string stGidenpac, string stFech, string StAsociarProceso = "", string ParaIdProceso = "")
		{
			//Recoge los par�metros mandados desde la clase
			rc = Conexion;
			stUsuario = stUsua;
			stIdentificador = stGidenpac;
			itiposer = itipo;
			ganoregi = gano;
			gnumregi = gnum;
			stFechaLlegada = stFech;
			AsocioProceso = StAsociarProceso;
			stIdProcesoSeleccionado = ParaIdProceso;
		}
		public void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string StSql = String.Empty;
			DataSet RrEpisodio = null;
			DataSet RrProceso = null;
			int gnumprinc = 0;
			int ganoprinc = 0;
			string itipprinc = String.Empty;
			bool bSalir = false;

			try
			{
				this.Cursor = Cursors.WaitCursor;
				//Si no existen filas o no ha elegido alguna, no se hace nada
				if (AsocioProceso == "S")
				{ //si viene de confirmacion masiva y solo hay 1 row se selecciona
					
                    sprProcesos.ReadOnly = true;
                    sprProcesos.SelectionMode = GridViewSelectionMode.FullRowSelect;
                    sprProcesos.Row = 1;
				}

				if (sprProcesos.MaxRows == 0 ) 
				{
				
					short tempRefParam = 1100;
					string [] tempRefParam2 = new string []{"Elecci�n de fila"};
					Serrores.iresume = Convert.ToInt32(oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, rc, tempRefParam2));
					this.Cursor = Cursors.Default;
					return;
				}
				//Inicializamos el episodio principal con el que se envia a la dll  y la variable para salir.
				itipprinc = itiposer;
				ganoprinc = ganoregi;
				gnumprinc = gnumregi;
				bSalir = false;
				//Comprobamos que el proceso est� abierto todav�a.
				sprProcesos.Col = 6; //Proceso
				StSql = "select * from dprocesova where ganoproc  = " + sprProcesos.Text.Trim().Substring(0, Math.Min(4, sprProcesos.Text.Trim().Length)) + " and  gnumproc = " + sprProcesos.Text.Trim().Substring(4);
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rc);
				RrProceso = new DataSet();
				tempAdapter.Fill(RrProceso);
				if (RrProceso.Tables[0].Rows.Count > 0)
				{
					//Si es de quir�fano o de consulta, comprobamos si tiene episodio principal.
					if (itiposer == "Q" || itiposer == "C")
					{
						if (itiposer == "Q")
						{
							StSql = "select itiposer, ganoprin, gnumprin from qintquir where gidenpac = '" + stIdentificador + "' and ganoregi=" + ganoregi.ToString() + "and gnumregi=" + gnumregi.ToString();
						}
						else
						{
							StSql = "select itiposer, ganoprin, gnumprin from cconsult where gidenpac = '" + stIdentificador + "' and ganoregi=" + ganoregi.ToString() + "and gnumregi=" + gnumregi.ToString();
						}
						StSql = StSql + " and itiposer is not null and  ganoprin is not null and  gnumprin is not  null";
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, rc);
						RrEpisodio = new DataSet();
						tempAdapter_2.Fill(RrEpisodio);
						if (RrEpisodio.Tables[0].Rows.Count > 0)
						{
							
							itipprinc = Convert.ToString(RrEpisodio.Tables[0].Rows[0]["itiposer"]);
							ganoprinc = Convert.ToInt32(RrEpisodio.Tables[0].Rows[0]["ganoprin"]);
							gnumprinc = Convert.ToInt32(RrEpisodio.Tables[0].Rows[0]["gnumprin"]);
						}
					
						RrEpisodio.Close();
					}
					//comprobamos que el episodio no exista en la tabla.
					sprProcesos.Col = 6; //Proceso
					StSql = "select * from depiproc where itiposer  = '" + itiposer + "' and  ganoregi = " + ganoregi.ToString() + " and gnumregi = " + gnumregi.ToString();
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, rc);
					RrEpisodio = new DataSet();
					tempAdapter_3.Fill(RrEpisodio);
					if (RrEpisodio.Tables[0].Rows.Count < 1)
					{
						
						RrEpisodio.AddNew();
						RrEpisodio.Tables[0].Rows[0]["ganoproc"] = sprProcesos.Text.Trim().Substring(0, Math.Min(4, sprProcesos.Text.Trim().Length));
						RrEpisodio.Tables[0].Rows[0]["gnumproc"] = sprProcesos.Text.Trim().Substring(4);
						RrEpisodio.Tables[0].Rows[0]["itiposer"] = itiposer;
						RrEpisodio.Tables[0].Rows[0]["ganoregi"] = ganoregi;
						RrEpisodio.Tables[0].Rows[0]["gnumregi"] = gnumregi;
						RrEpisodio.Tables[0].Rows[0]["itipprin"] = itipprinc;
						RrEpisodio.Tables[0].Rows[0]["ganoprin"] = ganoprinc;
						RrEpisodio.Tables[0].Rows[0]["gnumprin"] = gnumprinc;
						RrEpisodio.Tables[0].Rows[0]["gusuario"] = stUsuario;
					
						string tempQuery = RrEpisodio.Tables[0].TableName;
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tempQuery, "");
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_4);
						tempAdapter_4.Update(RrEpisodio, RrEpisodio.Tables[0].TableName);
						bSalir = true;
					}
					else
					{
						
						short tempRefParam3 = 1080;
						string [] tempRefParam4 = new string []{"Episodio"};
						Serrores.iresume = Convert.ToInt32(oClass.RespuestaMensaje(Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam3,  rc,  tempRefParam4));
					}
					
					RrEpisodio.Close();
				}
				else
				{
					
					short tempRefParam5 = 1100;
					string[] tempRefParam6 = new string[]{"Proceso abierto"};
					Serrores.iresume = Convert.ToInt32(oClass.RespuestaMensaje(Serrores.vNomAplicacion,  Serrores.vAyuda, tempRefParam5, rc,  tempRefParam6));
				}
				
				RrProceso.Close();
				this.Cursor = Cursors.Default;
				if (bSalir)
				{
					CbCerrar_Click(CbCerrar, new EventArgs());
				}
			}
			catch (System.Exception excep)
			{
			
				RadMessageBox.Show("error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
				this.Cursor = Cursors.Default;
				RrProceso = null;
				RrEpisodio = null;
			}
		}

		private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			//Cierra la pantalla y devuelve el control al programa llamador
			this.Close();
		}

		
		private void Asociar_Load(Object eventSender, EventArgs eventArgs)
		{
			oClass = new Mensajes.ClassMensajes();
			ObtenerValoresTipoBD();
			proCargaGrid();
			Llenar_etiquetas();
            //Ponemos la ayuda

            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

            Serrores.ObtenerNombreAplicacion(rc);
			//me.HelpContextID=???????
			Serrores.ObtenerNombreAplicacion(rc);
		}
		public void Llenar_etiquetas()
		{
			string sql = String.Empty;
			DataSet RR = null;
			string stPaciente = String.Empty;

			try
			{
				stPaciente = "";

				sql = "SELECT DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " + 
				      "HDOSSIER.ghistoria FROM DPACIENT " + 
				      "LEFT OUTER JOIN " + 
				      "HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac " + 
				      "WHERE DPACIENT.gidenpac = '" + stIdentificador + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rc);
				RR = new DataSet();
				tempAdapter.Fill(RR);
				if (RR.Tables[0].Rows.Count > 0)
				{
				
					if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["dnombpac"]))
					{
					
						stPaciente = Convert.ToString(RR.Tables[0].Rows[0]["dnombpac"]).Trim();
					}
					
					if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["dape1pac"]))
					{
					
						stPaciente = stPaciente + " " + Convert.ToString(RR.Tables[0].Rows[0]["dape1pac"]).Trim();
					}
					
					if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["dape2pac"]))
					{
					
						stPaciente = stPaciente + " " + Convert.ToString(RR.Tables[0].Rows[0]["dape2pac"]).Trim();
					}
					LblTextPaciente.Text = stPaciente;
					
					if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["ghistoria"]))
					{
					
						LblTextHistoria.Text = Convert.ToString(RR.Tables[0].Rows[0]["ghistoria"]).Trim();
					}
				}
				
				RR.Close();
			}
			catch (System.Exception excep)
			{
				
				RadMessageBox.Show("error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
			}
		}
		public void proCargaGrid()
		{
			//***********************************************
			//* Obtener los procesos del paciente elegido
			//***********************************************
			string sql = String.Empty;
			DataSet RR = null;
			DLLTextosSql.TextosSql oTextoSql = null;

			int icont = 1;
			try
			{
				string[] MatrizSql = new string[]{String.Empty, String.Empty, String.Empty};
				
				object tempRefParam = stFechaLlegada;
				MatrizSql[1] = Serrores.FormatFechaHMS( tempRefParam);
				stFechaLlegada = Convert.ToString(tempRefParam);
				MatrizSql[2] = stIdentificador;
				oTextoSql = new DLLTextosSql.TextosSql();
				string tempRefParam2 = "ProcesoAsociar";
				string tempRefParam3 = "Asociar";
				string tempRefParam4 = "ProCargaGrid";
				short tempRefParam5 = 1;
			
				sql = Convert.ToString(oTextoSql.DevuelveTexto( Serrores.VstTipoBD,  tempRefParam2,  tempRefParam3, tempRefParam4,  tempRefParam5, MatrizSql,  rc));
				if (stIdProcesoSeleccionado != "")
				{
					sql = sql + " AND ganoproc =" + stIdProcesoSeleccionado.Substring(0, Math.Min(4, stIdProcesoSeleccionado.Length));
					sql = sql + " AND gnumproc =" + stIdProcesoSeleccionado.Substring(4);
				}
				sql = sql + " order by finiproc ";
				oTextoSql = null;
				sprProcesos.MaxRows = 0;

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rc);
				RR = new DataSet();
				tempAdapter.Fill(RR);
				if (RR.Tables[0].Rows.Count > 0)
				{
					sprProcesos.MaxRows = RR.Tables[0].Rows.Count;
					foreach (DataRow iteration_row in RR.Tables[0].Rows)
					{
						sprProcesos.Row = icont;
						sprProcesos.Col = 1; //Fecha y hora
						sprProcesos.Text = StringsHelper.Format(iteration_row["finiproc"], "DD" + stSepFechBD + "MM" + stSepFechBD + "YYYY HH" + stSepHoraBD + "NN");
						sprProcesos.Col = 2; //Descripci�n del diagn�stico
						
						if (Convert.IsDBNull(iteration_row["dnombdia"]))
						{
						
							if (!Convert.IsDBNull(iteration_row["odiagnos"]))
							{
								sprProcesos.Text = Convert.ToString(iteration_row["odiagnos"]).Trim();
							}
						}
						else
						{
							sprProcesos.Text = Convert.ToString(iteration_row["dnombdia"]).Trim();
							sprProcesos.Col = 3; //C�digo del diagn�stico
							sprProcesos.Text = Convert.ToString(iteration_row["gdiagnos"]);
							sprProcesos.Col = 7;
						
							if (!Convert.IsDBNull(Convert.ToString(iteration_row["finvaldi"]).Trim()))
							{
								sprProcesos.Text = Convert.ToString(iteration_row["finvaldi"]).Trim();
							} //Fecha de inicio de validez del diagn�stico
						}
						
						if (!Convert.IsDBNull(iteration_row["dnomserv"]))
						{
							sprProcesos.Col = 4; //Descripci�n del Servicio
							sprProcesos.Text = Convert.ToString(iteration_row["dnomserv"]).Trim();
							sprProcesos.Col = 5; //C�digo del Servicio
							sprProcesos.Text = Convert.ToString(iteration_row["gservici"]);
						}
						sprProcesos.Col = 6; //Proceso
						sprProcesos.Text = Convert.ToString(iteration_row["ganoproc"]) + Convert.ToString(iteration_row["gnumproc"]);
						sprProcesos.Col = 8; //Proceso
					
						if (!Convert.IsDBNull(iteration_row["dprogram"]))
						{
							sprProcesos.Text = Convert.ToString(iteration_row["dprogram"]).Trim().ToUpper();
						}
						icont++;
					}
				}
			
				RR.Close();
			}
			catch (System.Exception excep)
			{
				RadMessageBox.Show("error: " + Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName);
			}
		}
		private void Asociar_Closed(Object eventSender, EventArgs eventArgs)
		{
			oClass = null;
		}
		private void sprProcesos_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
            if (Row > 0)
			{
                sprProcesos.ReadOnly = true;
                sprProcesos.SelectionMode = GridViewSelectionMode.FullRowSelect;
                sprProcesos.Row = Row;
			}
		}
	}
}