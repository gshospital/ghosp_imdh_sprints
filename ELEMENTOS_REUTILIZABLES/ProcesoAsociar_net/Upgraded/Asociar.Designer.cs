using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ProcesoAsociar
{
	partial class Asociar
	{

		#region "Upgrade Support "
		private static Asociar m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Asociar DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Asociar();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CbCerrar", "sprProcesos", "FrmProcesos", "CbAceptar", "lblHistoria", "LblTextHistoria", "LblTextPaciente", "LblPaciente", "FrmPaciente", "sprProcesos_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public UpgradeHelpers.Spread.FpSpread sprProcesos;
		public Telerik.WinControls.UI.RadGroupBox FrmProcesos;
		public Telerik.WinControls.UI.RadButton CbAceptar;
		public Telerik.WinControls.UI.RadLabel lblHistoria;
		public Telerik.WinControls.UI.RadTextBox LblTextHistoria;
		public Telerik.WinControls.UI.RadTextBox LblTextPaciente;
        public Telerik.WinControls.UI.RadLabel  LblPaciente;
		public Telerik.WinControls.UI.RadGroupBox FrmPaciente;
        //NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Asociar));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.CbCerrar = new Telerik.WinControls.UI.RadButton();
			this.FrmProcesos = new Telerik.WinControls.UI.RadGroupBox();
			this.sprProcesos = new UpgradeHelpers.Spread.FpSpread();
			this.CbAceptar = new Telerik.WinControls.UI.RadButton();
			this.FrmPaciente = new Telerik.WinControls.UI.RadGroupBox();
			this.lblHistoria = new Telerik.WinControls.UI.RadLabel();
			this.LblTextHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.LblTextPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.LblPaciente = new Telerik.WinControls.UI.RadLabel();
			this.FrmProcesos.SuspendLayout();
			this.FrmPaciente.SuspendLayout();
			this.SuspendLayout();
			// 
			// CbCerrar
			// 
			this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.CbCerrar.Location = new System.Drawing.Point(496, 241);
			this.CbCerrar.Name = "CbCerrar";
			this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbCerrar.Size = new System.Drawing.Size(81, 29);
			this.CbCerrar.TabIndex = 6;
			this.CbCerrar.Text = "&Salir";
			this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
			// 
			// FrmProcesos
			// 	
			this.FrmProcesos.Controls.Add(this.sprProcesos);
			this.FrmProcesos.Enabled = true;
			this.FrmProcesos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.FrmProcesos.Location = new System.Drawing.Point(4, 40);
			this.FrmProcesos.Name = "FrmProcesos";
			this.FrmProcesos.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmProcesos.Size = new System.Drawing.Size(573, 197);
			this.FrmProcesos.TabIndex = 5;
			this.FrmProcesos.Text = "Procesos Abiertos";
			this.FrmProcesos.Visible = true;
			// 
			// sprProcesos
			// 
			this.sprProcesos.Location = new System.Drawing.Point(9, 16);
			this.sprProcesos.Name = "sprProcesos";
			this.sprProcesos.Size = new System.Drawing.Size(555, 172);
			this.sprProcesos.TabIndex = 0;
			this.sprProcesos.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprProcesos_CellClick);
			// 
			// CbAceptar
			// 
			this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.CbAceptar.Location = new System.Drawing.Point(405, 241);
			this.CbAceptar.Name = "CbAceptar";
			this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbAceptar.Size = new System.Drawing.Size(81, 29);
			this.CbAceptar.TabIndex = 1;
			this.CbAceptar.Text = "&Aceptar";
			this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
			// 
			// FrmPaciente
			// 
			this.FrmPaciente.Controls.Add(this.lblHistoria);
			this.FrmPaciente.Controls.Add(this.LblTextHistoria);
			this.FrmPaciente.Controls.Add(this.LblTextPaciente);
			this.FrmPaciente.Controls.Add(this.LblPaciente);
			this.FrmPaciente.Enabled = true;
			this.FrmPaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.FrmPaciente.Location = new System.Drawing.Point(4, -1);
			this.FrmPaciente.Name = "FrmPaciente";
			this.FrmPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmPaciente.Size = new System.Drawing.Size(573, 40);
			this.FrmPaciente.TabIndex = 2;
			this.FrmPaciente.Visible = true;
			// 
			// lblHistoria
			// 
			this.lblHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			this.lblHistoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.lblHistoria.Location = new System.Drawing.Point(438, 16);
			this.lblHistoria.Name = "lblHistoria";
			this.lblHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblHistoria.Size = new System.Drawing.Size(54, 17);
			this.lblHistoria.TabIndex = 8;
			this.lblHistoria.Text = "N� Historia:";
			// 
			// LblTextHistoria
			// 
			this.LblTextHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			this.LblTextHistoria.Location = new System.Drawing.Point(496, 13);
			this.LblTextHistoria.Name = "LblTextHistoria";
			this.LblTextHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LblTextHistoria.Size = new System.Drawing.Size(71, 20);
			this.LblTextHistoria.TabIndex = 7;
			// 
			// LblTextPaciente
			// 
			this.LblTextPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.LblTextPaciente.Location = new System.Drawing.Point(55, 13);
			this.LblTextPaciente.Name = "LblTextPaciente";
			this.LblTextPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LblTextPaciente.Size = new System.Drawing.Size(375, 20);
			this.LblTextPaciente.TabIndex = 4;
			// 
			// LblPaciente
			// 		
			this.LblPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.LblPaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.LblPaciente.Location = new System.Drawing.Point(6, 16);
			this.LblPaciente.Name = "LblPaciente";
			this.LblPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LblPaciente.Size = new System.Drawing.Size(49, 17);
			this.LblPaciente.TabIndex = 3;
			this.LblPaciente.Text = "Paciente:";
			// 
			// Asociar
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(0, 0);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(580, 274);
			this.Controls.Add(this.CbCerrar);
			this.Controls.Add(this.FrmProcesos);
			this.Controls.Add(this.CbAceptar);
			this.Controls.Add(this.FrmPaciente);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(100, 201);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Asociar";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Asociar Episodios";
			this.Closed += new System.EventHandler(this.Asociar_Closed);
			this.Load += new System.EventHandler(this.Asociar_Load);
			this.FrmProcesos.ResumeLayout(false);
			this.FrmPaciente.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}