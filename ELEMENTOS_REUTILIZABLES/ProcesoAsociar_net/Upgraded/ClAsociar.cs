using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace ProcesoAsociar
{
	public class ClAsociar
	{

		string stFormatoBD = String.Empty;
		string stSepFechBD = String.Empty;
		string stSepHoraBD = String.Empty;
		public string MarcarRegistro = String.Empty;

		// OBTIENE VALORES DE SEPARADORES, Y FORMATO DE LA BASE DE DATOS
		public void ObtenerValoresTipoBD(SqlConnection rc)
		{
			string formato = String.Empty;
			int I = 0;
			string tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, rc);
			DataSet tRr = new DataSet();
			tempAdapter.Fill(tRr);
			
			switch(Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
			{
				case "SQL SERVER 6.5" : 
				
					stFormatoBD = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();  //MM/DD/YYYY HH:MI 
					formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
					for (I = 1; I <= formato.Length; I++)
					{
						if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && stSepFechBD == "")
						{
							stSepFechBD = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
						}
						if (stSepFechBD != "")
						{
							break;
						}
					} 
					formato = formato.Substring(Strings.InStr(I + 1, formato, stSepFechBD, CompareMethod.Binary)); 
					for (I = 1; I <= formato.Length; I++)
					{
						if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && stSepHoraBD == "")
						{
							stSepHoraBD = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
						}
						if (stSepHoraBD != "")
						{
							break;
						}
					} 
					break;
			}
			
			tRr.Close();
		}


		public void Iniciar(ref SqlConnection Conexion, string stUsua, string itiposer, int ganoregi, int gnumregi, string stGidenpac, string stFecha, string StvAsociar = "")
		{
			string sql = String.Empty;
			string Sql1 = String.Empty;
			DataSet RR = null;
			Mensajes.ClassMensajes oClass = new Mensajes.ClassMensajes();
			ObtenerValoresTipoBD(Conexion);
			Serrores.AjustarRelojCliente(Conexion);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
            //Ponemos la ayuda

            CDAyuda.Instance.setHelpFile(Asociar.DefInstance, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

            Serrores.ObtenerNombreAplicacion(Conexion);
			//Vemos si tiene procesos abiertos
			string stFechaLlegada = stFecha;


			//si Tiposervicio = C y ihoscons = H  se sale xq el episodio ya esta relacionado
			if (itiposer == "C")
			{
				sql = "select * from CCONSULT where ganoregi=" + ganoregi.ToString() + " and gnumregi= " + gnumregi.ToString() + " and ihoscons='H'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
				RR = new DataSet();
				tempAdapter.Fill(RR);
				if (RR.Tables[0].Rows.Count != 0)
				{
					return;
				}
			
				RR.Close();
			}

			sql = "select * from CCONSULT where ganoregi=" + ganoregi.ToString() + " and gnumregi= " + gnumregi.ToString();
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
			RR = new DataSet();
			tempAdapter_2.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["itiposer"]) && !Convert.IsDBNull(RR.Tables[0].Rows[0]["ganoprin"]) && !Convert.IsDBNull(RR.Tables[0].Rows[0]["gnumprin"]))
				{
				
					sql = "SELECT * " + 
					      "FROM DEPIPROC WITH (NOLOCK) " + 
					      "WHERE itipprin = '" + Convert.ToString(RR.Tables[0].Rows[0]["itiposer"]) + "' AND " + 
					      "ganoprin = " + Convert.ToString(RR.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
					      "gnumprin = " + Convert.ToString(RR.Tables[0].Rows[0]["gnumprin"]) + " AND " + 
					      "itiposer = 'C' AND " + 
					      "ganoregi = " + Convert.ToString(RR.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
					      "gnumregi = " + Convert.ToString(RR.Tables[0].Rows[0]["gnumregi"]);
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, Conexion);
					RR = new DataSet();
					tempAdapter_3.Fill(RR);
					if (RR.Tables[0].Rows.Count != 0)
					{
						return;
					}
			
					RR.Close();
				}
				else
				{
				
					Sql1 = "SELECT * FROM DEPIPROC WITH (NOLOCK) " + 
					       "WHERE itiposer = 'C' AND " + 
					       "ganoregi = " + Convert.ToString(RR.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
					       "gnumregi = " + Convert.ToString(RR.Tables[0].Rows[0]["gnumregi"]);
					//Si est� activo ienvicat, ....
					sql = "SELECT count(*) FROM SCONSGLO WITH (NOLOCK) WHERE gconsglo = 'IENVICAT' AND isnull(valfanu1,'N')='S'";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion);
					RR = new DataSet();
					tempAdapter_4.Fill(RR);
				
					if (Convert.ToBoolean(RR.Tables[0].Rows[0][0]))
					{
						//...comprobamos si la consulta est� asociada, aunque no est� relacionada.
						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(Sql1, Conexion);
						RR = new DataSet();
						tempAdapter_5.Fill(RR);
						if (RR.Tables[0].Rows.Count != 0)
						{
							return;
						}
					}
				
					RR.Close();
				}
			}

		
			object tempRefParam = stFechaLlegada;
			sql = "select * from dprocesova where gidenpac ='" + stGidenpac + "' and finiproc <=" + Serrores.FormatFechaHMS(tempRefParam) + "";
			stFechaLlegada = Convert.ToString(tempRefParam);
			SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql, Conexion);
			RR = new DataSet();
			tempAdapter_6.Fill(RR);
			if (RR.Tables[0].Rows.Count > 0)
			{
				if (StvAsociar == "")
				{
					
					string tempRefParam2 = "";
					short tempRefParam3 = 1125;
					string [] tempRefParam4 = new string [] {"Existen procesos abiertos para este paciente", "incluir el episodio en alguno"};
					Serrores.iresume = Convert.ToInt32(oClass.RespuestaMensaje(Serrores.vNomAplicacion,  tempRefParam2,  tempRefParam3,  Conexion,  tempRefParam4));
				}
				else
				{
                    if (StvAsociar == "S")
                    {
                    
                        string tempRefParam5 = "";
                        short tempRefParam6 = 1125;
                        string[] tempRefParam7 = new string []{"Existen procesos abiertos para este paciente", "incluir el episodio en alguno"};
						Serrores.iresume = Convert.ToInt32(oClass.RespuestaMensaje( Serrores.vNomAplicacion, tempRefParam5,  tempRefParam6,  Conexion,  tempRefParam7));
					}
					else
					{
						if (StvAsociar == "N")
						{
							if (RR.Tables[0].Rows.Count > 1)
							{
								
								string tempRefParam8 = "";
								short tempRefParam9 = 1125;
								string [] tempRefParam10 = new string [] {"Existen procesos abiertos para este paciente", "incluir el episodio en alguno"};
								Serrores.iresume = Convert.ToInt32(oClass.RespuestaMensaje( Serrores.vNomAplicacion,  tempRefParam8,  tempRefParam9, Conexion,  tempRefParam10));
							}
							else
							{
								Serrores.iresume = 6;
							}
						}
					}
				}

				if (Serrores.iresume == 6)
				{
					if ((StvAsociar != "N") || (RR.Tables[0].Rows.Count > 1))
					{
						MarcarRegistro = "N";
					}
					else
					{
						MarcarRegistro = "S";
					}
					Asociar.DefInstance.RecogerDatos(Conexion, stUsua, itiposer, ganoregi, gnumregi, stGidenpac, stFechaLlegada, MarcarRegistro);
					Asociar tempLoadForm = Asociar.DefInstance;
					if ((StvAsociar != "N") || (RR.Tables[0].Rows.Count > 1))
					{
						Asociar.DefInstance.ShowDialog();
					}
					else
					{
						
						Asociar.DefInstance.CbAceptar_Click(Asociar.DefInstance.CbAceptar, new EventArgs());
					}
					Asociar.DefInstance = null;
				}
			}
			oClass = null;
			
			RR.Close();
		}
	}
}