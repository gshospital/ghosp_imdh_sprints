using System;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Retorno
{
	public class Class2
	{

		public bool ElError = false;

		public void ProRecibirError(object var1, object var2)
		{
			ElError = false;
			//UPGRADE_WARNING: (2080) IsEmpty was upgraded to a comparison and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
			if (!Object.Equals(var1, null))
			{
				ElError = true;
			}
		}
		public bool RetonarError()
		{
			return ElError;
		}
	}
}