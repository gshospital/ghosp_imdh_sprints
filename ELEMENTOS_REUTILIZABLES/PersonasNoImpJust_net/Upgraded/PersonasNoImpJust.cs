using CrystalWrapper;
using ElementosCompartidos;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PersonasNoImpJust
{
	public class MCPersonasNoImpJust
	{
		FrmPersonasNoImpJust PersNoImpJust = null;

		public void Llamada(SqlConnection nConnet, string stiTipoSer, int iGano, int lGnum, string usuario, string stPac, string stIdenPac)
		{
			PersNoImpJust = new FrmPersonasNoImpJust();

			PersNoImpJust.REFERENCIAS(nConnet, stiTipoSer, iGano, lGnum, usuario, stIdenPac);
			PersNoImpJust.LbDPaciente.Text = stPac;
			PersNoImpJust.ShowDialog();

		}

		~MCPersonasNoImpJust()
		{
			FrmPersonasNoImpJust.DefInstance = null;
			PersNoImpJust = null;
		}

		public void ImprimirConsentimiento(SqlConnection nConnet, string stiTipoSer, int iGano, int lGnum, string stGidenpac, string StBaseTemporal, string NOMBRE_DSN_SQL, string NOMBRE_DSN_ACCESS, Crystal oCrystal, string PathString, string gUsuario_Crystal, string gContraseña_Crystal, bool btest, int iCopias)
		{
			object dbLangSpanish = null;
			object DBEngine = null;          


            bool bBase = false;
            DataSet bs = new DataSet();
            DataRow Rs = null;
            StringBuilder stPersonas = new StringBuilder();
			int iTamañoConAnteultimaPersona = 0;

            //'Obtener_Multilenguaje nConnet, stGidenpac

            bs.CreateTable("CR11_AGI12BR3", PathString + "\\rpt\\CR11_AGI12BR3.xsd");

			//bs.Execute("CREATE TABLE SinConsentimiento " + 
			//           " (Persona text)");
			//-----

			//    '*********Registramos el ODBC para ACCESS
			//    stSql = "Description= BASE TEMPORAL EN ACCESS" _
			//'    & Chr$(13) & "Oemtoansi=no" _
			//'    & Chr$(13) & "server =(local)" _
			//'    & Chr$(13) & "dbq=" & StBaseTemporal & ""
			//    rdoEngine.rdoRegisterDataSource NOMBRE_DSN_ACCESS, "Microsoft Access Driver (*.mdb)", True, stSql
			//Datos de las personas sin autorización.
			string stSql = "select ltrim(rtrim( ltrim(rtrim(isnull(dnombper,''))) + ' ' + ltrim(rtrim(isnull(dape1per,''))) ))+ ' ' + ltrim(rtrim(isnull(dape2per,''))) persona from dpersjus where itiposer = '" + stiTipoSer + "' and ganoregi= " + iGano.ToString().Trim() + " and gnumregi= " + lGnum.ToString().Trim() + " order by nnumorde ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, nConnet);
			DataSet RrJustificante = new DataSet();
			tempAdapter.Fill(RrJustificante);
			//Si hay persona a las que no se les debe imprimir el justificante...
			if (RrJustificante.Tables[0].Rows.Count != 0)
			{
				//.....llenamos la tabla temporal.
				stPersonas = new StringBuilder("");
                DataTable dttabla = bs.Tables["CR11_AGI12BR3"];
                if (btest)
				{ //Si se quiere uno tras otro, separado por coma
					iTamañoConAnteultimaPersona = 0;
					foreach (DataRow iteration_row in RrJustificante.Tables[0].Rows)
					{
						iTamañoConAnteultimaPersona = stPersonas.ToString().Trim().Length;
						stPersonas.Append((Convert.ToString(iteration_row["persona"]) + "").Trim() + ", ");
					}
					stPersonas = new StringBuilder(stPersonas.ToString().Trim().Substring(0, Math.Min(stPersonas.ToString().Trim().Length - 1, stPersonas.ToString().Trim().Length)));
					if (iTamañoConAnteultimaPersona != 0)
					{
						stPersonas = new StringBuilder(stPersonas.ToString().Substring(0, Math.Min(iTamañoConAnteultimaPersona - 1, stPersonas.ToString().Length)) + " y" + stPersonas.ToString().Substring(iTamañoConAnteultimaPersona));
					}
                    Rs = dttabla.NewRow();
                    Rs["Persona"] = stPersonas.ToString();
                    dttabla.Rows.Add(Rs);
                }
                else
				{
					foreach (DataRow iteration_row_2 in RrJustificante.Tables[0].Rows)
					{
                        Rs = dttabla.NewRow();
                        Rs["Persona"] = (Convert.ToString(iteration_row_2["persona"]) + "").Trim();
                        dttabla.Rows.Add(Rs);
					}
				}
            }
            RrJustificante.Close();

			//*********cabeceras
			stSql = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, nConnet);
			RrJustificante = new DataSet();
			tempAdapter_2.Fill(RrJustificante);
			
			string stGrupoHo = Convert.ToString(RrJustificante.Tables[0].Rows[0]["VALFANU1"]).Trim();
			stSql = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, nConnet);
			RrJustificante = new DataSet();
			tempAdapter_3.Fill(RrJustificante);
			
			string stNombreHo = Convert.ToString(RrJustificante.Tables[0].Rows[0]["VALFANU1"]).Trim();
			stSql = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, nConnet);
			RrJustificante = new DataSet();
			tempAdapter_4.Fill(RrJustificante);
			
			string stDireccHo = Convert.ToString(RrJustificante.Tables[0].Rows[0]["VALFANU1"]).Trim();
			//    stSql = "SELECT valfanu2 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' "
			//    Set RrJustificante = nConnet.OpenResultset(stSql, rdOpenKeyset, rdConcurReadOnly)
			//         stPoblaciHo = Trim(tRrCrystal("VALFANU2"))
			RrJustificante.Close();

            oCrystal.ReportFileName = PathString + "\\rpt\\agi12br3_CR11.rpt";
            oCrystal.Formulas["{@FOR1}"] = "\"" + stGrupoHo + "\"";
            oCrystal.Formulas["{@FOR2}"] = "\"" + stNombreHo + "\"";
            oCrystal.Formulas["{@FOR3}"] = "\"" + stDireccHo + "\"";

            //Datos del paciente
            stSql = " SELECT dnombpac,dape1pac,dape2pac,ndninifp From DPACIENT Where gidenpac ='" + stGidenpac + "'";
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSql, nConnet);
			RrJustificante = new DataSet();
			tempAdapter_5.Fill(RrJustificante);
			
			string sPaciente = "D/Dª " + Convert.ToString(RrJustificante.Tables[0].Rows[0]["dnombpac"]).Trim(); //DPACIENT.
			
			
			sPaciente = (sPaciente + ((Convert.IsDBNull(RrJustificante.Tables[0].Rows[0]["dape1pac"])) ? "" : " " + Convert.ToString(RrJustificante.Tables[0].Rows[0]["dape1pac"]))).Trim();
			
			sPaciente = (sPaciente + ((Convert.IsDBNull(RrJustificante.Tables[0].Rows[0]["dape2pac"])) ? "" : " " + Convert.ToString(RrJustificante.Tables[0].Rows[0]["dape2pac"]))).Trim();

            oCrystal.Formulas["{@nombre}"] = "\"" + sPaciente + "\"";

            if ((Convert.ToString(RrJustificante.Tables[0].Rows[0]["ndninifp"]) + "").Trim() == "")
			{
                //camaya todo_x_4
                oCrystal.Formulas["{@NumDni}"] = "" + "\"\"";
            }
            else
			{
                oCrystal.Formulas["{@NumDni}"] = "\"" + ", con DNI/NIE " + (Convert.ToString(RrJustificante.Tables[0].Rows[0]["ndninifp"]) + "").Trim() + "\"";
            }
            //*******************
            //INDRA camaya todo_x_4 contiene objetos de crystal
            
            oCrystal.WindowTitle = "AUTORIZACIÓN PARA EMISIÓN DE JUSTIFICANTE";
            if (stiTipoSer == "H")
            {
                oCrystal.Formulas["{@Titulo}"] = "\"" + "AUTORIZACIÓN PARA LA EMISIÓN DE JUSTIFICANTES DE HOSPITALIZACIÓN" + "\"";
            }
            else
            {
                oCrystal.Formulas["{@Titulo}"] = "\"" + "AUTORIZACIÓN PARA LA EMISIÓN DE JUSTIFICANTES DE URGENCIAS" + "\"";
            }

            oCrystal.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(nConnet) + "\"";

            oCrystal.DataFiles[0] = bs.Tables[0];

            oCrystal.SubreportToChange = "LogotipoIDC";
            oCrystal.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
            oCrystal.Connect = "" + NOMBRE_DSN_SQL + ";" + gUsuario_Crystal + ";" + gContraseña_Crystal + "";


            oCrystal.SubreportToChange = "PIEPAGINA";
            oCrystal.SQLQuery = "select OPIEINFO from   DPIEINFO where UPPER(ginforme)='agi12br3' and itipoper='T'";
            oCrystal.Connect = "" + NOMBRE_DSN_SQL + ";" + gUsuario_Crystal + ";" + gContraseña_Crystal;

            oCrystal.SubreportToChange = "";
            oCrystal.WindowState = (Crystal.WindowStateConstants)2; //crptMaximized
            oCrystal.CopiesToPrinter = iCopias;
            oCrystal.Destination = (Crystal.DestinationConstants)1; //ParaSalida
            oCrystal.WindowShowPrintSetupBtn = true;

            oCrystal.Action = 1;
            //oCrystal.PageCount();
            oCrystal.PrinterStopPage = oCrystal.PageCount;
            oCrystal.PrinterStopPage = 0;
            oCrystal.Reset();

            int tiForm = 0;
            foreach (var key in oCrystal.Formulas.Keys)
            {
                if (tiForm <= 6)
                    oCrystal.Formulas[key] = "";
            }
		}
	}
}