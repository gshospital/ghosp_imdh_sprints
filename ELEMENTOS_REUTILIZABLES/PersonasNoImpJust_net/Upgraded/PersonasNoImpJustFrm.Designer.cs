using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PersonasNoImpJust
{
	partial class FrmPersonasNoImpJust
	{

		#region "Upgrade Support "
		private static FrmPersonasNoImpJust m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static FrmPersonasNoImpJust DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new FrmPersonasNoImpJust();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprPerSinJust", "cbEliminar", "cbAceptar", "cbCerrar", "cbA�adir", "cbLimpiar", "tbApellido1", "tbApellido2", "tbNombre", "tbNIFOtrosDocDP", "LblDNINIF", "lbApellido1", "lbApellido2", "lbNombre", "frmPersonal", "LbPaciente", "LbDPaciente", "FrmPaciente", "lnveto", "Lbveto", "ShapeContainer1", "SprPerSinJust_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprPerSinJust;
		public Telerik.WinControls.UI.RadButton cbEliminar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadButton cbA�adir;
		public Telerik.WinControls.UI.RadButton cbLimpiar;
		public Telerik.WinControls.UI.RadTextBox tbApellido1;
		public Telerik.WinControls.UI.RadTextBox tbApellido2;
		public Telerik.WinControls.UI.RadTextBox tbNombre;
		public System.Windows.Forms.MaskedTextBox tbNIFOtrosDocDP;
		public Telerik.WinControls.UI.RadLabel LblDNINIF;
		public Telerik.WinControls.UI.RadLabel lbApellido1;
		public Telerik.WinControls.UI.RadLabel lbApellido2;
		public Telerik.WinControls.UI.RadLabel lbNombre;
		public Telerik.WinControls.UI.RadGroupBox frmPersonal;
		public Telerik.WinControls.UI.RadLabel LbPaciente;
		public Telerik.WinControls.UI.RadLabel LbDPaciente;
		public Telerik.WinControls.UI.RadGroupBox FrmPaciente;
		public Microsoft.VisualBasic.PowerPacks.LineShape lnveto;
		public Telerik.WinControls.UI.RadLabel Lbveto;
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		//private FarPoint.Win.Spread.SheetView SprPerSinJust_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.SprPerSinJust = new UpgradeHelpers.Spread.FpSpread();
            this.lnveto = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.cbEliminar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            this.frmPersonal = new Telerik.WinControls.UI.RadGroupBox();
            this.cbA�adir = new Telerik.WinControls.UI.RadButton();
            this.cbLimpiar = new Telerik.WinControls.UI.RadButton();
            this.tbApellido1 = new Telerik.WinControls.UI.RadTextBox();
            this.tbApellido2 = new Telerik.WinControls.UI.RadTextBox();
            this.tbNombre = new Telerik.WinControls.UI.RadTextBox();
            this.tbNIFOtrosDocDP = new System.Windows.Forms.MaskedTextBox();
            this.LblDNINIF = new Telerik.WinControls.UI.RadLabel();
            this.lbApellido1 = new Telerik.WinControls.UI.RadLabel();
            this.lbApellido2 = new Telerik.WinControls.UI.RadLabel();
            this.lbNombre = new Telerik.WinControls.UI.RadLabel();
            this.FrmPaciente = new Telerik.WinControls.UI.RadGroupBox();
            this.LbPaciente = new Telerik.WinControls.UI.RadLabel();
            this.LbDPaciente = new Telerik.WinControls.UI.RadLabel();
            this.Lbveto = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbEliminar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPersonal)).BeginInit();
            this.frmPersonal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbA�adir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbLimpiar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LblDNINIF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbApellido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbApellido2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmPaciente)).BeginInit();
            this.FrmPaciente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbveto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lnveto});
            this.ShapeContainer1.Size = new System.Drawing.Size(685, 493);
            this.ShapeContainer1.TabIndex = 18;
            this.ShapeContainer1.TabStop = false;
            // 
            // SprPerSinJust
            // 
            this.SprPerSinJust.Location = new System.Drawing.Point(4, 40);
            this.SprPerSinJust.Name = "SprPerSinJust";
            this.SprPerSinJust.Size = new System.Drawing.Size(676, 308);
            this.SprPerSinJust.TabIndex = 0;
            this.SprPerSinJust.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPerSinJust_CellClick);            
            this.SprPerSinJust.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SprPerSinJust_KeyDown);
            this.SprPerSinJust.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SprPerSinJust_MouseDown);
            this.SprPerSinJust.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SprPerSinJust_MouseUp);
            // 
            // lnveto
            // 
            this.lnveto.BorderColor = System.Drawing.Color.Fuchsia;
            this.lnveto.BorderWidth = 5;
            this.lnveto.Enabled = false;
            this.lnveto.Name = "lnveto";
            this.lnveto.X1 = 7;
            this.lnveto.X2 = 50;
            this.lnveto.Y1 = 475;
            this.lnveto.Y2 = 475;
            // 
            // cbEliminar
            // 
            this.cbEliminar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbEliminar.Location = new System.Drawing.Point(420, 454);
            this.cbEliminar.Name = "cbEliminar";
            this.cbEliminar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbEliminar.Size = new System.Drawing.Size(81, 32);
            this.cbEliminar.TabIndex = 7;
            this.cbEliminar.Text = "&Eliminar";
            this.cbEliminar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbEliminar.Click += new System.EventHandler(this.cbEliminar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(506, 454);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 8;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCerrar.Location = new System.Drawing.Point(592, 454);
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(87, 32);
            this.cbCerrar.TabIndex = 9;
            this.cbCerrar.Text = "&Cerrar";
            this.cbCerrar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // frmPersonal
            //            
            this.frmPersonal.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;            
            this.frmPersonal.Controls.Add(this.cbA�adir);
            this.frmPersonal.Controls.Add(this.cbLimpiar);
            this.frmPersonal.Controls.Add(this.tbApellido1);
            this.frmPersonal.Controls.Add(this.tbApellido2);
            this.frmPersonal.Controls.Add(this.tbNombre);
            this.frmPersonal.Controls.Add(this.tbNIFOtrosDocDP);
            this.frmPersonal.Controls.Add(this.LblDNINIF);
            this.frmPersonal.Controls.Add(this.lbApellido1);
            this.frmPersonal.Controls.Add(this.lbApellido2);
            this.frmPersonal.Controls.Add(this.lbNombre);
            this.frmPersonal.HeaderText = "";
            this.frmPersonal.Location = new System.Drawing.Point(4, 348);
            this.frmPersonal.Name = "frmPersonal";
            this.frmPersonal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmPersonal.Size = new System.Drawing.Size(676, 98);
            this.frmPersonal.TabIndex = 13;
            // 
            // cbA�adir
            // 
            this.cbA�adir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbA�adir.Location = new System.Drawing.Point(525, 68);
            this.cbA�adir.Name = "cbA�adir";
            this.cbA�adir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbA�adir.Size = new System.Drawing.Size(70, 24);
            this.cbA�adir.TabIndex = 5;
            this.cbA�adir.Text = "&A�adir";
            this.cbA�adir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbA�adir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbA�adir.Click += new System.EventHandler(this.cbA�adir_Click);
            // 
            // cbLimpiar
            // 
            this.cbLimpiar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbLimpiar.Location = new System.Drawing.Point(600, 68);
            this.cbLimpiar.Name = "cbLimpiar";
            this.cbLimpiar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbLimpiar.Size = new System.Drawing.Size(70, 24);
            this.cbLimpiar.TabIndex = 6;
            this.cbLimpiar.Text = "&Cancelar";
            this.cbLimpiar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbLimpiar.Click += new System.EventHandler(this.cbLimpiar_Click);
            // 
            // tbApellido1
            // 
            this.tbApellido1.AcceptsReturn = true;
            this.tbApellido1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbApellido1.Location = new System.Drawing.Point(421, 17);
            this.tbApellido1.MaxLength = 35;
            this.tbApellido1.Name = "tbApellido1";
            this.tbApellido1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbApellido1.Size = new System.Drawing.Size(249, 20);
            this.tbApellido1.TabIndex = 2;
            this.tbApellido1.Enter += new System.EventHandler(this.tbApellido1_Enter);
            this.tbApellido1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellido1_KeyPress);
            // 
            // tbApellido2
            // 
            this.tbApellido2.AcceptsReturn = true;
            this.tbApellido2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbApellido2.Location = new System.Drawing.Point(79, 42);
            this.tbApellido2.MaxLength = 35;
            this.tbApellido2.Name = "tbApellido2";
            this.tbApellido2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbApellido2.Size = new System.Drawing.Size(246, 20);
            this.tbApellido2.TabIndex = 3;
            this.tbApellido2.Enter += new System.EventHandler(this.tbApellido2_Enter);
            this.tbApellido2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellido2_KeyPress);
            // 
            // tbNombre
            // 
            this.tbNombre.AcceptsReturn = true;
            this.tbNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNombre.Location = new System.Drawing.Point(79, 17);
            this.tbNombre.MaxLength = 35;
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNombre.Size = new System.Drawing.Size(246, 20);
            this.tbNombre.TabIndex = 1;
            this.tbNombre.Enter += new System.EventHandler(this.tbNombre_Enter);
            this.tbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombre_KeyPress);
            // 
            // tbNIFOtrosDocDP
            // 
            this.tbNIFOtrosDocDP.AllowPromptAsInput = false;
            this.tbNIFOtrosDocDP.Location = new System.Drawing.Point(421, 43);
            this.tbNIFOtrosDocDP.Name = "tbNIFOtrosDocDP";
            this.tbNIFOtrosDocDP.PromptChar = ' ';
            this.tbNIFOtrosDocDP.Size = new System.Drawing.Size(96, 20);
            this.tbNIFOtrosDocDP.TabIndex = 4;
            this.tbNIFOtrosDocDP.Enter += new System.EventHandler(this.tbNIFOtrosDocDP_Enter);
            // 
            // LblDNINIF
            // 
            this.LblDNINIF.Cursor = System.Windows.Forms.Cursors.Default;
            this.LblDNINIF.Location = new System.Drawing.Point(358, 46);
            this.LblDNINIF.Name = "LblDNINIF";
            this.LblDNINIF.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblDNINIF.Size = new System.Drawing.Size(47, 18);
            this.LblDNINIF.TabIndex = 18;
            this.LblDNINIF.Text = "DNI/NIF";
            // 
            // lbApellido1
            // 
            this.lbApellido1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbApellido1.Location = new System.Drawing.Point(356, 18);
            this.lbApellido1.Name = "lbApellido1";
            this.lbApellido1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbApellido1.Size = new System.Drawing.Size(59, 18);
            this.lbApellido1.TabIndex = 16;
            this.lbApellido1.Text = "Apellido 1:";
            // 
            // lbApellido2
            // 
            this.lbApellido2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbApellido2.Location = new System.Drawing.Point(14, 43);
            this.lbApellido2.Name = "lbApellido2";
            this.lbApellido2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbApellido2.Size = new System.Drawing.Size(59, 18);
            this.lbApellido2.TabIndex = 15;
            this.lbApellido2.Text = "Apellido 2:";
            // 
            // lbNombre
            // 
            this.lbNombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNombre.Location = new System.Drawing.Point(14, 18);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNombre.Size = new System.Drawing.Size(50, 18);
            this.lbNombre.TabIndex = 14;
            this.lbNombre.Text = "Nombre:";
            // 
            // FrmPaciente
            // 
            this.FrmPaciente.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmPaciente.Controls.Add(this.LbPaciente);
            this.FrmPaciente.Controls.Add(this.LbDPaciente);
            this.FrmPaciente.HeaderText = "";
            this.FrmPaciente.Location = new System.Drawing.Point(4, -2);
            this.FrmPaciente.Name = "FrmPaciente";
            this.FrmPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmPaciente.Size = new System.Drawing.Size(676, 37);
            this.FrmPaciente.TabIndex = 10;
            // 
            // LbPaciente
            // 
            this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbPaciente.Location = new System.Drawing.Point(59, 14);
            this.LbPaciente.Name = "LbPaciente";
            this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbPaciente.Size = new System.Drawing.Size(51, 18);
            this.LbPaciente.TabIndex = 12;
            this.LbPaciente.Text = "Paciente:";
            // 
            // LbDPaciente
            // 
            this.LbDPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbDPaciente.Location = new System.Drawing.Point(114, 12);
            this.LbDPaciente.Name = "LbDPaciente";
            this.LbDPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbDPaciente.Size = new System.Drawing.Size(2, 2);
            this.LbDPaciente.TabIndex = 11;
            // 
            // Lbveto
            // 
            this.Lbveto.Cursor = System.Windows.Forms.Cursors.Default;
            this.Lbveto.Location = new System.Drawing.Point(61, 466);
            this.Lbveto.Name = "Lbveto";
            this.Lbveto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Lbveto.Size = new System.Drawing.Size(210, 18);
            this.Lbveto.TabIndex = 17;
            this.Lbveto.Text = "Personas vetadas en el anterior episodio.";
            // 
            // FrmPersonasNoImpJust
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(685, 493);
            this.Controls.Add(this.SprPerSinJust);
            this.Controls.Add(this.cbEliminar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cbCerrar);
            this.Controls.Add(this.frmPersonal);
            this.Controls.Add(this.FrmPaciente);
            this.Controls.Add(this.Lbveto);
            this.Controls.Add(ShapeContainer1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(12, 65);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPersonasNoImpJust";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Personas a las que no se les permite imprimir justificante.";
            this.Closed += new System.EventHandler(this.FrmPersonasNoImpJust_Closed);
            this.Load += new System.EventHandler(this.FrmPersonasNoImpJust_Load);
            this.Resize += new System.EventHandler(this.FrmPersonasNoImpJust_Resize);
            this.frmPersonal.ResumeLayout(false);
            this.FrmPaciente.ResumeLayout(false);
            this.ResumeLayout(false);
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Personas a las que no se les permite imprimir justificante.";
            this.Closed += new System.EventHandler(this.FrmPersonasNoImpJust_Closed);
            this.Load += new System.EventHandler(this.FrmPersonasNoImpJust_Load);
            this.Resize += new System.EventHandler(this.FrmPersonasNoImpJust_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.cbEliminar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPersonal)).EndInit();
            this.frmPersonal.ResumeLayout(false);
            this.frmPersonal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbA�adir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbLimpiar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LblDNINIF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbApellido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbApellido2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmPaciente)).EndInit();
            this.FrmPaciente.ResumeLayout(false);
            this.FrmPaciente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbveto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}