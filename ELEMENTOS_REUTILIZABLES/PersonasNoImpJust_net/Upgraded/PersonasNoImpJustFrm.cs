using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace PersonasNoImpJust
{
	public partial class FrmPersonasNoImpJust
		: Telerik.WinControls.UI.RadForm
	{

		SqlConnection Rc = null;
		string gUsuario = String.Empty;
		string stGidenpac = String.Empty;

		string stTiposervicio = String.Empty;
		int iA�oRegistro = 0;
		int lNumeroRegistro = 0;

		private const string stFORMATO_NIF = "########?";
		float vAncho = 0;
		float vLargo = 0;
		int vlposy = 0;
		public FrmPersonasNoImpJust()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int iFilas = 0;
			DataSet RrDatos = null;
			string stSql = String.Empty;
			System.DateTime dFechaMecaniz = DateTime.FromOADate(0);

			try
			{
				dFechaMecaniz = DateTime.Now;
				stSql = "Delete DPERSJUS WHERE itiposer = '" + stTiposervicio + "' and ganoregi=  " + iA�oRegistro.ToString() + " and gnumregi= " + lNumeroRegistro.ToString();
				SqlCommand tempCommand = new SqlCommand(stSql, Rc);
				tempCommand.ExecuteNonQuery();
				iFilas = 1;
				stSql = "select * from DPERSJUS WHERE itiposer = '" + stTiposervicio + "' and ganoregi=  " + iA�oRegistro.ToString() + " and gnumregi= " + lNumeroRegistro.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Rc);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				int tempForVar = SprPerSinJust.MaxRows;
				for (iFilas = 1; iFilas <= tempForVar; iFilas++)
				{
					SprPerSinJust.Row = iFilas;
					
					RrDatos.AddNew();
					
					RrDatos.Tables[0].Rows[iFilas]["itiposer"] = stTiposervicio;
					
					RrDatos.Tables[0].Rows[iFilas]["ganoregi"] = iA�oRegistro;
					
					RrDatos.Tables[0].Rows[iFilas]["gnumregi"] = lNumeroRegistro;
					
					RrDatos.Tables[0].Rows[iFilas]["nnumorde"] = iFilas;
					
					RrDatos.Tables[0].Rows[iFilas]["gidenpac"] = stGidenpac;

					SprPerSinJust.Col = 1; //Nombre
					
					RrDatos.Tables[0].Rows[iFilas]["dnombper"] = SprPerSinJust.Text.Trim();

					SprPerSinJust.Col = 2; //Apellido 1
					
					RrDatos.Tables[0].Rows[iFilas]["dape1per"] = SprPerSinJust.Text.Trim();

					SprPerSinJust.Col = 3; //Apellido 2
					
					RrDatos.Tables[0].Rows[iFilas]["dape2per"] = SprPerSinJust.Text.Trim();

					SprPerSinJust.Col = 4; //dni
					
					RrDatos.Tables[0].Rows[iFilas]["ndninifp"] = SprPerSinJust.Text.Trim();

					
					RrDatos.Tables[0].Rows[iFilas]["fmecaniz"] = dFechaMecaniz.ToString("dd/MM/yyyy HH:mm:ss");

					
					string tempQuery = RrDatos.Tables[0].TableName;
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
					tempAdapter_2.Update(RrDatos, RrDatos.Tables[0].TableName);
				}
				cbCerrar_Click(cbCerrar, new EventArgs());
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("No Imp. Just.", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "cbAceptar_Click:PersonasNoImpJust.dll", ex);
			}
		}

		private void cbA�adir_Click(Object eventSender, EventArgs eventArgs)
		{
			Mensajes.ClassMensajes clase_mensaje = null;

			if (tbNombre.Text.Trim() == "")
			{
				clase_mensaje = new Mensajes.ClassMensajes();
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{lbNombre.Text};
				clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam,Rc, tempRefParam2);
				clase_mensaje = null;
				tbNombre.Focus();
				return;
			}
			if (tbApellido1.Text.Trim() == "")
			{
				clase_mensaje = new Mensajes.ClassMensajes();
				short tempRefParam3 = 1040;
                string[] tempRefParam4 = new string[] { lbApellido1.Text};
				clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion,Serrores.vAyuda, tempRefParam3, Rc,tempRefParam4);
				clase_mensaje = null;
				tbApellido1.Focus();
				return;
			}
			//Si no hay selecci�n de persona, ....
			if (SprPerSinJust.SelectedRows.Count <= 0)
			{
				//...Comprobamos que no exista dicha persona ya en la rejilla.
				int tempForVar = SprPerSinJust.MaxRows;
				for (int iCont = 1; iCont <= tempForVar; iCont++)
				{
					SprPerSinJust.Row = iCont;
					SprPerSinJust.Col = 1;
					if (SprPerSinJust.Text.Trim().ToUpper() == tbNombre.Text.Trim().ToUpper())
					{
						SprPerSinJust.Col = 2;
						if (SprPerSinJust.Text.Trim().ToUpper() == tbApellido1.Text.Trim().ToUpper())
						{
							SprPerSinJust.Col = 3;
							if (SprPerSinJust.Text.Trim().ToUpper() == tbApellido2.Text.Trim().ToUpper())
							{
								SprPerSinJust.Col = 4;
								if (SprPerSinJust.Text.Trim().ToUpper() == tbNIFOtrosDocDP.Text.Trim().ToUpper())
								{
									clase_mensaje = new Mensajes.ClassMensajes();
									short tempRefParam5 = 1080;
                                    string[] tempRefParam6 = new string[] {"Pesona"};
									clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Rc, tempRefParam6);
									clase_mensaje = null;
									return;
								}
							}
						}
					}
				}
				//A�adimos en la rejilla la nueva persona.
				SprPerSinJust.MaxRows++;
				SprPerSinJust.Row = SprPerSinJust.MaxRows;
			}
			SprPerSinJust.Col = 1;
			SprPerSinJust.Text = tbNombre.Text.Trim().ToUpper();
			SprPerSinJust.Col = 2;
			SprPerSinJust.Text = tbApellido1.Text.Trim().ToUpper();
			SprPerSinJust.Col = 3;
			SprPerSinJust.Text = tbApellido2.Text.Trim().ToUpper();
			SprPerSinJust.Col = 4;
			SprPerSinJust.Text = tbNIFOtrosDocDP.Text.Trim().ToUpper();

		}

		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}


		private void cbEliminar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (SprPerSinJust.MaxRows == 0 || SprPerSinJust.SelectedRows.Count <= 0)
			{
				return;
			}
			int lFila = SprPerSinJust.Row - 1;
			if (lFila == 0)
			{
				lFila = 1;
			}
            //camaya todo_x_4
			//SprPerSinJust.Action = 5;
			SprPerSinJust.MaxRows--;
			cbLimpiar_Click(cbLimpiar, new EventArgs());
			if (SprPerSinJust.MaxRows == 0)
			{
				return;
			}
			SprPerSinJust_CellClick(SprPerSinJust, null);

		}

		private void cbLimpiar_Click(Object eventSender, EventArgs eventArgs)
		{
			tbNombre.Text = "";
			tbApellido1.Text = "";
			tbApellido2.Text = "";
			if (tbNIFOtrosDocDP.Mask == stFORMATO_NIF)
			{
				tbNIFOtrosDocDP.Text = "         ";
			}
			else
			{
				tbNIFOtrosDocDP.Text = "";
			}            
            SprPerSinJust.ReadOnly = true;
			tbNombre.Focus();
		}
		
		private void FrmPersonasNoImpJust_Load(Object eventSender, EventArgs eventArgs)
		{
			vAncho = (float) (this.Width * 15);
			vLargo = (float) (this.Height * 15);

			Serrores.vAyuda = Path.GetDirectoryName(Application.ExecutablePath) + "\\Ayuda_dlls.hlp";
			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(Rc);		
			
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			llenar_grid();
		}



		private void llenar_grid()
		{
			int iFilas = 0;
			DataSet RrDatos = null;
			string stSql = String.Empty;
			Color vstColor = new Color();


			try
			{
				SprPerSinJust.MaxRows = 0;
				iFilas = 1;
				vstColor = Color.Black;
				stSql = "select * from DPERSJUS WHERE itiposer = '" + stTiposervicio + "' and ganoregi=  " + iA�oRegistro.ToString() + " and gnumregi= " + lNumeroRegistro.ToString() + " order by nnumorde";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Rc);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					stSql = "select * from DPERSJUS where gidenpac ='" + stGidenpac + "' and fmecaniz in ( select max(fmecaniz) from DPERSJUS where gidenpac ='" + stGidenpac + "' group by gidenpac) ";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Rc);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);
					vstColor = lnveto.BorderColor;
				}
				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					//si hay datos se cargan en el grid
					SprPerSinJust.MaxRows = RrDatos.Tables[0].Rows.Count;
					SprPerSinJust.Row = iFilas;

					foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
					{
						SprPerSinJust.Row = iFilas;

						SprPerSinJust.Col = 1; //Nombre
						SprPerSinJust.Text = Convert.ToString(iteration_row["dnombper"]).Trim();
						SprPerSinJust.foreColor = vstColor;

						SprPerSinJust.Col = 2; //Apellido 1
						SprPerSinJust.Text = Convert.ToString(iteration_row["dape1per"]).Trim();
						SprPerSinJust.foreColor = vstColor;

						SprPerSinJust.Col = 3; //Apellido 2
						SprPerSinJust.Text = Convert.ToString(iteration_row["dape2per"]).Trim();
						SprPerSinJust.foreColor = vstColor;

						SprPerSinJust.Col = 4; //dni
						SprPerSinJust.Text = Convert.ToString(iteration_row["ndninifp"]).Trim();
						SprPerSinJust.foreColor = vstColor;
						iFilas++;
					}

				}
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("No Imp. Just.", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "llenar_grid:PersonasNoImpJust.dll", ex);
			}
		}
		public void REFERENCIAS(SqlConnection iConnet, string stParam1, int iParam2, int lParam3, string usuario, string stIdenPac)
		{

			Rc = iConnet;
			Serrores.AjustarRelojCliente(Rc);
			gUsuario = usuario;
			stTiposervicio = stParam1;
			iA�oRegistro = iParam2;
			lNumeroRegistro = lParam3;
			stGidenpac = stIdenPac;
		}

		private bool isInitializingComponent;
		private void FrmPersonasNoImpJust_Resize(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			try
			{

				//If Me.Width < 3400 Then Exit Sub
				if (((float) (this.Height * 15)) < 3700 || ((float) (this.Width * 15)) < 9300)
				{
					return;
				}
				//Marco del paciente.
				FrmPaciente.Width = (int) (FrmPaciente.Width + (this.Width - vAncho / 15));
				LbPaciente.Left = (int) (LbPaciente.Left + ((this.Width - vAncho / 15) * (1 / 2f)));
				LbDPaciente.Left = (int) (LbDPaciente.Left + ((this.Width - vAncho / 15) * (1 / 2f)));


				//Rejilla.
				SprPerSinJust.Width = (int) (SprPerSinJust.Width + (this.Width - vAncho / 15));
				SprPerSinJust.Height = (int) (SprPerSinJust.Height + (this.Height - vLargo / 15));
				SprPerSinJust.SetColWidth(1, (float) ((((float) (SprPerSinJust.Width * 15)) - 2 * SprPerSinJust.GetColWidth(0)) / 4)); //SprPerSinJust.ColWidth(1) + ((Me.Width - vAncho) / 4)
				SprPerSinJust.SetColWidth(2, (float) ((((float) (SprPerSinJust.Width * 15)) - 2 * SprPerSinJust.GetColWidth(0)) / 4)); //SprPerSinJust.ColWidth(2) + ((Me.Width - vAncho) / 4)
				SprPerSinJust.SetColWidth(3, (float) ((((float) (SprPerSinJust.Width * 15)) - 2 * SprPerSinJust.GetColWidth(0)) / 4)); //SprPerSinJust.ColWidth(3) + ((Me.Width - vAncho) / 4)
				SprPerSinJust.SetColWidth(4, (float) ((((float) (SprPerSinJust.Width * 15)) - 2 * SprPerSinJust.GetColWidth(0)) / 4)); //SprPerSinJust.ColWidth(4) + ((Me.Width - vAncho) / 4)

				//Datos de las personas
				frmPersonal.Top = (int) (frmPersonal.Top + (this.Height - vLargo / 15));
				frmPersonal.Width = (int) (frmPersonal.Width + (this.Width - vAncho / 15));

				tbNombre.Width = (int) (tbNombre.Width + ((this.Width - vAncho / 15) * (1 / 2f)));

				tbApellido2.Width = (int) (tbApellido2.Width + ((this.Width - vAncho / 15) * (1 / 2f)));

				lbApellido1.Left = (int) (lbApellido1.Left + ((this.Width - vAncho / 15) * (1 / 2f)));
				tbApellido1.Left = (int) (tbApellido1.Left + ((this.Width - vAncho / 15) * (1 / 2f)));
				tbApellido1.Width = (int) (tbApellido1.Width + ((this.Width - vAncho / 15) * (1 / 2f)));

				cbLimpiar.Left = (int) (cbLimpiar.Left + (this.Width - vAncho / 15));
				cbA�adir.Left = (int) (cbA�adir.Left + (this.Width - vAncho / 15));

				//    rbDNIOtroDocDP(0).Left = rbDNIOtroDocDP(0).Left + (Me.Width - vAncho)
				//    rbDNIOtroDocDP(1).Left = rbDNIOtroDocDP(1).Left + (Me.Width - vAncho)
				tbNIFOtrosDocDP.Left = (int) (tbNIFOtrosDocDP.Left + ((this.Width - vAncho / 15) * (1 / 2f)));
				LblDNINIF.Left = (int) (LblDNINIF.Left + ((this.Width - vAncho / 15) * (1 / 2f)));

				//Botones de administraci�n de la pantalla
				cbEliminar.Left = (int) (cbEliminar.Left + (this.Width - vAncho / 15));
				cbEliminar.Top = (int) (cbEliminar.Top + (this.Height - vLargo / 15));

				cbAceptar.Left = (int) (cbAceptar.Left + (this.Width - vAncho / 15));
				cbAceptar.Top = (int) (cbAceptar.Top + (this.Height - vLargo / 15));

				cbCerrar.Left = (int) (cbCerrar.Left + (this.Width - vAncho / 15));
				cbCerrar.Top = (int) (cbCerrar.Top + (this.Height - vLargo / 15));


				vAncho = (float) (this.Width * 15);
				vLargo = (float) (this.Height * 15);
			}
			catch
			{
			}
		}


		private void SprPerSinJust_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
            int Col = 0;
            int Row = 0;
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = SprPerSinJust.Col;
                Row = SprPerSinJust.Row;

            }
            if (Row > 0)
			{               
                if ((SprPerSinJust.ReadOnly == true || SprPerSinJust.SelectionMode == Telerik.WinControls.UI.GridViewSelectionMode.FullRowSelect) && SprPerSinJust.Row != Row)
                {
                    SprPerSinJust.Row = Row;
					SprPerSinJust.Col = 1;
					SprPerSinJust.Action = 0;
                    //UPGRADE_WARNING: (6021) Casting 'int' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
                    //camaya todo_x_4
                    //SprPerSinJust.OperationMode = (FarPoint.Win.Spread.OperationMode) (((int) FarPoint.Win.Spread.OperationMode.ReadOnly) + ((int) FarPoint.Win.Spread.OperationMode.RowMode));
					tbNombre.Text = SprPerSinJust.Text;
					SprPerSinJust.Col = 2;
					tbApellido1.Text = SprPerSinJust.Text;
					SprPerSinJust.Col = 3;
					tbApellido2.Text = SprPerSinJust.Text;
					SprPerSinJust.Col = 4;
					tbNIFOtrosDocDP.Text = SprPerSinJust.Text;
				}
				else
				{
					cbLimpiar_Click(cbLimpiar, new EventArgs());
				}

			}

			//Ordenamos las columnas.
			if (Row == 0 && Col > 1)
			{
				SprPerSinJust.SortBy = 0; //Ordeno por columnas
				SprPerSinJust.SetSortKey(1, Col); //Ordeno por la columna seleccionada
				if (Col == 2)
				{
					SprPerSinJust.SetSortKey(2, 3); //Si se ha seleccionado la de Nombre, luego se ordena por apellido 1 y apellido 2
					SprPerSinJust.SetSortKey(3, 4);
				}
				else if (Col == 3)
				{ 
					SprPerSinJust.SetSortKey(2, 4); //Si se ha seleccionado la de apellido 1, luego se ordena por apellido 2 y Nombre
					SprPerSinJust.SetSortKey(3, 2);
				}
				else if (Col == 4)
				{ 
					SprPerSinJust.SetSortKey(2, 3); //Si se ha seleccionado la de apellido 2, luego se ordena por apellido 1 y Nombre
					SprPerSinJust.SetSortKey(3, 2);
				}
				else if (Col == 5)
				{ 
					SprPerSinJust.SetSortKey(2, 2); //Si se ha seleccionado la de DNI, luego se ordena por apellido 1 y apellido 2
					SprPerSinJust.SetSortKey(3, 3);
				}
				SprPerSinJust.SetSortKeyOrder(1, (UpgradeHelpers.Spread.SortKeyOrderConstants) 1); //SORT_KEY_ORDER_ASC
				SprPerSinJust.SetSortKeyOrder(2, (UpgradeHelpers.Spread.SortKeyOrderConstants) 1);
				SprPerSinJust.SetSortKeyOrder(3, (UpgradeHelpers.Spread.SortKeyOrderConstants) 1);
				//------------------
				SprPerSinJust.Row = 0;
				SprPerSinJust.Col = 1;
				SprPerSinJust.Row2 = SprPerSinJust.MaxRows;
				SprPerSinJust.Col2 = SprPerSinJust.MaxCols;
				//SprPerSinJust.Action = 25;

				SprPerSinJust.Col = Col;
				SprPerSinJust.Row = 1;
				SprPerSinJust.Action = 0;
			}

		}

		private void SprPerSinJust_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			//Si pulsamos la flecha hacia arriba...
			if (KeyCode == ((int) Keys.Up))
			{
				//Si no hemos llegado a la primera fila
				if (SprPerSinJust.ActiveRowIndex > 1)
				{
					SprPerSinJust_CellClick(SprPerSinJust, null);
				} //Hacemos el click de la anterior
				//Para que no pase a la siguiente ya que en la instrucci�n anterior se ha simulado
				KeyCode = 0;
			}
			//Si pulsamos la flecha hacia abajo...
			if (KeyCode == ((int) Keys.Down))
			{
				//Si no hemos llegado a la �ltima fila
				//if (SprPerSinJust.ActiveRowIndex < SprPerSinJust.GetLastNonEmptyRow(FarPoint.Win.Spread.NonEmptyItemFlag.Data))
				if (SprPerSinJust.ActiveRowIndex < SprPerSinJust.Rows.Count)
                    {
					SprPerSinJust_CellClick(SprPerSinJust, null);
				} //Hacemos el click de la siguiente
				//Para que no pase a la siguiente ya que en la instrucci�n anterior se ha simulado
				KeyCode = 0;
			}

		}

		private void SprPerSinJust_MouseDown(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float x = (float) (eventArgs.X * 15);
			float y = (float) (eventArgs.Y * 15);
			vlposy = Convert.ToInt32(y);
		}

		private void SprPerSinJust_MouseUp(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float x = (float) (eventArgs.X * 15);
			float y = (float) (eventArgs.Y * 15);
            
            if (Button == 1 && (SprPerSinJust.ReadOnly == true || SprPerSinJust.SelectionMode == Telerik.WinControls.UI.GridViewSelectionMode.FullRowSelect) && vlposy != y) 
			{
				SprPerSinJust_CellClick(SprPerSinJust, null);
			}


		}

		private void tbApellido1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbApellido1.SelectionStart = 0;
			tbApellido1.SelectionLength = tbApellido1.Text.Trim().Length;
		}

		private void tbApellido1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
			}
			if (KeyAscii == 39)
			{
				KeyAscii = 180;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void tbApellido2_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbApellido2.SelectionStart = 0;
			tbApellido2.SelectionLength = tbApellido2.Text.Trim().Length;
		}

		private void tbApellido2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
			}
			if (KeyAscii == 39)
			{
				KeyAscii = 180;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		private void tbNIFOtrosDocDP_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNIFOtrosDocDP.SelectionStart = 0;
			tbNIFOtrosDocDP.SelectionLength = tbNIFOtrosDocDP.Text.Trim().Length;
		}

		private void tbNombre_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNombre.SelectionStart = 0;
			tbNombre.SelectionLength = tbNombre.Text.Trim().Length;
		}

		private void tbNombre_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
			}
			if (KeyAscii == 39)
			{
				KeyAscii = 180;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		//Un NIF si se trata de de 8 d�gitos y una letra. El DNI se compone de 8 caracteres en BBDD y el NIF se compone
		//de 9 caracteres en Base de Datos.
		//El campo al que se hace referencia en Base de Datos es el campo "NDNINIFP" (Char(9)) de la Tabla DPACIENT.
		//Por si acaso, la longitud (que es 9 actualmente), se pasa como un argumento. Es decir, al argumento "iLongitud"
		//se le pasar� un 9.
		public bool EsDNI_NIF(string stCadena, int iLongitud)
		{
			FixedLengthString stCaracter = new FixedLengthString(1);

			//si la longitud de la cadena es menor de 8, no se trata de un DNI ni de un NIF, puesto que en Base de Datos
			//se guarda el DNI con 8 caracteres y el NIF con 9 caracteres (campo "NDNINIFP" (Char(9)) de la Tabla DPACIENT).
			//Tambi�n, por si acaso, se a�ade la condici�n de que la cadena tenga una longitud mayor de 9, en cuyo caso
			//tampoco ser�a un NIF ni un DNI.
			if ((stCadena.Trim().Length < iLongitud - 1) || (stCadena.Trim().Length > iLongitud))
			{
				return false;
			}
			//(16/05/2006)para ver si es NIF/DNI o no, lo mejor es plantearlo del siguiente modo:
			//Caracteres del 1 al 8: Debe ser un d�gito;
			//Car�cter 9: Debe ser una letra (siempre y cuando haya car�cter 9)
			for (int iContador = 1; iContador <= stCadena.Trim().Length; iContador++)
			{
				stCaracter.Value = stCadena.Trim().Substring(iContador - 1, Math.Min(1, stCadena.Trim().Length - (iContador - 1))); //se obtiene el car�cter actual
				if (iContador <= (iLongitud - 1))
				{
					double dbNumericTemp = 0;
					if (!Double.TryParse(stCaracter.Value, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
					{
						return false;
					}
				}
				else if (iContador == iLongitud)
				{ 
					if (!((Strings.Asc(stCaracter.Value[0]) > 64 && Strings.Asc(stCaracter.Value[0]) < 91) || (Strings.Asc(stCaracter.Value[0]) > 96 && Strings.Asc(stCaracter.Value[0]) < 123)))
					{
						return false;
					}
				}
			}
			//si no se trata de ninguno de los casos anteriores, se trata de un DNI o de un NIF
			return true;
		}
		private void FrmPersonasNoImpJust_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}