using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLTextosSql
{
	public class TextosSql
	{


		public string DevuelveTexto(string TipoBd, string Modulo, string Formulario, string FunoProc, int Norden, object[] MatrizParametros, SqlConnection conexion)
		{
			string result = String.Empty;
			string sql = String.Empty;
			int nParametros = 0;
			string cadenaIni = String.Empty;
			string cadenaFin = String.Empty;
			string cadena = String.Empty;
			StringBuilder parametro = new StringBuilder();
			string separador = String.Empty;
			int inicio = 0;
			int fin = 0;
			int nparam = 0;
			DataSet cursor = null;

			try
			{

				sql = "select TEXTOSQL,NPARAMER from SQLTEXBD WHERE BASEDATOS='" + TipoBd + "' and" + " MODULO='" + Modulo + "' and FORMULARIO='" + Formulario + "' and " + " FUNOPROC='" + FunoProc + "' and " + " NORDEN=" + Norden.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, conexion);
				cursor = new DataSet();
				tempAdapter.Fill(cursor);
				if (cursor.Tables[0].Rows.Count == 0)
				{
					cursor.Close();
					return "No existe la sentencia sql en la Tabla SQLTEXBD";
				}
				else
				{
					
					cadena = Convert.ToString(cursor.Tables[0].Rows[0]["TEXTOSQL"]);
				
					nparam = Convert.ToInt32(cursor.Tables[0].Rows[0]["NPARAMER"]);
					
					cursor.Close();
					nParametros = MatrizParametros.GetUpperBound(0);
					if (nParametros != nparam)
					{
						return "Error en matriz de parámetros";
					}
					else
					{
						for (int x = 1; x <= nParametros; x++)
						{
							switch(TipoParametro( cadena, x))
							{
								case 0 : 
									result = "Error parametro erroneo"; 
									break;
								case 1 : 
									inicio = (cadena.IndexOf(TextoParametro(cadena, x, "inicio")) + 1); 
									fin = (cadena.IndexOf(TextoParametro( cadena, x, "fin")) + 1); 
									cadenaIni = cadena.Substring(0, Math.Min(inicio - 1, cadena.Length)); 
									cadenaFin = cadena.Substring(fin + TextoParametro(cadena, x, "fin").Length - 1); 
									
									parametro = new StringBuilder(Convert.ToString(MatrizParametros[x])); 
									cadena = cadenaIni + parametro.ToString() + cadenaFin; 
									break;
								case 2 : 
									inicio = (cadena.IndexOf(TextoParametro( cadena, x, "inicio")) + 1); 
									fin = (cadena.IndexOf(TextoParametro( cadena, x, "fin")) + 1); 
									cadenaIni = cadena.Substring(0, Math.Min(inicio - 1, cadena.Length)); 
									cadenaFin = cadena.Substring(fin + TextoParametro( cadena, x, "fin").Length - 1); 
									separador = cadenaIni.Substring(cadenaIni.Length - 1); 
									if (separador == " ")
									{
										separador = "";
										cadenaIni = cadenaIni.Substring(0, Math.Min(inicio - 2, cadenaIni.Length));
										cadenaFin = cadenaFin.Substring(1);
									} 
									parametro = new StringBuilder(""); 
									for (int y = 1; y <= ((Array) MatrizParametros[x]).GetUpperBound(0); y++)
									{
										
										parametro.Append(Convert.ToString(((Array) MatrizParametros[x]).GetValue(y)) + separador + "," + separador);
									} 
									if (separador != "")
									{
										parametro = new StringBuilder(parametro.ToString().Substring(0, Math.Min(parametro.ToString().Length - 3, parametro.ToString().Length)));
									}
									else
									{
										parametro = new StringBuilder(parametro.ToString().Substring(0, Math.Min(parametro.ToString().Length - 1, parametro.ToString().Length)));
									} 
									cadena = cadenaIni + parametro.ToString() + cadenaFin; 
									break;
								case 3 : 
									inicio = (cadena.IndexOf(TextoParametro( cadena, x, "inicio")) + 1); 
									fin = (cadena.IndexOf(TextoParametro( cadena, x, "fin")) + 1); 
									cadenaIni = cadena.Substring(0, Math.Min(inicio - 1, cadena.Length)); 
									cadenaFin = cadena.Substring(fin + 3 + TextoParametro( cadena, x, "fin").Length - 1); 
									separador = cadenaIni.Substring(cadenaIni.Length - 1); 
									if (separador == " ")
									{
										separador = "";
										cadenaIni = cadenaIni.Substring(0, Math.Min(inicio - 2, cadenaIni.Length));
										//                        cadenaFin = Mid(cadenaFin, 2)
									} 
									parametro = new StringBuilder(""); 
									for (int y = 1; y <= ((Array) MatrizParametros[x]).GetUpperBound(0); y++)
									{

										parametro.Append(Convert.ToString(((Array) MatrizParametros[x]).GetValue(y)) + separador);
										if (y + 1 <= ((Array) MatrizParametros[x]).GetUpperBound(0))
										{
											parametro.Append(" OR " + ObtenerCampo( cadena, x) + separador);
										}
									} 
									//            parametro = Mid(parametro, 1, Len(parametro) - 3) 
									cadena = cadenaIni + parametro.ToString() + cadenaFin; 
									break;
								case 4 : 
									inicio = (cadena.IndexOf(TextoParametro( cadena, x, "inicio")) + 1); 
									fin = (cadena.IndexOf(TextoParametro( cadena, x, "fin")) + 1); 
									cadenaIni = cadena.Substring(0, Math.Min(inicio - 1, cadena.Length)); 
									cadenaFin = cadena.Substring(fin + 3 + TextoParametro( cadena, x, "fin").Length - 1); 
									separador = cadenaIni.Substring(cadenaIni.Length - 1); 
									if (separador == " ")
									{
										separador = "";
										cadenaIni = cadenaIni.Substring(0, Math.Min(inicio - 2, cadenaIni.Length));
										//                        cadenaFin = Mid(cadenaFin, 2)
									} 
									parametro = new StringBuilder(""); 
									for (int y = 1; y <= ((Array) MatrizParametros[x]).GetUpperBound(0); y++)
									{

										parametro.Append(Convert.ToString(((Array) MatrizParametros[x]).GetValue(y)) + separador);
										if (y + 1 <= ((Array) MatrizParametros[x]).GetUpperBound(0))
										{
											parametro.Append(" AND " + ObtenerCampo(cadena, x) + separador);
										}
									} 
									//            parametro = Mid(parametro, 1, Len(parametro) - 3) 
									cadena = cadenaIni + parametro.ToString() + cadenaFin; 
									 
									break;
							}
						}
						return cadena;
					}
				}
			}
			catch
			{
				return "Error";
			}
		}


		private string ObtenerCampo(string cadena, int Nparametro)
		{
			string result = String.Empty;
			int posiIni = 0;
			int posiFin = 0;
			int encontrado = 0;
			try
			{

				result = "";
				posiFin = (cadena.IndexOf(TextoParametro(cadena, Nparametro, "inicio")) + 1);
				posiIni = posiFin;
				encontrado = 0;
				while (encontrado < 2)
				{
					posiIni--;
					if (cadena.Substring(posiIni - 1, Math.Min(1, cadena.Length - (posiIni - 1))) == " ")
					{
						encontrado++;
					}
				}
				return cadena.Substring(posiIni, Math.Min(posiFin - 2 - posiIni, cadena.Length - posiIni));
			}
			catch
			{
				return "Error Campo";
			}
		}


		private int TipoParametro( string cadenaPara, int Nparametro)
		{
			string cadena2 = String.Empty;
			string cadena = "@#" + Conversion.Str(Nparametro).Trim();
			int encontrado = (cadenaPara.IndexOf(cadena) + 1);
			if (encontrado != 0)
			{
				return 1;
			}
			else
			{
				cadena = "@$" + Conversion.Str(Nparametro).Trim();
				encontrado = (cadenaPara.IndexOf(cadena) + 1);
				if (encontrado != 0)
				{
					cadena2 = Conversion.Str(Nparametro).Trim() + "$@OR";
					encontrado = (cadenaPara.IndexOf(cadena2) + 1);
					if (encontrado != 0)
					{
						return 3;
					}
					else
					{
						cadena2 = Conversion.Str(Nparametro).Trim() + "$@AND";
						encontrado = (cadenaPara.IndexOf(cadena2) + 1);
						if (encontrado != 0)
						{
							return 4;
						}
						else
						{
							return 2;
						}
					}
				}
				else
				{
					return 0;
				}
			}
		}

		private string TextoParametro(string cadenaPara, int Nparametro, string PrincipioFin)
		{
			string cadena = "@#" + Conversion.Str(Nparametro).Trim();
			int encontrado = (cadenaPara.IndexOf(cadena) + 1);
			if (encontrado != 0)
			{
				if (PrincipioFin.ToUpper() == "FIN")
				{
					cadena = Conversion.Str(Nparametro).Trim() + "#@";
				}
				return cadena;
			}
			else
			{
				cadena = "@$" + Conversion.Str(Nparametro).Trim();
				encontrado = (cadenaPara.IndexOf(cadena) + 1);
				if (encontrado != 0)
				{
					if (PrincipioFin.ToUpper() == "FIN")
					{
						cadena = Conversion.Str(Nparametro).Trim() + "$@";
					}
					return cadena;
				}
				else
				{
					return "error";
				}
			}
		}
	}
}