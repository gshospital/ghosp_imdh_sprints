using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Telerik.WinControls;
//using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Mensajes
{
    public class ClassMensajes
    {

        public SqlConnection Rc = null;
        public DataSet RrTabla = null;
        public string stmensaje = String.Empty;
        private Control _CtrlMensaje = null;

        public Control CtrlMensaje
        {
            get
            {
                return _CtrlMensaje;
            }
            set
            {
                _CtrlMensaje = value;
            }

        }
         
        public string Sustituir(string varTira, string varBuscar, string varCambiar)
        {

            string varTiraDest = String.Empty;
            //No asumir valores concretos en ninguna de las tiras para que
            //sea m�s gen�rica la sustituci�n o busqueda incluyendo Blancos y nulos.
            int lComienzo = (varTira.IndexOf(varBuscar) + 1);
            if (lComienzo > 0)
            {
                varTiraDest = varTira.Substring(0, Math.Min(lComienzo - 1, varTira.Length)) + " " + varCambiar + " ";
                //Siempre se cumple que: 1 <= lComienzo <= Len(varBuscar) <= Len(varTira)
                varTiraDest = varTiraDest + varTira.Substring(Math.Max(varTira.Length - (varTira.Length + 1 - (lComienzo + varBuscar.Length)), 0));
            }
            else
            {
                //No existe varBuscar en varTira
                varTiraDest = varTira;
            }
            return varTiraDest;
        }

        public DialogResult RespuestaMensaje(string vNomAplicacion, string vAyuda, int icodigo, SqlConnection RcConexion, params string[] varSust)
        {
            //Autor: Jos� Alberto del Castillo
            //Fecha: 07/06/94
            //Modificado por Eusebio Carrasco 11/09/1997
            //Descr: Muestra el Mensaje de error,Codigo, pasado en la llamada y
            //       devuelve la contestaci�n a dicho Mensaje para ser analizada
            //       a partir de la sentencia que hace la llamada y obrar en consecuencia.
            //--->>>>Para ser coherentes, todos los Mensajes explicitos deberian
            //       ser Mensajes codificados. LLamadas recursivas a la propia funci�n
            //       pero sin analizar la Respuesta.
            //       Los contenidos no nulos del paramArray de varSust, etc. se sustituyen en el
            //       mensaje por los literales V1, V2, etc. en el orden establecido en el array.
            int iCtxt = 0; //Se debe usar s�lo n�meros en el contexto de la Ayuda
            int lContador = 0;

            //JFPENA ADAPTACION RADMESSAGEBOX
            MessageBoxButtons vbotones = 0;
            RadMessageIcon vicono = 0;
            MessageBoxDefaultButton vbotondefect = 0;
            int icono = 0;            

            int lIndiceArray = 0; //Se cuentan el n� de par�metros
            foreach (string varIndiceArray in varSust)
            {
                varSust[lIndiceArray] = SustituirDosPuntos(varIndiceArray);
                varSust[lIndiceArray] = varSust[lIndiceArray].Trim();
                lIndiceArray++;
            }

            Rc = RcConexion;
            SqlDataAdapter tempAdapter = new SqlDataAdapter("select * from SMENSAJE where gmensaje =" + "'" + icodigo.ToString().Trim() + "'", Rc);
            RrTabla = new DataSet();
            tempAdapter.Fill(RrTabla);

            if (RrTabla.Tables[0].Rows.Count == 0)
            {
                //Si no hay ning�n registro con ese n� de c�digo
                stmensaje = "C�digo DE ERROR " + icodigo.ToString() + " INEXISTENTE";
                
                return RadMessageBox.Show(stmensaje, vNomAplicacion);
            }
            else
            {
                //JFPENA ADAPTACION RADMESSAGEBOX
                vbotones = (MessageBoxButtons)Convert.ToInt32(RrTabla.Tables[0].Rows[0]["vbotones"]);
                vbotondefect = (MessageBoxDefaultButton)Convert.ToInt32(RrTabla.Tables[0].Rows[0]["vbotondefect"]);
                icono = Convert.ToInt32(RrTabla.Tables[0].Rows[0]["vicono"]);
                
                switch (icono)
                {
                    //Critical 16 Muestra el icono Mensaje cr�tico.
                    //Question 32 Muestra el icono Consulta de advertencia.
                    //Exclamation 48 Muestra el icono Mensaje de advertencia.
                    //Information 64 Muestra el icono Mensaje de informaci�n.

                    case (int)MsgBoxStyle.Critical:
                        vicono = RadMessageIcon.Error;
                        break;
                    case (int)MsgBoxStyle.Question:
                        vicono = RadMessageIcon.Question;
                        break;
                    case (int)MsgBoxStyle.Exclamation:
                        vicono = RadMessageIcon.Exclamation;
                        break;
                    case (int)MsgBoxStyle.Information:
                        vicono = RadMessageIcon.Info;
                        break;

                }               

                stmensaje = Convert.ToString(RrTabla.Tables[0].Rows[0]["gmensaje"]) + " : " + Environment.NewLine;
                stmensaje = stmensaje + Convert.ToString(RrTabla.Tables[0].Rows[0]["dmensaje"]);

                lContador = 0;
                for (lContador = 1; lContador <= lIndiceArray; lContador++)
                {
                    if (!Convert.IsDBNull(varSust[lContador - 1]))
                    {   
                        stmensaje = Sustituir(stmensaje, "V" + Conversion.Str(lContador).Trim(), Convert.ToString(varSust[lContador - 1]));
                    }
                }

                //Montamos el mensaje con la ayuda                

                if (CtrlMensaje != null)
                {
                    return RadMessageBox.Show(CtrlMensaje,stmensaje, vNomAplicacion, vbotones, vicono, vbotondefect);
                }
                else
                {
                    return RadMessageBox.Show(stmensaje, vNomAplicacion, vbotones, vicono, vbotondefect);                    
                }

            }

        }

        public string SustituirDosPuntos(string varTira)
        {
            //Sustituye los dos puntos
            string varTiraDest = String.Empty;

            string varLocalizar = varTira.TrimEnd().Substring(Math.Max(varTira.TrimEnd().Length - 1, 0));
            //Si no encontramos ":" nos vamos
            if (varLocalizar != ":")
            {
                return varTira;
            }

            string varCambio = " ";
            int lComienzo = (varTira.IndexOf(varLocalizar) + 1);
            if (lComienzo > 0)
            {
                varTiraDest = varTira.Substring(0, Math.Min(lComienzo - 1, varTira.Length)) + " " + varCambio + " ";
                //Siempre se cumple que: 1 <= lComienzo <= Len(varBuscar) <= Len(varTira)
                varTiraDest = varTiraDest + varTira.Substring(Math.Max(varTira.Length - (varTira.Length + 1 - (lComienzo + varLocalizar.Length)), 0));
            }
            else
            {
                //No existe ":" en varTira
                varTiraDest = varTira;
            }
            return varTiraDest;
        }
    }
}