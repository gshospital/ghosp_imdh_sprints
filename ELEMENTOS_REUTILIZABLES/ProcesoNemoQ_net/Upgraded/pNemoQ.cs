using Microsoft.VisualBasic;
using System;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ProcesoNemoQ
{
	internal static class pNemoQ
	{


		//[DllImport("user32.dll", EntryPoint = "FindWindowA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int FindWindow([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpClassName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpWindowName);
		//[DllImport("user32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int SetTimer(int hwnd, int nIDEvent, int uElapse, int lpTimerFunc);
		//[DllImport("user32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int KillTimer(int hwnd, int nIDEvent);
		//[DllImport("user32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int CloseWindow(int hwnd);
		//[DllImport("user32.dll", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int SendMessage(int hwnd, int wMsg, int wParam, System.IntPtr lParam);
		private const int WM_COMMAND = 0x111;
		private static string msgTitle = String.Empty;

		private struct IPINFO
		{
			public int dwAddr; // Dirección Ip
			public int dwIndex;
			public int dwMask;
			public int dwBCastAddr;
			public int dwReasmSize;
			public short unused1;
			public short unused2;
		}

		private struct MIB_IPADDRTABLE
		{
			public string dEntrys; //Numero de entradas de la tabla
			public pNemoQ.IPINFO[] mIPInfo; //Array de entradas de direcciones Ip
			public static MIB_IPADDRTABLE CreateInstance()
			{
					MIB_IPADDRTABLE result = new MIB_IPADDRTABLE();
					result.mIPInfo = new pNemoQ.IPINFO[6];
					return result;
			}
		}

		private struct IP_Array
		{
			public pNemoQ.MIB_IPADDRTABLE mBuffer;
			public int BufferLen;
			public static IP_Array CreateInstance()
			{
					IP_Array result = new IP_Array();
					result.mBuffer = MIB_IPADDRTABLE.CreateInstance();
					return result;
			}
		}

		//Función Api CopyMemory
		//[DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static void CopyMemory(System.IntPtr Destination, System.IntPtr Source, int Length);

		//Función Api GetIpAddrTable para obtener la tabla de direcciones IP
		//[DllImport("IPHlpApi.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetIpAddrTable(ref byte pIPAdrTable, ref int pdwSize, int Sort);

		//--- API para obtener el nombre del equipo2.Private
		//[DllImport("kernel32.dll", EntryPoint = "GetComputerNameA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetComputerName([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpbuffer, ref int nSize);


		internal static void TimerProc(int hwnd, int uMsg, int idEvent, int dwTime)
		{
			UpgradeSupportHelper.PInvoke.SafeNative.user32.KillTimer((IntPtr)hwnd, (IntPtr)idEvent);
			string tempRefParam = "#32770";
			int hMessageBox = UpgradeSupportHelper.PInvoke.SafeNative.user32.FindWindow(ref tempRefParam, ref msgTitle);
            int IParam = 0;
			if (hMessageBox != 0)
			{
                UpgradeSupportHelper.PInvoke.SafeNative.user32.SendMessage(hMessageBox, WM_COMMAND, 6, ref IParam);
			}
		}

		internal static DialogResult MsgBoxEx(int hwnd, string Prompt, int cSeconds, MsgBoxStyle Buttons, ref string Title, ref object HelpFile, ref object Context)
		{
			int cMSeconds = cSeconds * 100;
            IntPtr nIDIEvent = IntPtr.Zero;

            Title = (Title == "") ? AssemblyHelper.GetTitle(System.Reflection.Assembly.GetExecutingAssembly()) : Title;
			msgTitle = Title;
            UpgradeSupportHelper.PInvoke.SafeNative.user32.SetTimer((IntPtr)hwnd, nIDIEvent, (uint)cMSeconds,(IntPtr)cMSeconds);
			return (DialogResult) Interaction.MsgBox(Prompt, Buttons, Title);
		}

		internal static DialogResult MsgBoxEx(int hwnd, string Prompt, int cSeconds, MsgBoxStyle Buttons, ref string Title, ref object HelpFile)
		{
			object tempRefParam = null;
			return MsgBoxEx(hwnd, Prompt, cSeconds, Buttons, ref Title, ref HelpFile, ref tempRefParam);
		}

		internal static DialogResult MsgBoxEx(int hwnd, string Prompt, int cSeconds, MsgBoxStyle Buttons, ref string Title)
		{
			object tempRefParam2 = null;
			object tempRefParam3 = null;
			return MsgBoxEx(hwnd, Prompt, cSeconds, Buttons, ref Title, ref tempRefParam2, ref tempRefParam3);
		}

		internal static DialogResult MsgBoxEx(int hwnd, string Prompt, int cSeconds, MsgBoxStyle Buttons)
		{
			string tempRefParam4 = String.Empty;
			object tempRefParam5 = null;
			object tempRefParam6 = null;
			return MsgBoxEx(hwnd, Prompt, cSeconds, Buttons, ref tempRefParam4, ref tempRefParam5, ref tempRefParam6);
		}

		internal static DialogResult MsgBoxEx(int hwnd, string Prompt, int cSeconds)
		{
			string tempRefParam7 = String.Empty;
			object tempRefParam8 = null;
			object tempRefParam9 = null;
			return MsgBoxEx(hwnd, Prompt, cSeconds, MsgBoxStyle.OkOnly, ref tempRefParam7, ref tempRefParam8, ref tempRefParam9);
		}

		internal static DialogResult MsgBoxEx(int hwnd, string Prompt)
		{
			string tempRefParam10 = String.Empty;
			object tempRefParam11 = null;
			object tempRefParam12 = null;
			return MsgBoxEx(hwnd, Prompt, 1, MsgBoxStyle.OkOnly, ref tempRefParam10, ref tempRefParam11, ref tempRefParam12);
		}

		//Función que retorna un string con la dirección Ip final
		internal static string RecuperarIP()
		{
            //int ret = 0;
            //string TempIP = String.Empty;
            //pNemoQ.MIB_IPADDRTABLE Listing = pNemoQ.MIB_IPADDRTABLE.CreateInstance();
            //string L3 = String.Empty;


            //try
            //{

            //	byte tempRefParam = (byte) 0;
            //             UpgradeSupportHelper.PInvoke.SafeNative.iphlpapi.GetIpAddrTable(ref tempRefParam, ref ret, -1);

            //	if (ret <= 0)
            //	{
            //		return System.String.Empty;
            //	}
            //	byte[] bBytes = new byte[ret];

            //             //recuperamos la tabla con las ip
            //             UpgradeSupportHelper.PInvoke.SafeNative.iphlpapi.GetIpAddrTable(ref bBytes[0], ref ret, 0);
            //             UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory(out Listing.dEntrys, bBytes[0], 4);

            //	string[] TempList = ArraysHelper.InitializeArray<string>(Listing.dEntrys);

            //	for (int Tel = 0; Tel <= Listing.dEntrys - 1; Tel++)
            //	{
            //                 //Copiamos la estructura entera a la lista
            //                 UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory(out Listing.mIPInfo[Tel], bBytes[4 + (Tel * Marshal.SizeOf(Listing.mIPInfo[0]))], Marshal.SizeOf(Listing.mIPInfo[Tel]));
            //		TempList[Tel] = ConvertirDirecciónAstring(Listing.mIPInfo[Tel].dwAddr);
            //	}

            //	TempIP = TempList[0];
            //	for (int Tempi = 0; Tempi <= Listing.dEntrys - 1; Tempi++)
            //	{
            //		L3 = TempList[Tempi].Substring(0, Math.Min(3, TempList[Tempi].Length));
            //		if (L3 != "169" && L3 != "127" && L3 != "192" && L3 != "0.0")
            //		{
            //			TempIP = TempList[Tempi];
            //		}
            //	}


            //	return TempIP;
            //}
            //catch
            //{
            //	return "";
            //}

            try
            {
                string strHostName = "";
                strHostName = System.Net.Dns.GetHostName();
                IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
                IPAddress[] addr = ipEntry.AddressList;
                return addr[addr.Length - 1].ToString();
            }
            catch
            {
                return "";
            }
        }

		internal static string ComputerName()
		{

			//-- Funcion auxiliar que devuelve el nombre del equipo llamando al API
			string result = String.Empty;
			result = new String(' ', 260);
			int tempRefParam = result.Length;
            UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetComputerName(ref result, ref tempRefParam);
			return result.Substring(0, Math.Min(result.IndexOf("\0"), result.Length));
		}

		private static string ConvertirDirecciónAstring(int longAddr)
		{
			string result = String.Empty;
            //byte[] myByte = new byte[4]; //array de tipo Byte
            string myByte;
            UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory(out myByte,(IntPtr)longAddr, 4);
			for (int Cnt = 0; Cnt <= 3; Cnt++)
			{
				result = result + myByte[Cnt].ToString() + ".";
			}
			return result.Substring(0, Math.Min(Strings.Len(result) - 1, result.Length));
		}
	}
}