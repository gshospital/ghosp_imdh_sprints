using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Xml;
using UpgradeStubs;
using ElementosCompartidos;
using System.Web;

namespace ProcesoNemoQ
{
    public class clsProcesoNemoQ
	{
		public void procesaNemoQ(ref string codSala, ref string codAgenda, string codServicio, ref string codPrestac, ref SqlConnection Conexion, string idPaciente, int codPersona, string codSociedad, string idConsulta, string fechaConsulta, string horaConsulta, string tipoApunte, string tipoRegistro, bool generaConsulta = false)
		{            
			XmlDocument RespXML = null;
			int irespuesta = 0;

			string stSoapAcction = String.Empty;

			string stURL = String.Empty;
			string sql = String.Empty;
			DataSet RrDatos = null;

			string codEntidad = String.Empty;
			string desEntidad = String.Empty;
			string codEdificio = String.Empty;
			string codPlantas = String.Empty;
			string desEdificio = String.Empty;
			string desPlanta = String.Empty;
			string codSalaEspera = String.Empty;
			string desSala = String.Empty;
			string desAgenda = String.Empty;
			string desServicio = String.Empty;
			string desPrestac = String.Empty;
			string nombPersona = String.Empty;
			string ape1Persona = String.Empty;
			string ape2Persona = String.Empty;
			string nombPaciente = String.Empty;
			string ape1Paciente = String.Empty;
			string ape2Paciente = String.Empty;
			string fechaNacimiento = String.Empty;
			string codTarjera = String.Empty;
			string codDNI = String.Empty;
			string codHistoria = String.Empty;
			string desSociedad = String.Empty;
			bool blnError = false;
			string codUsuario = String.Empty;
			string passUsuario = String.Empty;
			string StSql = String.Empty;

            //DGMORENOG_TODO_X_5 - PENDIENTE IMPLEMENTACION
			//ProcesoIP.clsProcesoIP clase = null;

			string strIP = String.Empty;
			string nombreMaquina = String.Empty;
			string stError = String.Empty;
			float tiempoEspera = 0;

			string cadenaXML = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><SOAP-ENV:Body> <obtenerCitas xmlns=\"urn:NemoQWS\"> <XMLString xsi:type=\"xsd:string\"> <OGSWSAM xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"> ";

			Serrores.oClass = new Mensajes.ClassMensajes();

            try
            {
                RespXML = new XmlDocument();

                sql = "SELECT * FROM SCONSGLO WHERE GCONSGLO='INTNEMOQ'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
                RrDatos = new DataSet();
                tempAdapter.Fill(RrDatos);
                string cabecera = String.Empty;
                if (RrDatos.Tables[0].Rows.Count == 0)
                {
                    return;
                }
                else
                {
                    if ((Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1"]) + "").Trim() != "S")
                    {
                        return;
                    }
                    else
                    {
                        if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["nnumeri1"]))
                        {
                            tiempoEspera = 5;
                        }
                        else
                        {
                            tiempoEspera = Convert.ToSingle(RrDatos.Tables[0].Rows[0]["nnumeri1"]);
                        }
                        codEntidad = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]) + "").Trim();

                        stURL = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1i2"]) + "").Trim() + (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2i2"]) + "").Trim() + (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3i2"]) + "").Trim();

                    }
                    RrDatos.Close();

                    sql = "SELECT * FROM SCONSGLO WHERE GCONSGLO='SOANEMOQ'";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_2.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        codUsuario = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1"]) + "").Trim() + (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1i1"]) + "").Trim() + (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1i2"]) + "").Trim();
                        passUsuario = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]) + "").Trim() + (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2i1"]) + "").Trim() + (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2i2"]) + "").Trim();
                        stSoapAcction = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3"]) + "").Trim() + (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3i1"]) + "").Trim() + (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3i2"]) + "").Trim();
                    }
                    RrDatos.Close();


                    if (stURL.Trim() == "" || codUsuario.Trim() == "" || passUsuario.Trim() == "" || stSoapAcction.Trim() == "")
                    {
                        short tempRefParam = 4080;
                        string[] tempRefParam2 = new string[] { "el proceso de OGS, revise las constantes INTNEMOQ." };
                        irespuesta = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, Conexion, tempRefParam2));
                        return;
                    }


                    if (codServicio.Trim() == "")
                    {
                        short tempRefParam3 = 4080;
                        string[] tempRefParam4 = new string[] { "el proceso de OGS, el servicio es un dato obligatorio." };
                        irespuesta = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, Conexion, tempRefParam4));
                        return;
                    }

                    sql = "SELECT * FROM SCONSGLO WHERE GCONSGLO='GRUPOHO'";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_3.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        desEntidad = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1"]) + "").Trim();
                    }
                    RrDatos.Close();

                    sql = "SELECT ISNULL(dnomserv, '') as dnomserv FROM DSERVICIVA WHERE gservici = " + codServicio;
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_4.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        desServicio = Convert.ToString(RrDatos.Tables[0].Rows[0]["dnomserv"]);
                    }
                    RrDatos.Close();


                    if (codSala != "")
                    {
                        sql = "SELECT ISNULL(dsalacon, '') as dsalacon, ISNULL(gedifici, '') as gedifici, ISNULL(gplantas, '') as gplantas, ISNULL(nsalaesp,'') as nsalaesp FROM DSALACON WHERE gsalacon = '" + codSala + "'";
                        SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, Conexion);
                        RrDatos = new DataSet();
                        tempAdapter_5.Fill(RrDatos);
                        if (RrDatos.Tables[0].Rows.Count != 0)
                        {
                            if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["nsalaesp"]))
                            {
                                RrDatos.Close();
                                return;
                            }
                            if ((Convert.ToString(RrDatos.Tables[0].Rows[0]["nsalaesp"]) + "").Trim() == "")
                            {
                                RrDatos.Close();
                                return;
                            }
                            desSala = Convert.ToString(RrDatos.Tables[0].Rows[0]["dsalacon"]);
                            codEdificio = Convert.ToString(RrDatos.Tables[0].Rows[0]["gedifici"]);
                            codPlantas = Convert.ToString(RrDatos.Tables[0].Rows[0]["gplantas"]);
                            codSalaEspera = Convert.ToString(RrDatos.Tables[0].Rows[0]["nsalaesp"]);
                        }
                        else
                        {
                            RrDatos.Close();
                            return;
                        }
                        RrDatos.Close();
                        if (desSala == "")
                        {
                            desSala = desServicio;
                        }
                        if (codSalaEspera == "")
                        {
                            RrDatos.Close();
                            sql = "SELECT ISNULL(DGRUFUNC.dgrufunc, '') AS dgrufunc FROM DSERVICIVA INNER JOIN DGRUFUNC ON DSERVICIVA.ggrufunc = DGRUFUNC.ggrufunc WHERE DSERVICIVA.gservici = " + codServicio;
                            SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql, Conexion);
                            RrDatos = new DataSet();
                            tempAdapter_6.Fill(RrDatos);
                            if (RrDatos.Tables[0].Rows.Count != 0)
                            {
                                codSalaEspera = Convert.ToString(RrDatos.Tables[0].Rows[0]["dgrufunc"]).Trim();
                            }
                        }
                    }
                    else
                    {
                        sql = "SELECT dabrserv, ISNULL(DGRUFUNC.dgrufunc, '') AS dgrufunc FROM DSERVICIVA LEFT JOIN DGRUFUNC ON DSERVICIVA.ggrufunc = DGRUFUNC.ggrufunc WHERE DSERVICIVA.gservici = " + codServicio;
                        SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql, Conexion);
                        RrDatos = new DataSet();
                        tempAdapter_7.Fill(RrDatos);
                        if (RrDatos.Tables[0].Rows.Count != 0)
                        {
                            codSala = Convert.ToString(RrDatos.Tables[0].Rows[0]["dabrserv"]).Trim();
                            codSalaEspera = Convert.ToString(RrDatos.Tables[0].Rows[0]["dgrufunc"]).Trim();
                        }
                        desSala = desServicio;
                        codPlantas = "0";
                    }


                    if (codEdificio == "")
                    {
                        sql = "select Isnull(valfanu3, '0') as valfanu3 from sconsglo where gconsglo = 'INTNEMOQ'";
                        SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(sql, Conexion);
                        RrDatos = new DataSet();
                        tempAdapter_8.Fill(RrDatos);
                        if (RrDatos.Tables[0].Rows.Count != 0)
                        {
                            codEdificio = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3"]).Trim();
                        }
                        else
                        {
                            codEdificio = "";
                        }
                    }

                    sql = "SELECT ISNULL(dedifici, '') as dedifici FROM DEDIFICIVA WHERE gedifici = " + codEdificio;
                    SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_9.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        desEdificio = Convert.ToString(RrDatos.Tables[0].Rows[0]["dedifici"]);
                    }
                    RrDatos.Close();

                    sql = "SELECT ISNULL(dnomplan, '') as dnomplan FROM DPLANTASVA WHERE gplantas = " + codPlantas;
                    SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_10.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        desPlanta = Convert.ToString(RrDatos.Tables[0].Rows[0]["dnomplan"]);
                    }
                    RrDatos.Close();

                    sql = "SELECT ISNULL(dagendas, '') as dagendas FROM CAGENDAS WHERE gagendas = '" + codAgenda + "'";
                    SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_11.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        desAgenda = Convert.ToString(RrDatos.Tables[0].Rows[0]["dagendas"]);
                    }
                    RrDatos.Close();

                    if (codPrestac.Trim() != "")
                    {
                        sql = "SELECT ISNULL(dprestac, '') as dprestac FROM DCODPRESVA WHERE gprestac = '" + codPrestac + "'";
                        SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(sql, Conexion);
                        RrDatos = new DataSet();
                        tempAdapter_12.Fill(RrDatos);
                        if (RrDatos.Tables[0].Rows.Count != 0)
                        {
                            desPrestac = Convert.ToString(RrDatos.Tables[0].Rows[0]["dprestac"]);
                        }
                        RrDatos.Close();
                    }
                    else
                    {
                        sql = "SELECT dabrserv FROM dservici WHERE gservici = " + codServicio;
                        SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(sql, Conexion);
                        RrDatos = new DataSet();
                        tempAdapter_13.Fill(RrDatos);
                        if (RrDatos.Tables[0].Rows.Count != 0)
                        {
                            codPrestac = Convert.ToString(RrDatos.Tables[0].Rows[0]["dabrserv"]).Trim();
                        }
                        desPrestac = desServicio;
                    }

                    sql = "SELECT ISNULL(dnompers, '') as dnompers, ISNULL(dap1pers, '') as dap1pers, ISNULL(dap2pers, '') as dap2pers  FROM DPERSONAVA WHERE gpersona = " + codPersona.ToString();
                    SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_14.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        nombPersona = Convert.ToString(RrDatos.Tables[0].Rows[0]["dnompers"]);
                        ape1Persona = Convert.ToString(RrDatos.Tables[0].Rows[0]["dap1pers"]);
                        ape2Persona = Convert.ToString(RrDatos.Tables[0].Rows[0]["dap2pers"]);
                    }
                    RrDatos.Close();

                    sql = "SELECT ISNULL(ifilprov, 'N') as ifilprov, ISNULL(dnombpac, '') as dnombpac, ISNULL(dape1pac, '') as dape1pac, ISNULL(dape2pac, '') as dape2pac,  isnull(fnacipac,'') as fnacipac, ISNULL(nafiliac, '') as nafiliac, ISNULL(ndninifp, '') as ndninifp FROM DPACIENT WHERE gidenpac = '" + idPaciente + "'";

                    SqlDataAdapter tempAdapter_15 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_15.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        if (Convert.ToString(RrDatos.Tables[0].Rows[0]["ifilprov"]) == "S")
                        {
                            return;
                        }

                        nombPaciente = Convert.ToString(RrDatos.Tables[0].Rows[0]["dnombpac"]);
                        ape1Paciente = Convert.ToString(RrDatos.Tables[0].Rows[0]["dape1pac"]);
                        ape2Paciente = Convert.ToString(RrDatos.Tables[0].Rows[0]["dape2pac"]);

                        if (Convert.ToString(RrDatos.Tables[0].Rows[0]["fnacipac"]).Trim() == "")
                        {
                            fechaNacimiento = "";
                        }
                        else
                        {
                            fechaNacimiento = Convert.ToDateTime(RrDatos.Tables[0].Rows[0]["fnacipac"]).ToString("yyyy-MM-dd");
                        }

                        codTarjera = Convert.ToString(RrDatos.Tables[0].Rows[0]["nafiliac"]);
                        codDNI = Convert.ToString(RrDatos.Tables[0].Rows[0]["ndninifp"]);
                    }
                    RrDatos.Close();


                    if (tipoApunte == "C")
                    {
                        if (tipoRegistro == "1")
                        { // CCITPROG
                            sql = "SELECT ISNULL(nafiliac, '') as nafiliac FROM CCITPROG With (nolock) WHERE  ganoregi = " + idConsulta.Substring(0, Math.Min(4, idConsulta.Length)) + " AND gnumregi = " + Convert.ToInt32(Double.Parse(idConsulta.Substring(4))).ToString().Trim();
                            SqlDataAdapter tempAdapter_16 = new SqlDataAdapter(sql, Conexion);
                            RrDatos = new DataSet();
                            tempAdapter_16.Fill(RrDatos);
                            if (RrDatos.Tables[0].Rows.Count != 0)
                            {
                                codTarjera = Convert.ToString(RrDatos.Tables[0].Rows[0]["nafiliac"]);
                            }
                        }
                        else if (tipoRegistro == "3")
                        {  //' CCITUSUA
                            sql = "SELECT ISNULL(nafiliac, '') as nafiliac FROM CCITUSUA With (nolock) WHERE  ganoregi = " + idConsulta.Substring(0, Math.Min(4, idConsulta.Length)) + " AND gnumregi = " + Convert.ToInt32(Double.Parse(idConsulta.Substring(4))).ToString().Trim();
                            SqlDataAdapter tempAdapter_17 = new SqlDataAdapter(sql, Conexion);
                            RrDatos = new DataSet();
                            tempAdapter_17.Fill(RrDatos);
                            if (RrDatos.Tables[0].Rows.Count != 0)
                            {
                                codTarjera = Convert.ToString(RrDatos.Tables[0].Rows[0]["nafiliac"]);
                            }
                        }
                    }
                    else if (tipoApunte == "F")
                    {
                        sql = "SELECT ISNULL(nafiliac, '') as nafiliac FROM CCONSULT With (nolock) WHERE  ganoregi = " + idConsulta.Substring(0, Math.Min(4, idConsulta.Length)) + " AND gnumregi = " + Convert.ToInt32(Double.Parse(idConsulta.Substring(4))).ToString().Trim();
                        SqlDataAdapter tempAdapter_18 = new SqlDataAdapter(sql, Conexion);
                        RrDatos = new DataSet();
                        tempAdapter_18.Fill(RrDatos);
                        if (RrDatos.Tables[0].Rows.Count != 0)
                        {
                            codTarjera = Convert.ToString(RrDatos.Tables[0].Rows[0]["nafiliac"]);
                        }
                    }
                    else if (tipoApunte == "U")
                    {
                        sql = "SELECT ISNULL(nafiliac, '') as nafiliac FROM UEPISURG With (nolock) WHERE  ganourge = " + idConsulta.Substring(0, Math.Min(4, idConsulta.Length)) + " AND gnumurge = " + Convert.ToInt32(Double.Parse(idConsulta.Substring(4))).ToString().Trim();
                        SqlDataAdapter tempAdapter_19 = new SqlDataAdapter(sql, Conexion);
                        RrDatos = new DataSet();
                        tempAdapter_19.Fill(RrDatos);
                        if (RrDatos.Tables[0].Rows.Count != 0)
                        {
                            codTarjera = Convert.ToString(RrDatos.Tables[0].Rows[0]["nafiliac"]);
                        }
                    }

                    sql = "SELECT ghistoria  FROM HDOSSIER WHERE gidenpac = '" + idPaciente + "'";
                    SqlDataAdapter tempAdapter_20 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_20.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        codHistoria = Convert.ToString(RrDatos.Tables[0].Rows[0]["ghistoria"]);
                    }
                    RrDatos.Close();

                    sql = "SELECT dsocieda  FROM DSOCIEDAVA WHERE gsocieda = " + codSociedad;
                    SqlDataAdapter tempAdapter_21 = new SqlDataAdapter(sql, Conexion);
                    RrDatos = new DataSet();
                    tempAdapter_21.Fill(RrDatos);
                    if (RrDatos.Tables[0].Rows.Count != 0)
                    {
                        desSociedad = Convert.ToString(RrDatos.Tables[0].Rows[0]["dsocieda"]);
                    }
                    RrDatos.Close();

                    if (codAgenda.Trim() == "")
                    {
                        codAgenda = codServicio;
                    }
                    if (desAgenda.Trim() == "")
                    {
                        desAgenda = desServicio;
                    }
                    if (desPrestac.Trim() == "")
                    {
                        desPrestac = desServicio;
                    }

                    /*//DGMORENOG_X_5 - PENDIENTE IMPLEMENTAR
					if (tipoRegistro.Trim() == "3" || tipoRegistro.Trim() == "4")
					{
						clase = new ProcesoIP.clsProcesoIP();
						strIP = clase.RecuperarIP();
						nombreMaquina = clase.ComputerName();
						clase = null;
					}*/

                    cabecera = cadenaXML;
                    cadenaXML = cadenaXML + "<CITA>";
                    cadenaXML = cadenaXML + "<IDENTITY>" + Conversion.Str(Conversion.Val(codEntidad)).Trim() + "</IDENTITY>";
                    cadenaXML = cadenaXML + "<ENTITYNAME>" + desEntidad.Trim() + "</ENTITYNAME>";
                    cadenaXML = cadenaXML + "<IDCENTER>" + codEdificio.Trim() + "</IDCENTER>";
                    cadenaXML = cadenaXML + "<CENTERNAME>" + desEdificio.Trim() + "</CENTERNAME>";
                    cadenaXML = cadenaXML + "<IDFLOORNUMBER>" + codPlantas.Trim() + "</IDFLOORNUMBER>";
                    cadenaXML = cadenaXML + "<FLOORDESCRIPTION>" + desPlanta.Trim() + "</FLOORDESCRIPTION>";
                    cadenaXML = cadenaXML + "<WAITINGAREA>" + codSalaEspera.Trim() + "</WAITINGAREA>";
                    cadenaXML = cadenaXML + "<IDROOM>" + codSala.Trim() + "</IDROOM>";
                    cadenaXML = cadenaXML + "<ROOMDESCRIPTION>" + desSala.Trim() + "</ROOMDESCRIPTION>";
                    cadenaXML = cadenaXML + "<IDAGENDA>" + codAgenda.Trim() + "</IDAGENDA>";
                    cadenaXML = cadenaXML + "<AGENDANAME>" + desAgenda.Trim() + "</AGENDANAME>";
                    cadenaXML = cadenaXML + "<IDSPECIALITY>" + codServicio.Trim() + "</IDSPECIALITY>";
                    cadenaXML = cadenaXML + "<SPECIALITYNAME>" + desServicio.Trim() + "</SPECIALITYNAME>";
                    cadenaXML = cadenaXML + "<IDACTIVITY>" + codPrestac.Trim() + "</IDACTIVITY>";
                    cadenaXML = cadenaXML + "<ACTIVITYDESCRIPTION>" + desPrestac.Trim() + "</ACTIVITYDESCRIPTION>";
                    cadenaXML = cadenaXML + "<IDMEDICALACT>" + codPrestac.Trim() + "</IDMEDICALACT>";
                    cadenaXML = cadenaXML + "<ICU>" + tipoApunte + idConsulta.Trim() + "</ICU>";
                    cadenaXML = cadenaXML + "<DATUM>" + fechaConsulta.Substring(6, Math.Min(4, fechaConsulta.Length - 6)) + "-" + fechaConsulta.Substring(3, Math.Min(2, fechaConsulta.Length - 3)) + "-" + fechaConsulta.Substring(0, Math.Min(2, fechaConsulta.Length)) + "</DATUM>";
                    cadenaXML = cadenaXML + "<BOOKEDTIME>" + horaConsulta.Trim() + "</BOOKEDTIME>";
                    cadenaXML = cadenaXML + "<ATTENDERID>" + codPersona.ToString().Trim() + "</ATTENDERID>";
                    cadenaXML = cadenaXML + "<ATTENDER1LASTNAME>" + ape1Persona.Trim() + "</ATTENDER1LASTNAME>";
                    cadenaXML = cadenaXML + "<ATTENDER2LASTNAME>" + ape2Persona.Trim() + "</ATTENDER2LASTNAME>";
                    cadenaXML = cadenaXML + "<ATTENDERNAME>" + nombPersona.Trim() + "</ATTENDERNAME>";
                    cadenaXML = cadenaXML + "<CUSTOMERID>" + idPaciente.Trim() + "</CUSTOMERID>";
                    cadenaXML = cadenaXML + "<CUSTOMER1LASTNAME>" + ape1Paciente.Trim() + "</CUSTOMER1LASTNAME>";
                    cadenaXML = cadenaXML + "<CUSTOMER2LASTNAME>" + ape2Paciente.Trim() + "</CUSTOMER2LASTNAME>";
                    cadenaXML = cadenaXML + "<CUSTOMERNAME>" + nombPaciente.Trim() + "</CUSTOMERNAME>";
                    cadenaXML = cadenaXML + "<BIRTHDATE>" + fechaNacimiento.Trim() + "</BIRTHDATE>";
                    cadenaXML = cadenaXML + "<CIP>" + codTarjera.Trim() + "</CIP>";
                    cadenaXML = cadenaXML + "<DNI>" + codDNI.Trim() + "</DNI>";
                    cadenaXML = cadenaXML + "<NHC>" + codHistoria.Trim() + "</NHC>";
                    cadenaXML = cadenaXML + "<BOOKEDREFID>" + tipoApunte + idConsulta.Trim() + "</BOOKEDREFID>";
                    cadenaXML = cadenaXML + "<IDCUSTOMERTYPE>" + tipoApunte + "</IDCUSTOMERTYPE>";
                    cadenaXML = cadenaXML + "<CUSTOMERTYPEDESCRIPTION>Forzado</CUSTOMERTYPEDESCRIPTION>";
                    cadenaXML = cadenaXML + "<PRINT>SI</PRINT>";
                    cadenaXML = cadenaXML + "<FUNCTION>" + tipoRegistro.Trim() + "</FUNCTION>";
                    cadenaXML = cadenaXML + "<REPORTBACKHIS>SI</REPORTBACKHIS>";
                    cadenaXML = cadenaXML + "<SOCIETYID>" + codSociedad.Trim() + "</SOCIETYID>";
                    cadenaXML = cadenaXML + "<SOCIETYDESCRIPTION>" + desSociedad.Trim() + "</SOCIETYDESCRIPTION>";
                    cadenaXML = cadenaXML + "<IPNUMBER>" + strIP + "</IPNUMBER>";
                    cadenaXML = cadenaXML + "<PCNAME>" + nombreMaquina + "</PCNAME></CITA></OGSWSAM></XMLString></obtenerCitas></SOAP-ENV:Body></SOAP-ENV:Envelope>";

                    if (realizaProceso(tipoApunte + idConsulta.Trim(), fechaConsulta + " " + horaConsulta, idPaciente, codEntidad, desEntidad, codEdificio, desEdificio, codPlantas, desPlanta, codSalaEspera, codSala, desSala, codAgenda, desAgenda, codServicio, desServicio, codPrestac, desPrestac, codPersona.ToString(), ape1Persona, ape2Persona, nombPersona, ape1Paciente, ape2Paciente, nombPaciente, fechaNacimiento, codTarjera, codDNI, codHistoria, tipoApunte, tipoRegistro, codSociedad, desSociedad, Conexion, generaConsulta))
                    {

                        StSql = " SELECT *  FROM MENSHOGS  WHERE 2=1";
                        SqlDataAdapter tempAdapter_22 = new SqlDataAdapter(StSql, Conexion);
                        RrDatos = new DataSet();
                        tempAdapter_22.Fill(RrDatos);
                        RrDatos.AddNew();

                        RrDatos.Tables[0].Rows[0]["idcita"] = tipoApunte + idConsulta.Trim();
                        RrDatos.Tables[0].Rows[0]["ffeccita"] = fechaConsulta + " " + horaConsulta;
                        RrDatos.Tables[0].Rows[0]["gidenpac"] = idPaciente;

                        RrDatos.Tables[0].Rows[0]["gentidad"] = Conversion.Val(codEntidad);
                        if (desEntidad.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dentidad"] = desEntidad;
                        }
                        if (codEdificio.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["gedifici"] = Conversion.Val(codEdificio);
                        }
                        if (desEdificio.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dedifici"] = desEdificio;
                        }
                        if (codPlantas.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["gplantas"] = Conversion.Val(codPlantas);
                        }
                        RrDatos.Tables[0].Rows[0]["dnomplan"] = desPlanta.Trim();
                        if (codSalaEspera.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["gsalaesp"] = codSalaEspera.Trim();
                        }
                        if (codSala.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["gsalacon"] = codSala.Trim();
                        }
                        if (desSala.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dsalacon"] = desSala.Trim();
                        }
                        if (codAgenda.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["gagendas"] = codAgenda.Trim();
                        }
                        if (desAgenda.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dagendas"] = desAgenda.Trim();
                        }
                        if (codServicio.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["gservici"] = Conversion.Val(codServicio.Trim());
                        }
                        if (desServicio.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dservici"] = desServicio.Trim();
                        }
                        if (codPrestac.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["gprestac"] = codPrestac.Trim();
                        }
                        if (desPrestac.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dprestac"] = desPrestac.Trim();
                        }
                        if (codPersona.ToString().Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["gpersona"] = Conversion.Val(codPersona.ToString().Trim());
                        }
                        if (ape1Persona.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dap1pers"] = ape1Persona.Trim();
                        }
                        if (ape2Persona.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dap2pers"] = ape2Persona.Trim();
                        }
                        if (nombPersona.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dnompers"] = nombPersona.Trim();
                        }
                        if (ape1Paciente.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dape1pac"] = ape1Paciente.Trim();
                        }
                        if (ape2Paciente.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dape2pac"] = ape2Paciente.Trim();
                        }
                        if (nombPaciente.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dnombpac"] = nombPaciente.Trim();
                        }
                        if (fechaNacimiento.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["fnacipac"] = fechaNacimiento.Trim();
                        }
                        if (codTarjera.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["ntarjeta"] = codTarjera.Trim();
                        }
                        if (codDNI.Trim() != "")
                        {
                            if (codDNI.Trim() != "")
                            {
                                RrDatos.Tables[0].Rows[0]["ndninifp"] = codDNI.Trim();
                            }
                        }
                        if (codHistoria.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["ghistoria"] = Double.Parse(codHistoria.Trim());
                        }
                        RrDatos.Tables[0].Rows[0]["itiapunt"] = tipoApunte;
                        RrDatos.Tables[0].Rows[0]["itickets"] = "S";
                        RrDatos.Tables[0].Rows[0]["ifactele"] = "S";
                        RrDatos.Tables[0].Rows[0]["tfuncion"] = tipoRegistro;
                        if (codSociedad.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["gsocieda"] = codSociedad.Trim();
                        }
                        if (desSociedad.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["dsocieda"] = desSociedad.Trim();
                        }
                        RrDatos.Tables[0].Rows[0]["fmecaniz"] = DateTime.Now;
                        RrDatos.Tables[0].Rows[0]["fenviado"] = DateTime.Now;

                        if (strIP.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["IPNUMBER"] = strIP.Trim();
                        }
                        if (nombreMaquina.Trim() != "")
                        {
                            RrDatos.Tables[0].Rows[0]["PCNAME"] = nombreMaquina.Trim();
                        }

                        string tempQuery = RrDatos.Tables[0].TableName;
                        SqlDataAdapter tempAdapter_23 = new SqlDataAdapter(tempQuery, "");
                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_23);
                        tempAdapter_23.Update(RrDatos, RrDatos.Tables[0].TableName);
                        RrDatos.Close();


                        stError = "";
                        PeticionConsulta(ref cadenaXML, stURL, codUsuario, passUsuario, stSoapAcction, ref stError, tiempoEspera);


                        if ((cadenaXML.IndexOf("<STATUS>0</STATUS>") + 1) > 1)
                        {
                            blnError = false;
                        }
                        else
                        {
                            if ((cadenaXML.IndexOf("<STATUS>1</STATUS>") + 1) > 1)
                            {
                                blnError = true;
                            }
                        }

                        if (blnError || stError.Trim() != "")
                        {

                            StSql = " SELECT * FROM MENSHOGS  WHERE idcita = '" + tipoApunte + idConsulta.Trim() + "'";
                            SqlDataAdapter tempAdapter_24 = new SqlDataAdapter(StSql, Conexion);
                            RrDatos = new DataSet();
                            tempAdapter_24.Fill(RrDatos);
                            if (RrDatos.Tables[0].Rows.Count != 0)
                            {
                                RrDatos.Edit();
                                if (blnError)
                                {
                                    RrDatos.Tables[0].Rows[0]["deserror"] = "El Web-Service ha retornado error. (" + cadenaXML + ")";
                                }
                                else
                                {
                                    RrDatos.Tables[0].Rows[0]["deserror"] = stError;
                                }
                                string tempQuery_2 = RrDatos.Tables[0].TableName;
                                SqlDataAdapter tempAdapter_25 = new SqlDataAdapter(tempQuery_2, "");
                                SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_25);
                                tempAdapter_25.Update(RrDatos, RrDatos.Tables[0].TableName);
                                RrDatos.Close();
                            }
                        }

                    }
                }
                RrDatos = null;
            }
            catch (System.Exception excep)
            {
                short tempRefParam5 = 5000;
                string[] tempRefParam6 = new string[] { "del Web-Service., " + excep.Message};
				irespuesta = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, Conexion, tempRefParam6));
			}

		}

		public string UrlEncodeUtf8(string strSource)
		{
			string result = String.Empty;
            // ralopezn TODO_X_6 16/06/2016
			//MSScriptControl.ScriptControl objSC = null;

			try
			{
				//objSC = new MSScriptControl.ScriptControl();
				//objSC.Language = "Jscript";				
				result = HttpUtility.UrlEncode(strSource);
				//objSC = null;

				return result;
			}
			catch
			{

				return strSource;
			}
		}

		public void PeticionConsulta(ref string mensajeXML, string stURL, string codUsuario, string passUsuario, string SoapAcction, ref string stError, float TiempoPausa = 5)
		{
			float myTimer = 0;
			bool blnTimeOut = false;
			                                   
            MSXML2.ServerXMLHTTP oHttReq = null;
            XmlDocument xmlRespuesta = null;
            XmlNodeList nodoRespuesta = null;

            try
			{
				oHttReq = null;
				oHttReq = new MSXML2.ServerXMLHTTP();

                oHttReq.open("POST", stURL, true, codUsuario, passUsuario);
				oHttReq.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");

				oHttReq.setRequestHeader("SOAPAction", SoapAcction);
				oHttReq.send(mensajeXML);
				blnTimeOut = true;
				stError = "";
				myTimer = (float) DateTime.Now.TimeOfDay.TotalSeconds; // Establece la hora de inicio.
				Debug.WriteLine(DateTime.Now.ToString("HH:mm:SS"));

				while(DateTime.Now.TimeOfDay.TotalSeconds < myTimer + TiempoPausa)
				{
					if (oHttReq.status  == 4)
					{
						if (oHttReq.status == 200)
						{
							mensajeXML = oHttReq.responseText;
						}
						else
						{
							stError = " Error -- " + oHttReq.status.ToString() + ": " + oHttReq.statusText + " (" + oHttReq.responseText + ")";
						}
						blnTimeOut = false;
						break;
					}
					string tempRefParam = "Proceso NemoQ";
					pNemoQ.MsgBoxEx(0, "Procesando NemoQ ..", 5, (MsgBoxStyle) (((int) MsgBoxStyle.OkOnly) + ((int) MsgBoxStyle.Information)), ref tempRefParam);

				};
				Debug.WriteLine(DateTime.Now.ToString("HH:mm:SS"));
				oHttReq = null;
				if (blnTimeOut)
				{
					stError = stError + " Error -- Se ha superado el tiempo de espera. ";
				}
			}
			catch (System.Exception excep)
			{
				if (oHttReq != null)
				{
					if (oHttReq.status != 200)
					{
						stError = " Error " + oHttReq.status.ToString() + ": " + oHttReq.statusText + " (" + oHttReq.responseText + ")";

					}
				}
				else
				{
					stError = " Error (" + excep.Message + ")";
				}
			}
		}

		private bool realizaProceso(string idApunte, string fechaCita, string idPaciente, string codEntidad, string desEntidad, string codEdificio, string desEdificio, string codPlantas, string desPlanta, string codSalaEspera, string codSala, string desSala, string codAgenda, string desAgenda, string codServicio, string desServicio, string codPrestac, string desPrestac, string codPersona, string ape1Persona, string ape2Persona, string nombPersona, string ape1Paciente, string ape2Paciente, string nombPaciente, string fechaNacimiento, string codTarjera, string codDNI, string codHistoria, string tipoApunte, string tipoRegistro, string codSociedad, string desSociedad, SqlConnection Conexion, bool generaConsulta)
		{
			bool result = false;
			DataSet rdoTemp = null;
			string StSql = String.Empty;
			result = true;

			if (tipoRegistro != "6" && !generaConsulta)
			{
				StSql = "SELECT TOP 1 * FROM MENSHOGS With (nolock) WHERE idCita = '" + idApunte + "' Order By fmecaniz DESC ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["ffeccita"]) + "").Trim() == fechaCita.Trim() + ":00" && (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gservici"]) + "").Trim() == codServicio.Trim() && (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pac"]) + "").Trim() == ape1Paciente.Trim() && (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape2pac"]) + "").Trim() == ape2Paciente.Trim() && (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpac"]) + "").Trim() == nombPaciente.Trim() && (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gsalacon"]) + "").Trim() == codSala.Trim())
					{

						result = false;
					}
				}
				rdoTemp.Close();
			}
			return result;
		}
	}
}