using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace PeticionHistoriaClinica
{
	public partial class HTC120F1
		: RadForm
	{
		string marcado = String.Empty;
		string validarFechaPrevistaEntrega = String.Empty;
		string horaValidacionFechaPrevistaEntrega = String.Empty;
		int intCalendarioGeneral = 0;
		public HTC120F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		private void ActualizarFechaPrevistaDevolucion()
		{
			if (CBB_motivo.Text.Trim() != "")
			{
				BasHTC120F1.StComando = "Select NDIASPREST from DMOTPETH where DMOTPETH=";
				BasHTC120F1.StComando = BasHTC120F1.StComando + "'" + CBB_motivo.Text.Trim() + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
				BasHTC120F1.Rs = new DataSet();
				tempAdapter.Fill(BasHTC120F1.Rs);
                //if (DateTime.Parse(DateTime.Parse(SDC_entrega.Value.Date).ToString("dd/MM/yyyy")).AddDays(Convert.ToDouble(BasHTC120F1.Rs.Tables[0].Rows[0][0])) >= DateTime.Parse(DateTime.Parse(SDC_devolucion.MinDate).ToString("dd/MM/yyyy")))
                if (SDC_entrega.Value.AddDays(Convert.ToDouble(BasHTC120F1.Rs.Tables[0].Rows[0][0].ToString())) >= SDC_devolucion.MinDate)
                {
					//SDC_devolucion.Value = DateTime.Parse(DateTime.Parse(SDC_entrega.Value.Date).ToString("dd/MM/yyyy")).AddDays(Convert.ToDouble(BasHTC120F1.Rs.Tables[0].Rows[0][0]));
                    SDC_devolucion.Value = SDC_entrega.Value.AddDays(Convert.ToDouble(BasHTC120F1.Rs.Tables[0].Rows[0][0].ToString()));
                }
				else
				{
					SDC_devolucion.Value = SDC_devolucion.MinDate;
				}
				BasHTC120F1.Rs.Close();
			}
		}

		private void AltaConsulta(ref string StContenido)
		{
			//Dependiendo de los parametros opcionales que tengamos llamaremos al
			//procedimiento AltaHistoriaConsulta pasandole unos parametros u otros
			if (SDC_entrega.Text.Trim() == "")
			{
				if (SDC_devolucion.Text.Trim() == "")
				{
					if (TB_Observaciones.Text.Trim() == "")
					{
						string tempRefParam = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
						string tempRefParam2 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON");
						string tempRefParam3 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaConsulta(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam, ref tempRefParam2, ref tempRefParam3, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N");
					}
					else
					{
						string tempRefParam4 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
						string tempRefParam5 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON");
						string tempRefParam6 = SDC_peticion.Text.Trim();
						string tempRefParam7 = String.Empty;
						BasHTC120F1.AltaHistoriaConsulta(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam4, ref tempRefParam5, ref tempRefParam6, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, ref tempRefParam7, TB_Observaciones.Text.Trim());
					}
				}
				else
				{
					if (TB_Observaciones.Text.Trim() == "")
					{
						string tempRefParam8 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
						string tempRefParam9 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON");
						string tempRefParam10 = SDC_peticion.Text.Trim();
						string tempRefParam11 = SDC_devolucion.Text.Trim();
						BasHTC120F1.AltaHistoriaConsulta(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam8, ref tempRefParam9, ref tempRefParam10, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, ref tempRefParam11);
					}
					else
					{
						string tempRefParam12 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
						string tempRefParam13 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON");
						string tempRefParam14 = SDC_peticion.Text.Trim();
						string tempRefParam15 = SDC_devolucion.Text.Trim();
						BasHTC120F1.AltaHistoriaConsulta(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam12, ref tempRefParam13, ref tempRefParam14, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, ref tempRefParam15, TB_Observaciones.Text.Trim());
					}
				}
			}
			else
			{
				if (SDC_devolucion.Text.Trim() == "")
				{
					if (TB_Observaciones.Text.Trim() == "")
					{
						string tempRefParam16 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
						string tempRefParam17 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON");
						string tempRefParam18 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaConsulta(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam16, ref tempRefParam17, ref tempRefParam18, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", SDC_entrega.Text.Trim());
					}
					else
					{
						string tempRefParam19 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
						string tempRefParam20 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON");
						string tempRefParam21 = SDC_peticion.Text.Trim();
						string tempRefParam22 = String.Empty;
						BasHTC120F1.AltaHistoriaConsulta(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam19, ref tempRefParam20, ref tempRefParam21, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", SDC_entrega.Text.Trim(), ref tempRefParam22, TB_Observaciones.Text.Trim());
					}
				}
				else
				{
					if (TB_Observaciones.Text.Trim() == "")
					{
						string tempRefParam23 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
						string tempRefParam24 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON");
						string tempRefParam25 = SDC_peticion.Text.Trim();
						string tempRefParam26 = SDC_devolucion.Text.Trim();
						BasHTC120F1.AltaHistoriaConsulta(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam23, ref tempRefParam24, ref tempRefParam25, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", SDC_entrega.Text.Trim(), ref tempRefParam26);
					}
					else
					{
						string tempRefParam27 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
						string tempRefParam28 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON");
						string tempRefParam29 = SDC_peticion.Text.Trim();
						string tempRefParam30 = SDC_devolucion.Text.Trim();
						BasHTC120F1.AltaHistoriaConsulta(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam27, ref tempRefParam28, ref tempRefParam29, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", SDC_entrega.Text.Trim(), ref tempRefParam30, TB_Observaciones.Text.Trim());
					}
				}
			}
		}

		private void AltaEnfermeria(ref string StContenido)
		{
			//Dependiendo de los parametros opcionales que tengamos llamaremos al
			//procedimiento AltaHistoriaEnfermeria pasandole unos parametros u otros
			if (SDC_entrega.Text.Trim() == "")
			{
				if (SDC_devolucion.Text.Trim() == "")
				{
					if (CBB_Responsable.Text.Trim() == "")
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							string tempRefParam = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam2 = SDC_peticion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam, ref tempRefParam2, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N");
						}
						else
						{
							string tempRefParam3 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam4 = SDC_peticion.Text.Trim();
							object tempRefParam5 = String.Empty;
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam3, ref tempRefParam4, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam5, null, TB_Observaciones.Text.Trim());
						}
					}
					else
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							string tempRefParam6 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam7 = SDC_peticion.Text.Trim();
							object tempRefParam8 = String.Empty;
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam6, ref tempRefParam7, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam8, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex));
						}
						else
						{
							string tempRefParam9 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam10 = SDC_peticion.Text.Trim();
                            object tempRefParam11 = String.Empty;
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam9, ref tempRefParam10, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam11, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), TB_Observaciones.Text.Trim());
						}
					}
				}
				else
				{
					if (CBB_Responsable.Text.Trim() == "")
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							string tempRefParam12 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam13 = SDC_peticion.Text.Trim();
                            object tempRefParam14 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam12, ref tempRefParam13, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam14);
						}
						else
						{
							string tempRefParam15 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam16 = SDC_peticion.Text.Trim();
                            object tempRefParam17 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam15, ref tempRefParam16, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam17, null, TB_Observaciones.Text.Trim());
						}
					}
					else
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							string tempRefParam18 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam19 = SDC_peticion.Text.Trim();
                            object tempRefParam20 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam18, ref tempRefParam19, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam20, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex));
						}
						else
						{
							string tempRefParam21 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam22 = SDC_peticion.Text.Trim();
                            object tempRefParam23 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam21, ref tempRefParam22, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam23, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), TB_Observaciones.Text.Trim());
						}
					}
				}
			}
			else
			{
				string auxVar = String.Empty;
				if (SDC_devolucion.Text.Trim() == "")
				{
					if (CBB_Responsable.Text.Trim() == "")
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							string tempRefParam24 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam25 = SDC_peticion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam24, ref tempRefParam25, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim());
						}
						else
						{
							string tempRefParam26 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam27 = SDC_peticion.Text.Trim();
                            object tempRefParam28 = String.Empty;
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam26, ref tempRefParam27, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam28, null, TB_Observaciones.Text.Trim());
						}
					}
					else
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							string tempRefParam29 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam30 = SDC_peticion.Text.Trim();
                            object tempRefParam31 = String.Empty;
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam29, ref tempRefParam30, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam31, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex));
						}
						else
						{
							string tempRefParam32 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam33 = SDC_peticion.Text.Trim();
                            object tempRefParam34 = String.Empty;
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam32, ref tempRefParam33, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam34, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), TB_Observaciones.Text.Trim());
						}
					}
				}
				else
				{
					if (CBB_Responsable.Text.Trim() == "")
					{
						bool tempBool = false;
						auxVar = TB_Observaciones.Text.Trim();
						if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
						{
							string tempRefParam35 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam36 = SDC_peticion.Text.Trim();
                            object tempRefParam37 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam35, ref tempRefParam36, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam37);
						}
						else
						{
							string tempRefParam38 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam39 = SDC_peticion.Text.Trim();
                            object tempRefParam40 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam38, ref tempRefParam39, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam40, null, TB_Observaciones.Text.Trim());
						}
					}
					else
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							string tempRefParam41 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam42 = SDC_peticion.Text.Trim();
                            object tempRefParam43 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam41, ref tempRefParam42, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam43, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex));
						}
						else
						{
							string tempRefParam44 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE");
							object tempRefParam45 = SDC_peticion.Text.Trim();
                            object tempRefParam46 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaEnfermeria(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam44, ref tempRefParam45, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam46, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), TB_Observaciones.Text.Trim());
						}
					}
				}
			}
		}

		private void AltaExterno(ref string StContenido)
		{
			//Dependiendo de los parametros opcionales que tengamos llamaremos al
			//procedimiento AltaHistoriaExterno pasandole unos parametros u otros
			if (SDC_entrega.Text.Trim() == "")
			{
				if (SDC_devolucion.Text.Trim() == "")
				{
					if (TB_Observaciones.Text.Trim() == "")
					{
						string tempRefParam = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GENTIEXT", "DENTIEXT", "HSOLIEXT");
						object tempRefParam2 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaExterno(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam, ref tempRefParam2, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N");
					}
					else
					{
						string tempRefParam3 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GENTIEXT", "DENTIEXT", "HSOLIEXT");
						object tempRefParam4 = SDC_peticion.Text.Trim();
                        object tempRefParam5 = String.Empty;
						BasHTC120F1.AltaHistoriaExterno(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam3, ref tempRefParam4, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam5, TB_Observaciones.Text.Trim());
					}
				}
				else
				{
					if (TB_Observaciones.Text.Trim() == "")
					{
						string tempRefParam6 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GENTIEXT", "DENTIEXT", "HSOLIEXT");
						object tempRefParam7 = SDC_peticion.Text.Trim();
                        object tempRefParam8 = SDC_devolucion.Text.Trim();
						BasHTC120F1.AltaHistoriaExterno(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam6, ref tempRefParam7, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam8);
					}
					else
					{
						string tempRefParam9 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GENTIEXT", "DENTIEXT", "HSOLIEXT");
						object tempRefParam10 = SDC_peticion.Text.Trim();
                        object tempRefParam11 = SDC_devolucion.Text.Trim();
						BasHTC120F1.AltaHistoriaExterno(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam9, ref tempRefParam10, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, ref tempRefParam11, TB_Observaciones.Text.Trim());
					}
				}
			}
			else
			{
				if (SDC_devolucion.Text.Trim() == "")
				{
					if (TB_Observaciones.Text.Trim() == "")
					{
						string tempRefParam12 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GENTIEXT", "DENTIEXT", "HSOLIEXT");
						object tempRefParam13 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaExterno(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam12, ref tempRefParam13, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim());
					}
					else
					{
						string tempRefParam14 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GENTIEXT", "DENTIEXT", "HSOLIEXT");
						object tempRefParam15 = SDC_peticion.Text.Trim();
                        object tempRefParam16 = String.Empty;
						BasHTC120F1.AltaHistoriaExterno(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam14, ref tempRefParam15, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam16, TB_Observaciones.Text.Trim());
					}
				}
				else
				{
					if (TB_Observaciones.Text.Trim() == "")
					{
						string tempRefParam17 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GENTIEXT", "DENTIEXT", "HSOLIEXT");
						object tempRefParam18 = SDC_peticion.Text.Trim();
                        object tempRefParam19 = SDC_devolucion.Text.Trim();
						BasHTC120F1.AltaHistoriaExterno(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam17, ref tempRefParam18, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam19);
					}
					else
					{
						string tempRefParam20 = BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GENTIEXT", "DENTIEXT", "HSOLIEXT");
						object tempRefParam21 = SDC_peticion.Text.Trim();
                        object tempRefParam22 = SDC_devolucion.Text.Trim();
						BasHTC120F1.AltaHistoriaExterno(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam20, ref tempRefParam21, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), ref tempRefParam22, TB_Observaciones.Text.Trim());
					}
				}
			}
		}

		private void AltaSinNumero(ref string StContenido)
		{
			//Dependiendo de los parametros opcionales que tengamos llamaremos al
			//procedimiento AltaHistoriaSinNumero pasandole unos parametros u otros
			if (SDC_entrega.Text.Trim() == "")
			{
				if (SDC_devolucion.Text.Trim() == "")
				{
					if (CBB_Responsable.Text.Trim() == "")
					{
						object tempRefParam = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaSinNumero(BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI"), BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON"), ref tempRefParam, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N");
					}
					else
					{
						object tempRefParam2 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaSinNumero(BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI"), BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON"), ref tempRefParam2, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, String.Empty, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex));
					}
				}
				else
				{
					if (CBB_Responsable.Text.Trim() == "")
					{
						object tempRefParam3 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaSinNumero(BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI"), BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON"), ref tempRefParam3, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, SDC_devolucion.Text.Trim());
					}
					else
					{
						object tempRefParam4 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaSinNumero(BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI"), BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON"), ref tempRefParam4, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", String.Empty, SDC_devolucion.Text.Trim(), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex));
					}
				}
			}
			else
			{
				if (SDC_devolucion.Text.Trim() == "")
				{
					if (CBB_Responsable.Text.Trim() == "")
					{
						object tempRefParam5 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaSinNumero(BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI"), BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON"), ref tempRefParam5, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim());
					}
					else
					{
						object tempRefParam6 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaSinNumero(BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI"), BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON"), ref tempRefParam6, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), String.Empty, CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex));
					}
				}
				else
				{
					if (CBB_Responsable.Text.Trim() == "")
					{
						object tempRefParam7 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaSinNumero(BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI"), BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON"), ref tempRefParam7, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), SDC_devolucion.Text.Trim());
					}
					else
					{
						object tempRefParam8 = SDC_peticion.Text.Trim();
						BasHTC120F1.AltaHistoriaSinNumero(BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI"), BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GSALACON", "DSALACON", "DSALACON"), ref tempRefParam8, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), "N", SDC_entrega.Text.Trim(), SDC_devolucion.Text.Trim(), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex));
					}
				}
			}
		}

		private void AltaSolicitante(ref string StContenido)
		{
			//Dependiendo de los parametros opcionales que tengamos llamaremos al
			//procedimiento AltaHistoriaSolicitante pasandole unos parametros u otros
			if (CBB_unidad.Text.Trim() == "")
			{
				if (SDC_entrega.Text.Trim() == "")
				{
					if (SDC_devolucion.Text.Trim() == "")
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							object tempRefParam = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam2 = SDC_peticion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam, ref tempRefParam2, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N");
						}
						else
						{
							object tempRefParam3 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam4 = SDC_peticion.Text.Trim();
                            object tempRefParam5 = String.Empty;
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam3, ref tempRefParam4, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, String.Empty, ref tempRefParam5, TB_Observaciones.Text.Trim());
						}
					}
					else
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							object tempRefParam6 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam7 = SDC_peticion.Text.Trim();
                            object tempRefParam8 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam6, ref tempRefParam7, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, String.Empty, ref tempRefParam8);
						}
						else
						{
							object tempRefParam9 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam10 = SDC_peticion.Text.Trim();
                            object tempRefParam11 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam9, ref tempRefParam10, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, String.Empty, ref tempRefParam11, TB_Observaciones.Text.Trim());
						}
					}
				}
				else
				{
					if (SDC_devolucion.Text.Trim() == "")
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							object tempRefParam12 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam13 = SDC_peticion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam12, ref tempRefParam13, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, SDC_entrega.Text.Trim());
						}
						else
						{
							object tempRefParam14 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam15 = SDC_peticion.Text.Trim();
                            object tempRefParam16 = String.Empty;
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam14, ref tempRefParam15, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, SDC_entrega.Text.Trim(), ref tempRefParam16, TB_Observaciones.Text.Trim());
						}
					}
					else
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							object tempRefParam17 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam18 = SDC_peticion.Text.Trim();
                            object tempRefParam19 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam17, ref tempRefParam18, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, SDC_entrega.Text.Trim(), ref tempRefParam19);
						}
						else
						{
							object tempRefParam20 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam21 = SDC_peticion.Text.Trim();
                            object tempRefParam22 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam20, ref tempRefParam21, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", String.Empty, SDC_entrega.Text.Trim(), ref tempRefParam22, TB_Observaciones.Text.Trim());
						}
					}
				}
			}
			else
			{
				if (SDC_entrega.Text.Trim() == "")
				{
					if (SDC_devolucion.Text.Trim() == "")
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							object tempRefParam23 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam24 = SDC_peticion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam23, ref tempRefParam24, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GQUIROFA", "DQUIROFA", "QQUIROFA"));
						}
						else
						{
							object tempRefParam25 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam26 = SDC_peticion.Text.Trim();
                            object tempRefParam27 = String.Empty;
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam25, ref tempRefParam26, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GQUIROFA", "DQUIROFA", "QQUIROFA"), String.Empty, ref tempRefParam27, TB_Observaciones.Text.Trim());
						}
					}
					else
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							object tempRefParam28 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam29 = SDC_peticion.Text.Trim();
                            object tempRefParam30 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam28, ref tempRefParam29, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GQUIROFA", "DQUIROFA", "QQUIROFA"), String.Empty, ref tempRefParam30);
						}
						else
						{
							object tempRefParam31 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam32 = SDC_peticion.Text.Trim();
                            object tempRefParam33 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam31, ref tempRefParam32, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GQUIROFA", "DQUIROFA", "QQUIROFA"), String.Empty, ref tempRefParam33, TB_Observaciones.Text.Trim());
						}
					}
				}
				else
				{
					if (SDC_devolucion.Text.Trim() == "")
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							object tempRefParam34 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam35 = SDC_peticion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam34, ref tempRefParam35, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GQUIROFA", "DQUIROFA", "QQUIROFA"), SDC_entrega.Text.Trim());
						}
						else
						{
							object tempRefParam36 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam37 = SDC_peticion.Text.Trim();
                            object tempRefParam38 = String.Empty;
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam36, ref tempRefParam37, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GQUIROFA", "DQUIROFA", "QQUIROFA"), SDC_entrega.Text.Trim(), ref tempRefParam38, TB_Observaciones.Text.Trim());
						}
					}
					else
					{
						if (TB_Observaciones.Text.Trim() == "")
						{
							object tempRefParam39 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam40 = SDC_peticion.Text.Trim();
                            object tempRefParam41 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam39, ref tempRefParam40, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GQUIROFA", "DQUIROFA", "QQUIROFA"), SDC_entrega.Text.Trim(), ref tempRefParam41);
						}
						else
						{
							object tempRefParam42 = BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI");
							object tempRefParam43 = SDC_peticion.Text.Trim();
                            object tempRefParam44 = SDC_devolucion.Text.Trim();
							BasHTC120F1.AltaHistoriaSolicitante(ref BasHTC120F1.ICodSerArchivo, ref StContenido, BasHTC120F1.StPaciente, LB_hist.Text.Trim(), ref tempRefParam42, ref tempRefParam43, BasHTC120F1.ObtenerCodigo(CBB_motivo.Text.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH"), CBB_Responsable.GetItemData(CBB_Responsable.SelectedIndex), "N", BasHTC120F1.ObtenerCodigo(CBB_unidad.Text.Trim(), "GQUIROFA", "DQUIROFA", "QQUIROFA"), SDC_entrega.Text.Trim(), ref tempRefParam44, TB_Observaciones.Text.Trim());
						}
					}
				}
			}
		}

		private void ComprobarTodosLosDatos()
		{
			string StContenido = String.Empty;

			if (RB_Historia.IsChecked)
			{
				StContenido = "H";
			}
			else if (RB_Placas.IsChecked)
			{ 
				StContenido = "P";
			}
			else if (RB_Ambas.IsChecked)
			{ 
				StContenido = "A";
			}
			if (RB_Enfermeria.IsChecked)
			{
				if (CBB_motivo.Text == "")
				{
					short tempRefParam = 1040;
					string[] tempRefParam2 = new string[] { LB_Motivo.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BasHTC120F1.RefRc, tempRefParam2);
					return;
				}
				if (CBB_unidad.Text == "")
				{
					short tempRefParam3 = 1040;
                    string[] tempRefParam4 = new string[] { LB_Unidad.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, BasHTC120F1.RefRc, tempRefParam4);
					return;
				}
				if (CBB_Responsable.Text == "")
				{
					short tempRefParam5 = 1040;
                    string[] tempRefParam6 = new string[] { LB_Responsable.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, BasHTC120F1.RefRc, tempRefParam6);
					return;
				}
				AltaEnfermeria(ref StContenido);
			}
			else if (RB_Consulta.IsChecked)
			{ 
				if (CBB_motivo.Text == "")
				{
					short tempRefParam7 = 1040;
                    string[] tempRefParam8 = new string[] { LB_Motivo.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, BasHTC120F1.RefRc, tempRefParam8);
					return;
				}
				if (CBB_unidad.Text == "")
				{
					short tempRefParam9 = 1040;
                    string[] tempRefParam10 = new string[] { LB_Unidad.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, BasHTC120F1.RefRc, tempRefParam10);
					return;
				}
				if (CBB_Responsable.Text == "")
				{
					short tempRefParam11 = 1040;
                    string[] tempRefParam12 = new string[] { LB_Responsable.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, BasHTC120F1.RefRc, tempRefParam12);
					return;
				}
				if (CBB_peticionario.Text == "")
				{
					short tempRefParam13 = 1040;
                    string[] tempRefParam14 = new string[] { LB_Peticionario.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, BasHTC120F1.RefRc, tempRefParam14);
					return;
				}
				if (LB_hist.Text == "")
				{
					AltaSinNumero(ref StContenido);
				}
				else
				{
					AltaConsulta(ref StContenido);
				}
			}
			else if (Rb_Servicios.IsChecked)
			{ 
				if (CBB_motivo.Text == "")
				{
					short tempRefParam15 = 1040;
                    string[] tempRefParam16 = new string[] { LB_Motivo.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, BasHTC120F1.RefRc, tempRefParam16);
					return;
				}
				if (CBB_Responsable.Text == "")
				{
					short tempRefParam17 = 1040;
                    string[] tempRefParam18 = new string[] { LB_Responsable.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam17, BasHTC120F1.RefRc, tempRefParam18);
					return;
				}
				if (CBB_peticionario.Text == "")
				{
					short tempRefParam19 = 1040;
                    string[] tempRefParam20 = new string[] { LB_Peticionario.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam19, BasHTC120F1.RefRc, tempRefParam20);
					return;
				}
				AltaSolicitante(ref StContenido);
			}
			else if (RB_Entidad.IsChecked)
			{ 
				if (CBB_motivo.Text == "")
				{
					short tempRefParam21 = 1040;
                    string[] tempRefParam22 = new string[] { LB_Motivo.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam21, BasHTC120F1.RefRc, tempRefParam22);
					return;
				}
				if (CBB_unidad.Text == "")
				{
					short tempRefParam23 = 1040;
                    string[] tempRefParam24 = new string[] { LB_Unidad.Text.Trim()};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam23, BasHTC120F1.RefRc, tempRefParam24);
					return;
				}
				AltaExterno(ref StContenido);
			}
			else
			{
				short tempRefParam25 = 1040;
                string[] tempRefParam26 = new string[] { FRM_tipo.Text.Trim()};
				BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam25, BasHTC120F1.RefRc, tempRefParam26);
				return;
			}
		}
		private void LimpiarPeticionario()
		{
			SDC_devolucion.Value = SDC_devolucion.Value;
			SDC_entrega.Value = SDC_entrega.Value;
			SDC_peticion.Value = SDC_peticion.Value;
			CBB_Responsable.Items.Clear();
			CBB_unidad.Items.Clear();
		}


		public void RecogerError(int IError)
		{
			DialogResult res = (DialogResult) 0;
			if (IError != 0)
			{
				short tempRefParam = 1980;
                string[] tempRefParam2 = new string[] { "seleccionada", "prestar", "se ha producido el error" + Conversion.ErrorToString()};
				BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BasHTC120F1.RefRc, tempRefParam2);
				BasHTC120F1.ClassMensaje = null;
				this.Close();
				BasHTC120F1.FrRefFormulario.RecogerPeticion(0, "N");
			}
			else
			{
                if (BasHTC120F1.StAuto.Trim() == "N" && BasHTC120F1.OrigenSolicitud != "U" && BasHTC120F1.v_ResguardoPrestamo.Value == "S")
                {
                    short tempRefParam3 = 1820;
                    string[] tempRefParam4 = new string[] { "el resguardo del pr�stamo"};
					res = (DialogResult) Convert.ToInt32(BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, BasHTC120F1.RefRc, tempRefParam4));
					if (res == System.Windows.Forms.DialogResult.Yes)
					{
						BasHTC120F1.InformePrestamo();
					}
				}
				BasHTC120F1.ClassMensaje = null;
				this.Close();
				BasHTC120F1.FrRefFormulario.RecogerPeticion(0, "S");
			}
		}

		private void CB_Aceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string StContenido = String.Empty;
			System.DateTime datFechaPrevistaEntrega = DateTime.FromOADate(0);
			bool bOK = true;
			int intDias = 0;

			//Diego 24-05-2006

			if (validarFechaPrevistaEntrega == "S")
			{
				bOK = false;
				//si hora actual > hora de control
				if (DateTimeHelper.Time > DateTime.Parse(horaValidacionFechaPrevistaEntrega))
				{
					intDias = 2; //fecha prevista entrega 2 dias mas tarde
				}
				else
				{
					intDias = 1;
				}
				datFechaPrevistaEntrega = FechaSiguienteDiasHabiles(DateTime.Today, horaValidacionFechaPrevistaEntrega, ref intDias);
				if (SDC_entrega.Value  >= datFechaPrevistaEntrega)
				{
					bOK = true;
				}
				else
				{
					bOK = false;
					RadMessageBox.Show("La fecha prevista de entrega debe ser mayor o igual a " + datFechaPrevistaEntrega.ToString("dd/MM/yyyy"), Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
				}
			}

			if (bOK)
			{
				BasHTC120F1.ClassRefClase.FormularioExiste = true;
				ComprobarTodosLosDatos();
			}
			else
			{
			}
		}

		private void CB_cancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			BasHTC120F1.ClassMensaje = null;
			BasHTC120F1.FrRefFormulario.Cancelado();
			this.Close();
		}

		private void CBB_motivo_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			if (SDC_entrega.Text != "")
			{
				ActualizarFechaPrevistaDevolucion();
			}
			else
			{
				SDC_devolucion.Text = "";
			}
		}

		private void CBB_motivo_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (KeyCode == ((int) Keys.Delete))
			{
				KeyCode = (int) Keys.Cancel;
			}
		}

		private void CBB_motivo_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			//KeyAscii = 0
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void CBB_peticionario_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			int codigo = 0;
			if (RB_Consulta.IsChecked)
			{
				codigo = Convert.ToInt32(Double.Parse(BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI")));
				CBB_Responsable.Enabled = true;
				CBB_unidad.Enabled = true;

				BasHTC120F1.CargarResponsable("C", codigo);
			}
			else if (Rb_Servicios.IsChecked)
			{ 
				codigo = Convert.ToInt32(Double.Parse(BasHTC120F1.ObtenerCodigo(CBB_peticionario.Text.Trim(), "GSERVICI", "DNOMSERV", "DSERVICI")));
				CBB_Responsable.Enabled = true;
				BasHTC120F1.CargarResponsable("S", codigo);
			}
		}

		private void CBB_peticionario_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (KeyCode == ((int) Keys.Delete))
			{
				KeyCode = (int) Keys.Cancel;
			}
		}

		private void CBB_peticionario_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			//KeyAscii = 0
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void CBB_Responsable_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (KeyCode == ((int) Keys.Delete))
			{
				KeyCode = (int) Keys.Cancel;
			}
		}

		private void CBB_Responsable_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			//KeyAscii = 0
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void CBB_unidad_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			//
		}

		private void CBB_unidad_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (!Rb_Servicios.IsChecked)
			{
				if (KeyCode == ((int) Keys.Delete))
				{
					KeyCode = (int) Keys.Cancel;
				}
			}
		}

		private void CBB_unidad_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			//KeyAscii = 0
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void HTC120F1_Load(Object eventSender, EventArgs eventArgs)
		{

			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
            
            //this.Icon = (Icon) PIC_Peticion.Image;
            this.Icon = System.Drawing.Icon.FromHandle(((Bitmap)PIC_Peticion.Image).GetHicon());

            SDC_peticion.MaxDate = DateTime.Today;
			SDC_entrega.MinDate = DateTime.Today;
			SDC_devolucion.MinDate = DateTime.Today;
			BasHTC120F1.StAuto = "N";

			//Di�logo imprimir
			//DGMORENOG_TODO_X_5 - PENDIENTE CYSTALREPORT
			//BasHTC120F1.Cuadro_Diag_imprePrint = CDImprimir;
			
			CDAyuda.Instance.setHelpFile(this,CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			
			//DIEGO - 24-05-2006

			//Obtengo los valores asociados a la constante global CPETHIST
			validarFechaPrevistaEntrega = Serrores.ObternerValor_CTEGLOBAL(BasHTC120F1.RefRc, "CPETHIST", "valfanu1");
			horaValidacionFechaPrevistaEntrega = Serrores.ObternerValor_CTEGLOBAL(BasHTC120F1.RefRc, "CPETHIST", "valfanu2");
			intCalendarioGeneral = Convert.ToInt32(Double.Parse(Serrores.ObternerValor_CTEGLOBAL(BasHTC120F1.RefRc, "CTIPCALE", "nnumeri1")));
            RB_Entidad.Text = "Entidades externas y " + Environment.NewLine + " servicios no asistenciales";
        }

		//DIEGO - 24-05-2006
		// Devuelve una fecha intNumDias habiles superior a la fecha actual
		public System.DateTime FechaSiguienteDiasHabiles(System.DateTime datFechaActual, string strHoraControl, ref int intNumDiasHabiles)
		{
			bool bSeguir = true;
			System.DateTime datTempFecha = datFechaActual.AddDays(1);

			while(bSeguir)
			{
				BasHTC120F1.StComando = "SELECT COUNT(ffechdia) as ES_FESTIVO FROM CCALENDA WHERE ffechdia ='" + datTempFecha.ToString("MM/dd/yyyy") + "' AND gtipcale = " + intCalendarioGeneral.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
				BasHTC120F1.Rs = new DataSet();
				tempAdapter.Fill(BasHTC120F1.Rs);
				if (Convert.ToDouble(BasHTC120F1.Rs.Tables[0].Rows[0][0]) == 0)
				{ //si no encuentro nada es que no es festivo... OK
					intNumDiasHabiles--;
					if (intNumDiasHabiles == 0)
					{
						bSeguir = false;
					}
					if (bSeguir)
					{
						datTempFecha = datTempFecha.AddDays(1);
					}
				}
				else
				{
					datTempFecha = datTempFecha.AddDays(1);
				}
				BasHTC120F1.Rs.Close();
			};

			return datTempFecha;
		}

		private bool isInitializingComponent;
		private void RB_Ambas_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				PIC_contenido.Image = PIC_Ambas.Image;
			}
		}

		private void RB_Consulta_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (!PIC_tipo.Image.Equals(PIC_Consulta.Image))
				{
					PIC_tipo.Image = PIC_Consulta.Image;
					LimpiarPeticionario();
					CBB_peticionario.Enabled = true;
					CBB_Responsable.Enabled = false;
					CBB_unidad.Enabled = true;
					BasHTC120F1.CargarUnidadDestino("DSALACON", "DSALACON");
					BasHTC120F1.CargarServicio("C");
				}
			}
		}

		private void RB_Enfermeria_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (!PIC_tipo.Image.Equals(PIC_Enfermeria.Image))
				{
					PIC_tipo.Image = PIC_Enfermeria.Image;
					LimpiarPeticionario();
					CBB_peticionario.Enabled = false;
					CBB_Responsable.Enabled = true;
					CBB_unidad.Enabled = true;
					BasHTC120F1.CargarUnidadDestino("DUNIDENF", "DUNIENFE");
					BasHTC120F1.CargarResponsable("U");
				}
			}
		}

		private void RB_Entidad_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (!PIC_tipo.Image.Equals(PIC_Entidad.Image))
				{
					PIC_tipo.Image = PIC_Entidad.Image;
					LimpiarPeticionario();
					CBB_peticionario.Enabled = false;
					CBB_Responsable.Enabled = false;
					CBB_unidad.Enabled = true;
					BasHTC120F1.CargarUnidadDestino("DENTIEXT", "HSOLIEXT");
				}
			}
		}

		private void RB_Historia_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				PIC_contenido.Image = PIC_Historia.Image;
			}
		}

		private void RB_Placas_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				PIC_contenido.Image = PIC_Placas.Image;
			}
		}

		private void Rb_Servicios_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				if (!PIC_tipo.Image.Equals(PIC_servicios.Image))
				{
					PIC_tipo.Image = PIC_servicios.Image;
					LimpiarPeticionario();
					CBB_unidad.Enabled = true;
					CBB_peticionario.Enabled = true;
					CBB_Responsable.Enabled = false;
					BasHTC120F1.CargarUnidadDestino("DQUIROFA", "QQUIROFA");
					BasHTC120F1.CargarServicio("S");
				}
			}
		}

		private void SDC_entrega_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (SDC_entrega.Text != "")
			{
				ActualizarFechaPrevistaDevolucion();
			}
			else
			{
				SDC_devolucion.Text = "";
			}
		}

		private void TB_Observaciones_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		private void HTC120F1_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}