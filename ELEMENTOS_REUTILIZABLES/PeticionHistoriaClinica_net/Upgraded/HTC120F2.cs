using System;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace PeticionHistoriaClinica
{
	public partial class HTC120F2
		: RadForm
	{

		public HTC120F2()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void CB_Aceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			BasHTC120F1.FPrestamo = SDC_Prestamo.Text;
			this.Close();
		}

		private void HTC120F2_Load(Object eventSender, EventArgs eventArgs)
		{
			SDC_Prestamo.MaxDate = DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy"));
			System.DateTime TempDate = DateTime.FromOADate(0);
			SDC_Prestamo.MinDate = DateTime.Parse((DateTime.TryParse(BasHTC120F1.FechaPeticion.Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : BasHTC120F1.FechaPeticion.Trim());
		}
		private void HTC120F2_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}