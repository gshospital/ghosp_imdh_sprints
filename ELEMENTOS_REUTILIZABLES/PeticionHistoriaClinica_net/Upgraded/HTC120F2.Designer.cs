using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PeticionHistoriaClinica
{
	partial class HTC120F2
	{

		#region "Upgrade Support "
		private static HTC120F2 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static HTC120F2 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new HTC120F2();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CB_Aceptar", "SDC_Prestamo", "Label1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton CB_Aceptar;
		public Telerik.WinControls.UI.RadDateTimePicker SDC_Prestamo;
		public Telerik.WinControls.UI.RadLabel Label1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HTC120F2));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.CB_Aceptar = new Telerik.WinControls.UI.RadButton();
			this.SDC_Prestamo = new Telerik.WinControls.UI.RadDateTimePicker();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.SuspendLayout();
			// 
			// CB_Aceptar
			// 
			//this.CB_Aceptar.BackColor = System.Drawing.SystemColors.Control;
			this.CB_Aceptar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CB_Aceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CB_Aceptar.Location = new System.Drawing.Point(100, 88);
			this.CB_Aceptar.Name = "CB_Aceptar";
			this.CB_Aceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CB_Aceptar.Size = new System.Drawing.Size(81, 29);
			this.CB_Aceptar.TabIndex = 1;
			this.CB_Aceptar.Text = "&Aceptar";
			this.CB_Aceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.CB_Aceptar.UseVisualStyleBackColor = false;
			this.CB_Aceptar.Click += new System.EventHandler(this.CB_Aceptar_Click);
			// 
			// SDC_Prestamo
			// 
			//this.SDC_Prestamo.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.SDC_Prestamo.CustomFormat = "dd/MM/yyyy";
			this.SDC_Prestamo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.SDC_Prestamo.Location = new System.Drawing.Point(84, 48);
			this.SDC_Prestamo.Name = "SDC_Prestamo";
			this.SDC_Prestamo.Size = new System.Drawing.Size(113, 20);
			this.SDC_Prestamo.TabIndex = 2;
			// 
			// Label1
			// 
			//this.Label1.BackColor = System.Drawing.SystemColors.Control;
			//this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(24, 12);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(233, 21);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "Introduzca la fecha de pr�stamo y pulse aceptar:";
			// 
			// HTC120F2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(281, 131);
			this.ControlBox = false;
			this.Controls.Add(this.CB_Aceptar);
			this.Controls.Add(this.SDC_Prestamo);
			this.Controls.Add(this.Label1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location = new System.Drawing.Point(177, 174);
			this.MaximizeBox = true;
			this.MinimizeBox = true;
			this.Name = "HTC120F2";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Pr�stamo - HTC120F2";
			this.Closed += new System.EventHandler(this.HTC120F2_Closed);
			this.Load += new System.EventHandler(this.HTC120F2_Load);
			this.ResumeLayout(false);
		}
		#endregion
	}
}