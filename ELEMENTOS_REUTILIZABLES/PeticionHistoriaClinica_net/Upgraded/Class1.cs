using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace PeticionHistoriaClinica
{
	public class ClassHTC120F1
	{

		public int IError = 0;

		private void CargarUnidadDestino(string StDescripcion, string stTabla)
		{
			HTC120F1.DefInstance.CBB_unidad.Items.Clear();
			BasHTC120F1.StComando = "Select " + StDescripcion + " from  " + stTabla;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
			BasHTC120F1.Rs = new DataSet();
			tempAdapter.Fill(BasHTC120F1.Rs);
			foreach (DataRow iteration_row in BasHTC120F1.Rs.Tables[0].Rows)
			{
				HTC120F1.DefInstance.CBB_unidad.AddItem((object)Convert.ToString(iteration_row[0]),null);
			}
			BasHTC120F1.Rs.Close();
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional, object VarEntidadExterna_optional, string VarSalaConsulta, object VarSalaSolicitante_optional, object VarFechaPeticion_optional, object VarMotiPeticion_optional, object VarCodMedico_optional, object VarFechaEntrega_optional, object VarFechaDevol_optional, object VarObser_optional)
		{
			string VarTipoUnidad = (VarTipoUnidad_optional == Type.Missing) ? String.Empty : VarTipoUnidad_optional as string;
			string VarUnidadEnfermeria = (VarUnidadEnfermeria_optional == Type.Missing) ? String.Empty : VarUnidadEnfermeria_optional as string;
			object VarCodMedico = (VarCodMedico_optional == Type.Missing) ? null : VarCodMedico_optional as object;
			string VarSalaSolicitante = (VarSalaSolicitante_optional == Type.Missing) ? String.Empty : VarSalaSolicitante_optional as string;
			object VarCodServicioPeticionario = (VarCodServicioPeticionario_optional == Type.Missing) ? null : VarCodServicioPeticionario_optional as object;
			string VarEntidadExterna = (VarEntidadExterna_optional == Type.Missing) ? String.Empty : VarEntidadExterna_optional as string;
			object VarFechaPeticion = (VarFechaPeticion_optional == Type.Missing) ? null : VarFechaPeticion_optional as object;
			string VarMotiPeticion = (VarMotiPeticion_optional == Type.Missing) ? String.Empty : VarMotiPeticion_optional as string;
			string VarFechaEntrega = (VarFechaEntrega_optional == Type.Missing) ? String.Empty : VarFechaEntrega_optional as string;
			string VarFechaDevol = (VarFechaDevol_optional == Type.Missing) ? String.Empty : VarFechaDevol_optional as string;
			string VarObser = (VarObser_optional == Type.Missing) ? String.Empty : VarObser_optional as string;
			string StDescripcion = String.Empty;
			int dias = 0;
			string StCodigo = String.Empty;
			string num_sociedad = String.Empty;
			MiLoad(BasHTC120F1.ClassRefClase, BasHTC120F1.FrRefFormulario);
			BasHTC120F1.ICodSerArchivo = ICodServicioArchivo;
			BasHTC120F1.StPaciente = StIPaciente.Trim();
			BasHTC120F1.StComando = "Select NNUMERI1 from SCONSGLO where GCONSGLO='SEGURSOC' ";
			BasHTC120F1.StComando = BasHTC120F1.StComando + " or GCONSGLO='PRIVADO'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
			BasHTC120F1.Rs = new DataSet();
			tempAdapter.Fill(BasHTC120F1.Rs);
			BasHTC120F1.VarSEGURSOC = Convert.ToInt32(BasHTC120F1.Rs.Tables[0].Rows[0][0]);
			BasHTC120F1.Rs.MoveNext();
			BasHTC120F1.VarPRIVADO = Convert.ToInt32(BasHTC120F1.Rs.Tables[0].Rows[0][0]);
			BasHTC120F1.Rs.Close();

			BasHTC120F1.StComando = "Select NAFILIAC,GSOCIEDA from DPACIENT where GIDENPAC=";
			BasHTC120F1.StComando = BasHTC120F1.StComando + "'" + BasHTC120F1.StPaciente.Trim() + "'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
			BasHTC120F1.Rs = new DataSet();
			tempAdapter_2.Fill(BasHTC120F1.Rs);

			if (Convert.IsDBNull(Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["NAFILIAC"]).Trim()))
			{
				HTC120F1.DefInstance.LB_asegur.Text = "";
			}
			else
			{
				HTC120F1.DefInstance.LB_asegur.Text = Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["NAFILIAC"]).Trim();
			}

			//Comprobando que el identificativo de la sociedad no es nulo
			if (Convert.IsDBNull(Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["GSOCIEDA"]).Trim()))
			{
				BasHTC120F1.Rs.Close();
				HTC120F1.DefInstance.LB_finan.Text = "";
			}
			else
			{
				num_sociedad = Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["GSOCIEDA"]).Trim();
				BasHTC120F1.Rs.Close();
				BasHTC120F1.buscar_EFinanciadora(num_sociedad);
				HTC120F1.DefInstance.LB_finan.Text = BasHTC120F1.EFinanciera;
			}


			HTC120F1.DefInstance.LB_Servicio.Text = ObtenerDescripcion(ICodServicioArchivo, "GSERARCH", "DSERARCH", "HSERARCH");
			switch(StContActual.Trim())
			{
				case "H" : 
					HTC120F1.DefInstance.LB_Contenido.Text = "Historia"; 
					HTC120F1.DefInstance.PIC_Actual.Image = HTC120F1.DefInstance.PIC_Historia.Image; 
					HTC120F1.DefInstance.PIC_contenido.Image = HTC120F1.DefInstance.PIC_Historia.Image; 
					HTC120F1.DefInstance.RB_Historia.Enabled = true; 
					HTC120F1.DefInstance.RB_Placas.Enabled = false; 
					HTC120F1.DefInstance.RB_Ambas.Enabled = false; 
					HTC120F1.DefInstance.RB_Historia.IsChecked = true; 
					break;
				case "P" : 
					HTC120F1.DefInstance.LB_Contenido.Text = "Placas"; 
					HTC120F1.DefInstance.PIC_Actual.Image = HTC120F1.DefInstance.PIC_Placas.Image; 
					HTC120F1.DefInstance.PIC_contenido.Image = HTC120F1.DefInstance.PIC_Placas.Image; 
					HTC120F1.DefInstance.RB_Historia.Enabled = false; 
					HTC120F1.DefInstance.RB_Placas.Enabled = true; 
					HTC120F1.DefInstance.RB_Ambas.Enabled = false; 
					HTC120F1.DefInstance.RB_Placas.IsChecked = true; 
					break;
				case "A" : 
					HTC120F1.DefInstance.LB_Contenido.Text = "Ambas"; 
					HTC120F1.DefInstance.PIC_Actual.Image = HTC120F1.DefInstance.PIC_Ambas.Image; 
					HTC120F1.DefInstance.RB_Historia.Enabled = true; 
					HTC120F1.DefInstance.RB_Placas.Enabled = true; 
					HTC120F1.DefInstance.RB_Ambas.Enabled = true; 
					switch(StContenido.Trim())
					{
						case "H" : 
							HTC120F1.DefInstance.RB_Historia.IsChecked = true; 
							HTC120F1.DefInstance.PIC_contenido.Image = HTC120F1.DefInstance.PIC_Historia.Image; 
							break;
						case "P" : 
							HTC120F1.DefInstance.RB_Placas.IsChecked = true; 
							HTC120F1.DefInstance.PIC_contenido.Image = HTC120F1.DefInstance.PIC_Placas.Image; 
							break;
						case "A" : 
							HTC120F1.DefInstance.RB_Ambas.IsChecked = true; 
							HTC120F1.DefInstance.PIC_contenido.Image = HTC120F1.DefInstance.PIC_Ambas.Image; 
							break;
					} 
					break;
			}
			HTC120F1.DefInstance.LB_nombre.Text = ObtenerNombrePaciente(StIPaciente);
			HTC120F1.DefInstance.LB_cumplimentacion.Text = ObtenerCumplimentacion(LNHistoria);
			if (LNHistoria != null)
			{
				HTC120F1.DefInstance.LB_hist.Text = Convert.ToString(LNHistoria);
			}
			if (VarTipoUnidad_optional != Type.Missing && VarTipoUnidad_optional.ToString().Length > 0)
			{
				BasHTC120F1.CargarResponsable(VarTipoUnidad, VarCodServicioPeticionario, VarCodMedico);
				switch(VarTipoUnidad.Trim())
				{
					case "U" : 
						HTC120F1.DefInstance.RB_Enfermeria.IsChecked = true; 
						HTC120F1.DefInstance.PIC_tipo.Image = HTC120F1.DefInstance.PIC_Enfermeria.Image; 
						CargarUnidadDestino("DUNIDENF", "DUNIENFE"); 
						if (VarUnidadEnfermeria_optional != Type.Missing && VarUnidadEnfermeria_optional.ToString().Length > 0)
						{
							StDescripcion = ObtenerUnidadDestino(VarUnidadEnfermeria.Trim(), "GUNIDENF", "DUNIDENF", "DUNIENFE").Trim().ToUpper();
							for (int i = 0; i <= HTC120F1.DefInstance.CBB_unidad.Items.Count - 1; i++)
							{
								if (HTC120F1.DefInstance.CBB_unidad.GetListItem(i).Text.Trim().ToUpper() == StDescripcion)
								{
									HTC120F1.DefInstance.CBB_unidad.SelectedIndex = i;
									break;
								}
							}
							StDescripcion = "";
							//HTC120F1.CBB_unidad.Text = ObtenerUnidadDestino(Trim(VarUnidadEnfermeria), "GUNIDENF", "DUNIDENF", "DUNIENFE")
							if (BasHTC120F1.OrigenSolicitud == "U")
							{
								HTC120F1.DefInstance.CBB_unidad.Enabled = false;
							}
						} 
						if (VarCodMedico_optional != Type.Missing && VarCodMedico_optional.ToString().Length > 0)
						{
							StDescripcion = ObtenerResponsable(VarCodMedico, ICodServicioArchivo).Trim().ToUpper();
							for (int i = 0; i <= HTC120F1.DefInstance.CBB_Responsable.Items.Count - 1; i++)
							{
								if (HTC120F1.DefInstance.CBB_Responsable.GetListItem(i).Text.Trim().ToUpper() == StDescripcion)
								{
									HTC120F1.DefInstance.CBB_Responsable.SelectedIndex = i;
									break;
								}
							}
							StDescripcion = "";
							//HTC120F1.CBB_Responsable.Text = ObtenerResponsable(VarCodMedico, ICodServicioArchivo)
							if (BasHTC120F1.OrigenSolicitud == "U" || BasHTC120F1.OrigenSolicitud == "S")
							{
								HTC120F1.DefInstance.CBB_Responsable.Enabled = false;
							}
						} 
						HTC120F1.DefInstance.CBB_peticionario.Enabled = false; 
						break;
					case "C" : 
						HTC120F1.DefInstance.RB_Consulta.IsChecked = true; 
						 
						break;
					case "S" : 
						HTC120F1.DefInstance.Rb_Servicios.IsChecked = true; 
						HTC120F1.DefInstance.PIC_tipo.Image = HTC120F1.DefInstance.PIC_servicios.Image; 
						CargarUnidadDestino("DQUIROFA", "QQUIROFA"); 
						if (VarSalaSolicitante_optional != Type.Missing && VarSalaSolicitante_optional.ToString().Length > 0)
						{
							StDescripcion = ObtenerUnidadDestino(VarSalaSolicitante.Trim(), "GQUIROFA", "DQUIROFA", "QQUIROFA").Trim().ToUpper();
							for (int i = 0; i <= HTC120F1.DefInstance.CBB_unidad.Items.Count - 1; i++)
							{
								if (HTC120F1.DefInstance.CBB_unidad.GetListItem(i).Text.Trim().ToUpper() == StDescripcion)
								{
									HTC120F1.DefInstance.CBB_unidad.SelectedIndex = i;
									break;
								}
							}
							StDescripcion = "";
							//HTC120F1.CBB_unidad.Text = ObtenerUnidadDestino(Trim(VarSalaSolicitante), "GQUIROFA", "DQUIROFA", "QQUIROFA")
							if (BasHTC120F1.OrigenSolicitud == "U")
							{
								HTC120F1.DefInstance.CBB_unidad.Enabled = false;
							}
						} 
						BasHTC120F1.CargarServicio(); 
						if (VarCodServicioPeticionario_optional != Type.Missing && VarCodServicioPeticionario_optional.ToString().Length > 0)
						{
							StDescripcion = ObtenerDescripcion(VarCodServicioPeticionario, "GSERVICI", "DNOMSERV", "DSERVICI").Trim().ToUpper();
							for (int i = 0; i <= HTC120F1.DefInstance.CBB_peticionario.Items.Count - 1; i++)
							{
								if (HTC120F1.DefInstance.CBB_peticionario.GetListItem(i).Text.Trim().ToUpper() == StDescripcion.Trim().ToUpper())
								{
									HTC120F1.DefInstance.CBB_peticionario.SelectedIndex = i;
									break;
								}
							}
							StDescripcion = "";
							//HTC120F1.CBB_peticionario.Text = ObtenerDescripcion(VarCodServicioPeticionario, "GSERVICI", "DNOMSERV", "DSERVICI")
							if (BasHTC120F1.OrigenSolicitud == "S")
							{
								HTC120F1.DefInstance.CBB_peticionario.Enabled = false;
							}
						} 
						if (VarCodMedico_optional != Type.Missing && VarCodMedico_optional.ToString().Length > 0)
						{
							StDescripcion = ObtenerResponsable(VarCodMedico, ICodServicioArchivo).Trim().ToUpper();
							for (int i = 0; i <= HTC120F1.DefInstance.CBB_Responsable.Items.Count - 1; i++)
							{
								if (HTC120F1.DefInstance.CBB_Responsable.GetListItem(i).Text.Trim().ToUpper() == StDescripcion)
								{
									HTC120F1.DefInstance.CBB_Responsable.SelectedIndex = i;
									break;
								}
							}
							StDescripcion = "";
							//HTC120F1.CBB_Responsable.Text = ObtenerResponsable(VarCodMedico, ICodServicioArchivo)
							if (BasHTC120F1.OrigenSolicitud == "U" || BasHTC120F1.OrigenSolicitud == "S")
							{
								HTC120F1.DefInstance.CBB_Responsable.Enabled = false;
							}
						} 
						break;
					case "E" : 
						HTC120F1.DefInstance.RB_Entidad.IsChecked = true; 
						HTC120F1.DefInstance.PIC_tipo.Image = HTC120F1.DefInstance.PIC_Entidad.Image; 
						CargarUnidadDestino("DENTIEXT", "HSOLIEXT"); 
						if (VarEntidadExterna_optional != Type.Missing && VarEntidadExterna_optional.ToString().Length > 0)
						{
							HTC120F1.DefInstance.CBB_unidad.Text = ObtenerUnidadDestino(VarEntidadExterna.Trim(), "GENTIEXT", "DENTIEXT", "HSOLIEXT");
							if (BasHTC120F1.OrigenSolicitud == "U")
							{
								HTC120F1.DefInstance.CBB_unidad.Enabled = false;
							}
						} 
						HTC120F1.DefInstance.CBB_peticionario.Enabled = false; 
						HTC120F1.DefInstance.CBB_Responsable.Enabled = false; 
						break;
				}
				if (BasHTC120F1.OrigenSolicitud.Trim() == "H" || BasHTC120F1.OrigenSolicitud.Trim() == "U")
				{
					HTC120F1.DefInstance.RB_Consulta.Enabled = false;
					HTC120F1.DefInstance.RB_Entidad.Enabled = false;
					HTC120F1.DefInstance.Rb_Servicios.Enabled = false;
					HTC120F1.DefInstance.RB_Enfermeria.Enabled = false;
				}
			}
			else if (VarTipoUnidad_optional == Type.Missing || VarTipoUnidad_optional.ToString().Length == 0)
			{ 
				HTC120F1.DefInstance.RB_Consulta.IsChecked = false;
				HTC120F1.DefInstance.RB_Enfermeria.IsChecked = false;
				HTC120F1.DefInstance.RB_Entidad.IsChecked = false;
				HTC120F1.DefInstance.Rb_Servicios.IsChecked = false;
				if (BasHTC120F1.OrigenSolicitud.Trim() == "A" || BasHTC120F1.OrigenSolicitud.Trim() == "H")
				{
					HTC120F1.DefInstance.CBB_peticionario.Enabled = false;
					HTC120F1.DefInstance.CBB_Responsable.Enabled = false;
					HTC120F1.DefInstance.CBB_unidad.Enabled = false;
				}
			}
			if (VarFechaPeticion_optional != Type.Missing && VarFechaPeticion_optional.ToString().Length > 0)
			{
				if (String.CompareOrdinal(Convert.ToDateTime(VarFechaPeticion).ToString("dd/MM/yyyy"), DateTime.Today.ToString("dd/MM/yyyy")) <= 0)
				{
					HTC120F1.DefInstance.SDC_peticion.Value = DateTime.Parse(Convert.ToDateTime(VarFechaPeticion).ToString("dd/MM/yyyy"));
				}
				else
				{
					HTC120F1.DefInstance.SDC_peticion.Value = DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy"));
				}
			}
			else
			{
				HTC120F1.DefInstance.SDC_peticion.Value = DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy"));
			}
			if (BasHTC120F1.OrigenSolicitud == "S" || BasHTC120F1.OrigenSolicitud == "U")
			{
				HTC120F1.DefInstance.SDC_peticion.Enabled = false;
			}
			BasHTC120F1.CargarMotivo();
			if (VarMotiPeticion_optional != Type.Missing && VarMotiPeticion_optional.ToString().Length > 0)
			{
				StCodigo = "'" + VarMotiPeticion + "'";
				HTC120F1.DefInstance.CBB_motivo.Text = ObtenerDescripcion(StCodigo.Trim(), "GMOTPETH", "DMOTPETH", "DMOTPETH");
				if (BasHTC120F1.OrigenSolicitud == "U")
				{
					HTC120F1.DefInstance.CBB_motivo.Enabled = false;
				}
			}
			if (VarFechaEntrega_optional != Type.Missing && VarFechaEntrega_optional.ToString().Length > 0)
			{
				System.DateTime TempDate = DateTime.FromOADate(0);
				if (String.CompareOrdinal((DateTime.TryParse(VarFechaEntrega, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : VarFechaEntrega, DateTime.Today.ToString("dd/MM/yyyy")) >= 0)
				{
					System.DateTime TempDate2 = DateTime.FromOADate(0);
					HTC120F1.DefInstance.SDC_entrega.Value = DateTime.Parse((DateTime.TryParse(VarFechaEntrega, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy") : VarFechaEntrega);
				}
				else
				{
					HTC120F1.DefInstance.SDC_entrega.Value = DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy"));
				}
			}
			else
			{
				HTC120F1.DefInstance.SDC_entrega.Value = DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy"));
			}
			if (BasHTC120F1.OrigenSolicitud == "U")
			{
				HTC120F1.DefInstance.SDC_entrega.Enabled = false;
			}
			if (VarFechaDevol_optional != Type.Missing && VarFechaDevol_optional.ToString().Length > 0)
			{
				System.DateTime TempDate3 = DateTime.FromOADate(0);
				if (String.CompareOrdinal((DateTime.TryParse(VarFechaDevol, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : VarFechaDevol, DateTime.Today.ToString("dd/MM/yyyy")) >= 0)
				{
					System.DateTime TempDate4 = DateTime.FromOADate(0);
					HTC120F1.DefInstance.SDC_devolucion.Value = DateTime.Parse((DateTime.TryParse(VarFechaDevol, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy") : VarFechaDevol);
				}
				else
				{
					if (VarMotiPeticion_optional != Type.Missing && VarMotiPeticion_optional.ToString().Length > 0)
					{
						dias = BasHTC120F1.ObtenerDias(VarMotiPeticion);
						if (!Convert.IsDBNull(dias))
						{
                            //if (DateTime.Parse(DateTime.Parse(HTC120F1.DefInstance.SDC_peticion.Value.Date).ToString("dd/MM/yyyy")).AddDays(dias) >= DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                            if (HTC120F1.DefInstance.SDC_peticion.Value.AddDays(dias) >= DateTime.Today)
                            {
                                //HTC120F1.DefInstance.SDC_devolucion.Value = DateTime.Parse(DateTime.Parse(HTC120F1.DefInstance.SDC_peticion.Value.Date).ToString("dd/MM/yyyy")).AddDays(dias);
                                HTC120F1.DefInstance.SDC_devolucion.Value = HTC120F1.DefInstance.SDC_peticion.Value.AddDays(dias);
                            }
							else
							{								
                                HTC120F1.DefInstance.SDC_devolucion.NullableValue = null;
                                HTC120F1.DefInstance.SDC_devolucion.SetToNullValue();
                            }
						}
						else
						{
                            HTC120F1.DefInstance.SDC_devolucion.NullableValue = null;
                            HTC120F1.DefInstance.SDC_devolucion.SetToNullValue();
                        }
					}
					else
					{						
                        HTC120F1.DefInstance.SDC_devolucion.NullableValue = null;
                        HTC120F1.DefInstance.SDC_devolucion.SetToNullValue();
                    }
				}
			}
			else
			{
				if (VarMotiPeticion_optional != Type.Missing && VarMotiPeticion_optional.ToString().Length > 0)
				{
					dias = BasHTC120F1.ObtenerDias(VarMotiPeticion);
					if (!Convert.IsDBNull(dias))
					{
                        //if (DateTime.Parse(DateTime.Parse(HTC120F1.DefInstance.SDC_peticion.Value.Date).ToString("dd/MM/yyyy")).AddDays(dias) >= DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")))
                        if (HTC120F1.DefInstance.SDC_peticion.Value.AddDays(dias) >= DateTime.Today)
                        {
							//HTC120F1.DefInstance.SDC_devolucion.Value = DateTime.Parse(DateTime.Parse(HTC120F1.DefInstance.SDC_peticion.Value.Date).ToString("dd/MM/yyyy")).AddDays(dias);
                            HTC120F1.DefInstance.SDC_devolucion.Value = HTC120F1.DefInstance.SDC_peticion.Value.AddDays(dias);
                        }
						else
						{							
                            HTC120F1.DefInstance.SDC_devolucion.NullableValue = null;
                            HTC120F1.DefInstance.SDC_devolucion.SetToNullValue();
                        }
					}
					else
					{						
                        HTC120F1.DefInstance.SDC_devolucion.NullableValue = null;
                        HTC120F1.DefInstance.SDC_devolucion.SetToNullValue();
                    }
				}
				else
				{					
                    HTC120F1.DefInstance.SDC_devolucion.NullableValue = null;
                    HTC120F1.DefInstance.SDC_devolucion.SetToNullValue();
                }
			}
			if (VarObser_optional != Type.Missing && VarObser_optional.ToString().Length > 0)
			{
				HTC120F1.DefInstance.TB_Observaciones.Text = VarObser.Trim();
			}
			HTC120F1.DefInstance.ShowDialog();
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional, object VarEntidadExterna_optional, string VarSalaConsulta, object VarSalaSolicitante_optional, object VarFechaPeticion_optional, object VarMotiPeticion_optional, object VarCodMedico_optional, object VarFechaEntrega_optional, object VarFechaDevol_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, VarUnidadEnfermeria_optional, VarEntidadExterna_optional, VarSalaConsulta, VarSalaSolicitante_optional, VarFechaPeticion_optional, VarMotiPeticion_optional, VarCodMedico_optional, VarFechaEntrega_optional, VarFechaDevol_optional, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional, object VarEntidadExterna_optional, string VarSalaConsulta, object VarSalaSolicitante_optional, object VarFechaPeticion_optional, object VarMotiPeticion_optional, object VarCodMedico_optional, object VarFechaEntrega_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, VarUnidadEnfermeria_optional, VarEntidadExterna_optional, VarSalaConsulta, VarSalaSolicitante_optional, VarFechaPeticion_optional, VarMotiPeticion_optional, VarCodMedico_optional, VarFechaEntrega_optional, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional, object VarEntidadExterna_optional, string VarSalaConsulta, object VarSalaSolicitante_optional, object VarFechaPeticion_optional, object VarMotiPeticion_optional, object VarCodMedico_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, VarUnidadEnfermeria_optional, VarEntidadExterna_optional, VarSalaConsulta, VarSalaSolicitante_optional, VarFechaPeticion_optional, VarMotiPeticion_optional, VarCodMedico_optional, Type.Missing, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional, object VarEntidadExterna_optional, string VarSalaConsulta, object VarSalaSolicitante_optional, object VarFechaPeticion_optional, object VarMotiPeticion_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, VarUnidadEnfermeria_optional, VarEntidadExterna_optional, VarSalaConsulta, VarSalaSolicitante_optional, VarFechaPeticion_optional, VarMotiPeticion_optional, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional, object VarEntidadExterna_optional, string VarSalaConsulta, object VarSalaSolicitante_optional, object VarFechaPeticion_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, VarUnidadEnfermeria_optional, VarEntidadExterna_optional, VarSalaConsulta, VarSalaSolicitante_optional, VarFechaPeticion_optional, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional, object VarEntidadExterna_optional, string VarSalaConsulta, object VarSalaSolicitante_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, VarUnidadEnfermeria_optional, VarEntidadExterna_optional, VarSalaConsulta, VarSalaSolicitante_optional, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional, object VarEntidadExterna_optional, string VarSalaConsulta)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, VarUnidadEnfermeria_optional, VarEntidadExterna_optional, VarSalaConsulta, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional, object VarEntidadExterna_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, VarUnidadEnfermeria_optional, VarEntidadExterna_optional, String.Empty, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional, object VarUnidadEnfermeria_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, VarUnidadEnfermeria_optional, Type.Missing, String.Empty, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional, object VarCodServicioPeticionario_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, VarCodServicioPeticionario_optional, Type.Missing, Type.Missing, String.Empty, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria, object VarTipoUnidad_optional)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, VarTipoUnidad_optional, Type.Missing, Type.Missing, Type.Missing, String.Empty, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
		}

		private void LlenarDatosFormulario(string StContActual, int ICodServicioArchivo, string StContenido, string StIPaciente, object LNHistoria)
		{
			LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, LNHistoria, Type.Missing, Type.Missing, Type.Missing, Type.Missing, String.Empty, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
		}
		public object MiLoad(ClassHTC120F1 VarClass, object VarForm)
		{
			if (!VarClass.FormularioExiste)
			{
				//HTC120F1.Referencias VarClass, VarForm
				HTC120F1 tempLoadForm = HTC120F1.DefInstance;
			}
			return null;
		}
		private string ObtenerDescripcion(object ICodigo, string StCodigoTabla, string StDescTabla, string stTabla)
		{
			string result = String.Empty;
			BasHTC120F1.StComando = "Select " + StDescTabla + " from " + stTabla;
			BasHTC120F1.StComando = BasHTC120F1.StComando + " where " + StCodigoTabla + "=";
			BasHTC120F1.StComando = BasHTC120F1.StComando + Convert.ToString(ICodigo);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
			BasHTC120F1.Rs = new DataSet();
			tempAdapter.Fill(BasHTC120F1.Rs);
			if (BasHTC120F1.Rs.Tables[0].Rows.Count == 1)
			{
				result = Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0][0]).Trim();
			}
			BasHTC120F1.Rs.Close();
			return result;
		}

		private string ObtenerNombrePaciente(string StIPaciente)
		{
			string StNombre = String.Empty;
			BasHTC120F1.StComando = "Select DNOMBPAC,DAPE1PAC,DAPE2PAC from DPACIENT where ";
			BasHTC120F1.StComando = BasHTC120F1.StComando + "GIDENPAC=" + "'" + StIPaciente.Trim() + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
			BasHTC120F1.Rs = new DataSet();
			tempAdapter.Fill(BasHTC120F1.Rs);
			if (BasHTC120F1.Rs.Tables[0].Rows.Count == 1)
			{
				if (!Convert.IsDBNull(BasHTC120F1.Rs.Tables[0].Rows[0]["DNOMBPAC"]))
				{
					StNombre = Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["DNOMBPAC"]).Trim() + " ";
				}
				if (!Convert.IsDBNull(BasHTC120F1.Rs.Tables[0].Rows[0]["DAPE1PAC"]))
				{
					StNombre = StNombre + Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["DAPE1PAC"]).Trim() + " ";
				}
				if (!Convert.IsDBNull(BasHTC120F1.Rs.Tables[0].Rows[0]["DAPE2PAC"]))
				{
					StNombre = StNombre + Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["DAPE2PAC"]).Trim();
				}
			}
			BasHTC120F1.Rs.Close();
			return StNombre;
		}

		private string ObtenerCumplimentacion(object IHistoria)
		{
			//Obtiene el texto del estado de la cumplimentaci�n para el paciente con
			//el n� de historia que se pasa como par�metro
			string StCumplimentacion = String.Empty;
			BasHTC120F1.StComando = "SELECT hcumplim.dcumplim FROM hdossier,hcumplim WHERE hdossier.gcumplim=hcumplim.gcumplim ";
			BasHTC120F1.StComando = BasHTC120F1.StComando + " AND hdossier.ghistoria=" + Conversion.Val(Convert.ToString(IHistoria)).ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
			BasHTC120F1.Rs = new DataSet();
			tempAdapter.Fill(BasHTC120F1.Rs);
			if (BasHTC120F1.Rs.Tables[0].Rows.Count == 1)
			{
				if (!Convert.IsDBNull(BasHTC120F1.Rs.Tables[0].Rows[0]["DCUMPLIM"]))
				{
					StCumplimentacion = Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["DCUMPLIM"]).Trim() + " ";
				}
			}
			BasHTC120F1.Rs.Close();
			return StCumplimentacion;
		}

		private string ObtenerResponsable(object VarCodMedico, int ICodServicioArchivo)
		{
			string result = String.Empty;
			BasHTC120F1.StComando = "Select A.DNOMPERS,A.DAP1PERS,A.DAP2PERS from DPERSONA A,DPERSERV B ";
			BasHTC120F1.StComando = BasHTC120F1.StComando + "where A.GPERSONA=B.GPERSONA ";
			BasHTC120F1.StComando = BasHTC120F1.StComando + " and B.GSERVICI=" + ICodServicioArchivo.ToString();
			BasHTC120F1.StComando = BasHTC120F1.StComando + " and B.GPERSONA=" + Convert.ToString(VarCodMedico);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
			BasHTC120F1.Rs = new DataSet();
			tempAdapter.Fill(BasHTC120F1.Rs);
			if (BasHTC120F1.Rs.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0][1]).Trim() + " " + Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0][2]).Trim() + ", " + Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0][0]).Trim();
			}
			else
			{
				result = "";
			}
			BasHTC120F1.Rs.Close();
			return result;
		}

		private string ObtenerUnidadDestino(string StUnidadDestino, string StCodigo, string StDescripcion, string stTabla)
		{
			string result = String.Empty;
			BasHTC120F1.StComando = "Select " + StDescripcion + " from " + stTabla + " where ";
			BasHTC120F1.StComando = BasHTC120F1.StComando + StCodigo + "=" + "'" + StUnidadDestino.Trim() + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
			BasHTC120F1.Rs = new DataSet();
			tempAdapter.Fill(BasHTC120F1.Rs);
			if (BasHTC120F1.Rs.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0][0]).Trim();
			}
			BasHTC120F1.Rs.Close();
			return result;
		}


		public void ReferenciasClass(ClassHTC120F1 RefClass, object REFFORM, SqlConnection RC)
		{
			BasHTC120F1.FrRefFormulario = REFFORM;
			BasHTC120F1.ClassRefClase = RefClass;
			BasHTC120F1.RefRc = RC;
			Serrores.ObtenerFicheroAyuda();
			Serrores.ObtenerNombreAplicacion(BasHTC120F1.RefRc);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref BasHTC120F1.RefRc);
			Serrores.AjustarRelojCliente(BasHTC120F1.RefRc);
			BasHTC120F1.ClassMensaje = new Mensajes.ClassMensajes();

			//Si hay que mostrar el mensaje de resguardo al realizar un pr�stamo individual o masivo


			string comando = "select valfanu1, valfanu2 from sconsglo ";
			comando = comando + "where gconsglo = 'HMENRESG'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(comando, BasHTC120F1.RefRc);
			DataSet Rrs = new DataSet();
			tempAdapter.Fill(Rrs);
			BasHTC120F1.v_ResguardoPeticion.Value = "N";
			BasHTC120F1.v_ResguardoPrestamo.Value = "N";
			if (Rrs.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(Rrs.Tables[0].Rows[0]["valfanu1"]))
				{
					BasHTC120F1.v_ResguardoPeticion.Value = Convert.ToString(Rrs.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
				}

				if (!Convert.IsDBNull(Rrs.Tables[0].Rows[0]["valfanu2"]))
				{
					BasHTC120F1.v_ResguardoPrestamo.Value = Convert.ToString(Rrs.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper();
				}
			}
			Rrs.Close();

		}
		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta, string VarSalaSolicitante, ref string VarFechaPeticion, string VarMotiPeticion, string VarCodMedico, string VarFechaEntrega, ref string VarFechaDevol, string VarObser, string Consultas, int Prioridad)
		{ //, |        Optional RC As rdoConnection, Optional REFFORM As Object)
			string StContActual = String.Empty;
			string Conte = String.Empty;
			//*************************
			BasHTC120F1.vStPathReport = StPath.Trim() + "\\..\\elementoscomunes\\RPT";
			//*************************
			Serrores.AjustarRelojCliente(BasHTC120F1.RefRc);
			BasHTC120F1.PrioridadCita = Prioridad;
			BasHTC120F1.OrigenSolicitud = Origen;
			BasHTC120F1.ClassRefClase.FormularioExiste = false;
            /*If Not IsMissing(Consultas) Then
                If Not IsMissing(VarCodMedico)Then*/
            if (Consultas != String.Empty && Consultas.Length > 0 )
			{
                if (VarCodMedico != String.Empty && VarCodMedico.Length > 0)
                {
					BasHTC120F1.CodMedico = VarCodMedico; //lo relleno solo si vengo de la pantalla de consultas
				}
				else
				{
					BasHTC120F1.CodMedico = "";
				}
			}
			else
			{
				BasHTC120F1.CodMedico = "";
			}
            //If IsMissing(ICodServicioArchivo) Then
            if ((Object)ICodServicioArchivo == Type.Missing)
			{
				//Call MsgBox("El c�digo de Servicio de Archivo es obligatorio")
				short tempRefParam = 1040;
                string[] tempRefParam2 = new string[]{"c�digo de servicio de archivo"};
				BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BasHTC120F1.RefRc, tempRefParam2);
				IError = 1;
				BasHTC120F1.ClassMensaje = null;
				BasHTC120F1.FrRefFormulario.RecogerPeticion(IError);
				return;
			}
            //If IsMissing(StContenido) Then
            if ((Object)StContenido == Type.Missing || StContenido.Length == 0)
            {
				//Call MsgBox("El contenido es obligatorio")
				short tempRefParam3 = 1040;
                string[] tempRefParam4 = new string[]{"contenido"};
				BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, BasHTC120F1.RefRc, tempRefParam4);
				IError = 1;
				BasHTC120F1.ClassMensaje = null;
				BasHTC120F1.FrRefFormulario.RecogerPeticion(IError);
				return;
			}

            //If IsMissing(StIPaciente) Then
            if ((Object)StIPaciente == Type.Missing || StIPaciente.Length == 0)
            {
				//Call MsgBox("El identificador de paciente es obligatorio")
				short tempRefParam5 = 1040;
                string[] tempRefParam6 = new string[]{"identificador de paciente"};
				BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, BasHTC120F1.RefRc, tempRefParam6);
				IError = 1;
				BasHTC120F1.ClassMensaje = null;
				BasHTC120F1.FrRefFormulario.RecogerPeticion(IError);
				return;
			}
            /*If IsMissing(VarNHistoria) Then
                If Not IsMissing(VarTipoUnidad) Then*/
            if ((Object)VarNHistoria == Type.Missing || VarNHistoria.Length == 0)
            {
                if ((Object)VarTipoUnidad != Type.Missing && VarTipoUnidad.Length > 0)
                {
					if (VarTipoUnidad.Trim() == "C")
					{
						BasHTC120F1.StComando = "Select GHISTORIA from HDOSSIER where GIDENPAC=";
						BasHTC120F1.StComando = BasHTC120F1.StComando + "'" + StIPaciente.Trim() + "'";
						BasHTC120F1.StComando = BasHTC120F1.StComando + " and GSERPROPIET=";
						BasHTC120F1.StComando = BasHTC120F1.StComando + ICodServicioArchivo.ToString();
						SqlDataAdapter tempAdapter = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
						BasHTC120F1.Rs = new DataSet();
						tempAdapter.Fill(BasHTC120F1.Rs);
						if (BasHTC120F1.Rs.Tables[0].Rows.Count != 0)
						{
							VarNHistoria = Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["GHISTORIA"]);
						}
						BasHTC120F1.Rs.Close();
					}
					else
					{
						//Call MsgBox("El N� de Historia es obligatorio excepto que el Tipo de Unidad sea Consulta")
						short tempRefParam7 = 1040;
                        string[] tempRefParam8 = new string[]{"n� de historia"};
						BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, BasHTC120F1.RefRc, tempRefParam8);
						IError = 2;
						BasHTC120F1.ClassMensaje = null;
						BasHTC120F1.FrRefFormulario.RecogerPeticion(IError);
						return;
					}
				}
				else
				{
					//Call MsgBox("El N� de Historia es obligatorio excepto que el Tipo de Unidad sea Consulta")
					short tempRefParam9 = 1040;
                    string[] tempRefParam10 = new string[]{"n� de historia"};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, BasHTC120F1.RefRc, tempRefParam10);
					IError = 2;
					BasHTC120F1.ClassMensaje = null;
					BasHTC120F1.FrRefFormulario.RecogerPeticion(IError);
					return;
				}
			}
            //If Not IsMissing(VarNHistoria) Then 'Compruebo la existencia de la historia solicitada
            if ((Object)VarNHistoria != Type.Missing && VarNHistoria.Length > 0)
            { //Compruebo la existencia de la historia solicitada
				if (StContenido.Trim() == "A")
				{
					Conte = "H";
					for (int i = 1; i <= 2; i++)
					{
						BasHTC120F1.StComando = "Select * from HDOSSIER where GHISTORIA=";
						BasHTC120F1.StComando = BasHTC120F1.StComando + VarNHistoria;
						BasHTC120F1.StComando = BasHTC120F1.StComando + " and GSERPROPIET=";
						BasHTC120F1.StComando = BasHTC120F1.StComando + ICodServicioArchivo.ToString();
						BasHTC120F1.StComando = BasHTC120F1.StComando + " and ICONTENIDO=";
						BasHTC120F1.StComando = BasHTC120F1.StComando + "'" + Conte.Trim() + "'";
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
						BasHTC120F1.Rs = new DataSet();
						tempAdapter_2.Fill(BasHTC120F1.Rs);
						if (BasHTC120F1.Rs.Tables[0].Rows.Count == 0)
						{
							//Call MsgBox("El dossier solicitado no existe")
							short tempRefParam11 = 1100;
                            string[] tempRefParam12 = new string[]{"dossier solicitado"};
							BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, BasHTC120F1.RefRc, tempRefParam12);
							BasHTC120F1.Rs.Close();
							IError = 2;
							BasHTC120F1.ClassMensaje = null;
							BasHTC120F1.FrRefFormulario.RecogerPeticion(IError);
							return;
						}
						BasHTC120F1.Rs.Close();
						Conte = "P";
					}
				}
				else
				{
					BasHTC120F1.StComando = "Select * from HDOSSIER where GHISTORIA=";
					BasHTC120F1.StComando = BasHTC120F1.StComando + VarNHistoria;
					BasHTC120F1.StComando = BasHTC120F1.StComando + " and GSERPROPIET=";
					BasHTC120F1.StComando = BasHTC120F1.StComando + ICodServicioArchivo.ToString();
					BasHTC120F1.StComando = BasHTC120F1.StComando + " and ICONTENIDO=";
					BasHTC120F1.StComando = BasHTC120F1.StComando + "'" + StContenido.Trim() + "'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
					BasHTC120F1.Rs = new DataSet();
					tempAdapter_3.Fill(BasHTC120F1.Rs);
					if (BasHTC120F1.Rs.Tables[0].Rows.Count == 0)
					{
						//Call MsgBox("El dossier solicitado no existe")
						short tempRefParam13 = 1100;
                        string[] tempRefParam14 = new string[]{"dossier solicitado"};
						BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, BasHTC120F1.RefRc, tempRefParam14);
						BasHTC120F1.Rs.Close();
						IError = 2;
						BasHTC120F1.ClassMensaje = null;
						BasHTC120F1.FrRefFormulario.RecogerPeticion(IError);
						return;
					}
					BasHTC120F1.Rs.Close();
				}
				//Compruebo el contenido actual (P,H,A)
				BasHTC120F1.StComando = "Select ICONTENIDO from HDOSSIER where GHISTORIA=";
				BasHTC120F1.StComando = BasHTC120F1.StComando + VarNHistoria;
				BasHTC120F1.StComando = BasHTC120F1.StComando + " and GSERPROPIET=";
				BasHTC120F1.StComando = BasHTC120F1.StComando + ICodServicioArchivo.ToString();
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(BasHTC120F1.StComando, BasHTC120F1.RefRc);
				BasHTC120F1.Rs = new DataSet();
				tempAdapter_4.Fill(BasHTC120F1.Rs);
				if (BasHTC120F1.Rs.Tables[0].Rows.Count == 2)
				{
					StContActual = "A";
				}
				else
				{
					StContActual = Convert.ToString(BasHTC120F1.Rs.Tables[0].Rows[0]["ICONTENIDO"]).Trim();
				}
				BasHTC120F1.Rs.Close();
			}
            //If IsMissing(VarIAutomatica) Then
            if ((Object)VarIAutomatica == Type.Missing || VarIAutomatica.Length == 0)
            {
				VarIAutomatica = "N";
			}
			BasHTC120F1.StAuto = VarIAutomatica;
			if (VarIAutomatica.Trim() == "S")
			{
                //If Not IsMissing(VarTipoUnidad) Then
                if ((Object)VarTipoUnidad != Type.Missing && VarIAutomatica.Length > 0)
                {
					switch(VarTipoUnidad.Trim())
					{
						case "U" :
                            //OSCAR C 23/09/2005                             
                            //If Not IsMissing(VarCodServicioPeticionario) Then
                            if ((Object)VarCodServicioPeticionario != Type.Missing && VarCodServicioPeticionario.Length > 0)                            
							{
								if (!fGeneraPeticion(VarCodServicioPeticionario))
								{
									return;
								}
							}
                            //------------------                             
                            /*If Not IsMissing(VarUnidadEnfermeria) And Not IsMissing(VarFechaPeticion) And Not IsMissing(VarMotiPeticion) Then
                                If Not IsMissing(VarFechaEntrega) Then
                                    If Not IsMissing(VarFechaDevol) Then*/
                            if(((Object)VarUnidadEnfermeria != Type.Missing && VarUnidadEnfermeria.Length > 0) &&
                               ((Object)VarFechaPeticion != Type.Missing && VarFechaPeticion.Length > 0) &&
                               ((Object)VarMotiPeticion != Type.Missing && VarMotiPeticion.Length > 0))
							{
								if ((Object)VarFechaEntrega != Type.Missing && VarFechaEntrega.Length > 0)
								{
                                    if ((Object)VarFechaDevol != Type.Missing && VarFechaDevol.Length > 0)
                                    {
										BasHTC120F1.ComprobarFechas(VarFechaPeticion, VarFechaEntrega, VarFechaDevol);
									}
									else
									{
										BasHTC120F1.ComprobarFechas(VarFechaPeticion, VarFechaEntrega);
									}
								}
								else
								{
                                    //If Not IsMissing(VarFechaDevol) Then
                                    if ((Object)VarFechaDevol != Type.Missing && VarFechaDevol.Length > 0)
                                    {
										BasHTC120F1.ComprobarFechas(VarFechaPeticion, String.Empty, VarFechaDevol);
									}
									else
									{
										BasHTC120F1.ComprobarFechas(VarFechaPeticion);
									}
								}
								if (BasHTC120F1.FechaCorrecta)
								{
									object tempRefParam15 = VarFechaPeticion;
                                    object tempRefParamDevol = VarFechaDevol;

                                    BasHTC120F1.AltaHistoriaEnfermeria(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarUnidadEnfermeria, ref tempRefParam15, VarMotiPeticion, VarIAutomatica, VarFechaEntrega, ref tempRefParamDevol, VarCodMedico, VarObser);
									VarFechaPeticion = Convert.ToString(tempRefParam15);
								}
								else
								{
									LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
									//IError = 3
									//Call MsgBox("La fecha de petici�n debe ser <= que la fecha actual y la fecha prevista de entrega y la fecha prevista devoluci�n deben ser >= que la fecha actual")
								}
							}
							else
							{
								LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
								IError = 3;
								//Call MsgBox("No se han recibido todos los datos necesarios")
								short tempRefParam16 = 1980;
                                string[] tempRefParam17 = new string[]{"seleccionada", "solicitar", "no se han recibido todos los datos necesarios"};
								BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam16, BasHTC120F1.RefRc, tempRefParam17);
							} 
							break;
						case "C" :
                            //If Not IsMissing(Consultas) Then
                            if ((Object)Consultas != Type.Missing && Consultas.Length > 0)
                            {
								LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
							}
							else
							{
                                /*If Not IsMissing(VarCodServicioPeticionario) And Not IsMissing(VarSalaConsulta) And Not IsMissing(VarFechaPeticion) And Not IsMissing(VarMotiPeticion) Then
                                    If Not IsMissing(VarFechaEntrega) Then
                                        If Not IsMissing(VarFechaDevol) Then*/
                            if (((Object)VarCodServicioPeticionario != Type.Missing && VarCodServicioPeticionario.Length > 0) &&
                                   ((Object)VarSalaConsulta != Type.Missing && VarSalaConsulta.Length > 0) &&
                                   ((Object)VarFechaPeticion != Type.Missing && VarFechaPeticion.Length > 0) &&
                                   ((Object)VarMotiPeticion != Type.Missing && VarMotiPeticion.Length > 0))                                    
								{
									if ((Object)VarFechaEntrega != Type.Missing && VarFechaEntrega.Length > 0)
									{
                                        if ((Object)VarFechaDevol != Type.Missing && VarFechaDevol.Length > 0)
                                        {
											BasHTC120F1.ComprobarFechas(VarFechaPeticion, VarFechaEntrega, VarFechaDevol);
										}
										else
										{
											BasHTC120F1.ComprobarFechas(VarFechaPeticion, VarFechaEntrega);
										}
									}
									else
									{
                                        //If Not IsMissing(VarFechaDevol) Then
                                        if ((Object)VarFechaDevol != Type.Missing && VarFechaDevol.Length > 0)
                                        {
											BasHTC120F1.ComprobarFechas(VarFechaPeticion, String.Empty, VarFechaDevol);
										}
										else
										{
											BasHTC120F1.ComprobarFechas(VarFechaPeticion);
										}
									}
									if (BasHTC120F1.FechaCorrecta)
									{
                                        //If IsMissing(VarNHistoria)Then
                                        if ((Object)VarNHistoria == Type.Missing || VarNHistoria.Length == 0)
										{
											object tempRefParam18 = VarFechaPeticion;
											BasHTC120F1.AltaHistoriaSinNumero(ICodServicioArchivo, ref StContenido, StIPaciente, VarCodServicioPeticionario, VarSalaConsulta, ref tempRefParam18, VarMotiPeticion, VarIAutomatica, VarFechaEntrega, VarFechaDevol, VarCodMedico);
											VarFechaPeticion = Convert.ToString(tempRefParam18);
										}
										else
										{
											BasHTC120F1.AltaHistoriaConsulta(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarCodServicioPeticionario, ref VarSalaConsulta, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarIAutomatica, VarFechaEntrega, ref VarFechaDevol, VarObser);
										}
									}
									else
									{
										LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
										//Call MsgBox("La fecha de petici�n debe ser <= que la fecha actual y la fecha prevista de entrega y la fecha prevista devoluci�n deben ser >= que la fecha actual")
									}
								}
								else
								{
									LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
									IError = 3;
								}
							} 
							break;
						case "S" :
                            //OSCAR C 23/09/2005 
                            //If Not IsMissing(VarCodServicioPeticionario) Then
                            if ((Object)VarCodServicioPeticionario != Type.Missing && VarCodServicioPeticionario.Length > 0)
                            {
                                if (!fGeneraPeticion(VarCodServicioPeticionario))
								{
									return;
								}
							}
                            //------------------ 
                            /*If Not IsMissing(VarCodServicioPeticionario) And Not sMissing(VarFechaPeticion) And Not IsMissing(VarMotiPeticion) Then
                                If Not IsMissing(VarFechaEntrega) Then
                                    If Not IsMissing(VarFechaDevol) Then*/
                            if (((Object)VarCodServicioPeticionario != Type.Missing && VarCodServicioPeticionario.Length > 0) &&
                               ((Object)VarFechaPeticion != Type.Missing && VarFechaPeticion.Length > 0) &&
                               ((Object)VarMotiPeticion != Type.Missing && VarMotiPeticion.Length > 0))
							{
                                if ((Object)VarFechaEntrega != Type.Missing && VarFechaEntrega.Length > 0)
                                {
                                    if ((Object)VarFechaDevol != Type.Missing && VarFechaDevol.Length > 0)
                                    {
										BasHTC120F1.ComprobarFechas(VarFechaPeticion, VarFechaEntrega, VarFechaDevol);
									}
									else
									{
										BasHTC120F1.ComprobarFechas(VarFechaPeticion, VarFechaEntrega);
									}
								}
								else
								{
                                    //If Not IsMissing(VarFechaDevol) Then
                                    if ((Object)VarFechaDevol != Type.Missing && VarFechaDevol.Length > 0)
                                    {
										BasHTC120F1.ComprobarFechas(VarFechaPeticion, String.Empty, VarFechaDevol);
									}
									else
									{
										BasHTC120F1.ComprobarFechas(VarFechaPeticion);
									}
								}
								if (BasHTC120F1.FechaCorrecta)
								{
									object tempRefParam19 = VarCodServicioPeticionario;
									object tempRefParam20 = VarFechaPeticion;
                                    object tempRefParamDevol = VarFechaDevol;

                                    BasHTC120F1.AltaHistoriaSolicitante(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref tempRefParam19, ref tempRefParam20, VarMotiPeticion, VarCodMedico, VarIAutomatica, VarSalaSolicitante, VarFechaEntrega, ref tempRefParamDevol, VarObser);
									VarFechaPeticion = Convert.ToString(tempRefParam20);
									VarCodServicioPeticionario = Convert.ToString(tempRefParam19);
								}
								else
								{
									LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
									//Call MsgBox("La fecha de petici�n debe ser <= que la fecha actual y la fecha prevista de entrega y la fecha prevista devoluci�n deben ser >= que la fecha actual")
								}
							}
							else
							{
								LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
								IError = 3;
								//Call MsgBox("No se han recibido todos los datos necesarios")
								short tempRefParam21 = 1980;
                                string[] tempRefParam22 = new string[]{"seleccionada", "solicitar", "no se han recibido todos los datos necesarios"};
								BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam21, BasHTC120F1.RefRc, tempRefParam22);
							} 
							break;
						case "E" :
                            /*If Not IsMissing(VarEntidadExterna) And Not IsMissing(VarFechaPeticion) And Not IsMissing(VarMotiPeticion) Then
                                If Not IsMissing(VarFechaEntrega) Then
                                    If Not IsMissing(VarFechaDevol) Then*/
                            if (((Object)VarEntidadExterna != Type.Missing && VarEntidadExterna.Length > 0) &&
                               ((Object)VarFechaPeticion != Type.Missing && VarFechaPeticion.Length > 0) &&
                               ((Object)VarMotiPeticion != Type.Missing && VarMotiPeticion.Length > 0))
                            {
                                if ((Object)VarFechaEntrega != Type.Missing && VarFechaEntrega.Length > 0)
                                {
                                    if ((Object)VarFechaDevol != Type.Missing && VarFechaDevol.Length > 0)
                                    {
										BasHTC120F1.ComprobarFechas(VarFechaPeticion, VarFechaEntrega, VarFechaDevol);
									}
									else
									{
										BasHTC120F1.ComprobarFechas(VarFechaPeticion, VarFechaEntrega);
									}
								}
								else
								{
                                    //If Not IsMissing(VarFechaDevol) Then
                                    if ((Object)VarFechaDevol != Type.Missing && VarFechaDevol.Length > 0)
                                    {
										BasHTC120F1.ComprobarFechas(VarFechaPeticion, String.Empty, VarFechaDevol);
									}
									else
									{
										BasHTC120F1.ComprobarFechas(VarFechaPeticion);
									}
								}
								if (BasHTC120F1.FechaCorrecta)
								{
									object tempRefParam23 = VarFechaPeticion;
                                    object tempRefParamDevol = VarFechaDevol;

                                    BasHTC120F1.AltaHistoriaExterno(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarEntidadExterna, ref tempRefParam23, VarMotiPeticion, VarIAutomatica, VarFechaEntrega, ref tempRefParamDevol, VarObser);
									VarFechaPeticion = Convert.ToString(tempRefParam23);
								}
								else
								{
									LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
									//Call MsgBox("La fecha de petici�n debe ser <= que la fecha actual y la fecha prevista de entrega y la fecha prevista devoluci�n deben ser >= que la fecha actual")
								}
							}
							else
							{
								LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
								IError = 3;
								//Call MsgBox("No se han recibido todos los datos necesarios")
								short tempRefParam24 = 1980;
                                string[] tempRefParam25 = new string[]{"seleccionada", "solicitar", "no se han recibido todos los datos necesarios"};
								BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam24, BasHTC120F1.RefRc, tempRefParam25);
							} 
							break;
					}
				}
                //ElseIf IsMissing(VarTipoUnidad) Then
                else if ((Object)VarTipoUnidad == Type.Missing || VarTipoUnidad.Length == 0)
                { 
					LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
					IError = 3;
					//Call MsgBox("No se han recibido todos los datos necesarios")
					short tempRefParam26 = 1980;
                    string[] tempRefParam27 = new string[]{"seleccionada", "solicitar", "no se han recibido todos los datos necesarios"};
					BasHTC120F1.ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam26, BasHTC120F1.RefRc, tempRefParam27);
				}
			}
			else
			{
				LlenarDatosFormulario(StContActual, ICodServicioArchivo, StContenido, StIPaciente, VarNHistoria, VarTipoUnidad, VarCodServicioPeticionario, VarUnidadEnfermeria, VarEntidadExterna, VarSalaConsulta, VarSalaSolicitante, VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, VarFechaDevol, VarObser);
			}
			BasHTC120F1.FrRefFormulario = null;
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta, string VarSalaSolicitante, ref string VarFechaPeticion, string VarMotiPeticion, string VarCodMedico, string VarFechaEntrega, ref string VarFechaDevol, string VarObser, string Consultas)
		{
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, VarSalaSolicitante, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, ref VarFechaDevol, VarObser, Consultas, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta, string VarSalaSolicitante, ref string VarFechaPeticion, string VarMotiPeticion, string VarCodMedico, string VarFechaEntrega, ref string VarFechaDevol, string VarObser)
		{
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, VarSalaSolicitante, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, ref VarFechaDevol, VarObser, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta, string VarSalaSolicitante, ref string VarFechaPeticion, string VarMotiPeticion, string VarCodMedico, string VarFechaEntrega, ref string VarFechaDevol)
		{
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, VarSalaSolicitante, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, ref VarFechaDevol, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta, string VarSalaSolicitante, ref string VarFechaPeticion, string VarMotiPeticion, string VarCodMedico, string VarFechaEntrega)
		{
			string tempRefParam = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, VarSalaSolicitante, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarFechaEntrega, ref tempRefParam, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta, string VarSalaSolicitante, ref string VarFechaPeticion, string VarMotiPeticion, string VarCodMedico)
		{
			string tempRefParam2 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, VarSalaSolicitante, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, String.Empty, ref tempRefParam2, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta, string VarSalaSolicitante, ref string VarFechaPeticion, string VarMotiPeticion)
		{
			string tempRefParam3 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, VarSalaSolicitante, ref VarFechaPeticion, VarMotiPeticion, String.Empty, String.Empty, ref tempRefParam3, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta, string VarSalaSolicitante, ref string VarFechaPeticion)
		{
			string tempRefParam4 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, VarSalaSolicitante, ref VarFechaPeticion, String.Empty, String.Empty, String.Empty, ref tempRefParam4, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta, string VarSalaSolicitante)
		{
			string tempRefParam5 = String.Empty;
			string tempRefParam6 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, VarSalaSolicitante, ref tempRefParam5, String.Empty, String.Empty, String.Empty, ref tempRefParam6, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna, ref string VarSalaConsulta)
		{
			string tempRefParam7 = String.Empty;
			string tempRefParam8 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref VarSalaConsulta, String.Empty, ref tempRefParam7, String.Empty, String.Empty, String.Empty, ref tempRefParam8, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria, ref string VarEntidadExterna)
		{
			string tempRefParam9 = String.Empty;
			string tempRefParam10 = String.Empty;
			string tempRefParam11 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref VarEntidadExterna, ref tempRefParam9, String.Empty, ref tempRefParam10, String.Empty, String.Empty, String.Empty, ref tempRefParam11, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario, ref string VarUnidadEnfermeria)
		{
			string tempRefParam12 = String.Empty;
			string tempRefParam13 = String.Empty;
			string tempRefParam14 = String.Empty;
			string tempRefParam15 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref VarUnidadEnfermeria, ref tempRefParam12, ref tempRefParam13, String.Empty, ref tempRefParam14, String.Empty, String.Empty, String.Empty, ref tempRefParam15, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad, ref string VarCodServicioPeticionario)
		{
			string tempRefParam16 = String.Empty;
			string tempRefParam17 = String.Empty;
			string tempRefParam18 = String.Empty;
			string tempRefParam19 = String.Empty;
			string tempRefParam20 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref VarCodServicioPeticionario, ref tempRefParam16, ref tempRefParam17, ref tempRefParam18, String.Empty, ref tempRefParam19, String.Empty, String.Empty, String.Empty, ref tempRefParam20, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica, string VarTipoUnidad)
		{
			string tempRefParam21 = String.Empty;
			string tempRefParam22 = String.Empty;
			string tempRefParam23 = String.Empty;
			string tempRefParam24 = String.Empty;
			string tempRefParam25 = String.Empty;
			string tempRefParam26 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, VarTipoUnidad, ref tempRefParam21, ref tempRefParam22, ref tempRefParam23, ref tempRefParam24, String.Empty, ref tempRefParam25, String.Empty, String.Empty, String.Empty, ref tempRefParam26, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria, ref string VarIAutomatica)
		{
			string tempRefParam27 = String.Empty;
			string tempRefParam28 = String.Empty;
			string tempRefParam29 = String.Empty;
			string tempRefParam30 = String.Empty;
			string tempRefParam31 = String.Empty;
			string tempRefParam32 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref VarIAutomatica, String.Empty, ref tempRefParam27, ref tempRefParam28, ref tempRefParam29, ref tempRefParam30, String.Empty, ref tempRefParam31, String.Empty, String.Empty, String.Empty, ref tempRefParam32, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, ref string VarNHistoria)
		{
			string tempRefParam33 = String.Empty;
			string tempRefParam34 = String.Empty;
			string tempRefParam35 = String.Empty;
			string tempRefParam36 = String.Empty;
			string tempRefParam37 = String.Empty;
			string tempRefParam38 = String.Empty;
			string tempRefParam39 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref VarNHistoria, ref tempRefParam33, String.Empty, ref tempRefParam34, ref tempRefParam35, ref tempRefParam36, ref tempRefParam37, String.Empty, ref tempRefParam38, String.Empty, String.Empty, String.Empty, ref tempRefParam39, String.Empty, String.Empty, 0);
		}

		public void Recibir(string StPath, string Origen, ref int ICodServicioArchivo, ref string StContenido, string StIPaciente)
		{
			string tempRefParam40 = String.Empty;
			string tempRefParam41 = String.Empty;
			string tempRefParam42 = String.Empty;
			string tempRefParam43 = String.Empty;
			string tempRefParam44 = String.Empty;
			string tempRefParam45 = String.Empty;
			string tempRefParam46 = String.Empty;
			string tempRefParam47 = String.Empty;
			Recibir(StPath, Origen, ref ICodServicioArchivo, ref StContenido, StIPaciente, ref tempRefParam40, ref tempRefParam41, String.Empty, ref tempRefParam42, ref tempRefParam43, ref tempRefParam44, ref tempRefParam45, String.Empty, ref tempRefParam46, String.Empty, String.Empty, String.Empty, ref tempRefParam47, String.Empty, String.Empty, 0);
		}
		public bool FormularioExiste
		{
			get;
			set;
		}



		//OSCAR C 23/09/2005
		//Funcion que comprueba si el servicio genera o no peticion de historia al ingreso
		//devolvera FALSE cuando no se deba generar peticion de historia
		//devolvera TRUE cuando se deba generar peticion de historia
		private bool fGeneraPeticion(object VarCodServicioPeticionario)
		{
			bool result = false;
			result = true;
			string sql = "SELECT * FROM HSERNOGP WHERE gservici=" + Convert.ToString(VarCodServicioPeticionario);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BasHTC120F1.RefRc);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				result = false;
			}
			RR.Close();
			return result;
		}
		//------------------
	}
}
 
 