using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PeticionHistoriaClinica
{
	partial class HTC120F1
	{

		#region "Upgrade Support "
		private static HTC120F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static HTC120F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new HTC120F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "PIC_Peticion", "CrystalReport1", "PIC_servicios", "PIC_Placas", "PIC_Historia", "PIC_Ambas", "PIC_Entidad", "PIC_Consulta", "PIC_Enfermeria", "CB_cancelar", "CB_Aceptar", "PIC_Actual", "LB_cumplimentacion", "Label5", "LB_finan", "LB_hist", "LB_nombre", "Label4", "Label3", "Label2", "Label1", "LB_asegur", "Label12", "LB_Servicio", "Label15", "LB_Contenido", "FRM_paciente", "PIC_contenido", "RB_Placas", "RB_Historia", "RB_Ambas", "FRM_contenido", "CBB_motivo", "TB_Observaciones", "CBB_unidad", "CBB_peticionario", "Rb_Servicios", "PIC_tipo", "RB_Consulta", "RB_Enfermeria", "RB_Entidad", "Frame1", "CBB_Responsable", "LB_Unidad", "LB_Responsable", "LB_Peticionario", "FRM_tipo", "SDC_peticion", "SDC_entrega", "SDC_devolucion", "LbFechaPeticion", "LBPrevEntrega", "LbFechaPrevDevol", "LB_Motivo", "Label9"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public System.Windows.Forms.OpenFileDialog CDImprimirOpen;
		public System.Windows.Forms.SaveFileDialog CDImprimirSave;
		public System.Windows.Forms.FontDialog CDImprimirFont;
		public System.Windows.Forms.ColorDialog CDImprimirColor;
		public System.Windows.Forms.PrintDialog CDImprimirPrint;
		public System.Windows.Forms.PictureBox PIC_Peticion;
		//public AxCrystal.AxCrystalReport CrystalReport1;
		public System.Windows.Forms.PictureBox PIC_servicios;
		public System.Windows.Forms.PictureBox PIC_Placas;
		public System.Windows.Forms.PictureBox PIC_Historia;
		public System.Windows.Forms.PictureBox PIC_Ambas;
		public System.Windows.Forms.PictureBox PIC_Entidad;
		public System.Windows.Forms.PictureBox PIC_Consulta;
		public System.Windows.Forms.PictureBox PIC_Enfermeria;
		public Telerik.WinControls.UI.RadButton CB_cancelar;
		public Telerik.WinControls.UI.RadButton CB_Aceptar;
		public System.Windows.Forms.PictureBox PIC_Actual;
		public Telerik.WinControls.UI.RadTextBox LB_cumplimentacion;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadTextBox LB_finan;
		public Telerik.WinControls.UI.RadTextBox LB_hist;
		public Telerik.WinControls.UI.RadTextBox LB_nombre;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadTextBox LB_asegur;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadTextBox LB_Servicio;
		public Telerik.WinControls.UI.RadLabel Label15;
		public Telerik.WinControls.UI.RadTextBox LB_Contenido;
		public Telerik.WinControls.UI.RadGroupBox FRM_paciente;
		public System.Windows.Forms.PictureBox PIC_contenido;
		public Telerik.WinControls.UI.RadRadioButton RB_Placas;
		public Telerik.WinControls.UI.RadRadioButton RB_Historia;
		public Telerik.WinControls.UI.RadRadioButton RB_Ambas;
		public Telerik.WinControls.UI.RadGroupBox FRM_contenido;
		public UpgradeHelpers.MSForms.MSCombobox CBB_motivo;
		public Telerik.WinControls.UI.RadTextBoxControl TB_Observaciones;
		public UpgradeHelpers.MSForms.MSCombobox CBB_unidad;
		public UpgradeHelpers.MSForms.MSCombobox CBB_peticionario;
		public Telerik.WinControls.UI.RadRadioButton Rb_Servicios;
		public System.Windows.Forms.PictureBox PIC_tipo;
		public Telerik.WinControls.UI.RadRadioButton RB_Consulta;
		public Telerik.WinControls.UI.RadRadioButton RB_Enfermeria;
		public Telerik.WinControls.UI.RadRadioButton RB_Entidad;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public UpgradeHelpers.MSForms.MSCombobox CBB_Responsable;
		public Telerik.WinControls.UI.RadLabel LB_Unidad;
		public Telerik.WinControls.UI.RadLabel LB_Responsable;
		public Telerik.WinControls.UI.RadLabel LB_Peticionario;
		public Telerik.WinControls.UI.RadGroupBox FRM_tipo;
		public Telerik.WinControls.UI.RadDateTimePicker SDC_peticion;
		public Telerik.WinControls.UI.RadDateTimePicker SDC_entrega;
		public Telerik.WinControls.UI.RadDateTimePicker SDC_devolucion;
		public System.Windows.Forms.OpenFileDialog CDAyudaOpen;
		public System.Windows.Forms.SaveFileDialog CDAyudaSave;
		public System.Windows.Forms.FontDialog CDAyudaFont;
		public System.Windows.Forms.ColorDialog CDAyudaColor;
		public System.Windows.Forms.PrintDialog CDAyudaPrint;
		public Telerik.WinControls.UI.RadLabel LbFechaPeticion;
		public Telerik.WinControls.UI.RadLabel LBPrevEntrega;
		public Telerik.WinControls.UI.RadLabel LbFechaPrevDevol;
		public Telerik.WinControls.UI.RadLabel LB_Motivo;
		public Telerik.WinControls.UI.RadLabel Label9;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HTC120F1));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.CDImprimirOpen = new System.Windows.Forms.OpenFileDialog();
            this.CDImprimirSave = new System.Windows.Forms.SaveFileDialog();
            this.CDImprimirFont = new System.Windows.Forms.FontDialog();
            this.CDImprimirColor = new System.Windows.Forms.ColorDialog();
            this.CDImprimirPrint = new System.Windows.Forms.PrintDialog();
            this.PIC_Peticion = new System.Windows.Forms.PictureBox();
            this.PIC_servicios = new System.Windows.Forms.PictureBox();
            this.PIC_Placas = new System.Windows.Forms.PictureBox();
            this.PIC_Historia = new System.Windows.Forms.PictureBox();
            this.PIC_Ambas = new System.Windows.Forms.PictureBox();
            this.PIC_Entidad = new System.Windows.Forms.PictureBox();
            this.PIC_Consulta = new System.Windows.Forms.PictureBox();
            this.PIC_Enfermeria = new System.Windows.Forms.PictureBox();
            this.CB_cancelar = new Telerik.WinControls.UI.RadButton();
            this.CB_Aceptar = new Telerik.WinControls.UI.RadButton();
            this.FRM_paciente = new Telerik.WinControls.UI.RadGroupBox();
            this.PIC_Actual = new System.Windows.Forms.PictureBox();
            this.LB_cumplimentacion = new Telerik.WinControls.UI.RadTextBox();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.LB_finan = new Telerik.WinControls.UI.RadTextBox();
            this.LB_hist = new Telerik.WinControls.UI.RadTextBox();
            this.LB_nombre = new Telerik.WinControls.UI.RadTextBox();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.LB_asegur = new Telerik.WinControls.UI.RadTextBox();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.LB_Servicio = new Telerik.WinControls.UI.RadTextBox();
            this.Label15 = new Telerik.WinControls.UI.RadLabel();
            this.LB_Contenido = new Telerik.WinControls.UI.RadTextBox();
            this.FRM_contenido = new Telerik.WinControls.UI.RadGroupBox();
            this.PIC_contenido = new System.Windows.Forms.PictureBox();
            this.RB_Placas = new Telerik.WinControls.UI.RadRadioButton();
            this.RB_Historia = new Telerik.WinControls.UI.RadRadioButton();
            this.RB_Ambas = new Telerik.WinControls.UI.RadRadioButton();
            this.CBB_motivo = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.TB_Observaciones = new Telerik.WinControls.UI.RadTextBoxControl();
            this.FRM_tipo = new Telerik.WinControls.UI.RadGroupBox();
            this.CBB_unidad = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.CBB_peticionario = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Rb_Servicios = new Telerik.WinControls.UI.RadRadioButton();
            this.PIC_tipo = new System.Windows.Forms.PictureBox();
            this.RB_Consulta = new Telerik.WinControls.UI.RadRadioButton();
            this.RB_Enfermeria = new Telerik.WinControls.UI.RadRadioButton();
            this.RB_Entidad = new Telerik.WinControls.UI.RadRadioButton();
            this.CBB_Responsable = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.LB_Unidad = new Telerik.WinControls.UI.RadLabel();
            this.LB_Responsable = new Telerik.WinControls.UI.RadLabel();
            this.LB_Peticionario = new Telerik.WinControls.UI.RadLabel();
            this.SDC_peticion = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SDC_entrega = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SDC_devolucion = new Telerik.WinControls.UI.RadDateTimePicker();
            this.CDAyudaOpen = new System.Windows.Forms.OpenFileDialog();
            this.CDAyudaSave = new System.Windows.Forms.SaveFileDialog();
            this.CDAyudaFont = new System.Windows.Forms.FontDialog();
            this.CDAyudaColor = new System.Windows.Forms.ColorDialog();
            this.CDAyudaPrint = new System.Windows.Forms.PrintDialog();
            this.LbFechaPeticion = new Telerik.WinControls.UI.RadLabel();
            this.LBPrevEntrega = new Telerik.WinControls.UI.RadLabel();
            this.LbFechaPrevDevol = new Telerik.WinControls.UI.RadLabel();
            this.LB_Motivo = new Telerik.WinControls.UI.RadLabel();
            this.Label9 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Peticion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Placas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Historia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Ambas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Entidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Consulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Enfermeria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CB_cancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CB_Aceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_paciente)).BeginInit();
            this.FRM_paciente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Actual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_cumplimentacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_finan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_hist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_asegur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Servicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Contenido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_contenido)).BeginInit();
            this.FRM_contenido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_contenido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Placas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Historia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Ambas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBB_motivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TB_Observaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_tipo)).BeginInit();
            this.FRM_tipo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CBB_unidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBB_peticionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Rb_Servicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_tipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Consulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Enfermeria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Entidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBB_Responsable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Unidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Responsable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Peticionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SDC_peticion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SDC_entrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SDC_devolucion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechaPeticion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LBPrevEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechaPrevDevol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Motivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // PIC_Peticion
            // 
            this.PIC_Peticion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PIC_Peticion.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_Peticion.Image = ((System.Drawing.Image)(resources.GetObject("PIC_Peticion.Image")));
            this.PIC_Peticion.Location = new System.Drawing.Point(52, 416);
            this.PIC_Peticion.Name = "PIC_Peticion";
            this.PIC_Peticion.Size = new System.Drawing.Size(36, 35);
            this.PIC_Peticion.TabIndex = 51;
            this.PIC_Peticion.TabStop = false;
            this.PIC_Peticion.Visible = false;
            // 
            // PIC_servicios
            // 
            this.PIC_servicios.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PIC_servicios.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_servicios.Image = ((System.Drawing.Image)(resources.GetObject("PIC_servicios.Image")));
            this.PIC_servicios.Location = new System.Drawing.Point(212, 415);
            this.PIC_servicios.Name = "PIC_servicios";
            this.PIC_servicios.Size = new System.Drawing.Size(36, 35);
            this.PIC_servicios.TabIndex = 50;
            this.PIC_servicios.TabStop = false;
            this.PIC_servicios.Visible = false;
            // 
            // PIC_Placas
            // 
            this.PIC_Placas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PIC_Placas.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_Placas.Image = ((System.Drawing.Image)(resources.GetObject("PIC_Placas.Image")));
            this.PIC_Placas.Location = new System.Drawing.Point(348, 415);
            this.PIC_Placas.Name = "PIC_Placas";
            this.PIC_Placas.Size = new System.Drawing.Size(36, 35);
            this.PIC_Placas.TabIndex = 49;
            this.PIC_Placas.TabStop = false;
            this.PIC_Placas.Visible = false;
            // 
            // PIC_Historia
            // 
            this.PIC_Historia.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PIC_Historia.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_Historia.Image = ((System.Drawing.Image)(resources.GetObject("PIC_Historia.Image")));
            this.PIC_Historia.Location = new System.Drawing.Point(308, 415);
            this.PIC_Historia.Name = "PIC_Historia";
            this.PIC_Historia.Size = new System.Drawing.Size(36, 35);
            this.PIC_Historia.TabIndex = 48;
            this.PIC_Historia.TabStop = false;
            this.PIC_Historia.Visible = false;
            // 
            // PIC_Ambas
            // 
            this.PIC_Ambas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PIC_Ambas.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_Ambas.Image = ((System.Drawing.Image)(resources.GetObject("PIC_Ambas.Image")));
            this.PIC_Ambas.Location = new System.Drawing.Point(268, 415);
            this.PIC_Ambas.Name = "PIC_Ambas";
            this.PIC_Ambas.Size = new System.Drawing.Size(36, 35);
            this.PIC_Ambas.TabIndex = 47;
            this.PIC_Ambas.TabStop = false;
            this.PIC_Ambas.Visible = false;
            // 
            // PIC_Entidad
            // 
            this.PIC_Entidad.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PIC_Entidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_Entidad.Image = ((System.Drawing.Image)(resources.GetObject("PIC_Entidad.Image")));
            this.PIC_Entidad.Location = new System.Drawing.Point(172, 415);
            this.PIC_Entidad.Name = "PIC_Entidad";
            this.PIC_Entidad.Size = new System.Drawing.Size(36, 35);
            this.PIC_Entidad.TabIndex = 46;
            this.PIC_Entidad.TabStop = false;
            this.PIC_Entidad.Visible = false;
            // 
            // PIC_Consulta
            // 
            this.PIC_Consulta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PIC_Consulta.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_Consulta.Image = ((System.Drawing.Image)(resources.GetObject("PIC_Consulta.Image")));
            this.PIC_Consulta.Location = new System.Drawing.Point(132, 415);
            this.PIC_Consulta.Name = "PIC_Consulta";
            this.PIC_Consulta.Size = new System.Drawing.Size(36, 35);
            this.PIC_Consulta.TabIndex = 45;
            this.PIC_Consulta.TabStop = false;
            this.PIC_Consulta.Visible = false;
            // 
            // PIC_Enfermeria
            // 
            this.PIC_Enfermeria.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PIC_Enfermeria.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_Enfermeria.Image = ((System.Drawing.Image)(resources.GetObject("PIC_Enfermeria.Image")));
            this.PIC_Enfermeria.Location = new System.Drawing.Point(92, 415);
            this.PIC_Enfermeria.Name = "PIC_Enfermeria";
            this.PIC_Enfermeria.Size = new System.Drawing.Size(36, 35);
            this.PIC_Enfermeria.TabIndex = 44;
            this.PIC_Enfermeria.TabStop = false;
            this.PIC_Enfermeria.Visible = false;
            // 
            // CB_cancelar
            // 
            this.CB_cancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CB_cancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CB_cancelar.Location = new System.Drawing.Point(639, 410);
            this.CB_cancelar.Name = "CB_cancelar";
            this.CB_cancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CB_cancelar.Size = new System.Drawing.Size(81, 29);
            this.CB_cancelar.TabIndex = 16;
            this.CB_cancelar.Text = "&Cancelar";
            this.CB_cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CB_cancelar.Click += new System.EventHandler(this.CB_cancelar_Click);
            // 
            // CB_Aceptar
            // 
            this.CB_Aceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CB_Aceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CB_Aceptar.Location = new System.Drawing.Point(548, 410);
            this.CB_Aceptar.Name = "CB_Aceptar";
            this.CB_Aceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CB_Aceptar.Size = new System.Drawing.Size(81, 29);
            this.CB_Aceptar.TabIndex = 15;
            this.CB_Aceptar.Text = "&Aceptar";
            this.CB_Aceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CB_Aceptar.Click += new System.EventHandler(this.CB_Aceptar_Click);
            // 
            // FRM_paciente
            // 
            this.FRM_paciente.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FRM_paciente.Controls.Add(this.PIC_Actual);
            this.FRM_paciente.Controls.Add(this.LB_cumplimentacion);
            this.FRM_paciente.Controls.Add(this.Label5);
            this.FRM_paciente.Controls.Add(this.LB_finan);
            this.FRM_paciente.Controls.Add(this.LB_hist);
            this.FRM_paciente.Controls.Add(this.LB_nombre);
            this.FRM_paciente.Controls.Add(this.Label4);
            this.FRM_paciente.Controls.Add(this.Label3);
            this.FRM_paciente.Controls.Add(this.Label2);
            this.FRM_paciente.Controls.Add(this.Label1);
            this.FRM_paciente.Controls.Add(this.LB_asegur);
            this.FRM_paciente.Controls.Add(this.Label12);
            this.FRM_paciente.Controls.Add(this.LB_Servicio);
            this.FRM_paciente.Controls.Add(this.Label15);
            this.FRM_paciente.Controls.Add(this.LB_Contenido);
            this.FRM_paciente.HeaderText = " Datos del paciente ";
            this.FRM_paciente.Location = new System.Drawing.Point(4, 4);
            this.FRM_paciente.Name = "FRM_paciente";
            this.FRM_paciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FRM_paciente.Size = new System.Drawing.Size(719, 138);
            this.FRM_paciente.TabIndex = 25;
            this.FRM_paciente.Text = " Datos del paciente ";
            // 
            // PIC_Actual
            // 
            this.PIC_Actual.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_Actual.Image = ((System.Drawing.Image)(resources.GetObject("PIC_Actual.Image")));
            this.PIC_Actual.Location = new System.Drawing.Point(168, 74);
            this.PIC_Actual.Name = "PIC_Actual";
            this.PIC_Actual.Size = new System.Drawing.Size(36, 35);
            this.PIC_Actual.TabIndex = 26;
            this.PIC_Actual.TabStop = false;
            // 
            // LB_cumplimentacion
            // 
            this.LB_cumplimentacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_cumplimentacion.Enabled = false;
            this.LB_cumplimentacion.Location = new System.Drawing.Point(476, 80);
            this.LB_cumplimentacion.Name = "LB_cumplimentacion";
            this.LB_cumplimentacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_cumplimentacion.Size = new System.Drawing.Size(161, 20);
            this.LB_cumplimentacion.TabIndex = 53;
            // 
            // Label5
            // 
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Location = new System.Drawing.Point(376, 84);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(97, 18);
            this.Label5.TabIndex = 52;
            this.Label5.Text = "Cumplimentaci�n:";
            // 
            // LB_finan
            // 
            this.LB_finan.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_finan.Enabled = false;
            this.LB_finan.Location = new System.Drawing.Point(116, 111);
            this.LB_finan.Name = "LB_finan";
            this.LB_finan.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_finan.Size = new System.Drawing.Size(266, 20);
            this.LB_finan.TabIndex = 38;
            // 
            // LB_hist
            // 
            this.LB_hist.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_hist.Enabled = false;
            this.LB_hist.Location = new System.Drawing.Point(533, 24);
            this.LB_hist.Name = "LB_hist";
            this.LB_hist.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_hist.Size = new System.Drawing.Size(104, 20);
            this.LB_hist.TabIndex = 37;
            // 
            // LB_nombre
            // 
            this.LB_nombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_nombre.Enabled = false;
            this.LB_nombre.Location = new System.Drawing.Point(116, 52);
            this.LB_nombre.Name = "LB_nombre";
            this.LB_nombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_nombre.Size = new System.Drawing.Size(521, 20);
            this.LB_nombre.TabIndex = 36;
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(420, 26);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(99, 18);
            this.Label4.TabIndex = 35;
            this.Label4.Text = "N� Historia Cl�nica:";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(16, 113);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(84, 18);
            this.Label3.TabIndex = 34;
            this.Label3.Text = "E. Financiadora:";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(17, 52);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(51, 18);
            this.Label2.TabIndex = 33;
            this.Label2.Text = "Paciente:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(388, 114);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(79, 18);
            this.Label1.TabIndex = 32;
            this.Label1.Text = "N� Asegurado:";
            // 
            // LB_asegur
            // 
            this.LB_asegur.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_asegur.Enabled = false;
            this.LB_asegur.Location = new System.Drawing.Point(475, 111);
            this.LB_asegur.Name = "LB_asegur";
            this.LB_asegur.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_asegur.Size = new System.Drawing.Size(162, 20);
            this.LB_asegur.TabIndex = 31;
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Location = new System.Drawing.Point(17, 26);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(89, 18);
            this.Label12.TabIndex = 30;
            this.Label12.Text = "Serv. de Archivo:";
            // 
            // LB_Servicio
            // 
            this.LB_Servicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_Servicio.Enabled = false;
            this.LB_Servicio.Location = new System.Drawing.Point(116, 24);
            this.LB_Servicio.Name = "LB_Servicio";
            this.LB_Servicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_Servicio.Size = new System.Drawing.Size(266, 20);
            this.LB_Servicio.TabIndex = 29;
            // 
            // Label15
            // 
            this.Label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label15.Location = new System.Drawing.Point(17, 80);
            this.Label15.Name = "Label15";
            this.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label15.Size = new System.Drawing.Size(95, 18);
            this.Label15.TabIndex = 28;
            this.Label15.Text = "Contenido Actual:";
            // 
            // LB_Contenido
            // 
            this.LB_Contenido.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_Contenido.Enabled = false;
            this.LB_Contenido.Location = new System.Drawing.Point(116, 80);
            this.LB_Contenido.Name = "LB_Contenido";
            this.LB_Contenido.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_Contenido.Size = new System.Drawing.Size(45, 20);
            this.LB_Contenido.TabIndex = 27;
            // 
            // FRM_contenido
            // 
            this.FRM_contenido.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FRM_contenido.Controls.Add(this.PIC_contenido);
            this.FRM_contenido.Controls.Add(this.RB_Placas);
            this.FRM_contenido.Controls.Add(this.RB_Historia);
            this.FRM_contenido.Controls.Add(this.RB_Ambas);
            this.FRM_contenido.HeaderText = " Contenido ";
            this.FRM_contenido.Location = new System.Drawing.Point(640, 146);
            this.FRM_contenido.Name = "FRM_contenido";
            this.FRM_contenido.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FRM_contenido.Size = new System.Drawing.Size(86, 150);
            this.FRM_contenido.TabIndex = 23;
            this.FRM_contenido.Text = " Contenido ";
            // 
            // PIC_contenido
            // 
            this.PIC_contenido.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_contenido.Image = ((System.Drawing.Image)(resources.GetObject("PIC_contenido.Image")));
            this.PIC_contenido.Location = new System.Drawing.Point(30, 14);
            this.PIC_contenido.Name = "PIC_contenido";
            this.PIC_contenido.Size = new System.Drawing.Size(34, 34);
            this.PIC_contenido.TabIndex = 24;
            this.PIC_contenido.TabStop = false;
            // 
            // RB_Placas
            // 
            this.RB_Placas.Cursor = System.Windows.Forms.Cursors.Default;
            this.RB_Placas.Location = new System.Drawing.Point(10, 86);
            this.RB_Placas.Name = "RB_Placas";
            this.RB_Placas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RB_Placas.Size = new System.Drawing.Size(51, 18);
            this.RB_Placas.TabIndex = 8;
            this.RB_Placas.Text = "Placas";
            this.RB_Placas.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RB_Placas_CheckedChanged);
            // 
            // RB_Historia
            // 
            this.RB_Historia.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RB_Historia.Cursor = System.Windows.Forms.Cursors.Default;
            this.RB_Historia.Location = new System.Drawing.Point(10, 54);
            this.RB_Historia.Name = "RB_Historia";
            this.RB_Historia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RB_Historia.Size = new System.Drawing.Size(59, 18);
            this.RB_Historia.TabIndex = 7;
            this.RB_Historia.Text = "Historia";
            this.RB_Historia.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.RB_Historia.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RB_Historia_CheckedChanged);
            // 
            // RB_Ambas
            // 
            this.RB_Ambas.Cursor = System.Windows.Forms.Cursors.Default;
            this.RB_Ambas.Location = new System.Drawing.Point(10, 120);
            this.RB_Ambas.Name = "RB_Ambas";
            this.RB_Ambas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RB_Ambas.Size = new System.Drawing.Size(54, 18);
            this.RB_Ambas.TabIndex = 9;
            this.RB_Ambas.Text = "Ambas";
            this.RB_Ambas.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RB_Ambas_CheckedChanged);
            // 
            // CBB_motivo
            // 
            this.CBB_motivo.ColumnWidths = "";
            this.CBB_motivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.CBB_motivo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CBB_motivo.Location = new System.Drawing.Point(56, 307);
            this.CBB_motivo.Name = "CBB_motivo";
            this.CBB_motivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CBB_motivo.Size = new System.Drawing.Size(219, 20);
            this.CBB_motivo.TabIndex = 10;
            this.CBB_motivo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CBB_motivo_KeyPress);
            this.CBB_motivo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CBB_motivo_KeyDown);
            this.CBB_motivo.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CBB_motivo_SelectedIndexChanged);
            // 
            // TB_Observaciones
            // 
            this.TB_Observaciones.AcceptsReturn = true;
            this.TB_Observaciones.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TB_Observaciones.Location = new System.Drawing.Point(306, 325);
            this.TB_Observaciones.MaxLength = 100;
            this.TB_Observaciones.Multiline = true;
            this.TB_Observaciones.Name = "TB_Observaciones";
            this.TB_Observaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TB_Observaciones.Size = new System.Drawing.Size(417, 58);
            this.TB_Observaciones.TabIndex = 14;
            this.TB_Observaciones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TB_Observaciones_KeyPress);
            // 
            // FRM_tipo
            // 
            this.FRM_tipo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FRM_tipo.Controls.Add(this.CBB_unidad);
            this.FRM_tipo.Controls.Add(this.CBB_peticionario);
            this.FRM_tipo.Controls.Add(this.Frame1);
            this.FRM_tipo.Controls.Add(this.CBB_Responsable);
            this.FRM_tipo.Controls.Add(this.LB_Unidad);
            this.FRM_tipo.Controls.Add(this.LB_Responsable);
            this.FRM_tipo.Controls.Add(this.LB_Peticionario);
            this.FRM_tipo.HeaderText = " Tipo de unidad ";
            this.FRM_tipo.Location = new System.Drawing.Point(4, 146);
            this.FRM_tipo.Name = "FRM_tipo";
            this.FRM_tipo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FRM_tipo.Size = new System.Drawing.Size(632, 150);
            this.FRM_tipo.TabIndex = 17;
            this.FRM_tipo.Text = " Tipo de unidad ";
            // 
            // CBB_unidad
            // 
            this.CBB_unidad.ColumnWidths = "";
            this.CBB_unidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.CBB_unidad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CBB_unidad.Location = new System.Drawing.Point(286, 36);
            this.CBB_unidad.Name = "CBB_unidad";
            this.CBB_unidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CBB_unidad.Size = new System.Drawing.Size(343, 20);
            this.CBB_unidad.TabIndex = 4;
            this.CBB_unidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CBB_unidad_KeyPress);
            this.CBB_unidad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CBB_unidad_KeyDown);
            this.CBB_unidad.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CBB_unidad_SelectedIndexChanged);
            // 
            // CBB_peticionario
            // 
            this.CBB_peticionario.ColumnWidths = "";
            this.CBB_peticionario.Cursor = System.Windows.Forms.Cursors.Default;
            this.CBB_peticionario.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CBB_peticionario.Location = new System.Drawing.Point(285, 64);
            this.CBB_peticionario.Name = "CBB_peticionario";
            this.CBB_peticionario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CBB_peticionario.Size = new System.Drawing.Size(342, 20);
            this.CBB_peticionario.TabIndex = 5;
            this.CBB_peticionario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CBB_peticionario_KeyPress);
            this.CBB_peticionario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CBB_peticionario_KeyDown);
            this.CBB_peticionario.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CBB_peticionario_SelectedIndexChanged);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.Rb_Servicios);
            this.Frame1.Controls.Add(this.PIC_tipo);
            this.Frame1.Controls.Add(this.RB_Consulta);
            this.Frame1.Controls.Add(this.RB_Enfermeria);
            this.Frame1.Controls.Add(this.RB_Entidad);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(8, 16);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(167, 129);
            this.Frame1.TabIndex = 18;
            // 
            // Rb_Servicios
            // 
            this.Rb_Servicios.Cursor = System.Windows.Forms.Cursors.Default;
            this.Rb_Servicios.Location = new System.Drawing.Point(8, 97);
            this.Rb_Servicios.Name = "Rb_Servicios";
            this.Rb_Servicios.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Rb_Servicios.Size = new System.Drawing.Size(109, 18);
            this.Rb_Servicios.TabIndex = 3;
            this.Rb_Servicios.Text = "Servicios m�dicos";
            this.Rb_Servicios.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.Rb_Servicios_CheckedChanged);
            // 
            // PIC_tipo
            // 
            this.PIC_tipo.Cursor = System.Windows.Forms.Cursors.Default;
            this.PIC_tipo.Image = ((System.Drawing.Image)(resources.GetObject("PIC_tipo.Image")));
            this.PIC_tipo.Location = new System.Drawing.Point(104, 16);
            this.PIC_tipo.Name = "PIC_tipo";
            this.PIC_tipo.Size = new System.Drawing.Size(34, 34);
            this.PIC_tipo.TabIndex = 19;
            this.PIC_tipo.TabStop = false;
            // 
            // RB_Consulta
            // 
            this.RB_Consulta.Cursor = System.Windows.Forms.Cursors.Default;
            this.RB_Consulta.Location = new System.Drawing.Point(8, 33);
            this.RB_Consulta.Name = "RB_Consulta";
            this.RB_Consulta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RB_Consulta.Size = new System.Drawing.Size(64, 18);
            this.RB_Consulta.TabIndex = 1;
            this.RB_Consulta.Text = "Consulta";
            this.RB_Consulta.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RB_Consulta_CheckedChanged);
            // 
            // RB_Enfermeria
            // 
            this.RB_Enfermeria.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RB_Enfermeria.Cursor = System.Windows.Forms.Cursors.Default;
            this.RB_Enfermeria.Location = new System.Drawing.Point(8, 9);
            this.RB_Enfermeria.Name = "RB_Enfermeria";
            this.RB_Enfermeria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RB_Enfermeria.Size = new System.Drawing.Size(74, 18);
            this.RB_Enfermeria.TabIndex = 0;
            this.RB_Enfermeria.Text = "Enfermer�a";
            this.RB_Enfermeria.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.RB_Enfermeria.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RB_Enfermeria_CheckedChanged);
            // 
            // RB_Entidad
            // 
            this.RB_Entidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.RB_Entidad.Location = new System.Drawing.Point(8, 59);
            this.RB_Entidad.Name = "RB_Entidad";
            this.RB_Entidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RB_Entidad.Size = new System.Drawing.Size(248, 18);
            this.RB_Entidad.TabIndex = 2;
            this.RB_Entidad.Text = "Entidades externas y servicios no asistenciales";
            this.RB_Entidad.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RB_Entidad_CheckedChanged);
            // 
            // CBB_Responsable
            // 
            this.CBB_Responsable.ColumnWidths = "";
            this.CBB_Responsable.Cursor = System.Windows.Forms.Cursors.Default;
            this.CBB_Responsable.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CBB_Responsable.Location = new System.Drawing.Point(286, 94);
            this.CBB_Responsable.Name = "CBB_Responsable";
            this.CBB_Responsable.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CBB_Responsable.Size = new System.Drawing.Size(342, 20);
            this.CBB_Responsable.TabIndex = 6;
            this.CBB_Responsable.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CBB_Responsable_KeyPress);
            this.CBB_Responsable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CBB_Responsable_KeyDown);
            // 
            // LB_Unidad
            // 
            this.LB_Unidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_Unidad.Location = new System.Drawing.Point(179, 40);
            this.LB_Unidad.Name = "LB_Unidad";
            this.LB_Unidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_Unidad.Size = new System.Drawing.Size(101, 18);
            this.LB_Unidad.TabIndex = 22;
            this.LB_Unidad.Text = "Unidad de destino:";
            // 
            // LB_Responsable
            // 
            this.LB_Responsable.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_Responsable.Location = new System.Drawing.Point(179, 98);
            this.LB_Responsable.Name = "LB_Responsable";
            this.LB_Responsable.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_Responsable.Size = new System.Drawing.Size(94, 18);
            this.LB_Responsable.TabIndex = 21;
            this.LB_Responsable.Text = "Responsable pet.:";
            // 
            // LB_Peticionario
            // 
            this.LB_Peticionario.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_Peticionario.Location = new System.Drawing.Point(179, 68);
            this.LB_Peticionario.Name = "LB_Peticionario";
            this.LB_Peticionario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_Peticionario.Size = new System.Drawing.Size(68, 18);
            this.LB_Peticionario.TabIndex = 20;
            this.LB_Peticionario.Text = "Peticionario:";
            // 
            // SDC_peticion
            // 
            this.SDC_peticion.CustomFormat = "dd/MM/yyyy";
            this.SDC_peticion.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SDC_peticion.Location = new System.Drawing.Point(162, 337);
            this.SDC_peticion.Name = "SDC_peticion";
            this.SDC_peticion.Size = new System.Drawing.Size(113, 20);
            this.SDC_peticion.TabIndex = 11;
            this.SDC_peticion.TabStop = false;
            this.SDC_peticion.Text = "01/06/2016";
            this.SDC_peticion.Value = new System.DateTime(2016, 6, 1, 8, 29, 59, 583);
            // 
            // SDC_entrega
            // 
            this.SDC_entrega.CustomFormat = "dd/MM/yyyy";
            this.SDC_entrega.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SDC_entrega.Location = new System.Drawing.Point(162, 365);
            this.SDC_entrega.Name = "SDC_entrega";
            this.SDC_entrega.Size = new System.Drawing.Size(113, 20);
            this.SDC_entrega.TabIndex = 12;
            this.SDC_entrega.TabStop = false;
            this.SDC_entrega.Text = "01/06/2016";
            this.SDC_entrega.Value = new System.DateTime(2016, 6, 1, 8, 29, 59, 614);
            this.SDC_entrega.Leave += new System.EventHandler(this.SDC_entrega_Leave);
            // 
            // SDC_devolucion
            // 
            this.SDC_devolucion.CustomFormat = "dd/MM/yyyy";
            this.SDC_devolucion.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SDC_devolucion.Location = new System.Drawing.Point(162, 393);
            this.SDC_devolucion.Name = "SDC_devolucion";
            this.SDC_devolucion.Size = new System.Drawing.Size(113, 20);
            this.SDC_devolucion.TabIndex = 13;
            this.SDC_devolucion.TabStop = false;
            this.SDC_devolucion.Text = "01/06/2016";
            this.SDC_devolucion.Value = new System.DateTime(2016, 6, 1, 8, 29, 59, 630);
            // 
            // LbFechaPeticion
            // 
            this.LbFechaPeticion.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFechaPeticion.Location = new System.Drawing.Point(14, 341);
            this.LbFechaPeticion.Name = "LbFechaPeticion";
            this.LbFechaPeticion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFechaPeticion.Size = new System.Drawing.Size(96, 18);
            this.LbFechaPeticion.TabIndex = 43;
            this.LbFechaPeticion.Text = "Fecha de petici�n:";
            // 
            // LBPrevEntrega
            // 
            this.LBPrevEntrega.Cursor = System.Windows.Forms.Cursors.Default;
            this.LBPrevEntrega.Location = new System.Drawing.Point(14, 369);
            this.LBPrevEntrega.Name = "LBPrevEntrega";
            this.LBPrevEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LBPrevEntrega.Size = new System.Drawing.Size(137, 18);
            this.LBPrevEntrega.TabIndex = 42;
            this.LBPrevEntrega.Text = "Fecha prevista de entrega:";
            // 
            // LbFechaPrevDevol
            // 
            this.LbFechaPrevDevol.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFechaPrevDevol.Location = new System.Drawing.Point(14, 397);
            this.LbFechaPrevDevol.Name = "LbFechaPrevDevol";
            this.LbFechaPrevDevol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFechaPrevDevol.Size = new System.Drawing.Size(138, 18);
            this.LbFechaPrevDevol.TabIndex = 41;
            this.LbFechaPrevDevol.Text = "Fecha prevista devoluci�n:";
            // 
            // LB_Motivo
            // 
            this.LB_Motivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.LB_Motivo.Location = new System.Drawing.Point(14, 311);
            this.LB_Motivo.Name = "LB_Motivo";
            this.LB_Motivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LB_Motivo.Size = new System.Drawing.Size(44, 18);
            this.LB_Motivo.TabIndex = 40;
            this.LB_Motivo.Text = "Motivo:";
            // 
            // Label9
            // 
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Location = new System.Drawing.Point(306, 307);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(81, 18);
            this.Label9.TabIndex = 39;
            this.Label9.Text = "Observaciones:";
            // 
            // HTC120F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 449);
            this.Controls.Add(this.PIC_Peticion);
            this.Controls.Add(this.PIC_servicios);
            this.Controls.Add(this.PIC_Placas);
            this.Controls.Add(this.PIC_Historia);
            this.Controls.Add(this.PIC_Ambas);
            this.Controls.Add(this.PIC_Entidad);
            this.Controls.Add(this.PIC_Consulta);
            this.Controls.Add(this.PIC_Enfermeria);
            this.Controls.Add(this.CB_cancelar);
            this.Controls.Add(this.CB_Aceptar);
            this.Controls.Add(this.FRM_paciente);
            this.Controls.Add(this.FRM_contenido);
            this.Controls.Add(this.CBB_motivo);
            this.Controls.Add(this.TB_Observaciones);
            this.Controls.Add(this.FRM_tipo);
            this.Controls.Add(this.SDC_peticion);
            this.Controls.Add(this.SDC_entrega);
            this.Controls.Add(this.SDC_devolucion);
            this.Controls.Add(this.LbFechaPeticion);
            this.Controls.Add(this.LBPrevEntrega);
            this.Controls.Add(this.LbFechaPrevDevol);
            this.Controls.Add(this.LB_Motivo);
            this.Controls.Add(this.Label9);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HTC120F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Petici�n de Historia Cl�nica - HTC120F1";
            this.Closed += new System.EventHandler(this.HTC120F1_Closed);
            this.Load += new System.EventHandler(this.HTC120F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Peticion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Placas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Historia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Ambas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Entidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Consulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Enfermeria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CB_cancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CB_Aceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_paciente)).EndInit();
            this.FRM_paciente.ResumeLayout(false);
            this.FRM_paciente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_Actual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_cumplimentacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_finan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_hist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_asegur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Servicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Contenido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_contenido)).EndInit();
            this.FRM_contenido.ResumeLayout(false);
            this.FRM_contenido.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_contenido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Placas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Historia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Ambas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBB_motivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TB_Observaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRM_tipo)).EndInit();
            this.FRM_tipo.ResumeLayout(false);
            this.FRM_tipo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CBB_unidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBB_peticionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Rb_Servicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PIC_tipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Consulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Enfermeria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB_Entidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBB_Responsable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Unidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Responsable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Peticionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SDC_peticion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SDC_entrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SDC_devolucion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechaPeticion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LBPrevEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFechaPrevDevol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LB_Motivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}