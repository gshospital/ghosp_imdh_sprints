using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Microsoft.CSharp;
using ElementosCompartidos;
using System.Transactions;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace PeticionHistoriaClinica
{
	public static class BasHTC120F1
	{

		public static SqlConnection RefRc = null;
		public static DataSet Rs = null;
		public static DataSet Rs2 = null;
		public static string StComando = String.Empty;
		public static dynamic FrRefFormulario = null;
		public static int ICodSerArchivo = 0;
		public static string StPaciente = String.Empty;
		public static ClassHTC120F1 ClassRefClase = null; //ClassHTC120F1
		public static bool FechaCorrecta = false;
		public static string OrigenSolicitud = String.Empty;
		public static DialogResult res = (DialogResult) 0;
		public static string FPrestamo = String.Empty;
		public static string FechaPeticion = String.Empty;

        //DGMORENOG_TODO_5_5 PENDIENTE IMPLEMENTACION
        //public static Prestar.ClassHTC421F2 ClassPrestamo = null;
        public static dynamic ClassPrestamo = null;

        public static string EFinanciera = String.Empty;
		public static int VarSEGURSOC = 0;
		public static int VarPRIVADO = 0;
		public static string StAuto = String.Empty;

		// Constante que define si ha de mostrarse o no el mensaje del resguardo

		public static FixedLengthString v_ResguardoPeticion = new FixedLengthString(1);
		public static FixedLengthString v_ResguardoPrestamo = new FixedLengthString(1);

		static int ValorActivo = 0;
		static int ValorNoCumplimentada = 0;

		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public bool VfCabecera;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					return result;
			}
		}
		public static BasHTC120F1.CabCrystal VstrCrystal = BasHTC120F1.CabCrystal.CreateInstance();
		public static string PathString = String.Empty;

		public static Mensajes.ClassMensajes ClassMensaje = null;

		//Path donde va a estar el report
		public static string vStPathReport = String.Empty;
		//CUADRO DE DI�LOGO IMPRIMIR
		public static PrintDialog Cuadro_Diag_imprePrint = null;

		public static string CodMedico = String.Empty;
		public static int PrioridadCita = 0;

		//DIEGO 14/03/2007 - PARA CALCULAR LA FECHA DEL SERVIDOR
		static DataSet rsFechas = null;
		static string strSQLFecha = String.Empty;


		internal static void proDialogo_impre(dynamic Pcrystal)
		{
			Pcrystal.PrinterStopPage = 0;
			Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
            
            //DGMORENOG_TODO_X_5 - IMPLEMENTA CRISTALREPORT
            /*Cuadro_Diag_imprePrint.CancelError = true;            
            Cuadro_Diag_imprePrint.setPrinterDefault(true);*/

			Cuadro_Diag_imprePrint.ShowDialog();
			Pcrystal.CopiesToPrinter = (short) Cuadro_Diag_imprePrint.PrinterSettings.Copies;
		}
		internal static void buscar_EFinanciadora(object num_sociedad)
		{
			if (Convert.ToDouble(num_sociedad) == VarSEGURSOC)
			{
				StComando = "select VALFANU1 from SCONSGLO ";
				StComando = StComando + "where NNUMERI1=" + VarSEGURSOC.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter.Fill(Rs);
				EFinanciera = Strings.StrConv(Convert.ToString(Rs.Tables[0].Rows[0]["VALFANU1"]).Trim(), VbStrConv.ProperCase, 0);
			}
			else if (Convert.ToDouble(num_sociedad) == VarPRIVADO)
			{ 
				StComando = "select VALFANU1 from SCONSGLO ";
				StComando = StComando + "where NNUMERI1=" + VarPRIVADO.ToString();
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter_2.Fill(Rs);
				EFinanciera = Strings.StrConv(Convert.ToString(Rs.Tables[0].Rows[0]["VALFANU1"]).Trim(), VbStrConv.ProperCase, 0);
			}
			else
			{
				StComando = "select DSOCIEDA from DSOCIEDA ";
				StComando = StComando + "where GSOCIEDA=" + Convert.ToString(num_sociedad);
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter_3.Fill(Rs);
				EFinanciera = Strings.StrConv(Convert.ToString(Rs.Tables[0].Rows[0]["DSOCIEDA"]).Trim(), VbStrConv.ProperCase, 0);
			}
			Rs.Close();
		}
		internal static void ComprobarFechas(object VarFechaPeticion_optional, object VarFechaEntrega_optional, object VarFechaDevol_optional)
		{
			object VarFechaPeticion = (VarFechaPeticion_optional == Type.Missing) ? null : VarFechaPeticion_optional as object;
			string VarFechaEntrega = (VarFechaEntrega_optional == Type.Missing) ? String.Empty : VarFechaEntrega_optional as string;
			string VarFechaDevol = (VarFechaDevol_optional == Type.Missing) ? String.Empty : VarFechaDevol_optional as string;
			FechaCorrecta = true;
			if (VarFechaPeticion_optional != Type.Missing && VarFechaPeticion_optional.ToString().Length > 0)
			{
				if (!Convert.IsDBNull(VarFechaPeticion))
				{
					if (String.CompareOrdinal(Convert.ToDateTime(VarFechaPeticion).ToString("yyyy/MM/dd"), DateTime.Today.ToString("yyyy/MM/dd")) > 0)
					{
						//Call MsgBox("La fecha de petici�n debe ser <= que la fecha actual", vbCritical + vbOKOnly)
						short tempRefParam = 1020;
						string[] tempRefParam2 = new string[] { Convert.ToString(FrRefFormulario.LbFechaPeticion.Caption).Trim(), "<=", "fecha actual"};
						ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RefRc, tempRefParam2);
						FechaCorrecta = false;
						return;
					}
				}
			}

			if (OrigenSolicitud != "U" && OrigenSolicitud != "H")
			{
				if (VarFechaEntrega_optional != Type.Missing && VarFechaEntrega_optional.ToString().Length > 0)
				{
					if (!Convert.IsDBNull(VarFechaEntrega))
					{
						if (String.CompareOrdinal(DateTime.Parse(VarFechaEntrega).ToString("yyyy/MM/dd"), DateTime.Today.ToString("yyyy/MM/dd")) < 0)
						{
							//Call MsgBox("La fecha prevista de entrega debe ser >= que la fecha actual", vbCritical + vbOKOnly)
							short tempRefParam3 = 1020;
                            string[] tempRefParam4 = new string[] { Convert.ToString(FrRefFormulario.LBPrevEntrega.Caption).Trim(), ">=", "fecha actual"};
							ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RefRc, tempRefParam4);
							FechaCorrecta = false;
							return;
						}
					}
				}
				if (VarFechaDevol_optional != Type.Missing && VarFechaDevol_optional.ToString().Length > 0)
				{
					if (!Convert.IsDBNull(VarFechaDevol))
					{
						if (String.CompareOrdinal(DateTime.Parse(VarFechaDevol).ToString("yyyy/MM/dd"), DateTime.Today.ToString("yyyy/MM/dd")) < 0)
						{
							//Call MsgBox("La fecha prevista de devolucion debe ser >= que la fecha actual", vbCritical + vbOKOnly)
							short tempRefParam5 = 1020;
                            string[] tempRefParam6 = new string[] { Convert.ToString(FrRefFormulario.LbFechaPrevDevol.Caption).Trim(), ">=", "fecha actual"};
							ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, RefRc, tempRefParam6);
							FechaCorrecta = false;
							return;
						}
					}
				}
			}
		}

		internal static void ComprobarFechas(object VarFechaPeticion_optional, object VarFechaEntrega_optional)
		{
			ComprobarFechas(VarFechaPeticion_optional, VarFechaEntrega_optional, Type.Missing);
		}

		internal static void ComprobarFechas(object VarFechaPeticion_optional)
		{
			ComprobarFechas(VarFechaPeticion_optional, Type.Missing, Type.Missing);
		}

		internal static void ComprobarFechas()
		{
			ComprobarFechas(Type.Missing, Type.Missing, Type.Missing);
		}
		internal static void ActualizarFechaUltiMov(string StIPaciente)
		{
			object tempRefParam = DateTime.Today;
			StComando = "update dpacient set FULTMOVI = " + Serrores.FormatFechaHMS(tempRefParam) + " where GIDENPAC=";
			StComando = StComando + "'" + StIPaciente.Trim() + "'";
			SqlCommand tempCommand = new SqlCommand(StComando, RefRc);
			tempCommand.ExecuteNonQuery();
		}

		internal static void CargarResponsable(string VarTipoU, object VarSerpeti_optional, object VarCodMedico)
		{
			object VarSerpeti = (VarSerpeti_optional == Type.Missing) ? null : VarSerpeti_optional as object;
			//C SERVICIOS DE CONSULTAS
			//S SERVICIOS MEDICOS
			//U ENFERMERIA

			int iIndex = -1;

			if (VarTipoU.Trim() != "E")
			{
				switch(VarTipoU.Trim())
				{
					case "C" :  //, "S" 
						if (VarSerpeti_optional != Type.Missing && VarSerpeti_optional.ToString().Length > 0)
						{
							HTC120F1.DefInstance.CBB_Responsable.Items.Clear();
							StComando = "select distinct(C.DNOMPERS),C.DAP1PERS,C.DAP2PERS,C.GPERSONA ";
							StComando = StComando + " from DPERSERV B,DPERSONA C ,dservici d ";
							StComando = StComando + " where B.GPERSONA=C.GPERSONA and B.GSERVICI=";
							StComando = StComando + ((Convert.ToString(VarSerpeti) == "") ? "''" : Convert.ToString(VarSerpeti));
							StComando = StComando + " and  C.FBORRADO is null AND  b.gservici=d.gservici and d.iconsult='S' ";
							StComando = StComando + " order by DAP1PERS,DAP2PERS,DNOMPERS ";
							SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
							Rs = new DataSet();
							tempAdapter.Fill(Rs);
                            

                            foreach (DataRow iteration_row in Rs.Tables[0].Rows)
							{
								//                    HTC120F1.CBB_Responsable.AddItem Trim(Rs(1)) & " " & Trim(Rs(2)) & ", " & Trim(Rs(0))
								//                    HTC120F1.CBB_Responsable.ItemData(HTC120F1.CBB_Responsable.NewIndex) = Rs(3)

								HTC120F1.DefInstance.CBB_Responsable.AddItem((object)(Convert.ToString(iteration_row[1]).Trim() + " " + Convert.ToString(iteration_row[2]).Trim() + ", " + Convert.ToString(iteration_row[0]).Trim()), null);
								HTC120F1.DefInstance.CBB_Responsable.SetItemData(HTC120F1.DefInstance.CBB_Responsable.Items.Count - 1, Convert.ToInt32(iteration_row[3]));
								if (CodMedico != "" && CodMedico == Conversion.Str(iteration_row[3]).Trim())
								{
									iIndex = HTC120F1.DefInstance.CBB_Responsable.Items.Count - 1;
								}

							}
							Rs.Close();
						} 
						break;
					case "S" : 
						if (VarSerpeti_optional != Type.Missing && VarSerpeti_optional.ToString().Length > 0)
						{
							HTC120F1.DefInstance.CBB_Responsable.Items.Clear();
							StComando = "select distinct(C.DNOMPERS),C.DAP1PERS,C.DAP2PERS,C.GPERSONA ";
							StComando = StComando + " from DPERSERV B,DPERSONA C ,dservici d ";
							StComando = StComando + " where B.GPERSONA=C.GPERSONA and B.GSERVICI=";
							StComando = StComando + Convert.ToString(VarSerpeti);
							StComando = StComando + " and  C.FBORRADO is null AND  b.gservici=d.gservici and (d.iconsult='S' or d.iurgenci='S' OR d.ihospita='S' OR d.icentral='S' OR d.iquirofa='S') ";
							StComando = StComando + " order by DAP1PERS,DAP2PERS,DNOMPERS ";
							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, RefRc);
							Rs = new DataSet();
							tempAdapter_2.Fill(Rs);
							foreach (DataRow iteration_row_2 in Rs.Tables[0].Rows)
							{
								HTC120F1.DefInstance.CBB_Responsable.AddItem((object)(Convert.ToString(iteration_row_2[1]).Trim() + " " + Convert.ToString(iteration_row_2[2]).Trim() + ", " + Convert.ToString(iteration_row_2[0]).Trim()),null);
								HTC120F1.DefInstance.CBB_Responsable.SetItemData(HTC120F1.DefInstance.CBB_Responsable.GetNewIndex(), Convert.ToInt32(iteration_row_2[3]));
								//'''                    HTC120F1.CBB_Responsable.ItemData(HTC120F1.CBB_Responsable.ListCount - 1) = Rs(3)
								//'''                    If CodMedico <> "" And CodMedico = Trim(Str(Rs(3))) Then
								//'''                       iIndex = HTC120F1.CBB_Responsable.ListCount - 1
								//'''                    End If
							}
							Rs.Close();
						} 
						break;
					case "U" : 
						HTC120F1.DefInstance.CBB_Responsable.Items.Clear(); 
						StComando = "select DNOMPERS,DAP1PERS,DAP2PERS,GPERSONA "; 
						StComando = StComando + "from DPERSONA "; 
						StComando = StComando + "where FBORRADO is null AND (imedico='S' OR  ienfermeria ='S') "; 
						StComando = StComando + "order by DAP1PERS,DAP2PERS,DNOMPERS "; 
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StComando, RefRc); 
						Rs = new DataSet(); 
						tempAdapter_3.Fill(Rs); 
						foreach (DataRow iteration_row_3 in Rs.Tables[0].Rows)
						{
							HTC120F1.DefInstance.CBB_Responsable.AddItem((object)(Convert.ToString(iteration_row_3[1]).Trim() + " " + Convert.ToString(iteration_row_3[2]).Trim() + ", " + Convert.ToString(iteration_row_3[0]).Trim()),null);
							HTC120F1.DefInstance.CBB_Responsable.SetItemData(HTC120F1.DefInstance.CBB_Responsable.GetNewIndex(), Convert.ToInt32(iteration_row_3[3]));
							//'''                HTC120F1.CBB_Responsable.ItemData(HTC120F1.CBB_Responsable.ListCount - 1) = Rs(3)
							//'''                If CodMedico <> "" And CodMedico = Trim(Str(Rs(3))) Then
							//'''                   iIndex = HTC120F1.CBB_Responsable.ListCount - 1
							//'''                End If
						} 
						Rs.Close(); 
						break;
				}
				if (iIndex != -1)
				{
					HTC120F1.DefInstance.CBB_Responsable.SelectedIndex = iIndex;
				}
			}
		}

		internal static void CargarResponsable(string VarTipoU, object VarSerpeti_optional)
		{
			CargarResponsable(VarTipoU, VarSerpeti_optional, null);
		}

		internal static void CargarResponsable(string VarTipoU)
		{
			CargarResponsable(VarTipoU, Type.Missing, null);
		}
		internal static void AltaHistoriaConsulta(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, string VarNHistoria, ref string VarCodServicioPeticionario, ref string VarSalaConsulta, ref string VarFechaPeticion, string VarMotiPeticion, object VarCodMedico, string VarIAutomatica, string VarFechaEntrega, ref string VarFechaDevol, string VarObser)
		{
			int IError = 0;
			string Msj = String.Empty;
			int INumRegistros = 0;
			//error = False

			//Compruebo que para iguales valores en los campos: Servicio de archivo,
			//N� de historia,Contenido dossier, servicio peticionario y unidad de
			//destino, no exista ya un registro en HMOVCONS
			if (VarIAutomatica != "S")
			{

				StComando = "Select * from HMOVCONS where GHISTORIA=";
				StComando = StComando + VarNHistoria;
				StComando = StComando + " and GSERPROPIET=";
				StComando = StComando + ICodServicioArchivo.ToString();

				StComando = StComando + " and GSERSOLI = ";
				StComando = StComando + VarCodServicioPeticionario;
				StComando = StComando + " and GSALACON=";
				StComando = StComando + "'" + VarSalaConsulta.Trim() + "'";
				StComando = StComando + " and IESTMOVIH='P'";
				StComando = StComando + " and GMEDSOLI= ";
				StComando = StComando + Convert.ToString(VarCodMedico);
				//Linea a�adida para permitir dos peticiones iguales en diferentes dias:
				System.DateTime TempDate2 = DateTime.FromOADate(0);
				System.DateTime TempDate = DateTime.FromOADate(0);
				StComando = StComando + " and fpreventreg >= '" + ((DateTime.TryParse(VarFechaEntrega, out TempDate)) ? TempDate.ToString("yyyy/MM/dd") : VarFechaEntrega) + " 00:00:00" + 
				            "' AND fpreventreg <= '" + ((DateTime.TryParse(VarFechaEntrega, out TempDate2)) ? TempDate2.ToString("yyyy/MM/dd") : VarFechaEntrega) + " 23:59:59' ";
				if (StContenido.Trim() != "A")
				{
					StComando = StComando + " and ICONTENIDO=";
					StComando = StComando + "'" + StContenido.Trim() + "'";
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter.Fill(Rs);
				if (Rs.Tables[0].Rows.Count != 0)
				{
					IError = 4;
					//If Trim(OrigenSolicitud) <> "S" Then
					//Call MsgBox("Ya existe una solicitud de historia pendiente para esta sala de consulta")
					short tempRefParam = 2040;
					string[] tempRefParam2 = new string[] { "consulta"};
					ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RefRc, tempRefParam2);
					//End If
					return;
				}

			}

			//Si no existe esta solicitud de historia como pendiente:

			if (StContenido.Trim() == "A")
			{ //Si el contenido es ambas
				StContenido = "H"; //habra que grabar 2 registros, H y P
				INumRegistros = 2;
			}
			else
			{
				INumRegistros = 1;
			}

			try
			{
				//RefRc.BeginTrans

				for (int i = 1; i <= INumRegistros; i++)
				{

					//'    StComando = "select * from HMOVCONS where 2=1"    'Para que no me traiga ning�n registro
					//'    Set Rs = RefRc.OpenResultset(StComando, rdOpenDynamic, rdConcurValues)
					//'    Rs.AddNew
					//'    Rs("GHISTORIA") = VarNHistoria
					//'    Rs("GSERPROPIET") = ICodServicioArchivo
					//'    Rs("ICONTENIDO") = Trim(UCase(StContenido))
					//'    Rs("GSERSOLI") = VarCodServicioPeticionario
					//'    Rs("GSALACON") = UCase(Trim(VarSalaConsulta))

					//diego - 14/03/2007 - obtengo la fecha del servidor
					strSQLFecha = "SELECT CONVERT(CHAR(8), getdate(), 114) AS HORA";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(strSQLFecha, RefRc);
					rsFechas = new DataSet();
					tempAdapter_2.Fill(rsFechas);

					if (INumRegistros == 1)
					{
						//        incremento = Time
						//        While DateAdd("s", 1, incremento) > Time
						//        Wend
						// DIEGO 14/03/2007 - VarFechaPeticion = Format(CDate(Trim(VarFechaPeticion) & " " & Time), "dd/mm/yyyy hh:nn:ss")
						VarFechaPeticion = DateTime.Parse(VarFechaPeticion.Trim() + " " + Convert.ToString(rsFechas.Tables[0].Rows[0]["HORA"])).ToString("dd/MM/yyyy HH:mm:ss");
						VarFechaPeticion = DateTimeHelper.ToString(DateTime.Parse(VarFechaPeticion).AddSeconds(PrioridadCita));
						//sumamos el n� de orden de la cita a los segundos. Si pedimos cita multiple con distinto n� de orden y el mismo
						//servicio, a la hora de poner los segundos ponia los mismos en todas las citas y fallaba la PK
					}
					//'    Rs("FPETICIO") = CDate(VarFechaPeticion)
					//'    Rs("GMOTPETH") = UCase(Trim(VarMotiPeticion))
					//'    Rs("GMEDSOLI") = VarCodMedico

					//''    'peticiones manuales poner a 00:00 en vez de a 23:59; de FJD ESTE COMENTARIO MOSQUEA CO�O!!!!
					//''    If Not IsMissing(VarFechaEntrega) And Not IsNull(VarFechaEntrega) Then
					//''        If VarIAutomatica <> "N" Then
					//''            Rs("FPREVENTREG") = CDate(VarFechaEntrega)
					//''        Else
					//''            Rs("FPREVENTREG") = Format(CDate(VarFechaEntrega), "dd/mm/yyyy") & " " & "23:59"
					//''        End If
					//''    Else
					//''        Rs("FPREVENTREG") = Null
					//'''''    End If
					//''    Rs("FPRESTAMOH") = Null
					//''    If Not IsMissing(VarFechaDevol) And Not IsNull(VarFechaDevol) Then
					//''        Rs("FPREVDEVOL") = CDate(VarFechaDevol)
					//''    Else
					//''        Rs("FPREVDEVOL") = Null
					//''    End If
					//''    Rs("FDEVOLUCH") = Null
					//''    Rs("IESTMOVIH") = "P"
					//''    If Not IsMissing(VarObser) And Not IsNull(VarObser) Then
					//''        Rs("DOBSMOVHIS") = UCase(Trim(VarObser))
					//''    Else
					//''        Rs("DOBSMOVHIS") = Null
					//''    End If
					//''    Rs.Update
					//''
					//''    Rs.Close
					StComando = "insert into hmovcons (GHISTORIA,GSERPROPIET,ICONTENIDO,GSERSOLI,GSALACON,FPETICIO,GMOTPETH,";
					StComando = StComando + "GMEDSOLI,FPREVENTREG,FPREVDEVOL,FPRESTAMOH,FDEVOLUCH,IESTMOVIH,DOBSMOVHIS) values (" + VarNHistoria + ",";
					StComando = StComando + ICodServicioArchivo.ToString() + ",'" + StContenido.Trim().ToUpper() + "'," + VarCodServicioPeticionario + ",'" + VarSalaConsulta.Trim().ToUpper() + "',";
					object tempRefParam3 = DateTime.Parse(VarFechaPeticion);
					StComando = StComando + Serrores.FormatFechaHMS(tempRefParam3) + ",'" + VarMotiPeticion.Trim().ToUpper() + "'," + Convert.ToString(VarCodMedico) + ",";

                    //peticiones manuales poner a 00:00 en vez de a 23:59; de FJD ESTE COMENTARIO MOSQUEA CO�O!!!!
                    //If Not IsMissing(VarFechaEntrega) And Not IsNull(VarFechaEntrega) Then
                    if (((Object)VarFechaEntrega != Type.Missing && VarFechaEntrega.Length > 0)
                     && !Convert.IsDBNull(VarFechaEntrega))
					{
						if (VarIAutomatica != "N")
						{
							object tempRefParam4 = DateTime.Parse(VarFechaEntrega);
							StComando = StComando + Serrores.FormatFechaHMS(tempRefParam4) + ",";
						}
						else
						{
							object tempRefParam5 = DateTime.Parse(VarFechaEntrega.Trim() + " " + "23:59");
							StComando = StComando + Serrores.FormatFechaHMS(tempRefParam5) + ", ";
						}
					}
					else
					{
						StComando = StComando + " Null, ";
					}

                    // Completamos los campos FPREVDEVOL, FPRESTAMOH, FDEVOLUCH

                    //If Not IsMissing(VarFechaDevol) And Not IsNull(VarFechaDevol) Then
                    if (((Object)VarFechaDevol != Type.Missing && VarFechaDevol.Length > 0)
                        && !Convert.IsDBNull(VarFechaDevol))
					{
						object tempRefParam6 = DateTime.Parse(VarFechaDevol);
						StComando = StComando + Serrores.FormatFechaHMS(tempRefParam6) + ", Null, Null,";
					}
					else
					{
						StComando = StComando + "Null, Null, Null,";
					}

					// Completamos el campo IESTMOVIH

					StComando = StComando + "'P',";

                    // Completamos el campo DOBSMOVIH

                    //If Not IsMissing(VarObser) And Not IsNull(VarObser) Then
                    if (((Object)VarObser != Type.Missing && VarObser.Length > 0) && !Convert.IsDBNull(VarObser))
					{
						if (VarObser.Trim() == "")
						{
							StComando = StComando + "Null)";
						}
						else
						{
							StComando = StComando + "'" + VarObser.Trim().ToUpper() + "')";
						}
					}
					else
					{
						StComando = StComando + "Null)";
					}

					//    StComando = StComando & "'P', Null)"
					SqlCommand tempCommand = new SqlCommand(StComando, RefRc);
					tempCommand.ExecuteNonQuery();
					rsFechas.Close();

					//''Si existe env�o de datos de lista de espera al servicio web de CatSalut, no modificamos aqu� los datos del paciente.
					//''Como son dos transacciones distintas, en esta se bloquea la tabla de pacientes y en la otra, al insertar en AEPISLEQ, se produce el bloqueo.
					//''Por eso, si est� activa la constante, se ralizar� la actualizaci�n de la fecha de �ltimo movimiento en el servicio web.
					strSQLFecha = "SELECT count(*) FROM SCONSGLO WHERE GCONSGLO='IENVICAT' and valfanu1='S'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(strSQLFecha, RefRc);
					rsFechas = new DataSet();
					tempAdapter_3.Fill(rsFechas);
					if (Convert.ToDouble(rsFechas.Tables[0].Rows[0][0]) != 1)
					{
						ActualizarFechaUltiMov(StIPaciente);
					}
					rsFechas.Close();

					if (INumRegistros != 1)
					{
						StContenido = "P";
					}
				}
				//RefRc.CommitTrans
				if (VarIAutomatica.Trim() == "N" && OrigenSolicitud != "U" && v_ResguardoPeticion.Value == "S")
				{
					//DAR POSIBILIDAD DE EMISION DEL RESGUARDO DE SOLICITUDES H4580
					//res = MsgBox("�Desea un resguardo de su solicitud?", vbQuestion + vbYesNo)
					short tempRefParam7 = 1820;
                    string[] tempRefParam8 = new string[]{"un resguardo de su solicitud"};
					res = (DialogResult) Convert.ToInt32(ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, RefRc, tempRefParam8));
					if (res == System.Windows.Forms.DialogResult.Yes)
					{
						InformeSolicitud();
					}
				}
				//Solo se preguntar� si se quiere hacer el pr�stamo si viene de Archivo
				//y la petici�n no es autom�tica
				if (VarIAutomatica.Trim() == "N" && OrigenSolicitud.Trim() == "A")
				{
					if (INumRegistros != 1)
					{
						StContenido = "A";
					}
					if (EstaPrestada(Conversion.Val(VarNHistoria), StContenido, ICodServicioArchivo))
					{
						//Call MsgBox("La carpeta solicitada ya est� prestada "), vbInformation + vbOKOnly)
						short tempRefParam9 = 1980;
                        string[] tempRefParam10 = new string[]{"solicitada", "prestar", "ya est� prestada"};
						ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam9, RefRc, tempRefParam10);
					}
					else if (!ComprobarDisponible(Conversion.Val(VarNHistoria), StContenido, ICodServicioArchivo))
					{ 
						//Call MsgBox("La carpeta solicitada no est� disponible para ser prestada", vbInformation + vbOKOnly)
						short tempRefParam11 = 1980;
                        string[] tempRefParam12 = new string[]{"solicitada", "prestar", "no est� disponible"};
						ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam11, RefRc, tempRefParam12);
					}
					else
					{
						//If Not EstaPrestada(Val(VarNHistoria), StContenido, ICodServicioArchivo) And ComprobarDisponible(Val(VarNHistoria), StContenido, ICodServicioArchivo) = True Then
						//res = MsgBox("�Desea hacer el pr�stamo en este momento?", vbQuestion + vbYesNo)
						short tempRefParam13 = 3300;
                        string[] tempRefParam14 = new string[]{};
						res = (DialogResult) Convert.ToInt32(ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam13, RefRc, tempRefParam14));
						//PREGUNTAR SI SE QUIERE HACER EL PRESTAMO EN ESTE MOMENTO H4200

						if (res == System.Windows.Forms.DialogResult.Yes)
						{
							if (ComprobarActivoONoCumplimentada(Conversion.Str(VarNHistoria)))
							{
                                //If Not IsMissing(VarObser) And Not IsNull(VarObser) Then
                                if (((Object)VarObser != Type.Missing && VarObser.Length > 0)
                                    && !Convert.IsDBNull(VarObser))
								{
									double tempRefParam15 = Conversion.Val(VarNHistoria);
									string tempRefParam16 = VarFechaPeticion.Trim();
									object tempRefParam17 = VarCodServicioPeticionario;
									string tempRefParam18 = VarObser.ToUpper();
                                    object tempRefParam19 = VarFechaDevol;

                                    LlamarPrestar("C", ref tempRefParam15, ref StContenido, ref tempRefParam16, ref ICodServicioArchivo, ref tempRefParam19, ref tempRefParam17, ref VarSalaConsulta, ref tempRefParam18);
									VarCodServicioPeticionario = Convert.ToString(tempRefParam17);
								}
								else
								{
									double tempRefParam19 = Conversion.Val(VarNHistoria);
									string tempRefParam20 = VarFechaPeticion.Trim();
									object tempRefParam21 = VarCodServicioPeticionario;
                                    object tempRefParam22 = VarFechaDevol;

                                    LlamarPrestar("C", ref tempRefParam19, ref StContenido, ref tempRefParam20, ref ICodServicioArchivo, ref tempRefParam22, ref tempRefParam21, ref VarSalaConsulta);
									VarCodServicioPeticionario = Convert.ToString(tempRefParam21);
								}
							}
							return;
						}
					}
				}
				ClassMensaje = null;
				if (ClassRefClase.FormularioExiste)
				{
					HTC120F1.DefInstance.Close();
				}
				FrRefFormulario.RecogerPeticion(0, "N");
			}
			catch (Exception e)
			{
				//Dim er As rdoError
				if (Information.Err().Number != 0)
				{
					Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
					RadMessageBox.Show(Msj, "Error");
				}
				//    RefRc.RollbackTrans
				//For Each er In rdoErrors
				//    'GestionErrorBaseDatos(rdoErrors.number)
				//    Call MsgBox(er.Number & "-" & er.Description, vbCritical + vbOKOnly)
				//Next er
				//rdoErrors.Clear
				//Error = True
				//    Call MsgBox("Si el error es de clave primaria " _
				//'    & "duplicada se controlar� con un mensaje: La carpeta esta " _
				//'    & "prestada al mismo destino solicitante")

				//Si no se llega a mostrar el formulario y se produce un error
				//mando el n� de error 5 que se da cuando no se puede efectuar
				//el alta
				if (StAuto == "S")
				{
					ClassMensaje = null;
					FrRefFormulario.RecogerPeticion(5, "N");
				}

				return;
			}
		}

		internal static void AltaHistoriaConsulta(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, string VarNHistoria, ref string VarCodServicioPeticionario, ref string VarSalaConsulta, ref string VarFechaPeticion, string VarMotiPeticion, object VarCodMedico, string VarIAutomatica, string VarFechaEntrega, ref string VarFechaDevol)
		{
			AltaHistoriaConsulta(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarCodServicioPeticionario, ref VarSalaConsulta, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarIAutomatica, VarFechaEntrega, ref VarFechaDevol, String.Empty);
		}

		internal static void AltaHistoriaConsulta(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, string VarNHistoria, ref string VarCodServicioPeticionario, ref string VarSalaConsulta, ref string VarFechaPeticion, string VarMotiPeticion, object VarCodMedico, string VarIAutomatica, string VarFechaEntrega)
		{
			string tempRefParam3 = String.Empty;
			AltaHistoriaConsulta(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarCodServicioPeticionario, ref VarSalaConsulta, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarIAutomatica, VarFechaEntrega, ref tempRefParam3, String.Empty);
		}

		internal static void AltaHistoriaConsulta(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, string VarNHistoria, ref string VarCodServicioPeticionario, ref string VarSalaConsulta, ref string VarFechaPeticion, string VarMotiPeticion, object VarCodMedico, string VarIAutomatica)
		{
			string tempRefParam4 = String.Empty;
			AltaHistoriaConsulta(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarCodServicioPeticionario, ref VarSalaConsulta, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarIAutomatica, String.Empty, ref tempRefParam4, String.Empty);
		}
		private static bool ComprobarDisponible(double LHistoria, string StContenido, int IServicio)
		{
			bool result = false;
			StComando = "Select IESTHISTORIA from HDOSSIER where GHISTORIA=";
			StComando = StComando + LHistoria.ToString();
			StComando = StComando + " and GSERPROPIET=" + IServicio.ToString();
			bool tempBool = false;
			string auxVar = (StContenido == "A").ToString().Trim();
			if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
			{
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
				Rs2 = new DataSet();
				tempAdapter.Fill(Rs2);
				for (int i = 1; i <= 2; i++)
				{
					if (Convert.ToString(Rs2.Tables[0].Rows[0]["IESTHISTORIA"]).Trim() != "D")
					{
						result = false;
						Rs2.Close();
						return result;
					}
				}
			}
			else
			{
				StComando = StComando + " and ICONTENIDO=";
				StComando = StComando + "'" + StContenido.Trim() + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, RefRc);
				Rs2 = new DataSet();
				tempAdapter_2.Fill(Rs2);
				if (Convert.ToString(Rs2.Tables[0].Rows[0]["IESTHISTORIA"]).Trim() != "D")
				{
					result = false;
					Rs2.Close();
					return result;
				}
			}
			result = true;
			Rs2.Close();
			return result;
		}


		internal static void AltaHistoriaEnfermeria(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref string VarUnidadEnfermeria, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional, ref object VarFechaDevol_optional, object VarCodMedico_optional, object VarObser_optional)
		{
			string VarFechaEntrega = (VarFechaEntrega_optional == Type.Missing) ? String.Empty : VarFechaEntrega_optional as string;
			object VarCodMedico = (VarCodMedico_optional == Type.Missing) ? null : VarCodMedico_optional as object;
			string VarFechaDevol = (VarFechaDevol_optional == Type.Missing) ? String.Empty : VarFechaDevol_optional as string;
			string VarObser = (VarObser_optional == Type.Missing) ? String.Empty : VarObser_optional as string;
			try
			{
				int IError = 0;
				string Msj = String.Empty;
				int INumRegistros = 0;
				//error = False

				//Compruebo que para iguales valores en los campos: Servicio de archivo,
				//N� de historia,Contenido dossier y unidad de destino, no exista ya un
				//registro en HMOVUENF
				StComando = "Select * from HMOVUENF where GSERPROPIET=";
				StComando = StComando + ICodServicioArchivo.ToString();
				StComando = StComando + " and GHISTORIA = ";
				StComando = StComando + Convert.ToString(VarNHistoria);
				StComando = StComando + " and GUNIDENF=";
				StComando = StComando + "'" + VarUnidadEnfermeria.Trim() + "'";
				StComando = StComando + " and IESTMOVIH='P'";
				if (VarFechaEntrega_optional != Type.Missing && VarFechaEntrega_optional.ToString().Length > 0)
				{
					//Linea a�adida para permitir dos peticiones iguales en diferentes dias:
					object tempRefParam = VarFechaEntrega + " 00:00:00";
					object tempRefParam2 = VarFechaEntrega + " 23:59:59";
					StComando = StComando + " and fpreventreg >= " + Serrores.FormatFechaHMS(tempRefParam) + 
					            " AND fpreventreg <= " + Serrores.FormatFechaHMS(tempRefParam2);
				}
				if (StContenido.Trim() != "A")
				{
					StComando = StComando + " and ICONTENIDO=";
					StComando = StComando + "'" + StContenido.Trim() + "'";
				}
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter.Fill(Rs);
				if (Rs.Tables[0].Rows.Count != 0)
				{
					IError = 4;
					//If Trim(OrigenSolicitud) <> "S" Then
					//Call MsgBox("Ya existe una solicitud de historia pendiente para esta unidad de enfermeria")
					short tempRefParam3 = 2040;
                    string[] tempRefParam4 = new string[]{"Unidad de Enfermer�a"};
					ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam3, RefRc, tempRefParam4);
					//End If
					return;
				}

				//Si no existe esta solicitud de historia como pendiente:

				if (StContenido.Trim() == "A")
				{ //Si el contenido es ambas
					StContenido = "H"; //habra que grabar 2 registros, H y P
					INumRegistros = 2;
				}
				else
				{
					INumRegistros = 1;
				}

				try
				{
					//RefRc.BeginTrans
					for (int i = 1; i <= INumRegistros; i++)
					{
						StComando = "select * from HMOVUENF where 2=1"; //Para que no me traiga ning�n registro
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, RefRc);
						Rs = new DataSet();
						tempAdapter_2.Fill(Rs);
						Rs.AddNew();
						Rs.Tables[0].Rows[0]["GHISTORIA"] = VarNHistoria;
						Rs.Tables[0].Rows[0]["ICONTENIDO"] = StContenido.Trim().ToUpper();
						Rs.Tables[0].Rows[0]["GUNIDENF"] = VarUnidadEnfermeria.Trim().ToUpper();

						//diego - 14/03/2007 - obtengo la fecha del servidor
						strSQLFecha = "SELECT CONVERT(CHAR(8), getdate(), 114) AS HORA";
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(strSQLFecha, RefRc);
						rsFechas = new DataSet();
						tempAdapter_3.Fill(rsFechas);

						if (INumRegistros == 1)
						{
							//        VarFechaPeticion = Format(CDate(Trim(VarFechaPeticion) & " " & Time), "dd/mm/yyyy hh:mm")
							//diego - 14/03/2007 - VarFechaPeticion = Format(CDate(Trim(VarFechaPeticion) & " " & Time), "dd/mm/yyyy hh:nn:ss")
							VarFechaPeticion = DateTime.Parse(Convert.ToString(VarFechaPeticion).Trim() + " " + Convert.ToString(rsFechas.Tables[0].Rows[0]["HORA"])).ToString("dd/MM/yyyy HH:mm:ss");
							VarFechaPeticion = Convert.ToDateTime(VarFechaPeticion).AddSeconds(PrioridadCita);

						}
						Rs.Tables[0].Rows[0]["FPETICIO"] = Convert.ToDateTime(VarFechaPeticion);
						Rs.Tables[0].Rows[0]["GMOTPETH"] = VarMotiPeticion.Trim().ToUpper();
						if (VarCodMedico_optional != Type.Missing && VarCodMedico_optional.ToString().Length > 0)
						{
							Rs.Tables[0].Rows[0]["GMEDSOLI"] = Conversion.Val(Convert.ToString(VarCodMedico));
						}
						else
						{
							Rs.Tables[0].Rows[0]["GMEDSOLI"] = DBNull.Value;
						}
						//AQUI
						if (VarFechaEntrega_optional != Type.Missing && !Convert.IsDBNull(VarFechaEntrega) && VarFechaEntrega_optional.ToString().Length > 0)
						{
							Rs.Tables[0].Rows[0]["FPREVENTREG"] = DateTime.Parse(VarFechaEntrega);
						}
						else
						{
							Rs.Tables[0].Rows[0]["FPREVENTREG"] = DBNull.Value;
						}
						Rs.Tables[0].Rows[0]["FPRESTAMOH"] = DBNull.Value;
						if (VarFechaDevol_optional != Type.Missing && !Convert.IsDBNull(VarFechaDevol) && VarFechaDevol_optional.ToString().Length > 0)
						{
							Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DateTime.Parse(VarFechaDevol);
						}
						else
						{
							Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DBNull.Value;
						}
						Rs.Tables[0].Rows[0]["FDEVOLUCH"] = DBNull.Value;
						Rs.Tables[0].Rows[0]["IESTMOVIH"] = "P";
						if (VarObser_optional != Type.Missing && !Convert.IsDBNull(VarObser) && VarObser_optional.ToString().Length > 0)
						{
							Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = VarObser.Trim().ToUpper();
						}
						else
						{
							Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = DBNull.Value;
						}
						Rs.Tables[0].Rows[0]["GSERPROPIET"] = ICodServicioArchivo;
						string tempQuery = Rs.Tables[0].TableName;						
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                        tempAdapter_2.Update(Rs, tempQuery);
						Rs.Close();
						rsFechas.Close();
						ActualizarFechaUltiMov(StIPaciente);
						if (INumRegistros != 1)
						{
							StContenido = "P";
						}
					}
					//RefRc.CommitTrans
					if (VarIAutomatica.Trim() == "N" && OrigenSolicitud != "U" && v_ResguardoPeticion.Value == "S")
					{
						//DAR POSIBILIDAD DE EMISION DEL RESGUARDO DE SOLICITUDES H4580
						//res = MsgBox("�Desea un resguardo de su solicitud?", vbQuestion + vbYesNo)
						short tempRefParam5 = 1820;
                        string[] tempRefParam6 = new string[]{"el resguardo de su solicitud"};
						res = (DialogResult) Convert.ToInt32(ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam5, RefRc, tempRefParam6));
						if (res == System.Windows.Forms.DialogResult.Yes)
						{
							InformeSolicitud();
						}
					}
					//Solo se preguntar� si se quiere hacer el pr�stamo si viene de Archivo
					//y la petici�n no es autom�tica
					if (VarIAutomatica.Trim() == "N" && OrigenSolicitud.Trim() == "A")
					{
						//PREGUNTAR SI SE QUIERE HACER EL PRESTAMO EN ESTE MOMENTO H4200
						if (INumRegistros != 1)
						{
							StContenido = "A";
						}
						if (EstaPrestada(Conversion.Val(Convert.ToString(VarNHistoria)), StContenido, ICodServicioArchivo))
						{
							//Call MsgBox("La carpeta solicitada ya est� prestada ", vbInformation + vbOKOnly)
							short tempRefParam7 = 1980;
                            string[] tempRefParam8 = new string[]{"solicitada", "prestar", "ya est� prestada"};
							ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam7, RefRc, tempRefParam8);
						}
						else if (!ComprobarDisponible(Conversion.Val(Convert.ToString(VarNHistoria)), StContenido, ICodServicioArchivo))
						{ 
							//Call MsgBox("La carpeta solicitada no est� disponible para ser prestada", vbInformation + vbOKOnly)
							short tempRefParam9 = 1980;
                            string[] tempRefParam10 = new string[]{"solicitada", "prestar", "no est� disponible"};
							ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam9, RefRc, tempRefParam10);
						}
						else
						{
							//If Not EstaPrestada(Val(VarNHistoria), StContenido, ICodServicioArchivo) And ComprobarDisponible(Val(VarNHistoria), StContenido, ICodServicioArchivo) = True Then
							//res = MsgBox("�Desea hacer el pr�stamo en este momento?", vbQuestion + vbYesNo)
							short tempRefParam11 = 3300;
                            string[] tempRefParam12 = new string[]{};
                            object tempRefParamDevol = VarFechaDevol;

                            res = (DialogResult) Convert.ToInt32(ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam11, RefRc, tempRefParam12));
							if (res == System.Windows.Forms.DialogResult.Yes)
							{
								if (ComprobarActivoONoCumplimentada(Conversion.Str(VarNHistoria)))
								{
									if (VarObser_optional != Type.Missing && !Convert.IsDBNull(VarObser) && VarObser_optional.ToString().Length > 0)
									{
										double tempRefParam13 = Conversion.Val(Convert.ToString(VarNHistoria));
										string tempRefParam14 = Convert.ToString(VarFechaPeticion).Trim();
										object tempRefParam15 = null;
										string tempRefParam16 = VarObser.ToUpper();
                                        
                                        LlamarPrestar("U", ref tempRefParam13, ref StContenido, ref tempRefParam14, ref ICodServicioArchivo, ref tempRefParamDevol, ref tempRefParam15, ref VarUnidadEnfermeria, ref tempRefParam16);
									}
									else
									{
										double tempRefParam17 = Conversion.Val(Convert.ToString(VarNHistoria));
										string tempRefParam18 = Convert.ToString(VarFechaPeticion).Trim();
										object tempRefParam19 = null;
										LlamarPrestar("U", ref tempRefParam17, ref StContenido, ref tempRefParam18, ref ICodServicioArchivo, ref tempRefParamDevol, ref tempRefParam19, ref VarUnidadEnfermeria);
									}
								}
								return;
							}
						}
					}
					ClassMensaje = null;
					if (ClassRefClase.FormularioExiste)
					{
						HTC120F1.DefInstance.Close();
					}
					FrRefFormulario.RecogerPeticion(0, "N");
					return;
				}
				catch (Exception e)
				{
					//Dim er As rdoError
					if (Information.Err().Number != 0)
					{
						Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
						RadMessageBox.Show(Msj, "Error");
					}
					//    RefRc.RollbackTrans
					//For Each er In rdoErrors
					//    'GestionErrorBaseDatos(rdoErrors.number)
					//    Call MsgBox(er.Number & "-" & er.Description, vbCritical + vbOKOnly)
					//Next er
					//rdoErrors.Clear
					//RefRc.RollbackTrans
					//    Error = True
					//    Call MsgBox("Si el error es de clave primaria " _
					//'    & "duplicada se controlar� con un mensaje: La carpeta esta " _
					//'    & "prestada al mismo destino solicitante")

					//Si no se llega a mostrar el formulario y se produce un error
					//mando el n� de error 5 que se da cuando no se puede efectuar
					//el alta
					if (StAuto == "S")
					{
						ClassMensaje = null;
						FrRefFormulario.RecogerPeticion(5, "N");
					}

					return;
				}
			}
			finally
			{
				VarFechaDevol_optional = VarFechaDevol;
			}
		}

		internal static void AltaHistoriaEnfermeria(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref string VarUnidadEnfermeria, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional, ref object VarFechaDevol_optional, object VarCodMedico_optional)
		{
			AltaHistoriaEnfermeria(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarUnidadEnfermeria, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, VarFechaEntrega_optional, ref VarFechaDevol_optional, VarCodMedico_optional, Type.Missing);
		}

		internal static void AltaHistoriaEnfermeria(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref string VarUnidadEnfermeria, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional, ref object VarFechaDevol_optional)
		{
			AltaHistoriaEnfermeria(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarUnidadEnfermeria, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, VarFechaEntrega_optional, ref VarFechaDevol_optional, Type.Missing, Type.Missing);
		}

		internal static void AltaHistoriaEnfermeria(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref string VarUnidadEnfermeria, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional)
		{
			object tempRefParam5 = Type.Missing;
			AltaHistoriaEnfermeria(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarUnidadEnfermeria, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, VarFechaEntrega_optional, ref tempRefParam5, Type.Missing, Type.Missing);
		}

		internal static void AltaHistoriaEnfermeria(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref string VarUnidadEnfermeria, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica)
		{
			object tempRefParam6 = Type.Missing;
			AltaHistoriaEnfermeria(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarUnidadEnfermeria, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, Type.Missing, ref tempRefParam6, Type.Missing, Type.Missing);
		}




		internal static void AltaHistoriaExterno(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref string VarEntidadExterna, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional, ref object VarFechaDevol_optional, object VarObser_optional)
		{
			string VarFechaEntrega = (VarFechaEntrega_optional == Type.Missing) ? String.Empty : VarFechaEntrega_optional as string;
			string VarFechaDevol = (VarFechaDevol_optional == Type.Missing) ? String.Empty : VarFechaDevol_optional as string;
			string VarObser = (VarObser_optional == Type.Missing) ? String.Empty : VarObser_optional as string;
            object tempRefVarFechaDevol = VarFechaDevol;

            try
            {
                int IError = 0;
                string Msj = String.Empty;
                int INumRegistros = 0;
                //error = False

                //Compruebo que para iguales valores en los campos: Servicio de archivo,
                //N� de historia,Contenido dossier y unidad de destino, no exista ya un
                //registro en HMOVEXTE
                StComando = "Select * from HMOVEXTE where GSERPROPIET=";
                StComando = StComando + ICodServicioArchivo.ToString();
                StComando = StComando + " and GHISTORIA = ";
                StComando = StComando + Convert.ToString(VarNHistoria);
                StComando = StComando + " and GENTIEXT=";
                StComando = StComando + "'" + VarEntidadExterna.Trim() + "'";
                StComando = StComando + " and IESTMOVIH='P'";
                //Linea a�adida para permitir dos peticiones iguales en diferentes dias:
                System.DateTime TempDate2 = DateTime.FromOADate(0);
                System.DateTime TempDate = DateTime.FromOADate(0);
                StComando = StComando + " and fpreventreg >= '" + ((DateTime.TryParse(VarFechaEntrega, out TempDate)) ? TempDate.ToString("yyyy/MM/dd") : VarFechaEntrega) + " 00:00:00" +
                            "' AND fpreventreg <= '" + ((DateTime.TryParse(VarFechaEntrega, out TempDate2)) ? TempDate2.ToString("yyyy/MM/dd") : VarFechaEntrega) + " 23:59:59' ";
                if (StContenido.Trim() != "A")
                {
                    StComando = StComando + " and ICONTENIDO=";
                    StComando = StComando + "'" + StContenido.Trim() + "'";
                }
                SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
                Rs = new DataSet();
                tempAdapter.Fill(Rs);
                if (Rs.Tables[0].Rows.Count != 0)
                {
                    IError = 4;
                    //If Trim(OrigenSolicitud) <> "S" Then
                    //Call MsgBox("Ya existe una solicitud de historia pendiente para esta Entidad Externa")
                    short tempRefParam = 2040;
                    string[] tempRefParam2 = new string[] { "Entidad Externa" };
                    ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RefRc, tempRefParam2);
                    //End If
                    return;
                }

                //Si no existe esta solicitud de historia como pendiente:

                if (StContenido.Trim() == "A")
                { //Si el contenido es ambas
                    StContenido = "H"; //habra que grabar 2 registros, H y P
                    INumRegistros = 2;
                }
                else
                {
                    INumRegistros = 1;
                }

                using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                {
                    try
                    {

                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();
                        for (int i = 1; i <= INumRegistros; i++)
                        {
                            StComando = "select * from HMOVEXTE where 2=1"; //Para que no me traiga ning�n registro
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, RefRc);
                            Rs = new DataSet();
                            tempAdapter_2.Fill(Rs);
                            Rs.AddNew();
                            Rs.Tables[0].Rows[0]["GHISTORIA"] = VarNHistoria;
                            Rs.Tables[0].Rows[0]["ICONTENIDO"] = StContenido.Trim().ToUpper();
                            Rs.Tables[0].Rows[0]["GENTIEXT"] = VarEntidadExterna.Trim().ToUpper();

                            //diego - 14/03/2007 - obtengo la fecha del servidor
                            strSQLFecha = "SELECT CONVERT(CHAR(8), getdate(), 114) AS HORA";
                            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(strSQLFecha, RefRc);
                            rsFechas = new DataSet();
                            tempAdapter_3.Fill(rsFechas);

                            if (INumRegistros == 1)
                            {
                                //        VarFechaPeticion = Format(CDate(Trim(VarFechaPeticion) & " " & Time), "dd/mm/yyyy hh:mm")
                                //diego - 14/03/2007 - VarFechaPeticion = Format(CDate(Trim(VarFechaPeticion) & " " & Time), "dd/mm/yyyy hh:nn:ss")
                                VarFechaPeticion = DateTime.Parse(Convert.ToString(VarFechaPeticion).Trim() + " " + Convert.ToString(rsFechas.Tables[0].Rows[0]["HORA"])).ToString("dd/MM/yyyy HH:mm:ss");
                                VarFechaPeticion = Convert.ToDateTime(VarFechaPeticion).AddSeconds(PrioridadCita);

                            }
                            Rs.Tables[0].Rows[0]["FPETICIO"] = Convert.ToDateTime(VarFechaPeticion);
                            Rs.Tables[0].Rows[0]["GMOTPETH"] = VarMotiPeticion.Trim().ToUpper();
                            if (VarFechaEntrega_optional != Type.Missing && !Convert.IsDBNull(VarFechaEntrega) && VarFechaEntrega_optional.ToString().Length > 0)
                            {
                                Rs.Tables[0].Rows[0]["FPREVENTREG"] = DateTime.Parse(VarFechaEntrega);
                            }
                            else
                            {
                                Rs.Tables[0].Rows[0]["FPREVENTREG"] = DBNull.Value;
                            }
                            Rs.Tables[0].Rows[0]["FPRESTAMOH"] = DBNull.Value;
                            if (VarFechaDevol_optional != Type.Missing && !Convert.IsDBNull(VarFechaDevol) && VarFechaDevol_optional.ToString().Length > 0)
                            {
                                Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DateTime.Parse(VarFechaDevol);
                            }
                            else
                            {
                                Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DBNull.Value;
                            }
                            Rs.Tables[0].Rows[0]["FDEVOLUCH"] = DBNull.Value;
                            Rs.Tables[0].Rows[0]["IESTMOVIH"] = "P";
                            if (VarObser_optional != Type.Missing && !Convert.IsDBNull(VarObser) && VarObser_optional.ToString().Length > 0)
                            {
                                Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = VarObser.Trim().ToUpper();
                            }
                            else
                            {
                                Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = DBNull.Value;
                            }
                            Rs.Tables[0].Rows[0]["GSERPROPIET"] = ICodServicioArchivo;
                            string tempQuery = Rs.Tables[0].TableName;
                            SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tempQuery, "");
                            SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_4);
                            tempAdapter_4.Update(Rs, Rs.Tables[0].TableName);
                            Rs.Close();
                            rsFechas.Close();
                            ActualizarFechaUltiMov(StIPaciente);
                            if (INumRegistros != 1)
                            {
                                StContenido = "P";
                            }
                        }
                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.CommitTrans();
                        scope.Complete();

                        if (VarIAutomatica.Trim() == "N" && OrigenSolicitud != "U" && v_ResguardoPeticion.Value == "S")
                        {
                            //DAR POSIBILIDAD DE EMISION DEL RESGUARDO DE SOLICITUDES H4580
                            //res = MsgBox("�Desea un resguardo de su solicitud?", vbQuestion + vbYesNo)
                            short tempRefParam3 = 1820;
                            string[] tempRefParam4 = new string[] { "el resguardo de su solicitud" };
                            res = (DialogResult)Convert.ToInt32(ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RefRc, tempRefParam4));
                            if (res == System.Windows.Forms.DialogResult.Yes)
                            {
                                InformeSolicitud();
                            }
                        }
                        //Solo se preguntar� si se quiere hacer el pr�stamo si viene de Archivo
                        //y la petici�n no es autom�tica
                        if (VarIAutomatica.Trim() == "N" && OrigenSolicitud.Trim() == "A")
                        {
                            //PREGUNTAR SI SE QUIERE HACER EL PRESTAMO EN ESTE MOMENTO H4200
                            if (INumRegistros != 1)
                            {
                                StContenido = "A";
                            }
                            if (EstaPrestada(Conversion.Val(Convert.ToString(VarNHistoria)), StContenido, ICodServicioArchivo))
                            {
                                //Call MsgBox("La carpeta solicitada ya est� prestada", vbInformation + vbOKOnly)
                                short tempRefParam5 = 1980;
                                string[] tempRefParam6 = new string[] { "solicitada", "prestar", "ya est� prestada" };
                                ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, RefRc, tempRefParam6);
                            }
                            else if (!ComprobarDisponible(Conversion.Val(Convert.ToString(VarNHistoria)), StContenido, ICodServicioArchivo))
                            {
                                //Call MsgBox("La carpeta solicitada no est� disponible para ser prestada", vbInformation + vbOKOnly)
                                short tempRefParam7 = 1980;
                                string[] tempRefParam8 = new string[] { "solicitada", "prestar", "no est� disponible" };
                                ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, RefRc, tempRefParam8);
                            }
                            else
                            {
                                //If Not EstaPrestada(Val(VarNHistoria), StContenido, ICodServicioArchivo) And ComprobarDisponible(Val(VarNHistoria), StContenido, ICodServicioArchivo) = True Then
                                //res = MsgBox("�Desea hacer el pr�stamo en este momento?", vbQuestion + vbYesNo)
                                short tempRefParam9 = 3300;
                                string[] tempRefParam10 = new string[] { };
                                res = (DialogResult)Convert.ToInt32(ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, RefRc, tempRefParam10));
                                if (res == System.Windows.Forms.DialogResult.Yes)
                                {
                                    if (ComprobarActivoONoCumplimentada(Conversion.Str(VarNHistoria)))
                                    {
                                        if (VarObser_optional != Type.Missing && !Convert.IsDBNull(VarObser) && VarObser_optional.ToString().Length > 0)
                                        {
                                            double tempRefParam11 = Conversion.Val(Convert.ToString(VarNHistoria));
                                            string tempRefParam12 = Convert.ToString(VarFechaPeticion).Trim();
                                            object tempRefParam13 = null;
                                            string tempRefParam14 = VarObser.ToUpper();
                                            LlamarPrestar("E", ref tempRefParam11, ref StContenido, ref tempRefParam12, ref ICodServicioArchivo, ref tempRefVarFechaDevol, ref tempRefParam13, ref VarEntidadExterna, ref tempRefParam14);
                                        }
                                        else
                                        {
                                            double tempRefParam15 = Conversion.Val(Convert.ToString(VarNHistoria));
                                            string tempRefParam16 = Convert.ToString(VarFechaPeticion).Trim();
                                            object tempRefParam17 = null;
                                            LlamarPrestar("E", ref tempRefParam15, ref StContenido, ref tempRefParam16, ref ICodServicioArchivo, ref tempRefVarFechaDevol, ref tempRefParam17, ref VarEntidadExterna);
                                        }
                                    }
                                    return;
                                }
                            }
                        }
                        ClassMensaje = null;
                        if (ClassRefClase.FormularioExiste)
                        {
                            HTC120F1.DefInstance.Close();
                        }
                        FrRefFormulario.RecogerPeticion(0, "N");
                        return;
                    }

                    catch (Exception e)
                    {
                        //Dim er As rdoError
                        if (Information.Err().Number != 0)
                        {
                            Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
                            RadMessageBox.Show(Msj, "Error");
                        }
                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
                        //For Each er In rdoErrors
                        //    'GestionErrorBaseDatos(rdoErrors.number)
                        //    Call MsgBox(er.Number & "-" & er.Description, vbCritical + vbOKOnly)
                        //Next er
                        //rdoErrors.Clear
                        //Error = True
                        //    Call MsgBox("Si el error es de clave primaria " _
                        //'    & "duplicada se controlar� con un mensaje: La carpeta esta " _
                        //'    & "prestada al mismo destino solicitante")

                        //Si no se llega a mostrar el formulario y se produce un error
                        //mando el n� de error 5 que se da cuando no se puede efectuar
                        //el alta
                        if (StAuto == "S")
                        {
                            ClassMensaje = null;
                            FrRefFormulario.RecogerPeticion(5, "N");
                        }

                        return;
                    }
                }            
			}
			finally
			{
				VarFechaDevol_optional = VarFechaDevol;
			}
		}

		internal static void AltaHistoriaExterno(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref string VarEntidadExterna, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional, ref object VarFechaDevol_optional)
		{
			AltaHistoriaExterno(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarEntidadExterna, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, VarFechaEntrega_optional, ref VarFechaDevol_optional, Type.Missing);
		}

		internal static void AltaHistoriaExterno(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref string VarEntidadExterna, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional)
		{
			object tempRefParam7 = Type.Missing;
			AltaHistoriaExterno(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarEntidadExterna, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, VarFechaEntrega_optional, ref tempRefParam7, Type.Missing);
		}

		internal static void AltaHistoriaExterno(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref string VarEntidadExterna, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica)
		{
			object tempRefParam8 = Type.Missing;
			AltaHistoriaExterno(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarEntidadExterna, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, Type.Missing, ref tempRefParam8, Type.Missing);
		}

		internal static void AltaHistoriaSinNumero(int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarCodServicioPeticionario, string VarSalaConsulta, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional, object VarFechaDevol_optional, object VarCodMedico_optional)
		{
			object VarCodMedico = (VarCodMedico_optional == Type.Missing) ? null : VarCodMedico_optional as object;
			string VarFechaEntrega = (VarFechaEntrega_optional == Type.Missing) ? String.Empty : VarFechaEntrega_optional as string;
			string VarFechaDevol = (VarFechaDevol_optional == Type.Missing) ? String.Empty : VarFechaDevol_optional as string;
			string Msj = String.Empty;
			int INumRegistros = 0;
			try
			{
				//error = False

				if (StContenido.Trim() == "A")
				{ //Si el contenido es ambas
					StContenido = "H"; //habra que grabar 2 registros, H y P
					INumRegistros = 2;
				}
				else
				{
					INumRegistros = 1;
				}

				//RefRc.BeginTrans
				for (int i = 1; i <= INumRegistros; i++)
				{
					StComando = "select * from HPETSINH where 2=1"; //Para que no me traiga ning�n registro
					SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
					Rs = new DataSet();
					tempAdapter.Fill(Rs);
					Rs.AddNew();
					Rs.Tables[0].Rows[0]["GIDENPAC"] = StIPaciente.Trim();
					Rs.Tables[0].Rows[0]["GSERPROPIET"] = ICodServicioArchivo;
					Rs.Tables[0].Rows[0]["ICONTENIDO"] = StContenido.Trim().ToUpper();
					Rs.Tables[0].Rows[0]["GSERSOLI"] = VarCodServicioPeticionario;
					Rs.Tables[0].Rows[0]["GSALACON"] = VarSalaConsulta.Trim().ToUpper();

					//diego - 14/03/2007 - obtengo la fecha del servidor
					strSQLFecha = "SELECT CONVERT(CHAR(8), getdate(), 114) AS HORA";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(strSQLFecha, RefRc);
					rsFechas = new DataSet();
					tempAdapter_2.Fill(rsFechas);


					if (INumRegistros == 1)
					{
						//        VarFechaPeticion = Format(CDate(Trim(VarFechaPeticion) & " " & Time), "dd/mm/yyyy hh:mm")
						//diego - 14/03/2007 - VarFechaPeticion = Format(CDate(Trim(VarFechaPeticion) & " " & Time), "dd/mm/yyyy hh:nn:ss")
						VarFechaPeticion = DateTime.Parse(Convert.ToString(VarFechaPeticion).Trim() + " " + Convert.ToString(rsFechas.Tables[0].Rows[0]["HORA"])).ToString("dd/MM/yyyy HH:mm:ss");
						VarFechaPeticion = Convert.ToDateTime(VarFechaPeticion).AddSeconds(PrioridadCita);
					}
					Rs.Tables[0].Rows[0]["FPETICIO"] = Convert.ToDateTime(VarFechaPeticion);
					Rs.Tables[0].Rows[0]["GMOTPETH"] = VarMotiPeticion.Trim().ToUpper();
					if (VarCodMedico_optional != Type.Missing && VarCodMedico_optional.ToString().Length > 0)
					{
						Rs.Tables[0].Rows[0]["GMEDSOLI"] = Conversion.Val(Convert.ToString(VarCodMedico));
					}
					else
					{
						Rs.Tables[0].Rows[0]["GMEDSOLI"] = DBNull.Value;
					}
					if (VarFechaEntrega_optional != Type.Missing && !Convert.IsDBNull(VarFechaEntrega) && VarFechaEntrega_optional.ToString().Length > 0)
					{
						Rs.Tables[0].Rows[0]["FPREVENTREG"] = DateTime.Parse(VarFechaEntrega);
					}
					else
					{
						Rs.Tables[0].Rows[0]["FPREVENTREG"] = DBNull.Value;
					}
					if (VarFechaDevol_optional != Type.Missing && !Convert.IsDBNull(VarFechaDevol) && VarFechaDevol_optional.ToString().Length > 0)
					{
						Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DateTime.Parse(VarFechaDevol);
					}
					else
					{
						Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DBNull.Value;
					}
					string tempQuery = Rs.Tables[0].TableName;
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
					tempAdapter_3.Update(Rs, Rs.Tables[0].TableName);
					Rs.Close();
					rsFechas.Close();

					//''Si existe env�o de datos de lista de espera al servicio web de CatSalut, no modificamos aqu� la tabla del paciente.
					//''Como son dos transacciones distintas, en esta se bloquea la tabla de pacientes y en la otra, al insertar en AEPISLEQ, se produce el bloqueo.
					//''Por eso, si est� activa la constante, se ralizar� la actualizaci�n de la fecha de �ltimo movimiento en el servicio web.
					strSQLFecha = "SELECT count(*) FROM SCONSGLO WHERE GCONSGLO='IENVICAT' and valfanu1='S'";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(strSQLFecha, RefRc);
					rsFechas = new DataSet();
					tempAdapter_4.Fill(rsFechas);
					if (Convert.ToDouble(rsFechas.Tables[0].Rows[0][0]) != 1)
					{
						ActualizarFechaUltiMov(StIPaciente);
					}
					rsFechas.Close();


					StContenido = "P";
				}
				//RefRc.CommitTrans
				ClassMensaje = null;
				if (ClassRefClase.FormularioExiste)
				{
					HTC120F1.DefInstance.Close();
				}
				FrRefFormulario.RecogerPeticion(0, "N");
			}
			catch (Exception e)
			{
				//Dim er As rdoError
				if (Information.Err().Number != 0)
				{
					Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
					RadMessageBox.Show(Msj, "Error");
				}
				//For Each er In rdoErrors
				//GestionErrorBaseDatos(rdoErrors.number)
				//    Call MsgBox(er.Number & "-" & er.Description, vbCritical + vbOKOnly)
				//Next er
				//Call MsgBox(rdoErrors(0).Number & "-" & rdoErrors(0).Description, vbCritical + vbOKOnly)
				//rdoErrors.Clear
				//    RefRc.RollbackTrans
				//Error = True
				//Err.Clear
				//Unload HTC120F1
				//Exit Sub
				//    Call MsgBox("Si el error es de clave primaria " _
				//'    & "duplicada se controlar� con un mensaje: La carpeta esta " _
				//'    & "prestada al mismo destino solicitante")

				//Si no se llega a mostrar el formulario y se produce un error
				//mando el n� de error 5 que se da cuando no se puede efectuar
				//el alta
				if (StAuto == "S")
				{
					ClassMensaje = null;
					FrRefFormulario.RecogerPeticion(5, "N");
				}
			}
		}

		internal static void AltaHistoriaSinNumero(int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarCodServicioPeticionario, string VarSalaConsulta, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional, object VarFechaDevol_optional)
		{
			AltaHistoriaSinNumero(ICodServicioArchivo, ref StContenido, StIPaciente, VarCodServicioPeticionario, VarSalaConsulta, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, VarFechaEntrega_optional, VarFechaDevol_optional, Type.Missing);
		}

		internal static void AltaHistoriaSinNumero(int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarCodServicioPeticionario, string VarSalaConsulta, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica, object VarFechaEntrega_optional)
		{
			AltaHistoriaSinNumero(ICodServicioArchivo, ref StContenido, StIPaciente, VarCodServicioPeticionario, VarSalaConsulta, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, VarFechaEntrega_optional, Type.Missing, Type.Missing);
		}

		internal static void AltaHistoriaSinNumero(int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarCodServicioPeticionario, string VarSalaConsulta, ref object VarFechaPeticion, string VarMotiPeticion, string VarIAutomatica)
		{
			AltaHistoriaSinNumero(ICodServicioArchivo, ref StContenido, StIPaciente, VarCodServicioPeticionario, VarSalaConsulta, ref VarFechaPeticion, VarMotiPeticion, VarIAutomatica, Type.Missing, Type.Missing, Type.Missing);
		}

		internal static void AltaHistoriaSolicitante(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref object VarCodServicioPeticionario, ref object VarFechaPeticion, string VarMotiPeticion, object VarCodMedico, string VarIAutomatica, object VarSalaSolicitante_optional, object VarFechaEntrega_optional, ref object VarFechaDevol_optional, object VarObser_optional)
		{
			string VarSalaSolicitante = (VarSalaSolicitante_optional == Type.Missing) ? String.Empty : VarSalaSolicitante_optional as string;
			string VarFechaEntrega = (VarFechaEntrega_optional == Type.Missing) ? String.Empty : VarFechaEntrega_optional as string;
			string VarFechaDevol = (VarFechaDevol_optional == Type.Missing) ? String.Empty : VarFechaDevol_optional as string;
			string VarObser = (VarObser_optional == Type.Missing) ? String.Empty : VarObser_optional as string;

            

            try
			{
				int IError = 0;
				string Msj = String.Empty;
				int INumRegistros = 0;
				string mensaje = String.Empty;
				//error = False


				//Compruebo que para iguales valores en los campos: Servicio de archivo,
				//N� de historia,Contenido dossier y servicio peticionario, no exista ya un
				//registro en HMOVSERV
				StComando = "Select * from HMOVSERV where GSERPROPIET=";
				StComando = StComando + ICodServicioArchivo.ToString();
				StComando = StComando + " and GHISTORIA = ";
				StComando = StComando + Convert.ToString(VarNHistoria);
				StComando = StComando + " and GSERSOLI=";
				StComando = StComando + Convert.ToString(VarCodServicioPeticionario);
				StComando = StComando + " and IESTMOVIH='P'";
				//Linea a�adida para permitir dos peticiones iguales en diferentes dias:
				System.DateTime TempDate2 = DateTime.FromOADate(0);
				System.DateTime TempDate = DateTime.FromOADate(0);
				StComando = StComando + " and fpreventreg >= '" + ((DateTime.TryParse(VarFechaEntrega, out TempDate)) ? TempDate.ToString("yyyy/MM/dd") : VarFechaEntrega) + " 00:00:00" + 
				            "' AND fpreventreg <= '" + ((DateTime.TryParse(VarFechaEntrega, out TempDate2)) ? TempDate2.ToString("yyyy/MM/dd") : VarFechaEntrega) + " 23:59:59' ";
				if (StContenido.Trim() != "A")
				{
					StComando = StComando + " and ICONTENIDO=";
					StComando = StComando + "'" + StContenido.Trim() + "'";
				}
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter.Fill(Rs);
				if (Rs.Tables[0].Rows.Count != 0)
				{
					IError = 4;
					//If Trim(OrigenSolicitud) <> "S" Then
					//        mensaje = "Ya existe una solicitud de historia pendiente para este servicio"
					//        Call MsgBox(mensaje)
					short tempRefParam = 2040;
                    string[] tempRefParam2 = new string[]{"Servicio Solicitante"};
					ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam, RefRc, tempRefParam2);
					//End If
					return;
				}

				//Si no existe esta solicitud de historia como pendiente:

				if (StContenido.Trim() == "A")
				{ //Si el contenido es ambas
					StContenido = "H"; //habra que grabar 2 registros, H y P
					INumRegistros = 2;
				}
				else
				{
					INumRegistros = 1;
				}

				try
				{
					//RefRc.BeginTrans
					for (int i = 1; i <= INumRegistros; i++)
					{
						StComando = "select * from HMOVSERV where 2=1"; //Para que no me traiga ning�n registro
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, RefRc);
						Rs = new DataSet();
						tempAdapter_2.Fill(Rs);
						Rs.AddNew();
						Rs.Tables[0].Rows[0]["GHISTORIA"] = VarNHistoria;
						Rs.Tables[0].Rows[0]["ICONTENIDO"] = StContenido.Trim().ToUpper();
						Rs.Tables[0].Rows[0]["GSERSOLI"] = VarCodServicioPeticionario;

						//diego - 14/03/2007 - obtengo la fecha del servidor
						strSQLFecha = "SELECT CONVERT(CHAR(8), getdate(), 114) AS HORA";
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(strSQLFecha, RefRc);
						rsFechas = new DataSet();
						tempAdapter_3.Fill(rsFechas);

						if (INumRegistros == 1)
						{
							//        VarFechaPeticion = Format(CDate(Trim(VarFechaPeticion) & " " & Time), "dd/mm/yyyy hh:mm")
							//diego - 14/03/2007 - VarFechaPeticion = Format(CDate(Trim(VarFechaPeticion) & " " & Time), "dd/mm/yyyy hh:nn:ss")
							VarFechaPeticion = DateTime.Parse(Convert.ToString(VarFechaPeticion).Trim() + " " + Convert.ToString(rsFechas.Tables[0].Rows[0]["HORA"])).ToString("dd/MM/yyyy HH:mm:ss");
							VarFechaPeticion = Convert.ToDateTime(VarFechaPeticion).AddSeconds(PrioridadCita);

						}
						Rs.Tables[0].Rows[0]["FPETICIO"] = Convert.ToDateTime(VarFechaPeticion);
						Rs.Tables[0].Rows[0]["GMOTPETH"] = VarMotiPeticion.Trim();
						if (VarSalaSolicitante_optional!= null && VarSalaSolicitante_optional != Type.Missing && VarSalaSolicitante_optional.ToString().Length > 0)
						{
							Rs.Tables[0].Rows[0]["GQUIROFA"] = VarSalaSolicitante.Trim().ToUpper();
						}
						else
						{
							Rs.Tables[0].Rows[0]["GQUIROFA"] = DBNull.Value;
						}
						Rs.Tables[0].Rows[0]["GMEDSOLI"] = VarCodMedico;
						if (VarFechaEntrega_optional != Type.Missing && !Convert.IsDBNull(VarFechaEntrega) && VarFechaEntrega_optional.ToString().Length > 0)
						{
							Rs.Tables[0].Rows[0]["FPREVENTREG"] = DateTime.Parse(VarFechaEntrega);
						}
						else
						{
							Rs.Tables[0].Rows[0]["FPREVENTREG"] = DBNull.Value;
						}
						Rs.Tables[0].Rows[0]["FPRESTAMOH"] = DBNull.Value;
						if (VarFechaDevol_optional != Type.Missing && !Convert.IsDBNull(VarFechaDevol) && VarFechaDevol_optional.ToString().Length > 0)
						{
							Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DateTime.Parse(VarFechaDevol);
						}
						else
						{
							Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DBNull.Value;
						}
						Rs.Tables[0].Rows[0]["FDEVOLUCH"] = DBNull.Value;
						Rs.Tables[0].Rows[0]["IESTMOVIH"] = "P";
						if (VarObser_optional != Type.Missing && !Convert.IsDBNull(VarObser) && VarObser_optional.ToString().Length > 0)
						{
							Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = VarObser.Trim().ToUpper();
						}
						else
						{
							Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = DBNull.Value;
						}
						Rs.Tables[0].Rows[0]["GSERPROPIET"] = ICodServicioArchivo;
						string tempQuery = Rs.Tables[0].TableName;						
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                        tempAdapter_2.Update(Rs, tempQuery);
						Rs.Close();
						rsFechas.Close();
						ActualizarFechaUltiMov(StIPaciente);
						if (INumRegistros != 1)
						{
							StContenido = "P";
						}
					}
					//RefRc.CommitTrans
					if (VarIAutomatica.Trim() == "N" && OrigenSolicitud != "U" && v_ResguardoPeticion.Value == "S")
					{
						//DAR POSIBILIDAD DE EMISION DEL RESGUARDO DE SOLICITUDES H4580
						//res = MsgBox("�Desea un resguardo de su solicitud?", vbQuestion + vbYesNo)
						short tempRefParam3 = 1820;
                        string[] tempRefParam4 = new string[]{"el resguardo de su solicitud"};
						res = (DialogResult) Convert.ToInt32(ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam3, RefRc, tempRefParam4));
						if (res == System.Windows.Forms.DialogResult.Yes)
						{
							InformeSolicitud();
						}
					}
					//Solo se preguntar� si se quiere hacer el pr�stamo si viene de Archivo
					//y la petici�n no es autom�tica
					if (VarIAutomatica.Trim() == "N" && OrigenSolicitud.Trim() == "A")
					{
						//PREGUNTAR SI SE QUIERE HACER EL PRESTAMO EN ESTE MOMENTO H4200
						if (INumRegistros != 1)
						{
							StContenido = "A";
						}
						if (EstaPrestada(Conversion.Val(Convert.ToString(VarNHistoria)), StContenido, ICodServicioArchivo))
						{
							//Call MsgBox("La carpeta solicitada no est� disponible para ser prestada", vbInformation + vbOKOnly)
							short tempRefParam5 = 1980;
                            string[] tempRefParam6 = new string[]{"solicitada", "prestar", "ya est� prestada"};
							ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam5, RefRc, tempRefParam6);
						}
						else if (!ComprobarDisponible(Conversion.Val(Convert.ToString(VarNHistoria)), StContenido, ICodServicioArchivo))
						{ 
							//Call MsgBox("La carpeta solicitada no est� disponible para ser prestada", vbInformation + vbOKOnly)
							short tempRefParam7 = 1980;
                            string[] tempRefParam8 = new string[]{"solicitada", "prestar", "no est� disponible"};
							ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam7, RefRc, tempRefParam8);
						}
						else
						{
							//If Not EstaPrestada(Val(VarNHistoria), StContenido, ICodServicioArchivo) And ComprobarDisponible(Val(VarNHistoria), StContenido, ICodServicioArchivo) = True Then
							//res = MsgBox("�Desea hacer el pr�stamo en este momento?", vbQuestion + vbYesNo)
							short tempRefParam9 = 3300;
                            string[] tempRefParam10 = new string[]{""};
							res = (DialogResult) Convert.ToInt32(ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam9, RefRc, tempRefParam10));

                            object tempRefParamDevol = VarFechaDevol;

                            if (res == System.Windows.Forms.DialogResult.Yes)
							{
								if (ComprobarActivoONoCumplimentada(Conversion.Str(VarNHistoria)))
								{
									if (VarObser_optional != Type.Missing && !Convert.IsDBNull(VarObser) && VarObser_optional.ToString().Length > 0)
									{
										double tempRefParam11 = Conversion.Val(Convert.ToString(VarNHistoria));
										string tempRefParam12 = Convert.ToString(VarFechaPeticion).Trim();
										string tempRefParam13 = String.Empty;
										string tempRefParam14 = VarObser.ToUpper();
										LlamarPrestar("S", ref tempRefParam11, ref StContenido, ref tempRefParam12, ref ICodServicioArchivo, ref tempRefParamDevol, ref VarCodServicioPeticionario, ref tempRefParam13, ref tempRefParam14);
									}
									else
									{
										double tempRefParam15 = Conversion.Val(Convert.ToString(VarNHistoria));
										string tempRefParam16 = Convert.ToString(VarFechaPeticion).Trim();
										LlamarPrestar("S", ref tempRefParam15, ref StContenido, ref tempRefParam16, ref ICodServicioArchivo, ref tempRefParamDevol, ref VarCodServicioPeticionario);
									}
								}
								return;
							}
						}
					}
					ClassMensaje = null;
					if (ClassRefClase.FormularioExiste)
					{
						HTC120F1.DefInstance.Close();
					}
					FrRefFormulario.RecogerPeticion(0, "N");
					return;
				}
				catch (Exception e)
				{
					//Dim er As rdoError
					if (Information.Err().Number != 0)
					{
						Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
						RadMessageBox.Show(Msj, "Error");
					}
					//    RefRc.RollbackTrans
					//For Each er In rdoErrors
					//    'GestionErrorBaseDatos(rdoErrors.number)
					//    Call MsgBox(er.Number & "-" & er.Description, vbCritical + vbOKOnly)
					//Next er
					//rdoErrors.Clear
					//Error = True
					//    Call MsgBox("Si el error es de clave primaria " _
					//'    & "duplicada se controlar� con un mensaje: La carpeta esta " _
					//'    & "prestada al mismo destino solicitante")

					//Si no se llega a mostrar el formulario y se produce un error
					//mando el n� de error 5 que se da cuando no se puede efectuar
					//el alta
					if (StAuto == "S")
					{
						ClassMensaje = null;
						FrRefFormulario.RecogerPeticion(5, "N");
					}

					return;
				}
			}
			finally
			{
				VarFechaDevol_optional = VarFechaDevol;
			}
		}

		internal static void AltaHistoriaSolicitante(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref object VarCodServicioPeticionario, ref object VarFechaPeticion, string VarMotiPeticion, object VarCodMedico, string VarIAutomatica, object VarSalaSolicitante_optional, object VarFechaEntrega_optional, ref object VarFechaDevol_optional)
		{
			AltaHistoriaSolicitante(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarCodServicioPeticionario, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarIAutomatica, VarSalaSolicitante_optional, VarFechaEntrega_optional, ref VarFechaDevol_optional, Type.Missing);
		}

		internal static void AltaHistoriaSolicitante(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref object VarCodServicioPeticionario, ref object VarFechaPeticion, string VarMotiPeticion, object VarCodMedico, string VarIAutomatica, object VarSalaSolicitante_optional, object VarFechaEntrega_optional)
		{
			object tempRefParam9 = Type.Missing;
			AltaHistoriaSolicitante(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarCodServicioPeticionario, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarIAutomatica, VarSalaSolicitante_optional, VarFechaEntrega_optional, ref tempRefParam9, Type.Missing);
		}

		internal static void AltaHistoriaSolicitante(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref object VarCodServicioPeticionario, ref object VarFechaPeticion, string VarMotiPeticion, object VarCodMedico, string VarIAutomatica, object VarSalaSolicitante_optional)
		{
			object tempRefParam10 = Type.Missing;
			AltaHistoriaSolicitante(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarCodServicioPeticionario, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarIAutomatica, VarSalaSolicitante_optional, Type.Missing, ref tempRefParam10, Type.Missing);
		}

		internal static void AltaHistoriaSolicitante(ref int ICodServicioArchivo, ref string StContenido, string StIPaciente, object VarNHistoria, ref object VarCodServicioPeticionario, ref object VarFechaPeticion, string VarMotiPeticion, object VarCodMedico, string VarIAutomatica)
		{
			object tempRefParam11 = Type.Missing;
			AltaHistoriaSolicitante(ref ICodServicioArchivo, ref StContenido, StIPaciente, VarNHistoria, ref VarCodServicioPeticionario, ref VarFechaPeticion, VarMotiPeticion, VarCodMedico, VarIAutomatica, Type.Missing, Type.Missing, ref tempRefParam11, Type.Missing);
		}

		internal static void CargarServicio(string stTipo = "")
		{

			HTC120F1.DefInstance.CBB_peticionario.Items.Clear();
			StComando = "select DNOMSERV from DSERVICI ";
			StComando = StComando + "where FBORRADO is null ";

			switch(stTipo)
			{
				case "C" :  //consultas 
					StComando = StComando + " and iconsult='S' "; 
					break;
				case "S" :  //servicios medicos 
					StComando = StComando + " and (iconsult='S' or iurgenci='S' OR " + " ihospita='S' OR  iconsult='S' OR  icentral='S' OR  iquirofa='S') "; 
					 
					break;
			}
			StComando = StComando + "order by 1";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
			Rs = new DataSet();
			tempAdapter.Fill(Rs);
			foreach (DataRow iteration_row in Rs.Tables[0].Rows)
			{
				HTC120F1.DefInstance.CBB_peticionario.AddItem((object)(Convert.ToString(iteration_row[0]).Trim()),null);
			}
			Rs.Close();

		}

		internal static void CargarUnidadDestino(string StDescripcion, string stTabla)
		{
			HTC120F1.DefInstance.CBB_unidad.Items.Clear();
			StComando = "Select " + StDescripcion + " from  " + stTabla;
			StComando = StComando + " where FBORRADO is null ";
			//If Trim(StTabla) = "DSALACON" And Not IsMissing(VarCodigo) Then
			//    StComando = StComando & " and GSERVICI=" & VarCodigo
			//End If
			StComando = StComando + " order by 1";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
			Rs = new DataSet();
			tempAdapter.Fill(Rs);
			foreach (DataRow iteration_row in Rs.Tables[0].Rows)
			{
				HTC120F1.DefInstance.CBB_unidad.AddItem((object)(Convert.ToString(iteration_row[0]).Trim()),null);
			}
			Rs.Close();
		}
		internal static void CargarMotivo()
		{
			HTC120F1.DefInstance.CBB_motivo.Items.Clear();
			StComando = "select DMOTPETH ";
			StComando = StComando + "from DMOTPETH ";
			StComando = StComando + "where FBORRADO is null ";
			StComando = StComando + "order by 1";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
			Rs = new DataSet();
			tempAdapter.Fill(Rs);
			foreach (DataRow iteration_row in Rs.Tables[0].Rows)
			{
				HTC120F1.DefInstance.CBB_motivo.AddItem((object)Convert.ToString(iteration_row[0]).Trim(),null);
			}
			Rs.Close();
		}



		private static bool EstaPrestada(double LNHistoria, string StContenido, int ICodServicioAREFRChivo)
		{
			bool result = false;
			int INumRegistros = 0;
			if (StContenido == "A")
			{
				INumRegistros = 2;
				StContenido = "H";
			}
			else
			{
				INumRegistros = 1;
			}
			for (int i = 1; i <= INumRegistros; i++)
			{
				StComando = "Select * from HMOVSERV where GHISTORIA=";
				StComando = StComando + LNHistoria.ToString();
				StComando = StComando + " and IESTMOVIH='E'";
				StComando = StComando + " and ICONTENIDO=" + "'" + StContenido.Trim() + "'";
				StComando = StComando + " and GSERPROPIET=" + ICodServicioAREFRChivo.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter.Fill(Rs);
				if (Rs.Tables[0].Rows.Count != 0)
				{
					result = true;
					Rs.Close();
					return result;
				}
				Rs.Close();
				StComando = "Select * from HMOVUENF where GHISTORIA=";
				StComando = StComando + LNHistoria.ToString();
				StComando = StComando + " and IESTMOVIH='E'";
				StComando = StComando + " and ICONTENIDO=" + "'" + StContenido.Trim() + "'";
				StComando = StComando + " and GSERPROPIET=" + ICodServicioAREFRChivo.ToString();
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter_2.Fill(Rs);
				if (Rs.Tables[0].Rows.Count != 0)
				{
					result = true;
					Rs.Close();
					return result;
				}
				Rs.Close();
				StComando = "Select * from HMOVCONS where GHISTORIA=";
				StComando = StComando + LNHistoria.ToString();
				StComando = StComando + " and IESTMOVIH='E'";
				StComando = StComando + " and ICONTENIDO=" + "'" + StContenido.Trim() + "'";
				StComando = StComando + " and GSERPROPIET=" + ICodServicioAREFRChivo.ToString();
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter_3.Fill(Rs);
				if (Rs.Tables[0].Rows.Count != 0)
				{
					result = true;
					Rs.Close();
					return result;
				}
				Rs.Close();
				StComando = "Select * from HMOVEXTE where GHISTORIA=";
				StComando = StComando + LNHistoria.ToString();
				StComando = StComando + " and IESTMOVIH='E'";
				StComando = StComando + " and ICONTENIDO=" + "'" + StContenido.Trim() + "'";
				StComando = StComando + " and GSERPROPIET=" + ICodServicioAREFRChivo.ToString();
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StComando, RefRc);
				Rs = new DataSet();
				tempAdapter_4.Fill(Rs);
				if (Rs.Tables[0].Rows.Count != 0)
				{
					result = true;
					Rs.Close();
					return result;
				}
				Rs.Close();
				if (INumRegistros != 1)
				{
					StContenido = "P";
				}
			}
			return false;
		}

		internal static void InformePrestamo()
		{
			string StContenido = String.Empty;

            /*//DGMORENOG_TODO_X_5_IMPLEMENTA CRYSTAL REPORT - INICIO
            PathString = Path.GetDirectoryName(Application.ExecutablePath);
			try
			{
				//Saca la pantalla para elegir la impresora y el n� de copias
				Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
				proDialogo_impre(HTC120F1.DefInstance.CrystalReport1);

				// Llamada al CRYSTAL para sacar el informe, CONEXION

				//HTC120F1.CrystalReport1.ReportFileName = "" & PathString & "\rpt\HTC459R3.rpt"
				HTC120F1.DefInstance.CrystalReport1.ReportFileName = vStPathReport + "\\HTC459R3.rpt";
				HTC120F1.DefInstance.CrystalReport1.WindowTitle = "Resguardo de Pr�stamos de Historias";

				if (!VstrCrystal.VfCabecera)
				{
					proCabCrystal();
				}

				HTC120F1.DefInstance.CrystalReport1.set_Formulas(0, "Empresa= \"" + VstrCrystal.VstGrupoHo + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(1, "Hospital= \"" + VstrCrystal.VstNombreHo + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(2, "Direccion= \"" + VstrCrystal.VstDireccHo + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(3, "Servicio= \"" + HTC120F1.DefInstance.LB_Servicio.Text.Trim() + "\"");
				if (HTC120F1.DefInstance.RB_Historia.Checked)
				{
					StContenido = "H";
				}
				else if (HTC120F1.DefInstance.RB_Placas.Checked)
				{ 
					StContenido = "P";
				}
				else
				{
					StContenido = "H/P";
				}
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(4, "Contenido= \"" + StContenido.Trim() + "\"");
				if (HTC120F1.DefInstance.RB_Enfermeria.Checked)
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(5, "TipoDes= \"" + "UNIDAD DE ENFERMERIA" + "\"");
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(6, "Peticionario= \"" + HTC120F1.DefInstance.CBB_unidad.Text.Trim() + "\"");
				}
				else if (HTC120F1.DefInstance.RB_Consulta.Checked)
				{ 
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(5, "TipoDes= \"" + "CONSULTA" + "\"");
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(6, "Peticionario= \"" + HTC120F1.DefInstance.CBB_peticionario.Text.Trim() + "\"");
				}
				else if (HTC120F1.DefInstance.RB_Entidad.Checked)
				{ 
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(5, "TipoDes= \"" + "ENTIDAD EXTERNA" + "\"");
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(6, "Peticionario= \"" + HTC120F1.DefInstance.CBB_unidad.Text.Trim() + "\"");
				}
				else if (HTC120F1.DefInstance.Rb_Servicios.Checked)
				{ 
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(5, "TipoDes= \"" + "SERVICIOS MEDICOS" + "\"");
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(6, "Peticionario= \"" + HTC120F1.DefInstance.CBB_peticionario.Text.Trim() + "\"");
				}
				if (HTC120F1.DefInstance.CBB_unidad.Text != "")
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(7, "Destino= \"" + HTC120F1.DefInstance.CBB_unidad.Text.Trim() + "\"");
				}
				else
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(7, "Destino= \"" + HTC120F1.DefInstance.CBB_peticionario.Text.Trim() + "\"");
				}
				if (HTC120F1.DefInstance.CBB_Responsable.Text != "")
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(8, "Responsable= \"" + ObtenerResponsable(HTC120F1.DefInstance.CBB_Responsable.GetItemData(HTC120F1.DefInstance.CBB_Responsable.SelectedIndex)) + "\"");
				}
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(9, "Paciente= \"" + HTC120F1.DefInstance.LB_nombre.Text.Trim() + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(10, "Historia= \"" + HTC120F1.DefInstance.LB_hist.Text.Trim() + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(11, "FPeticion= \"" + HTC120F1.DefInstance.SDC_peticion.Text.Trim() + "\"");
				if (HTC120F1.DefInstance.SDC_devolucion.Text != "")
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(12, "FPrevDevol= \"" + HTC120F1.DefInstance.SDC_devolucion.Text.Trim() + "\"");
				}
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(13, "FPrestamo= \"" + FPrestamo.Trim() + "\"");

				HTC120F1.DefInstance.CrystalReport1.Destination = Crystal.DestinationConstants.crptToPrinter;
				HTC120F1.DefInstance.CrystalReport1.CopiesToPrinter = (short) Cuadro_Diag_imprePrint.PrinterSettings.Copies;
				HTC120F1.DefInstance.CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
				HTC120F1.DefInstance.CrystalReport1.WindowShowPrintSetupBtn = true;
				HTC120F1.DefInstance.CrystalReport1.Action = 1;
				HTC120F1.DefInstance.CrystalReport1.PrinterStopPage = HTC120F1.DefInstance.CrystalReport1.PageCount();
				HTC120F1.DefInstance.CrystalReport1.Reset();
			}
			catch (Exception e)
			{

				HTC120F1.DefInstance.CrystalReport1.Reset();
				//cancela la impresi�n de etiquetas
				if (Information.Err().Number == 32755)
				{
					return;
				}
			}*/
            //DGMORENOG_TODO_X_5_IMPLEMENTA CRYSTAL REPORT - FIN
        }

        internal static string ObtenerResponsable(int lCodigo)
		{

			string result = String.Empty;
			string StComando = "Select DNOMPERS,DAP1PERS,DAP2PERS " + "from DPERSONA " + "where GPERSONA=" + lCodigo.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
			DataSet RsResponsable = new DataSet();
			tempAdapter.Fill(RsResponsable);

			if (!Convert.IsDBNull(RsResponsable.Tables[0].Rows[0]["DNOMPERS"]))
			{
				result = Convert.ToString(RsResponsable.Tables[0].Rows[0]["DNOMPERS"]).Trim();
			}

			if (!Convert.IsDBNull(RsResponsable.Tables[0].Rows[0]["DAP1PERS"]))
			{
				result = result.Trim() + " " + Convert.ToString(RsResponsable.Tables[0].Rows[0]["DAP1PERS"]).Trim();
			}

			if (!Convert.IsDBNull(RsResponsable.Tables[0].Rows[0]["DAP2PERS"]))
			{
				result = result.Trim() + " " + Convert.ToString(RsResponsable.Tables[0].Rows[0]["DAP2PERS"]).Trim();
			}
			RsResponsable.Close();
			return result;
		}
		private static void InformeSolicitud()
		{
			string StContenido = String.Empty;

            /*//DGMORENOG_TODO_X_5_IMPLEMENTA CRYSTAL REPORT - INICIO
            PathString = Path.GetDirectoryName(Application.ExecutablePath);
			try
			{
				//Saca la pantalla para elegir la impresora y el n� de copias
				Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
				//Call proDialogo_impre(HTC120F1.CrystalReport1)

				// Llamada al CRYSTAL para sacar el informe, CONEXION

				//HTC120F1.CrystalReport1.ReportFileName = "" & PathString & "\rpt\HTC458R2.rpt"
				HTC120F1.DefInstance.CrystalReport1.ReportFileName = vStPathReport + "\\HTC458R2.rpt";
				HTC120F1.DefInstance.CrystalReport1.WindowTitle = "Resguardo de Solicitudes de Historias Pendientes";


				proCabCrystal();
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(0, "Empresa= \"" + VstrCrystal.VstGrupoHo + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(1, "Hospital= \"" + VstrCrystal.VstNombreHo + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(2, "Direccion= \"" + VstrCrystal.VstDireccHo + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(3, "Servicio= \"" + HTC120F1.DefInstance.LB_Servicio.Text.Trim() + "\"");
				if (HTC120F1.DefInstance.RB_Historia.Checked)
				{
					StContenido = "H";
				}
				else if (HTC120F1.DefInstance.RB_Placas.Checked)
				{ 
					StContenido = "P";
				}
				else
				{
					StContenido = "H/P";
				}
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(4, "Contenido= \"" + StContenido.Trim() + "\"");
				if (HTC120F1.DefInstance.RB_Enfermeria.Checked)
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(5, "TipoDes= \"" + "UNIDAD DE ENFERMERIA" + "\"");
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(6, "Peticionario= \"" + HTC120F1.DefInstance.CBB_unidad.Text.Trim() + "\"");
				}
				else if (HTC120F1.DefInstance.RB_Consulta.Checked)
				{ 
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(5, "TipoDes= \"" + "CONSULTA" + "\"");
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(6, "Peticionario= \"" + HTC120F1.DefInstance.CBB_peticionario.Text.Trim() + "\"");
				}
				else if (HTC120F1.DefInstance.RB_Entidad.Checked)
				{ 
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(5, "TipoDes= \"" + "ENTIDAD EXTERNA" + "\"");
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(6, "Peticionario= \"" + HTC120F1.DefInstance.CBB_unidad.Text.Trim() + "\"");
				}
				else if (HTC120F1.DefInstance.Rb_Servicios.Checked)
				{ 
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(5, "TipoDes= \"" + "SERVICIOS MEDICOS" + "\"");
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(6, "Peticionario= \"" + HTC120F1.DefInstance.CBB_peticionario.Text.Trim() + "\"");
				}
				if (HTC120F1.DefInstance.CBB_unidad.Text != "")
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(7, "Destino= \"" + HTC120F1.DefInstance.CBB_unidad.Text.Trim() + "\"");
				}
				else
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(7, "Destino= \"" + HTC120F1.DefInstance.CBB_peticionario.Text.Trim() + "\"");
				}
				if (HTC120F1.DefInstance.CBB_Responsable.Text != "")
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(8, "Responsable= \"" + ObtenerResponsable(HTC120F1.DefInstance.CBB_Responsable.GetItemData(HTC120F1.DefInstance.CBB_Responsable.SelectedIndex)) + "\"");
				}
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(9, "Paciente= \"" + HTC120F1.DefInstance.LB_nombre.Text.Trim() + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(10, "Historia= \"" + HTC120F1.DefInstance.LB_hist.Text.Trim() + "\"");
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(11, "FPeticio= \"" + HTC120F1.DefInstance.SDC_peticion.Text.Trim() + "\"");
				if (HTC120F1.DefInstance.SDC_devolucion.Text != "")
				{
					HTC120F1.DefInstance.CrystalReport1.set_Formulas(12, "FPrevDevol= \"" + HTC120F1.DefInstance.SDC_devolucion.Text.Trim() + "\"");
				}
				HTC120F1.DefInstance.CrystalReport1.set_Formulas(13, "FPrevEntreg= \"" + HTC120F1.DefInstance.SDC_entrega.Text.Trim() + "\"");


				HTC120F1.DefInstance.CrystalReport1.Destination = Crystal.DestinationConstants.crptToPrinter;
				HTC120F1.DefInstance.CrystalReport1.CopiesToPrinter = (short) Cuadro_Diag_imprePrint.PrinterSettings.Copies;
				HTC120F1.DefInstance.CrystalReport1.WindowState = Crystal.WindowStateConstants.crptMaximized;
				HTC120F1.DefInstance.CrystalReport1.WindowShowPrintSetupBtn = true;
				HTC120F1.DefInstance.CrystalReport1.Action = 1;
				HTC120F1.DefInstance.CrystalReport1.PrinterStopPage = HTC120F1.DefInstance.CrystalReport1.PageCount();
				HTC120F1.DefInstance.CrystalReport1.Reset();
			}
			catch (Exception e)
			{

				HTC120F1.DefInstance.CrystalReport1.Reset();
				//cancela la impresi�n de etiquetas
				if (Information.Err().Number == 32755)
				{
					return;
				}
			}
            //DGMORENOG_TODO_X_5_IMPLEMENTA CRYSTAL REPORT - FIN*/

		}

		internal static void ObtenerServicioUsuario()
		{
			object IServicio = null;
			string stServicio = String.Empty;
			string vUsuario = String.Empty;
			string comando = "Select B.DSERARCH,B.GSERARCH from SUSUARIO A,HSERARCH B ";
			comando = comando + "where A.GSERARCH=B.GSERARCH ";
			comando = comando + " and A.GUSUARIO=" + "'" + vUsuario.Trim() + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(comando, RefRc);
			Rs = new DataSet();
			tempAdapter.Fill(Rs);
			if (Rs.Tables[0].Rows.Count != 0)
			{
				stServicio = Convert.ToString(Rs.Tables[0].Rows[0][0]).Trim();
				IServicio = Rs.Tables[0].Rows[0][1];
			}
			Rs.Close();
		}

		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************


			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RefRc);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			VstrCrystal.VstGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, RefRc);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			VstrCrystal.VstNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, RefRc);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			VstrCrystal.VstDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tRrCrystal.Close();
			VstrCrystal.VfCabecera = true;
		}


		private static void LlamarPrestar(string TipoU, ref double LNHistoria, ref string StContenido, ref string StFechaPeticion, ref int ICodServicioArchivo, ref object VarFechaDevol_optional, ref object VarSerSoli, ref string VarUnidad, ref string Observaciones)
		{
			string VarFechaDevol = (VarFechaDevol_optional == Type.Missing) ? String.Empty : VarFechaDevol_optional as string;
			try
			{

				FechaPeticion = StFechaPeticion;
				HTC120F2.DefInstance.ShowDialog();

                //DGMORENOG_TODO_5_5 - PENDIENTE IMPLEMENTACION DLL
				//ClassPrestamo = new Prestar.ClassHTC421F2();

				object tempRefParam = HTC120F1.DefInstance;
				ClassPrestamo.ReferenciasClass(ref ClassPrestamo, ref tempRefParam, ref RefRc);
				switch(TipoU.Trim())
				{
					case "S" : 
						if (VarFechaDevol_optional != Type.Missing && VarFechaDevol_optional.ToString().Length > 0)
						{
							string tempRefParam2 = "S";
							object tempRefParam3 = LNHistoria;
							object tempRefParam4 = VarFechaDevol;
							object tempRefParam5 = null;
							short tempRefParam6 = (short) ICodServicioArchivo;
							ClassPrestamo.Recibir(ref tempRefParam2, ref tempRefParam3, ref StContenido, ref StFechaPeticion, ref tempRefParam6, ref FPrestamo, ref tempRefParam4, ref VarSerSoli, ref tempRefParam5, ref Observaciones);
							ICodServicioArchivo = tempRefParam6;
							VarFechaDevol = Convert.ToString(tempRefParam4);
							LNHistoria = Convert.ToDouble(tempRefParam3);
						}
						else
						{
							string tempRefParam7 = "S";
							object tempRefParam8 = LNHistoria;
							object tempRefParam9 = null;
							object tempRefParam10 = null;
							short tempRefParam11 = (short) ICodServicioArchivo;
							ClassPrestamo.Recibir(ref tempRefParam7, ref tempRefParam8, ref StContenido, ref StFechaPeticion, ref tempRefParam11, ref FPrestamo, ref tempRefParam9, ref VarSerSoli, ref tempRefParam10, ref Observaciones);
							ICodServicioArchivo = tempRefParam11;
							LNHistoria = Convert.ToDouble(tempRefParam8);
						} 
						break;
					case "U" : 
						if (VarFechaDevol_optional != Type.Missing && VarFechaDevol_optional.ToString().Length > 0)
						{
							string tempRefParam12 = "U";
							object tempRefParam13 = LNHistoria;
							object tempRefParam14 = VarFechaDevol;
							object tempRefParam15 = null;
							object tempRefParam16 = VarUnidad;
							short tempRefParam17 = (short) ICodServicioArchivo;
							ClassPrestamo.Recibir(ref tempRefParam12, ref tempRefParam13, ref StContenido, ref StFechaPeticion, ref tempRefParam17, ref FPrestamo, ref tempRefParam14, ref tempRefParam15, ref tempRefParam16, ref Observaciones);
							ICodServicioArchivo = tempRefParam17;
							VarUnidad = Convert.ToString(tempRefParam16);
							VarFechaDevol = Convert.ToString(tempRefParam14);
							LNHistoria = Convert.ToDouble(tempRefParam13);
						}
						else
						{
							string tempRefParam18 = "U";
							object tempRefParam19 = LNHistoria;
							object tempRefParam20 = null;
							object tempRefParam21 = null;
							object tempRefParam22 = VarUnidad;
							short tempRefParam23 = (short) ICodServicioArchivo;
							ClassPrestamo.Recibir(ref tempRefParam18, ref tempRefParam19, ref StContenido, ref StFechaPeticion, ref tempRefParam23, ref FPrestamo, ref tempRefParam20, ref tempRefParam21, ref tempRefParam22, ref Observaciones);
							ICodServicioArchivo = tempRefParam23;
							VarUnidad = Convert.ToString(tempRefParam22);
							LNHistoria = Convert.ToDouble(tempRefParam19);
						} 
						break;
					case "C" : 
						if (VarFechaDevol_optional != Type.Missing && VarFechaDevol_optional.ToString().Length > 0)
						{
							string tempRefParam24 = "C";
							object tempRefParam25 = LNHistoria;
							object tempRefParam26 = VarFechaDevol;
							object tempRefParam27 = VarUnidad;
							short tempRefParam28 = (short) ICodServicioArchivo;
							ClassPrestamo.Recibir(ref tempRefParam24, ref tempRefParam25, ref StContenido, ref StFechaPeticion, ref tempRefParam28, ref FPrestamo, ref tempRefParam26, ref VarSerSoli, ref tempRefParam27, ref Observaciones);
							ICodServicioArchivo = tempRefParam28;
							VarUnidad = Convert.ToString(tempRefParam27);
							VarFechaDevol = Convert.ToString(tempRefParam26);
							LNHistoria = Convert.ToDouble(tempRefParam25);
						}
						else
						{
							string tempRefParam29 = "C";
							object tempRefParam30 = LNHistoria;
							object tempRefParam31 = null;
							object tempRefParam32 = VarUnidad;
							short tempRefParam33 = (short) ICodServicioArchivo;
							ClassPrestamo.Recibir(ref tempRefParam29, ref tempRefParam30, ref StContenido, ref StFechaPeticion, ref tempRefParam33, ref FPrestamo, ref tempRefParam31, ref VarSerSoli, ref tempRefParam32, ref Observaciones);
							ICodServicioArchivo = tempRefParam33;
							VarUnidad = Convert.ToString(tempRefParam32);
							LNHistoria = Convert.ToDouble(tempRefParam30);
						} 
						break;
					case "E" : 
						if (VarFechaDevol_optional != Type.Missing && VarFechaDevol_optional.ToString().Length > 0)
						{
							string tempRefParam34 = "E";
							object tempRefParam35 = LNHistoria;
							object tempRefParam36 = VarFechaDevol;
							object tempRefParam37 = null;
							object tempRefParam38 = VarUnidad;
							short tempRefParam39 = (short) ICodServicioArchivo;
							ClassPrestamo.Recibir(ref tempRefParam34, ref tempRefParam35, ref StContenido, ref StFechaPeticion, ref tempRefParam39, ref FPrestamo, ref tempRefParam36, ref tempRefParam37, ref tempRefParam38, ref Observaciones);
							ICodServicioArchivo = tempRefParam39;
							VarUnidad = Convert.ToString(tempRefParam38);
							VarFechaDevol = Convert.ToString(tempRefParam36);
							LNHistoria = Convert.ToDouble(tempRefParam35);
						}
						else
						{
							string tempRefParam40 = "E";
							object tempRefParam41 = LNHistoria;
							object tempRefParam42 = null;
							object tempRefParam43 = null;
							object tempRefParam44 = VarUnidad;
							short tempRefParam45 = (short) ICodServicioArchivo;
							ClassPrestamo.Recibir(ref tempRefParam40, ref tempRefParam41, ref StContenido, ref StFechaPeticion, ref tempRefParam45, ref FPrestamo, ref tempRefParam42, ref tempRefParam43, ref tempRefParam44, ref Observaciones);
							ICodServicioArchivo = tempRefParam45;
							VarUnidad = Convert.ToString(tempRefParam44);
							LNHistoria = Convert.ToDouble(tempRefParam41);
						} 
						break;
				}
				ClassPrestamo = null;
			}
			finally
			{
				VarFechaDevol_optional = VarFechaDevol;
			}

		}

		private static void LlamarPrestar(string TipoU, ref double LNHistoria, ref string StContenido, ref string StFechaPeticion, ref int ICodServicioArchivo, ref object VarFechaDevol_optional, ref object VarSerSoli, ref string VarUnidad)
		{
			string tempRefParam12 = String.Empty;
			LlamarPrestar(TipoU, ref LNHistoria, ref StContenido, ref StFechaPeticion, ref ICodServicioArchivo, ref VarFechaDevol_optional, ref VarSerSoli, ref VarUnidad, ref tempRefParam12);
		}

		private static void LlamarPrestar(string TipoU, ref double LNHistoria, ref string StContenido, ref string StFechaPeticion, ref int ICodServicioArchivo, ref object VarFechaDevol_optional, ref object VarSerSoli)
		{
			string tempRefParam13 = String.Empty;
			string tempRefParam14 = String.Empty;
			LlamarPrestar(TipoU, ref LNHistoria, ref StContenido, ref StFechaPeticion, ref ICodServicioArchivo, ref VarFechaDevol_optional, ref VarSerSoli, ref tempRefParam13, ref tempRefParam14);
		}

		private static void LlamarPrestar(string TipoU, ref double LNHistoria, ref string StContenido, ref string StFechaPeticion, ref int ICodServicioArchivo, ref object VarFechaDevol_optional)
		{
			object tempRefParam15 = null;
			string tempRefParam16 = String.Empty;
			string tempRefParam17 = String.Empty;
			LlamarPrestar(TipoU, ref LNHistoria, ref StContenido, ref StFechaPeticion, ref ICodServicioArchivo, ref VarFechaDevol_optional, ref tempRefParam15, ref tempRefParam16, ref tempRefParam17);
		}

		private static void LlamarPrestar(string TipoU, ref double LNHistoria, ref string StContenido, ref string StFechaPeticion, ref int ICodServicioArchivo)
		{
			object tempRefParam18 = Type.Missing;
			object tempRefParam19 = null;
			string tempRefParam20 = String.Empty;
			string tempRefParam21 = String.Empty;
			LlamarPrestar(TipoU, ref LNHistoria, ref StContenido, ref StFechaPeticion, ref ICodServicioArchivo, ref tempRefParam18, ref tempRefParam19, ref tempRefParam20, ref tempRefParam21);
		}

		internal static string ObtenerCodigo(string StDescripcion, string StCodigoTabla, string StDescTabla, string stTabla)
		{
			string result = String.Empty;
			StComando = "Select " + StCodigoTabla + " from " + stTabla;
			StComando = StComando + " where " + StDescTabla + "=";
			StComando = StComando + "'" + StDescripcion.Trim() + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
			Rs = new DataSet();
			tempAdapter.Fill(Rs);
			if (Rs.Tables[0].Rows.Count == 1)
			{
				result = Convert.ToString(Rs.Tables[0].Rows[0][0]).Trim();
			}
			Rs.Close();
			return result;
		}


		internal static int ObtenerDias(string VarCodigo)
		{
			int result = 0;
			StComando = "Select NDIASPREST from DMOTPETH where GMOTPETH=";
			StComando = StComando + "'" + VarCodigo.Trim() + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RefRc);
			Rs = new DataSet();
			tempAdapter.Fill(Rs);
			if (Rs.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToInt32(Rs.Tables[0].Rows[0][0]);
			}
			else
			{
                //result = DBNull.Value;
                result = 0;
            }
			Rs.Close();
			return result;
		}

		private static bool ComprobarActivoONoCumplimentada(string historia)
		{
			DialogResult res = (DialogResult) 0;

			//OSCAR C 30/06/2005
			string qmensaje = String.Empty;
			bool MostrarMensaje1 = false;
			bool MostrarMensaje2 = false;
			string sql = "SELECT ISNULL(VALFANU1,'S') valfanu1, ISNULL(VALFANU2,'S') valfanu2 " + 
			             "FROM SCONSGLO WHERE GCONSGLO='HPRUBICU'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RefRc);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			//---------

			sql = "select valfanu2, nnumeri1 from sconsglo ";
			sql = sql + "where gconsglo = 'APERTFIS'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RefRc);
			DataSet RrActivo = new DataSet();
			tempAdapter_2.Fill(RrActivo);
			ValorActivo = Convert.ToInt32(Conversion.Val(Convert.ToString(RrActivo.Tables[0].Rows[0]["valfanu2"])));
			ValorNoCumplimentada = Convert.ToInt32(Conversion.Val(Convert.ToString(RrActivo.Tables[0].Rows[0]["nnumeri1"])));
			RrActivo.Close();

			sql = "select gubicfisic, gcumplim from HDOSSIER where ghistoria = " + historia;

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, RefRc);
			RrActivo = new DataSet();
			tempAdapter_3.Fill(RrActivo);

			if (RrActivo.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(RrActivo.Tables[0].Rows[0]["gubicfisic"]))
				{
					if (Conversion.Val(Convert.ToString(RrActivo.Tables[0].Rows[0]["gubicfisic"])) != ValorActivo)
					{
						//OSCAR C 30/06/2005
						if (Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
						{
							MostrarMensaje1 = true;
							qmensaje = "La historia " + historia + " no est� en el Activo";
						}
						//----------
					}
				}

				if (!Convert.IsDBNull(RrActivo.Tables[0].Rows[0]["gcumplim"]))
				{
					if (Conversion.Val(Convert.ToString(RrActivo.Tables[0].Rows[0]["gcumplim"])) == ValorNoCumplimentada)
					{
						//OSCAR C 30/06/2005
						if (Convert.ToString(RR.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper() == "S")
						{
							MostrarMensaje2 = true;
							qmensaje = "La historia " + historia + " no est� Cumplimentada";
						}
						//-----------
					}
				}
			}

			RrActivo.Close();

			//OSCAR C 30/06/2005
			RR.Close();
			if (MostrarMensaje1 && MostrarMensaje2)
			{
				qmensaje = "La historia " + historia + " no est� en el Activo o no est� cumplimentada";
			}
			//----------------------

			if (MostrarMensaje1 || MostrarMensaje2)
			{
				short tempRefParam = 1125;
                string[] tempRefParam2 = new string[]{qmensaje, "continuar"};
				res = (DialogResult) Convert.ToInt32(ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,tempRefParam, RefRc, tempRefParam2));
				return res == System.Windows.Forms.DialogResult.Yes;
			}
			else
			{
				return true;
			}

		}
	}
}
 