using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace GraficaVal
{
	public partial class Sel_Grafica
          : Telerik.WinControls.UI.RadForm
    {

		int iErrorFecha = 0;
		string stClaveConcepto = String.Empty;
       /* Database bs = null;*/
        //UPGRADE_ISSUE: (2068) Workspaces object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx

        // pario TODO _X_5 BASE DE DATOS DE ACCESS
        /*
          Workspaces ws = null;
          //UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx

          //UPGRADE_ISSUE: (2068) Recordset object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
          Recordset Rs = null; //de la tabla de access*/
        public Sel_Grafica()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{
			if (fComprobarDatos())
			{
				this.Cursor = Cursors.WaitCursor;
				proIniciar_grafica();
				//UPGRADE_ISSUE: (2036) Form property Sel_Grafica.MousePointer does not support custom mousepointers. More Information: http://www.vbtonet.com/ewis/ewi2036.aspx
				this.Cursor = Cursors.Default;
			}
		}

		private bool fComprobarDatos()
		{

			bool result = false;
			if (!ValidarIntervaloFechas(SdcFechaDesde, sdcFechaHasta, ref MValGrafica.RcValoracion))
			{
				return false;
			}
            //-----

            if (cbbValoracion.SelectedIndex == -1)
            {
                result = false;
                short tempRefParam = 1040;
                string[] tempRefParam2 = new string [] {lbConcepto.Text};
				MValGrafica.clase_mensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam,  MValGrafica.RcValoracion,  tempRefParam2);
				return result;
			}

			return true;
		}

		
		private void Sel_Grafica_Load(Object eventSender, EventArgs eventArgs)
		{


			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
			MValGrafica.ObtenerValoresTipoBD(); //para obtener el tipo de base de datos
			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(MValGrafica.RcValoracion);
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);


            cbbValoracion.Items.Clear();

			string sql = "select dconclin.gcodconc, dconclin.dconclin " + 
			             " from dconclin where dconclin.iticocli = 'N' order by dconclin.dconclin ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MValGrafica.RcValoracion);
			DataSet RrValoracion = new DataSet();
			tempAdapter.Fill(RrValoracion);
			if (RrValoracion.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in RrValoracion.Tables[0].Rows)
				{
					cbbValoracion.Items.Add(Convert.ToString(iteration_row["dconclin"]).Trim());
					cbbValoracion.SetItemData(cbbValoracion.GetNewIndex(), Convert.ToInt32(iteration_row["gcodconc"]));
				}
			}
			if (StringsHelper.ToDoubleSafe(MValGrafica.gbCodigoConcepto) != 0)
			{
				for (int i = 0; i <= cbbValoracion.Items.Count - 1; i++)
				{
					if (cbbValoracion.Items[i].ToString() == (MValGrafica.gbCodigoConcepto))


                    {
						cbbValoracion.SelectedIndex = i;
						cbbValoracion.Enabled = false;
						break;
					}
				}
			}
		
			RrValoracion.Close();

			//(maplaza)(19/12/2006)Se ponen las fechas por defecto en el intervalo
			PonerFechasDefectoDesde( SdcFechaDesde);
			PonerFechasDefectoHasta(sdcFechaHasta);
			//-----

		}


		private void proIniciar_grafica()
		{
			object DBEngine = null;

			string stTitulo = String.Empty;
			//hay que buscar los datos para graficar
			CrearTablaTemporal();

			string sqltemporal = "select * from GRAFICA where 1=2"; //select vacia

			//UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
		// PARIO TODO _X_5 BASE DE DATOS ACCESS 
    /*	Rs = (Recordset) bs.OpenRecordset(sqltemporal);*/

			if (cbbValoracion.SelectedIndex != -1)
			{
				MValGrafica.gbCodigoConcepto = cbbValoracion.GetItemData(cbbValoracion.SelectedIndex).ToString();
			}

			//Consultas
			string sql = "select fapuntes, nvalncon , ndecimal from cconsult inner join dvaloram on";
			sql = sql + " cconsult.ganoregi = dvaloram.ganoregi And cconsult.gnumregi = dvaloram.gnumregi";
			sql = sql + " inner join DCONCLIN on DCONCLIN.gcodconc = dvaloram.gcodconc";
			sql = sql + " where cconsult.gidenpac = '" + MValGrafica.GstGidenpac + "' and dvaloram.itiposer = 'C' and";
			sql = sql + " nvalncon is not null";
			sql = sql + " and dvaloram.gcodconc = " + MValGrafica.gbCodigoConcepto + " ";
			object tempRefParam = SdcFechaDesde.Text;
			sql = sql + " and dvaloram.fapuntes >= " + Serrores.FormatFechaHMS( tempRefParam) + " ";
			object tempRefParam2 = sdcFechaHasta.Text + " 23:59:59";
			sql = sql + " and dvaloram.fapuntes <= " + Serrores.FormatFechaHMS( tempRefParam2) + " ";
			//---AEPISADM--
			sql = sql + " Union select fapuntes, nvalncon , ndecimal from";
			sql = sql + " aepisadm inner join dvaloram on aepisadm.ganoadme = dvaloram.ganoregi and aepisadm.gnumadme = dvaloram.gnumregi";
			sql = sql + " inner join DCONCLIN on DCONCLIN.gcodconc = dvaloram.gcodconc";
			sql = sql + " where aepisadm.gidenpac = '" + MValGrafica.GstGidenpac + "' and dvaloram.itiposer = 'H' and";
			sql = sql + " nvalncon is not null";
			sql = sql + " and dvaloram.gcodconc = " + MValGrafica.gbCodigoConcepto + " ";
			object tempRefParam3 = SdcFechaDesde.Text;
			sql = sql + " and dvaloram.fapuntes >= " + Serrores.FormatFechaHMS( tempRefParam3) + " ";
			object tempRefParam4 = sdcFechaHasta.Text + " 23:59:59";
			sql = sql + " and dvaloram.fapuntes <= " + Serrores.FormatFechaHMS( tempRefParam4) + " ";
			//---URGENCIAS--
			sql = sql + " Union select fapuntes, nvalncon , ndecimal from uepisurg inner join dvaloram on ";
			sql = sql + " uepisurg.ganourge = dvaloram.ganoregi and uepisurg.gnumurge = dvaloram.gnumregi";
			sql = sql + " inner join DCONCLIN on DCONCLIN.gcodconc = dvaloram.gcodconc";
			sql = sql + " Where uepisurg.gidenpac = '" + MValGrafica.GstGidenpac + "' and dvaloram.itiposer = 'U' and";
			sql = sql + " nvalncon is not null";
			sql = sql + " and dvaloram.gcodconc = " + MValGrafica.gbCodigoConcepto + " ";
			object tempRefParam5 = SdcFechaDesde.Text;
			sql = sql + " and dvaloram.fapuntes >= " + Serrores.FormatFechaHMS( tempRefParam5) + " ";
			object tempRefParam6 = sdcFechaHasta.Text + " 23:59:59";
			sql = sql + " and dvaloram.fapuntes <= " + Serrores.FormatFechaHMS( tempRefParam6) + " ";
			//---QUIROFANOS--
			sql = sql + " Union select fapuntes, nvalncon , ndecimal from ";
			sql = sql + " qintquir inner join dvaloram on qintquir.ganoregi = dvaloram.ganoregi and ";
			sql = sql + " qintquir.gnumregi = dvaloram.gnumregi inner join DCONCLIN on ";
			sql = sql + " DCONCLIN.gcodconc = dvaloram.gcodconc Where qintquir.gidenpac = '" + MValGrafica.GstGidenpac + "' and";
			sql = sql + " dvaloram.itiposer = 'Q' and nvalncon is not null ";
			sql = sql + " and dvaloram.gcodconc = " + MValGrafica.gbCodigoConcepto;
			object tempRefParam7 = SdcFechaDesde.Text;
			sql = sql + " and dvaloram.fapuntes >= " + Serrores.FormatFechaHMS( tempRefParam7) + " ";
			object tempRefParam8 = sdcFechaHasta.Text + " 23:59:59";
			sql = sql + " and dvaloram.fapuntes <= " + Serrores.FormatFechaHMS( tempRefParam8) + " ";
			//---SESIONES--
			//Jes�s (20/03/2007) Faltaba el de sesiones
			sql = sql + " Union select fapuntes, nvalncon , ndecimal from ";
			sql = sql + " csolcita inner join dvaloram on csolcita.ganoregi = dvaloram.ganoregi and ";
			sql = sql + " csolcita.gnumregi = dvaloram.gnumregi inner join DCONCLIN on ";
			sql = sql + " DCONCLIN.gcodconc = dvaloram.gcodconc Where csolcita.gidenpac = '" + MValGrafica.GstGidenpac + "' and";
			sql = sql + " dvaloram.itiposer = 'S' and nvalncon is not null ";
			sql = sql + " and dvaloram.gcodconc = " + MValGrafica.gbCodigoConcepto;
			object tempRefParam9 = SdcFechaDesde.Text;
			sql = sql + " and dvaloram.fapuntes >= " + Serrores.FormatFechaHMS( tempRefParam9) + " ";
			object tempRefParam10 = sdcFechaHasta.Text + " 23:59:59";
			sql = sql + " and dvaloram.fapuntes <= " + Serrores.FormatFechaHMS( tempRefParam10) + " ";
			//---NIDO--
			sql = sql + " Union select fapuntes, nvalncon , ndecimal from";
			sql = sql + " NEPISNEO inner join dvaloram on NEPISNEO.ganoregi = dvaloram.ganoregi and NEPISNEO.gnumregi = dvaloram.gnumregi";
			sql = sql + " inner join DCONCLIN on DCONCLIN.gcodconc = dvaloram.gcodconc";
			sql = sql + " where NEPISNEO.gidenpac = '" + MValGrafica.GstGidenpac + "' and dvaloram.itiposer = 'N' and";
			sql = sql + " nvalncon is not null";
			sql = sql + " and dvaloram.gcodconc = " + MValGrafica.gbCodigoConcepto + " ";
			object tempRefParam11 = SdcFechaDesde.Text;
			sql = sql + " and dvaloram.fapuntes >= " + Serrores.FormatFechaHMS( tempRefParam11) + " ";
			object tempRefParam12 = sdcFechaHasta.Text + " 23:59:59";
			sql = sql + " and dvaloram.fapuntes <= " + Serrores.FormatFechaHMS( tempRefParam12) + " ";

			sql = sql + " order by fapuntes ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MValGrafica.RcValoracion);
			DataSet RrSQL = new DataSet();
			tempAdapter.Fill(RrSQL);
            if (RrSQL.Tables[0].Rows.Count != 0)
            {

                //********
                //UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx

                foreach (DataRow iteration_row in RrSQL.Tables[0].Rows)
                {
                    //UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    // PARIO TODO _X_ 5 BASE DE DATOS ACCESS

                    /*    Rs.AddNew();
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(fapuntes). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    Rs["fapuntes"] = (Recordset)Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:mm");
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(nvalncon). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    //UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
                    //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                    //UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
                    string tempRefParam13 = Convert.ToString(iteration_row["nvalncon"]);
                    Rs["nvalncon"] = (Recordset)Serrores.ConvertirDecimales(ref tempRefParam13, ref MValGrafica.vstSeparadorDecimal, (Convert.IsDBNull(iteration_row["ndecimal"])) ? 2 : iteration_row["ndecimal"]);
                    //UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    Rs.Update();
                }
                //UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                Rs.Close();
                //UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                bs.Close();
                //UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                DBEngine.Workspaces(0).Close();*/

                    //se muestra el rpt con los datos en la grafica
                  
                   MValGrafica.CabecerasListado();
                    MValGrafica.LISTADO_PARTICULAR.ReportFileName = MValGrafica.vstPathAplicacion + "\\..\\ElementosComunes\\RPT\\mcv260r3.rpt";
                    //MValGrafica.LISTADO_PARTICULAR.Formulas[0] = "Grupo = \"" + MValGrafica.stGrupoHo + "\"";
                    MValGrafica.LISTADO_PARTICULAR.Formulas["Grupo"] = "\"" + MValGrafica.stGrupoHo + "\"";
                    //MValGrafica.LISTADO_PARTICULAR.Formulas[1] = "NombreH= \"" + MValGrafica.stNombreHo + "\"";
                    MValGrafica.LISTADO_PARTICULAR.Formulas["NombreH"] = "\"" + MValGrafica.stNombreHo + "\"";
                    //MValGrafica.LISTADO_PARTICULAR.Formulas[2] = "DireccionH = \"" + MValGrafica.stDireccHo + "\"";
                    MValGrafica.LISTADO_PARTICULAR.Formulas["DireccionH"] = "\"" + MValGrafica.stDireccHo + "\"";
                    stTitulo = "GR�FICA DE " + cbbValoracion.Text.Trim().ToUpper();
                    //MValGrafica.LISTADO_PARTICULAR.Formulas[3] = "Titulo = \"" + stTitulo + "\"";
                    MValGrafica.LISTADO_PARTICULAR.Formulas["Titulo"] = "\"" + stTitulo + "\"";
                    //MValGrafica.LISTADO_PARTICULAR.Formulas[4] = "NInforme = \"" + "MCV260R3" + "\"";
                    MValGrafica.LISTADO_PARTICULAR.Formulas["NInforme"] = "\"" + "MCV260R3" + "\"";
                    //MValGrafica.LISTADO_PARTICULAR.Formulas[5] = "NPaciente= \"" + MValGrafica.vstNombrePaciente + "\"";
                    MValGrafica.LISTADO_PARTICULAR.Formulas["NPaciente"] = "\"" + MValGrafica.vstNombrePaciente + "\"";
                    //MValGrafica.LISTADO_PARTICULAR.Formulas[6] = "FDesde= \"" + DateTime.Parse(SdcFechaDesde.Text).ToString("dd/MM/yyyy") + "\"";
                    MValGrafica.LISTADO_PARTICULAR.Formulas["FDesde"] = "\"" + DateTime.Parse(SdcFechaDesde.Text).ToString("dd/MM/yyyy") + "\"";
                    //MValGrafica.LISTADO_PARTICULAR.Formulas[7] = "FHasta= \"" + DateTime.Parse(sdcFechaHasta.Text).ToString("dd/MM/yyyy") + "\"";
                    MValGrafica.LISTADO_PARTICULAR.Formulas["FHasta"] = "\"" + DateTime.Parse(sdcFechaHasta.Text).ToString("dd/MM/yyyy") + "\"";
                    //-----

                    MValGrafica.LISTADO_PARTICULAR.Destination = 0; //crptToWindow
                                                                    
                    MValGrafica.LISTADO_PARTICULAR.DataFiles[0] = MValGrafica.vstPathAplicacion + "\\" + MValGrafica.vstNombreMDB;
                  
                    MValGrafica.LISTADO_PARTICULAR.SQLQuery = "select * from GRAFICA ";
                    MValGrafica.LISTADO_PARTICULAR.WindowState = 2; //crptMaximized
                                                                  
                    MValGrafica.LISTADO_PARTICULAR.WindowTitle = "Gr�fica de " + cbbValoracion.Text.Trim().ToLower();
                    MValGrafica.LISTADO_PARTICULAR.Action = 1;
                    MValGrafica.LISTADO_PARTICULAR.PageCount();
                    MValGrafica.LISTADO_PARTICULAR.PrinterStopPage = MValGrafica.LISTADO_PARTICULAR.PageCount;
                    //Limpiar las formulas
                    for (int tiForm = 0; tiForm <= 8; tiForm++)
                    {
                       
                        MValGrafica.LISTADO_PARTICULAR.Formulas[tiForm] = "";
                      
                        MValGrafica.LISTADO_PARTICULAR.GraphOptions[tiForm] = "";
                    }
                
                    MValGrafica.LISTADO_PARTICULAR.Reset();

                }
            }
            else
            {



                //UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                // PARIO TODO _X_ 5 BASE DE DATOS ACCESS

               /* Rs.Close();
                //UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                bs.Close();
                //UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                DBEngine.Workspaces(0).Close();*/
                short tempRefParam14 = 1520;
                string [] tempRefParam15 = new string[]{cbbValoracion.Text.ToLower(), "la gr�fica"};
				MValGrafica.clase_mensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam14,  MValGrafica.RcValoracion,  tempRefParam15);
			}

			
			RrSQL.Close();
			return;

		
			RrSQL.Close();
		
			Serrores.GestionErrorUpdate(Information.Err().Number, MValGrafica.RcValoracion);
			//se vacian las formulas

            
			for (int tiForm = 0; tiForm <= 8; tiForm++)
			{
				
				MValGrafica.LISTADO_PARTICULAR.Formulas[tiForm] = "";
			}
			
			MValGrafica.LISTADO_PARTICULAR.Reset();
		}


		private void CrearTablaTemporal()
		{
            //PARIO TODO_X_5 BASE DE DATOS ACCESS
         /*   object dbLangSpanish = null;
			object DBEngine = null;
			//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			string vstPathTemp = String.Empty;

			try
			{

				//Se crea una base de datos temporal para realizar el informe
				vstPathTemp = MValGrafica.vstPathAplicacion + "\\" + MValGrafica.vstNombreMDB;
				if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
				{
					//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					bs = (Database) DBEngine.Workspaces(0).OpenDatabase(vstPathTemp);
					//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					foreach (TableDef tabla in bs.TableDefs)
					{
						//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						if (Convert.ToString(tabla.Name) == "GRAFICA")
						{
							//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							bs.Execute("DROP TABLE GRAFICA");
						}
					}
				}
				else
				{
                    //UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                  

                    bs = (Database) DBEngine.Workspaces(0).CreateDatabase(vstPathTemp, dbLangSpanish);
				}

				//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				bs.Execute("CREATE TABLE GRAFICA (fapuntes text, nvalncon double); ");
			}
			catch
			{

				MessageBox.Show(" Error al cargar la tabla temporal", Application.ProductName);
			}*/

		}

		//(maplaza)(19/12/2006)Los m�todos y funciones que aparecen a continuaci�n tienen que ver con la sustituci�n de la
		//ocx del Intervalo de Fechas por controles que hagan una funci�n equivalente

		//(maplaza)(19/12/2006)Se pone la fecha por defecto para la fecha de inicio del intervalo de fechas
		private void PonerFechasDefectoDesde(RadDateTimePicker sdcFechaInicio)
		{
			sdcFechaInicio.Text = DateTime.Now.ToString("dd/MM/yyyy");
		}

		//(maplaza)(19/12/2006)Se pone la fecha por defecto para la fecha de fin del intervalo de fechas
		private void PonerFechasDefectoHasta(RadDateTimePicker sdcFechaFin)
		{ 
			sdcFechaFin.Text = DateTime.Now.ToString("dd/MM/yyyy");
		}

		//(maplaza)(19/12/2006)Esta funci�n devuelve TRUE si se cumplen todas las validaciones correspondientes para
		//el intervalo de las fechas, y devuelve FALSE si no se cumplen todas las validaciones correspondientes para
		//el intervalo de las fechas. Los par�metros que se pasan son los controles correspondientes a los intervalos
		//de las fechas, y la conexi�n.
		private bool ValidarIntervaloFechas(RadDateTimePicker sdcFechaInicio, RadDateTimePicker sdcFechaFin, ref SqlConnection rcConexion)
		{

			bool result = false;
			result = true;

			if (sdcFechaInicio.Text.Trim() == "" || sdcFechaFin.Text.Trim() == "")
			{
				result = false;
				short tempRefParam = 1040;
				string [] tempRefParam2 = new string []{fraIntervaloFechas.Text};
				MValGrafica.clase_mensaje.RespuestaMensaje( Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam,  rcConexion,  tempRefParam2);
				return result;
			}

			//la fecha de inicio no debe ser mayor que la de fin
			if (sdcFechaInicio.Value.Date > sdcFechaFin.Value.Date)
			{
				result = false;
				short tempRefParam3 = 1020;
				string [] tempRefParam4 = new string  []{"Fecha desde", "<=", "Fecha hasta"};
				MValGrafica.clase_mensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam3,  rcConexion,  tempRefParam4);
				if (sdcFechaInicio.Enabled)
				{
					sdcFechaInicio.Focus();
				}
				return result;
			}

            //la fecha de inicio no debe ser superior a la del sistema
            if (sdcFechaInicio.Value.Date  > DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy")))
            {
                result = false;
                short tempRefParam5 = 1020;
                string[] tempRefParam6 = new string[] {"Fecha desde", "<=", DateTime.Now.ToString("dd/MM/yyyy")};
				MValGrafica.clase_mensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda, tempRefParam5,rcConexion, tempRefParam6);
				if (sdcFechaInicio.Enabled)
				{
					sdcFechaInicio.Focus();
				}
				return result;
			}

			//La fecha de fin no debe ser superior a la fecha del sistema
			if (sdcFechaFin.Value.Date > DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy")))
			{
				result = false;
				short tempRefParam7 = 1020;
				string [] tempRefParam8 = new string [] {"Fecha hasta", "<=", DateTime.Now.ToString("dd/MM/yyyy")};
				MValGrafica.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion,  Serrores.vAyuda, tempRefParam7,  rcConexion,  tempRefParam8);
				if (sdcFechaFin.Enabled)
				{
					sdcFechaFin.Focus();
				}
				return result;
			}

			return result;
		}
		private void Sel_Grafica_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}