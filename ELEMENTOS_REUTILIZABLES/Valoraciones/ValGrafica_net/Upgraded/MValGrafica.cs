using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace GraficaVal
{
	internal static class MValGrafica
	{

		public static SqlConnection RcValoracion = null;
		public static Mensajes.ClassMensajes clase_mensaje = null; //para la rutina de mensajes
		public static string gbCodigoConcepto = String.Empty;
		public static string stTablaValoracion = String.Empty;
		public static string vstSeparadorDecimal = String.Empty;
		public static string stDecimalBD = String.Empty;
		public static string vstPathAplicacion = String.Empty;
		public static dynamic LISTADO_PARTICULAR = null;
		//cabeceras del listado
		public static string stGrupoHo = String.Empty;
		public static string stNombreHo = String.Empty;
		public static string stDireccHo = String.Empty;

		public static string vstNombreMDB = String.Empty;
		public static string GstGidenpac = String.Empty;
		public static string vstNombrePaciente = String.Empty; //nombre y apellidos del paciente

		internal static void ObtenerValoresTipoBD()
		{
			string Formato_Fecha_SQL = String.Empty;
			string Separador_Fecha_SQL = String.Empty;
			string Separador_Hora_SQL = String.Empty;
			string formato = String.Empty;
			int i = 0;
			string tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, RcValoracion);
			DataSet tRr = new DataSet();
			tempAdapter.Fill(tRr);
		
			switch(Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
			{
				case "SQL SERVER 6.5" : 
				
					Formato_Fecha_SQL = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();  //MM/DD/YYYY HH:MI 
				
					formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
					for (i = 1; i <= formato.Length; i++)
					{
						if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && Separador_Fecha_SQL == "")
						{
							Separador_Fecha_SQL = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
						}
						if (Separador_Fecha_SQL != "")
						{
							break;
						}
					} 
					formato = formato.Substring(Strings.InStr(i + 1, formato, Separador_Fecha_SQL, CompareMethod.Binary)); 
					for (i = 1; i <= formato.Length; i++)
					{
						if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && Separador_Hora_SQL == "")
						{
							Separador_Hora_SQL = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
						}
						if (Separador_Hora_SQL != "")
						{
							break;
						}
					} 
					break;
			}
		
			tRr.Close();
		}
		internal static void CabecerasListado()
		{
			string stNumHistoria = String.Empty;
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************

			//buscarDatos de hospital
			string sql = "select valfanu1 from sconsglo where gconsglo = 'GrupoHo'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
			DataSet RrSQL = new DataSet();
			tempAdapter.Fill(RrSQL);
			
			stGrupoHo = Convert.ToString(RrSQL.Tables[0].Rows[0]["valfanu1"]).Trim();

			sql = "select valfanu1 from sconsglo where gconsglo = 'NOMBREHO'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RcValoracion);
			RrSQL = new DataSet();
			tempAdapter_2.Fill(RrSQL);
			
			stNombreHo = Convert.ToString(RrSQL.Tables[0].Rows[0]["valfanu1"]).Trim();

			sql = "select valfanu1 from sconsglo where gconsglo = 'direccho'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, RcValoracion);
			RrSQL = new DataSet();
			tempAdapter_3.Fill(RrSQL);
		

			//numero de historia
			sql = "Select hdossier.ghistoria from hdossier where hdossier.gidenpac = '" + GstGidenpac + "' and hdossier.icontenido = 'H'";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, RcValoracion);
			RrSQL = new DataSet();
			tempAdapter_4.Fill(RrSQL);
			if (RrSQL.Tables[0].Rows.Count != 0)
			{
				
				stNumHistoria = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["ghistoria"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["ghistoria"]).Trim();
			}
			else
			{
				stNumHistoria = "";
			}
			
			RrSQL.Close();
		}
		internal static string ObTenerNombrePaciente(string stGidenpac)
		{
			string result = String.Empty;
			string stCadena = String.Empty;

			string sql = "Select * from DPACIENT where gidenpac = '" + stGidenpac + "' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
			DataSet RrSQL = new DataSet();
			tempAdapter.Fill(RrSQL);
			if (RrSQL.Tables[0].Rows.Count != 0)
			{
				
				stCadena = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["dnombpac"]).Trim();
			
				stCadena = stCadena + " " + ((Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dape1pac"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["dape1pac"]).Trim());
				
				stCadena = stCadena + " " + ((Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dape2pac"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["dape2pac"]).Trim());
				result = stCadena;
			}
			
			RrSQL.Close();
			return result;
		}
	}
}