using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace GraficaVal
{
	public class MCValGrafica
	{
		public void Llamada(SqlConnection iConnet, string PathAplicacion, object ObjetoCristal, string stNombreMDB, string stGidenpac, object CodigoConcepto_optional)
		{
			string CodigoConcepto = (CodigoConcepto_optional == Type.Missing) ? String.Empty : CodigoConcepto_optional as string;

			MValGrafica.RcValoracion = iConnet;
			Serrores.AjustarRelojCliente(MValGrafica.RcValoracion);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref MValGrafica.RcValoracion);
			MValGrafica.vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(MValGrafica.RcValoracion);
			if (CodigoConcepto_optional == Type.Missing)
			{
				MValGrafica.gbCodigoConcepto = "0";
			}
			else
			{
				MValGrafica.gbCodigoConcepto = CodigoConcepto;
			}
			MValGrafica.stTablaValoracion = "DVALORAM";
			MValGrafica.vstPathAplicacion = PathAplicacion;
			MValGrafica.LISTADO_PARTICULAR = ObjetoCristal;
			MValGrafica.vstNombreMDB = stNombreMDB;
			MValGrafica.GstGidenpac = stGidenpac;
			MValGrafica.vstNombrePaciente = MValGrafica.ObTenerNombrePaciente(stGidenpac);
			Sel_Grafica.DefInstance.ShowDialog();



		}

		public void Llamada(SqlConnection iConnet, string PathAplicacion, object ObjetoCristal, string stNombreMDB, string stGidenpac)
		{
			Llamada(iConnet, PathAplicacion, ObjetoCristal, stNombreMDB, stGidenpac, Type.Missing);
		}
		public MCValGrafica()
		{
			try
			{
				MValGrafica.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{
				MessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}
		}

		~MCValGrafica()
		{
			MValGrafica.clase_mensaje = null;
		}
	}
}