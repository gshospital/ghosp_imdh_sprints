using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace GraficaVal
{
	partial class Sel_Grafica
	{

		#region "Upgrade Support "
		private static Sel_Grafica m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Sel_Grafica DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Sel_Grafica();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sdcFechaHasta", "SdcFechaDesde", "lblFechaDesde", "lblFechaHasta", "fraIntervaloFechas", "cbbValoracion", "cbCancelar", "cbPantalla", "lbConcepto"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechaHasta;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFechaDesde;
		public Telerik.WinControls.UI.RadLabel lblFechaDesde;
		public Telerik.WinControls.UI.RadLabel lblFechaHasta;
		public Telerik.WinControls.UI.RadGroupBox fraIntervaloFechas;
		public UpgradeHelpers.MSForms.MSCombobox cbbValoracion;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbPantalla;
		public Telerik.WinControls.UI.RadLabel lbConcepto;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.fraIntervaloFechas = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcFechaHasta = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SdcFechaDesde = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblFechaDesde = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaHasta = new Telerik.WinControls.UI.RadLabel();
            this.cbbValoracion = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbPantalla = new Telerik.WinControls.UI.RadButton();
            this.lbConcepto = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.fraIntervaloFechas)).BeginInit();
            this.fraIntervaloFechas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbValoracion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // fraIntervaloFechas
            // 
            this.fraIntervaloFechas.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.fraIntervaloFechas.Controls.Add(this.sdcFechaHasta);
            this.fraIntervaloFechas.Controls.Add(this.SdcFechaDesde);
            this.fraIntervaloFechas.Controls.Add(this.lblFechaDesde);
            this.fraIntervaloFechas.Controls.Add(this.lblFechaHasta);
            this.fraIntervaloFechas.HeaderText = "";
            this.fraIntervaloFechas.Location = new System.Drawing.Point(4, 4);
            this.fraIntervaloFechas.Name = "fraIntervaloFechas";
            this.fraIntervaloFechas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fraIntervaloFechas.Size = new System.Drawing.Size(325, 43);
            this.fraIntervaloFechas.TabIndex = 6;
            // 
            // sdcFechaHasta
            // 
            this.sdcFechaHasta.CustomFormat = "dd/MM/yyyy";
            this.sdcFechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechaHasta.Location = new System.Drawing.Point(214, 16);
            this.sdcFechaHasta.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.sdcFechaHasta.Name = "sdcFechaHasta";
            this.sdcFechaHasta.Size = new System.Drawing.Size(105, 20);
            this.sdcFechaHasta.TabIndex = 1;
            this.sdcFechaHasta.TabStop = false;
            this.sdcFechaHasta.Text = "17/05/2016";
            this.sdcFechaHasta.Value = new System.DateTime(2016, 5, 17, 0, 0, 0, 0);
            // 
            // SdcFechaDesde
            // 
            this.SdcFechaDesde.CustomFormat = "dd/MM/yyyy";
            this.SdcFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaDesde.Location = new System.Drawing.Point(54, 16);
            this.SdcFechaDesde.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.SdcFechaDesde.Name = "SdcFechaDesde";
            this.SdcFechaDesde.Size = new System.Drawing.Size(105, 20);
            this.SdcFechaDesde.TabIndex = 0;
            this.SdcFechaDesde.TabStop = false;
            this.SdcFechaDesde.Text = "17/05/2016";
            this.SdcFechaDesde.Value = new System.DateTime(2016, 5, 17, 0, 0, 0, 0);
            // 
            // lblFechaDesde
            // 
            this.lblFechaDesde.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblFechaDesde.Location = new System.Drawing.Point(8, 17);
            this.lblFechaDesde.Name = "lblFechaDesde";
            this.lblFechaDesde.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFechaDesde.Size = new System.Drawing.Size(40, 18);
            this.lblFechaDesde.TabIndex = 8;
            this.lblFechaDesde.Text = "Desde:";
            // 
            // lblFechaHasta
            // 
            this.lblFechaHasta.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblFechaHasta.Location = new System.Drawing.Point(171, 17);
            this.lblFechaHasta.Name = "lblFechaHasta";
            this.lblFechaHasta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFechaHasta.Size = new System.Drawing.Size(37, 18);
            this.lblFechaHasta.TabIndex = 7;
            this.lblFechaHasta.Text = "Hasta:";
            // 
            // cbbValoracion
            // 
            this.cbbValoracion.ColumnWidths = "";
            this.cbbValoracion.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbValoracion.Location = new System.Drawing.Point(67, 56);
            this.cbbValoracion.Name = "cbbValoracion";
            this.cbbValoracion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbValoracion.Size = new System.Drawing.Size(256, 20);
            this.cbbValoracion.TabIndex = 4;
            this.cbbValoracion.Text = "cbbValoracion";
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(248, 86);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 3;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbPantalla
            // 
            this.cbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbPantalla.Location = new System.Drawing.Point(158, 86);
            this.cbPantalla.Name = "cbPantalla";
            this.cbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbPantalla.Size = new System.Drawing.Size(81, 32);
            this.cbPantalla.TabIndex = 2;
            this.cbPantalla.Text = "Aceptar";
            this.cbPantalla.Click += new System.EventHandler(this.cbPantalla_Click);
            // 
            // lbConcepto
            // 
            this.lbConcepto.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbConcepto.Location = new System.Drawing.Point(4, 60);
            this.lbConcepto.Name = "lbConcepto";
            this.lbConcepto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbConcepto.Size = new System.Drawing.Size(57, 18);
            this.lbConcepto.TabIndex = 5;
            this.lbConcepto.Text = "Concepto:";
            // 
            // Sel_Grafica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 120);
            this.Controls.Add(this.fraIntervaloFechas);
            this.Controls.Add(this.cbbValoracion);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbPantalla);
            this.Controls.Add(this.lbConcepto);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 23);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Sel_Grafica";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Grafica valoraci�n m�dica";
            this.Closed += new System.EventHandler(this.Sel_Grafica_Closed);
            this.Load += new System.EventHandler(this.Sel_Grafica_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fraIntervaloFechas)).EndInit();
            this.fraIntervaloFechas.ResumeLayout(false);
            this.fraIntervaloFechas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFechaDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbValoracion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbConcepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}