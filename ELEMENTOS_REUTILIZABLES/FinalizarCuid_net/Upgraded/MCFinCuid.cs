using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace FinalizarCuid
{
	public class MCFinCuid
	{
		//Aqu� no se realizan actualizaciones en la Base de Datos, pero se obtiene la informaci�n necesaria para saber
		//d�nde hay que actualizar posteriormente en Base de Datos. Y tambi�n se recoge la informaci�n de si el usuario
		//desea abortar el proceso. Todos estos datos se asignan a variables p�blicas de los formularios desde los que
		//se realiza la llamada.
		public void proFinalizarCuidado(object nForm, SqlConnection nConnet, string stUsuario, string stCodPlan, string stCodDiag, string stTipoServ, int iA�oReg, int lNumReg, string stFecha, string stCodCuidado, bool bRealizarPregunta = true)
		{
			string stCondicion = String.Empty;
			string stTexto = String.Empty;
			string stDesPlan = String.Empty;

            //camaya todo_x_5
            //UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\ayuda_dlls.hlp");

            mbFinCuid.gForm = nForm;
			mbFinCuid.gConexion = nConnet;
			mbFinCuid.gstUsuario = stUsuario.Trim();
			mbFinCuid.gstCodPlan = stCodPlan.Trim();
			mbFinCuid.gstCodDiag = stCodDiag.Trim();
			mbFinCuid.gstTipoServ = stTipoServ.Trim();
			mbFinCuid.gintA�oReg = iA�oReg;
			mbFinCuid.glNumReg = lNumReg;
			mbFinCuid.gstFecha = stFecha.Trim();
			mbFinCuid.gstCodCuidado = stCodCuidado;
			bool bPreguntar = bRealizarPregunta; //para saber si hay que mostrar o no la pregunta al usuario (mediante los msgbox)

			//Si el cuidado est� asociado a un plan y no a un diagn�stico
			if ((mbFinCuid.gstCodPlan != "") && (mbFinCuid.gstCodDiag == ""))
			{
				stCondicion = " And gplantip='" + mbFinCuid.gstCodPlan + "' ";
				if (mbFinCuid.fnUnicoCuidadoPlan(stCondicion))
				{
					if (!bPreguntar)
					{
                        						
						//mbFinCuid.gForm.bFinalizarPlan = true; //para m�s adelante, finalizar el plan
					}
					else
					{
						//Si el cuidado a finalizar pertenece a un plan y no tiene diagn�stico asociado:Si se trata del �nico
						//cuidado del plan, se mostrar� un mensaje preguntando al usuario.
						stDesPlan = mbFinCuid.fnObtenerDesPlan(mbFinCuid.gstCodPlan);
						stTexto = "Al finalizar el cuidado se finalizar� el plan " + stDesPlan + ". �Desea continuar?";
						if (MessageBox.Show(stTexto, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
						{
							
							//mbFinCuid.gForm.bContinuarFinalizacion = false;
							return;
						}
						else
						{
							//camaya todo_x_5
							//mbFinCuid.gForm.bFinalizarPlan = true; //para m�s adelante, finalizar el plan
						}
					}
				}
			}

		}

		//aqu� se actualizan los valores correspondientes en Base de Datos
		public void proActualizar(SqlConnection nConnet, string stUsuario, string stCodPlan, string stCodDiag, string stTipoServ, int intA�oReg, int lngNumReg, string stFecha, bool blnFinalizarPlan, bool blnFinalizarDiag, bool blnFinalizarDiagPlan, int intMotivoFin, string stFechaFin)
		{
			DataSet RrPlan = null;
			string stPlan = String.Empty;
			DataSet RrDiag = null;
			string stDiag = String.Empty;
			
			//camaya todo_x_5
			//UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\ayuda_dlls.hlp");

			SqlConnection Conexion = nConnet;
			Serrores.AjustarRelojCliente(Conexion);
			string sUsuario = stUsuario.Trim();
			string sCodPlan = stCodPlan.Trim();
			string sCodDiag = stCodDiag.Trim();
			string sTipoServ = stTipoServ.Trim();
			int iA�oReg = intA�oReg;
			int lNumReg = lngNumReg;
			bool bFinalizarPlan = blnFinalizarPlan;
			bool bFinalizarDiag = blnFinalizarDiag;
			bool bFinalizarDiagPlan = blnFinalizarDiagPlan;
			int iMotFin = intMotivoFin;
			string sFechaFin = stFechaFin;

			if (bFinalizarPlan)
			{
				//se finaliza el plan
				stPlan = "Select gmotifin, ffinplan, gusuario From Eplanpac Where itiposer='" + sTipoServ + "' ";
				stPlan = stPlan + "And ganoregi=" + iA�oReg.ToString() + " And gnumregi=" + lNumReg.ToString() + " ";
				//stPlan = stPlan & "And fapuntes=" & FormatFechaHMS(sFecha) & " "
				stPlan = stPlan + "And gplantip='" + sCodPlan + "' And ffinplan is null ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stPlan, Conexion);
				RrPlan = new DataSet();
				tempAdapter.Fill(RrPlan);

				if (RrPlan.Tables[0].Rows.Count != 0)
				{					
					RrPlan.Edit();					
					RrPlan.Tables[0].Rows[0]["gmotifin"] = iMotFin;					
					RrPlan.Tables[0].Rows[0]["ffinplan"] = DateTime.Parse(sFechaFin);					
					RrPlan.Tables[0].Rows[0]["gusuario"] = sUsuario;					
					
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrPlan, RrPlan.Tables[0].TableName);
				}
				
				RrPlan.Close();
			}
			else if (bFinalizarDiag)
			{ 
				//se finaliza el diagn�stico
				stDiag = "Select gmotfpro, ffinprob, gusuario From eprobenf Where itiposer='" + sTipoServ + "' ";
				stDiag = stDiag + "And ganoregi=" + iA�oReg.ToString() + " And gnumregi=" + lNumReg.ToString() + " ";
				//stDiag = stDiag & "And fapuntes=" & FormatFechaHMS(sFecha) & " "
				stDiag = stDiag + "And gprobenf='" + sCodDiag + "' And ffinprob is null ";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stDiag, Conexion);
				RrDiag = new DataSet();
				tempAdapter_3.Fill(RrDiag);

				if (RrDiag.Tables[0].Rows.Count != 0)
				{					
					RrDiag.Edit();					
					RrDiag.Tables[0].Rows[0]["gmotfpro"] = iMotFin;					
					RrDiag.Tables[0].Rows[0]["ffinprob"] = sFechaFin;					
					RrDiag.Tables[0].Rows[0]["gusuario"] = sUsuario;					
					
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);
                    tempAdapter_3.Update(RrDiag, RrDiag.Tables[0].TableName);
				}
                				
				RrDiag.Close();
			}
			else if (bFinalizarDiagPlan)
			{ 
				//se finaliza el diagn�stico y el plan
				stPlan = "Select gmotifin, ffinplan, gusuario From Eplanpac Where itiposer='" + sTipoServ + "' ";
				stPlan = stPlan + "And ganoregi=" + iA�oReg.ToString() + " And gnumregi=" + lNumReg.ToString() + " ";
				//stPlan = stPlan & "And fapuntes=" & FormatFechaHMS(sFecha) & " "
				stPlan = stPlan + "And gplantip='" + sCodPlan + "' And ffinplan is null ";

				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stPlan, Conexion);
				RrPlan = new DataSet();
				tempAdapter_5.Fill(RrPlan);

				if (RrPlan.Tables[0].Rows.Count != 0)
				{					
					RrPlan.Edit();					
					RrPlan.Tables[0].Rows[0]["gmotifin"] = iMotFin;					
					RrPlan.Tables[0].Rows[0]["ffinplan"] = sFechaFin;					
					RrPlan.Tables[0].Rows[0]["gusuario"] = sUsuario;					
					
					SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_5);
                    tempAdapter_5.Update(RrPlan, RrPlan.Tables[0].TableName);
				}
                				
				RrPlan.Close();

				//se finaliza el diagn�stico
				stDiag = "Select gmotfpro, ffinprob, gusuario From eprobenf Where itiposer='" + sTipoServ + "' ";
				stDiag = stDiag + "And ganoregi=" + iA�oReg.ToString() + " And gnumregi=" + lNumReg.ToString() + " ";
				//stDiag = stDiag & "And fapuntes=" & FormatFechaHMS(sFecha) & " "
				stDiag = stDiag + "And gprobenf='" + sCodDiag + "' And ffinprob is null ";

				SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stDiag, Conexion);
				RrDiag = new DataSet();
				tempAdapter_7.Fill(RrDiag);

				if (RrDiag.Tables[0].Rows.Count != 0)
				{
					
					RrDiag.Edit();					
					RrDiag.Tables[0].Rows[0]["gmotfpro"] = iMotFin;					
					RrDiag.Tables[0].Rows[0]["ffinprob"] = sFechaFin;					
					RrDiag.Tables[0].Rows[0]["gusuario"] = sUsuario;					
					
					SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_7);
                    tempAdapter_7.Update(RrDiag, RrDiag.Tables[0].TableName);
				}
			
				RrDiag.Close();
			}

		}

		public MCFinCuid()
		{

			try
			{

				mbFinCuid.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{

				MessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}
		}

		~MCFinCuid()
		{
			mbFinCuid.clase_mensaje = null;
		}
	}
}