using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace FinalizarCuid
{
	internal static class mbFinCuid
	{
		public static object gForm = null;
		public static SqlConnection gConexion = null;
		public static Mensajes.ClassMensajes clase_mensaje = null;
		public static string gstUsuario = String.Empty;
		public static string gstCodPlan = String.Empty;
		public static string gstCodDiag = String.Empty;
		public static string gstTipoServ = String.Empty;
		public static int gintA�oReg = 0;
		public static int glNumReg = 0;
		public static string gstFecha = String.Empty;
		public static string gstCodCuidado = String.Empty;

		//Esta funci�n devuelve la descripci�n del plan cuyo c�digo se pasa como argumento
		internal static string fnObtenerDesPlan(string sCodPlan)
		{
			string result = String.Empty;
			string stSql = "Select dplantip From Eplantip Where gplantip='" + sCodPlan + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, gConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{				
				result = Convert.ToString(RrSql.Tables[0].Rows[0]["dplantip"]).Trim();
			}
            			
			RrSql.Close();
			return result;
		}

		//Esta funci�n devuelve la descripci�n del diagn�stico cuyo c�digo se pasa como argumento
		internal static string fnObtenerDesDiag(string sCodDiag)
		{
			string result = String.Empty;
			string stSql = "Select dcuidado From ecodcuid Where gcuidenf='" + sCodDiag + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, gConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{				
				result = Convert.ToString(RrSql.Tables[0].Rows[0]["dplantip"]).Trim();
			}
			
			RrSql.Close();
			return result;
		}

		//Si en la consulta ejecutada en esta funci�n (teniendo en cuenta el argumento "stCondicion")
		//no se devuelve ning�n registro, significa que el registro actualmente seleccionado en el grid de apuntes es el
		//�nico que cumple las condiciones especificadas en la consulta
		internal static bool fnUnicoCuidadoPlan(string stCondicion)
		{
			bool result = false;
			bool fnContinuar = false;

			string stSql = "Select itiposer From ecuidado Where itiposer='" + gstTipoServ + "' ";
			stSql = stSql + "And ganoregi=" + gintA�oReg.ToString() + " And gnumregi=" + glNumReg.ToString() + " ";
			stSql = stSql + "And gcuidado<>'" + gstCodCuidado + "' ";
			stSql = stSql + "And (ffintrat is null or (ffintrat is not null and ffintrat >= getdate())) ";
			stSql = stSql + stCondicion;

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, gConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count == 0)
			{
				fnContinuar = true;
			}
			
			RrSql.Close();

			if (fnContinuar)
			{
				//Vemos si hay alg�n diagn�stico asociado al plan
				stSql = "Select itiposer From eprobenf Where itiposer='" + gstTipoServ + "' ";
				stSql = stSql + "And ganoregi=" + gintA�oReg.ToString() + " And gnumregi=" + glNumReg.ToString() + " ";
				stSql = stSql + "And ffinprob is null ";
				stSql = stSql + stCondicion;

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, gConexion);
				RrSql = new DataSet();
				tempAdapter_2.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count == 0)
				{
					result = true;
				}
				
				RrSql.Close();
			}

			return result;
		}
	}
}