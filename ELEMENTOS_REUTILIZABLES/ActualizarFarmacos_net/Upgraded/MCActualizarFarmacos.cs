using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace ActualizarFarmacos
{
	public class MCActualizarFarmacos
	{
		public void Llamada(ref SqlConnection nConnet, string FechaActual)
		{
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref nConnet);

			BasActualizar.Conexion = nConnet;
			Serrores.AjustarRelojCliente(BasActualizar.Conexion);
			//GstFechaActual = FechaActual 'formato DD/MM/YYYY HH:NN:SS

			//Para evitar que se produzcan descuadres en las horas en las que se actualizan los farmacos
			//buscamos la fecha que tiene el servidor
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select getdate()", BasActualizar.Conexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
            			
			BasActualizar.GstFechaActual = Convert.ToDateTime(RrSql.Tables[0].Rows[0][0]).ToString("dd/MM/yyyy HH:mm:ss");
            			
			RrSql.Close();

			BasActualizar.BuscarFarmacos();

		}
	}
}