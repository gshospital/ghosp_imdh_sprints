using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace ActualizarFarmacos
{
	internal static class BasActualizar
	{
		public static SqlConnection Conexion = null;
		public static string GstFechaActual = String.Empty;
		public static object clase_mensaje = null;

		internal static void BuscarFarmacos()
		{
			//Este proceso trata de actualizar el estado de los farmacos que llevan una fecha prevista.
			string Sql = String.Empty;
			DataSet RrSQL = null;
			StringBuilder sqlUpdate = new StringBuilder();
			StringBuilder sqlGrabar = new StringBuilder();
			int iContador = 0;
			string stFechaPrevista = String.Empty;
			string stEpisodio = String.Empty;
			bool ActualizarFecha = false;

			try
			{
				Sql = "select * from EFARMACO ";
				Sql = Sql + " where EFARMACO.fprevist is not null ";			
				object tempRefParam = GstFechaActual;
				Sql = Sql + "and EFARMACO.fprevist <= " + Serrores.FormatFechaHMS(tempRefParam) + " ";
				GstFechaActual = Convert.ToString(tempRefParam);
				Sql = Sql + " order by ganoregi,gnumregi ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, Conexion);
				RrSQL = new DataSet();
				tempAdapter.Fill(RrSQL);

				if (RrSQL.Tables[0].Rows.Count != 0)
				{				
					RrSQL.MoveFirst();
					iContador = 0;
					stEpisodio = "";
					ActualizarFecha = true;
                    Conexion.BeginTransScope();				
					foreach (DataRow iteration_row in RrSQL.Tables[0].Rows)
					{
						//por cada registro comprobamos si el episodio es de hospitalizacion
						//si es de hospitalizacion comprobamos que el paciente no esta ausente
						if (Convert.ToString(iteration_row["itiposer"]).Trim().ToUpper() == "H")
						{
							if (stEpisodio.Trim() != Convert.ToString(iteration_row["itiposer"]) + Convert.ToString(iteration_row["ganoregi"]) + Convert.ToString(iteration_row["gnumregi"]))
							{
								stEpisodio = Convert.ToString(iteration_row["itiposer"]) + Convert.ToString(iteration_row["ganoregi"]) + Convert.ToString(iteration_row["gnumregi"]);
								//por cada nuevo registro compruebo si el paciente esta ausente
								if (PacienteAusente(Convert.ToInt32(iteration_row["ganoregi"]), Convert.ToInt32(iteration_row["gnumregi"]), GstFechaActual))
								{
									//saltamos al siguiente registro
									ActualizarFecha = false;
								}
								else
								{
									//hacemos todo como hasta ahora
									ActualizarFecha = true;
								}
							}
							else
							{
								//no modifico ActualizarFecha
							}
						}
						else
						{
							//si no es hospitalizacion hacemos lo mismo que hasta ahora
							ActualizarFecha = true;
						}
						//por cada registro recuperado hay que cerrar el movimiento
						//y grabar un nuevo registro con el nuevo estado
						if (ActualizarFecha)
						{
							iContador++;
							//stFechaPrevista = Format(RrSQL("fprevist"), "DD/MM/YYYY HH:NN") & ":" & Format(iContador, "00")
							stFechaPrevista = Convert.ToDateTime(iteration_row["fprevist"]).AddSeconds(iContador).ToString("dd/MM/yyyy HH:NN:SS");							
							object tempRefParam2 = stFechaPrevista;
							sqlUpdate = new StringBuilder("Update EFARMACO set ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam2) + " ");
							stFechaPrevista = Convert.ToString(tempRefParam2);
							if (Convert.ToString(iteration_row["iesttrat"]) == "I")
							{
								sqlUpdate.Append(",fprevist = null, iestprev = null, gmsuspro = null ");
							}
							else
							{
								sqlUpdate.Append(",fprevist = null, iestprev = null ");
							}
							sqlUpdate.Append(" where itiposer = '" + Convert.ToString(iteration_row["itiposer"]) + "'");
							sqlUpdate.Append(" and ganoregi = " + Convert.ToString(iteration_row["ganoregi"]) + " ");
							sqlUpdate.Append(" and gnumregi = " + Convert.ToString(iteration_row["gnumregi"]) + " ");							
							object tempRefParam3 = iteration_row["fapuntes"];
							sqlUpdate.Append(" and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam3) + " ");
							sqlUpdate.Append(" and gfarmaco = '" + Convert.ToString(iteration_row["gfarmaco"]) + "' ");
							sqlUpdate.Append(" and ffinmovi is null ");

							sqlGrabar = new StringBuilder("Insert into EFARMACO (itiposer, ganoregi,gnumregi,fapuntes,finitrat,gfarmaco,gviadmin,");
							sqlGrabar.Append("gunitoma,iesttrat,gusuario,cdiastra,gpersona,ffinmovi,gmsuspro,fsuspens,");
							sqlGrabar.Append("ffintrat,oobserva,cdosisfa00,cdosisfa01,cdosisfa02,cdosisfa03,cdosisfa04,cdosisfa05,");
							sqlGrabar.Append("cdosisfa06,cdosisfa07,cdosisfa08,cdosisfa09,cdosisfa10,cdosisfa11,cdosisfa12,");
							sqlGrabar.Append("cdosisfa13,cdosisfa14,cdosisfa15,cdosisfa16,cdosisfa17,cdosisfa18,cdosisfa19,");
							sqlGrabar.Append("cdosisfa20,cdosisfa21,cdosisfa22,cdosisfa23,vdiasfre,periodic,iprecisa,");
							sqlGrabar.Append("fcubiped,cdosisac,sitadmtt,fprevist,iestprev,gfretoma,ilargtra,fcubirec,");
							sqlGrabar.Append("ncodperf,nordfarm,veloperf,oobsperf,iproppac,gsofarma,idounica,icoagula,indicinr,indittpa,gcodfarm,imotdoce ) ");							
							object tempRefParam4 = stFechaPrevista;
							object tempRefParam5 = iteration_row["finitrat"];
							sqlGrabar.Append("values ('" + Convert.ToString(iteration_row["itiposer"]) + "'," + Convert.ToString(iteration_row["ganoregi"]) + "," + Convert.ToString(iteration_row["gnumregi"]) + "," + Serrores.FormatFechaHMS(tempRefParam4) + "," + Serrores.FormatFechaHMS(tempRefParam5) + ",'" + Convert.ToString(iteration_row["gfarmaco"]) + "'" + "," + Convert.ToString(iteration_row["gviadmin"]) + "," + Convert.ToString(iteration_row["gunitoma"]) + ",'" + Convert.ToString(iteration_row["iestprev"]) + "','" + Convert.ToString(iteration_row["gusuario"]).Trim() + "'");
							stFechaPrevista = Convert.ToString(tempRefParam4);
							
							if (Convert.IsDBNull(iteration_row["cdiastra"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append("," + Convert.ToString(iteration_row["cdiastra"]));
							}
							
							if (Convert.IsDBNull(iteration_row["gpersona"]))
							{
								sqlGrabar.Append(",null,null");
							}
							else
							{
								sqlGrabar.Append("," + Convert.ToString(iteration_row["gpersona"]) + ",null");
							}
							
							if (Convert.IsDBNull(iteration_row["gmsuspro"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append("," + Convert.ToString(iteration_row["gmsuspro"]));
							}
							if (Convert.ToString(iteration_row["iestprev"]).Trim() == "S")
							{
							
								object tempRefParam6 = stFechaPrevista;
								sqlGrabar.Append("," + Serrores.FormatFechaHMS(tempRefParam6) + ",null");
								stFechaPrevista = Convert.ToString(tempRefParam6);
							}
							else
							{							
								object tempRefParam7 = stFechaPrevista;
								sqlGrabar.Append(",null," + Serrores.FormatFechaHMS(tempRefParam7));
								stFechaPrevista = Convert.ToString(tempRefParam7);
							}
						
							if (Convert.IsDBNull(iteration_row["oobserva"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["oobserva"]).Trim() + "'");
							}
							for (int aik = 0; aik <= 23; aik++)
							{						
								if (Convert.IsDBNull(iteration_row["cdosisfa" + StringsHelper.Format(aik, "00") + ""]))
								{
									sqlGrabar.Append(",null");
								}
								else
								{
									sqlGrabar.Append("," + Convert.ToString(iteration_row["cdosisfa" + StringsHelper.Format(aik, "00") + ""]));
								}
							}
							
							if (Convert.IsDBNull(iteration_row["vdiasfre"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["vdiasfre"]) + "'");
							}
							
							if (Convert.IsDBNull(iteration_row["periodic"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append("," + Convert.ToString(iteration_row["periodic"]));
							}
							
							if (Convert.IsDBNull(iteration_row["iprecisa"]))
							{
								sqlGrabar.Append(",null,null,null,null,null,null,null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["iprecisa"]) + "',null,null,null,null,null");
							
								if (Convert.IsDBNull(iteration_row["gfretoma"]))
								{
									sqlGrabar.Append(",null");
								}
								else
								{
									sqlGrabar.Append("," + Convert.ToString(iteration_row["gfretoma"]));
								}
							}
							
							if (Convert.IsDBNull(iteration_row["ilargtra"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["ilargtra"]) + "'");
							}
							
							if (Convert.IsDBNull(iteration_row["fcubirec"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{							
								object tempRefParam8 = iteration_row["fcubirec"];
								sqlGrabar.Append(", " + Serrores.FormatFechaHMS(tempRefParam8));
							}
						
							if (Convert.IsDBNull(iteration_row["ncodperf"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append("," + Convert.ToString(iteration_row["ncodperf"]));
							}
							
							if (Convert.IsDBNull(iteration_row["nordfarm"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append("," + Convert.ToString(iteration_row["nordfarm"]));
							}
							
							if (Convert.IsDBNull(iteration_row["veloperf"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append("," + Convert.ToString(iteration_row["veloperf"]));
							}
							
							if (Convert.IsDBNull(iteration_row["oobsperf"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["oobsperf"]) + "'");
							}
							
							if (Convert.IsDBNull(iteration_row["iproppac"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["iproppac"]) + "'");
							}
							
							if (Convert.IsDBNull(iteration_row["gsofarma"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append("," + Convert.ToString(iteration_row["gsofarma"]));
							}
							
							if (Convert.IsDBNull(iteration_row["idounica"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["idounica"]) + "'");
							}
							
							if (Convert.IsDBNull(iteration_row["icoagula"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["icoagula"]) + "'");
							}
							
							if (Convert.IsDBNull(iteration_row["indicinr"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["indicinr"]) + "'");
							}
							
							if (Convert.IsDBNull(iteration_row["indittpa"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["indittpa"]) + "'");
							}
							
							if (Convert.IsDBNull(iteration_row["gcodfarm"]))
							{
								sqlGrabar.Append(",null");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["gcodfarm"]) + "'");
							}
							
							if (Convert.IsDBNull(iteration_row["imotdoce"]))
							{
								sqlGrabar.Append(",null)");
							}
							else
							{
								sqlGrabar.Append(",'" + Convert.ToString(iteration_row["imotdoce"]) + "')");
							}

							SqlCommand tempCommand = new SqlCommand(sqlUpdate.ToString(), Conexion);
							tempCommand.ExecuteNonQuery();
							SqlCommand tempCommand_2 = new SqlCommand(sqlGrabar.ToString(), Conexion);
							tempCommand_2.ExecuteNonQuery();
						}
						else
						{
							//pasamos de actualizar el registro
						}
					}
                    Conexion.CommitTransScope();
					
				}
				
				RrSQL.Close();
			}
			catch
			{
                Conexion.RollbackTransScope();				
				RadMessageBox.Show("Se ha producido un error en la actualización del estado de los farmacos", Application.ProductName);
			}
		}

		private static bool PacienteAusente(int iAñoapunte, int lNumeroApunte, string stFechaAusencia)
		{
			//**funcion que va a devolver si un paciente esta ausente en una fecha determinada

			bool result = false;

			string Sql = "select * from AAUSENCI ";
			Sql = Sql + "where ganoadme = " + iAñoapunte.ToString() + " and gnumadme = " + lNumeroApunte.ToString() + " ";
			System.DateTime TempDate = DateTime.FromOADate(0);
			Sql = Sql + "and fausenci <= '" + ((DateTime.TryParse(stFechaAusencia, out TempDate)) ? TempDate.ToString("MM/dd/yyyy HH:mm:ss") : stFechaAusencia) + "'";
			System.DateTime TempDate2 = DateTime.FromOADate(0);
			Sql = Sql + "and (freavuel >= '" + ((DateTime.TryParse(stFechaAusencia, out TempDate2)) ? TempDate2.ToString("MM/dd/yyyy HH:mm:ss") : stFechaAusencia) + "' ";
			Sql = Sql + "or freavuel is null) ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, Conexion);
			DataSet RrSQL = new DataSet();
			tempAdapter.Fill(RrSQL);
			if (RrSQL.Tables[0].Rows.Count != 0)
			{
				//si devuelve algun registro con esta condicion
				//significa que esta ausente y tiene que devolver un true
				result = true;
			}
			else
			{
				//no devuelve ningun registro con esta condicion
				//no esta ausente el paciente por lo que devuelve un false
				result = false;
			}
		
			RrSQL.Close();
			return result;
		}
	}
}