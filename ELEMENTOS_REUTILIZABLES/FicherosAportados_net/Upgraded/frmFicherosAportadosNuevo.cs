using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using System.Transactions;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace FicherosAportados
{
	public partial class frmFicherosAportadosNuevo
		: RadForm 
	{

		private const int SE_ERR_ACCESSDENIED = 5;
		private const int SE_ERR_ASSOCINCOMPLETE = 27;
		private const int SE_ERR_DDEBUSY = 30;
		private const int SE_ERR_DDEFAIL = 29;
		private const int SE_ERR_DDETIMEOUT = 28;
		private const int SE_ERR_DLLNOTFOUND = 32;
		private const int SE_ERR_FNF = 2;
		private const int SE_ERR_NOASSOC = 31;
		private const int SE_ERR_OOM = 8;
		private const int SE_ERR_PNF = 3;
		private const int SE_ERR_SHARE = 26;
		private const int ERROR_FILE_NOT_FOUND = 2;
		private const int ERROR_PATH_NOT_FOUND = 3;
		private const int ERROR_BAD_FORMAT = 11;

		int[, ] arrError = new int[15, 3];
		object[] arrFicheros = null;



		byte Col_Chequeo = 0;
		byte Col_FecCrea = 0;
		byte Col_Descrip = 0;
		byte Col_NomDocu = 0;
		byte Col_NumDocu = 0;
		byte Col_ClaDocu = 0;
		byte Col_Usuario = 0;

		int intNumDocu = 0;
		string stFiltro = String.Empty;
		string stInitDir = String.Empty;
		string stNomDocu = String.Empty;
		bool registro_seleccionado = false;
		int row_ultima = 0;
		int vlposy = 0;
		public frmFicherosAportadosNuevo()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch(Exception Exep)
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}
        
		private void cmbBotones_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cmbBotones, eventSender);
			int iRespuesta = 0;
			string strFichero = String.Empty;

			switch(Index)
			{
				case 0 :  //Insertar 
					vspguardar.Visible = true; 
					vspguardar.Enabled = true; 
					txtDescripcion.Enabled = false; 
					txtNombreFichero.Enabled = false; 
					LlamarVentanaCargaFicheros(); 

					 
					break;
				case 1 :  //Aceptar 
					if (vspguardar.Visible)
					{
						if (vspguardar.MaxRows > 0)
						{
							vspguardar.Col = 2;
							int tempForVar = vspguardar.MaxRows;
							for (int intContador = 1; intContador <= tempForVar; intContador++)
							{
								vspguardar.Row = intContador;
								if (vspguardar.Text.Trim() == "")
								{
									RadMessageBox.Show("El campo descripci�n, es un valor obligatorio", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
									vspguardar.Action = 0;
									return;
								}
							}
						}
						GrabarDatos();
						vspguardar.Visible = false;
						vspguardar.Enabled = false;
						txtDescripcion.Enabled = true;
						txtNombreFichero.Enabled = true;

					}
					else
					{
						if (txtDescripcion.Text.Trim() == "")
						{
							RadMessageBox.Show("El campo descripci�n, es un valor obligatorio", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
							txtDescripcion.Focus();
						}
						else if (txtNombreFichero.Text.Trim() == "")
						{
                            RadMessageBox.Show("El campo archivo, es un valor obligatorio", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
							txtNombreFichero.Focus();
						}
						else
						{
							ActualizaDatos();
						}
					} 
					break;
				case 2 :  //Cancelar 
					int tempForVar2 = vspFicheros.MaxRows; 
					for (int intContador = 1; intContador <= tempForVar2; intContador++)
					{
						vspFicheros.Row = intContador;
						vspFicheros.Col = Col_NomDocu;
						BGFicherosAportados.Cerrar_ventana(vspFicheros.Text);

						if (FileSystem.Dir(Path.GetDirectoryName(Application.ExecutablePath) + "\\tmp" + vspFicheros.Text, FileAttribute.Normal) != "")
						{
							File.Delete(Path.GetDirectoryName(Application.ExecutablePath) + "\\tmp" + vspFicheros.Text);
						}

					} 
					this.Close(); 
					break;
				case 3 :  //Visualizar 
					fVisualiza(); 
					break;
				case 4 :  //Eliminar 
					iRespuesta = 0; 
					int tempForVar3 = vspFicheros.MaxRows; 
					string auxVar = String.Empty; 
					for (int intContador = 1; intContador <= tempForVar3; intContador++)
					{
						vspFicheros.Row = intContador;
						vspFicheros.Col = Col_Chequeo;
						bool tempBool = false;
						auxVar = Convert.ToString(vspFicheros.Value);
						if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
						{
							short tempRefParam = 1125;
							string[] tempRefParam2 = new string[]{"Los ficheros seleccionados se eliminaran", "continuar"};
							iRespuesta = Convert.ToInt32(BGFicherosAportados.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BGFicherosAportados.cnConexion, tempRefParam2));
							break;
						}

					} 
					if (iRespuesta == 6)
					{

						EliminaRegistros();
						frmFicherosAportadosNuevo_Load(this, new EventArgs());
					} 
					 
					break;
			}
			return;


		}
		public void LlamarVentanaCargaFicheros()
		{
			//used in call setup

			//used after call
			string buff = String.Empty;
			string stNombreCorto = String.Empty;
			string stCamino = String.Empty;
			string stMensaje = String.Empty;
			int IntFila = 0;
			int intContador = 0;
			bool bTest = false;
			//If Right(sFilters, 1) & "|" Then
			//Cambiamos el caracter "|" por el vbNullChar en el filtro
			string sFilters = stFiltro;

			while(sFilters.IndexOf('|') >= 0)
			{
				sFilters = sFilters.Trim().Substring(0, Math.Min(sFilters.IndexOf('|'), sFilters.Trim().Length)) + "\0" + sFilters.Trim().Substring(sFilters.IndexOf('|') + 1);
			};




			//Tama�o de la estructura OFN
			BGFicherosAportados.OFN.nStructSize = Marshal.SizeOf(BGFicherosAportados.OFN);

			//window owning the dialog
			BGFicherosAportados.OFN.hWndOwner = this.Handle.ToInt32();

			//filtro para los tipos de archivos.
			BGFicherosAportados.OFN.sFilter = sFilters;

			//De todos los tipos de archivos del filtro, c�al es el que aparece por defecto.
			BGFicherosAportados.OFN.nFilterIndex = 1;

			//Podemos poner que aparezca un archivo por defecto. Aqu� saldr� vac�o. Desde aqu� se recuperar�n los archivos seleccionados y se enviar�n al buffer
			BGFicherosAportados.OFN.sFile = "" + new String(' ', 1024) + "\0" + "\0";

			//tama�� del buffer que contine los archivos seleccionados
			BGFicherosAportados.OFN.nMaxFile = BGFicherosAportados.OFN.sFile.Length;

			//default extension applied to
			//file if it has no extention
			//.sDefFileExt = "bas" & vbNullChar & vbNullChar

			//Puntero a un buffer que recibe el nombre de archivo y extensi�n (sin informaci�n de la ruta) del archivo seleccionado. Este miembro puede ser NULL.
			BGFicherosAportados.OFN.sFileTitle = "\0" + new String(' ', 512) + "\0" + "\0";
			BGFicherosAportados.OFN.nMaxTitle = BGFicherosAportados.OFN.sFileTitle.Length;

			//directorio por defecto, double-null terminated
			//.sInitialDir = "d:\vb5" & vbNullChar & vbNullChar

			//t�tulo
			BGFicherosAportados.OFN.sDialogTitle = "Seleccione un archivo";

			//default open flags and multiselect
			BGFicherosAportados.OFN.flags = BGFicherosAportados.OFS_FILE_OPEN_FLAGS | BGFicherosAportados.OFN_ALLOWMULTISELECT;


			//call the API
			if (UpgradeSupportHelper.PInvoke.SafeNative.comdlg32.GetOpenFileName(ref BGFicherosAportados.OFN) != 0)
			{

				//Quitamos los dos �ltimos nulos
				buff = BGFicherosAportados.OFN.sFile.Substring(0, Math.Min(BGFicherosAportados.OFN.sFile.Length - 2, BGFicherosAportados.OFN.sFile.Length)).Trim();
				IntFila = 1;
				stCamino = "";
				stMensaje = "";
				//     muestra los archivos seleccionados en sFile. Si la selecci�n es m�ltiple,
				//     el primer miembro es el camino, y los restantes miembros son los archivos.

				while(buff.Length > 3)
				{
					//Cogemos el camino
					if (stCamino == "")
					{
						//s�lo ha elegido un fichero
						if ((buff.Trim().IndexOf("\0") + 1) == buff.Trim().Length - 1)
						{
							for (intContador = 1; intContador <= buff.Trim().Length; intContador++)
							{
								if (buff.Trim().Substring(Math.Max(buff.Trim().Length - intContador, 0)).StartsWith("\\"))
								{
									break;
								}
							}
							stCamino = buff.Trim().Substring(0, Math.Min((buff.Trim().Length - intContador) + 1, buff.Trim().Length));
							buff = buff.Trim().Substring(Math.Max(buff.Trim().Length - (intContador - 1), 0));
						}
						else
						{
							// ha elegido m�s de uno
							stCamino = StripDelimitedItem(ref buff, "\0");
						}
					}
					//comprobamos si ya est� metido
					bTest = true;
					stNombreCorto = StripDelimitedItem(ref buff, "\0");
					if (vspFicheros.MaxRows > 0)
					{
						vspFicheros.Col = Col_NomDocu;
						int tempForVar = vspFicheros.MaxRows;
						for (intContador = 1; intContador <= tempForVar; intContador++)
						{
							vspFicheros.Row = intContador;
							if (stNombreCorto == vspFicheros.Text.Trim())
							{
								bTest = false;
                                RadMessageBox.Show("El fichero " + stNombreCorto + " ya est� guardado", Application.ProductName);
								break;
							}
						}
					}
					//Si no lo est� lo guardamos
					if (bTest)
					{

						vspguardar.MaxRows = IntFila;
						vspguardar.Row = IntFila;


						//Nombre corto del archivo.
						vspguardar.Col = 1;
						vspguardar.CellType =  5;
                        vspguardar.Text = stNombreCorto;

						//Descripci�n del archivo.
						vspguardar.Col = 2;
						vspguardar.CellType =  1;
						if (stNomDocu != "")
						{
							vspguardar.Text = stNomDocu;
						}
						else
						{
							vspguardar.Text = stNombreCorto;
						}

						//Nombre completo del archivo
						vspguardar.Col = 3;
						vspguardar.CellType =  5;
						vspguardar.Text = stCamino + stNombreCorto;
						vspguardar.SetColHidden(vspguardar.Col, true);


						IntFila++;
					}
				};
				if (vspguardar.MaxRows > 0)
				{
					vspguardar.Row = 1;
					vspguardar.Col = 2;
					vspguardar.Row2 = vspguardar.MaxRows;
					vspguardar.Col2 = 2;
					vspguardar.BlockMode = true;
					//sdsalazar todo 5_5
                    //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property vspguardar.EditMode was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//vspguardar.setEditMode(true);
					vspguardar.Lock = false;
					vspguardar.BlockMode = false;
				}
			}
		}

		private string StripDelimitedItem(ref string startStrg, string delimiter)
		{

			//take a string separated by nulls,
			//split off 1 item, and shorten the string
			//so the next item is ready for removal.
			string result = String.Empty;

			int pos = (startStrg.IndexOf(delimiter) + 1);

			if (pos != 0)
			{

				result = startStrg.Substring(0, Math.Min(pos - 1, startStrg.Length));
				startStrg = startStrg.Substring(pos, Math.Min(startStrg.Length, startStrg.Length - pos));

			}

			return result;
		}


		//UPGRADE_NOTE: (7001) The following declaration (TrimNull) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string TrimNull(string item)
		//{
				//
				//
				//int pos = (item.IndexOf(Strings.Chr(0).ToString()) + 1);
				//if (pos != 0)
				//{
					//return item.Substring(0, Math.Min(pos - 1, item.Length));
				//}
				//else
				//{
					//return item;
				//}
				//
		//}



		private void frmFicherosAportadosNuevo_Load(Object eventSender, EventArgs eventArgs)
		{

			if (!mbAcceso.AplicarControlAcceso(BGFicherosAportados.sUsuario, "A", this, "FICHEROSAP", ref BGFicherosAportados.astrcontroles, ref BGFicherosAportados.astreventos))
			{
				cmbBotones_Click(cmbBotones[2], new EventArgs());
			}

			BGFicherosAportados.ObtenerValoresTipoBD();
			BGFicherosAportados.ObtenerLocale();
			BGFicherosAportados.vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(BGFicherosAportados.cnConexion);
			BGFicherosAportados.stDecimalBD = Serrores.ObtenerSeparadorDecimalBD(BGFicherosAportados.cnConexion);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref BGFicherosAportados.cnConexion);

			stNomDocu = "";
			string strSql = "SELECT valfanu3 FROM SCONSGLO WHERE gconsglo = 'DOCAEDEF'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu3"]) + "").Trim().ToUpper() == "S")
				{
					stNomDocu = BGFicherosAportados.gObserva.Substring(0, Math.Min(100, BGFicherosAportados.gObserva.Length));
				}
			}
			rdoTemp.Close();


			//File1.Pattern = "*.*"
			strSql = "SELECT ltrim(rtrim(isnull(valfanu1,''))) + ltrim(rtrim(isnull(valfanu2,''))) + ltrim(rtrim(isnull(valfanu3,''))) filtro FROM SCONSGLO WHERE gconsglo = 'FICHVISU'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
			rdoTemp = new DataSet();
			tempAdapter_2.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				//File1.Pattern = IIf(Trim(rdoTemp("valfanu1")) = "", "*.*", rdoTemp("valfanu1"))
				stFiltro = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["filtro"]).Trim() == "") ? "*.*" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["filtro"]).Trim();
			}
            rdoTemp.Close();

			Col_Chequeo = 1;
			Col_FecCrea = 2;
			Col_Descrip = 3;
			Col_NomDocu = 4;
			Col_NumDocu = 5;
			Col_ClaDocu = 6;
			Col_Usuario = 7;

			lblTextos[1].Text = BGFicherosAportados.gPaciente;
			lblTextos[4].Text = fHistoria();



			intNumDocu = 0;
			//blnVacio = True

			vspFicheros.MaxRows = 0;

			CargaGrid();

			CargaErrorShell();

			registro_seleccionado = false;
			row_ultima = 0;
            vspFicheros.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly; 
			cmbBotones[0].Enabled = true;
			txtNombreFichero.Text = "";
			txtDescripcion.Text = "";
			//Ocultamos el grid hasta que busque ficheros.
			vspguardar.Visible = false;
			vspguardar.Enabled = false;
			vspguardar.MaxRows = 0;
			txtDescripcion.Enabled = true;
			txtNombreFichero.Enabled = true;


		}

		private string fHistoria()
		{

			string result = String.Empty;
			string strSql = "Select ghistoria FROM HDOSSIER " + 
			                "WHERE gidenpac = '" + BGFicherosAportados.gstIdenPaciente + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(rdoTemp.Tables[0].Rows[0]["ghistoria"]);
			}
			else
			{
				result = "";
			}
            rdoTemp.Close();

			return result;
		}


		private void CargaGrid()
		{
			string strSql = String.Empty;
			DataSet rdoTemp = null;
			int IntFila = 0;
			Color varColor = new Color();


			vspFicheros.MaxRows = 0;

			if (BGFicherosAportados.gNombreDoc != "")
			{

				strSql = "SELECT * FROM DDOCUPDF WHERE gnombdoc = '" + BGFicherosAportados.gNombreDoc + "' Order By fechalta, inumdocu";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);

				vspFicheros.MaxRows = rdoTemp.Tables[0].Rows.Count;
				rdoTemp.MoveFirst();
				IntFila = 1;
				varColor = vspFicheros.BackColor;
				foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
				{
					vspFicheros.Row = IntFila;

					vspFicheros.Col = Col_FecCrea;
					vspFicheros.CellType =  5;
					vspFicheros.Text = Convert.ToString(iteration_row["fechalta"]);
					vspFicheros.TypeHAlign = ContentAlignment.MiddleCenter;

					vspFicheros.Col = Col_Descrip;
					vspFicheros.CellType =  5;
					vspFicheros.setTypeEditLen(100);
					vspFicheros.Text = Convert.ToString(iteration_row["ddescrip"]);

					vspFicheros.Col = Col_NomDocu;
					vspFicheros.CellType =  5;
					vspFicheros.Text = Convert.ToString(iteration_row["dnombpdf"]);


					vspFicheros.Col = Col_NumDocu;
					vspFicheros.CellType =  5;
					vspFicheros.Text = Convert.ToString(iteration_row["inumdocu"]);
					vspFicheros.SetColHidden(vspFicheros.Col, true);

					vspFicheros.Col = Col_ClaDocu;
					vspFicheros.CellType =  5;
					vspFicheros.Text = Convert.ToString(iteration_row["gnombdoc"]);
					vspFicheros.SetColHidden(vspFicheros.Col, true);

					vspFicheros.Col = Col_Usuario;
					vspFicheros.CellType = 5;
					vspFicheros.Text = Convert.ToString(iteration_row["gusuario"]);
					vspFicheros.SetColHidden(vspFicheros.Col, true);


					intNumDocu = Convert.ToInt32(iteration_row["inumdocu"]);
					IntFila++;
				}
                rdoTemp.Close();
                rdoTemp = null;
			}
		}

		private void GrabarDatos()
		{
			bool ErrGraba = false;
			byte[] Binario = null;
			string strClave = String.Empty;
			System.DateTime datFecha = DateTime.FromOADate(0);
			DataSet rdoTemp = null;
			string strSql = String.Empty;
			string strMensaje = String.Empty;
			bool blnContinua = false;
			string stTmpCadena = String.Empty;
			string strFichero = String.Empty;

			try
			{
				ErrGraba = true;

				datFecha = DateTime.Now;
				blnContinua = true;

				if (BGFicherosAportados.gNombreDoc.Trim() == "")
				{
					strClave = BGFicherosAportados.gstIdenPaciente.Trim();
					strClave = strClave + DateTime.Now.ToString("yyyyMMdd") + DateTime.Now.TimeOfDay.TotalSeconds.ToString();
					strClave = strClave + "PDF";

					stTmpCadena = strClave;
					strClave = "";
					for (int intContador = 1; intContador <= stTmpCadena.Length; intContador++)
					{
						if (stTmpCadena.Substring(intContador - 1, Math.Min(1, stTmpCadena.Length - (intContador - 1))) != "," && stTmpCadena.Substring(intContador - 1, Math.Min(1, stTmpCadena.Length - (intContador - 1))) != ".")
						{
							strClave = strClave + stTmpCadena.Substring(intContador - 1, Math.Min(1, stTmpCadena.Length - (intContador - 1)));
						}
					}
				}
				else
				{
					strClave = BGFicherosAportados.gNombreDoc;
				}


				if (strClave.Trim() != "")
				{
					int tempForVar = vspguardar.MaxRows;
					for (int intContador = 1; intContador <= tempForVar; intContador++)
					{
						vspguardar.Row = intContador;

						strSql = "SELECT * FROM DDOCUPDF WHERE 1=2";
						SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
						rdoTemp = new DataSet();
						tempAdapter.Fill(rdoTemp);
						vspguardar.Col = 3;
						Binario = (byte[]) Serrores.fPasaBinario(vspguardar.Text);
						rdoTemp.AddNew();
						rdoTemp.Tables[0].Rows[0]["gnombdoc"] = strClave;
						rdoTemp.Tables[0].Rows[0]["inumdocu"] = intNumDocu + 1;
						vspguardar.Col = 2;
						rdoTemp.Tables[0].Rows[0]["ddescrip"] = vspguardar.Text;
						rdoTemp.Tables[0].Rows[0]["fechalta"] = datFecha;
						rdoTemp.Tables[0].Rows[0]["odocupdf"] = Binario;
						vspguardar.Col = 1;
						rdoTemp.Tables[0].Rows[0]["dnombpdf"] = vspguardar.Text;
						rdoTemp.Tables[0].Rows[0]["gusuario"] = BGFicherosAportados.sUsuario;
						string tempQuery = rdoTemp.Tables[0].TableName;
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
						tempAdapter_2.Update(rdoTemp, rdoTemp.Tables[0].TableName);

						rdoTemp = null;
						intNumDocu++;

					}

					if ( !Convert.ToInt32(BGFicherosAportados.Instancia.fDocumentos(strClave)) != 0)
					{
						strMensaje = "El registro del fichero " + strFichero + " produjo un error de grabaci�n.    " + "\r";
                        RadMessageBox.Show(strMensaje, "Errores en proceso de grabaci�n", MessageBoxButtons.OK, RadMessageIcon.Info);
					}
					else
					{
						BGFicherosAportados.gNombreDoc = strClave;
						frmFicherosAportadosNuevo_Load(this, new EventArgs());
					}
					CargaGrid();
				}
				else
				{
                    RadMessageBox.Show(strMensaje, "Error al generar clave del registro.", MessageBoxButtons.OK, RadMessageIcon.Info);
				}
                vspFicheros.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
				cmbBotones[0].Enabled = true;

				txtNombreFichero.Text = "";
				txtDescripcion.Text = "";
				ErrGraba = false;
			}
			catch (Exception excep)
			{
				if (!ErrGraba)
				{
					throw excep;
				}

				if (ErrGraba)
				{

                    //    cnConexion.RollbackTrans
                    RadMessageBox.Show(excep.Message, "Error en proceso de grabaci�n", MessageBoxButtons.OK, RadMessageIcon.Info);
				}
			}
		}


		private void EliminaRegistros()
		{
			bool ErrBorra = false;
			string strNomDoc = String.Empty;
			int intNumDoc = 0;
			System.DateTime datFechAlta = DateTime.FromOADate(0);

			try
			{
				ErrBorra = true;


			    BGFicherosAportados.cnConexion.BeginTransScope();

                int tempForVar = vspFicheros.MaxRows;
				for (int intContador = 1; intContador <= tempForVar; intContador++)
				{
					vspFicheros.Row = intContador;
					vspFicheros.Col = Col_Chequeo;
					bool tempBool = false;
                    string auxVar = String.Empty;
                    auxVar = Convert.ToString(vspFicheros.Value);
					if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
					{
						vspFicheros.Col = Col_ClaDocu;
						strNomDoc = vspFicheros.Text;
						vspFicheros.Col = Col_FecCrea;
						datFechAlta = DateTime.Parse(vspFicheros.Text);

						vspFicheros.Col = Col_NumDocu;
						intNumDoc = Convert.ToInt32(Double.Parse(vspFicheros.Text));

						object tempRefParam = datFechAlta;
						SqlCommand tempCommand = new SqlCommand("DELETE DDOCUPDF " + 
						                         "WHERE gnombdoc = '" + strNomDoc + "' AND " + 
						                         "inumdocu = " + intNumDoc.ToString() + " AND " + 
						                         "fechalta = " + Serrores.FormatFechaHMS(tempRefParam), BGFicherosAportados.cnConexion);
						tempCommand.ExecuteNonQuery();
						datFechAlta = Convert.ToDateTime(tempRefParam);
					}
				}
			    BGFicherosAportados.cnConexion.CommitTransScope();

                ErrBorra = false;
			}
			catch (Exception excep)
			{
				if (!ErrBorra)
				{
					throw excep;
				}

				if (ErrBorra)
				{

			        BGFicherosAportados.cnConexion.RollbackTransScope();
                    RadMessageBox.Show(excep.Message, "Error en proceso de eliminaci�n", MessageBoxButtons.OK, RadMessageIcon.Info);

				}
			}
		}


		private bool fVisualiza()
		{
			int intNumDoc = 0;
			System.DateTime datFechAlta = DateTime.FromOADate(0);
			string strSql = String.Empty;
			DataSet rdoTemp = null;
			string varDocum = String.Empty;
			string strNomFichero = String.Empty;
			int intError = 0;

			try
			{
				int tempForVar = vspFicheros.MaxRows;
				for (int intContador = 1; intContador <= tempForVar; intContador++)
				{
					vspFicheros.Row = intContador;
					vspFicheros.Col = Col_Chequeo;
					bool tempBool = false;
                    string auxVar = String.Empty;
                    auxVar = Convert.ToString(vspFicheros.Value);
					
					if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
					{
						vspFicheros.Col = Col_NumDocu;
						intNumDoc = Convert.ToInt32(Double.Parse(vspFicheros.Text));
						vspFicheros.Col = Col_FecCrea;
						datFechAlta = DateTime.Parse(vspFicheros.Text);
						vspFicheros.Col = Col_NomDocu;
						strNomFichero = Path.GetDirectoryName(Application.ExecutablePath) + "\\tmp" + vspFicheros.Text;

						object tempRefParam = datFechAlta;
						strSql = "SELECT odocupdf FROM DDOCUPDF " + 
						         "WHERE gnombdoc = '" + BGFicherosAportados.gNombreDoc + "' AND " + 
						         "inumdocu = " + intNumDoc.ToString() + " AND " + 
						         "fechalta = " + Serrores.FormatFechaHMS(tempRefParam);
						datFechAlta = Convert.ToDateTime(tempRefParam);
						SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
						rdoTemp = new DataSet();
						tempAdapter.Fill(rdoTemp);

						if (rdoTemp.Tables[0].Rows.Count != 0)
						{
							//UPGRADE_ISSUE: (2068) rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
						 
							varDocum = RecuperarFichero((string) rdoTemp.Tables[0].Rows[0]["odocupdf"], strNomFichero);
							if (varDocum.Trim() != "")
							{
								string tempRefParam2 = "open";
								string tempRefParam3 = "";
								string tempRefParam4 = "";
								intError = UpgradeSupportHelper.PInvoke.SafeNative.shell32.ShellExecute(this.Handle, ref tempRefParam2, ref strNomFichero, ref tempRefParam3, ref tempRefParam4, 1);

								if (intError <= 31)
								{

									for (int intConError = 0; intConError <= arrError.GetUpperBound(0); intConError++)
									{
										if (intError == arrError[intConError, 0])
										{
                                            RadMessageBox.Show("- Error al procesar el fichero " + strNomFichero + "\r" + "\r" + arrError[intConError, 1].ToString(), "Error de proceso.", MessageBoxButtons.OK, RadMessageIcon.Info);
										}
									}
								}
								if (BGFicherosAportados.gstIdenPaciente.Trim() != "")
								{
									Serrores.GrabarDCONSUHC(BGFicherosAportados.gstIdenPaciente, "S", BGFicherosAportados.sUsuario, BGFicherosAportados.cnConexion);
								}
							}
						}
                        rdoTemp.Close();
					}
				}
			}
			catch (System.Exception excep)
			{
                RadMessageBox.Show(Information.Err().Number.ToString() + " - " + excep.Message, "Error al visualizar", MessageBoxButtons.OK, RadMessageIcon.Info);
			}
			return false;
		}


		public string RecuperarFichero(string varFichero, string strNomFichero)
		{
			bool error = false;
			string result = String.Empty;
			int intFichero = 0;
			byte[] bytFichero = null; //Variable a almacenar en Fichero

			try
			{
				error = true;




				//UPGRADE_ISSUE: (2064) RDO.rdoColumn property varFichero.Value was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				bytFichero = (byte[])UpgradeHelpers.Helpers.ArraysHelper.DeepCopy(varFichero);

				//'strNomFichero = App.Path & "\tmp" & strNomFichero
				if (FileSystem.Dir(strNomFichero, FileAttribute.Normal) != "")
				{
					File.Delete(strNomFichero);
				}

				intFichero = FileSystem.FreeFile();

				FileSystem.FileOpen(intFichero, strNomFichero, OpenMode.Binary, OpenAccess.Write, OpenShare.Default, -1);

				//lngLongitud = UBound(bytFichero())

				//Escribimos los bytes del array anterior, en el nuevo archivo ( archivo 2 )
				//UPGRADE_WARNING: (2080) Put was upgraded to FilePutObject and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				FileSystem.FilePutObject(intFichero, bytFichero, 0);


				FileSystem.FileClose(intFichero);

				result = strNomFichero;

				error = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!error)
				{
					throw excep;
				}

				if (error)
				{

                    RadMessageBox.Show(excep.Message, "Error al generar fichero temporal " + strNomFichero, MessageBoxButtons.OK, RadMessageIcon.Info);
					return "";

				}
			}
			return System.String.Empty;
		}



		private void CargaErrorShell()
		{
			//ERROR_FILE_NOT_FOUND : Fichero no encontrado
			//ERROR_PATH_NOT_FOUND : Directorio no encontrado
			//ERROR_BAD_FORMAT : Fichero .EXE inv�lido
			//SE_ERR_ACCESSDENIED:  Acceso denegado
			//SE_ERR_ASSOCINCOMPLETE : La asociaci�n del fichero es inv�lida o incompleta
			//SE_ERR_DDEBUSY : La opreaci�n DDE no puede ser completada porque se est� procesando otra operaci�n DDE
			//SE_ERR_DDEFAIL : La operaci�n DDE fall�
			//SE_ERR_DDETIMEOUT : Time out en la operaci�n DDE
			//SE_ERR_DLLNOTFOUND : Dll no encontrada
			//SE_ERR_FNF : Fichero no encontrado
			//SE_ERR_NOASSOC : No hay aplicaci�n asociada con la extensi�n dada
			//SE_ERR_OOM : No hay memoria suficiente para completar la operaci�n
			//SE_ERR_PNF El camino especificado no fue encontrado
			//SE_ERR_SHARE Violaci�n de compartici�n


			arrError[0, 0] = SE_ERR_ACCESSDENIED;
			arrError[0, 1] = Convert.ToInt32(Double.Parse("Fichero no encontrado."));

			arrError[1, 0] = SE_ERR_ASSOCINCOMPLETE;
			arrError[1, 1] = Convert.ToInt32(Double.Parse("Directorio no encontrado."));

			arrError[2, 0] = ERROR_BAD_FORMAT;
			arrError[2, 1] = Convert.ToInt32(Double.Parse("Fichero .EXE inv�lido."));

			arrError[3, 0] = SE_ERR_ACCESSDENIED;
			arrError[3, 1] = Convert.ToInt32(Double.Parse("Acceso denegado."));

			arrError[4, 0] = SE_ERR_ASSOCINCOMPLETE;
			arrError[4, 1] = Convert.ToInt32(Double.Parse("La asociaci�n del fichero es inv�lida o incompleta."));

			arrError[5, 0] = SE_ERR_DDEBUSY;
			arrError[5, 1] = Convert.ToInt32(Double.Parse("La opreaci�n DDE no puede ser completada porque se est� procesando otra operaci�n DDE."));

			arrError[6, 0] = SE_ERR_DDEFAIL;
			arrError[6, 1] = Convert.ToInt32(Double.Parse("La operaci�n DDE fall�."));

			arrError[7, 0] = SE_ERR_DDETIMEOUT;
			arrError[7, 1] = Convert.ToInt32(Double.Parse("Time out en la operaci�n DDE."));

			arrError[8, 0] = SE_ERR_DLLNOTFOUND;
			arrError[8, 1] = Convert.ToInt32(Double.Parse("Dll no encontrada."));

			arrError[9, 0] = SE_ERR_FNF;
			arrError[9, 1] = Convert.ToInt32(Double.Parse("Fichero no encontrado."));

			arrError[10, 0] = SE_ERR_NOASSOC;
			arrError[10, 1] = Convert.ToInt32(Double.Parse("No hay aplicaci�n asociada con la extensi�n dada."));

			arrError[11, 0] = SE_ERR_OOM;
			arrError[11, 1] = Convert.ToInt32(Double.Parse("No hay memoria suficiente para completar la operaci�n."));

			arrError[12, 0] = SE_ERR_PNF;
			arrError[12, 1] = Convert.ToInt32(Double.Parse("El camino especificado no fue encontrado"));

			arrError[13, 0] = SE_ERR_SHARE;
			arrError[13, 1] = Convert.ToInt32(Double.Parse("Violaci�n de compartici�n."));

		}


		//UPGRADE_NOTE: (7001) The following declaration (fDescrip) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private bool fDescrip(string pstClave, string pstDescri)
		//{
				//
				//bool result = false;
				//string stSql = "SELECT * FROM DDOCUPDF " + 
				//               "WHERE gnombdoc = '" + pstClave + "'";
				//SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, BGFicherosAportados.cnConexion);
				//DataSet rdTemp = new DataSet();
				//tempAdapter.Fill(rdTemp);
				//
				//result = rdTemp.Tables[0].Rows.Count != 0;
				//
				////UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//rdTemp.Close();
				//
				//return result;
		//}

		private void ActualizaDatos()
		{

			vspFicheros.Row = vspFicheros.ActiveRowIndex;
			vspFicheros.Col = Col_ClaDocu;
			string stDocum = vspFicheros.Text;
			vspFicheros.Col = Col_NumDocu;
			int inDocum = Convert.ToInt32(Double.Parse(vspFicheros.Text));
			vspFicheros.Col = Col_Descrip;

			string stSql = "UPDATE DDOCUPDF " + 
			               "SET ddescrip = '" + txtDescripcion.Text + "' " + 
			               "WHERE gnombdoc = '" + stDocum + "' AND " + 
			               "inumdocu = " + inDocum.ToString();

			SqlCommand tempCommand = new SqlCommand(stSql, BGFicherosAportados.cnConexion);
			tempCommand.ExecuteNonQuery();
			frmFicherosAportadosNuevo_Load(this, new EventArgs());


		}



        private void vspFicheros_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {

            int Col = 0;
            int Row = 0;
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = vspFicheros.Col;
                Row = vspFicheros.Row;
            }
              
            //         int Col = eventArgs.Column;
            //int Row = eventArgs.Row;
            if (Row == 0)
			{
				return;
			}
			//Si ha pinchado en una fila...
			vspguardar.Visible = false;
			vspguardar.Enabled = false;
			txtDescripcion.Enabled = true;
			txtNombreFichero.Enabled = true;
			if (Row > 0 && Col > 0)
			{
				string auxVar = String.Empty;
				if ((registro_seleccionado && row_ultima == Row) || Col == Col_Chequeo)
				{
                    vspFicheros.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly; 
					cmbBotones[0].Enabled = true;
					txtNombreFichero.Text = "";
					txtDescripcion.Text = "";
					registro_seleccionado = false;
					row_ultima = 0;
					if (Col == Col_Chequeo)
					{
						vspFicheros.Row = Row;
						vspFicheros.Col = Col;
						bool tempBool = false;
						auxVar = Convert.ToString(vspFicheros.Value);
						vspFicheros.Value = !((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)));
					}
				}
				else
				{
					vspFicheros.OperationMode = (UpgradeHelpers.Spread.OperationModeEnums) (((int)UpgradeHelpers.Spread.OperationModeEnums.ReadOnly) + ((int)UpgradeHelpers.Spread.OperationModeEnums.RowMode));
					vspFicheros.Row = Row;
					vspFicheros.Col = Col_Descrip;
					txtDescripcion.Text = vspFicheros.Text;
					vspFicheros.Col = Col_NomDocu;
					txtNombreFichero.Text = vspFicheros.Text;
					cmbBotones[0].Enabled = false;
					//            cmbBotones(1).Enabled = True
					//            cmbBotones(3).Enabled = True
					//            cmbBotones(4).Enabled = True
					registro_seleccionado = true;
					row_ultima = Row;
				}
			}

		}


		private void vspFicheros_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (KeyCode == ((int) Keys.Up))
			{
				if (vspFicheros.ActiveRowIndex > 1)
				{
                    vspFicheros.Col = 2;
                    vspFicheros.Row = vspFicheros.ActiveRowIndex;
                    vspFicheros_CellClick(vspFicheros, null);
				}
				else
				{
					KeyCode = 0;
				}
			}
			if (KeyCode == ((int) Keys.Down))
			{
				if (vspFicheros.ActiveRowIndex < vspFicheros.MaxRows)
				{
                    vspFicheros.Col = 2;
                    vspFicheros.Row = vspFicheros.ActiveRowIndex;

                    vspFicheros_CellClick(vspFicheros, null);
				}
				else
				{
					KeyCode = 0;
				}
			}

		}

		private void vspFicheros_MouseDown(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float x = (float) (eventArgs.X * 15);
			float y = (float) (eventArgs.Y * 15);
			vlposy = Convert.ToInt32(y);
		}

		private void vspFicheros_MouseUp(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float x = (float) (eventArgs.X * 15);
			float y = (float) (eventArgs.Y * 15);
			if (Button == 1 && registro_seleccionado && vlposy != y)
			{
                vspFicheros.Col = 2;
                vspFicheros.Row = vspFicheros.ActiveRowIndex;

                vspFicheros_CellClick(vspFicheros, null);
			}
		}
		private void frmFicherosAportadosNuevo_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}
	}
}