using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace FicherosAportados
{
	public class MCFicherosAportados
	{


		frmFicherosAportados frmFicherosA = null;
		frmFicherosAportadosNuevo frmFicherosN = null;



		public void Llamada(SqlConnection pConexion, string pUsuario, int pAnoRegi, int pNumRegi, string pIdenPac, string pPaciente, string pNombreDoc, string ParaPathAplicacion, object pObjAnt, string pObserva)
		{

			//'pDnsSqlServer As String,

			BGFicherosAportados.Instancia = pObjAnt;
			BGFicherosAportados.cnConexion = pConexion;


			BGFicherosAportados.gstIdenPaciente = pIdenPac;
			BGFicherosAportados.giGanoregi = pAnoRegi;
			BGFicherosAportados.glGnumregi = pNumRegi;
			BGFicherosAportados.sUsuario = pUsuario;
			BGFicherosAportados.gNombreDoc = pNombreDoc;
			BGFicherosAportados.gPaciente = pPaciente;
			BGFicherosAportados.gObserva = pObserva;
			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(BGFicherosAportados.cnConexion);
			Serrores.AjustarRelojCliente(BGFicherosAportados.cnConexion);
			Serrores.vAyuda = Serrores.ObtenerFicheroAyuda();


			//'stNombreDsn = paraDSNSQL
			BGFicherosAportados.stPathAplicacion = ParaPathAplicacion;

			CDAyuda.Instance.setHelpFile(frmFicherosA, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

            string strSql = "SELECT isnull(valfanu1,'A') as pantalla FROM  sconsglo WHERE gconsglo = 'FICHAPOR'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			string stPantalla = "A";
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				stPantalla = Convert.ToString(rdoTemp.Tables[0].Rows[0]["pantalla"]).Trim().ToUpper();
			}

			rdoTemp.Close();

			if (stPantalla == "A")
			{
				frmFicherosA = new frmFicherosAportados();
				frmFicherosA.ShowDialog();
			}
			else
			{
				frmFicherosN = new frmFicherosAportadosNuevo();
				frmFicherosN.ShowDialog();
			}

		}

		public MCFicherosAportados()
		{

			try
			{
				BGFicherosAportados.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{

				MessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}
		}


		~MCFicherosAportados()
		{
			BGFicherosAportados.clase_mensaje = null;
			frmFicherosA = null;
			frmFicherosN = null;

		}

		//UPGRADE_NOTE: (7001) The following declaration (Inicializar_Datos) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void Inicializar_Datos()
		//{
				//BGFicherosAportados.stNombre = "";
				//BGFicherosAportados.stApellido1 = "";
				//BGFicherosAportados.stApellido2 = "";
				//BGFicherosAportados.stFnac = "";
				//BGFicherosAportados.stSexo = "";
				//BGFicherosAportados.stDomicilio = "";
				//BGFicherosAportados.stNIF = "";
				//BGFicherosAportados.stIdPaciente = "";
				//BGFicherosAportados.stCodPostal = "";
				//BGFicherosAportados.stAsegurado = "";
				//BGFicherosAportados.stCodPob = "";
				//BGFicherosAportados.stFnac = "";
				//BGFicherosAportados.stHistoria = "";
				//BGFicherosAportados.stCodSociedad = "";
				//BGFicherosAportados.stFilProv = "";
				//BGFicherosAportados.stCodInspec = "";
		//}


		public void Recoger_Datos(string Nombre2, string apellido1, string apellido2, string fechaNac, string sexo, string domicilio, string NIF, string IdPaciente, string CodPostal, string Asegurado, string codPob, string Historia, string codsoc, string filprov, object IndActPen = null, string Inspec = "")
		{

			//Paso de los datos obtenidos por el ActiveX a variables p�blicas
			BGFicherosAportados.stNombre = Nombre2.Trim();
			BGFicherosAportados.stApellido1 = apellido1.Trim();
			BGFicherosAportados.stApellido2 = apellido2.Trim();
			BGFicherosAportados.stFnac = fechaNac.Trim();
			BGFicherosAportados.stSexo = sexo.Trim();
			BGFicherosAportados.stDomicilio = domicilio.Trim();
			BGFicherosAportados.stNIF = NIF.Trim();
			BGFicherosAportados.stIdPaciente = IdPaciente.Trim();
			BGFicherosAportados.stCodPostal = CodPostal.Trim();
			BGFicherosAportados.stAsegurado = Asegurado.Trim();
			BGFicherosAportados.stCodPob = codPob.Trim();
			BGFicherosAportados.stHistoria = Historia.Trim();
			BGFicherosAportados.stCodSociedad = codsoc.Trim();
			BGFicherosAportados.stFilProv = filprov.Trim();
			//stIndActPen = IndActPen
			BGFicherosAportados.stCodInspec = Inspec.Trim();

		}


        //UPGRADE_NOTE: (7001) The following declaration (fPacBloq) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
        //private bool fPacBloq()
        //{
        //    bool ErrPacBloq = false;
        //    bool result = false;
        //    string strSql = String.Empty;
        //    DataSet rdoTemp = null;


        //    try
        //    {
        //        ErrPacBloq = true;

        //        strSql = "SELECT isnull(ibloqueo,'N') as ibloqueo FROM  DPACIENT WHERE GIDENPAC = '" + BGFicherosAportados.stIdPaciente + "'";

        //        SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
        //        rdoTemp = new DataSet();
        //        tempAdapter.Fill(rdoTemp);

        //        if (rdoTemp.Tables[0].Rows.Count != 0)
        //        {
        //            if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["ibloqueo"]).Trim().ToUpper() == "S")
        //            {
        //                result = true;
        //                string tempRefParam = "Registro de actividad masiva sobre un paciente.";
        //                short tempRefParam2 = 3920;
        //                object tempRefParam3 = new object { };
        //                BGFicherosAportados.clase_mensaje.RespuestaMensaje(tempRefParam, Serrores.vAyuda, tempRefParam2, BGFicherosAportados.cnConexion, tempRefParam3);
        //            }
        //        }

        //        rdoTemp.Close();
        //        rdoTemp = null;
        //        ErrPacBloq = false;

        //        return result;
        //    }
        //    catch (Exception excep)
        //    {
        //        if (!ErrPacBloq)
        //        {
        //            throw excep;
        //        }

        //        if (ErrPacBloq)
        //        {

        //            MessageBox.Show(excep.Message, "Registro de actividad masiva sobre un paciente.", MessageBoxButtons.OK, MessageBoxIcon.Error);

        //            return result;
        //        }
        //    }
        //    return false;
        //}
    }
}