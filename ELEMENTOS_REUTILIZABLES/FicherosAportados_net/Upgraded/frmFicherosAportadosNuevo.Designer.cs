using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace FicherosAportados
{
	partial class frmFicherosAportadosNuevo
	{

		#region "Upgrade Support "
		private static frmFicherosAportadosNuevo m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmFicherosAportadosNuevo DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmFicherosAportadosNuevo();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_cmbBotones_4", "_cmbBotones_3", "_cmbBotones_2", "_cmbBotones_1", "vspFicheros", "vspguardar", "txtNombreFichero", "txtDescripcion", "_cmbBotones_0", "Label2", "Label1", "Frame3", "Frame2", "_lblTextos_0", "_lblTextos_1", "_lblTextos_3", "_lblTextos_4", "frame1", "cmbBotones", "lblTextos", "commandButtonHelper1", "vspguardar_Sheet1", "vspFicheros_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		//public System.Windows.Forms.OpenFileDialog CommonDialog1Open;
        public System.Windows.Forms.OpenFileDialog CommonDialog1Open;
        public System.Windows.Forms.SaveFileDialog CommonDialog1Save;
		public System.Windows.Forms.FontDialog CommonDialog1Font;
		public System.Windows.Forms.ColorDialog CommonDialog1Color;
		public System.Windows.Forms.PrintDialog CommonDialog1Print;
		private Telerik.WinControls.UI.RadButton _cmbBotones_4;
		private Telerik.WinControls.UI.RadButton _cmbBotones_3;
		private Telerik.WinControls.UI.RadButton _cmbBotones_2;
		private Telerik.WinControls.UI.RadButton _cmbBotones_1;
		public UpgradeHelpers.Spread.FpSpread vspFicheros;
		public UpgradeHelpers.Spread.FpSpread vspguardar;
		public Telerik.WinControls.UI.RadTextBox txtNombreFichero;
		public Telerik.WinControls.UI.RadTextBox txtDescripcion;
		private Telerik.WinControls.UI.RadButton _cmbBotones_0;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public System.Windows.Forms.GroupBox Frame3;
		public System.Windows.Forms.GroupBox Frame2;
		private Telerik.WinControls.UI.RadLabel _lblTextos_0;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_1;
		private Telerik.WinControls.UI.RadLabel _lblTextos_3;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_4;
		public System.Windows.Forms.GroupBox frame1;
		public Telerik.WinControls.UI.RadButton[] cmbBotones = new Telerik.WinControls.UI.RadButton[5];
		public Telerik.WinControls.UI.RadLabel[] lblTextos = new Telerik.WinControls.UI.RadLabel[5];
		//private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
		//private FarPoint.Win.Spread.SheetView vspguardar_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView vspFicheros_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFicherosAportadosNuevo));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			//this.CommonDialog1Open = new System.Windows.Forms.OpenFileDialog();
            this.CommonDialog1Open = new System.Windows.Forms.OpenFileDialog();
            this.CommonDialog1Save = new System.Windows.Forms.SaveFileDialog();
			this.CommonDialog1Font = new System.Windows.Forms.FontDialog();
			this.CommonDialog1Color = new System.Windows.Forms.ColorDialog();
			this.CommonDialog1Print = new System.Windows.Forms.PrintDialog();
			this.frame1 = new System.Windows.Forms.GroupBox();
			this._cmbBotones_4 = new Telerik.WinControls.UI.RadButton();
			this._cmbBotones_3 = new Telerik.WinControls.UI.RadButton();
			this._cmbBotones_2 = new Telerik.WinControls.UI.RadButton();
			this._cmbBotones_1 = new Telerik.WinControls.UI.RadButton();
			this.Frame2 = new System.Windows.Forms.GroupBox();
			this.vspFicheros = new UpgradeHelpers.Spread.FpSpread();
			this.Frame3 = new System.Windows.Forms.GroupBox();
			this.vspguardar = new UpgradeHelpers.Spread.FpSpread();
			this.txtNombreFichero = new Telerik.WinControls.UI.RadTextBox();
			this.txtDescripcion = new Telerik.WinControls.UI.RadTextBox();
			this._cmbBotones_0 = new Telerik.WinControls.UI.RadButton();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_0 = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_1 = new Telerik.WinControls.UI.RadTextBox();
			this._lblTextos_3 = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_4 = new Telerik.WinControls.UI.RadTextBox();
			this.frame1.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.Frame3.SuspendLayout();
			this.SuspendLayout();
			//this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
			// 
			// frame1
			// 
			//this.frame1.BackColor = System.Drawing.SystemColors.Control;
			this.frame1.Controls.Add(this._cmbBotones_4);
			this.frame1.Controls.Add(this._cmbBotones_3);
			this.frame1.Controls.Add(this._cmbBotones_2);
			this.frame1.Controls.Add(this._cmbBotones_1);
			this.frame1.Controls.Add(this.Frame2);
			this.frame1.Controls.Add(this._lblTextos_0);
			this.frame1.Controls.Add(this._lblTextos_1);
			this.frame1.Controls.Add(this._lblTextos_3);
			this.frame1.Controls.Add(this._lblTextos_4);
			this.frame1.Enabled = true;
			//this.frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.frame1.Location = new System.Drawing.Point(1, -2);
			this.frame1.Name = "frame1";
			this.frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frame1.Size = new System.Drawing.Size(675, 457);
			this.frame1.TabIndex = 0;
			this.frame1.Visible = true;
			// 
			// _cmbBotones_4
			// 
			//this._cmbBotones_4.BackColor = System.Drawing.SystemColors.Control;
			this._cmbBotones_4.Cursor = System.Windows.Forms.Cursors.Default;
			//this._cmbBotones_4.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_4.Location = new System.Drawing.Point(370, 420);
			this._cmbBotones_4.Name = "_cmbBotones_4";
			this._cmbBotones_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_4.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_4.TabIndex = 3;
			this._cmbBotones_4.Text = "&Eliminar";
			this._cmbBotones_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this._cmbBotones_4.UseVisualStyleBackColor = false;
			this._cmbBotones_4.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_4.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _cmbBotones_3
            // 
            //this._cmbBotones_3.BackColor = System.Drawing.SystemColors.Control;
			this._cmbBotones_3.Cursor = System.Windows.Forms.Cursors.Default;
			//this._cmbBotones_3.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_3.Location = new System.Drawing.Point(446, 420);
			this._cmbBotones_3.Name = "_cmbBotones_3";
			this._cmbBotones_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_3.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_3.TabIndex = 4;
			this._cmbBotones_3.Text = "&Visualizar";
			this._cmbBotones_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this._cmbBotones_3.UseVisualStyleBackColor = false;
			this._cmbBotones_3.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_3.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _cmbBotones_2
            // 
            //this._cmbBotones_2.BackColor = System.Drawing.SystemColors.Control;
			this._cmbBotones_2.Cursor = System.Windows.Forms.Cursors.Default;
			//this._cmbBotones_2.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_2.Location = new System.Drawing.Point(598, 420);
			this._cmbBotones_2.Name = "_cmbBotones_2";
			this._cmbBotones_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_2.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_2.TabIndex = 6;
			this._cmbBotones_2.Text = "&Salir";
			this._cmbBotones_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this._cmbBotones_2.UseVisualStyleBackColor = false;
			this._cmbBotones_2.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_2.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _cmbBotones_1
            // 
            //this._cmbBotones_1.BackColor = System.Drawing.SystemColors.Control;
			this._cmbBotones_1.Cursor = System.Windows.Forms.Cursors.Default;
			//this._cmbBotones_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_1.Location = new System.Drawing.Point(524, 420);
			this._cmbBotones_1.Name = "_cmbBotones_1";
			this._cmbBotones_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_1.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_1.TabIndex = 5;
			this._cmbBotones_1.Text = "&Aceptar";
			this._cmbBotones_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this._cmbBotones_1.UseVisualStyleBackColor = false;
			this._cmbBotones_1.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_1.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame2
            // 
            //this.Frame2.BackColor = System.Drawing.SystemColors.Control;
			this.Frame2.Controls.Add(this.vspFicheros);
			this.Frame2.Controls.Add(this.Frame3);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(8, 40);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(661, 375);
			this.Frame2.TabIndex = 11;
			this.Frame2.Visible = true;
			// 
			// vspFicheros
			// 
			this.vspFicheros.Location = new System.Drawing.Point(4, 14);
			this.vspFicheros.Name = "vspFicheros";
			this.vspFicheros.Size = new System.Drawing.Size(651, 179);
			this.vspFicheros.TabIndex = 1;
			this.vspFicheros.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.vspFicheros_CellClick);
			this.vspFicheros.KeyDown += new System.Windows.Forms.KeyEventHandler(this.vspFicheros_KeyDown);
			this.vspFicheros.MouseDown += new System.Windows.Forms.MouseEventHandler(this.vspFicheros_MouseDown);
			this.vspFicheros.MouseUp += new System.Windows.Forms.MouseEventHandler(this.vspFicheros_MouseUp);
			// 
			// Frame3
			// 
			//this.Frame3.BackColor = System.Drawing.SystemColors.Control;
			this.Frame3.Controls.Add(this.vspguardar);
			this.Frame3.Controls.Add(this.txtNombreFichero);
			this.Frame3.Controls.Add(this.txtDescripcion);
			this.Frame3.Controls.Add(this._cmbBotones_0);
			this.Frame3.Controls.Add(this.Label2);
			this.Frame3.Controls.Add(this.Label1);
			this.Frame3.Enabled = true;
			//this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame3.Location = new System.Drawing.Point(5, 202);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(651, 165);
			this.Frame3.TabIndex = 12;
			this.Frame3.Visible = true;
			// 
			// vspguardar
			// 
			this.vspguardar.Location = new System.Drawing.Point(6, 18);
			this.vspguardar.Name = "vspguardar";
			this.vspguardar.Size = new System.Drawing.Size(602, 139);
			this.vspguardar.TabIndex = 14;
			this.vspguardar.Visible = false;
			// 
			// txtNombreFichero
			// 
			this.txtNombreFichero.AcceptsReturn = true;
			this.txtNombreFichero.BackColor = System.Drawing.SystemColors.Menu;
			//this.txtNombreFichero.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.txtNombreFichero.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtNombreFichero.ForeColor = System.Drawing.SystemColors.WindowText;
			this.txtNombreFichero.Location = new System.Drawing.Point(90, 24);
			this.txtNombreFichero.MaxLength = 0;
			this.txtNombreFichero.Name = "txtNombreFichero";
			this.txtNombreFichero.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtNombreFichero.Size = new System.Drawing.Size(518, 26);
			this.txtNombreFichero.TabIndex = 15;
			// 
			// txtDescripcion
			// 
			this.txtDescripcion.AcceptsReturn = true;
			this.txtDescripcion.BackColor = System.Drawing.SystemColors.Window;
			//this.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.txtDescripcion.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtDescripcion.ForeColor = System.Drawing.SystemColors.WindowText;
			this.txtDescripcion.Location = new System.Drawing.Point(90, 60);
			this.txtDescripcion.MaxLength = 100;
			this.txtDescripcion.Multiline = true;
			this.txtDescripcion.Name = "txtDescripcion";
			this.txtDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtDescripcion.Size = new System.Drawing.Size(518, 88);
			this.txtDescripcion.TabIndex = 13;
			this.txtDescripcion.WordWrap = false;
			// 
			// _cmbBotones_0
			// 
			//this._cmbBotones_0.BackColor = System.Drawing.SystemColors.Control;
			this._cmbBotones_0.Cursor = System.Windows.Forms.Cursors.Default;
			//this._cmbBotones_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_0.Image = (System.Drawing.Image) resources.GetObject("_cmbBotones_0.Image");
			this._cmbBotones_0.Location = new System.Drawing.Point(609, 18);
			this._cmbBotones_0.Name = "_cmbBotones_0";
			this._cmbBotones_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_0.Size = new System.Drawing.Size(38, 37);
			this._cmbBotones_0.TabIndex = 2;
			this._cmbBotones_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cmbBotones_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this._cmbBotones_0.UseVisualStyleBackColor = false;
			this._cmbBotones_0.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_0.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label2
            // 
            //this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(19, 65);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(65, 13);
			this.Label2.TabIndex = 17;
			this.Label2.Text = "Descripción";
			// 
			// Label1
			// 
			//this.Label1.BackColor = System.Drawing.SystemColors.Control;
			//this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(20, 32);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(55, 20);
			this.Label1.TabIndex = 16;
			this.Label1.Text = "Archivo";
			// 
			// _lblTextos_0
			// 
			//this._lblTextos_0.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_0.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_0.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_0.Location = new System.Drawing.Point(16, 18);
			this._lblTextos_0.Name = "_lblTextos_0";
			this._lblTextos_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_0.Size = new System.Drawing.Size(49, 17);
			this._lblTextos_0.TabIndex = 10;
			this._lblTextos_0.Text = "Paciente:";
			// 
			// _lblTextos_1
			// 
			//this._lblTextos_1.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._lblTextos_1.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_1.Location = new System.Drawing.Point(70, 18);
			this._lblTextos_1.Name = "_lblTextos_1";
			this._lblTextos_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_1.Size = new System.Drawing.Size(419, 17);
			this._lblTextos_1.TabIndex = 9;
            this._lblTextos_1.Enabled = false;
			// 
			// _lblTextos_3
			// 
			//this._lblTextos_3.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_3.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_3.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_3.Location = new System.Drawing.Point(500, 18);
			this._lblTextos_3.Name = "_lblTextos_3";
			this._lblTextos_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_3.Size = new System.Drawing.Size(69, 17);
			this._lblTextos_3.TabIndex = 8;
			this._lblTextos_3.Text = "Hist. Clínica:";
			// 
			// _lblTextos_4
			// 
			//this._lblTextos_4.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._lblTextos_4.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_4.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_4.Location = new System.Drawing.Point(572, 18);
			this._lblTextos_4.Name = "_lblTextos_4";
			this._lblTextos_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_4.Size = new System.Drawing.Size(97, 17);
			this._lblTextos_4.TabIndex = 7;
            this._lblTextos_4.Enabled = false;
			// 
			// frmFicherosAportadosNuevo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(679, 461);
			this.Controls.Add(this.frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmFicherosAportadosNuevo";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Documentación Entregada/Aportada – Ficheros ";
			//commandButtonHelper1.SetStyle(this._cmbBotones_0, 1);
			this.Closed += new System.EventHandler(this.frmFicherosAportadosNuevo_Closed);
			this.Load += new System.EventHandler(this.frmFicherosAportadosNuevo_Load);
			this.frame1.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializelblTextos();
			InitializecmbBotones();
		}
		void InitializelblTextos()
		{
			this.lblTextos = new Telerik.WinControls.UI.RadLabel[5];
			this.lblTextos[0] = _lblTextos_0;
			//this.lblTextos[1] = _lblTextos_1;
			this.lblTextos[3] = _lblTextos_3;
			//this.lblTextos[4] = _lblTextos_4;
		}
		void InitializecmbBotones()
		{
			this.cmbBotones = new Telerik.WinControls.UI.RadButton[5];
			this.cmbBotones[4] = _cmbBotones_4;
			this.cmbBotones[3] = _cmbBotones_3;
			this.cmbBotones[2] = _cmbBotones_2;
			this.cmbBotones[1] = _cmbBotones_1;
			this.cmbBotones[0] = _cmbBotones_0;
		}
		#endregion
	}
}