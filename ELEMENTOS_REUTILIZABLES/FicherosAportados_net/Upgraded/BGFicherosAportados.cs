using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Microsoft.CSharp;
using ElementosCompartidos;

namespace FicherosAportados
{
	internal static class BGFicherosAportados
	{

		public static Mensajes.ClassMensajes clase_mensaje = null;

		public static string VstUsuario_NT = String.Empty; //Usuario de NT

		public static mbAcceso.strEventoNulo[] astreventos = null; //Array para el control de acceso en los eventos
		public static mbAcceso.strControlOculto[] astrcontroles = null; //Array para el control de acceso de los controles

		//************ O.Frias Abrir y cerrar un fichero.

		public static string vstSeparadorDecimal = String.Empty;
		public static string stDecimalBD = String.Empty;

		public static string lSep_Fecha = String.Empty; //SEPARADOR FECHA CONFIGURACION REGIONAL
		public static string lSep_Hora = String.Empty; //SEPARADOR HORA CONFIGURACION REGIONAL
		public static string lFor_fecha = String.Empty; // FORMATO CONFIGURACION REGIONAL FECHA SOLO
		public static string LFOR_HORA = String.Empty; //FORMATO DE CONFIGURACION DE HORA
		public static string lEsp_For_Siglo = String.Empty; // MOSTRAR SIGLO O NO
		public static int LNumero_Ceros = 0; // numero de ceros de las horas
		public static int iFormato_Fecha = 0; // entero que identifica al formato  de fecha -> vaspread.typedateformat
		public static string lSep_Decimal = String.Empty; // separador decimal (triage temperatura y peso)
		public static string FechaHora_Sistema = String.Empty;


		public const int LOCALE_SDATE = 0x1D; //
		public const int LOCALE_STIME = 0x1E;
		public const int LOCALE_SSHORTDATE = 0x1F;
		public const int LOCALE_STIMEFORMAT = 0x1003; //formato de hora
		public const int LOCALE_SYSTEM_DEFAULT = 0x800;
		public const int LOCALE_ICENTURY = 0x24; //cuatro cifras de a�o
		public const int LOCALE_ITLZERO = 0x25; // numero de ceros en la hora
		public const int LOCALE_IDATE = 0x21; // entero que devuelve el formato de fecha
		public const int LOCALE_ITIME = 0x23; // ENTERO QUE IDENTIFICA A LA HORA
		public const int LOCALE_SDECIMAL = 0xE; // devuelve el caracter decimal


		public static string VstSeparadorFecha_BaseDatos = String.Empty;
		public static string VstSeparadorHora_BaseDatos = String.Empty;
		public static string VstFormato_BaseDatos = String.Empty;


		public static SqlConnection cnConexion = null;
		public static object cryListado = null;
		public static string sUsuario = String.Empty;
		public static string stNombreDsn = String.Empty;
		public static string stNombreDsnA = String.Empty;
		public static string stNombreBDAccess = String.Empty;
		public static string stPathAplicacion = String.Empty;


		public static string gstIdenPaciente = String.Empty;
		public static int giGanoregi = 0;
		public static int glGnumregi = 0;
		public static dynamic Instancia = null;
		public static string gNombreDoc = String.Empty;
		public static string gPaciente = String.Empty;
		public static string gObserva = String.Empty;

		public static string stNombre = String.Empty;
		public static string stApellido1 = String.Empty;
		public static string stApellido2 = String.Empty;
		public static string stFnac = String.Empty;
		public static string stSexo = String.Empty;
		public static string stDomicilio = String.Empty;
		public static string stNIF = String.Empty;
		public static string stIdPaciente = String.Empty;
		public static string stCodPostal = String.Empty;
		public static string stAsegurado = String.Empty;
		public static string stCodPob = String.Empty;
		public static string stHistoria = String.Empty;
		public static string stCodSociedad = String.Empty;
		public static string stFilProv = String.Empty;
		public static string stCodInspec = String.Empty;



		static string El_titulo = String.Empty;
		//Constantes para usar con SendMessage
		const int WM_SYSCOMMAND = 0x112;
		const int SC_CLOSE = 0xF060;
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		// Copyright �1996-2009 VBnet, Randy Birch, All Rights Reserved.
		// Some pages may also contain other copyrights by the author.
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		// Distribution: You can freely use this code in your own
		//               applications, but you may not reproduce
		//               or publish this code on any web site,
		//               online service, or distribute as source
		//               on any media without express permission.
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		public const int OFN_ALLOWMULTISELECT = 0x200;
		public const int OFN_CREATEPROMPT = 0x2000;
		//Public Const OFN_ENABLEHOOK As Long = &H20
		//Public Const OFN_ENABLETEMPLATE As Long = &H40
		//Public Const OFN_ENABLETEMPLATEHANDLE As Long = &H80
		public const int OFN_EXPLORER = 0x80000;
		//Public Const OFN_EXTENSIONDIFFERENT As Long = &H400
		//Public Const OFN_FILEMUSTEXIST As Long = &H1000
		//Public Const OFN_HIDEREADONLY As Long = &H4
		public const int OFN_LONGNAMES = 0x200000;
		//Public Const OFN_NOCHANGEDIR As Long = &H8
		public const int OFN_NODEREFERENCELINKS = 0x100000;
		//Public Const OFN_NOLONGNAMES As Long = &H40000
		//Public Const OFN_NONETWORKBUTTON As Long = &H20000
		//Public Const OFN_NOREADONLYRETURN As Long = &H8000& 'see comments
		//Public Const OFN_NOTESTFILECREATE As Long = &H10000
		//Public Const OFN_NOVALIDATE As Long = &H100
		//Public Const OFN_OVERWRITEPROMPT As Long = &H2
		//Public Const OFN_PATHMUSTEXIST As Long = &H800
		public const int OFN_READONLY = 0x1;
		//Public Const OFN_SHAREAWARE As Long = &H4000
		//Public Const OFN_SHAREFALLTHROUGH As Long = 2
		//Public Const OFN_SHAREWARN As Long = 0
		//Public Const OFN_SHARENOWARN As Long = 1
		//Public Const OFN_SHOWHELP As Long = &H10
		public const int OFS_MAXPATHNAME = 260;

		//OFS_FILE_OPEN_FLAGS and OFS_FILE_SAVE_FLAGS below
		//are mine to save long statements; they're not
		//a standard Win32 type.
		public static readonly int OFS_FILE_OPEN_FLAGS = OFN_EXPLORER | OFN_LONGNAMES | OFN_CREATEPROMPT | OFN_NODEREFERENCELINKS;

		//Public Const OFS_FILE_SAVE_FLAGS = OFN_EXPLORER _
		//Or OFN_LONGNAMES _
		//Or OFN_OVERWRITEPROMPT _
		//Or OFN_HIDEREADONLY

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[StructLayout(LayoutKind.Sequential)]
		//public struct OPENFILENAME
		//{
			//public int nStructSize;
			//public int hWndOwner;
			//public int hInstance;
			//public string sFilter;
			//public string sCustomFilter;
			//public int nMaxCustFilter;
			//public int nFilterIndex;
			//public string sFile;
			//public int nMaxFile;
			//public string sFileTitle;
			//public int nMaxTitle;
			//public string sInitialDir;
			//public string sDialogTitle;
			//public int flags;
			//public short nFileOffset;
			//public short nFileExtension;
			//public string sDefFileExt;
			//public int nCustData;
			//public int fnHook;
			//public string sTemplateName;
			//private static void InitStruct(ref OPENFILENAME result, bool init)
			//{
					//if (init)
					//{
						//result.sFilter = String.Empty;
						//result.sCustomFilter = String.Empty;
						//result.sFile = String.Empty;
						//result.sFileTitle = String.Empty;
						//result.sInitialDir = String.Empty;
						//result.sDialogTitle = String.Empty;
						//result.sDefFileExt = String.Empty;
						//result.sTemplateName = String.Empty;
					//}
			//}
			//public static OPENFILENAME CreateInstance()
			//{
					//OPENFILENAME result = new OPENFILENAME();
					//InitStruct(ref result, true);
					//return result;
			//}
			//public OPENFILENAME Clone()
			//{
					//OPENFILENAME result = this;
					//InitStruct(ref result, false);
					//return result;
			//}
		//};

		public static UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OPENFILENAME OFN = UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.OPENFILENAME.CreateInstance();

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		////UPGRADE_TODO: (1050) Structure OPENFILENAME may require marshalling attributes to be passed as an argument in this Declare statement. More Information: http://www.vbtonet.com/ewis/ewi1050.aspx
		//[DllImport("comdlg32.dll", EntryPoint = "GetOpenFileNameA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetOpenFileName(ref FicherosAportadosSupport.PInvoke.UnsafeNative.Structures.OPENFILENAME pOpenfilename);

		//Public Declare Function GetSaveFileName Lib "comdlg32" _
		//'   Alias "GetSaveFileNameA" _
		//'  (pOpenfilename As OPENFILENAME) As Long
		//
		//Public Declare Function GetShortPathName Lib "kernel32" _
		//'    Alias "GetShortPathNameA" _
		//'   (ByVal lpszLongPath As String, _
		//'    ByVal lpszShortPath As String, _
		//'    ByVal cchBuffer As Long) As Long
		//







		// Recibe el t�tulo parcial o igual de las ventanas a cerrar
		internal static void Cerrar_ventana(string El_Caption)
		{
			El_titulo = El_Caption;
            //UPGRADE_WARNING: (1048) Add a delegate for AddressOf EnumCallback More Information: http://www.vbtonet.com/ewis/ewi1048.aspx
            //SDSALAZAR PASA 00 EN EnumCallback
            UpgradeSupportHelper.PInvoke.SafeNative.user32.EnumWindows(EnumCallback(0,0), 0);
		}


		// Funci�n para recorrer las ventanas abiertas
		internal static int EnumCallback(int A_hwnd, int param)
		{

			FixedLengthString Buffer = new FixedLengthString(256);

			//Retorna la cantidad de caracteres del t�tulo de la ventana actual
			string tempRefParam = Buffer.Value;
			int Size_buffer = UpgradeSupportHelper.PInvoke.SafeNative.user32.GetWindowText(A_hwnd, ref tempRefParam, Buffer.Value.Length);
			Buffer.Value = tempRefParam;
			//Elimina los espacios nulos de la cadena
			string Titulo_Win = Buffer.Value.Substring(0, Math.Min(Size_buffer, Buffer.Value.Length));

			//si se encuentra la cadena en el caption de la ventana se cierra
			if (Titulo_Win.IndexOf(El_titulo) >= 0)
			{
                int Param = 0;
                // Finaliza la ventana
                UpgradeSupportHelper.PInvoke.SafeNative.user32.SendMessage(A_hwnd, WM_SYSCOMMAND, SC_CLOSE, ref Param);
			}

			// Esto contin�a enumerando las siguientes ventanas de windows
			return 1;
		}
		//UPGRADE_NOTE: (7001) The following declaration (LPSTRToVBString) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string LPSTRToVBString(string s)
		//{
				//int nullpos = (s.IndexOf(Strings.Chr(0).ToString()) + 1);
				//if (nullpos > 0)
				//{
					//return s.Substring(0, Math.Min(nullpos - 1, s.Length));
				//}
				//else
				//{
					//return "";
				//}
		//}

		// obtiene en lsep_hora y lsep_fecha los separadores
		internal static void ObtenerLocale()
		{
			//'    Dim Buffer As String * 100
			//'    Dim dl&
			//'
			//'    #If Win32 Then
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDATE, Buffer, 99)
			//'        lSep_Fecha = LPSTRToVBString(Buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIME, Buffer, 99)
			//'        lSep_Hora = LPSTRToVBString(Buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SSHORTDATE, Buffer, 99)
			//'        lFor_fecha = LPSTRToVBString(Buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIMEFORMAT, Buffer, 99)
			//'        LFOR_HORA = LPSTRToVBString(Buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ICENTURY, Buffer, 99)
			//'        lEsp_For_Siglo = LPSTRToVBString(Buffer)
			//''        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ITLZERO, buffer, 99)
			//''        LNumero_Ceros = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_IDATE, Buffer, 99)
			//'        iFormato_Fecha = LPSTRToVBString(Buffer)
			//''        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ITIME, buffer, 99)
			//''        iFormato_Hora = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDECIMAL, Buffer, 99)
			//'        lSep_Decimal = LPSTRToVBString(Buffer)
			//'
			//'    #Else
			//'        Print " Not implemented under Win16"
			//'    #End If

			//'CURIAE - EQUIPO DE DESARROLLO : NUEVA FORMA DE OBETENER LA CONFIGURACION REGIONAL
		}

		// OBTIENE VALORES DE SEPARADORES, Y FORMATO DE LA BASE DE DATOS
		internal static void ObtenerValoresTipoBD()
		{
			string formato = String.Empty;
			int I = 0;
			string tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, cnConexion);
			DataSet tRr = new DataSet();
			tempAdapter.Fill(tRr);
			switch(Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
			{
				case "SQL SERVER 6.5" : 
					VstFormato_BaseDatos = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
					formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
					for (I = 1; I <= formato.Length; I++)
					{
						if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && VstSeparadorFecha_BaseDatos == "")
						{
							VstSeparadorFecha_BaseDatos = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
						}
						if (VstSeparadorFecha_BaseDatos != "")
						{
							break;
						}
					} 
					formato = formato.Substring(Strings.InStr(I + 1, formato, VstSeparadorFecha_BaseDatos, CompareMethod.Binary)); 
					for (I = 1; I <= formato.Length; I++)
					{
						if (formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))) != " " && (Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1))).ToUpper()[0]) >= 90) && VstSeparadorHora_BaseDatos == "")
						{
							VstSeparadorHora_BaseDatos = formato.Substring(I - 1, Math.Min(1, formato.Length - (I - 1)));
						}
						if (VstSeparadorHora_BaseDatos != "")
						{
							break;
						}
					} 
					break;
			}
			tRr.Close();
		}
	}
}