using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace FicherosAportados
{
	partial class frmFicherosAportados
	{

		#region "Upgrade Support "
		private static frmFicherosAportados m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmFicherosAportados DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmFicherosAportados();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_cmbBotones_5", "_cmbBotones_4", "_cmbBotones_3", "_cmbBotones_2", "_cmbBotones_1", "vspFicheros", "ProgressBar1", "lstFicheros", "txtDescripcion", "File1", "_cmbBotones_0", "Dir1", "Drive1", "_lblTextos_2", "Frame3", "Frame2", "Label1", "Line1", "_lblTextos_0", "_lblTextos_1", "_lblTextos_3", "_lblTextos_4", "frame1","frame4", "cmbBotones", "lblTextos", "ShapeContainer1", "listBoxHelper1", "listBoxComboBoxHelper1", "vspFicheros_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		private Telerik.WinControls.UI.RadButton _cmbBotones_5;
		private Telerik.WinControls.UI.RadButton _cmbBotones_4;
		private Telerik.WinControls.UI.RadButton _cmbBotones_3;
		private Telerik.WinControls.UI.RadButton _cmbBotones_2;
		private Telerik.WinControls.UI.RadButton _cmbBotones_1;
		public UpgradeHelpers.Spread.FpSpread vspFicheros;
        public Telerik.WinControls.UI.RadProgressBar ProgressBar1;
        public Telerik.WinControls.UI.RadListControl lstFicheros;
		public Telerik.WinControls.UI.RadTextBox txtDescripcion;
        public Telerik.WinControls.UI.RadBrowseEditor Fila2;
		public Microsoft.VisualBasic.Compatibility.VB6.FileListBox File1;
		private Telerik.WinControls.UI.RadButton _cmbBotones_0;
		public Microsoft.VisualBasic.Compatibility.VB6.DirListBox Dir1;
		public Microsoft.VisualBasic.Compatibility.VB6.DriveListBox Drive1;
		private Telerik.WinControls.UI.RadLabel _lblTextos_2;
		public System.Windows.Forms.Panel Frame3;
		public System.Windows.Forms.Panel Frame2;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
		private Telerik.WinControls.UI.RadLabel _lblTextos_0;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_1;
		private Telerik.WinControls.UI.RadLabel _lblTextos_3;
		private Telerik.WinControls.UI.RadTextBox _lblTextos_4;
		public System.Windows.Forms.Panel frame1;
		public Telerik.WinControls.UI.RadButton[] cmbBotones = new Telerik.WinControls.UI.RadButton[6];
		public Telerik.WinControls.UI.RadLabel[] lblTextos = new Telerik.WinControls.UI.RadLabel[5];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
        private UpgradeHelpers.Gui.ListBoxHelper listBoxHelper1;
		private UpgradeHelpers.Gui.ListControlHelper listBoxComboBoxHelper1;
        //private FarPoint.Win.Spread.SheetView vspFicheros_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{

			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFicherosAportados));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip (this.components);
			this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.frame1 = new System.Windows.Forms.Panel();
            this._cmbBotones_5 = new Telerik.WinControls.UI.RadButton();
			this._cmbBotones_4 = new Telerik.WinControls.UI.RadButton();
			this._cmbBotones_3 = new Telerik.WinControls.UI.RadButton();
			this._cmbBotones_2 = new Telerik.WinControls.UI.RadButton();
			this._cmbBotones_1 = new Telerik.WinControls.UI.RadButton();
			this.Frame2 = new System.Windows.Forms.Panel();
			this.vspFicheros = new UpgradeHelpers.Spread.FpSpread();
			this.Frame3 = new System.Windows.Forms.Panel();
			this.ProgressBar1 = new Telerik.WinControls.UI.RadProgressBar();
			this.lstFicheros = new Telerik.WinControls.UI.RadListControl();
			this.txtDescripcion = new Telerik.WinControls.UI.RadTextBox();
            this.Fila2 = new Telerik.WinControls.UI.RadBrowseEditor();
            this.File1 = new Microsoft.VisualBasic.Compatibility.VB6.FileListBox();
			this._cmbBotones_0 = new Telerik.WinControls.UI.RadButton();
			this.Dir1 = new Microsoft.VisualBasic.Compatibility.VB6.DirListBox();
			this.Drive1 = new Microsoft.VisualBasic.Compatibility.VB6.DriveListBox();
			this._lblTextos_2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this._lblTextos_0 = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_1 = new Telerik.WinControls.UI.RadTextBox();
			this._lblTextos_3 = new Telerik.WinControls.UI.RadLabel();
			this._lblTextos_4 = new Telerik.WinControls.UI.RadTextBox();
			this.frame1.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.Frame3.SuspendLayout();
            this.SuspendLayout();
			this.listBoxHelper1 = new UpgradeHelpers.Gui.ListBoxHelper(this.components);
			this.listBoxComboBoxHelper1 = new UpgradeHelpers.Gui.ListControlHelper(this.components);
			((System.ComponentModel.ISupportInitialize) this.listBoxComboBoxHelper1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fila2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();

            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.ShapeContainer1.Size = new System.Drawing.Size(675, 457);
            //this.shapecontainer1.size = new system.drawing.size(269, 33);
            this.ShapeContainer1.Shapes.Add(Line1);


            // 
            // frame1
            // 
            //this.frame1.BackColor = System.Drawing.SystemColors.Control;
            this.frame1.Controls.Add(this._cmbBotones_5);
            this.frame1.Controls.Add(this._cmbBotones_4);
            this.frame1.Controls.Add(this._cmbBotones_3);
            this.frame1.Controls.Add(this._cmbBotones_2);
            this.frame1.Controls.Add(this._cmbBotones_1);
            this.frame1.Controls.Add(this.Frame2);
            this.frame1.Controls.Add(this.Label1);
            this.frame1.Controls.Add(this._lblTextos_0);
            this.frame1.Controls.Add(this._lblTextos_1);
            this.frame1.Controls.Add(this._lblTextos_3);
            this.frame1.Controls.Add(this._lblTextos_4);
            this.frame1.Enabled = true;
			//this.frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			//this.frame1.Location = new System.Drawing.Point(2, -2);
			this.frame1.Name = "frame1";
			this.frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frame1.Size = new System.Drawing.Size(675, 457);
			this.frame1.TabIndex = 0;
			this.frame1.Visible = true;

            // 
            // _cmbBotones_5
            // 
            //this._cmbBotones_5.BackColor = System.Drawing.SystemColors.Control;
            this._cmbBotones_5.Cursor = System.Windows.Forms.Cursors.Default;
			this._cmbBotones_5.Enabled = false;
			//this._cmbBotones_5.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_5.Location = new System.Drawing.Point(294, 420);
			this._cmbBotones_5.Name = "_cmbBotones_5";
			this._cmbBotones_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_5.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_5.TabIndex = 20;
			this._cmbBotones_5.Text = "&Modificar";
			this._cmbBotones_5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cmbBotones_5.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_5.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _cmbBotones_4
            // 
            //this._cmbBotones_4.BackColor = System.Drawing.SystemColors.Control;
            this._cmbBotones_4.Cursor = System.Windows.Forms.Cursors.Default;
			this._cmbBotones_4.Enabled = false;
			//this._cmbBotones_4.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_4.Location = new System.Drawing.Point(370, 420);
			this._cmbBotones_4.Name = "_cmbBotones_4";
			this._cmbBotones_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_4.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_4.TabIndex = 18;
			this._cmbBotones_4.Text = "&Eliminar";
			this._cmbBotones_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cmbBotones_4.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_4.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _cmbBotones_3
            // 
            //this._cmbBotones_3.BackColor = System.Drawing.SystemColors.Control;
            this._cmbBotones_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._cmbBotones_3.Enabled = false;
			//this._cmbBotones_3.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_3.Location = new System.Drawing.Point(446, 420);
			this._cmbBotones_3.Name = "_cmbBotones_3";
			this._cmbBotones_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_3.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_3.TabIndex = 17;
			this._cmbBotones_3.Text = "&Visualizar";
			this._cmbBotones_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cmbBotones_3.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_3.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _cmbBotones_2
            // 
            //this._cmbBotones_2.BackColor = System.Drawing.SystemColors.Control;
            this._cmbBotones_2.Cursor = System.Windows.Forms.Cursors.Default;
			//this._cmbBotones_2.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_2.Location = new System.Drawing.Point(598, 420);
			this._cmbBotones_2.Name = "_cmbBotones_2";
			this._cmbBotones_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_2.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_2.TabIndex = 15;
			this._cmbBotones_2.Text = "&Salir";
			this._cmbBotones_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this._cmbBotones_2.UseVisualStyleBackColor = false;
			this._cmbBotones_2.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_2.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _cmbBotones_1
            // 
            //this._cmbBotones_1.BackColor = System.Drawing.SystemColors.Control;
            this._cmbBotones_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._cmbBotones_1.Enabled = false;
			//this._cmbBotones_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_1.Location = new System.Drawing.Point(524, 420);
			this._cmbBotones_1.Name = "_cmbBotones_1";
			this._cmbBotones_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_1.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_1.TabIndex = 14;
			this._cmbBotones_1.Text = "&Aceptar";
			this._cmbBotones_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cmbBotones_1.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_1.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame2
            // 
            //this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            this.Frame2.Controls.Add(this.vspFicheros);
			this.Frame2.Controls.Add(this.Frame3);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(8, 40);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(661, 375);
			this.Frame2.TabIndex = 5;
			this.Frame2.Visible = true;
			// 
			// vspFicheros
			// 
			this.vspFicheros.Location = new System.Drawing.Point(4, 14);
			this.vspFicheros.Name = "vspFicheros";
			this.vspFicheros.Size = new System.Drawing.Size(651, 109);
			this.vspFicheros.TabIndex = 6;
			this.vspFicheros.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.vspFicheros_ButtonClicked);
			
			// 
			// Frame3
			// 
			//this.Frame3.BackColor = System.Drawing.SystemColors.Control;
			this.Frame3.Controls.Add(this.ProgressBar1);
			this.Frame3.Controls.Add(this.lstFicheros);
			this.Frame3.Controls.Add(this.txtDescripcion);
			this.Frame3.Controls.Add(this.File1);
            this.Frame3.Controls.Add(this.Fila2);
            this.Frame3.Controls.Add(this._cmbBotones_0);
			this.Frame3.Controls.Add(this.Dir1);
			this.Frame3.Controls.Add(this.Drive1);
			this.Frame3.Controls.Add(this._lblTextos_2);
			this.Frame3.Enabled = true;
			//this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame3.Location = new System.Drawing.Point(6, 124);
			this.Frame3.Name = "Frame3";
			this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame3.Size = new System.Drawing.Size(651, 247);
			this.Frame3.TabIndex = 7;
			this.Frame3.Visible = true;
			// 
			// ProgressBar1
			// 
			this.ProgressBar1.Location = new System.Drawing.Point(12, 186);
			this.ProgressBar1.Name = "ProgressBar1";
			this.ProgressBar1.Size = new System.Drawing.Size(189, 15);
			this.ProgressBar1.TabIndex = 19;
			this.ProgressBar1.Visible = false;
			// 
			// lstFicheros
			// 
			this.lstFicheros.BackColor = System.Drawing.SystemColors.Window;
			//this.lstFicheros.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lstFicheros.CausesValidation = true;
			//this.lstFicheros.ColumnWidth = this.lstFicheros.ClientSize.Width;
			this.lstFicheros.Cursor = System.Windows.Forms.Cursors.Default;
			this.lstFicheros.Enabled = false;
			this.lstFicheros.ForeColor = System.Drawing.SystemColors.WindowText;
			//this.lstFicheros.IntegralHeight = false;
			this.lstFicheros.Location = new System.Drawing.Point(466, 22);
			this.lstFicheros.Name = "lstFicheros";
			this.lstFicheros.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lstFicheros.Size = new System.Drawing.Size(177, 126);
            //this.lstFicheros.Sorted = false;
			this.lstFicheros.TabIndex = 16;
			this.lstFicheros.TabStop = true;
			this.lstFicheros.Visible = true;
			this.lstFicheros.Items.AddRange(new string[]{"lstFicheros"});
			this.lstFicheros.Leave += new System.EventHandler(this.lstFicheros_Leave);
			//this.lstFicheros.SelectedIndexChanged += new System.EventHandler(this.lstFicheros_SelectedIndexChanged);
        
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.AcceptsReturn = true;
			//this.txtDescripcion.BackColor = System.Drawing.SystemColors.Control;
			//this.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.txtDescripcion.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtDescripcion.Enabled = false;
			//this.txtDescripcion.ForeColor = System.Drawing.SystemColors.WindowText;
			this.txtDescripcion.Location = new System.Drawing.Point(210, 151);
			this.txtDescripcion.MaxLength = 100;
			this.txtDescripcion.Multiline = true;
			this.txtDescripcion.Name = "txtDescripcion";
			this.txtDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtDescripcion.Size = new System.Drawing.Size(433, 89);
			this.txtDescripcion.TabIndex = 13;
			this.txtDescripcion.WordWrap = false;
			this.txtDescripcion.Enter += new System.EventHandler(this.txtDescripcion_Enter);
			this.txtDescripcion.Leave += new System.EventHandler(this.txtDescripcion_Leave);
			this.txtDescripcion.TextChanged += new System.EventHandler(this.txtDescripcion_TextChanged);
            //
            //Fila2
            //
            this.Fila2.Location = new System.Drawing.Point(12, 70);
            this.Fila2.Name = "RadBrowseEditor";
            this.Fila2.Size = new System.Drawing.Size(270, 20);
            this.Fila2.TabIndex = 0;
            // 
            // File1
            // 
            this.File1.Archive = true;
			this.File1.BackColor = System.Drawing.SystemColors.Window;
			this.File1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.File1.CausesValidation = true;
			this.File1.Cursor = System.Windows.Forms.Cursors.Default;
			this.File1.Enabled = true;
			this.File1.ForeColor = System.Drawing.SystemColors.WindowText;
			this.File1.Hidden = false;
			this.File1.Location = new System.Drawing.Point(212, 22);
			this.File1.Name = "File1";
			this.File1.Normal = true;
			this.File1.Pattern = "*.*";
			this.File1.ReadOnly = true;
			this.File1.SelectionMode = System.Windows.Forms.SelectionMode.One;
			this.File1.Size = new System.Drawing.Size(177, 123);
			this.File1.System = false;
			this.File1.TabIndex = 11;
			this.File1.TabStop = true;
			this.File1.TopIndex = 0;
			this.File1.Visible = false;
			this.File1.SelectedIndexChanged += new System.EventHandler(this.File1_SelectedIndexChanged);
			// 
			// _cmbBotones_0
			// 
			//this._cmbBotones_0.BackColor = System.Drawing.SystemColors.Control;
			this._cmbBotones_0.Cursor = System.Windows.Forms.Cursors.Default;
			//this._cmbBotones_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._cmbBotones_0.Location = new System.Drawing.Point(394, 70);
			this._cmbBotones_0.Name = "_cmbBotones_0";
			this._cmbBotones_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmbBotones_0.Size = new System.Drawing.Size(69, 31);
			this._cmbBotones_0.TabIndex = 10;
			this._cmbBotones_0.Text = "&Insertar";
			this._cmbBotones_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this._cmbBotones_0.UseVisualStyleBackColor = false;
			this._cmbBotones_0.Click += new System.EventHandler(this.cmbBotones_Click);
            this._cmbBotones_0.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Dir1
            // 
            this.Dir1.BackColor = System.Drawing.SystemColors.Window;
			this.Dir1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Dir1.CausesValidation = true;
			this.Dir1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Dir1.Enabled = true;
			this.Dir1.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Dir1.Location = new System.Drawing.Point(14, 50);
			this.Dir1.Name = "Dir1";
			this.Dir1.Size = new System.Drawing.Size(187, 96);
			this.Dir1.TabIndex = 9;
			this.Dir1.TabStop = true;
			this.Dir1.Visible = false;
			this.Dir1.Change += new System.EventHandler(this.Dir1_Change);
			// 
			// Drive1
			// 
			this.Drive1.BackColor = System.Drawing.SystemColors.Window;
			this.Drive1.CausesValidation = true;
			this.Drive1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Drive1.Enabled = true;
			this.Drive1.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Drive1.Location = new System.Drawing.Point(14, 22);
			this.Drive1.Name = "Drive1";
			this.Drive1.Size = new System.Drawing.Size(187, 21);
			this.Drive1.TabIndex = 8;
			this.Drive1.TabStop = true;
			this.Drive1.Visible = false;
			this.Drive1.SelectedIndexChanged += new System.EventHandler(this.Drive1_SelectedIndexChanged);
			// 
			// _lblTextos_2
			// 
			//this._lblTextos_2.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_2.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_2.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_2.Location = new System.Drawing.Point(12, 154);
			this._lblTextos_2.Name = "_lblTextos_2";
			this._lblTextos_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_2.Size = new System.Drawing.Size(191, 13);
			this._lblTextos_2.TabIndex = 12;
			this._lblTextos_2.Text = "Descripción:";
			//this._lblTextos_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label1
			// 
			//this.Label1.BackColor = System.Drawing.SystemColors.Control;
			//this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.ForeColor = System.Drawing.Color.Red;
			this.Label1.Location = new System.Drawing.Point(46, 428);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(221, 15);
			this.Label1.TabIndex = 21;
			this.Label1.Text = "Registros sin descripción, no actualizados.";
			this.Label1.Visible = false;
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.Color.Red;
            this.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.Line1.BorderWidth = 5;
            this.Line1.Enabled = false;
            this.Line1.Name = "Line1";
            this.Line1.Visible = true;
            this.Line1.X1 = (int)100;
            this.Line1.X2 = (int)39;
            this.Line1.Y1 = (int)422;
            this.Line1.Y2 = (int)423;

            //         this.Line1.StartPoint = new System.Drawing.Point(8, 337);


            //this.Line1.BorderColor = System.Drawing.Color.Red;
            //this.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            //this.Line1.BorderWidth = 5;
            //this.Line1.Enabled = false;
            //this.Line1.Name = "Line1";
            //this.Line1.Visible = true;
            //this.Line1.X1 = (int)8;
            //this.Line1.X2 = (int)50;
            //this.Line1.Y1 = (int)0;
            //this.Line1.Y2 = (int)310;
            // 


            // _lblTextos_0
            // 
            //this._lblTextos_0.BackColor = System.Drawing.SystemColors.Control;
            //this._lblTextos_0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._lblTextos_0.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_0.Location = new System.Drawing.Point(16, 18);
			this._lblTextos_0.Name = "_lblTextos_0";
			this._lblTextos_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_0.Size = new System.Drawing.Size(49, 17);
			this._lblTextos_0.TabIndex = 4;
			this._lblTextos_0.Text = "Paciente:";
			// 
			// _lblTextos_1
			// 
			//this._lblTextos_1.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._lblTextos_1.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_1.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_1.Location = new System.Drawing.Point(70, 18);
			this._lblTextos_1.Name = "_lblTextos_1";
			this._lblTextos_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_1.Size = new System.Drawing.Size(419, 17);
			this._lblTextos_1.TabIndex = 3;
            this._lblTextos_1.Enabled = false;
			// 
			// _lblTextos_3
			// 
			//this._lblTextos_3.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._lblTextos_3.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_3.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_3.Location = new System.Drawing.Point(500, 18);
			this._lblTextos_3.Name = "_lblTextos_3";
			this._lblTextos_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_3.Size = new System.Drawing.Size(69, 17);
			this._lblTextos_3.TabIndex = 2;
			this._lblTextos_3.Text = "Hist. Clínica:";
			// 
			// _lblTextos_4
			// 
			//this._lblTextos_4.BackColor = System.Drawing.SystemColors.Control;
			//this._lblTextos_4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._lblTextos_4.Cursor = System.Windows.Forms.Cursors.Default;
			//this._lblTextos_4.ForeColor = System.Drawing.SystemColors.ControlText;
			this._lblTextos_4.Location = new System.Drawing.Point(572, 18);
			this._lblTextos_4.Name = "_lblTextos_4";
			this._lblTextos_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lblTextos_4.Size = new System.Drawing.Size(97, 17);
			this._lblTextos_4.TabIndex = 1;
            this._lblTextos_4.Enabled = false;
			// 
			// frmFicherosAportados
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(679, 457);
			this.Controls.Add(this.frame1);
			this.frame1.Controls.Add(ShapeContainer1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmFicherosAportados";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Documentación Entregada/Aportada – Ficheros ";
			//this.listBoxComboBoxHelper1.SetItemData(this.lstFicheros, new int[]{0});
			//listBoxHelper1.SetSelectionMode(this.lstFicheros, System.Windows.Forms.SelectionMode.One);
			this.Closed += new System.EventHandler(this.frmFicherosAportados_Closed);
			this.Load += new System.EventHandler(this.frmFicherosAportados_Load);
			((System.ComponentModel.ISupportInitialize) this.listBoxComboBoxHelper1).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fila2)).EndInit();

            this.frame1.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.Frame3.ResumeLayout(false);
            this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializelblTextos();
			InitializecmbBotones();
		}
		void InitializelblTextos()
		{
			this.lblTextos = new Telerik.WinControls.UI.RadLabel[5];
			this.lblTextos[2] = _lblTextos_2;
			this.lblTextos[0] = _lblTextos_0;
			//this.lblTextos[1] = _lblTextos_1;
			this.lblTextos[3] = _lblTextos_3;
			//this.lblTextos[4] = _lblTextos_4;
		}
		void InitializecmbBotones()
		{
			this.cmbBotones = new Telerik.WinControls.UI.RadButton[6];
			this.cmbBotones[5] = _cmbBotones_5;
			this.cmbBotones[4] = _cmbBotones_4;
			this.cmbBotones[3] = _cmbBotones_3;
			this.cmbBotones[2] = _cmbBotones_2;
			this.cmbBotones[1] = _cmbBotones_1;
			this.cmbBotones[0] = _cmbBotones_0;
		}
		#endregion
	}
}