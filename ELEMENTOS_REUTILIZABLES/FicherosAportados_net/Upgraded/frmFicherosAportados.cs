using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Microsoft.CSharp;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace FicherosAportados
{
	public partial class frmFicherosAportados
            : RadForm
    {
		private const int SE_ERR_ACCESSDENIED = 5;
		private const int SE_ERR_ASSOCINCOMPLETE = 27;
		private const int SE_ERR_DDEBUSY = 30;
		private const int SE_ERR_DDEFAIL = 29;
		private const int SE_ERR_DDETIMEOUT = 28;
		private const int SE_ERR_DLLNOTFOUND = 32;
		private const int SE_ERR_FNF = 2;
		private const int SE_ERR_NOASSOC = 31;
		private const int SE_ERR_OOM = 8;
		private const int SE_ERR_PNF = 3;
		private const int SE_ERR_SHARE = 26;
		private const int ERROR_FILE_NOT_FOUND = 2;
		private const int ERROR_PATH_NOT_FOUND = 3;
		private const int ERROR_BAD_FORMAT = 11;

		int[, ] arrError = new int[15, 3];
		Array arrFicheros = null;

		string strFichero = String.Empty;

		byte Col_Elimina = 0;
		byte Col_FecCrea = 0;
		byte col_Modific = 0;
		byte Col_Descrip = 0;
		byte Col_Imprime = 0;
		byte Col_NomDocu = 0;
		byte Col_NumDocu = 0;
		byte Col_ClaDocu = 0;
		byte Col_Usuario = 0;

		int intNumDocu = 0;
		bool blnVacio = false;
		bool blnInsert = false;
		bool blnSelect = false;
		int intIndex = 0;
		bool blnModific = false;
		public frmFicherosAportados()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}



		private void cmbBotones_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cmbBotones, eventSender);
			bool blnContinua = false;
			int iRespuesta = 0;

			switch(Index)
			{
				case 0 :  //Insertar 
					blnContinua = true; 
					blnInsert = true; 
					 
					if (lstFicheros.Items.Count > 0)
					{
						for (int intContador = 1; intContador <= lstFicheros.Items.Count; intContador++)
						{
                            //Sdsalazar TODO_SPRINT_5_5
							//ListBoxHelper.SetSelectedIndex(lstFicheros, intContador - 1);
                          
							if (strFichero.Trim() == lstFicheros.Text.Trim())
							{
								blnContinua = false;
								break;
							}
						}
					} 
					if (blnContinua)
					{
						lstFicheros.Items.Add(strFichero);

						if (lstFicheros.Items.Count - 1 <= 0)
						{
							arrFicheros = Array.CreateInstance(typeof(object), new int[]{4, lstFicheros.Items.Count}, new int[]{0, 0});
						}
						else
						{
							arrFicheros = ArraysHelper.RedimPreserve<object[, ]>((object[, ]) arrFicheros, new int[]{4, lstFicheros.Items.Count}, new int[]{0, 0});
						}

						arrFicheros.SetValue(txtDescripcion.Text, 0, lstFicheros.Items.Count - 1);
						arrFicheros.SetValue(false, 2, lstFicheros.Items.Count - 1);
						arrFicheros.SetValue(strFichero, 3, lstFicheros.Items.Count - 1);
						for (int intContador = 1; intContador <= strFichero.Trim().Length; intContador++)
						{
							if (strFichero.Trim().Substring(Math.Max(strFichero.Trim().Length - intContador, 0)).StartsWith("\\"))
							{
								break;
							}
							arrFicheros.SetValue(strFichero.Trim().Substring(Math.Max(strFichero.Trim().Length - intContador, 0)).Substring(0, Math.Min(1, strFichero.Trim().Substring(Math.Max(strFichero.Trim().Length - intContador, 0)).Length)) + Convert.ToString(arrFicheros.GetValue(1, lstFicheros.Items.Count - 1)), 1, lstFicheros.Items.Count - 1);

						}

						cmbBotones[1].Enabled = true;
						lstFicheros.Enabled = true;
                        //Sdsalazar Cambio
                        //if (ListBoxHelper.GetSelectedIndex(lstFicheros) >= 0)
                        if (lstFicheros.SelectedIndex >= 0)
						{
                            //Sdsalazar TODO �SPRINT_5_5
                            //ListBoxHelper.SetSelected(lstFicheros, ListBoxHelper.GetSelectedIndex(lstFicheros), false);
						}

					}
					else
					{
						//MsgBox "El fichero ya esta incorporado", vbInformation, "Error al insertar el fichero"
						short tempRefParam = 1200;
						string[] tempRefParam2 = new string[]{"El fichero", "incorporado"};
						BGFicherosAportados.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, BGFicherosAportados.cnConexion, tempRefParam2);
					} 
					File1.SetSelected(File1.SelectedIndex, false); 
					strFichero = ""; 
					txtDescripcion.Text = ""; 
					txtDescripcion.Enabled = false; 
					txtDescripcion.BackColor = SystemColors.Control; 
					cmbBotones[0].Enabled = false; 
					blnInsert = false; 
					//  cmbBotones(1).SetFocus 
					break;
				case 1 :  //Aceptar 
					GrabarDatos(); 
					 
					break;
				case 2 :  //Cancelar 

					 
					int tempForVar = vspFicheros.MaxRows; 
					for (int intContador = 1; intContador <= tempForVar; intContador++)
					{
						vspFicheros.Row = intContador;
						vspFicheros.Col = Col_NomDocu;
						BGFicherosAportados.Cerrar_ventana(vspFicheros.Text);

						if (FileSystem.Dir(Path.GetDirectoryName(Application.ExecutablePath) + "\\tmp" + vspFicheros.Text, FileAttribute.Normal) != "")
						{
							File.Delete(Path.GetDirectoryName(Application.ExecutablePath) + "\\tmp" + vspFicheros.Text);
						}
					} 
					 
					this.Close(); 
					 
					break;
				case 3 :  //Visualizar 
					fVisualiza(); 
					break;
				case 4 :  //Eliminar 
					short tempRefParam3 = 1125; 
					string[] tempRefParam4 = new string[]{"Los ficheros seleccionados se eliminaran", "continuar"}; 
					iRespuesta = Convert.ToInt32(BGFicherosAportados.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, BGFicherosAportados.cnConexion, tempRefParam4)); 
					if (iRespuesta == 6)
					{
						EliminaRegistros();
						frmFicherosAportados_Load(this, new EventArgs());
					} 
					break;
				case 5 :  // Modificar 
					 
					ActualizaDatos(); 
					 
					break;
			}

		}

		private void Drive1_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			bool ErrDrive = false;
			try
			{
				ErrDrive = true;
				Dir1.Path = Drive1.Drive.ToUpper();
				strFichero = "";
				txtDescripcion.Enabled = false;
				ErrDrive = false;
			}
			catch (Exception excep)
			{
				if (!ErrDrive)
				{
					throw excep;
				}

				if (ErrDrive)
				{

					RadMessageBox.Show(Information.Err().Number.ToString() + "  - " + excep.Message, Application.ProductName);
				}
			}
		}

		private void Dir1_Change(Object eventSender, EventArgs eventArgs)
		{
			bool ErrDir = false;
			try
			{
				ErrDir = true;
				File1.Path = Dir1.Path;
				strFichero = "";
				txtDescripcion.Enabled = false;
				ErrDir = false;
			}
			catch (Exception excep)
			{
				if (!ErrDir)
				{
					throw excep;
				}

				if (ErrDir)
				{

					MessageBox.Show(Information.Err().Number.ToString() + "  - " + excep.Message, Application.ProductName);
				}
			}
		}


		private void File1_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{

			if (File1.SelectedIndex >= 0)
			{
				if (File1.Path.Substring(Strings.Len(File1.Path) - 1) == "\\")
				{
					strFichero = File1.Path + File1.FileName;
				}
				else
				{
					strFichero = File1.Path + "\\" + File1.FileName;
				}

				txtDescripcion.Enabled = true;
				txtDescripcion.BackColor = SystemColors.Window;
				if (txtDescripcion.Text.Trim() == "")
				{
					if (BGFicherosAportados.gObserva.Trim() != "")
					{
						txtDescripcion.Text = BGFicherosAportados.gObserva.Substring(0, Math.Min(100, BGFicherosAportados.gObserva.Length));
					}
					else
					{
						txtDescripcion.Text = File1.FileName;
					}
				}

			}
		}

		private void frmFicherosAportados_Load(Object eventSender, EventArgs eventArgs)
		{

			if (!mbAcceso.AplicarControlAcceso(BGFicherosAportados.sUsuario, "A", this, "FICHEROSAP", ref BGFicherosAportados.astrcontroles, ref BGFicherosAportados.astreventos))
			{
				cmbBotones_Click(cmbBotones[2], new EventArgs());
			}

			BGFicherosAportados.ObtenerValoresTipoBD();
			BGFicherosAportados.ObtenerLocale();
			BGFicherosAportados.vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(BGFicherosAportados.cnConexion);
			BGFicherosAportados.stDecimalBD = Serrores.ObtenerSeparadorDecimalBD(BGFicherosAportados.cnConexion);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref BGFicherosAportados.cnConexion);


			File1.Pattern = "*.*";
			string strSql = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'FICHVISU'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["valfanu1"]))
				{
					File1.Pattern = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]).Trim() == "") ? "*.*" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]);
				}
			}
            rdoTemp.Close();



			lstFicheros.Items.Clear();
			cmbBotones[0].Enabled = false;
			cmbBotones[1].Enabled = false;
			cmbBotones[3].Enabled = false;
			cmbBotones[4].Enabled = false;
			txtDescripcion.Enabled = false;

			Col_Elimina = 1;
			Col_FecCrea = 2;
			col_Modific = 3;
			Col_Descrip = 4;
			Col_Imprime = 5;
			Col_NomDocu = 6;
			Col_NumDocu = 7;
			Col_ClaDocu = 8;
			Col_Usuario = 9;

			lblTextos[1].Text = BGFicherosAportados.gPaciente;
			lblTextos[4].Text = fHistoria();



			intNumDocu = 0;
			blnVacio = true;

			vspFicheros.MaxRows = 0;

			CargaGrid();

			CargaErrorShell();



		}

		private string fHistoria()
		{

			string result = String.Empty;
			string strSql = "Select ghistoria FROM HDOSSIER " + 
			                "WHERE gidenpac = '" + BGFicherosAportados.gstIdenPaciente + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(rdoTemp.Tables[0].Rows[0]["ghistoria"]);
			}
			else
			{
				result = "";
			}
            rdoTemp.Close();
			return result;
		}



		private void lstFicheros_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			if (!blnInsert)
			{
				//if (ListBoxHelper.GetSelectedIndex(lstFicheros) >= 0)
                if(lstFicheros.SelectedIndex >=0)
				{
                    //Sdsalazar TODO �SPRINT_5_5
                    //txtDescripcion.Text = Convert.ToString(arrFicheros.GetValue(0, ListBoxHelper.GetSelectedIndex(lstFicheros)));
					txtDescripcion.Enabled = true;
					txtDescripcion.BackColor = SystemColors.Window;
					txtDescripcion.Focus();
				}
			}
		}

		private void lstFicheros_Leave(Object eventSender, EventArgs eventArgs)
		{
			//if (!blnInsert && ListBoxHelper.GetSelectedIndex(lstFicheros) >= 0)
              if (!blnInsert && lstFicheros.SelectedIndex >= 0)
                {
				blnSelect = true;
                //Sdsalazar TODO �SPRINT_5_5
                //intIndex = ListBoxHelper.GetSelectedIndex(lstFicheros);

				blnInsert = true;
                //Sdsalazar TODO �SPRINT_5_5
                //ListBoxHelper.SetSelected(lstFicheros, ListBoxHelper.GetSelectedIndex(lstFicheros), false);
				blnInsert = false;

			}
		}

		private void txtDescripcion_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (!blnModific)
			{
				cmbBotones[0].Enabled = txtDescripcion.Text.Trim() != "";
			}
		}



		private void CargaGrid()
		{
			string strSql = String.Empty;
			DataSet rdoTemp = null;
			int IntFila = 0;
			Color varColor = new Color();


			vspFicheros.MaxRows = 0;

			if (BGFicherosAportados.gNombreDoc != "")
			{

				strSql = "SELECT * FROM DDOCUPDF WHERE gnombdoc = '" + BGFicherosAportados.gNombreDoc + "' Order By fechalta, inumdocu";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);

				vspFicheros.MaxRows = rdoTemp.Tables[0].Rows.Count;
				rdoTemp.MoveFirst();
				IntFila = 1;
				varColor = vspFicheros.BackColor;
				foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
				{
					vspFicheros.Row = IntFila;
					blnVacio = false;


					vspFicheros.Col = Col_Elimina;

					if (BGFicherosAportados.sUsuario.Trim() != Convert.ToString(iteration_row["gusuario"]).Trim())
					{
						vspFicheros.CellType =  5;
						vspFicheros.Lock = true;
						vspFicheros.BackColor = SystemColors.ScrollBar;
					}
					else
					{
						vspFicheros.CellType =  10;
						vspFicheros.TypeCheckCenter = true;
						vspFicheros.Lock = false;
						vspFicheros.BackColor = varColor;
					}

					vspFicheros.Col = Col_FecCrea;
					vspFicheros.CellType = 5;
					vspFicheros.Text = Convert.ToString(iteration_row["fechalta"]);
					//vspFicheros.Text = Format(rdoTemp("fechalta"), "DD/MM/YYYY HH:NN")
					vspFicheros.TypeHAlign = ContentAlignment.MiddleCenter;

                    vspFicheros.Col = Col_Descrip;
					vspFicheros.CellType =  5;
					vspFicheros.setTypeEditLen(100);

					vspFicheros.Text = Convert.ToString(iteration_row["ddescrip"]);

					vspFicheros.Col = Col_Imprime;
					vspFicheros.CellType = 10;
					vspFicheros.TypeCheckCenter = true;

					vspFicheros.Col = col_Modific;
					vspFicheros.CellType = 10;
					vspFicheros.TypeCheckCenter = true;

					vspFicheros.Col = Col_NomDocu;
					vspFicheros.CellType = 5;
					vspFicheros.Text = Convert.ToString(iteration_row["dnombpdf"]);
					vspFicheros.SetColHidden(vspFicheros.Col, true);


					vspFicheros.Col = Col_NumDocu;
					vspFicheros.CellType = 5;
					vspFicheros.Text = Convert.ToString(iteration_row["inumdocu"]);
					vspFicheros.SetColHidden(vspFicheros.Col, true);

					vspFicheros.Col = Col_ClaDocu;
					vspFicheros.CellType = 5;
					vspFicheros.Text = Convert.ToString(iteration_row["gnombdoc"]);
					vspFicheros.SetColHidden(vspFicheros.Col, true);

					vspFicheros.Col = Col_Usuario;
					vspFicheros.CellType = 5;
					vspFicheros.Text = Convert.ToString(iteration_row["gusuario"]);
					vspFicheros.SetColHidden(vspFicheros.Col, true);


					intNumDocu = Convert.ToInt32(iteration_row["inumdocu"]);
					IntFila++;
				}
                rdoTemp.Close();
				rdoTemp = null;
			}
		}

		private void GrabarDatos()
		{
			bool ErrGraba = false;
			byte[] Binario = null;
			string strClave = String.Empty;
			System.DateTime datFecha = DateTime.FromOADate(0);
			DataSet rdoTemp = null;
			string strSql = String.Empty;
			string strMensaje = String.Empty;
			bool blnContinua = false;
			string stTmpCadena = String.Empty;
			string[, ] arrTemp = null;
			int intarray = 0;

			try
			{
				ErrGraba = true;

				datFecha = DateTime.Now;
				blnContinua = true;

				if (BGFicherosAportados.gNombreDoc.Trim() == "")
				{
					strClave = BGFicherosAportados.gstIdenPaciente.Trim();
					strClave = strClave + DateTime.Now.ToString("yyyyMMdd") + DateTime.Now.TimeOfDay.TotalSeconds.ToString();
					strClave = strClave + "PDF";

					stTmpCadena = strClave;
					strClave = "";
					for (int intContador = 1; intContador <= stTmpCadena.Length; intContador++)
					{
						if (stTmpCadena.Substring(intContador - 1, Math.Min(1, stTmpCadena.Length - (intContador - 1))) != "," && stTmpCadena.Substring(intContador - 1, Math.Min(1, stTmpCadena.Length - (intContador - 1))) != ".")
						{
							strClave = strClave + stTmpCadena.Substring(intContador - 1, Math.Min(1, stTmpCadena.Length - (intContador - 1)));
						}
					}
				}
				else
				{
					strClave = BGFicherosAportados.gNombreDoc;
				}


				if (strClave.Trim() != "")
				{


					for (int intContador = 0; intContador <= lstFicheros.Items.Count - 1; intContador++)
					{
						strSql = "SELECT * FROM DDOCUPDF WHERE 1=2";
						SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
						rdoTemp = new DataSet();
						tempAdapter.Fill(rdoTemp);

						blnInsert = true;
                        //Sdsalazar TODO �SPRINT_5_5
                        //ListBoxHelper.SetSelectedIndex(lstFicheros, intContador);

						strFichero = lstFicheros.Text.Trim().Trim();
						Binario = (byte[]) Serrores.fPasaBinario(strFichero);
						lblTextos[2].Text = "Descripci�n:";
						lblTextos[2].Refresh();

						//   cnConexion.BeginTrans
						rdoTemp.AddNew();
						rdoTemp.Tables[0].Rows[0]["gnombdoc"] = strClave;
						rdoTemp.Tables[0].Rows[0]["inumdocu"] = intNumDocu + 1;
						rdoTemp.Tables[0].Rows[0]["ddescrip"] = arrFicheros.GetValue(0, intContador);
						rdoTemp.Tables[0].Rows[0]["fechalta"] = datFecha;
						 
						rdoTemp.Tables[0].Rows[0]["odocupdf"] = Binario;
						 
						rdoTemp.Tables[0].Rows[0]["dnombpdf"] = arrFicheros.GetValue(1, intContador);
						 
						rdoTemp.Tables[0].Rows[0]["gusuario"] = BGFicherosAportados.sUsuario;
						 
						string tempQuery = rdoTemp.Tables[0].TableName;
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
						tempAdapter_2.Update(rdoTemp, rdoTemp.Tables[0].TableName);
						intNumDocu++;
						if (blnContinua)
						{
							if (Convert.ToBoolean(BGFicherosAportados.Instancia.fDocumentos(strClave)))
							{
								blnContinua = false;
								blnVacio = false;
								//          cnConexion.CommitTrans
								arrFicheros.SetValue(true, 2, intContador);
							}
							else
							{
								strMensaje = strMensaje + "- El registro del fichero " + strFichero + " produjo un error de grabaci�n.    " + "\r";
							}
						}
						else
						{
							//      cnConexion.CommitTrans
							arrFicheros.SetValue(true, 2, intContador);
						}

					}
					intarray = 0;
					for (int intContador = 0; intContador <= lstFicheros.Items.Count - 1; intContador++)
					{
						if ( ~Convert.ToInt32(arrFicheros.GetValue(2, intContador)) != 0)
						{
							arrTemp = ArraysHelper.RedimPreserve<string[, ]>(arrTemp, new int[]{4, intarray + 1});
							arrTemp[0, intarray] = Convert.ToString(arrFicheros.GetValue(0, intContador));
							arrTemp[1, intarray] = Convert.ToString(arrFicheros.GetValue(1, intContador));
							arrTemp[2, intarray] = Convert.ToString(arrFicheros.GetValue(2, intContador));
							arrTemp[3, intarray] = Convert.ToString(arrFicheros.GetValue(3, intContador));
							lstFicheros.Items.Clear();
							intarray++;
						}
					}

					if (intarray - 1 >= 0)
					{
						arrFicheros = Array.CreateInstance(typeof(object), new int[]{4, intarray}, new int[]{0, 0});

						for (int intContador = 0; intContador <= intarray - 1; intContador++)
						{

							arrFicheros.SetValue(arrTemp[0, intContador], 0, intContador);
							arrFicheros.SetValue(arrTemp[1, intContador], 1, intContador);
							arrFicheros.SetValue(arrTemp[2, intContador], 2, intContador);
							arrFicheros.SetValue(arrTemp[3, intContador], 3, intContador);
							lstFicheros.Items.Add(arrTemp[3, intContador]);
						}
					}
					else
					{
						arrFicheros = Array.CreateInstance(typeof(object), new int[]{3}, new int[]{0});
					}
					if (strMensaje.Trim() != "")
					{
						//        cnConexion.RollbackTrans

						RadMessageBox.Show(strMensaje, "Errores en proceso de grabaci�n", MessageBoxButtons.OK, RadMessageIcon.Info);
					}
					else
					{
						BGFicherosAportados.gNombreDoc = strClave;
						frmFicherosAportados_Load(this, new EventArgs());
					}
					CargaGrid();
					rdoTemp = null;
				}
				else
				{
					RadMessageBox.Show(strMensaje, "Error al generar clave del registro.", MessageBoxButtons.OK, RadMessageIcon.Info);
				}

				blnInsert = false;

				ErrGraba = false;
			}
			catch (Exception excep)
			{
				if (!ErrGraba)
				{
					throw excep;
				}

				if (ErrGraba)
				{

					//    cnConexion.RollbackTrans
					RadMessageBox.Show(excep.Message, "Error en proceso de grabaci�n", MessageBoxButtons.OK, RadMessageIcon.Info);
					blnInsert = false;


				}
			}
		}

		private void txtDescripcion_Enter(Object eventSender, EventArgs eventArgs)
		{
			if (blnSelect)
			{
				if (intIndex >= 0)
				{
					blnInsert = true;
                    //Sdsalazar TODO �SPRINT_5_5
					//ListBoxHelper.SetSelected(lstFicheros, intIndex, true);
					blnInsert = false;
					txtDescripcion.Text = Convert.ToString(arrFicheros.GetValue(0, intIndex));
					txtDescripcion.Enabled = true;
					txtDescripcion.BackColor = SystemColors.Window;
				}
			}
		}

		private void txtDescripcion_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (blnSelect)
			{
                if (intIndex >= 0)
                {
                    //Sdsalazar TODO �SPRINT_5_5
                    //ListBoxHelper.SetSelected(lstFicheros, intIndex, false);
					arrFicheros.SetValue(txtDescripcion.Text, 0, intIndex);
					txtDescripcion.Text = "";
					txtDescripcion.Enabled = false;
					txtDescripcion.BackColor = SystemColors.Control;
				}
				blnSelect = false;
			}
			if (blnModific)
			{
				vspFicheros.Col = col_Modific;
				if (StringsHelper.ToDoubleSafe(Convert.ToString(vspFicheros.Value)) == 1)
				{
					vspFicheros.Col = Col_Descrip;
					vspFicheros.Text = txtDescripcion.Text;
				}
				blnModific = false;
				txtDescripcion.Text = "";
				txtDescripcion.Enabled = false;
				txtDescripcion.BackColor = SystemColors.Control;
			}
		}

		private void vspFicheros_ButtonClicked(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			//int Col = eventArgs.Column;
			//int Row = eventArgs.Row;
            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;
            int ButtonDown = 0;
			vspFicheros.Row = Row;
			string auxVar_2 = String.Empty;
			if (Col == Col_Elimina)
			{
				cmbBotones[4].Enabled = false;
				vspFicheros.Col = Col_Elimina;
				int tempForVar = vspFicheros.MaxRows;
				string auxVar = String.Empty;
				for (int intContador = 1; intContador <= tempForVar; intContador++)
				{
					vspFicheros.Row = intContador;
					if (Convert.ToString(vspFicheros.Value) != "")
					{
						bool tempBool = false;
						auxVar = Convert.ToString(vspFicheros.Value);
						if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
						{
							cmbBotones[4].Enabled = true;
							break;
						}
					}
				}
				vspFicheros.Row = Row;
			}
			else if (Col == Col_Imprime)
			{ 
				cmbBotones[3].Enabled = false;
				vspFicheros.Col = Col_Imprime;
				int tempForVar2 = vspFicheros.MaxRows;
				for (int intContador = 1; intContador <= tempForVar2; intContador++)
				{
					vspFicheros.Row = intContador;
					bool tempBool2 = false;
					auxVar_2 = Convert.ToString(vspFicheros.Value);
					if ((Boolean.TryParse(auxVar_2, out tempBool2)) ? tempBool2 : Convert.ToBoolean(Double.Parse(auxVar_2)))
					{
						cmbBotones[3].Enabled = true;
						break;
					}
				}
				vspFicheros.Row = Row;
			}
			else if (Col == col_Modific)
			{ 
				vspFicheros.Col = col_Modific;
				if (StringsHelper.ToDoubleSafe(Convert.ToString(vspFicheros.Value)) == 1)
				{
					blnModific = true;
					vspFicheros.Col = Col_Descrip;

					vspFicheros.CellType =  1;
                    //sdsalazar todo sprint 5_5
                    //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property vspFicheros.EditMode was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    //vspFicheros.setEditMode(true);
					vspFicheros.Lock = false;

					vspFicheros.Focus();
					vspFicheros.Action = 0; //activa la celda


					cmbBotones[5].Enabled = true;
				}
				else
				{
					vspFicheros.Col = Col_Descrip;
					//sdsalazar todo sprint 5_5
                    //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property vspFicheros.EditMode was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//vspFicheros.setEditMode(false);
					vspFicheros.Lock = true;
					vspFicheros.Focus();
					vspFicheros.Action = 0; //activa la celda
				}

			}
		}

		private void EliminaRegistros()
		{
			bool ErrBorra = false;
			string strNomDoc = String.Empty;
			int intNumDoc = 0;
			System.DateTime datFechAlta = DateTime.FromOADate(0);

			try
			{
				ErrBorra = true;

                BGFicherosAportados.cnConexion.BeginTransScope();

                int tempForVar = vspFicheros.MaxRows;
				for (int intContador = 1; intContador <= tempForVar; intContador++)
				{
					vspFicheros.Row = intContador;
					vspFicheros.Col = Col_Elimina;
					string auxVar = String.Empty;
					if (Convert.ToString(vspFicheros.Value) != "")
					{
						bool tempBool = false;
						auxVar = Convert.ToString(vspFicheros.Value);
						if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
						{
							vspFicheros.Col = Col_ClaDocu;
							strNomDoc = vspFicheros.Text;
							vspFicheros.Col = Col_FecCrea;
							datFechAlta = DateTime.Parse(vspFicheros.Text);

							vspFicheros.Col = Col_NumDocu;
							intNumDoc = Convert.ToInt32(Double.Parse(vspFicheros.Text));

							object tempRefParam = datFechAlta;
							SqlCommand tempCommand = new SqlCommand("DELETE DDOCUPDF " + 
							                         "WHERE gnombdoc = '" + strNomDoc + "' AND " + 
							                         "inumdocu = " + intNumDoc.ToString() + " AND " + 
							                         "fechalta = " + Serrores.FormatFechaHMS(tempRefParam), BGFicherosAportados.cnConexion);
							tempCommand.ExecuteNonQuery();
							datFechAlta = Convert.ToDateTime(tempRefParam);

						}
					}
				}

                BGFicherosAportados.cnConexion.CommitTransScope();

                ErrBorra = false;
			}
			catch (Exception excep)
			{
				if (!ErrBorra)
				{
					throw excep;
				}

				if (ErrBorra)
				{

					BGFicherosAportados.cnConexion.RollbackTransScope();

                    RadMessageBox.Show(excep.Message, "Error en proceso de eliminaci�n", MessageBoxButtons.OK, RadMessageIcon.Info);

				}
			}
		}


		private bool fVisualiza()
		{
			int intNumDoc = 0;
			System.DateTime datFechAlta = DateTime.FromOADate(0);
			string strSql = String.Empty;
			DataSet rdoTemp = null;
			string varDocum = String.Empty;
			string strNomFichero = String.Empty;
			int intError = 0;

			try
			{

				int tempForVar = vspFicheros.MaxRows;
				for (int intContador = 1; intContador <= tempForVar; intContador++)
				{
					vspFicheros.Row = intContador;
					vspFicheros.Col = Col_Imprime;
					bool tempBool = false;
                    string auxVar = String.Empty;
                    auxVar = Convert.ToString(vspFicheros.Value);
					
					if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
					{

						vspFicheros.Value = 0;

						vspFicheros.Col = Col_NumDocu;
						intNumDoc = Convert.ToInt32(Double.Parse(vspFicheros.Text));
						vspFicheros.Col = Col_FecCrea;
						datFechAlta = DateTime.Parse(vspFicheros.Text);
						vspFicheros.Col = Col_NomDocu;
						strNomFichero = Path.GetDirectoryName(Application.ExecutablePath) + "\\tmp" + vspFicheros.Text;

						object tempRefParam = datFechAlta;
						strSql = "SELECT odocupdf FROM DDOCUPDF " + 
						         "WHERE gnombdoc = '" + BGFicherosAportados.gNombreDoc + "' AND " + 
						         "inumdocu = " + intNumDoc.ToString() + " AND " + 
						         "fechalta = " + Serrores.FormatFechaHMS(tempRefParam);
						datFechAlta = Convert.ToDateTime(tempRefParam);
						SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, BGFicherosAportados.cnConexion);
						rdoTemp = new DataSet();
						tempAdapter.Fill(rdoTemp);

						if (rdoTemp.Tables[0].Rows.Count != 0)
						{
							varDocum = RecuperarFichero((string) rdoTemp.Tables[0].Rows[0]["odocupdf"], strNomFichero);
							if (varDocum.Trim() != "")
							{
								string tempRefParam2 = "open";
								string tempRefParam3 = "";
								string tempRefParam4 = "";
								intError = UpgradeSupportHelper.PInvoke.SafeNative.shell32.ShellExecute(this.Handle, ref tempRefParam2, ref strNomFichero, ref tempRefParam3, ref tempRefParam4, 1);

								if (intError <= 31)
								{

									for (int intConError = 0; intConError <= arrError.GetUpperBound(0); intConError++)
									{
										if (intError == arrError[intConError, 0])
										{
											RadMessageBox.Show("- Error al procesar el fichero " + strNomFichero + "\r" + "\r" + arrError[intConError, 1].ToString(), "Error de proceso.", MessageBoxButtons.OK, RadMessageIcon.Info);
										}
									}

								}
								if (BGFicherosAportados.gstIdenPaciente.Trim() != "")
								{
									Serrores.GrabarDCONSUHC(BGFicherosAportados.gstIdenPaciente, "S", BGFicherosAportados.sUsuario, BGFicherosAportados.cnConexion);
								}
							}
						}
                        rdoTemp.Close();
					}
				}
			}
			catch (System.Exception excep)
			{

				RadMessageBox.Show(Information.Err().Number.ToString() + " - " + excep.Message, "Error al visualizar", MessageBoxButtons.OK, RadMessageIcon.Info);
			}
			return false;
		}

		public string RecuperarFichero(string varFichero, string strNomFichero)
		{
			bool error = false;
			string result = String.Empty;
			int intFichero = 0;
			byte[] bytFichero = null; //Variable a almacenar en Fichero

			try
			{
				error = true;



				lblTextos[2].Text = "Proceso de recuperaci�n";
				lblTextos[2].Refresh();

				bytFichero = (byte[]) ArraysHelper.DeepCopy(varFichero);

				//'strNomFichero = App.Path & "\tmp" & strNomFichero
				if (FileSystem.Dir(strNomFichero, FileAttribute.Normal) != "")
				{
					File.Delete(strNomFichero);
				}

				intFichero = FileSystem.FreeFile();

				FileSystem.FileOpen(intFichero, strNomFichero, OpenMode.Binary, OpenAccess.Write, OpenShare.Default, -1);

				//lngLongitud = UBound(bytFichero())

				//Escribimos los bytes del array anterior, en el nuevo archivo ( archivo 2 )
				FileSystem.FilePutObject(intFichero, bytFichero, 0);


				FileSystem.FileClose(intFichero);

				lblTextos[2].Text = "Descripci�n:";
				lblTextos[2].Refresh();

				result = strNomFichero;

				error = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!error)
				{
					throw excep;
				}

				if (error)
				{

					RadMessageBox.Show(excep.Message, "Error al generar fichero temporal " + strNomFichero, MessageBoxButtons.OK, RadMessageIcon.Info);
					return "";

				}
			}
			return System.String.Empty;
		}



		private void CargaErrorShell()
		{
			//ERROR_FILE_NOT_FOUND : Fichero no encontrado
			//ERROR_PATH_NOT_FOUND : Directorio no encontrado
			//ERROR_BAD_FORMAT : Fichero .EXE inv�lido
			//SE_ERR_ACCESSDENIED:  Acceso denegado
			//SE_ERR_ASSOCINCOMPLETE : La asociaci�n del fichero es inv�lida o incompleta
			//SE_ERR_DDEBUSY : La opreaci�n DDE no puede ser completada porque se est� procesando otra operaci�n DDE
			//SE_ERR_DDEFAIL : La operaci�n DDE fall�
			//SE_ERR_DDETIMEOUT : Time out en la operaci�n DDE
			//SE_ERR_DLLNOTFOUND : Dll no encontrada
			//SE_ERR_FNF : Fichero no encontrado
			//SE_ERR_NOASSOC : No hay aplicaci�n asociada con la extensi�n dada
			//SE_ERR_OOM : No hay memoria suficiente para completar la operaci�n
			//SE_ERR_PNF El camino especificado no fue encontrado
			//SE_ERR_SHARE Violaci�n de compartici�n


			arrError[0, 0] = SE_ERR_ACCESSDENIED;
			arrError[0, 1] = Convert.ToInt32(Double.Parse("Fichero no encontrado."));

			arrError[1, 0] = SE_ERR_ASSOCINCOMPLETE;
			arrError[1, 1] = Convert.ToInt32(Double.Parse("Directorio no encontrado."));

			arrError[2, 0] = ERROR_BAD_FORMAT;
			arrError[2, 1] = Convert.ToInt32(Double.Parse("Fichero .EXE inv�lido."));

			arrError[3, 0] = SE_ERR_ACCESSDENIED;
			arrError[3, 1] = Convert.ToInt32(Double.Parse("Acceso denegado."));

			arrError[4, 0] = SE_ERR_ASSOCINCOMPLETE;
			arrError[4, 1] = Convert.ToInt32(Double.Parse("La asociaci�n del fichero es inv�lida o incompleta."));

			arrError[5, 0] = SE_ERR_DDEBUSY;
			arrError[5, 1] = Convert.ToInt32(Double.Parse("La opreaci�n DDE no puede ser completada porque se est� procesando otra operaci�n DDE."));

			arrError[6, 0] = SE_ERR_DDEFAIL;
			arrError[6, 1] = Convert.ToInt32(Double.Parse("La operaci�n DDE fall�."));

			arrError[7, 0] = SE_ERR_DDETIMEOUT;
			arrError[7, 1] = Convert.ToInt32(Double.Parse("Time out en la operaci�n DDE."));

			arrError[8, 0] = SE_ERR_DLLNOTFOUND;
			arrError[8, 1] = Convert.ToInt32(Double.Parse("Dll no encontrada."));

			arrError[9, 0] = SE_ERR_FNF;
			arrError[9, 1] = Convert.ToInt32(Double.Parse("Fichero no encontrado."));

			arrError[10, 0] = SE_ERR_NOASSOC;
			arrError[10, 1] = Convert.ToInt32(Double.Parse("No hay aplicaci�n asociada con la extensi�n dada."));

			arrError[11, 0] = SE_ERR_OOM;
			arrError[11, 1] = Convert.ToInt32(Double.Parse("No hay memoria suficiente para completar la operaci�n."));

			arrError[12, 0] = SE_ERR_PNF;
			arrError[12, 1] = Convert.ToInt32(Double.Parse("El camino especificado no fue encontrado"));

			arrError[13, 0] = SE_ERR_SHARE;
			arrError[13, 1] = Convert.ToInt32(Double.Parse("Violaci�n de compartici�n."));

		}


		//UPGRADE_NOTE: (7001) The following declaration (fDescrip) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private bool fDescrip(string pstClave, string pstDescri)
		//{
				//
				//bool result = false;
				//string stSql = "SELECT * FROM DDOCUPDF " + 
				//               "WHERE gnombdoc = '" + pstClave + "'";
				//SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, BGFicherosAportados.cnConexion);
				//DataSet rdTemp = new DataSet();
				//tempAdapter.Fill(rdTemp);
				//
				//result = rdTemp.Tables[0].Rows.Count != 0;
				//
				////UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//rdTemp.Close();
				//
				//return result;
		//}

		private void ActualizaDatos()
		{
			string stSql = String.Empty;
			string stDocum = String.Empty;
			int inDocum = 0;
			bool blnMensa = false;


			Line1.Visible = false;
			Label1.Visible = false;
			int tempForVar = vspFicheros.MaxRows;
			for (int intContador = 1; intContador <= tempForVar; intContador++)
			{
				vspFicheros.Row = intContador;

				vspFicheros.Col = col_Modific;
				if (StringsHelper.ToDoubleSafe(Convert.ToString(vspFicheros.Value)) == 1)
				{
					vspFicheros.Col = Col_Descrip;

					if (vspFicheros.Text.Trim() == "" && !blnMensa)
					{
						short tempRefParam = 1040;
						string[] tempRefParam2 = new string[]{"descripci�n, es un valor"};
						BGFicherosAportados.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda,  tempRefParam, BGFicherosAportados.cnConexion, tempRefParam2);
						Line1.Visible = true;
						Label1.Visible = true;
						blnMensa = true;
						int tempForVar2 = vspFicheros.MaxCols;
						for (int I = 1; I <= tempForVar2; I++)
						{
							vspFicheros.Col = I;
							vspFicheros.ForeColor = Color.Red;
							//vspFicheros.FontBold = True
						}

					}
					else
					{

						vspFicheros.Col = Col_ClaDocu;
						stDocum = vspFicheros.Text;
						vspFicheros.Col = Col_NumDocu;
						inDocum = Convert.ToInt32(Double.Parse(vspFicheros.Text));
						vspFicheros.Col = Col_Descrip;

						stSql = "UPDATE DDOCUPDF " + 
						        "SET ddescrip = '" + vspFicheros.Text + "' " + 
						        "WHERE gnombdoc = '" + stDocum + "' AND " + 
						        "inumdocu = " + inDocum.ToString();

						SqlCommand tempCommand = new SqlCommand(stSql, BGFicherosAportados.cnConexion);
						tempCommand.ExecuteNonQuery();

						int tempForVar3 = vspFicheros.MaxCols;
						for (int I = 1; I <= tempForVar3; I++)
						{
							vspFicheros.Col = I;
							vspFicheros.ForeColor = Color.Black;
							//vspFicheros.FontBold = True
						}

					}
					vspFicheros.Col = col_Modific;
					vspFicheros.Value = 0;
				}
			}

			cmbBotones[5].Enabled = false;


		}
		private void frmFicherosAportados_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}
	}
}