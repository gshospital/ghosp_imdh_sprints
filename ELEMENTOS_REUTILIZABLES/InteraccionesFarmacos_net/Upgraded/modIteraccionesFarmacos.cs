using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace InteraccionesFarmacos
{
	internal static class modIteraccionesFarmacos
	{
        // Constantes globales

		public const int Col_0_CodArticulo1 = 1;
		public const int Col_0_DesArticulo1 = 2;
		public const int Col_0_IteraIncom = 3;
		public const int Col_0_CodArticulo2 = 4;
		public const int Col_0_DesArticulo2 = 5;


		public const int Col_1_CodPActivo1 = 1;
		public const int Col_1_DesPActivo1 = 2;
		public const int Col_1_IteraIncom = 3;
		public const int Col_1_CodPActivo2 = 4;
		public const int Col_1_DesPActivo2 = 5;

		public const int Col_2_PActivo1 = 1;
		public const int Col_2_Especialidad1 = 2;
		public const int Col_2_PActivo2 = 3;
		public const int Col_2_Especialidad2 = 4;



		public static string vstA�oRegistro = String.Empty;
		public static string vstNumRegistro = String.Empty;
		public static string vstTipoServicio = String.Empty;

		public static SqlConnection RcFiliacion = null;

		public static object fPantalla = null;
		public static clsIteracciones fClase = null;
		public static string gUsuario = String.Empty;
		public static string gContrase�a = String.Empty;

		//para formatear la fecha
		public static string Formato_Fecha_SQL = String.Empty;
		public static string Separador_Fecha_SQL = String.Empty;
		public static string Separador_Hora_SQL = String.Empty;
		public const int COLOR_NOFINPROCESOS = 0; //Negro
		public const int COLOR_FINPROCESOS = 194; //Rojo
		public const int COLOR_NOFINPROGRAMAS = 0x808080; //Gris
		public const int COLOR_FINPROGRAMAS = 0xFF0000; //Azul
		public static string vstSeparadorDecimal = String.Empty;
		public static Mensajes.ClassMensajes clase_mensaje = null;

		public static string VstUsuario_NT = String.Empty; //Usuario de NT
		public static string GAplicacion = String.Empty;
		public static string GAyudaFile = String.Empty;



        internal static int MuestraMensaje(ref int icodigo, ref string st1, ref string st2, ref string st3, ref string st4, ref string st5, ref string st6)
        {

            int result = 0;
            Mensajes.ClassMensajes oClass = new Mensajes.ClassMensajes();
            short tempRefParam = (short)icodigo;
            string[] tempRefParam2 = new string []{st1, st2, st3, st4, st5, st6};
			result = Convert.ToInt32(oClass.RespuestaMensaje( GAplicacion,  GAyudaFile, tempRefParam,  RcFiliacion,  tempRefParam2));
			icodigo = tempRefParam;

			return result;
		}

		internal static int MuestraMensaje(ref int icodigo, ref string st1, ref string st2, ref string st3, ref string st4, ref string st5)
		{
			string tempRefParam = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref st2, ref st3, ref st4, ref st5, ref tempRefParam);
		}

		internal static int MuestraMensaje(ref int icodigo, ref string st1, ref string st2, ref string st3, ref string st4)
		{
			string tempRefParam2 = String.Empty;
			string tempRefParam3 = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref st2, ref st3, ref st4, ref tempRefParam2, ref tempRefParam3);
		}

		internal static int MuestraMensaje(ref int icodigo, ref string st1, ref string st2, ref string st3)
		{
			string tempRefParam4 = String.Empty;
			string tempRefParam5 = String.Empty;
			string tempRefParam6 = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref st2, ref st3, ref tempRefParam4, ref tempRefParam5, ref tempRefParam6);
		}

		internal static int MuestraMensaje(ref int icodigo, ref string st1, ref string st2)
		{
			string tempRefParam7 = String.Empty;
			string tempRefParam8 = String.Empty;
			string tempRefParam9 = String.Empty;
			string tempRefParam10 = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref st2, ref tempRefParam7, ref tempRefParam8, ref tempRefParam9, ref tempRefParam10);
		}

		internal static int MuestraMensaje(ref int icodigo, ref string st1)
		{
			string tempRefParam11 = String.Empty;
			string tempRefParam12 = String.Empty;
			string tempRefParam13 = String.Empty;
			string tempRefParam14 = String.Empty;
			string tempRefParam15 = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref tempRefParam11, ref tempRefParam12, ref tempRefParam13, ref tempRefParam14, ref tempRefParam15);
		}

		internal static int MuestraMensaje(ref int icodigo)
		{
			string tempRefParam16 = String.Empty;
			string tempRefParam17 = String.Empty;
			string tempRefParam18 = String.Empty;
			string tempRefParam19 = String.Empty;
			string tempRefParam20 = String.Empty;
			string tempRefParam21 = String.Empty;
			return MuestraMensaje(ref icodigo, ref tempRefParam16, ref tempRefParam17, ref tempRefParam18, ref tempRefParam19, ref tempRefParam20, ref tempRefParam21);
		}

		internal static void ObtenerValoresTipoBD()
		{
			string formato = String.Empty;
			int i = 0;
			string tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, RcFiliacion);
			DataSet tRr = new DataSet();
			tempAdapter.Fill(tRr);
		
			switch(Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
			{
				case "SQL SERVER 6.5" : 
					
					Formato_Fecha_SQL = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();  //MM/DD/YYYY HH:MI 
					
					formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim(); 
					for (i = 1; i <= formato.Length; i++)
					{
						if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && Separador_Fecha_SQL == "")
						{
							Separador_Fecha_SQL = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
						}
						if (Separador_Fecha_SQL != "")
						{
							break;
						}
					} 
					formato = formato.Substring(Strings.InStr(i + 1, formato, Separador_Fecha_SQL, CompareMethod.Binary)); 
					for (i = 1; i <= formato.Length; i++)
					{
						if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && Separador_Hora_SQL == "")
						{
							Separador_Hora_SQL = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
						}
						if (Separador_Hora_SQL != "")
						{
							break;
						}
					} 
					break;
			}
			
			tRr.Close();

		}
	}
}