using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace InteraccionesFarmacos
{
	public class clsIteracciones
	{
		public void Llamada(object nForm, clsIteracciones nClase, SqlConnection nConnet, string DatosPaciente, string stiTipoSer, int anoRegi, int numRegi, string usuario)
		{
			modIteraccionesFarmacos.fPantalla = nForm;
			modIteraccionesFarmacos.fClase = nClase;
			modIteraccionesFarmacos.RcFiliacion = nConnet;
			modIteraccionesFarmacos.clase_mensaje = new Mensajes.ClassMensajes();

			Serrores.Obtener_TipoBD_FormatoFechaBD(ref modIteraccionesFarmacos.RcFiliacion);
			Serrores.AjustarRelojCliente(modIteraccionesFarmacos.RcFiliacion);
			Serrores.ObtenerNombreAplicacion(modIteraccionesFarmacos.RcFiliacion);
			modIteraccionesFarmacos.gUsuario = usuario;

			modIteraccionesFarmacos.vstTipoServicio = stiTipoSer;
			modIteraccionesFarmacos.vstAņoRegistro = anoRegi.ToString();
			modIteraccionesFarmacos.vstNumRegistro = numRegi.ToString();

			frmIteraccionesFarmacos.DefInstance.lbPaciente.Text = DatosPaciente;
			frmIteraccionesFarmacos.DefInstance.ShowDialog();
		}
	}
}