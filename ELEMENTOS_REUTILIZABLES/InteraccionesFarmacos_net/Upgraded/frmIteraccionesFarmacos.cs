using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
//using UpgradeHelpers.VB6.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace InteraccionesFarmacos
{
	public partial class frmIteraccionesFarmacos
          : Telerik.WinControls.UI.RadForm
    {

		public string FormularioAnterior = String.Empty;
		bool blntieneAccesoUsuario = false;
		bool blnVieneIteraciones = false;
		public frmIteraccionesFarmacos()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}

		private void cbSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}
        
		private void cmbIncompatibilidad_Click()
		{
			bool existeIteracion = false;
			string farmaco1 = String.Empty;
			string farmaco2 = String.Empty;

			string StSql = "SELECT a.gfarmaco farmaco1, C.dnomfarm desFarmaco1, b.gfarmaco farmaco2, D.dnomfarm desFarmaco2, IFMSFM_INTERINCOM.ctipintinc, IFMSFM_TIPOINTINC.dtipintinc " + 
			               "FROM IFMSFM_INTERINCOM " + 
			               "LEFT JOIN EFARMACO a ON a.gfarmaco = articula  and a.ffinmovi is null AND a.iesttrat <> 'F'  AND a.iesttrat <> 'S' AND " + 
			               "a.itiposer =  '" + modIteraccionesFarmacos.vstTipoServicio + "' AND a.ganoregi = " + modIteraccionesFarmacos.vstA�oRegistro + " AND a.gnumregi = " + modIteraccionesFarmacos.vstNumRegistro + " " + 
			               "INNER JOIN DCODFARMVT C ON  a.gfarmaco = C.gfarmaco " + 
			               "LEFT JOIN EFARMACO b ON b.gfarmaco = articulb AND b.ffinmovi is null AND b.iesttrat <> 'F'  AND b.iesttrat <> 'S' AND " + 
			               "b.itiposer =  '" + modIteraccionesFarmacos.vstTipoServicio + "' AND b.ganoregi = " + modIteraccionesFarmacos.vstA�oRegistro + " AND b.gnumregi = " + modIteraccionesFarmacos.vstNumRegistro + " " + 
			               "INNER JOIN DCODFARMVT D ON  b.gfarmaco = D.gfarmaco " + 
			               "LEFT JOIN IFMSFM_TIPOINTINC  ON IFMSFM_INTERINCOM.ctipintinc = IFMSFM_TIPOINTINC.ctipintinc " + 
			               "WHERE IFMSFM_INTERINCOM.iborrado='N' " + 
			               "GROUP BY a.gfarmaco, C.dnomfarm,  b.gfarmaco, D.dnomfarm, IFMSFM_INTERINCOM.ctipintinc, dtipintinc " + 
			               "ORDER BY a.gfarmaco, b.gfarmaco";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, modIteraccionesFarmacos.RcFiliacion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			vaSpread[0].MaxRows = 0;
			// Recorremos las interacciones y las apuntamos
			foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
			{
				existeIteracion = false;
				int tempForVar = vaSpread[0].MaxRows;
				for (int contador = 1; contador <= tempForVar; contador++)
				{
					vaSpread[0].Row = contador;
					vaSpread[0].Col = 1;
					farmaco1 = vaSpread[0].Text;
					vaSpread[0].Col = 4;
					farmaco2 = vaSpread[0].Text;

					if (farmaco2 == Convert.ToString(iteration_row["farmaco1"]) && farmaco1 == Convert.ToString(iteration_row["farmaco2"]))
					{
						existeIteracion = true;
						break;
					}
				}

				if (!existeIteracion)
				{
					vaSpread[0].MaxRows++;
					vaSpread[0].Row = vaSpread[0].MaxRows;
					vaSpread[0].Col = 1;
					vaSpread[0].Text = Convert.ToString(iteration_row["farmaco1"]);
					vaSpread[0].Col = 2;
					vaSpread[0].Text = Convert.ToString(iteration_row["desFarmaco1"]);
					vaSpread[0].Col = 3;
					vaSpread[0].Text = Convert.ToString(iteration_row["dtipintinc"]);
					vaSpread[0].Col = 4;
					vaSpread[0].Text = Convert.ToString(iteration_row["farmaco2"]);
					vaSpread[0].Col = 5;
					vaSpread[0].Text = Convert.ToString(iteration_row["desFarmaco2"]);
				}
			}
			rdoTemp.Close();


			StSql = "SELECT D.gpactivo pActivo1, F.tpactivo desPActivo1, e.gpactivo pActivo2, g.tpactivo desPActivo2, dtipintinc " + 
			        "FROM IFMSFM_INTERINPA " + 
			        "LEFT JOIN FCOMPFAR D ON IFMSFM_INTERINPA.pactivoa = D.gpactivo " + 
			        "INNER JOIN IFMSAL_PACTIVO F ON D.gpactivo = F.PACTIVO " + 
			        "LEFT JOIN FCOMPFAR E ON IFMSFM_INTERINPA.pactivob = E.gpactivo " + 
			        "INNER JOIN IFMSAL_PACTIVO G  ON E.gpactivo = G.PACTIVO " + 
			        "INNER JOIN EFARMACO a ON A.gfarmaco = D.gfarmaco AND a.ffinmovi is null AND a.iesttrat <> 'F'  AND a.iesttrat <> 'S' AND " + 
			        "a.itiposer =  '" + modIteraccionesFarmacos.vstTipoServicio + "' AND a.ganoregi = " + modIteraccionesFarmacos.vstA�oRegistro + "  AND a.gnumregi = " + modIteraccionesFarmacos.vstNumRegistro + "  " + 
			        "INNER JOIN EFARMACO b ON B.gfarmaco = e.gfarmaco  and b.ffinmovi is null AND b.iesttrat <> 'F'  AND b.iesttrat <> 'S' AND " + 
			        "b.itiposer =  '" + modIteraccionesFarmacos.vstTipoServicio + "' AND b.ganoregi = " + modIteraccionesFarmacos.vstA�oRegistro + "  AND b.gnumregi = " + modIteraccionesFarmacos.vstNumRegistro + "  " + 
			        "LEFT JOIN IFMSFM_TIPOINTINC ON IFMSFM_INTERINPA.ctipintinc = IFMSFM_TIPOINTINC.ctipintinc " + 
			        "WHERE IFMSFM_INTERINPA.iborrado='N' " + 
			        "GROUP BY D.gpactivo, F.tpactivo, e.gpactivo, g.tpactivo, IFMSFM_INTERINPA.ctipintinc, dtipintinc " + 
			        "ORDER BY D.gpactivo, e.gpactivo";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, modIteraccionesFarmacos.RcFiliacion);
			rdoTemp = new DataSet();
			tempAdapter_2.Fill(rdoTemp);
			vaSpread[1].MaxRows = 0;
			foreach (DataRow iteration_row_2 in rdoTemp.Tables[0].Rows)
			{
				existeIteracion = false;
				int tempForVar2 = vaSpread[1].MaxRows;
				for (int contador = 1; contador <= tempForVar2; contador++)
				{
					vaSpread[1].Row = contador;
					vaSpread[1].Col = 1;
					farmaco1 = vaSpread[1].Text;
					vaSpread[1].Col = 4;
					farmaco2 = vaSpread[1].Text;

					if (farmaco2 == Convert.ToString(iteration_row_2["pActivo1"]) && farmaco1 == Convert.ToString(iteration_row_2["pActivo2"]))
					{
						existeIteracion = true;
						break;
					}
				}

				if (!existeIteracion)
				{
					vaSpread[1].MaxRows++;
					vaSpread[1].Row = vaSpread[1].MaxRows;
					vaSpread[1].Col = 1;
					vaSpread[1].Text = Convert.ToString(iteration_row_2["pActivo1"]);
					vaSpread[1].Col = 2;
					vaSpread[1].Text = Convert.ToString(iteration_row_2["desPActivo1"]);
					vaSpread[1].Col = 3;
					vaSpread[1].Text = Convert.ToString(iteration_row_2["dtipintinc"]);
					vaSpread[1].Col = 4;
					vaSpread[1].Text = Convert.ToString(iteration_row_2["pActivo2"]);
					vaSpread[1].Col = 5;
					vaSpread[1].Text = Convert.ToString(iteration_row_2["desPActivo2"]);
				}
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rdoTemp.Close();
		}

		private void cmbIteracciones_Click(Object eventSender, EventArgs eventArgs)
		{

			bool existeIteracion = false;
			string farmaco1 = String.Empty;
			string farmaco2 = String.Empty;
			Color colorFila = new Color();

			string StSql = "SELECT  C.dnomfarm desFarmaco1, F.tpactivo desPActivo1, D.dnomfarm desFarmaco2, G.tpactivo desPActivo2, a.gfarmaco farmaco1, " + 
			               "b.gfarmaco farmaco2, cod1a pActivo1, cod2a pActivo2, INTSIG , intmed  " + 
			               "FROM ifmsfm_interasfar " + 
			               "LEFT JOIN efarmaco a ON a.gfarmaco= art1 AND a.ffinmovi is null AND a.iesttrat <> 'F'  AND a.iesttrat <> 'S' AND " + 
			               "a.itiposer = '" + modIteraccionesFarmacos.vstTipoServicio + "' AND a.ganoregi = " + modIteraccionesFarmacos.vstA�oRegistro + " AND a.gnumregi =  " + modIteraccionesFarmacos.vstNumRegistro + "  " + 
			               "INNER JOIN DCODFARMVT C ON a.gfarmaco = C.gfarmaco " + 
			               "LEFT JOIN efarmaco b ON b.gfarmaco= art2 AND b.ffinmovi is null AND b.iesttrat <> 'F'  AND b.iesttrat <> 'S' AND " + 
			               "b.itiposer = '" + modIteraccionesFarmacos.vstTipoServicio + "' AND b.ganoregi = " + modIteraccionesFarmacos.vstA�oRegistro + " AND b.gnumregi =  " + modIteraccionesFarmacos.vstNumRegistro + "  " + 
			               "INNER JOIN DCODFARMVT D ON b.gfarmaco = D.gfarmaco " + 
			               "INNER JOIN IFMSAL_PACTIVO F ON cast(cod1a as char(10)) = F.PACTIVO " + 
			               "INNER JOIN IFMSAL_PACTIVO G ON cast(cod2a as char(10)) = G.PACTIVO " + 
			               "WHERE cod1a IS NOT NULL AND cod2a IS NOT NULL " + 
			               "GROUP BY C.dnomfarm, F.tpactivo, D.dnomfarm, G.tpactivo, a.gfarmaco, b.gfarmaco, cod1a, cod2a, INTSIG, intmed " + 
			               "ORDER BY a.gfarmaco, cod1a";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, modIteraccionesFarmacos.RcFiliacion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			vaSpread[2].MaxRows = 0;
			Color colorDefault = vaSpread[2].BackColor;
			foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
			{
				existeIteracion = false;
				int tempForVar = vaSpread[2].MaxRows;
				for (int contador = 1; contador <= tempForVar; contador++)
				{
					vaSpread[2].Row = contador;
					vaSpread[2].Col = 1;
					farmaco1 = vaSpread[2].Text;
					vaSpread[2].Col = 4;
					farmaco2 = vaSpread[2].Text;

					if (farmaco2 == Convert.ToString(iteration_row["pActivo1"]) && farmaco1 == Convert.ToString(iteration_row["pActivo2"]))
					{
						existeIteracion = true;
						break;
					}
				}

				if (!existeIteracion)
				{
					vaSpread[2].MaxRows++;
					vaSpread[2].Row = vaSpread[2].MaxRows;
					colorFila = colorDefault;

					if (Convert.ToDouble(iteration_row["intmed"]) == 2)
					{
						colorFila = lblAzul.BackColor;
					}
					if (Convert.ToDouble(iteration_row["intmed"]) == 3)
					{
						colorFila = lblNaraja.BackColor;
					}

					if (Convert.ToDouble(iteration_row["INTSIG"]) == 1 && Convert.ToDouble(iteration_row["intmed"]) != 2 && Convert.ToDouble(iteration_row["intmed"]) != 3)
					{
						colorFila = lblRojo.BackColor;
					}
					if (Convert.ToDouble(iteration_row["INTSIG"]) >= 2 && Convert.ToDouble(iteration_row["INTSIG"]) <= 6 && Convert.ToDouble(iteration_row["intmed"]) != 2 && Convert.ToDouble(iteration_row["intmed"]) != 3)
					{
						colorFila = lblAmarillo.BackColor;
					}

					if (Convert.ToDouble(iteration_row["INTSIG"]) > 6 && Convert.ToDouble(iteration_row["intmed"]) != 2 && Convert.ToDouble(iteration_row["intmed"]) != 3)
					{
						colorFila = lblVerde.BackColor;
					}

					vaSpread[2].Col = 1;
					vaSpread[2].Text = Convert.ToString(iteration_row["desPActivo1"]);
					vaSpread[2].SetColWidth(1, 20);
					vaSpread[2].BackColor = colorFila;

					vaSpread[2].Col = 2;
					vaSpread[2].Text = Convert.ToString(iteration_row["desFarmaco1"]);
					vaSpread[2].SetColWidth(2, 30);
					vaSpread[2].BackColor = colorFila;

					vaSpread[2].Col = 3;
					vaSpread[2].Text = Convert.ToString(iteration_row["desPActivo2"]);
					vaSpread[2].SetColWidth(3, 20);
					vaSpread[2].BackColor = colorFila;

					vaSpread[2].Col = 4;
					vaSpread[2].SetColWidth(4, 30);
					vaSpread[2].Text = Convert.ToString(iteration_row["desFarmaco2"]);
					vaSpread[2].BackColor = colorFila;

					vaSpread[2].Col = 5;
					vaSpread[2].Text = Convert.ToString(iteration_row["pActivo1"]);
					vaSpread[2].BackColor = colorFila;
					vaSpread[2].SetColHidden(vaSpread[2].Col, true);

					vaSpread[2].Col = 6;
					vaSpread[2].Text = Convert.ToString(iteration_row["farmaco1"]);
					vaSpread[2].BackColor = colorFila;
					vaSpread[2].SetColHidden(vaSpread[2].Col, true);

					vaSpread[2].Col = 7;
					vaSpread[2].Text = Convert.ToString(iteration_row["pActivo2"]);
					vaSpread[2].BackColor = colorFila;
					vaSpread[2].SetColHidden(vaSpread[2].Col, true);

					vaSpread[2].Col = 8;
					vaSpread[2].Text = Convert.ToString(iteration_row["farmaco2"]);
					vaSpread[2].BackColor = colorFila;
					vaSpread[2].SetColHidden(vaSpread[2].Col, true);

					vaSpread[2].Col = 9;
					vaSpread[2].Text = (Convert.IsDBNull(iteration_row["INTSIG"])) ? "" : Convert.ToString(iteration_row["INTSIG"]);
					vaSpread[2].BackColor = colorFila;
					vaSpread[2].SetColHidden(vaSpread[2].Col, true);

					vaSpread[2].Col = 10;
					vaSpread[2].Text = (Convert.IsDBNull(iteration_row["intmed"])) ? "" : Convert.ToString(iteration_row["intmed"]);
					vaSpread[2].BackColor = colorFila;
					vaSpread[2].SetColHidden(vaSpread[2].Col, true);

					//       vaSpread(2).SelModeIndex =
				}
			}
            
			rdoTemp.Close();
			rdoTemp = null;
		}

		
		private void frmIteraccionesFarmacos_Load(Object eventSender, EventArgs eventArgs)
		{
			string sql = String.Empty;
			DataSet Rr = null;

			modIteraccionesFarmacos.ObtenerValoresTipoBD();
			modIteraccionesFarmacos.vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(modIteraccionesFarmacos.RcFiliacion); //obtener el separador decimal
            
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			cmbIncompatibilidad_Click();
			cmbIteracciones_Click(cmbIteracciones, new EventArgs());

			SSTabHelper.SetSelectedIndex(SSTab1, 0);

            _SSTab1_TabPage0.Item.BorderColor = Color.FromArgb(110, 153, 210);
            _SSTab1_TabPage1.Item.BorderColor = Color.FromArgb(110, 153, 210);
            _SSTab1_TabPage2.Item.BorderColor = Color.FromArgb(110, 153, 210);
        }

		private void SSTab1_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{

			if (SSTabHelper.GetSelectedIndex(SSTab1) == 2 && !blnVieneIteraciones)
			{
				SSTabHelper.SetSelectedIndex(SSTab1, SSTab1PreviousTab);
			}
            
			SSTab1PreviousTab = SSTab1.SelectedPage.Item.ZIndex;
        }


        private void vaSpread_DoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            int Index = Array.IndexOf(this.vaSpread, eventSender);
            string StSql = String.Empty;
            DataSet rdoTemp = null;
            int Row = eventArgs.RowIndex + 1;
            string stCod1A = String.Empty;
            string stCod2A = String.Empty;

            int[,] inicio = new int[10, 10];
            string cadena = String.Empty;


           
            if (Index == 2 && Row > 0)
            {
                //pario  TODO PARIO_X_5
                /*txtDetalle.Rtf = "";
                txtDetalle.SelectionStart = 0;
                txtDetalle.SelectionLength = Strings.Len(txtDetalle.Rtf);
                txtDetalle.SelectionFont = txtDetalle.SelectionFont.Change(bold: false);
                txtDetalle.SelectionFont = txtDetalle.SelectionFont.Change(underline: false);*/
                txtDetalle.ToggleBold();
                txtDetalle.ToggleUnderline();

                vaSpread[2].Row = Row;

                vaSpread[2].Col = 1;
                lblDescripPrincipio1.Text = vaSpread[2].Text;

                vaSpread[2].Col = 2;
                lblDescriEspecialidad1.Text = vaSpread[2].Text;

                vaSpread[2].Col = 5;
                lblCodigoPrincipio1.Text = vaSpread[2].Text;

                vaSpread[2].Col = 6;
                lblCodigoEspecialidad1.Text = vaSpread[2].Text;

                vaSpread[2].Col = 3;
                lblDescripPrincipio2.Text = vaSpread[2].Text;

                vaSpread[2].Col = 4;
                lblDescriEspecialidad2.Text = vaSpread[2].Text;

                vaSpread[2].Col = 7;
                lblCodigoPrincipio2.Text = vaSpread[2].Text;

                vaSpread[2].Col = 8;
                lblCodigoEspecialidad2.Text = vaSpread[2].Text;

                vaSpread[2].Col = 1;

                StSql = "Select ISNULL(IFMSFM_INTSEN.nombre, '') sentido, ISNULL(IFMSFM_INTSIG.nombre, '') significacion, ISNULL(IFMSFM_INTNAT.nombre, '') naturaleza, " +
                        "ISNULL(IFMSFM_INTMED.nombre, '') medidas, ISNULL(titulo, '') titulo, ISNULL(A.texto_com, '') efectos, ISNULL(B.texto_com, '') improtancia, " +
                        "ISNULL(C.texto_com, '') mecanismo, ISNULL(D.texto_com, '') evidencias, ISNULL(E.texto_com, '') referencias " +
                        "From ifmsfm_interas " +
                        "LEFT JOIN IFMSFM_INTSEN ON ifmsfm_interas.INTSEN = IFMSFM_INTSEN.INTSEN " +
                        "LEFT JOIN IFMSFM_INTSIG ON ifmsfm_interas.intsig = IFMSFM_INTSIG.intsig " +
                        "LEFT JOIN IFMSFM_INTNAT ON ifmsfm_interas.INTNAT = IFMSFM_INTNAT.INTNAT " +
                        "LEFT JOIN IFMSFM_INTMED ON ifmsfm_interas.INTMED = IFMSFM_INTMED.INTMED " +
                        "LEFT JOIN IFMSFM_TEXTO A ON ifmsfm_interas.EFECTO = A.coditext " +
                        "LEFT JOIN IFMSFM_TEXTO B ON ifmsfm_interas.importancia  = B.coditext " +
                        "LEFT JOIN IFMSFM_TEXTO C ON ifmsfm_interas.MECANISMO  = C.coditext " +
                        "LEFT JOIN IFMSFM_TEXTO D ON ifmsfm_interas.EVIDENCIAS  = D.coditext " +
                        "LEFT JOIN IFMSFM_TEXTO E ON ifmsfm_interas.REFERENCIAs  = E.coditext " +
                        "WHERE cod1A = '" + lblCodigoPrincipio1.Text + "' and cod2A='" + lblCodigoPrincipio2.Text + "'";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, modIteraccionesFarmacos.RcFiliacion);
                rdoTemp = new DataSet();
                tempAdapter.Fill(rdoTemp);
                if (rdoTemp.Tables[0].Rows.Count != 0)
                {
                    inicio = new int[10, 10];

                    inicio[0, 0] = 0;
                    inicio[0, 1] = ("SENTIDO:").Length;

                 
                    cadena = "SENTIDO:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["sentido"]) + "\r" + "\r";
                    inicio[1, 0] = cadena.Length;
                    inicio[1, 1] = ("SIGNIFICACI�N CL�NICA:").Length;

                  
                    cadena = cadena + "SIGNIFICACI�N CL�NICA:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["significacion"]) + "\r" + "\r";
                    inicio[2, 0] = cadena.Length;
                    inicio[2, 1] = ("NATURALEZA DE LA INTERACCI�N:").Length;
                 
                    cadena = cadena + "NATURALEZA DE LA INTERACCI�N:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["naturaleza"]) + "\r" + "\r";
                    inicio[3, 0] = cadena.Length;
                    inicio[3, 1] = ("MEDIDAS A TOMAR:").Length;

                 
                    cadena = cadena + "MEDIDAS A TOMAR:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["medidas"]) + "\r" + "\r";
                    inicio[4, 0] = cadena.Length;
                    inicio[4, 1] = ("DESCRIPCI�N:").Length;

                   
                    cadena = cadena + "DESCRIPCI�N:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["titulo"]) + "\r" + "\r";
                    inicio[5, 0] = cadena.Length;
                    inicio[5, 1] = ("EFECTOS:").Length;

                    cadena = cadena + "EFECTOS:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["efectos"]) + "\r" + "\r";
                    inicio[6, 0] = cadena.Length;
                    inicio[6, 1] = ("IMPORTANCIA:").Length;

                    
                    cadena = cadena + "IMPORTANCIA:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["improtancia"]) + "\r" + "\r";
                    inicio[7, 0] = cadena.Length;
                    inicio[7, 1] = ("MECANISMO:").Length;

                  
                    cadena = cadena + "MECANISMO:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["mecanismo"]) + "\r" + "\r";
                    inicio[8, 0] = cadena.Length;
                    inicio[8, 1] = ("EVIDENCIAS:").Length;

                 
                    cadena = cadena + "EVIDENCIAS:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["evidencias"]) + "\r" + "\r";
                    inicio[9, 0] = cadena.Length;
                    inicio[9, 1] = ("REFERENCIAS:").Length;

              
                    cadena = cadena + "REFERENCIAS:" + "\r" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["referencias"]) + "\r" + "\r";

                    //TODO PARIO_X_5
                    /*txtDetalle.Rtf = cadena;*/
                    for (int contador = 0; contador <= 9; contador++)
                    {
                        //TODO PARIO_X_5
                        /* txtDetalle.SelectionStart = inicio[contador, 0];
                         txtDetalle.SelectionLength = inicio[contador, 1];*/
                        txtDetalle.ToggleBold();
                        txtDetalle.ToggleUnderline();
                        /*  txtDetalle.SelectionFont = txtDetalle.SelectionFont.Change(bold:true);
                          txtDetalle.SelectionFont = txtDetalle.SelectionFont.Change(underline:true);*/
                    }

                    /* txtDetalle.SelectionStart = 0;
                     txtDetalle.SelectionLength = 0;*/

                }

                rdoTemp.Close();
                rdoTemp = null;
                blnVieneIteraciones = true;
                SSTabHelper.SetSelectedIndex(SSTab1, 2);
                blnVieneIteraciones = false;
            }
        }
        
		private void frmIteraccionesFarmacos_Closed(Object eventSender, EventArgs eventArgs)
		{
		}

        private void _SSTab1_TabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}