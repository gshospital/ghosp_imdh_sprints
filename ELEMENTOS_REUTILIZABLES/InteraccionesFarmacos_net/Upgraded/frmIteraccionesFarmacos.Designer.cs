using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace InteraccionesFarmacos
{
    partial class frmIteraccionesFarmacos
    {
        #region "Upgrade Support "
        private static frmIteraccionesFarmacos m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static frmIteraccionesFarmacos DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new frmIteraccionesFarmacos();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "lbPaciente", "Label1", "frmPaciente", "_vaSpread_0", "Frame1", "_vaSpread_1", "Frame2", "_SSTab1_TabPage0", "_vaSpread_2", "cmbIteracciones", "Label12", "Label11", "Label10", "Label9", "Label8", "lblNaraja", "lblAzul", "lblVerde", "lblAmarillo", "lblRojo", "_SSTab1_TabPage1", "txtDetalle", "lblDescriEspecialidad2", "lblCodigoEspecialidad2", "lblDescripPrincipio2", "lblCodigoPrincipio2", "lblDescriEspecialidad1", "lblCodigoEspecialidad1", "lblDescripPrincipio1", "lblCodigoPrincipio1", "Label16", "Label15", "Label14", "Label13", "_SSTab1_TabPage2", "SSTab1", "cbSalir", "vaSpread", "_vaSpread_2_Sheet1", "_vaSpread_1_Sheet1", "_vaSpread_0_Sheet1" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public Telerik.WinControls.RadToolTip ToolTipMain;
        public Telerik.WinControls.UI.RadTextBox lbPaciente;
        public Telerik.WinControls.UI.RadLabel Label1;
        public Telerik.WinControls.UI.RadGroupBox frmPaciente;
        private UpgradeHelpers.Spread.FpSpread _vaSpread_0;
        public Telerik.WinControls.UI.RadGroupBox Frame1;
        private UpgradeHelpers.Spread.FpSpread _vaSpread_1;
        public Telerik.WinControls.UI.RadGroupBox Frame2;
        private Telerik.WinControls.UI.RadPageViewPage _SSTab1_TabPage0; 
        private UpgradeHelpers.Spread.FpSpread _vaSpread_2;
        public Telerik.WinControls.UI.RadButton cmbIteracciones;
        public Telerik.WinControls.UI.RadLabel Label12;
        public Telerik.WinControls.UI.RadLabel Label11;
        public Telerik.WinControls.UI.RadLabel Label10;
        public Telerik.WinControls.UI.RadLabel Label9;
        public Telerik.WinControls.UI.RadLabel Label8;
        public Telerik.WinControls.UI.RadLabel lblNaraja;
        public Telerik.WinControls.UI.RadLabel lblAzul;
        public Telerik.WinControls.UI.RadLabel lblVerde;
        public Telerik.WinControls.UI.RadLabel lblAmarillo;
        public Telerik.WinControls.UI.RadLabel lblRojo;
        private Telerik.WinControls.UI.RadPageViewPage _SSTab1_TabPage1;
        public Telerik.WinControls.UI.RadRichTextEditor txtDetalle;
        public Telerik.WinControls.UI.RadTextBox lblDescriEspecialidad2;
        public Telerik.WinControls.UI.RadTextBox lblCodigoEspecialidad2;
        public Telerik.WinControls.UI.RadTextBox lblDescripPrincipio2;
        public Telerik.WinControls.UI.RadTextBox lblCodigoPrincipio2;
        public Telerik.WinControls.UI.RadTextBox lblDescriEspecialidad1;
        public Telerik.WinControls.UI.RadTextBox lblCodigoEspecialidad1;
        public Telerik.WinControls.UI.RadTextBox lblDescripPrincipio1;
        public Telerik.WinControls.UI.RadTextBox lblCodigoPrincipio1;
        public Telerik.WinControls.UI.RadLabel Label16;
        public Telerik.WinControls.UI.RadLabel Label15;
        public Telerik.WinControls.UI.RadLabel Label14;
        public Telerik.WinControls.UI.RadLabel Label13;
        private Telerik.WinControls.UI.RadPageViewPage _SSTab1_TabPage2;
        public Telerik.WinControls.UI.RadPageView SSTab1;
        public Telerik.WinControls.UI.RadButton cbSalir;
        public UpgradeHelpers.Spread.FpSpread[] vaSpread = new UpgradeHelpers.Spread.FpSpread[3];
        private int SSTab1PreviousTab;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIteraccionesFarmacos));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.frmPaciente = new Telerik.WinControls.UI.RadGroupBox();
            this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.SSTab1 = new Telerik.WinControls.UI.RadPageView();
            this._SSTab1_TabPage0 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this._vaSpread_0 = new UpgradeHelpers.Spread.FpSpread();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this._vaSpread_1 = new UpgradeHelpers.Spread.FpSpread();
            this._SSTab1_TabPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this._vaSpread_2 = new UpgradeHelpers.Spread.FpSpread();
            this.cmbIteracciones = new Telerik.WinControls.UI.RadButton();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.Label11 = new Telerik.WinControls.UI.RadLabel();
            this.Label10 = new Telerik.WinControls.UI.RadLabel();
            this.Label9 = new Telerik.WinControls.UI.RadLabel();
            this.Label8 = new Telerik.WinControls.UI.RadLabel();
            this.lblNaraja = new Telerik.WinControls.UI.RadLabel();
            this.lblAzul = new Telerik.WinControls.UI.RadLabel();
            this.lblVerde = new Telerik.WinControls.UI.RadLabel();
            this.lblAmarillo = new Telerik.WinControls.UI.RadLabel();
            this.lblRojo = new Telerik.WinControls.UI.RadLabel();
            this._SSTab1_TabPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.txtDetalle = new Telerik.WinControls.UI.RadRichTextEditor();
            this.lblDescriEspecialidad2 = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigoEspecialidad2 = new Telerik.WinControls.UI.RadTextBox();
            this.lblDescripPrincipio2 = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigoPrincipio2 = new Telerik.WinControls.UI.RadTextBox();
            this.lblDescriEspecialidad1 = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigoEspecialidad1 = new Telerik.WinControls.UI.RadTextBox();
            this.lblDescripPrincipio1 = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigoPrincipio1 = new Telerik.WinControls.UI.RadTextBox();
            this.Label16 = new Telerik.WinControls.UI.RadLabel();
            this.Label15 = new Telerik.WinControls.UI.RadLabel();
            this.Label14 = new Telerik.WinControls.UI.RadLabel();
            this.Label13 = new Telerik.WinControls.UI.RadLabel();
            this.cbSalir = new Telerik.WinControls.UI.RadButton();
            this.frmPaciente.SuspendLayout();
            this.SSTab1.SuspendLayout();
            this._SSTab1_TabPage0.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.Frame2.SuspendLayout();
            this._SSTab1_TabPage1.SuspendLayout();
            this._SSTab1_TabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // frmPaciente
            // 
            
            this.frmPaciente.Controls.Add(this.lbPaciente);
            this.frmPaciente.Controls.Add(this.Label1);
            this.frmPaciente.Enabled = true;
            this.frmPaciente.Location = new System.Drawing.Point(6, 0);
            this.frmPaciente.Name = "frmPaciente";
            this.frmPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmPaciente.Size = new System.Drawing.Size(511, 44);
            this.frmPaciente.TabIndex = 3;
            this.frmPaciente.Visible = true;
            // 
            // lbPaciente
            // 
          
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Location = new System.Drawing.Point(66, 13);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(430, 20);
            this.lbPaciente.TabIndex = 5;
            this.lbPaciente.ReadOnly = true;
            this.lbPaciente.Enabled = true;
            // 
            // Label1
            // 
          
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(13, 15);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(49, 17);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "Paciente:";
            // 
            // SSTab1
            // 
        //    this.SSTab1.Alignment = System.Windows.Forms.TabAlignment.Top;
           
            this.SSTab1.Controls.Add(this._SSTab1_TabPage0);
            this.SSTab1.Controls.Add(this._SSTab1_TabPage1);
            this.SSTab1.Controls.Add(this._SSTab1_TabPage2);
            this.SSTab1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
          //  this.SSTab1.HotTrack = false;
            this.SSTab1.ItemSize = new System.Drawing.Size(254, 24);
            this.SSTab1.Location = new System.Drawing.Point(6, 48);
          //  this.SSTab1.Multiline = true;

            this.SSTab1.Name = "SSTab1";
            this.SSTab1.Size = new System.Drawing.Size(770, 439);
           // this.SSTab1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.SSTab1.TabIndex = 0;
          //  this.SSTab1.SelectedIndexChanged += new System.EventHandler(this.SSTab1_SelectedIndexChanged);
            // 
            // _SSTab1_TabPage0
            // 
            this._SSTab1_TabPage0.Controls.Add(this.Frame1);
            this._SSTab1_TabPage0.Controls.Add(this.Frame2);
            this._SSTab1_TabPage0.Text = "Interacciones / Incompatibilidades.";
            // 
            // Frame1
            // 
           
            this.Frame1.Controls.Add(this._vaSpread_0);
            this.Frame1.Enabled = true;
            this.Frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.Frame1.Location = new System.Drawing.Point(10, 7);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(749, 191);
            this.Frame1.TabIndex = 6;
            this.Frame1.Text = "Interacciones incompatibles por articulo";
            this.Frame1.Visible = true;
            // 
            // _vaSpread_0
            // 
            this._vaSpread_0.Location = new System.Drawing.Point(8, 22);
            this._vaSpread_0.Name = "_vaSpread_0";
            this._vaSpread_0.Size = new System.Drawing.Size(734, 158);
            this._vaSpread_0.TabIndex = 7;
            this._vaSpread_0.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.vaSpread_DoubleClick);
          

            // 
            // Frame2
            // 
         
            this.Frame2.Controls.Add(this._vaSpread_1);
            this.Frame2.Enabled = true;
            this.Frame2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.Frame2.Location = new System.Drawing.Point(10, 203);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(749, 191);
            this.Frame2.TabIndex = 8;
            this.Frame2.Text = "Interacciones incompatibles por principio activo";
            this.Frame2.Visible = true;
            // 
            // _vaSpread_1
            // 
            this._vaSpread_1.Location = new System.Drawing.Point(8, 22);
            this._vaSpread_1.Name = "_vaSpread_1";
            this._vaSpread_1.Size = new System.Drawing.Size(734, 160);
            this._vaSpread_1.TabIndex = 9;
            this._vaSpread_1.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.vaSpread_DoubleClick);
            // 
            // _SSTab1_TabPage1
            // 
            this._SSTab1_TabPage1.Controls.Add(this._vaSpread_2);
            this._SSTab1_TabPage1.Controls.Add(this.cmbIteracciones);
            this._SSTab1_TabPage1.Controls.Add(this.Label12);
            this._SSTab1_TabPage1.Controls.Add(this.Label11);
            this._SSTab1_TabPage1.Controls.Add(this.Label10);
            this._SSTab1_TabPage1.Controls.Add(this.Label9);
            this._SSTab1_TabPage1.Controls.Add(this.Label8);
            this._SSTab1_TabPage1.Controls.Add(this.lblNaraja);
            this._SSTab1_TabPage1.Controls.Add(this.lblAzul);
            this._SSTab1_TabPage1.Controls.Add(this.lblVerde);
            this._SSTab1_TabPage1.Controls.Add(this.lblAmarillo);
            this._SSTab1_TabPage1.Controls.Add(this.lblRojo);
            this._SSTab1_TabPage1.Text = "Interacciones por principio activo de especialidades.";
            // 
            // _vaSpread_2
            // 
            this._vaSpread_2.Location = new System.Drawing.Point(10, 61);
            this._vaSpread_2.Name = "_vaSpread_2";
            this._vaSpread_2.Size = new System.Drawing.Size(746, 332);
            this._vaSpread_2.TabIndex = 1;
            this._vaSpread_2.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.vaSpread_DoubleClick);


            // 
            // cmbIteracciones
            // 

            this.cmbIteracciones.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbIteracciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.cmbIteracciones.Location = new System.Drawing.Point(668, 47);
            this.cmbIteracciones.Name = "cmbIteracciones";
            this.cmbIteracciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmbIteracciones.Size = new System.Drawing.Size(87, 31);
            this.cmbIteracciones.TabIndex = 32;
            this.cmbIteracciones.Text = "Iteracciones";
            this.cmbIteracciones.Visible = false;
            this.cmbIteracciones.Click += new System.EventHandler(this.cmbIteracciones_Click);
            // 
            // Label12
            // 
            
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.Label12.Location = new System.Drawing.Point(540, 11);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(191, 17);
            this.Label12.TabIndex = 19;
            this.Label12.Text = "Medidas dietéticas.";
            // 
            // Label11
            // 
          
            this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.Label11.Location = new System.Drawing.Point(286, 11);
            this.Label11.Name = "Label11";
            this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label11.Size = new System.Drawing.Size(191, 17);
            this.Label11.TabIndex = 18;
            this.Label11.Text = "Espaciar administración.";
            // 
            // Label10
            // 
          
            this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.Label10.Location = new System.Drawing.Point(286, 35);
            this.Label10.Name = "Label10";
            this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label10.Size = new System.Drawing.Size(191, 17);
            this.Label10.TabIndex = 17;
            this.Label10.Text = "Casos aislados o teórica.";
            // 
            // Label9
            // 
            
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.Label9.Location = new System.Drawing.Point(44, 35);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(191, 17);
            this.Label9.TabIndex = 16;
            this.Label9.Text = "Evidencia menor / Precaución.";
            // 
            // Label8
            // 
           
            this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.Label8.Location = new System.Drawing.Point(44, 12);
            this.Label8.Name = "Label8";
            this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label8.Size = new System.Drawing.Size(191, 17);
            this.Label8.TabIndex = 15;
            this.Label8.Text = "Ampliar evidencia / Evitar.";
            // 
            // lblNaraja
            // 
            this.lblNaraja.BackColor = System.Drawing.Color.FromArgb(255, 192, 128);
            this.lblNaraja.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblNaraja.Location = new System.Drawing.Point(508, 10);
            this.lblNaraja.Name = "lblNaraja";
            this.lblNaraja.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblNaraja.Size = new System.Drawing.Size(25, 19);
            this.lblNaraja.TabIndex = 14;
            this.lblNaraja.AutoSize = false;


            // 
            // lblAzul
            // 
            this.lblAzul.BackColor = System.Drawing.Color.FromArgb(128, 128, 255);
            this.lblAzul.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblAzul.Location = new System.Drawing.Point(254, 9);
            this.lblAzul.Name = "lblAzul";
            this.lblAzul.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblAzul.Size = new System.Drawing.Size(25, 19);
            this.lblAzul.TabIndex = 13;
            this.lblAzul.AutoSize = false;
            // 
            // lblVerde
            // 
            this.lblVerde.BackColor = System.Drawing.Color.FromArgb(128, 255, 128);
            this.lblVerde.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblVerde.Location = new System.Drawing.Point(254, 34);
            this.lblVerde.Name = "lblVerde";
            this.lblVerde.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblVerde.Size = new System.Drawing.Size(25, 19);
            this.lblVerde.TabIndex = 12;
            this.lblVerde.AutoSize = false;
            // 
            // lblAmarillo
            // 
            this.lblAmarillo.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
            this.lblAmarillo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblAmarillo.Location = new System.Drawing.Point(12, 33);
            this.lblAmarillo.Name = "lblAmarillo";
            this.lblAmarillo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblAmarillo.Size = new System.Drawing.Size(25, 19);
            this.lblAmarillo.TabIndex = 11;
            this.lblAmarillo.AutoSize = false;
            // 
            // lblRojo
            // 
            this.lblRojo.BackColor = System.Drawing.Color.FromArgb(255, 128, 128);
            this.lblRojo.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblRojo.Location = new System.Drawing.Point(12, 9);
            this.lblRojo.Name = "lblRojo";
            this.lblRojo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblRojo.Size = new System.Drawing.Size(25, 19);
            this.lblRojo.TabIndex = 10;
            this.lblRojo.AutoSize = false;
            // 
            // _SSTab1_TabPage2
            // 
            this._SSTab1_TabPage2.Controls.Add(this.txtDetalle);
            this._SSTab1_TabPage2.Controls.Add(this.lblDescriEspecialidad2);
            this._SSTab1_TabPage2.Controls.Add(this.lblCodigoEspecialidad2);
            this._SSTab1_TabPage2.Controls.Add(this.lblDescripPrincipio2);
            this._SSTab1_TabPage2.Controls.Add(this.lblCodigoPrincipio2);
            this._SSTab1_TabPage2.Controls.Add(this.lblDescriEspecialidad1);
            this._SSTab1_TabPage2.Controls.Add(this.lblCodigoEspecialidad1);
            this._SSTab1_TabPage2.Controls.Add(this.lblDescripPrincipio1);
            this._SSTab1_TabPage2.Controls.Add(this.lblCodigoPrincipio1);
            this._SSTab1_TabPage2.Controls.Add(this.Label16);
            this._SSTab1_TabPage2.Controls.Add(this.Label15);
            this._SSTab1_TabPage2.Controls.Add(this.Label14);
            this._SSTab1_TabPage2.Controls.Add(this.Label13);
            this._SSTab1_TabPage2.Text = "Detalle interacción por principio activo.";
            // 
            // txtDetalle
            // 
          
            this.txtDetalle.Location = new System.Drawing.Point(14, 123);
            this.txtDetalle. Name = "txtDetalle";
           // this.txtDetalle.Rtf = resources.GetString("txtDetalle.TextRTF");
            this.txtDetalle.VerticalScrollBarVisibility = Telerik.WinControls.RichTextEditor.UI.ScrollBarVisibility.Visible;
            this.txtDetalle.Size = new System.Drawing.Size(738, 270);
            this.txtDetalle.TabIndex = 33;
            // 
            // lblDescriEspecialidad2
            // 
            
            this.lblDescriEspecialidad2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescriEspecialidad2.Location = new System.Drawing.Point(234, 95);
            this.lblDescriEspecialidad2.Name = "lblDescriEspecialidad2";
            this.lblDescriEspecialidad2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDescriEspecialidad2.Size = new System.Drawing.Size(360, 20);
            this.lblDescriEspecialidad2.TabIndex = 31;
            this.lblDescriEspecialidad2.Enabled = true;
            this.lblDescriEspecialidad2.ReadOnly = true;
            // 
            // lblCodigoEspecialidad2
            // 
         
            this.lblCodigoEspecialidad2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCodigoEspecialidad2.Location = new System.Drawing.Point(108, 95);
            this.lblCodigoEspecialidad2.Name = "lblCodigoEspecialidad2";
            this.lblCodigoEspecialidad2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCodigoEspecialidad2.Size = new System.Drawing.Size(118, 20);
            this.lblCodigoEspecialidad2.TabIndex = 30;
            this.lblCodigoEspecialidad2.Enabled = true;
            this.lblCodigoEspecialidad2.ReadOnly = true;
            // 
            // lblDescripPrincipio2
            // 
           
            this.lblDescripPrincipio2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescripPrincipio2.Location = new System.Drawing.Point(234, 69);
            this.lblDescripPrincipio2.Name = "lblDescripPrincipio2";
            this.lblDescripPrincipio2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDescripPrincipio2.Size = new System.Drawing.Size(360, 20);
            this.lblDescripPrincipio2.TabIndex = 29;
            this.lblDescripPrincipio2.Enabled = true;
            this.lblDescripPrincipio2.ReadOnly = true;
            // 
            // lblCodigoPrincipio2
            // 
           
            this.lblCodigoPrincipio2.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCodigoPrincipio2.Location = new System.Drawing.Point(108, 69);
            this.lblCodigoPrincipio2.Name = "lblCodigoPrincipio2";
            this.lblCodigoPrincipio2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCodigoPrincipio2.Size = new System.Drawing.Size(118, 20);
            this.lblCodigoPrincipio2.TabIndex = 28;
            this.lblCodigoPrincipio2.Enabled = true;
            this.lblCodigoPrincipio2.ReadOnly = true;
            // 
            // lblDescriEspecialidad1
            // 
            
            this.lblDescriEspecialidad1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescriEspecialidad1.Location = new System.Drawing.Point(234, 43);
            this.lblDescriEspecialidad1.Name = "lblDescriEspecialidad1";
            this.lblDescriEspecialidad1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDescriEspecialidad1.Size = new System.Drawing.Size(360, 20);
            this.lblDescriEspecialidad1.TabIndex = 27;
            this.lblDescriEspecialidad1.Enabled = true;
            this.lblDescriEspecialidad1.ReadOnly = true;
            // 
            // lblCodigoEspecialidad1
            // 
            
            this.lblCodigoEspecialidad1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCodigoEspecialidad1.Location = new System.Drawing.Point(108, 43);
            this.lblCodigoEspecialidad1.Name = "lblCodigoEspecialidad1";
            this.lblCodigoEspecialidad1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCodigoEspecialidad1.Size = new System.Drawing.Size(118, 20);
            this.lblCodigoEspecialidad1.TabIndex = 26;
            this.lblCodigoEspecialidad1.Enabled = true;
            this.lblCodigoEspecialidad1.ReadOnly = true;
            // 
            // lblDescripPrincipio1
            // 
           
            this.lblDescripPrincipio1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDescripPrincipio1.Location = new System.Drawing.Point(234, 17);
            this.lblDescripPrincipio1.Name = "lblDescripPrincipio1";
            this.lblDescripPrincipio1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDescripPrincipio1.Size = new System.Drawing.Size(360, 20);
            this.lblDescripPrincipio1.TabIndex = 25;
            this.lblDescripPrincipio1.Enabled = true;
            this.lblDescripPrincipio1.ReadOnly = true;

            // 
            // lblCodigoPrincipio1
            // 
           
            this.lblCodigoPrincipio1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCodigoPrincipio1.Location = new System.Drawing.Point(108, 17);
            this.lblCodigoPrincipio1.Name = "lblCodigoPrincipio1";
            this.lblCodigoPrincipio1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCodigoPrincipio1.Size = new System.Drawing.Size(118, 20);
            this.lblCodigoPrincipio1.TabIndex = 24;
            this.lblCodigoPrincipio1.Enabled = true;
            this.lblCodigoPrincipio1.ReadOnly = true;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label16.Location = new System.Drawing.Point(14, 99);
            this.Label16.Name = "Label16";
            this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label16.Size = new System.Drawing.Size(72, 13);
            this.Label16.TabIndex = 23;
            this.Label16.Text = "Especialidad 2:";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label15.Location = new System.Drawing.Point(14, 73);
            this.Label15.Name = "Label15";
            this.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label15.Size = new System.Drawing.Size(85, 13);
            this.Label15.TabIndex = 22;
            this.Label15.Text = "Principio Activo 2:";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label14.Location = new System.Drawing.Point(14, 47);
            this.Label14.Name = "Label14";
            this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label14.Size = new System.Drawing.Size(72, 13);
            this.Label14.TabIndex = 21;
            this.Label14.Text = "Especialidad 1:";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label13.Location = new System.Drawing.Point(14, 21);
            this.Label13.Name = "Label13";
            this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label13.Size = new System.Drawing.Size(85, 13);
            this.Label13.TabIndex = 20;
            this.Label13.Text = "Principio Activo 1:";
            // 
            // cbSalir
            // 

            this.cbSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbSalir.Location = new System.Drawing.Point(699, 490);
            this.cbSalir.Name = "cbSalir";
            this.cbSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbSalir.Size = new System.Drawing.Size(73, 27);
            this.cbSalir.TabIndex = 2;
            this.cbSalir.Text = "&Cerrar";
            this.cbSalir.Click += new System.EventHandler(this.cbSalir_Click);
            // 
            // frmIteraccionesFarmacos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(781, 520);
            this.Controls.Add(this.frmPaciente);
            this.Controls.Add(this.SSTab1);
            this.Controls.Add(this.cbSalir);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(9, 28);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmIteraccionesFarmacos";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Consulta de Interacciones de Fármacos.";
            this.Closed += new System.EventHandler(this.frmIteraccionesFarmacos_Closed);
            this.Load += new System.EventHandler(this.frmIteraccionesFarmacos_Load);
            this.frmPaciente.ResumeLayout(false);
            this.SSTab1.ResumeLayout(false);
            this._SSTab1_TabPage0.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.Frame2.ResumeLayout(false);
            this._SSTab1_TabPage1.ResumeLayout(false);
            this._SSTab1_TabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        void ReLoadForm(bool addEvents)
        {
            InitializevaSpread();
            SSTab1PreviousTab = SSTab1.SelectedPage.Item.ZIndex;
               
        }
        void InitializevaSpread()
        {
            this.vaSpread = new UpgradeHelpers.Spread.FpSpread[3];
            this.vaSpread[2] = _vaSpread_2;
            this.vaSpread[1] = _vaSpread_1;
            this.vaSpread[0] = _vaSpread_0;
        }
        #endregion
    }
}
 