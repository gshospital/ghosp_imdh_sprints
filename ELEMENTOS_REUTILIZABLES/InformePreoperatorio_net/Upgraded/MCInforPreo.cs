using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Microsoft.CSharp;
using CrystalWrapper;


namespace InforPreo
{
	public class MCInforPreo
	{

        //camaya todo_x_5
        DataSet bs = null;		
		DataRow Rs = null;
		Mensajes.ClassMensajes clase_mensaje = null;
		string Nombre_DSN_SQL = String.Empty;
		string Nombre_DSN_ACCESS = String.Empty;
		public void imprimir(ref SqlConnection Conexion, bool bQuirurgico, string itiposer, string ganoregi, string gnumregi, string gidenpac, Crystal Crystal, string CaminoInformes, string scryUsuario, string codprestacion, string Prestacion, object CuadroImpresora, string nombreBaseDatosTemp, string FPREVISTA = "", string FPeticio = "", bool bImprimirXImpresora = false)
		{
			try
			{
				string stFPrevista = String.Empty;
				string stFormato = String.Empty;

				string Lsql = String.Empty;
				DataSet LRR = null;

				Serrores.Obtener_Multilenguaje(Conexion, gidenpac);
				Serrores.AjustarRelojCliente(Conexion);

				Serrores.ObtenerTextosRPT(Conexion, "INFORPREO", "QPQ100R4");

				if (bQuirurgico)
				{
					stFormato = "dd/MM/yyyy";
					stFPrevista = ObtenerFINTERVE(ganoregi, gnumregi, itiposer, Conexion, codprestacion);
				}
				else
				{
					stFormato = "dd/MM/yyyy HH:mm";
					stFPrevista = StringsHelper.Format(FPeticio, stFormato);
				}
				Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);

				//oscar 14/06/04
				Serrores.Datos_Conexion(Conexion);
				//--------
				Crear_TBTemp(Conexion, codprestacion, CuadroImpresora, nombreBaseDatosTemp, stFPrevista);
				
				Crystal.Reset();
				//cabeceras				
				Crystal.ReportFileName = CaminoInformes + "\\..\\ElementosComunes\\rpt\\qpq100r4.RPT";
                //Crystal.Formulas[0] = "'" + GetGlobalVar("GRUPOHO", "VALFANU1", Conexion) + "'";
                //Crystal.Formulas[0] = "ORDEN_Hosp ='" + GetGlobalVar("GRUPOHO", "VALFANU1", Conexion) + "'";
                Crystal.Formulas["ORDEN_Hosp"] = "'" + GetGlobalVar("GRUPOHO", "VALFANU1", Conexion) + "'";
                //Crystal.Formulas[1] = "Nom_Hosp ='" + GetGlobalVar("NombreHo", "VALFANU1", Conexion) + "'";
                Crystal.Formulas["Nom_Hosp"] = "'" + GetGlobalVar("NombreHo", "VALFANU1", Conexion) + "'";
                //Crystal.Formulas[2] = "Dir_Hosp ='" + GetGlobalVar("DireccHO", "VALFANU1", Conexion) + "'";
                Crystal.Formulas["Dir_Hosp"] = "'" + GetGlobalVar("DireccHO", "VALFANU1", Conexion) + "'";
                //paciente
                //Crystal.Formulas[3] = "Paciente='" + ObtenerPaciente(gidenpac, Conexion) + "'";
                Crystal.Formulas["Paciente"] = "'" + ObtenerPaciente(gidenpac, Conexion) + "'";
                //Crystal.Formulas[4] = "edad='" + ObtenerEdad(gidenpac, Conexion) + "'";
                Crystal.Formulas["edad"] = "'" + ObtenerEdad(gidenpac, Conexion) + "'";

				if (bQuirurgico)
				{
                    //Crystal.Formulas[5] = "FPRealiza='" + StringsHelper.Format(stFPrevista, stFormato) + "'";
                    Crystal.Formulas["FPRealiza"] = "'" + StringsHelper.Format(stFPrevista, stFormato) + "'";
                    //        .Formulas(6) = "tbFPingreso='Fecha prevista de ingreso:'"					
                    Crystal.Formulas["tbFPingreso"] = "' " + Serrores.VarRPT[4] + "'";
                    //Crystal.Formulas[7] = "FPingreso='" + Obtenerfpreving(ganoregi, gnumregi, itiposer, bQuirurgico, Conexion) + "'";
                    Crystal.Formulas["FPingreso"] = "'" + Obtenerfpreving(ganoregi, gnumregi, itiposer, bQuirurgico, Conexion) + "'";
					//        .Formulas(9) = "titulo='INSTRUCCIONES PREOPERATORIAS'"					
					Crystal.Formulas["titulo"] = "' " + Serrores.VarRPT[1] + "'";
				}
				else
				{
                    //Crystal.Formulas[5] = "FPRealiza='" + StringsHelper.Format(stFPrevista, stFormato) + "'";				
                    Crystal.Formulas["FPRealiza"] = "'" + StringsHelper.Format(FPREVISTA, stFormato) + "'";
                    //        .Formulas(6) = "tbFPingreso='Fecha prevista de ingreso:'"					
                    Crystal.Formulas["tbFPingreso"] = "''";
                    //Crystal.Formulas[7] = "FPingreso='" + Obtenerfpreving(ganoregi, gnumregi, itiposer, bQuirurgico, Conexion) + "'";				
                    Crystal.Formulas["FPingreso"] = "''";
					//        .Formulas(9) = "titulo='PROTOCOLO'"					
					Crystal.Formulas["titulo"] = "' " + Serrores.VarRPT[2] + "'";
				}

				//oscar C: Falta traducir la prestacion
				//.Formulas(8) = "Prestacion='" & prestacion & "'"
				Lsql = "SELECT dprestac, dprestaci1,  dprestaci2 FROM DCODPRES WHERE gprestac='" + codprestacion + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Lsql, Conexion);
				LRR = new DataSet();
				tempAdapter.Fill(LRR);
				if (LRR.Tables[0].Rows.Count != 0)
				{
					switch(Serrores.CampoIdioma)
					{
						case "IDIOMA0" :
                            //Crystal.Formulas[8] = "Prestacion='" + Convert.ToString(LRR.Tables[0].Rows[0]["dprestac"]).Trim().ToUpper() + "'";
                            Crystal.Formulas["Prestacion"] = "'" + Convert.ToString(LRR.Tables[0].Rows[0]["dprestac"]).Trim().ToUpper() + "'"; 
							break;
						case "IDIOMA1" :  					
							Crystal.Formulas["Prestacion"] = "'" + Convert.ToString(LRR.Tables[0].Rows[0]["dprestaci1"]).Trim().ToUpper() + "'"; 
							break;
						case "IDIOMA2" : 					
							Crystal.Formulas["Prestacion"] = "'" + Convert.ToString(LRR.Tables[0].Rows[0]["dprestaci2"]).Trim().ToUpper() + "'"; 
							break;
					}
				}
				
				LRR.Close();
                //---------------
                //Crystal.Formulas[10] = "LogoIDC='" + Serrores.ExisteLogo(Conexion) + "'";
                Crystal.Formulas["LogoIDC"] = "'" + Serrores.ExisteLogo(Conexion) + "'";
                //Crystal.Formulas[11] = "IOFPRIPP='" + Serrores.ObternerValor_CTEGLOBAL(Conexion, "IOFPRIPP", "VALFANU1") + "'";
                Crystal.Formulas["IOFPRIPP"] = "'" + Serrores.ObternerValor_CTEGLOBAL(Conexion, "IOFPRIPP", "VALFANU1") + "'";

				//oscar 14/06/04
				//.CopiesToPrinter = CuadroImpresora.Copies
				//.Destination = 1 'crptToPrinter

				if (bImprimirXImpresora)
				{					
					//camaya todo_x_5 Crystal Report
                    //Crystal.CopiesToPrinter = CuadroImpresora.Copies;					
					Crystal.Destination = (Crystal.DestinationConstants)1;
				}
				else
				{					
					Crystal.Destination = (Crystal.DestinationConstants)0; //crptToWindow				
					Crystal.WindowShowPrintSetupBtn = true;
				}
				//---------------
				//    .WindowTitle = "Instrucciones previas"				
				Crystal.WindowTitle = Serrores.VarRPT[3];
                Crystal.DataFiles[0] = bs.Tables["PLANTILLA"];
				Crystal.Connect = "DSN=" + scryUsuario + "";				
				Crystal.SQLQuery = "SELECT * FROM PLANTILLA";
				//LLAMADA VARIABLES DSN
				Serrores.CrearCadenaDSN(ref Nombre_DSN_SQL, ref Nombre_DSN_ACCESS);				
				Crystal.SubreportToChange = "LogotipoIDC";				
				Crystal.Connect = "DSN=" + Nombre_DSN_SQL + " ; UID=" + Serrores.VVstUsuarioBD + " ; PWD=" + Serrores.VVstPasswordBD;
				//-----
				
				Crystal.SubreportToChange = "";				
				Crystal.WindowState = (Crystal.WindowStateConstants)2;
				Serrores.LLenarFormulas(Conexion, Crystal, "QPQ100R4", "QPQ100R4");				
				Crystal.Action = 1;				
				Crystal.PrinterStopPage = Crystal.PageCount;				
				Crystal.Reset();
			}
			catch (Exception e)
			{				
				if (Information.Err().Number != 32755)
				{
					Serrores.AnalizaError("InformePreoperatorio", CaminoInformes, scryUsuario, "imprimir", e);
				}
			}
		}
		private string ObtenerFINTERVE(string ganoregi, string gnumregi, string itiposer, SqlConnection Conexion, string codprestacion)
		{
			string result = String.Empty;
			string sql = "select FINTERVE from qprogqui where ganoregi = " + ganoregi + " and gnumregi = " + gnumregi;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{				
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0][0]))
				{					
					result = Convert.ToDateTime(RR.Tables[0].Rows[0][0]).ToString("dd/MM/yyyy");
				}
			}

            RR.Close();

			return result;
		}
		private string Obtenerfpreving(string ganoregi, string gnumregi, string itiposer, bool bQuirurgico, SqlConnection Conexion)
		{
			string result = String.Empty;
			string sql = String.Empty;
			DataSet RR = null;
			result = "";
			if (bQuirurgico)
			{
				sql = "select fpreving from qprogqui where ganoregi = " + ganoregi + " and gnumregi = " + gnumregi;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
				RR = new DataSet();
				tempAdapter.Fill(RR);
				if (RR.Tables[0].Rows.Count != 0)
				{					
					if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["fpreving"]))
					{						
						result = Convert.ToDateTime(RR.Tables[0].Rows[0]["fpreving"]).ToString("dd/MM/yyyy");
					}
				}
				
				RR.Close();
			}

			return result;
		}
		private string ObtenerPaciente(string codigo, SqlConnection Conexion)
		{
			string result = String.Empty;
			result = "";
			string sql = "select dnombpac, dape1pac, dape2pac from dpacient where gidenpac = '" + codigo + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{			
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["dnombpac"]))
				{				
					result = Convert.ToString(RR.Tables[0].Rows[0]["dnombpac"]).Trim();
				}				
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["dape1pac"]))
				{					
					result = result + " " + Convert.ToString(RR.Tables[0].Rows[0]["dape1pac"]).Trim();
				}				
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["dape2pac"]))
				{					
					result = result + " " + Convert.ToString(RR.Tables[0].Rows[0]["dape2pac"]).Trim();
				}
			}
			
			RR.Close();
			return result;
		}

		private string ObtenerEdad(string codigo, SqlConnection Conexion)
		{
			string result = String.Empty;
			result = "";
			string sql = "select fnacipac from dpacient where gidenpac = '" + codigo + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{				
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["fnacipac"]))
				{					
					result = Serrores.CalcularEdad(Convert.ToDateTime(RR.Tables[0].Rows[0]["fnacipac"]), DateTime.Now, Conexion, Serrores.CampoIdioma);
				}
			}
			
			RR.Close();
			return result;
		}
		public string GetGlobalVar(string stVar, string stField, SqlConnection Conexion)
		{
			// Devuelve el valor del la constante almacenada en SCONSGLO
			// con el nombre stVar y el valor de stField
			string result = String.Empty;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("Select * from sConsGlo where gConsGlo = '" + stVar + "'", Conexion);
			DataSet RrGlobal = new DataSet();
			tempAdapter.Fill(RrGlobal);
			
			result = Convert.ToString(RrGlobal.Tables[0].Rows[0][stField]).Trim();
			
			RrGlobal.Close();
			return result;
		}
		private string ObtenerPlantilla(string codprestacion, SqlConnection Conexion, string stFecha)
		{
			string result = String.Empty;
			result = "";			
			
			object tempRefParam = stFecha;
			object tempRefParam2 = stFecha;
			string sql = "select ginfplan from dcodpres where gprestac = '" + codprestacion + "' AND " + 
			             "finivali <= " + Serrores.FormatFechaHMS(tempRefParam) + " AND (ffinvali >= " + Serrores.FormatFechaHMS(tempRefParam2) + "OR ffinvali IS NULL) ";
			stFecha = Convert.ToString(tempRefParam2);
			stFecha = Convert.ToString(tempRefParam);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{				
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["ginfplan"]))
				{					
					result = Convert.ToString(RR.Tables[0].Rows[0]["ginfplan"]);
				}
			}
			
			RR.Close();
			return result;
		}
		private void Crear_TBTemp(SqlConnection Conexion, string codprestacion, object CuadroImpresora, string stnombre_base_datos, string stFecha)
		{
			object dbLangSpanish = null;
			object DBEngine = null;

			//Dim rrsql As rdoResultset
			//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			string sqltemporal = String.Empty;
			bool vbBase = false;
			//oscar 14/06/04
			//CuadroImpresora.Copies = 1
			//CuadroImpresora.CancelError = True
			//CuadroImpresora.ShowPrinter
			//---------

			string vstPathTemp = stnombre_base_datos;

			if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
			{
				vbBase = true;
			}
            
            bs = new DataSet();
            bs.CreateTable("PLANTILLA", @"ruta");

			string sql = "select ofichinf from xinfplan where " + 
			             " ginfplan = " + ObtenerPlantilla(codprestacion, Conexion, stFecha);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				sqltemporal = "select * from PLANTILLA where 1=2";
                Rs = bs.Tables["PLANTILLA"].NewRow();
                Rs["plantilla"] = Convert.ToString(RR.Tables[0].Rows[0]["ofichinf"]).Trim();
                bs.Tables["PLANTILLA"].Rows.Add(Rs);
            }			
			RR.Close();
		}

		public MCInforPreo()
		{
			try
			{
				clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{
				RadMessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}
		}

		~MCInforPreo()
		{
			clase_mensaje = null;
		}
	}
}