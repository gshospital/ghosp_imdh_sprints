using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace FechasDisponibles
{
	partial class CCI360F1
	{

		#region "Upgrade Support "
		private static CCI360F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static CCI360F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new CCI360F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprHuecos", "CbbAgendas", "rbTarde", "rbMa�ana", "frmTurno", "cbbResponsable", "cbbServicio", "cbCancelar", "SSFechaDesde", "SSFechaHasta", "Label6", "Label5", "Frame1", "rbNormal", "rbPreferente", "frmTipoPaciente", "cbAceptar", "cbBuscar", "Label1", "Line3", "Label7", "Line2", "Frame2", "lblTotal", "_Label3_2", "cbbPrestacion", "cbbSalaConsulta", "_Label3_1", "Label2", "_Label3_0", "Label4", "Label3", "ShapeContainer1", "sprHuecos_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread sprHuecos;
        public UpgradeHelpers.MSForms.MSCombobox CbbAgendas;
		public Telerik.WinControls.UI.RadRadioButton rbTarde;
		public Telerik.WinControls.UI.RadRadioButton rbMa�ana;
		public Telerik.WinControls.UI.RadGroupBox frmTurno;
		public UpgradeHelpers.MSForms.MSCombobox cbbResponsable;
		public UpgradeHelpers.MSForms.MSCombobox cbbServicio;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadDateTimePicker SSFechaDesde;
		public Telerik.WinControls.UI.RadDateTimePicker SSFechaHasta;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadRadioButton rbNormal;
		public Telerik.WinControls.UI.RadRadioButton rbPreferente;
		public Telerik.WinControls.UI.RadGroupBox frmTipoPaciente;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbBuscar;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line3;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line2;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadLabel lblTotal;
		private Telerik.WinControls.UI.RadLabel _Label3_2;
        public UpgradeHelpers.MSForms.MSCombobox cbbPrestacion;
		public UpgradeHelpers.MSForms.MSCombobox cbbSalaConsulta;
		private Telerik.WinControls.UI.RadLabel _Label3_1;
		public Telerik.WinControls.UI.RadLabel Label2;
		private Telerik.WinControls.UI.RadLabel _Label3_0;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel[] Label3 = new Telerik.WinControls.UI.RadLabel[3];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.Line3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Line2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.sprHuecos = new UpgradeHelpers.Spread.FpSpread();
            this.CbbAgendas = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.frmTurno = new Telerik.WinControls.UI.RadGroupBox();
            this.rbTarde = new Telerik.WinControls.UI.RadRadioButton();
            this.rbMa�ana = new Telerik.WinControls.UI.RadRadioButton();
            this.cbbResponsable = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbServicio = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.SSFechaDesde = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SSFechaHasta = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.frmTipoPaciente = new Telerik.WinControls.UI.RadGroupBox();
            this.rbNormal = new Telerik.WinControls.UI.RadRadioButton();
            this.rbPreferente = new Telerik.WinControls.UI.RadRadioButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbBuscar = new Telerik.WinControls.UI.RadButton();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label7 = new Telerik.WinControls.UI.RadLabel();
            this.lblTotal = new Telerik.WinControls.UI.RadLabel();
            this._Label3_2 = new Telerik.WinControls.UI.RadLabel();
            this.cbbPrestacion = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbSalaConsulta = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this._Label3_1 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this._Label3_0 = new Telerik.WinControls.UI.RadLabel();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.sprHuecos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprHuecos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbAgendas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmTurno)).BeginInit();
            this.frmTurno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbTarde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbMa�ana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbResponsable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSFechaDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSFechaHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmTipoPaciente)).BeginInit();
            this.frmTipoPaciente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbNormal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPreferente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPrestacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSalaConsulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label3_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(2, 18);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.Line3,
            this.Line2});
            this.ShapeContainer1.Size = new System.Drawing.Size(125, 29);
            this.ShapeContainer1.TabIndex = 21;
            this.ShapeContainer1.TabStop = false;
            // 
            // Line3
            // 
            this.Line3.BorderColor = System.Drawing.Color.Black;
            this.Line3.BorderWidth = 3;
            this.Line3.Enabled = false;
            this.Line3.Name = "Line3";
            this.Line3.X1 = 8;
            this.Line3.X2 = 32;
            this.Line3.Y1 = 4;
            this.Line3.Y2 = 4;
            // 
            // Line2
            // 
            this.Line2.BorderColor = System.Drawing.Color.Fuchsia;
            this.Line2.BorderWidth = 3;
            this.Line2.Enabled = false;
            this.Line2.Name = "Line2";
            this.Line2.X1 = 8;
            this.Line2.X2 = 32;
            this.Line2.Y1 = 20;
            this.Line2.Y2 = 20;
            // 
            // sprHuecos
            // 
            this.sprHuecos.Location = new System.Drawing.Point(8, 162);
            // 
            // 
            // 
            gridViewTextBoxColumn1.HeaderText = "Fecha";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.WrapText = true;
            gridViewTextBoxColumn2.HeaderText = "Hora";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.WrapText = true;
            gridViewTextBoxColumn3.HeaderText = "N�mero orden";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.WrapText = true;
            gridViewTextBoxColumn4.HeaderText = "D�a de la semana";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.WrapText = true;
            gridViewTextBoxColumn5.HeaderText = "Huecos pac. normales";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 70;
            gridViewTextBoxColumn5.WrapText = true;
            gridViewTextBoxColumn6.HeaderText = "Huecos pac. preferentes";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 80;
            gridViewTextBoxColumn6.WrapText = true;
            gridViewTextBoxColumn7.HeaderText = "Servicio";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.Width = 100;
            gridViewTextBoxColumn7.WrapText = true;
            gridViewTextBoxColumn8.HeaderText = "Responsable";
            gridViewTextBoxColumn8.Name = "column8";
            gridViewTextBoxColumn8.Width = 100;
            gridViewTextBoxColumn8.WrapText = true;
            gridViewTextBoxColumn9.HeaderText = "Sala consulta";
            gridViewTextBoxColumn9.Name = "column9";
            gridViewTextBoxColumn9.Width = 80;
            gridViewTextBoxColumn9.WrapText = true;
            gridViewTextBoxColumn10.HeaderText = "Agenda";
            gridViewTextBoxColumn10.Name = "column10";
            gridViewTextBoxColumn10.WrapText = true;
            gridViewTextBoxColumn11.HeaderText = "Horario consulta";
            gridViewTextBoxColumn11.Name = "column11";
            gridViewTextBoxColumn11.Width = 60;
            gridViewTextBoxColumn11.WrapText = true;
            gridViewTextBoxColumn12.HeaderText = "C�digo agenda";
            gridViewTextBoxColumn12.Name = "column12";
            gridViewTextBoxColumn12.Width = 60;
            gridViewTextBoxColumn12.WrapText = true;
            gridViewTextBoxColumn13.HeaderText = "C�digo bloque";
            gridViewTextBoxColumn13.Name = "column13";
            gridViewTextBoxColumn13.Width = 60;
            gridViewTextBoxColumn13.WrapText = true;
            gridViewTextBoxColumn14.HeaderText = "Duraci�n prestaci�n";
            gridViewTextBoxColumn14.Name = "column14";
            gridViewTextBoxColumn14.Width = 60;
            gridViewTextBoxColumn14.WrapText = true;
            gridViewTextBoxColumn15.HeaderText = "Tiempo obtenci�n";
            gridViewTextBoxColumn15.Name = "column15";
            gridViewTextBoxColumn15.Width = 60;
            gridViewTextBoxColumn15.WrapText = true;
            gridViewTextBoxColumn16.HeaderText = "P";
            gridViewTextBoxColumn16.Name = "column16";
            gridViewTextBoxColumn16.WrapText = true;
            gridViewTextBoxColumn17.HeaderText = "Q";
            gridViewTextBoxColumn17.Name = "column17";
            gridViewTextBoxColumn17.WrapText = true;
            this.sprHuecos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17});
            this.sprHuecos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.sprHuecos.Name = "sprHuecos";
            this.sprHuecos.Size = new System.Drawing.Size(741, 257);
            this.sprHuecos.TabIndex = 9;
            this.sprHuecos.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprHuecos_CellDoubleClick);
            this.sprHuecos.MouseMove += new System.Windows.Forms.MouseEventHandler(this.sprHuecos_MouseMove);
            // 
            // CbbAgendas
            // 
            this.CbbAgendas.ColumnWidths = "";
            this.CbbAgendas.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbbAgendas.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CbbAgendas.Location = new System.Drawing.Point(88, 10);
            this.CbbAgendas.Name = "CbbAgendas";
            this.CbbAgendas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbbAgendas.Size = new System.Drawing.Size(365, 20);
            this.CbbAgendas.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            this.CbbAgendas.TabIndex = 26;
            this.CbbAgendas.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbAgendas_SelectedIndexChanged);
            this.CbbAgendas.TextChanged += new System.EventHandler(this.CbbAgendas_TextChanged);
            // 
            // frmTurno
            // 
            this.frmTurno.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmTurno.Controls.Add(this.rbTarde);
            this.frmTurno.Controls.Add(this.rbMa�ana);
            this.frmTurno.HeaderText = "Turno";
            this.frmTurno.Location = new System.Drawing.Point(514, 100);
            this.frmTurno.Name = "frmTurno";
            this.frmTurno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmTurno.Size = new System.Drawing.Size(137, 55);
            this.frmTurno.TabIndex = 21;
            this.frmTurno.Text = "Turno";
            // 
            // rbTarde
            // 
            this.rbTarde.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbTarde.Location = new System.Drawing.Point(78, 22);
            this.rbTarde.Name = "rbTarde";
            this.rbTarde.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbTarde.Size = new System.Drawing.Size(48, 18);
            this.rbTarde.TabIndex = 7;
            this.rbTarde.Text = "Tarde";
            // 
            // rbMa�ana
            // 
            this.rbMa�ana.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbMa�ana.Location = new System.Drawing.Point(6, 22);
            this.rbMa�ana.Name = "rbMa�ana";
            this.rbMa�ana.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbMa�ana.Size = new System.Drawing.Size(60, 18);
            this.rbMa�ana.TabIndex = 6;
            this.rbMa�ana.Text = "Ma�ana";
            // 
            // cbbResponsable
            // 
            this.cbbResponsable.ColumnWidths = "";
            this.cbbResponsable.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbResponsable.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbResponsable.Location = new System.Drawing.Point(450, 43);
            this.cbbResponsable.Name = "cbbResponsable";
            this.cbbResponsable.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbResponsable.Size = new System.Drawing.Size(299, 20);
            this.cbbResponsable.TabIndex = 1;
            this.cbbResponsable.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbResponsable_SelectedIndexChanged);
            this.cbbResponsable.Enter += new System.EventHandler(this.cbbResponsable_Enter);
            // 
            // cbbServicio
            // 
            this.cbbServicio.ColumnWidths = "";
            this.cbbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbServicio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbServicio.Location = new System.Drawing.Point(88, 43);
            this.cbbServicio.Name = "cbbServicio";
            this.cbbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbServicio.Size = new System.Drawing.Size(267, 20);
            this.cbbServicio.TabIndex = 0;
            this.cbbServicio.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbServicio_SelectedIndexChanged);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(668, 440);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 11;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.SSFechaDesde);
            this.Frame1.Controls.Add(this.SSFechaHasta);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.HeaderText = "Rango de fechas";
            this.Frame1.Location = new System.Drawing.Point(8, 100);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(335, 55);
            this.Frame1.TabIndex = 13;
            this.Frame1.Text = "Rango de fechas";
            // 
            // SSFechaDesde
            // 
            this.SSFechaDesde.CustomFormat = "dd/MM/yy";
            this.SSFechaDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SSFechaDesde.Location = new System.Drawing.Point(46, 24);
            this.SSFechaDesde.Name = "SSFechaDesde";
            this.SSFechaDesde.Size = new System.Drawing.Size(121, 18);
            this.SSFechaDesde.TabIndex = 22;
            this.SSFechaDesde.TabStop = false;
            this.SSFechaDesde.Text = "24/05/16";
            this.SSFechaDesde.Value = new System.DateTime(2016, 5, 24, 0, 0, 0, 0);
            // 
            // SSFechaHasta
            // 
            this.SSFechaHasta.CustomFormat = "dd/MM/yy";
            this.SSFechaHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSFechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SSFechaHasta.Location = new System.Drawing.Point(208, 24);
            this.SSFechaHasta.Name = "SSFechaHasta";
            this.SSFechaHasta.Size = new System.Drawing.Size(121, 18);
            this.SSFechaHasta.TabIndex = 23;
            this.SSFechaHasta.TabStop = false;
            this.SSFechaHasta.Text = "24/05/16";
            this.SSFechaHasta.Value = new System.DateTime(2016, 5, 24, 0, 0, 0, 0);
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Location = new System.Drawing.Point(170, 24);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(37, 18);
            this.Label6.TabIndex = 25;
            this.Label6.Text = "Hasta:";
            // 
            // Label5
            // 
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Location = new System.Drawing.Point(4, 24);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(40, 18);
            this.Label5.TabIndex = 24;
            this.Label5.Text = "Desde:";
            // 
            // frmTipoPaciente
            // 
            this.frmTipoPaciente.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmTipoPaciente.Controls.Add(this.rbNormal);
            this.frmTipoPaciente.Controls.Add(this.rbPreferente);
            this.frmTipoPaciente.HeaderText = "Tipo de paciente";
            this.frmTipoPaciente.Location = new System.Drawing.Point(355, 100);
            this.frmTipoPaciente.Name = "frmTipoPaciente";
            this.frmTipoPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmTipoPaciente.Size = new System.Drawing.Size(147, 55);
            this.frmTipoPaciente.TabIndex = 12;
            this.frmTipoPaciente.Text = "Tipo de paciente";
            // 
            // rbNormal
            // 
            this.rbNormal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbNormal.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbNormal.Location = new System.Drawing.Point(6, 24);
            this.rbNormal.Name = "rbNormal";
            this.rbNormal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbNormal.Size = new System.Drawing.Size(57, 18);
            this.rbNormal.TabIndex = 4;
            this.rbNormal.Text = "Normal";
            this.rbNormal.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // rbPreferente
            // 
            this.rbPreferente.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbPreferente.Location = new System.Drawing.Point(70, 24);
            this.rbPreferente.Name = "rbPreferente";
            this.rbPreferente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbPreferente.Size = new System.Drawing.Size(72, 18);
            this.rbPreferente.TabIndex = 5;
            this.rbPreferente.Text = "Preferente";
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Enabled = false;
            this.cbAceptar.Location = new System.Drawing.Point(568, 440);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 10;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cbBuscar
            // 
            this.cbBuscar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscar.Location = new System.Drawing.Point(668, 123);
            this.cbBuscar.Name = "cbBuscar";
            this.cbBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbBuscar.Size = new System.Drawing.Size(81, 32);
            this.cbBuscar.TabIndex = 8;
            this.cbBuscar.Text = "&Buscar";
            this.cbBuscar.Click += new System.EventHandler(this.cbBuscar_Click);
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.Controls.Add(this.Label7);
            this.Frame2.Controls.Add(this.ShapeContainer1);
            this.Frame2.HeaderText = "";
            this.Frame2.Location = new System.Drawing.Point(8, 424);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(129, 49);
            this.Frame2.TabIndex = 18;
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(48, 16);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(71, 12);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "Horas disponibles";
            // 
            // Label7
            // 
            this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label7.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(48, 32);
            this.Label7.Name = "Label7";
            this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label7.Size = new System.Drawing.Size(66, 12);
            this.Label7.TabIndex = 19;
            this.Label7.Text = "Horas saturadas";
            // 
            // lblTotal
            // 
            this.lblTotal.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTotal.Location = new System.Drawing.Point(148, 456);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTotal.Size = new System.Drawing.Size(160, 18);
            this.lblTotal.TabIndex = 28;
            this.lblTotal.Text = "N�mero de fechas disponibles:";
            this.lblTotal.Visible = false;
            // 
            // _Label3_2
            // 
            this._Label3_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._Label3_2.Location = new System.Drawing.Point(8, 10);
            this._Label3_2.Name = "_Label3_2";
            this._Label3_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Label3_2.Size = new System.Drawing.Size(47, 18);
            this._Label3_2.TabIndex = 27;
            this._Label3_2.Text = "Agenda:";
            // 
            // cbbPrestacion
            // 
            this.cbbPrestacion.ColumnWidths = "";
            this.cbbPrestacion.Location = new System.Drawing.Point(450, 72);
            this.cbbPrestacion.Name = "cbbPrestacion";
            this.cbbPrestacion.Size = new System.Drawing.Size(299, 20);
            this.cbbPrestacion.TabIndex = 3;
            this.cbbPrestacion.Click += new System.EventHandler(this.cbbPrestacion_ClickEvent);
            // 
            // cbbSalaConsulta
            // 
            this.cbbSalaConsulta.ColumnWidths = "";
            this.cbbSalaConsulta.Location = new System.Drawing.Point(88, 72);
            this.cbbSalaConsulta.Name = "cbbSalaConsulta";
            this.cbbSalaConsulta.Size = new System.Drawing.Size(267, 20);
            this.cbbSalaConsulta.TabIndex = 2;
            this.cbbSalaConsulta.Click += new System.EventHandler(this.cbbSalaConsulta_ClickEvent);
            // 
            // _Label3_1
            // 
            this._Label3_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._Label3_1.Location = new System.Drawing.Point(8, 43);
            this._Label3_1.Name = "_Label3_1";
            this._Label3_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Label3_1.Size = new System.Drawing.Size(47, 18);
            this._Label3_1.TabIndex = 17;
            this._Label3_1.Text = "Servicio:";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(370, 43);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(72, 18);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "Responsable:";
            // 
            // _Label3_0
            // 
            this._Label3_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._Label3_0.Location = new System.Drawing.Point(8, 76);
            this._Label3_0.Name = "_Label3_0";
            this._Label3_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Label3_0.Size = new System.Drawing.Size(74, 18);
            this._Label3_0.TabIndex = 15;
            this._Label3_0.Text = "Sala consulta:";
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(370, 76);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(60, 18);
            this.Label4.TabIndex = 14;
            this.Label4.Text = "Prestaci�n:";
            // 
            // CCI360F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 480);
            this.Controls.Add(this.sprHuecos);
            this.Controls.Add(this.CbbAgendas);
            this.Controls.Add(this.frmTurno);
            this.Controls.Add(this.cbbResponsable);
            this.Controls.Add(this.cbbServicio);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.frmTipoPaciente);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cbBuscar);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this._Label3_2);
            this.Controls.Add(this.cbbPrestacion);
            this.Controls.Add(this.cbbSalaConsulta);
            this.Controls.Add(this._Label3_1);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this._Label3_0);
            this.Controls.Add(this.Label4);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CCI360F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Consulta de fechas disponibles - CCI360F1";
            this.Closed += new System.EventHandler(this.CCI360F1_Closed);
            this.Load += new System.EventHandler(this.CCI360F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sprHuecos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprHuecos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbAgendas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmTurno)).EndInit();
            this.frmTurno.ResumeLayout(false);
            this.frmTurno.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbTarde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbMa�ana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbResponsable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSFechaDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSFechaHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmTipoPaciente)).EndInit();
            this.frmTipoPaciente.ResumeLayout(false);
            this.frmTipoPaciente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbNormal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPreferente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPrestacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSalaConsulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label3_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        void ReLoadForm(bool addEvents)
		{
			InitializeLabel3();
		}
		void InitializeLabel3()
		{
			this.Label3 = new Telerik.WinControls.UI.RadLabel[3];
			this.Label3[2] = _Label3_2;
			this.Label3[1] = _Label3_1;
			this.Label3[0] = _Label3_0;
		}
		#endregion
	}
}