using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace FechasDisponibles
{
	public partial class CCI360F1
		: RadForm
	{


		// Parametros recibidos
		SqlConnection Conexion = null;
		string Modo = String.Empty;
		dynamic Formulario = null;
		string stUsuario = String.Empty;
		string iAgendaSolicitada = String.Empty;
		string StAgendaSolicitada = String.Empty;
		int iServicioSolicitado = 0;
		string stDServicioSolicitado = String.Empty;
		int lResponsableSolicitado = 0;
		string stNResponsableSolicitado = String.Empty;
		string stSalaSolicitada = String.Empty;
		string stDSalaSolicitada = String.Empty;
		string stPrestacionSolicitada = String.Empty;
		string stDPrestacionSolicitada = String.Empty;
		System.DateTime dtFechaSolicitada = DateTime.FromOADate(0);
		System.DateTime dtFechaPrescripcion = DateTime.FromOADate(0);
		string stTipoPaciente = String.Empty;
		string stTurno = String.Empty;

		int ServicioUsuario = 0;
		DataSet rsd = null;
		private SqlCommand _Rq = null;
		public CCI360F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}


		SqlCommand Rq
		{
			get
			{
				if (_Rq == null)
				{
					_Rq = new SqlCommand();
				}
				return _Rq;
			}
			set
			{
				_Rq = value;
			}
		}

		bool filaseleccionada = false;
		Mensajes.ClassMensajes ClaseMensaje = null;
		string vNomAplicacion = String.Empty;
		string vAyuda = String.Empty;
		System.DateTime FechaDesde = DateTime.FromOADate(0), FechaHasta = DateTime.FromOADate(0);
		FixedLengthString CambioTurno = new FixedLengthString(5);
		int INSociedad = 0;
		string strInspeccion = String.Empty;
		string StAgenda = String.Empty;
		//********************************************************************
		// CARGA LOS PARAMETROS RECIBIDOS PARA LA CONSULTA
		//********************************************************************

		string iServicio = String.Empty;
		int lResponsable = 0;
		bool VengoDeAgenda = false;
		bool VengoDeServicio = false;
		string stCodAgenda = String.Empty;
		string DesAgenda = String.Empty;
		string CodPersona = String.Empty;
		string DesSala = String.Empty;
		string DesServicio = String.Empty;
		int StServicioCita = 0;
		int StResponsableCita = 0;

		private static readonly Color COLOR_CREMA = Color.FromArgb(255, 255, 192);

		public void RecibirParametros(SqlConnection pConexion, dynamic PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, object PrestacionSolicitada_optional, object DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, object Turno, object NSociedad, object stInspeccion, string CodAgendaSolicitada, System.DateTime FechaPrescripcion)
		{
			string PrestacionSolicitada = (PrestacionSolicitada_optional == Type.Missing) ? String.Empty : PrestacionSolicitada_optional as string;

			Conexion = pConexion;
			Formulario = PFormulario;
			Modo = PModo;
			stUsuario = pUsuario;
			iAgendaSolicitada = CodAgendaSolicitada;
			iServicioSolicitado = ServicioSolicitado;
			stDServicioSolicitado = DServicioSolicitado;
			lResponsableSolicitado = ResponsableSolicitado;
			stNResponsableSolicitado = NResponsableSolicitado;
			stSalaSolicitada = SalaSolicitada;
			stDSalaSolicitada = DSalaSolicitada;
			if (PrestacionSolicitada_optional != Type.Missing)
			{
				stPrestacionSolicitada = PrestacionSolicitada;
			}
			if (DPrestacionSolicitada != Type.Missing)
			{
				stDPrestacionSolicitada = DPrestacionSolicitada.ToString();
			}


			if (Convert.ToString(Formulario.Name) == "CRP410F3")
			{ //Formulario de Reprogramacion de Citas
				//Oscar C Marzo 2009
				//Al reprogramar, la aplicaci�n debe permitir modificar las fechas desde - hasta para buscar los huecos de cita con
				//la limitaci�n que la fecha inicial (fecha desde) no podr� ser inferior a la fecha de indicaci�n (Prescripci�n) de la cita.
				//Al volver de la selecci�n de hueco, si la Fecha de la Cita es anterior a la Fecha de la Solicitud, �sta se modificar� con la Fecha de la Cita.
				if (FechaPrescripcion < DateTime.Today)
				{
					dtFechaSolicitada = DateTime.Today;
				}
				else
				{
					dtFechaSolicitada = FechaPrescripcion;
				}
			}
			else
			{
				if (FechaSolicitada < DateTime.Today)
				{
					dtFechaSolicitada = DateTime.Today;
				}
				else
				{
					dtFechaSolicitada = FechaSolicitada;
				}
			}


			stTipoPaciente = TipoPaciente;
			if (Turno != Type.Missing)
			{
				stTurno = Turno.ToString();
			}
			if (NSociedad != Type.Missing)
			{
				INSociedad = Convert.ToInt32(NSociedad);
			}
			else
			{
				INSociedad = 0;
			}
			if (stInspeccion != Type.Missing)
			{
				strInspeccion = stInspeccion.ToString();
			}
			else
			{
				strInspeccion = "";
			}
			iAgendaSolicitada = CodAgendaSolicitada;

			dtFechaPrescripcion = FechaPrescripcion; //Oscar C Marzo 2009

		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, object PrestacionSolicitada_optional, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, string Turno, int NSociedad, string stInspeccion, string CodAgendaSolicitada)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada_optional, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, Turno, NSociedad, stInspeccion, CodAgendaSolicitada, DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, object PrestacionSolicitada_optional, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, string Turno, int NSociedad, string stInspeccion)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada_optional, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, Turno, NSociedad, stInspeccion, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, object PrestacionSolicitada_optional, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, string Turno, int NSociedad)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada_optional, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, Turno, NSociedad, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, object PrestacionSolicitada_optional, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, string Turno)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada_optional, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, Turno, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, object PrestacionSolicitada_optional, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada_optional, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, object PrestacionSolicitada_optional, string DPrestacionSolicitada, System.DateTime FechaSolicitada)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada_optional, DPrestacionSolicitada, FechaSolicitada, String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, object PrestacionSolicitada_optional, string DPrestacionSolicitada)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada_optional, DPrestacionSolicitada, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, object PrestacionSolicitada_optional)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada_optional, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, Type.Missing, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, String.Empty, Type.Missing, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, String.Empty, String.Empty, Type.Missing, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, String.Empty, String.Empty, String.Empty, Type.Missing, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado, string DServicioSolicitado)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, DServicioSolicitado, 0, String.Empty, String.Empty, String.Empty, Type.Missing, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario, int ServicioSolicitado)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, ServicioSolicitado, String.Empty, 0, String.Empty, String.Empty, String.Empty, Type.Missing, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void RecibirParametros(SqlConnection pConexion, object PFormulario, string PModo, string pUsuario)
		{
			RecibirParametros(pConexion, PFormulario, PModo, pUsuario, 0, String.Empty, 0, String.Empty, String.Empty, String.Empty, Type.Missing, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		private bool isInitializingComponent;
		private void CbbAgendas_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			int I = 0;
			VengoDeAgenda = true;

			CargarComboServicio();
			VengoDeAgenda = false;

		}

		private void CbbAgendas_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			int I = 0;
			DataSet RrGRID = null;
			string StSql = String.Empty;
			if (Modo != "SELECCION")
			{
				// Guardo la descripcion de la Agenda seleccionada
				StAgenda = CbbAgendas.Text;
				stCodAgenda = "";
				// Consulta para sacar el codigo de la Agenda
				StSql = "Select gagendas From  Cagendas Where dagendas ='" + StAgenda + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
				RrGRID = new DataSet();
				tempAdapter.Fill(RrGRID);
				if (RrGRID.Tables[0].Rows.Count != 0)
				{
					// Guardo el codigo de la agenda seleccionada
					stCodAgenda = Convert.ToString(RrGRID.Tables[0].Rows[0]["gagendas"]);
					iAgendaSolicitada = Convert.ToString(RrGRID.Tables[0].Rows[0]["gagendas"]);
				}

				VengoDeAgenda = true;

				CargarComboServicio();
				VengoDeAgenda = false;
			}
		}

		private void CCI360F1_Load(Object eventSender, EventArgs eventArgs)
		{

			this.Cursor = Cursors.WaitCursor;
			sprHuecos.MaxCols = 17;

			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);

			//Oscar: cambio de literales
			string StSql = "Select VALFANU1 from SCONSGLO where GCONSGLO='LITCONSU'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet rsUsuario = new DataSet();
			tempAdapter.Fill(rsUsuario);
			if (rsUsuario.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(rsUsuario.Tables[0].Rows[0][0]).Trim().ToUpper() == "S")
				{
					sprHuecos.Row = 0;
					sprHuecos.Col = 5;
					sprHuecos.Text = "Disponib. pac. normales";
					sprHuecos.Col = 6;
					sprHuecos.Text = "Disponib. pac. preferentes";
				}
			}
			rsUsuario.Close();
			//---------
			//Coger descripcion de agenda
			StAgendaSolicitada = "";
			if (iAgendaSolicitada != "")
			{
				StSql = "Select dagendas from CAGENDAS where gagendas='" + iAgendaSolicitada + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
				rsUsuario = new DataSet();
				tempAdapter_2.Fill(rsUsuario);
				if (rsUsuario.Tables[0].Rows.Count != 0)
				{
					StAgendaSolicitada = Convert.ToString(rsUsuario.Tables[0].Rows[0]["dagendas"]);
				}
				rsUsuario.Close();
			}
			ClaseMensaje = new Mensajes.ClassMensajes();

			// Se limpian los combos
			cbbServicio.Items.Clear();
			cbbResponsable.Items.Clear();
			cbbSalaConsulta.Clear();
			cbbPrestacion.Clear();
			CbbAgendas.Items.Clear();

			// Se establece como fechas minimas la fecha actual
			SSFechaDesde.MinDate = DateTime.Parse(dtFechaSolicitada.ToString("dd/MM/yyyy"));
			SSFechaHasta.MinDate = DateTime.Parse(dtFechaSolicitada.ToString("dd/MM/yyyy"));

			sprHuecos.MaxRows = 0;
			// Inicialmente no se selecciona ninguna fila
			sprHuecos.setSelModeIndex(0);

			// Si el formulario se ejecuta en modo seleccion se presentan los
			// datos permitiendo modificar unicamente la fecha hasta

			if (Modo == "SELECCION")
			{
				if (iAgendaSolicitada != "")
				{
					CbbAgendas.AddItem(StAgendaSolicitada);
					//         CbbAgendas.Column(1, 0) = StAgendaSolicitada
					CbbAgendas.SelectedIndex = 0;
				}
				CbbAgendas.Enabled = false;

				if (iServicioSolicitado != 0)
				{
					cbbServicio.AddItem(stDServicioSolicitado);
					cbbServicio.SetItemData(cbbServicio.GetNewIndex(), iServicioSolicitado);
					cbbServicio.SelectedIndex = cbbServicio.GetNewIndex();
				}
				cbbServicio.Enabled = false;

				if (lResponsableSolicitado != 0)
				{
					cbbResponsable.AddItem(stNResponsableSolicitado);
					cbbResponsable.SetItemData(cbbResponsable.GetNewIndex(), lResponsableSolicitado);
					cbbResponsable.SelectedIndex = cbbResponsable.GetNewIndex();
				}
				cbbResponsable.Enabled = false;

				if (stSalaSolicitada != "")
				{
					object tempRefParam = stDSalaSolicitada;
					object tempRefParam2 = Type.Missing;
					cbbSalaConsulta.AddItem(tempRefParam, tempRefParam2);
					stDSalaSolicitada = Convert.ToString(tempRefParam);
					cbbSalaConsulta.set_Column(1, 0, stSalaSolicitada);
					cbbSalaConsulta.set_ListIndex(0);
				}
				cbbSalaConsulta.Enabled = false;

				if (stPrestacionSolicitada != "")
				{
					object tempRefParam3 = stDPrestacionSolicitada;
					object tempRefParam4 = Type.Missing;
					cbbPrestacion.AddItem(tempRefParam3, tempRefParam4);
					stDPrestacionSolicitada = Convert.ToString(tempRefParam3);
					cbbPrestacion.set_Column(1, 0, stPrestacionSolicitada);
					cbbPrestacion.set_ListIndex(0);
				}
				cbbPrestacion.Enabled = false;

				//      If dtFechaSolicitada < Now Then
				//         SSFechaDesde.DateValue = Format(Now, "dd/mm/yyyy")
				//      Else
				SSFechaDesde.Value = DateTime.Parse(dtFechaSolicitada.ToString("dd/MM/yyyy"));
				//      End If
				SSFechaDesde.Enabled = false;
				if (stTipoPaciente == "N")
				{
					rbNormal.IsChecked = true;
				}
				if (stTipoPaciente == "P")
				{
					rbPreferente.IsChecked = true;
				}
				frmTipoPaciente.Enabled = false;
				if (stTurno == "M")
				{
					rbMa�ana.IsChecked = true;
					frmTurno.Enabled = false;
				}
				if (stTurno == "T")
				{
					rbTarde.IsChecked = true;
					frmTurno.Enabled = false;
				}

				//Oscar C Marzo 2009
				//Al reprogramar, la aplicaci�n debe permitir modificar las fechas desde - hasta para buscar los huecos de cita con
				//la limitaci�n que la fecha inicial (fecha desde) no podr� ser inferior a la fecha de indicaci�n (Prescripci�n) de la cita.
				//Al volver de la selecci�n de hueco, si la Fecha de la Cita es anterior a la Fecha de la Solicitud, �sta se modificar� con la Fecha de la Cita.
				if (Convert.ToString(Formulario.Name) == "CRP410F3")
				{ //Formulario de Reprogramacion de Citas
					SSFechaDesde.Enabled = true;
					SSFechaHasta.Enabled = true;
					SSFechaDesde.MinDate = DateTime.Parse(dtFechaSolicitada.ToString("dd/MM/yyyy"));
					SSFechaDesde.Text = dtFechaSolicitada.ToString("dd/MM/yyyy");
					SSFechaDesde.Value = DateTime.Parse(dtFechaSolicitada.ToString("dd/MM/yyyy"));
					SSFechaHasta.MinDate = DateTime.Parse(dtFechaSolicitada.ToString("dd/MM/yyyy"));
				}
				//----------------

			}
			else
			{
				// si el formulario se ejecuta en modo consulta se permite capturar
				// todos los parametros. Se muestra por defecto en fecha desde la
				// fecha de hoy y como tipo de paciente normal
				CargarComboAgendas();
				CargarComboServicio();
				CargarComboSala();
				CargarComboPrestacion();
				SSFechaDesde.Value = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				rbNormal.IsChecked = true;
				cbBuscar.Enabled = false;

			}

			// Se carga la fecha hasta con la fecha desde mas 15 dias
			SSFechaHasta.Value = DateTime.Parse(DateTime.Parse(SSFechaDesde.Text).AddDays(15).ToString("dd/MM/yyyy"));

			// Si el formulario se carga en modo seleccion se permite seleccionar
			// una fila en el spread. En caso contrario los datos son solo de
			// lectura
			if (Modo == "SELECCION")
			{
                sprHuecos.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
			}
			else
			{
                sprHuecos.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
			}

			// Se obtiene la hora a la que cambia el turno entre ma�ana y tarde
			StSql = "SELECT valfanu2 " + 
			        "FROM sconsglo " + 
			        "WHERE gconsglo = 'HOTURNOM'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, Conexion);
			DataSet rsTurno = new DataSet();
			tempAdapter_3.Fill(rsTurno);
			CambioTurno.Value = Convert.ToString(rsTurno.Tables[0].Rows[0]["valfanu2"]);

			// Se obtiene el servicio del usuario
			StSql = "SELECT gservici " + 
			        "FROM susuario " + 
			        "WHERE gusuario = '" + stUsuario + "'";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSql, Conexion);
			rsUsuario = new DataSet();
			tempAdapter_4.Fill(rsUsuario);
			ServicioUsuario = Convert.ToInt32(rsUsuario.Tables[0].Rows[0]["gservici"]);

			// Se obtiene el nombre de la aplicacion y el fichero de ayuda
			vNomAplicacion = ObtenerNombreAplicacion();
			vAyuda = ObtenerFicheroAyuda();

			// Se obtiene el nombre del fichero de ayuda
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_DLLS_ASTERISCO);

			this.Cursor = Cursors.Default;

		}

		private string ObtenerNombreAplicacion()
		{

			//Buscando en la tabla de constantes globales el nombre de la aplicaci�n
			string StSql = "SELECT valfanu1 " + 
			               "FROM SCONSGLO " + 
			               "WHERE gconsglo = 'NOMAPLIC'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet Rs = new DataSet();
			tempAdapter.Fill(Rs);
			return Convert.ToString(Rs.Tables[0].Rows[0]["valfanu1"]).Trim();

		}

		private string ObtenerFicheroAyuda()
		{

			//Como ejemplo se usa la de calculadora
			return "C:\\WINNT\\system32\\calc.hlp";

		}

		private void CargarComboAgendas()
		{

			string StSql = "SELECT gagendas, dagendas FROM CAGENDAS where fborrado is null ORDER BY dagendas";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet Rs = new DataSet();
			tempAdapter.Fill(Rs);
			CbbAgendas.AddItem("<Sin Definir>");
			foreach (DataRow iteration_row in Rs.Tables[0].Rows)
			{
				CbbAgendas.AddItem(Convert.ToString(iteration_row["dagendas"]).Trim());
			}

			Rs.Close();

		}
		private void CargarComboServicio()
		{
			string StSql1 = String.Empty;
			DataSet rsAgenda = null;

			DesServicio = "";
			CodPersona = "";
			DesSala = "";

			cbbServicio.Items.Clear();
			cbbSalaConsulta.Clear();
			cbbResponsable.Items.Clear();
			cbbPrestacion.Clear();

			if (CbbAgendas.SelectedIndex != -1)
			{
				StSql1 = "SELECT  dagendas, gservici, gpersona, gsalacon FROM cagendas WHERE  " + 
				         " gagendas = '" + stCodAgenda + "' ";
				//              " gagendas = '" & CbbAgenda.Column(1, CbbAgenda.ListIndex) & "' "

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql1, Conexion);
				rsAgenda = new DataSet();
				tempAdapter.Fill(rsAgenda);
				if (rsAgenda.Tables[0].Rows.Count != 0)
				{
					DesServicio = Convert.ToString(rsAgenda.Tables[0].Rows[0]["gservici"]).Trim();
					CodPersona = Convert.ToString(rsAgenda.Tables[0].Rows[0]["gpersona"]).Trim();
					DesSala = Convert.ToString(rsAgenda.Tables[0].Rows[0]["gsalacon"]).Trim();
					DesAgenda = Convert.ToString(rsAgenda.Tables[0].Rows[0]["dagendas"]).Trim();

				}
				rsAgenda.Close();

			}
			string StSql = "SELECT DISTINCT dservici.gservici, dnomserv FROM cagendas, dservici " + 
			               "WHERE cagendas.gservici = dservici.gservici ORDER BY dnomserv";
			//

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
			DataSet Rs = new DataSet();
			tempAdapter_2.Fill(Rs);
			cbbServicio.AddItem("<Sin Definir>");
			foreach (DataRow iteration_row in Rs.Tables[0].Rows)
			{

				cbbServicio.AddItem(Convert.ToString(iteration_row["dnomserv"]));
				cbbServicio.SetItemData(cbbServicio.GetNewIndex(), Convert.ToInt32(iteration_row["gservici"]));
				if (CbbAgendas.SelectedIndex != -1)
				{
					if (DesServicio == Convert.ToString(iteration_row["gservici"]).Trim())
					{
						cbbServicio.SelectedIndex = cbbServicio.GetNewIndex();
					}
				}

			}
			DesServicio = "";
			Rs.Close();

		}

		private void FiltrarComboAgenda()
		{
			string codigoservicio = String.Empty;
			string StSql = String.Empty;

			CbbAgendas.Items.Clear();

			if (cbbServicio.SelectedIndex != -1 && cbbServicio.Text != "<Sin Definir>" && cbbServicio.Text != "")
			{
				codigoservicio = cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString();
				StSql = "SELECT gagendas, dagendas FROM cagendas where gservici = '" + codigoservicio + "' and fborrado is null ORDER BY dagendas";
			}
			else
			{
				StSql = "SELECT gagendas, dagendas FROM cagendas where fborrado is null  ORDER BY dagendas";
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet Rs = new DataSet();
			tempAdapter.Fill(Rs);
			CbbAgendas.AddItem("<Sin Definir>");
			foreach (DataRow iteration_row in Rs.Tables[0].Rows)
			{
				CbbAgendas.AddItem(Convert.ToString(iteration_row["dagendas"]).Trim());
				//      CbbAgenda.Column(1, i) = Trim(Rs!gagendas)
				//      i = i + 1
			}
			Rs.Close();

		}

		private void CargarComboSala()
		{
			string Sala = String.Empty;

			if (cbbSalaConsulta.Text != "<Sin Definir>" && cbbSalaConsulta.Text != "")
			{
				object tempRefParam = 1;
				object tempRefParam2 = cbbSalaConsulta.get_ListIndex();
				Sala = Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam, tempRefParam2));
			}
			else
			{
				Sala = String.Empty;
			}

			string StSql = "SELECT DISTINCT dsalacon.gsalacon, dsalacon " + 
			               "FROM dsalacon, cagendas " + 
			               "WHERE dsalacon.gsalacon = cagendas.gsalacon ";
			if (cbbServicio.SelectedIndex != -1 && cbbServicio.Text != "<Sin Definir>" && cbbServicio.Text != "")
			{
				StSql = StSql + 
				        "AND cagendas.gservici = " + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString() + " ";
			}
			if (cbbResponsable.SelectedIndex != -1 && cbbResponsable.Text != "<Sin Definir>" && cbbResponsable.Text != "")
			{
				StSql = StSql + 
				        "AND cagendas.gpersona = " + cbbResponsable.GetItemData(cbbResponsable.SelectedIndex).ToString() + " ";
			}
			StSql = StSql + 
			        "ORDER BY dsalacon";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet Rs = new DataSet();
			tempAdapter.Fill(Rs);

			int I = 1;

			cbbSalaConsulta.Clear();
			object tempRefParam3 = "<Sin Definir>";
			object tempRefParam4 = Type.Missing;
			cbbSalaConsulta.AddItem(tempRefParam3, tempRefParam4);

			foreach (DataRow iteration_row in Rs.Tables[0].Rows)
			{

				object tempRefParam5 = iteration_row["DSALACON"];
				object tempRefParam6 = Type.Missing;
				cbbSalaConsulta.AddItem(tempRefParam5, tempRefParam6);
				cbbSalaConsulta.set_Column(1, I, iteration_row["gsalacon"].ToString());
				if (Sala == Convert.ToString(iteration_row["gsalacon"]))
				{
					cbbSalaConsulta.set_ListIndex(I);
				}
				if (DesSala.Trim() == Convert.ToString(iteration_row["gsalacon"]).Trim())
				{
					cbbSalaConsulta.set_ListIndex(I);
				}
				I++;

			}

			Rs.Close();
			DesSala = "";


		}

		private void CargarComboPrestacion()
		{
			string Prestacion = String.Empty;
			DataSet Rs = null;
			bool EncontradaPrestacion = false;
			int I = 0;
			string StSql = String.Empty;


			if (Convert.ToDouble(cbbPrestacion.get_ListIndex()) != -1)
			{
				object tempRefParam = 1;
				object tempRefParam2 = cbbPrestacion.get_ListIndex();
				Prestacion = Convert.ToString(cbbPrestacion.get_Column(tempRefParam, tempRefParam2));
			}
			else
			{
				Prestacion = String.Empty;
			}
			cbBuscar.Enabled = false;
			if (CbbAgendas.SelectedIndex == -1 && cbbServicio.SelectedIndex == -1 && cbbResponsable.SelectedIndex == -1 && Convert.ToDouble(cbbSalaConsulta.get_ListIndex()) == -1)
			{
				StSql = "SELECT DISTINCT DCODPRES.gprestac, dprestac FROM DCODPRES, CPRESPRO " + 
				        " WHERE CPRESPRO.gprestac = DCODPRES.gprestac AND ffinvali is null ORDER BY dprestac";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
				Rs = new DataSet();
				tempAdapter.Fill(Rs);
				//    i = 1
				I = 0;
				cbbPrestacion.Clear();
				//    cbbPrestacion.AddItem "<Sin Definir>"

				foreach (DataRow iteration_row in Rs.Tables[0].Rows)
				{
					object tempRefParam3 = iteration_row["dprestac"];
					object tempRefParam4 = Type.Missing;
					cbbPrestacion.AddItem(tempRefParam3, tempRefParam4);
					cbbPrestacion.set_Column(1, I, iteration_row["gprestac"].ToString());
					I++;
				}
				Rs.Close();
			}
			else
			{

				StSql = "SELECT DISTINCT cprespro.gprestac, dprestac, iservlab,ibancsan,DCODPRES.finivali,cagendas.gagendas " + 
				        "FROM cprespro, dcodpres, cagendas, dservici " + 
				        "WHERE cprespro.gprestac = dcodpres.gprestac AND cprespro.gagendas = cagendas.gagendas AND " + 
				        "cagendas.gservici = dservici.gservici and dcodpres.ffinvali is null ";

				if (cbbServicio.SelectedIndex != -1 && cbbServicio.Text != "<Sin Definir>" && cbbServicio.Text != "")
				{
					StSql = StSql + "AND cagendas.gservici = " + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString() + " ";
				}
				if (cbbResponsable.SelectedIndex != -1 && cbbResponsable.Text != "<Sin Definir>" && cbbResponsable.Text != "")
				{
					StSql = StSql + "AND cagendas.gpersona = " + cbbResponsable.GetItemData(cbbResponsable.SelectedIndex).ToString() + " ";
				}
				if (Convert.ToDouble(cbbSalaConsulta.get_ListIndex()) != -1)
				{
					object tempRefParam5 = 0;
					object tempRefParam6 = cbbSalaConsulta.get_ListIndex();
					object tempRefParam7 = 0;
					object tempRefParam8 = cbbSalaConsulta.get_ListIndex();
					if (Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam5, tempRefParam6)) != "<Sin Definir>" && Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam7, tempRefParam8)) != "")
					{
						object tempRefParam9 = 1;
						object tempRefParam10 = cbbSalaConsulta.get_ListIndex();
						StSql = StSql + "AND cagendas.gsalacon = '" + Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam9, tempRefParam10)) + "' ";
					}
				}
				if (CbbAgendas.SelectedIndex != -1)
				{
					StSql = StSql + " AND  cagendas.gagendas = '" + stCodAgenda + "' ";
				}
				StSql = StSql + 
				        "ORDER BY dprestac";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
				Rs = new DataSet();
				tempAdapter_2.Fill(Rs);

				I = 0;
				cbbPrestacion.Clear();
				//    cbbPrestacion.AddItem "<Sin Definir>"

				foreach (DataRow iteration_row_2 in Rs.Tables[0].Rows)
				{
					EncontradaPrestacion = false;
					for (int X = 0; X <= cbbPrestacion.ListCount - 1; X++)
					{
						object tempRefParam11 = 1;
						object tempRefParam12 = X;
						if (iteration_row_2["gprestac"] == cbbPrestacion.get_Column(tempRefParam11, tempRefParam12))
						{
							X = Convert.ToInt32(tempRefParam12);
							EncontradaPrestacion = true;
							break;
						}
						else
						{
							X = Convert.ToInt32(tempRefParam12);
						}
					}
					if (!EncontradaPrestacion)
					{
						object tempRefParam13 = iteration_row_2["dprestac"];
						object tempRefParam14 = Type.Missing;
						cbbPrestacion.AddItem(tempRefParam13, tempRefParam14);
						cbbPrestacion.set_Column(1, I, iteration_row_2["gprestac"].ToString());
						if (Prestacion == Convert.ToString(iteration_row_2["gprestac"]))
						{
							cbbPrestacion.set_ListIndex(I);
							cbBuscar.Enabled = true;
						}
						I++;
					}

				}

				Rs.Close();
			}
		}

		private void cbbServicio_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{

			if (Modo != "SELECCION")
			{
				if (!VengoDeAgenda || cbbServicio.SelectedIndex == 0)
				{
					FiltrarComboAgenda();
				}

				if (cbbServicio.SelectedIndex != -1 && cbbServicio.Text != "<Sin Definir>" && cbbServicio.Text != "")
				{
					CargarComboResponsable();
					CargarComboSala();
					CargarComboPrestacion();
				}

				if (Modo == "CONSULTA")
				{
					CargarComboResponsable();
					CargarComboSala();
					CargarComboPrestacion();
				}
			}
		}

		private void CargarComboResponsable()
		{
			int Responsable = 0;
			string nombre = String.Empty;

			if (cbbResponsable.SelectedIndex != -1 && cbbResponsable.Text != "<Sin Definir>" && cbbResponsable.Text != "")
			{
				Responsable = int.Parse(cbbResponsable.GetItemData(cbbResponsable.SelectedIndex));
			}
			else
			{
				Responsable = 0;
			}

			string StSql = "SELECT DISTINCT cagendas.gpersona, dnompers, dap1pers, dap2pers " + 
			               "FROM cagendas, dpersona " + 
			               "WHERE cagendas.gpersona = dpersona.gpersona ";
			if (cbbServicio.SelectedIndex != -1 && cbbServicio.Text != "<Sin Definir>" && cbbServicio.Text != "")
			{
				StSql = StSql + 
				        "AND cagendas.gservici = " + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString() + " ";
			}
			if (CbbAgendas.SelectedIndex != -1)
			{

				StSql = StSql + 
				        " AND  cagendas.gagendas = '" + stCodAgenda + "' ";
				//                " AND  cagendas.gagendas = '" & CbbAgenda.Column(1, CbbAgenda.ListIndex) & "' "
			}
			StSql = StSql + 
			        "and dpersona.fborrado is null ORDER BY dap1pers, dap2pers, dnompers";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet Rs = new DataSet();
			tempAdapter.Fill(Rs);

			cbbResponsable.Items.Clear();
			cbbResponsable.AddItem("<Sin Definir>");

			foreach (DataRow iteration_row in Rs.Tables[0].Rows)
			{

				nombre = Convert.ToString(iteration_row["dap1pers"]).Trim() + " " + Convert.ToString(iteration_row["dap2pers"]).Trim() + ", " + Convert.ToString(iteration_row["dnompers"]).Trim();
				cbbResponsable.AddItem(nombre);
				cbbResponsable.SetItemData(cbbResponsable.GetNewIndex(), Convert.ToInt32(iteration_row["gpersona"]));
				if (Responsable == Convert.ToDouble(iteration_row["gpersona"]))
				{
					cbbResponsable.SelectedIndex = cbbResponsable.GetNewIndex();
				}
				if (CodPersona.Trim() == Convert.ToString(iteration_row["gpersona"]).Trim())
				{
					cbbResponsable.SelectedIndex = cbbResponsable.GetNewIndex();
				}

			}

			Rs.Close();
			CodPersona = "";

		}

		private void cbbSalaConsulta_ClickEvent(Object eventSender, EventArgs eventArgs)
		{

			if (Modo != "SELECCION")
			{
				if (Modo == "CONSULTA")
				{
					cbbPrestacion.Enabled = true;
					CargarComboPrestacion();
				}
			}
		}

		private void cbbResponsable_Enter(Object eventSender, EventArgs eventArgs)
		{

			if (cbbResponsable.Items.Count == 0)
			{
				CargarComboResponsable();
			}

		}

		private void cbbResponsable_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			if (Modo != "SELECCION")
			{

				if (Modo == "CONSULTA")
				{
					CargarComboSala();
					cbbPrestacion.Enabled = true;
					CargarComboPrestacion();
				}
			}
		}

		private void cbbPrestacion_ClickEvent(Object eventSender, EventArgs eventArgs)
		{

			cbBuscar.Enabled = true;

		}

		private void rbMa�ana_DblClick()
		{

			if (rbMa�ana.IsChecked)
			{
				rbMa�ana.IsChecked = false;
			}

		}

		private void rbTarde_DblClick()
		{

			if (rbTarde.IsChecked)
			{
				rbTarde.IsChecked = false;
			}

		}

		private void cbBuscar_Click(Object eventSender, EventArgs eventArgs)
		{
			StringBuilder StSql = new StringBuilder();
			string vNomAplicacion = String.Empty;
			DataSet cursor = null;
			DataSet RsHuecos = null;
			int I = 0;
			string[, ] Lista = null;
			string stDESSociedad = String.Empty;
			string stDESPrestacion = String.Empty;
			int codsegsoc = 0;
			try
			{
				StSql = new StringBuilder("select nnumeri1 from sconsglo where gconsglo = 'segursoc'");
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql.ToString(), Conexion);
				cursor = new DataSet();
				tempAdapter.Fill(cursor);
				codsegsoc = Convert.ToInt32(cursor.Tables[0].Rows[0]["nnumeri1"]);
				cursor.Close();
				vNomAplicacion = ObtenerNombreAplicacion();
				lblTotal.Visible = false;
				//-----------------------Comprobamos si el servicio est� dado de baja------------------------------------------------
				if (cbbServicio.SelectedIndex > 0)
				{
					StSql = new StringBuilder("select fborrado from dservici where gservici=" + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString());

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql.ToString(), Conexion);
					cursor = new DataSet();
					tempAdapter_2.Fill(cursor);
					if (cursor.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(cursor.Tables[0].Rows[0]["fborrado"]))
						{
							short tempRefParam = 1520;
							string[] tempRefParam2 = new string[]{"fechas disponibles", "los par�metros de la consulta"};
							ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam, Conexion, tempRefParam2);
							return;
						}
					}
					else
					{
						short tempRefParam3 = 1520;
						string[] tempRefParam4 = new string[]{"fechas disponibles", "los par�metros de la consulta"};
						ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam3, Conexion, tempRefParam4);
						return;
					}
					cursor.Close();
				}
				//--------------------------------------------------------------------------------------------------------

				//selecciono todas las agendas y bloques que pasan consulta para la sociedad y prestacion
				if (INSociedad != 0)
				{
					object tempRefParam5 = 1;
					object tempRefParam6 = cbbPrestacion.get_ListIndex();
					StSql = new StringBuilder("select * from cagesoci where gprestac='" + Convert.ToString(cbbPrestacion.get_Column(tempRefParam5, tempRefParam6)) + "' and gsocieda=" + INSociedad.ToString());
					if (strInspeccion != "")
					{
						StSql.Append(" and ginspecc = '" + strInspeccion + "'");
					}
					//    If StAgenda <> "" Then stsql = stsql & " and gagendas = '" & StAgenda & "'"
					if (stCodAgenda != "")
					{
						StSql.Append(" and gagendas = '" + stCodAgenda + "'");
					}

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql.ToString(), Conexion);
					cursor = new DataSet();
					tempAdapter_3.Fill(cursor);
					if (cursor.Tables[0].Rows.Count != 0)
					{
						cursor.MoveFirst();
						I = 1;
						foreach (DataRow iteration_row in cursor.Tables[0].Rows)
						{
							Lista = ArraysHelper.RedimPreserve<string[, ]>(Lista, new int[]{3, I + 1});
							Lista[1, I] = Convert.ToString(iteration_row["gagendas"]);
							Lista[2, I] = Convert.ToString(iteration_row["gbloques"]);
							I++;
						}
					}
					cursor.Close();
				}

				// Se comprueba que la fecha desde sea menor o igual que la fecha hasta
				if (Information.IsDate(SSFechaDesde.Value.Date) && Information.IsDate(SSFechaHasta.Value.Date))
				{
					FechaDesde = DateTime.Parse(SSFechaDesde.Value.Date.ToString("dd/MM/yyyy"));
					FechaHasta = DateTime.Parse(SSFechaHasta.Value.Date.ToString("dd/MM/yyyy"));
					if (FechaDesde > FechaHasta)
					{
						short tempRefParam7 = 1020;
						string[] tempRefParam8 = new string[]{"La fecha desde", "menor o igual", "la fecha hasta"};
						ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam7, Conexion, tempRefParam8);
						SSFechaDesde.Focus();
						return;
					}
				}

				this.Cursor = Cursors.WaitCursor;

				sprHuecos.MaxRows = 0;

				Rq = null;

				Rq.Connection = Conexion;

				// Se obtienen las fechas con huecos disponibles para las condiciones
				// capturadas o recibidas. Si el tipo de paciente es preferente se obtienen
				// todos los puntos de citacion


				//Se debe tener en cuenta el perfil de citacion del usuario
				int PerFilCitacionUsuario = 0;
				PerFilCitacionUsuario = Serrores.fPerfilCitacionUsuario(Conexion, stUsuario);
				//----------------




				Rq.CommandText = "SELECT distinct cagendas.*, cbloques.hinicons, cbloques.hfincons, cbloques.imodoges, cprespro.gprestac, " + 
				                 "cprespro.ndurpres, cprespro.ntobtres, cprespro.nmaxcita, choraspu.*, dservici.dnomserv, " + 
				                 "dpersona.dap1pers, dpersona.dap2pers, dpersona.dnompers, DSALACON.DSALACON, CPRESPRO.gagendas, " + 
				                 "CPRESPRO.gbloques, CPRESPRO.gprestac, " + 
				                 "ISNULL(CBLOQUES.avisoblo,'') avisoblo " + 
				                 "From CPECITPR " + 
				                 "INNER JOIN CPRESPRO ON CPECITPR.gagendas = CPRESPRO.gagendas AND " + 
				                 "CPECITPR.gbloques = CPRESPRO.gbloques AND " + 
				                 "CPECITPR.gprestac = CPRESPRO.gprestac " + 
				                 "INNER JOIN CAGENDAS " + 
				                 "INNER JOIN CBLOQUES ON CAGENDAS.gagendas = CBLOQUES.gagendas ON " + 
				                 "CPRESPRO.gagendas = CBLOQUES.gagendas AND " + 
				                 "CPRESPRO.gbloques = CBLOQUES.gbloques " + 
				                 "INNER JOIN CDIETARI ON CPRESPRO.gagendas = CDIETARI.gagendas AND " + 
				                 "CPRESPRO.gbloques = CDIETARI.gbloques " + 
				                 "INNER JOIN CHORASPU ON CDIETARI.gagendas = CHORASPU.gagendas AND " + 
				                 "CDIETARI.gbloques = CHORASPU.gbloques AND " + 
				                 "CONVERT(Char(10), CDIETARI.fdietari, 101) = CONVERT(Char(10), CHORASPU.fdietari, 101) " + 
				                 "INNER JOIN DSERVICI ON CAGENDAS.gservici = DSERVICI.gservici " + 
				                 "INNER JOIN DPERSONA ON CAGENDAS.gpersona = DPERSONA.gpersona " + 
				                 "INNER JOIN DSALACON ON CAGENDAS.gsalacon = DSALACON.gsalacon ";

				if ( ~((rbNormal.IsChecked) ? -1 : 0) != 0)
				{
					Rq.CommandText = Rq.CommandText + "LEFT OUTER JOIN CHUECPRO ON CHORASPU.gagendas = CHUECPRO.gagendas AND " + 
					                 "CHORASPU.gbloques = CHUECPRO.gbloques AND " + 
					                 "CHORASPU.nordcita = CHUECPRO.nordcita And " + 
					                 "CHORASPU.fdietari = CHUECPRO.fhueccit";
				}
				//Se tienen que tener en cuenta los perfiles del usuario
				Rq.CommandText = Rq.CommandText + " WHERE ( CPECITPR.gpercita =" + PerFilCitacionUsuario.ToString() + " OR " + 
				                 "   EXISTS (SELECT '' FROM CUSUPCIT where CUSUPCIT.GPERCITA=CPECITPR.GPERCITA AND gusuario='" + stUsuario + "') " + 
				                 " ) AND ";
				if (CbbAgendas.SelectedIndex != -1 && CbbAgendas.Text != "<Sin Definir>")
				{
					Rq.CommandText = Rq.CommandText + "(CAGENDAS.gagendas = '" + iAgendaSolicitada + "') AND";
				}
				if (cbbServicio.SelectedIndex != -1 && cbbServicio.Text != "<Sin Definir>")
				{
					Rq.CommandText = Rq.CommandText + "(CAGENDAS.gservici = " + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString() + ") AND";
				}
				if (cbbResponsable.SelectedIndex != -1 && cbbResponsable.Text != "<Sin Definir>")
				{
					Rq.CommandText = Rq.CommandText + "(CAGENDAS.gpersona = " + cbbResponsable.GetItemData(cbbResponsable.SelectedIndex).ToString() + ") AND ";
				}
				if (Convert.ToDouble(cbbSalaConsulta.get_ListIndex()) != -1)
				{
					object tempRefParam9 = 0;
					object tempRefParam10 = cbbSalaConsulta.get_ListIndex();
					if (Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam9, tempRefParam10)) != "<Sin Definir>")
					{
						object tempRefParam11 = 1;
						object tempRefParam12 = cbbSalaConsulta.get_ListIndex();
						Rq.CommandText = Rq.CommandText + "(CAGENDAS.gsalacon = '" + Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam11, tempRefParam12)) + "') AND ";
					}
				}



				object tempRefParam13 = 1;
				object tempRefParam14 = cbbPrestacion.get_ListIndex();
				object tempRefParam15 = DateTimeHelper.ToString(FechaDesde) + " 00:00";
				object tempRefParam16 = DateTimeHelper.ToString(FechaHasta) + " 23:59";
				Rq.CommandText = Rq.CommandText + "(CBLOQUES.icitprop = 'N' OR " + 
				                 "(CBLOQUES.icitprop = 'S' AND CAGENDAS.gservici = " + ServicioUsuario.ToString() + ")) AND " + 
				                 "(CPRESPRO.gprestac = '" + Convert.ToString(cbbPrestacion.get_Column(tempRefParam13, tempRefParam14)) + "') AND " + 
				                 "(CDIETARI.fdietari >= " + Serrores.FormatFechaHM(tempRefParam15) + ") AND " + 
				                 "(CDIETARI.fdietari <= " + Serrores.FormatFechaHM(tempRefParam16) + ") AND " + 
				                 "(CDIETARI.ibloquea = '0') AND ";


				if (INSociedad != 0 && I != 0)
				{
					Rq.CommandText = Rq.CommandText + "ltrim(rtrim(cprespro.gagendas))+ltrim(rtrim(str(cprespro.gbloques))) not in (";
					for (int X = 1; X <= Lista.GetUpperBound(1); X++)
					{
						Rq.CommandText = Rq.CommandText + " '" + Lista[1, X].Trim() + Lista[2, X].Trim() + "'";
						if (X != Lista.GetUpperBound(1))
						{
							Rq.CommandText = Rq.CommandText + ",";
						}
					}
					Rq.CommandText = Rq.CommandText + ") and ";
				}



				if (rbNormal.IsChecked)
				{
					Rq.CommandText = Rq.CommandText + "(CDIETARI.npacnorm > 0 OR CDIETARI.nminnorm >= CPRESPRO.ndurpres) AND " + 
					                 "(CHORASPU.npacnorm > 0 OR CHORASPU.nminnorm > 0) AND ";
				}

				object tempRefParam17 = DateTime.Now;
				Rq.CommandText = Rq.CommandText + "(CHORASPU.ibloquea = '0') AND (CHORASPU.fdietari > " + Serrores.FormatFechaHM(tempRefParam17) + " ) AND ";

				if (rbMa�ana.IsChecked)
				{
					Rq.CommandText = Rq.CommandText + "CONVERT(char(5), CHORASPU.fdietari, 108) < '" + CambioTurno.Value.Trim() + "' AND ";
				}

				if (rbTarde.IsChecked)
				{
					Rq.CommandText = Rq.CommandText + "CONVERT(char(5), CHORASPU.fdietari, 108) >= '" + CambioTurno.Value.Trim() + "' AND ";
				}

				Rq.CommandText = Rq.CommandText + "( (cbloques.imodoges='D' and " + 
				                 "( cbloques.nexcemin is null OR ( cbloques.nexcemin is not null and  " + 
				                 "(((cdietari.nminnorm + cdietari.nminp- (Select isnull(sum(nminpref) + sum(nminnorm),0) from choraspu a where a.gagendas=choraspu.gagendas and a.gbloques=choraspu.gbloques and a.fdietari> cdietari.fdietari  and a.fdietari< dateadd(d,1,cdietari.fdietari) and a.ibloquea<>0))*(-1))+ cprespro.ndurpres ) <= cbloques.nexcemin)) )" + 
				                 "OR (cbloques.imodoges='C' and " + 
				                 "( cbloques.nexcepac is null OR ( cbloques.nexcepac is not null and  " + 
				                 "(cdietari.npacnorm + cdietari.npacp-(Select isnull(sum(npacpref) + sum(npacnorm),0) from choraspu a where a.gagendas=choraspu.gagendas and a.gbloques=choraspu.gbloques and a.fdietari> cdietari.fdietari  and a.fdietari< dateadd(d,1,cdietari.fdietari) and a.ibloquea<>0))*(-1) < cbloques.nexcepac)) ) )";

				Rq.CommandText = Rq.CommandText + "ORDER BY choraspu.fdietari, choraspu.gagendas, choraspu.gbloques";


				//******************** O.Frias (SQL-2005) ********************


				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(Rq);
				rsd = new DataSet();
				tempAdapter_4.Fill(rsd);

				bool m = false;
				if (rsd.Tables[0].Rows.Count == 0)
				{




					//Oscar C Septiembre 2011. Requerimiento Argentina 3.2.12.
					//Explicaci�n al no encontrar hueco libre en Citaci�n.
					//Los operadores que utilizan la aplicaci�n necesitan conocer el motivo por el cual el sistema de citaci�n no proporciona hueco, con el fin de informar al paciente de forma telef�nica o presencial.
					//Se solicita que cuando el sistema de citaci�n no proporcione hueco para una determinada cita se especifique el motivo por pantalla. Los motivos que deber�an ser identificados son los siguientes:
					//1) Hacemos la busqueda como hasta ahora. Si hay huecos, guay. Si no, pasamos al punto 2.
					//2) Hacemos la select inicial, pero quitando la restriccion del perfil y usando count(*) en lugar de campos. Si encontramos huecos, el problema eran los permisos (Mensaje: �Hay huecos pero el usuario no tiene permiso para asignarlos por perfil de citaci�n� . Si no, al punto 3.
					//3) Hacemos la select inicial, sin la restricion de entidad y con el count(*). Si encontramos algo, era por la entidad (Mensaje: La entidad econ�mica seleccionada no cubre la prestaci�n. Si no, es que no hay huecos (Mensaje: No existen huecos para los criterios introducidos) y punto.

					m = false;

					//2) Hacemos la select inicial, pero quitando la restriccion del perfil y usando count(*) en lugar de campos.
					//   Si encontramos huecos, el problema eran los permisos (Mensaje: �Hay huecos pero el usuario no tiene permiso para asignarlos por perfil de citaci�n� . Si no, al punto 3.
					if (!m)
					{

						StSql = new StringBuilder("SELECT count(cdietari.fdietari) " + 
						        " From CAGENDAS " + 
						        "INNER JOIN CBLOQUES ON CAGENDAS.gagendas = CBLOQUES.gagendas  " + 
						        "INNER JOIN CPRESPRO ON CPRESPRO.gagendas = CBLOQUES.gagendas AND " + 
						        "   CPRESPRO.gbloques = CBLOQUES.gbloques " + 
						        "INNER JOIN CDIETARI ON CPRESPRO.gagendas = CDIETARI.gagendas AND " + 
						        "CPRESPRO.gbloques = CDIETARI.gbloques " + 
						        "INNER JOIN CHORASPU ON CDIETARI.gagendas = CHORASPU.gagendas AND " + 
						        "CDIETARI.gbloques = CHORASPU.gbloques AND " + 
						        "CONVERT(Char(10), CDIETARI.fdietari, 101) = CONVERT(Char(10), CHORASPU.fdietari, 101) " + 
						        "INNER JOIN DSERVICI ON CAGENDAS.gservici = DSERVICI.gservici " + 
						        "INNER JOIN DPERSONA ON CAGENDAS.gpersona = DPERSONA.gpersona " + 
						        "INNER JOIN DSALACON ON CAGENDAS.gsalacon = DSALACON.gsalacon ");

						if ( ~((rbNormal.IsChecked) ? -1 : 0) != 0)
						{
							StSql.Append("LEFT OUTER JOIN CHUECPRO ON CHORASPU.gagendas = CHUECPRO.gagendas AND " + 
							             "CHORASPU.gbloques = CHUECPRO.gbloques AND " + 
							             "CHORASPU.nordcita = CHUECPRO.nordcita And " + 
							             "CHORASPU.fdietari = CHUECPRO.fhueccit");
						}
						StSql.Append(" WHERE ");
						if (CbbAgendas.SelectedIndex != -1 && CbbAgendas.Text != "<Sin Definir>")
						{
							StSql.Append("(CAGENDAS.gagendas = '" + iAgendaSolicitada + "') AND");
						}
						if (cbbServicio.SelectedIndex != -1 && cbbServicio.Text != "<Sin Definir>")
						{
							StSql.Append("(CAGENDAS.gservici = " + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString() + ") AND");
						}
						if (cbbResponsable.SelectedIndex != -1 && cbbResponsable.Text != "<Sin Definir>")
						{
							StSql.Append("(CAGENDAS.gpersona = " + cbbResponsable.GetItemData(cbbResponsable.SelectedIndex).ToString() + ") AND ");
						}
						if (Convert.ToDouble(cbbSalaConsulta.get_ListIndex()) != -1)
						{
							object tempRefParam18 = 0;
							object tempRefParam19 = cbbSalaConsulta.get_ListIndex();
							if (Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam18, tempRefParam19)) != "<Sin Definir>")
							{
								object tempRefParam20 = 1;
								object tempRefParam21 = cbbSalaConsulta.get_ListIndex();
								StSql.Append("(CAGENDAS.gsalacon = '" + Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam20, tempRefParam21)) + "') AND ");
							}
						}

						object tempRefParam22 = 1;
						object tempRefParam23 = cbbPrestacion.get_ListIndex();
						object tempRefParam24 = DateTimeHelper.ToString(FechaDesde) + " 00:00";
						object tempRefParam25 = DateTimeHelper.ToString(FechaHasta) + " 23:59";
						StSql.Append(
						             "(CBLOQUES.icitprop = 'N' OR " + 
						             "(CBLOQUES.icitprop = 'S' AND CAGENDAS.gservici = " + ServicioUsuario.ToString() + ")) AND " + 
						             "(CPRESPRO.gprestac = '" + Convert.ToString(cbbPrestacion.get_Column(tempRefParam22, tempRefParam23)) + "') AND " + 
						             "(CDIETARI.fdietari >= " + Serrores.FormatFechaHM(tempRefParam24) + ") AND " + 
						             "(CDIETARI.fdietari <= " + Serrores.FormatFechaHM(tempRefParam25) + ") AND " + 
						             "(CDIETARI.ibloquea = '0') AND ");

						if (INSociedad != 0 && I != 0)
						{
							StSql.Append("ltrim(rtrim(cprespro.gagendas))+ltrim(rtrim(str(cprespro.gbloques))) not in (");
							for (int X = 1; X <= Lista.GetUpperBound(1); X++)
							{
								StSql.Append(" '" + Lista[1, X].Trim() + Lista[2, X].Trim() + "'");
								if (X != Lista.GetUpperBound(1))
								{
									StSql.Append(",");
								}
							}
							StSql.Append(") and ");
						}

						if (rbNormal.IsChecked)
						{
							StSql.Append("(CDIETARI.npacnorm > 0 OR CDIETARI.nminnorm >= CPRESPRO.ndurpres) AND " + 
							             "(CHORASPU.npacnorm > 0 OR CHORASPU.nminnorm > 0) AND ");
						}

						object tempRefParam26 = DateTime.Now;
						StSql.Append("(CHORASPU.ibloquea = '0') AND (CHORASPU.fdietari > " + Serrores.FormatFechaHM(tempRefParam26) + " ) AND ");

						if (rbMa�ana.IsChecked)
						{
							StSql.Append("CONVERT(char(5), CHORASPU.fdietari, 108) < '" + CambioTurno.Value.Trim() + "' AND ");
						}
						if (rbTarde.IsChecked)
						{
							StSql.Append("CONVERT(char(5), CHORASPU.fdietari, 108) >= '" + CambioTurno.Value.Trim() + "' AND ");
						}

						StSql.Append(
						             "( (cbloques.imodoges='D' and " + 
						             "( cbloques.nexcemin is null OR ( cbloques.nexcemin is not null and  " + 
						             "(((cdietari.nminnorm + cdietari.nminp- (Select isnull(sum(nminpref) + sum(nminnorm),0) from choraspu a where a.gagendas=choraspu.gagendas and a.gbloques=choraspu.gbloques and a.fdietari> cdietari.fdietari  and a.fdietari< dateadd(d,1,cdietari.fdietari) and a.ibloquea<>0))*(-1))+ cprespro.ndurpres ) <= cbloques.nexcemin)) )" + 
						             "OR (cbloques.imodoges='C' and " + 
						             "( cbloques.nexcepac is null OR ( cbloques.nexcepac is not null and  " + 
						             "(cdietari.npacnorm + cdietari.npacp-(Select isnull(sum(npacpref) + sum(npacnorm),0) from choraspu a where a.gagendas=choraspu.gagendas and a.gbloques=choraspu.gbloques and a.fdietari> cdietari.fdietari  and a.fdietari< dateadd(d,1,cdietari.fdietari) and a.ibloquea<>0))*(-1) < cbloques.nexcepac)) ) )");

						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSql.ToString(), Conexion);
						RsHuecos = new DataSet();
						tempAdapter_5.Fill(RsHuecos);
						if (Convert.ToDouble(RsHuecos.Tables[0].Rows[0][0]) > 0)
						{
							short tempRefParam27 = 4036;
							string[] tempRefParam28 = new string[]{" asignar huecos a ", " por perfil de citaci�n"};
							ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam27, Conexion, tempRefParam28);
							m = true;
						}
						RsHuecos.Close();


					}

					//3) Hacemos la select inicial, sin la restricion de entidad y con el count(*). Si encontramos algo, era por la entidad (Mensaje: La entidad econ�mica seleccionada no cubre la prestaci�n. Si no, es que no hay huecos (Mensaje: No existen huecos para los criterios introducidos) y punto.
					if (!m)
					{

						if (INSociedad != 0 && I != 0)
						{
							if (INSociedad != codsegsoc)
							{
								StSql = new StringBuilder("Select dsocieda from dsocieda where gsocieda = " + INSociedad.ToString());
							}
							else
							{
								StSql = new StringBuilder("Select dinspecc from dinspecc where ginspecc = '" + strInspeccion + "'");
							}
							SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(StSql.ToString(), Conexion);
							RsHuecos = new DataSet();
							tempAdapter_6.Fill(RsHuecos);
							if (RsHuecos.Tables[0].Rows.Count != 0)
							{
								stDESSociedad = Convert.ToString(RsHuecos.Tables[0].Rows[0][0]).Trim();
							}
							else
							{
								stDESSociedad = "";
							}
							RsHuecos.Close();
						}
						stDESPrestacion = cbbPrestacion.Text;

						StSql = new StringBuilder("SELECT count(*) " + 
						        "From CPECITPR " + 
						        "INNER JOIN CPRESPRO ON CPECITPR.gagendas = CPRESPRO.gagendas AND " + 
						        "CPECITPR.gbloques = CPRESPRO.gbloques AND " + 
						        "CPECITPR.gprestac = CPRESPRO.gprestac " + 
						        "INNER JOIN CAGENDAS " + 
						        "INNER JOIN CBLOQUES ON CAGENDAS.gagendas = CBLOQUES.gagendas ON " + 
						        "CPRESPRO.gagendas = CBLOQUES.gagendas AND " + 
						        "CPRESPRO.gbloques = CBLOQUES.gbloques " + 
						        "INNER JOIN CDIETARI ON CPRESPRO.gagendas = CDIETARI.gagendas AND " + 
						        "CPRESPRO.gbloques = CDIETARI.gbloques " + 
						        "INNER JOIN CHORASPU ON CDIETARI.gagendas = CHORASPU.gagendas AND " + 
						        "CDIETARI.gbloques = CHORASPU.gbloques AND " + 
						        "CONVERT(Char(10), CDIETARI.fdietari, 101) = CONVERT(Char(10), CHORASPU.fdietari, 101) " + 
						        "INNER JOIN DSERVICI ON CAGENDAS.gservici = DSERVICI.gservici " + 
						        "INNER JOIN DPERSONA ON CAGENDAS.gpersona = DPERSONA.gpersona " + 
						        "INNER JOIN DSALACON ON CAGENDAS.gsalacon = DSALACON.gsalacon ");

						if ( ~((rbNormal.IsChecked) ? -1 : 0) != 0)
						{
							StSql.Append("LEFT OUTER JOIN CHUECPRO ON CHORASPU.gagendas = CHUECPRO.gagendas AND " + 
							             "CHORASPU.gbloques = CHUECPRO.gbloques AND " + 
							             "CHORASPU.nordcita = CHUECPRO.nordcita And " + 
							             "CHORASPU.fdietari = CHUECPRO.fhueccit");
						}
						StSql.Append(" WHERE ");
						StSql.Append(" ( CPECITPR.gpercita =" + PerFilCitacionUsuario.ToString() + " OR " + 
						             "   EXISTS (SELECT '' FROM CUSUPCIT where CUSUPCIT.GPERCITA=CPECITPR.GPERCITA AND gusuario='" + stUsuario + "') " + 
						             " ) AND ");
						if (CbbAgendas.SelectedIndex != -1 && CbbAgendas.Text != "<Sin Definir>")
						{
							StSql.Append("(CAGENDAS.gagendas = '" + iAgendaSolicitada + "') AND");
						}
						if (cbbServicio.SelectedIndex != -1 && cbbServicio.Text != "<Sin Definir>")
						{
							StSql.Append("(CAGENDAS.gservici = " + cbbServicio.GetItemData(cbbServicio.SelectedIndex).ToString() + ") AND");
						}
						if (cbbResponsable.SelectedIndex != -1 && cbbResponsable.Text != "<Sin Definir>")
						{
							StSql.Append("(CAGENDAS.gpersona = " + cbbResponsable.GetItemData(cbbResponsable.SelectedIndex).ToString() + ") AND ");
						}
						if (Convert.ToDouble(cbbSalaConsulta.get_ListIndex()) != -1)
						{
							object tempRefParam29 = 0;
							object tempRefParam30 = cbbSalaConsulta.get_ListIndex();
							if (Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam29, tempRefParam30)) != "<Sin Definir>")
							{
								object tempRefParam31 = 1;
								object tempRefParam32 = cbbSalaConsulta.get_ListIndex();
								StSql.Append("(CAGENDAS.gsalacon = '" + Convert.ToString(cbbSalaConsulta.get_Column(tempRefParam31, tempRefParam32)) + "') AND ");
							}
						}

						object tempRefParam33 = 1;
						object tempRefParam34 = cbbPrestacion.get_ListIndex();
						object tempRefParam35 = DateTimeHelper.ToString(FechaDesde) + " 00:00";
						object tempRefParam36 = DateTimeHelper.ToString(FechaHasta) + " 23:59";
						StSql.Append("(CBLOQUES.icitprop = 'N' OR " + 
						             "(CBLOQUES.icitprop = 'S' AND CAGENDAS.gservici = " + ServicioUsuario.ToString() + ")) AND " + 
						             "(CPRESPRO.gprestac = '" + Convert.ToString(cbbPrestacion.get_Column(tempRefParam33, tempRefParam34)) + "') AND " + 
						             "(CDIETARI.fdietari >= " + Serrores.FormatFechaHM(tempRefParam35) + ") AND " + 
						             "(CDIETARI.fdietari <= " + Serrores.FormatFechaHM(tempRefParam36) + ") AND " + 
						             "(CDIETARI.ibloquea = '0') AND ");

						if (rbNormal.IsChecked)
						{
							StSql.Append("(CDIETARI.npacnorm > 0 OR CDIETARI.nminnorm >= CPRESPRO.ndurpres) AND " + 
							             "(CHORASPU.npacnorm > 0 OR CHORASPU.nminnorm > 0) AND ");
						}

						object tempRefParam37 = DateTime.Now;
						StSql.Append("(CHORASPU.ibloquea = '0') AND (CHORASPU.fdietari > " + Serrores.FormatFechaHM(tempRefParam37) + " ) AND ");

						if (rbMa�ana.IsChecked)
						{
							StSql.Append("CONVERT(char(5), CHORASPU.fdietari, 108) < '" + CambioTurno.Value.Trim() + "' AND ");
						}
						if (rbTarde.IsChecked)
						{
							StSql.Append("CONVERT(char(5), CHORASPU.fdietari, 108) >= '" + CambioTurno.Value.Trim() + "' AND ");
						}

						StSql.Append("( (cbloques.imodoges='D' and " + 
						             "( cbloques.nexcemin is null OR ( cbloques.nexcemin is not null and  " + 
						             "(((cdietari.nminnorm + cdietari.nminp- (Select isnull(sum(nminpref) + sum(nminnorm),0) from choraspu a where a.gagendas=choraspu.gagendas and a.gbloques=choraspu.gbloques and a.fdietari> cdietari.fdietari  and a.fdietari< dateadd(d,1,cdietari.fdietari) and a.ibloquea<>0))*(-1))+ cprespro.ndurpres ) <= cbloques.nexcemin)) )" + 
						             "OR (cbloques.imodoges='C' and " + 
						             "( cbloques.nexcepac is null OR ( cbloques.nexcepac is not null and  " + 
						             "(cdietari.npacnorm + cdietari.npacp-(Select isnull(sum(npacpref) + sum(npacnorm),0) from choraspu a where a.gagendas=choraspu.gagendas and a.gbloques=choraspu.gbloques and a.fdietari> cdietari.fdietari  and a.fdietari< dateadd(d,1,cdietari.fdietari) and a.ibloquea<>0))*(-1) < cbloques.nexcepac)) ) )");


						SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(StSql.ToString(), Conexion);
						RsHuecos = new DataSet();
						tempAdapter_7.Fill(RsHuecos);
						if (Convert.ToDouble(RsHuecos.Tables[0].Rows[0][0]) > 0)
						{
							if (INSociedad != 0 && I != 0)
							{
								if (INSociedad != codsegsoc)
								{
									short tempRefParam38 = 3810;
									string[] tempRefParam39 = new string[]{stDESPrestacion, " sociedad " + stDESSociedad};
									ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam38, Conexion, tempRefParam39);
								}
								else
								{
									short tempRefParam40 = 3810;
									string[] tempRefParam41 = new string[]{stDESPrestacion, " inspecci�n " + stDESSociedad};
									ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam40, Conexion, tempRefParam41);
								}
							}
							else
							{
								short tempRefParam42 = 3810;
								string[] tempRefParam43 = new string[]{stDESPrestacion, " sociedad/inspecci�n del paciente"};
								ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam42, Conexion, tempRefParam43);
							}

							m = true;
						}
						RsHuecos.Close();


					}

					//3. No existen huecos para los criterios introducidos.
					if (!m)
					{
						short tempRefParam44 = 1520;
						string[] tempRefParam45 = new string[]{"fechas disponibles", "los par�metros de la consulta"};
						ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam44, Conexion, tempRefParam45);
						m = true;
					}

					if (m)
					{
						this.Cursor = Cursors.Default;
						return;
					}

					//-----------------------------------------------------------------------------------------------------------

				}
				else
				{
					if (Modo == "SELECCION")
					{
						cbAceptar.Enabled = true;
					}
				}

				// Se ocultan los codigos de agenda, bloque, duraci�n de la prestaci�n y tiempo de obtenci�n de resultados
				for (I = 12; I <= 15; I++)
				{
					sprHuecos.Col = I;
					sprHuecos.SetColHidden(sprHuecos.Col, true);
				}



				int huecos = 0;
				huecos = Cargar_Huecos();
				lblTotal.Visible = true;
				lblTotal.Text = "N�mero de fechas disponibles: " + StringsHelper.Format(sprHuecos.MaxRows, "##,##0");
				if (sprHuecos.MaxRows == 0)
				{
					cbAceptar.Enabled = false;
					short tempRefParam46 = 1520;
					string[] tempRefParam47 = new string[]{"fechas disponibles", "los par�metros de la consulta"};
					ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam46, Conexion, tempRefParam47);
				}
			}
			catch(Exception ex)
			{
                if (ex.InnerException is SqlException)
                {
                    var errors = new RDO_rdoErrors(ex.InnerException as SqlException);
                    RadMessageBox.Show(errors.rdoErrors[errors.rdoErrors.Count - 1].Message, Application.ProductName);
                }
                else
                {
                    RadMessageBox.Show(ex.Message, Application.ProductName);
                }
				
			}
		}

        private int Cargar_Huecos()
        {
            int result = 0;
            string StSql = String.Empty;
            bool hueco = false;
            DataSet rss = null;
            DataSet rsc = null;
            DataSet RsHuecos = null;
            DataSet rsSesiones = null;
            bool MaximoCitas = false;


            int I = 0;
            System.DateTime FechaAnt = DateTime.FromOADate(0);
            string AgendaAnt = String.Empty;
            int BloqueAnt = 0;

            // Si se ha encontrado alguna hora
            if (rsd.Tables[0].Rows.Count > 0)
            {

                rsd.MoveFirst();

                //'''      Do Until rsd.EOF Or i >= 50
                bool bPerimitirExceso = false;
                DataSet rAux = null;
                foreach (DataRow iteration_row in rsd.Tables[0].Rows)
                {
                    I++;

                    // Si el tipo de paciente es normal y existe un n�mero maximo de
                    // citas para la prestacion en el bloque, se comprueba que no
                    // se haya mas citas programadas que el maximo
                    if (Convert.ToDateTime(iteration_row["fdietari"]).ToString("MM/dd/yyyy") != FechaAnt.ToString("MM/dd/yyyy") || Convert.ToString(iteration_row["fgagendas"]) != AgendaAnt || Convert.ToDouble(iteration_row["fgbloques"]) != BloqueAnt)
                    {
                        MaximoCitas = false;
                        FechaAnt = Convert.ToDateTime(iteration_row["ffdietari"]);
                        AgendaAnt = Convert.ToString(iteration_row["fgagendas"]);
                        BloqueAnt = Convert.ToInt32(iteration_row["fgbloques"]);
                        if ((((rbNormal.IsChecked) ? -1 : 0) & ((!Convert.IsDBNull(iteration_row["fnmaxcita"])) ? -1 : 0) & ((Convert.ToDouble(iteration_row["fnmaxcita"]) > 0) ? -1 : 0)) != 0)
                        {
                            StSql = "SELECT COUNT(*)as NumeroCitas " +
                                    "FROM ccitprog " +
                                    "WHERE gagendas = '" + Convert.ToString(iteration_row["fgagendas"]) + "' AND " +
                                    "gbloques = " + Convert.ToString(iteration_row["fgbloques"]) + " AND " +
                                    "ffeccita >= '" + Convert.ToDateTime(iteration_row["ffdietari"]).ToString("MM/dd/yyyy") + " 00:00' AND " +
                                    "ffeccita <= '" + Convert.ToDateTime(iteration_row["ffdietari"]).ToString("MM/dd/yyyy") + " 23:59' AND " +
                                    "gprestac = '" + Convert.ToString(iteration_row["fgprestac"]) + "'";
                            SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
                            rsc = new DataSet();
                            tempAdapter.Fill(rsc);
                            StSql = "SELECT COUNT(*)as NumeroCitas " +
                                    "FROM chuecpro " +
                                    "WHERE gagendas = '" + Convert.ToString(iteration_row["fgagendas"]) + "' AND " +
                                    "gbloques = " + Convert.ToString(iteration_row["fgbloques"]) + " AND " +
                                    "fhueccit >= '" + Convert.ToDateTime(iteration_row["ffdietari"]).ToString("MM/dd/yyyy") + " 00:00' AND " +
                                    "fhueccit <= '" + Convert.ToDateTime(iteration_row["ffdietari"]).ToString("MM/dd/yyyy") + " 23:59' AND " +
                                    "gprestac = '" + Convert.ToString(iteration_row["fgprestac"]) + "'";
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
                            RsHuecos = new DataSet();
                            tempAdapter_2.Fill(RsHuecos);
                            StSql = "SELECT COUNT(*)as NumeroCitas " +
                                    "FROM csesione, csolcita " +
                                    "WHERE gagendas = '" + Convert.ToString(iteration_row["fgagendas"]) + "' AND " +
                                    "gbloques = " + Convert.ToString(iteration_row["fgbloques"]) + " AND " +
                                    "ffeccita >= '" + Convert.ToDateTime(iteration_row["ffdietari"]).ToString("MM/dd/yyyy") + " 00:00' AND " +
                                    "ffeccita <= '" + Convert.ToDateTime(iteration_row["ffdietari"]).ToString("MM/dd/yyyy") + " 23:59' AND " +
                                    "csesione.ganoregi = csolcita.ganoregi AND " +
                                    "csesione.gnumregi = csolcita.gnumregi AND " +
                                    "gprestac = '" + Convert.ToString(iteration_row["fgprestac"]) + "'";
                            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, Conexion);
                            rsSesiones = new DataSet();
                            tempAdapter_3.Fill(rsSesiones);
                            if (Convert.ToDouble(Convert.ToDouble(rsc.Tables[0].Rows[0]["fNumeroCitas"]) + Convert.ToDouble(RsHuecos.Tables[0].Rows[0]["fNumeroCitas"])) + Convert.ToDouble(rsSesiones.Tables[0].Rows[0]["fNumeroCitas"]) >= Convert.ToDouble(iteration_row["fnmaxcita"]))
                            {
                                MaximoCitas = true;
                            }
                        }
                    }

                    if (!MaximoCitas)
                    {

                        hueco = true;
                        // Si el modo de gestion de la cita es por duracion de la
                        // prestacion se considera con hueco si desde el punto de citacion
                        // en adelante hay tiempo suficiente para realizar la prestacion. Si
                        // es por cantidad de pacientes se considera con huecos si tiene huecos
                        // de normales o preferentes
                        if (Convert.ToString(iteration_row["fimodoges"]) == "D")
                        {

                            if (((((rbNormal.IsChecked) ? -1 : 0) & ((Convert.ToDouble(iteration_row["fnminnorm"]) <= 0) ? -1 : 0)) | (((rbPreferente.IsChecked) ? -1 : 0) & (((Convert.ToDouble(Convert.ToDouble(iteration_row["fnminnorm"]) + Convert.ToDouble(iteration_row["fnminpref"]))) <= 0) ? -1 : 0))) != 0)
                            {
                                hueco = false;
                            }
                            else
                            {
                                if (((((rbNormal.IsChecked) ? -1 : 0) & ((Convert.ToDouble(iteration_row["fnminnorm"]) > 0) ? -1 : 0) & ((Convert.ToInt32(iteration_row["fnminnorm"]) < Convert.ToInt32(iteration_row["fndurpres"])) ? -1 : 0)) | (((rbPreferente.IsChecked) ? -1 : 0) & (((Convert.ToDouble(Convert.ToDouble(iteration_row["fnminnorm"]) + Convert.ToDouble(iteration_row["fnminpref"]))) > 0) ? -1 : 0) & (((Convert.ToDouble(iteration_row["fnminnorm"]) + Convert.ToDouble(iteration_row["fnminpref"])) < Convert.ToDouble(iteration_row["fndurpres"])) ? -1 : 0))) != 0)
                                {

                                    StSql = "SELECT SUM(nminnorm)as MinutosNormales, SUM(nminpref)as MinutosPreferentes " +
                                            "FROM choraspu " +
                                            "WHERE gagendas = '" + Convert.ToString(iteration_row["fgagendas"]) + "' AND " +
                                            "gbloques = " + Convert.ToString(iteration_row["fgbloques"]) + " AND " +
                                            "fdietari >= '" + Convert.ToDateTime(iteration_row["ffdietari"]).ToString("MM/dd/yyyy HH:mm") + "' AND " +
                                            "fdietari <= '" + Convert.ToDateTime(iteration_row["fdietari"]).ToString("MM/dd/yyyy") + " 23:59' AND " +
                                            "ibloquea = '0'";

                                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSql, Conexion);
                                    rss = new DataSet();
                                    tempAdapter_4.Fill(rss);

                                    if (((((rbNormal.IsChecked) ? -1 : 0) & ((Convert.ToInt32(rss.Tables[0].Rows[0]["MinutosNormales"]) >= Convert.ToInt32(iteration_row["ndurpres"])) ? -1 : 0)) | (((rbPreferente.IsChecked) ? -1 : 0) & (((Convert.ToDouble(rss.Tables[0].Rows[0]["MinutosNormales"]) + Convert.ToDouble(rss.Tables[0].Rows[0]["MinutosPreferentes"])) >= Convert.ToDouble(iteration_row["fndurpres"])) ? -1 : 0))) != 0)
                                    {
                                    }
                                    else
                                    {
                                        hueco = false;
                                    }

                                }

                            }

                        }
                        else
                        {
                            if (((((rbNormal.IsChecked) ? -1 : 0) & ((Convert.ToDouble(iteration_row["npacnorm"]) <= 0) ? -1 : 0)) | (((rbPreferente.IsChecked) ? -1 : 0) & (((Convert.ToDouble(Convert.ToDouble(iteration_row["fnpacnorm"]) + Convert.ToDouble(iteration_row["fnpacpref"]))) <= 0) ? -1 : 0))) != 0)
                            {
                                hueco = false;
                            }
                        }

                    }

                    //OSCAR C 17/04/2007
                    //COMPROBAMOS EL EXCESO POR AGENDA
                    bPerimitirExceso = true;
                    if (!Convert.IsDBNull(iteration_row["fAmodoges"]))
                    {
                        StSql = "SELECT SUM(nminnorm)+SUM(nminpref) duracion, SUM(npacnorm)+ SUM(npacpref) paciente" +
                                " FROM CHORASPU " +
                                " WHERE gagendas = '" + Convert.ToString(iteration_row["fgagendas"]) + "' AND IBLOQUEA = '0' AND " +
                                " fdietari >= '" + Convert.ToDateTime(iteration_row["ffdietari"]).ToString("MM/dd/yyyy") + "' AND " +
                                " fdietari <= '" + Convert.ToDateTime(iteration_row["ffdietari"]).ToString("MM/dd/yyyy") + " 23:59' ";
                        SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSql, Conexion);
                        rAux = new DataSet();
                        tempAdapter_5.Fill(rAux);

                        if (Convert.ToString(iteration_row["fAmodoges"]) == "D")
                        {
                            if (!Convert.IsDBNull(iteration_row["fanexcemin"]))
                            {
                                if ((Convert.ToDouble(rAux.Tables[0].Rows[0]["fDuracion"]) * (-1)) + Convert.ToDouble(iteration_row["fndurpres"]) > Convert.ToDouble(iteration_row["fanexcemin"]))
                                {
                                    bPerimitirExceso = false;
                                }
                            }
                        }
                        else
                        {
                            if (!Convert.IsDBNull(iteration_row["fanexcepac"]))
                            {
                                if (Convert.ToDouble(rAux.Tables[0].Rows[0]["fPaciente"]) * (-1) >= Convert.ToDouble(iteration_row["fanexcepac"]))
                                {
                                    bPerimitirExceso = false;
                                }
                            }
                        }
                        rAux.Close();
                        rAux = null;
                    }
                    //------


                    // Si el tipo de paciente es normal y el punto de citacion tiene hueco
                    // se carga en la spread. Si es preferente se carga siempre
                    if ((((!MaximoCitas) ? -1 : 0) & (((rbPreferente.IsChecked) ? -1 : 0) | (((rbNormal.IsChecked) ? -1 : 0) & ((hueco) ? -1 : 0))) & ((bPerimitirExceso) ? -1 : 0)) != 0)
                    {

                        result++;
                        sprHuecos.MaxRows++;
                        sprHuecos.Row = sprHuecos.MaxRows;

                        sprHuecos.Col = -1;
                        if (!hueco)
                        {
                            sprHuecos.ForeColor = Color.Fuchsia;
                        }

                        sprHuecos.Col = 1;
                        sprHuecos.Text = Convert.ToDateTime(iteration_row["ffdietari"]).ToString("dd/MM/yyyy");
                        sprHuecos.Col = 2;
                        sprHuecos.Text = Convert.ToDateTime(iteration_row["ffdietari"]).ToString("HH:mm");
                        sprHuecos.Col = 3;
                        sprHuecos.Text = Conversion.Str(Convert.ToDouble(iteration_row["fnordcita"]) + 1);
                        sprHuecos.Col = 4;
                        sprHuecos.Text = Strings.StrConv(Convert.ToDateTime(iteration_row["ffdietari"]).ToString("dddd"), VbStrConv.ProperCase, 0);

                        sprHuecos.Col = 5;
                        sprHuecos.Text = Conversion.Str(Convert.ToDouble(iteration_row["fnpacnorm"]) + Convert.ToDouble(iteration_row["fnminnorm"]));
                        if (Convert.ToString(iteration_row["fimodoges"]) == "C")
                        {
                            sprHuecos.Text = sprHuecos.Text + " pac.";
                        }
                        else
                        {
                            sprHuecos.Text = sprHuecos.Text + " min.";
                        }
                        sprHuecos.Col = 6;
                        sprHuecos.Text = Conversion.Str(Convert.ToDouble(iteration_row["fnpacpref"]) + Convert.ToDouble(iteration_row["fnminpref"]));
                        if (Convert.ToString(iteration_row["fimodoges"]) == "C")
                        {
                            sprHuecos.Text = sprHuecos.Text + " pac.";
                        }
                        else
                        {
                            sprHuecos.Text = sprHuecos.Text + " min.";
                        }

                        sprHuecos.Col = 7;
                        sprHuecos.Text = Convert.ToString(iteration_row["fdnomserv"]);

                        sprHuecos.Col = 8;
                        sprHuecos.Text = Convert.ToString(iteration_row["fdap1pers"]).Trim() + " " + Convert.ToString(iteration_row["fdap2pers"]).Trim() + ", " + Convert.ToString(iteration_row["fdnompers"]).Trim();

                        sprHuecos.Col = 9;
                        sprHuecos.Text = Convert.ToString(iteration_row["fDSALACON"]);

                        sprHuecos.Col = 10;
                        sprHuecos.Text = Convert.ToString(iteration_row["fdagendas"]);
                        if (Convert.ToString(iteration_row["favisoblo"]).Trim() != "")
                        {
                            sprHuecos.BackColor = COLOR_CREMA;
                        }

                        sprHuecos.Col = 11;
                        sprHuecos.Text = Convert.ToDateTime(iteration_row["fhinicons"]).ToString("HH:mm") + "-" + Convert.ToDateTime(iteration_row["fhfincons"]).ToString("HH:mm");
                        if (Convert.ToString(iteration_row["favisoblo"]).Trim() != "")
                        {
                            sprHuecos.BackColor = COLOR_CREMA;
                        }

                        sprHuecos.Col = 12;
                        sprHuecos.Text = Convert.ToString(iteration_row["fgagendas"]);

                        sprHuecos.Col = 13;
                        sprHuecos.Text = Convert.ToString(iteration_row["fgbloques"]);

                        sprHuecos.Col = 14;
                        sprHuecos.Text = Convert.ToString(iteration_row["fndurpres"]);
                        sprHuecos.Col = 15;
                        if (!Convert.IsDBNull(iteration_row["fntobtres"]))
                        {
                            sprHuecos.Text = Convert.ToString(iteration_row["fntobtres"]);
                        }

                        sprHuecos.Col = 16;
                        sprHuecos.setTypeEditLen(300);
                        sprHuecos.Text = Convert.ToString(iteration_row["favisoblo"]).Trim().ToUpper();
                        sprHuecos.SetColHidden(sprHuecos.Col, true);

                        sprHuecos.Col = 17;
                        sprHuecos.setTypeEditLen(300);
                        sprHuecos.Text = Convert.ToString(iteration_row["fgsalacon"]).Trim().ToUpper();
                        sprHuecos.SetColHidden(sprHuecos.Col, true);

                    }



                }

            }

            this.Cursor = Cursors.Default;

            return result;
        }

        private void sprHuecos_CellDoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;

			if (Row > 0 && Modo == "SELECCION")
			{
				sprHuecos.Row = Row;
				cbAceptar_Click(cbAceptar, new EventArgs());
			}

		}


		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string Saturado = String.Empty;
			string sql = String.Empty;
			DataSet RR1 = null;

			this.Cursor = Cursors.WaitCursor;

			sprHuecos.Row = sprHuecos.getSelModeIndex();

			sprHuecos.Col = 1;
			string FechaHora = sprHuecos.Text;
			sprHuecos.Col = 2;
			FechaHora = FechaHora + " " + sprHuecos.Text;
			System.DateTime TempDate = DateTime.FromOADate(0);
			System.DateTime FechaDietario = DateTime.Parse((DateTime.TryParse(FechaHora, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : FechaHora);
			sprHuecos.Col = 3;
			int NumeroOrden = Convert.ToInt32(Double.Parse(sprHuecos.Text));
			sprHuecos.Col = 12;
			string Agenda = sprHuecos.Text;
			sprHuecos.Col = 13;
			int Bloque = Convert.ToInt32(Conversion.Val(sprHuecos.Text));
			sprHuecos.Col = 14;
			int DuracionPrestacion = Convert.ToInt32(Conversion.Val(sprHuecos.Text));
			sprHuecos.Col = 15;
			int TiempoResultados = Convert.ToInt32(Conversion.Val(sprHuecos.Text));
			sprHuecos.Col = 17;
			string SalaConsulta = sprHuecos.Text;

			if (ColorTranslator.ToOle(sprHuecos.ForeColor) == 0)
			{
				Saturado = "N";
			}
			else
			{
				Saturado = "S";
			}

			if (Convert.ToString(Formulario.Name) != "CCS830F1" && Convert.ToString(Formulario.Name) != "DetallePaciente" && Convert.ToString(Formulario.Name) != "CCI310F3")
			{
				System.DateTime tempRefParam = Convert.ToDateTime(Formulario.dtFechaCita);
				if (Restaurar_Hueco_Provisional(Conexion, 0, Convert.ToString(Formulario.dtFechaCita).Substring(11, Math.Min(5, Convert.ToString(Formulario.dtFechaCita).Length - 11)), Convert.ToInt32(Formulario.iNumeroOrden), tempRefParam))
				{
					Formulario.dtFechaCita = tempRefParam;
				}
				else
				{
					Formulario.dtFechaCita = tempRefParam;
				}
			}

			Formulario.RecogerHueco(Agenda, Bloque, FechaDietario, NumeroOrden, Saturado, DuracionPrestacion, TiempoResultados, null, null, null, SalaConsulta);

			//DIEGO - 29/12/2006 - AHORA RECOGEMOS EL HUECO PARA LA FECHA DE PRESCRIPCI�N
			//SER� LA PRIMERA FECHA NO SATURADA DE LAS QUE APARECEN LISTADAS EN EL GRID
			//Jes�s y �scar C. 31/01/2007 Esto solamente va a ser para cita r�pida y cita simple. En Sesiones ni solicudes pendientes ni reprogramaciones tiene que calcular los nuevos campos
			if (Convert.ToString(Formulario.Name) == "CCI310F4" || Convert.ToString(Formulario.Name) == "CCI310F2")
			{
				Formulario.RecogerHueco_FechaPrescripcion(Agenda, Bloque, ObtienePrimerHuecoNoSaturado().ToString("dd/MM/yyyy HH:mm"), NumeroOrden, Saturado, DuracionPrestacion, TiempoResultados);
			}
			this.Cursor = Cursors.Default;

			this.Close();

		}

		private System.DateTime ObtienePrimerHuecoNoSaturado()
		{


			sprHuecos.Col = 1;
			string FechaHora = sprHuecos.Text;
			sprHuecos.Col = 2;
			FechaHora = FechaHora + " " + sprHuecos.Text;
			System.DateTime TempDate = DateTime.FromOADate(0);
			System.DateTime FechaDietario = DateTime.Parse((DateTime.TryParse(FechaHora, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : FechaHora);


			FechaHora = "";

			int tempForVar = sprHuecos.MaxRows;
			for (int I = 1; I <= tempForVar; I++)
			{
				sprHuecos.Row = I;
				//if sprHuecos.ForeColor = &HFF00FF
				if (ColorTranslator.ToOle(sprHuecos.ForeColor) == 0)
				{
					sprHuecos.Col = 1;
					FechaHora = sprHuecos.Text;
					sprHuecos.Col = 2;
					FechaHora = FechaHora + " " + sprHuecos.Text;
					//FechaDietario = Format(FechaHora, "dd/mm/yyyy hh:mm")
					break;
				}
			}
			if (FechaHora == "")
			{
				FechaHora = DateTimeHelper.ToString(FechaDietario);
			}
			return DateTime.Parse(FechaHora);

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{

			Formulario.RecogerHueco(null, null, null, null, null, null, null);
			this.Close();

		}
		public bool Restaurar_Hueco_Provisional(SqlConnection Conexion, int Fila, string Hora, int Orden, System.DateTime FechaCita)
		{
			bool result = false;
			string StSql = String.Empty;
			DataSet rsp = null;
			DataSet Rsh = null;

			try
			{
				string tstMAquina = String.Empty;
				Conexion.Obtener_conexion tInstancia = null;

				tInstancia = new Conexion.Obtener_conexion();
				tstMAquina = tInstancia.NonMaquina();
				tInstancia = null;

                Conexion.BeginTransScope();

				// Se obtienen los huecos utilizados de forma provisional en la
				// fila recibida como parametro
				if (FechaCita.Equals(DateTime.FromOADate(0)))
				{ //viene de cita rapida, trae la fecha completa para solo borrar ese registro
					StSql = "SELECT * FROM chuecpro " + 
					        "WHERE gusuario = '" + stUsuario + "' AND " + 
					        "gnumregi = " + Fila.ToString() + " AND puestoPc = '" + tstMAquina + "'";

				}
				else
				{
					object tempRefParam = FechaCita;
					StSql = "SELECT * FROM chuecpro " + 
					        "WHERE gusuario = '" + stUsuario + "' AND " + 
					        "gnumregi = " + Fila.ToString() + " and " + 
					        "fhueccit = " + Serrores.FormatFechaHM(tempRefParam) + "";
					FechaCita = Convert.ToDateTime(tempRefParam);
				}
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
				rsp = new DataSet();
				tempAdapter.Fill(rsp);

				rsp.MoveFirst();

				foreach (DataRow iteration_row in rsp.Tables[0].Rows)
				{

					// Se accede al hueco utilizado
					object tempRefParam2 = iteration_row["fhueccit"];
					StSql = "SELECT * " + 
					        "FROM choraspu " + 
					        "WHERE gagendas = '" + Convert.ToString(iteration_row["gagendas"]) + "' AND " + 
					        "gbloques = " + Convert.ToString(iteration_row["gbloques"]) + " AND " + 
					        "fdietari = " + Serrores.FormatFechaHM(tempRefParam2) + "";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
					Rsh = new DataSet();
					tempAdapter_2.Fill(Rsh);

					Rsh.Edit();

					// Se suma el hueco utilizado al punto de citacion y al dietario

					Rsh.Tables[0].Rows[0]["npacnorm"] = Convert.ToDouble(Rsh.Tables[0].Rows[0]["npacnorm"]) + Convert.ToDouble(iteration_row["npacnorm"]);
					Rsh.Tables[0].Rows[0]["npacpref"] = Convert.ToDouble(Rsh.Tables[0].Rows[0]["npacpref"]) + Convert.ToDouble(iteration_row["npacpref"]);
					Rsh.Tables[0].Rows[0]["nminnorm"] = Convert.ToDouble(Rsh.Tables[0].Rows[0]["nminnorm"]) + Convert.ToDouble(iteration_row["nminnorm"]);
					Rsh.Tables[0].Rows[0]["nminpref"] = Convert.ToDouble(Rsh.Tables[0].Rows[0]["nminpref"]) + Convert.ToDouble(iteration_row["nminpref"]);
					if (Convert.ToDateTime(Rsh.Tables[0].Rows[0]["fdietari"]).ToString("HH:NN") == Hora && Convert.ToDouble(Rsh.Tables[0].Rows[0]["nordcita"]) == Orden)
					{
						Rsh.Tables[0].Rows[0]["nordcita"] = Convert.ToDouble(Rsh.Tables[0].Rows[0]["nordcita"]) - 1;
					}
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
					tempAdapter_2.Update(Rsh, Rsh.Tables[0].TableName);

					object tempRefParam3 = iteration_row["fhueccit"];
					StSql = "UPDATE cdietari " + 
					        "SET npacnorm = npacnorm + " + Convert.ToString(iteration_row["npacnorm"]) + ", " + 
					        "npacp= npacp+ " + Convert.ToString(iteration_row["npacpref"]) + ", " + 
					        "nminnorm = nminnorm + " + Convert.ToString(iteration_row["nminnorm"]) + ", " + 
					        "nminp= nminp+ " + Convert.ToString(iteration_row["nminpref"]) + " " + 
					        "WHERE gagendas = '" + Convert.ToString(iteration_row["gagendas"]) + "' AND " + 
					        "gbloques = " + Convert.ToString(iteration_row["gbloques"]) + " AND " + 
					        "fdietari = " + Serrores.FormatFecha(tempRefParam3) + "";
					SqlCommand tempCommand = new SqlCommand(StSql, Conexion);
					tempCommand.ExecuteNonQuery();

					rsp.Edit();

					rsp.Delete(tempAdapter);


				}

                Conexion.CommitTransScope();


				return true;
			}
			catch
			{

                Conexion.RollbackTransScope();
				short tempRefParam4 = 1860;
				string[] tempRefParam5 = new string[]{"no terminada o no"};
				ClaseMensaje.RespuestaMensaje(vNomAplicacion, vAyuda, tempRefParam4, Conexion, tempRefParam5);
				return result;
			}
		}

		public bool Restaurar_Hueco_Provisional(SqlConnection Conexion, int Fila, string Hora, int Orden)
		{
			System.DateTime tempRefParam = DateTime.FromOADate(0);
			return Restaurar_Hueco_Provisional(Conexion, Fila, Hora, Orden, tempRefParam);
		}


		//OSCAR C Octubre 2008
		private void sprHuecos_MouseMove(Object eventSender, MouseEventArgs eventArgs)
		{
			int Button = (int) eventArgs.Button;
			int Shift = (int) (Control.ModifierKeys & Keys.Shift);
			float X = (float) (eventArgs.X * 15);
			float Y = (float) (eventArgs.Y * 15);
			//Muestra la descripci�n del aviso AGENDA - BLOQUE de la fila donde este situado el rat�n
			//cuando dicho paciente tiene cambios en el tratamiento farmacologico

			int lFila = 0; // Fila sobre la que se encuentra el puntero
			int lCol = 0; // Columna sobre la que se encuentra el puntero

			sprHuecos.GetCellFromScreenCoord(ref lCol, ref lFila, X, Y); // � De que celda se trata ?
			if (lFila > 0 && lCol > 0)
			{
				sprHuecos.Row = lFila;
				//sprHuecos.Col = lCol
				//If (sprHuecos.BackColor = COLOR_CREMA) Then
				sprHuecos.Col = 16;
				if (sprHuecos.Text.Trim() != "")
				{
					ToolTipMain.SetToolTip(sprHuecos, sprHuecos.Text);
				}
				else
				{
					ToolTipMain.SetToolTip(sprHuecos, "");
				}
				sprHuecos.Row = sprHuecos.ActiveRowIndex;
			}
			else
			{
				ToolTipMain.SetToolTip(sprHuecos, "");
			}
		}
		//-------------
		private void CCI360F1_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}