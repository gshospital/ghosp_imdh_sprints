using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace FechasDisponibles
{
	public class clsFechasDisponibles
	{


		CCI360F1 frmFechasDisponibles = null;


		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, string PrestacionSolicitada, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, string Turno, int NSociedad, string stInspecc, string CodAgendaSolicitada, System.DateTime FechaPrescripcion)
		{
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			// Se inicializa y carga la pantalla de fechas disponibles

			Serrores.AjustarRelojCliente(Conexion);
			frmFechasDisponibles = new CCI360F1();
			frmFechasDisponibles.RecibirParametros(Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, Turno, NSociedad, stInspecc, CodAgendaSolicitada, FechaPrescripcion);
			frmFechasDisponibles.ShowDialog();

		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, string PrestacionSolicitada, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, string Turno, int NSociedad, string stInspecc, string CodAgendaSolicitada)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, Turno, NSociedad, stInspecc, CodAgendaSolicitada, DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, string PrestacionSolicitada, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, string Turno, int NSociedad, string stInspecc)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, Turno, NSociedad, stInspecc, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, string PrestacionSolicitada, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, string Turno, int NSociedad)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, Turno, NSociedad, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, string PrestacionSolicitada, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente, string Turno)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, Turno, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, string PrestacionSolicitada, string DPrestacionSolicitada, System.DateTime FechaSolicitada, string TipoPaciente)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada, DPrestacionSolicitada, FechaSolicitada, TipoPaciente, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, string PrestacionSolicitada, string DPrestacionSolicitada, System.DateTime FechaSolicitada)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada, DPrestacionSolicitada, FechaSolicitada, String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, string PrestacionSolicitada, string DPrestacionSolicitada)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada, DPrestacionSolicitada, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada, string PrestacionSolicitada)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, PrestacionSolicitada, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada, string DSalaSolicitada)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, DSalaSolicitada, String.Empty, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado, string SalaSolicitada)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, SalaSolicitada, String.Empty, String.Empty, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado, string NResponsableSolicitado)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, NResponsableSolicitado, String.Empty, String.Empty, String.Empty, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado, int ResponsableSolicitado)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, ResponsableSolicitado, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado, string DServicioSolicitado)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, DServicioSolicitado, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario, int ServicioSolicitado)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, ServicioSolicitado, String.Empty, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}

		public void Cargar(ref SqlConnection Conexion, object Formulario, string Modo, string usuario)
		{
			Cargar(ref Conexion, Formulario, Modo, usuario, 0, String.Empty, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, DateTime.FromOADate(0), String.Empty, String.Empty, 0, String.Empty, "", DateTime.FromOADate(0));
		}
	}
}