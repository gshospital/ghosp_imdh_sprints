using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace mantecamas
{
	partial class VISUALIZACION
	{

		#region "Upgrade Support "
		private static VISUALIZACION m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static VISUALIZACION DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new VISUALIZACION();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private void Ctx_mante_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
            System.Collections.Generic.List<Telerik.WinControls.RadItem> list = new System.Collections.Generic.List<Telerik.WinControls.RadItem>();
            Ctx_mante.Items.Clear();
            //We are moving the submenus from original menu to the context menu before displaying it
            foreach (Telerik.WinControls.RadItem item in mante.Items)
            {
                list.Add(item);
            }
            foreach (Telerik.WinControls.RadItem item in list)
            {
                Ctx_mante.Items.Add(item);
            }
            e.Cancel = false;
		}
		private void Ctx_mante_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
            System.Collections.Generic.List<Telerik.WinControls.RadItem> list = new System.Collections.Generic.List<Telerik.WinControls.RadItem>();
            //We are moving the submenus the context menu back to the original menu after displaying
            foreach (Telerik.WinControls.RadItem item in Ctx_mante.Items)
            {
                list.Add(item);
            }
            foreach (Telerik.WinControls.RadItem item in list)
            {
                mante.Items.Add(item);
            }
        }
		private string[] visualControls = new string[]{"components", "ToolTipMain", "nuevo", "Eliminar", "modificar", "ver", "Paciente", "mante", "MainMenu1", "CbCerrar", "Cbaceptar", "_Imcama_5", "IlIconos", "_Imcama_4", "_Imcama_3", "_Imcama_2", "_Imcama_1", "_Imcama_0", "LbBloqueadas", "LbInhabilitadas", "LbOcupadas", "LbLibres", "LbMultiples", "Lbotrarea", "_Imcama_6", "_Imcama_7", "_Imcama_8", "_Imcama_9", "_Imcama_10", "Label3", "Label4", "Label5", "Label6", "Label7", "Label1", "Label2", "Label8", "Label9", "Label10", "Label11", "Label12", "Label13", "_Imcama_11", "_Imcama_12", "_Imcama_13", "_Imcama_14", "_Imcama_15", "Frame5", "SPRCAMAS", "cmdReserva", "LbNombre", "LbApellido2", "LbApellido1", "Imseleccionada", "LbCama", "LbDcama", "Frame2", "LbCamas", "LbPacientes", "Frame3", "CmbUniEnfe", "FrmUniEnfe", "CmbServicio", "FrmServicio", "CmbCama", "FrmCama", "ChbCompartido", "ChbIndividual", "FrmAlojamiento", "chbLimpieza", "chbReservada", "ChbTodas", "ChbInhabilitada", "ChbBloqueada", "ChbOcupada", "ChbLibre", "chbConReserva", "ChbHombre", "ChbMujer", "FrmSexo", "Frame1", "SprPlantas", "TbRefresco", "LbHabitacion", "LbPlanta", "Frame4", "Imcama", "Ctx_mante", "SprPlantas_Sheet1", "SPRCAMAS_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadMenuItem nuevo;
		public Telerik.WinControls.UI.RadMenuItem Eliminar;
		public Telerik.WinControls.UI.RadMenuItem modificar;
		public Telerik.WinControls.UI.RadMenuItem ver;
		public Telerik.WinControls.UI.RadMenuItem Paciente;
		public Telerik.WinControls.UI.RadMenuItem mante;
		public Telerik.WinControls.UI.RadMenu MainMenu1;
		public Telerik.WinControls.UI.RadButton CbCerrar;
		public Telerik.WinControls.UI.RadButton Cbaceptar;
		private System.Windows.Forms.PictureBox _Imcama_5;
		public System.Windows.Forms.ImageList IlIconos;
		private System.Windows.Forms.PictureBox _Imcama_4;
		private System.Windows.Forms.PictureBox _Imcama_3;
		private System.Windows.Forms.PictureBox _Imcama_2;
		private System.Windows.Forms.PictureBox _Imcama_1;
		private System.Windows.Forms.PictureBox _Imcama_0;
		public Telerik.WinControls.UI.RadLabel LbBloqueadas;
		public Telerik.WinControls.UI.RadLabel LbInhabilitadas;
		public Telerik.WinControls.UI.RadLabel LbOcupadas;
		public Telerik.WinControls.UI.RadLabel LbLibres;
		public Telerik.WinControls.UI.RadLabel LbMultiples;
		public Telerik.WinControls.UI.RadLabel Lbotrarea;
		private System.Windows.Forms.PictureBox _Imcama_6;
		private System.Windows.Forms.PictureBox _Imcama_7;
		private System.Windows.Forms.PictureBox _Imcama_8;
		private System.Windows.Forms.PictureBox _Imcama_9;
		private System.Windows.Forms.PictureBox _Imcama_10;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadLabel Label9;
		public Telerik.WinControls.UI.RadLabel Label10;
		public Telerik.WinControls.UI.RadLabel Label11;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadLabel Label13;
		private System.Windows.Forms.PictureBox _Imcama_11;
		private System.Windows.Forms.PictureBox _Imcama_12;
		private System.Windows.Forms.PictureBox _Imcama_13;
		private System.Windows.Forms.PictureBox _Imcama_14;
		private System.Windows.Forms.PictureBox _Imcama_15;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		
        public Telerik.WinControls.UI.RadButton cmdReserva;
		public Telerik.WinControls.UI.RadTextBox LbNombre;
		public Telerik.WinControls.UI.RadTextBox LbApellido2;
		public Telerik.WinControls.UI.RadTextBox LbApellido1;
		public System.Windows.Forms.PictureBox Imseleccionada;
		public Telerik.WinControls.UI.RadLabel LbCama;
		public Telerik.WinControls.UI.RadLabel LbDcama;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadLabel LbCamas;
		public Telerik.WinControls.UI.RadLabel LbPacientes;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
        public Telerik.WinControls.UI.RadDropDownList CmbUniEnfe;
		public Telerik.WinControls.UI.RadGroupBox FrmUniEnfe;
		public Telerik.WinControls.UI.RadDropDownList CmbServicio;
		public Telerik.WinControls.UI.RadGroupBox FrmServicio;
		public Telerik.WinControls.UI.RadDropDownList CmbCama;
		public Telerik.WinControls.UI.RadGroupBox FrmCama;
		public Telerik.WinControls.UI.RadCheckBox ChbCompartido;
		public Telerik.WinControls.UI.RadCheckBox ChbIndividual;
		public Telerik.WinControls.UI.RadGroupBox FrmAlojamiento;
		public Telerik.WinControls.UI.RadRadioButton chbLimpieza;
		public Telerik.WinControls.UI.RadRadioButton chbReservada;
		public Telerik.WinControls.UI.RadRadioButton ChbTodas;
		public Telerik.WinControls.UI.RadRadioButton ChbInhabilitada;
		public Telerik.WinControls.UI.RadRadioButton ChbBloqueada;
		public Telerik.WinControls.UI.RadRadioButton ChbOcupada;
		public Telerik.WinControls.UI.RadRadioButton ChbLibre;
		public Telerik.WinControls.UI.RadGroupBox chbConReserva;
		public Telerik.WinControls.UI.RadCheckBox ChbHombre;
		public Telerik.WinControls.UI.RadCheckBox ChbMujer;
		public Telerik.WinControls.UI.RadGroupBox FrmSexo;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		
		public Telerik.WinControls.UI.RadButton TbRefresco;
		public Telerik.WinControls.UI.RadLabel LbHabitacion;
		public Telerik.WinControls.UI.RadLabel LbPlanta;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		public System.Windows.Forms.PictureBox[] Imcama = new System.Windows.Forms.PictureBox[16];
		public Telerik.WinControls.UI.RadContextMenu Ctx_mante;
        public UpgradeHelpers.Spread.FpSpread SPRCAMAS;
        public UpgradeHelpers.Spread.FpSpread SprPlantas;
        //private FarPoint.Win.Spread.SheetView SprPlantas_Sheet1 = null;
        //private FarPoint.Win.Spread.SheetView SPRCAMAS_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VISUALIZACION));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.SPRCAMAS = new UpgradeHelpers.Spread.FpSpread();
            this.SprPlantas = new UpgradeHelpers.Spread.FpSpread();
            this._Imcama_4 = new System.Windows.Forms.PictureBox();
            this._Imcama_3 = new System.Windows.Forms.PictureBox();
            this._Imcama_2 = new System.Windows.Forms.PictureBox();
            this._Imcama_1 = new System.Windows.Forms.PictureBox();
            this._Imcama_0 = new System.Windows.Forms.PictureBox();
            this._Imcama_6 = new System.Windows.Forms.PictureBox();
            this._Imcama_7 = new System.Windows.Forms.PictureBox();
            this._Imcama_8 = new System.Windows.Forms.PictureBox();
            this._Imcama_9 = new System.Windows.Forms.PictureBox();
            this._Imcama_10 = new System.Windows.Forms.PictureBox();
            this._Imcama_11 = new System.Windows.Forms.PictureBox();
            this._Imcama_12 = new System.Windows.Forms.PictureBox();
            this._Imcama_13 = new System.Windows.Forms.PictureBox();
            this._Imcama_14 = new System.Windows.Forms.PictureBox();
            this._Imcama_15 = new System.Windows.Forms.PictureBox();
            this.MainMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.mante = new Telerik.WinControls.UI.RadMenuItem();
            this.nuevo = new Telerik.WinControls.UI.RadMenuItem();
            this.Eliminar = new Telerik.WinControls.UI.RadMenuItem();
            this.modificar = new Telerik.WinControls.UI.RadMenuItem();
            this.ver = new Telerik.WinControls.UI.RadMenuItem();
            this.Paciente = new Telerik.WinControls.UI.RadMenuItem();
            this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
            this.CbCerrar = new Telerik.WinControls.UI.RadButton();
            this.Cbaceptar = new Telerik.WinControls.UI.RadButton();
            this._Imcama_5 = new System.Windows.Forms.PictureBox();
            this.LbBloqueadas = new Telerik.WinControls.UI.RadLabel();
            this.LbInhabilitadas = new Telerik.WinControls.UI.RadLabel();
            this.LbOcupadas = new Telerik.WinControls.UI.RadLabel();
            this.LbLibres = new Telerik.WinControls.UI.RadLabel();
            this.LbMultiples = new Telerik.WinControls.UI.RadLabel();
            this.Lbotrarea = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.Label7 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label8 = new Telerik.WinControls.UI.RadLabel();
            this.Label9 = new Telerik.WinControls.UI.RadLabel();
            this.Label10 = new Telerik.WinControls.UI.RadLabel();
            this.Label11 = new Telerik.WinControls.UI.RadLabel();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.Label13 = new Telerik.WinControls.UI.RadLabel();
            this.IlIconos = new System.Windows.Forms.ImageList(this.components);
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.cmdReserva = new Telerik.WinControls.UI.RadButton();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.LbNombre = new Telerik.WinControls.UI.RadTextBox();
            this.LbApellido2 = new Telerik.WinControls.UI.RadTextBox();
            this.LbApellido1 = new Telerik.WinControls.UI.RadTextBox();
            this.Imseleccionada = new System.Windows.Forms.PictureBox();
            this.LbCama = new Telerik.WinControls.UI.RadLabel();
            this.LbDcama = new Telerik.WinControls.UI.RadLabel();
            this.LbCamas = new Telerik.WinControls.UI.RadLabel();
            this.LbPacientes = new Telerik.WinControls.UI.RadLabel();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.FrmUniEnfe = new Telerik.WinControls.UI.RadGroupBox();
            this.CmbUniEnfe = new Telerik.WinControls.UI.RadDropDownList();
            this.FrmServicio = new Telerik.WinControls.UI.RadGroupBox();
            this.CmbServicio = new Telerik.WinControls.UI.RadDropDownList();
            this.FrmCama = new Telerik.WinControls.UI.RadGroupBox();
            this.CmbCama = new Telerik.WinControls.UI.RadDropDownList();
            this.FrmAlojamiento = new Telerik.WinControls.UI.RadGroupBox();
            this.ChbCompartido = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbIndividual = new Telerik.WinControls.UI.RadCheckBox();
            this.chbConReserva = new Telerik.WinControls.UI.RadGroupBox();
            this.chbLimpieza = new Telerik.WinControls.UI.RadRadioButton();
            this.chbReservada = new Telerik.WinControls.UI.RadRadioButton();
            this.ChbTodas = new Telerik.WinControls.UI.RadRadioButton();
            this.ChbInhabilitada = new Telerik.WinControls.UI.RadRadioButton();
            this.ChbBloqueada = new Telerik.WinControls.UI.RadRadioButton();
            this.ChbOcupada = new Telerik.WinControls.UI.RadRadioButton();
            this.ChbLibre = new Telerik.WinControls.UI.RadRadioButton();
            this.FrmSexo = new Telerik.WinControls.UI.RadGroupBox();
            this.ChbHombre = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbMujer = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.TbRefresco = new Telerik.WinControls.UI.RadButton();
            this.LbHabitacion = new Telerik.WinControls.UI.RadLabel();
            this.LbPlanta = new Telerik.WinControls.UI.RadLabel();
            this.Ctx_mante = new Telerik.WinControls.UI.RadContextMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cbaceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbBloqueadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbInhabilitadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbOcupadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbLibres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbMultiples)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbotrarea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReserva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LbNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbApellido2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbApellido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Imseleccionada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDcama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCamas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrmUniEnfe)).BeginInit();
            this.FrmUniEnfe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbUniEnfe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmServicio)).BeginInit();
            this.FrmServicio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmCama)).BeginInit();
            this.FrmCama.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbCama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmAlojamiento)).BeginInit();
            this.FrmAlojamiento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChbCompartido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbIndividual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbConReserva)).BeginInit();
            this.chbConReserva.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbLimpieza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbReservada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbTodas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbInhabilitada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbBloqueada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbOcupada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbLibre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmSexo)).BeginInit();
            this.FrmSexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChbHombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbMujer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TbRefresco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbHabitacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPlanta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // _Imcama_4
            // 
            this._Imcama_4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_4.Location = new System.Drawing.Point(184, 11);
            this._Imcama_4.Name = "_Imcama_4";
            this._Imcama_4.Size = new System.Drawing.Size(38, 37);
            this._Imcama_4.TabIndex = 34;
            this._Imcama_4.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_4, "Varios estados de cama en la misma habitaci�n");
            this._Imcama_4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_4.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_3
            // 
            this._Imcama_3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_3.Location = new System.Drawing.Point(139, 11);
            this._Imcama_3.Name = "_Imcama_3";
            this._Imcama_3.Size = new System.Drawing.Size(38, 37);
            this._Imcama_3.TabIndex = 35;
            this._Imcama_3.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_3, "Camas inhabilitadas");
            this._Imcama_3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_3.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_2
            // 
            this._Imcama_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_2.Location = new System.Drawing.Point(95, 11);
            this._Imcama_2.Name = "_Imcama_2";
            this._Imcama_2.Size = new System.Drawing.Size(38, 37);
            this._Imcama_2.TabIndex = 36;
            this._Imcama_2.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_2, "Camas bloqueadas");
            this._Imcama_2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_2.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_1
            // 
            this._Imcama_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_1.Location = new System.Drawing.Point(50, 11);
            this._Imcama_1.Name = "_Imcama_1";
            this._Imcama_1.Size = new System.Drawing.Size(38, 37);
            this._Imcama_1.TabIndex = 37;
            this._Imcama_1.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_1, "Camas ocupadas");
            this._Imcama_1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_1.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_0
            // 
            this._Imcama_0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_0.Location = new System.Drawing.Point(6, 11);
            this._Imcama_0.Name = "_Imcama_0";
            this._Imcama_0.Size = new System.Drawing.Size(38, 37);
            this._Imcama_0.TabIndex = 38;
            this._Imcama_0.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_0, "Camas libres");
            this._Imcama_0.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_0.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_6
            // 
            this._Imcama_6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_6.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_6.Location = new System.Drawing.Point(494, 11);
            this._Imcama_6.Name = "_Imcama_6";
            this._Imcama_6.Size = new System.Drawing.Size(38, 37);
            this._Imcama_6.TabIndex = 52;
            this._Imcama_6.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_6, "Cunas libres");
            this._Imcama_6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_6.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_7
            // 
            this._Imcama_7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_7.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_7.Location = new System.Drawing.Point(541, 11);
            this._Imcama_7.Name = "_Imcama_7";
            this._Imcama_7.Size = new System.Drawing.Size(38, 37);
            this._Imcama_7.TabIndex = 53;
            this._Imcama_7.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_7, "Cunas ocupadas");
            this._Imcama_7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_7.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_8
            // 
            this._Imcama_8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_8.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_8.Location = new System.Drawing.Point(586, 11);
            this._Imcama_8.Name = "_Imcama_8";
            this._Imcama_8.Size = new System.Drawing.Size(38, 37);
            this._Imcama_8.TabIndex = 54;
            this._Imcama_8.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_8, "Cunas  bloqueadas");
            this._Imcama_8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_8.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_9
            // 
            this._Imcama_9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_9.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_9.Location = new System.Drawing.Point(634, 11);
            this._Imcama_9.Name = "_Imcama_9";
            this._Imcama_9.Size = new System.Drawing.Size(38, 37);
            this._Imcama_9.TabIndex = 55;
            this._Imcama_9.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_9, "Cunas inhabilitadas");
            this._Imcama_9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_9.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_10
            // 
            this._Imcama_10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_10.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_10.Location = new System.Drawing.Point(678, 11);
            this._Imcama_10.Name = "_Imcama_10";
            this._Imcama_10.Size = new System.Drawing.Size(38, 37);
            this._Imcama_10.TabIndex = 56;
            this._Imcama_10.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_10, "Varios estados de cuna en la misma habitaci�n");
            this._Imcama_10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_10.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_11
            // 
            this._Imcama_11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_11.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_11.Location = new System.Drawing.Point(272, 11);
            this._Imcama_11.Name = "_Imcama_11";
            this._Imcama_11.Size = new System.Drawing.Size(38, 37);
            this._Imcama_11.TabIndex = 57;
            this._Imcama_11.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_11, "Plazas de D�a Libres");
            this._Imcama_11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_11.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_12
            // 
            this._Imcama_12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_12.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_12.Location = new System.Drawing.Point(316, 11);
            this._Imcama_12.Name = "_Imcama_12";
            this._Imcama_12.Size = new System.Drawing.Size(38, 37);
            this._Imcama_12.TabIndex = 58;
            this._Imcama_12.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_12, "Plazas de D�a ocupadas");
            this._Imcama_12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_12.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_13
            // 
            this._Imcama_13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_13.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_13.Location = new System.Drawing.Point(360, 11);
            this._Imcama_13.Name = "_Imcama_13";
            this._Imcama_13.Size = new System.Drawing.Size(38, 37);
            this._Imcama_13.TabIndex = 59;
            this._Imcama_13.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_13, "Plazas de D�a  bloqueadas");
            this._Imcama_13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_13.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_14
            // 
            this._Imcama_14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_14.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_14.Location = new System.Drawing.Point(404, 11);
            this._Imcama_14.Name = "_Imcama_14";
            this._Imcama_14.Size = new System.Drawing.Size(38, 37);
            this._Imcama_14.TabIndex = 60;
            this._Imcama_14.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_14, "Plazas de D�a inhabilitadas");
            this._Imcama_14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_14.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // _Imcama_15
            // 
            this._Imcama_15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_15.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_15.Location = new System.Drawing.Point(448, 11);
            this._Imcama_15.Name = "_Imcama_15";
            this._Imcama_15.Size = new System.Drawing.Size(38, 37);
            this._Imcama_15.TabIndex = 61;
            this._Imcama_15.TabStop = false;
            this.ToolTipMain.SetToolTip(this._Imcama_15, "Varios estados de Plazas de d�a en la misma habitaci�n");
            this._Imcama_15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            this._Imcama_15.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imcama_GiveFeedback);
            // 
            // MainMenu1
            // 
            this.MainMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mante});
            this.MainMenu1.Location = new System.Drawing.Point(0, 0);
            this.MainMenu1.Name = "MainMenu1";
            this.MainMenu1.Size = new System.Drawing.Size(794, 20);
            this.MainMenu1.TabIndex = 0;
            // 
            // mante
            // 
            this.mante.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nuevo,
            this.Eliminar,
            this.modificar,
            this.ver,
            this.Paciente});
            this.mante.Name = "mante";
            this.mante.Text = "Mantenimiento";
            this.mante.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // nuevo
            // 
            this.nuevo.Name = "nuevo";
            this.nuevo.Text = "&Nuevo";
            this.nuevo.Click += new System.EventHandler(this.nuevo_Click);
            // 
            // Eliminar
            // 
            this.Eliminar.Name = "Eliminar";
            this.Eliminar.Text = "&Eliminar cama";
            this.Eliminar.Click += new System.EventHandler(this.Eliminar_Click);
            this.Eliminar.ClickMode = Telerik.WinControls.ClickMode.Press;

            // 
            // modificar
            // 
            this.modificar.Name = "modificar";
            this.modificar.Text = "&Modificar cama";
            this.modificar.Click += new System.EventHandler(this.modificar_Click);
            this.modificar.ClickMode = Telerik.WinControls.ClickMode.Press;
            // 
            // ver
            // 
            this.ver.Name = "ver";
            this.ver.Text = "&Ver cama";
            this.ver.Click += new System.EventHandler(this.ver_Click);
            this.ver.ClickMode = Telerik.WinControls.ClickMode.Press;

            // 
            // Paciente
            // 
            this.Paciente.Name = "Paciente";
            this.Paciente.Text = "Ver &Paciente";
            this.Paciente.Click += new System.EventHandler(this.Paciente_Click);
            this.Paciente.ClickMode = Telerik.WinControls.ClickMode.Press;
            // 
            // Frame5
            // 
            this.Frame5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame5.Controls.Add(this.CbCerrar);
            this.Frame5.Controls.Add(this.Cbaceptar);
            this.Frame5.Controls.Add(this._Imcama_5);
            this.Frame5.Controls.Add(this._Imcama_4);
            this.Frame5.Controls.Add(this._Imcama_3);
            this.Frame5.Controls.Add(this._Imcama_2);
            this.Frame5.Controls.Add(this._Imcama_1);
            this.Frame5.Controls.Add(this._Imcama_0);
            this.Frame5.Controls.Add(this.LbBloqueadas);
            this.Frame5.Controls.Add(this.LbInhabilitadas);
            this.Frame5.Controls.Add(this.LbOcupadas);
            this.Frame5.Controls.Add(this.LbLibres);
            this.Frame5.Controls.Add(this.LbMultiples);
            this.Frame5.Controls.Add(this.Lbotrarea);
            this.Frame5.Controls.Add(this._Imcama_6);
            this.Frame5.Controls.Add(this._Imcama_7);
            this.Frame5.Controls.Add(this._Imcama_8);
            this.Frame5.Controls.Add(this._Imcama_9);
            this.Frame5.Controls.Add(this._Imcama_10);
            this.Frame5.Controls.Add(this.Label3);
            this.Frame5.Controls.Add(this.Label4);
            this.Frame5.Controls.Add(this.Label5);
            this.Frame5.Controls.Add(this.Label6);
            this.Frame5.Controls.Add(this.Label7);
            this.Frame5.Controls.Add(this.Label1);
            this.Frame5.Controls.Add(this.Label2);
            this.Frame5.Controls.Add(this.Label8);
            this.Frame5.Controls.Add(this.Label9);
            this.Frame5.Controls.Add(this.Label10);
            this.Frame5.Controls.Add(this.Label11);
            this.Frame5.Controls.Add(this.Label12);
            this.Frame5.Controls.Add(this.Label13);
            this.Frame5.Controls.Add(this._Imcama_11);
            this.Frame5.Controls.Add(this._Imcama_12);
            this.Frame5.Controls.Add(this._Imcama_13);
            this.Frame5.Controls.Add(this._Imcama_14);
            this.Frame5.Controls.Add(this._Imcama_15);
            this.Frame5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Frame5.HeaderText = "";
            this.Frame5.Location = new System.Drawing.Point(0, 519);
            this.Frame5.Name = "Frame5";
            this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame5.Size = new System.Drawing.Size(794, 85);
            this.Frame5.TabIndex = 30;
            // 
            // CbCerrar
            // 
            this.CbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbCerrar.Location = new System.Drawing.Point(722, 50);
            this.CbCerrar.Name = "CbCerrar";
            this.CbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbCerrar.Size = new System.Drawing.Size(63, 31);
            this.CbCerrar.TabIndex = 32;
            this.CbCerrar.Text = "C&errar";
            this.CbCerrar.Click += new System.EventHandler(this.CbCerrar_Click);
            // 
            // Cbaceptar
            // 
            this.Cbaceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.Cbaceptar.Location = new System.Drawing.Point(721, 10);
            this.Cbaceptar.Name = "Cbaceptar";
            this.Cbaceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Cbaceptar.Size = new System.Drawing.Size(63, 31);
            this.Cbaceptar.TabIndex = 31;
            this.Cbaceptar.Text = "&Aceptar";
            this.Cbaceptar.Click += new System.EventHandler(this.Cbaceptar_Click);
            // 
            // _Imcama_5
            // 
            this._Imcama_5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._Imcama_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._Imcama_5.Location = new System.Drawing.Point(228, 11);
            this._Imcama_5.Name = "_Imcama_5";
            this._Imcama_5.Size = new System.Drawing.Size(38, 37);
            this._Imcama_5.TabIndex = 33;
            this._Imcama_5.TabStop = false;
            this._Imcama_5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imcama_MouseDown);
            // 
            // LbBloqueadas
            // 
            this.LbBloqueadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbBloqueadas.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbBloqueadas.Location = new System.Drawing.Point(93, 51);
            this.LbBloqueadas.Name = "LbBloqueadas";
            this.LbBloqueadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbBloqueadas.Size = new System.Drawing.Size(45, 12);
            this.LbBloqueadas.TabIndex = 51;
            this.LbBloqueadas.Text = "Bloqueada";
            this.LbBloqueadas.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LbInhabilitadas
            // 
            this.LbInhabilitadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbInhabilitadas.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbInhabilitadas.Location = new System.Drawing.Point(137, 51);
            this.LbInhabilitadas.Name = "LbInhabilitadas";
            this.LbInhabilitadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbInhabilitadas.Size = new System.Drawing.Size(47, 12);
            this.LbInhabilitadas.TabIndex = 50;
            this.LbInhabilitadas.Text = "Inhabilitada";
            this.LbInhabilitadas.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LbOcupadas
            // 
            this.LbOcupadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbOcupadas.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbOcupadas.Location = new System.Drawing.Point(51, 51);
            this.LbOcupadas.Name = "LbOcupadas";
            this.LbOcupadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbOcupadas.Size = new System.Drawing.Size(39, 12);
            this.LbOcupadas.TabIndex = 49;
            this.LbOcupadas.Text = "Ocupada";
            this.LbOcupadas.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LbLibres
            // 
            this.LbLibres.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbLibres.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbLibres.Location = new System.Drawing.Point(14, 51);
            this.LbLibres.Name = "LbLibres";
            this.LbLibres.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbLibres.Size = new System.Drawing.Size(23, 12);
            this.LbLibres.TabIndex = 48;
            this.LbLibres.Text = "Libre";
            this.LbLibres.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LbMultiples
            // 
            this.LbMultiples.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbMultiples.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbMultiples.Location = new System.Drawing.Point(189, 51);
            this.LbMultiples.Name = "LbMultiples";
            this.LbMultiples.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbMultiples.Size = new System.Drawing.Size(29, 12);
            this.LbMultiples.TabIndex = 47;
            this.LbMultiples.Text = "Varios";
            this.LbMultiples.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Lbotrarea
            // 
            this.Lbotrarea.Cursor = System.Windows.Forms.Cursors.Default;
            this.Lbotrarea.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbotrarea.Location = new System.Drawing.Point(229, 51);
            this.Lbotrarea.Name = "Lbotrarea";
            this.Lbotrarea.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Lbotrarea.Size = new System.Drawing.Size(40, 12);
            this.Lbotrarea.TabIndex = 46;
            this.Lbotrarea.Text = "Otra �rea";
            this.Lbotrarea.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(685, 51);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(29, 12);
            this.Label3.TabIndex = 45;
            this.Label3.Text = "Varios";
            this.Label3.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(504, 51);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(23, 12);
            this.Label4.TabIndex = 44;
            this.Label4.Text = "Libre";
            this.Label4.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label5
            // 
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(543, 51);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(39, 12);
            this.Label5.TabIndex = 43;
            this.Label5.Text = "Ocupada";
            this.Label5.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(632, 51);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(47, 12);
            this.Label6.TabIndex = 42;
            this.Label6.Text = "Inhabilitada";
            this.Label6.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label7
            // 
            this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label7.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(584, 51);
            this.Label7.Name = "Label7";
            this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label7.Size = new System.Drawing.Size(45, 12);
            this.Label7.TabIndex = 41;
            this.Label7.Text = "Bloqueada";
            this.Label7.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label1
            // 
            this.Label1.AutoSize = false;
            this.Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.Label1.Location = new System.Drawing.Point(6, 66);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(221, 17);
            this.Label1.TabIndex = 40;
            this.Label1.Text = "C A M A S";
            this.Label1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label2
            // 
            this.Label2.AutoSize = false;
            this.Label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.Label2.Location = new System.Drawing.Point(496, 66);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(220, 17);
            this.Label2.TabIndex = 39;
            this.Label2.Text = "C U N A S";
            this.Label2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label8
            // 
            this.Label8.AutoSize = false;
            this.Label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.Label8.Location = new System.Drawing.Point(270, 66);
            this.Label8.Name = "Label8";
            this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label8.Size = new System.Drawing.Size(220, 17);
            this.Label8.TabIndex = 38;
            this.Label8.Text = "P L A Z A S   D I A";
            this.Label8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label9
            // 
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(358, 51);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(45, 12);
            this.Label9.TabIndex = 37;
            this.Label9.Text = "Bloqueada";
            this.Label9.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label10
            // 
            this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label10.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.Location = new System.Drawing.Point(402, 51);
            this.Label10.Name = "Label10";
            this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label10.Size = new System.Drawing.Size(47, 12);
            this.Label10.TabIndex = 36;
            this.Label10.Text = "Inhabilitada";
            this.Label10.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label11
            // 
            this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label11.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.Location = new System.Drawing.Point(318, 51);
            this.Label11.Name = "Label11";
            this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label11.Size = new System.Drawing.Size(39, 12);
            this.Label11.TabIndex = 35;
            this.Label11.Text = "Ocupada";
            this.Label11.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(281, 51);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(23, 12);
            this.Label12.TabIndex = 34;
            this.Label12.Text = "Libre";
            this.Label12.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label13
            // 
            this.Label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label13.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.Location = new System.Drawing.Point(455, 51);
            this.Label13.Name = "Label13";
            this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label13.Size = new System.Drawing.Size(29, 12);
            this.Label13.TabIndex = 33;
            this.Label13.Text = "Varios";
            this.Label13.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // IlIconos
            // 
            this.IlIconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IlIconos.ImageStream")));
            this.IlIconos.TransparentColor = System.Drawing.Color.Silver;
            this.IlIconos.Images.SetKeyName(0, "");
            this.IlIconos.Images.SetKeyName(1, "");
            this.IlIconos.Images.SetKeyName(2, "");
            this.IlIconos.Images.SetKeyName(3, "");
            this.IlIconos.Images.SetKeyName(4, "");
            this.IlIconos.Images.SetKeyName(5, "");
            this.IlIconos.Images.SetKeyName(6, "");
            this.IlIconos.Images.SetKeyName(7, "");
            this.IlIconos.Images.SetKeyName(8, "");
            this.IlIconos.Images.SetKeyName(9, "");
            this.IlIconos.Images.SetKeyName(10, "");
            this.IlIconos.Images.SetKeyName(11, "");
            this.IlIconos.Images.SetKeyName(12, "");
            this.IlIconos.Images.SetKeyName(13, "");
            this.IlIconos.Images.SetKeyName(14, "");
            this.IlIconos.Images.SetKeyName(15, "");
            this.IlIconos.Images.SetKeyName(16, "");
            this.IlIconos.Images.SetKeyName(17, "");
            this.IlIconos.Images.SetKeyName(18, "");
            this.IlIconos.Images.SetKeyName(19, "");
            this.IlIconos.Images.SetKeyName(20, "");
            this.IlIconos.Images.SetKeyName(21, "");
            this.IlIconos.Images.SetKeyName(22, "");
            this.IlIconos.Images.SetKeyName(23, "");
            this.IlIconos.Images.SetKeyName(24, "");
            this.IlIconos.Images.SetKeyName(25, "");
            this.IlIconos.Images.SetKeyName(26, "");
            this.IlIconos.Images.SetKeyName(27, "");
            this.IlIconos.Images.SetKeyName(28, "");
            this.IlIconos.Images.SetKeyName(29, "");
            this.IlIconos.Images.SetKeyName(30, "");
            this.IlIconos.Images.SetKeyName(31, "");
            this.IlIconos.Images.SetKeyName(32, "");
            this.IlIconos.Images.SetKeyName(33, "");
            this.IlIconos.Images.SetKeyName(34, "");
            this.IlIconos.Images.SetKeyName(35, "");
            this.IlIconos.Images.SetKeyName(36, "");
            this.IlIconos.Images.SetKeyName(37, "");
            this.IlIconos.Images.SetKeyName(38, "");
            this.IlIconos.Images.SetKeyName(39, "");
            this.IlIconos.Images.SetKeyName(40, "");
            this.IlIconos.Images.SetKeyName(41, "");
            this.IlIconos.Images.SetKeyName(42, "");
            this.IlIconos.Images.SetKeyName(43, "");
            this.IlIconos.Images.SetKeyName(44, "");
            this.IlIconos.Images.SetKeyName(45, "");
            this.IlIconos.Images.SetKeyName(46, "");
            this.IlIconos.Images.SetKeyName(47, "");
            this.IlIconos.Images.SetKeyName(48, "");
            this.IlIconos.Images.SetKeyName(49, "");
            this.IlIconos.Images.SetKeyName(50, "");
            this.IlIconos.Images.SetKeyName(51, "");
            this.IlIconos.Images.SetKeyName(52, "");
            this.IlIconos.Images.SetKeyName(53, "");
            this.IlIconos.Images.SetKeyName(54, "");
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.SPRCAMAS);
            this.Frame3.Controls.Add(this.cmdReserva);
            this.Frame3.Controls.Add(this.Frame2);
            this.Frame3.Controls.Add(this.LbCamas);
            this.Frame3.Controls.Add(this.LbPacientes);
            this.Frame3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Frame3.HeaderText = "";
            this.Frame3.Location = new System.Drawing.Point(0, 330);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(794, 189);
            this.Frame3.TabIndex = 21;
            // 
            // SPRCAMAS
            //
            this.SPRCAMAS.MasterTemplate.EnableSorting = false;
            this.SPRCAMAS.MasterTemplate.AllowColumnResize = false;
            this.SPRCAMAS.MasterTemplate.AllowRowResize = false;
            this.SPRCAMAS.MasterTemplate.AllowColumnReorder = false;
            this.SPRCAMAS.AllowCellContextMenu = true;
            this.SPRCAMAS.AllowColumnHeaderContextMenu = false;
            this.SPRCAMAS.AllowRowHeaderContextMenu = false;
            this.SPRCAMAS.ShowRowHeaderColumn = true;
            this.SPRCAMAS.Location = new System.Drawing.Point(40, 34);
            this.SPRCAMAS.Name = "SPRCAMAS";
            this.SPRCAMAS.Size = new System.Drawing.Size(442, 148);
            this.SPRCAMAS.TabIndex = 22;
            this.SPRCAMAS.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SPRCAMAS_CellClick);
            this.SPRCAMAS.DragDrop += new System.Windows.Forms.DragEventHandler(SPRCAMAS_DragDrop);
            this.SPRCAMAS.DragEnter += new System.Windows.Forms.DragEventHandler(SPRCAMAS_DragEnter);
            this.SPRCAMAS.DragOver += new System.Windows.Forms.DragEventHandler(SPRCAMAS_DragOver);                        
            this.SPRCAMAS.ViewCellFormatting += SprCamas_ViewCellFormatting;
            this.SPRCAMAS.CreateCell += new Telerik.WinControls.UI.GridViewCreateCellEventHandler(SPRCAMAS_CreateCell);
            this.SPRCAMAS.ContextMenuOpening += SPRCAMAS_ContextMenuOpening;
            this.SPRCAMAS.ReadOnly = true;
            this.SPRCAMAS.AllowDrop = true;
            this.SPRCAMAS.TableElement.RowHeaderColumnWidth = 50;
            this.SPRCAMAS.TableElement.RowHeight = 45;
            // 
            // cmdReserva
            // 
            this.cmdReserva.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdReserva.Location = new System.Drawing.Point(672, 158);
            this.cmdReserva.Name = "cmdReserva";
            this.cmdReserva.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdReserva.Size = new System.Drawing.Size(81, 25);
            this.cmdReserva.TabIndex = 58;
            this.cmdReserva.Text = "Re&serva";
            this.cmdReserva.Click += new System.EventHandler(this.cmdReserva_Click);
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.LbNombre);
            this.Frame2.Controls.Add(this.LbApellido2);
            this.Frame2.Controls.Add(this.LbApellido1);
            this.Frame2.Controls.Add(this.Imseleccionada);
            this.Frame2.Controls.Add(this.LbCama);
            this.Frame2.Controls.Add(this.LbDcama);
            this.Frame2.HeaderText = "";
            this.Frame2.Location = new System.Drawing.Point(640, 8);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(143, 148);
            this.Frame2.TabIndex = 52;
            // 
            // LbNombre
            // 
            this.LbNombre.AutoSize = false;
            this.LbNombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbNombre.Location = new System.Drawing.Point(4, 87);
            this.LbNombre.Name = "LbNombre";
            this.LbNombre.Enabled = false;
            this.LbNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbNombre.Size = new System.Drawing.Size(133, 16);
            this.LbNombre.TabIndex = 57;
            // 
            // LbApellido2
            // 
            this.LbApellido2.AutoSize = false;
            this.LbApellido2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbApellido2.Location = new System.Drawing.Point(4, 125);
            this.LbApellido2.Name = "LbApellido2";
            this.LbApellido2.Enabled = false;
            this.LbApellido2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbApellido2.Size = new System.Drawing.Size(133, 16);
            this.LbApellido2.TabIndex = 56;
            // 
            // LbApellido1
            // 
            this.LbApellido1.AutoSize = false;
            this.LbApellido1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbApellido1.Location = new System.Drawing.Point(4, 106);
            this.LbApellido1.Name = "LbApellido1";
            this.LbApellido1.Enabled = false;
            this.LbApellido1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbApellido1.Size = new System.Drawing.Size(133, 16);
            this.LbApellido1.TabIndex = 55;
            // 
            // Imseleccionada
            // 
            this.Imseleccionada.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Imseleccionada.Cursor = System.Windows.Forms.Cursors.Default;
            this.Imseleccionada.Location = new System.Drawing.Point(6, 44);
            this.Imseleccionada.Name = "Imseleccionada";
            this.Imseleccionada.Size = new System.Drawing.Size(43, 37);
            this.Imseleccionada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Imseleccionada.TabIndex = 58;
            this.Imseleccionada.TabStop = false;            
            this.Imseleccionada.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Imseleccionada_MouseDown);            
            this.Imseleccionada.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(Imseleccionada_GiveFeedback);
            // 
            // LbCama
            // 
            this.LbCama.AutoSize = false;
            this.LbCama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.LbCama.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbCama.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCama.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.LbCama.Location = new System.Drawing.Point(3, 9);
            this.LbCama.Name = "LbCama";
            this.LbCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbCama.Size = new System.Drawing.Size(135, 33);
            this.LbCama.TabIndex = 54;
            this.LbCama.Text = "CAMA SELECCIONADA";
            this.LbCama.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LbDcama
            // 
            this.LbDcama.AutoSize = false;
            this.LbDcama.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.LbDcama.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbDcama.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LbDcama.Location = new System.Drawing.Point(72, 44);
            this.LbDcama.Name = "LbDcama";
            this.LbDcama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbDcama.Size = new System.Drawing.Size(64, 26);
            this.LbDcama.TabIndex = 53;
            this.LbDcama.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LbCamas
            // 
            this.LbCamas.AutoSize = false;
            this.LbCamas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.LbCamas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbCamas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCamas.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.LbCamas.Location = new System.Drawing.Point(16, 57);
            this.LbCamas.Name = "LbCamas";
            this.LbCamas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbCamas.Size = new System.Drawing.Size(22, 124);
            this.LbCamas.TabIndex = 24;
            this.LbCamas.Text = "C A M A S";
            this.LbCamas.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LbPacientes
            // 
            this.LbPacientes.AutoSize = false;
            this.LbPacientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.LbPacientes.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbPacientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbPacientes.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.LbPacientes.Location = new System.Drawing.Point(99, 12);
            this.LbPacientes.Name = "LbPacientes";
            this.LbPacientes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbPacientes.Size = new System.Drawing.Size(382, 22);
            this.LbPacientes.TabIndex = 23;
            this.LbPacientes.Text = "PERSONAS DE LA HABITACION";
            this.LbPacientes.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.FrmUniEnfe);
            this.Frame1.Controls.Add(this.FrmServicio);
            this.Frame1.Controls.Add(this.FrmCama);
            this.Frame1.Controls.Add(this.FrmAlojamiento);
            this.Frame1.Controls.Add(this.chbConReserva);
            this.Frame1.Controls.Add(this.FrmSexo);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(0, 24);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(794, 93);
            this.Frame1.TabIndex = 0;
            // 
            // FrmUniEnfe
            // 
            this.FrmUniEnfe.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmUniEnfe.Controls.Add(this.CmbUniEnfe);
            this.FrmUniEnfe.Enabled = false;
            this.FrmUniEnfe.HeaderText = "Unidad de Enfermer�a";
            this.FrmUniEnfe.Location = new System.Drawing.Point(491, 48);
            this.FrmUniEnfe.Name = "FrmUniEnfe";
            this.FrmUniEnfe.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmUniEnfe.Size = new System.Drawing.Size(295, 40);
            this.FrmUniEnfe.TabIndex = 15;
            this.FrmUniEnfe.Text = "Unidad de Enfermer�a";
            // 
            // CmbUniEnfe
            // 
            this.CmbUniEnfe.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbUniEnfe.Location = new System.Drawing.Point(8, 16);
            this.CmbUniEnfe.Name = "CmbUniEnfe";
            this.CmbUniEnfe.Size = new System.Drawing.Size(283, 20);
            this.CmbUniEnfe.TabIndex = 18;
            this.CmbUniEnfe.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(CmbUniEnfe_ClickEvent);
            // 
            // FrmServicio
            // 
            this.FrmServicio.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmServicio.Controls.Add(this.CmbServicio);
            this.FrmServicio.Enabled = false;
            this.FrmServicio.HeaderText = "Servicio";
            this.FrmServicio.Location = new System.Drawing.Point(216, 48);
            this.FrmServicio.Name = "FrmServicio";
            this.FrmServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmServicio.Size = new System.Drawing.Size(275, 40);
            this.FrmServicio.TabIndex = 14;
            this.FrmServicio.Text = "Servicio";
            // 
            // CmbServicio
            // 
            this.CmbServicio.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbServicio.BackColor = System.Drawing.SystemColors.Window;
            this.CmbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.CmbServicio.ForeColor = System.Drawing.SystemColors.WindowText;
            this.CmbServicio.Location = new System.Drawing.Point(8, 16);
            this.CmbServicio.Name = "CmbServicio";
            this.CmbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CmbServicio.Size = new System.Drawing.Size(263, 20);
            this.CmbServicio.TabIndex = 17;
            this.CmbServicio.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CmbServicio_SelectionChangeCommitted);
            // 
            // FrmCama
            // 
            this.FrmCama.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmCama.Controls.Add(this.CmbCama);
            this.FrmCama.Enabled = false;
            this.FrmCama.HeaderText = "Tipo de Cama";
            this.FrmCama.Location = new System.Drawing.Point(490, 8);
            this.FrmCama.Name = "FrmCama";
            this.FrmCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmCama.Size = new System.Drawing.Size(295, 40);
            this.FrmCama.TabIndex = 13;
            this.FrmCama.Text = "Tipo de Cama";
            // 
            // CmbCama
            // 
            this.CmbCama.BackColor = System.Drawing.SystemColors.Window;
            this.CmbCama.Cursor = System.Windows.Forms.Cursors.Default;
            this.CmbCama.ForeColor = System.Drawing.SystemColors.WindowText;
            this.CmbCama.Location = new System.Drawing.Point(8, 16);
            this.CmbCama.Name = "CmbCama";
            this.CmbCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CmbCama.Size = new System.Drawing.Size(283, 20);
            this.CmbCama.TabIndex = 16;
            this.CmbCama.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbCama.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(CmbCama_SelectionChangeCommitted);
            // 
            // FrmAlojamiento
            // 
            this.FrmAlojamiento.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmAlojamiento.Controls.Add(this.ChbCompartido);
            this.FrmAlojamiento.Controls.Add(this.ChbIndividual);
            this.FrmAlojamiento.Enabled = false;
            this.FrmAlojamiento.HeaderText = "Alojamiento";
            this.FrmAlojamiento.Location = new System.Drawing.Point(329, 8);
            this.FrmAlojamiento.Name = "FrmAlojamiento";
            this.FrmAlojamiento.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmAlojamiento.Size = new System.Drawing.Size(155, 40);
            this.FrmAlojamiento.TabIndex = 5;
            this.FrmAlojamiento.Text = "Alojamiento";
            // 
            // ChbCompartido
            // 
            this.ChbCompartido.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbCompartido.Location = new System.Drawing.Point(73, 18);
            this.ChbCompartido.Name = "ChbCompartido";
            this.ChbCompartido.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbCompartido.Size = new System.Drawing.Size(80, 18);
            this.ChbCompartido.TabIndex = 7;
            this.ChbCompartido.Text = "Compartido";
            this.ChbCompartido.CheckStateChanged += new System.EventHandler(this.ChbCompartido_CheckStateChanged);
            // 
            // ChbIndividual
            // 
            this.ChbIndividual.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbIndividual.Location = new System.Drawing.Point(5, 18);
            this.ChbIndividual.Name = "ChbIndividual";
            this.ChbIndividual.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbIndividual.Size = new System.Drawing.Size(69, 18);
            this.ChbIndividual.TabIndex = 6;
            this.ChbIndividual.Text = "Individual";
            this.ChbIndividual.CheckStateChanged += new System.EventHandler(this.ChbIndividual_CheckStateChanged);
            // 
            // chbConReserva
            // 
            this.chbConReserva.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.chbConReserva.Controls.Add(this.chbLimpieza);
            this.chbConReserva.Controls.Add(this.chbReservada);
            this.chbConReserva.Controls.Add(this.ChbTodas);
            this.chbConReserva.Controls.Add(this.ChbInhabilitada);
            this.chbConReserva.Controls.Add(this.ChbBloqueada);
            this.chbConReserva.Controls.Add(this.ChbOcupada);
            this.chbConReserva.Controls.Add(this.ChbLibre);
            this.chbConReserva.HeaderText = "Estado de la cama";
            this.chbConReserva.Location = new System.Drawing.Point(9, 8);
            this.chbConReserva.Name = "chbConReserva";
            this.chbConReserva.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbConReserva.Size = new System.Drawing.Size(205, 80);
            this.chbConReserva.TabIndex = 4;
            this.chbConReserva.Text = "Estado de la cama";
            // 
            // chbLimpieza
            // 
            this.chbLimpieza.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbLimpieza.Location = new System.Drawing.Point(80, 58);
            this.chbLimpieza.Name = "chbLimpieza";
            this.chbLimpieza.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbLimpieza.Size = new System.Drawing.Size(64, 18);
            this.chbLimpieza.TabIndex = 20;
            this.chbLimpieza.Text = "Limpieza";
            this.chbLimpieza.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbLimpieza_CheckedChanged);
            // 
            // chbReservada
            // 
            this.chbReservada.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbReservada.Location = new System.Drawing.Point(4, 58);
            this.chbReservada.Name = "chbReservada";
            this.chbReservada.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbReservada.Size = new System.Drawing.Size(71, 18);
            this.chbReservada.TabIndex = 19;
            this.chbReservada.Text = "Reservada";
            this.chbReservada.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbReservada_CheckedChanged);
            // 
            // ChbTodas
            // 
            this.ChbTodas.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbTodas.Location = new System.Drawing.Point(4, 18);
            this.ChbTodas.Name = "ChbTodas";
            this.ChbTodas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbTodas.Size = new System.Drawing.Size(50, 18);
            this.ChbTodas.TabIndex = 12;
            this.ChbTodas.Text = "Todas";
            this.ChbTodas.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.ChbTodas_CheckedChanged);
            // 
            // ChbInhabilitada
            // 
            this.ChbInhabilitada.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbInhabilitada.Location = new System.Drawing.Point(80, 38);
            this.ChbInhabilitada.Name = "ChbInhabilitada";
            this.ChbInhabilitada.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbInhabilitada.Size = new System.Drawing.Size(79, 18);
            this.ChbInhabilitada.TabIndex = 11;
            this.ChbInhabilitada.Text = "Inhabilitada";
            this.ChbInhabilitada.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.ChbInhabilitada_CheckedChanged);
            // 
            // ChbBloqueada
            // 
            this.ChbBloqueada.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbBloqueada.Location = new System.Drawing.Point(4, 38);
            this.ChbBloqueada.Name = "ChbBloqueada";
            this.ChbBloqueada.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbBloqueada.Size = new System.Drawing.Size(73, 18);
            this.ChbBloqueada.TabIndex = 10;
            this.ChbBloqueada.Text = "Bloqueada";
            this.ChbBloqueada.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.ChbBloqueada_CheckedChanged);
            // 
            // ChbOcupada
            // 
            this.ChbOcupada.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbOcupada.Location = new System.Drawing.Point(135, 18);
            this.ChbOcupada.Name = "ChbOcupada";
            this.ChbOcupada.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbOcupada.Size = new System.Drawing.Size(65, 18);
            this.ChbOcupada.TabIndex = 9;
            this.ChbOcupada.Text = "Ocupada";
            this.ChbOcupada.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.ChbOcupada_CheckedChanged);
            // 
            // ChbLibre
            // 
            this.ChbLibre.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbLibre.Location = new System.Drawing.Point(80, 18);
            this.ChbLibre.Name = "ChbLibre";
            this.ChbLibre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbLibre.Size = new System.Drawing.Size(45, 18);
            this.ChbLibre.TabIndex = 8;
            this.ChbLibre.Text = "Libre";
            this.ChbLibre.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.ChbLibre_CheckedChanged);
            // 
            // FrmSexo
            // 
            this.FrmSexo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmSexo.Controls.Add(this.ChbHombre);
            this.FrmSexo.Controls.Add(this.ChbMujer);
            this.FrmSexo.Enabled = false;
            this.FrmSexo.HeaderText = "Sexo";
            this.FrmSexo.Location = new System.Drawing.Point(214, 8);
            this.FrmSexo.Name = "FrmSexo";
            this.FrmSexo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmSexo.Size = new System.Drawing.Size(115, 40);
            this.FrmSexo.TabIndex = 1;
            this.FrmSexo.Text = "Sexo";
            // 
            // ChbHombre
            // 
            this.ChbHombre.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbHombre.Location = new System.Drawing.Point(5, 18);
            this.ChbHombre.Name = "ChbHombre";
            this.ChbHombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbHombre.Size = new System.Drawing.Size(61, 18);
            this.ChbHombre.TabIndex = 3;
            this.ChbHombre.Text = "Hombre";
            this.ChbHombre.CheckStateChanged += new System.EventHandler(this.ChbHombre_CheckStateChanged);
            // 
            // ChbMujer
            // 
            this.ChbMujer.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbMujer.Location = new System.Drawing.Point(65, 18);
            this.ChbMujer.Name = "ChbMujer";
            this.ChbMujer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbMujer.Size = new System.Drawing.Size(49, 18);
            this.ChbMujer.TabIndex = 2;
            this.ChbMujer.Text = "Mujer";
            this.ChbMujer.CheckStateChanged += new System.EventHandler(this.ChbMujer_CheckStateChanged);
            // 
            // Frame4
            // 
            this.Frame4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame4.Controls.Add(this.SprPlantas);
            this.Frame4.Controls.Add(this.TbRefresco);
            this.Frame4.Controls.Add(this.LbHabitacion);
            this.Frame4.Controls.Add(this.LbPlanta);
            this.Frame4.HeaderText = "";
            this.Frame4.Location = new System.Drawing.Point(0, 117);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(794, 213);
            this.Frame4.TabIndex = 25;
            // 
            // SprPlantas
            //  
            this.SprPlantas.AllowDrop = true;
            this.SprPlantas.MasterTemplate.EnableSorting = false;
            this.SprPlantas.MasterTemplate.AllowColumnResize = false;
            this.SprPlantas.MasterTemplate.AllowRowResize = false;
            this.SprPlantas.MasterTemplate.AllowCellContextMenu = false;
            this.SprPlantas.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.SprPlantas.MasterTemplate.AllowRowHeaderContextMenu = false;
            this.SprPlantas.MasterTemplate.AllowColumnReorder = false;
            this.SprPlantas.MasterTemplate.ShowRowHeaderColumn = true;
            this.SprPlantas.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            this.SprPlantas.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.SprPlantas.Location = new System.Drawing.Point(33, 38);
            this.SprPlantas.Name = "SprPlantas";
            this.SprPlantas.Size = new System.Drawing.Size(747, 170);
            this.SprPlantas.TabIndex = 26;
            this.SprPlantas.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(SprPlantas_CellClick);
            this.SprPlantas.MouseDown += new System.Windows.Forms.MouseEventHandler(SprPlantas_MouseDown);
            this.SprPlantas.CreateCell += new Telerik.WinControls.UI.GridViewCreateCellEventHandler(SprPlantas_CreateCell);
            this.SprPlantas.ViewCellFormatting += SprPlantas_ViewCellFormatting;
            this.SprPlantas.DragDrop += new System.Windows.Forms.DragEventHandler(SprPlantas_DragDrop);
            this.SprPlantas.DragEnter += new System.Windows.Forms.DragEventHandler(SprPlantas_DragEnter);
            this.SprPlantas.DragOver += new System.Windows.Forms.DragEventHandler(SprPlantas_DragOver);
            this.SprPlantas.Leave += new System.EventHandler(SprPlantas_Leave);
            // 
            // TbRefresco
            // 
            this.TbRefresco.Cursor = System.Windows.Forms.Cursors.Default;
            this.TbRefresco.Location = new System.Drawing.Point(8, 12);
            this.TbRefresco.Name = "TbRefresco";
            this.TbRefresco.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TbRefresco.Size = new System.Drawing.Size(55, 21);
            this.TbRefresco.TabIndex = 28;
            this.TbRefresco.Text = "&Refresco";
            this.TbRefresco.Click += new System.EventHandler(this.TbRefresco_Click);
            // 
            // LbHabitacion
            // 
            this.LbHabitacion.AutoSize = false;
            this.LbHabitacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.LbHabitacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbHabitacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbHabitacion.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.LbHabitacion.Location = new System.Drawing.Point(67, 12);
            this.LbHabitacion.Name = "LbHabitacion";
            this.LbHabitacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbHabitacion.Size = new System.Drawing.Size(714, 22);
            this.LbHabitacion.TabIndex = 29;
            this.LbHabitacion.Text = "HABITACIONES";
            this.LbHabitacion.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LbPlanta
            // 
            this.LbPlanta.AutoSize = false;
            this.LbPlanta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.LbPlanta.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbPlanta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbPlanta.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.LbPlanta.Location = new System.Drawing.Point(10, 52);
            this.LbPlanta.Name = "LbPlanta";
            this.LbPlanta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbPlanta.Size = new System.Drawing.Size(22, 146);
            this.LbPlanta.TabIndex = 27;
            this.LbPlanta.Text = "P L A N T A S";
            this.LbPlanta.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Ctx_mante
            //             
            this.Ctx_mante.DropDownOpening += new System.ComponentModel.CancelEventHandler(this.Ctx_mante_Opening);
            this.Ctx_mante.DropDownClosing += new System.ComponentModel.CancelEventHandler(this.Ctx_mante_Closing);
            // 
            // VISUALIZACION
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 604);
            this.Controls.Add(this.MainMenu1);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.Frame4);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.Frame5);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VISUALIZACION";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DragEnter += VISUALIZACION_DragEnter;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Visualizaci�n general del estado de las camas - AGI310F1";

            //dgmorenogtopmost
            //this.TopMost = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.VISUALIZACION_Activated);
            this.Closed += new System.EventHandler(this.VISUALIZACION_Closed);
            this.Load += new System.EventHandler(this.VISUALIZACION_Load);
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cbaceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Imcama_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbBloqueadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbInhabilitadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbOcupadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbLibres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbMultiples)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbotrarea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdReserva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LbNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbApellido2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbApellido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Imseleccionada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDcama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCamas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FrmUniEnfe)).EndInit();
            this.FrmUniEnfe.ResumeLayout(false);
            this.FrmUniEnfe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbUniEnfe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmServicio)).EndInit();
            this.FrmServicio.ResumeLayout(false);
            this.FrmServicio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmCama)).EndInit();
            this.FrmCama.ResumeLayout(false);
            this.FrmCama.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbCama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmAlojamiento)).EndInit();
            this.FrmAlojamiento.ResumeLayout(false);
            this.FrmAlojamiento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChbCompartido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbIndividual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbConReserva)).EndInit();
            this.chbConReserva.ResumeLayout(false);
            this.chbConReserva.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbLimpieza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbReservada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbTodas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbInhabilitada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbBloqueada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbOcupada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbLibre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmSexo)).EndInit();
            this.FrmSexo.ResumeLayout(false);
            this.FrmSexo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChbHombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbMujer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TbRefresco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbHabitacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPlanta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
            this.AllowDrop = true;            
        }        

        void ReLoadForm(bool addEvents)
		{
			InitializeImcama();
		}
		void InitializeImcama()
		{
			this.Imcama = new System.Windows.Forms.PictureBox[16];
			this.Imcama[5] = _Imcama_5;
			this.Imcama[4] = _Imcama_4;
			this.Imcama[3] = _Imcama_3;
			this.Imcama[2] = _Imcama_2;
			this.Imcama[1] = _Imcama_1;
			this.Imcama[0] = _Imcama_0;
			this.Imcama[6] = _Imcama_6;
			this.Imcama[7] = _Imcama_7;
			this.Imcama[8] = _Imcama_8;
			this.Imcama[9] = _Imcama_9;
			this.Imcama[10] = _Imcama_10;
			this.Imcama[11] = _Imcama_11;
			this.Imcama[12] = _Imcama_12;
			this.Imcama[13] = _Imcama_13;
			this.Imcama[14] = _Imcama_14;
			this.Imcama[15] = _Imcama_15;
		}
		#endregion
	}
}