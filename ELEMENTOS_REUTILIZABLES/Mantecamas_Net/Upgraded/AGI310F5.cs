using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.Transactions;

namespace mantecamas
{
	public partial class SERVICIOMEDICOR
		: Telerik.WinControls.UI.RadForm
    {

		public SERVICIOMEDICOR()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            this.TopMost = true;
        }


		private void SERVICIOMEDICOR_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		string stGidenpac = String.Empty;
		int iAnoEpi = 0;
		int lNumEpi = 0;
		SqlConnection RcCambio = null;
		int iSerOri = 0;
		int lMedOri = 0;
		int iSerDes = 0;
		int lMedDes = 0;
		//oscar 27/11/2003
		int iPlanOr = 0;
		int iHabiOr = 0;
		string stCamaOr = String.Empty;
		bool ObligarTraslado = false;
		string ServicioOrigen = String.Empty;
		string ServicioDestino = String.Empty;
        //------


        private void proGrabarMovmiento()
        {
            string gunenfor = String.Empty;
            int gplantor = 0;
            int ghabitor = 0;
            string gcamaori = String.Empty;
            string Usu = String.Empty;
            System.DateTime fecha = DateTime.FromOADate(0);

            string vstfecha = DateTimeHelper.ToString(DateTime.Today);
            string vsthora = DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
            string vstfecha_hora_sys = vstfecha + " " + vsthora;
            //cierro el último movimiento de amovimie

            string tstG = "select * from amovimie where " + " ganoregi=" + iAnoEpi.ToString() + " and gnumregi=" + lNumEpi.ToString() + " and ffinmovi is null";

            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {
                //SqlTransaction transaction = RcCambio.BeginTrans();
                try
                {
                    if (RcCambio.State == ConnectionState.Open)
                        RcCambio.Close();

                    RcCambio.Open();

                    SqlCommand command = new SqlCommand(tstG, RcCambio);
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
                    DataSet trrGrabar = new DataSet();
                    tempAdapter.Fill(trrGrabar);
                    if (trrGrabar.Tables[0].Rows.Count != 0)
                    {
                        fecha = Convert.ToDateTime(trrGrabar.Tables[0].Rows[0]["fmovimie"]).AddSeconds(1);

                        //le sumo un segundo a la hora del movimiento para evitar problemas de clave
                        trrGrabar.Tables[0].Rows[0]["ffinmovi"] = fecha;
                        trrGrabar.Tables[0].Rows[0]["gunenfde"] = trrGrabar.Tables[0].Rows[0]["gunenfor"];
                        trrGrabar.Tables[0].Rows[0]["gservide"] = iSerDes;
                        trrGrabar.Tables[0].Rows[0]["gmedicde"] = lMedDes;
                        trrGrabar.Tables[0].Rows[0]["gplantde"] = trrGrabar.Tables[0].Rows[0]["gplantor"];
                        trrGrabar.Tables[0].Rows[0]["ghabitde"] = trrGrabar.Tables[0].Rows[0]["ghabitor"];
                        trrGrabar.Tables[0].Rows[0]["gcamades"] = trrGrabar.Tables[0].Rows[0]["gcamaori"];

                        string tempQuery = trrGrabar.Tables[0].TableName;
                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                        tempAdapter.Update(trrGrabar, trrGrabar.Tables[0].TableName);
                        //guardo esto en variables
                        gunenfor = Convert.ToString(trrGrabar.Tables[0].Rows[0]["gunenfor"]);
                        gplantor = Convert.ToInt32(trrGrabar.Tables[0].Rows[0]["gplantor"]);
                        ghabitor = Convert.ToInt32(trrGrabar.Tables[0].Rows[0]["ghabitor"]);
                        gcamaori = Convert.ToString(trrGrabar.Tables[0].Rows[0]["gcamaori"]);
                        Usu = Convert.ToString(trrGrabar.Tables[0].Rows[0]["gUsuario"]);
                    }
                    trrGrabar.AddNew();
                    trrGrabar.Tables[0].Rows[0]["ganoregi"] = iAnoEpi;
                    trrGrabar.Tables[0].Rows[0]["gnumregi"] = lNumEpi;
                    trrGrabar.Tables[0].Rows[0]["itiposer"] = "H";
                    trrGrabar.Tables[0].Rows[0]["fmovimie"] = fecha;
                    trrGrabar.Tables[0].Rows[0]["gunenfor"] = gunenfor;
                    trrGrabar.Tables[0].Rows[0]["gplantor"] = gplantor;
                    trrGrabar.Tables[0].Rows[0]["ghabitor"] = ghabitor;
                    trrGrabar.Tables[0].Rows[0]["gcamaori"] = gcamaori;
                    trrGrabar.Tables[0].Rows[0]["gservior"] = iSerDes;
                    trrGrabar.Tables[0].Rows[0]["gmedicor"] = lMedDes;
                    trrGrabar.Tables[0].Rows[0]["gUsuario"] = Module1.VstCodUsua;
                    trrGrabar.Tables[0].Rows[0]["fsistema"] = vstfecha_hora_sys;

                    SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(trrGrabar, trrGrabar.Tables[0].TableName);
                    trrGrabar.Close();
                    //actualizo aepisadm
                    tstG = "select * from aepisadm where " + " ganoadme=" + iAnoEpi.ToString() + " and gnumadme=" + lNumEpi.ToString();
                    command = new SqlCommand(tstG, RcCambio);
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(command);
                    trrGrabar = new DataSet();
                    tempAdapter_4.Fill(trrGrabar);
                    if (trrGrabar.Tables[0].Rows.Count != 0)
                    {
                        trrGrabar.Tables[0].Rows[0]["gserulti"] = iSerDes;
                        trrGrabar.Tables[0].Rows[0]["gperulti"] = lMedDes;

                        SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_4);
                        tempAdapter_4.Update(trrGrabar, trrGrabar.Tables[0].TableName);
                    }
                    trrGrabar.Close();

                    //OSCAR C
                    ActualizarDMOVFACT(iAnoEpi, lNumEpi, ref fecha, iSerDes, lMedDes);

                    //RcCambio.CommitTrans(transaction);
                    scope.Complete();
                    ObligarTraslado = false;

                    this.Close();
                }
                catch (Exception ex)
                {
                    //RcCambio.RollbackTrans(transaction);
                    RadMessageBox.Show(this, ex.Message);
                }
                finally
                {
                    //transaction.Dispose();                    
                }
            }
        }

		//oscar 27/11/2003
		//Public Sub Recibe_datos(Gidenpac As String, iAno As Integer, lNum As Long, Rc As rdoConnection)
		public void Recibe_datos(string Gidenpac, int iAno, int lNum, SqlConnection Rc, int pPlanOr, int PHabiOr, string PCamaOr, bool ObligarTras = false, string SerOrigen = "", string SerDestino = "")
		{

			iPlanOr = pPlanOr;
			iHabiOr = PHabiOr;
			stCamaOr = PCamaOr;
			//----

			stGidenpac = Gidenpac;
			iAnoEpi = iAno;
			lNumEpi = lNum;
			ObligarTraslado = ObligarTras;
			ServicioOrigen = SerOrigen;
			ServicioDestino = SerDestino;
			RcCambio = Rc;



		}

		private void Rellenar()
		{

			string tstPAc = String.Empty;

			string tstSql = "select * from dpacient where gidenpac='" + stGidenpac + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstSql, RcCambio);
			DataSet tRrR1 = new DataSet();
			tempAdapter.Fill(tRrR1);
			if (tRrR1.Tables[0].Rows.Count != 0)
			{
				tstPAc = Convert.ToString(tRrR1.Tables[0].Rows[0]["dape1pac"]).Trim();
				tstPAc = tstPAc + ((Convert.IsDBNull(tRrR1.Tables[0].Rows[0]["dape2pac"])) ? "" : " " + Convert.ToString(tRrR1.Tables[0].Rows[0]["dape2pac"]).Trim());
				tstPAc = tstPAc + ", " + Convert.ToString(tRrR1.Tables[0].Rows[0]["dnombpac"]).Trim();
			}
			tRrR1.Close();
			lbPaciente.Text = tstPAc.ToUpper();

			tstSql = "select gserulti,gperulti,dnomserv,dnompers,dap1pers,dap2pers " + " from AEPISADM inner join dservici on " + " aepisadm.gserulti=dservici.gservici" + " inner join dpersona on " + " aepisadm.gperulti=dpersona.gpersona " + " where gidenpac='" + stGidenpac + "'" + " AND GANOadme=" + iAnoEpi.ToString() + " AND Gnumadme=" + lNumEpi.ToString();

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstSql, RcCambio);
			tRrR1 = new DataSet();
			tempAdapter_2.Fill(tRrR1);

			if (tRrR1.Tables[0].Rows.Count != 0)
			{
				iSerOri = Convert.ToInt32(tRrR1.Tables[0].Rows[0]["gserulti"]);
				lMedOri = Convert.ToInt32(tRrR1.Tables[0].Rows[0]["gperulti"]);
				tstPAc = Convert.ToString(tRrR1.Tables[0].Rows[0]["dap1pers"]).Trim();
				tstPAc = tstPAc + ((Convert.IsDBNull(tRrR1.Tables[0].Rows[0]["dap2pers"])) ? "" : " " + Convert.ToString(tRrR1.Tables[0].Rows[0]["dap2pers"]).Trim());
				tstPAc = tstPAc + ", " + Convert.ToString(tRrR1.Tables[0].Rows[0]["dnompers"]).Trim();
				lbMedicor.Text = tstPAc;
				lbServior.Text = Convert.ToString(tRrR1.Tables[0].Rows[0]["dnomserv"]).Trim().ToUpper();
			}
			tRrR1.Close();


			//RELLENO EL COMBO DE SERVICIOS
			proServicio();

		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//valido que los combos tengan algo relleno
			if (cbbSerdestino.SelectedIndex != -1)
			{
				if (cbbMedDestino.SelectedIndex != -1)
				{
					proGrabarMovmiento();
				}
				else
				{
					RadMessageBox.Show(this, "Debe seleccionar un médico válido", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
					cbbMedDestino.Focus();
				}

			}
			else
			{
				RadMessageBox.Show("Debe seleccionar un servicio válido", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
				cbbSerdestino.Focus();
			}

            this.Dispose();
		}

		private void cbbMedDestino_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{

			if (cbbMedDestino.SelectedIndex != -1)
			{
                lMedDes = Convert.ToInt32(cbbMedDestino.SelectedValue);
			}

		}


		private void cbbSerdestino_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{
			string tstCom = String.Empty;
			DataSet tRrCom = null;
			try
			{
				//*********************
				if (cbbSerdestino.SelectedIndex != -1)
				{
					//    fServicio = True
					iSerDes = int.Parse(cbbSerdestino.SelectedValue.ToString());
					proMedicoA(iSerDes);

					if (!ValidacionEdadSexo(iSerDes))
					{
						RadMessageBox.Show(this, "El Servicio no es adecuado para el paciente", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
					}
					//    If fServicio = True And fMedico = False Then
					//        tstCom = "SELECT * " _
					//'        & " FROM DPERSONAva,DPERSERV " _
					//'        & " WHERE GSERVICI=" & CbbServicioA.ItemData(CbbServicioA.ListIndex) _
					//'        & " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA " _
					//'        & " and dpersonava.gpersona =" & viCodMedico
					//        Set tRrCom = RcAdmision.OpenResultset(tstCom, 1, 1)
					//        If tRrCom.RowCount = 0 Then    'DEBE CAMBIAR EL MÉDICO POR UNO QUE CORRESPONDA AL SERVICIO
					//            CbbMedicoA.SetFocus
					//            cbAceptar.Enabled = False
					//        Else
					//            cbAceptar.Enabled = True
					//        End If
					//    End If
				}
				else
				{
					//    fServicio = False
					cbAceptar.Enabled = false;
				}
			}
			catch
			{
				//*********************
			}
		}


		private bool ValidacionEdadSexo(int iCodigoServicio)
		{
			//Funcion que devuelve un true si el servicio esta acorde con el paciente
			bool result = false;
			string StSexo = String.Empty;
			int viEdad = 0;
			bool fEdad = false;

			string stFechaValidacion = DateTimeHelper.ToString(DateTime.Now);

			string sql = "select itipsexo,fnacipac,* from DPACIENT where gidenpac = '" + stGidenpac + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcCambio);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				StSexo = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["itipsexo"])) ? "I" : Convert.ToString(RrSql.Tables[0].Rows[0]["itipsexo"]);
				if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fnacipac"]))
				{
					fEdad = false;
				}
				else
				{
					viEdad = (int) DateAndTime.DateDiff("m", Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fnacipac"]), DateTime.Parse(stFechaValidacion), Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
					fEdad = true;
				}
			}
			else
			{

			}
			//buscamos la edad y sexo del servicio
			sql = "select * from DSERVICI ";
			sql = sql + "where gservici = " + iSerDes.ToString() + " ";
			sql = sql + "and (itipsexo = '" + StSexo + "' or itipsexo is null) ";
			sql = sql + "and (cedadmin <=" + viEdad.ToString() + " or cedadmin is null) ";
			sql = sql + "and (cedadmax >=" + viEdad.ToString() + " or cedadmax is null) ";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RcCambio);
			RrSql = new DataSet();
			tempAdapter_2.Fill(RrSql);
			result = RrSql.Tables[0].Rows.Count != 0;
			RrSql.Close();

			return result;
		}

		private void cmdSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			//If ObligarTraslado Then
			//    If cbbSerdestino.ListIndex <> -1 Then
			//        If cbbMedDestino.ListIndex <> -1 Then
			//'            proGrabarMovmiento
			//        Else
			//            MsgBox "Debe seleccionar un médico válido", vbInformation
			//            cbbMedDestino.SetFocus
			//        End If
			//
			//    Else
			//        MsgBox "Debe seleccionar un servicio válido", vbInformation
			//        cbbSerdestino.SetFocus
			//        Exit Sub
			//    End If
			//Else
			this.Close();
            this.Dispose();
			//End If

		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void SERVICIOMEDICOR_Load(Object eventSender, EventArgs eventArgs)
		{
			//If ObligarTraslado Then
			//    cmdSalir.Enabled = False
			//End If
			Rellenar();

		}

		private void proServicio()
		{


			int elemento = -1;

			//oscar 27/11/2003
			//Si la cama de origen es una plaza de dia (isillon='S') entonces solo se permite
			//cambiar a otro servicio de hospital de dia y que ademas coincida el indicador IMEDQUIR
			//del servicio origen con el indicador IMEDQUIR del servicio destino.
			//stSqServicio = "SELECT GSERVICI, DNOMSERV " _
			//& " FROM DSERVICIva " _
			//& " WHERE IHOSPITA='S' and " _
			//& " gservici<>" & iSerOri _
			//& "order by dnomserv"
			string sql = "select isillon from dtipocamva  " + 
			             " inner join dcamasbova on dcamasbova.gtipocam=dtipocamva.gtipocam" + 
			             " where gplantas=" + iPlanOr.ToString() + 
			             "   AND GHABITAC=" + iHabiOr.ToString() + 
			             "   AND GCAMASBO='" + stCamaOr + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcCambio);
			DataSet RrServicio = new DataSet();
			tempAdapter.Fill(RrServicio);
			string stSqServicio = "SELECT GSERVICI, DNOMSERV " + " FROM DSERVICIva " + " WHERE IHOSPITA='S' and " + " gservici<>" + iSerOri.ToString();
			if (RrServicio.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrServicio.Tables[0].Rows[0]["isillon"]).Trim().ToUpper() == "S")
				{
					stSqServicio = stSqServicio + " AND ICENTDIA='S'" + 
					               " AND IMEDQUIR=(SELECT IMEDQUIR FROM DSERVICI WHERE gservici=" + iSerOri.ToString() + ")";
				}
			}

			stSqServicio = stSqServicio + " order by dnomserv";
			//-----

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSqServicio, RcCambio);
			RrServicio = new DataSet();
			tempAdapter_2.Fill(RrServicio);
			cbbSerdestino.Items.Clear();
            foreach (DataRow iteration_row in RrServicio.Tables[0].Rows)
			{
				if (ObligarTraslado && ServicioDestino == "" && ServicioOrigen == Convert.ToString(iteration_row["GSERVICI"]))
				{
					//no grabo este servicio, tiene q elegir cualquier otro
				}
				else
				{
					cbbSerdestino.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["DNOMSERV"]), Convert.ToInt32(iteration_row["GSERVICI"])));
				}
				if (ObligarTraslado && ServicioDestino != "" && ServicioDestino == Convert.ToString(iteration_row["GSERVICI"]))
				{
					elemento = cbbSerdestino.Items.Count - 1;
				}

			}
			if (elemento != -1)
			{ //servicio de destino lo pongo en el combo y lo bloqueo para q no puedan cambiarlo
				cbbSerdestino.Enabled = false;
				cbbSerdestino.SelectedIndex = elemento;
			}

			RrServicio.Close();

		}


		private void proMedicoA(int iServicio)
		{
			string stSqServicio = String.Empty;
			DataSet RrServicio = null;

			//*********************
			try
			{
				//*****************
				cbbMedDestino.Items.Clear();
				stSqServicio = "SELECT DNOMPERS,DAP1PERS, DAP2PERS,DPERSONAva.GPERSONA " + " FROM DPERSONAva,DPERSERV " + " WHERE GSERVICI=" + iServicio.ToString() + " and DPERSONAva.GPERSONA=DPERSERV.GPERSONA ";

				//oscar 25/11/2003
				stSqServicio = stSqServicio + " AND DPERSONAVA.IMEDICO='S' ";
				//------

				stSqServicio = stSqServicio + " order by dap1pers,dap2pers,dnompers";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqServicio, RcCambio);
				RrServicio = new DataSet();
				tempAdapter.Fill(RrServicio);
				foreach (DataRow iteration_row in RrServicio.Tables[0].Rows)
				{
                    cbbMedDestino.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["DAP1PERS"]).Trim() + " " + (Convert.ToString(iteration_row["DAP2PERS"]) + "").Trim() + " " + Convert.ToString(iteration_row["DNOMPERS"]).Trim(), Convert.ToInt32(iteration_row["GPERSONA"])));
					if (cbbMedDestino.Items.Count == lMedOri)
					{
						cbbMedDestino.SelectedIndex = cbbMedDestino.Items.Count;
						cbbMedDestino.Text = cbbMedDestino.Items[cbbMedDestino.SelectedIndex].Text;

					}
				}
				RrServicio.Close();
				if (cbbMedDestino.Items.Count == 0 && !cbbSerdestino.Enabled)
				{
					cbbSerdestino.Enabled = true; //le dejo cambiar el servicio xq no puede elegir ningún responsable
				}
			}
			catch (Exception ex)
			{
				//******************
				Serrores.AnalizaError("Mantecamas", Path.GetDirectoryName(Application.ExecutablePath), Module1.VstCodUsua, "SERVICIOMEDICO:proMedicoA", ex);
			}
		}

		private void SERVICIOMEDICOR_Closed(Object eventSender, EventArgs eventArgs)
		{
			bool bError = false;

			if (ObligarTraslado)
			{
				//'    MsgBox "Debe seleccionar un servicio válido y pulsar aceptar", vbInformation
				//'    Cancel = 1
				//anular el traslado de cama y dejarlo como estaba al principio.
				bError = AnularTraslado();
			}

		}

		private bool AnularTraslado()
		{
			bool result = false;
			string StSexo = String.Empty;

			//iPlanOr = pPlanOr
			//iHabiOr = PHabiOr
			//stCamaOr = PCamaOr
			//'----
			//
			//stGidenpac = Gidenpac
			//iAnoEpi = iAno
			//lNumEpi = lNum
			//ObligarTraslado = ObligarTras
			//ServicioOrigen = SerOrigen
			//ServicioDestino = SerDestino
			int iPlanDe = 0;
			int iHabiDe = 0;
			string stCamaDe = String.Empty;
			string sqValde = String.Empty;
			DataSet Rrvalde = null;

			//cama destino
			string ERRORES = String.Empty;

			//***************Movimiento************
			string sqMov = String.Empty;
			DataSet RrMov = null;
			//**************************


                //***************************
                //*************************

                //sqMov = "SELECT *  FROM DCAMASBO  WHERE GPLANTAS=" & iPlanOr _
                //'    & " AND GHABITAC=" & iHabiOr & " AND GCAMASBO='" & stCamaOr & "'"
                //Set RrMov = RcCambio.OpenResultset(sqMov, rdOpenKeyset, rdConcurValues)
                //If RrMov("iestcama") <> "O" Then
                //    AnularTraslado = True
                //    IResume = oClass.RespuestaMensaje(vNomAplicacion, VAyuda, 1940, RcCambio, "realizar", "ya no existe el paciente en la cama origen")
                //    Exit Function
                //End If
                //RrMov.Close

                //sqMov = "select max(fmovimie) as Maxi  from  amovimie,aepisadm " _
                //'    & " where ganoregi=ganoadme and gnumregi = Gnumadme and " _
                //'    & " gidenpac='" & stGidenpac & "' and  ffinmovi is null "
                //
                //Set RrMov = RcCambio.OpenResultset(sqMov, 1, 1)
                //'''If RrMov("Maxi") > CDate(dtfechmovi.Text & " " & dtHoramovi.Text & ":" & DtSegundos) Then
                //'''    'MsgBox "Fecha operación anterior a último movimiento del paciente "
                //'''    IResume = oClass.RespuestaMensaje(vNomAplicacion, VAyuda, 1020, RcAdmision, "La fecha del movimiento", "mayor o igual", "la fecha del último movimiento")
                //'''    dtfechmovi.SetFocus
                //'''    proCamaDe = True
                //'''    Exit Function
                //'''End If
                //stfecha = RrMov("maxi")
                //RrMov.Close

                //************************************

                //************grabar
                //If cbbMotivtras.ListIndex = -1 Then
                //    'MsgBox "Debe seleccionar un motivo de traslado", vbInformation + vbOKOnly, "Traslados"
                //    IResume = oClass.RespuestaMensaje(vNomAplicacion, VAyuda, 1040, RcAdmision, LbMotivo.Caption)
                //    cbbMotivtras.SetFocus
                //    proCamaDe = True
                //    Exit Function
                //End If

                //AMOVIMIE1
                SqlTransaction transaction = RcCambio.BeginTrans();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                sqMov = "SELECT * FROM AMOVIMIE  where ganoregi=" + iAnoEpi.ToString() + " and gnumregi = " + lNumEpi.ToString() + "";

                SqlCommand command = new SqlCommand(sqMov, RcCambio, transaction);
                SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
                RrMov = new DataSet();
                tempAdapter.Fill(RrMov);

                int moveLast = RrMov.MoveLast(null);
                RrMov.Delete(tempAdapter, moveLast);
                moveLast = RrMov.MoveLast(null);

                iPlanDe = Convert.ToInt32(RrMov.Tables[0].Rows[moveLast]["gplantde"]);
                iHabiDe = Convert.ToInt32(RrMov.Tables[0].Rows[moveLast]["ghabitde"]);
                stCamaDe = Convert.ToString(RrMov.Tables[0].Rows[moveLast]["gcamades"]);
                //
                RrMov.Tables[0].Rows[moveLast]["ffinmovi"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["gunenfde"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["gservide"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["gmedicde"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["gplantde"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["ghabitde"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["gcamades"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["itipotra"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["gmottras"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["irotacio"] = DBNull.Value;
                RrMov.Tables[0].Rows[moveLast]["fpasfact"] = DBNull.Value;

                //string tempQuery = RrMov.Tables[0].TableName;
                //SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(RrMov, RrMov.Tables[0].TableName);
                RrMov.Close();


                sqValde = "SELECT gplantas,ghabitac,gcamasbo,ITIPSEXO,irestsex,iestcama,gidenpac,gcounien FROM DCAMASBO" + " WHERE GPLANTAS=" + iPlanOr.ToString() + " AND GHABITAC=" + iHabiOr.ToString() + " AND GCAMASBO='" + stCamaOr + "'";
                command = new SqlCommand(sqValde, RcCambio, transaction);
                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(command);
                Rrvalde = new DataSet();
                tempAdapter_3.Fill(Rrvalde);
                //If Rrvalde("iestcama") <> "L" Then
                //    If Rrvalde("iestcama") <> "B" Or Rrvalde("gidenpac") <> stGidenpac Then
                //        IResume = oClass.RespuestaMensaje(vNomAplicacion, VAyuda, 1940, RcCambio, "realizar", "ya esta ocupada la cama destino")
                //        Exit Function
                //    End If
                //End If


                //DCAMASBO1
                sqMov = "SELECT *  FROM DCAMASBO WHERE GPLANTAS=" + iPlanDe.ToString() + " AND GHABITAC=" + iHabiDe.ToString() + " AND GCAMASBO='" + stCamaDe + "'";
                command = new SqlCommand(sqMov, RcCambio, transaction);
                SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(command);
                RrMov = new DataSet();
                tempAdapter_4.Fill(RrMov);

                StSexo = Convert.ToString(RrMov.Tables[0].Rows[0]["itipsexo"]);

                RrMov.Tables[0].Rows[0]["iestcama"] = "L";
                RrMov.Tables[0].Rows[0]["gidenpac"] = DBNull.Value;
                RrMov.Tables[0].Rows[0]["itipsexo"] = DBNull.Value;

                //string tempQuery_2 = RrMov.Tables[0].TableName;
                //SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery_2, "");
                SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
                tempAdapter_4.Update(RrMov, RrMov.Tables[0].TableName);
                RrMov.Close();
                //'''
                //'''
                //'''
                //DCAMASBO2
                Rrvalde.Tables[0].Rows[0]["gidenpac"] = stGidenpac;
                Rrvalde.Tables[0].Rows[0]["iestcama"] = "O";
                Rrvalde.Tables[0].Rows[0]["itipsexo"] = StSexo;

                //string tempQuery_3 = Rrvalde.Tables[0].TableName;
                //SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(tempQuery_3, "");
                SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_3);
                tempAdapter_3.Update(Rrvalde, Rrvalde.Tables[0].TableName);

                RcCambio.CommitTrans(transaction);

                this.Cursor = Cursors.Default;

                return result;

            }
            catch (System.Exception excep)
            {
                //*****************error
                this.Cursor = Cursors.Default;
                RcCambio.RollbackTrans(transaction);
                ERRORES = excep.Message;
                RadMessageBox.Show(this, "Error en cancelación de traslado", Application.ProductName);
                return true;
            }
            finally
            {
                transaction.Dispose();
            }
		}

		//************************************************************************************
		//oscar C 05/10/05
		//************************************************************************************
		private object ActualizarDMOVFACT(int PAnoregi, int PNumregi, ref System.DateTime PFecha, int PServicio, int PMedico)
		{
			DataSet RrEditarAñadir = null;
			double Cantidad = 0;
			string stFarmaco = String.Empty;
			double nequival = 0;
			int i = 0;

			//En el caso de que existan movimientos en DMOVFACT con el campo FFECHFIN mayor que la fecha del apunte,
			//se actuara de diferente forma segun el campo GCODEVEN
			object tempRefParam = PFecha;
			object tempRefParam2 = PFecha;
			StringBuilder sql = new StringBuilder();
			sql.Append("SELECT * FROM DMOVFACT WHERE" + " itiposer = 'H' and " + " ganoregi=" + PAnoregi.ToString() + " and " + " gnumregi=" + PNumregi.ToString() + " and " + " (ffechfin >" + Serrores.FormatFechaHMS(tempRefParam) + " or ffinicio >" + Serrores.FormatFechaHMS(tempRefParam2) + ") ");
			PFecha = Convert.ToDateTime(tempRefParam2);
			PFecha = Convert.ToDateTime(tempRefParam);

            SqlCommand command = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
			SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
			DataSet Rr = new DataSet();
			tempAdapter.Fill(Rr);
			if (Rr.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in Rr.Tables[0].Rows)
				{

					//'            Case "AL", "AN", "DQ", "PR", "AY", "HM", "IN"
					//'
					//'              En estos casos no se hace nada para el cambio de servicio / médico (si
					//'              se hacía en el cambio de sociedad

					switch(Convert.ToString(iteration_row["GCODEVEN"]).Trim().ToUpper())
					{
						case "AU" : case "ES" : 
							 
							//En el caso que la fecha de inicio sea menor que la fecha del apunte, se modificaran las lineas correspondentes de 
							//DMOVFACT poniendo en FFECHFIN la fecha del apunte. Por cada linea modificada en DMOVFACT, se grabara una linea nueva 
							//con los campos GSERVICI, GPERSONA correspondientes, poniendo como FFINICIO la fecha 
							//del apunte y como FFECHFIN la fecha de fin de movimiento que tuviera antes de ser modificada. 
							 
							if (DateTime.Parse(Convert.ToDateTime(iteration_row["ffinicio"]).ToString("dd/MM/yyyy HH:mm:ss")) < DateTime.Parse(PFecha.ToString("dd/MM/yyyy HH:mm:ss")))
							{

								sql = new StringBuilder("SELECT * FROM DMOVFACT WHERE 2=1");
                                command = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
								SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(command);
								RrEditarAñadir = new DataSet();
								tempAdapter_2.Fill(RrEditarAñadir);
								RrEditarAñadir.AddNew();

								i = 0;
                                while(RrEditarAñadir.Tables[0].Columns.Count != i)
                                {
                                    //Debug.Print RrEditarAñadir.rdoColumns(i).Name
                                    switch(RrEditarAñadir.Tables[0].Columns[i].ColumnName.ToUpper())
                                    {
                                        case "FILA_ID" : 
                                            break;
                                        case "GSERREAL" : 
                                            RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = PServicio; 
                                            break;
                                        case "GPERSONA" : 
                                            RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = PMedico; 
                                            break;
                                        case "FFINICIO" : 
                                            RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = DateTime.Parse(PFecha.ToString("dd/MM/yyyy HH:mm:ss")); 
                                            break;
                                        default:
                                            if (!Convert.IsDBNull(iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName]))
                                            {
                                                RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName];
                                            }
                                            else
                                            {
                                                RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = DBNull.Value;
                                            } 
                                            break;
                                    }
                                    i++;
                                };
                                
								SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                                tempAdapter_2.Update(RrEditarAñadir, RrEditarAñadir.Tables[0].TableName);

								object tempRefParam3 = PFecha;
								sql = new StringBuilder("UPDATE DMOVFACT SET " + 
								      "FFECHFIN=" + Serrores.FormatFechaHMS(tempRefParam3) + 
								      " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
								PFecha = Convert.ToDateTime(tempRefParam3);
								SqlCommand tempCommand = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
								tempCommand.ExecuteNonQuery();

								RrEditarAñadir.Close();


							}
							else
							{

								sql = new StringBuilder("UPDATE DMOVFACT SET " + 
								      "GSERREAL = " + PServicio.ToString() + "," + 
								      "GPERSONA = " + PMedico.ToString());
								sql.Append(
								           " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
								SqlCommand tempCommand_2 = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
								tempCommand_2.ExecuteNonQuery();
								//''''''''''''''''

							} 
							 
							break;
						case "FA" : 
							 
							//En el caso que la fecha de inicio sea menor que la fecha del apunte, se realizara el calculo 
							//de farmacos administrados en el periodo. Una vez calculado, se actualizara el campo NCANTIDA de 
							//DMOVFACT y se generara otra linea para el periodo desde fecha de cambio hasta fecha fin con los 
							//mismos valores que el anterior, pero con los nuevos valores para GSOCIEDA, NAFILIAC y GINSPECC 
							//correspondientes a las tomas de dicho periodo. 
							 
							if (DateTime.Parse(Convert.ToDateTime(iteration_row["ffinicio"]).ToString("dd/MM/yyyy HH:mm:ss")) < DateTime.Parse(PFecha.ToString("dd/MM/yyyy HH:mm:ss")))
							{
								//********************* O.Frias (SQL-2005 ) *********************
								object tempRefParam4 = PFecha;
								object tempRefParam5 = PFecha;
								object tempRefParam6 = PFecha;
								sql = new StringBuilder("SELECT GFARMACO,SUM(CDOSISTO) CANTIDAD " + 
								      "FROM ETOMASFA E INNER JOIN DMOVFACT D ON " + 
								      "E.ITIPOSER = D.ITIPOSER AND " + 
								      "E.GANOREGI = D.GANOREGI AND " + 
								      "E.GNUMREGI = D.GNUMREGI AND " + 
								      "E.FPASFACT = D.FPERFACT AND " + 
								      "E.GFARMACO = D.GCONCFAC " + 
								      "WHERE GCODEVEN='FA' AND " + 
								      "D.FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]) + " AND " + 
								      "FFINICIO<=" + Serrores.FormatFechaHMS(tempRefParam4) + " AND " + 
								      "FFECHFIN>=" + Serrores.FormatFechaHMS(tempRefParam5) + " and " + 
								      "FTOMASFA>= FFINICIO and " + 
								      "FTOMASFA<" + Serrores.FormatFechaHMS(tempRefParam6) + " " + 
								      "GROUP BY GFARMACO " + 
								      "ORDER BY GFARMACO ");
								PFecha = Convert.ToDateTime(tempRefParam6);
								PFecha = Convert.ToDateTime(tempRefParam5);
								PFecha = Convert.ToDateTime(tempRefParam4);
                                //********************* O.Frias (SQL-2005 ) *********************
                                command = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
								SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(command);
								RrEditarAñadir = new DataSet();
								tempAdapter_4.Fill(RrEditarAñadir);
								if (RrEditarAñadir.Tables[0].Rows.Count != 0)
								{
									stFarmaco = Convert.ToString(RrEditarAñadir.Tables[0].Rows[0]["GFARMACO"]);
									string tempRefParam7 = Convert.ToString(RrEditarAñadir.Tables[0].Rows[0]["CANTIDAD"]);
									Cantidad = (Convert.IsDBNull(RrEditarAñadir.Tables[0].Rows[0]["CANTIDAD"])) ? 0 : NumeroConvertido(ref tempRefParam7);
									//RrEditarAñadir.Close

									sql = new StringBuilder("SELECT nequival FROM DCODFARMVA WHERE gfarmaco='" + stFarmaco + "'");
                                    command = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
									SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(command);
									RrEditarAñadir = new DataSet();
									tempAdapter_5.Fill(RrEditarAñadir);
									if (RrEditarAñadir.Tables[0].Rows.Count != 0)
									{
										string tempRefParam8 = Convert.ToString(RrEditarAñadir.Tables[0].Rows[0][0]);
										nequival = (Convert.IsDBNull(RrEditarAñadir.Tables[0].Rows[0][0])) ? 0 : NumeroConvertido(ref tempRefParam8);
									}
									//RrEditarAñadir.Close

									if (nequival == 0)
									{
										RadMessageBox.Show(this, "ERROR al obtener la Unidad de equivalencia de algún(os) de los Farmaco(s) implicados.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
										return null;
									}

									sql = new StringBuilder("SELECT * FROM DMOVFACT WHERE 2=1");
                                    command = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
									SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(command);
									RrEditarAñadir = new DataSet();
									tempAdapter_6.Fill(RrEditarAñadir);
									RrEditarAñadir.AddNew();

									i = 0;
                                    while (RrEditarAñadir.Tables[0].Columns.Count != i)
                                    {
                                        //Debug.Print RrEditarAñadir.rdoColumns(i).Name
                                        switch (RrEditarAñadir.Tables[0].Columns[i].ColumnName.ToUpper())
                                        {
                                            case "FILA_ID":
                                                break;
                                            case "GSERSOLI":
                                            case "GSERRESP":
                                                RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = PServicio;
                                                break;
                                            case "FFINICIO":
                                                RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = DateTime.Parse(PFecha.ToString("dd/MM/yyyy HH:mm:ss"));
                                                break;
                                            case "NCANTIDA":
                                                string tempRefParam10 = Convert.ToString(iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName]);
                                                string tempRefParam9 = (NumeroConvertido(ref tempRefParam10) - (Cantidad / nequival)).ToString();
                                                string tempRefParam11 = Serrores.ObtenerSeparadorDecimalBD(RcCambio);
                                                RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = Serrores.ConvertirDecimales(ref tempRefParam9, ref tempRefParam11, 2);
                                                break;
                                            default:
                                                if (!Convert.IsDBNull(iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName]))
                                                {
                                                    RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = iteration_row[RrEditarAñadir.Tables[0].Columns[i].ColumnName];
                                                }
                                                else
                                                {
                                                    RrEditarAñadir.Tables[0].Rows[0][RrEditarAñadir.Tables[0].Columns[i].ColumnName] = DBNull.Value;
                                                }
                                                break;
                                        }
                                        i++;
                                    };

									SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_6);
                                    tempAdapter_6.Update(RrEditarAñadir, RrEditarAñadir.Tables[0].TableName);

									string tempRefParam12 = (Cantidad / nequival).ToString();
									string tempRefParam13 = Serrores.ObtenerSeparadorDecimalBD(RcCambio);
									object tempRefParam14 = PFecha;
									sql = new StringBuilder("UPDATE DMOVFACT SET " + 
									      "NCANTIDA=" + Serrores.ConvertirDecimales(ref tempRefParam12, ref tempRefParam13, 2) + 
									      ",FFECHFIN=" + Serrores.FormatFechaHMS(tempRefParam14) + 
									      " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
									PFecha = Convert.ToDateTime(tempRefParam14);
									SqlCommand tempCommand_3 = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
									tempCommand_3.ExecuteNonQuery();

								}
								else
								{
									//se actualiza el servicio cuando hay tomas en el periodo entre la fecha de inicio del movimiento y la fecha de fin del movimiento
									//pero no hay tomas entre FFINICIO y Fecha de Cambio
									object tempRefParam15 = PFecha;
									sql = new StringBuilder("UPDATE DMOVFACT SET " + 
									      "GSERSOLI = " + PServicio.ToString() + "," + 
									      "GSERRESP = " + PServicio.ToString() + "," + 
									      "FFINICIO=" + Serrores.FormatFechaHMS(tempRefParam15));
									PFecha = Convert.ToDateTime(tempRefParam15);
									sql.Append(
									           " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
									SqlCommand tempCommand_4 = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
									tempCommand_4.ExecuteNonQuery();
								}
								RrEditarAñadir.Close();


							}
							else
							{

								//cuando la fecha de inicio sea mayor que la fecha de cambio,
								//se actualiza la sociedad.
								sql = new StringBuilder("UPDATE DMOVFACT SET " + 
								      "GSERSOLI = " + PServicio.ToString() + "," + 
								      "GSERRESP = " + PServicio.ToString());
								sql.Append(
								           " WHERE FILA_ID=" + Convert.ToString(iteration_row["FILA_ID"]));
								SqlCommand tempCommand_5 = new SqlCommand(sql.ToString(), RcCambio, RcCambio.getCurrentTransaction());
								tempCommand_5.ExecuteNonQuery();
								//''''''''''''''''

							} 
							 
							break;
						case "DI" : 
							// no se cambian 
							break;
					}
				}
			}
			Rr.Close();
			return null;
		}

		private double NumeroConvertido(ref string PNumero)
		{
			double result = 0;
			int CuantosDec = 0;
			int Constante = 0;

			if (PNumero == "")
			{
				return result;
			}

			int posdec = (PNumero.IndexOf('.') + 1);
			if (posdec == 0)
			{
				posdec = (PNumero.IndexOf(',') + 1);
			}
			if (posdec == 0)
			{
				//No hay decimales en el numero
				return Double.Parse(PNumero);
			}
			else
			{
				//Tratamiento de numero en dotacion decimal
				//Ej: 3,33 = (333 / 100)
				//     3.3 = (33  / 10)
				CuantosDec = PNumero.Substring(posdec).Length;
				Constante = Convert.ToInt32(Double.Parse("1" + new string('0', CuantosDec)));
				return Double.Parse(PNumero.Substring(0, Math.Min(posdec - 1, PNumero.Length)) + PNumero.Substring(posdec)) / Constante;
			}
			return result;
		}

        private void cbbSerdestino_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            this.cbbSerdestino_SelectionChangeCommitted(sender, e);
        }

        private void cbbMedDestino_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            this.cbbMedDestino_SelectionChangeCommitted(sender, e);
        }
        //******************************************************************************
    }
}