using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace mantecamas
{
	partial class MOTIVO_INHABILITACION
	{

		#region "Upgrade Support "
		private static MOTIVO_INHABILITACION m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static MOTIVO_INHABILITACION DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new MOTIVO_INHABILITACION();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbAceptar", "cmdSalir", "CbbInhabilita", "FrmInhabilita"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
        public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cmdSalir;
		public Telerik.WinControls.UI.RadDropDownList CbbInhabilita;
		public Telerik.WinControls.UI.RadGroupBox FrmInhabilita;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cmdSalir = new Telerik.WinControls.UI.RadButton();
            this.FrmInhabilita = new Telerik.WinControls.UI.RadGroupBox();
            this.CbbInhabilita = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmInhabilita)).BeginInit();
            this.FrmInhabilita.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbbInhabilita)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(135, 70);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 29);
            this.cbAceptar.TabIndex = 3;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cmdSalir
            // 
            this.cmdSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSalir.Location = new System.Drawing.Point(223, 70);
            this.cmdSalir.Name = "cmdSalir";
            this.cmdSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSalir.Size = new System.Drawing.Size(81, 29);
            this.cmdSalir.TabIndex = 2;
            this.cmdSalir.Text = "&Cancelar";
            this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            // 
            // FrmInhabilita
            // 
            this.FrmInhabilita.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmInhabilita.Controls.Add(this.CbbInhabilita);
            this.FrmInhabilita.HeaderText = "Motivo de inhabilitación";
            this.FrmInhabilita.Location = new System.Drawing.Point(6, 6);
            this.FrmInhabilita.Name = "FrmInhabilita";
            this.FrmInhabilita.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmInhabilita.Size = new System.Drawing.Size(298, 59);
            this.FrmInhabilita.TabIndex = 0;
            this.FrmInhabilita.Text = "Motivo de inhabilitación";
            // 
            // CbbInhabilita
            // 
            this.CbbInhabilita.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbbInhabilita.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CbbInhabilita.Location = new System.Drawing.Point(7, 22);
            this.CbbInhabilita.Name = "CbbInhabilita";
            this.CbbInhabilita.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbbInhabilita.Size = new System.Drawing.Size(281, 20);
            this.CbbInhabilita.TabIndex = 1;
            this.CbbInhabilita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbInhabilita_KeyPress);
            this.CbbInhabilita.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbInhabilita_SelectedIndexChanged);
            this.CbbInhabilita.TextChanged += new System.EventHandler(this.CbbInhabilita_TextChanged);
            // 
            // MOTIVO_INHABILITACION
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 103);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cmdSalir);
            this.Controls.Add(this.FrmInhabilita);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MOTIVO_INHABILITACION";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.Text = "Motivo de inhabilitación AGI310F2";
            this.Activated += new System.EventHandler(this.MOTIVO_INHABILITACION_Activated);
            this.Closed += new System.EventHandler(this.MOTIVO_INHABILITACION_Closed);
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmInhabilita)).EndInit();
            this.FrmInhabilita.ResumeLayout(false);
            this.FrmInhabilita.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbbInhabilita)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}