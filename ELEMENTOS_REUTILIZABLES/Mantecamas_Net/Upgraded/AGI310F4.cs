using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using System.Transactions;

namespace mantecamas
{
	public partial class DATOS_TRASLADO
         : Telerik.WinControls.UI.RadForm
    {

		string stTipo = String.Empty;
		string StCama1 = String.Empty;
		string stCama2 = String.Empty;
		bool ERRORTRAS = false;
		bool bSALIR = false;
		DataSet RrMottras = null;
		string sqMottras = String.Empty;
		string sqValde = String.Empty;
		DataSet Rrvalde = null;
		string sqValor = String.Empty;
		DataSet Rrvalor = null;
		bool swtipo = false;
		bool swmotivo = false;
		//
		string vstfecha = String.Empty;
		string vsthora = String.Empty;
		string vstfecha_hora_sys = String.Empty;
		//
		int sthorasystem = 0; // horas del sistema
		int stminutossystem = 0; //minutos del sistema
		int iPlanOr = 0, iPlanDe = 0;
		int iHabiOr = 0, iHabiDe = 0;
		string stCamaOr = String.Empty, stCamaDe = String.Empty;
		string DtSegundos = String.Empty;
		string vstGidenpac1 = String.Empty;
		string vstGidenpac2 = String.Empty;
		SqlConnection RcAdmision = null;
		VISUALIZACION Instancia = null;
		bool ObligarTrasladarCama = false;
		bool DejarTrasladarCama = false;
		bool PreguntarTrasladarCama = false;
		bool NegarTrasladarCama = false;
		string ServicioCamaOrigen = String.Empty;
		string ServicioCamaDestino = String.Empty;

		public DATOS_TRASLADO()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch (Exception ex)
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            this.TopMost = true;
        }

		public void fnrecogertag(string informacion, VISUALIZACION formulario, SqlConnection conexion)
		{
			this.Tag = informacion;
			RcAdmision = conexion;
			Instancia = formulario;
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			DialogResult iRespuesta =  0;
			int iAnoEpi = 0;
			int lNumEpi = 0;
			bool bError = false;
			this.Cursor = Cursors.WaitCursor;
			DtSegundos = DateTime.Now.ToString("ss");
			vstfecha = DateTimeHelper.ToString(DateTime.Today);
			vsthora = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm") + ":" + DateTime.Now.ToString("ss");
			vstfecha_hora_sys = vstfecha + " " + vsthora;
			
			string tstGidenpac = Convert.ToString(Rrvalor.Tables[0].Rows[0]["gidenpac"]);
			switch(stTipo)
			{
				case "T" : 
					this.Tag = ""; 
					bError = proCamaDe(ref iAnoEpi, ref lNumEpi); 
					if (!bError)
					{
						ObligarTrasladarCama = false;
						PreguntarTrasladarCama = false;
						NegarTrasladarCama = false;

						ObligarTrasladoCama(iPlanOr, iHabiOr, stCamaOr, iPlanDe, iHabiDe, stCamaDe);

                        try
                        {
                            if (PreguntarTrasladarCama)
                            {
                                using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                                {
                                    if (RcAdmision.State == ConnectionState.Open)
                                        RcAdmision.Close();

                                    ////RcAdmision.CommitTrans(transaction);
                                    //RcAdmision.CommitTransScope();
                                    iRespuesta = Telerik.WinControls.RadMessageBox.Show(this, "�Desea cambiar el servicio/m�dico del paciente?", Application.ProductName, MessageBoxButtons.YesNo, Telerik.WinControls.RadMessageIcon.Question);
                                    if (iRespuesta == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        SERVICIOMEDICOR.DefInstance.Recibe_datos(tstGidenpac, iAnoEpi, lNumEpi, RcAdmision, iPlanOr, iHabiOr, stCamaOr);
                                        //------
                                        SERVICIOMEDICOR.DefInstance.ShowDialog();
                                    }
                                    else if (ObligarTrasladarCama)
                                    {
                                     //   RcAdmision.CommitTransScope();
                                        SERVICIOMEDICOR.DefInstance.Recibe_datos(tstGidenpac, iAnoEpi, lNumEpi, RcAdmision, iPlanOr, iHabiOr, stCamaOr, ObligarTrasladarCama, ServicioCamaOrigen, ServicioCamaDestino);
                                        SERVICIOMEDICOR.DefInstance.ShowDialog();
                                    }
                                    else if (NegarTrasladarCama)
                                    {
                                        Telerik.WinControls.RadMessageBox.Show(this, "No puede hacer el traslado. Informar al administrador del sistema", Application.ProductName);
                                        //RcAdmision.RollbackTransScope();
                                    }
                                    scope.Complete();

                                }
                            }
                        }
                        catch(Exception Excep)
                        {
                            //RcAdmision.RollbackTrans(transaction);
                            //RcAdmision.RollbackTransScope();
                        }

					} 
					break;
				case "R" :
                    //SDSALAZAR
                    //Se saca las transacciones para que las ejecute desde el case ya que al realizar la conexion
                    //se esta generando un error al hacer el Complete
                    //RcAdmision.BeginTransScope();
                    using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
                    {
                        if (RcAdmision.State == ConnectionState.Open)
                            RcAdmision.Close();
                    

                        try
                        {
                            ERRORTRAS = false;
                            RcAdmision.Open();
                            proRotacion();
                            if (!ERRORTRAS)
                            {
                                proAcep_Rot();
                            }
                            scope.Complete();

                        }
                        catch (Exception excep)
                        {
                           // RcAdmision.RollbackTransScope();
                        }
                        break;
                    }
            }
			this.Cursor = Cursors.Default;
			if (bSALIR)
			{
				Instancia.Tag = "ACEPTAR";
				this.Close();
			}
		}

		private bool isInitializingComponent;
		private void cbbMotivtras_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (cbbMotivtras.Text == "")
			{
				cbAceptar.Enabled = false;
			}
		}

		private void cbbMotivtras_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void cmdSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void dtfechmovi_Leave(Object eventSender, EventArgs eventArgs)
		{
            
			if (DateTime.Parse(dtfechmovi.Text) > DateTime.Now)
			{				
				short tempRefParam = 1020;
                string[] tempRefParam2 = new string[] { "La fecha del movimiento no", "mayor", "la del sistema" };
				Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcAdmision, tempRefParam2);
				cbAceptar.Enabled = false;
				dtfechmovi.Text = DateTimeHelper.ToString(DateTime.Now);
				dtfechmovi.Focus();
			}
			else
			{
				activa_aceptar();
			}

		}

		private void dtHoramovi_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			//cbAceptar.Enabled = True
			//DtSegundos = Format(Now, "SS")
			sthorasystem = Convert.ToInt32(Double.Parse(DateTime.Now.ToString("HH")));
			stminutossystem = Convert.ToInt32(Double.Parse(DateTime.Now.ToString("mm")));
		}

		public void activa_aceptar()
		{
			if (swtipo && swmotivo && dtHoramovi.Text != "  :  ")
			{
				cbAceptar.Enabled = true;
			}
		}

		private void dtHoramovi_Enter(Object eventSender, EventArgs eventArgs)
		{
			dtHoramovi.SelectionStart = 0;
			dtHoramovi.SelectionLength = Strings.Len(dtHoramovi.Text);
		}

		private void dtHoramovi_Leave(Object eventSender, EventArgs eventArgs)
		{
			int sthora = 0;
			int stminutos = 0;
			string stfecha = String.Empty;
			//compruebo que es formato de hora valido
			//******************
			try
			{
				//***************

				int tiI = 0;
				if (dtHoramovi.Text != "  :  ")
				{
					tiI = (dtHoramovi.Text.IndexOf(':') + 1);
					if (Convert.ToInt32(Double.Parse(dtHoramovi.Text.Substring(0, Math.Min(tiI - 1, dtHoramovi.Text.Length)))) > 24 || Convert.ToInt32(Double.Parse(dtHoramovi.Text.Substring(tiI))) > 60)
					{						
						short tempRefParam = 1150;
                        string[] tempRefParam2 = new string[] { Label9.Text };
						Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcAdmision, tempRefParam2);
						cbAceptar.Enabled = false;
						dtHoramovi.Focus();
						return;
					}
					//cbAceptar.Enabled = True
					sthora = Convert.ToInt32(Double.Parse(DateTime.Parse(dtHoramovi.Text).ToString("HH")));
					stminutos = Convert.ToInt32(Double.Parse(DateTime.Parse(dtHoramovi.Text).ToString("mm")));
					stfecha = DateTime.Now.ToString("dd/MM/yyyy");
					if (stfecha == dtfechmovi.Text)
					{
						if (sthora == sthorasystem)
						{
							if (stminutos > stminutossystem)
							{
								//ERROR MINUTOS MAYOR QUE LOS DEL SYSTEMA								
								short tempRefParam3 = 1020;
                                string[] tempRefParam4 = new string[] { "La hora no", "mayor", "la del sistema" };
								Serrores.iresume =  Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RcAdmision, tempRefParam4));
								return;
							}
						}
						else
						{
							if (sthora > sthorasystem)
							{
								//ERROR HORA MAYOR QUE LA DEL SYSTEMA								
								short tempRefParam5 = 1020;
                                string[] tempRefParam6 = new string[] { "La hora no", "mayor", "la del sistema" };
								Serrores.iresume =  Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, RcAdmision, tempRefParam6));
								return;
							}
						}
					}
					dtHoramovi.Text = DateTime.Parse(dtHoramovi.Text).ToString("HH") + ":" + DateTime.Parse(dtHoramovi.Text).ToString("mm");
				}
				else
				{
					cbAceptar.Enabled = false;
				}
				DtSegundos = (DateTime.Now.ToString("ss"));
			}
			//**********
			catch (InvalidCastException e)
			{				
				short tempRefParam7 = 1150;
                string[] tempRefParam8 = new string[] { Label9.Text };
				Serrores.iresume =  Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, RcAdmision, tempRefParam8));
				dtHoramovi.Focus();
				return;
			}
		}

		private void DATOS_TRASLADO_Activated(Object eventSender, EventArgs eventArgs)
		{
            dtfechmovi.Text = Convert.ToString(DateTime.Now);

            if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				sqMottras = "SELECT * FROM DMOTTRASva";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqMottras, RcAdmision);
				RrMottras = new DataSet();
				tempAdapter.Fill(RrMottras);

                
				foreach (DataRow iteration_row in RrMottras.Tables[0].Rows)
				{
                    cbbMotivtras.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dmotitra"]).Trim(), Convert.ToString(iteration_row["gmottras"]).Trim())); 
                }

                dtfechmovi.MaxDate = DateTime.Today;
				dtHoramovi.Text = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm");
				DtSegundos = DateTime.Now.ToString("ss");

				// VARIABLE QUE CONTROLA LA SALIDA DEL FORMULARIO
				// EN CASO DE ERROR
				bSALIR = false;
				// variables que controlan la activacion
				// del boton aceptar
				swtipo = false;
				swmotivo = false;

				cbbMotivtras.Focus();
				cbAceptar.Enabled = false;
				string tstComp = "/";
				int tiTag = (Convert.ToString(this.Tag).IndexOf(tstComp, StringComparison.CurrentCultureIgnoreCase) + 1);
				int tiTag2 = Strings.InStr(tiTag + 1, Convert.ToString(this.Tag), tstComp, CompareMethod.Text);
				stTipo = Convert.ToString(this.Tag).Substring(0, Math.Min(tiTag - 1, Convert.ToString(this.Tag).Length));
				StCama1 = Convert.ToString(this.Tag).Substring(tiTag, Math.Min(tiTag2 - tiTag - 1, Convert.ToString(this.Tag).Length - tiTag));
				stCama2 = Convert.ToString(this.Tag).Substring(tiTag2);
				iPlanOr = Convert.ToInt32(Double.Parse(StCama1.Substring(0, Math.Min(2, StCama1.Length))));
				iHabiOr = Convert.ToInt32(Double.Parse(StCama1.Substring(2, Math.Min(2, StCama1.Length - 2))));
				stCamaOr = StCama1.Substring(4, Math.Min(2, StCama1.Length - 4));
                iPlanDe = Convert.ToInt32(Double.Parse(stCama2.Substring(0, Math.Min(2, stCama2.Length))));
                iHabiDe = Convert.ToInt32(Double.Parse(stCama2.Substring(2, Math.Min(2, stCama2.Length - 2))));
                stCamaDe = stCama2.Substring(4, Math.Min(2, stCama2.Length - 4));
                				
				proCamaOr();
				if (stTipo.ToUpper() == "R")
				{
					rbBloqueo.IsChecked = false;
					rbBloqueo.Enabled = false;
					rbNormal.IsChecked = true;
					rbNormal.Enabled = false;
				}
			}
		}

		public void proCamaOr()
		{
			//Cama origen
			sqValor = "SELECT ITIPSEXO,aepisadm.GIDENPAC,iestcama,iregaloj " + " FROM DCAMASBOVA,aepisadm " + " WHERE GPLANTAS=" + iPlanOr.ToString() + " AND GHABITAC=" + iHabiOr.ToString() + " AND GCAMASBO='" + stCamaOr + "'" + " and dcamasboVA.gidenpac=aepisadm.gidenpac";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqValor, RcAdmision);
			Rrvalor = new DataSet();
			tempAdapter.Fill(Rrvalor);
		}

		public bool proCamaDe(ref int iAnoEpi, ref int lNumEpi)
		{
			bool result = false;
			string aux = String.Empty;
			//cama destino
			int iGano = 0;
			int iGnum = 0;
			int iGser = 0;
			int iGmed = 0;
			string sGunenf = String.Empty;
			string ERRORES = String.Empty;
			//Dim SALIR As Boolean

			//***************Movimiento************
			string sqMov = String.Empty;
			DataSet RrMov = null;
			string stfecha = String.Empty;
			//**************************
			
			//***************************
			//*************************
			//SALIR = False

			sqMov = "SELECT * " + " FROM DCAMASBO " + " WHERE GPLANTAS=" + iPlanOr.ToString() + " AND GHABITAC=" + iHabiOr.ToString() + " AND GCAMASBO='" + stCamaOr + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqMov, RcAdmision);
			RrMov = new DataSet();
			tempAdapter.Fill(RrMov);
				
			if (Convert.ToString(RrMov.Tables[0].Rows[0]["iestcama"]) != "O")
			{
				result = true;
				bSALIR = true;
					
				short tempRefParam = 1940;
                string[] tempRefParam2 = new string[] { "realizar", "ya no existe el paciente en la cama origen" };
				Serrores.iresume =  Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcAdmision, tempRefParam2));
				return result;
			}
			RrMov.Close();
				
			sqMov = "select max(fmovimie) as Maxi " + " from  amovimie,aepisadm " + " where ganoregi=ganoadme and gnumregi = Gnumadme and " + " gidenpac='" + Convert.ToString(Rrvalor.Tables[0].Rows[0]["gidenpac"]) + "' and " + " ffinmovi is null ";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqMov, RcAdmision);
			RrMov = new DataSet();
			tempAdapter_2.Fill(RrMov);
				
			if (Convert.ToDateTime(RrMov.Tables[0].Rows[0]["Maxi"]) > DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos))
			{
				//MsgBox "Fecha operaci�n anterior a �ltimo movimiento del paciente "					
				short tempRefParam3 = 1020;
                string[] tempRefParam4 = new string[] { "La fecha del movimiento", "mayor o igual", "la fecha del �ltimo movimiento" };
				Serrores.iresume =  Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RcAdmision, tempRefParam4));
				dtfechmovi.Focus();
				return true;
			}
				
			stfecha = Convert.ToString(RrMov.Tables[0].Rows[0]["maxi"]); // & ":" & Format(RrMov("maxi"), "nnAM/PM")
			//stFecha = RrMov("MAXI")
			RrMov.Close();

			//************************************
			sqValde = "SELECT ITIPSEXO,irestsex,iestcama,gidenpac,gcounien,gplantas,ghabitac,gcamasbo FROM DCAMASBO" + " WHERE GPLANTAS=" + iPlanDe.ToString() + " AND GHABITAC=" + iHabiDe.ToString() + " AND GCAMASBO='" + stCamaDe + "'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqValde, RcAdmision);
			Rrvalde = new DataSet();
			tempAdapter_3.Fill(Rrvalde);
				
			if (Convert.ToString(Rrvalde.Tables[0].Rows[0]["iestcama"]) != "L")
			{					
				if (Convert.ToString(Rrvalde.Tables[0].Rows[0]["iestcama"]) != "B" || Rrvalde.Tables[0].Rows[0]["gidenpac"].ToString() != Rrvalor.Tables[0].Rows[0]["gidenpac"].ToString())
				{
					result = true;
					bSALIR = true;
						
					short tempRefParam5 = 1940;
                    string[] tempRefParam6 = new string[] { "realizar", "ya esta ocupada la cama destino" };
					Serrores.iresume =  Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, RcAdmision, tempRefParam6));
					return result;
				}
			}
			//***********unidad de enfermeria destino
				
			sGunenf = Convert.ToString(Rrvalde.Tables[0].Rows[0]["gcounien"]);

			//************grabar
			if (cbbMotivtras.SelectedIndex == -1)
			{
				//MsgBox "Debe seleccionar un motivo de traslado", vbInformation + vbOKOnly, "Traslados"					
				short tempRefParam7 = 1040;
                string[] tempRefParam8 = new string[] { LbMotivo.Text };
				Serrores.iresume =  Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, RcAdmision, tempRefParam8));
				cbbMotivtras.Focus();
				return true;
			}

            //AMOVIMIE1
            RcAdmision.BeginTransScope();

            if (RcAdmision.State == ConnectionState.Open)
                RcAdmision.Close();
            RcAdmision.Open();

            try
            {
                {
                    this.Cursor = Cursors.WaitCursor;
                    object tempRefParam9 = stfecha;
                    sqMov = "SELECT * FROM AEPISADM,AMOVIMIE " + " where ganoregi=ganoadme and gnumregi = Gnumadme and " + " gidenpac='" + Convert.ToString(Rrvalor.Tables[0].Rows[0]["gidenpac"]) + "' and " + " ffinmovi is null AND FMOVIMIE=" + Serrores.FormatFechaHMS(tempRefParam9) + "";
                    stfecha = Convert.ToString(tempRefParam9);
                    //var command = new SqlCommand(sqMov, RcAdmision, transaction);
                    var command = new SqlCommand(sqMov, RcAdmision);
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(command);
                    RrMov = new DataSet();
                    tempAdapter_4.Fill(RrMov);

                    iGano = Convert.ToInt32(RrMov.Tables[0].Rows[0]["ganoadme"]);

                    iGnum = Convert.ToInt32(RrMov.Tables[0].Rows[0]["gnumadme"]);

                    iGser = Convert.ToInt32(RrMov.Tables[0].Rows[0]["gservior"]);

                    iGmed = Convert.ToInt32(RrMov.Tables[0].Rows[0]["gmedicor"]);

                    iAnoEpi = iGano;
                    lNumEpi = iGnum;


                    RrMov.Tables[0].Rows[0]["ffinmovi"] = DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos);
                    //RrMov("ffinmovi") = CDate(dtfechmovi.Text & " " & dtHoramovi.Text & ":" & Format$(Now, "SS"))

                    RrMov.Tables[0].Rows[0]["gunenfde"] = sGunenf;

                    RrMov.Tables[0].Rows[0]["gservide"] = iGser;

                    RrMov.Tables[0].Rows[0]["gmedicde"] = iGmed;

                    RrMov.Tables[0].Rows[0]["gplantde"] = iPlanDe;

                    RrMov.Tables[0].Rows[0]["ghabitde"] = iHabiDe;

                    RrMov.Tables[0].Rows[0]["gcamades"] = stCamaDe;
                    if (rbNormal.IsChecked)
                    {
                        RrMov.Tables[0].Rows[0]["itipotra"] = "N";
                    }
                    else if (rbBloqueo.IsChecked)
                    {
                        RrMov.Tables[0].Rows[0]["itipotra"] = "B";
                    }

                    RrMov.Tables[0].Rows[0]["gmottras"] = cbbMotivtras.Items[cbbMotivtras.SelectedIndex].Value; //cbbMotivtras.GetItemData(cbbMotivtras.SelectedIndex);
                    string tempQuery = RrMov.Tables[0].TableName;

                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_4);
                    string sql2 = "UPDATE AMOVIMIE SET ffinmovi= " + "'" + DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos).ToString("yyyy-MM-dd HH:mm") + "'" + "," +
                                   "gunenfde =" + "'" + sGunenf + "'" + "," + "gservide =" + "'" + iGser + "'" + "," + "gmedicde=" + "'" + iGmed + "'" + "," + "gplantde=" + iPlanDe +  "," + 
                                   "ghabitde=" + iHabiDe + "," + "gcamades = " + "'" + stCamaDe + "'" + "," + "itipotra=" + "'" + RrMov.Tables[0].Rows[0]["itipotra"].ToString() + "'" + "," + "gmottras=" + cbbMotivtras.Items[cbbMotivtras.SelectedIndex].Value +
                                   "where ganoregi =" + Convert.ToInt32(RrMov.Tables[0].Rows[0]["ganoadme"].ToString()) + " And " + "gnumregi = " + iGnum + " And " + "ffinmovi is null AND FMOVIMIE = " + Serrores.FormatFechaHMS(tempRefParam9);


                    SqlCommand tempCommand = new SqlCommand(sql2, RcAdmision);
                    tempCommand.ExecuteNonQuery();
                    //tempAdapter_4.Update(RrMov, tempQuery);

                    RrMov.Close();
                    //AMOVIMIE2
                    sqMov = "SELECT * FROM AMOVIMIE " + " where 2=1";
                    //var command1 = new SqlCommand(sqMov, RcAdmision, transaction);
                    var command1 = new SqlCommand(sqMov, RcAdmision);
                    SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(command1);
                    RrMov = new DataSet();
                    tempAdapter_6.Fill(RrMov);

                    RrMov.AddNew();

                    RrMov.Tables[0].Rows[0]["fmovimie"] = DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos);
                    //RrMov("fmovimie") = CDate(dtfechmovi.Text & " " & dtHoramovi.Text & ":" & Format$(Now, "SS"))

                    RrMov.Tables[0].Rows[0]["ganoregi"] = iGano;

                    RrMov.Tables[0].Rows[0]["gnumregi"] = iGnum;

                    RrMov.Tables[0].Rows[0]["itiposer"] = "H";

                    RrMov.Tables[0].Rows[0]["gservior"] = iGser;

                    RrMov.Tables[0].Rows[0]["gmedicor"] = iGmed;

                    RrMov.Tables[0].Rows[0]["gplantor"] = iPlanDe;

                    RrMov.Tables[0].Rows[0]["ghabitor"] = iHabiDe;

                    RrMov.Tables[0].Rows[0]["gcamaori"] = stCamaDe;

                    RrMov.Tables[0].Rows[0]["gunenfor"] = sGunenf;

                    RrMov.Tables[0].Rows[0]["gusuario"] = Module1.VstCodUsua.ToUpper().Trim();

                    RrMov.Tables[0].Rows[0]["fsistema"] = vstfecha_hora_sys;
                    string tempQuery_2 = RrMov.Tables[0].TableName;

                    SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_6);
                    var tempcommand4 = new SqlCommand(sqMov, RcAdmision);
                    //var tempcommand4 = new SqlCommand(sqMov, RcAdmision, transaction);

                    //tempcommand4.ExecuteNonQuery();
                    tempAdapter_6.Update(RrMov, tempQuery_2);
                    RrMov.Close();

                    //DCAMASBO1
                    sqMov = "SELECT * " + " FROM DCAMASBO " + " WHERE GPLANTAS=" + iPlanOr.ToString() + " AND GHABITAC=" + iHabiOr.ToString() + " AND GCAMASBO='" + stCamaOr + "'";

                    var command2 = new SqlCommand(sqMov, RcAdmision);
                    SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(command2);
                    RrMov = new DataSet();
                    tempAdapter_8.Fill(RrMov);

                    //'''oscar 03/12/2003
                    //''Dim FECHALTA1 As String
                    //''FECHALTA1 = RrMov("FPREVALT") & ""
                    //'''------

                    if (rbNormal.IsChecked)
                    {
                        aux = mbAcceso.ObtenerCodigoSConsglo("INHABALT", "NNUMERI2", RcAdmision).Trim().ToUpper() + "";
                        if (aux != "")
                        {

                            RrMov.Tables[0].Rows[0]["iestcama"] = "I";

                            RrMov.Tables[0].Rows[0]["gidenpac"] = DBNull.Value;

                            RrMov.Tables[0].Rows[0]["gmotinha"] = Convert.ToInt32(Double.Parse(aux));
                        }
                        else
                        {
                            RrMov.Tables[0].Rows[0]["iestcama"] = "L";
                            RrMov.Tables[0].Rows[0]["gidenpac"] = DBNull.Value;
                            RrMov.Tables[0].Rows[0]["itipsexo"] = DBNull.Value;
                        }

                    }
                    else if (rbBloqueo.IsChecked)
                    {
                        RrMov.Tables[0].Rows[0]["iestcama"] = "B";
                        RrMov.Tables[0].Rows[0]["gidenpac"] = Rrvalor.Tables[0].Rows[0]["gidenpac"];
                    }

                    //'''oscar 03/12/2003
                    //''RrMov("FPREVALT") = Null
                    //'''-----------------

                    string tempQuery_3 = RrMov.Tables[0].TableName;

                    SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_8);
                    var tempcommand3 = new SqlCommand(sqMov, RcAdmision);
                    //tempcommand3.ExecuteNonQuery();
                    tempAdapter_8.Update(RrMov, tempQuery_3);

                    RrMov.Close();

                    //DCAMASBO2

                    Rrvalde.Tables[0].Rows[0]["gidenpac"] = Rrvalor.Tables[0].Rows[0]["gidenpac"];

                    Rrvalde.Tables[0].Rows[0]["iestcama"] = "O";

                    Rrvalde.Tables[0].Rows[0]["itipsexo"] = (Convert.IsDBNull(Rrvalor.Tables[0].Rows[0]["itipsexo"])) ? DBNull.Value : Rrvalor.Tables[0].Rows[0]["itipsexo"];

                    //'''oscar 03/12/2003
                    //''If FECHALTA1 = "" Then
                    //''    Rrvalde("FPREVALT") = Null
                    //''Else
                    //''    Rrvalde("FPREVALT") = FECHALTA1
                    //''End If
                    //'''------------

                    string tempQuery_4 = Rrvalde.Tables[0].TableName;

                    SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_3);
                    var tempcommand5 = new SqlCommand(sqMov, RcAdmision);
                    //tempcommand5.ExecuteNonQuery();
                    tempAdapter_3.Update(Rrvalde, tempQuery_4);
                    bSALIR = true;
                    this.Cursor = Cursors.Default;
                    //''RcAdmision.CommitTrans  hago el commit al preguntar por OBLITRAS

                    RcAdmision.CommitTransScope();
                  }
            }

            catch (System.Exception excep)
            {
                //*****************error
                RcAdmision.RollbackTransScope();
                this.Cursor = Cursors.Default;
                ERRORES = excep.Message;
                Telerik.WinControls.RadMessageBox.Show(this, "Error en traslado", Application.ProductName);
                return true;
            }

            return result;
			
			
		}



		private void DATOS_TRASLADO_Closed(Object eventSender, EventArgs eventArgs)
		{
			ErrorHandlingHelper.ResumeNext(
				() => {Rrvalde.Close();}, 
				() => {Rrvalor.Close();}, 
				() => {RrMottras.Close();});
		}

		public void proRotacion()
		{
			string sqRot1 = String.Empty;
			DataSet RrRot1 = null;
			DataSet RrRot11 = null;
			string sqRot2 = String.Empty;
			DataSet RrRot2 = null;
			DataSet RrRot22 = null;
            //*************cama primera**********
            /*SDSALAZAR SE COMENTA ESTE CODIGO TEMPORALMENTE PRUEBA TRANSACTIONSCOPE*/    
            //RcAdmision.BeginTransaction();

            //if (RcAdmision.State == ConnectionState.Open)
            //    RcAdmision.Close();
            //RcAdmision.Open();

            try
			{
				this.Cursor = Cursors.WaitCursor;
				sqRot1 = "select * from dcamasbova,AEPISADM " + " WHERE GPLANTAS=" + iPlanOr.ToString() + " AND GHABITAC=" + iHabiOr.ToString() + " AND GCAMASBO='" + stCamaOr + "'" + " AND AEPISADM.GIDENPAC=DCAMASBOva.GIDENPAC";
                //var command1 = new SqlCommand(sqRot1, RcAdmision, transaction);
                var command1 = new SqlCommand(sqRot1, RcAdmision);
                SqlDataAdapter tempAdapter = new SqlDataAdapter(command1);
                RrRot1 = new DataSet();
				tempAdapter.Fill(RrRot1);
				//***************cama segunda************
				sqRot2 = "select * from dcamasbova,AEPISADM " + " WHERE GPLANTAS=" + iPlanDe.ToString() + " AND GHABITAC=" + iHabiDe.ToString() + " AND GCAMASBO='" + stCamaDe + "'" + " AND AEPISADM.GIDENPAC=DCAMASBOva.GIDENPAC";
                //var command2 = new SqlCommand(sqRot2, RcAdmision, transaction);
                var command2 = new SqlCommand(sqRot2, RcAdmision);
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(command2);
                RrRot2 = new DataSet();
				tempAdapter_2.Fill(RrRot2);
				//**************cama primera*******************
				sqRot1 = "select * from dcamasbova " + " WHERE GPLANTAS=" + iPlanOr.ToString() + " AND GHABITAC=" + iHabiOr.ToString() + " AND IESTCAMA='O'";
                //var command3 = new SqlCommand(sqRot1, RcAdmision, transaction);
                var command3 = new SqlCommand(sqRot1, RcAdmision);
                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(command3);
                RrRot11 = new DataSet();
				tempAdapter_3.Fill(RrRot11);
				//*************cama segunda********
				sqRot2 = "select * from dcamasbova " + " WHERE GPLANTAS=" + iPlanDe.ToString() + " AND GHABITAC=" + iHabiDe.ToString() + " AND IESTCAMA='O'";
                //var command4 = new SqlCommand(sqRot2, RcAdmision, transaction);
                var command4 = new SqlCommand(sqRot2, RcAdmision);
                SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(command4);
                RrRot22 = new DataSet();
				tempAdapter_4.Fill(RrRot22);
				
				vstGidenpac1 = Convert.ToString(RrRot1.Tables[0].Rows[0]["gidenpac"]);
				
				vstGidenpac2 = Convert.ToString(RrRot2.Tables[0].Rows[0]["gidenpac"]);
				
				RrRot1.Close();
				
				RrRot11.Close();
				
				RrRot2.Close();
				
				RrRot22.Close();
				this.Cursor = Cursors.Default;

                /*SDSALAZAR SE COMENTA ESTE CODIGO TEMPORALMENTE PRUEBA TRANSACTIONSCOPE*/
                ////RcAdmision.CommitTransScope();
			}
			catch(Exception Excep)
			{
				this.Cursor = Cursors.Default;
                /*SDSALAZAR SE COMENTA ESTE CODIGO TEMPORALMENTE PRUEBA TRANSACTIONSCOPE*/
                ////RcAdmision.RollbackTransScope();

                Telerik.WinControls.RadMessageBox.Show(this, "Error en traslado", Application.ProductName);
				//Si no se inicializa a TRUE, despues seguira ejecutandose el procedimiento
				//proAcep_Rot con los consiguientes errores de ejecucion.
				ERRORTRAS = true;
			}
		}

		public void proAcep_Rot()
		{
            //***************Movimiento************

            //**************************
            //On Error GoTo Etiqueta
            //***************************
            /*SDSALAZAR SE COMENTA ESTE CODIGO TEMPORALMENTE PRUEBA TRANSACTIONSCOPE*/
            ////using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            ////{
            ////    if (RcAdmision.State == ConnectionState.Open)
            ////        RcAdmision.Close();
            ////    RcAdmision.Open();

            try
            {
                //************tablas amovimie*************
                //primer paciente
                this.Cursor = Cursors.WaitCursor;
                string sqMov = "select max(fmovimie) as Maxi " + " from  amovimie,aepisadm " + " where ganoregi=ganoadme and gnumregi = Gnumadme and " + " gidenpac='" + vstGidenpac1 + "' and " + " ffinmovi is null ";
                //var command1 = new SqlCommand(sqMov, RcAdmision, transaction);
                var command1 = new SqlCommand(sqMov, RcAdmision);
                SqlDataAdapter tempAdapter = new SqlDataAdapter(command1);
                DataSet RrMov = new DataSet();
                tempAdapter.Fill(RrMov);
                //primera cama
                //If RrMov("Maxi") > CDate(dtfechmovi.Text & " " & dtHoramovi.Text) Then

                if (Convert.ToDateTime(RrMov.Tables[0].Rows[0]["Maxi"]) > DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos))
                {
                    //MsgBox "Fecha operaci�n anterior a �ltimo movimiento del paciente "				
                    short tempRefParam = 1020;
                    string[] tempRefParam2 = new string[] { "La fecha del movimiento", "mayor o igual", "la fecha del �ltimo movimiento" };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcAdmision, tempRefParam2));
                    dtfechmovi.Focus();
                    return;
                }

                string tstFecha1 = Convert.ToString(RrMov.Tables[0].Rows[0]["maxi"]); //validaciones de fecha en amovimie // & ":" & Format(RrMov("maxi"), "nnAM/PM")

                RrMov.Close();
                //segundo paciente
                sqMov = "select max(fmovimie) as Maxi " + " from  amovimie,aepisadm " + " where ganoregi=ganoadme and gnumregi = Gnumadme and " + " gidenpac='" + vstGidenpac2 + "' and " + " ffinmovi is null ";
                //var command2 = new SqlCommand(sqMov, RcAdmision, transaction);
                var command2 = new SqlCommand(sqMov, RcAdmision);
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(command2);
                RrMov = new DataSet();
                tempAdapter_2.Fill(RrMov);

                if (Convert.ToDateTime(RrMov.Tables[0].Rows[0]["Maxi"]) > DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos))
                {
                    //MsgBox "Fecha operaci�n anterior a �ltimo movimiento del paciente "				
                    short tempRefParam3 = 1020;
                    string[] tempRefParam4 = new string[] { "La fecha del movimiento", "mayor o igual", "la fecha del �ltimo movimiento" };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RcAdmision, tempRefParam4));
                    dtfechmovi.Focus();
                    return;
                }

                string tstFecha2 = Convert.ToString(RrMov.Tables[0].Rows[0]["maxi"]);

                RrMov.Close();

                //************grabar
                if (cbbMotivtras.SelectedIndex == -1)
                {
                    //MsgBox "Debe seleccionar un motivo de traslado", vbInformation + vbOKOnly, "Traslados"				
                    short tempRefParam5 = 1040;
                    string[] tempRefParam6 = new string[] { LbMotivo.Text };
                    Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, RcAdmision, tempRefParam6));
                    cbbMotivtras.Focus();
                    return;
                }
                //AMOVIMIE1  primer paciente
                object tempRefParam7 = tstFecha1;
                sqMov = "SELECT * FROM AEPISADM,AMOVIMIE " + " where ganoregi=ganoadme and gnumregi = Gnumadme and " + " gidenpac='" + vstGidenpac1 + "' and " + " ffinmovi is null AND FMOVIMIE=" + Serrores.FormatFechaHMS(tempRefParam7) + "";
                tstFecha1 = Convert.ToString(tempRefParam7);
                //var command3 = new SqlCommand(sqMov, RcAdmision, transaction);
                var command3 = new SqlCommand(sqMov, RcAdmision);
                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(command3);
                RrMov = new DataSet();
                tempAdapter_3.Fill(RrMov);

                int tiGanopac1 = Convert.ToInt32(RrMov.Tables[0].Rows[0]["ganoadme"]); //meto en variables los valores del paciente

                int tiGnumpac1 = Convert.ToInt32(RrMov.Tables[0].Rows[0]["gnumadme"]);

                int tiGserpac1 = Convert.ToInt32(RrMov.Tables[0].Rows[0]["gservior"]);

                int tlGmedpac1 = Convert.ToInt32(RrMov.Tables[0].Rows[0]["gmedicor"]);

                string tstGunenf = Convert.ToString(RrMov.Tables[0].Rows[0]["gunenfor"]);
                string tstGunioripac1 = tstGunenf;

                //AMOVIMIE1  segundo paciente
                object tempRefParam8 = tstFecha2;
                string sqMovpac2 = "SELECT * FROM AEPISADM,AMOVIMIE " + " where ganoregi=ganoadme and gnumregi = Gnumadme and " + " gidenpac='" + vstGidenpac2 + "' and " + " ffinmovi is null AND FMOVIMIE=" + Serrores.FormatFechaHMS(tempRefParam8) + "";
                tstFecha2 = Convert.ToString(tempRefParam8);
                //var command4 = new SqlCommand(sqMovpac2, RcAdmision, transaction);
                var command4 = new SqlCommand(sqMovpac2, RcAdmision);
                SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(command4);
                DataSet RrMovpac2 = new DataSet();
                tempAdapter_4.Fill(RrMovpac2);
                //primera cama

                int tiGanopac2 = Convert.ToInt32(RrMovpac2.Tables[0].Rows[0]["ganoadme"]); //meto en variables los valores del paciente

                int tiGnumpac2 = Convert.ToInt32(RrMovpac2.Tables[0].Rows[0]["gnumadme"]);

                int tiGserpac2 = Convert.ToInt32(RrMovpac2.Tables[0].Rows[0]["gservior"]);

                int tlGmedpac2 = Convert.ToInt32(RrMovpac2.Tables[0].Rows[0]["gmedicor"]);

                tstGunenf = Convert.ToString(RrMovpac2.Tables[0].Rows[0]["gunenfor"]);
                string tstGunioripac2 = tstGunenf;


                //rellenar el movimiento actual del paciente 1

                RrMov.Tables[0].Rows[0]["ffinmovi"] = DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos);
                //RrMov("ffinmovi") = CDate(dtfechmovi.Text & " " & dtHoramovi.Text & ":" & Format$(Now, "SS"))

                RrMov.Tables[0].Rows[0]["gunenfde"] = tstGunioripac2;

                RrMov.Tables[0].Rows[0]["gservide"] = tiGserpac1;

                RrMov.Tables[0].Rows[0]["gmedicde"] = tlGmedpac1;

                RrMov.Tables[0].Rows[0]["gplantde"] = iPlanDe;

                RrMov.Tables[0].Rows[0]["ghabitde"] = iHabiDe;

                RrMov.Tables[0].Rows[0]["gcamades"] = stCamaDe;

                RrMov.Tables[0].Rows[0]["itipotra"] = "N";

                RrMov.Tables[0].Rows[0]["gmottras"] = cbbMotivtras.Items[cbbMotivtras.SelectedIndex].Value; 

                RrMov.Tables[0].Rows[0]["irotacio"] = "S";

                string tempQuery = RrMov.Tables[0].TableName;
                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
                string sql2 = "UPDATE AMOVIMIE SET ffinmovi = " + "'" + DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos).ToString("yyyy-MM-dd HH:mm") + "'" + "," +
                                   "gunenfde = " + "'" + tstGunioripac2 + "'" + "," + "gservide =" + "'" + tiGserpac1 + "'" + "," + "gmedicde=" + "'" + tlGmedpac1 + "'" + "," + "gplantde=" + iPlanDe + "," +
                                   "ghabitde = " + iHabiDe + "," + "gcamades = " + "'" + stCamaDe + "'" + "," + "itipotra=" + "'N'" + "," + "gmottras=" + cbbMotivtras.Items[cbbMotivtras.SelectedIndex].Value + "," +
                                   "irotacio = " + "'S'" + "where ganoregi =" + tiGanopac1 + " AND " + "gnumregi =" + tiGnumpac1 + " AND " + " ffinmovi is null AND FMOVIMIE=" + Serrores.FormatFechaHMS(tempRefParam7);
                //tempAdapter_3.Update(RrMov, tempQuery);
                SqlCommand tempCommand = new SqlCommand(sql2, RcAdmision);

                tempCommand.ExecuteNonQuery();
                RrMov.Close();


                //rellenar el movimiento actual del paciente 2

                RrMovpac2.Tables[0].Rows[0]["ffinmovi"] = DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos);
                //RrMovpac2("ffinmovi") = CDate(dtfechmovi.Text & " " & dtHoramovi.Text & ":" & Format$(Now, "SS"))			
                RrMovpac2.Tables[0].Rows[0]["gunenfde"] = tstGunioripac1;

                RrMovpac2.Tables[0].Rows[0]["gservide"] = tiGserpac2;

                RrMovpac2.Tables[0].Rows[0]["gmedicde"] = tlGmedpac2;

                RrMovpac2.Tables[0].Rows[0]["gplantde"] = iPlanOr;

                RrMovpac2.Tables[0].Rows[0]["ghabitde"] = iHabiOr;

                RrMovpac2.Tables[0].Rows[0]["gcamades"] = stCamaOr;

                RrMovpac2.Tables[0].Rows[0]["itipotra"] = "N";

                RrMovpac2.Tables[0].Rows[0]["gmottras"] = cbbMotivtras.Items[cbbMotivtras.SelectedIndex].Value; 

                RrMovpac2.Tables[0].Rows[0]["irotacio"] = "S";

                string tempQuery_2 = RrMovpac2.Tables[0].TableName;
                SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
                string sql3 = "UPDATE AMOVIMIE SET ffinmovi = " + "'" + DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos).ToString("yyyy-MM-dd HH:mm") + "'" + "," +
                                  "gunenfde = " + "'" + tstGunioripac1 + "'" + "," + "gservide =" + "'" + tiGserpac2 + "'" + "," + "gmedicde=" + "'" + tlGmedpac2 + "'" + "," + "gplantde=" + iPlanOr + "," +
                                  "ghabitde = " + iHabiOr + "," + "gcamades = " + "'" + stCamaOr + "'" + "," + "itipotra=" + "'N'" + "," + "gmottras=" + cbbMotivtras.Items[cbbMotivtras.SelectedIndex].Value + "," +
                                  "irotacio = " + "'S'" + "where ganoregi =" + tiGanopac1 + " AND " + "gnumregi =" + tiGnumpac1 + " AND " + " ffinmovi is null AND FMOVIMIE=" + Serrores.FormatFechaHMS(tempRefParam8);
                SqlCommand tempCommand1 = new SqlCommand(sql2, RcAdmision);
                tempCommand1.ExecuteNonQuery();
               
                RrMovpac2.Close();

                //AMOVIMIE2 primer paciente
                sqMov = "SELECT * FROM AMOVIMIE " + " where 2=1";
                //var command5 = new SqlCommand(sqMov, RcAdmision, transaction);
                var command5 = new SqlCommand(sqMov, RcAdmision);
                SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(command5);
                RrMov = new DataSet();
                tempAdapter_7.Fill(RrMov);

                RrMov.AddNew();

                RrMov.Tables[0].Rows[0]["fmovimie"] = DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos);
                //RrMov("fmovimie") = CDate(dtfechmovi.Text & " " & dtHoramovi.Text & ":" & Format$(Now, "SS"))

                RrMov.Tables[0].Rows[0]["ganoregi"] = tiGanopac1;

                RrMov.Tables[0].Rows[0]["gnumregi"] = tiGnumpac1;

                RrMov.Tables[0].Rows[0]["itiposer"] = Module1.vstHosUr;

                RrMov.Tables[0].Rows[0]["gservior"] = tiGserpac1;

                RrMov.Tables[0].Rows[0]["gmedicor"] = tlGmedpac1;

                RrMov.Tables[0].Rows[0]["gplantor"] = iPlanDe;

                RrMov.Tables[0].Rows[0]["ghabitor"] = iHabiDe;

                RrMov.Tables[0].Rows[0]["gcamaori"] = stCamaDe;

                RrMov.Tables[0].Rows[0]["gunenfor"] = tstGunioripac2;

                RrMov.Tables[0].Rows[0]["gusuario"] = Module1.VstCodUsua.ToUpper().Trim();

                RrMov.Tables[0].Rows[0]["fsistema"] = vstfecha_hora_sys;

                string tempQuery_3 = RrMov.Tables[0].TableName;
                SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_7);
                tempAdapter_7.Update(RrMov, tempQuery_3);

                RrMov.Close();
                //AMOVIMIE2 segundo paciente
                sqMov = "SELECT * FROM AMOVIMIE " + " where 2=1";
                //var command6 = new SqlCommand(sqMov, RcAdmision, transaction);
                var command6 = new SqlCommand(sqMov, RcAdmision);
                SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(command6);
                RrMov = new DataSet();
                tempAdapter_9.Fill(RrMov);

                RrMov.AddNew();

                RrMov.Tables[0].Rows[0]["fmovimie"] = DateTime.Parse(dtfechmovi.Text + " " + dtHoramovi.Text + ":" + DtSegundos);
                //RrMov("fmovimie") = CDate(dtfechmovi.Text & " " & dtHoramovi.Text & ":" & Format$(Now, "SS"))

                RrMov.Tables[0].Rows[0]["ganoregi"] = tiGanopac2;

                RrMov.Tables[0].Rows[0]["gnumregi"] = tiGnumpac2;

                RrMov.Tables[0].Rows[0]["itiposer"] = Module1.vstHosUr;

                RrMov.Tables[0].Rows[0]["gservior"] = tiGserpac2;

                RrMov.Tables[0].Rows[0]["gmedicor"] = tlGmedpac2;

                RrMov.Tables[0].Rows[0]["gplantor"] = iPlanOr;

                RrMov.Tables[0].Rows[0]["ghabitor"] = iHabiOr;

                RrMov.Tables[0].Rows[0]["gcamaori"] = stCamaOr;

                RrMov.Tables[0].Rows[0]["gunenfor"] = tstGunioripac1;

                RrMov.Tables[0].Rows[0]["gusuario"] = Module1.VstCodUsua.ToUpper().Trim();

                RrMov.Tables[0].Rows[0]["fsistema"] = vstfecha_hora_sys;

                string tempQuery_4 = RrMov.Tables[0].TableName;
                SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_9);
                tempAdapter_9.Update(RrMov, tempQuery_4);

                RrMov.Close();

                //*************CAMAS cambio los identificadores**********************************
                //cama  primera
                sqMov = "SELECT * " + " FROM DCAMASBO " + " WHERE GPLANTAS=" + iPlanOr.ToString() + " AND GHABITAC=" + iHabiOr.ToString() + " AND GCAMASBO='" + stCamaOr + "'" + " and gidenpac='" + vstGidenpac1 + "'";
                //var command7 = new SqlCommand(sqMov, RcAdmision, transaction);
                var command7 = new SqlCommand(sqMov, RcAdmision);
                SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(command7);
                RrMov = new DataSet();
                tempAdapter_11.Fill(RrMov);
                //cama segunda
                sqMov = "SELECT * " + " FROM DCAMASBO " + " WHERE GPLANTAS=" + iPlanDe.ToString() + " AND GHABITAC=" + iHabiDe.ToString() + " AND GCAMASBO='" + stCamaDe + "'" + " and gidenpac='" + vstGidenpac2 + "'";
                //var command8 = new SqlCommand(sqMov, RcAdmision, transaction);
                var command8 = new SqlCommand(sqMov, RcAdmision);
                SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(command8);
                DataSet RrMov2 = new DataSet();
                tempAdapter_12.Fill(RrMov2);
                //segunda cama

                string SEXO1 = Convert.ToString(RrMov.Tables[0].Rows[0]["ITIPSEXO"]);
                //''FECHALTA1 = RrMov("FPREVALT") & ""

                RrMov.Tables[0].Rows[0]["gidenpac"] = vstGidenpac2;

                RrMov.Tables[0].Rows[0]["ITIPSEXO"] = RrMov2.Tables[0].Rows[0]["ITIPSEXO"];

                //'''oscar 03/12/2003
                //'''If RrMov2("FPREVALT") <> Null Then
                //'''    RrMov("FPREVALT") = RrMov2("FPREVALT")
                //'''End If
                //''If Not IsNull(RrMov2("FPREVALT")) Then
                //''    RrMov("FPREVALT") = RrMov2("FPREVALT")
                //''Else
                //''    RrMov("FPREVALT") = Null
                //''End If
                //'''-----------

                //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                string tempQuery_5 = RrMov.Tables[0].TableName;
                SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter_11);
                tempAdapter_11.Update(RrMov, tempQuery_5);

                RrMov.Close();

                RrMov2.Tables[0].Rows[0]["gidenpac"] = vstGidenpac1;

                RrMov2.Tables[0].Rows[0]["ITIPSEXO"] = SEXO1;

                //'''oscar 03/12/2003
                //'''If FECHALTA1 <> Null Then
                //'''    RrMov2("FPREVALT") = FECHALTA1
                //'''End If
                //''If FECHALTA1 = "" Then
                //''     RrMov2("FPREVALT") = Null
                //''Else
                //''     RrMov2("FPREVALT") = FECHALTA1
                //''End If
                //'''------

                string tempQuery_6 = RrMov2.Tables[0].TableName;
                SqlCommandBuilder tempCommandBuilder_6 = new SqlCommandBuilder(tempAdapter_12);
                tempAdapter_12.Update(RrMov2, RrMov2.Tables[0].TableName);

                RrMov2.Close();
                //******************************************************
                bSALIR = true;
                this.Cursor = Cursors.Default;
                /*SDSALAZAR SE COMENTA ESTE CODIGO TEMPORALMENTE PRUEBA TRANSACTIONSCOPE*/
                ////scope.Complete();
                }
            catch
            {
                //*****************error
                this.Cursor = Cursors.Default;
                   
                Telerik.WinControls.RadMessageBox.Show(this, "Error en traslado", Application.ProductName);
            }
            return;
            /*SDSALAZAR SE COMENTA ESTE CODIGO TEMPORALMENTE PRUEBA TRANSACTIONSCOPE*/
            ////}
        }

        private void rbBloqueo_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (((RadRadioButton)sender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                swtipo = true;
                activa_aceptar();
            }
        }

        private void rbNormal_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (((RadRadioButton)sender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                swtipo = true;
                activa_aceptar();
            }
        }

        public void ObligarTrasladoCama(int StPlanta, int StHabitacion, string StCama, int StPlanta1, int StHabitacion1, string StCama1)
		{
			ObligarTrasladarCama = false;
			PreguntarTrasladarCama = false;
			NegarTrasladarCama = false;
			string ServicioIndicadorDestino = "";
			string ServicioIndicadorOrigen = "";
			ServicioCamaOrigen = "";
			ServicioCamaDestino = "";

			string sql = "select * from SCONSGLO where gconsglo = 'OBLITRAS'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcAdmision);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{				
				if (Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					PreguntarTrasladarCama = true;
				}
				else
				{
					PreguntarTrasladarCama = true; //lo dejo como estaba
					return;
				}
			}
			RrSql.Close();

			sql = "Select gservici from DCAMASBO where gplantas = '" + StPlanta1.ToString() + "' and " + " ghabitac = '" + StHabitacion1.ToString() + "' and   gcamasbo ='" + StCama1 + "'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RcAdmision);
			RrSql = new DataSet();
			tempAdapter_2.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{				
				ServicioCamaDestino = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gservici"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["gservici"]);
			}
			RrSql.Close();

			sql = "Select gservici from DCAMASBO where gplantas = '" + StPlanta.ToString() + "' and " + " ghabitac = '" + StHabitacion.ToString() + "' and   gcamasbo ='" + StCama + "'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, RcAdmision);
			RrSql = new DataSet();
			tempAdapter_3.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{				
				ServicioCamaOrigen = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gservici"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["gservici"]);
			}
			RrSql.Close();

			if (ServicioCamaOrigen != "")
			{
				sql = "Select oblitras from DSERVICI where gservici = '" + ServicioCamaOrigen + "' ";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, RcAdmision);
				RrSql = new DataSet();
				tempAdapter_4.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{					
					ServicioIndicadorOrigen = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["oblitras"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["oblitras"]).ToUpper();
				}
				RrSql.Close();
			}

			if (ServicioCamaDestino != "")
			{
				sql = "Select oblitras from DSERVICI where gservici = '" + ServicioCamaDestino + "' ";
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, RcAdmision);
				RrSql = new DataSet();
				tempAdapter_5.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{					
					ServicioIndicadorDestino = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["oblitras"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["oblitras"]).ToUpper();
				}
				RrSql.Close();
			}

			if ((ServicioCamaOrigen == "" && ServicioIndicadorDestino == "S") || (ServicioIndicadorOrigen == "S" && ServicioCamaOrigen != ServicioCamaDestino) || (ServicioIndicadorOrigen == "" && ServicioIndicadorDestino == "S") || (ServicioIndicadorOrigen == "N" && ServicioIndicadorDestino == "S"))
			{
				PreguntarTrasladarCama = false;
				ObligarTrasladarCama = true;
			}

			//'''sql = "Select gservici,oblitras from DCAMASBO where gplantas = '" & StPlanta & "' and " _
			//''''    & " ghabitac = '" & StHabitacion & "' and   gcamasbo ='" & StCama & "'"
			//'''Set RrSql = RcAdmision.OpenResultset(sql, rdOpenKeyset, rdConcurValues)
			//'''If Not RrSql.EOF Then
			//'''    If UCase(Trim(RrSql("oblitras"))) = "S" And (IsNull(RrSql("gservici")) Or ServicioCamaOrigen <> RrSql("gservici")) Then
			//'''        NegarTrasladarCama = True
			//'''        PreguntarTrasladarCama = False
			//'''        Exit Sub
			//'''    End If
			//'''    If UCase(Trim(RrSql("oblitras"))) = "S" And ServicioCamaOrigen = RrSql("gservici") Then
			//'''        NegarTrasladarCama = False
			//'''        PreguntarTrasladarCama = True
			//'''        Exit Sub
			//'''    End If
			//'''    If UCase(Trim(RrSql("oblitras"))) = "S" Then
			//'''        PreguntarTrasladarCama = False
			//'''        ObligarTrasladarCama = True
			//'''    End If
			//'''    If UCase(Trim(RrSql("oblitras"))) = "N" Or IsNull(RrSql("oblitras")) Then
			//'''        PreguntarTrasladarCama = True
			//'''        ObligarTrasladarCama = False
			//'''        NegarTrasladarCama = False
			//'''    End If
			//''End If
			//''RrSql.Close

		}

        private void cbbMotivtras_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
        {
            swmotivo = false;
            if (cbbMotivtras.SelectedIndex != -1)
            {
                swmotivo = true;
                activa_aceptar();
            }
        }

    }
}