using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Collections.Generic;

namespace mantecamas
{
	public partial class VISUALIZACION
		: Telerik.WinControls.UI.RadForm
	{
		string camasele = String.Empty;
		bool refresco = false;
		string crojo = String.Empty;
		string cverde = String.Empty;
		string cazul = String.Empty;
		string camarillo = String.Empty;
		string cnaranja = String.Empty;
		string cblanco = String.Empty;
		string cvioleta = String.Empty;
		int ESTADO_CAMA = 0;
		Image rojo = null;
        Image verde = null;
        Image azul = null;
        Image amarillo = null;
        Image naranja = null;
        Image rojo2 = null;
        Image verde2 = null;
        Image azul2 = null;
        Image amarillo2 = null;
        Image naranja2 = null;
        Image ImaOtroserv = null;
        Image blanco = null;
        Image violeta = null;
		string sqlplanta = String.Empty; //SQL DEL GRID DE HABITACIONES
		string Estado = String.Empty; //ESTADO DE LA HABITACION
		int maxplanta = 0; //NUMERO DE PLANTAS DEL HOSPITAL
		int maxhabitaci = 0; //MAXIMO NUMERO DE HABITACIONES
		int plantasel = 0; //CAMA SELECCIONADA EN EL GRID
		int habitasel = 0; //HACITACION SELECCIONADA EN EL GRID

		DataSet Rringreso = null; //ESTADO DE LAS CAMAS
		DataSet RrPlantas = null;

		object FIngreso1 = null;

		bool traslado_valido = false;
		bool cambio_filtro = false;
        private bool _isMouseRightDown = false;

		int vlColCam = 0, vlCol = 0;
		int vlRowCam = 0, vlRow = 0;
		object vformulario = null;
		manteclass vclase = null;
		public SqlConnection RcAdmision = null;
		int inicio_columna = 0;
        int[] nplantas = null;
        string[] ncamas = null;
        string VstSexoPaciente = String.Empty;
		string VstRegaPaciente = String.Empty;
		bool BRestriPaciente = false;
		dynamic FormularioRespuesta = null;
		string VstTipoConsulta = String.Empty;

		//*********************** O.Frias (14/11/2006) ***********************
		// Variable para el control del boton de reserva
		bool blnReserva = false;

        bool SPRCAMAS_mouse_r = false;

		private ClaseReservasAdmision _ObjReservaAdmision = null;
		public VISUALIZACION()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();           
			isInitializingComponent = false;
			ReLoadForm(false);
            this.TopMost = true;
        }

		public ClaseReservasAdmision ObjReservaAdmision
		{
			get
			{
				if (_ObjReservaAdmision == null)
				{
					_ObjReservaAdmision = new ClaseReservasAdmision();
				}
				return _ObjReservaAdmision;
			}
			set
			{
				_ObjReservaAdmision = value;
			}
		}
		 //  OBJECTO REFERENTE A LA RESERVA ACTUAL

		//****************************************************************
		//*                                                              *
		//*  Definici�n de estado cama                                   *
		//*                                                              *
		//*      ESTADO_CAMA = 1 --> Cama Libre                          *
		//*      ESTADO_CAMA = 2 --> Cama Ocupada                        *
		//*      ESTADO_CAMA = 3 --> Cama Bloqueada                      *
		//*      ESTADO_CAMA = 4 --> Cama Inhabilitada                   *
		//*      ESTADO_CAMA = 5 --> Cama Varios                         *
		//*      ESTADO_CAMA = 6 --> Cama Otra Area                      *
		//*      ESTADO_CAMA = 7 --> Cuna Libre                          *
		//*      ESTADO_CAMA = 8 --> Cuna Ocupada                        *
		//*      ESTADO_CAMA = 9 --> Cuna Bloqueada                      *
		//*      ESTADO_CAMA = 10 --> Cuna Inhabilitada                  *
		//*      ESTADO_CAMA = 11 --> Cuna Varios                        *
		//*      ESTADO_CAMA = 12 --> Sillon Libre                       *
		//*      ESTADO_CAMA = 13 --> Sillon Ocupada                     *
		//*      ESTADO_CAMA = 14 --> Sillon Bloqueada                   *
		//*      ESTADO_CAMA = 15 --> Sillon Inhabilitada                *
		//*      ESTADO_CAMA = 15 --> Sillon Varios                      *
		//*                                                              *
		//****************************************************************

		public void carga_planta()
		{
			bool bCama = false;
			bool bServicio = false;
			bool bUniEnfe = false;
			int y = 0;
			int x = 0;
			string sqlConsulta = String.Empty;
			string sqlLibre = String.Empty;
			string sqlReservada = String.Empty;
			string sqlOcupada = String.Empty;
			string sqlBloqueada = String.Empty;
			string sqlInhabilitada = String.Empty;
			string sqlLimpieza = String.Empty;
			string sqlIndividual = String.Empty;
			string sqlCompartida = String.Empty;
			string sqlMujer = String.Empty;
			string sqlHombre = String.Empty;
			string sqlCamaCuna = String.Empty;
			string sqlServicio = String.Empty;
			string sqlUniEnfe = String.Empty;
			StringBuilder vstEstado = new StringBuilder();
			StringBuilder vstEstado2 = new StringBuilder();
			string vstHabitacion = String.Empty;
			bool bHayAlgunaCunaHabitacion = false;
			string sqlTipoCama = String.Empty;

			//oscar 27/11/2003
			bool bHayAlgunSillonHabitacion = false;
			bool bTodasCamasconReserva = false;
			bool bTodasCamasInhabEsp = false;
			int LMotivoInhaEsp = 0;

			string Lsql = "select NNUMERI1 FROM SCONSGLO WHERE GCONSGLO='INHABALT'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Lsql, RcAdmision);
			DataSet LRR = new DataSet();
			tempAdapter.Fill(LRR);
			if (LRR.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(LRR.Tables[0].Rows[0]["nnumeri1"]))
				{
					LMotivoInhaEsp = Convert.ToInt32(LRR.Tables[0].Rows[0]["nnumeri1"]);
				}
			}
			//------

			bool bFiltro = false;
			this.Cursor = Cursors.WaitCursor;
			SprPlantas.Row = 0;
			SprPlantas.MaxRows = 0;
			SprPlantas.MaxCols = 1;
            SprPlantas.SetColHidden(1, true);
			//RECOGER EL NUMERO MAXIMO DE PLANTAS DEL HOSPITAL
			string sqlmaxplan = "select count(gplantas)as maximo from dplantasva";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlmaxplan, RcAdmision);
			RrPlantas = new DataSet();
			tempAdapter_2.Fill(RrPlantas);
			maxplanta = Convert.ToInt32(RrPlantas.Tables[0].Rows[0]["maximo"]);
			//RECOGER EL NUMERO MAXIMO DE HABITACIONES POR PLANTA DEL HOSPITAL
			sqlmaxplan = "select max(ghabitac)as maximocol from dcamasbova";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlmaxplan, RcAdmision);
			RrPlantas = new DataSet();
			tempAdapter_3.Fill(RrPlantas);
			maxhabitaci = Convert.ToInt32((!Convert.IsDBNull(RrPlantas.Tables[0].Rows[0]["maximocol"])) ? Convert.ToDouble(RrPlantas.Tables[0].Rows[0]["maximocol"]) + 1 + inicio_columna : 0 + 1 + inicio_columna);
			SprPlantas.MaxRows = maxplanta;
			//SprPlantas.StartingColNumber = -1
			//obtiene las plantas
			RrPlantas.Close();
			sqlmaxplan = "select gplantas from dplantasva order by gplantas";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlmaxplan, RcAdmision);
			RrPlantas = new DataSet();
			tempAdapter_4.Fill(RrPlantas);

            for (int indiceHab = 0; indiceHab < maxhabitaci; indiceHab++)
            {
                GridViewImageColumn imgHab = new GridViewImageColumn();
                imgHab.ImageLayout = ImageLayout.Stretch;
                imgHab.Width = 35;
                //rellena las cabeceras de habitaci�n
                imgHab.HeaderText = (indiceHab - inicio_columna).ToString();
                imgHab.Name = indiceHab.ToString();

                SprPlantas.MasterTemplate.Columns.Add(imgHab);
            }

            //vacia el grid de imagenes
            for (x = 0; x < maxplanta; x++)
            {
                for (y = 1; y < maxhabitaci; y++)
                {
                    SprPlantas.Rows[x].Height = 32;
                    SprPlantas.Row = x + 1;
                    SprPlantas.Col = y;
                    SprPlantas.setTypePictPicture(blanco);
                    SprPlantas.setTypePictStretch(true);
                }
            }

			//vacia la cabecera de la �ltima habitaci�n
			SprPlantas.Col = maxhabitaci;
			SprPlantas.Text = " ";

            //rellenar cabeceras de planta
            x = 0;
            SprPlantas.Col = 1;
            nplantas = new int[RrPlantas.Tables[0].Rows.Count];
            foreach (DataRow iteration_row in RrPlantas.Tables[0].Rows)
            {
                var valor = int.Parse(iteration_row["gplantas"].ToString());
                nplantas[x] = valor;
                SprPlantas.Row = x + 1;
                SprPlantas.Text = Convert.ToString(iteration_row["gplantas"]);
                x++;
            }
            RrPlantas.Close();
            SprPlantas.Row = 1;
            SprPlantas.Columns[0].HeaderText = "";

            if (Module1.vstHosUr == "U")
            {
                sqlLibre = "select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac)  from dcamasbova" + " where " + " iestcama='L'";
                sqlOcupada = "select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " + " where " + " iestcama='O'";
                sqlBloqueada = "select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac)  from dcamasbova " + " where " + " iestcama='B'";
                sqlInhabilitada = "select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova" + " where " + " iestcama='I'";
                sqlLimpieza = "select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova" + " where " + " iestcama='I' and  gmotinha=" + LMotivoInhaEsp.ToString();
                sqlHombre = " select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " + " where iestcama='O' and " + " itipsexo='H'";
                sqlMujer = " select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " + " where iestcama='O' and " + " itipsexo='M'";
                //   sqlReservada = " select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova, aresingrva" & _
                //" where " & _
                //" dcamasbova.gplantas = aresingrva.gplantas and " & _
                //" dcamasbova.ghabitac = aresingrva.ghabitac and " & _
                //" dcamasbova.gcamasbo = aresingrva.gcamasbo and " & _
                //" aresingrva.fpreving >= getdate() and " & _
                //" dcamasbova.iestcama='L'"
                sqlReservada = "";
            }
            else
            {
                sqlLibre = "select str(gplantas)+str(ghabitac)  from dcamasbova" + " where " + " iestcama='L'";
                sqlOcupada = "select str(gplantas)+str(ghabitac) from dcamasbova " + " where " + " iestcama='O'";
                sqlBloqueada = "select str(gplantas)+str(ghabitac)  from dcamasbova " + " where " + " iestcama='B'";
                sqlInhabilitada = "select str(gplantas)+str(ghabitac) from dcamasbova" + " where " + " iestcama='I'";
                sqlLimpieza = "select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova" + " where " + " iestcama='I' and  gmotinha=" + LMotivoInhaEsp.ToString();
                sqlIndividual = "select str(gplantas)+str(ghabitac) from dcamasbova,aepisadm" + " where iestcama='O' and " + " dcamasbova.gidenpac=aepisadm.gidenpac and " + " faltplan is null and iregaloj='I'";
                sqlCompartida = " select str(gplantas)+str(ghabitac) from dcamasbova,aepisadm" + " where iestcama='O' and " + " dcamasbova.gidenpac=aepisadm.gidenpac and " + " faltplan is null and iregaloj='C'";
                sqlHombre = " select str(gplantas)+str(ghabitac) from dcamasbova " + " where iestcama='O' and " + " itipsexo='H'";
                sqlMujer = " select str(gplantas)+str(ghabitac) from dcamasbova " + " where iestcama='O' and " + " itipsexo='M'";

                sqlReservada = " select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova, aresingrva" +
                               " where " +
                               " dcamasbova.gplantas = aresingrva.gplantas and " +
                               " dcamasbova.ghabitac = aresingrva.ghabitac and " +
                               " dcamasbova.gcamasbo = aresingrva.gcamasbo and " +
                               " dcamasbova.iestcama='L'";

                if (Serrores.ObternerValor_CTEGLOBAL(RcAdmision, "VERESERV", "VALFANU1") == "N")
                {
                    sqlReservada = sqlReservada + " and aresingrva.fpreving >= getdate() ";
                }
            }
            if (CmbCama.SelectedIndex > 0)
            {
                sqlCamaCuna = "  dcamasbova.gtipocam = " + CmbCama.SelectedValue.ToString() + " ";
                bCama = true;
            }
            if (CmbServicio.SelectedIndex > 0)
            {
                sqlServicio = "(dcamasbova.gservici = " + CmbServicio.SelectedValue.ToString() + " or " + " dcamasbova.gservici is null) ";
                bServicio = true;
            }
            if (Convert.ToDouble(CmbUniEnfe.SelectedIndex) > 0)
            {
                sqlUniEnfe = " dcamasbova.gcounien = '" + CmbUniEnfe.SelectedValue.ToString() + "' ";
                bUniEnfe = true;
            }

            //Oscar C Septiembre 2012
            //ingreso --> Ingreso en GG
            //ingreso_hd --> Ingreso en HD
            //DetallePaciente --> Manimiento de pacientes en tratamiento periodico en Hospital de Dia (dialisis)
            //ConversionHDaH --> Conversion de Episodio de HD a Episodio de HG
            //ConversionHaHD --> Conversion de Episodio de HG a Episodio de HD

            string nameFrm = FormularioRespuesta.Name;
            switch (nameFrm)
            {
                case "ingreso":
                case "ConversionHDaH":
                    sqlTipoCama = " and dtipocam.isillon='N'";
                    break;
                case "ingreso_hd":
                case "DetallePaciente":
                case "ConversionHaHD":
                    sqlTipoCama = " and dtipocam.isillon='S'";
                    break;
                default:
                    sqlTipoCama = "";
                    break;
            }
            //-------------------

            if (ChbTodas.IsChecked)
            {
                sqlConsulta = "select itiposer,gplantas,ghabitac,gcamasbo, iestcama, isnull(gmotinha,0) as gmotinha1,dtipocam.iescuna,dtipocam.isillon" + "  from dcamasbova," + " dunienfeva,dtipocam where ";
                sqlConsulta = sqlConsulta +
                              "    (itiposer<>'" + Module1.vstHosUr + " ' and gcounien = gunidenf and dcamasbova.gtipocam = dtipocam.gtipocam " + sqlTipoCama + ") or " +
                              "    gcounien = gunidenf  and dcamasbova.gtipocam = dtipocam.gtipocam " + sqlTipoCama;

                if (CmbCama.SelectedIndex > 0 || Convert.ToDouble(CmbUniEnfe.SelectedIndex) > 0 || CmbServicio.SelectedIndex > 0)
                {
                    sqlConsulta = sqlConsulta + " and Str (dcamasbova.gplantas) + Str(dcamasbova.ghabitac) " + " in ( select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) " + " from dcamasbova  where ";
                    if (CmbCama.SelectedIndex > 0)
                    {
                        bFiltro = true;
                        sqlConsulta = sqlConsulta + sqlCamaCuna;
                    }
                    if (CmbServicio.SelectedIndex > 0)
                    {
                        if (bFiltro)
                        {
                            sqlConsulta = sqlConsulta + " and ";
                        }
                        sqlConsulta = sqlConsulta + sqlServicio;
                        bFiltro = true;
                    }
                    if (Convert.ToDouble(CmbUniEnfe.SelectedIndex) > 0)
                    {
                        if (bFiltro)
                        {
                            sqlConsulta = sqlConsulta + " and ";
                        }
                        sqlConsulta = sqlConsulta + sqlUniEnfe;
                        bFiltro = false;
                    }

                    sqlConsulta = sqlConsulta + ")";
                }

                sqlConsulta = sqlConsulta + " order by gplantas,ghabitac ";
            }
            else
            {
                sqlConsulta = "select dcamasbova.gplantas,dcamasbova.ghabitac, dcamasbova.gcamasbo, isnull(dcamasbova.gmotinha,0) as gmotinha1,iestcama,itiposer,dtipocam.iescuna,dtipocam.isillon" + " from dcamasbova,dunienfeva,dtipocam where " + "(itiposer<>'" + Module1.vstHosUr + " ' and gcounien = gunidenf and dcamasbova.gtipocam = dtipocam.gtipocam " + sqlTipoCama + ")  or  " + " gcounien = gunidenf and dcamasbova.gtipocam = dtipocam.gtipocam " + sqlTipoCama + " and str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) in (";

                if (ChbLibre.IsChecked || chbReservada.IsChecked)
                {
                    if (ChbLibre.IsChecked)
                    {
                        sqlConsulta = sqlConsulta + sqlLibre;
                    }
                    if (chbReservada.IsChecked)
                    {
                        sqlConsulta = sqlConsulta + sqlReservada;
                    }

                    //''' REGIMEN DE ALOJAMIENTO
                    if (Module1.vstHosUr != "U")
                    {
                        if (ChbIndividual.CheckState == CheckState.Checked && ChbCompartido.CheckState != CheckState.Checked)
                        {
                            sqlConsulta = sqlConsulta + ") and str(gplantas)+str(ghabitac)" + " not in (" + sqlOcupada + ") and " + "str(gplantas)+str(ghabitac) " + "not in (" + sqlBloqueada + ")";
                        }
                        else if (ChbCompartido.CheckState == CheckState.Checked && ChbIndividual.CheckState != CheckState.Checked)
                        {
                            sqlConsulta = sqlConsulta + ") and str(gplantas)+str(ghabitac)" + " NOT in (" + sqlIndividual + ")";
                        }
                        else if (ChbCompartido.CheckState != CheckState.Checked && ChbIndividual.CheckState != CheckState.Checked)
                        {
                            //SI ELIGE FILTRO DE SEXO Y NO ALOJAMIENTO
                            //PRESENTO LAS HABITACIONES LIBRES Y
                            //LAS OCUPADAS NO INDIVIDUALES
                            if (ChbHombre.CheckState == CheckState.Checked || ChbMujer.CheckState == CheckState.Checked)
                            {
                                sqlConsulta = sqlConsulta + ") and str(gplantas)+str(ghabitac) NOT in (" + sqlIndividual + ")";
                            }
                        }
                    }
                    //''''
                    if (Module1.vstHosUr == "U")
                    {
                        if (ChbHombre.CheckState == CheckState.Checked && ChbMujer.CheckState != CheckState.Checked)
                        {
                            sqlConsulta = sqlConsulta + ") and str(dcamasbova.gplantas)+str(dcamasbova.ghabitac)" + " NOT in (select " + " str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " + " where iestcama='O' and " + " (itipsexo='M' and irestsex='S'))";
                        }
                        if (ChbMujer.CheckState == CheckState.Checked && ChbHombre.CheckState != CheckState.Checked)
                        {
                            sqlConsulta = sqlConsulta + ") and str(dcamasbova.gplantas)+str(dcamasbova.ghabitac)" + " NOT in (select " + " str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " + " where iestcama='O' and " + " (itipsexo='H' and irestsex='S'))";
                        }
                        if (ChbMujer.CheckState == CheckState.Checked && ChbHombre.CheckState == CheckState.Checked)
                        {
                            sqlConsulta = sqlConsulta + " ) ";
                        }
                    }
                    else
                    {
                        if (ChbHombre.CheckState == CheckState.Checked && ChbMujer.CheckState != CheckState.Checked)
                        {
                            // sqlConsulta = sqlConsulta & " and str(dcamasbova.gplantas)+str(dcamasbova.ghabitac)" _
                            //' & " not in (select " _
                            //' & " str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " _
                            //' & " where iestcama='O' and " _
                            //' & " (itipsexo='M' and irestsex='S')) "
                            sqlConsulta = sqlConsulta + " and str(dcamasbova.gplantas)+str(dcamasbova.ghabitac)" + " NOT in (select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " + " where  irestsex='S' and  " + " str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) in " + " (select  str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " + " where iestcama='O' and  itipsexo='M'))";
                        }
                        else if (ChbMujer.CheckState == CheckState.Checked && ChbHombre.CheckState != CheckState.Checked)
                        {
                            //     sqlConsulta = sqlConsulta & " and str(gplantas)+str(ghabitac)" _
                            //'     & " NOT in (select " _
                            //'     & " str(gplantas)+str(ghabitac) from dcamasbova " _
                            //'     & " where iestcama='O' and " _
                            //'     & " (itipsexo='H' and irestsex='S')) "
                            sqlConsulta = sqlConsulta + " and str(dcamasbova.gplantas)+str(dcamasbova.ghabitac)" + " NOT in (select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " + " where  irestsex='S' and  " + " str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) in " + " (select  str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) from dcamasbova " + " where iestcama='O' and  itipsexo='H'))";
                        }
                    }
                    //NO FILTRO POR SEXO NI REGIMEN DE ALOJAMIENTO
                    //PERO SI FILTRO POR CAMA,U.ENFER,SERVICIO
                    if (ChbIndividual.CheckState != CheckState.Checked && ChbCompartido.CheckState != CheckState.Checked && ChbHombre.CheckState != CheckState.Checked && ChbMujer.CheckState != CheckState.Checked)
                    {
                        if (CmbCama.SelectedIndex > 0)
                        {
                            sqlConsulta = sqlConsulta + " and " + sqlCamaCuna;
                        }
                        if (CmbServicio.SelectedIndex > 0)
                        {
                            sqlConsulta = sqlConsulta + " and " + sqlServicio;
                        }
                        if (Convert.ToDouble(CmbUniEnfe.SelectedIndex) > 0)
                        {
                            sqlConsulta = sqlConsulta + " and " + sqlUniEnfe;
                        }
                        sqlConsulta = sqlConsulta + ")";
                    }
                    else
                    {
                        //FILTRO POR REGIMEN DE ALOJAMIENTO O POR SEXO, O LOS DOS
                        //Y ADEMAS POR CAMA � POR U.ENFERMERIA � POR SERVICIO
                        if (ChbCompartido.CheckState == CheckState.Checked && ChbIndividual.CheckState == CheckState.Checked)
                        {
                            sqlConsulta = sqlConsulta + ")";
                        }
                        else
                        {
                            if (ChbMujer.CheckState != CheckState.Checked && ChbHombre.CheckState != CheckState.Checked)
                            {
                                if (ChbCompartido.CheckState == CheckState.Checked && ChbIndividual.CheckState == CheckState.Checked)
                                {
                                    sqlConsulta = sqlConsulta + ")";
                                }
                            }
                        }
                        if (CmbCama.SelectedIndex > 0 || Convert.ToDouble(CmbUniEnfe.SelectedIndex) > 0 || CmbServicio.SelectedIndex > 0)
                        {
                            sqlConsulta = sqlConsulta + " and Str (dcamasbova.gplantas) + Str(dcamasbova.ghabitac) " + " IN (select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) " + " from dcamasbova  where IESTCAMA = 'L' and ";
                            if (CmbCama.SelectedIndex > 0)
                            {
                                bFiltro = true;
                                sqlConsulta = sqlConsulta + sqlCamaCuna;
                            }
                            if (CmbServicio.SelectedIndex > 0)
                            {
                                if (bFiltro)
                                {
                                    sqlConsulta = sqlConsulta + " and ";
                                }
                                sqlConsulta = sqlConsulta + sqlServicio;
                                bFiltro = true;
                            }
                            if (Convert.ToDouble(CmbUniEnfe.SelectedIndex) > 0)
                            {
                                if (bFiltro)
                                {
                                    sqlConsulta = sqlConsulta + " and ";
                                }
                                sqlConsulta = sqlConsulta + sqlUniEnfe;
                                bFiltro = false;
                            }
                            sqlConsulta = sqlConsulta + ") ";
                        }
                    }
                } //'''''''''' FIN LIBRE

                if (ChbOcupada.IsChecked || ChbBloqueada.IsChecked || ChbInhabilitada.IsChecked || chbLimpieza.IsChecked)
                {
                    if (ChbInhabilitada.IsChecked)
                    {
                        sqlConsulta = sqlConsulta + sqlInhabilitada;
                    }
                    else
                    {
                        if (ChbOcupada.IsChecked)
                        {
                            sqlConsulta = sqlConsulta + sqlOcupada;
                        }
                        if (ChbBloqueada.IsChecked)
                        {
                            sqlConsulta = sqlConsulta + sqlBloqueada;
                        }
                        if (chbLimpieza.IsChecked)
                        {
                            sqlConsulta = sqlConsulta + sqlLimpieza;
                        }
                        if (Module1.vstHosUr != "U")
                        {
                            if (ChbIndividual.CheckState == CheckState.Checked && ChbCompartido.CheckState != CheckState.Checked)
                            {
                                sqlConsulta = sqlConsulta + ") and str(gplantas)+str(ghabitac)" + " in (" + sqlIndividual + ")";
                            }
                            if (ChbCompartido.CheckState == CheckState.Checked && ChbIndividual.CheckState != CheckState.Checked)
                            {
                                sqlConsulta = sqlConsulta + ") and str(gplantas)+str(ghabitac)" + " in (" + sqlCompartida + ")";
                            }
                        }
                        if (ChbHombre.CheckState == CheckState.Checked && ChbMujer.CheckState != CheckState.Checked)
                        {
                            if (ChbIndividual.CheckState != CheckState.Checked && ChbCompartido.CheckState != CheckState.Checked)
                            {
                                sqlConsulta = sqlConsulta + ")";
                            }
                            sqlConsulta = sqlConsulta + " and " + " str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) in (" + sqlHombre + ")";
                        }
                        if (ChbMujer.CheckState == CheckState.Checked && ChbHombre.CheckState != CheckState.Checked)
                        {
                            if (ChbIndividual.CheckState != CheckState.Checked && ChbCompartido.CheckState != CheckState.Checked)
                            {
                                sqlConsulta = sqlConsulta + ")";
                            }
                            sqlConsulta = sqlConsulta + " and " + " str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) in (" + sqlMujer + ")";
                        }
                    } //FIN DE ESTADO CAMA
                    //'''''''''''''''''''''''''''''''''''''''
                    //NO FILTRO POR SEXO NI REGIMEN DE ALOJAMIENTO
                    //PERO SI FILTRO POR CAMA,U.ENFER,SERVICIO
                    if (ChbIndividual.CheckState != CheckState.Checked && ChbCompartido.CheckState != CheckState.Checked && ChbHombre.CheckState != CheckState.Checked && ChbMujer.CheckState != CheckState.Checked)
                    {
                        if (CmbCama.SelectedIndex > 0)
                        {
                            sqlConsulta = sqlConsulta + " and " + sqlCamaCuna;
                        }
                        if (CmbServicio.SelectedIndex > 0)
                        {
                            sqlConsulta = sqlConsulta + " and " + sqlServicio;
                        }
                        if (Convert.ToDouble(CmbUniEnfe.SelectedIndex) > 0)
                        {
                            sqlConsulta = sqlConsulta + " and " + sqlUniEnfe;
                        }
                        sqlConsulta = sqlConsulta + ")";
                    }
                    else
                    {
                        if (CmbCama.SelectedIndex > 0 || Convert.ToDouble(CmbUniEnfe.SelectedIndex) > 0 || CmbServicio.SelectedIndex > 0)
                        {
                            //SI FILTRO POR CAMA,U.ENFER,SERVICIO
                            //SE QUE HE FILTRADO O POR SEXO/ ALOJAMENTO,PERO
                            //HAY QUE COMPROBAR CUAL DE LOS DOS
                            if (ChbHombre.CheckState != CheckState.Unchecked && ChbMujer.CheckState != CheckState.Unchecked && ChbCompartido.CheckState != CheckState.Unchecked && ChbIndividual.CheckState != CheckState.Unchecked)
                            {
                                sqlConsulta = sqlConsulta + ") "; //TODOS CHEQUEADOS
                            }
                            else if (ChbHombre.CheckState != CheckState.Unchecked && ChbMujer.CheckState != CheckState.Unchecked)
                            {
                                //HOMBRE/MUJER
                                if (ChbCompartido.CheckState != CheckState.Checked && ChbIndividual.CheckState != CheckState.Checked)
                                {
                                    sqlConsulta = sqlConsulta + ") ";
                                }
                            }
                            else if (ChbHombre.CheckState != CheckState.Checked && ChbMujer.CheckState != CheckState.Checked)
                            {
                                //COMPARTIDO/INDIVIDUAL
                                if (ChbCompartido.CheckState != CheckState.Unchecked && ChbIndividual.CheckState != CheckState.Unchecked)
                                {
                                    sqlConsulta = sqlConsulta + ") ";
                                }
                            }
                            else if (ChbCompartido.CheckState != CheckState.Unchecked && ChbIndividual.CheckState != CheckState.Unchecked)
                            {
                                //COMPARTIDO/INDIVIDUAL/HOMBRE O MUJER
                                if (ChbHombre.CheckState != CheckState.Checked || ChbMujer.CheckState != CheckState.Checked)
                                {
                                    sqlConsulta = sqlConsulta + ") ";
                                }
                            }
                            sqlConsulta = sqlConsulta + " and Str (dcamasbova.gplantas) + Str(dcamasbova.ghabitac) " + " IN (select str(dcamasbova.gplantas)+str(dcamasbova.ghabitac) " + " from dcamasbova  WHERE  IESTCAMA = 'O' ";
                            if (ChbHombre.CheckState == CheckState.Checked && ChbMujer.CheckState != CheckState.Checked)
                            {
                                sqlConsulta = sqlConsulta + " and itipsexo = 'H' ";
                            }
                            else
                            {
                                if (ChbHombre.CheckState != CheckState.Checked && ChbMujer.CheckState == CheckState.Checked)
                                {
                                    sqlConsulta = sqlConsulta + " and itipsexo = 'M' ";
                                }
                            }
                            if (CmbCama.SelectedIndex > 0)
                            {
                                sqlConsulta = sqlConsulta + " and " + sqlCamaCuna;
                            }
                            if (CmbServicio.SelectedIndex > 0)
                            {
                                sqlConsulta = sqlConsulta + " and " + sqlServicio;
                            }
                            if (Convert.ToDouble(CmbUniEnfe.SelectedIndex) > 0)
                            {
                                sqlConsulta = sqlConsulta + " and " + sqlUniEnfe;
                            }
                            sqlConsulta = sqlConsulta + ")";

                        }
                        else if (ChbMujer.CheckState == CheckState.Checked && ChbHombre.CheckState == CheckState.Checked)
                        {
                            if (ChbIndividual.CheckState == CheckState.Checked)
                            {
                                if (ChbCompartido.CheckState == CheckState.Checked)
                                {
                                    sqlConsulta = sqlConsulta + ")";
                                }
                            }
                            else
                            {
                                if (ChbCompartido.CheckState == CheckState.Unchecked)
                                {
                                    sqlConsulta = sqlConsulta + ")";
                                }
                            }
                        }
                        else if (ChbIndividual.CheckState == CheckState.Checked && ChbCompartido.CheckState == CheckState.Checked)
                        {
                            sqlConsulta = sqlConsulta + ")";
                        }
                    } //'''''' FIN TIPO CAMA SIN OTROS FILTROS
                } //FIN DE OCUPADAS

                sqlConsulta = sqlConsulta + " order by dcamasbova.gplantas,dcamasbova.ghabitac";
            } //FIN TODAS
            SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sqlConsulta, RcAdmision);
            RrPlantas = new DataSet();
            tempAdapter_5.Fill(RrPlantas);

            //SPRCAMAS.DataSource = RrPlantas.Tables[0];

            Int32 indice = 0;
            while (indice < RrPlantas.Tables[0].Rows.Count)
            {
                bHayAlgunaCunaHabitacion = false;
                //oscar 27/11/2003
                bHayAlgunSillonHabitacion = false;
                bTodasCamasconReserva = true;
                bTodasCamasInhabEsp = true;
                //---------------
                //posiciono en el grid
                SprPlantas.Col = fnposicion(Convert.ToInt32(RrPlantas.Tables[0].Rows[indice]["gplantas"]), Convert.ToInt32(RrPlantas.Tables[0].Rows[indice]["ghabitac"]), "C");
                SprPlantas.Row = fnposicion(Convert.ToInt32(RrPlantas.Tables[0].Rows[indice]["gplantas"]), Convert.ToInt32(RrPlantas.Tables[0].Rows[indice]["ghabitac"]), "F");
                vstHabitacion = Conversion.Str(RrPlantas.Tables[0].Rows[indice]["gplantas"]) + Conversion.Str(RrPlantas.Tables[0].Rows[indice]["ghabitac"]);
                vstEstado = new StringBuilder(Convert.ToString(RrPlantas.Tables[0].Rows[indice]["iestcama"]));

                //busco estado de la habitacion
                while (indice < RrPlantas.Tables[0].Rows.Count && vstHabitacion == Conversion.Str(RrPlantas.Tables[0].Rows[indice]["gplantas"]) + Conversion.Str(RrPlantas.Tables[0].Rows[indice]["ghabitac"]))
                {
                    if (Convert.ToString(RrPlantas.Tables[0].Rows[indice]["iestcama"]) != vstEstado.ToString() && vstEstado.ToString() != "W")
                    {
                        vstEstado.Append(Convert.ToString(RrPlantas.Tables[0].Rows[indice]["iestcama"]));
                        //vstEstado = "V"
                    }
                    if (Convert.ToString(RrPlantas.Tables[0].Rows[indice]["itiposer"]) != Module1.vstHosUr)
                    {
                        vstEstado = new StringBuilder("W");
                    }
                    if (Convert.ToDouble(RrPlantas.Tables[0].Rows[indice]["gplantas"]) == 3 && Convert.ToDouble(RrPlantas.Tables[0].Rows[indice]["ghabitac"]) == 0)
                    {
                        vstEstado = new StringBuilder(vstEstado.ToString());
                    }

                    //oscar 27/11/2003
                    //If RrPlantas("iescuna") = "S" Then
                    //    bHayAlgunaCunaHabitacion = True
                    //End If
                    if (Convert.ToString(RrPlantas.Tables[0].Rows[indice]["iescuna"]) == "S" && Convert.ToString(RrPlantas.Tables[0].Rows[indice]["isillon"]) == "N")
                    {
                        bHayAlgunaCunaHabitacion = true;
                    }
                    if (Convert.ToString(RrPlantas.Tables[0].Rows[indice]["isillon"]) == "S" && Convert.ToString(RrPlantas.Tables[0].Rows[indice]["iescuna"]) == "N")
                    {
                        bHayAlgunSillonHabitacion = true;
                    }
                    if (Module1.vstHosUr == "U")
                    {
                        bTodasCamasconReserva = false;
                    }
                    else
                    {
                        Lsql = "SELECT * FROM ARESINGRVA WHERE " +
                               " gplantas=" + Convert.ToString(RrPlantas.Tables[0].Rows[indice]["gplantas"]) + " AND " +
                               " ghabitac=" + Convert.ToString(RrPlantas.Tables[0].Rows[indice]["ghabitac"]) + " AND " +
                               " gcamasbo='" + Convert.ToString(RrPlantas.Tables[0].Rows[indice]["gcamasbo"]) + "' AND " +
                               " ganoadme IS NULL and gnumadme IS NULL";
                        SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(Lsql, RcAdmision);
                        LRR = new DataSet();
                        tempAdapter_6.Fill(LRR);
                        if (LRR.Tables[0].Rows.Count == 0)
                        {
                            bTodasCamasconReserva = false;
                        }
                        LRR.Close();
                    }
                    if (LMotivoInhaEsp != 0)
                    {
                        if (LMotivoInhaEsp != Convert.ToInt32(RrPlantas.Tables[0].Rows[indice]["gmotinha1"]))
                        {
                            bTodasCamasInhabEsp = false;
                        }
                    }
                    //----------------

                    indice++;
                    if (indice == RrPlantas.Tables[0].Rows.Count)
                    {
                        break;
                    }
                };
                //relleno con el icono
                //vstEstado2 = ""

                if (vstEstado.ToString().IndexOf('L') >= 0)
                {
                    vstEstado2 = new StringBuilder("L");
                }
                if (vstEstado.ToString().IndexOf('O') >= 0)
                {
                    vstEstado2.Append("O");
                }
                if (vstEstado.ToString().IndexOf('B') >= 0)
                {
                    vstEstado2.Append("B");
                }
                if (vstEstado.ToString().IndexOf('I') >= 0)
                {
                    vstEstado2.Append("I");
                }
                if (vstEstado.ToString() == "W")
                {
                    vstEstado2 = new StringBuilder("W");
                }

                switch (vstEstado2.ToString())
                {
                    case "L":
                        if (!bHayAlgunaCunaHabitacion)
                        { // si no es cuna
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(5).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[4]); // cama libre
                                if (bTodasCamasconReserva)
                                {
                                    SprPlantas.setTypePictPicture(IlIconos.Images[49]);
                                } // cama libre  con reserva
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[41]); // sillon libre
                                if (bTodasCamasconReserva)
                                {
                                    SprPlantas.setTypePictPicture(IlIconos.Images[51]);
                                } // sillon libre con reserva
                            }
                            //--------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[35]); // cuna libre
                            if (bTodasCamasconReserva)
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[53]);
                            } // cuna libre con reserva
                        }
                        break;
                    case "O":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(6).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[5]); // cama ocupada
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[43]); // sillon ocupado
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[39]); // cuna ocupada
                        }
                        break;
                    case "I":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(8).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[7]); // cama inhabilitada
                                if (bTodasCamasInhabEsp)
                                {
                                    SprPlantas.setTypePictPicture(IlIconos.Images[50]);
                                } // cama inhabilitada por motivo de constante global
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[45]); // sillon inhabilitado
                                if (bTodasCamasInhabEsp)
                                {
                                    SprPlantas.setTypePictPicture(IlIconos.Images[52]);
                                } // sillon inhabilitado por motivo de constante global
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[33]); // cuna inhabilitada
                            if (bTodasCamasInhabEsp)
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[54]);
                            } // cuna inhabilitada por motivo de constante global
                        }
                        break;
                    case "B":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(4).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[3]); // cama bloqueada
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[47]); // sillon bloqueado
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[31]); // cuna bloqueada
                        }
                        break;
                    case "LO":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(7).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[6]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]); //varias cunas
                        }
                        break;
                    case "LB":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(22).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[21]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "LI":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(21).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[20]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "OB":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(24).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[23]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "OI":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(23).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[22]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "BI":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(30).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[29]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "LOB":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(26).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[25]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "LOI":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(27).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[26]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "LBI":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(25).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[24]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "OBI":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(29).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[28]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "LOBI":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(28).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[27]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[48]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[37]);
                        }
                        break;
                    case "V":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(7).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[6]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[18]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[18]);
                        }
                        break;
                    case "W":
                        if (!bHayAlgunaCunaHabitacion)
                        {
                            //oscar 27/11/2003
                            //SprPlantas.TypePictPicture = IlIconos.ListImages(19).Picture
                            if (!bHayAlgunSillonHabitacion)
                            { //si no es sillon
                                SprPlantas.setTypePictPicture(IlIconos.Images[18]);
                            }
                            else
                            {
                                SprPlantas.setTypePictPicture(IlIconos.Images[18]);
                            }
                            //-------------
                        }
                        else
                        {
                            SprPlantas.setTypePictPicture(IlIconos.Images[18]);
                        }
                        break;
                    default:
                        SprPlantas.setTypePictPicture(blanco);
                        break;
                }
                vstEstado2 = new StringBuilder("");
            }

            bool vstSalirFor = false;
            if (cambio_filtro)
            {
                for (x = 0; x < maxplanta; x++)
                {
                    for (y = 1; y <= maxhabitaci; y++)
                    {
                        SprPlantas.Col = y;
                        SprPlantas.Row = x;
                        if (SprPlantas.getTypePictPicture() != blanco && !compareBmp((Bitmap)SprPlantas.getTypePictPicture(), (Bitmap)IlIconos.Images[18]))
                        {
                            vstSalirFor = true;
                            break;
                        }
                    }
                    if (vstSalirFor)
                    {
                        break;
                    }
                }
                if (vstSalirFor)
                {
                    //habitasel = rrGenera("ghabitac") '+ inicio_columna
                    //plantasel = fncamafila(rrGenera("gplantas"))
                    habitasel = y - inicio_columna;
                    plantasel = x;
                    SprPlantas.Rows[x].Cells[y].IsSelected = true;
                }
                else
                {
                    habitasel = -1;
                    plantasel = -1;
                }
                cambio_filtro = false;
            }
            carga_camas();
            this.Cursor = Cursors.Default;
		}

		public void fnactiva_filtro()
		{
			CmbCama.Items.Clear();
			CmbUniEnfe.Items.Clear();
			CmbServicio.Items.Clear();
			if (ChbLibre.IsChecked || ChbOcupada.IsChecked || ChbBloqueada.IsChecked || chbReservada.IsChecked)
			{
				FrmSexo.Enabled = true;
				FrmAlojamiento.Enabled = true;
				//    FrmCama.Enabled = True
				//    FrmServicio.Enabled = True
				//    FrmUniEnfe.Enabled = True
				ChbMujer.Enabled = true;
				ChbHombre.Enabled = true;
				ChbIndividual.Enabled = true;
				ChbCompartido.Enabled = true;
				// CmbCama.Enabled = True
				// CmbUniEnfe.Enabled = True
				// CmbServicio.Enabled = True
				//CARGAR COMBOS: TIPO CAMA, SERVICIO, UNIDAD DE ENFERMERIA
				// CargarCombos
			}
			else
			{
				FrmSexo.Enabled = false;
				FrmAlojamiento.Enabled = false;
				//    FrmCama.Enabled = False
				//    FrmServicio.Enabled = False
				//    FrmUniEnfe.Enabled = False
				ChbMujer.CheckState = CheckState.Unchecked;
				ChbHombre.CheckState = CheckState.Unchecked;
				ChbIndividual.CheckState = CheckState.Unchecked;
				ChbCompartido.CheckState = CheckState.Unchecked;
				ChbMujer.Enabled = false;
				ChbHombre.Enabled = false;
				ChbIndividual.Enabled = false;
				ChbCompartido.Enabled = false;
				ChbCompartido.Enabled = false;
				//    CmbCama.Enabled = False
				//    CmbUniEnfe.Enabled = False
				//    CmbServicio.Enabled = False
			}
			//CARGAR COMBOS: TIPO CAMA, SERVICIO, UNIDAD DE ENFERMERIA
			FrmCama.Enabled = true;
			FrmServicio.Enabled = true;
			FrmUniEnfe.Enabled = true;

			if (Module1.vstHosUr == "U")
			{
				FrmAlojamiento.Enabled = false;
				ChbIndividual.Enabled = false;
				ChbCompartido.Enabled = false;
			}
			CargarCombos();
		}

		public object fnestado_habita(ref string Pestado, ref string Ptipo, ref string Psexo, ref string Paloj, ref string Phabisex)
		{
			object vsticono = new object();
			object vstVuelta = blanco;
			if (Pestado != "ninguno")
			{
				if (Pestado.Length != 1)
				{
					vsticono = naranja;
				}
				else
				{
					switch(Ptipo)
					{
						case "L" : 
							vsticono = verde; 
							break;
						case "O" : 
							vsticono = rojo; 
							break;
						case "B" : 
							vsticono = amarillo; 
							break;
						case "I" : 
							vsticono = azul; 
							break;
					}
				}
			}
			else
			{
				vsticono = blanco;
			}
			if (Pestado.IndexOf(Ptipo) >= 0)
			{
				vstVuelta = vsticono;
				//SI HA MARCADO RESTRICCION DE SEXO
				if (ChbHombre.CheckState == CheckState.Checked && ChbHombre.Enabled)
				{
					//si no hay restriccion de sexo debe salir
					if (Ptipo == "L")
					{
						if (Psexo != "no")
						{
							//si hay restriccion pero con varios sexos no debe salir
							if (Psexo == "varios")
							{
								vstVuelta = blanco;
							}
							else
							{
								//si hay restriccion de sexo y es hombre si debe salir
								if ((Psexo.IndexOf('H') + 1) == 0)
								{
									//    vstVuelta = vsticono
									//Else
									vstVuelta = blanco;
								}
							}
						}
					}
					else
					{
						if ((Phabisex.IndexOf('H') + 1) == 0)
						{
							//    vstVuelta = vsticono
							//Else
							vstVuelta = blanco;
						}
					}
				}
				if (ChbMujer.CheckState == CheckState.Checked && ChbMujer.Enabled)
				{
					//si no hay restriccion de sexo debe salir
					if (Ptipo == "L")
					{
						if (Psexo != "no")
						{
							//si hay restriccion pero con varios sexos no debe salir
							if (Psexo == "varios")
							{
								vstVuelta = blanco;
							}
							else
							{
								//si hay restriccion de sexo y es hombre si debe salir
								if ((Psexo.IndexOf('M') + 1) == 0)
								{
									//    vstVuelta = vsticono
									//Else
									vstVuelta = blanco;
								}
							}
						}
					}
					else
					{
						if ((Phabisex.IndexOf('M') + 1) == 0)
						{
							//    vstVuelta = vsticono
							//Else
							vstVuelta = blanco;
						}
					}
				}
				//regimen de alojamiento
				if (ChbIndividual.CheckState == CheckState.Checked && ChbIndividual.Enabled)
				{
					//si no existe regimen de alojamiento si debe salir
					if (Paloj != "ninguno")
					{
						//vstVuelta = vsticono

						// si existe varios no debe salir
						if (Paloj == "varios")
						{
							vstVuelta = blanco;
						}
						else
						{
							//si es libre
							//muestra las posibles habitaciones libres
							//para regimen individual
							if (Ptipo == "L")
							{
								if (Phabisex.Length > 0)
								{
									vstVuelta = blanco;
								}
							}
							else
							{
								// sino muestra aquellos que tienen el regimen individual
								if ((Paloj.IndexOf('I') + 1) == 0)
								{
									vstVuelta = blanco;
								}
							}
						}
					}
				}
				if (ChbCompartido.CheckState == CheckState.Checked && ChbCompartido.Enabled)
				{
					//si no existe regimen de alojamiento si debe salir
					if (Paloj != "ninguno")
					{
						//vstVuelta = vsticono

						// si existe varios no debe salir
						if (Paloj == "varios")
						{
							vstVuelta = blanco;
						}
						else
						{
							//si es libre
							//muestra las posibles habitaciones libres
							//para regimen compartido
							if (Ptipo == "L")
							{
								if (Paloj.IndexOf('I') >= 0)
								{
									vstVuelta = blanco;
								}
							}
							else
							{
								// sino muestra aquellos que tienen el regimen individual
								if ((Paloj.IndexOf('C') + 1) == 0)
								{
									vstVuelta = blanco;
								}
							}
						}
					}
				}
			}
			return vstVuelta;
		}

		public bool fnmarcado_alguno()
		{
			return ChbLibre.IsChecked || ChbOcupada.IsChecked || ChbBloqueada.IsChecked || ChbInhabilitada.IsChecked || ChbMujer.CheckState != CheckState.Unchecked || ChbHombre.CheckState != CheckState.Unchecked || ChbIndividual.CheckState != CheckState.Unchecked || ChbCompartido.CheckState != CheckState.Unchecked;
		}

		public int fnposicion(int Pfil, int Pcol, string Ptipo)
		{
			int result = 0;
			int x = 1;
			int actual_fil = SprPlantas.Row;
			int actual_row = SprPlantas.Col;
			if (Ptipo == "F")
			{
				SprPlantas.Col = 1;
				x = 0;
				SprPlantas.Row = x;
				while (SprPlantas.Text.Trim() != Conversion.Str(Pfil).Trim() && x < SprPlantas.MaxRows)
				{
					x++;
					SprPlantas.Row = x;
				}
			}
			else
			{
				SprPlantas.Row = 0;
				x = 0;
				SprPlantas.Col = x;
				while (SprPlantas.Text.Trim() != Conversion.Str(Pcol).Trim() && x < SprPlantas.MaxCols)
				{
					x++;
					SprPlantas.Col = x;
				}
			}
			result = x;
			SprPlantas.Row = actual_fil;
			SprPlantas.Col = actual_row;
			return result;
		}

		//********************************************************************************************************
		//*                                                                                                      *
		//*  Procedimiento: fnrecogeser                                                                          *
		//*                                                                                                      *
		//*  Modificaci�n:                                                                                       *
		//*                                                                                                      *
		//*    O.Frias (14/11/2006) Se incorpora la variable pBlnReserva, est� contendr� el valor para mostrar   *
		//*                         el boton de reserva o no.                                                    *
		//*                                                                                                      *
		//********************************************************************************************************
		public void fnrecogeser(manteclass clase, dynamic formulario, SqlConnection conexion, string tipo, string consulta, string Parasexo, bool Pararestri, string Pararega, string PtipoCama = "", bool pBlnReserva = false)
		{
			vformulario = formulario;
			vclase = clase;
			RcAdmision = conexion;

			//******************* O.Frias (14/11/2006) Inicio *******************
			blnReserva = pBlnReserva;
			//******************** O.Frias (14/11/2006) Fin  ********************

			Module1.vstHosUr = tipo;
			VstTipoConsulta = consulta;
			VstSexoPaciente = Parasexo;
			BRestriPaciente = Pararestri;
			VstRegaPaciente = Pararega;
			FormularioRespuesta = formulario;
		}

		public void FNRECOGETAG(string PARAACCION)
		{
			this.Tag = PARAACCION;
		}

		public int fncamafila(int cama)
		{
			int result = 0;
			int actual_fil = cama;
			int actual_col = SprPlantas.Col;
			SprPlantas.Col = 1;
			SprPlantas.Row = 1;
			int x = 1;
			while (SprPlantas.Text != Conversion.Str(cama).Trim())
			{
				x++;
				SprPlantas.Row = x;
			}
			result = x;
			SprPlantas.Row = actual_fil;
			SprPlantas.Col = actual_col;
			return result;
		}

		public int fnfilacama(int fila, int columna)
		{
			int result = 0;
			int actual_fil = SprPlantas.Row;
			int actual_col = columna;
			SprPlantas.Col = 1;
			SprPlantas.Row = fila + 1;
			result = Convert.ToInt32(Double.Parse(SprPlantas.Text));
			SprPlantas.Col = actual_col;
			SprPlantas.Row = actual_fil;
			return result;
		}

		public void COMPROBAR_TRASLADO(int tipo)
		{
			string sqpaciente2 = String.Empty;
			string sqregaloj = String.Empty;
			string sqlsexo = String.Empty;
			string sqnumocupadas1 = String.Empty;
			string sqnumocupadas2 = String.Empty;

			DataSet RrPaciente2 = null;
			DataSet RrRegaloj = null;
			DataSet Rrsexo = null;
			DataSet RrNumocupadas1 = null;
			DataSet RrNumocupadas2 = null;

			bool seguir_comprobar = true;

			//CAMA ORIGEN
			string iPlanDe = SPRCAMAS.Text.Substring(0, Math.Min(2, SPRCAMAS.Text.Length));
			string iHabiDe = SPRCAMAS.Text.Substring(2, Math.Min(2, SPRCAMAS.Text.Length - 2));
			string stCamaDe = SPRCAMAS.Text.Substring(4, Math.Min(2, SPRCAMAS.Text.Length - 4));

			//CAMA DESTINO
			string iPlanOr = LbDcama.Text.Substring(0, Math.Min(2, LbDcama.Text.Length));
			string iHabiOr = LbDcama.Text.Substring(2, Math.Min(2, LbDcama.Text.Length - 2));
			string stCamaOr = LbDcama.Text.Substring(4, Math.Min(2, LbDcama.Text.Length - 4));

			//DATOS PACIENTE ORIGEN
			string sqpaciente1 = "SELECT ITIPSEXO,AEPISADM.GIDENPAC," + " IESTCAMA,IREGALOJ,IRESTSEX " + " FROM DCAMASBOVA,AEPISADM " + " WHERE GPLANTAS=" + iPlanOr + " AND GHABITAC=" + iHabiOr + " AND GCAMASBO='" + stCamaOr + "'" + " AND DCAMASBOVA.GIDENPAC=AEPISADM.GIDENPAC and aepisadm.faltplan is null";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqpaciente1, RcAdmision);
			DataSet RrPaciente1 = new DataSet();
			tempAdapter.Fill(RrPaciente1);

			//+++++
			if (RrPaciente1.Tables[0].Rows.Count == 0)
			{
				traslado_valido = false;				
				short tempRefParam = 1940;
				string[] tempRefParam2 = new string[]{"realizar", "ya no existe el paciente en la cama origen"};
                Serrores.oClass.CtrlMensaje = this;
                Serrores.iresume = (int)Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcAdmision, tempRefParam2);
                Serrores.oClass.CtrlMensaje = null;
                return;
			}
			//+++++

			//DATOS HABITACION ORIGEN
			string sqhabitaci1 = "SELECT ITIPSEXO,IRESTSEX,IESTCAMA," + " GIDENPAC,GCOUNIEN FROM DCAMASBOVA" + " WHERE GPLANTAS=" + iPlanOr + " AND GHABITAC=" + iHabiOr + " AND irestsex='S'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqhabitaci1, RcAdmision);
			DataSet RrHabitaci1 = new DataSet();
			tempAdapter_2.Fill(RrHabitaci1);

			if (tipo == 2)
			{
				//DATOS PACIENTE PARA LA ROTACION
				sqpaciente2 = "SELECT ITIPSEXO,AEPISADM.GIDENPAC," + " IESTCAMA,IREGALOJ,IRESTSEX " + " FROM DCAMASBOVA,AEPISADM " + " WHERE GPLANTAS=" + iPlanDe + " AND GHABITAC=" + iHabiDe + " AND GCAMASBO='" + stCamaDe + "'" + " AND DCAMASBOVA.GIDENPAC=AEPISADM.GIDENPAC and aepisadm.faltplan is null";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqpaciente2, RcAdmision);
				RrPaciente2 = new DataSet();
				tempAdapter_3.Fill(RrPaciente2);

				//+++++
				if (RrPaciente2.Tables[0].Rows.Count == 0)
				{
					traslado_valido = false;					
					short tempRefParam3 = 1940;
					string[] tempRefParam4 = new string[]{"realizar", "ya no existe el paciente en la cama destino"};
                    Serrores.oClass.CtrlMensaje = this;
                    Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RcAdmision, tempRefParam4);
                    Serrores.oClass.CtrlMensaje = null;
                    return;
				}
				//+++++
			}

			//DATOS HABITACION DESTINO PARA EL SEXO
			string sqhabitaci2 = "SELECT ITIPSEXO,IRESTSEX,IESTCAMA," + " GIDENPAC,GCOUNIEN FROM DCAMASBOVA" + " WHERE GPLANTAS=" + iPlanDe + " AND GHABITAC=" + iHabiDe + " AND irestsex='S'";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqhabitaci2, RcAdmision);
			DataSet RrHabitaci2 = new DataSet();
			tempAdapter_4.Fill(RrHabitaci2);

			if (tipo == 1)
			{
				//TRASLADO
				//COMPROBAR RESTRICCION DE SEXO
				//COMPROBAR REGIMEN ALOJAMIENTO
				//****************sexo*********************
				//    If RrHabitaci2.RowCount > 0 Or RrPaciente1("irestsex") = "S" Then
				if (RrHabitaci2.Tables[0].Rows.Count > 0)
				{
					// MIRAR SEXO DE LA HABITACI�N
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToString(RrPaciente1.Tables[0].Rows[0]["ITIPSEXO"]) != "I")
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						sqlsexo = "SELECT ITIPSEXO FROM DCAMASBOVA " + "WHERE ITIPSEXO <> '" + Convert.ToString(RrPaciente1.Tables[0].Rows[0]["itipsexo"]) + "' AND ITIPSEXO is not NULL" + " AND GPLANTAS=" + iPlanDe + " AND GHABITAC=" + iHabiDe + " and gidenpac <> '" + Convert.ToString(RrPaciente1.Tables[0].Rows[0]["gidenpac"]) + "'";
						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sqlsexo, RcAdmision);
						Rrsexo = new DataSet();
						tempAdapter_5.Fill(Rrsexo);
						if (Rrsexo.Tables[0].Rows.Count != 0)
						{							
							short tempRefParam5 = 1350;
							string[] tempRefParam6 = new string[]{};
                            Serrores.oClass.CtrlMensaje = this;
                            Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, RcAdmision, tempRefParam6);
                            Serrores.oClass.CtrlMensaje = null;
                            traslado_valido = false;
							seguir_comprobar = false;
						}
						Rrsexo.Close();
					}
				}
				if (seguir_comprobar)
				{
					//***************alojamiento******
					//COMPROBAR EL ESTADO DE LA HABITACION
					sqregaloj = "SELECT IESTCAMA,DCAMASBOVA.GIDENPAC,IREGALOJ" + " FROM DCAMASBOVA,AEPISADM " + " WHERE GPLANTAS=" + iPlanDe + " AND GHABITAC=" + iHabiDe + " and (iestcama='O' OR IESTCAMA='B')" + " AND DCAMASBOVA.GIDENPAC=AEPISADM.GIDENPAC and aepisadm.faltplan is null";
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sqregaloj, RcAdmision);
					RrRegaloj = new DataSet();
					tempAdapter_6.Fill(RrRegaloj);
					
					if (Convert.ToString(RrPaciente1.Tables[0].Rows[0]["iregaloj"]) == "I")
					{
						foreach (DataRow iteration_row in RrRegaloj.Tables[0].Rows)
						{
							if (iteration_row["GIDENPAC"] != RrPaciente1.Tables[0].Rows[0]["GIDENPAC"])
							{								
								short tempRefParam7 = 1340;
								string[] tempRefParam8 = new string[]{};
                                Serrores.oClass.CtrlMensaje = this;
                                Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, RcAdmision, tempRefParam8);
                                Serrores.oClass.CtrlMensaje = null;
                                traslado_valido = false;
								break;
							}
						}
						RrRegaloj.Close();
					}
					else
					{
						// COMPROBAR QUE ALGUNO DE LA HABITACION NO TENGA INDIVIDUAL
						foreach (DataRow iteration_row_2 in RrRegaloj.Tables[0].Rows)
						{
							if (Convert.ToString(iteration_row_2["IREGALOJ"]) == "I" && iteration_row_2["GIDENPAC"] != RrPaciente1.Tables[0].Rows[0]["GIDENPAC"])
							{
								traslado_valido = false;
								// error incompatible los regimenes								
								short tempRefParam9 = 1340;
								string[] tempRefParam10 = new string[]{};
                                Serrores.oClass.CtrlMensaje = this;
                                Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, RcAdmision, tempRefParam10);
                                Serrores.oClass.CtrlMensaje = null;
                                break;
							}
							else
							{								
								RrRegaloj.MoveLast(null);
							}
						}
						RrRegaloj.Close();
					}
				}
			}
			else
			{
				//ROTACION

				// comprobar regimen de alojamiento
				// MIRAR EL REGIMEN DE ALOJAMIENTO DEL ORIGEN SI ES INDIVIDUAL
				// COMPROBAR EN EL DESTINO QUE NO HAY MAS DE UNA CAMA OCUPADA
				// MIRAR EL REGIMEN DEL ALOJAMIENTO DEL DESTINO SI ES INDIVIDUAL
				// COMPROBAR QUE EN ORIGEN NO HAY MAS DE UNA CAMA OCUPADA

				//PACIENTE DESTINO
				sqnumocupadas2 = "SELECT ITIPSEXO,IRESTSEX,IESTCAMA,IREGALOJ," + " DCAMASBOVA.GIDENPAC FROM DCAMASBOVA,AEPISADM" + " WHERE GPLANTAS=" + iPlanDe + " AND GHABITAC=" + iHabiDe + " AND (IESTCAMA='O' OR IESTCAMA='B')" + " AND DCAMASBOVA.GIDENPAC=AEPISADM.GIDENPAC and aepisadm.faltplan is null";
				SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sqnumocupadas2, RcAdmision);
				RrNumocupadas2 = new DataSet();
				tempAdapter_7.Fill(RrNumocupadas2);
				
				if (Convert.ToString(RrPaciente1.Tables[0].Rows[0]["IREGALOJ"]) == "I")
				{
					// COMPROBAR HABITACION DESTINO
					
					RrNumocupadas2.MoveFirst();
					foreach (DataRow iteration_row_3 in RrNumocupadas2.Tables[0].Rows)
					{
						if (iteration_row_3["GIDENPAC"] != RrPaciente1.Tables[0].Rows[0]["GIDENPAC"] && iteration_row_3["GIDENPAC"] != RrPaciente2.Tables[0].Rows[0]["GIDENPAC"])
						{							
							short tempRefParam11 = 1340;
							string[] tempRefParam12 = new string[]{};
                            Serrores.oClass.CtrlMensaje = this;
                            Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, RcAdmision, tempRefParam12);
                            Serrores.oClass.CtrlMensaje = null;
                            traslado_valido = false;
							seguir_comprobar = false;
							break;
						}
					}
					RrNumocupadas2.Close();
				}
				else
				{
					// COMPROBAR SI ALGUNO DE LA HABITACION TIENE INDIVIDUAL					
					RrNumocupadas2.MoveFirst();
					foreach (DataRow iteration_row_4 in RrNumocupadas2.Tables[0].Rows)
					{
						if (Convert.ToString(iteration_row_4["IREGALOJ"]) == "I" && iteration_row_4["GIDENPAC"] != RrPaciente1.Tables[0].Rows[0]["GIDENPAC"] && iteration_row_4["GIDENPAC"] != RrPaciente2.Tables[0].Rows[0]["GIDENPAC"])
						{
							//UPGRADE_ISSUE: (1040) MoveLast function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
							RrNumocupadas2.MoveLast(null);
							traslado_valido = false;
							seguir_comprobar = false;
							// error incompatible los regimenes
							short tempRefParam13 = 1340;
							string[] tempRefParam14 = new string[]{};
                            Serrores.oClass.CtrlMensaje = this;
                            Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, RcAdmision, tempRefParam14);
                            Serrores.oClass.CtrlMensaje = null;
                            break;
						}
						else
						{							
							RrNumocupadas2.MoveLast(null);
						}
					}
					RrNumocupadas2.Close();
				}

				//PACIENTE ORIGEN
				if (seguir_comprobar)
				{
					sqnumocupadas1 = "SELECT ITIPSEXO,IRESTSEX,IESTCAMA,IREGALOJ," + " DCAMASBOVA.GIDENPAC FROM DCAMASBOVA,AEPISADM" + " WHERE GPLANTAS=" + iPlanOr + " AND GHABITAC=" + iHabiOr + " AND (IESTCAMA='O' OR IESTCAMA='B')" + " AND DCAMASBOVA.GIDENPAC=AEPISADM.GIDENPAC and aepisadm.faltplan is null";
					SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(sqnumocupadas1, RcAdmision);
					RrNumocupadas1 = new DataSet();
					tempAdapter_8.Fill(RrNumocupadas1);
					
					if (Convert.ToString(RrPaciente2.Tables[0].Rows[0]["IREGALOJ"]) == "I")
					{						
						RrNumocupadas1.MoveFirst();
						foreach (DataRow iteration_row_5 in RrNumocupadas1.Tables[0].Rows)
						{
							if (iteration_row_5["GIDENPAC"] != RrPaciente2.Tables[0].Rows[0]["GIDENPAC"] && iteration_row_5["GIDENPAC"] != RrPaciente1.Tables[0].Rows[0]["GIDENPAC"])
							{								
								short tempRefParam15 = 1340;
								string[] tempRefParam16 = new string[]{};
                                Serrores.oClass.CtrlMensaje = this;
                                Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, RcAdmision, tempRefParam16);
                                Serrores.oClass.CtrlMensaje = null;
                                traslado_valido = false;
								seguir_comprobar = false;
								break;
							}
						}
						RrNumocupadas1.Close();
					}
					else
					{
						// COMPROBAR SI ALGUNO DE LA HABITACION TIENE INDIVIDUAL
						//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
						RrNumocupadas1.MoveFirst();
						foreach (DataRow iteration_row_6 in RrNumocupadas1.Tables[0].Rows)
						{
							if (Convert.ToString(iteration_row_6["IREGALOJ"]) == "I" && iteration_row_6["GIDENPAC"] != RrPaciente2.Tables[0].Rows[0]["GIDENPAC"] && iteration_row_6["GIDENPAC"] != RrPaciente1.Tables[0].Rows[0]["GIDENPAC"])
							{								
								RrNumocupadas1.MoveLast(null);
								traslado_valido = false;
								seguir_comprobar = false;
								// error incompatible los regimenes								
								short tempRefParam17 = 1340;
								string[] tempRefParam18 = new string[]{};
                                Serrores.oClass.CtrlMensaje = this;
                                Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam17, RcAdmision, tempRefParam18);
                                Serrores.oClass.CtrlMensaje = null;
                                break;
							}
							else
							{								
								RrNumocupadas1.MoveLast(null);
							}
						}
						RrNumocupadas1.Close();
					}
				}

				if (seguir_comprobar)
				{
					//COMPROBAR EL SEXO
					// MIRAR SI ALGUNA DE LAS CAMAS CON RESTRICCION DE SEXO
					//MIRAR SI CUMPLE CONDICIONES EN LA HABITACION DESTINO
					//VER SI EXISTEN MAS CAMAS OCUPADAS
					//CON SEXO DISTINO AL ORIGEN					
					sqlsexo = "SELECT ITIPSEXO,IRESTSEX FROM DCAMASBOVA " + "WHERE ITIPSEXO <> '" + Convert.ToString(RrPaciente1.Tables[0].Rows[0]["itipsexo"]) + "' AND ITIPSEXO is not NULL" + " AND GPLANTAS=" + iPlanDe + " AND GHABITAC=" + iHabiDe + " AND GIDENPAC <> '" + Convert.ToString(RrPaciente1.Tables[0].Rows[0]["GIDENPAC"]) + "'" + " AND GIDENPAC <> '" + Convert.ToString(RrPaciente2.Tables[0].Rows[0]["GIDENPAC"]) + "'";
					SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(sqlsexo, RcAdmision);
					Rrsexo = new DataSet();
					tempAdapter_9.Fill(Rrsexo);
					
					Rrsexo.MoveFirst();
					foreach (DataRow iteration_row_7 in Rrsexo.Tables[0].Rows)
					{
						if (Convert.ToString(iteration_row_7["IRESTSEX"]) == "S")
						{
							// NO CUMPLE LAS RESTRICCIONES DE SEXO
							short tempRefParam19 = 1350;
							string[] tempRefParam20 = new string[]{};
                            Serrores.oClass.CtrlMensaje = this;
                            Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam19, RcAdmision, tempRefParam20);
                            Serrores.oClass.CtrlMensaje = null;
                            traslado_valido = false;
							seguir_comprobar = false;
							break;
							//            Else
							//                If RrPaciente1("ITIPSEXO") <> "I" And RrPaciente1("IRESTSEX") = "S" Then
							//                    ' NO CUMPLE LAS RESTRICCIONES DE SEXO
							//                    IResume = oClass.RespuestaMensaje(vNomAplicacion, VAyuda, 1350, RcAdmision)
							//                    traslado_valido = False
							//                    seguir_comprobar = False
							//                    Exit Do
							//                 End If
						}
					}
					Rrsexo.Close();
					if (seguir_comprobar)
					{
						//If RrPaciente2("IRESTSEX") = "S" And RrPaciente2("ITIPSEXO") <> "I" Then
						//MIRAR SI CUMPLE CONDICIONES EN LA HABITACION ORIGEN
						sqlsexo = "SELECT ITIPSEXO,IRESTSEX FROM DCAMASBOVA " + "WHERE ITIPSEXO <> '" + Convert.ToString(RrPaciente2.Tables[0].Rows[0]["itipsexo"]) + "' AND ITIPSEXO is not NULL" + " AND GPLANTAS=" + iPlanOr + " AND GHABITAC=" + iHabiOr + " AND GIDENPAC <> '" + Convert.ToString(RrPaciente1.Tables[0].Rows[0]["GIDENPAC"]) + "'" + " AND GIDENPAC <> '" + Convert.ToString(RrPaciente2.Tables[0].Rows[0]["GIDENPAC"]) + "'";
						SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(sqlsexo, RcAdmision);
						Rrsexo = new DataSet();
						tempAdapter_10.Fill(Rrsexo);
						
						Rrsexo.MoveFirst();
						foreach (DataRow iteration_row_8 in Rrsexo.Tables[0].Rows)
						{
							if (Convert.ToString(iteration_row_8["IRESTSEX"]) == "S")
							{
								// NO CUMPLE LAS RESTRICCIONES DE SEXO								
								short tempRefParam21 = 1350;
								string[] tempRefParam22 = new string[]{};
                                Serrores.oClass.CtrlMensaje = this;
                                Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam21, RcAdmision, tempRefParam22);
                                Serrores.oClass.CtrlMensaje = null;
                                traslado_valido = false;
								seguir_comprobar = false;
								break;
								//                Else
								//                    If RrPaciente2("ITIPSEXO") <> "I" And RrPaciente2("IRESTSEX") = "S" Then
								//                        ' NO CUMPLE LAS RESTRICCIONES DE SEXO
								//                        IResume = oClass.RespuestaMensaje(vNomAplicacion, VAyuda, 1350, RcAdmision)
								//                        traslado_valido = False
								//                        seguir_comprobar = False
								//                        Exit Do
								//                     End If
							}
						}
						Rrsexo.Close();
					}
				}
				RrPaciente2.Close();
			}
			RrPaciente1.Close();
			RrHabitaci1.Close();
			RrHabitaci2.Close();

			//oscar 27/11/2003
			//No se podran efectuar traslados de pacientes ingresados en una plaza de dia
			//(isillon=S) a una cama de hospitalizacion / cuna (isillon=N) y viceversa, es decir
			//traslado_valido=false cuando el indicador isillon de la cama origen y destino son diferentes.

			//***************** O.Frias (SQL-2005) *****************

			sqlsexo = " SELECT T1.ISILLON FROM " + 
			          " (SELECT ISILLON FROM DCAMASBOVA" + 
			          "    INNER JOIN DTIPOCAMVA on DTIPOCAMVA.gtipocam = DCAMASBOVA.gtipocam" + 
			          "    WHERE GPLANTAS=" + iPlanDe + " AND GHABITAC=" + iHabiDe + " AND GCAMASBO='" + stCamaDe + "'" + 
			          "    UNION" + 
			          "    SELECT ISILLON FROM DCAMASBOVA" + 
			          "    INNER JOIN DTIPOCAMVA on DTIPOCAMVA.gtipocam = DCAMASBOVA.gtipocam" + 
			          "    WHERE GPLANTAS=" + iPlanOr + " AND GHABITAC=" + iHabiOr + " AND GCAMASBO='" + stCamaOr + "')" + 
			          " T1 GROUP BY T1.ISILLON " + 
			          "ORDER BY T1.ISILLON ";

			//***************** O.Frias (SQL-2005) *****************

			SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(sqlsexo, RcAdmision);
			Rrsexo = new DataSet();
			tempAdapter_11.Fill(Rrsexo);
			if (Rrsexo.Tables[0].Rows.Count != 0)
			{				
				Rrsexo.MoveLast(null);				
				Rrsexo.MoveFirst();
				if (Rrsexo.Tables[0].Rows.Count > 1)
				{
					RadMessageBox.Show(this, "No se puede realizar un traslado entre plazas de D�a y camas.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
					traslado_valido = false;
				}
			}
			//-------------------
		}        

        public void genera_sql()
        {
            carga_planta();
            bool bOcultarColumna = false;
            bool bOcultarFila = false;

            if (Serrores.ObternerValor_CTEGLOBAL(RcAdmision, "VERCAMAS", "VALFANU3") == "S")
            {
                //Ocultamos las columnas/filas en las que no haya cama en ninguna planta
                for (int iColumna = 0; iColumna <= maxhabitaci; iColumna++)
                {
                    bOcultarColumna = true;
                    SprPlantas.Col = iColumna;
                    for (int iFila = 0; iFila < maxplanta; iFila++)
                    {
                        SprPlantas.Row = iFila;
                        if (SprPlantas.getTypePictPicture() != null)
                        {
                            bOcultarColumna = false;
                            break;
                        }
                    }
                    if (bOcultarColumna)
                    {
                        SprPlantas.SetColHidden(iColumna + 1, true);
                    }
                }

                for (int iFila = 0; iFila < maxplanta; iFila++)
                {
                    bOcultarFila = true;
                    SprPlantas.Row = iFila;
                    for (int iColumna = 0; iColumna <= maxhabitaci; iColumna++)
                    {
                        SprPlantas.Col = iColumna;
                        if (SprPlantas.getTypePictPicture() != null)
                        {
                            bOcultarFila = false;
                            break;
                        }
                    }
                    if (bOcultarFila)
                    {
                        SprPlantas.SetRowHidden(iFila, true);
                    }
                }
            }
        }

		public void genera_sql2()
		{
			string Sexo = "no";
			Estado = "(";
			if (((ChbLibre.IsChecked) ? -1 : 0) == 1)
			{
				Estado = Estado + "dcamasbova.iestcama='L'";
			}

			if (((ChbBloqueada.IsChecked) ? -1 : 0) == 1)
			{
				if (Estado != "(")
				{
					Estado = Estado + " or ";
				}
				Estado = Estado + "dcamasbova.iestcama='B'";
			}
			if (((ChbInhabilitada.IsChecked) ? -1 : 0) == 1)
			{
				if (Estado != "(")
				{
					Estado = Estado + " or ";
				}
				Estado = Estado + "dcamasbova.iestcama='I'";
			}
			if (Estado == "(")
			{
				Estado = "";
			}
			else
			{
				Estado = Estado + ")";
			}
			if (((ChbOcupada.IsChecked) ? -1 : 0) == 1)
			{
				if (ChbHombre.CheckState == CheckState.Checked)
				{
					Sexo = "si";
					if (Estado != "")
					{
						Estado = Estado + " or (";
					}
					else
					{
						Estado = "(";
					}
					Estado = Estado + "dcamasbova.iestcama='O' and dcamasbova.itipsexo='H'";
				}
				if (ChbMujer.CheckState == CheckState.Checked)
				{
					if (Estado != "")
					{
						if (Sexo == "no")
						{
							Estado = Estado + " or (";
						}
						else
						{
							Estado = Estado + " or ";
						}
					}
					else
					{
						Estado = "(";
					}
					Sexo = "si";
					Estado = Estado + "dcamasbova.iestcama='O' and dcamasbova.itipsexo='M'";
				}
				if (Sexo == "si")
				{
					Estado = Estado + ")";
				}
				else
				{
					if (Estado != "")
					{
						Estado = Estado + " or ";
					}
					Estado = Estado + "dcamasbova.iestcama='O'";
				}
			}
			//selecciona el primero de admision
			carga_planta();
		}

        private void Imseleccionada_GiveFeedback(object sender, System.Windows.Forms.GiveFeedbackEventArgs e)
        {            
            if (e.Effect == DragDropEffects.Copy)
            {
                e.UseDefaultCursors = false;
                Cursor.Current = new Cursor(((Bitmap)Imseleccionada.Image).GetHicon());
            }
            else
                Cursor.Current = System.Windows.Forms.Cursors.Default;
        }

        private void VISUALIZACION_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void Cbaceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int AcPlanta = 0;
			int AcHabitacion = 0;
			string ACCama = String.Empty;
			bool bError = false;
			if (VstTipoConsulta == "I")
			{
                //validar la cama respecto al sexo y regimen de alojamiento
                //validar que la cama este libre (HAY QUE METER EL ICONO DE CAMA LIBRE Y CUNA LIBRE Y SILLON)

                //if (!Imseleccionada.Image.Equals(IlIconos.Images[4]) && !Imseleccionada.Image.Equals(IlIconos.Images[35]) && !Imseleccionada.Image.Equals(IlIconos.Images[41]) && !Imseleccionada.Image.Equals(IlIconos.Images[49]) && !Imseleccionada.Image.Equals(IlIconos.Images[51]) && !Imseleccionada.Image.Equals(IlIconos.Images[53]))
                if (!compareBmp((Bitmap)Imseleccionada.Image, (Bitmap)IlIconos.Images[4]) && !compareBmp((Bitmap)Imseleccionada.Image, (Bitmap)IlIconos.Images[35]) && !compareBmp((Bitmap)Imseleccionada.Image, (Bitmap)IlIconos.Images[41]) && !compareBmp((Bitmap)Imseleccionada.Image, (Bitmap)IlIconos.Images[49]) && !compareBmp((Bitmap)Imseleccionada.Image, (Bitmap)IlIconos.Images[51]) && !compareBmp((Bitmap)Imseleccionada.Image, (Bitmap)IlIconos.Images[53]))
                { //oscar 27/11/2003
                  //error la cama esta ocupada
                    bError = true;
                }
                else
                {
                    if (!fnSexo())
					{
						bError = true;
						//error sexo
						short tempRefParam = 1350;
						string[] tempRefParam2 = new string[]{};
                        Serrores.oClass.CtrlMensaje = this;
                        Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcAdmision, tempRefParam2);
                        Serrores.oClass.CtrlMensaje = null;
                    }
					else
					{
						if (VstRegaPaciente != "")
						{ // COMPROBAR EL REGIMEN SOLO CUANDO LO MANDE
							if (!fnRegimenal())
							{
								bError = true;
								//error regimen
								short tempRefParam3 = 1340;
								string[] tempRefParam4 = new string[]{};
                                Serrores.oClass.CtrlMensaje = this;
                                Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RcAdmision, tempRefParam4);
                                Serrores.oClass.CtrlMensaje = null;
                            }
						}
					}
				}
			}
			if (!bError)
			{
				if (VstTipoConsulta == "M")
				{
					//salir sin mas
					this.Close();
				}
				else
				{
					if (LbDcama.Text != "")
					{
						AcPlanta = Convert.ToInt32(Double.Parse(LbDcama.Text.Substring(0, Math.Min(2, LbDcama.Text.Length))));
						AcHabitacion = Convert.ToInt32(Double.Parse(LbDcama.Text.Substring(2, Math.Min(2, LbDcama.Text.Length - 2))));
						ACCama = LbDcama.Text.Substring(4);
						FormularioRespuesta.proRecojo_Cama("A", LbDcama.Text.Trim());
					}
					else
					{
						FormularioRespuesta.proRecojo_Cama("C", "      ");
					}
					this.Close();
				}
			}

		}

		public bool fnSexo()
		{
			int tempplanta = Convert.ToInt32(Double.Parse(LbDcama.Text.Substring(0, Math.Min(2, LbDcama.Text.Length))));
			int temphabita = Convert.ToInt32(Double.Parse(LbDcama.Text.Substring(2, Math.Min(2, LbDcama.Text.Length - 2))));
			string sqltemp = "select * from dcamasbova where gplantas=" + tempplanta.ToString() + " and " + " ghabitac=" + temphabita.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqltemp, RcAdmision);
			DataSet RrTemp = new DataSet();
			tempAdapter.Fill(RrTemp);
			bool Restriccion = false;
			string tipo_sexo = " ";
			bool bVuelta = true;
			foreach (DataRow iteration_row in RrTemp.Tables[0].Rows)
			{
				if (Convert.ToString(iteration_row["irestsex"]) == "S")
				{
					Restriccion = true;
				}
				if (Convert.ToString(iteration_row["iestcama"]) == "O" || Convert.ToString(iteration_row["iestcama"]) == "B")
				{
					if (tipo_sexo != Convert.ToString(iteration_row["itipsexo"]))
					{
						if (tipo_sexo != " ")
						{
							tipo_sexo = "V";
						}
						else
						{
							tipo_sexo = Convert.ToString(iteration_row["itipsexo"]);
						}
					}
				}
			}
			RrTemp.Close();
			if (Restriccion || BRestriPaciente)
			{
				//si el paciente tiene restriccion de sexo o
				//la habitaci�n tiene restriccion de sexo
				//comparar los sexos
				if (tipo_sexo != VstSexoPaciente && tipo_sexo != " ")
				{
					bVuelta = false;
				}
			}
			return bVuelta;
		}

		public bool fnRegimenal()
		{
			int tempplanta = Convert.ToInt32(Double.Parse(LbDcama.Text.Substring(0, Math.Min(2, LbDcama.Text.Length))));
			int temphabita = Convert.ToInt32(Double.Parse(LbDcama.Text.Substring(2, Math.Min(2, LbDcama.Text.Length - 2))));
			bool bVuelta = true;
			string sqltemp = "select * from dcamasbova,aepisadm where gplantas=" + tempplanta.ToString() + " and " + " ghabitac=" + temphabita.ToString() + " and dcamasbova.gidenpac = aepisadm.gidenpac and " + " faltplan is null";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqltemp, RcAdmision);
			DataSet RrTemp = new DataSet();
			tempAdapter.Fill(RrTemp);
			string tempreg = " ";
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in RrTemp.Tables[0].Rows)
				{
					if (tempreg != Convert.ToString(iteration_row["iregaloj"]))
					{
						if (tempreg == " ")
						{
							tempreg = Convert.ToString(iteration_row["iregaloj"]);
						}
						else
						{
							tempreg = "V";
						}
					}
				}
				if (VstRegaPaciente != tempreg)
				{
					bVuelta = false;
				}
			}
			RrTemp.Close();
			return bVuelta;
		}

		private void CbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (VstTipoConsulta == "I" && Module1.vstHosUr == "H")
			{
				FormularioRespuesta.proRecojo_Cama("C", "      ");
			}
			this.Close();
		}

		private void cmdReserva_Click(Object eventSender, EventArgs eventArgs)
		{
			proLLamarFiliacion();
		}

		private bool isInitializingComponent;

		private void ChbBloqueada_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				fnactiva_filtro();
				cambio_filtro = true;
			}
		}

		private void ChbCompartido_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			cambio_filtro = true;
		}

		private void ChbHombre_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			cambio_filtro = true;
		}

		private void ChbIndividual_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			cambio_filtro = true;
		}

		private void ChbInhabilitada_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				fnactiva_filtro();
				cambio_filtro = true;
			}
		}

		private void ChbLibre_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				fnactiva_filtro();
				cambio_filtro = true;
			}
		}

		private void chbLimpieza_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				fnactiva_filtro();
				cambio_filtro = true;
			}
		}

		private void ChbMujer_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			cambio_filtro = true;
		}

		private void ChbOcupada_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				fnactiva_filtro();
				cambio_filtro = true;
			}
		}

		private void chbReservada_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				fnactiva_filtro();
				cambio_filtro = true;
			}
		}

		private void ChbTodas_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				fnactiva_filtro();
				cambio_filtro = true;
			}
		}
		private void CmbCama_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{
			cambio_filtro = true;
		}
		private void CmbCama_KeyUp(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if ((Strings.Asc(Strings.Chr(KeyCode).ToString().ToUpper()[0]) > 47 && Strings.Asc(Strings.Chr(KeyCode).ToString().ToUpper()[0]) < 58) || (Strings.Asc(Strings.Chr(KeyCode).ToString().ToUpper()[0]) > 64 && Strings.Asc(Strings.Chr(KeyCode).ToString().ToUpper()[0]) < 91))
			{
				string tempRefParam = CmbCama.Text;
				object tempRefParam2 = CmbCama;
				Module1.Buscarcombo.BusquedaCombos(tempRefParam, tempRefParam2);
				CmbCama = (RadDropDownList) tempRefParam2;
			}
		}
		private void CmbServicio_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
		{
			cambio_filtro = true;
		}

		private void CmbServicio_KeyUp(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if ((Strings.Asc(Strings.Chr(KeyCode).ToString().ToUpper()[0]) > 47 && Strings.Asc(Strings.Chr(KeyCode).ToString().ToUpper()[0]) < 58) || (Strings.Asc(Strings.Chr(KeyCode).ToString().ToUpper()[0]) > 64 && Strings.Asc(Strings.Chr(KeyCode).ToString().ToUpper()[0]) < 91))
			{
				string tempRefParam = CmbServicio.Text;
				object tempRefParam2 = CmbServicio;
				Module1.Buscarcombo.BusquedaCombos(tempRefParam, tempRefParam2);
				CmbServicio = (RadDropDownList) tempRefParam2;
			}
		}

		private void CmbUniEnfe_ClickEvent(Object eventSender, EventArgs eventArgs)
		{
			cambio_filtro = true;
		}

        // ralopezn_TODO_3_3 31/03/2016
        private void CmbUniEnfe_KeyUpEvent(Object eventSender, KeyEventArgs eventArgs)
        {
            if (((int)eventArgs.KeyCode > 47 && (int)eventArgs.KeyCode < 58) || ((int)eventArgs.KeyCode > 64 && (int)eventArgs.KeyCode < 91))
            {
                string tempRefParam = CmbUniEnfe.Text;
                object tempRefParam2 = CmbUniEnfe;
                Module1.Buscarcombo.BusquedaCombos(tempRefParam, tempRefParam2);
                CmbUniEnfe = (RadDropDownList)tempRefParam2;
            }
        }

        public void Eliminar_Click(Object eventSender, EventArgs eventArgs)
		{
			string cama = String.Empty;
			object habita = null;
			object planta = null;
			DataSet RrReserva = null;
			DataSet RrEliminar = null;
			string sql = String.Empty;
			int HABITA_IZQUI = 0;
			int PLANTA_ABAJO = 0;
			bool tstborrado = false;
			string PLANTAMOD = SprPlantas.Row.ToString();
			string HABITAMOD = SprPlantas.Col.ToString();
            try
            {
                string tempRefParam = "";
                if (!mbAcceso.PermitirEvento(RcAdmision, Module1.astrEventos, "Eliminar", tempRefParam, "Click"))
                {
                    return;
                }
                if (SPRCAMAS.Row > 0)
                {
                    SPRCAMAS.Col = 1;

                    planta = SPRCAMAS.Text.Substring(0, Math.Min(2, SPRCAMAS.Text.Length));
                    habita = SPRCAMAS.Text.Substring(2, Math.Min(2, SPRCAMAS.Text.Length - 2));
                    cama = SPRCAMAS.Text.Substring(4, Math.Min(2, SPRCAMAS.Text.Length - 4));

                    object tempRefParam2 = DateTime.Now;
                    sql = "select * from aresingrva where aresingrva.gplantas = " + Convert.ToString(planta) + " and " +
                          " aresingrva.ghabitac = " + Convert.ToString(habita) + " And aresingrva.gcamasbo = '" + cama + "' and  " +
                          " aresingrva.fpreving >= " + Serrores.FormatFechaHMS(tempRefParam2) + "";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcAdmision);
                    RrReserva = new DataSet();
                    tempAdapter.Fill(RrReserva);
                    if (RrReserva.Tables[0].Rows.Count != 0)
                    {
                        //Existe alguna reserva futura						
                        short tempRefParam3 = 3420;
                        string[] tempRefParam4 = new string[] { "la cama " + Convert.ToString(planta) + Convert.ToString(habita) + cama, "el " + Convert.ToDateTime(RrReserva.Tables[0].Rows[0]["fpreving"]).ToString("dd/MM/yyyy HH:mm") };
                        Serrores.oClass.CtrlMensaje = this;
                        Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RcAdmision, tempRefParam4);
                        Serrores.oClass.CtrlMensaje = null;
                    }
                    else
                    {
                        //basura = MsgBox("Se eliminar� la cama", vbOKCancel)						
                        short tempRefParam5 = 1050;
                        string[] tempRefParam6 = new string[] { };
                        Serrores.oClass.CtrlMensaje = this;
                        Serrores.iresume = (int)Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, RcAdmision, tempRefParam6);
                        Serrores.oClass.CtrlMensaje = null;
                        if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.OK)
                        {
                            //Eliminar la cama
                            SPRCAMAS.Col = 1;
                            planta = SPRCAMAS.Text.Substring(0, Math.Min(2, SPRCAMAS.Text.Length));
                            habita = SPRCAMAS.Text.Substring(2, Math.Min(2, SPRCAMAS.Text.Length - 2));
                            cama = SPRCAMAS.Text.Substring(4, Math.Min(2, SPRCAMAS.Text.Length - 4));

                            sql = "select * from dcamasbo where " + "gplantas =" + Convert.ToString(planta) + " and ghabitac =" + Convert.ToString(habita) + " and gcamasbo='" + cama + "'";
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RcAdmision);
                            RrEliminar = new DataSet();
                            
                            tempAdapter_2.Fill(RrEliminar);
                            DataSet dsCopy = RrEliminar.Copy();
                            try
                            {
                                RrEliminar.Delete(tempAdapter_2);
                            }
                            catch
                            {
                                if (!tstborrado)
                                {
                                    RrEliminar = dsCopy;
                                    tstborrado = true;
                                }
                            }
                            if (tstborrado)
                            {
                                RrEliminar.Tables[0].Rows[0]["fborrado"] = DateTime.Now;
                                string tempQuery = RrEliminar.Tables[0].TableName;
                                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                                tempAdapter_2.Update(RrEliminar, tempQuery);
                                tstborrado = false;
                            }
                            RrEliminar.Close();
                            refresco = true;
                            
                            SprPlantas.GetBottomRightCell(HABITA_IZQUI, PLANTA_ABAJO);
                            cambio_filtro = true;
                            genera_sql();
                            //SprPlantas.Row = HABITA_IZQUI
                            //SprPlantas.Col = PLANTA_ABAJO
                            //SprPlantas.Action = 0
                            //SprPlantas.Action = 1
                            LbDcama.Text = "";
                            LbNombre.Text = "";
                            LbApellido1.Text = "";
                            LbApellido2.Text = "";
                            Imseleccionada.Image = blanco;                                                        
                            plantasel = Convert.ToInt32(Double.Parse(PLANTAMOD) +1);
                            habitasel = Convert.ToInt32(Double.Parse(HABITAMOD));

                            planta = plantasel;
                            habita = habitasel;
                            carga_camas();
                        }
                    }
                    RrReserva.Close();
                }
            }
            catch
            {

            }		
		}


		//***********************************************************************************************************
		//*                                                                                                          *
		//*  Procedimiento: Form_Activate                                                                            *
		//*                                                                                                          *
		//*  Modificaci�n:                                                                                           *
		//*                                                                                                          *
		//*    O.Frias (13/11/2006) Proceso para ajustar el tama�o de la pantalla segun la resolucion del monitor.   *
		//*                                                                                                          *
		//************************************************************************************************************
		private void VISUALIZACION_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				Frame5.Top = (int) (this.ClientRectangle.Height - Frame5.Height);
				Frame3.Top = (int) (this.ClientRectangle.Height - Frame3.Height - Frame5.Height);

				Frame1.Width = (int) (this.Width - 10);
				Frame3.Width = (int) (this.Width - 10);
				Frame4.Width = (int) (this.Width - 10);
				Frame5.Width = (int) (this.Width - 10);

				Frame4.Height = (int) (Frame3.Top - Frame4.Top);

				SprPlantas.Width = (int) (Frame4.Width - 7 - SprPlantas.Left);
				SprPlantas.Height = (int) ((Frame4.Height - SprPlantas.Top) - 7);
				LbHabitacion.Width = (int) (Frame4.Width - 7 - LbHabitacion.Left);

				cmdReserva.Enabled = false;
			}
		}

		//************************************************************************************************
		//*                                                                                              *
		//*  Procedimiento: Form_Load                                                                    *
		//*                                                                                              *
		//*  Modificaci�n:                                                                               *
		//*                                                                                              *
		//*    O.Frias (13/11/2006) Dependiedo del valor de la variable blnReserva se muestra u oculta   *
		//*                         el boton de Reserva.                                                  *
		//************************************************************************************************
		private void VISUALIZACION_Load(Object eventSender, EventArgs eventArgs)
		{
			//recoge si es urgencias u hospitalizaci�n

			if (Module1.vstHosUr == "")
			{
				Module1.vstHosUr = "H";
			}
			if (!mbAcceso.AplicarControlAcceso(Module1.VstCodUsua, "A", this, "AGI310F1", ref Module1.astrControles, ref Module1.astrEventos, RcAdmision))
			{
                CbCerrar.PerformClick();
			}
			chbReservada.Visible = Module1.vstHosUr != "U";
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			inicio_columna = 0; //para columna inicio cero
			// PARA COLUMNA INCIO -1 EL VALOR SERA 2
			// ASI SUCESIVAMENTE

			//RECOGER EL NUMERO MAXIMO DE PLANTAS DEL HOSPITAL
			string sqlmaxplan = "select count(gplantas)as maximo from dplantasva";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlmaxplan, RcAdmision);
			RrPlantas = new DataSet();
			tempAdapter.Fill(RrPlantas);
			maxplanta = Convert.ToInt32(RrPlantas.Tables[0].Rows[0]["maximo"]);
			//RECOGER EL NUMERO MAXIMO DE HABITACIONES POR PLANTA DEL HOSPITAL
			sqlmaxplan = "select max(ghabitac)as maximocol from dcamasbova";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlmaxplan, RcAdmision);
			RrPlantas = new DataSet();
			tempAdapter_2.Fill(RrPlantas);
			maxhabitaci = Convert.ToInt32((!Convert.IsDBNull(RrPlantas.Tables[0].Rows[0]["maximocol"])) ? Convert.ToDouble(RrPlantas.Tables[0].Rows[0]["maximocol"]) + 1 : 1);
			SprPlantas.MaxRows = maxplanta;
			SprPlantas.MaxCols = maxhabitaci;
			rojo = IlIconos.Images[5];
			verde = IlIconos.Images[4];
			azul = IlIconos.Images[7];
			amarillo = IlIconos.Images[3];
            blanco = new PictureBox().Image;
			naranja = IlIconos.Images[6];
			rojo2 = IlIconos.Images[16];
			verde2 = IlIconos.Images[15];
			azul2 = IlIconos.Images[14];
			amarillo2 = IlIconos.Images[13];
			ImaOtroserv = IlIconos.Images[18];

            _Imcama_0.Image = IlIconos.Images[4];
            _Imcama_1.Image = IlIconos.Images[5];
            _Imcama_2.Image = IlIconos.Images[3];
            _Imcama_3.Image = IlIconos.Images[7];
            _Imcama_4.Image = IlIconos.Images[6];
            _Imcama_5.Image = IlIconos.Images[18];
            // pictures de cunas
            _Imcama_6.Image = IlIconos.Images[35]; // libres
            _Imcama_7.Image = IlIconos.Images[39]; // ocupadas
            _Imcama_8.Image = IlIconos.Images[31]; // bloqueadas
            _Imcama_9.Image = IlIconos.Images[33]; // inhabilitadas
            _Imcama_10.Image = IlIconos.Images[37]; // distintos estados

            //oscar 27/11/2003
            //pictures de sillones
            _Imcama_11.Image = IlIconos.Images[41]; // libres
            _Imcama_12.Image = IlIconos.Images[43]; // ocupadas
            _Imcama_13.Image = IlIconos.Images[47]; // bloqueadas
            _Imcama_14.Image = IlIconos.Images[45]; // inhabilitadas
            _Imcama_15.Image = IlIconos.Images[48]; // distintos estados
			//-------

			crojo = (0x8080FF).ToString();
			cverde = (0x80FF80).ToString();
			cazul = (0xFFFF00).ToString();
			camarillo = (0x80FFFF).ToString();
			cblanco = (0xFFFFFF).ToString();
			cnaranja = (0x80C0FF).ToString();
			cvioleta = (0xFF8080).ToString();
			LbCamas.BackColor = ColorTranslator.FromOle(Convert.ToInt32(Double.Parse(cvioleta)));
			LbPlanta.BackColor = ColorTranslator.FromOle(Convert.ToInt32(Double.Parse(cvioleta)));
			LbPacientes.BackColor = ColorTranslator.FromOle(Convert.ToInt32(Double.Parse(cvioleta)));
			ESTADO_CAMA = 0;
			plantasel = 1;
			habitasel = 1;
			refresco = false;
			cambio_filtro = true;
			ChbTodas.IsChecked = true;

			//oscar 27/11/2003
			//Dependiendo de los valores de la constante global VERCAMAS,
			//ocultamos las cunas y/o sillones

			string sql = "select * FROM SCONSGLO WHERE GCONSGLO='VERCAMAS'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, RcAdmision);
			RrPlantas = new DataSet();
			tempAdapter_3.Fill(RrPlantas);
			Module1.bVerCunas = Convert.ToString(RrPlantas.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper() == "S";
			Module1.bVerPlazasHD = Convert.ToString(RrPlantas.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S";
			RrPlantas.Close();

            //CUNAS
            _Imcama_6.Visible = Module1.bVerCunas; //libre
            _Imcama_7.Visible = Module1.bVerCunas; //ocupada
            _Imcama_8.Visible = Module1.bVerCunas; //bloqueada
            _Imcama_9.Visible = Module1.bVerCunas; //inhabilitada
            _Imcama_10.Visible = Module1.bVerCunas; //distintos estados
			Label2.Visible = Module1.bVerCunas;
			Label3.Visible = Module1.bVerCunas;
			Label4.Visible = Module1.bVerCunas;
			Label5.Visible = Module1.bVerCunas;
			Label6.Visible = Module1.bVerCunas;
			Label7.Visible = Module1.bVerCunas;

            //SILLONES
            _Imcama_11.Visible = Module1.bVerPlazasHD; //libre
            _Imcama_12.Visible = Module1.bVerPlazasHD; //ocupado
            _Imcama_13.Visible = Module1.bVerPlazasHD; //bloqueado
            _Imcama_14.Visible = Module1.bVerPlazasHD; //inhabilitado
            _Imcama_15.Visible = Module1.bVerPlazasHD; //distintos estados
			Label8.Visible = Module1.bVerPlazasHD;
			Label9.Visible = Module1.bVerPlazasHD;
			Label10.Visible = Module1.bVerPlazasHD;
			Label11.Visible = Module1.bVerPlazasHD;
			Label12.Visible = Module1.bVerPlazasHD;
			Label13.Visible = Module1.bVerPlazasHD;
			//----------------

			genera_sql();

			//******************* O.Frias (14/11/2006) Inicio *******************
			cmdReserva.Visible = blnReserva;
			//******************** O.Frias (14/11/2006) Fin  ********************
		}

		//''oscar 27/11/2003
		//''AL PARECER NO SE USA EL PROCEDIMIENTO CARGA_PLANTA3() EN TODO EL PROYECTO
		//''P0R LO QUE NO SE HA ACTUALIZADO CON EL NUEVO REQUERIMIENTO DE PINTAR LOS ICONOS
		//''CORRESPONDIENTE A LAS PLAZAS DE DIA.
		//''---------
		//'Sub carga_planta3()
		//'Dim tRringreso2 As rdoResultset
		//'Dim tsqlplanta2 As String
		//'Dim color As Variant
		//'Dim color2 As Variant
		//'Dim Y As Integer
		//'Dim X As Integer
		//'Dim vstSalirFor As Boolean
		//'Me.MousePointer = 11
		//'SprPlantas.Row = 0
		//'SprPlantas.MaxRows = 0
		//'SprPlantas.MaxCols = 0
		//''RECOGER EL NUMERO MAXIMO DE PLANTAS DEL HOSPITAL
		//'sqlmaxplan = "select count(gplantas)as maximo from dplantasva"
		//'Set RrPlantas = RcAdmision.OpenResultset(sqlmaxplan, rdOpenKeyset, rdConcurReadOnly)
		//'maxplanta = RrPlantas("maximo")
		//''RECOGER EL NUMERO MAXIMO DE HABITACIONES POR PLANTA DEL HOSPITAL
		//'sqlmaxplan = "select max(ghabitac)as maximocol from dcamasbova"
		//'Set RrPlantas = RcAdmision.OpenResultset(sqlmaxplan, rdOpenKeyset, rdConcurReadOnly)
		//'maxhabitaci = RrPlantas("maximocol") + 1 + inicio_columna
		//'SprPlantas.MaxRows = maxplanta
		//'SprPlantas.MaxCols = maxhabitaci
		//''SprPlantas.StartingColNumber = -1
		//''obtiene las plantas
		//'sqlmaxplan = "select gplantas from dplantasva order by gplantas"
		//'Set RrPlantas = RcAdmision.OpenResultset(sqlmaxplan, rdOpenKeyset, rdConcurReadOnly)
		//'
		//''vacia el grid de imagenes
		//'For X = 1 To maxplanta
		//'    For Y = 1 To maxhabitaci
		//'        SprPlantas.Row = X
		//'        SprPlantas.Col = Y
		//'        SprPlantas.TypePictPicture = LoadPicture()
		//'        SprPlantas.TypePictStretch = True
		//'    Next
		//'Next
		//'
		//''rellena las cabeceras de habitaci�n
		//'SprPlantas.Row = 0
		//'For X = 1 To maxhabitaci
		//'    SprPlantas.Col = X
		//'    SprPlantas.Text = X - inicio_columna
		//'Next
		//''vacia la cabecera de la �ltima habitaci�n
		//'SprPlantas.Col = maxhabitaci
		//'SprPlantas.Text = " "
		//'
		//'
		//''rellena las habitaciones con las imagenes
		//'X = 1
		//'RrPlantas.MoveFirst
		//'SprPlantas.Col = 0
		//'While Not RrPlantas.EOF
		//'
		//'    sqlplanta = "SELECT ghabitac,iestcama,dunienfeva.itiposer FROM dcamasbova,dunienfeva " _
		//''        & " where  " _
		//''        & " gcounien=gunidenf and gplantas= " & RrPlantas("gplantas")
		//'    Set Rringreso = RcAdmision.OpenResultset(sqlplanta, rdOpenKeyset, rdConcurReadOnly)
		//'    iMaxFilas = Rringreso.RowCount    'Cuento cuantos registros hay y seran el n�mero de columnas
		//'    Rringreso.MoveFirst
		//'    SprPlantas.Row = X
		//'    SprPlantas.Col = 0
		//'    SprPlantas.Text = RrPlantas("gplantas")
		//'    While Not Rringreso.EOF
		//'        SprPlantas.Col = Rringreso("ghabitac") + inicio_columna
		//'        estcama = UCase(Rringreso("iestcama"))
		//'        If IsNull(estcama) Then
		//'            'SprPlantas.Text = " "
		//'            SprPlantas.TypePictPicture = LoadPicture()
		//'        Else
		//'            'SprPlantas.Text = estcama
		//'            If Rringreso("itiposer") <> vstHosUr Then
		//'                SprPlantas.TypePictPicture = IlIconos.ListImages(19).Picture
		//'                SprPlantas.Text = Rringreso("itiposer")
		//'            Else
		//'                If SprPlantas.TypePictPicture = blanco Then
		//'                    color = Verifica_habitacion(Rringreso("ghabitac"), RrPlantas("gplantas"))
		//'                    Select Case color
		//'                        Case verde
		//'                           SprPlantas.TypePictPicture = IlIconos.ListImages(5).Picture
		//'                        Case rojo
		//'                           SprPlantas.TypePictPicture = IlIconos.ListImages(6).Picture
		//'                        Case amarillo
		//'                           SprPlantas.TypePictPicture = IlIconos.ListImages(4).Picture
		//'                        Case azul
		//'                           SprPlantas.TypePictPicture = IlIconos.ListImages(8).Picture
		//'                        Case naranja
		//'                           SprPlantas.TypePictPicture = IlIconos.ListImages(7).Picture
		//'                        Case blanco
		//'                           SprPlantas.TypePictPicture = LoadPicture()
		//'                    End Select
		//'                End If
		//'            End If
		//'        End If
		//'        Rringreso.MoveNext
		//'    Wend
		//'    RrPlantas.MoveNext
		//'    X = X + 1
		//'Wend
		//'vstSalirFor = False
		//'If cambio_filtro = True Then
		//'    For X = 1 To maxplanta
		//'        For Y = 1 To maxhabitaci
		//'            SprPlantas.Col = Y
		//'            SprPlantas.Row = X
		//'            If SprPlantas.TypePictPicture <> blanco And SprPlantas.TypePictPicture <> IlIconos.ListImages(19).Picture Then
		//'                vstSalirFor = True
		//'                Exit For
		//'            End If
		//'        Next
		//'        If vstSalirFor = True Then
		//'            Exit For
		//'        End If
		//'    Next
		//'    If vstSalirFor = True Then
		//'        'habitasel = rrGenera("ghabitac") '+ inicio_columna
		//'        'plantasel = fncamafila(rrGenera("gplantas"))
		//'        habitasel = Y - inicio_columna
		//'        plantasel = X
		//'    Else
		//'        habitasel = -1
		//'        plantasel = -1
		//'    End If
		//'    cambio_filtro = False
		//'End If
		//'carga_camas
		//'Me.MousePointer = 0
		//'End Sub

        /// <summary>
        /// ralopezn TODO_X_3 13/04/2016
        /// Este m�todo se utiliza desde otra funcionalidad
        /// </summary>
        /// <param name="Phabitacion"></param>
        /// <param name="Pplanta"></param>
        /// <returns></returns>
		public object Verifica_habitacion(int Phabitacion, int Pplanta)
		{
			object result = null;


			//***************** O.Frias (SQL-2005) *****************

			string sqlHabitacion = "select iestcama,itipsexo,iregaloj,irestsex from " + " dcamasbova,dunienfeva,aepisadm " + " where ghabitac=" + Phabitacion.ToString() + " and " + " gplantas=" + Pplanta.ToString() + " and " + " (aepisadm.gidenpac=*dcamasbova.gidenpac and " + " aepisadm.faltplan is null and iestcama is not null) and " + " gcounien=gunidenf and itiposer= '" + Module1.vstHosUr + "'";


			sqlHabitacion = "SELECT dcamasbova.iestcama, dcamasbova.itipsexo, AEPISADM.iregaloj, dcamasbova.irestsex " + 
			                "From dcamasbova " + 
			                "LEFT OUTER JOIN AEPISADM ON dcamasbova.gidenpac = AEPISADM.gidenpac AND " + 
			                "AEPISADM.faltplan IS NULL " + 
			                "INNER JOIN DUNIENFEVA ON dcamasbova.gcounien = DUNIENFEVA.gunidenf " + 
			                "WHERE (dcamasbova.ghabitac = " + Phabitacion.ToString() + " ) AND " + 
			                "(dcamasbova.gplantas = " + Pplanta.ToString() + " ) AND " + 
			                "(dcamasbova.iestcama IS NOT NULL) AND " + 
			                "(DUNIENFEVA.itiposer = '" + Module1.vstHosUr + "')";


			//***************** O.Frias (SQL-2005) *****************

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlHabitacion, RcAdmision);
			DataSet rrHabitacion = new DataSet();
			tempAdapter.Fill(rrHabitacion);
			//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
			rrHabitacion.MoveFirst();
			string vstRestSexo = "no";
			string vstSexo = "ninguno";
			string vstAloj = "ninguno";
			string vstEstado = "ninguno";
			foreach (DataRow iteration_row in rrHabitacion.Tables[0].Rows)
			{
				if (Convert.ToString(iteration_row["irestsex"]) == "S")
				{
					vstRestSexo = "si";
				}
				
				if (!Convert.IsDBNull(iteration_row["iregaloj"]))
				{
					if (vstAloj == "ninguno")
					{
						vstAloj = Convert.ToString(iteration_row["iregaloj"]);
					}
					else
					{
						if (vstAloj != Convert.ToString(iteration_row["iregaloj"]))
						{
							vstAloj = "varios";
						}
					}
				}
				
				if (!Convert.IsDBNull(iteration_row["iestcama"]))
				{
					if (vstEstado == "ninguno")
					{
						vstEstado = Convert.ToString(iteration_row["iestcama"]);
					}
					else
					{
						if ((vstEstado.IndexOf(iteration_row["iestcama"].ToString()) + 1) == 0)
						{
							vstEstado = vstEstado + Convert.ToString(iteration_row["iestcama"]);
						}
					}
				}
				
				if (!Convert.IsDBNull(iteration_row["itipsexo"]))
				{
					if (vstSexo == "ninguno")
					{
						vstSexo = Convert.ToString(iteration_row["itipsexo"]);
					}
					else
					{
						if (vstSexo != Convert.ToString(iteration_row["itipsexo"]))
						{
							vstSexo = vstSexo + Convert.ToString(iteration_row["itipsexo"]);
						}
					}
				}
			}
			object vstVuelta = blanco;
			if (!fnmarcado_alguno())
			{
				if (vstEstado != "ninguno")
				{
					if (vstEstado.Length > 1)
					{
						vstVuelta = naranja;
					}
					else
					{
						switch(vstEstado)
						{
							case "L" : 
								vstVuelta = verde; 
								break;
							case "O" : 
								vstVuelta = rojo; 
								break;
							case "I" : 
								vstVuelta = azul; 
								break;
							case "B" : 
								vstVuelta = amarillo; 
								break;
						}
					}
				}
			}
			else
			{
				if (vstRestSexo == "si")
				{					
					rrHabitacion.MoveFirst();
					foreach (DataRow iteration_row_2 in rrHabitacion.Tables[0].Rows)
					{						
						if (!Convert.IsDBNull(iteration_row_2["itipsexo"]))
						{
							if (vstRestSexo == "si")
							{
								vstRestSexo = Convert.ToString(iteration_row_2["itipsexo"]);
							}
							else
							{
								if (vstRestSexo != Convert.ToString(iteration_row_2["itipsexo"]))
								{
									vstRestSexo = "varios";
								}
							}
						}
					}
				}

				vstVuelta = blanco;

				//ESTADO LIBRE
				if (ChbLibre.IsChecked)
				{
					string tempRefParam = "L";
					vstVuelta = fnestado_habita(ref vstEstado, ref tempRefParam, ref vstRestSexo, ref vstAloj, ref vstSexo);
				}

				//ESTADO ocupada
				if (ChbOcupada.IsChecked)
				{
					string tempRefParam2 = "O";
					vstVuelta = fnestado_habita(ref vstEstado, ref tempRefParam2, ref vstRestSexo, ref vstAloj, ref vstSexo);
				}

				//ESTADO bloqueadas
				if (ChbBloqueada.IsChecked)
				{
					string tempRefParam3 = "B";
					vstVuelta = fnestado_habita(ref vstEstado, ref tempRefParam3, ref vstRestSexo, ref vstAloj, ref vstSexo);
				}

				//ESTADO inhabilitada
				if (ChbInhabilitada.IsChecked)
				{
					string tempRefParam4 = "I";
					vstVuelta = fnestado_habita(ref vstEstado, ref tempRefParam4, ref vstRestSexo, ref vstAloj, ref vstSexo);
				}
			}
			result = vstVuelta;
			rrHabitacion.Close();
			return result;
		}

		//oscar 27/11/2003
		//AL PARECER NO SE USA EL PROCEDIMIENTO CARGA_PLANTAOLD() EN TODO EL PROYECTO
		//P0R LO QUE NO SE HA ACTUALIZADO CON EL NUEVO REQUERIMIENTO DE PINTAR LOS ICONOS
		//CORRESPONDIENTE A LAS PLAZAS DE DIA.
		//---------
		//'Sub carga_plantaold()
		//'Dim tRringreso2 As rdoResultset
		//'Dim tsqlplanta2 As String
		//'Dim color As Variant
		//'Dim color2 As Variant
		//'Dim Y As Integer
		//'Dim X As Integer
		//'Dim sqlGenera As String
		//'Dim rrGenera As rdoResultset
		//'Me.MousePointer = 11
		//'SprPlantas.Row = 0
		//'SprPlantas.MaxRows = 0
		//'SprPlantas.MaxCols = 0
		//''RECOGER EL NUMERO MAXIMO DE PLANTAS DEL HOSPITAL
		//'sqlmaxplan = "select count(gplantas)as maximo from dplantasva"
		//'Set RrPlantas = RcAdmision.OpenResultset(sqlmaxplan, rdOpenKeyset, rdConcurReadOnly)
		//'maxplanta = RrPlantas("maximo")
		//''RECOGER EL NUMERO MAXIMO DE HABITACIONES POR PLANTA DEL HOSPITAL
		//'sqlmaxplan = "select max(ghabitac)as maximocol from dcamasbova"
		//'Set RrPlantas = RcAdmision.OpenResultset(sqlmaxplan, rdOpenKeyset, rdConcurReadOnly)
		//'maxhabitaci = RrPlantas("maximocol") + 1 + inicio_columna
		//'SprPlantas.MaxRows = maxplanta
		//'SprPlantas.MaxCols = maxhabitaci
		//''SprPlantas.StartingColNumber = -1
		//''obtiene las plantas
		//'sqlmaxplan = "select gplantas from dplantasva order by gplantas"
		//'Set RrPlantas = RcAdmision.OpenResultset(sqlmaxplan, rdOpenKeyset, rdConcurReadOnly)
		//'
		//''vacia el grid de imagenes
		//'For X = 1 To maxplanta
		//'    For Y = 1 To maxhabitaci
		//'        SprPlantas.Row = X
		//'        SprPlantas.Col = Y
		//'        SprPlantas.TypePictPicture = LoadPicture()
		//'        SprPlantas.TypePictStretch = True
		//'    Next
		//'Next
		//'
		//''rellena las cabeceras de habitaci�n
		//'SprPlantas.Row = 0
		//'For X = 1 To maxhabitaci
		//'    SprPlantas.Col = X
		//'    SprPlantas.Text = X - inicio_columna
		//'Next
		//''vacia la cabecera de la �ltima habitaci�n
		//'SprPlantas.Col = maxhabitaci
		//'SprPlantas.Text = " "
		//'
		//'
		//''rellena las habitaciones con las imagenes
		//'X = 1
		//'RrPlantas.MoveFirst
		//'SprPlantas.Col = 0
		//'While Not RrPlantas.EOF
		//''For X = 1 To maxplanta
		//'    ' CONSULTAR LA TABLA DE CAMAS POR PLANTAS
		//'    '& " where DUNIENFEva.ITIPOSER='" & vstHosUr & "' " _
		//''
		//'     sqlplanta = "SELECT ghabitac,iestcama,dunienfeva.itiposer FROM dcamasbova,dunienfeva " _
		//''        & " where  " _
		//''        & " gcounien=gunidenf and gplantas= " & RrPlantas("gplantas")
		//'    If Estado <> "" Then
		//'        sqlplanta = sqlplanta & " and (" & Estado & ")"
		//'    End If
		//'    Set Rringreso = RcAdmision.OpenResultset(sqlplanta, rdOpenKeyset, rdConcurReadOnly)
		//'    iMaxFilas = Rringreso.RowCount    'Cuento cuantos registros hay y seran el n�mero de columnas
		//'    '***********************************************
		//'    'comprobar que hay mas estados en la habitaci�n
		//'    If Estado <> "" And Rringreso.RowCount > 0 Then
		//'        '& " where DUNIENFEva.ITIPOSER='" & vstHosUr & "' "
		//'        sqlplanta = "SELECT ghabitac,iestcama,dunienfeva.itiposer " _
		//''            & " FROM dcamasbova,dunienfeva " _
		//''            & " where " _
		//''            & " gcounien=gunidenf and gplantas= " & RrPlantas("gplantas") _
		//''            & " and ghabitac in " _
		//''            & " (SELECT ghabitac From dcamasbova " _
		//''            & " where  " _
		//''            & "  gplantas= " & RrPlantas("gplantas") _
		//''            & " and (" & Estado & "))"
		//'        Set Rringreso = RcAdmision.OpenResultset(sqlplanta, rdOpenKeyset, rdConcurReadOnly)
		//'    End If
		//'    '***********************************************
		//'    Rringreso.MoveFirst
		//'    SprPlantas.Row = X
		//'    SprPlantas.Col = 0
		//'    SprPlantas.Text = RrPlantas("gplantas")
		//'    While Not Rringreso.EOF
		//'        SprPlantas.Col = Rringreso("ghabitac") + inicio_columna
		//'        estcama = UCase(Rringreso("iestcama"))
		//'        If IsNull(estcama) Then
		//'            'SprPlantas.Text = " "
		//'            SprPlantas.TypePictPicture = LoadPicture()
		//'        Else
		//'            'SprPlantas.Text = estcama
		//'            If Rringreso("itiposer") <> vstHosUr Then
		//'                SprPlantas.TypePictPicture = IlIconos.ListImages(19).Picture
		//'                SprPlantas.Text = Rringreso("itiposer")
		//'            Else
		//'                color = SprPlantas.TypePictPicture
		//'                Select Case estcama
		//'                    Case "L"
		//'                        If color <> blanco And color <> verde Then
		//'                            SprPlantas.TypePictPicture = IlIconos.ListImages(7).Picture
		//'                        Else
		//'                            SprPlantas.TypePictPicture = IlIconos.ListImages(5).Picture
		//'                        End If
		//'                    Case "O"
		//'                        If color <> blanco And color <> rojo Then
		//'                            SprPlantas.TypePictPicture = IlIconos.ListImages(7).Picture
		//'                        Else
		//'                            SprPlantas.TypePictPicture = IlIconos.ListImages(6).Picture
		//'                        End If
		//'                   Case "I"
		//'                        If color <> blanco And color <> azul Then
		//'                            SprPlantas.TypePictPicture = IlIconos.ListImages(7).Picture
		//'                        Else
		//'                            SprPlantas.TypePictPicture = IlIconos.ListImages(8).Picture
		//'                        End If
		//'                  Case "B"
		//'                        If color <> blanco And color <> amarillo Then
		//'                            SprPlantas.TypePictPicture = IlIconos.ListImages(7).Picture
		//'                        Else
		//'                            SprPlantas.TypePictPicture = IlIconos.ListImages(4).Picture
		//'                        End If
		//'                End Select
		//'            End If
		//'        End If
		//'        Rringreso.MoveNext
		//'    Wend
		//'    RrPlantas.MoveNext
		//'    X = X + 1
		//'Wend
		//'If cambio_filtro = True Then
		//'    If Estado = "" Then
		//'        sqlGenera = "SELECT ghabitac,gplantas,iestcama,dunienfeva.itiposer " _
		//''            & " FROM dcamasbova,dunienfeva " _
		//''            & " where " _
		//''            & " gcounien=gunidenf and itiposer= '" & vstHosUr & "'" _
		//''            & " order by gplantas,ghabitac"
		//'    Else
		//'        sqlGenera = "SELECT ghabitac,gplantas,iestcama,dunienfeva.itiposer " _
		//''            & " FROM dcamasbova,dunienfeva " _
		//''            & " where " _
		//''            & " gcounien=gunidenf and itiposer= '" & vstHosUr & "'" _
		//''            & " and (" & Estado & ")" _
		//''            & " order by gplantas,ghabitac"
		//'    End If
		//'    Set rrGenera = RcAdmision.OpenResultset(sqlGenera, rdOpenKeyset, rdConcurReadOnly)
		//'    If Not rrGenera.EOF Then
		//'        rrGenera.MoveFirst
		//'        habitasel = rrGenera("ghabitac") '+ inicio_columna
		//'        plantasel = fncamafila(rrGenera("gplantas"))
		//'        cambio_filtro = False
		//'    End If
		//'    rrGenera.Close
		//'End If
		//'carga_camas
		//'Me.MousePointer = 0
		//'
		//'End Sub

		public void carga_camas()
		{
            SPRCAMAS.BeginUpdate();
            string estcama = String.Empty;
            int iMaxFilas = 0;
            string sqlPaciente = String.Empty;
            DataSet Rrcama = null; // CAMAS
            DataSet rrPaciente = null; // PACIENTES
            string sqlcamas = String.Empty; // CONSULTA CAMAS
            StringBuilder tstPaciente = new StringBuilder(); //Nombre paciente
            string stEdad = String.Empty;

            string Lsql = String.Empty;
            DataSet LRR = null;

            //TAMA�O DE LA COLUMNAS
            //sprcamas.Font=rgf
            //SPRCAMAS.FontSize = 8

            SPRCAMAS.MaxRows = 0;
            SPRCAMAS.MaxCols = 9; //7 '6  oscar 27/11/2003
            SPRCAMAS.MasterTemplate.Columns[3] = new GridViewImageColumn();
            SPRCAMAS.SetColWidth(1, 800);
            SPRCAMAS.SetColWidth(2, 1400);
            SPRCAMAS.SetColWidth(3, 3000);
            SPRCAMAS.SetColWidth(4, 660);
            SPRCAMAS.SetColWidth(5, 550);
            SPRCAMAS.SetColWidth(6, 2000);

            //marta
            //SPRCAMAS.ColWidth(0) = 650
            //SPRCAMAS.ColWidth(1) = 1150
            //SPRCAMAS.ColWidth(2) = 2700
            //SPRCAMAS.ColWidth(3) = 500
            //SPRCAMAS.ColWidth(4) = 450
            //SPRCAMAS.ColWidth(5) = 1100
            SPRCAMAS.Col = 7;
            //SPRCAMAS.CellType
            SPRCAMAS.SetColHidden(SPRCAMAS.Col, true); //si es una cuna o una cama

            //oscar 27/11/2003
            SPRCAMAS.Col = 8;
            SPRCAMAS.SetColHidden(SPRCAMAS.Col, true); //si es una plaza de dia (Sillon)
            SPRCAMAS.Col = 9;
            SPRCAMAS.SetColHidden(SPRCAMAS.Col, true); //si TIENE RESERVA
            SPRCAMAS.Col = 10;
            SPRCAMAS.SetColHidden(SPRCAMAS.Col, true); //si ESTA INHABILITADA POR MOTIVO ESPECIAL (CONSTANTE INHABALT)
                                                       //------

            int suma = 350;
            suma = Convert.ToInt32(suma + SPRCAMAS.Columns[1].Width + SPRCAMAS.Columns[2].Width + SPRCAMAS.Columns[3].Width);
            suma = Convert.ToInt32(suma + SPRCAMAS.Columns[4].Width + SPRCAMAS.Columns[5].Width + SPRCAMAS.Columns[6].Width);
            SPRCAMAS.Width = (int)(suma / 15);
            LbPacientes.Width = (int)((suma - SPRCAMAS.Columns[1].Width) / 15);
            LbPacientes.Left = (int)(SPRCAMAS.Left + SPRCAMAS.Columns[1].Width / 15);
            //CABECERA DEL GRID
            SPRCAMAS.Row = 0;
            SPRCAMAS.Col = 1;
            SPRCAMAS.Text = "Cama";
            SPRCAMAS.Col = 2;
            SPRCAMAS.Text = "Estado";
            SPRCAMAS.Col = 3;
            string LitPersona = Serrores.ObtenerLiteralPersona(RcAdmision);
            LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
            SPRCAMAS.Text = LitPersona;
            SPRCAMAS.Col = 4;
            SPRCAMAS.Text = "Sexo";

            SPRCAMAS.Col = 5;
            SPRCAMAS.Text = "Edad";
            SPRCAMAS.Col = 6;
            SPRCAMAS.Text = "Alojamiento";
            //SPRCAMAS.MaxCols = 6
            if (habitasel != -1 && plantasel != -1)
            {
                sqlcamas = "select gcamasbo,gplantas,ghabitac,gidenpac,iestcama,isnull(gmotinha,0) as gmotinha, dtipocam.iescuna,dtipocam.isillon " + " from dcamasbova,dtipocam Where gplantas = " + fnfilacama(plantasel, habitasel).ToString() + " and  ghabitac= " + (habitasel - 1).ToString() + " and dtipocam.gtipocam = dcamasbova.gtipocam";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlcamas, RcAdmision);
                Rrcama = new DataSet();
                tempAdapter.Fill(Rrcama);
                iMaxFilas = Rrcama.Tables[0].Rows.Count; //Cuento cuantos registros hay y seran el n�mero de columnas
                                                         //UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
                Rrcama.MoveFirst();
                SPRCAMAS.MaxRows = iMaxFilas;

                ncamas = new string[Rrcama.Tables[0].Rows.Count];

                for (int iFilas = 0; iFilas < iMaxFilas; iFilas++)
                {
                    SPRCAMAS.Row = iFilas + 1;
                    SPRCAMAS.Col = 1;

                    SPRCAMAS.Text = Convert.ToString(int.Parse(Rrcama.Tables[0].Rows[iFilas]["gplantas"].ToString()).ToString("D2")) + Convert.ToString(int.Parse(Rrcama.Tables[0].Rows[iFilas]["ghabitac"].ToString()).ToString("D2")) + Rrcama.Tables[0].Rows[iFilas]["gcamasbo"].ToString();

                    if (SPRCAMAS.Text.Length > 0)
                        ncamas[iFilas] = SPRCAMAS.Text;

                    SPRCAMAS.Col = 2;
                    SPRCAMAS.TypeEditMultiLine = true;
                    
                    SPRCAMAS.setTypeEditLen(500);

                    estcama = Convert.ToString(Rrcama.Tables[0].Rows[iFilas]["iestcama"]).ToUpper();
                    if (Convert.IsDBNull(estcama))
                    {
                        SPRCAMAS.Text = " ";
                    }
                    else
                    {
                        //BUSCAR PACIENTE SELECCIONADO EN LA CAMA
                        if (Module1.vstHosUr == "U")
                        {
                            sqlPaciente = "select dnombpac,dape1pac,dape2pac,fnacipac,itipsexo from dpacient,UEPISURG" + " Where dpacient.gidenpac='" + Convert.ToString(Rrcama.Tables[0].Rows[iFilas]["gidenpac"]) + "'" + " and UEPISURG.gidenpac=dpacient.gidenpac and FALTAURG is null";
                        }
                        else
                        {
                            sqlPaciente = "select dnombpac,dape1pac,dape2pac,fnacipac,itipsexo,iregaloj from dpacient,aepisadm" + " Where dpacient.gidenpac='" + Convert.ToString(Rrcama.Tables[0].Rows[iFilas]["gidenpac"]) + "'" + " and aepisadm.gidenpac=dpacient.gidenpac and faltplan is null";
                        }
                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlPaciente, RcAdmision);
                        rrPaciente = new DataSet();
                        tempAdapter_2.Fill(rrPaciente);
                        
                        rrPaciente.MoveFirst();
                        if (rrPaciente.Tables[0].Rows.Count != 0)
                        {
                            tstPaciente = new StringBuilder("");
                            SPRCAMAS.Col = 3;
                            if (!Convert.IsDBNull(rrPaciente.Tables[0].Rows[0]["dnombpac"]))
                            {
                                tstPaciente = new StringBuilder(Convert.ToString(rrPaciente.Tables[0].Rows[0]["dnombpac"]).Trim());
                            }
                            if (!Convert.IsDBNull(rrPaciente.Tables[0].Rows[0]["dape1pac"]))
                            {
                                tstPaciente.Append(" " + Convert.ToString(rrPaciente.Tables[0].Rows[0]["dape1pac"]).Trim());
                            }

                            if (!Convert.IsDBNull(rrPaciente.Tables[0].Rows[0]["dape2pac"]))
                            {
                                tstPaciente.Append(" " + Convert.ToString(rrPaciente.Tables[0].Rows[0]["dape2pac"]).Trim());
                            }
                            SPRCAMAS.Text = tstPaciente.ToString();
                            SPRCAMAS.Col = 5;
                            if (!Convert.IsDBNull(rrPaciente.Tables[0].Rows[0]["fnacipac"]))
                            {
                                //SPRCAMAS.Text = Mid(Year(Date - rrPaciente("fnacipac")), 3, 2)
                                stEdad = Serrores.CalcularEdad(Convert.ToDateTime(rrPaciente.Tables[0].Rows[0]["fnacipac"]), DateTime.Today, RcAdmision);
                                //                    If InStr(1, stEdad, "a�") > 0 Then
                                //                        SPRCAMAS.Text = Mid(stEdad, 1, InStr(1, stEdad, "a�") - 1) & "A"
                                //                    ElseIf InStr(1, stEdad, "me") > 0 Then
                                //                        SPRCAMAS.Text = Mid(stEdad, 1, InStr(1, stEdad, "me") - 1) & "M"
                                //                    ElseIf InStr(1, stEdad, "d") > 0 Then
                                //                        SPRCAMAS.Text = Mid(stEdad, 1, InStr(1, stEdad, "d") - 1) & "D"
                                //                    End If
                                SPRCAMAS.Text = stEdad.Substring(0, Math.Min(stEdad.IndexOf(' ') + 1, stEdad.Length)) + stEdad.Substring(stEdad.IndexOf(' ') + 1, Math.Min(1, stEdad.Length - (stEdad.IndexOf(' ') + 1)));
                                //SPRCAMAS.Text = Mid(CalcularEdad(rrPaciente("fnacipac"), Date), 1, 3)
                            }
                            SPRCAMAS.Col = 4;
                            SPRCAMAS.setTypePictStretch(false);
                            SPRCAMAS.setTypePictMaintainScale(false);

                            if (Convert.ToString(rrPaciente.Tables[0].Rows[0]["itipsexo"]) == "H")
                            {
                                SPRCAMAS.setTypePictPicture(IlIconos.Images[1]);
                            }
                            else
                            {
                                if (Convert.ToString(rrPaciente.Tables[0].Rows[0]["itipsexo"]) == "M")
                                {
                                    SPRCAMAS.setTypePictPicture(IlIconos.Images[0]);
                                }
                                else
                                {
                                    SPRCAMAS.setTypePictPicture(IlIconos.Images[19]);
                                }
                            }
                            SPRCAMAS.Col = 6;
                            if (Module1.vstHosUr == "U")
                            {
                                SPRCAMAS.Text = "Compartido";
                            }
                            else
                            {
                                if (Convert.ToString(rrPaciente.Tables[0].Rows[0]["IREGALOJ"]) == "I")
                                {
                                    SPRCAMAS.Text = "Individual";
                                }
                                else
                                {

                                    if (Convert.ToString(rrPaciente.Tables[0].Rows[0]["IREGALOJ"]) == "C")
                                    {
                                        SPRCAMAS.Text = "Compartido";
                                    }
                                    else
                                    {
                                        SPRCAMAS.Text = " ";
                                    }
                                }
                            }
                        }
                        SPRCAMAS.Col = 2;
                        SPRCAMAS.Rows[iFilas].Cells[1].Style.CustomizeFill = true;
                        SPRCAMAS.Text = estcama;
                        switch (estcama)
                        {
                            case "L":
                                SPRCAMAS.Rows[iFilas].Cells[1].Style.BackColor = ColorTranslator.FromOle(Convert.ToInt32(Double.Parse(cverde)));
                                SPRCAMAS.Text = "LIBRE";
                                break;
                            case "O":

                                SPRCAMAS.Rows[iFilas].Cells[1].Style.BackColor = ColorTranslator.FromOle(Convert.ToInt32(Double.Parse(crojo)));
                                SPRCAMAS.Text = "OCUPADA";
                                break;
                            case "I":
                                SPRCAMAS.Rows[iFilas].Cells[1].Style.BackColor = ColorTranslator.FromOle(Convert.ToInt32(Double.Parse(cazul)));
                                SPRCAMAS.Text = "INHABILITADA";
                                break;
                            case "B":
                                SPRCAMAS.Rows[iFilas].Cells[1].Style.BackColor = ColorTranslator.FromOle(Convert.ToInt32(Double.Parse(camarillo)));
                                SPRCAMAS.Text = "BLOQUEADA";
                                break;
                        }
                        SPRCAMAS.Col = 7;
                        SPRCAMAS.Text = Convert.ToString(Rrcama.Tables[0].Rows[iFilas]["iescuna"]);

                        //oscar 27/11/203
                        SPRCAMAS.Col = 8;
                        SPRCAMAS.Text = Convert.ToString(Rrcama.Tables[0].Rows[iFilas]["isillon"]);
                        //-------

                        //oscar C: 06/10/04
                        SPRCAMAS.Col = 9;
                        SPRCAMAS.Text = "N";
                        Lsql = " SELECT FPREVING, DNOMBPAC, DAPE1PAC, DAPE2PAC FROM ARESINGRVA INNER JOIN DPACIENT ON" +
                               " DPACIENT.gidenpac = ARESINGRVA.gidenpac " +
                               " WHERE " +
                               " gplantas=" + Convert.ToString(Rrcama.Tables[0].Rows[iFilas]["gplantas"]) + " AND " +
                               " ghabitac=" + Convert.ToString(Rrcama.Tables[0].Rows[iFilas]["ghabitac"]) + " AND " +
                               " gcamasbo='" + Convert.ToString(Rrcama.Tables[0].Rows[iFilas]["gcamasbo"]) + "' AND " +
                               " ganoadme IS NULL and gnumadme IS NULL";
                        SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Lsql, RcAdmision);
                        LRR = new DataSet();
                        tempAdapter_3.Fill(LRR);
                        if (LRR.Tables[0].Rows.Count != 0)
                        {
                            SPRCAMAS.Text = "S";
                            SPRCAMAS.Col = 2;
                            if (SPRCAMAS.Text == "LIBRE")
                            {
                                SPRCAMAS.Col = 3;
                                SPRCAMAS.Text = "Con RESERVA para" + Environment.NewLine +
                                                ((Convert.IsDBNull(LRR.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(LRR.Tables[0].Rows[0]["dnombpac"]).Trim().ToUpper()) + " " +
                                                ((Convert.IsDBNull(LRR.Tables[0].Rows[0]["dape1pac"])) ? "" : Convert.ToString(LRR.Tables[0].Rows[0]["dape1pac"]).Trim().ToUpper()) + " " +
                                                ((Convert.IsDBNull(LRR.Tables[0].Rows[0]["dape2pac"])) ? "" : Convert.ToString(LRR.Tables[0].Rows[0]["dape2pac"]).Trim().ToUpper()) + " " + Environment.NewLine +
                                                "para el " + Convert.ToDateTime(LRR.Tables[0].Rows[0]["fpreving"]).ToString("dd/MM/yyyy HH:mm");
                            }
                        }
                        LRR.Close();

                        SPRCAMAS.Col = 11;
                        SPRCAMAS.Text = "N";
                        Lsql = "select NNUMERI1 FROM SCONSGLO WHERE GCONSGLO='INHABALT'";
                        SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(Lsql, RcAdmision);
                        LRR = new DataSet();
                        tempAdapter_4.Fill(LRR);
                        if (LRR.Tables[0].Rows.Count != 0)
                        {
                            if (!Convert.IsDBNull(LRR.Tables[0].Rows[0]["nnumeri1"]))
                            {                                
                                if (Convert.ToInt32(LRR.Tables[0].Rows[0]["nnumeri1"]) == Convert.ToInt32(Rrcama.Tables[0].Rows[0]["gmotinha"]))
                                {
                                    SPRCAMAS.Text = "S";
                                }
                            }
                        }
                        LRR.Close();
                        //---------

                    }
                    
                    Rrcama.MoveNext();
                }
            }
            SPRCAMAS.SetColHidden(1, true);
            SPRCAMAS.EndUpdate();
        }



        private void Imcama_MouseDown(Object eventSender, MouseEventArgs eventArgs)
		{
            int Index = Array.IndexOf(this.Imcama, eventSender);
            ESTADO_CAMA = Index + 1;
            if (!mbAcceso.PermitirEvento(RcAdmision, Module1.astrEventos, "Imcama", "", "MouseDown"))
            {
            	return;
            }
            switch(Index)
            {
                case 0:  //camas libres 
                    Imcama[0].DoDragDrop(Imcama[0], DragDropEffects.Copy);
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:  //camas inhabilitadas 
                    Imcama[3].DoDragDrop(Imcama[3], DragDropEffects.Copy);
                    break;
                case 4:
                    break;
                case 6:  // cunas libres 
                    Imcama[6].DoDragDrop(Imcama[6], DragDropEffects.Copy);
                    break;
                case 9:  // cunas inhabilitadas 
                    Imcama[9].DoDragDrop(Imcama[9], DragDropEffects.Copy);
                    //oscar 27/11/2003 
                    break;
                case 11:  //sillones libres 
                    Imcama[11].DoDragDrop(Imcama[11], DragDropEffects.Copy);
                    break;
                case 14:  //sillones inhabilitados 
                    Imcama[14].DoDragDrop(Imcama[14], DragDropEffects.Copy);
                    //------ 
                    break;
            }
        }

        private void Imcama_GiveFeedback(object sender, System.Windows.Forms.GiveFeedbackEventArgs e)
        {            
            if (e.Effect == DragDropEffects.Copy)
            {
                e.UseDefaultCursors = false;
                Cursor.Current = new Cursor(((Bitmap)((sender as PictureBox).Image)).GetHicon());
            }
            else
                Cursor.Current = Cursors.Default;
        }


        private void Imseleccionada_MouseDown(object eventSender, MouseEventArgs eventArgs)
		{
            int Button = (int)eventArgs.Button;
            int Shift = (int)(Control.ModifierKeys & Keys.Shift);
            float x = (float)(eventArgs.X * 15);
            float y = (float)(eventArgs.Y * 15);
            int columna = SPRCAMAS.Col;
            SPRCAMAS.Col = 1;
                        
            if (Module1.vstHosUr == "H")
            {                
                if (compareBmp((Bitmap)Imseleccionada.Image, (Bitmap)IlIconos.Images[5]))
                {
                    ESTADO_CAMA = 2;
                    Imseleccionada.DoDragDrop(((PictureBox)eventSender).Image,DragDropEffects.Copy);
                    SPRCAMAS.Col = columna;
                    return;
                }
                
                if (compareBmp((Bitmap)Imseleccionada.Image, (Bitmap)IlIconos.Images[39]))
                {
                    ESTADO_CAMA = 2;
                    Imseleccionada.DoDragDrop(((PictureBox)eventSender).Image, DragDropEffects.Copy);
                    SPRCAMAS.Col = columna;
                    return;
                }

                //oscar 27/11/2003                                
                if (compareBmp((Bitmap)Imseleccionada.Image, (Bitmap)IlIconos.Images[43]))
                {
                    ESTADO_CAMA = 2;
                    Imseleccionada.DoDragDrop(((PictureBox)eventSender).Image, DragDropEffects.Copy);
                    SPRCAMAS.Col = columna;
                    return;
                }
                //--------
            }            
        }

        public void modificar_Click(Object eventSender, EventArgs eventArgs)
		{
			object HABITAMOD = null;
			object PLANTAMOD = null;
			//presentar pantalla de la cama para modificaci�n
			SPRCAMAS.Col = 1;
            SPRCAMAS.Row = SPRCAMAS.CurrentRow.Index + 1;
            //CAMA_ESTADO.Tag = SPRCAMAS.Text & "B VER"
            string VERPLAN = (SPRCAMAS.Text != "") ? SPRCAMAS.Text.Substring(0, Math.Min(2, SPRCAMAS.Text.Length)) : string.Empty;
            string VERHABI = (SPRCAMAS.Text != "") ? SPRCAMAS.Text.Substring(2, Math.Min(2, SPRCAMAS.Text.Length - 2)) : string.Empty;
            string VERCAMA = (SPRCAMAS.Text != "") ? SPRCAMAS.Text.Substring(4, Math.Min(2, SPRCAMAS.Text.Length - 4)) : string.Empty;
            CAMA_ESTADO instancia_camam = new CAMA_ESTADO();
            CAMA_ESTADO tempLoadForm = instancia_camam;
            instancia_camam.FNRECOGETAG(VERPLAN, VERHABI, VERCAMA, "B", Module1.vstHosUr, "MODIFICAR", this, RcAdmision, ESTADO_CAMA);
            instancia_camam.ShowDialog();
            if (Convert.ToString(this.Tag) == "ACEPTAR")
            {
                //si confirma refrescar el grid
                PLANTAMOD = plantasel;
                HABITAMOD = habitasel;
                genera_sql();
                plantasel = Convert.ToInt32(PLANTAMOD);
                habitasel = Convert.ToInt32(HABITAMOD);
                carga_camas();
            }
		}

		public void nuevo_Click(Object eventSender, EventArgs eventArgs)
		{
			//presentar pantalla de la cama para captura

			//Load CAMA_ESTADO
			//CAMA_ESTADO.Tag = ""
			//CAMA_ESTADO.Show 1


			//If VISUALIZACION.Tag = "ACEPTAR" Then
			//    'si confirma refrescar el grid
			//    genera_sql
			//    carga_camas
			//
			//End If

		}


		//UPGRADE_NOTE: (7001) The following declaration (Option1_Click) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void Option1_Click()
		//{
				//
		//}

		//********************************************************************************
		//*                                                                              *
		//*  Modificaciones:                                                             *
		//*                                                                              *
		//*      O.Frias 21/02/2007  Realizo la operativa para mostrar el tel�fono.      *
		//*                                                                              *
		//********************************************************************************
		public void Paciente_Click(Object eventSender, EventArgs eventArgs)
		{
			string stGiden = String.Empty;
			string sqlPaciente = String.Empty;
			DataSet rrPaciente = null;

			//************************ O.Frias 21/02/2007 ************************
			string strTelefono = String.Empty;
			//************************ O.Frias 21/02/2007 ************************

			SPRCAMAS.Col = 1;
            SPRCAMAS.Row = SPRCAMAS.CurrentRow.Index + 1;
			int vstPlanta = Convert.ToInt32(Double.Parse(SPRCAMAS.Text.Substring(0, Math.Min(2, SPRCAMAS.Text.Length))));
			int vstHabita = Convert.ToInt32(Double.Parse(SPRCAMAS.Text.Substring(2, Math.Min(2, SPRCAMAS.Text.Length - 2))));
			string vstCama = SPRCAMAS.Text.Substring(4, Math.Min(2, SPRCAMAS.Text.Length - 4));
			SPRCAMAS.Col = 2;
			//SPRCAMAS.Row = Row
			string vstEstadoCamas = SPRCAMAS.Text.Trim();
			if (vstEstadoCamas == "OCUPADA" || vstEstadoCamas == "BLOQUEADA")
			{
				SPRCAMAS.Col = 1;

				//************************ O.Frias 21/02/2007 ************************
				sqlPaciente = "select gidenpac, ntelecam from dcamasbova where " + " gplantas= " + vstPlanta.ToString() + "and  ghabitac= " + vstHabita.ToString() + " and gcamasbo='" + vstCama + "'";
				//************************ O.Frias 21/02/2007 ************************

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlPaciente, RcAdmision);
				rrPaciente = new DataSet();
				tempAdapter.Fill(rrPaciente);
				if (!Convert.IsDBNull(rrPaciente.Tables[0].Rows[0]["gidenpac"]))
				{
					stGiden = Convert.ToString(rrPaciente.Tables[0].Rows[0]["gidenpac"]);
					strTelefono = (!Convert.IsDBNull(rrPaciente.Tables[0].Rows[0]["ntelecam"])) ? Convert.ToString(rrPaciente.Tables[0].Rows[0]["ntelecam"]) : "";
					rrPaciente.Close();

					//************************ O.Frias 21/02/2007 ************************
					PACIENTE_CAMA.DefInstance.proLLenar_pac(stGiden, SPRCAMAS.Text, RcAdmision, vstEstadoCamas, strTelefono);
					//************************ O.Frias 21/02/2007 ************************

					PACIENTE_CAMA.DefInstance.ShowDialog();
				}
				else
				{
					rrPaciente.Close();
				}

			}

		}
        

		//********************************************************************************************************
		//*                                                                                                      *
		//*  Procedimiento: SPRCAMAS_Click                                                                       *
		//*                                                                                                      *
		//*  Modificaci�n:                                                                                       *
		//*                                                                                                      *
		//*    O.Frias (14/11/2006) Activa o desactiva el boton de reserva seg�n el estado de la cama            *
		//*                                                                                                      *
		//********************************************************************************************************
		private void SPRCAMAS_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
            int Col = 0;
            int Row = 0;

            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = SPRCAMAS.Col;
                Row = SPRCAMAS.Row;
            }
            
            string cama = String.Empty;
            string habita = String.Empty;
            string planta = String.Empty;

            bool bEsCuna = false;
            //oscar 27/11/2003
            bool bEsSillon = false;
            bool bHayalgunaReservaHab = false;
            bool bInhaEsp = false;
            //---------------

            //guardo los datos en variables par un posible refresco, y siga seleccionada
            //la misma cama
            vlColCam = Col;
            vlRowCam = Row;
            //*****
            string sqlcambio2 = String.Empty;
            DataSet RrCambio2 = null;
            //relleno los datos de la cama seleccionada en las cajas de la derecha
            if (Row != 0 && Col != 0)
            {
                SPRCAMAS.Col = 1;
                SPRCAMAS.Row = Row;
                LbDcama.Text = SPRCAMAS.Text;
                LbNombre.Text = "";
                LbApellido1.Text = "";
                LbApellido2.Text = "";
                SPRCAMAS.Col = 7;
                bEsCuna = (SPRCAMAS.Text == "S");
                //oscar '27/11/2003
                SPRCAMAS.Col = 8;
                bEsSillon = (SPRCAMAS.Text == "S");

                if (bEsCuna && !bEsSillon)
                {
                    LbCama.Text = "CUNA SELECCIONADA";
                }
                if (!bEsCuna && bEsSillon)
                {
                    LbCama.Text = " PLAZA SELECCIONADA";
                }
                if (!bEsCuna && !bEsSillon)
                {
                    LbCama.Text = "CAMA SELECCIONADA";
                }

                //******************* O.Frias (14/11/2006) Inicio *******************
                cmdReserva.Enabled = true;
                //******************** O.Frias (14/11/2006) Fin  ********************


                SPRCAMAS.Col = 9;
                bHayalgunaReservaHab = (SPRCAMAS.Text == "S");
                SPRCAMAS.Col = 10;
                bInhaEsp = (SPRCAMAS.Text == "S");
                //--------

                SPRCAMAS.Col = 2;
                switch (SPRCAMAS.Text)
                {
                    case "LIBRE":

                        if (!bEsCuna)
                        {
                            //oscar 27/11/2003
                            //Imseleccionada.Picture = IlIconos.ListImages(5).Picture
                            if (!bEsSillon)
                            {
                                Imseleccionada.Image = IlIconos.Images[4]; //cama                                    
                                if (bHayalgunaReservaHab)
                                {
                                    Imseleccionada.Image = IlIconos.Images[49]; // cama con reserva                                        
                                }
                            }
                            else
                            {
                                Imseleccionada.Image = IlIconos.Images[41]; //sillon
                                if (bHayalgunaReservaHab)
                                {
                                    Imseleccionada.Image = IlIconos.Images[51]; //sillon con reserva                                        
                                }
                            }
                            //----------
                        }
                        else
                        {
                            Imseleccionada.Image = IlIconos.Images[35]; //cuna                                
                            if (bHayalgunaReservaHab)
                            {
                                Imseleccionada.Image = IlIconos.Images[53]; //cuna con reserva                                    
                            }
                        }
                        break;
                    case "OCUPADA":
                        if (!bEsCuna)
                        {
                            //oscar 27/11/2003
                            //Imseleccionada.Picture = IlIconos.ListImages(6).Picture
                            if (!bEsSillon)
                            {
                                Imseleccionada.Image = IlIconos.Images[5];                                    
                            }
                            else
                            {
                                Imseleccionada.Image = IlIconos.Images[43];
                            }
                            //----------
                        }
                        else
                        {
                            Imseleccionada.Image = IlIconos.Images[39];                                
                        }
                        //buscar el paciente y rellenar nombre y apellidos 
                        SPRCAMAS.Col = 1;
                        planta = SPRCAMAS.Text.Substring(0, Math.Min(2, SPRCAMAS.Text.Length));
                        habita = SPRCAMAS.Text.Substring(2, Math.Min(2, SPRCAMAS.Text.Length - 2));
                        cama = SPRCAMAS.Text.Substring(4, Math.Min(2, SPRCAMAS.Text.Length - 4));
                        sqlcambio2 = "select dnombpac,dape1pac,dape2pac from dpacient, dcamasbova where " + "gplantas ='" + planta + "' and ghabitac ='" + habita + "' and gcamasbo='" + cama + "'" + "and dcamasbova.gidenpac=dpacient.gidenpac";
                        SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlcambio2, RcAdmision);
                        RrCambio2 = new DataSet();
                        tempAdapter.Fill(RrCambio2);
                        if (RrCambio2.Tables[0].Rows.Count == 1)
                        {                                
                            if (Convert.ToString(RrCambio2.Tables[0].Rows[0]["dnombpac"]) != "")
                            {                                    
                                LbNombre.Text = Convert.ToString(RrCambio2.Tables[0].Rows[0]["dnombpac"]).Trim();
                            }
                            else
                            {
                                LbNombre.Text = "";
                            }
                                
                            if (Convert.ToString(RrCambio2.Tables[0].Rows[0]["dape1pac"]) != "")
                            {                                    
                                LbApellido1.Text = Convert.ToString(RrCambio2.Tables[0].Rows[0]["dape1pac"]).Trim();
                            }
                            else
                            {
                                LbApellido1.Text = "";
                            }
                                
                            if (Convert.ToString(RrCambio2.Tables[0].Rows[0]["dape2pac"]) != "")
                            {                                    
                                LbApellido2.Text = Convert.ToString(RrCambio2.Tables[0].Rows[0]["dape2pac"]).Trim();
                            }
                            else
                            {
                                LbApellido2.Text = "";
                            }
                        }
                        SPRCAMAS.Col = 2;
                        break;
                    case "INHABILITADA":
                        if (!bEsCuna)
                        {
                            //oscar 27/11/2003
                            //Imseleccionada.Picture = IlIconos.ListImages(8).Picture
                            if (!bEsSillon)
                            {
                                Imseleccionada.Image = IlIconos.Images[7]; //cama                                    
                                if (bInhaEsp)
                                {
                                    Imseleccionada.Image = IlIconos.Images[50];                                        
                                } //cama con inhabilitacion especial
                            }
                            else
                            {
                                Imseleccionada.Image = IlIconos.Images[45]; //sillon                                    
                                if (bInhaEsp)
                                {
                                    Imseleccionada.Image = IlIconos.Images[52];                                        
                                } //sillon con inhabilitacion especial
                            }
                            //----------
                        }
                        else
                        {
                            Imseleccionada.Image = IlIconos.Images[33]; //cuna                                
                            if (bInhaEsp)
                            {
                                Imseleccionada.Image = IlIconos.Images[54];                                    
                            } //cuna con inhabilitacion especial
                        }
                        break;
                    case "BLOQUEADA":
                        if (!bEsCuna)
                        {
                            //oscar 27/11/2003
                            //Imseleccionada.Picture = IlIconos.ListImages(4).Picture
                            if (!bEsSillon)
                            {
                                Imseleccionada.Image = IlIconos.Images[3];                                    
                            }
                            else
                            {
                                Imseleccionada.Image = IlIconos.Images[47];                                    
                            }
                            //----------
                        }
                        else
                        {
                            Imseleccionada.Image = IlIconos.Images[31];                                
                        }
                        break;
                }
            }                        
        }

        private void SPRCAMAS_ContextMenuOpening(object sender, Telerik.WinControls.UI.ContextMenuOpeningEventArgs e)
        {
            RadContextMenu rdxContexMenu = new RadContextMenu();            
            SPRCAMAS.Row = SPRCAMAS.CurrentRow.Index+1;
            SPRCAMAS.Col = SPRCAMAS.CurrentColumn.Index + 1;

            SPRCAMAS_CellClick(sender, null);
            SPRCAMAS.Col = 2;
            rdxContexMenu.Items.Clear();
            rdxContexMenu.Items.Add(Eliminar);
            rdxContexMenu.Items.Add(modificar);
            rdxContexMenu.Items.Add(ver);
            rdxContexMenu.Items.Add(Paciente);
            
            if (SPRCAMAS.Text == "LIBRE" || SPRCAMAS.Text == "INHABILITADA")
            {                               
                Eliminar.Enabled = true;
                modificar.Enabled = true;
                Paciente.Enabled = false;                                
            }
            else
            {                
                Eliminar.Enabled = false;
                modificar.Enabled = false;
                Paciente.Enabled = true;                
            }
            e.ContextMenu = rdxContexMenu.DropDown;
            e.ContextMenu.Show(this, (int)PointToClient(Cursor.Position).X, (int)PointToClient(Cursor.Position).Y);
        }

        /// <summary>
        /// Metodo que reemplaza obtiene la fila dependiendo unas coordenadas de click - Usadas para detectar donde se soltaron las imagenes en los eventos dragdrop
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        GridViewRowInfo GetRowAtPoint(RadGridView grid, Point location)
        {
            RadElement element = grid.ElementTree.GetElementAtPoint(location);
            if (element is GridCellElement)
            {
                return ((GridCellElement)element).RowInfo;
            }
            if (element is GridDataRowElement)
            {
                return ((GridRowElement)element).RowInfo;
            }
            return null;
        }

        /// <summary>
        /// Metodo que reemplaza obtiene la columna dependiendo unas coordenadas de click - Usadas para detectar donde se soltaron las imagenes en los eventos dragdrop
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        GridViewColumn GetColumnAtPoint(RadGridView grid, Point location)
        {
            RadElement element = grid.ElementTree.GetElementAtPoint(location);
            if (element is GridCellElement)
            {
                return ((GridCellElement)element).ColumnInfo;
            }
            return null;
        }

        private void SPRCAMAS_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        //********************************************************************************************
        //*                                                                                          *
        //*  Modificaciones:                                                                         *
        //*                                                                                          *
        //*      O.Frias (20/02/2007) - Realizo el proceso de cambiar el estado bloqueado a libre    *
        //*      sin necesidad de que el paciente regrese a la cama anteriormere bloqueda por un     *
        //*      traslado.                                                                           *
        //*                                                                                          *
        //********************************************************************************************        

        private void SPRCAMAS_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{            
            int columna = 0;
			bool cambio = false;
			DataSet RrCambio = null;
			string sqlcambio = String.Empty;
			int planta = 0;
			int habita = 0;
			string cama = String.Empty;
			DataSet RrCambio2 = null;
			string sqlcambio2 = String.Empty;
			int planta2 = 0;
			int habita2 = 0;
			string cama2 = String.Empty;
			int PLANTA_ABAJO = 0;
			int HABITA_IZQUI = 0;
			string vsttag = String.Empty;
			DATOS_TRASLADO instancia_traslado = null;
			MOTIVO_INHABILITACION instancia_inhabili = null;

			//oscar 27/11/2003
			bool bCuna = false;
			bool bSillon = false;
			//-----
			//******************** O.Frias 20/02/2007 ********************
			bool bActualiza = false;

			traslado_valido = true;            

			if (SPRCAMAS.Row > 0)
			{
				bActualiza = false;
				cambio = false;
				columna = SPRCAMAS.Col;
				SPRCAMAS.Col = 1;
				planta = Convert.ToInt32(Double.Parse(SPRCAMAS.Text.Substring(0, Math.Min(2, SPRCAMAS.Text.Length))));
				habita = Convert.ToInt32(Double.Parse(SPRCAMAS.Text.Substring(2, Math.Min(2, SPRCAMAS.Text.Length - 2))));
				cama = SPRCAMAS.Text.Substring(4, Math.Min(2, SPRCAMAS.Text.Length - 4));
				if (LbDcama.Text != "")
				{
					planta2 = Convert.ToInt32(Double.Parse(LbDcama.Text.Substring(0, Math.Min(2, LbDcama.Text.Length))));
					habita2 = Convert.ToInt32(Double.Parse(LbDcama.Text.Substring(2, Math.Min(2, LbDcama.Text.Length - 2))));
					cama2 = LbDcama.Text.Substring(4, Math.Min(2, LbDcama.Text.Length - 4));
				}
                
                sqlcambio = "select iestcama,gmotinha,gidenpac,itipsexo,gplantas,ghabitac,gcamasbo from dcamasbo where " + "gplantas =" + planta.ToString() + " and ghabitac =" + habita.ToString() + " and gcamasbo='" + cama + "'";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlcambio, RcAdmision);
                RrCambio = new DataSet();
                tempAdapter.Fill(RrCambio);
                sqlcambio2 = "select iestcama,gmotinha,gidenpac,gplantas,ghabitac,gcamasbo from dcamasbo where " + "gplantas =" + planta2.ToString() + " and ghabitac =" + habita2.ToString() + " and gcamasbo='" + cama2 + "'";

                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlcambio2, RcAdmision);
                RrCambio2 = new DataSet();
                tempAdapter_2.Fill(RrCambio2);
                SPRCAMAS.Col = 2;

                switch (SPRCAMAS.Text)
				{
					case "LIBRE" : 
						// modificacion -> si no se pone ESTADO_CAMA = 10 -> NO SE PUEDEN INHABILITAR CUNAS 
						//oscar 27/11/2003 
						//If ESTADO_CAMA = 4 Or ESTADO_CAMA = 10 Then 
						if (ESTADO_CAMA == 4 || ESTADO_CAMA == 10 || ESTADO_CAMA == 15)
						{
							//------

							//compruebo si los tipos son iguales
							SPRCAMAS.Col = 7; // columna oculta que nos dice si es una cuna o no

							//oscar 27/11/2003
							bCuna = SPRCAMAS.Text == "S";
							SPRCAMAS.Col = 8; // columna oculta que nos dice si es una plaza de dia (sillon) o no
							bSillon = SPRCAMAS.Text == "S";

							//If (Trim(UCase(SPRCAMAS.Text)) = "S" And ESTADO_CAMA = 10) Or _
							//(Trim(UCase(SPRCAMAS.Text)) = "N" And ESTADO_CAMA = 4) Then '
							if ((bCuna && !bSillon && ESTADO_CAMA == 10) || (!bCuna && bSillon && ESTADO_CAMA == 15) || (!bCuna && !bSillon && ESTADO_CAMA == 4))
							{
								//------
								// cambiar estado a inhabilitada
								this.Tag = "!";
								instancia_inhabili = new MOTIVO_INHABILITACION();
								MOTIVO_INHABILITACION tempLoadForm = instancia_inhabili;
								instancia_inhabili.fnrecogedatos(this, RcAdmision);
								instancia_inhabili.ShowDialog();
								if (Convert.ToString(this.Tag) != "!")
								{																		
									RrCambio.Tables[0].Rows[0]["iestcama"] = "I";									
									RrCambio.Tables[0].Rows[0]["gmotinha"] = Convert.ToInt32(Double.Parse(Convert.ToString(this.Tag)));									
									RrCambio.Tables[0].Rows[0]["gidenpac"] = DBNull.Value;
									string tempQuery = RrCambio.Tables[0].TableName;									
									SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                                    tempAdapter.Update(RrCambio, tempQuery);
								}
								cambio = true;
							}
							SPRCAMAS.Col = 1;
						} 
						 
						if (ESTADO_CAMA == 2)
						{
							SPRCAMAS.Col = 1;
							//SE PRODUCE UN TRASLADO
							//CARGAR PANTALLA DE TRASLADOS
							// VERIFICAR QUE PUEDE REALIZAR LA OPERACION
							//If Not CoincidenTipoCama Then
							//    MsgBox "No coinciden los tipos de cama", vbInformation
							//Else
							COMPROBAR_TRASLADO(1);
							if (traslado_valido)
							{
								instancia_traslado = new DATOS_TRASLADO();
								DATOS_TRASLADO tempLoadForm2 = instancia_traslado;
								//CARGA EN EL TAG "T" DE TRASLADO MAS
								//CODIGO DE LA CAMA ORIGEN LBDCAMA.CAPTION
								//CODIGO DEL DESTINO SPRCAMAS.TEXT
								vsttag = "T" + "/" + LbDcama.Text + "/" + SPRCAMAS.Text;
								instancia_traslado.fnrecogertag(vsttag, this, RcAdmision);
								instancia_traslado.ShowDialog();
								//SI CONFIRMA CAMBIO=TRUE
								if (Convert.ToString(this.Tag) == "ACEPTAR")
								{
									cambio = true;
								}
								LbDcama.Text = "";
								LbNombre.Text = "";
								LbApellido1.Text = "";
								LbApellido2.Text = "";
								Imseleccionada.Image = blanco;
							}
							//End If
						} 
						break;
					case "BLOQUEADA" : 
						//If ESTADO_CAMA = 4 Then 
						//cambiar estado a LIBRE 
						//If ESTADO_CAMA = 1 Then 
						//    RrCambio.Edit 
						//    RrCambio("iestcama") = "L" 
						//    RrCambio.Update 
						//Else 
						//    'carga pantalla motivo inhabilitaci�n 
						//    Me.Tag = "!" 
						//    Set instancia_inhabili = New MOTIVO_INHABILITACION 
						//    Load instancia_inhabili 
						//    instancia_inhabili.fnrecogedatos Me, RcAdmision 
						//    instancia_inhabili.Show 1 
						//    If Me.Tag <> "!" Then 
						//        RrCambio.Edit 
						//        RrCambio("iestcama") = "I" 
						//        RrCambio("gmotinha") = CInt(Me.Tag) 
						//        RrCambio("gidenpac") = Null 
						//        RrCambio.Update 
						//    End If 
						//End If 
						//cambio = True 
						//Else 
						if (ESTADO_CAMA == 2)
						{ //SE PRODUCE UN TRASLADO
							SPRCAMAS.Col = 1;
							//verificar que el paciente origen y destino es el mismo
							
							if (RrCambio.Tables[0].Rows[0]["gidenpac"].ToString() != RrCambio2.Tables[0].Rows[0]["gidenpac"].ToString())
							{
								// VERIFICAR QUE PUEDE REALIZAR LA OPERACION
								
								short tempRefParam = 1320;
								string[] tempRefParam2 = new string[]{"bloqueada"};
                                Serrores.oClass.CtrlMensaje = this;
                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcAdmision, tempRefParam2));
                                Serrores.oClass.CtrlMensaje = null;
                            }
							else
							{
								//If Not CoincidenTipoCama Then
								//    MsgBox "No coinciden los tipos de cama", vbInformation
								//Else
								COMPROBAR_TRASLADO(1);
								if (traslado_valido)
								{
									instancia_traslado = new DATOS_TRASLADO();
									DATOS_TRASLADO tempLoadForm3 = instancia_traslado;
									vsttag = "T" + "/" + LbDcama.Text + "/" + SPRCAMAS.Text;
									instancia_traslado.fnrecogertag(vsttag, this, RcAdmision);
									instancia_traslado.ShowDialog();
									//SI CONFIRM CAMBIO=TRUE
									if (Convert.ToString(this.Tag) == "ACEPTAR")
									{
										cambio = true;
									}
									LbDcama.Text = "";
									LbNombre.Text = "";
									LbApellido1.Text = "";
									LbApellido2.Text = "";
									Imseleccionada.Image = blanco;
								}
								//End If
							}
							//******************** O.Frias 20/02/2007 ********************
						}
						else if (ESTADO_CAMA == 1 || ESTADO_CAMA == 7 || ESTADO_CAMA == 12)
						{  //' Paso de bloqueo a libre
							SPRCAMAS.Col = 7;
							bCuna = SPRCAMAS.Text == "S";
							SPRCAMAS.Col = 8;
							bSillon = SPRCAMAS.Text == "S";

							switch(ESTADO_CAMA)
							{
								case 1 : 
									bActualiza = !bCuna && !bSillon; 
									 
									break;
								case 7 : 
									bActualiza = bCuna && !bSillon; 
									 
									break;
								case 12 : 
									bActualiza = !bCuna && bSillon; 
									 
									break;
							}

							if (bActualiza)
							{
                                if (RadMessageBox.Show(this, "La cama est� bloqueada. �Desea cambiar su estado a libre?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                                {                                                                                                            
                                    RrCambio.Tables[0].Rows[0]["iestcama"] = "L";
                                    
                                    RrCambio.Tables[0].Rows[0]["gidenpac"] = DBNull.Value;
                                    
                                    RrCambio.Tables[0].Rows[0]["itipsexo"] = DBNull.Value;
                                    
                                    string tempQuery_2 = RrCambio.Tables[0].TableName;                                    
                                    SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
                                    tempAdapter.Update(RrCambio, tempQuery_2);
									cambio = true;
								}
							}
							else
							{

							}
						} 
						//******************** O.Frias 20/02/2007 ******************** 
						break;
					case "INHABILITADA" : 
						//oscar 27/117/2003 
						//If ESTADO_CAMA = 1 Or ESTADO_CAMA = 3 Or ESTADO_CAMA = 7 Or ESTADO_CAMA = 9 Then 
						if (ESTADO_CAMA == 1 || ESTADO_CAMA == 3 || ESTADO_CAMA == 7 || ESTADO_CAMA == 9 || ESTADO_CAMA == 12 || ESTADO_CAMA == 46)
						{ // si estoy liberando (1) o bloqueando (3) cunas o camas o sillones
							//compruebo si los tipos son iguales
							SPRCAMAS.Col = 7; // columna oculta que nos dice si es una cuna o no

							//oscar 27/11/2003
							bCuna = SPRCAMAS.Text == "S";
							SPRCAMAS.Col = 8; // columna oculta que nos dice si es una plaza de dia (sillon) o no
							bSillon = SPRCAMAS.Text == "S";

							//If Trim(UCase(SPRCAMAS.Text)) = "S" And (ESTADO_CAMA = 7 Or ESTADO_CAMA = 9) Then 'si lo es
							if (bCuna && !bSillon && (ESTADO_CAMA == 7 || ESTADO_CAMA == 9))
							{
								//---------								
								RrCambio.Edit();
								if (ESTADO_CAMA == 7)
								{ // a cuna libre
									//cambiar estado a libre									
									RrCambio.Tables[0].Rows[0]["iestcama"] = "L";
								}
								else
								{
									//cambiar estado a bloqueada									
									RrCambio.Tables[0].Rows[0]["iestcama"] = "B";
								}
								
								RrCambio.Tables[0].Rows[0]["gmotinha"] = DBNull.Value;								
								string tempQuery_3 = RrCambio.Tables[0].TableName;
								SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter);
                                tempAdapter.Update(RrCambio, tempQuery_3);
								cambio = true;
							}

							//oscar 27/11/2003
							//If Trim(UCase(SPRCAMAS.Text)) = "N" And (ESTADO_CAMA = 1 Or ESTADO_CAMA = 3) Then 'si lo es
							if (!bCuna && !bSillon && (ESTADO_CAMA == 1 || ESTADO_CAMA == 3))
							{ //si lo es
								//---------								
								RrCambio.Edit();
								if (ESTADO_CAMA == 1)
								{ // a cama libre
									//cambiar estado a libre									
									RrCambio.Tables[0].Rows[0]["iestcama"] = "L";
								}
								else
								{
									//cambiar estado a bloqueada									
									RrCambio.Tables[0].Rows[0]["iestcama"] = "B";
								}								
								RrCambio.Tables[0].Rows[0]["gmotinha"] = DBNull.Value;
								
								string tempQuery_4 = RrCambio.Tables[0].TableName;								
								SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter);
                                tempAdapter.Update(RrCambio, tempQuery_4);
								cambio = true;
							}

							//oscar 27/11/2003
							if (!bCuna && bSillon && (ESTADO_CAMA == 12 || ESTADO_CAMA == 46))
							{ //si lo es								
								RrCambio.Edit();
								if (ESTADO_CAMA == 12)
								{ // a sillon libre
									//cambiar estado a libre									
									RrCambio.Tables[0].Rows[0]["iestcama"] = "L";
								}
								else
								{
									//cambiar estado a bloqueada									
									RrCambio.Tables[0].Rows[0]["iestcama"] = "B";
								}								
								RrCambio.Tables[0].Rows[0]["gmotinha"] = DBNull.Value;
								
								string tempQuery_5 = RrCambio.Tables[0].TableName;								
								SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter);
								tempAdapter.Update(RrCambio, tempQuery_5);
								cambio = true;
							}
							//---------
							SPRCAMAS.Col = 1;
						} 
						break;
					case "OCUPADA" : 
						if (ESTADO_CAMA == 2)
						{
							// SE PRODUCE UNA ROTACI�N
							//PRESENTA PANTALLA DE TRASLADO
							// MENOS CUANDO EL ORIGEN Y EL DESTINO SON
							// EL MISMO
							//CAMA DESTINO
							SPRCAMAS.Col = 1;
							if (LbDcama.Text != SPRCAMAS.Text)
							{
								//VERIFICAR QUE PUEDA REALIZAR LA OPERACION
								//If Not CoincidenTipoCama Then
								//    MsgBox "No coinciden los tipos de cama", vbInformation
								//Else
								COMPROBAR_TRASLADO(2);
								SPRCAMAS.Col = 1;
								if (traslado_valido)
								{
									instancia_traslado = new DATOS_TRASLADO();
									DATOS_TRASLADO tempLoadForm4 = instancia_traslado;
									//CARGA EN EL TAG "R" DE ROTACION MAS
									//CODIGO DE LA CAMA ORIGEN LBDCAMA.CAPTION
									//CODIGO DEL DESTINO SPRCAMAS.TEXT DE LA COLUMNA 0
									SPRCAMAS.Col = 1;
									vsttag = "R" + "/" + LbDcama.Text + "/" + SPRCAMAS.Text;
									instancia_traslado.fnrecogertag(vsttag, this, RcAdmision);
									instancia_traslado.ShowDialog();
									// SI CONFIRMA CAMBIO =TRUE
									if (Convert.ToString(this.Tag) == "ACEPTAR")
									{
										cambio = true;
									}
									LbDcama.Text = "";
									LbNombre.Text = "";
									LbApellido1.Text = "";
									LbApellido2.Text = "";
									Imseleccionada.Image = blanco;
								}
								//End If
							}
						} 
						break;
				}
                if (cambio)
                {
                    //refresca pantalla                                                                                
                    carga_planta();
                    
                    SprPlantas.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell;
					SprPlantas.Action = UpgradeHelpers.Spread.ActionConstants.ActionGotoCell;
                    
                    //SprPlantas.Col = habita
                    //SprPlantas.Row = fncamafila(planta)
                    //SprPlantas.Action = 0
                    plantasel = fncamafila(planta)-1;
                    habitasel = habita+1;
                    SprPlantas.Rows[plantasel].Cells[habitasel].IsSelected = true;
                    //SprPlantas.Action = 0
                    //SprPlantas.Action = 1
                    carga_camas();
                }
                SPRCAMAS.Col = columna;
				RrCambio.Close();
				RrCambio2.Close();
			}
		}
		
        private void SPRCAMAS_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
		{			
            int cx = Convert.ToInt32(e.X);
            int cy = Convert.ToInt32(e.Y);
                        
            GridViewRowInfo rowInfo2 = GetRowAtPoint(SPRCAMAS, SPRCAMAS.PointToClient(new Point(e.X, e.Y)));

            GridViewColumn colInfo2= GetColumnAtPoint(SPRCAMAS, SPRCAMAS.PointToClient(new Point(e.X, e.Y)));

            if (rowInfo2 != null)                            
                SPRCAMAS.Row = rowInfo2.Index + 1;
            if(colInfo2!=null)
                SPRCAMAS.Col = colInfo2.Index + 1;        
        }                                                

        private void SprPlantas_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
            if (_isMouseRightDown || (_isMouseRightDown && eventArgs.ColumnIndex == -1) ||
                (_isMouseRightDown && eventArgs.RowIndex == -1))
            {
                int ClickType = 0;
                int Col = eventArgs.ColumnIndex + 1;
                int Row = eventArgs.RowIndex;
                //int MouseX = eventArgs.X;
                //int MouseY = eventArgs.Y;
                string sqlTotal = String.Empty;
                DataSet Rrcuenta = null;
                string consulta = String.Empty;
                string Estado = String.Empty;
                ESTADO_PLANTAS instancia_estplan = null;
                int columna = Col;
                int fila = Row;
                SprPlantas.Col = Col;
                SprPlantas.Row = Row;
                // n�mero de planta
                if (Col == 0 && Row == -1)
                {
                    consulta = "";
                    Estado = "Todas/";
                }
                else
                {
                    if (Row != -1)
                    {
                        consulta = "gplantas=" + fnfilacama(fila, columna).ToString() + " and ";
                        Estado = Conversion.Str(fnfilacama(fila, columna)) + "/";
                    }
                }
                if (Row != -1 || (Row == -1 && Col == 0))
                {
                    sqlTotal = "SELECT count(iestcama) as maximo  FROM dcamasbova,dunienfeva " + " where DUNIENFEva.ITIPOSER='" + Module1.vstHosUr + "' and " + consulta + " gcounien=gunidenf and iestcama='L' ";
                    // total libre
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTotal, RcAdmision);
                    Rrcuenta = new DataSet();
                    tempAdapter.Fill(Rrcuenta);
                    Estado = Estado + Conversion.Str(Rrcuenta.Tables[0].Rows[0]["maximo"]) + "/";
                    // total ocupadas
                    sqlTotal = "SELECT count(iestcama) as maximo  FROM dcamasbova,dunienfeva " + " where DUNIENFEva.ITIPOSER='" + Module1.vstHosUr + "' and " + consulta + " gcounien=gunidenf and iestcama='O' ";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlTotal, RcAdmision);
                    Rrcuenta = new DataSet();
                    tempAdapter_2.Fill(Rrcuenta);
                    Estado = Estado + Conversion.Str(Rrcuenta.Tables[0].Rows[0]["maximo"]) + "/";
                    // total INHABILITADA
                    sqlTotal = "SELECT count(iestcama) as maximo  FROM dcamasbova,dunienfeva " + " where DUNIENFEva.ITIPOSER='" + Module1.vstHosUr + "' and " + consulta + " gcounien=gunidenf and iestcama='I' ";
                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlTotal, RcAdmision);
                    Rrcuenta = new DataSet();
                    tempAdapter_3.Fill(Rrcuenta);
                    Estado = Estado + Conversion.Str(Rrcuenta.Tables[0].Rows[0]["maximo"]) + "/";
                    // total BLOQUEADA
                    sqlTotal = "SELECT count(iestcama) as maximo  FROM dcamasbova,dunienfeva " + " where DUNIENFEva.ITIPOSER='" + Module1.vstHosUr + "' and " + consulta + " gcounien=gunidenf and iestcama='B' ";
                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlTotal, RcAdmision);
                    Rrcuenta = new DataSet();
                    tempAdapter_4.Fill(Rrcuenta);
                    Estado = Estado + Conversion.Str(Rrcuenta.Tables[0].Rows[0]["maximo"]) + "/";
                    switch (Module1.vstHosUr)
                    {
                        case "H":
                            Estado = Estado + "HOSPITALIZACI�N" + "/";
                            break;
                        case "U":
                            Estado = Estado + "URGENCIAS" + "/";
                            break;
                        case "Q":
                            Estado = Estado + "QUIROFANOS" + "/";
                            break;
                        case "D":
                            Estado = Estado + "COMUNES" + "/";
                            break;
                        case "C":
                            Estado = Estado + "CITACIONES" + "/";
                            break;
                    }
                    instancia_estplan = new ESTADO_PLANTAS();
                    ESTADO_PLANTAS tempLoadForm = instancia_estplan;
                    instancia_estplan.fnrecogertag(Estado, RcAdmision);
                    instancia_estplan.ShowDialog();
                }
            }
            else if (!_isMouseRightDown && eventArgs.ColumnIndex != -1 && eventArgs.RowIndex != -1)
            {
                int Col = eventArgs.ColumnIndex;
                int Row = eventArgs.RowIndex;
                object dibujo = new object();
                vlCol = Col;
                vlRow = Row;
                SprPlantas.Col = vlCol;
                SprPlantas.Row = vlRow;
                if (SprPlantas.Col != 0 && SprPlantas.Row != -1)
                {
                    dibujo = SprPlantas.getTypePictPicture();
                    //if (dibujo != blanco && dibujo != ImaOtroserv)
                    if (dibujo != blanco && !compareBmp((Bitmap)dibujo , (Bitmap)ImaOtroserv))
                    {
                        plantasel = vlRow;
                        habitasel = vlCol - inicio_columna;
                        carga_camas();
                    }
                    switch (SprPlantas.Text)
                    {
                        case "H":
                            ToolTipMain.SetToolTip(SprPlantas, "HOSPITALIZACION");
                            break;
                        case "U":
                            ToolTipMain.SetToolTip(SprPlantas, "URGENCIAS");
                            break;
                        case "Q":
                            ToolTipMain.SetToolTip(SprPlantas, "QUIROFANOS");
                            break;
                        default:
                            ToolTipMain.SetToolTip(SprPlantas, "");
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Dgmorenog - Metodo que compara dos imagenes y determina si son iguales
        /// </summary>
        /// <param name="bmp1"></param>
        /// <param name="bmp2"></param>
        /// <returns></returns>
        private bool compareBmp(Bitmap bmp1, Bitmap bmp2)
        {
            bool equals = true;
            bool flag = true;  //Inner loop isn't broken
            
            //Test to see if we have the same size of image
            if (bmp1 != null && bmp2 != null && bmp1.Size == bmp2.Size)
            {
                for (int x = 0; x < bmp1.Width; ++x)
                {
                    for (int y = 0; y < bmp1.Height; ++y)
                    {
                        if (bmp1.GetPixel(x, y) != bmp2.GetPixel(x, y))
                        {
                            equals = false;
                            flag = false;
                            break;
                        }
                    }
                    if (!flag)
                    {
                        break;
                    }
                }
            }
            else
            {
                equals = false;
            }
            return equals;
        }

        private void SprPlantas_DragDrop(object sender, DragEventArgs e)
        {
            string tsqlVal = String.Empty; //para validar la habitaci�n
			DataSet tRrVal = null;
			string tipoalta = String.Empty;
			int PLANTAMOD = 0;
			int HABITAMOD = 0;
			object BASURA = null;
            //int HABITA_IZQUI = 0;
            //int PLANTA_ABAJO = 0;
			CAMA_ESTADO instancia_cama = null;
			string tempRefParam = "";
			if (!mbAcceso.PermitirEvento(RcAdmision, Module1.astrEventos, "SprPlantas", tempRefParam, "DragDrop"))
			{
				return;
			}
			if (SprPlantas.Row >= 0 && SprPlantas.Col > 0)
			{
				//oscar 27/11/2003
				//If ESTADO_CAMA = 1 Or ESTADO_CAMA = 7 Then   ' cama libre o cuna libre
				if (ESTADO_CAMA == 1 || ESTADO_CAMA == 7 || ESTADO_CAMA == 12)
				{ // cama, cuna o sillon libre
					//---------------
					//validamos que no hay camas de otro servicio en esa habitaci�n
					tsqlVal = "SELECT itiposer FROM dcamasbova,dunienfeva " + " where ghabitac=" + ((SprPlantas.Col - inicio_columna) - 1).ToString() + " and " + " gplantas=" + fnfilacama(SprPlantas.Row, SprPlantas.Col).ToString() + " and gcounien = gunidenf";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(tsqlVal, RcAdmision);
					tRrVal = new DataSet();
					tempAdapter.Fill(tRrVal);

					if (tRrVal.Tables[0].Rows.Count > 0)
					{
						if (!Convert.IsDBNull(tRrVal.Tables[0].Rows[0]["itiposer"]) || Convert.ToString(tRrVal.Tables[0].Rows[0]["itiposer"]) != "")
						{
							if (Convert.ToString(tRrVal.Tables[0].Rows[0]["itiposer"]).ToUpper() != Module1.vstHosUr.ToUpper())
							{
								//MsgBox "Habitaci�n utilizada por otro servicio", vbInformation + vbOKOnly
								string[] tempRefParam3 = new string[]{};
                                Serrores.oClass.CtrlMensaje = this;
                                Serrores.iresume = Convert.ToInt32(Serrores.oClass.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1430, RcAdmision, tempRefParam3));
                                Serrores.oClass.CtrlMensaje = null;
                                return;
							}
						}
					}
					tRrVal.Close();
					// dar de alta la cama o la cuna, para ello distinguimos el estadosala
					//oscar 27 / 11 / 2003
					//If ESTADO_CAMA = 1 Then  ' cama
					//    SprPlantas.TypePictPicture = IlIconos.ListImages(5).Picture
					//Else   'cuna
					//    SprPlantas.TypePictPicture = IlIconos.ListImages(35).Picture
					//End If
					switch(ESTADO_CAMA)
					{
						case 1 :  // cama 
                            SprPlantas.Rows[SprPlantas.Row].Cells[SprPlantas.Col].Value = IlIconos.Images[4];
							break;
						case 7 :  //cuna 
                            SprPlantas.Rows[SprPlantas.Row].Cells[SprPlantas.Col].Value = IlIconos.Images[34];
							break;
						case 12 :  //sillon 
                            SprPlantas.Rows[SprPlantas.Row].Cells[SprPlantas.Col].Value = IlIconos.Images[41];
							break;
					}
                    //-------
                    if (SprPlantas.Col == maxhabitaci)
                    {
                        tipoalta = "A";
                    }
                    else
                    {
                        tipoalta = "B";
                    }

                    //RECOGE LA CAMA ANTES DEL REFRESCO
                    PLANTAMOD = SprPlantas.Row;
                    HABITAMOD = SprPlantas.Col;
                    //RECOGE LA CAMA SITUADA A LA IZQUIERA Y ABAJO DEL GRID
                    instancia_cama = new CAMA_ESTADO();
                    CAMA_ESTADO tempLoadForm = instancia_cama;

                    //instancia_cama.FNRECOGETAG(fnfilacama(SprPlantas.Row, SprPlantas.Col).ToString("D2"), ((SprPlantas.Col - inicio_columna)).ToString("D2"), "  ", tipoalta, Module1.vstHosUr, "ALTA", this, RcAdmision, ESTADO_CAMA);
                    instancia_cama.FNRECOGETAG(fnfilacama(SprPlantas.Row, SprPlantas.Col).ToString("D2"), ((SprPlantas.Col - 1)).ToString("D2"), "  ", tipoalta, Module1.vstHosUr, "ALTA", this, RcAdmision, ESTADO_CAMA);
                    instancia_cama.ShowDialog();
                    //refrescar el grid
                    genera_sql();
                    // VUELVE A LA POSICION ORIGEN
                    SprPlantas.Row = PLANTAMOD;
                    SprPlantas.Col = HABITAMOD;
                    plantasel = PLANTAMOD;
                    habitasel = HABITAMOD - inicio_columna;
                    SprPlantas.Rows[SprPlantas.Row ].Cells[SprPlantas.Col].IsSelected = true;
                    carga_camas();
                    
				}
			}
		}

        private void SprPlantas_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void SprPlantas_DragOver(object sender, DragEventArgs e)
		{
			GridViewRowInfo rowInfo = GetRowAtPoint(SprPlantas, SprPlantas.PointToClient(new Point(e.X, e.Y)));
            GridViewColumn colInfo = GetColumnAtPoint(SprPlantas, SprPlantas.PointToClient(new Point(e.X, e.Y)));

            if (rowInfo != null)
                SprPlantas.Row = rowInfo.Index;
            else
                SprPlantas.Row = -1;
            if (colInfo != null)
                SprPlantas.Col = colInfo.Index;
            else
                SprPlantas.Col = -1;
		}

        private void SprPlantas_Leave(object sender, EventArgs e)
        {
            ToolTipMain.SetToolTip(SprPlantas, "");
        }

        public void proSelec_Camas(object fIngreso)
		{
			FIngreso1 = fIngreso;
		}

		public string fnRecogerPac(int fnRow)
		{
			//********************RECOGER LOS DATOS DEL PACIENTE DEL SPREAD*************************
			string result = String.Empty;
			string stCamas = String.Empty; //Columna de las camas
			int iNuPlan = 0, iNuHab = 0;
			string stNuCama = String.Empty;
			DataSet RrGideCam = null;
			string stPacHab = String.Empty;
			//**********************
			//On Error GoTo Etiqueta

			//****************Compruebo que hay datos
			SPRCAMAS.Col = 1;
			SPRCAMAS.Row = fnRow;
			if (SPRCAMAS.Text != "")
			{
				stCamas = SPRCAMAS.Text;
				iNuPlan = Convert.ToInt32(Double.Parse(stCamas.Substring(0, Math.Min(2, stCamas.Length))));
				iNuHab = Convert.ToInt32(Double.Parse(stCamas.Substring(2, Math.Min(2, stCamas.Length - 2))));
				stNuCama = stCamas.Substring(4);
				stPacHab = "SELECT " + " DCAMASBOva.GIDENPAC" + " FROM " + " DCAMASBOva " + " WHERE " + " DCAMASBOva.GPLANTAS = " + iNuPlan.ToString() + " AND " + " DCAMASBOva.GHABITAC = " + iNuHab.ToString() + " AND " + " DCAMASBOva.GCAMASBO = '" + stNuCama.Trim() + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stPacHab, RcAdmision);
				RrGideCam = new DataSet();
				tempAdapter.Fill(RrGideCam);
				result = Convert.ToString(RrGideCam.Tables[0].Rows[0]["gidenpac"]);
				RrGideCam.Close();
			}

			return result;
		}		

		public void TbRefresco_Click(Object eventSender, EventArgs eventArgs)
		{
			int HABITA_IZQUI = 1;
			int PLANTA_ABAJO = 0;
			//refresco = True
			vlCol = SprPlantas.Col;
			vlRow = SprPlantas.Row;
			SprPlantas.GetBottomRightCell(HABITA_IZQUI, PLANTA_ABAJO);
			cambio_filtro = true;
			genera_sql();
			SprPlantas.Col = HABITA_IZQUI;
			SprPlantas.Row = PLANTA_ABAJO;
			SprPlantas.Action = UpgradeHelpers.Spread.ActionConstants.ActionActiveCell;

            var currentRow = SprPlantas.Rows[PLANTA_ABAJO].Cells[HABITA_IZQUI].RowInfo;
            var currentCol = SprPlantas.Rows[PLANTA_ABAJO].Cells[HABITA_IZQUI].ColumnInfo;
            SprPlantas_CellClick(SprPlantas, new GridViewCellEventArgs(currentRow, currentCol, null));

            if (SPRCAMAS.MaxRows >= vlRowCam)
			{
                currentRow = SPRCAMAS.Rows[0].Cells[2].RowInfo;
                currentCol = SPRCAMAS.Rows[0].Cells[2].ColumnInfo;
                SPRCAMAS_CellClick(SPRCAMAS, new GridViewCellEventArgs(currentRow, currentCol, null));
            }
            else
			{
                currentRow = SPRCAMAS.Rows[0].Cells[2].RowInfo;
                currentCol = SPRCAMAS.Rows[0].Cells[2].ColumnInfo;
                SPRCAMAS_CellClick(SPRCAMAS, new GridViewCellEventArgs(currentRow, currentCol, null));
            }

        }


		public void ver_Click(Object eventSender, EventArgs eventArgs)
		{

			//presentar pantalla de consulta de la cama
			SPRCAMAS.Col = 1;
            SPRCAMAS.Row = SPRCAMAS.CurrentRow.Index + 1;
			//CAMA_ESTADO.Tag = SPRCAMAS.Text & "B VER"
			string VERPLAN = SPRCAMAS.Text.Substring(0, Math.Min(2, SPRCAMAS.Text.Length));
			string VERHABI = SPRCAMAS.Text.Substring(2, Math.Min(2, SPRCAMAS.Text.Length - 2));
			string VERCAMA = SPRCAMAS.Text.Substring(4, Math.Min(2, SPRCAMAS.Text.Length - 4));
			CAMA_ESTADO instancia_camav = new CAMA_ESTADO();
			CAMA_ESTADO tempLoadForm = instancia_camav;
			instancia_camav.FNRECOGETAG(VERPLAN, VERHABI, VERCAMA, "B", Module1.vstHosUr, "VER", this, RcAdmision, ESTADO_CAMA);
			instancia_camav.ShowDialog();
		}

		//oscar 27/11/2003
		//AL PARECER NO SE USA EL PROCEDIMIENTO CoincidenTipoCama() EN TODO EL PROYECTO
		//PERO POR SI ACASO SE HA ACTUALIZADO CON EL NUEVO REQUERIMIENTO DE PINTAR LOS ICONOS
		//CORRESPONDIENTE A LAS PLAZAS DE DIA.
		//---------
		//UPGRADE_NOTE: (7001) The following declaration (CoincidenTipoCama) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private bool CoincidenTipoCama()
		//{
				//bool result = false;
				//FixedLengthString IndCuna = new FixedLengthString(1);
				//int IndColumna = SPRCAMAS.Col; // indicador de cama/cuna/sillon
				//SPRCAMAS.Col = 6;
				//IndCuna.Value = SPRCAMAS.Text;
				//
				//oscar 27/11/2003
				//FixedLengthString indSillon = new FixedLengthString(1);
				//SPRCAMAS.Col = 7;
				////UPGRADE_ISSUE: (2064) FPSpread.vaSpread method SPRCAMAS.Tex was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				////UPGRADE_TODO: (1067) Member Tex is not defined in type FPSpread.vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				//indSillon.Value = (FixedLengthString) SPRCAMAS.getTex();
				//
				//if (indSillon.Value == "S" && IndCuna.Value == "N" && this.Imseleccionada.Image.Equals(this.IlIconos.Images[43]))
				//{ // es un sillon
					//result = true;
				//}
				//--------------
				//
				//if (IndCuna.Value == "S" && indSillon.Value == "N" && this.Imseleccionada.Image.Equals(this.IlIconos.Images[39]))
				//{ // es una cuna
					//result = true;
				//}
				//
				//if (IndCuna.Value == "N" && indSillon.Value == "N" && this.Imseleccionada.Image.Equals(this.IlIconos.Images[5]))
				//{ // es una cama
					//result = true;
				//}
				//
				//
				//SPRCAMAS.Col = IndColumna;
				//return result;
		//}

		private object CargarCombos()
		{
			//CAMAS
			string SqlCombos = "select * from DTIPOCAM ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlCombos, RcAdmision);
			DataSet rrCombos = new DataSet();
			tempAdapter.Fill(rrCombos);
            
            CmbCama.Items.Add("");
			foreach (DataRow iteration_row in rrCombos.Tables[0].Rows)
			{
                CmbCama.Items.Add(new RadListDataItem(iteration_row["dtipcama"].ToString().ToUpper(), Convert.ToInt32(iteration_row["gtipocam"])));
			}
			rrCombos.Close();
			//SERVICIO
			SqlCombos = "select * from DSERVICIVA where ";
			if (Module1.vstHosUr == "U")
			{
				SqlCombos = SqlCombos + "iurgenci = 'S' ";
			}
			else if (Module1.vstHosUr == "H")
			{ 
				SqlCombos = SqlCombos + "ihospita = 'S' ";
			}


			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SqlCombos, RcAdmision);
			rrCombos = new DataSet();
			tempAdapter_2.Fill(rrCombos);
			CmbServicio.Items.Add("");
			foreach (DataRow iteration_row_2 in rrCombos.Tables[0].Rows)
			{
                CmbServicio.Items.Add(new RadListDataItem(iteration_row_2["dnomserv"].ToString().ToUpper(), Convert.ToInt32(iteration_row_2["gservici"])));
			}
			//UNIDADES DE ENFERMERIA
			SqlCombos = "select * from DUNIENFE where ";
			if (Module1.vstHosUr == "U")
			{
				SqlCombos = SqlCombos + "itiposer = 'U'";
			}
			else if (Module1.vstHosUr == "H")
			{ 
				SqlCombos = SqlCombos + "itiposer = 'H'";
			}
			SqlCombos = SqlCombos + "and fborrado is null ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SqlCombos, RcAdmision);
			rrCombos = new DataSet();
			tempAdapter_3.Fill(rrCombos);
            CmbUniEnfe.Items.Add("");
			int i = 1;
			foreach (DataRow iteration_row_3 in rrCombos.Tables[0].Rows)
			{
                CmbUniEnfe.Items.Add(new RadListDataItem(iteration_row_3["dunidenf"].ToString(), iteration_row_3["gunidenf"].ToString()));
				i++;
			}
			rrCombos.Close();
			return null;
		}



		public void Recoger_datos(string Nombre, string Apellido1, string Apellido2, string fechaNac, string Sexo, string Domicilio, string NIF, string Identificador, string CodPostal, string Asegurado, string codPob, string Historia, string CodSOC, string Filprov, string IndActPen = "", string Inspec = "")
		{
			string stCodPacE = String.Empty;
			string VstNombreFiliacion = String.Empty;
			// recogida de datos de filiacion
			//Public Sub Recoger_datos(nombre As String, Apellido1 As String, Apellido2 As String, FechaNacto As String, Sexo As String,        Historia As String,        Identificador As String)

			if (Identificador.Trim() != "")
			{
				ObjReservaAdmision.IdentificadorPaciente = Identificador.Trim();
				VstNombreFiliacion = (Apellido1.Trim() + " " + Apellido2.Trim() + ", " + Nombre.Trim()).ToUpper();
				//OSCAR C ENE 2005
				stCodPacE = Identificador.Trim();
				ObjReservaAdmision.gsocieda = CodSOC;
				ObjReservaAdmision.ginspecc = Inspec;
				//---------------
			}
			else
			{
				ObjReservaAdmision.IdentificadorPaciente = "";
				VstNombreFiliacion = "";
				//OSCAR C ENE 2005
				stCodPacE = "";
				ObjReservaAdmision.gsocieda = "";
				ObjReservaAdmision.ginspecc = "";
				//---------------
			}
		}

		public void proLLamarFiliacion()
		{
            object Clase_Filiacion = null;
            string VstOperacionReserva = String.Empty;
            //'Dim formReservas As New Mant_reserva
            string sqlTablaReservas = String.Empty;
            DataSet RrTablaReservas = null;
            try
            {
                // ralopezn_TODO_X_3 11/04/2016
                //Clase_Filiacion = new filiacionDLL.Filiacion();
                ////UPGRADE_TODO: (1067) Member Load is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //Clase_Filiacion.Load(Clase_Filiacion, this, "ALTA", "Admision", RcAdmision);
                // COMPROBAR QUE NO ESTA YA EN LA TABLA DE RESERVAS
                if (ObjReservaAdmision.IdentificadorPaciente == "")
                {
                    return;
                }
                //sqlTablaReservas = "select gidenpac from aresingrva where gidenpac='" & ObjReservaAdmision.IdentificadorPaciente & "'"
                //Set RrTablaReservas = RcAdmision.OpenResultset(sqlTablaReservas, rdOpenKeyset)
                //If RrTablaReservas.RowCount = 1 Then
                //    MsgBox "Ya existe una reserva para el paciente"
                //    Exit Sub
                //End If
                // COMPROBAR QUE NO ESTA EN ADMISION
                sqlTablaReservas = "select gidenpac from aepisadm where gidenpac='" + ObjReservaAdmision.IdentificadorPaciente + "'";
                sqlTablaReservas = sqlTablaReservas + " and faltplan is null";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTablaReservas, RcAdmision);
                RrTablaReservas = new DataSet();
                tempAdapter.Fill(RrTablaReservas);
                if (RrTablaReservas.Tables[0].Rows.Count == 1)
                {
                    MessageBox.Show("El paciente se encuentra en admisi�n", Application.ProductName);
                    return;
                }

                VstOperacionReserva = "A�ADIR_RESERVA";

                object obManteReservas = null;
                ObjReservaAdmision.OperacionReserva = VstOperacionReserva;
                ObjReservaAdmision.usuario = Module1.VstCodUsua;

                ObjReservaAdmision.CodigoCompletoCama = LbDcama.Text;
                ObjReservaAdmision.UnidadReserva = fnCamaUnidad(LbDcama.Text);

                // ralopezn_TODO_X_3 11/04/2016
                //obManteReservas = new reserva.MCRESERVA();
                ////UPGRADE_TODO: (1067) Member Mante_Reserva is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //obManteReservas.Mante_Reserva(this, RcAdmision, ObjReservaAdmision);
                obManteReservas = null;
                //UPGRADE_WARNING: (2065) Form event VISUALIZACION.Activated has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
                VISUALIZACION_Activated(this, new EventArgs());

                SprPlantas.Col = vlCol;
                vlRow = (SprPlantas.Row == vlRow) ? -1 : 0;
                TbRefresco.PerformClick();
                // ralopezn_TODO_X_3 11/04/2016
                //SprPlantas_CellClick(SprPlantas, new EventArgs());
                //-----------
                cmdReserva.Enabled = false;
            }
            catch (Exception ex)
            {
                Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Module1.VstCodUsua, "MateCamas:ProLlamarFiliacion", ex);
            }
		}

		public void RecogerIDreserva(string ParaIdReserva)
		{
		}

		private string fnCamaUnidad(string strCama)
		{
			string result = String.Empty;
			string sqlUnidad = String.Empty;
			DataSet RrRUnidad = null;
			try
			{
				sqlUnidad = "select gcounien from dcamasbo where ";
				sqlUnidad = sqlUnidad + " gplantas=" + strCama.Substring(0, Math.Min(2, strCama.Length)) + " and ghabitac=";
				sqlUnidad = sqlUnidad + strCama.Substring(2, Math.Min(2, strCama.Length - 2)) + " and gcamasbo='" + strCama.Substring(4).Trim() + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlUnidad, RcAdmision);
				RrRUnidad = new DataSet();
				tempAdapter.Fill(RrRUnidad);
				if (RrRUnidad.Tables[0].Rows.Count != 0)
				{
					result = Convert.ToString(RrRUnidad.Tables[0].Rows[0]["gcounien"]);
				}
				else
				{
					result = "";
				}
				RrRUnidad.Close();
				return result.Trim();
			}
			catch (Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), Module1.VstCodUsua, "ManteCamas:fnCamaUnidad", ex);
				return result;
			}
		}
		private void VISUALIZACION_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}

        private void SprPlantas_CreateCell(object sender, Telerik.WinControls.UI.GridViewCreateCellEventArgs e)
        {
            if (e.CellType == typeof(GridRowHeaderCellElement) && e.Row is GridDataRowElement)
            {
                e.CellType = typeof(UpgradeHelpers.Spread.FpSpreadGridRowHeaderCellElement);
            }
        }

        private void SprPlantas_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                _isMouseRightDown = true;
            } else
            {
                _isMouseRightDown = false;
            }
        }

        private void SprPlantas_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            var cell = e.CellElement;
            if (cell is UpgradeHelpers.Spread.FpSpreadGridRowHeaderCellElement)
            {
                cell.Image = null;
                if (e.RowIndex < nplantas.Length)
                {
                    cell.Text = nplantas[e.RowIndex].ToString();
                }
                else
                {
                    cell.Text = "";
                }
            }
        }


        /// <summary>
        /// Permite pintar en el RowHeader el numero de cama
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SprCamas_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {            
            if (e.CellElement is UpgradeHelpers.Spread.FpSpreadGridRowHeaderCellElement)
            {                
                if (e.RowIndex < ncamas.Length)
                {
                    if (ncamas[e.RowIndex] != null)
                    {
                        e.CellElement.Text = ncamas[e.RowIndex].ToString();                        
                        e.CellElement.Image = null;                        
                    }
                }               
            }

            GridTableHeaderCellElement headerCell = e.CellElement as GridTableHeaderCellElement;
            if (headerCell != null)
            {
                headerCell.Text = "Cama";
            }            
        }

        /// <summary>
        /// Permite pintar en el RowHeader el numero de cama
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SPRCAMAS_CreateCell(object sender, Telerik.WinControls.UI.GridViewCreateCellEventArgs e)
        {
            if (e.CellType == typeof(GridRowHeaderCellElement) && e.Row is GridDataRowElement)
            {
                e.CellType = typeof(UpgradeHelpers.Spread.FpSpreadGridRowHeaderCellElement);                
            }
        }
    }
}