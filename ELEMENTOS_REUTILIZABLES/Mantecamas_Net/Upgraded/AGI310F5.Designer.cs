using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace mantecamas
{
	partial class SERVICIOMEDICOR
	{

		#region "Upgrade Support "
		private static SERVICIOMEDICOR m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static SERVICIOMEDICOR DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new SERVICIOMEDICOR();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cmdSalir", "cbAceptar", "cbbSerdestino", "cbbMedDestino", "lbMedDestino", "lbSerDes", "lbMedOr", "lbseror", "lbMedicor", "lbServior", "frMovmiento", "lbPac", "lbPaciente", "frPacienet"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cmdSalir;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadDropDownList cbbSerdestino;
		public Telerik.WinControls.UI.RadDropDownList cbbMedDestino;
		public Telerik.WinControls.UI.RadLabel lbMedDestino;
		public Telerik.WinControls.UI.RadLabel lbSerDes;
		public Telerik.WinControls.UI.RadLabel lbMedOr;
		public Telerik.WinControls.UI.RadLabel lbseror;
		public Telerik.WinControls.UI.RadTextBox lbMedicor;
		public Telerik.WinControls.UI.RadTextBox lbServior;
		public Telerik.WinControls.UI.RadGroupBox frMovmiento;
		public Telerik.WinControls.UI.RadLabel lbPac;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadGroupBox frPacienet;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cmdSalir = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.frMovmiento = new Telerik.WinControls.UI.RadGroupBox();
            this.cbbSerdestino = new Telerik.WinControls.UI.RadDropDownList();
            this.cbbMedDestino = new Telerik.WinControls.UI.RadDropDownList();
            this.lbMedDestino = new Telerik.WinControls.UI.RadLabel();
            this.lbSerDes = new Telerik.WinControls.UI.RadLabel();
            this.lbMedOr = new Telerik.WinControls.UI.RadLabel();
            this.lbseror = new Telerik.WinControls.UI.RadLabel();
            this.lbMedicor = new Telerik.WinControls.UI.RadTextBox();
            this.lbServior = new Telerik.WinControls.UI.RadTextBox();
            this.frPacienet = new Telerik.WinControls.UI.RadGroupBox();
            this.lbPac = new Telerik.WinControls.UI.RadLabel();
            this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frMovmiento)).BeginInit();
            this.frMovmiento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSerdestino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMedDestino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMedDestino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSerDes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMedOr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbseror)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMedicor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbServior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPacienet)).BeginInit();
            this.frPacienet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbPac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdSalir
            // 
            this.cmdSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSalir.Location = new System.Drawing.Point(451, 204);
            this.cmdSalir.Name = "cmdSalir";
            this.cmdSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSalir.Size = new System.Drawing.Size(81, 29);
            this.cmdSalir.TabIndex = 9;
            this.cmdSalir.Text = "&Cancelar";
            this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(363, 204);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 29);
            this.cbAceptar.TabIndex = 8;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // frMovmiento
            // 
            this.frMovmiento.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frMovmiento.Controls.Add(this.cbbSerdestino);
            this.frMovmiento.Controls.Add(this.cbbMedDestino);
            this.frMovmiento.Controls.Add(this.lbMedDestino);
            this.frMovmiento.Controls.Add(this.lbSerDes);
            this.frMovmiento.Controls.Add(this.lbMedOr);
            this.frMovmiento.Controls.Add(this.lbseror);
            this.frMovmiento.Controls.Add(this.lbMedicor);
            this.frMovmiento.Controls.Add(this.lbServior);
            this.frMovmiento.HeaderText = "";
            this.frMovmiento.Location = new System.Drawing.Point(3, 45);
            this.frMovmiento.Name = "frMovmiento";
            this.frMovmiento.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frMovmiento.Size = new System.Drawing.Size(526, 154);
            this.frMovmiento.TabIndex = 3;
            // 
            // cbbSerdestino
            // 
            this.cbbSerdestino.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbbSerdestino.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbSerdestino.Location = new System.Drawing.Point(99, 81);
            this.cbbSerdestino.Name = "cbbSerdestino";
            this.cbbSerdestino.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbSerdestino.Size = new System.Drawing.Size(418, 20);
            this.cbbSerdestino.TabIndex = 13;
            this.cbbSerdestino.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbSerdestino_SelectedIndexChanged);
            // 
            // cbbMedDestino
            // 
            this.cbbMedDestino.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbbMedDestino.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbMedDestino.Location = new System.Drawing.Point(99, 117);
            this.cbbMedDestino.Name = "cbbMedDestino";
            this.cbbMedDestino.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbMedDestino.Size = new System.Drawing.Size(418, 20);
            this.cbbMedDestino.TabIndex = 12;
            this.cbbMedDestino.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbMedDestino_SelectedIndexChanged);
            // 
            // lbMedDestino
            // 
            this.lbMedDestino.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbMedDestino.Location = new System.Drawing.Point(12, 123);
            this.lbMedDestino.Name = "lbMedDestino";
            this.lbMedDestino.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbMedDestino.Size = new System.Drawing.Size(86, 18);
            this.lbMedDestino.TabIndex = 11;
            this.lbMedDestino.Text = "M�dico destino:";
            // 
            // lbSerDes
            // 
            this.lbSerDes.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbSerDes.Location = new System.Drawing.Point(12, 84);
            this.lbSerDes.Name = "lbSerDes";
            this.lbSerDes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbSerDes.Size = new System.Drawing.Size(87, 18);
            this.lbSerDes.TabIndex = 10;
            this.lbSerDes.Text = "Servicio destino:";
            // 
            // lbMedOr
            // 
            this.lbMedOr.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbMedOr.Location = new System.Drawing.Point(12, 48);
            this.lbMedOr.Name = "lbMedOr";
            this.lbMedOr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbMedOr.Size = new System.Drawing.Size(81, 18);
            this.lbMedOr.TabIndex = 7;
            this.lbMedOr.Text = "M�dico origen:";
            // 
            // lbseror
            // 
            this.lbseror.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbseror.Location = new System.Drawing.Point(12, 21);
            this.lbseror.Name = "lbseror";
            this.lbseror.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbseror.Size = new System.Drawing.Size(83, 18);
            this.lbseror.TabIndex = 6;
            this.lbseror.Text = "Servicio origen:";
            // 
            // lbMedicor
            // 
            this.lbMedicor.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbMedicor.Location = new System.Drawing.Point(99, 48);
            this.lbMedicor.Name = "lbMedicor";
            this.lbMedicor.Enabled = false;
            this.lbMedicor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbMedicor.Size = new System.Drawing.Size(412, 20);
            this.lbMedicor.TabIndex = 5;
            // 
            // lbServior
            // 
            this.lbServior.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbServior.Location = new System.Drawing.Point(99, 15);
            this.lbServior.Name = "lbServior";
            this.lbServior.Enabled = false;
            this.lbServior.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbServior.Size = new System.Drawing.Size(412, 20);
            this.lbServior.TabIndex = 4;
            // 
            // frPacienet
            // 
            this.frPacienet.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frPacienet.Controls.Add(this.lbPac);
            this.frPacienet.Controls.Add(this.lbPaciente);
            this.frPacienet.HeaderText = "";
            this.frPacienet.Location = new System.Drawing.Point(3, 0);
            this.frPacienet.Name = "frPacienet";
            this.frPacienet.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frPacienet.Size = new System.Drawing.Size(526, 43);
            this.frPacienet.TabIndex = 0;
            // 
            // lbPac
            // 
            this.lbPac.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPac.Location = new System.Drawing.Point(9, 18);
            this.lbPac.Name = "lbPac";
            this.lbPac.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPac.Size = new System.Drawing.Size(51, 18);
            this.lbPac.TabIndex = 2;
            this.lbPac.Text = "Paciente:";
            // 
            // lbPaciente
            // 
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Location = new System.Drawing.Point(75, 15);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.Enabled = false;
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(436, 20);
            this.lbPaciente.TabIndex = 1;
            // 
            // SERVICIOMEDICOR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 238);
            this.ControlBox = false;
            this.Controls.Add(this.cmdSalir);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.frMovmiento);
            this.Controls.Add(this.frPacienet);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SERVICIOMEDICOR";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Cambio Servicio/M�dico - AGI310F5";
            this.Closed += new System.EventHandler(this.SERVICIOMEDICOR_Closed);
            this.Load += new System.EventHandler(this.SERVICIOMEDICOR_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frMovmiento)).EndInit();
            this.frMovmiento.ResumeLayout(false);
            this.frMovmiento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSerdestino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMedDestino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMedDestino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbSerDes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMedOr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbseror)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbMedicor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbServior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frPacienet)).EndInit();
            this.frPacienet.ResumeLayout(false);
            this.frPacienet.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbPac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}