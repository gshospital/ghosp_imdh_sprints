using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;

namespace mantecamas
{
	public partial class CAMA_ESTADO
		: RadForm
	{
		CheckState Estancia_Dcamhist = (CheckState) 0;
		string vstTipoSer = String.Empty;
		string ACCION = String.Empty;
		int planta = 0;
		int habita = 0;
		string cama = String.Empty;
		string stEstado = String.Empty;
		bool bEnf = false;
		string stEnf = String.Empty;
		bool bTCama = false;
		bool bEstado = false;
		bool bInhab = false;
		VISUALIZACION Instancia = null;
		SqlConnection RcAdmision = null;
		int CodigoEstado = 0;

		//Dim CodigoEstado As Long
		public CAMA_ESTADO()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            this.TopMost = true;
        }

		public void FNRECOGETAG(string PARAPLANTA, string PARAHABITA, string ParaCama, string PARATIPO, string PARASERVI, string PARAACCION, VISUALIZACION formulario, SqlConnection conexion, int Estado)
		{
			this.Tag = PARAPLANTA + PARAHABITA + ParaCama + PARATIPO + PARASERVI + PARAACCION;
			Instancia = formulario;
			RcAdmision = conexion;
			CodigoEstado = Estado;
		}

		//****************************************************************************************
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*      O.Frias 21/02/2007  Realizo la actualizaci�n del nuevo dato(tel�fono) de camas. *
		//*                                                                                      *
		//****************************************************************************************
		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string stSqCama = String.Empty;
			DataSet RrCamaN = null;
			string sqcamasexo = String.Empty;
			DataSet RrCamasexo = null;
			bool VstContinua = false;
			string sqDcamhist = String.Empty;
			DataSet rrDcamhist = null;

			//Dim sqlCuna As String
			//Dim RrCuna As rdoResultset
			//Dim bCunaCorrecto As Boolean

			//Dim ibucle As Long

			//************************************
			//On Error GoTo Etiqueta
			//***********************************
			bool VstError = false;
			this.Cursor = Cursors.WaitCursor;
			string vstfecha = DateTimeHelper.ToString(DateTime.Today);
			string vsthora = DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("NN") + ":" + DateTime.Now.ToString("SS");
			string vstfecha_hora_sys = vstfecha + " " + vsthora;

			switch(ACCION)
			{
				case "ALTA" : 
					stSqCama = "SELECT * FROM DCAMASBO WHERE " + " GPLANTAS=" + planta.ToString() + " AND GHABITAC = " + habita.ToString() + 
					           " AND GCAMASBO='" + tbCama.Text.Trim() + "'"; 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSqCama, RcAdmision); 
					RrCamaN = new DataSet(); 
					tempAdapter.Fill(RrCamaN); 
					//        ' si es cuna no dejar meter cama 
					//        If RrCamaN("gtipocam") <> CbbTipocama.ItemData(CbbTipocama.ListIndex) Then 'si ha cambiado el estado 
					//            sqlCuna = "select gtipocam from dtipocamva " 
					//            If RrCamaN("iescuna") = "S" Then  'si tenia cuna -> obtengo todos los codigos de cuna 
					//                sqlCuna = sqlCuna & " where iescuna ='S'" 
					//            Else 
					//                sqlCuna = sqlCuna & " where iescuna ='N'"  'si no el resto 
					//            End If 
					//            Set RrCuna = RcAdmision.OpenResultset(sqlCuna, rdOpenKeyset) 
					//            bCunaCorrecto = False 
					//            Do While Not RrCuna.EOF 
					//                If RrCamaN("gtipocam") = CbbTipocama.ItemData(CbbTipocama.ListIndex) Then 
					//                    bCunaCorrecto = True ' ha encontrado -> correcto 
					//                    Exit Do 
					//                End If 
					//            RrCuna.MoveNext 
					//            Loop 
					//            RrCuna.Close 
					//            If Not bCunaCorrecto Then  ' error de tipo de estado 
					//                MsgBox "Tipo cama seleccionada no es coherente (Cama/Cuna)", vbInformation 
					//                Me.MousePointer = 0 
					//                Exit Sub 
					//            End If 
					//        End If 
					// DAR MENSAJE DE AVISO SI TIENE CAMAS CON O SIN 
					// RESTRICCION DE SEXO DENTRO DE LA MISMO HABITACION 
					if (Chbrestriccion.CheckState == CheckState.Unchecked)
					{
						// COMPROBAR QUE EXISTEN CAMAS CON RESTRICCION DE SEXO
						sqcamasexo = "SELECT * FROM DCAMASBOVA WHERE " + " GPLANTAS=" + planta.ToString() + " AND GHABITAC = " + habita.ToString() + " AND GCAMASBO='" + tbCama.Text.Trim() + "' AND " + "IRESTSEX='S'";
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqcamasexo, RcAdmision);
						RrCamasexo = new DataSet();
						tempAdapter_2.Fill(RrCamasexo);
						if (RrCamasexo.Tables[0].Rows.Count > 0)
						{
							//mensaje existen camas con restriccion de sexo en esta
							//habitacion
						}
						RrCamasexo.Close();
					}
					else
					{
						// COMPROBAR QUE EXISTEN CAMAS SIN RESTRICCION DE SEXO
						sqcamasexo = "SELECT * FROM DCAMASBOVA WHERE " + " GPLANTAS=" + planta.ToString() + " AND GHABITAC = " + habita.ToString() + " AND GCAMASBO='" + tbCama.Text.Trim() + "' AND " + "IRESTSEX='N'";
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqcamasexo, RcAdmision);
						RrCamasexo = new DataSet();
						tempAdapter_3.Fill(RrCamasexo);
						if (RrCamasexo.Tables[0].Rows.Count > 0)
						{
							//mensaje existen camas sin restriccion de sexo en esta
							//habitacion
						}
						RrCamasexo.Close();
					} 
					 
					if (RrCamaN.Tables[0].Rows.Count > 0)
					{
						//MsgBox "Cama ya existente", vbInformation + vbOKOnly, "Altas de camas"						
						short tempParam = 1080;
						string[] tempParam2 = new string[]{LbCama.Text};
						Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam,  RcAdmision,  tempParam2);
						RrCamaN.Close();
						tbCama.Focus();
						VstError = true;
						//Exit Sub
					}
					else
					{
						if (!bEnf)
						{
							//MsgBox "Debe seleccionar un servicio de enfermeria", vbInformation + vbOKOnly, "Alta camas"							
							short tempParam3 = 1040;
							string[] tempParam4 = new string[]{LbEnfermeria.Text};
							Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam3,  RcAdmision,  tempParam4);
							CbbEnfermeria.Focus();
							VstError = true;
							//Exit Sub
						}
						else
						{
							if (!bTCama)
							{
								//MsgBox "Debe seleccionar un tipo de cama", vbInformation + vbOKOnly, "Alta camas"								
								short tempParam5 = 1040;
								string[] tempParam6 = new string[]{LbTipocama.Text};
								Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam5,  RcAdmision,  tempParam6);
								CbbTipocama.Focus();
								VstError = true;
								//Exit Sub
							}
							else
							{
								if (!bEstado)
								{
									//MsgBox "Debe seleccionar un estado de cama", vbInformation + vbOKOnly, "Alta camas"									
									short tempParam7 = 1040;
									string[] tempParam8 = new string[]{LbEstado.Text};
									Serrores.iresume = (int)Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam7,  RcAdmision,  tempParam8);
									CbbEstado.Focus();
									VstError = true;
									//Exit Sub
								}
								else
								{
									VstContinua = true;
									if (stEstado == "I")
									{
										if (!bInhab)
										{
											//MsgBox "Debe seleccionar un motivo de inhabiliatacion"											
											short tempParam9 = 1040;
											string[] tempParam10 = new string[]{LbMotivo.Text};
											Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam9,  RcAdmision,  tempParam10);
											CbbEnfermeria.Focus();
											VstContinua = false;
											VstError = true;
											//Exit Sub
										}
									}
									if (VstContinua)
									{
										if (tbCama.Text.Trim() == "")
										{
											//MsgBox "Debe seleccionar un identificador de cama", vbInformation + vbOKOnly, "Alta camas"											
											short tempParam11 = 1040;
											string[] tempParam12 = new string[]{LbCama.Text};
											Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam11,  RcAdmision,  tempParam12);
											tbCama.Focus();
											VstError = true;
											//Exit Sub
										}
										else
										{
											//RcAdmision.BeginTrans

											stSqCama = " select * from dcamasbo where 2=1";
											SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSqCama, RcAdmision);
											RrCamaN = new DataSet();
											tempAdapter_4.Fill(RrCamaN);
											
											RrCamaN.AddNew();
											
											RrCamaN.Tables[0].Rows[0]["gplantas"] = planta;
											
											RrCamaN.Tables[0].Rows[0]["ghabitac"] = habita;
											
											RrCamaN.Tables[0].Rows[0]["gcamasbo"] = tbCama.Text.Trim();
											
											RrCamaN.Tables[0].Rows[0]["gcounien"] = stEnf;

                                            RrCamaN.Tables[0].Rows[0]["gtipocam"] = CbbTipocama.Items[CbbTipocama.SelectedIndex].Value.ToString();
											
											RrCamaN.Tables[0].Rows[0]["fcamesta"] = DateTime.Now;
											if (Chbrestriccion.CheckState == CheckState.Unchecked)
											{												
												RrCamaN.Tables[0].Rows[0]["irestsex"] = "N";
											}
											else if (Chbrestriccion.CheckState == CheckState.Checked)
											{ 												
												RrCamaN.Tables[0].Rows[0]["irestsex"] = "S";
											}
											//oscar C: 06/10/04
											if (chbVirtual.CheckState == CheckState.Unchecked)
											{												
												RrCamaN.Tables[0].Rows[0]["ivirtual"] = "N";
											}
											else if (chbVirtual.CheckState == CheckState.Checked)
											{ 												
												RrCamaN.Tables[0].Rows[0]["ivirtual"] = "S";
											}
											//------
											//'''                                If ChbEstancia.Value = 0 Then
											//'''                                     RrCamaN("igeneest") = "N"
											//'''                                ElseIf ChbEstancia.Value = 1 Then
											
											RrCamaN.Tables[0].Rows[0]["igeneest"] = "S";
											//'''                                End If
											
											RrCamaN.Tables[0].Rows[0]["iestcama"] = stEstado;
											if (stEstado == "I")
											{												        
												RrCamaN.Tables[0].Rows[0]["gmotinHA"] = Cbbinhabilitacion.Items[Cbbinhabilitacion.SelectedIndex].Value.ToString();
											}
											if (CbbServicio.SelectedIndex > -1)
											{												                
												RrCamaN.Tables[0].Rows[0]["gservici"] = CbbServicio.Items[CbbServicio.SelectedIndex].Value.ToString();
											}
											else
											{												
												RrCamaN.Tables[0].Rows[0]["gservici"] = DBNull.Value;
											}

											//************************ O.Frias 21/02/2007 ************************
											if (txtTelefono.Text.Trim() != "")
											{												
												RrCamaN.Tables[0].Rows[0]["ntelecam"] = txtTelefono.Text.Trim();
											}
											else
											{												
												RrCamaN.Tables[0].Rows[0]["ntelecam"] = DBNull.Value;
											}
											//************************ O.Frias 21/02/2007 ************************

											string tempQuery = RrCamaN.Tables[0].TableName;											
											SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_4);
                                            tempAdapter_4.Update(RrCamaN, tempQuery);
											//RcAdmision.CommitTrans
										}
									}
								}
							}
						}
					} 
					break;
				case "MODIFICAR" : 
					if (ChbEstancia.CheckState != Estancia_Dcamhist)
					{
						sqDcamhist = "select * from DCAMHIST where 2=1";
						SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sqDcamhist, RcAdmision);
						rrDcamhist = new DataSet();
						tempAdapter_6.Fill(rrDcamhist);
						
						rrDcamhist.AddNew();
						
						rrDcamhist.Tables[0].Rows[0]["gplantas"] = planta;
						
						rrDcamhist.Tables[0].Rows[0]["ghabitac"] = habita;
						
						rrDcamhist.Tables[0].Rows[0]["gcamasbo"] = tbCama.Text.Trim();
						
						rrDcamhist.Tables[0].Rows[0]["fsistema"] = vstfecha_hora_sys;
						
						rrDcamhist.Tables[0].Rows[0]["gusuario"] = Module1.VstCodUsua;
						//''            If Estancia_Dcamhist = 1 Then						
						rrDcamhist.Tables[0].Rows[0]["igeneest"] = "S";
						//''            Else
						//''               rrDcamhist("igeneest") = "N"
						//''            End If
						string tempQuery_2 = rrDcamhist.Tables[0].TableName;						
						SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_6);
                        tempAdapter_6.Update(rrDcamhist, tempQuery_2);
						rrDcamhist.Close();
					} 
					stSqCama = "SELECT * FROM DCAMASBO WHERE " + " GPLANTAS=" + planta.ToString() + " AND GHABITAC = " + habita.ToString() + 
					           " AND GCAMASBO='" + tbCama.Text.Trim() + "'"; 
					SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(stSqCama, RcAdmision); 
					RrCamaN = new DataSet(); 
					tempAdapter_8.Fill(RrCamaN); 
					// 
					// si es cuna no dejar meter cama 
					//        If RrCamaN("gtipocam") <> Me.CbbTipocama.ItemData(CbbTipocama.ListIndex) Then 'si ha cambiado el estado 
					//            sqlCuna = "select gtipocam from dtipocamva " 
					//            If RrCamaN("iescuna") = "S" Then  'si tenia cuna -> obtengo todos los codigos de cuna 
					//                sqlCuna = sqlCuna & " where iescuna ='S'" 
					//            Else 
					//                sqlCuna = sqlCuna & " where iescuna ='N'" 
					//            End If 
					//            Set RrCuna = RcAdmision.OpenResultset(sqlCuna, rdOpenKeyset) 
					//            bCunaCorrecto = False 
					//            Do While Not RrCuna.EOF  ' recorremos el conjunto de registros del tipo de cama 
					//                If RrCamaN("gtipocam") = CbbTipocama.ItemData(CbbTipocama.ListIndex) Then 
					//                    bCunaCorrecto = True  ' ha encontrado -> correcto 
					//                    Exit Do 
					//                End If 
					//            RrCuna.MoveNext 
					//            Loop 
					//            RrCuna.Close 
					//            If Not bCunaCorrecto Then  ' error de tipo de estado 
					//                MsgBox "Tipo cama seleccionada no es coherente (Cama/Cuna)", vbInformation 
					//                Me.MousePointer = 0 
					//                Exit Sub 
					//            End If 
					//        End If 
					if (RrCamaN.Tables[0].Rows.Count == 1)
					{
						if (!bEnf)
						{
							//MsgBox "Debe seleccionar un servicio de enfermeria", vbInformation + vbOKOnly, "Alta camas"							
							short tempParam13 = 1040;
							string[] tempParam14 = new string[]{LbEnfermeria.Text};
							Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam13,  RcAdmision,  tempParam14);
							CbbEnfermeria.Focus();
							this.Cursor = Cursors.Default;
							return;
						}
						if (!bTCama)
						{
							//MsgBox "Debe seleccionar un tipo de cama", vbInformation + vbOKOnly, "Alta camas"							
							short tempParam15 = 1040;
							string[] tempParam16 = new string[]{LbTipocama.Text};
							Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam15,  RcAdmision,  tempParam16);
							CbbTipocama.Focus();
							this.Cursor = Cursors.Default;
							return;
						}
						if (!bEstado)
						{
							//MsgBox "Debe seleccionar un estado de cama", vbInformation + vbOKOnly, "Alta camas"
							
							short tempParam17 = 1040;
							string[] tempParam18 = new string[]{LbEstado.Text};
							Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam17,  RcAdmision,  tempParam18);
							CbbEstado.Focus();
							this.Cursor = Cursors.Default;
							return;
						}
						if (stEstado == "I")
						{
							if (!bInhab)
							{
								//MsgBox "Debe seleccionar un motivo de inhabiliatacion"
								
								short tempParam19 = 1040;
								string[] tempParam20 = new string[]{LbMotivo.Text};
								Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam19,  RcAdmision,  tempParam20);
								CbbEnfermeria.Focus();
								this.Cursor = Cursors.Default;
								return;
							}
						}
						
						if (bEnf)
						{							
							RrCamaN.Tables[0].Rows[0]["gcounien"] = stEnf;
						}
						if (bTCama)
						{							        
							RrCamaN.Tables[0].Rows[0]["gtipocam"] = CbbTipocama.Items[CbbTipocama.SelectedIndex].Value.ToString();
						}
						if (bEstado)
						{							
							RrCamaN.Tables[0].Rows[0]["iestcama"] = stEstado;
						}
						if (bInhab && stEstado == "I")
						{							        
							RrCamaN.Tables[0].Rows[0]["gmotinHA"] = Cbbinhabilitacion.Items[Cbbinhabilitacion.SelectedIndex].Value.ToString();
						}
						else
						{							
							RrCamaN.Tables[0].Rows[0]["gmotinha"] = DBNull.Value;
						}
						if (Chbrestriccion.CheckState == CheckState.Unchecked)
						{							
							RrCamaN.Tables[0].Rows[0]["irestsex"] = "N";
						}
						else if (Chbrestriccion.CheckState == CheckState.Checked)
						{ 							
							RrCamaN.Tables[0].Rows[0]["irestsex"] = "S";
						}
						//oscar C: 06/10/04
						if (chbVirtual.CheckState == CheckState.Unchecked)
						{							
							RrCamaN.Tables[0].Rows[0]["ivirtual"] = "N";
						}
						else if (chbVirtual.CheckState == CheckState.Checked)
						{ 							
							RrCamaN.Tables[0].Rows[0]["ivirtual"] = "S";
						}
						//---------
						//''            If ChbEstancia.Value = 0 Then
						//''                    RrCamaN("igeneest") = "N"
						//''            ElseIf ChbEstancia.Value = 1 Then
						
						RrCamaN.Tables[0].Rows[0]["igeneest"] = "S";
						//''            End If
						if (CbbServicio.SelectedIndex > -1)
						{							        
							RrCamaN.Tables[0].Rows[0]["gservici"] = CbbServicio.Items[CbbServicio.SelectedIndex].Value.ToString();
						}
						else
						{							
							RrCamaN.Tables[0].Rows[0]["gservici"] = DBNull.Value;
						}
						
						RrCamaN.Tables[0].Rows[0]["fborrado"] = DBNull.Value;

						//************************ O.Frias 21/02/2007 ************************
						if (txtTelefono.Text.Trim() != "")
						{							
							RrCamaN.Tables[0].Rows[0]["ntelecam"] = txtTelefono.Text.Trim();
						}
						else
						{							
							RrCamaN.Tables[0].Rows[0]["ntelecam"] = DBNull.Value;
						}
						//************************ O.Frias 21/02/2007 ************************
						string tempQuery_3 = RrCamaN.Tables[0].TableName;						
						SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_8);
                        tempAdapter_8.Update(RrCamaN, tempQuery_3);
					} 
					break;
			}
			this.Cursor = Cursors.Default;
			if (!VstError)
			{
				this.Tag = "";
				Instancia.FNRECOGETAG("ACEPTAR");
				this.Close();
			}
			return;
			//***************************
			this.Cursor = Cursors.Default;			
			MessageBox.Show("Error en camas", Application.ProductName);
		}
		
		private bool isInitializingComponent;

		private void CbbEnfermeria_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (CbbEnfermeria.Text == "")
			{
				bEnf = false;
			}
		}

		private void CbbEnfermeria_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
        {
			DataSet RS2 = null;
			string sql2 = String.Empty;
			if (CbbEnfermeria.SelectedIndex != -1)
			{                
				sql2 = "SELECT * from dunienfeva WHERE dunidenf='" + CbbEnfermeria.Items[CbbEnfermeria.SelectedIndex].Text.Trim() + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql2, RcAdmision);
				RS2 = new DataSet();
				tempAdapter.Fill(RS2);
				if (RS2.Tables[0].Rows.Count == 1)
				{					
					stEnf = Convert.ToString(RS2.Tables[0].Rows[0]["gunidenf"]);
				}
				bEnf = true;
				//cbAceptar.Enabled = True
			}
			else
			{
				bEnf = false;
			}
			PROACTIVA_ACEPTAR();
		}

		public void PROACTIVA_ACEPTAR()
		{
			if (tbCama.Text != "" && bEnf && bTCama && bEstado)
			{
				if (stEstado == "I")
				{
					cbAceptar.Enabled = bInhab;
				}
				else
				{
					cbAceptar.Enabled = true;
				}
			}
			else
			{
				cbAceptar.Enabled = false;
			}
			if (CbbEstado.Text == "INHABILITADA")
			{
				Cbbinhabilitacion.Enabled = true;
			}
		}

		private void CbbEnfermeria_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		
		private void CbbEstado_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (CbbEstado.Text == "")
			{
				bEstado = false;
			}

		}

		private void CbbEstado_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
        {
			//compruebo el estado de la cama
			if (CbbEstado.SelectedIndex != -1)
			{                				
                switch (int.Parse(CbbEstado.Items[CbbEstado.SelectedIndex].Value.ToString()))
				{
					case 1 : 
						stEstado = "L"; 
						Cbbinhabilitacion.Enabled = false; 
						Cbbinhabilitacion.Text = ""; 
						bInhab = false; 
						break;
					case 2 : 
						stEstado = "I"; 
						Cbbinhabilitacion.Enabled = true; 
						//bInhab = True 
						break;
					case 3 : 
						stEstado = "B"; 
						break;
					case 4 : 
						stEstado = "O"; 
						break;
				}
				bEstado = true;
				//cbAceptar.Enabled = True
			}
			else
			{
				bEstado = false;
			}
			PROACTIVA_ACEPTAR();
		}

		private void CbbEstado_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		
		private void Cbbinhabilitacion_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (Cbbinhabilitacion.Text == "")
			{
				bInhab = false;
			}
		}

		private void Cbbinhabilitacion_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
        {
			bInhab = Cbbinhabilitacion.SelectedIndex != -1;
			PROACTIVA_ACEPTAR();
		}

		private void Cbbinhabilitacion_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		
		private void CbbTipocama_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (CbbTipocama.Text == "")
			{
				bTCama = false;
			}
		}

		private void CbbTipocama_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
        {
			bTCama = CbbTipocama.SelectedIndex != -1;
			PROACTIVA_ACEPTAR();
		}

		private void CbbTipocama_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void cmdSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Tag = "";
			Instancia.FNRECOGETAG("CANCELAR");
			this.Close();
			//devolver el valor de que se ha cancelado
		}

		private void CAMA_ESTADO_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				Application.DoEvents();
				string N = Application.OpenForms.Count.ToString();
				//cargar combo de enfermeria
				//cargar combo de tipos de cama
				//cargar combo de motivos de inhabilitacion
				ACCION = Convert.ToString(this.Tag).Substring(8);
				vstTipoSer = Convert.ToString(this.Tag).Substring(7, Math.Min(1, Convert.ToString(this.Tag).Length - 7));
				cbAceptar.Enabled = false;
				bEnf = false;
				bTCama = false;
				bInhab = false;
				bEstado = false;

				if (Convert.ToString(this.Tag) != "")
				{
					//CAMBIAR PARA QUE SIRVA PARA CAMA NUEVA
					proCama_Tag();
					cargar_combos();
					if (ACCION == "VER")
					{
						//cargar los controles con los datos de la cama
						carga_cama();
						//DESHABILITAR CONTROLES
						CONTROL_HABI(false);
					}
					else if (ACCION == "ALTA")
					{ 
						this.Text = "Mantenimiento Camas AGI310F3";
						//HABILITA CONTROLES
						CONTROL_HABI(true);
					}
					else if (ACCION == "MODIFICAR")
					{ 
						//HABILITAR CONTROLES
						this.Text = "Mantenimiento Camas AGI310F3";
						//HABILITA CONTROLES
						CONTROL_HABI(true);
						carga_cama();
						tbCama.Enabled = false;
						bEnf = true;
						bTCama = true;
						bEstado = true;
						bInhab = stEstado == "I";
						PROACTIVA_ACEPTAR();
					}
					tbPlanta.Enabled = false;
					//tbHabitacion.Enabled = False
					//Cbbinhabilitacion.Enabled = False
				}


				//oscar C (06/10/04)
				chbVirtual.Visible = mbAcceso.ObtenerCodigoSConsglo("CVIRTUAL", "VALFANU1", RcAdmision).Trim().ToUpper() == "S";
				//-------------------
			}
		}

		//****************************************************************************************
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*      O.Frias 21/02/2007  Incorporo el nuevo control del telefono                     *
		//*                                                                                      *
		//****************************************************************************************
		public void CONTROL_HABI(bool Estado)
		{
			if (ACCION == "ALTA")
			{
				tbCama.Enabled = true;
			}
			else
			{
				tbCama.Enabled = Estado;
			}
			Cbbinhabilitacion.Enabled = Estado;
			CbbEnfermeria.Enabled = Estado;
			CbbTipocama.Enabled = Estado;
			CbbEstado.Enabled = Estado;
			ChbEstancia.Enabled = Estado;
			Chbrestriccion.Enabled = Estado;
			chbVirtual.Enabled = Estado; //oscar C: 06/10/04
			CbbServicio.Enabled = Estado;

			//************************ O.Frias 21/02/2007 ************************
			txtTelefono.Enabled = Estado;
			//************************ O.Frias 21/02/2007 ************************
		}

		//****************************************************************************************
		//*                                                                                      *
		//*  Modificaciones:                                                                     *
		//*                                                                                      *
		//*      O.Frias 21/02/2007  Incorporo la nueva operativa de tel�fono                    *
		//*                                                                                      *
		//****************************************************************************************

		public void carga_cama()
		{
			int X = 0;
			string enfermeria = String.Empty;
			int tipocama = 0;
			string Restriccion = String.Empty;
			string lvirtual = String.Empty; //oscar C: 06/10/04
			string estancia = String.Empty;
			int inhabilitacion = 0;
			string Estado = String.Empty;
			int Servicio = 0;

			string sqlcambio = "select * from dcamasbo where " + "gplantas =" + planta.ToString() + " and ghabitac =" + habita.ToString() + " and gcamasbo='" + cama + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlcambio, RcAdmision);
			DataSet RrCambio = new DataSet();
			tempAdapter.Fill(RrCambio);
			//obtengo el codigo de la enfermeria
			if (RrCambio.Tables[0].Rows.Count == 1)
			{				
				if (Convert.ToString(RrCambio.Tables[0].Rows[0]["GCOUNIEN"]) != "")
				{					
					enfermeria = Convert.ToString(RrCambio.Tables[0].Rows[0]["GCOUNIEN"]);
				}
				
				if (Convert.ToString(RrCambio.Tables[0].Rows[0]["GTIPOCAM"]) != "")
				{					
					tipocama = Convert.ToInt32(RrCambio.Tables[0].Rows[0]["GTIPOCAM"]);
				}
				
				if (Convert.ToString(RrCambio.Tables[0].Rows[0]["IRESTSEX"]) != "")
				{					
					Restriccion = Convert.ToString(RrCambio.Tables[0].Rows[0]["IRESTSEX"]);
				}
				
				if (Convert.ToString(RrCambio.Tables[0].Rows[0]["IVIRTUAL"]) != "")
				{					
					lvirtual = Convert.ToString(RrCambio.Tables[0].Rows[0]["IVIRTUAL"]);
				} //oscar c: 06/10/04
				
				if (Convert.ToString(RrCambio.Tables[0].Rows[0]["IGENEEST"]) != "")
				{					
					estancia = Convert.ToString(RrCambio.Tables[0].Rows[0]["IGENEEST"]);
				}
				
				if (Convert.ToString(RrCambio.Tables[0].Rows[0]["IESTCAMA"]) != "")
				{					
					Estado = Convert.ToString(RrCambio.Tables[0].Rows[0]["IESTCAMA"]);
				}
				
				if (Convert.ToString(RrCambio.Tables[0].Rows[0]["GMOTINHA"]) != "")
				{					
					inhabilitacion = Convert.ToInt32(RrCambio.Tables[0].Rows[0]["GMOTINHA"]);
				}
				
				if (Convert.ToString(RrCambio.Tables[0].Rows[0]["GSERVICI"]) != "")
				{					
					Servicio = Convert.ToInt32(RrCambio.Tables[0].Rows[0]["GSERVICI"]);
				}

				//************************ O.Frias 21/02/2007 ************************				
				if (!Convert.IsDBNull(RrCambio.Tables[0].Rows[0]["ntelecam"]))
				{					
					txtTelefono.Text = Convert.ToString(RrCambio.Tables[0].Rows[0]["ntelecam"]);
				}
				//************************ O.Frias 21/02/2007 ************************
			}
			
			RrCambio.Close();
			//obtengo la descripci�n de la enfermeria
			sqlcambio = "select DUNIDENF from DUNIENFEva where " + "GUNIDENF ='" + enfermeria + "'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlcambio, RcAdmision);
			RrCambio = new DataSet();
			tempAdapter_2.Fill(RrCambio);
			if (RrCambio.Tables[0].Rows.Count == 1)
			{
				//ACTUALIZAR EL INDICE DEL COMBO
				
				CbbEnfermeria.Text = Convert.ToString(RrCambio.Tables[0].Rows[0]["DUNIDENF"]);
               
                CbbEnfermeria.SelectedIndex = CbbEnfermeria.FindString(RrCambio.Tables[0].Rows[0]["DUNIDENF"].ToString());

                // si depues de comprobar las unidades de enfermer�a no encuentro la que he
                // sacado en  RrCambio("DUNIDENF"), le pongo que la cama pertenece a la 1� unidad
                // de enfermer�a q. encuentre
                if (CbbEnfermeria.SelectedIndex < 0)
				{                    
					CbbEnfermeria.Text = CbbEnfermeria.Items[0].Text;
					CbbEnfermeria.SelectedIndex = 0;
				}
			}
			//obtengo la descripci�n del tipo de cama
			
			RrCambio.Close();
			sqlcambio = "select DTIPCAMA from DTIPOCAMva where " + "GTIPOCAM =" + tipocama.ToString();
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sqlcambio, RcAdmision);
			RrCambio = new DataSet();
			tempAdapter_3.Fill(RrCambio);
			if (RrCambio.Tables[0].Rows.Count == 1)
			{
				//ACTUALIZAR INDICE DEL COMBO
				
				CbbTipocama.Text = Convert.ToString(RrCambio.Tables[0].Rows[0]["DTIPCAMA"]).Trim();
				X = 0;

                X = CbbTipocama.FindString(Convert.ToString(RrCambio.Tables[0].Rows[0]["DTIPCAMA"]).Trim());                

				if (X == CbbTipocama.Items.Count)
				{
					//no ha encontrado el tipo de cama
					CbbTipocama.SelectedIndex = -1;
					CbbTipocama.Text = "";
				}
				else
				{
					CbbTipocama.SelectedIndex = X;
				}
			}
			
			RrCambio.Close();
			//obtengo restricci�n de sexo
			if (Restriccion == "S")
			{
				Chbrestriccion.CheckState = CheckState.Checked;
			}
			else
			{
				Chbrestriccion.CheckState = CheckState.Unchecked;
			}
			//oscar C: 06/10/04
			if (lvirtual == "S")
			{
				chbVirtual.CheckState = CheckState.Checked;
			}
			else
			{
				chbVirtual.CheckState = CheckState.Unchecked;
			}
			//------------
			//obtengo genera estancia
			if (estancia == "S")
			{
				Estancia_Dcamhist = CheckState.Checked;
				ChbEstancia.CheckState = CheckState.Checked;
			}
			else
			{
				Estancia_Dcamhist = CheckState.Unchecked;
				ChbEstancia.CheckState = CheckState.Unchecked;
			}
			//obtengo estado
			switch(Estado)
			{
				case "L" : 
					CbbEstado.Text = "LIBRE"; 
					break;
				case "O" : 
					CbbEstado.Text = "OCUPADA"; 
					break;
				case "B" : 
					CbbEstado.Text = "BLOQUEADA"; 
					break;
				case "I" : 
					CbbEstado.Text = "INHABILITADA"; 
					break;
				default:
					CbbEstado.Text = ""; 
					break;
			}
			//ACTUALIZAR INDICE DEL COMBO
			X = 0;
            
            X = CbbEstado.FindString(CbbEstado.Text);

			CbbEstado.SelectedIndex = X;
			//obtengo motivo de inhabilitaci�n si existe
			if (CbbEstado.Text == "INHABILITADA")
			{
				sqlcambio = "select DINHABIL from DMOTINHAva where " + "GMOTINHA =" + inhabilitacion.ToString();
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlcambio, RcAdmision);
				RrCambio = new DataSet();
				tempAdapter_4.Fill(RrCambio);
				
				Cbbinhabilitacion.Text = Convert.ToString(RrCambio.Tables[0].Rows[0]["DINHABIL"]);
				
				RrCambio.Close();
				Cbbinhabilitacion.Enabled = true;
				//ACTUALIZAR INDICE DEL COMBO
				X = 0;
               
                X = Cbbinhabilitacion.FindString(Cbbinhabilitacion.Text.Trim());

                Cbbinhabilitacion.SelectedIndex = X;
			}
			else
			{
				Cbbinhabilitacion.Text = "";
				Cbbinhabilitacion.Enabled = false;
			}

			//obtengo la descripci�n del SERVICIO
			sqlcambio = "select  DNOMSERV from DSERVICIVA where " + "GSERVICI =" + Servicio.ToString();
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sqlcambio, RcAdmision);
			RrCambio = new DataSet();
			tempAdapter_5.Fill(RrCambio);
			if (RrCambio.Tables[0].Rows.Count == 1)
			{
				//ACTUALIZAR INDICE DEL COMBO
				
				CbbServicio.Text = Convert.ToString(RrCambio.Tables[0].Rows[0]["DNOMSERV"]);
				X = 0;
                
                X = CbbServicio.FindString(Convert.ToString(RrCambio.Tables[0].Rows[0]["DNOMSERV"]));

                if (X == CbbServicio.Items.Count)
				{
					//no ha encontrado el tipo de servicio
					CbbServicio.SelectedIndex = -1;
					CbbServicio.Text = "";
				}
				else
				{
					CbbServicio.SelectedIndex = X;
				}
			}
			
			RrCambio.Close();
		}

		public void cargar_combos()
		{
            CbbEnfermeria.Items.Clear();
            CbbEstado.Items.Clear();
            Cbbinhabilitacion.Items.Clear();
            CbbServicio.Items.Clear();
            CbbTipocama.Items.Clear();
            
			// Combo de enfermeria
			string sql = "SELECT * from dunienfeva WHERE ITIPOSER='" + vstTipoSer + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcAdmision);
			DataSet rs1 = new DataSet();
			tempAdapter.Fill(rs1);

			foreach (DataRow iteration_row in rs1.Tables[0].Rows)
			{
				if (Convert.ToString(iteration_row["dunidenf"]) != "")
				{
                    CbbEnfermeria.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dunidenf"]).Trim(), Convert.ToString(iteration_row["gunidenf"]).Trim()));
				}
			}

			rs1.Close();

			//combo de tipos de cama
			if (ACCION == "ALTA")
			{
				//oscar 27/11/2003
				//sql = "SELECT * from dtipocamva where iescuna ="
				//If CodigoEstado = 1 Then   ' cama libre
				//    sql = sql & "'N'"
				//End If
				//If CodigoEstado = 7 Then ' cuna libre
				//    sql = sql & "'S'"
				//End If
				sql = "SELECT * from dtipocamva where ";
				switch(CodigoEstado)
				{
					case 1 :  
						sql = sql + " iescuna='N'  AND isillon='N'";  // cama libre 
						break;
					case 7 :  
						sql = sql + " iescuna='S'  AND isillon='N'";  // cuna libre 
						break;
					case 12 :  
						sql = sql + " iescuna='N'  AND isillon='S'";  // sillon libre 
						break;
				}
				//----------------
			}
			else
			{
				sql = "SELECT * from dtipocamva where iescuna in (select iescuna from dcamasbova,DTIPOCAMVA where " + 
				      " gcamasbo ='" + cama + "' and ghabitac=" + habita.ToString() + " and gplantas =" + planta.ToString() + " AND " + 
				      " dtipocamva.gtipocam=dcamasbova.gtipocam)";

				//oscar 27/11/2003
				sql = sql + " AND isillon in" + 
				      " (select isillon from dcamasbova,DTIPOCAMVA where " + 
				      " gcamasbo ='" + cama + "' and ghabitac=" + habita.ToString() + " and gplantas =" + planta.ToString() + " AND " + 
				      " dtipocamva.gtipocam=dcamasbova.gtipocam) ";
				//--------
			}
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RcAdmision);
			rs1 = new DataSet();
			tempAdapter_2.Fill(rs1);

			foreach (DataRow iteration_row_2 in rs1.Tables[0].Rows)
			{
				if (Convert.ToString(iteration_row_2["dtipcama"]) != "")
				{
                    CbbTipocama.Items.Add(new RadListDataItem(Convert.ToString(iteration_row_2["dtipcama"]).Trim(), Convert.ToString(iteration_row_2["gtipocam"]).Trim()));
                }
			}

			rs1.Close();

			//combo de estado de la cama
			if (ACCION == "VER")
			{                
                CbbEstado.Items.Add(new RadListDataItem("LIBRE", 1));
                CbbEstado.Items.Add(new RadListDataItem("OCUPADA", 4));
                CbbEstado.Items.Add(new RadListDataItem("BLOQUEADA", 3));
                CbbEstado.Items.Add(new RadListDataItem("INHABILITADA", 2));                                
			}
			else
			{
                CbbEstado.Items.Add(new RadListDataItem("LIBRE", 1));
                CbbEstado.Items.Add(new RadListDataItem("INHABILITADA", 2));                
            }

			// Combo de motivos de inhabilitacion
			sql = "SELECT * from dmotinhava ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, RcAdmision);
			rs1 = new DataSet();
			tempAdapter_3.Fill(rs1);

			foreach (DataRow iteration_row_3 in rs1.Tables[0].Rows)
			{
				if (Convert.ToString(iteration_row_3["dinhabil"]) != "")
				{
                    Cbbinhabilitacion.Items.Add(new RadListDataItem(Convert.ToString(iteration_row_3["dinhabil"]).Trim(), Convert.ToString(iteration_row_3["gmotinha"]).Trim()));
                }
			}
			rs1.Close();

			//OSCAR 27/11/2003
			bool bCamaDia = false;
			if (ACCION == "ALTA")
			{
				if (CodigoEstado == 12)
				{
					bCamaDia = true;
				}
			}
			else
			{
				sql = " select isillon from dcamasbova,DTIPOCAMVA where " + 
				      " gcamasbo ='" + cama + "' and ghabitac=" + habita.ToString() + " and gplantas =" + planta.ToString() + " AND " + 
				      " dtipocamva.gtipocam=dcamasbova.gtipocam";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, RcAdmision);
				rs1 = new DataSet();
				tempAdapter_4.Fill(rs1);
				
				if (Convert.ToString(rs1.Tables[0].Rows[0]["isillon"]).Trim().ToUpper() == "S")
				{
					bCamaDia = true;
				}
				rs1.Close();
			}
			//------

			// Combo de servicios
			sql = "SELECT * from dserviciva  WHERE ";
			if (vstTipoSer == "U")
			{
				sql = sql + "iurgenci = 'S' ";
			}
			else
			{
				sql = sql + "ihospita = 'S' ";
			}

			//oscar 27/11/2003
			//Si damos de alta / modificamos una plaza de dia, solo se mostraran los servicios que tengan el
			//indicador de hospital de dia (icentdia)
			if (bCamaDia)
			{
				sql = sql + " AND icentdia='S'";
			}
			//---------

			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, RcAdmision);
			rs1 = new DataSet();
			tempAdapter_5.Fill(rs1);

			foreach (DataRow iteration_row_4 in rs1.Tables[0].Rows)
			{
				if (Convert.ToString(iteration_row_4["dnomserv"]) != "")
				{
                    CbbServicio.Items.Add(new RadListDataItem(Convert.ToString(iteration_row_4["dnomserv"]).Trim(), Convert.ToString(iteration_row_4["gservici"]).Trim()));                    
                }
			}
			rs1.Close();
		}

		public void proCama_Tag()
		{
			planta = Convert.ToInt32(Double.Parse(Convert.ToString(this.Tag).Substring(0, Math.Min(2, Convert.ToString(this.Tag).Length))));
			habita = Convert.ToInt32(Double.Parse(Convert.ToString(this.Tag).Substring(2, Math.Min(2, Convert.ToString(this.Tag).Length - 2))));
			cama = Convert.ToString(this.Tag).Substring(4, Math.Min(2, Convert.ToString(this.Tag).Length - 4)).Trim();
			string tipoalta = Convert.ToString(this.Tag).Substring(6, Math.Min(1, Convert.ToString(this.Tag).Length - 6));
			vstTipoSer = Convert.ToString(this.Tag).Substring(7, Math.Min(1, Convert.ToString(this.Tag).Length - 7));

			string SqlCmatag = "SELECT DNOMPLAN FROM DPLANTASva WHERE GPLANTAS= " + planta.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlCmatag, RcAdmision);
			DataSet RrCmaTag = new DataSet();
			tempAdapter.Fill(RrCmaTag);
			if (RrCmaTag.Tables[0].Rows.Count == 1)
			{				
				tbPlanta.Text = Convert.ToString(RrCmaTag.Tables[0].Rows[0]["dnomplan"]).Trim();
			}
			RrCmaTag.Close();
			if (tipoalta == "A")
			{
				tbHabitacion.Enabled = true;
				tbHabitacion.Focus();
			}
			else
			{
				tbHabitacion.Enabled = false;

			}
			tbHabitacion.Text = habita.ToString();
			tbCama.Text = cama;
		}

		private void tbCama_Enter(Object eventSender, EventArgs eventArgs)
		{
			if (tbCama.Text == "")
			{
				tbCama.Text = "  ";
			}
			tbCama.SelectionStart = 0;
			tbCama.SelectionLength = Strings.Len(tbCama.Text);
		}

		private void tbCama_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbCama_Leave(Object eventSender, EventArgs eventArgs)
		{
			string tRespuesta = String.Empty;
			if (ACCION == "VER" || ACCION == "MODIFICAR")
			{
				return;
			}
			if (tbCama.Text.Trim() != "")
			{
				tbCama.Text = tbCama.Text.ToUpper();
				PROACTIVA_ACEPTAR();
			}

			if (Strings.Len(tbCama.Text) > 2)
			{
				//MsgBox "Cama no v�lida, el n�mero m�ximo de caracteres permitidos es dos", vbOKOnly + vbInformation, "Alta Camas"
				
				short tempParam = 1020;
				string[] tempParam2 = new string[]{LbCama.Text, "menor o igual", "2"};
				Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam,  RcAdmision,  tempParam2);
			}
			string tstCama = "SELECT * FROM DCAMASBO WHERE " + " GPLANTAS=" + planta.ToString() + " AND GHABITAC = " + habita.ToString() + 
			                 " AND GCAMASBO='" + tbCama.Text.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCama, RcAdmision);
			DataSet tRrCama = new DataSet();
			tempAdapter.Fill(tRrCama);
			if (tRrCama.Tables[0].Rows.Count > 0)
			{				
				if (Convert.IsDBNull(tRrCama.Tables[0].Rows[0]["fborrado"]))
				{ //la cama ya existe
					//MsgBox "Cama ya existente", vbInformation + vbOKOnly, "Altas de camas"					
					short tempParam3 = 1080;
					string[] tempParam4 = new string[]{LbCama.Text};
					Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam3,  RcAdmision,  tempParam4);
					
					tRrCama.Close();
					tbCama.Text = "";
					tbCama.Focus();
					return;
				}
				else if (!Convert.IsDBNull(tRrCama.Tables[0].Rows[0]["fborrado"]))
				{  //la cama ya existe pero esta borrada
					tRespuesta = StringsHelper.Format(planta, "00") + StringsHelper.Format(habita, "00") + tbCama.Text.Trim();
					
					short tempParam5 = 1450;
					string[] tempParam6 = new string[]{tRespuesta, Convert.ToDateTime(tRrCama.Tables[0].Rows[0]["fborrado"]).ToString("dd/MM/yyyy")};
					Serrores.iresume = (int) Serrores.oClass.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempParam5,  RcAdmision,  tempParam6);
					if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.Yes)
					{						
						tRrCama.Close();
						cama = tbCama.Text.Trim();
						ACCION = "MODIFICAR";
						carga_cama();
						return;
					}
					else if (Serrores.iresume == (int)System.Windows.Forms.DialogResult.No)
					{ 						
						tRrCama.Close();
						tbCama.Text = "";
						tbCama.Focus();
						return;
					}
				}
			}
		}

		private void tbHabitacion_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (tbHabitacion.Text == "")
			{
				habita = 0;
			}
			else
			{
				habita = Convert.ToInt32(Double.Parse(tbHabitacion.Text));
			}
		}

		private void tbHabitacion_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void txtTelefono_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void CAMA_ESTADO_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}