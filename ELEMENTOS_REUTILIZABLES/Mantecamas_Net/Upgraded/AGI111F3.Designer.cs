using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace mantecamas
{
	partial class ESTADO_PLANTAS
	{

		#region "Upgrade Support "
		private static ESTADO_PLANTAS m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static ESTADO_PLANTAS DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new ESTADO_PLANTAS();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cmdSalir", "Label8", "PorcentajeTotal", "PorcentajeTotalBloquedas", "PorcentajeTotalInhabilitadas", "PorcentajeTotalOcupadas", "PorcentajeTotalLibres", "lbCabeceraParcial", "TotalParcial", "PorcentajeParcialBloquedas", "PorcentajeParcialInhabilitadas", "PorcentajeParcialOcupadas", "PorcentajeParcialLibres", "Lbtotlibres2", "LbNumlibres", "LbTotocupadas", "LbNumocupadas", "LbTotInhabilitadas", "LbNuminhabilitadas", "LbTotbloqueadas", "LbNumbloqueadas", "LbTotal", "LbNumtotal", "Frmtotal", "LbTiposerv", "LbPlanta", "LbnumPlanta"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cmdSalir;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadTextBox PorcentajeTotal;
		public Telerik.WinControls.UI.RadTextBox PorcentajeTotalBloquedas;
		public Telerik.WinControls.UI.RadTextBox PorcentajeTotalInhabilitadas;
		public Telerik.WinControls.UI.RadTextBox PorcentajeTotalOcupadas;
		public Telerik.WinControls.UI.RadTextBox PorcentajeTotalLibres;
		public Telerik.WinControls.UI.RadLabel lbCabeceraParcial;
		public Telerik.WinControls.UI.RadTextBox TotalParcial;
		public Telerik.WinControls.UI.RadTextBox PorcentajeParcialBloquedas;
		public Telerik.WinControls.UI.RadTextBox PorcentajeParcialInhabilitadas;
		public Telerik.WinControls.UI.RadTextBox PorcentajeParcialOcupadas;
		public Telerik.WinControls.UI.RadTextBox PorcentajeParcialLibres;
		public Telerik.WinControls.UI.RadLabel Lbtotlibres2;
		public Telerik.WinControls.UI.RadTextBox LbNumlibres;
		public Telerik.WinControls.UI.RadLabel LbTotocupadas;
		public Telerik.WinControls.UI.RadTextBox LbNumocupadas;
		public Telerik.WinControls.UI.RadLabel LbTotInhabilitadas;
		public Telerik.WinControls.UI.RadTextBox LbNuminhabilitadas;
		public Telerik.WinControls.UI.RadLabel LbTotbloqueadas;
		public Telerik.WinControls.UI.RadTextBox LbNumbloqueadas;
		public Telerik.WinControls.UI.RadLabel LbTotal;
		public Telerik.WinControls.UI.RadTextBox LbNumtotal;
		public Telerik.WinControls.UI.RadGroupBox Frmtotal;
		public Telerik.WinControls.UI.RadLabel LbTiposerv;
		public Telerik.WinControls.UI.RadLabel LbPlanta;
		public Telerik.WinControls.UI.RadTextBox LbnumPlanta;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cmdSalir = new Telerik.WinControls.UI.RadButton();
            this.Frmtotal = new Telerik.WinControls.UI.RadGroupBox();
            this.Label8 = new Telerik.WinControls.UI.RadLabel();
            this.PorcentajeTotal = new Telerik.WinControls.UI.RadTextBox();
            this.PorcentajeTotalBloquedas = new Telerik.WinControls.UI.RadTextBox();
            this.PorcentajeTotalInhabilitadas = new Telerik.WinControls.UI.RadTextBox();
            this.PorcentajeTotalOcupadas = new Telerik.WinControls.UI.RadTextBox();
            this.PorcentajeTotalLibres = new Telerik.WinControls.UI.RadTextBox();
            this.lbCabeceraParcial = new Telerik.WinControls.UI.RadLabel();
            this.TotalParcial = new Telerik.WinControls.UI.RadTextBox();
            this.PorcentajeParcialBloquedas = new Telerik.WinControls.UI.RadTextBox();
            this.PorcentajeParcialInhabilitadas = new Telerik.WinControls.UI.RadTextBox();
            this.PorcentajeParcialOcupadas = new Telerik.WinControls.UI.RadTextBox();
            this.PorcentajeParcialLibres = new Telerik.WinControls.UI.RadTextBox();
            this.Lbtotlibres2 = new Telerik.WinControls.UI.RadLabel();
            this.LbNumlibres = new Telerik.WinControls.UI.RadTextBox();
            this.LbTotocupadas = new Telerik.WinControls.UI.RadLabel();
            this.LbNumocupadas = new Telerik.WinControls.UI.RadTextBox();
            this.LbTotInhabilitadas = new Telerik.WinControls.UI.RadLabel();
            this.LbNuminhabilitadas = new Telerik.WinControls.UI.RadTextBox();
            this.LbTotbloqueadas = new Telerik.WinControls.UI.RadLabel();
            this.LbNumbloqueadas = new Telerik.WinControls.UI.RadTextBox();
            this.LbTotal = new Telerik.WinControls.UI.RadLabel();
            this.LbNumtotal = new Telerik.WinControls.UI.RadTextBox();
            this.LbTiposerv = new Telerik.WinControls.UI.RadLabel();
            this.LbPlanta = new Telerik.WinControls.UI.RadLabel();
            this.LbnumPlanta = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frmtotal)).BeginInit();
            this.Frmtotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotalBloquedas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotalInhabilitadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotalOcupadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotalLibres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCabeceraParcial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalParcial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeParcialBloquedas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeParcialInhabilitadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeParcialOcupadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeParcialLibres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbtotlibres2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumlibres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTotocupadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumocupadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTotInhabilitadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNuminhabilitadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTotbloqueadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumbloqueadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumtotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTiposerv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPlanta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbnumPlanta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdSalir
            // 
            this.cmdSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSalir.Location = new System.Drawing.Point(228, 230);
            this.cmdSalir.Name = "cmdSalir";
            this.cmdSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSalir.Size = new System.Drawing.Size(81, 29);
            this.cmdSalir.TabIndex = 11;
            this.cmdSalir.Text = "C&errar";
            this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            // 
            // Frmtotal
            // 
            this.Frmtotal.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frmtotal.Controls.Add(this.Label8);
            this.Frmtotal.Controls.Add(this.PorcentajeTotal);
            this.Frmtotal.Controls.Add(this.PorcentajeTotalBloquedas);
            this.Frmtotal.Controls.Add(this.PorcentajeTotalInhabilitadas);
            this.Frmtotal.Controls.Add(this.PorcentajeTotalOcupadas);
            this.Frmtotal.Controls.Add(this.PorcentajeTotalLibres);
            this.Frmtotal.Controls.Add(this.lbCabeceraParcial);
            this.Frmtotal.Controls.Add(this.TotalParcial);
            this.Frmtotal.Controls.Add(this.PorcentajeParcialBloquedas);
            this.Frmtotal.Controls.Add(this.PorcentajeParcialInhabilitadas);
            this.Frmtotal.Controls.Add(this.PorcentajeParcialOcupadas);
            this.Frmtotal.Controls.Add(this.PorcentajeParcialLibres);
            this.Frmtotal.Controls.Add(this.Lbtotlibres2);
            this.Frmtotal.Controls.Add(this.LbNumlibres);
            this.Frmtotal.Controls.Add(this.LbTotocupadas);
            this.Frmtotal.Controls.Add(this.LbNumocupadas);
            this.Frmtotal.Controls.Add(this.LbTotInhabilitadas);
            this.Frmtotal.Controls.Add(this.LbNuminhabilitadas);
            this.Frmtotal.Controls.Add(this.LbTotbloqueadas);
            this.Frmtotal.Controls.Add(this.LbNumbloqueadas);
            this.Frmtotal.Controls.Add(this.LbTotal);
            this.Frmtotal.Controls.Add(this.LbNumtotal);
            this.Frmtotal.HeaderText = "";
            this.Frmtotal.Location = new System.Drawing.Point(7, 52);
            this.Frmtotal.Name = "Frmtotal";
            this.Frmtotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frmtotal.Size = new System.Drawing.Size(302, 172);
            this.Frmtotal.TabIndex = 0;
            this.Frmtotal.TabStop = false;
            // 
            // Label8
            // 
            this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label8.Location = new System.Drawing.Point(226, 15);
            this.Label8.Name = "Label8";
            this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label8.Size = new System.Drawing.Size(60, 18);
            this.Label8.TabIndex = 26;
            this.Label8.Text = "% Hospital";
            // 
            // PorcentajeTotal
            // 
            this.PorcentajeTotal.Cursor = System.Windows.Forms.Cursors.Default;
            this.PorcentajeTotal.Location = new System.Drawing.Point(237, 140);
            this.PorcentajeTotal.Name = "PorcentajeTotal";
            this.PorcentajeTotal.Enabled = false;
            this.PorcentajeTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PorcentajeTotal.Size = new System.Drawing.Size(40, 20);
            this.PorcentajeTotal.TabIndex = 25;
            // 
            // PorcentajeTotalBloquedas
            // 
            this.PorcentajeTotalBloquedas.Cursor = System.Windows.Forms.Cursors.Default;
            this.PorcentajeTotalBloquedas.Location = new System.Drawing.Point(237, 114);
            this.PorcentajeTotalBloquedas.Name = "PorcentajeTotalBloquedas";
            this.PorcentajeTotalBloquedas.Enabled = false;
            this.PorcentajeTotalBloquedas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PorcentajeTotalBloquedas.Size = new System.Drawing.Size(40, 20);
            this.PorcentajeTotalBloquedas.TabIndex = 24;
            // 
            // PorcentajeTotalInhabilitadas
            // 
            this.PorcentajeTotalInhabilitadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.PorcentajeTotalInhabilitadas.Location = new System.Drawing.Point(237, 89);
            this.PorcentajeTotalInhabilitadas.Name = "PorcentajeTotalInhabilitadas";
            this.PorcentajeTotalInhabilitadas.Enabled = false;
            this.PorcentajeTotalInhabilitadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PorcentajeTotalInhabilitadas.Size = new System.Drawing.Size(40, 20);
            this.PorcentajeTotalInhabilitadas.TabIndex = 23;
            // 
            // PorcentajeTotalOcupadas
            // 
            this.PorcentajeTotalOcupadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.PorcentajeTotalOcupadas.Location = new System.Drawing.Point(237, 63);
            this.PorcentajeTotalOcupadas.Name = "PorcentajeTotalOcupadas";
            this.PorcentajeTotalOcupadas.Enabled = false;
            this.PorcentajeTotalOcupadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PorcentajeTotalOcupadas.Size = new System.Drawing.Size(40, 20);
            this.PorcentajeTotalOcupadas.TabIndex = 22;
            // 
            // PorcentajeTotalLibres
            // 
            this.PorcentajeTotalLibres.Cursor = System.Windows.Forms.Cursors.Default;
            this.PorcentajeTotalLibres.Location = new System.Drawing.Point(237, 38);
            this.PorcentajeTotalLibres.Name = "PorcentajeTotalLibres";
            this.PorcentajeTotalLibres.Enabled = false;
            this.PorcentajeTotalLibres.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PorcentajeTotalLibres.Size = new System.Drawing.Size(40, 20);
            this.PorcentajeTotalLibres.TabIndex = 21;
            // 
            // lbCabeceraParcial
            // 
            this.lbCabeceraParcial.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbCabeceraParcial.Location = new System.Drawing.Point(171, 15);
            this.lbCabeceraParcial.Name = "lbCabeceraParcial";
            this.lbCabeceraParcial.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbCabeceraParcial.Size = new System.Drawing.Size(49, 18);
            this.lbCabeceraParcial.TabIndex = 20;
            this.lbCabeceraParcial.Text = "% Planta";
            // 
            // TotalParcial
            // 
            this.TotalParcial.Cursor = System.Windows.Forms.Cursors.Default;
            this.TotalParcial.Location = new System.Drawing.Point(177, 140);
            this.TotalParcial.Name = "TotalParcial";
            this.TotalParcial.Enabled = false;
            this.TotalParcial.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TotalParcial.Size = new System.Drawing.Size(40, 20);
            this.TotalParcial.TabIndex = 19;
            // 
            // PorcentajeParcialBloquedas
            // 
            this.PorcentajeParcialBloquedas.Cursor = System.Windows.Forms.Cursors.Default;
            this.PorcentajeParcialBloquedas.Location = new System.Drawing.Point(177, 114);
            this.PorcentajeParcialBloquedas.Name = "PorcentajeParcialBloquedas";
            this.PorcentajeParcialBloquedas.Enabled = false;
            this.PorcentajeParcialBloquedas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PorcentajeParcialBloquedas.Size = new System.Drawing.Size(40, 20);
            this.PorcentajeParcialBloquedas.TabIndex = 18;
            // 
            // PorcentajeParcialInhabilitadas
            // 
            this.PorcentajeParcialInhabilitadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.PorcentajeParcialInhabilitadas.Location = new System.Drawing.Point(177, 89);
            this.PorcentajeParcialInhabilitadas.Name = "PorcentajeParcialInhabilitadas";
            this.PorcentajeParcialInhabilitadas.Enabled = false;
            this.PorcentajeParcialInhabilitadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PorcentajeParcialInhabilitadas.Size = new System.Drawing.Size(40, 20);
            this.PorcentajeParcialInhabilitadas.TabIndex = 17;
            // 
            // PorcentajeParcialOcupadas
            // 
            this.PorcentajeParcialOcupadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.PorcentajeParcialOcupadas.Location = new System.Drawing.Point(177, 63);
            this.PorcentajeParcialOcupadas.Name = "PorcentajeParcialOcupadas";
            this.PorcentajeParcialOcupadas.Enabled = false;
            this.PorcentajeParcialOcupadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PorcentajeParcialOcupadas.Size = new System.Drawing.Size(40, 20);
            this.PorcentajeParcialOcupadas.TabIndex = 16;
            // 
            // PorcentajeParcialLibres
            // 
            this.PorcentajeParcialLibres.Cursor = System.Windows.Forms.Cursors.Default;
            this.PorcentajeParcialLibres.Location = new System.Drawing.Point(177, 38);
            this.PorcentajeParcialLibres.Name = "PorcentajeParcialLibres";
            this.PorcentajeParcialLibres.Enabled = false;
            this.PorcentajeParcialLibres.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PorcentajeParcialLibres.Size = new System.Drawing.Size(40, 20);
            this.PorcentajeParcialLibres.TabIndex = 15;
            // 
            // Lbtotlibres2
            // 
            this.Lbtotlibres2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Lbtotlibres2.Location = new System.Drawing.Point(9, 39);
            this.Lbtotlibres2.Name = "Lbtotlibres2";
            this.Lbtotlibres2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Lbtotlibres2.Size = new System.Drawing.Size(74, 18);
            this.Lbtotlibres2.TabIndex = 10;
            this.Lbtotlibres2.Text = "Camas Libres:";
            // 
            // LbNumlibres
            // 
            this.LbNumlibres.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbNumlibres.Location = new System.Drawing.Point(123, 37);
            this.LbNumlibres.Name = "LbNumlibres";
            this.LbNumlibres.Enabled = false;
            this.LbNumlibres.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbNumlibres.Size = new System.Drawing.Size(37, 20);
            this.LbNumlibres.TabIndex = 9;
            // 
            // LbTotocupadas
            // 
            this.LbTotocupadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbTotocupadas.Location = new System.Drawing.Point(9, 64);
            this.LbTotocupadas.Name = "LbTotocupadas";
            this.LbTotocupadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbTotocupadas.Size = new System.Drawing.Size(95, 18);
            this.LbTotocupadas.TabIndex = 8;
            this.LbTotocupadas.Text = "Camas Ocupadas:";
            // 
            // LbNumocupadas
            // 
            this.LbNumocupadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbNumocupadas.Location = new System.Drawing.Point(123, 62);
            this.LbNumocupadas.Name = "LbNumocupadas";
            this.LbNumocupadas.Enabled = false;
            this.LbNumocupadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbNumocupadas.Size = new System.Drawing.Size(37, 20);
            this.LbNumocupadas.TabIndex = 7;
            // 
            // LbTotInhabilitadas
            // 
            this.LbTotInhabilitadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbTotInhabilitadas.Location = new System.Drawing.Point(9, 89);
            this.LbTotInhabilitadas.Name = "LbTotInhabilitadas";
            this.LbTotInhabilitadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbTotInhabilitadas.Size = new System.Drawing.Size(108, 18);
            this.LbTotInhabilitadas.TabIndex = 6;
            this.LbTotInhabilitadas.Text = "Camas Inhabilitadas:";
            // 
            // LbNuminhabilitadas
            // 
            this.LbNuminhabilitadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbNuminhabilitadas.Location = new System.Drawing.Point(123, 88);
            this.LbNuminhabilitadas.Name = "LbNuminhabilitadas";
            this.LbNuminhabilitadas.Enabled = false;
            this.LbNuminhabilitadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbNuminhabilitadas.Size = new System.Drawing.Size(37, 20);
            this.LbNuminhabilitadas.TabIndex = 5;
            // 
            // LbTotbloqueadas
            // 
            this.LbTotbloqueadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbTotbloqueadas.Location = new System.Drawing.Point(9, 115);
            this.LbTotbloqueadas.Name = "LbTotbloqueadas";
            this.LbTotbloqueadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbTotbloqueadas.Size = new System.Drawing.Size(103, 18);
            this.LbTotbloqueadas.TabIndex = 4;
            this.LbTotbloqueadas.Text = "Camas Bloqueadas:";
            // 
            // LbNumbloqueadas
            // 
            this.LbNumbloqueadas.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbNumbloqueadas.Location = new System.Drawing.Point(123, 113);
            this.LbNumbloqueadas.Name = "LbNumbloqueadas";
            this.LbNumbloqueadas.Enabled = false;
            this.LbNumbloqueadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbNumbloqueadas.Size = new System.Drawing.Size(37, 20);
            this.LbNumbloqueadas.TabIndex = 3;
            this.LbNumbloqueadas.Text = "0";
            // 
            // LbTotal
            // 
            this.LbTotal.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbTotal.Location = new System.Drawing.Point(9, 140);
            this.LbTotal.Name = "LbTotal";
            this.LbTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbTotal.Size = new System.Drawing.Size(84, 18);
            this.LbTotal.TabIndex = 2;
            this.LbTotal.Text = "Total de camas:";
            // 
            // LbNumtotal
            // 
            this.LbNumtotal.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbNumtotal.Location = new System.Drawing.Point(123, 139);
            this.LbNumtotal.Name = "LbNumtotal";
            this.LbNumtotal.Enabled = false;
            this.LbNumtotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbNumtotal.Size = new System.Drawing.Size(37, 20);
            this.LbNumtotal.TabIndex = 1;
            this.LbNumtotal.Text = "0";
            // 
            // LbTiposerv
            // 
            this.LbTiposerv.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbTiposerv.Location = new System.Drawing.Point(7, 3);
            this.LbTiposerv.Name = "LbTiposerv";
            this.LbTiposerv.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbTiposerv.Size = new System.Drawing.Size(2, 2);
            this.LbTiposerv.TabIndex = 14;
            this.LbTiposerv.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LbPlanta
            // 
            this.LbPlanta.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbPlanta.Location = new System.Drawing.Point(12, 32);
            this.LbPlanta.Name = "LbPlanta";
            this.LbPlanta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbPlanta.Size = new System.Drawing.Size(39, 18);
            this.LbPlanta.TabIndex = 13;
            this.LbPlanta.Text = "Planta:";
            // 
            // LbnumPlanta
            // 
            this.LbnumPlanta.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbnumPlanta.Location = new System.Drawing.Point(80, 30);
            this.LbnumPlanta.Name = "LbnumPlanta";
            this.LbnumPlanta.Enabled = false;
            this.LbnumPlanta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbnumPlanta.Size = new System.Drawing.Size(53, 20);
            this.LbnumPlanta.TabIndex = 12;
            // 
            // ESTADO_PLANTAS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 268);
            this.Controls.Add(this.cmdSalir);
            this.Controls.Add(this.Frmtotal);
            this.Controls.Add(this.LbTiposerv);
            this.Controls.Add(this.LbPlanta);
            this.Controls.Add(this.LbnumPlanta);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 23);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ESTADO_PLANTAS";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Estado de las planta - AGI111F3";
            this.Activated += new System.EventHandler(this.ESTADO_PLANTAS_Activated);
            this.Closed += new System.EventHandler(this.ESTADO_PLANTAS_Closed);
            this.Load += new System.EventHandler(this.ESTADO_PLANTAS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frmtotal)).EndInit();
            this.Frmtotal.ResumeLayout(false);
            this.Frmtotal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotalBloquedas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotalInhabilitadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotalOcupadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeTotalLibres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCabeceraParcial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalParcial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeParcialBloquedas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeParcialInhabilitadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeParcialOcupadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorcentajeParcialLibres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lbtotlibres2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumlibres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTotocupadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumocupadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTotInhabilitadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNuminhabilitadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTotbloqueadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumbloqueadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNumtotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTiposerv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPlanta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbnumPlanta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion
    }
}