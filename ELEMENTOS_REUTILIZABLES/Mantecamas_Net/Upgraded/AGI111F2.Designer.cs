using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace mantecamas
{
	partial class PACIENTE_CAMA
	{

		#region "Upgrade Support "
		private static PACIENTE_CAMA m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static PACIENTE_CAMA DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new PACIENTE_CAMA();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cmdSalir", "lblNumTele", "lblTele", "LbNBloqueada", "LbBloqueada", "LbCServicio", "LbDhistoria", "LbDasegurado", "LbDfinanciadora", "LbServicio", "LbDedad", "LbDsexo", "LbDcama", "LbDpaciente", "LbHistoria", "LbAsegurado", "LbFinanciadora", "LbEdad", "LbSexo", "LbCama", "LbPaciente", "FrmPaciente"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cmdSalir;
		public Telerik.WinControls.UI.RadTextBox lblNumTele;
		public Telerik.WinControls.UI.RadLabel lblTele;
		public Telerik.WinControls.UI.RadTextBox LbNBloqueada;
		public Telerik.WinControls.UI.RadLabel LbBloqueada;
		public Telerik.WinControls.UI.RadLabel LbCServicio;
		public Telerik.WinControls.UI.RadTextBox LbDhistoria;
		public Telerik.WinControls.UI.RadTextBox LbDasegurado;
		public Telerik.WinControls.UI.RadTextBox LbDfinanciadora;
		public Telerik.WinControls.UI.RadTextBox LbServicio;
		public Telerik.WinControls.UI.RadTextBox LbDedad;
		public Telerik.WinControls.UI.RadTextBox LbDsexo;
		public Telerik.WinControls.UI.RadTextBox LbDcama;
		public Telerik.WinControls.UI.RadTextBox LbDpaciente;
		public Telerik.WinControls.UI.RadLabel LbHistoria;
		public Telerik.WinControls.UI.RadLabel LbAsegurado;
		public Telerik.WinControls.UI.RadLabel LbFinanciadora;
		public Telerik.WinControls.UI.RadLabel LbEdad;
		public Telerik.WinControls.UI.RadLabel LbSexo;
		public Telerik.WinControls.UI.RadLabel LbCama;
		public Telerik.WinControls.UI.RadLabel LbPaciente;
		public Telerik.WinControls.UI.RadGroupBox FrmPaciente;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cmdSalir = new Telerik.WinControls.UI.RadButton();
            this.FrmPaciente = new Telerik.WinControls.UI.RadGroupBox();
            this.lblNumTele = new Telerik.WinControls.UI.RadTextBox();
            this.lblTele = new Telerik.WinControls.UI.RadLabel();
            this.LbNBloqueada = new Telerik.WinControls.UI.RadTextBox();
            this.LbBloqueada = new Telerik.WinControls.UI.RadLabel();
            this.LbCServicio = new Telerik.WinControls.UI.RadLabel();
            this.LbDhistoria = new Telerik.WinControls.UI.RadTextBox();
            this.LbDasegurado = new Telerik.WinControls.UI.RadTextBox();
            this.LbDfinanciadora = new Telerik.WinControls.UI.RadTextBox();
            this.LbServicio = new Telerik.WinControls.UI.RadTextBox();
            this.LbDedad = new Telerik.WinControls.UI.RadTextBox();
            this.LbDsexo = new Telerik.WinControls.UI.RadTextBox();
            this.LbDcama = new Telerik.WinControls.UI.RadTextBox();
            this.LbDpaciente = new Telerik.WinControls.UI.RadTextBox();
            this.LbHistoria = new Telerik.WinControls.UI.RadLabel();
            this.LbAsegurado = new Telerik.WinControls.UI.RadLabel();
            this.LbFinanciadora = new Telerik.WinControls.UI.RadLabel();
            this.LbEdad = new Telerik.WinControls.UI.RadLabel();
            this.LbSexo = new Telerik.WinControls.UI.RadLabel();
            this.LbCama = new Telerik.WinControls.UI.RadLabel();
            this.LbPaciente = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmPaciente)).BeginInit();
            this.FrmPaciente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumTele)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTele)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNBloqueada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbBloqueada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDhistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDasegurado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDfinanciadora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDedad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDsexo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDcama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDpaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbAsegurado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFinanciadora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbEdad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbSexo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdSalir
            // 
            this.cmdSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSalir.Location = new System.Drawing.Point(384, 196);
            this.cmdSalir.Name = "cmdSalir";
            this.cmdSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSalir.Size = new System.Drawing.Size(81, 28);
            this.cmdSalir.TabIndex = 16;
            this.cmdSalir.Text = "C&errar";
            this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            // 
            // FrmPaciente
            // 
            this.FrmPaciente.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmPaciente.Controls.Add(this.lblNumTele);
            this.FrmPaciente.Controls.Add(this.lblTele);
            this.FrmPaciente.Controls.Add(this.LbNBloqueada);
            this.FrmPaciente.Controls.Add(this.LbBloqueada);
            this.FrmPaciente.Controls.Add(this.LbCServicio);
            this.FrmPaciente.Controls.Add(this.LbDhistoria);
            this.FrmPaciente.Controls.Add(this.LbDasegurado);
            this.FrmPaciente.Controls.Add(this.LbDfinanciadora);
            this.FrmPaciente.Controls.Add(this.LbServicio);
            this.FrmPaciente.Controls.Add(this.LbDedad);
            this.FrmPaciente.Controls.Add(this.LbDsexo);
            this.FrmPaciente.Controls.Add(this.LbDcama);
            this.FrmPaciente.Controls.Add(this.LbDpaciente);
            this.FrmPaciente.Controls.Add(this.LbHistoria);
            this.FrmPaciente.Controls.Add(this.LbAsegurado);
            this.FrmPaciente.Controls.Add(this.LbFinanciadora);
            this.FrmPaciente.Controls.Add(this.LbEdad);
            this.FrmPaciente.Controls.Add(this.LbSexo);
            this.FrmPaciente.Controls.Add(this.LbCama);
            this.FrmPaciente.Controls.Add(this.LbPaciente);
            this.FrmPaciente.HeaderText = "";
            this.FrmPaciente.Location = new System.Drawing.Point(5, 0);
            this.FrmPaciente.Name = "FrmPaciente";
            this.FrmPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmPaciente.Size = new System.Drawing.Size(461, 191);
            this.FrmPaciente.TabIndex = 0;
            // 
            // lblNumTele
            // 
            this.lblNumTele.Enabled = false;
            this.lblNumTele.AutoSize = false;
            this.lblNumTele.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblNumTele.Location = new System.Drawing.Point(374, 18);
            this.lblNumTele.Name = "lblNumTele";
            this.lblNumTele.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblNumTele.Size = new System.Drawing.Size(80, 19);
            this.lblNumTele.TabIndex = 21;
            this.lblNumTele.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTele
            // 
            this.lblTele.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTele.Location = new System.Drawing.Point(324, 18);
            this.lblTele.Name = "lblTele";
            this.lblTele.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTele.Size = new System.Drawing.Size(52, 18);
            this.lblTele.TabIndex = 20;
            this.lblTele.Text = "Tel�fono:";
            // 
            // LbNBloqueada
            // 
            this.LbNBloqueada.Enabled = false;
            this.LbNBloqueada.AutoSize = false;
            this.LbNBloqueada.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbNBloqueada.Location = new System.Drawing.Point(259, 18);
            this.LbNBloqueada.Name = "LbNBloqueada";
            this.LbNBloqueada.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbNBloqueada.Size = new System.Drawing.Size(52, 19);
            this.LbNBloqueada.TabIndex = 19;
            this.LbNBloqueada.Visible = false;
            // 
            // LbBloqueada
            // 
            this.LbBloqueada.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbBloqueada.Location = new System.Drawing.Point(145, 18);
            this.LbBloqueada.Name = "LbBloqueada";
            this.LbBloqueada.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbBloqueada.Size = new System.Drawing.Size(118, 18);
            this.LbBloqueada.TabIndex = 18;
            this.LbBloqueada.Text = "Con cama bloqueada :";
            this.LbBloqueada.Visible = false;
            // 
            // LbCServicio
            // 
            this.LbCServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbCServicio.Location = new System.Drawing.Point(11, 156);
            this.LbCServicio.Name = "LbCServicio";
            this.LbCServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbCServicio.Size = new System.Drawing.Size(47, 18);
            this.LbCServicio.TabIndex = 17;
            this.LbCServicio.Text = "Servicio:";
            // 
            // LbDhistoria
            // 
            this.LbDhistoria.Enabled = false;
            this.LbDhistoria.AutoSize = false;
            this.LbDhistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbDhistoria.Location = new System.Drawing.Point(356, 74);
            this.LbDhistoria.Name = "LbDhistoria";
            this.LbDhistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbDhistoria.Size = new System.Drawing.Size(96, 19);
            this.LbDhistoria.TabIndex = 15;
            // 
            // LbDasegurado
            // 
            this.LbDasegurado.Enabled = false;
            this.LbDasegurado.AutoSize = false;
            this.LbDasegurado.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbDasegurado.Location = new System.Drawing.Point(95, 129);
            this.LbDasegurado.Name = "LbDasegurado";
            this.LbDasegurado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbDasegurado.Size = new System.Drawing.Size(192, 19);
            this.LbDasegurado.TabIndex = 14;
            // 
            // LbDfinanciadora
            // 
            this.LbDfinanciadora.Enabled = false;
            this.LbDfinanciadora.AutoSize = false;
            this.LbDfinanciadora.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbDfinanciadora.Location = new System.Drawing.Point(81, 101);
            this.LbDfinanciadora.Name = "LbDfinanciadora";
            this.LbDfinanciadora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbDfinanciadora.Size = new System.Drawing.Size(373, 19);
            this.LbDfinanciadora.TabIndex = 13;
            // 
            // LbServicio
            // 
            this.LbServicio.Enabled = false;
            this.LbServicio.AutoSize = false;
            this.LbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbServicio.Location = new System.Drawing.Point(81, 156);
            this.LbServicio.Name = "LbServicio";
            this.LbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbServicio.Size = new System.Drawing.Size(373, 19);
            this.LbServicio.TabIndex = 12;
            // 
            // LbDedad
            // 
            this.LbDedad.Enabled = false;
            this.LbDedad.AutoSize = false;
            this.LbDedad.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbDedad.Location = new System.Drawing.Point(221, 74);
            this.LbDedad.Name = "LbDedad";
            this.LbDedad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbDedad.Size = new System.Drawing.Size(52, 19);
            this.LbDedad.TabIndex = 11;
            // 
            // LbDsexo
            // 
            this.LbDsexo.Enabled = false;
            this.LbDsexo.AutoSize = false;
            this.LbDsexo.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbDsexo.Location = new System.Drawing.Point(81, 74);
            this.LbDsexo.Name = "LbDsexo";
            this.LbDsexo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbDsexo.Size = new System.Drawing.Size(52, 19);
            this.LbDsexo.TabIndex = 10;
            // 
            // LbDcama
            // 
            this.LbDcama.Enabled = false;
            this.LbDcama.AutoSize = false;
            this.LbDcama.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbDcama.Location = new System.Drawing.Point(81, 18);
            this.LbDcama.Name = "LbDcama";
            this.LbDcama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbDcama.Size = new System.Drawing.Size(52, 19);
            this.LbDcama.TabIndex = 9;
            // 
            // LbDpaciente
            // 
            this.LbDpaciente.Enabled = false;
            this.LbDpaciente.AutoSize = false;
            this.LbDpaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbDpaciente.Location = new System.Drawing.Point(81, 46);
            this.LbDpaciente.Name = "LbDpaciente";
            this.LbDpaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbDpaciente.Size = new System.Drawing.Size(373, 19);
            this.LbDpaciente.TabIndex = 8;
            // 
            // LbHistoria
            // 
            this.LbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbHistoria.Location = new System.Drawing.Point(290, 74);
            this.LbHistoria.Name = "LbHistoria";
            this.LbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbHistoria.Size = new System.Drawing.Size(64, 18);
            this.LbHistoria.TabIndex = 7;
            this.LbHistoria.Text = "N� Historia:";
            // 
            // LbAsegurado
            // 
            this.LbAsegurado.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbAsegurado.Location = new System.Drawing.Point(11, 129);
            this.LbAsegurado.Name = "LbAsegurado";
            this.LbAsegurado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbAsegurado.Size = new System.Drawing.Size(78, 18);
            this.LbAsegurado.TabIndex = 6;
            this.LbAsegurado.Text = "N� asegurado:";
            // 
            // LbFinanciadora
            // 
            this.LbFinanciadora.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFinanciadora.Location = new System.Drawing.Point(11, 101);
            this.LbFinanciadora.Name = "LbFinanciadora";
            this.LbFinanciadora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFinanciadora.Size = new System.Drawing.Size(72, 18);
            this.LbFinanciadora.TabIndex = 5;
            this.LbFinanciadora.Text = "Financiadora:";
            // 
            // LbEdad
            // 
            this.LbEdad.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbEdad.Location = new System.Drawing.Point(164, 74);
            this.LbEdad.Name = "LbEdad";
            this.LbEdad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbEdad.Size = new System.Drawing.Size(33, 18);
            this.LbEdad.TabIndex = 4;
            this.LbEdad.Text = "Edad:";
            // 
            // LbSexo
            // 
            this.LbSexo.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbSexo.Location = new System.Drawing.Point(11, 74);
            this.LbSexo.Name = "LbSexo";
            this.LbSexo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbSexo.Size = new System.Drawing.Size(32, 18);
            this.LbSexo.TabIndex = 3;
            this.LbSexo.Text = "Sexo:";
            // 
            // LbCama
            // 
            this.LbCama.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbCama.Location = new System.Drawing.Point(11, 18);
            this.LbCama.Name = "LbCama";
            this.LbCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbCama.Size = new System.Drawing.Size(37, 18);
            this.LbCama.TabIndex = 2;
            this.LbCama.Text = "Cama:";
            // 
            // LbPaciente
            // 
            this.LbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbPaciente.Location = new System.Drawing.Point(11, 46);
            this.LbPaciente.Name = "LbPaciente";
            this.LbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbPaciente.Size = new System.Drawing.Size(51, 18);
            this.LbPaciente.TabIndex = 1;
            this.LbPaciente.Text = "Paciente:";
            // 
            // PACIENTE_CAMA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 229);
            this.Controls.Add(this.cmdSalir);
            this.Controls.Add(this.FrmPaciente);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 23);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PACIENTE_CAMA";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Paciente ingresado - AGI111F2";
            this.Closed += new System.EventHandler(this.PACIENTE_CAMA_Closed);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmPaciente)).EndInit();
            this.FrmPaciente.ResumeLayout(false);
            this.FrmPaciente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumTele)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTele)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbNBloqueada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbBloqueada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDhistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDasegurado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDfinanciadora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDedad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDsexo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDcama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbDpaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbAsegurado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFinanciadora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbEdad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbSexo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}