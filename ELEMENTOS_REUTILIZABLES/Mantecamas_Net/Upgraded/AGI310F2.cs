using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace mantecamas
{
	public partial class MOTIVO_INHABILITACION
        : Telerik.WinControls.UI.RadForm
    {

		DataSet Rrinhabilita = null;
		SqlConnection RcAdmision = null;
		VISUALIZACION instancia = null;
		public MOTIVO_INHABILITACION()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            this.TopMost = true;
        }



		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
            instancia.FNRECOGETAG(CbbInhabilita.SelectedValue.ToString());
          
            this.Close();
		}
		public void fnrecogedatos(VISUALIZACION formulario, SqlConnection conexion)
		{
			instancia = formulario;
			RcAdmision = conexion;
		}

       
        private bool isInitializingComponent;


        private void CbbInhabilita_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (CbbInhabilita.Text == "")
			{
				cbAceptar.Enabled = false;
			}
		}

		

		private void CbbInhabilita_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void cmdSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			instancia.FNRECOGETAG("!");
			this.Close();
		}


		private void MOTIVO_INHABILITACION_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				string sqlinha = String.Empty;

				try
				{

					cbAceptar.Enabled = false;
					this.Top = 269;
					this.Left = 87;
					sqlinha = "select gmotinha,dinhabil from dmotinhava";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlinha, RcAdmision);
					Rrinhabilita = new DataSet();
					tempAdapter.Fill(Rrinhabilita);
					
					foreach (DataRow iteration_row in Rrinhabilita.Tables[0].Rows)
					{
                        CbbInhabilita.Items.Add(new Telerik.WinControls.UI.RadListDataItem(Convert.ToString(iteration_row["dinhabil"]), Convert.ToInt32(iteration_row["gmotinha"])));


                    }
					instancia.FNRECOGETAG("!");

					return;
				}
				catch (SqlException ex)
				{
					//***
					Serrores.GestionErrorBaseDatos(ex, RcAdmision);
				}
                catch (Exception exception)
                {
                    System.Console.Write(exception.Message);
                }
            }
		}

		private void MOTIVO_INHABILITACION_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}

        private void CbbInhabilita_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (CbbInhabilita.SelectedIndex != -1)
            {
                cbAceptar.Enabled = true;
            }
        }
    }
}