using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace mantecamas
{
	partial class DATOS_TRASLADO
	{

		#region "Upgrade Support "
		private static DATOS_TRASLADO m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DATOS_TRASLADO DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DATOS_TRASLADO();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbAceptar", "cmdSalir", "rbNormal", "rbBloqueo", "Frame3", "cbbMotivtras", "dtHoramovi", "dtfechmovi", "Label9", "Label10", "FrmFecha", "LbMotivo", "Frtraslado"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cmdSalir;
		public Telerik.WinControls.UI.RadRadioButton rbNormal;
		public Telerik.WinControls.UI.RadRadioButton rbBloqueo;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
        public UpgradeHelpers.MSForms.MSCombobox cbbMotivtras;
        //public Telerik.WinControls.UI.RadDropDownList cbbMotivtras;
		public Telerik.WinControls.UI.RadMaskedEditBox dtHoramovi;
		public Telerik.WinControls.UI.RadDateTimePicker dtfechmovi;
		public Telerik.WinControls.UI.RadLabel Label9;
		public Telerik.WinControls.UI.RadLabel Label10;
		public Telerik.WinControls.UI.RadGroupBox FrmFecha;
		public Telerik.WinControls.UI.RadLabel LbMotivo;
		public Telerik.WinControls.UI.RadGroupBox Frtraslado;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cmdSalir = new Telerik.WinControls.UI.RadButton();
            this.Frtraslado = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.rbNormal = new Telerik.WinControls.UI.RadRadioButton();
            this.rbBloqueo = new Telerik.WinControls.UI.RadRadioButton();
            this.cbbMotivtras = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.FrmFecha = new Telerik.WinControls.UI.RadGroupBox();
            this.dtHoramovi = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.dtfechmovi = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label9 = new Telerik.WinControls.UI.RadLabel();
            this.Label10 = new Telerik.WinControls.UI.RadLabel();
            this.LbMotivo = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frtraslado)).BeginInit();
            this.Frtraslado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbNormal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbBloqueo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMotivtras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmFecha)).BeginInit();
            this.FrmFecha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtHoramovi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtfechmovi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbMotivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(134, 138);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 29);
            this.cbAceptar.TabIndex = 6;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cmdSalir
            // 
            this.cmdSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSalir.Location = new System.Drawing.Point(222, 138);
            this.cmdSalir.Name = "cmdSalir";
            this.cmdSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSalir.Size = new System.Drawing.Size(81, 29);
            this.cmdSalir.TabIndex = 7;
            this.cmdSalir.Text = "&Cancelar";
            this.cmdSalir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cmdSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            // 
            // Frtraslado
            // 
            this.Frtraslado.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frtraslado.Controls.Add(this.Frame3);
            this.Frtraslado.Controls.Add(this.cbbMotivtras);
            this.Frtraslado.Controls.Add(this.FrmFecha);
            this.Frtraslado.Controls.Add(this.LbMotivo);
            this.Frtraslado.HeaderText = "Datos del traslado o rotaci�n";
            this.Frtraslado.Location = new System.Drawing.Point(7, 4);
            this.Frtraslado.Name = "Frtraslado";
            this.Frtraslado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frtraslado.Size = new System.Drawing.Size(296, 128);
            this.Frtraslado.TabIndex = 0;
            this.Frtraslado.Text = "Datos del traslado o rotaci�n";
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.rbNormal);
            this.Frame3.Controls.Add(this.rbBloqueo);
            this.Frame3.HeaderText = "Tipo";
            this.Frame3.Location = new System.Drawing.Point(183, 13);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(81, 65);
            this.Frame3.TabIndex = 11;
            this.Frame3.Text = "Tipo";
            // 
            // rbNormal
            // 
            this.rbNormal.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbNormal.Location = new System.Drawing.Point(8, 16);
            this.rbNormal.Name = "rbNormal";
            this.rbNormal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbNormal.Size = new System.Drawing.Size(57, 18);
            this.rbNormal.TabIndex = 3;
            this.rbNormal.Text = "Normal";
            this.rbNormal.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbNormal_ToggleStateChanged);
            // 
            // rbBloqueo
            // 
            this.rbBloqueo.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbBloqueo.Location = new System.Drawing.Point(8, 40);
            this.rbBloqueo.Name = "rbBloqueo";
            this.rbBloqueo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbBloqueo.Size = new System.Drawing.Size(62, 18);
            this.rbBloqueo.TabIndex = 4;
            this.rbBloqueo.Text = "Bloqueo";
            this.rbBloqueo.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbBloqueo_ToggleStateChanged);
            // 
            // cbbMotivtras
            // 
            this.cbbMotivtras.ColumnWidths = "";
            this.cbbMotivtras.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbMotivtras.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbMotivtras.Location = new System.Drawing.Point(97, 103);
            this.cbbMotivtras.Name = "cbbMotivtras";
            this.cbbMotivtras.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbMotivtras.Size = new System.Drawing.Size(193, 20);
            this.cbbMotivtras.TabIndex = 5;
            this.cbbMotivtras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbMotivtras_KeyPress);
            this.cbbMotivtras.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbMotivtras_SelectedIndexChanged);
            this.cbbMotivtras.TextChanged += new System.EventHandler(this.cbbMotivtras_TextChanged);
            // 
            // FrmFecha
            // 
            this.FrmFecha.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmFecha.Controls.Add(this.dtHoramovi);
            this.FrmFecha.Controls.Add(this.dtfechmovi);
            this.FrmFecha.Controls.Add(this.Label9);
            this.FrmFecha.Controls.Add(this.Label10);
            this.FrmFecha.HeaderText = "";
            this.FrmFecha.Location = new System.Drawing.Point(16, 13);
            this.FrmFecha.Name = "FrmFecha";
            this.FrmFecha.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmFecha.Size = new System.Drawing.Size(161, 65);
            this.FrmFecha.TabIndex = 8;
            // 
            // dtHoramovi
            // 
            this.dtHoramovi.Location = new System.Drawing.Point(48, 38);
            this.dtHoramovi.Mask = "99:99";
            this.dtHoramovi.Name = "dtHoramovi";
            this.dtHoramovi.PromptChar = ' ';
            this.dtHoramovi.Size = new System.Drawing.Size(37, 20);
            this.dtHoramovi.TabIndex = 2;
            this.dtHoramovi.TabStop = false;
            this.dtHoramovi.TextChanged += new System.EventHandler(this.dtHoramovi_TextChanged);
            this.dtHoramovi.Enter += new System.EventHandler(this.dtHoramovi_Enter);
            this.dtHoramovi.Leave += new System.EventHandler(this.dtHoramovi_Leave);
            // 
            // dtfechmovi
            // 
            this.dtfechmovi.CustomFormat = "dd/MM/yyyy";
            this.dtfechmovi.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtfechmovi.Location = new System.Drawing.Point(48, 16);
            this.dtfechmovi.Name = "dtfechmovi";
            this.dtfechmovi.Size = new System.Drawing.Size(105, 20);
            this.dtfechmovi.TabIndex = 1;
            this.dtfechmovi.TabStop = false;
            this.dtfechmovi.Text = "07/04/2016";
            this.dtfechmovi.Value = new System.DateTime(2016, 4, 7, 0, 0, 0, 0);
            this.dtfechmovi.Leave += new System.EventHandler(this.dtfechmovi_Leave);
            // 
            // Label9
            // 
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Location = new System.Drawing.Point(8, 40);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(33, 18);
            this.Label9.TabIndex = 10;
            this.Label9.Text = "Hora:";
            // 
            // Label10
            // 
            this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label10.Location = new System.Drawing.Point(8, 16);
            this.Label10.Name = "Label10";
            this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label10.Size = new System.Drawing.Size(37, 18);
            this.Label10.TabIndex = 9;
            this.Label10.Text = "Fecha:";
            // 
            // LbMotivo
            // 
            this.LbMotivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbMotivo.Location = new System.Drawing.Point(5, 104);
            this.LbMotivo.Name = "LbMotivo";
            this.LbMotivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbMotivo.Size = new System.Drawing.Size(87, 18);
            this.LbMotivo.TabIndex = 12;
            this.LbMotivo.Text = "Motivo traslado:";
            // 
            // DATOS_TRASLADO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 168);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cmdSalir);
            this.Controls.Add(this.Frtraslado);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DATOS_TRASLADO";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.Text = "Traslado de pacientes AGI310F4";
            this.Activated += new System.EventHandler(this.DATOS_TRASLADO_Activated);
            this.Closed += new System.EventHandler(this.DATOS_TRASLADO_Closed);
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frtraslado)).EndInit();
            this.Frtraslado.ResumeLayout(false);
            this.Frtraslado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbNormal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbBloqueo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMotivtras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmFecha)).EndInit();
            this.FrmFecha.ResumeLayout(false);
            this.FrmFecha.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtHoramovi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtfechmovi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbMotivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}