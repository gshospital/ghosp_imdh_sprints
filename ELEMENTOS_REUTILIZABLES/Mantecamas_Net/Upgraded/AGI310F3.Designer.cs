using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace mantecamas
{
	partial class CAMA_ESTADO
	{

		#region "Upgrade Support "
		private static CAMA_ESTADO m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static CAMA_ESTADO DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new CAMA_ESTADO();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CbbServicio", "txtTelefono", "chbVirtual", "CbbTipocama", "Chbrestriccion", "ChbEstancia", "CbbEstado", "Cbbinhabilitacion", "CbbEnfermeria", "tbHabitacion", "tbPlanta", "tbCama", "lblTelefono", "LbServicio", "LbTipocama", "LbEstado", "LbMotivo", "LbEnfermeria", "LbPlanta", "LbHabitacion", "LbCama", "FrCama", "cbAceptar", "cmdSalir", "Label2", "Label1", "SprPlantas_Sheet1", "SPRCAMAS_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadDropDownList CbbServicio;
		public Telerik.WinControls.UI.RadTextBoxControl txtTelefono;
		public Telerik.WinControls.UI.RadCheckBox chbVirtual;
		public Telerik.WinControls.UI.RadDropDownList CbbTipocama;
		public Telerik.WinControls.UI.RadCheckBox Chbrestriccion;
		public Telerik.WinControls.UI.RadCheckBox ChbEstancia;
		public Telerik.WinControls.UI.RadDropDownList CbbEstado;
		public Telerik.WinControls.UI.RadDropDownList Cbbinhabilitacion;
		public Telerik.WinControls.UI.RadDropDownList CbbEnfermeria;
		public Telerik.WinControls.UI.RadTextBoxControl tbHabitacion;
        public Telerik.WinControls.UI.RadTextBoxControl tbPlanta;
        public Telerik.WinControls.UI.RadTextBoxControl tbCama;
		public Telerik.WinControls.UI.RadLabel lblTelefono;
		public Telerik.WinControls.UI.RadLabel LbServicio;
		public Telerik.WinControls.UI.RadLabel LbTipocama;
		public Telerik.WinControls.UI.RadLabel LbEstado;
		public Telerik.WinControls.UI.RadLabel LbMotivo;
		public Telerik.WinControls.UI.RadLabel LbEnfermeria;
		public Telerik.WinControls.UI.RadLabel LbPlanta;
		public Telerik.WinControls.UI.RadLabel LbHabitacion;
		public Telerik.WinControls.UI.RadLabel LbCama;
        public Telerik.WinControls.UI.RadGroupBox FrCama;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cmdSalir;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label1;
		//private FarPoint.Win.Spread.SheetView SprPlantas_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView SPRCAMAS_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.CbbServicio = new Telerik.WinControls.UI.RadDropDownList();
            this.FrCama = new Telerik.WinControls.UI.RadGroupBox();
            this.txtTelefono = new Telerik.WinControls.UI.RadTextBoxControl();
            this.chbVirtual = new Telerik.WinControls.UI.RadCheckBox();
            this.CbbTipocama = new Telerik.WinControls.UI.RadDropDownList();
            this.Chbrestriccion = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbEstancia = new Telerik.WinControls.UI.RadCheckBox();
            this.CbbEstado = new Telerik.WinControls.UI.RadDropDownList();
            this.Cbbinhabilitacion = new Telerik.WinControls.UI.RadDropDownList();
            this.CbbEnfermeria = new Telerik.WinControls.UI.RadDropDownList();
            this.tbHabitacion = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbPlanta = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbCama = new Telerik.WinControls.UI.RadTextBoxControl();
            this.lblTelefono = new Telerik.WinControls.UI.RadLabel();
            this.LbServicio = new Telerik.WinControls.UI.RadLabel();
            this.LbTipocama = new Telerik.WinControls.UI.RadLabel();
            this.LbEstado = new Telerik.WinControls.UI.RadLabel();
            this.LbMotivo = new Telerik.WinControls.UI.RadLabel();
            this.LbEnfermeria = new Telerik.WinControls.UI.RadLabel();
            this.LbPlanta = new Telerik.WinControls.UI.RadLabel();
            this.LbHabitacion = new Telerik.WinControls.UI.RadLabel();
            this.LbCama = new Telerik.WinControls.UI.RadLabel();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cmdSalir = new Telerik.WinControls.UI.RadButton();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.CbbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrCama)).BeginInit();
            this.FrCama.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbVirtual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbTipocama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chbrestriccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbEstancia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cbbinhabilitacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbEnfermeria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHabitacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlanta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTipocama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbMotivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbEnfermeria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPlanta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbHabitacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CbbServicio
            // 
            this.CbbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbbServicio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CbbServicio.Location = new System.Drawing.Point(152, 88);
            this.CbbServicio.Name = "CbbServicio";
            this.CbbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbbServicio.Size = new System.Drawing.Size(219, 20);
            this.CbbServicio.TabIndex = 6;
            // 
            // FrCama
            // 
            this.FrCama.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrCama.Controls.Add(this.txtTelefono);
            this.FrCama.Controls.Add(this.chbVirtual);
            this.FrCama.Controls.Add(this.CbbTipocama);
            this.FrCama.Controls.Add(this.Chbrestriccion);
            this.FrCama.Controls.Add(this.ChbEstancia);
            this.FrCama.Controls.Add(this.CbbEstado);
            this.FrCama.Controls.Add(this.Cbbinhabilitacion);
            this.FrCama.Controls.Add(this.CbbEnfermeria);
            this.FrCama.Controls.Add(this.tbHabitacion);
            this.FrCama.Controls.Add(this.tbPlanta);
            this.FrCama.Controls.Add(this.tbCama);
            this.FrCama.Controls.Add(this.lblTelefono);
            this.FrCama.Controls.Add(this.LbServicio);
            this.FrCama.Controls.Add(this.LbTipocama);
            this.FrCama.Controls.Add(this.LbEstado);
            this.FrCama.Controls.Add(this.LbMotivo);
            this.FrCama.Controls.Add(this.LbEnfermeria);
            this.FrCama.Controls.Add(this.LbPlanta);
            this.FrCama.Controls.Add(this.LbHabitacion);
            this.FrCama.Controls.Add(this.LbCama);
            this.FrCama.HeaderText = "Estado de la cama";
            this.FrCama.Location = new System.Drawing.Point(2, 0);
            this.FrCama.Name = "FrCama";
            this.FrCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrCama.Size = new System.Drawing.Size(376, 249);
            this.FrCama.TabIndex = 14;
            this.FrCama.Text = "Estado de la cama";
            // 
            // txtTelefono
            // 
            this.txtTelefono.AcceptsReturn = true;
            this.txtTelefono.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtTelefono.Location = new System.Drawing.Point(132, 217);
            this.txtTelefono.MaxLength = 9;
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTelefono.Size = new System.Drawing.Size(71, 20);
            this.txtTelefono.TabIndex = 12;
            this.txtTelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono_KeyPress);
            // 
            // chbVirtual
            // 
            this.chbVirtual.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbVirtual.Location = new System.Drawing.Point(6, 140);
            this.chbVirtual.Name = "chbVirtual";
            this.chbVirtual.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbVirtual.Size = new System.Drawing.Size(53, 18);
            this.chbVirtual.TabIndex = 9;
            this.chbVirtual.Text = "Virtual";
            // 
            // CbbTipocama
            // 
            this.CbbTipocama.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbbTipocama.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CbbTipocama.Location = new System.Drawing.Point(150, 64);
            this.CbbTipocama.Name = "CbbTipocama";
            this.CbbTipocama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbbTipocama.Size = new System.Drawing.Size(167, 20);
            this.CbbTipocama.TabIndex = 5;
            this.CbbTipocama.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbTipocama_KeyPress);
            this.CbbTipocama.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbTipocama_SelectedIndexChanged);
            this.CbbTipocama.TextChanged += new System.EventHandler(this.CbbTipocama_TextChanged);
            // 
            // Chbrestriccion
            // 
            this.Chbrestriccion.Cursor = System.Windows.Forms.Cursors.Default;
            this.Chbrestriccion.Location = new System.Drawing.Point(6, 113);
            this.Chbrestriccion.Name = "Chbrestriccion";
            this.Chbrestriccion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Chbrestriccion.Size = new System.Drawing.Size(116, 18);
            this.Chbrestriccion.TabIndex = 7;
            this.Chbrestriccion.Text = "Restricci�n de sexo";
            // 
            // ChbEstancia
            // 
            this.ChbEstancia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbEstancia.Location = new System.Drawing.Point(248, 138);
            this.ChbEstancia.Name = "ChbEstancia";
            this.ChbEstancia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbEstancia.Size = new System.Drawing.Size(99, 18);
            this.ChbEstancia.TabIndex = 8;
            this.ChbEstancia.Text = "Genera estancia";
            this.ChbEstancia.Visible = false;
            // 
            // CbbEstado
            // 
            this.CbbEstado.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbbEstado.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CbbEstado.Location = new System.Drawing.Point(131, 165);
            this.CbbEstado.Name = "CbbEstado";
            this.CbbEstado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbbEstado.Size = new System.Drawing.Size(165, 20);
            this.CbbEstado.TabIndex = 10;
            this.CbbEstado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbEstado_KeyPress);
            this.CbbEstado.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbEstado_SelectedIndexChanged);
            this.CbbEstado.TextChanged += new System.EventHandler(this.CbbEstado_TextChanged);
            // 
            // Cbbinhabilitacion
            // 
            this.Cbbinhabilitacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.Cbbinhabilitacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Cbbinhabilitacion.Location = new System.Drawing.Point(131, 189);
            this.Cbbinhabilitacion.Name = "Cbbinhabilitacion";
            this.Cbbinhabilitacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Cbbinhabilitacion.Size = new System.Drawing.Size(237, 20);
            this.Cbbinhabilitacion.TabIndex = 11;
            this.Cbbinhabilitacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbbinhabilitacion_KeyPress);
            this.Cbbinhabilitacion.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.Cbbinhabilitacion_SelectedIndexChanged);
            this.Cbbinhabilitacion.TextChanged += new System.EventHandler(this.Cbbinhabilitacion_TextChanged);
            // 
            // CbbEnfermeria
            // 
            this.CbbEnfermeria.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbbEnfermeria.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CbbEnfermeria.Location = new System.Drawing.Point(150, 39);
            this.CbbEnfermeria.Name = "CbbEnfermeria";
            this.CbbEnfermeria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbbEnfermeria.Size = new System.Drawing.Size(221, 20);
            this.CbbEnfermeria.TabIndex = 4;
            this.CbbEnfermeria.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbEnfermeria_KeyPress);
            this.CbbEnfermeria.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbEnfermeria_SelectedIndexChanged);
            this.CbbEnfermeria.TextChanged += new System.EventHandler(this.CbbEnfermeria_TextChanged);
            // 
            // tbHabitacion
            // 
            this.tbHabitacion.AcceptsReturn = true;
            this.tbHabitacion.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbHabitacion.Location = new System.Drawing.Point(261, 16);
            this.tbHabitacion.MaxLength = 2;
            this.tbHabitacion.Name = "tbHabitacion";
            this.tbHabitacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbHabitacion.Size = new System.Drawing.Size(31, 20);
            this.tbHabitacion.TabIndex = 2;
            this.tbHabitacion.TextChanged += new System.EventHandler(this.tbHabitacion_TextChanged);
            this.tbHabitacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbHabitacion_KeyPress);
            // 
            // tbPlanta
            // 
            this.tbPlanta.AcceptsReturn = true;
            this.tbPlanta.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPlanta.Location = new System.Drawing.Point(61, 16);
            this.tbPlanta.MaxLength = 0;
            this.tbPlanta.Name = "tbPlanta";
            this.tbPlanta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPlanta.Size = new System.Drawing.Size(129, 20);
            this.tbPlanta.TabIndex = 1;
            // 
            // tbCama
            // 
            this.tbCama.AcceptsReturn = true;
            this.tbCama.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCama.Location = new System.Drawing.Point(338, 16);
            this.tbCama.MaxLength = 2;
            this.tbCama.Name = "tbCama";
            this.tbCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCama.Size = new System.Drawing.Size(29, 20);
            this.tbCama.TabIndex = 3;
            this.tbCama.Enter += new System.EventHandler(this.tbCama_Enter);
            this.tbCama.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCama_KeyPress);
            this.tbCama.Leave += new System.EventHandler(this.tbCama_Leave);
            // 
            // lblTelefono
            // 
            this.lblTelefono.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTelefono.Location = new System.Drawing.Point(6, 219);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTelefono.Size = new System.Drawing.Size(109, 18);
            this.lblTelefono.TabIndex = 25;
            this.lblTelefono.Text = "Tel�fono Habitaci�n:";
            // 
            // LbServicio
            // 
            this.LbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbServicio.Location = new System.Drawing.Point(6, 88);
            this.LbServicio.Name = "LbServicio";
            this.LbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbServicio.Size = new System.Drawing.Size(47, 18);
            this.LbServicio.TabIndex = 24;
            this.LbServicio.Text = "Servicio:";
            // 
            // LbTipocama
            // 
            this.LbTipocama.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbTipocama.Location = new System.Drawing.Point(6, 66);
            this.LbTipocama.Name = "LbTipocama";
            this.LbTipocama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbTipocama.Size = new System.Drawing.Size(76, 18);
            this.LbTipocama.TabIndex = 21;
            this.LbTipocama.Text = "Tipo de cama:";
            // 
            // LbEstado
            // 
            this.LbEstado.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbEstado.Location = new System.Drawing.Point(6, 163);
            this.LbEstado.Name = "LbEstado";
            this.LbEstado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbEstado.Size = new System.Drawing.Size(99, 18);
            this.LbEstado.TabIndex = 20;
            this.LbEstado.Text = "Estado de la cama:";
            // 
            // LbMotivo
            // 
            this.LbMotivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbMotivo.Location = new System.Drawing.Point(6, 187);
            this.LbMotivo.Name = "LbMotivo";
            this.LbMotivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbMotivo.Size = new System.Drawing.Size(114, 18);
            this.LbMotivo.TabIndex = 19;
            this.LbMotivo.Text = "Motivo inhabilitaci�n:";
            // 
            // LbEnfermeria
            // 
            this.LbEnfermeria.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbEnfermeria.Location = new System.Drawing.Point(6, 42);
            this.LbEnfermeria.Name = "LbEnfermeria";
            this.LbEnfermeria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbEnfermeria.Size = new System.Drawing.Size(138, 18);
            this.LbEnfermeria.TabIndex = 18;
            this.LbEnfermeria.Text = "Unidad de hospitalizaci�n:";
            // 
            // LbPlanta
            // 
            this.LbPlanta.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbPlanta.Location = new System.Drawing.Point(6, 18);
            this.LbPlanta.Name = "LbPlanta";
            this.LbPlanta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbPlanta.Size = new System.Drawing.Size(39, 18);
            this.LbPlanta.TabIndex = 17;
            this.LbPlanta.Text = "Planta:";
            // 
            // LbHabitacion
            // 
            this.LbHabitacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbHabitacion.Location = new System.Drawing.Point(198, 18);
            this.LbHabitacion.Name = "LbHabitacion";
            this.LbHabitacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbHabitacion.Size = new System.Drawing.Size(62, 18);
            this.LbHabitacion.TabIndex = 16;
            this.LbHabitacion.Text = "Habitaci�n:";
            // 
            // LbCama
            // 
            this.LbCama.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbCama.Location = new System.Drawing.Point(300, 18);
            this.LbCama.Name = "LbCama";
            this.LbCama.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbCama.Size = new System.Drawing.Size(37, 18);
            this.LbCama.TabIndex = 15;
            this.LbCama.Text = "Cama:";
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(208, 256);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 29);
            this.cbAceptar.TabIndex = 0;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cmdSalir
            // 
            this.cmdSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSalir.Location = new System.Drawing.Point(296, 256);
            this.cmdSalir.Name = "cmdSalir";
            this.cmdSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSalir.Size = new System.Drawing.Size(81, 29);
            this.cmdSalir.TabIndex = 13;
            this.cmdSalir.Text = "&Cancelar";
            this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(0, 0);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(76, 18);
            this.Label2.TabIndex = 23;
            this.Label2.Text = "Tipo de cama:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(0, 83);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(138, 18);
            this.Label1.TabIndex = 22;
            this.Label1.Text = "Unidad de hospitalizaci�n:";
            // 
            // CAMA_ESTADO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 288);
            this.Controls.Add(this.CbbServicio);
            this.Controls.Add(this.FrCama);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cmdSalir);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CAMA_ESTADO";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.Text = "Estado de la cama AGI310F3";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.CAMA_ESTADO_Activated);
            this.Closed += new System.EventHandler(this.CAMA_ESTADO_Closed);
            ((System.ComponentModel.ISupportInitialize)(this.CbbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrCama)).EndInit();
            this.FrCama.ResumeLayout(false);
            this.FrCama.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbVirtual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbTipocama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chbrestriccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbEstancia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cbbinhabilitacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbEnfermeria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHabitacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPlanta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbTipocama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbMotivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbEnfermeria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbPlanta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbHabitacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbCama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}