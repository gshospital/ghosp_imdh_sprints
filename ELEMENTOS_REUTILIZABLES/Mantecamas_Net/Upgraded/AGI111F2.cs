using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace mantecamas
{
	public partial class PACIENTE_CAMA
        : Telerik.WinControls.UI.RadForm
	{

		public PACIENTE_CAMA()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            this.TopMost = true;
		}


		private void PACIENTE_CAMA_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		private void cmdSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		//********************************************************************
		//*                                                                  *
		//*  Modificaciones:                                                 *
		//*                                                                  *
		//*      O.Frias 21/02/2007  Incorporo el nuevo dato (tel�fono)      *
		//*                                                                  *
		//********************************************************************
		public void proLLenar_pac(string sGiden, string camas, SqlConnection RcPac, string Ptipo, string strTelefono)
		{
			int vstSociedad = 0;
			string stEncontrado = String.Empty;

			//************************ O.Frias 21/02/2007 ************************
			lblNumTele.Text = strTelefono;
			//************************ O.Frias 21/02/2007 ************************

			SqlConnection tRcPac = RcPac;
			string SqPacCam = "select * from dpacient where gidenpac='" + sGiden + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SqPacCam, tRcPac);
			DataSet RrpacCam = new DataSet();
			tempAdapter.Fill(RrpacCam);

			if (Convert.ToString(RrpacCam.Tables[0].Rows[0]["DAPE1PAC"]) != "")
			{
				LbDpaciente.Text = Convert.ToString(RrpacCam.Tables[0].Rows[0]["DAPE1PAC"]).Trim();
			}
			
			if (Convert.ToString(RrpacCam.Tables[0].Rows[0]["DAPE2PAC"]) != "")
			{
				LbDpaciente.Text = LbDpaciente.Text + " " + Convert.ToString(RrpacCam.Tables[0].Rows[0]["DAPE2PAC"]).Trim();
			}
			
			if (Convert.ToString(RrpacCam.Tables[0].Rows[0]["DNOMBPAC"]) != "")
			{
				LbDpaciente.Text = LbDpaciente.Text + " " + Convert.ToString(RrpacCam.Tables[0].Rows[0]["DNOMBPAC"]).Trim();
			}
			
			if (Convert.ToString(RrpacCam.Tables[0].Rows[0]["NAFILIAC"]) != "")
			{
				LbDasegurado.Text = Convert.ToString(RrpacCam.Tables[0].Rows[0]["NAFILIAC"]);
			}
			LbDcama.Text = camas;
			//**********sexo
			if (Convert.ToString(RrpacCam.Tables[0].Rows[0]["itipsexo"]) == "H")
			{
				LbDsexo.Text = "Hombre";
			}
			else if (Convert.ToString(RrpacCam.Tables[0].Rows[0]["itipsexo"]) == "M")
			{ 
				LbDsexo.Text = "Mujer";
			}
			else
			{
				LbDsexo.Text = "";
			}

			//*mirar si tiene camas bloqueadas
			string sqlBloqueadas = " select * from dcamasbova " + " WHERE GIDENPAC = '" + sGiden + "'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlBloqueadas, tRcPac);
			DataSet rrBloqueadas = new DataSet();
			tempAdapter_2.Fill(rrBloqueadas);
			if (rrBloqueadas.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in rrBloqueadas.Tables[0].Rows)
				{
					if (Ptipo == "OCUPADA")
					{
						if (Convert.ToString(iteration_row["iestcama"]) == "B")
						{
							LbBloqueada.Visible = true;
							LbNBloqueada.Visible = true;
							LbNBloqueada.Text = StringsHelper.Format(iteration_row["gplantas"], "00") + StringsHelper.Format(iteration_row["ghabitac"], "00") + StringsHelper.Format(iteration_row["gcamasbo"], "00");
						}
					}
					else
					{
						if (Convert.ToString(iteration_row["iestcama"]) == "O")
						{
							LbBloqueada.Text = "Cama que ocupa:";
							LbBloqueada.Visible = true;
							LbNBloqueada.Visible = true;
							LbNBloqueada.Text = StringsHelper.Format(iteration_row["gplantas"], "00") + StringsHelper.Format(iteration_row["ghabitac"], "00") + StringsHelper.Format(iteration_row["gcamasbo"], "00");
						}
					}
				}
			}
			rrBloqueadas.Close();

			//******edad
			//LbDedad = Mid(Year(Date - RrpacCam("fnacipac")), 3, 2)
			if (!Convert.IsDBNull(RrpacCam.Tables[0].Rows[0]["fnacipac"]))
			{
				//LbDedad.Caption = Mid(Year(Date - RrpacCam("fnacipac")), 3, 2)
				LbDedad.Text = Serrores.CalcularEdad(Convert.ToDateTime(RrpacCam.Tables[0].Rows[0]["fnacipac"]), DateTime.Today, tRcPac);
			}
			//******historia


			string StSqHistoria = "SELECT GHISTORIA FROM HSERARCH, HDOSSIER " + " WHERE GIDENPAC = '" + sGiden + "' AND " + " GSERPROPIET = GSERARCH AND " + " ICENTRAL='S'";

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSqHistoria, tRcPac);
			DataSet RrHistoria = new DataSet();
			tempAdapter_3.Fill(RrHistoria);

			if (RrHistoria.Tables[0].Rows.Count == 1)
			{
				LbDhistoria.Text = Conversion.Str(RrHistoria.Tables[0].Rows[0]["GHISTORIA"]);
			}
			else
			{
				LbDhistoria.Text = "";
			}
			RrHistoria.Close();
			//********Busco la sociedad
			string SQSoci = String.Empty;

			//SQSoci = "SELECT * FROM  SCONSGLO WHERE NNUMERI1 = " & RrpacCam("GSOCIEDA")

			//Set RrSoci = tRcPac.OpenResultset(SQSoci, 1, 1)

			//If RrSoci.RowCount <> 0 Then    'SS o Privado
			//    LbDfinanciadora.Caption = Trim(RrSoci("VALFANU1"))
			//Else
			//Sociedad
			// buscarla en amovifin
			if (Module1.vstHosUr == "U")
			{
				SQSoci = "SELECT GSOCIEDA FROM UEPISURG WHERE " + " GIDENPAC= '" + sGiden + "' AND FALTAURG IS NULL";
			}
			else
			{
				SQSoci = "SELECT GSOCIEDA FROM AMOVIFIN,AEPISADM WHERE " + " GNUMREGI=GNUMADME AND GANOREGI=GANOADME AND " + " GIDENPAC= '" + sGiden + "' AND FALTPLAN IS NULL" + " order by amovifin.fmovimie";
			}
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQSoci, tRcPac);
			DataSet RrSoci = new DataSet();
			tempAdapter_4.Fill(RrSoci);
			if (RrSoci.Tables[0].Rows.Count > 0)
			{
				if (!Convert.IsDBNull(RrSoci.Tables[0].Rows[0]["gsocieda"]))
				{
					vstSociedad = Convert.ToInt32(RrSoci.Tables[0].Rows[0]["GSOCIEDA"]);

					SQSoci = "SELECT * FROM  SCONSGLO WHERE gconsglo ='PRIVADO' or GCONSGLO='SEGURSOC'";
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQSoci, tRcPac);
					RrSoci = new DataSet();
					tempAdapter_5.Fill(RrSoci);
					stEncontrado = " ";
					if (RrSoci.Tables[0].Rows.Count > 0)
					{
						foreach (DataRow iteration_row_2 in RrSoci.Tables[0].Rows)
						{
							if (vstSociedad == Convert.ToDouble(iteration_row_2["NNUMERI1"]))
							{
								if (Convert.ToString(iteration_row_2["gconsglo"]).Trim() == "PRIVADO")
								{
									stEncontrado = "PRIVADO";
								}
								else
								{
									stEncontrado = "SEGURIDAD SOCIAL";
								}
							}
						}
						if (stEncontrado == " ")
						{
							SQSoci = "SELECT DSOCIEDA,IREGALOJ FROM DSOCIEDA WHERE GSOCIEDA = " + vstSociedad.ToString();
							SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQSoci, tRcPac);
							RrSoci = new DataSet();
							tempAdapter_6.Fill(RrSoci);
							if (RrSoci.Tables[0].Rows.Count > 0)
							{
								LbDfinanciadora.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["dsocieda"]);
							}
						}
						else
						{
							LbDfinanciadora.Text = stEncontrado;
						}
					}
					else
					{
						LbDfinanciadora.Text = "";
					}
				}
			}
			RrSoci.Close();
			//End If
			if (Module1.vstHosUr == "U")
			{
				SQSoci = "SELECT dnomserv FROM UEPISURG,dservici WHERE " + " GIDENPAC= '" + sGiden + "' AND FALTAURG IS NULL and" + " dservici.gservici=UEPISURG.gserVICI";
			}
			else
			{
				SQSoci = "SELECT dnomserv FROM aEPISADM,dservici WHERE " + " GIDENPAC= '" + sGiden + "' AND FALTPLAN IS NULL and" + " dservici.gservici=gserulti";
			}
			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(SQSoci, tRcPac);
			RrSoci = new DataSet();
			tempAdapter_7.Fill(RrSoci);
			if (RrSoci.Tables[0].Rows.Count > 0)
			{
				if (!Convert.IsDBNull(RrSoci.Tables[0].Rows[0]["dnomserv"]))
				{
					LbServicio.Text = Convert.ToString(RrSoci.Tables[0].Rows[0]["dnomserv"]);
				}
			}

			RrSoci.Close();
			RrpacCam.Close();

		}

		//private void LbDdiagnostico_Click()
		//{
				//
		//}
		private void PACIENTE_CAMA_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
		}
	}
}