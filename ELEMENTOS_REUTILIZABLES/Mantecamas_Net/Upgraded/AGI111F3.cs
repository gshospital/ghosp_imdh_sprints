using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace mantecamas
{
	public partial class ESTADO_PLANTAS
		: Telerik.WinControls.UI.RadForm
	{


		SqlConnection Conexion = null;
		public ESTADO_PLANTAS()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            this.TopMost = true;
        }



		private void cmdSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void ESTADO_PLANTAS_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;


				int TotalPlanta = 0;
				bool bPlanta = false; // true si los totales corresponden a una planta
				//false si los totales son de todas

				string cadena = Convert.ToString(this.Tag);
				int corte = (cadena.IndexOf('/') + 1);
				LbnumPlanta.Text = cadena.Substring(0, Math.Min(corte - 1, cadena.Length));
				double dbNumericTemp = 0;
				if (Double.TryParse(LbnumPlanta.Text, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
				{ // si es una planta en concreto
					TotalPlanta = ObtenerTotalPlanta();
					bPlanta = true;
				}
				else
				{
					// cuando sea todas las plantas poner invisible el porcentaje parcial por plantas
					this.lbCabeceraParcial.Visible = false;
					this.PorcentajeParcialBloquedas.Visible = false;
					this.PorcentajeParcialInhabilitadas.Visible = false;
					this.PorcentajeParcialLibres.Visible = false;
					this.PorcentajeParcialOcupadas.Visible = false;
					this.TotalParcial.Visible = false;
					bPlanta = false;

				}
				int TotalHospital = ObtenerTotalHospital();
				cadena = cadena.Substring(corte, Math.Min(cadena.Length, cadena.Length - corte));
				corte = (cadena.IndexOf('/') + 1);
				LbNumlibres.Text = cadena.Substring(0, Math.Min(corte - 1, cadena.Length));
				cadena = cadena.Substring(corte, Math.Min(cadena.Length, cadena.Length - corte));
				corte = (cadena.IndexOf('/') + 1);
				LbNumocupadas.Text = cadena.Substring(0, Math.Min(corte - 1, cadena.Length));
				cadena = cadena.Substring(corte, Math.Min(cadena.Length, cadena.Length - corte));
				corte = (cadena.IndexOf('/') + 1);
				LbNuminhabilitadas.Text = cadena.Substring(0, Math.Min(corte - 1, cadena.Length));
				cadena = cadena.Substring(corte, Math.Min(cadena.Length, cadena.Length - corte));
				corte = (cadena.IndexOf('/') + 1);
				LbNumbloqueadas.Text = cadena.Substring(0, Math.Min(corte - 1, cadena.Length));
				cadena = cadena.Substring(corte, Math.Min(cadena.Length, cadena.Length - corte));
				corte = (cadena.IndexOf('/') + 1);
				LbNumtotal.Text = (Conversion.Val(LbNumlibres.Text) + Conversion.Val(LbNumocupadas.Text) + Conversion.Val(LbNuminhabilitadas.Text) + Conversion.Val(LbNumbloqueadas.Text)).ToString();
				//CADENA = Mid(CADENA, corte + 1, Len(CADENA))
				//corte = InStr(1, CADENA, "/")
				LbTiposerv.Text = cadena.Substring(0, Math.Min(corte - 1, cadena.Length));
				if (bPlanta)
				{
					if (TotalPlanta != 0)
					{
						this.PorcentajeParcialBloquedas.Text = StringsHelper.Format(Double.Parse(LbNumbloqueadas.Text) / TotalPlanta * 100, "00.00");
						this.PorcentajeParcialInhabilitadas.Text = StringsHelper.Format(Double.Parse(LbNuminhabilitadas.Text) / TotalPlanta * 100, "00.00");
						this.PorcentajeParcialLibres.Text = StringsHelper.Format(Double.Parse(LbNumlibres.Text) / TotalPlanta * 100, "00.00");
						this.PorcentajeParcialOcupadas.Text = StringsHelper.Format(Double.Parse(LbNumocupadas.Text) / TotalPlanta * 100, "00.00");
						this.TotalParcial.Text = StringsHelper.Format(Double.Parse(LbNumtotal.Text) / TotalPlanta * 100, "00.00");
					}
					else
					{
						// O
						this.PorcentajeParcialBloquedas.Text = "00,00";
						this.PorcentajeParcialInhabilitadas.Text = "00,00";
						this.PorcentajeParcialLibres.Text = "00,00";
						this.PorcentajeParcialOcupadas.Text = "00,00";
						this.TotalParcial.Text = "00,00";
					}
				}
				if (TotalHospital != 0)
				{
					this.PorcentajeTotalBloquedas.Text = StringsHelper.Format(Double.Parse(LbNumbloqueadas.Text) / TotalHospital * 100, "00.00");
					this.PorcentajeTotalInhabilitadas.Text = StringsHelper.Format(Double.Parse(LbNuminhabilitadas.Text) / TotalHospital * 100, "00.00");
					this.PorcentajeTotalLibres.Text = StringsHelper.Format(Double.Parse(LbNumlibres.Text) / TotalHospital * 100, "00.00");
					this.PorcentajeTotalOcupadas.Text = StringsHelper.Format(Double.Parse(LbNumocupadas.Text) / TotalHospital * 100, "00.00");
					this.PorcentajeTotal.Text = StringsHelper.Format(Double.Parse(LbNumtotal.Text) / TotalHospital * 100, "00.00");
				}
				else
				{
					this.PorcentajeTotalBloquedas.Text = "00,00";
					this.PorcentajeTotalInhabilitadas.Text = "00,00";
					this.PorcentajeTotalLibres.Text = "00,00";
					this.PorcentajeTotalOcupadas.Text = "00,00";
					this.PorcentajeTotal.Text = "00,00";
				}
			}
		}
		public void fnrecogertag(string informacion, SqlConnection RcConexion)
		{
			this.Tag = informacion;
			Conexion = RcConexion;
		}


		private int ObtenerTotalPlanta()
		{
			int result = 0;
			try
			{
				string sqlTotal = String.Empty;
				DataSet RrTotal = null;
				// total de camas en la planta dada
				sqlTotal = "select count(gcamasbo) from dcamasbova,DUNIENFEva where " + 
				           " DUNIENFEva.ITIPOSER='" + Module1.vstHosUr + "'" + 
				           " and gcounien=gunidenf and gplantas =" + this.LbnumPlanta.Text.Trim();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTotal, Conexion);
				RrTotal = new DataSet();
				tempAdapter.Fill(RrTotal);
				result = Convert.ToInt32(RrTotal.Tables[0].Rows[0][0]);
				RrTotal.Close();
				return result;
			}
			catch (SqlException ex)
			{
				Serrores.GestionErrorBaseDatos(ex, Conexion);

				return result;
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
                return result;
            }
        }


		private int ObtenerTotalHospital()
		{
			int result = 0;
			try
			{
				string sqlTotal = String.Empty;
				DataSet RrTotal = null;
				// total de camas en el hospital
				sqlTotal = "select count(gcamasbo) from dcamasbova,DUNIENFEva where " + 
				           " DUNIENFEva.ITIPOSER='" + Module1.vstHosUr + "'" + 
				           " and gcounien=gunidenf";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTotal, Conexion);
				RrTotal = new DataSet();
				tempAdapter.Fill(RrTotal);
				result = Convert.ToInt32(RrTotal.Tables[0].Rows[0][0]);
				RrTotal.Close();
				return result;
			}
			catch (SqlException ex)
			{
				Serrores.GestionErrorBaseDatos(ex, Conexion);
				return result;
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
                return result;
            }
        }

		private void ESTADO_PLANTAS_Load(Object eventSender, EventArgs eventArgs)
		{
			//Set Conexion = VISUALIZACION.RcAdmision
		}

		private void ESTADO_PLANTAS_Closed(Object eventSender, EventArgs eventArgs)
		{
			Conexion = null;
		}
	}
}