using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace mantecamas
{
	public class manteclass
	{

		private VISUALIZACION Instancia = null;
		public string vNomAplicacion = String.Empty;
		public string VAyuda = String.Empty;

		//********************************************************************************************************
		//*                                                                                                      *
		//*  Procedimiento: proload                                                                              *
		//*                                                                                                      *
		//*  Modificación:                                                                                       *
		//*                                                                                                      *
		//*    O.Frias (14/11/2006) Se incorpora la variable pBlnReserva, está contendrá el valor para mostrar   *
		//*                         el boton de reserva o no, esta variable es mandada a fnrecogeser             *
		//*                                                                                                      *
		//********************************************************************************************************
		public object proload(manteclass clase, object Formulario, ref SqlConnection conexion, string tipo, string usuario, string TipoConsulta, string SexoPaciente = "", bool Restriccion = false, string RegimenPaciente = "", bool pBlnReserva = false)
		{
			Instancia = new VISUALIZACION();
			Serrores.oClass = new Mensajes.ClassMensajes();
			vNomAplicacion = Serrores.ObtenerNombreAplicacion(conexion);
			VAyuda = Serrores.ObtenerFicheroAyuda();
			Module1.VstCodUsua = usuario;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref conexion);
			Module1.Buscarcombo = new BusquedaCombos.ClsBuscarElemCombos();


			//******************* O.Frias (14/11/2006) Inicio *******************
			Instancia.fnrecogeser(clase, Formulario, conexion, tipo, TipoConsulta, SexoPaciente, Restriccion, RegimenPaciente, String.Empty, pBlnReserva);
			//******************** O.Frias (14/11/2006) Fin  ********************
			Instancia.ShowDialog();

			return null;
		}
	}
}