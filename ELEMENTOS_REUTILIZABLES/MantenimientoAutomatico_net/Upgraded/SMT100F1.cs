using Microsoft.VisualBasic;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace MantenAutomatico
{
	public partial class SMT100F1
		: RadForm
	{

		private SqlCommand _RqSQL = null;
		public SMT100F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}


		SqlCommand RqSQL
		{
			get
			{
				if (_RqSQL == null)
				{
					_RqSQL = new SqlCommand();
				}
				return _RqSQL;
			}
			set
			{
				_RqSQL = value;
			}
		}


		bool AlgoNadaVisible = false;

		int iIndice = 0; //indice para recorrer el Resultset
		int iindice2 = 0; //indice para recorrer las columnas de la Spread
		string stPulsado = String.Empty; //bot�n pulsado en la Spread

		int iIndiceTexto = 0; //<indices para crear cada uno de los controles
		int iIndiceCombo = 0; //<
		int iIndiceFecha = 0; //<
		int iIndiceMultilinea = 0; //<
		int iIndiceEtiqueta = 0; //<
		int iIndiceMascara = 0; //<

		string stCaptionForm = String.Empty; //< Nombre del formulario

		int iIndiceCampoTexto = 0; //<indices de apoyo para crear controles
		int iIndiceCampoFecha = 0; //<
		int iIndiceCampoCombo = 0; //<
		int iIndiceCampoMultilinea = 0; //<
		int iIndiceCampoMascara = 0; //<

		bool vfComprobador = false; //<Booleanos para salir de las compro-
		bool vfComprCombo = false; //<baciones del primer campo creado
		bool vfComprFecha = false; //<
		bool vfComprTexto = false; //<
		bool vfComprMultilinea = false; //<
		bool vfComprMascara = false; //<
		bool vfCambioScroll = false; //<


		int viNuevoTop = 0; //Nuevo Top al cambio de linea
		int viNuevoLeft = 0;

		int viControlarScroll = 0; //Controles del Scroll vertical del Detalle
		int viControlarValorScroll = 0;

		private struct Consultas
		{
			public string Campo;
			public string Busqueda;
			public static Consultas CreateInstance()
			{
					Consultas result = new Consultas();
					result.Campo = String.Empty;
					result.Busqueda = String.Empty;
					return result;
			}
		}

		private SMT100F1.Consultas[] LaConsulta = null;

		//*************************CRIS******************************
		//Variable que me da la diferencia entre la altura del frame contenedor
		//de los otros frames, y la del frame contenedor de los controles
		int iDiferencia = 0;
		//Boolena que me indica si el formato o la m�scara son validos o no
		bool FFormatoValido = false;
		bool FMascaraValida = false;
		//Cadenas que van a tener los caracteres v�lidos para formato y m�scara
		string StFormatoNumero = String.Empty;
		string StFormatoCadena = String.Empty;
		string StFormatoFechaHora = String.Empty;
		string StMascaraNumero = String.Empty;
		string StMascaraCadena = String.Empty;
		string StMascaraFechaHora = String.Empty;

		//Para saber que el vcampo fcha tiene que ser null
		bool fSoloNULL = false;
		//Para saber el indice del control fecha que solo puede ser null
		int iIndexFecha = 0;
		//Para saber si se ha cambiado una fecha de borrado a NULA
		bool fActivarRegistro = false;
		//N� de clicks que se tienen que hacer en la scrollbar para ver
		//todo el detalle de campos
		int lClicks = 0;
		//Integer para saber el indice de los controles OptionButton
		int iIndiceOption = 0;
		//Se encuentran en la tabla de constantes globales
		//Constante ALGONATO valores valfanu1, valfanu2 y valfanu3,
		//y sirven para saber los literales de los combos de consulta
		string StALGO = String.Empty;
		string StNADA = String.Empty;
		string StTODO = String.Empty;
		string StCOMIENZE = String.Empty;
		string StCONTENGA = String.Empty;
		string StCOINCIDA = String.Empty;

		//Longitud m�xima de las tres variables anteriores
		int IMaxLen = 0;

		int[] aLongitudListado = null;

		//Booleana que indica si hay alg�n control activo
		bool fControlActivo = false;

		SqlCommand Rq = null;

		//**********************
		//Variable de formulario para crear instancias de �l mismo
		//cuando el programa se llame a s� mismo
		dynamic FrInstanciaFormulario = null;
		object FrInstanciaFormulario2 = null;
		//Variable de Clase para volver a llamarse el programa
		//a s� mismo
		object ClassMantenimiento = null;
		//Columnas que nos pasa el Mantenimiento Autom�tico cuando
		//viene de combos y sirven para saber que valores tenemos que devolver
		string[] AStColumnasFK = null;
		//Columnas que pasamos cuando llamamos a un Mantenimeinto Autom�tico
		//desde un combo, y son las que utiliza para saber los valores que
		//nos tiene que devolver
		string[] AStColumnasFKPadre = null;
		//Booleana que sirve para saber si un Mantenimiento Autom�tico viene
		//llamado por un combo o no
		bool fVieneDeCombos = false;

		//Variables que se van a devolver a un combo que
		//haya llamado a Mantenimiento Autom�tico

		//Descripci�n del combo
		string StDescripcion = String.Empty;
		//Valores de las columnas clave
		string[] AStClaves = null;
		//Tipo de dato de las columnas clave
		string[] AStTipoCol = null;
		//Nombre de las columnas clave
		string[] AStColumnas = null;

		//Cantidad de registros que tiene la consulta
		int vlCantidad = 0;
		//Variable para el cambio de contrase�a
		string vstContrase�a = String.Empty;
		//Ser� OR � AND
		string vstCriterioConsulta = String.Empty; //para las consultas
		string[] aComparacion = null;
		string[] aActualizaSpread = null;
		int[] aNumeroColumna = null;
		int[] aLongitud = null;
		//Nombre de la tabla que se est� manteniendo
		public string vstNombreTabla = String.Empty;
		//Descripci�n larga de la tabla que se est� manteniendo
		public string vstDescrLarTabla = String.Empty;
		//Descripci�n corta de la tabla que se est� manteniendo
		public string vstDescrCortaTabla = String.Empty;
		//Variable que nos dice si la tabla es
		//actualizable o es solo de consulta
		public string vstActualizable = String.Empty;
		int viColumnaPassword = 0;
		ModuloMantenim.ConexionControles[] LaConexion = null;
		bool vfCambios = false;
		bool vfCambiosPassword = false;
		public bool vfTodo = false;
		ModuloMantenim.Clave[] AStPK = null;
		int Tope = 0;
		bool CargarMas = false;
		bool NuevaConsulta = false;
		ModuloMantenim.StrOrden[] AStOrden = null;
		bool fOrden = false;
		ModuloMantenim.StrParametro[] AStParametro = null;
		bool fUnicos = false;
		//HelpContextID de la ayuda de cada pantalla
		public int vlAyuda = 0;
		//Para no cerrar el Requery si no se ha abierto
		public bool vfCerrar = false;
		ModuloMantenim.StrColumnasFK[] aStrFK = null;
		ModuloMantenim.StrToolbar[] AStrToolbar = null;
		ModuloMantenim.StrFKUnicos[] AStrFKUnicos = null;
		int viIndiceComboInhabilitado = 0;
		//Controles de la pantalla
		ModuloMantenim.strConjuntoResultados[] astrElConjunto = null;

		int iNumeroFilasVisibles = 0;

		private string ObtenerOrdenFilas(SqlConnection RcConexion, string stTablaOrden)
		{
			//Obtengo las columnas por las que se ordenan los registros
			//de las consultas y las caracter�sticas de las mismas

			string result = String.Empty;

			ModuloMantenim.stSQLOrder = "";

			string StSQL = "select gcolumncaman,nordenacion,iascendente," + "nparteentera,npartedecima,gtipodato,nordencoltab,valordefecto ";
			StSQL = StSQL + "from SPRETBSPV1 ";
			StSQL = StSQL + "where GTABLAMANAUT='";
			StSQL = StSQL + stTablaOrden + "'";
			StSQL = StSQL + " and nordenacion <> 0";
			StSQL = StSQL + " order by nordenacion";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSQL, ModuloMantenim.RcManten);
			ModuloMantenim.RsManten2 = new DataSet();
			tempAdapter.Fill(ModuloMantenim.RsManten2);
			if (ModuloMantenim.RsManten2.Tables[0].Rows.Count <= 0)
			{
				ModuloMantenim.RsManten2.Close();
				result = "";
				//**************************************
				fOrden = false;
				//**************************************
				return result;
			}
			else
			{
				//**************************************
				AStOrden = new ModuloMantenim.StrOrden[ModuloMantenim.RsManten2.Tables[0].Rows.Count];
				fOrden = true;
				//**************************************
				ModuloMantenim.stSQLOrder = " order by ";
				for (int iIndiceOrden = 0; iIndiceOrden <= ModuloMantenim.RsManten2.Tables[0].Rows.Count - 1; iIndiceOrden++)
				{
					if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["iascendente"]) == "S")
					{
						ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim() + " " + "ASC,";
					}
					else
					{
						//El orden siempre va a ser ascendente para poder hacer el
						//ReQuery ordenado
						ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim() + " " + "ASC,"; //"DESC,"
					}
					//***********************CRIS***********************************
					AStOrden[iIndiceOrden].StColumna = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim();
					//*******************LUIS*******************
					if (!Convert.IsDBNull(ModuloMantenim.RsManten2.Tables[0].Rows[0]["valordefecto"]))
					{
						AStOrden[iIndiceOrden].vDefecto = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["valordefecto"]).Trim();
					}
					else
					{
						AStOrden[iIndiceOrden].vDefecto = "";
					}
					//*************Luis**************

					switch(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim())
					{
						case "NUMERIC" :  //Un espacio m�s para el signo 
							AStOrden[iIndiceOrden].iLongitud = Convert.ToInt16(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 1); 
							//Un espacio m�s para el punto decimal 
							if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
							{
								AStOrden[iIndiceOrden].iLongitud = (short) (AStOrden[iIndiceOrden].iLongitud + 1);
							} 
							break;
						case "INT" : case "SMALLINT" :  //Un espacio m�s para el signo 
							AStOrden[iIndiceOrden].iLongitud = Convert.ToInt16(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 1); 
							break;
						case "CHAR" : case "VARCHAR" : case "DATETIME" : 
							AStOrden[iIndiceOrden].iLongitud = (short) (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])); 
							 
							break;
					}

					AStOrden[iIndiceOrden].StTipo = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim();
					AStOrden[iIndiceOrden].iOrdenColumnaSpread = (short) ModuloMantenim.RsManten2.Tables[0].Rows[0]["nordencoltab"];
					//********************************************************************************************************
					ModuloMantenim.RsManten2.MoveNext();
				}
			}
			ModuloMantenim.RsManten2.Close();
			//Para quitar la �ltima coma
			return ModuloMantenim.stSQLOrder.Substring(0, Math.Min(ModuloMantenim.stSQLOrder.Length - 1, ModuloMantenim.stSQLOrder.Length));
			//ObtenerOrdenFilas = stSQLOrder
		}


		public string MontarSelectConsulta()
		{

			StringBuilder stSQLBorrar = new StringBuilder();

			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{
				if (i == 0)
				{
					stSQLBorrar = new StringBuilder("SELECT * FROM " + vstNombreTabla);
					stSQLBorrar.Append(" where ");
				}
				else
				{
					stSQLBorrar.Append(" and ");
				}

				switch(AStPK[i].StTipo)
				{
					case "DATETIME" :  //Campo fecha 
						//Formateamos la fecha DD/MM/YYYY 
						stSQLBorrar.Append("CONVERT(char(10)," + AStPK[i].StColumna + ",103)"); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							System.DateTime TempDate = DateTime.FromOADate(0);
							stSQLBorrar.Append("=" + "'" + ((DateTime.TryParse(AStPK[i].stTexto.Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : AStPK[i].stTexto.Trim()) + "'");
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
					case "VARCHAR" : case "CHAR" :  //Campo texto con o sin m�scara 
						stSQLBorrar.Append(AStPK[i].StColumna); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							stSQLBorrar.Append("=" + "'" + AStPK[i].stTexto.Trim() + "'");
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
					case "NUMERIC" : case "SMALLINT" : case "INT" :  //Campo num�rico 
						stSQLBorrar.Append(AStPK[i].StColumna); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							stSQLBorrar.Append("=" + AStPK[i].stTexto.Trim());
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
				}
			}
			return stSQLBorrar.ToString();
		}


		private bool IncorporarDB()
		{
			//Dar un alta en la Base de Datos
			bool result = false;
			string stSQLManten = String.Empty;
			Control[] MiControl = null;
			int iIndiceIncorporar = 0;
			string lNumerico = null; //As Long

			int i = 0;

			try
			{

				if (vfCambiosPassword)
				{
					SMT200F1 tempLoadForm = SMT200F1.DefInstance;
					SMT200F1.DefInstance.RecibirContrase�a(vstContrase�a);
					SMT200F1.DefInstance.ShowDialog();
				}
                ModuloMantenim.RcManten.BeginTransScope();
                stSQLManten = "select * from " + vstNombreTabla;
				stSQLManten = stSQLManten + " where 2=1";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQLManten, ModuloMantenim.RcManten);
				ModuloMantenim.RsManten2 = new DataSet();
				tempAdapter.Fill(ModuloMantenim.RsManten2);
				ModuloMantenim.RsManten2.AddNew();

				//Bucle For para Incorporar en la DB.
				for (iIndiceIncorporar = 0; iIndiceIncorporar <= (sprMultiple.MaxCols - 1); iIndiceIncorporar++)
				{
					MiControl = new Control[iIndiceIncorporar + 1];
					MiControl[iIndiceIncorporar] = LaConexion[iIndiceIncorporar].NombreControl;

					if (LaConexion[iIndiceIncorporar].NombreControl.Name != "cbbCombo")
					{
						if (ComprobarNumericos(LaConexion[iIndiceIncorporar].NombreCampo))
						{
							if (Convert.ToString(LaConexion[iIndiceIncorporar].NombreControl.Text).Trim() == "")
							{
								ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
							}
							else
							{
								if (LaConexion[iIndiceIncorporar].NombreControl is MaskedTextBox)
								{
									lNumerico = LaConexion[iIndiceIncorporar].NombreControl.FormattedText(); //CLng(Trim(LaConexion(iIndiceIncorporar).NombreControl.text))
								}
								else
								{
									//lNumerico = Val(LaConexion(iIndiceIncorporar).NombreControl.Text)
									lNumerico = LaConexion[iIndiceIncorporar].NombreControl.Text;
								}


								//OSCAR C Diciembre 2009
								//En el caso de que el tipo de campo sea decimal, se deben considerar los decimales
								//RsManten2(i) = Val(lNumerico)
								if (ModuloMantenim.RsManten2.Tables[0].Rows[0][i].GetType() == typeof(decimal))
								{
									ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = lNumerico;
								}
								else
								{
									ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = Conversion.Val(Convert.ToString(lNumerico));
								}
								//--------------------

							}
						}
						else
						{
							if (Convert.ToString(LaConexion[iIndiceIncorporar].NombreControl.Text).Trim() == "")
							{
								ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
							}
							else
							{
								if (LaConexion[iIndiceIncorporar].NombreControl is MaskedTextBox)
								{
									ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = LaConexion[iIndiceIncorporar].NombreControl.FormattedText();
								}
								else
								{
									ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = LaConexion[iIndiceIncorporar].NombreControl.Text;
								}
							}
						}
						i++;
					}
				}

				//Oscar
				//Parece ser que se inserta un registro de la BD, por lo que se tiene
				//que auditar.Se hace antes del update, porque en caso de producirse un error
				//en la actualizacion, se hace un rollback de la transaccion
				ModuloMantenim.AUDITARMOVIMIENTO("A", vstNombreTabla, Serrores.VVstUsuarioApli, ModuloMantenim.RsManten2);
				//---------------

				//oscar 04/06/2004
				string sql = String.Empty;
				if (vfCambiosPassword && vstNombreTabla == "SUSUARIO")
				{
					sql = "INSERT INTO SUSUCCON (FCAMBIO, GUSUARIO, NPASSWORD) " + 
					      " VALUES (GETDATE(), '" + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gusuario"]) + "','" + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["vpassword"]) + "')";
				}
				//-------

				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(ModuloMantenim.RsManten2, ModuloMantenim.RsManten2.Tables[0].TableName);

				//oscar 04/06/2004
				if (vfCambiosPassword && vstNombreTabla == "SUSUARIO")
				{
					SqlCommand tempCommand = new SqlCommand(sql, ModuloMantenim.RcManten);
					tempCommand.ExecuteNonQuery();
				}
				//----------------

				//Cargar el �ltimo registro de la grid
				SumarSpread();

				//''''''''''''''Visualizar el �ltimo registro
				ModuloMantenim.VisualizarRegistro(sprMultiple.MaxRows, this);

				for (int iindice2 = 0; iindice2 <= (iIndiceIncorporar - 1); iindice2++)
				{
					MiControl[iindice2] = null;
				}


				ModuloMantenim.RsManten2.Close();
				result = true;
                ModuloMantenim.RcManten.CommitTransScope();


                return result;
			}
			catch(Exception ex)
			{
				//En pruebas
				ModuloMantenim.ColeccionErrores(ex);
				ModuloMantenim.RsManten2.Close();
                ModuloMantenim.RcManten.RollbackTransScope();



                return result;
			}
		}
		private bool ModificaUpdate(DataSet Cursor_Renamed, SqlDataAdapter tempAdapter)
		{
			bool result = false;
			try
			{
				result = true;
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
				tempAdapter.Update(Cursor_Renamed, Cursor_Renamed.Tables[0].TableName);
				return result;
			}
			catch
			{
				result = false;
				RadMessageBox.Show("Existen incongruencias entre el tipo de campo y el valor introducido" + "\n" + "\r" + "Revise el contenido de los campos ", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
				return result;
			}
		}

		public bool ComprobarDependenciasTabla(string stNombreTabla, string stSQLBorrar, bool fBorrar, SqlDataAdapter tempAdapter_3)
		{

			bool result = false;
			int i = 0;
			StringBuilder StFK = new StringBuilder();
			string StMensaje = String.Empty;
			bool fHayFK = false;
			DialogResult StRes = (DialogResult) 0;

			StringBuilder StComando = new StringBuilder();
			StComando.Append("select A.gtabcamanhij gtabcamanhij,A.gfkcaman gfkcaman," + "A.gcolcamanhij gcolcamanhij,B.gtipodato gtipodato,a.gcolumncaman gcolumncaman," + "B.dcoltabmanco dcoltabmanco " + "from scolfkpkv1 A,SCOCACOLV1 B " + "where A.gtabcamanpad=" + "'" + stNombreTabla.Trim() + "'" + " and A.gtabcamanhij=B.gtablacaman" + " and A.gcolcamanhij=B.gcolumncaman" + " order by A.gtabcamanhij,A.gfkcaman ");

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
			DataSet RsFK = new DataSet();
			tempAdapter.Fill(RsFK);

			if (RsFK.Tables[0].Rows.Count != 0)
			{

				aStrFK = new ModuloMantenim.StrColumnasFK[RsFK.Tables[0].Rows.Count];
				i = 0;

				foreach (DataRow iteration_row in RsFK.Tables[0].Rows)
				{

					aStrFK[i].StTablaHija = Convert.ToString(iteration_row["gtabcamanhij"]).Trim();
					aStrFK[i].StColumnaHija = Convert.ToString(iteration_row["gcolcamanhij"]).Trim();
					aStrFK[i].StNombreGrupoFK = Convert.ToString(iteration_row["gfkcaman"]).Trim();
					aStrFK[i].Stgtipodato = Convert.ToString(iteration_row["gtipodato"]).Trim();
					aStrFK[i].Stdcoltabmanco = Convert.ToString(iteration_row["dcoltabmanco"]).Trim();
					aStrFK[i].fEsta = false;

					for (int k = 0; k <= astrElConjunto.GetUpperBound(0); k++)
					{ //-1
						if (astrElConjunto[k].gcolumncaman.Trim() == Convert.ToString(iteration_row["gcolumncaman"]).Trim())
						{
							aStrFK[i].NombreControl = astrElConjunto[k].NombreControl;
							break;
						}
					}

					i++;
				}
				RsFK.Close();

				for (i = 0; i <= aStrFK.GetUpperBound(0); i++)
				{
					if (!aStrFK[i].fEsta)
					{
						//aStrFK(i).fEsta nos indica si el campo est� ya
						//en el conjunto de grupos de FK

						StComando = new StringBuilder("select * from " + aStrFK[i].StTablaHija.Trim() + " where ");

						//Recorro el array en busca de otras columnas que
						//pertenezcan al mismo grupo de FK y la
						//a�ado al where
						for (int j = 0; j <= aStrFK.GetUpperBound(0); j++)
						{
							if (aStrFK[i].StNombreGrupoFK.Trim() == aStrFK[j].StNombreGrupoFK.Trim())
							{

								aStrFK[i].fEsta = true;
								aStrFK[j].fEsta = true;

								//Compruebo si ya ha habido anteriormente alguna columna
								//perteneciente a este grupo de FK
								if (fHayFK)
								{
									StComando.Append(" AND ");
								}
								fHayFK = true;

								//SI EL CAMPO ES DATETIME, NO SE TENDR� EN CUENTA LA HORA.
								//SE CONVIERTE LA FECHA-HORA A FECHA FORMATEADA A "DD/MM/YYYY"
								if (aStrFK[j].Stgtipodato.Trim() == "DATETIME")
								{
									StComando.Append("CONVERT(char(10)," + aStrFK[j].StColumnaHija + ",103)");
								}
								else
								{
									StComando.Append(aStrFK[j].StColumnaHija);
								}

								if (Convert.ToString(aStrFK[j].NombreControl.Text).Trim() != "")
								{

									StComando.Append("=");

									//SI ES "CHAR", "VARCHAR" O "DATETIME",
									//EL DATO VA ENTRE COMILLA SIMPLE
									if (aStrFK[j].Stgtipodato.Trim() == "VARCHAR" || aStrFK[j].Stgtipodato.Trim() == "CHAR" || aStrFK[j].Stgtipodato.Trim() == "DATETIME")
									{
										StComando.Append("'");
									}

									//SI EL CONTROL ES M�SCARA, HABR� QUE COGER EL FORMATTEDTEXT
									//EN VEZ DEL TEXT PARA QUE COJA EL PUNTO EN LOS DECIMALES
									if (aStrFK[j].NombreControl.Name == "mebMascara")
									{
										StComando.Append(Convert.ToString(aStrFK[j].NombreControl.FormattedText()).Trim());
									}
									else
									{
										StComando.Append(Convert.ToString(aStrFK[j].NombreControl.Text).Trim());
									}

									//SI ES "CHAR", "VARCHAR" O "DATETIME",
									//EL DATO VA ENTRE COMILLA SIMPLE
									if (aStrFK[j].Stgtipodato.Trim() == "VARCHAR" || aStrFK[j].Stgtipodato.Trim() == "CHAR" || aStrFK[j].Stgtipodato.Trim() == "DATETIME")
									{
										StComando.Append("'");
									}

								}
								else
								{
									//SI EL CAMPO EST� EN BLANCO, SE COMPARA CON NULL
									StComando.Append(" IS NULL ");
								}
							}
						}


						if (fHayFK)
						{
							//Si hemos encontrado algun grupo de FK
							//Lanzamos el resulset y vemos si hay algun registro igual en la
							//tabla
							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
							RsFK = new DataSet();
							tempAdapter_2.Fill(RsFK);

							if (RsFK.Tables[0].Rows.Count != 0)
							{

								//Montamos el mensaje de error con el nombre del grupo
								//y el de las columnas que pertenecen al grupo de FK
								StFK = new StringBuilder("");
								if (aStrFK.GetUpperBound(0) != 0)
								{
									for (int j = 0; j <= aStrFK.GetUpperBound(0) - 1; j++)
									{
										if (aStrFK[i].StNombreGrupoFK.Trim() == aStrFK[j].StNombreGrupoFK.Trim())
										{
											if (StFK.ToString().Trim() != "")
											{
												StFK.Append(", ");
											}
											StFK.Append(aStrFK[j].Stdcoltabmanco);
										}
									}
								}
								else
								{
									StFK.Append(aStrFK[0].Stdcoltabmanco);
								}

								//Compruebo si viene de borrado
								if (fBorrar)
								{
									//*****************************************************
									//Compruebo que la tabla es una de las que se
									//puede realizar borrado l�gico

									if (ModuloMantenim.TablaBorradoLogico(vstNombreTabla, this))
									{
										if (Convert.IsDBNull(ModuloMantenim.RsManten2.Tables[0].Rows[0][ModuloMantenim.StFBORRADO]))
										{
											//Pregunto si desea borrar el registro l�gicamente
											StRes = RadMessageBox.Show("Se ha producido un error de integridad referencial. �Desea eliminar el registro l�gicamente?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question);
											if (StRes == System.Windows.Forms.DialogResult.Yes)
											{
												//Si la respuesta es s� actualizo la
												//fecha de borrado con la del sistema
												ModuloMantenim.RsManten2.Edit();
												ModuloMantenim.RsManten2.Tables[0].Rows[0][ModuloMantenim.StFBORRADO] = DateTime.Today.ToString("dd/MM/yyyy");
												SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
												tempAdapter_3.Update(ModuloMantenim.RsManten2, ModuloMantenim.RsManten2.Tables[0].TableName);
												ActualizarFechaBorradoSpread();
											}
										}
										else
										{
											//Se muestra que se ha detectado el error
											//de problemas de integridad referencial
											RadMessageBox.Show("Hay errores de integridad y el registro ya ha sido borrado l�gicamente", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
										}
									}
									else
									{
										StMensaje = "No se puede eliminar el registro porque se ha producido " + "un error de integridad referencial con el " + "Grupo de Foreign Key " + aStrFK[i].StNombreGrupoFK + " formado por " + StFK.ToString();
										RadMessageBox.Show(StMensaje, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);

									}
									//*****************************************************
								}
								return result;
							}
							RsFK.Close();
						}
						fHayFK = false;
					}
				}
			}

			return true;
			//****************************************************************************
			//Si llega a esta parte es porque no ha encontrado ningun registro igual en las
			//tablas referenciadas.
			//Se desactivan las CONSTRAINTS, se borra el registro y se vuelven a activar
			//los constraints
			//****************************************************************************

		}

		private bool ValidarTama�oFijo(int stTama�o, int iIndiceControl)
		{
			//Comprobaremos que el campo est� relleno al m�ximo

			if (stTama�o < (astrElConjunto[iIndiceControl].npartedecima) + (astrElConjunto[iIndiceControl].nparteentera) && astrElConjunto[iIndiceControl].NombreControl.Enabled)
			{
				//Mensaje para decir al usuario que debe rellenar todo el campo
				short tempRefParam = 1640;
				string[] tempRefParam2 = new string[]{astrElConjunto[iIndiceControl].dcoltabmanco};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
				//*************************CRIS**************************
				astrElConjunto[iIndiceControl].NombreControl.Focus();
				//*******************************************************
				return false;
			}
			return true;
		}
		private void DevolverDatos()
		{

			AStClaves = ArraysHelper.InitializeArray<string>(AStColumnasFK.GetUpperBound(0) + 1);
			AStTipoCol = ArraysHelper.InitializeArray<string>(AStColumnasFK.GetUpperBound(0) + 1);

			for (int i = 0; i <= AStColumnasFK.GetUpperBound(0); i++)
			{
				foreach (ModuloMantenim.strConjuntoResultados astrElConjunto_item in astrElConjunto)
				{
					if (AStColumnasFK[i].Trim() == astrElConjunto_item.gcolumncaman.Trim())
					{
						//Comprobamos si es control fecha
						if (astrElConjunto_item.NombreControl.Name == "sdcFecha")
						{
							System.DateTime TempDate = DateTime.FromOADate(0);
							AStClaves[i] = (DateTime.TryParse(Convert.ToString(astrElConjunto_item.NombreControl.Text).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : Convert.ToString(astrElConjunto_item.NombreControl.Text).Trim();
							//Comprobamos si el control es Masked Edit
						}
						else if (astrElConjunto_item.NombreControl.Name == "mebMascara")
						{ 
							AStClaves[i] = Convert.ToString(astrElConjunto_item.NombreControl.FormattedText()).Trim();
						}
						else
						{
							AStClaves[i] = Convert.ToString(astrElConjunto_item.NombreControl.Text).Trim();
						}
						AStTipoCol[i] = astrElConjunto_item.gtipodato.Trim();
						break;
					}
				}
			}
		}

		public void InstanciaForm(int lNumRegistros, object FrInstancia, object varAStColumnasClave_optional, int? varIndiceCombo_optional)
		{
			string[] varAStColumnasClave = (varAStColumnasClave_optional == Type.Missing) ? null : varAStColumnasClave_optional as string[];
			int varIndiceCombo = (varIndiceCombo_optional == null) ? 0 : varIndiceCombo_optional.Value;

			vlCantidad = lNumRegistros;

			FrInstanciaFormulario = FrInstancia;

			if (varAStColumnasClave_optional != Type.Missing)
			{
				AStColumnasFK = new string[]{String.Empty};
				for (int Elemento = 0; Elemento <= varAStColumnasClave.GetUpperBound(0); Elemento++)
				{
					AStColumnasFK = ArraysHelper.RedimPreserve(AStColumnasFK, new int[]{Elemento + 1});
					AStColumnasFK[Elemento] = varAStColumnasClave[Elemento];
				}
				fVieneDeCombos = true;
			}
			else
			{
				fVieneDeCombos = false;
			}

			if (varIndiceCombo_optional != null)
			{
				viIndiceComboInhabilitado = varIndiceCombo;
			}
		}

		public void InstanciaForm(int lNumRegistros, object FrInstancia, object varAStColumnasClave_optional)
		{
			InstanciaForm(lNumRegistros, FrInstancia, varAStColumnasClave_optional, null);
		}

		public void InstanciaForm(int lNumRegistros, object FrInstancia)
		{
			InstanciaForm(lNumRegistros, FrInstancia, Type.Missing, null);
		}

		private void AlturaFrame(Control control)
		{

			frmDetalle.Height = (int) (frmDetalle.Height + control.Height + ModuloMantenim.COiSeparacion / 15);

		}

		private void CargarComboAlgoNada()
		{
			int i = 0;
			//********************************************************
			return;
			//********************************************************
			Cbb_AlgoNada[i].Items.Clear();
			ObtenerConstantesCombo();
			int tempForVar = Cbb_AlgoNada.GetUpperBound(0);
			for (i = 1; i <= tempForVar; i++)
			{
				string tempRefParam = "";
				object tempRefParam2 = Type.Missing;
                Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam, tempRefParam2));
				string tempRefParam3 = StALGO;
				object tempRefParam4 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam3, tempRefParam4));
				StALGO = Convert.ToString(tempRefParam3);
				string tempRefParam5 = StNADA;
				object tempRefParam6 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam5, tempRefParam6));
				StNADA = Convert.ToString(tempRefParam5);
				string tempRefParam7 = StTODO;
				object tempRefParam8 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam7, tempRefParam8));
				StTODO = Convert.ToString(tempRefParam7);
				string tempRefParam9 = StCOMIENZE;
				object tempRefParam10 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam9, tempRefParam10));
				StCOMIENZE = Convert.ToString(tempRefParam9);
				string tempRefParam11 = StCONTENGA;
				object tempRefParam12 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam11, tempRefParam12));
				StCONTENGA = Convert.ToString(tempRefParam11);
				string tempRefParam13 = StCOINCIDA;
				object tempRefParam14 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam13, tempRefParam14));
				StCOINCIDA = Convert.ToString(tempRefParam13);
				//5.4 son el n� de caracteres X en 1 cm
				//1 caracter X son 105 twips
				//Necesitamos saber los cm porque esa es la unidad de
				//la propiedad ListWidth
				//    Cbb_AlgoNada(i).ListWidth = ((IMaxLen + 2) / 5.4) & "cm"
			}
		}

		private object ComprobarCamposConsulta()
		{

			string stFecha = String.Empty;
			LaConsulta = new SMT100F1.Consultas[LaConexion.GetUpperBound(0) + 1];

			this.Cursor = Cursors.WaitCursor;

			//Recorrer los controles y comprobar si no est�n vac�os
			for (int iControles = 0; iControles <= LaConsulta.GetUpperBound(0) - 1; iControles++)
			{
				if (Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() != "")
				{
					//Deberiamos comprobar si los campos son num�ricos 0 fecha para convertirlos a cadenas
					if (ComprobarNumericos(LaConexion[iControles].NombreCampo))
					{

						LaConsulta[iControles].Campo = "CONVERT(VARCHAR," + LaConexion[iControles].NombreCampo + ")";
						if (LaConexion[iControles].NombreControl is MaskedTextBox)
						{
							if (LaConexion[iControles].NombreControl.Visible)
							{
								LaConsulta[iControles].Busqueda = "'%" + Convert.ToString(LaConexion[iControles].NombreControl.FormattedText()).Trim() + "%'" + " ";
							}
							else
							{
								LaConsulta[iControles].Busqueda = "'" + Convert.ToString(LaConexion[iControles].NombreControl.FormattedText()).Trim() + "'" + " ";
							}
						}
						else
						{
							if (LaConexion[iControles].NombreControl.Visible)
							{
								LaConsulta[iControles].Busqueda = "'%" + Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() + "%'" + " ";
							}
							else
							{
								LaConsulta[iControles].Busqueda = "'" + Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() + "'" + " ";
							}
						}

					}
					else if (ComprobarFechas(LaConexion[iControles].NombreCampo))
					{ 
						LaConsulta[iControles].Campo = LaConexion[iControles].NombreCampo;
						stFecha = (Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim());
						System.DateTime TempDate = DateTime.FromOADate(0);
						stFecha = (DateTime.TryParse(stFecha, out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : stFecha;
						LaConsulta[iControles].Busqueda = "'" + stFecha + "'" + " ";

					}
					else
					{

						LaConsulta[iControles].Campo = LaConexion[iControles].NombreCampo;
						if (LaConexion[iControles].NombreControl is MaskedTextBox)
						{
							if (LaConexion[iControles].NombreControl.Visible)
							{
								LaConsulta[iControles].Busqueda = "'%" + Convert.ToString(LaConexion[iControles].NombreControl.FormattedText()).Trim() + "%'" + " ";
							}
							else
							{
								LaConsulta[iControles].Busqueda = "'" + Convert.ToString(LaConexion[iControles].NombreControl.FormattedText()).Trim() + "'" + " ";
							}
						}
						else
						{
							if (LaConexion[iControles].NombreControl.Visible)
							{
								LaConsulta[iControles].Busqueda = "'%" + Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() + "%'" + " ";
							}
							else
							{
								LaConsulta[iControles].Busqueda = "'" + Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() + "'" + " ";
							}
						}

					}
				}
			}

			this.Cursor = Cursors.Default;

			return null;
		}


		private void HacerSelect(string StTabla, string StComando, bool fOrdenar)
		{
			int p = 0;
			bool fEsta = false;
			//stConsulta = StComando & " "
			ModuloMantenim.stConsulta = " ";
			string ConsultaTempo = StComando;
			string ConsultaFiltro = " ";

			if (fOrden)
			{
				AStParametro = new ModuloMantenim.StrParametro[AStOrden.GetUpperBound(0) + 1];
				//Montamos el WHERE necesario para hacer el ReQuery:
				//concatenando las columnas por las que se ordena y
				//las que forman parte de la clave primaria
				for (int i = 0; i <= AStOrden.GetUpperBound(0); i++)
				{
					switch(AStOrden[i].StTipo.Trim())
					{
						case "VARCHAR" : case "CHAR" : 
							//(maplaza)(18/12/2006)Si el campo es NULO, se produce error para BBDD con compatibilidad 70 
							//stConsulta = stConsulta & " CONVERT(CHAR(" _
							//'& AStOrden(i).iLongitud & ")," _
							//'& AStOrden(i).StColumna & ")" 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(" + AStOrden[i].iLongitud.ToString() + ")," + "ISNULL(" + AStOrden[i].StColumna + ",''))"; 
							//----- 
							 
							break;
						case "NUMERIC" : case "INT" : case "SMALLINT" : 
							//Para las columnas num�ricas tengo que coger el n�mero, 
							//concatenarle espacios por la izquierda hasta la longitud 
							//del campo 
							 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " REPLICATE('0'," + AStOrden[i].iLongitud.ToString() + "-DATALENGTH(RTRIM(CONVERT(CHAR(" + AStOrden[i].iLongitud.ToString() + ")," + AStOrden[i].StColumna + ")))) + " + "RTRIM(CONVERT(CHAR(" + AStOrden[i].iLongitud.ToString() + ")," + AStOrden[i].StColumna + "))"; 
							break;
						case "DATETIME" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(10)," + AStOrden[i].StColumna + ",111)"; 
							 
							break;
					}
					AStParametro[i].StTipo = AStOrden[i].StTipo;
					AStParametro[i].iLongitud = AStOrden[i].iLongitud;
					AStParametro[i].iOrdenColumnaSpread = AStOrden[i].iOrdenColumnaSpread;
					p = i;
					if (i != AStParametro.GetUpperBound(0))
					{
						ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " + ";
					}

				}
			}

			bool fDimensionar = true;

			if (!fOrden)
			{
				AStParametro = new ModuloMantenim.StrParametro[1];
				fDimensionar = false;
			}

			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{

				fEsta = false;
				foreach (ModuloMantenim.StrOrden AStOrden_item in AStOrden)
				{
					if (AStPK[i].StColumna.Trim() == AStOrden_item.StColumna.Trim())
					{
						fEsta = true;
						break;
					}
				}

				if (!fEsta)
				{
					if (fDimensionar)
					{
						AStParametro = ArraysHelper.RedimPreserve(AStParametro, new int[]{p + 2});
						ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " + ";
						if (fOrdenar)
						{
							ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + " , ";
						}
					}

					switch(AStPK[i].StTipo.Trim())
					{
						case "VARCHAR" : case "CHAR" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ")"; 
							break;
						case "NUMERIC" : case "INT" : case "SMALLINT" : 
							//Para las columnas num�ricas tengo que coger el n�mero, 
							//concatenarle espacios por la izquierda hasta la longitud 
							//del campo 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " SPACE(" + AStPK[i].iLongitud.ToString() + "-DATALENGTH(RTRIM(CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ")))) + " + "RTRIM(CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + "))"; 
							break;
						case "DATETIME" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ",112)" + " + " + " CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ",108)"; 
							 
							break;
					}

					if (fOrdenar)
					{
						ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + AStPK[i].StColumna;
					}

					p++;
					AStParametro[p].StTipo = AStPK[i].StTipo;
					AStParametro[p].iLongitud = AStPK[i].iLongitud;
					AStParametro[p].iOrdenColumnaSpread = AStPK[i].iOrdenColumnaSpread;
					fDimensionar = true;
				}
			}
			if (ConsultaFiltro != " ")
			{
				ConsultaFiltro = ConsultaFiltro.Substring(0, Math.Min(ConsultaFiltro.Length - 4, ConsultaFiltro.Length));
			}
			ModuloMantenim.stConsulta = ConsultaTempo + ConsultaFiltro + ModuloMantenim.stConsulta;

			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " > @paramStr ";

			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + ModuloMantenim.stSQLOrder;

		}
		private string Padl(string stCadena, int iEspacios, string stRelleno = " ")
		{
			//Funcion que concatena blancos u otro caracter por la izquierda
			return new string(stRelleno[0], iEspacios - stCadena.Trim().Length) + stCadena.Trim();
		}
		private void InhabilitarControles()
		{
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				//    If Trim(UCase(astrElConjunto(i).gcolumncaman)) = Trim(UCase(StFBORRADO)) Then
				if (astrElConjunto[i].icolfborrado.ToString().ToUpper() == "S")
				{
					astrElConjunto[i].NombreControl.Enabled = true;
					iIndexFecha = ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl);
				}
				else
				{
					astrElConjunto[i].NombreControl.Enabled = false;
				}
			}
		}

		private string MontarConsulta(string stTablaSQL, string stSQLSelect, bool fAND)
		{

			string result = String.Empty;
			bool fQuitarOr = false;

			this.Cursor = Cursors.WaitCursor;

			string stSQLFrom = " from " + stTablaSQL;
			StringBuilder stSQLWhere = new StringBuilder();
			stSQLWhere.Append(" where ");

			for (int iIndiceConsulta = 0; iIndiceConsulta <= LaConsulta.GetUpperBound(0) - 1; iIndiceConsulta++)
			{
				if (LaConsulta[iIndiceConsulta].Busqueda != "")
				{
					if (!ComprobarFechas(LaConsulta[iIndiceConsulta].Campo))
					{
						stSQLWhere.Append("(" + LaConsulta[iIndiceConsulta].Campo + " like " + 
						                  LaConsulta[iIndiceConsulta].Busqueda + ")" + vstCriterioConsulta);
					}
					else
					{
						stSQLWhere.Append("(" + LaConsulta[iIndiceConsulta].Campo + " = " + 
						                  LaConsulta[iIndiceConsulta].Busqueda + ")" + vstCriterioConsulta);
					}
					fQuitarOr = true;
				}
			}

			//Para quitar el �ltimo OR
			if (fQuitarOr)
			{ //Si ha rellenado la Where, se queda con un OR
				//de m�s
				stSQLWhere = new StringBuilder(stSQLWhere.ToString().Substring(0, Math.Min(stSQLWhere.ToString().Length - 4, stSQLWhere.ToString().Length)));
			}
			else
			{
				//Si no ha rellenado la Where, quito el "where"
				stSQLWhere = new StringBuilder("");
			}

			//**************************************
			if (fAND && fQuitarOr)
			{ //Si hay que a�adir un "and" y ha
				//rellenado la Where
				stSQLWhere.Append(" and ");
			}
			else if (fAND)
			{  //Si no ha rellenado la where pero hay que
				//poner un "and", se pone un "where"
				stSQLWhere = new StringBuilder(" where ");
			}
			//**************************************

			string stSqlBuscar = stSQLSelect + stSQLFrom + stSQLWhere.ToString();
			result = stSqlBuscar;

			this.Cursor = Cursors.Default;

			return result;
		}


		private void CrearEtiqueta()
		{
			//Crea una etiqueta nueva,(si no es la primera)

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				if (!vfComprobador)
				{
					iIndiceEtiqueta = 1;
					vfComprobador = true;
					return;
				}
			}

			ControlArrayHelper.LoadControl(this, "lbEtiqueta", iIndiceEtiqueta);
			iIndiceEtiqueta++;

		}

		private bool FBorradoNula()
		{

			bool result = false;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(ObtenerConsultaFBorrado(), ModuloMantenim.RcManten);
			DataSet RsFBorrado = new DataSet();
			tempAdapter.Fill(RsFBorrado);
			result = Convert.IsDBNull(RsFBorrado.Tables[0].Rows[0][ModuloMantenim.StFBORRADO]);
			RsFBorrado.Close();
			return result;
		}

		private void ObtenerConstantesCombo()
		{
			string StComando = "select VALFANU1, VALFANU2, VALFANU3 " + "from SCONSGLO " + "where GCONSGLO IN ('ALGONATO','COMCONCO') order by GCONSGLO";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsAlgo = new DataSet();
			tempAdapter.Fill(RsAlgo);
			if (RsAlgo.Tables[0].Rows.Count != 0)
			{
				StALGO = Convert.ToString(RsAlgo.Tables[0].Rows[0]["valfanu1"]).Trim();
				StNADA = Convert.ToString(RsAlgo.Tables[0].Rows[0]["valfanu2"]).Trim();
				StTODO = Convert.ToString(RsAlgo.Tables[0].Rows[0]["valfanu3"]).Trim();
				RsAlgo.MoveNext();
				StCOMIENZE = Convert.ToString(RsAlgo.Tables[0].Rows[0]["valfanu1"]).Trim();
				StCONTENGA = Convert.ToString(RsAlgo.Tables[0].Rows[0]["valfanu2"]).Trim();
				StCOINCIDA = Convert.ToString(RsAlgo.Tables[0].Rows[0]["valfanu3"]).Trim();
			}
			RsAlgo.Close();
			IMaxLen = StALGO.Length;
			if (IMaxLen < StNADA.Length)
			{
				IMaxLen = StNADA.Length;
			}
			if (IMaxLen < StTODO.Length)
			{
				IMaxLen = StTODO.Length;
			}
		}

		private string ObtenerConsultaFBorrado()
		{
			StringBuilder stSQLBorrar = new StringBuilder();

			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{
				if (i == 0)
				{
					stSQLBorrar = new StringBuilder("SELECT " + ModuloMantenim.StFBORRADO + " FROM " + vstNombreTabla);
					stSQLBorrar.Append(" where ");
				}
				else
				{
					stSQLBorrar.Append(" and ");
				}

				switch(AStPK[i].StTipo)
				{
					case "DATETIME" :  //Campo fecha 
						//Formateamos la fecha DD/MM/YYYY 
						stSQLBorrar.Append("CONVERT(char(10)," + AStPK[i].StColumna + ",103)"); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							System.DateTime TempDate = DateTime.FromOADate(0);
							stSQLBorrar.Append("=" + "'" + ((DateTime.TryParse(AStPK[i].stTexto.Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : AStPK[i].stTexto.Trim()) + "'");
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
					case "VARCHAR" : case "CHAR" :  //Campo texto con o sin m�scara 
						stSQLBorrar.Append(AStPK[i].StColumna); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							stSQLBorrar.Append("=" + "'" + AStPK[i].stTexto.Trim() + "'");
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
					case "NUMERIC" : case "SMALLINT" : case "INT" :  //Campo num�rico 
						stSQLBorrar.Append(AStPK[i].StColumna); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							stSQLBorrar.Append("=" + AStPK[i].stTexto.Trim());
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
				}
			}
			return stSQLBorrar.ToString();
		}


		private void PintarEtiqueta()
		{
			//24/10/1997
			//Comprobar primero si existe espacio suficiente el el Frame

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				lbEtiqueta[0].Top = (int) ((ModuloMantenim.COiSupEtiqueta + viNuevoTop) / 15);
				lbEtiqueta[0].Left = (int) (ModuloMantenim.COiIzda / 15);
				lbEtiqueta[0].Visible = true;
				viNuevoLeft = Convert.ToInt32(((float) (lbEtiqueta[0].Left * 15)) + ((float) (lbEtiqueta[0].Width * 15))); //+ COiSeparacion
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["iadmitenull"]) == "N")
				{
                    
                    lbEtiqueta[0].Font = lbEtiqueta[0].Font.Change(bold:true);
				}
				else
				{
					lbEtiqueta[0].Font = lbEtiqueta[0].Font.Change(bold:false);
				}
			}
			else
			{
				lbEtiqueta[iIndice].Top = (int) ((ModuloMantenim.COiDiferenciaControl + viNuevoTop + ModuloMantenim.COiSeparacion) / 15);
				lbEtiqueta[iIndice].Left = (int) (ModuloMantenim.COiIzda / 15);
				lbEtiqueta[iIndice].Visible = true;
				viNuevoLeft = Convert.ToInt32(((float) (lbEtiqueta[iIndice].Left * 15)) + ((float) (lbEtiqueta[iIndice].Width * 15))); //+ COiSeparacion
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["iadmitenull"]) == "N")
				{
					lbEtiqueta[iIndice].Font = lbEtiqueta[iIndice].Font.Change(bold:true);
				}
				else
				{
					lbEtiqueta[iIndice].Font = lbEtiqueta[iIndice].Font.Change(bold:false);
				}

			}

		}

		private void RecorrerRegistros()
		{
			//Recorro el resulset de la consulta y lleno la spread
			double xx = 0;
			int iRecorrerArray = 0;
			object stTexto = null;

			viColumnaPassword = ComprobarCamposPassword();

			sprMultiple.Col = 0;

			int tempForVar = ModuloMantenim.RsManten.Tables[0].Rows.Count;
			for (iIndice = 1; iIndice <= tempForVar; iIndice++)
			{ //Recorrer hasta final del Resultset
				//Siguiente registro de la spread
				sprMultiple.MaxRows++;
				sprMultiple.Row = sprMultiple.MaxRows;
				for (iindice2 = 0; iindice2 <= sprMultiple.MaxCols - 1; iindice2++)
				{
					sprMultiple.Col = iindice2 + 1;

					//**************************************************************
					//Comprobamos si la columna es password
					if (sprMultiple.Col == viColumnaPassword)
					{
						sprMultiple.setTypeEditPassword(true);
					}

					if (aNumeroColumna.GetUpperBound(0) > iRecorrerArray)
					{
						iRecorrerArray++;
					}
					stTexto = ModuloMantenim.RsManten.Tables[0].Rows[0][iindice2];
					if (Convert.IsDBNull(stTexto))
					{
						sprMultiple.Text = "";
					}
					else
					{
						//****************************CRIS***********************************
						for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
						{
							//if (ModuloMantenim.RsManten.Tables[0].Rows[0][iindice2].getName().ToUpper() == astrElConjunto[i].gcolumncaman.ToUpper())
                            if (ModuloMantenim.RsManten.Tables[0].Columns[iindice2].ColumnName.ToUpper() == astrElConjunto[i].gcolumncaman.ToUpper())
							{
								//********************************CRIS********************************
								//Calculamos la longitud m�xima que va a tener el campo en la spread
								//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
								//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
								//posici�n para el punto decimal
								if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
								{
									if (astrElConjunto[i].nparteentera != 0)
									{
										sprMultiple.setTypeEditLen(astrElConjunto[i].nparteentera + astrElConjunto[i].npartedecima + 2);
									}
									else
									{
										sprMultiple.setTypeEditLen(astrElConjunto[i].nparteentera + astrElConjunto[i].npartedecima + 1);
									}
								}
								else
								{
									sprMultiple.setTypeEditLen(astrElConjunto[i].nparteentera + astrElConjunto[i].npartedecima);
								}
								//*******************************************************************
								//Miro si la columna es fecha para formatearla a DD/MM/YYYY
								//si no tiene formato
								if (ModuloMantenim.RsManten.Tables[0].Columns[iindice2].DataType == typeof(DateTime) && astrElConjunto[i].vformato.Trim() == "")
								{
									sprMultiple.Text = Convert.ToDateTime(stTexto).ToString("dd/MM/yyyy");
								}
								else
								{
									if (astrElConjunto[i].vformato != "")
									{
										sprMultiple.Text = StringsHelper.Format(stTexto, astrElConjunto[i].vformato);
									}
									else
									{
										if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
										{
											if (ModuloMantenim.RsManten.Tables[0].Columns[iindice2].DataType == typeof(decimal))
											{
												xx = Convert.ToDouble(stTexto);
											}
											else
											{
												xx = Conversion.Val(Convert.ToString(stTexto));
											}
											sprMultiple.Text = xx.ToString();
										}
										else
										{
											sprMultiple.Text = Convert.ToString(stTexto);
										}
									}
								}
								break;
							}
						}
						//*******************************************************************
					}
				}
				ModuloMantenim.RsManten.MoveNext();
				iRecorrerArray = 0;
			}
			//La consulta no se cierra, ya que se volver� a utilizar
			//en el ReQuery
		}





		private void NombrarEtiqueta()
		{
			//Se nombran las Etiquetas y se les da tama�o
			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				lbEtiqueta[0].Text = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim() + " :";
			}
			else
			{
				lbEtiqueta[iIndice].Text = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim() + " :";
			}

		}

		private void LlenarSpread(string StTabla, string stQuery = "", bool fTodo = false, bool fMaxFilas = false)
		{


			ModuloMantenim.DesactivarCampos(this, astrElConjunto, AStrToolbar);
			if (!fTodo)
			{
				sprMultiple.MaxRows = 0;
				cbSpread[3].Enabled = true;
				return;
			}
			//Reloj arena
			this.Cursor = Cursors.WaitCursor;
			sprMultiple.MaxRows = 0;
			//**************************************
			Requery(stQuery);
			//**************************************

			RecorrerRegistros();

			this.Cursor = Cursors.Default;

		}
		private void Requery(string stQuery)
		{
			//Hacemos el requery de nuestra consulta


			//Si es una nueva consulta, cierro la consulta anterior
			if (NuevaConsulta)
			{
				if (vfTodo)
				{
					ModuloMantenim.RsManten.Close();
					NuevaConsulta = false;
				}
				else
				{
					NuevaConsulta = false;
				}
			}

			Rq = ModuloMantenim.RcManten.CreateQuery("", stQuery);

			Rq.CommandText = stQuery;
            // ralopezn TODO_X_5 18/05/2016
            //Rq.setMaxRows(ModuloMantenim.vMaximoFilas);

            Rq.Parameters.Add("@paramStr");
			Rq.Parameters[0].Value = "";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(Rq);
			ModuloMantenim.RsManten = new DataSet();
            tempAdapter.Fill(ModuloMantenim.RsManten, 0, ModuloMantenim.vMaximoFilas, "Table1");

			if (ModuloMantenim.RsManten.Tables[0].Rows.Count == ModuloMantenim.vMaximoFilas)
			{
				CargarMas = true;
				Tope = ModuloMantenim.RsManten.Tables[0].Rows.Count - iNumeroFilasVisibles; //11
			}
			else if (ModuloMantenim.RsManten.Tables[0].Rows.Count < ModuloMantenim.vMaximoFilas)
			{ 
				CargarMas = false;
			}

			vfTodo = true;

			//Si no hay filas en la spread, deshabilitamos el bot�n de imprimir
			cbSpread[4].Enabled = sprMultiple.MaxRows != 0;
		}

		private void CrearSpread(string StTabla, string vstDescrLarTabla)
		{
			//Crea las columnas de la Spread
			Text = Text + " " + vstDescrLarTabla;
			//Vamos a crear el tama�o de las etiquetas
			Tama�oEtiqueta(StTabla);
			this.Cursor = Cursors.WaitCursor;

			ModuloMantenim.stSQLOrder = ModuloMantenim.ObtenerOrdenColumnas();

			ModuloMantenim.stConsulta = "";
			ModuloMantenim.stConsulta = "select * from SPRETBSPV1 ";
			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + "where GTABLAMANAUT='";
			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + StTabla + "'";
			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + ModuloMantenim.stSQLOrder;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(ModuloMantenim.stConsulta, ModuloMantenim.RcManten);
			ModuloMantenim.RsManten2 = new DataSet();
			tempAdapter.Fill(ModuloMantenim.RsManten2);

			iIndice = ModuloMantenim.RsManten2.Tables[0].Rows.Count;
			//Ampliamos las columnas de la Spread al total de las columnas
			sprMultiple.MaxCols = iIndice;
			sprMultiple.MaxRows = 1;
			//Los siguientes arrays siempre estar�n dimensionados a un elemento mas
			//que los campos de la tabla a mantener (MAXCOL), aunque el �ltimo elemento
			//no se va a tratar nunca ya que los arrays siempre se recorren de 0 a UBOUND - 1
			LaConexion = new ModuloMantenim.ConexionControles[iIndice + 1];
			astrElConjunto = new ModuloMantenim.strConjuntoResultados[iIndice + 1];


			if (ModuloMantenim.RsManten2.Tables[0].Rows.Count <= 0)
			{
				ModuloMantenim.RsManten2.Close();
				this.Cursor = Cursors.Default;
				return;
			}

			CargarEstrucResultados(ModuloMantenim.RsManten2);

			ModuloMantenim.RsManten2.MoveFirst();

			sprMultiple.Col = 1;
			sprMultiple.Row = 0;
			iIndice = 0;


			while(ModuloMantenim.RsManten2.Tables[0].Rows.Count != 0)
			{
				sprMultiple.SetColWidth(iIndice + 1, Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim().Length);
				if (sprMultiple.GetColWidth(iIndice + 1) <= 4)
				{
					sprMultiple.SetColWidth(iIndice + 1, 6);
					sprMultiple.Text = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim();
					LaConexion[iIndice].NumeroColumna = (short) sprMultiple.Col;
					aNumeroColumna = ArraysHelper.RedimPreserve(aNumeroColumna, new int[]{iIndice + 1});
					aLongitud = ArraysHelper.RedimPreserve(aLongitud, new int[]{iIndice + 1});
					aNumeroColumna[iIndice] = sprMultiple.Col;
					//********************************CRIS********************************
					//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
					//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
					//posici�n para el punto decimal
					if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
					{
						if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
						{
							aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 2);
						}
						else
						{
							aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 1);
						}
					}
					else
					{
						aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
					}
					//********************************************************************
					viColumnaPassword = ComprobarCamposPassword();
					if (sprMultiple.Col == viColumnaPassword)
					{
						sprMultiple.setTypeEditPassword(true);
					}

					CrearEtiqueta();
					NombrarEtiqueta(); //Se nombran y se crean los tama�os
					PintarEtiqueta();
					//Nombre de la tabla que estamos pintando
					LaConexion[iIndice].NombreTabla = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtablamanaut"]).Trim();
					//Nombre del campo de la tabla
					LaConexion[iIndice].NombreCampo = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim();
					CrearControles();
					PintarControles();
					ModuloMantenim.RsManten2.MoveNext();
					iIndice++;
					sprMultiple.Col++;
				}
				else
				{
					sprMultiple.Text = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim();
					LaConexion[iIndice].NumeroColumna = (short) sprMultiple.Col;
					CrearEtiqueta();
					NombrarEtiqueta(); //Se nombran y se crean los tama�os
					PintarEtiqueta();
					//Nombre de la tabla que estamos pintando
					LaConexion[iIndice].NombreTabla = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtablamanaut"]).Trim();
					//Nombre del campo de la tabla
					LaConexion[iIndice].NombreCampo = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim();
					aNumeroColumna = ArraysHelper.RedimPreserve(aNumeroColumna, new int[]{iIndice + 1});
					aLongitud = ArraysHelper.RedimPreserve(aLongitud, new int[]{iIndice + 1});
					aNumeroColumna[iIndice] = sprMultiple.Col;
					//aLongitud(iIndice) = RsManten2("nparteentera")
					//********************************CRIS********************************
					//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
					//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
					//posici�n para el punto decimal
					if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
					{
						if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
						{
							aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 2);
						}
						else
						{
							aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 1);
						}
					}
					else
					{
						aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
					}
					//********************************************************************
					//aLongitud(iIndice) = RsManten2("nparteentera")
					viColumnaPassword = ComprobarCamposPassword();
					if (sprMultiple.Col == viColumnaPassword)
					{
						sprMultiple.setTypeEditPassword(true);
					}

					CrearControles();
					PintarControles();
					ModuloMantenim.RsManten2.MoveNext();
					iIndice++;
					sprMultiple.Col++;
				}
			};

			ModuloMantenim.RsManten2.Close();

			this.Cursor = Cursors.Default;

		}

		private void CrearControles()
		{
			//Rutina para comprobar el tipo de dato y as�
			//crear el tipo de control que necesitamos




			switch(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim())
			{
				case "DATETIME" :  //Campo fecha 
					 
					CrearCampoFecha(); 
					 
					break;
				case "VARCHAR" : case "CHAR" :  //Campo texto con o sin m�scara 
					 
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) > 2 * ModuloMantenim.COiMaxTamLineaMultiline)
					{
						CrearCampoMultilinea(3);
					}
					else if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) > ModuloMantenim.COiMaxTamText)
					{ 
						CrearCampoMultilinea(2);
					}
					else
					{
						if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "N" && Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["icolformato"]) == "N" && Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["icolmascara"]) == "N")
						{
							CrearCampoMascara();
						}
						else
						{
							//S�lo se creer� campo texto no multilinea
							//cuando sea con password, formato o m�scara
							CrearCampoTexto();
						}
					} 
					 
					break;
				case "NUMERIC" : case "SMALLINT" : case "INT" :  //Campo num�rico 
					 
					CrearCampoMascara(); 
					 
					break;
			}

		}

		private void PintarControles()
		{
			//Pintar los controles en el Frame de detalle

			switch(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim())
			{
				case "DATETIME" :  //Campo fecha 
					PintarCampoFecha(); 
					 
					break;
				case "VARCHAR" : case "CHAR" :  //Campo texto con o sin m�scara 
					//Si el campo es muy largo se crea un multilinea directamente 
					//No se comprueba si tiene m�scara porque no es l�gico. 
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) > 2 * ModuloMantenim.COiMaxTamLineaMultiline)
					{
						PintarCampoMultilinea(3);
					}
					else if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) > ModuloMantenim.COiMaxTamText)
					{ 
						PintarCampoMultilinea(2);
					}
					else
					{
						//Si la columna "ipassword" nos dice que el campo no llevar� protecci�n con
						//caracteres para password
						if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "N" && Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["icolformato"]) == "N" && Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["icolmascara"]) == "N")
						{
							PintarCampoMascara();

						}
						else
						{
							//Si el Control debe llevar la propiedad password con un car�cter de protecci�n
							PintarCampoTexto();
						}

					} 
					 
					break;
				case "NUMERIC" : case "SMALLINT" : case "INT" :  //Campo num�rico 
					 
					PintarCampoMascara(); 
					 
					break;
			}

		}

		private void Tama�oEtiqueta(string StTabla)
		{

			string consulta = "select MAX(DATALENGTH(RTRIM(dcoltabmanco))) from SPRETBSPV1 " + "where gtablamanaut=" + "'" + StTabla.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(consulta, ModuloMantenim.RcManten);
			ModuloMantenim.RsManten2 = new DataSet();
			tempAdapter.Fill(ModuloMantenim.RsManten2);

			int iTama�oEtiqueta = Convert.ToInt32(Double.Parse(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0][0]).Trim()) * ModuloMantenim.COiTamCaractEtiqueta);
			lbEtiqueta[0].Width = (int) (iTama�oEtiqueta / 15);
			ModuloMantenim.RsManten2.Close();

		}



		private void ValidarFormato(int IAscii, string stCampo, string stTipoDato)
		{
			//Rutina para dejar s�lo introducir los caracteres de formato
			string stLetra = String.Empty;
			string StFormato = String.Empty;

			FFormatoValido = false;
			switch(stTipoDato)
			{
				case "CHAR" : case "VARCHAR" : 
					StFormato = StFormatoCadena + StFormatoFechaHora; 
					for (int i = 1; i <= StFormato.Length; i++)
					{
						stLetra = StFormato.Substring(i - 1, Math.Min(1, StFormato.Length - (i - 1)));
						if (IAscii == Strings.Asc(stLetra[0]) || IAscii == Strings.Asc(stLetra.ToUpper()[0]))
						{
							FFormatoValido = true;
							break;
						}
					} 
					 
					break;
				case "NUMERIC" : case "SMALLINT" : case "INT" : 
					StFormato = StFormatoNumero + StFormatoFechaHora; 
					for (int i = 1; i <= StFormato.Length; i++)
					{
						stLetra = StFormato.Substring(i - 1, Math.Min(1, StFormato.Length - (i - 1)));
						if (IAscii == Strings.Asc(stLetra[0]) || IAscii == Strings.Asc(stLetra.ToUpper()[0]))
						{
							FFormatoValido = true;
							break;
						}
					} 
					 
					break;
			}


			if (!FFormatoValido)
			{
				//Muestra al usuario los formatos v�lidos
				short tempRefParam = 1750;
                string[] tempRefParam2 = new string[]{"el valor de " + stCampo.Trim(), StFormato};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
			}
		}

		private void ValidarMascara(int IAscii, string stCampo, string stTipoDato)
		{
			//Rutina para dejar s�lo introducir los caracteres de m�scara
			string stLetra = String.Empty;
			string StMascara = String.Empty;

			FMascaraValida = false;
			switch(stTipoDato)
			{
				case "CHAR" : case "VARCHAR" : 
					StMascara = StMascaraNumero + StMascaraFechaHora; 
					for (int i = 1; i <= StMascara.Length; i++)
					{
						stLetra = StMascara.Substring(i - 1, Math.Min(1, StMascara.Length - (i - 1)));
						if (IAscii == Strings.Asc(stLetra[0]))
						{
							FMascaraValida = true;
							break;
						}
					} 
					break;
				case "NUMERIC" : case "SMALLINT" : case "INT" : 
					StMascara = StMascaraCadena + StMascaraFechaHora; 
					for (int i = 1; i <= StMascara.Length; i++)
					{
						stLetra = StMascara.Substring(i - 1, Math.Min(1, StMascara.Length - (i - 1)));
						if (IAscii == Strings.Asc(stLetra[0]))
						{
							FMascaraValida = true;
							break;
						}
					} 
					break;
			}
			if (!FMascaraValida)
			{
				//Muestra al usuario las m�scaras v�lidas
				short tempRefParam = 1750;
                string[] tempRefParam2 = new string[]{"el valor de " + stCampo.Trim(), StMascara};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
			}
		}

		private bool ValidarPrecision()
		{
			//Debemos buscar en la estructura el control
			//y comprobar si la precision de la parte entera
			//es la correcta
			Control MiControl = null;
			double varEntera = 0;
			bool fPrecision = false;

			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{

				fPrecision = true;

				if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
				{

					MiControl = astrElConjunto[i].NombreControl;

					if (MiControl.Name == "mebMascara")
					{
						varEntera = Math.Abs(Math.Floor(Conversion.Val(Convert.ToString(MiControl.FormattedText()))));
					}
					else
					{
						varEntera = Math.Abs(Math.Floor(Convert.ToDouble(MiControl.Text)));
					}


					switch(astrElConjunto[i].gtipodato)
					{
						case "SMALLINT" : 
							if (Math.Sign(Conversion.Val(Convert.ToString(MiControl.Text))) >= 0)
							{
								if (varEntera > Conversion.Val(ModuloMantenim.CoIntegerPosi) || Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
								{
									fPrecision = false;
								}
							}
							else
							{
								if (varEntera > Conversion.Val(ModuloMantenim.CoIntegerNega) || Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
								{
									fPrecision = false;
								}
							} 
							break;
						case "INT" : 
							if (Math.Sign(Conversion.Val(Convert.ToString(MiControl.Text))) >= 0)
							{
								if (varEntera > Conversion.Val(ModuloMantenim.CoLongPosi) || Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
								{
									fPrecision = false;
								}
							}
							else
							{
								if (varEntera > Conversion.Val(ModuloMantenim.CoLongNega) || Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
								{
									fPrecision = false;
								}
							} 
							break;
						case "NUMERIC" : 
							if (Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
							{
								fPrecision = false;
							} 
							break;
					}

					if (!fPrecision)
					{
						//MENSAJE:V1 excede la Precisi�n definida.
						short tempRefParam = 1730;
                        string[] tempRefParam2 = new string[]{astrElConjunto[i].dcoltabmanco};
						ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
						MiControl.Focus();
						MiControl = null;
						return false;
					}

					MiControl = null;

				}
			}
			return true;
		}



		private void VisibleComboBox(bool fVisible)
		{
			//*************************************************
			return;
			//*************************************************
			if (fVisible)
			{
				int tempForVar = Cbb_AlgoNada.GetUpperBound(0);
				for (int i = 1; i <= tempForVar; i++)
				{
					Cbb_AlgoNada[i].Visible = true;
				}
			}
			else
			{
				int tempForVar2 = Cbb_AlgoNada.GetUpperBound(0);
				for (int i = 1; i <= tempForVar2; i++)
				{
					Cbb_AlgoNada[i].Visible = false;
				}
				//Tengo que refrescar el frame porque no desaparecen
				//los combos
				frmDetalle.Refresh();
			}
		}


		private void cbbCombo_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cbbCombo, eventSender);
			if (ActiveControl == cbbCombo[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}
		}


		private void cbBotones_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cbBotones, eventSender);
			int lCantidad = 0;
			int iRespuesta = 0;
			int i = 0;
			DialogResult StRes = (DialogResult) 0;
			DataSet RsCount = null;
			bool ErrorValidacion = false;
			vfCambios = false;
			string consulta = String.Empty;

			if (Index == 0)
			{ //Aceptar


				switch(stPulsado)
				{
					case "incorporar" : 
						//Si no hemos buscado anteriormente los valores 
						//�nicos, los buscamos 
						if (!fUnicos)
						{
							BuscarUnicos();
						} 
						BuscarPK(true); 
						ErrorValidacion = false; 
						if (ValidarRequerido())
						{
							if (ValidarPrecision())
							{
								if (ValidarValoresUnicos())
								{
									if (ValidarPKUnicos())
									{
										if (!IncorporarDB())
										{
											return;
										}
										else
										{
											cbBotones[0].Enabled = true;
											cbBotones[1].Enabled = true;
											Descaracterizar();
										}
									}
									else
									{
										ErrorValidacion = true;
									}
								}
								else
								{
									ErrorValidacion = true;
								}
							}
							else
							{
								ErrorValidacion = true;
							}
						}
						else
						{
							ErrorValidacion = true;
						} 
						if (ErrorValidacion)
						{
							cbBotones[0].Enabled = true;
							cbBotones[1].Enabled = true;
							cbBotones[2].Enabled = true;
							this.Cursor = Cursors.Default;
							return;
						} 
						LimpiarCampos(); 
						break;
					case "modificar" : 
						if (fActivarRegistro)
						{
							fActivarRegistro = false;
							StRes = RadMessageBox.Show("Si Fecha de Borrado no tiene valor, el c�digo volver� a estar activo." + "\r" + "�Desea continuar?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question);
							if (StRes == System.Windows.Forms.DialogResult.No)
							{
								Descaracterizar();
								cbSpread[0].Enabled = true;
								cbSpread[3].Enabled = true;
								cbSpread[4].Enabled = true;
								cbBotones[0].Enabled = false;
								cbBotones[1].Enabled = false;
								cbBotones[2].Enabled = false;
								VisibleComboBox(false);
								frmConsulta.Visible = false;
								vfCambiosPassword = false;
								rbConsulta[1].IsChecked = true;

								ModuloMantenim.DesactivarCampos(this, astrElConjunto, AStrToolbar);

								sprMultiple.Enabled = true;
                                sprMultiple.ReadOnly = true;
								this.Text = stCaptionForm;
								//Tengo que activar los frames para
								//poder limpiar los campos
								//*********************************
								frmDetalleFondo.Enabled = true;
								//*********************************
								LimpiarCampos();
								//Inhabilito los frames para dejarlos
								//como estaban
								//************************************
								frmDetalleFondo.Enabled = false;
								//************************************
								return;
							}
						} 
						//Si no hemos buscado anteriormente los valores 
						//�nicos, los buscamos 
						if (!fUnicos)
						{
							BuscarUnicos();
						} 
						ErrorValidacion = false; 
						if (ValidarRequerido())
						{
							if (ValidarPrecision())
							{
								if (ValidarValoresUnicos(true))
								{
									if (!ModificarDB())
									{
										return;
									}
									else
									{
										Descaracterizar();
									}
								}
								else
								{
									ErrorValidacion = true;
								}
							}
							else
							{
								ErrorValidacion = true;
							}
						}
						else
						{
							ErrorValidacion = true;
						} 
						if (ErrorValidacion)
						{
							cbBotones[0].Enabled = true;
							cbBotones[1].Enabled = true;
							cbBotones[2].Enabled = true;
							vfCambiosPassword = false;
							this.Cursor = Cursors.Default;
							return;
						} 
						 
						LimpiarCampos(); 
						break;
					case "consultar" : 
						 
						 
						//Recorrer todos los campos para ver si no est�n 
						//vac�os o tienen datos;comprobar campos requeridos en la 
						//consulta y, si hay, verificar que est�n introducidos 
						//y montar la Select, si no, enviar mensaje requiri�ndolos. 
						if (ValidarRequeridoConsulta())
						{
							//*********CRIS****************
							NuevaConsulta = true;
							//*****************************
							//Ver si los campos est�n vacios o con
							//datos y incluirlos para la consulta
							ComprobarCamposConsulta();
							//montar la consulta
							consulta = MontarConsulta(vstNombreTabla, "Select count(*) ", false);
							//Call HacerSelect(vstNombreTabla, consulta)
							//Dar mensaje de cantidad de registros
							SqlDataAdapter tempAdapter = new SqlDataAdapter(consulta, ModuloMantenim.RcManten);
							RsCount = new DataSet();
							tempAdapter.Fill(RsCount);
							//Call Requery(consulta)
							lCantidad = Convert.ToInt32(RsCount.Tables[0].Rows[0][0]);
							RsCount.Close();
							//MENSAJE:Se han encontrado V1 elementos.
							//''''''''�Desea continuar?.
							short tempRefParam = 1110;
							string[] tempRefParam2 = new string[]{lCantidad.ToString()};
							iRespuesta = Convert.ToInt32(ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2));

							if (iRespuesta == 6)
							{ //"S�"
								vfCerrar = true;
								consulta = MontarConsulta(vstNombreTabla, "Select * ", true);
								//                        'Para quitar el �ltimo OR
								//                        consulta = Mid(consulta, 1, Len(consulta) - 4)
								HacerSelect(vstNombreTabla, consulta, false);
								//                        'Obtener el Order by de la tabla
								//                        stSQLOrder = ObtenerOrdenFilas(rcmanten, vstNombreTabla)
								//consulta = consulta & stSQLOrder
								LlenarSpread(vstNombreTabla, ModuloMantenim.stConsulta, true, true);
								cbBotones[0].Enabled = true;
								cbBotones[1].Enabled = true;
								Descaracterizar();
							}
							else
							{
								//********************
								NuevaConsulta = false;
								//********************
								return;
							}

						}
						else
						{
							//Si la respuesta es No

							DesactivarBotonesSpread();
							cbBotones[0].Enabled = true;
							cbBotones[1].Enabled = true;
							Descaracterizar();
							return;
						} 
						break;
				}
				cbSpread[0].Enabled = true;
				cbSpread[3].Enabled = true;
				cbSpread[4].Enabled = true;
				cbSpread[5].Enabled = true;
				cbSpread[6].Enabled = true;
				cbBotones[0].Enabled = false;
				cbBotones[1].Enabled = false;
				cbBotones[2].Enabled = false;

			}
			else if (Index == 1)
			{  //Cancelar

				switch(stPulsado)
				{
					case "incorporar" : 
						 
						LimpiarCampos(); 
						cbBotones[2].Enabled = false;
                        this.AcceptButton = null;

                        vfCambios = false; 
						vfCambiosPassword = false; 
						 
						break;
					case "modificar" : case "consultar" : 
						 
						Descaracterizar(); 
						LimpiarCampos(); 
						cbBotones[2].Enabled = false;
                        //VB6.SetDefault(cbBotones[0], false);
                        this.AcceptButton = null;
                        vfCambios = false; 
						vfCambiosPassword = false; 
						 
						break;
				}
				//habilitar los botones correspondientes

				cbSpread[0].Enabled = true;
				cbSpread[3].Enabled = true;
				cbSpread[4].Enabled = true;
				cbBotones[0].Enabled = false;
				cbBotones[1].Enabled = false;
				cbBotones[2].Enabled = false;
			}
			else if (Index == 2)
			{  //Limpiar

				LimpiarCampos();
				cbSpread[1].Enabled = false;
				cbSpread[2].Enabled = false;
				return;

			}

			vfCambiosPassword = false;
			rbConsulta[1].IsChecked = true;
			VisibleComboBox(false);
			frmConsulta.Visible = false;

			ModuloMantenim.DesactivarCampos(this, astrElConjunto, AStrToolbar);

			sprMultiple.Enabled = true;
			sprMultiple.ReadOnly = true;
			this.Text = stCaptionForm;

			//Si no hay filas en la spread, deshabilitamos el bot�n de imprimir
			cbSpread[4].Enabled = sprMultiple.MaxRows != 0;
		}

		private void cbSpread_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cbSpread, eventSender);
			DialogResult iRespuesta = (DialogResult) 0;
			int iIndiceModificar = 0;
			string stColumnas = String.Empty;
			int iEspacio = 0;
			string StComando = String.Empty;
			DataSet RsModificar = null;

			//****************************CRIS*************************
			//Antes de cambiar el modo hay que descaracterizar el detalle
			Descaracterizar();
			//*********************************************************
			cbBotones[2].Enabled = true;

			//********************************************************

			switch(Index)
			{
				case 0 :  //Incorporar 

					 
					this.Text = this.Text + " (Incorporar)"; 
					stPulsado = "incorporar"; 
					ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar); 
					LimpiarCampos(); 
					//Todos los botones de la Spread se han de inhabilitar 
					DesactivarBotonesSpread(); 
					//y habilitar los de Aceptar y cancelar con los campos. 
					cbBotones[0].Enabled = true; 
					cbBotones[1].Enabled = true; 
					ModuloMantenim.StFBORRADO = ObtenerColumnaFBorrado(); 
					CaracterizarIncorporar();  //Caracter�sticas de los campos 
                                               //VB6.SetDefault(cbBotones[0], true); 
                    this.AcceptButton = cbBotones[0];
                    vfCambios = false; 
					vfCambiosPassword = false; 
					ObtenerFoco(); 
					cbBotones[2].Enabled = false; 
					//******************************************************** 
					 
					break;
				case 1 :  //Eliminar 
					this.Text = this.Text + " (Eliminar)"; 
					//Preguntar si est� seguro y si la respuesta es s� 
					//hacer las validaciones correspondientes y si todo es correcto 
					//ELIMINAR. 
					 
					DesactivarBotonesSpread(); 
					//Enviamos un mensaje pidiendo confirmaci�n 
					short tempRefParam = 1050; 
					iRespuesta = ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, ""); 
					 
					//************************ 
					 
					if (iRespuesta == System.Windows.Forms.DialogResult.OK)
					{ //Aceptar

						//Borrar el registro activo
						//y quitarlo de la Spread
						ModuloMantenim.BorrarDB(this);

						this.Text = stCaptionForm;
						cbSpread[0].Enabled = true;
						cbSpread[3].Enabled = true;
						ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar);
						if (sprMultiple.MaxRows > 0)
						{
							cbSpread[4].Enabled = true;
						}
						//Tengo que activar los frames para
						//poder limpiar los campos
						//*********************************
						frmDetalleFondo.Enabled = true;
						//*********************************
						LimpiarCampos();
						sprMultiple.Enabled = true;
                        sprMultiple.ReadOnly = true;
						vfCambios = false;
						vfCambiosPassword = false;
						cbSpread[3].Focus();
						//Inhabilito los frames para dejarlos
						//como estaban
						//************************************
						frmDetalleFondo.Enabled = false;
						//************************************
						return;

					}
					else
					{
						//Cancelar Eliminar

						//No eliminar�a nada
						//Volver a la situaci�n anterior de botones
						this.Text = stCaptionForm;
						cbSpread[0].Enabled = true;
						cbSpread[3].Enabled = true;
						if (sprMultiple.MaxRows > 0)
						{
							cbSpread[4].Enabled = true;
						}
						//Tengo que activar los frames para
						//poder limpiar los campos
						//*********************************
						frmDetalleFondo.Enabled = true;
						//*********************************
						LimpiarCampos();
						sprMultiple.Enabled = true;
                        sprMultiple.ReadOnly = true;
						cbBotones[2].Enabled = false;
						vfCambios = false;
						vfCambiosPassword = false;
						cbSpread[3].Focus();
						//Inhabilito los frames para dejarlos
						//como estaban
						//************************************
						frmDetalleFondo.Enabled = false;
						//************************************
						return;
					} 
					 
					//******************************************************** 

				case 2 :  //Modificar 
					 
					this.Text = this.Text + " (Modificar)"; 
					stPulsado = "modificar"; 
					 
					iIndiceModificar = 0; 
					 
					//*************************CRIS************************** 
					BuscarPK(); 
					//******************************************************* 
					ModuloMantenim.stSQLManten = MontarSelectConsulta();  //Para modificar 
					 
					if (!ModuloMantenim.PermitirBorrar(ModuloMantenim.stSQLManten))
					{
						//Borrar de la spread el registro eliminado
                        // ralopezn TODO_X_5 19/05/2016
						//this.sprMultiple.Action = 5; //Borra de la Spread el registro actual
						this.sprMultiple.MaxRows--;
						return;
					} 
					//**************** 
					ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar); 
					DesactivarBotonesSpread(); 
					//Saber qu� columna hay que modificar y presentar en los campos 
					//''''''sprMultiple.Row = sprMultiple.ActiveRow (en el Click) 
					 
					LlenarCampos(); 
					 
					//habilitar los botones de Aceptar y Cancelar e 
					//inhabilitar Limpiar. 
					cbBotones[0].Enabled = true; 
					cbBotones[1].Enabled = true;
                    this.AcceptButton = cbBotones[0];

                    if (ModuloMantenim.TablaBorradoLogico(vstNombreTabla, this))
					{
						if (!FBorradoNula())
						{
							RadMessageBox.Show("S�lo podr� modificar la columna de Fecha de Borrado a NULA " + "\r" + "ya que el registro ha sido borrado l�gicamente", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
							fSoloNULL = true;
							InhabilitarControles();
						}
						else
						{
							CaracterizarModificar(true); //Caracter�sticas de los campos
						}
					}
					else
					{
						CaracterizarModificar(); //Caracter�sticas de los campos
					} 
                    this.AcceptButton = cbBotones[0];
                    vfCambios = false; 
					vfCambiosPassword = false; 
					cbBotones[2].Enabled = false; 
					ObtenerFoco(); 
					 
					//********************************************************* 
					 
					break;
				case 3 :  //Consultar 
					 
					//        '*************************CRIS********************** 
					//        'Quito la m�scara de todos los controles 
					// 
					//        For i = 0 To mebMascara.UBound 
					//            mebMascara(i).Mask = "" 
					//        Next iIndiceLimpiar 
					//        '**************************************************** 
					this.Text = this.Text + " (Consultar)"; 
					stPulsado = "consultar"; 
					 
					//Si est�n inhabilitados los botones de eliminar 
					//y modificar, significa que el �ltimo bot�n pulsado 
					//ha sido el consultar. Si esto ocurre, no limpio los 
					//campos para que tenga la consulta anterior 
					if (cbSpread[1].Enabled || cbSpread[2].Enabled)
					{
						LimpiarCampos();
					} 
					DesactivarBotonesSpread(); 
					//**************CRIS*************** 
					VisibleComboBox(true); 
					//********************************* 
					frmConsulta.Visible = true; 
					 
					cbBotones[0].Enabled = true; 
					cbBotones[1].Enabled = true; 
					ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar); 
					 
                    this.AcceptButton = cbBotones[0];
                    cbBotones[2].Enabled = true; 
					ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar); 
					cbBotones[0].Focus(); 
					ObtenerFoco(); 
					 
					//******************************************************** 
					 
					break;
				case 4 :  //Imprimir 
					if (sprMultiple.MaxRows >= ModuloMantenim.vMaximoFilas)
					{
						//If vlCantidad >= vMaximoFilas Then
						if (CargarMas)
						{ //tiene que cargar mas
							short tempRefParam3 = 1660;
							string[] tempRefParam4 = new string[]{vlCantidad.ToString()};
							iRespuesta = (DialogResult) Convert.ToInt32(ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, ModuloMantenim.RcManten, tempRefParam4));
						}
						else
						{
							short tempRefParam5 = 1660;
							string[] tempRefParam6 = new string[]{sprMultiple.MaxRows.ToString()};
							iRespuesta = (DialogResult) Convert.ToInt32(ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, ModuloMantenim.RcManten, tempRefParam6)); //vlCantidad
						}
						if (iRespuesta == System.Windows.Forms.DialogResult.Yes)
						{
							//***************************
							while (CargarMas)
							{
								sprMultiple.Row = sprMultiple.MaxRows;
								sprMultiple.Col = sprMultiple.ActiveColumnIndex;
								sprMultiple.Action = 0;
							}
							//***************************
							DesactivarBotonesSpread();
							ModuloMantenim.CrearDBTemporal();
							ModuloMantenim.CrearTablaTemporal();
							ImprimirListado();
							cbSpread[0].Enabled = true;
							cbSpread[3].Enabled = true;
							cbSpread[4].Enabled = true;
							vfCambios = false;
							vfCambiosPassword = false;
						}
					}
					else
					{
						DesactivarBotonesSpread();
						ModuloMantenim.CrearDBTemporal();
						ModuloMantenim.CrearTablaTemporal();
						ImprimirListado();
						cbSpread[0].Enabled = true;
						cbSpread[3].Enabled = true;
						cbSpread[4].Enabled = true;
						vfCambios = false;
						vfCambiosPassword = false;
					} 
					//Tengo que activar los frames para 
					//poder limpiar los campos 
					//********************************* 
					frmDetalleFondo.Enabled = true; 
					//********************************* 
					LimpiarCampos(); 
					//Inhabilito los frames para dejarlos 
					//como estaban 
					//************************************ 
					frmDetalleFondo.Enabled = false; 
					//************************************ 
					//****************************************************************** 
					break;
				case 5 :  //Aceptar selecci�n 
					//Este bot�n y el siguiente solo aparecer�n en caso de que se halla 
					//llamado a este Active X desde un combo 
					if (sprMultiple.MaxRows == 0)
					{
						return;
					} 
					this.Cursor = Cursors.WaitCursor; 
					DevolverDatos(); 
					FrInstanciaFormulario.RecogerDatosMantenimiento(AStClaves, AStColumnasFK, AStTipoCol, viIndiceComboInhabilitado); 
					this.Close(); 
					this.Cursor = Cursors.Default; 
					return;
				case 6 :  //Cancelar selecci�n 
					this.Close(); 
					return;
				case 7 :  //Salir 
					this.Close(); 
					return;
				case 8 : 
					//        MsgBox "Presenta la ayuda que no esta disponible" 
					// ralopezn TODO_X_5 18/05/2016
                    //CDAyuda.setHelpContext(this.getHelpContextID()); 
					//CDAyuda.setHelpCommand(0x1); 
					//CDAyuda.ShowHelp(); 
					//****************************************************************** 
					break;
			}
            sprMultiple.ReadOnly = true;
			if (Index == 4)
			{
				return;
			}
			//sprMultiple.Enabled = False

			//Si no hay filas en la spread, deshabilitamos el bot�n de imprimir
			cbSpread[4].Enabled = sprMultiple.MaxRows != 0;
		}

		private void SMT100F1_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				this.WindowState = FormWindowState.Maximized;
				if (sprMultiple.MaxRows != 0)
				{
					this.cbSpread[3].Enabled = true;
					this.cbSpread[4].Enabled = true;
					this.cbSpread[3].Focus();
				}
				stCaptionForm = this.Text;

			}
		}

		private void SMT100F1_Load(Object eventSender, EventArgs eventArgs)
		{
			int NewLargeChange = 0;

			string sql = String.Empty;
			string lTotal = String.Empty;
			string iRespuesta = String.Empty;

			this.Top = (int) 0;
			this.Left = (int) 0;
			AlgoNadaVisible = true;
			// ralopezn TODO_X_5 18/05/2016
			//this.setHelpContextID(vlAyuda);

			proRecolocaControles();

			int RowHeightinTwips = 0;
			sprMultiple.RowHeightToTwips(0, (float) sprMultiple.GetRowHeight(0), RowHeightinTwips);
			iNumeroFilasVisibles = Convert.ToInt32(Conversion.Val(((((float) (sprMultiple.Height * 15)) / RowHeightinTwips) - 3).ToString()));

			//MsgBox iNumeroFilasVisibles
			fControlActivo = false;

			iIndiceOption = 1;

			//Primero comprobamos si s�lo puede hacer consultas
			//o puede hacer TODO.
			//Si viene de haberse llamado desde un combo,
			//entonces el modo es consulta
			if (fVieneDeCombos)
			{
				cbSpread[0].Visible = false;
				cbSpread[1].Visible = false;
				cbSpread[2].Visible = false;

				cbSpread[5].Visible = true;
				cbSpread[6].Visible = true;

			}
			else
			{
				ComprobarModoTotal();
			}
			//Inicializar la variable de consulta; por defecto 'AND'
			vstCriterioConsulta = " AND ";
			//Reloj de arena
			this.Cursor = Cursors.WaitCursor;
			//Incluir una Formula en Crystal para dar al listado la descripci�n
			//de la tabla de la que crearemos el informe.
			string stListado = "Listado de " + vstDescrLarTabla;
			//ModuloMantenim.cryCrystal.Formulas[3] = "Tabla= \"" + stListado + "\"";
            ModuloMantenim.cryCrystal.Formulas["Tabla"] = "\"" + stListado + "\"";

            //**************************CRIS**********************
            frmDetalle.Height = (int) (ModuloMantenim.COiSuperiorCampo / 15);
			//***************************************************

			//Crea las cabeceras de la Spread y los controles del detalle.
			CrearSpread(vstNombreTabla, vstDescrLarTabla);

			//*****************CRIS***************************
			NuevaConsulta = false;
			//Obtener el Order by de la tabla
			ModuloMantenim.stSQLOrder = ObtenerOrdenFilas(ModuloMantenim.RcManten, vstNombreTabla);
			BuscarPK();
			HacerSelect(vstNombreTabla, "Select * from " + vstNombreTabla + " where ", true);
			//************************************************

			LlenarSpread(vstNombreTabla, ModuloMantenim.stConsulta, vfTodo);

			//Ahora tenemos que comprobar si han cogido en los campos
			//o si tenemos que usar el scroll
			if (viNuevoTop > ((float) (frmDetalle.Height * 15)))
			{
				scbvCampos.Enabled = true;
				scbvCampos.Minimum = 255;
				scbvCampos.Maximum = (Convert.ToInt32((viNuevoTop - ((float) (frmDetalle.Height * 15))) - 255) + scbvCampos.LargeChange - 1);
				NewLargeChange = Convert.ToInt32(viNuevoTop - ((float) (frmDetalle.Height * 15)));
				scbvCampos.Maximum = scbvCampos.Maximum + NewLargeChange - scbvCampos.LargeChange;
				scbvCampos.LargeChange = NewLargeChange;
				scbvCampos.SmallChange = Convert.ToInt32((Convert.ToInt32(viNuevoTop - ((float) (frmDetalle.Height * 15)))) / 10);
			}
			//*********************************************************************
			cbBotones[2].Enabled = false;
			this.Cursor = Cursors.Default;
			vfCambios = false;
			vfCambiosPassword = false;

			//*************************CRIS*******************************
			//Esta variable (iDiferencia) me sirve luego para poder controlar los
			//l�mites de la ScrollBar
			iDiferencia = Convert.ToInt32(((float) (frmDetalle.Height * 15)) - ((float) (frmDetalleFondo.Height * 15)));

			//Lleno las variables de caracteres v�lidos de formato y m�scara
			//Caracteres obtenidos seg�n la funci�n Format y propiedad
			//Mask de VB
			StFormatoNumero = "#0%e,.-+()$\\\"";
			StFormatoCadena = "@&<>!";
			StFormatoFechaHora = "acdhmnpqstwy/:";
			StMascaraNumero = "#.,\\&9C";
			StMascaraCadena = "\\&<>AaC?";
			StMascaraFechaHora = ":\\/&9C";
			//***********************************************************

			//****************************************CRIS************************
			//Limpiamos los controles despues de crearlos,pintarlos y asignar todas
			//sus caracter�sticas porque, al ser arrays de controles, puede que se
			//hereden algunas propiedades de unos controles a otros
			LimpiarCampos();
			//********************************************************************
			//Obtengo el n� de clicks que necesita la scroll para
			//que se vean todos los campos.
			//Esto lo saco restando el espacio total de los campos menos el espacio
			//de los campos que se ven, y dividiendo esto entre 150, ya que
			//este es el desplazamiento que se hace en cada click
			lClicks = Convert.ToInt32((((float) (frmDetalle.Height * 15)) - ((float) (FrmDetalleMedio.Height * 15))) / 150);
			//Si la variable lClick es <= que cero, significa que no necesito
			//la scroll, ya que todos los campos caben en el espacio disponible
			//para verlos
			if (lClicks <= 0)
			{
				//oculto la scroll
				scbvCampos.Visible = false;
			}
			else
			{
				//Si necesito la scroll, pongo el salto de la scroll, y esto lo hallo
				//dividiendo el valor m�ximo entre el n� de clicks
				scbvCampos.SmallChange = (scbvCampos.Maximum - (scbvCampos.LargeChange + 1)) / ((int) lClicks);
				NewLargeChange = scbvCampos.SmallChange;
				scbvCampos.Maximum = scbvCampos.Maximum + NewLargeChange - scbvCampos.LargeChange;
				scbvCampos.LargeChange = NewLargeChange;
			}
			CargarComboAlgoNada();

			fControlActivo = true;
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
		}
		private void SMT100F1_Closed(Object eventSender, CancelEventArgs eventArgs)
		{
			int iRespuesta = 0;

			if (vfCambios)
			{
				//MENSAJE:Antes de V1, �Desea guardar los cambios efectuados en V2?
				short tempRefParam = 1510;
				string[] tempRefParam2 = new string[]{"cerrar", vstDescrLarTabla};
				iRespuesta = Convert.ToInt32(ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2)); //1070

				//Comprobamos el valor del bot�n pulsado en el mensaje

				switch(iRespuesta)
				{
					case 2 :  //Cancelar 
						eventArgs.Cancel = true; 
						//Exit Sub 
						 
						break;
					case 6 :  //Si 
						 
						GuardarCambios(); 
						vfCambios = false; 
						vfCambiosPassword = false; 
						//Set Mantenimiento = Nothing 
						//Exit Sub 
						 
						break;
					case 7 :  //No 
						 
						vfCambios = false; 
						vfCambiosPassword = false; 
						//Set Mantenimiento = Nothing 
						//Exit Sub 
						 
						break;
				}

			}

			if (vfCerrar && vfTodo)
			{
				Rq.Close();
			}
			if (NuevaConsulta)
			{
				ModuloMantenim.RsManten.Close();
			}



			MemoryHelper.ReleaseMemory();
		}



		private void mebMascara_Change(int Index)
		{
			if (mebMascara[Index].Text.Trim() != "" && mebMascara[Index].Enabled)
			{
				cbBotones[2].Enabled = true;
				if (sprMultiple.Enabled)
				{
					return;
				}
				vfCambios = true;
			}
		}

		private void mebMascara_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mebMascara, eventSender);

			mebMascara[Index].SelectionStart = 0;
            mebMascara[Index].SelectionLength = mebMascara[Index].Text.Length;
			if (ActiveControl == mebMascara[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}
		}





		private void mebMascara_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			int Index = Array.IndexOf(this.mebMascara, eventSender);
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].NombreControl.Name == "mebMascara" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
				{
					if (astrElConjunto[i].imayusculas == "S")
					{
						if (KeyAscii == 39)
						{
							KeyAscii = Serrores.SustituirComilla();
						}
						Serrores.ValidarTecla(ref KeyAscii);
						KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]);
					}
					if (KeyAscii == 39)
					{
						KeyAscii = Serrores.SustituirComilla();
					}
					if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC")
					{
						if ((KeyAscii < Strings.Asc('0') || KeyAscii > Strings.Asc('9')) && KeyAscii != Strings.Asc('-') && KeyAscii != Strings.Asc(',') || (mebMascara[Index].Text.IndexOf(',') >= 0 && KeyAscii == Strings.Asc(',')))
						{
							KeyAscii = 0;
						}
					}
					if (astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
					{
						if ((KeyAscii < Strings.Asc('0') || KeyAscii > Strings.Asc('9')) && KeyAscii != Strings.Asc('-'))
						{
							KeyAscii = 0;
						}
					}
					break;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void mebMascara_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mebMascara, eventSender);
			int IposiNegativo = 0;
			int IposiPositivo = 0;
			bool FBool = false;

			if (this.ActiveControl.Name == cbBotones[1].Name)
			{
				if (!(ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[1]) && ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[2])) && mebMascara[Index].Text.Trim() != "" && stPulsado != "consultar")
				{ //Si no pulsa cancelar, ni limpiar
					//el campo no est� vacio y el bot�n pulsado no es el de consultar
					//se valida el tama�o fijo
					for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].NombreControl.Name == "mebMascara" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
						{
							if (astrElConjunto[i].itamaniofijo == "S")
							{
								IposiNegativo = (mebMascara[Index].Text.IndexOf('-') + 1);
								IposiPositivo = (mebMascara[Index].Text.IndexOf('+') + 1);
								if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
								{
									if (IposiNegativo != 0 || IposiPositivo != 0)
									{
										FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length - 1, i);
									}
									else
									{
										FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length, i);
									}
								}
								else
								{
									FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length, i);
								}
							}
							break;
						}
					}
				}
			}
			else if (mebMascara[Index].Text.Trim() != "" && stPulsado != "consultar")
			{  //Si no pulsa cancelar, ni limpiar
				//el campo no est� vacio y el bot�n pulsado no es el de consultar
				//se valida el tama�o fijo
				for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
				{
					if (astrElConjunto[i].NombreControl.Name == "mebMascara" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
					{
						if (astrElConjunto[i].itamaniofijo == "S")
						{
							IposiNegativo = (mebMascara[Index].Text.IndexOf('-') + 1);
							IposiPositivo = (mebMascara[Index].Text.IndexOf('+') + 1);
							if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
							{
								if (IposiNegativo != 0 || IposiPositivo != 0)
								{
									FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length - 1, i);
								}
								else
								{
									FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length, i);
								}
							}
							else
							{
								FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length, i);
							}
						}
						break;
					}
				}
			}
		}


		private bool isInitializingComponent;
		private void rbConsulta_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.rbConsulta, eventSender);


				switch(Index)
				{
					case 0 :  //OR 
						vstCriterioConsulta = " OR "; 
						break;
					case 1 :  //AND 
						vstCriterioConsulta = " AND "; 
						break;
				}

			}
		}

		private void scbvCampos_Change(int newScrollValue)
		{
			int ControlEnfoque = 0;
			//Mover los controles en el scrollbar

			//Comprobamos si el Scroll nos ordena
			//subir o bajar los controles
			//ControlEnfoque = ((ActiveControl.Top + ActiveControl.Width) * scbvCampos.Max / frmDetalle.Width)
			//scbvCampos.Value = Int(ControlEnfoque / scbvCampos.SmallChange) * scbvCampos.SmallChange

			viControlarValorScroll = Convert.ToInt32(Double.Parse(newScrollValue.ToString()));
			//viControlarValorScroll = ActiveControl.Top * scbvCampos.Max / frmDetalle.Width
			if (viControlarScroll < viControlarValorScroll)
			{
				//********************CRIS**************************
				//Mientras el top del frame sea mayor que la diferencia entre
				//las alturas de los frames DetalleFondo y Detalle (iDiferencia),
				//mas la separaci�n de campo con los l�mites del frame (COiSuperiorCampo),
				//dejo que se vayan subiendo los campos. Una vez que sean iguales ya
				//no subir�n mas, ya que querr� decir que se ve el �ltimo campo

				if (((float) (frmDetalle.Top * 15)) > -(iDiferencia + ModuloMantenim.COiSuperiorCampo))
				{
					//Compruebo que la barra no haya sido movida arrastrando
					if (Math.Abs(viControlarScroll - viControlarValorScroll) == scbvCampos.SmallChange)
					{
						frmDetalle.Top = (int) (frmDetalle.Top - 10); //esto
					}
					else
					{
						//Si se ha movido arrastrando, tendr� que saber el equivalente
						//de veces que se ha movido y eso lo obtengo restando la posicion
						//anterior y la actual y dividiendo esto entre el salto de la scroll
						//ya tengo el n� de veces que se ha movido.
						//Si multiplico eso por 150 es la cantidad que hay que sumarle al top
						//para que nos de el top actual
						frmDetalle.Top = (int) (frmDetalle.Top + 150 * ((viControlarScroll - viControlarValorScroll) / ((int) scbvCampos.SmallChange)) / 15);
					}
				}
				//**************************************************
			}
			else
			{
				//**********************CRIS**************************
				//Mientras el top del Frame Detalle  sea menor que cero puedo seguir
				//bajando los controles. Una vez que sea igual a cero ya no los bajar�
				//mas porque eso querr� decir que ya se ve el primero
				if (((float) (frmDetalle.Top * 15)) < 0)
				{
					//Compruebo que la barra no haya sido movida arrastrando
					if (Math.Abs(viControlarScroll - viControlarValorScroll) == scbvCampos.SmallChange)
					{
						frmDetalle.Top = (int) (frmDetalle.Top + 10); //esto
					}
					else
					{
						//Si se ha movido arrastrando, tendr� que saber el equivalente
						//de veces que se ha movido y eso lo obtengo restando la posicion
						//anterior y la actual y dividiendo esto entre el salto de la scroll
						//ya tengo el n� de veces que se ha movido.
						//Si multiplico eso por 150 es la cantidad que hay que sumarle al top
						//para que nos de el top actual.
						//Tanto el top anterior como la cantidad de veces multiplicada por 150
						//nos van a dar n�meros negativos, por eso se suman
						frmDetalle.Top = (int) (frmDetalle.Top + 150 * ((viControlarScroll - viControlarValorScroll) / ((int) scbvCampos.SmallChange)) / 15);
					}
				}
				//**************************************************
			}
			//Cogemos el �ltimo valor del Scroll
			//para poderlo comparar en el pr�ximo cambio
			viControlarScroll = Convert.ToInt32(Double.Parse(newScrollValue.ToString()));
		}

		private void sdcFecha_Change(int Index)
		{
			if (sprMultiple.Enabled)
			{
				return;
			}
			if (sdcFecha[Index].Text != "" && sdcFecha[Index].Enabled)
			{
				cbBotones[2].Enabled = true;
				vfCambios = true;
			}

		}

		private void sdcFecha_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.sdcFecha, eventSender);
			if (ActiveControl == sdcFecha[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}
		}


		private void sdcFecha_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.sdcFecha, eventSender);
			//Si no se ha ejecutado todavia el Form_Load
			//no habr� todav�a ning�n control activo, con
			//lo que cascar�a en este procedimiento.
			//Por eso inicializamos la varible fControlActivo=false
			//en el Form_Load y hasta que no termine no la ponemos a true
			if (fControlActivo)
			{
				//Miro si la fecha solo admite null,
				//si el control es el de fecha borrado
				//y no se ha pulsado cancelar ni limpiar
				if (fSoloNULL && Index == iIndexFecha && sdcFecha[Index].Text.Trim() != "")
				{
					if (this.ActiveControl.Name == cbBotones[1].Name)
					{
						if (ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[0]))
						{
							if (!(this.ActiveControl.Name == cbBotones[1].Name && ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[1])))
							{
								RadMessageBox.Show("S�lo admite cambio a null", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
								sdcFecha[Index].Focus();
								fActivarRegistro = false;
							}
						}
					}
					else if (fSoloNULL && Index == iIndexFecha && this.ActiveControl.Name != cbBotones[1].Name && sdcFecha[Index].Text.Trim() == "")
					{ 
						fActivarRegistro = true;
					}
				}
				else if (fSoloNULL && Index == iIndexFecha && sdcFecha[Index].Text.Trim() == "")
				{ 
					fActivarRegistro = true;
				}
			}
		}

        bool _isMouseRightDown = false;
        private void SprMultiple_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            _isMouseRightDown = (e.Button == MouseButtons.Right);
        }

        private void sprMultiple_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
            if (!_isMouseRightDown)
			{
				int col = eventArgs.ColumnIndex;
				int Row = eventArgs.RowIndex;
				//Si lo que se ha pulsado es la cabecera del campo
				if (Row == 0)
				{
					return;
				}

				//Si el modo es de solo consulta
				if (!cbSpread[0].Visible)
				{
                    sprMultiple.SelectionMode = GridViewSelectionMode.CellSelect;
                    sprMultiple.Row = Row;
					LlenarCampos();
					frmDetalleFondo.Enabled = true;
					scbvCampos.Enabled = true;
					for (int i = 0; i <= LaConexion.GetUpperBound(0) - 1; i++)
					{
						LaConexion[i].NombreControl.Enabled = false;
					}
					cbSpread[5].Enabled = true;
					cbSpread[6].Enabled = true;

				}
				else
				{
					if (sprMultiple.MaxRows != 0)
					{
						if (cbSpread[3].Enabled)
						{
                            sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
							sprMultiple.Row = Row;
							ActivarBotonesSpread();
							LlenarCampos();
							ModuloMantenim.DesactivarCampos(this, astrElConjunto, AStrToolbar);
							cbSpread[2].Focus();
							cbBotones[0].Enabled = false;
							cbBotones[1].Enabled = false;
							cbBotones[2].Enabled = false;
						}
					}
				}
			}
		}

        private void sprMultiple_CellDoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			if (Row == 0)
			{
				return;
			}
			if (!cbSpread[0].Visible)
			{
				return;
			}
			if (sprMultiple.MaxRows != 0)
			{
                sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
				sprMultiple.Row = Row;
				ActivarBotonesSpread();
				LlenarCampos();
				cbBotones[2].Enabled = false;
				//Ejecuta modificar
				cbSpread_Click(cbSpread[2], new EventArgs());
			}

		}


		private void sprMultiple_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;

			int FilaActiva = 0;
			int ColActiva = 0;

			if (KeyCode == ((int) Keys.Down))
			{
				FilaActiva = sprMultiple.ActiveRowIndex + 1;
				if (FilaActiva > sprMultiple.MaxRows)
				{
					FilaActiva = sprMultiple.ActiveRowIndex;
				}
				else
				{
                    var currentRow = sprMultiple.Rows[FilaActiva].Cells[ColActiva].RowInfo;
                    var currentCol = sprMultiple.Rows[FilaActiva].Cells[ColActiva].ColumnInfo;
                    sprMultiple_CellClick(sprMultiple, new GridViewCellEventArgs(currentRow, currentCol, null));
					LlenarCampos();
				}

			}
			else if (KeyCode == ((int) Keys.Up))
			{ 
				FilaActiva = sprMultiple.ActiveRowIndex - 1;
				if (FilaActiva == 0)
				{
					FilaActiva = sprMultiple.ActiveRowIndex;
				}
				else
				{
                    var currentRow = sprMultiple.Rows[FilaActiva].Cells[ColActiva].RowInfo;
                    var currentCol = sprMultiple.Rows[FilaActiva].Cells[ColActiva].ColumnInfo;
                    sprMultiple_CellClick(sprMultiple, new GridViewCellEventArgs(currentRow, currentCol, null));
                    LlenarCampos();
				}
			}

		}

		private void sprMultiple_TopLeftChange(object eventSender, UpgradeHelpers.Spread.TopLeftChangeEventArgs eventArgs)
		{
			//Cada vez que se baje la barra de la spread habr� que mirar
			//si hay m�s registros por cargar. Si los hay, habr� que hacer el
			//Requery comparando con la �ltima fila de la spread que cargamos en
			//nuestra variable par�metro

			//HE PUESTO
			//******************************
			StringBuilder StParametro = new StringBuilder();

			if (CargarMas)
			{
				if (eventArgs.NewTop >= Tope)
				{
					sprMultiple.Row = sprMultiple.MaxRows;
					foreach (ModuloMantenim.StrParametro AStParametro_item in AStParametro)
					{
						sprMultiple.Col = AStParametro_item.iOrdenColumnaSpread;
						switch(AStParametro_item.StTipo)
						{
							case "NUMERIC" : case "INT" : case "SMALLINT" : 
								//Relleno con blancos por la izquierda 
								StParametro.Append(Padl(sprMultiple.Text.Trim(), AStParametro_item.iLongitud, "0")); 
								break;
							case "CHAR" : case "VARCHAR" : 
								//Relleno con blancos por la derecha 
								StParametro.Append(Padr(sprMultiple.Text.Trim(), AStParametro_item.iLongitud)); 
								break;
							case "DATETIME" : 
								//Relleno con blancos por la derecha 
								StParametro.Append(Padr(DateTime.Parse(sprMultiple.Text).ToString("yyyy/MM/dd").Trim(), AStParametro_item.iLongitud)); 
								break;
						}
					}

					Rq.Parameters[0].Value = StParametro.ToString();
					ModuloMantenim.RsManten.Reset();
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Rq);
					tempAdapter.Fill(ModuloMantenim.RsManten);
					if (ModuloMantenim.RsManten.Tables[0].Rows.Count < ModuloMantenim.vMaximoFilas)
					{
						CargarMas = false;
					}
					RecorrerRegistros();
					Tope = sprMultiple.MaxRows - iNumeroFilasVisibles; //11
				}
			}
			//********************************
		}

		private string Padr(string stCadena, int iEspacios, string stRelleno = " ")
		{
			//Funcion que concatena blancos u otro caracter por la derecha
			return stCadena.Trim() + new string(stRelleno[0], iEspacios - stCadena.Trim().Length);
		}
		private void tbMultilinea_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbMultilinea, eventSender);
			if (tbMultilinea[Index].Text.Trim() != "" && tbMultilinea[Index].Enabled)
			{
				cbBotones[2].Enabled = true;
				if (sprMultiple.Enabled)
				{
					return;
				}
				vfCambios = true;
			}
		}

		private void tbMultilinea_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbMultilinea, eventSender);

			tbMultilinea[Index].SelectionStart = 0;
			tbMultilinea[Index].SelectionLength = tbMultilinea[Index].MaxLength;
			if (ActiveControl == tbMultilinea[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}
		}

		private void tbMultilinea_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			int Index = Array.IndexOf(this.tbMultilinea, eventSender);
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].NombreControl.Name == "tbMultilinea" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
				{
					//Oscar 25/06/04 Impedimos que se pulse "Ctrl+Intro"
					if (KeyAscii == 10 || KeyAscii == 13)
					{
						KeyAscii = 0;
					}
					//-------
					if (KeyAscii == 39)
					{
						KeyAscii = Serrores.SustituirComilla();
					}
					if (astrElConjunto[i].imayusculas == "S")
					{
						Serrores.ValidarTecla(ref KeyAscii);
						KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]);
					}
					break;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbMultilinea_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbMultilinea, eventSender);
			int IposiNegativo = 0;
			int IposiPositivo = 0;
			bool FBool = false;

			if (this.ActiveControl.ToString() == cbBotones[1].Name)
			{
				if (!(ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[1]) && ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[2])) && tbMultilinea[Index].Text.Trim() != "" && stPulsado != "consultar")
				{ //Si no pulsa cancelar, ni limpiar
					//el campo no est� vacio y el bot�n pulsado no es el de consultar
					//se valida el tama�o fijo
					for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].NombreControl.Name == "tbMultilinea" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
						{
							if (astrElConjunto[i].itamaniofijo == "S")
							{
								if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
								{
									IposiNegativo = (tbMultilinea[Index].Text.IndexOf('-') + 1);
									IposiPositivo = (tbMultilinea[Index].Text.IndexOf('+') + 1);
									if (IposiNegativo != 0 || IposiPositivo != 0)
									{
										FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length + 1, i);
									}
									else
									{
										FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length, i);
									}
								}
								else
								{
									FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length, i);
								}
							}
							break;
						}
					}
				}
			}
			else if (tbMultilinea[Index].Text.Trim() != "" && stPulsado != "consultar")
			{  //Si no pulsa cancelar, ni limpiar
				//el campo no est� vacio y el bot�n pulsado no es el de consultar
				//se valida el tama�o fijo
				for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
				{
					if (astrElConjunto[i].NombreControl.Name == "tbMultilinea" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
					{
						if (astrElConjunto[i].itamaniofijo == "S")
						{
							if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
							{
								IposiNegativo = (tbMultilinea[Index].Text.IndexOf('-') + 1);
								IposiPositivo = (tbMultilinea[Index].Text.IndexOf('+') + 1);
								if (IposiNegativo != 0 || IposiPositivo != 0)
								{
									FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length + 1, i);
								}
								else
								{
									FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length, i);
								}
							}
							else
							{
								FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length, i);
							}
						}
						break;
					}
				}
			}

		}


		private void ObtenerFoco()
		{
			Control MiControl = null;

			for (int iIndiceControl = 0; iIndiceControl <= astrElConjunto.GetUpperBound(0) - 1; iIndiceControl++)
			{
				MiControl = astrElConjunto[iIndiceControl].NombreControl;
				if (MiControl.TabIndex == ModuloMantenim.COiDesplazamTab && UpgradeHelpers.Gui.ControlHelper.GetEnabled(MiControl))
				{
					ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar);
					MiControl.Focus();
					MiControl = null;
					return;
				}
				MiControl = null;

			}

		}
		public void BuscarFK()
		{
			string StComando2 = String.Empty;
			StringBuilder StOrden = new StringBuilder();
			DataSet RsCombos2 = null;
			bool fDimensionar = false, fDimensionarCombo = false;
			int iLongitudColumna = 0;


			ModuloMantenim.StrFK[] AStColFK = null;

			ModuloMantenim.StrOrdenFK[] AStOrdenFK = null;

			int iIndiceCombo = 0;

			//Obtengo los grupos de columnas de FK
			string StComando = "SELECT * FROM SCOLFKPKV1 " + "WHERE GTABCAMANHIJ= " + "'" + vstNombreTabla.Trim() + "'" + "ORDER BY gfkcaman,nordencolfk ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsCombos = new DataSet();
			tempAdapter.Fill(RsCombos);

			//Obtenemos las columnas FK que se van a sustituir

			string StNombreFK = Convert.ToString(RsCombos.Tables[0].Rows[0]["GFKCAMAN"]).Trim();

			ModuloMantenim.StrFKCombo[] AStColFKCombo = ArraysHelper.InitializeArray<ModuloMantenim.StrFKCombo>(1);

			while (RsCombos.Tables[0].Rows.Count != 0)
			{

				AStColFK = new ModuloMantenim.StrFK[1];
				fDimensionar = false;

				//Mientras la columna pertenezca al mismo grupo
				//de valores de FK, vamos a�adiendo sus datos a un array
				//de estructura
				foreach (DataRow iteration_row in RsCombos.Tables[0].Rows)
				{

					if (StNombreFK.Trim() == Convert.ToString(iteration_row["GFKCAMAN"]).Trim())
					{
						if (fDimensionar)
						{
							AStColFK = ArraysHelper.RedimPreserve(AStColFK, new int[]{AStColFK.GetUpperBound(0) + 2});
						}

						fDimensionar = true;

						AStColFK[AStColFK.GetUpperBound(0)].StColHija = Convert.ToString(iteration_row["GCOLCAMANHIJ"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StColPadre = Convert.ToString(iteration_row["GCOLUMNCAMAN"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre = Convert.ToString(iteration_row["GTABCAMANPAD"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StLabelCombo = Convert.ToString(iteration_row["VLABELCOMBO"]).Trim();

						if (Convert.IsDBNull(iteration_row["GVISTACOMBO"]))
						{
							AStColFK[AStColFK.GetUpperBound(0)].fVistaNula = true;
						}
						else
						{
							AStColFK[AStColFK.GetUpperBound(0)].fVistaNula = false;
							AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo = Convert.ToString(iteration_row["GVISTACOMBO"]).Trim();
						}

						//Guardo el tipo de dato de cada columna
						for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
						{
							if (AStColFK[AStColFK.GetUpperBound(0)].StColHija.Trim() == astrElConjunto[i].gcolumncaman.Trim())
							{
								AStColFK[AStColFK.GetUpperBound(0)].stTipoDato = astrElConjunto[i].gtipodato.Trim();
								break;
							}
						}

					}
					else
					{
						//Si pasa por aqu�, significa que ya se
						//ha cambiado de grupo de columnas FK,
						//con lo que sadremos del Do While
						//y llenaremos el combo con los datos correspondientes
						//al anterior grupo de columnas FK
						break;
					}
				}


				//Si la vista est� a null, los datos se sacan de la tabla
				//padre
				if (AStColFK[AStColFK.GetUpperBound(0)].fVistaNula)
				{
					StComando = "SELECT * from SPREVICMV1 " + "Where gtablacaman = " + "'" + AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre.Trim() + "'" + " AND IVISIBCOMTAB='S' " + "AND ISNULL(gvistacaman,'NULL') = 'NULL' " + "order by NORDENCOLTAB ";

					//Una de las condiciones de la select deber�a ser:
					//AND GVISTACAMAN IS NULL, pero esto no funciona con esta vista
					//y hemos tenido que sustituirlo por:
					//ISNULL(gvistacaman,'NULL') = 'NULL'
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombos2 = new DataSet();
					tempAdapter_2.Fill(RsCombos2);


					//Montamos la select de las columnas que van a ir en el combo

					StComando = "";
					AStOrdenFK = new ModuloMantenim.StrOrdenFK[1];
					iLongitudColumna = 0;
					foreach (DataRow iteration_row_2 in RsCombos2.Tables[0].Rows)
					{
						StComando = StComando + "A." + Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim() + ",";

						//Calculamos la longitud m�xima que va a tener el campo en la spread
						//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
						//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
						//posici�n para el punto decimal

						if (Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "INT")
						{
							if (Convert.ToDouble(iteration_row_2["npartedecima"]) != 0)
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]) + 2);
							}
							else
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]) + 1);
							}
						}
						else
						{
							iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]));
						}

						//Nos creamos un array para guardar el orden de columnas
						//y si �stas son ascendentes o no
						if (Convert.ToDouble(iteration_row_2["NORDENACTAB"]) != 0)
						{
							if (Convert.ToDouble(iteration_row_2["NORDENACTAB"]) > AStOrdenFK.GetUpperBound(0))
							{
								AStOrdenFK = ArraysHelper.RedimPreserve(AStOrdenFK, new int[]{Convert.ToInt32(iteration_row_2["NORDENACTAB"]) + 1});
							}
							AStOrdenFK[Convert.ToInt32(iteration_row_2["NORDENACTAB"])].StAscendente = Convert.ToString(iteration_row_2["IASCENDETAB"]).Trim();
							AStOrdenFK[Convert.ToInt32(iteration_row_2["NORDENACTAB"])].StColumna = Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim();
						}
					}

					RsCombos2.Close();

					//Quitamos la �ltima coma
					StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
					StComando = StComando + "from " + AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre.Trim() + " A";

					//Montamos la sentencia ORDER BY con el array
					//que nos hemos cargado anteriormente
					StComando2 = "";
					StOrden = new StringBuilder("");
					if (AStOrdenFK.GetUpperBound(0) != 0)
					{
						for (int j = 1; j <= AStOrdenFK.GetUpperBound(0); j++)
						{
							StOrden.Append("A." + AStOrdenFK[j].StColumna);
							if (AStOrdenFK[j].StAscendente.Trim() == "N")
							{
								StOrden.Append(" DESC");
							}
							StOrden.Append(",");
						}
						StComando2 = StComando2 + " order by " + StOrden.ToString() + " ";
					}


					//Sustituimos las columnas en la spread y llenamos el combo
					SustituirEnSpreadFK(StComando, StComando2, iLongitudColumna, fDimensionarCombo, AStColFKCombo, AStColFK, iIndiceCombo);

				}
				else
				{
					//Si la vista no esta a null, los datos se sacan de la vista
					StComando = "SELECT * from SPREVICMV1 " + "Where gvistacaman = " + "'" + AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo.Trim() + "'" + " AND IVISIBCOMVIS='S' " + "order by NORDENVISTA ";

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombos2 = new DataSet();
					tempAdapter_3.Fill(RsCombos2);

					//Montamos la select de las columnas que van a ir en el combo

					StComando = "";
					AStOrdenFK = new ModuloMantenim.StrOrdenFK[1];
					iLongitudColumna = 0;
					foreach (DataRow iteration_row_3 in RsCombos2.Tables[0].Rows)
					{
						StComando = StComando + "A." + Convert.ToString(iteration_row_3["GCOLUMNCAMAN"]).Trim() + ",";

						//Calculamos la longitud m�xima que va a tener el campo en la spread
						//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
						//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
						//posici�n para el punto decimal

						//Adem�s le a�adimos siempre uno mas de longitud, ya que
						//los campos ir�n separados por espacios
						if (Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "INT")
						{
							if (Convert.ToDouble(iteration_row_3["npartedecima"]) != 0)
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 2 + 1);
							}
							else
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 1 + 1);
							}
						}
						else
						{
							iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 1);
						}

						//Nos creamos un array para guardar el orden de columnas
						//y si �stas son ascendentes o no
						if (Convert.ToDouble(iteration_row_3["NORDENACVIS"]) != 0)
						{
							if (Convert.ToDouble(iteration_row_3["NORDENACVIS"]) > AStOrdenFK.GetUpperBound(0))
							{
								AStOrdenFK = ArraysHelper.RedimPreserve(AStOrdenFK, new int[]{Convert.ToInt32(iteration_row_3["NORDENACVIS"]) + 1});
							}
							AStOrdenFK[Convert.ToInt32(iteration_row_3["NORDENACVIS"])].StAscendente = Convert.ToString(iteration_row_3["IASCENDEVIS"]).Trim();
							AStOrdenFK[Convert.ToInt32(iteration_row_3["NORDENACVIS"])].StColumna = Convert.ToString(iteration_row_3["GCOLUMNCAMAN"]).Trim();
						}
					}

					RsCombos2.Close();

					//Quitamos la �ltima coma
					StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
					StComando = StComando + "from " + AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo.Trim() + " A";

					//Montamos la sentencia ORDER BY con el array
					//que nos hemos cargado anteriormente
					StOrden = new StringBuilder("");
					StComando2 = "";
					if (AStOrdenFK.GetUpperBound(0) != 0)
					{
						for (int j = 1; j <= AStOrdenFK.GetUpperBound(0); j++)
						{
							StOrden.Append("A." + AStOrdenFK[j].StColumna);
							if (AStOrdenFK[j].StAscendente.Trim() == "N")
							{
								StOrden.Append(" DESC");
							}
							StOrden.Append(",");
						}
						//Quitamos la �ltima coma
						StOrden = new StringBuilder(StOrden.ToString().Substring(0, Math.Min(StOrden.ToString().Length - 1, StOrden.ToString().Length)) + " ");
						StComando2 = " order by " + StOrden.ToString();
					}


					//Sustituimos las columnas en la spread y llenamos el combo
					SustituirEnSpreadFK(StComando, StComando2, iLongitudColumna, fDimensionarCombo, AStColFKCombo, AStColFK, iIndiceCombo);

				}

				//Guardo el nombre del nuevo grupo de columnas FK
				if (RsCombos.Tables[0].Rows.Count != 0)
				{
					StNombreFK = Convert.ToString(RsCombos.Tables[0].Rows[0]["GFKCAMAN"]).Trim();
				}

			}

			RsCombos.Close();
		}


		private void BuscarUnicos()
		{
			//************************************************************************
			//Busca todas las columnas de valores �nicos de la tabla y establece en
			//el array de controles sus propiedades

			string StComando = "Select GGRUNIMANAUT,GCOLUMNCAMAN,NORDCOLINUN " + "from SCOGRMTBV1 " + "where GTABLACAMAN = " + "'" + vstNombreTabla.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsUnico = new DataSet();
			tempAdapter.Fill(RsUnico);

			foreach (DataRow iteration_row in RsUnico.Tables[0].Rows)
			{
				for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
				{
					if (astrElConjunto[i].gcolumncaman.Trim() == Convert.ToString(iteration_row["GCOLUMNCAMAN"]).Trim())
					{
						astrElConjunto[i].ggrunimanaut = Convert.ToString(iteration_row["GGRUNIMANAUT"]).Trim();
						astrElConjunto[i].nordcolinun = (short) iteration_row["NORDCOLINUN"];
						break;
					}
				}
			}

			RsUnico.Close();
			fUnicos = true;
			//************************************************************************
		}

		public void ActualizarFechaBorradoSpread()
		{
			//Una vez eliminada l�gicamente una fila, hacemos que aparezca
			//la fecha del sistema en la casilla de la spread correspondiente
			sprMultiple.Row = sprMultiple.ActiveRowIndex;
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				//If Trim(UCase(astrElConjunto(i).gcolumncaman)) =  Trim(UCase(StFBORRADO)) Then
				if (astrElConjunto[i].icolfborrado.ToString().ToUpper() == "S")
				{
					sprMultiple.Col = astrElConjunto[i].nordencoltab;
					sprMultiple.Text = DateTime.Today.ToString("dd/MM/yyyy");
					break;
				}
			}
		}
		public void BuscarPK(bool Parainsert = false)
		{
			//Busca todas las PK de la tabla y sus caracter�sticas
			bool FArrayCreado = false;

			ModuloMantenim.iIndiceBuscar = 0;
			AStPK = new ModuloMantenim.Clave[1];
			sprMultiple.Row = sprMultiple.ActiveRowIndex;
			for (ModuloMantenim.iIndiceBuscar = 0; ModuloMantenim.iIndiceBuscar <= astrElConjunto.GetUpperBound(0) - 1; ModuloMantenim.iIndiceBuscar++)
			{
				if (StringsHelper.ToDoubleSafe(astrElConjunto[ModuloMantenim.iIndiceBuscar].nordencolpk) != 0)
				{
					if (FArrayCreado)
					{
						AStPK = ArraysHelper.RedimPreserve(AStPK, new int[]{AStPK.GetUpperBound(0) + 2});
					}
					AStPK[AStPK.GetUpperBound(0)].StColumna = astrElConjunto[ModuloMantenim.iIndiceBuscar].gcolumncaman;
					AStPK[AStPK.GetUpperBound(0)].StTipo = astrElConjunto[ModuloMantenim.iIndiceBuscar].gtipodato;
					sprMultiple.Col = astrElConjunto[ModuloMantenim.iIndiceBuscar].nordencoltab;
					if (!Convert.IsDBNull(Parainsert))
					{
						//SI ES "CHAR", "VARCHAR" O "DATETIME",
						AStPK[AStPK.GetUpperBound(0)].stTexto = Convert.ToString(astrElConjunto[ModuloMantenim.iIndiceBuscar].NombreControl.Text);
					}
					else
					{
						AStPK[AStPK.GetUpperBound(0)].stTexto = sprMultiple.Text.Trim();
					}
					AStPK[AStPK.GetUpperBound(0)].iOrdenColumnaSpread = astrElConjunto[ModuloMantenim.iIndiceBuscar].nordencoltab;
					//***********************CRIS***********************************

					switch(astrElConjunto[ModuloMantenim.iIndiceBuscar].gtipodato.Trim())
					{
						case "NUMERIC" :  //Un espacio m�s para el signo 
							AStPK[AStPK.GetUpperBound(0)].iLongitud = (short) (astrElConjunto[ModuloMantenim.iIndiceBuscar].nparteentera + astrElConjunto[ModuloMantenim.iIndiceBuscar].npartedecima + 1); 
							//Un espacio m�s para el punto decimal 
							if (astrElConjunto[ModuloMantenim.iIndiceBuscar].npartedecima != 0)
							{
								AStPK[AStPK.GetUpperBound(0)].iLongitud = (short) (AStPK[AStPK.GetUpperBound(0)].iLongitud + 1);
							} 
							break;
						case "INT" : case "SMALLINT" :  //Un espacio m�s para el signo 
							AStPK[AStPK.GetUpperBound(0)].iLongitud = (short) (astrElConjunto[ModuloMantenim.iIndiceBuscar].nparteentera + astrElConjunto[ModuloMantenim.iIndiceBuscar].npartedecima + 1); 
							break;
						case "CHAR" : case "VARCHAR" : case "DATETIME" : 
							AStPK[AStPK.GetUpperBound(0)].iLongitud = (short) (astrElConjunto[ModuloMantenim.iIndiceBuscar].nparteentera + astrElConjunto[ModuloMantenim.iIndiceBuscar].npartedecima); 
							 
							break;
					}
					//********************************************************************************************************
					FArrayCreado = true;
				}
			}
		}
		private bool ComprobarFechas(string stCampo)
		{

			for (int iIndiceComprobacion = 0; iIndiceComprobacion <= astrElConjunto.GetUpperBound(0); iIndiceComprobacion++)
			{
				if (astrElConjunto[iIndiceComprobacion].gcolumncaman == stCampo)
				{
					return astrElConjunto[iIndiceComprobacion].gtipodato == "DATETIME";
				}
			}
			return false;
		}

		private bool ValidarRequeridoConsulta()
		{
			//Rutina donde tenemos que comprobar si hay campos
			//requeridos en la consulta, sac�ndolo de la estructura.
			//>>>>>>>>>ireqconsulta
			Control MiControl = null;

			for (int iIndiceControl = 0; iIndiceControl <= astrElConjunto.GetUpperBound(0) - 1; iIndiceControl++)
			{

				MiControl = astrElConjunto[iIndiceControl].NombreControl;
				if (Convert.ToString(MiControl.Text).Trim() == "" && astrElConjunto[iIndiceControl].ireqconsulta == "S")
				{
					short tempRefParam = 1040;
					string[] tempRefParam2 = new string[]{astrElConjunto[iIndiceControl].dcoltabmanco};
					ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
					ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar);
					MiControl.Focus();
					MiControl = null;
					return false;
				}
				MiControl = null;
			}
			return true;
		}
		private int ComprobarCamposPassword()
		{
			int result = 0;

			for (int iIndiceEstructura = 0; iIndiceEstructura <= astrElConjunto.GetUpperBound(0) - 1; iIndiceEstructura++)
			{
				if (astrElConjunto[iIndiceEstructura].ipassword == "S")
				{
					for (int iIndiceColumna = 0; iIndiceColumna <= LaConexion.GetUpperBound(0) - 1; iIndiceColumna++)
					{
						if (LaConexion[iIndiceColumna].NombreCampo == astrElConjunto[iIndiceEstructura].gcolumncaman)
						{
							result = LaConexion[iIndiceColumna].NumeroColumna;
						}
					}
				}

			}

			return result;
		}
		public bool ComprobarNumericos(string stCampo)
		{
			//S�lo comprobamos que el campo es un n�mero o no

			for (int iIndiceComprobacion = 0; iIndiceComprobacion <= astrElConjunto.GetUpperBound(0) - 1; iIndiceComprobacion++)
			{
				if (astrElConjunto[iIndiceComprobacion].gcolumncaman == stCampo)
				{
					return astrElConjunto[iIndiceComprobacion].gtipodato == "NUMERIC" || astrElConjunto[iIndiceComprobacion].gtipodato == "DECIMAL" || astrElConjunto[iIndiceComprobacion].gtipodato == "INT" || astrElConjunto[iIndiceComprobacion].gtipodato == "SMALLINT";
				}
			}

			return false;
		}
		public string ObtenerColumnaFBorrado()
		{
			//Buscamos el nombre de la columna que sea Fecha de Borrado:
			//ICOLFBORRADO="S"
			string result = String.Empty;
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].icolfborrado.ToString().ToUpper() == "S")
				{
					result = astrElConjunto[i].gcolumncaman.Trim();
					break;
				}
			}
			return result;
		}
		private bool ValidarRequerido()
		{
			//Debemos buscar en la estructura el control y comprobar si admite nulos
			Control MiControl = null;

			for (int iIndiceControl = 0; iIndiceControl <= astrElConjunto.GetUpperBound(0) - 1; iIndiceControl++)
			{

				MiControl = astrElConjunto[iIndiceControl].NombreControl;
				if (Convert.ToString(MiControl.Text).Trim() == "" && astrElConjunto[iIndiceControl].iadmitenull == "N")
				{
					//MENSAJE:Campo V1 requerido.
					short tempRefParam = 1040;
					string[] tempRefParam2 = new string[]{astrElConjunto[iIndiceControl].dcoltabmanco};
					ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
					MiControl.Focus();
					MiControl = null;
					return false;
				}
				MiControl = null;
			}
			return true;
		}
		private bool ValidarPKUnicos()
		{
			//Si la tabla tiene valores �nicos, se valida si existe algun registro
			//con los valores �nicos iguales a los que quiero insertar

			bool result = false;
			StringBuilder StSelectPK = new StringBuilder();
			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{
				if (i == 0)
				{
					StSelectPK.Append(" (");
				}
				else
				{
					StSelectPK.Append(" AND ");
				}

				StSelectPK.Append(AStPK[i].StColumna.Trim() + "=");
				switch(AStPK[i].StTipo.Trim())
				{
					case "NUMERIC" : case "INT" : case "SMALLINT" : 
						StSelectPK.Append(AStPK[i].stTexto); 
						break;
					case "CHAR" : case "VARCHAR" : 
						StSelectPK.Append("'" + AStPK[i].stTexto.Trim() + "'"); 
						break;
					case "DATETIME" : 
						System.DateTime TempDate = DateTime.FromOADate(0); 
						StSelectPK.Append("'" + ((DateTime.TryParse(AStPK[i].stTexto, out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : AStPK[i].stTexto) + "'"); 
						break;
				}
			}

			StSelectPK.Append(")");
			string StComando = "select * from " + vstNombreTabla + " where " + StSelectPK.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsUnico = new DataSet();
			tempAdapter.Fill(RsUnico);
			if (RsUnico.Tables[0].Rows.Count > 0)
			{
				result = false;
				short tempRefParam = 1080;
				string[] tempRefParam2 = new string[]{"Elemento"};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
			}
			else
			{
				result = true;
			}
			RsUnico.Close();
			return result;
		}

		private bool ValidarValoresUnicos(bool? fModificar_optional, object fVieneDeCombos_optional)
		{
			bool fModificar = (fModificar_optional == null) ? false : fModificar_optional.Value;
			object fVieneDeCombos = (fVieneDeCombos_optional == Type.Missing) ? null : fVieneDeCombos_optional as object;
			//Si la tabla tiene valores �nicos, se valida si existe algun registro
			//con los valores �nicos iguales a los que quiero insertar

			bool result = false;
			DataSet RsUnico = null;
			StringBuilder StComando = new StringBuilder();
			bool fHayValoresUnicos = false;
			StringBuilder StUnicos = new StringBuilder();
			string StMensaje = String.Empty;
			StringBuilder StSelectPK = new StringBuilder();

			//Monto lo que le voy a tener que a�adir al where para que, si
			//lo que estoy haciendo es modificar, compruebe que no hay valores �nicos,
			//pero para otra fila que no sea la que estoy modificando

			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{
				if (i == 0)
				{
					StSelectPK.Append(" AND (");
				}
				else
				{
					StSelectPK.Append(" OR ");
				}

				StSelectPK.Append(AStPK[i].StColumna.Trim() + "=");
				switch(AStPK[i].StTipo.Trim())
				{
					case "NUMERIC" : case "INT" : case "SMALLINT" : 
						StSelectPK.Append(AStPK[i].stTexto); 
						break;
					case "CHAR" : case "VARCHAR" : 
						StSelectPK.Append("'" + AStPK[i].stTexto.Trim() + "'"); 
						break;
					case "DATETIME" : 
						System.DateTime TempDate = DateTime.FromOADate(0); 
						StSelectPK.Append("'" + ((DateTime.TryParse(AStPK[i].stTexto, out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : AStPK[i].stTexto) + "'"); 
						break;
				}
			}

			StSelectPK.Append(")");

			result = true;

			//astrElConjunto(j).fEsta nos indica si el campo est� ya
			//en el conjunto de grupos de valores �nicos
			//Inicializo la booleana de todo el array a falso
			for (int j = 0; j <= astrElConjunto.GetUpperBound(0) - 1; j++)
			{
				astrElConjunto[j].fEsta = false;
			}

			//Recorre el array buscando las columnas que tengan valores �nicos
			//Cuando encuentra una, recorre otra vez el array para ver
			//si hay alguna columna m�s que sea de valor �nico y pertenezca
			//a su grupo, y cuando ya no hay ninguna m�s del mismo grupo,
			//lanza el resulset.
			//Si encuentra que existe en la tabla alg�n registro con el grupo
			//de valores �nicos repetidos, lanza un mensaje de error y ya no
			//sigue buscando si existen otros grupos de valores �nicos.
			//Si no encuentra ning�n registro, comprueba si hay otro grupo de
			//valores �nicos y repite la operaci�n
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].nordcolinun != 0 && !astrElConjunto[i].fEsta)
				{
					//astrElConjunto(i).fEsta nos indica si el campo est� ya
					//en el conjunto de grupos de valores �nicos

					StComando = new StringBuilder("select * from " + vstNombreTabla + " where ");

					//Recorro el array en busca de otras columnas que
					//pertenezcan al mismo grupo de valores �nicos y la
					//a�ado al where
					for (int j = 0; j <= astrElConjunto.GetUpperBound(0) - 1; j++)
					{
						if (astrElConjunto[i].ggrunimanaut.Trim() == astrElConjunto[j].ggrunimanaut.Trim())
						{

							astrElConjunto[i].fEsta = true;
							astrElConjunto[j].fEsta = true;

							//Compruebo si ya ha habido anteriormente alguna columna
							//perteneciente a este grupo de valores �nicos
							if (fHayValoresUnicos)
							{
								StComando.Append(" AND ");
							}
							fHayValoresUnicos = true;

							//SI EL CAMPO ES DATETIME, NO SE TENDR� EN CUENTA LA HORA.
							//SE CONVIERTE LA FECHA-HORA A FECHA FORMATEADA A "DD/MM/YYYY"
							if (astrElConjunto[j].gtipodato.Trim() == "DATETIME")
							{
								StComando.Append("CONVERT(char(10)," + astrElConjunto[j].gcolumncaman + ",103)");
							}
							else
							{
								StComando.Append(astrElConjunto[j].gcolumncaman);
							}

							if (Convert.ToString(astrElConjunto[j].NombreControl.Text).Trim() != "")
							{

								StComando.Append("=");

								//SI ES "CHAR", "VARCHAR" O "DATETIME",
								//EL DATO VA ENTRE COMILLA SIMPLE
								if (astrElConjunto[j].gtipodato.Trim() == "VARCHAR" || astrElConjunto[j].gtipodato.Trim() == "CHAR" || astrElConjunto[j].gtipodato.Trim() == "DATETIME")
								{
									StComando.Append("'");
								}

								//SI EL CONTROL ES M�SCARA, HABR� QUE COGER EL FORMATTEDTEXT
								//EN VEZ DEL TEXT PARA QUE COJA EL PUNTO EN LOS DECIMALES
								if (astrElConjunto[j].NombreControl.Name == "mebMascara")
								{
									StComando.Append(Convert.ToString(astrElConjunto[j].NombreControl.FormattedText()).Trim());
								}
								else
								{
									StComando.Append(Convert.ToString(astrElConjunto[j].NombreControl.Text).Trim());
								}

								//SI ES "CHAR", "VARCHAR" O "DATETIME",
								//EL DATO VA ENTRE COMILLA SIMPLE
								if (astrElConjunto[j].gtipodato.Trim() == "VARCHAR" || astrElConjunto[j].gtipodato.Trim() == "CHAR" || astrElConjunto[j].gtipodato.Trim() == "DATETIME")
								{
									StComando.Append("'");
								}

							}
							else
							{
								//SI EL CAMPO EST� EN BLANCO, SE COMPARA CON NULL
								StComando.Append(" IS NULL ");
							}
						}
					}


					if (fModificar_optional != null)
					{
						if (fModificar)
						{
							StComando.Append(StSelectPK.ToString());
						}
					}
					if (fHayValoresUnicos)
					{
						//Si hemos encontrado algun grupo de valores �nicos
						//Lanzamos el resulset y vemos si hay algun registro igual en la
						//tabla
						SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
						RsUnico = new DataSet();
						tempAdapter.Fill(RsUnico);
						if ((RsUnico.Tables[0].Rows.Count != 0 && !((fModificar_optional != null) ? fModificar : false)) || (RsUnico.Tables[0].Rows.Count > 1 && ((fModificar_optional != null) ? fModificar : false)))
						{

							result = false;

							//Montamos el mensaje de error con el nombre del grupo
							//y el de las columnas que pertenecen al grupo de valores
							//�nicos
							StUnicos = new StringBuilder("");
							for (int j = 0; j <= astrElConjunto.GetUpperBound(0) - 1; j++)
							{
								if (astrElConjunto[i].ggrunimanaut.Trim() == astrElConjunto[j].ggrunimanaut.Trim())
								{
									if (!astrElConjunto[j].NombreControl.Visible && fVieneDeCombos_optional != Type.Missing)
									{
										foreach (ModuloMantenim.StrFKUnicos AStrFKUnicos_item in AStrFKUnicos)
										{
											if (astrElConjunto[j].gcolumncaman.Trim() == AStrFKUnicos_item.StColHija.Trim())
											{
												if (StUnicos.ToString().Trim() != "")
												{
													StUnicos.Append(", ");
												}
												StUnicos.Append(AStrFKUnicos_item.StLabelCombo.Trim());
												break;
											}
										}
									}
									else
									{
										if (StUnicos.ToString().Trim() != "")
										{
											StUnicos.Append(", ");
										}
										StUnicos.Append(astrElConjunto[j].dcoltabmanco.Trim());
									}
								}
							}

							StMensaje = "Grupo de valores �nicos " + astrElConjunto[i].ggrunimanaut + " formado por " + StUnicos.ToString();
							short tempRefParam = 1080;
							string[] tempRefParam2 = new string[]{StMensaje};
							ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);

							RsUnico.Close();
							return result;
						}
						RsUnico.Close();
					}
					fHayValoresUnicos = false;
				}
			}

			return result;
		}

		private bool ValidarValoresUnicos(bool? fModificar_optional)
		{
			return ValidarValoresUnicos(fModificar_optional, Type.Missing);
		}

		private bool ValidarValoresUnicos()
		{
			return ValidarValoresUnicos(null, Type.Missing);
		}

		private bool ModificarDB()
		{
			bool errorborrar = false;
			bool errormodificar = false;
			//Modificar un registro en la Base de Datos
			bool result = false;
			object lNumerico = null;
			Control[] MiControl = null;
			int i = 0;
			DialogResult res = (DialogResult) 0;
			bool p = true;
            SqlDataAdapter tempAdapter_2 = null;
            //Se redimensiona para introducir los valores de la Spread
            aActualizaSpread = ArraysHelper.InitializeArray<string>(sprMultiple.MaxCols + 1);

			if (vfCambiosPassword)
			{
				SMT200F1 tempLoadForm = SMT200F1.DefInstance;
				SMT200F1.DefInstance.RecibirContrase�a(vstContrase�a);
				SMT200F1.DefInstance.ShowDialog();
			}

			//CRIS
			//*******************************
			aComparacion = ArraysHelper.InitializeArray<string>(astrElConjunto.GetUpperBound(0));

			//Introducir los datos del registro en un array para luego compararlo
			for (int iIndiceArray = 0; iIndiceArray <= astrElConjunto.GetUpperBound(0) - 1; iIndiceArray++)
			{
				if (astrElConjunto[iIndiceArray].dcoltabmanco.Trim() != "")
				{
					sprMultiple.Col = astrElConjunto[iIndiceArray].nordencoltab;
					aComparacion[iIndiceArray] = sprMultiple.Text;
				}
				else
				{
					//Si el orden de la columna en la tabla es cero, pongo la constante
					//CoEsCombo para luego mirar que, cada vez que tenga esta constante,
					//no lo comparo con la spread
					aComparacion[iIndiceArray] = ModuloMantenim.CoEsCombo;
				}
			}
			//*******************************

			int iIndiceModificar = 0;


			try
			{
				errormodificar = true;
				errorborrar = false;

				//stSQLManten = MontarSelectConsulta  'Para modificar

				//****************
				if (!ModuloMantenim.PermitirBorrar(ModuloMantenim.stSQLManten))
				{
					//Borrar de la spread el registro eliminado
                    // ralopezn_TODO_X_5 18/05/2016
					//sprMultiple.Action = 5; //Borra de la Spread el registro actual
					//sprMultiple.MaxRows--;
					return false;
				}
				//****************
				if (!ModuloMantenim.PermitirModificar(ModuloMantenim.stSQLManten, aComparacion, this))
				{
					//No seguir pues no hay cambio
					return result;
				}
                //****************
                //Abro una transacci�n para borrar el registro que se quiere modificar.
                //Si da erores de integridad porque algunas de sus claves est� siendo
                //referenciada en otra tabla, avisaremos de que si modifica alguno
                //de los datos, puede cambiar el significado  del registro.
                //Si no da error seguiremos realizando la modificaci�n
                ModuloMantenim.RcManten.BeginTransScope();
                SqlDataAdapter tempAdapter = new SqlDataAdapter(ModuloMantenim.stSQLManten, ModuloMantenim.RcManten);
				ModuloMantenim.RsManten2 = new DataSet();
				tempAdapter.Fill(ModuloMantenim.RsManten2);


				errorborrar = true;
				errormodificar = false;
                // RESUME NEXT
                try
                {
                    ModuloMantenim.RsManten2.Delete(tempAdapter);
                }
                catch
                {
                    if (ComprobarDependenciasTabla(vstNombreTabla, ModuloMantenim.stSQLManten, false, tempAdapter))
                    {
                        ModuloMantenim.RsManten2.Close();

                        ModuloMantenim.RcManten.RollbackTransScope();
                    }
                    else
                    {
                        res = RadMessageBox.Show("Si modifica el elemento seleccionado, puede cambiar el significado del mismo. �Desea continuar?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question);
                        if (res == System.Windows.Forms.DialogResult.Yes)
                        {
                            ModuloMantenim.RsManten2.Close();

                            ModuloMantenim.RcManten.RollbackTransScope();
                        }
                        else
                        {
                            ModuloMantenim.RcManten.RollbackTransScope();
                            return result;
                        }
                    }
                }
				errormodificar = true;
				errorborrar = false;

				tempAdapter_2 = new SqlDataAdapter(ModuloMantenim.stSQLManten, ModuloMantenim.RcManten);
				ModuloMantenim.RsManten2 = new DataSet();
				tempAdapter_2.Fill(ModuloMantenim.RsManten2);


				//Modificar los registros, porque no hay variaciones.
				ModuloMantenim.RsManten2.Edit();
				iIndiceModificar = 0;
				i = 0;

				//Bucle For para Modificar en la DB.
				for (iIndiceModificar = 0; iIndiceModificar <= sprMultiple.MaxCols - 1; iIndiceModificar++)
				{
					MiControl = new Control[iIndiceModificar + 1];
					MiControl[iIndiceModificar] = LaConexion[iIndiceModificar].NombreControl;

					//Si el control es combo no lo utilizo para grabar los datos en
					//la Base de Datos
					if (LaConexion[iIndiceModificar].NombreControl.Name != "cbbCombo")
					{
						//***********************************************************************
						//Aqu� comprobar las modificaciones con el array
						if (LaConexion[iIndiceModificar].NombreControl is MaskedTextBox)
						{
							if (aComparacion[iIndiceModificar] != Convert.ToString(LaConexion[iIndiceModificar].NombreControl.FormattedText()))
							{
								if (ComprobarNumericos(LaConexion[iIndiceModificar].NombreCampo))
								{
									if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
										//                        aActualizaSpread(iIndiceModificar) = ""
									}
									else
									{
										lNumerico = LaConexion[iIndiceModificar].NombreControl.FormattedText();
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = Convert.ToDouble(lNumerico);
										//                        aActualizaSpread(iIndiceModificar) = CStr(lNumerico)
									}
								}
								else
								{

									if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
										//                        aActualizaSpread(iIndiceModificar) = ""
									}
									else
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = LaConexion[iIndiceModificar].NombreControl.FormattedText();
										//                        aActualizaSpread(iIndiceModificar) = LaConexion(iIndiceModificar).NombreControl.FormattedText
									}
								}
							}
							else
							{
								//Son iguales
								//                aActualizaSpread(iIndiceModificar) = aComparacion(iIndiceModificar)
							}
						}
						else
						{
							if (aComparacion[iIndiceModificar] != Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text))
							{
								if (ComprobarNumericos(LaConexion[iIndiceModificar].NombreCampo))
								{
									if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
										//                        aActualizaSpread(iIndiceModificar) = ""
									}
									else
									{
										lNumerico = LaConexion[iIndiceModificar].NombreControl.Text;
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = Convert.ToDouble(lNumerico);
										//                        aActualizaSpread(iIndiceModificar) = CStr(lNumerico)
									}
								}
								else
								{

									if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
										//                        aActualizaSpread(iIndiceModificar) = ""
									}
									else
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = LaConexion[iIndiceModificar].NombreControl.Text;
										//                        aActualizaSpread(iIndiceModificar) = LaConexion(iIndiceModificar).NombreControl.Text
									}
								}
							}
							else
							{
								//Son iguales
								//                aActualizaSpread(iIndiceModificar) = aComparacion(iIndiceModificar)
							}
						}
						i++;
					}
					else
					{
						//        aActualizaSpread(iIndiceModificar) = LaConexion(iIndiceModificar).NombreControl.Text
					}
					MiControl[iIndiceModificar] = null;
				}

                try
                {
                    //oscar 04/06/2004
                    string sql = String.Empty;
                    if (vfCambiosPassword && vstNombreTabla == "SUSUARIO")
                    {
                        sql = "INSERT INTO SUSUCCON (FCAMBIO, GUSUARIO, NPASSWORD) " +
                              " VALUES (GETDATE(), '" + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gusuario"]) + "','" + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["vpassword"]) + "')";
                        SqlCommand tempCommand = new SqlCommand(sql, ModuloMantenim.RcManten);
                        tempCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception excep)
                {
                    if (excep.InnerException is SqlException)
                    {
                        var errors = new RDO_rdoErrors(excep.InnerException as SqlException);

                        if (errors.rdoErrors.Count > 1)
                        {
                            //Si el error es que hay una clave
                            //referenci�ndose en otra tabla
                            if (errors.rdoErrors[1].Number == 547)
                            {
                                res = RadMessageBox.Show("Si modifica el elemento seleccionado, puede cambiar el significado del mismo. �Desea continuar?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (res == System.Windows.Forms.DialogResult.Yes)
                                {
                                }
                                else
                                {
                                    ModuloMantenim.RcManten.RollbackTransScope();
                                    return result;
                                }
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                                return result;
                            }
                        }
                        else
                        {
                            if (errors.rdoErrors[0].Number == 431)
                            {
                                if (!ModuloMantenim.DesHabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                    p = ModificaUpdate(ModuloMantenim.RsManten2, tempAdapter_2);
                                }
                                if (!ModuloMantenim.HabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                }
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                            }
                        }
                    }
                }
                //----------------

                //Oscar
                //Parece ser que se modifica un registro de la BD, por lo que se tiene
                //que auditar. Se hace antes sel update, porque en caso de producirse un error
                //en la actualizacion, se hace un rollback de la transaccion
                try
                {
                    ModuloMantenim.AUDITARMOVIMIENTO("M", vstNombreTabla, Serrores.VVstUsuarioApli, ModuloMantenim.RsManten2);
                }
                catch (Exception excep)
                {
                    if (excep.InnerException is SqlException)
                    {
                        var errors = new RDO_rdoErrors(excep.InnerException as SqlException);

                        if (errors.rdoErrors.Count > 1)
                        {
                            //Si el error es que hay una clave
                            //referenci�ndose en otra tabla
                            if (errors.rdoErrors[1].Number == 547)
                            {
                                res = RadMessageBox.Show("Si modifica el elemento seleccionado, puede cambiar el significado del mismo. �Desea continuar?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (res == System.Windows.Forms.DialogResult.Yes)
                                {
                                }
                                else
                                {
                                    ModuloMantenim.RcManten.RollbackTransScope();
                                    return result;
                                }
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                                return result;
                            }
                        }
                        else
                        {
                            if (errors.rdoErrors[0].Number == 431)
                            {
                                if (!ModuloMantenim.DesHabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                    p = ModificaUpdate(ModuloMantenim.RsManten2, tempAdapter_2);
                                }
                                if (!ModuloMantenim.HabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                }
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                            }
                        }
                    }
                }
                //---------------
                try
                {
                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                    tempAdapter_2.Update(ModuloMantenim.RsManten2, ModuloMantenim.RsManten2.Tables[0].TableName);
                    ModuloMantenim.RsManten2.Close();
                }
                catch (Exception excep)
                {
                    if (excep.InnerException is SqlException)
                    {
                        var errors = new RDO_rdoErrors(excep.InnerException as SqlException);

                        if (errors.rdoErrors.Count > 1)
                        {
                            //Si el error es que hay una clave
                            //referenci�ndose en otra tabla
                            if (errors.rdoErrors[1].Number == 547)
                            {
                                res = RadMessageBox.Show("Si modifica el elemento seleccionado, puede cambiar el significado del mismo. �Desea continuar?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question);
                                if (res == System.Windows.Forms.DialogResult.Yes)
                                {
                                }
                                else
                                {
                                    ModuloMantenim.RcManten.RollbackTransScope();
                                    return result;
                                }
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                                return result;
                            }
                        }
                        else
                        {
                            if (errors.rdoErrors[0].Number == 431)
                            {
                                if (!ModuloMantenim.DesHabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                    p = ModificaUpdate(ModuloMantenim.RsManten2, tempAdapter_2);
                                }
                                if (!ModuloMantenim.HabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                    //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
                                }
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                            }
                        }
                    }
                }


				if (p)
				{
					iIndiceModificar = 0;
					i = 0;

					//Bucle For para Modificar en la DB.
					for (iIndiceModificar = 0; iIndiceModificar <= sprMultiple.MaxCols - 1; iIndiceModificar++)
					{
						MiControl = new Control[iIndiceModificar + 1];
						MiControl[iIndiceModificar] = LaConexion[iIndiceModificar].NombreControl;

						//Si el control es combo no lo utilizo para grabar los datos en
						//la Base de Datos
						if (LaConexion[iIndiceModificar].NombreControl.Name != "cbbCombo")
						{
							//***********************************************************************
							//Aqu� comprobar las modificaciones con el array
							if (LaConexion[iIndiceModificar].NombreControl is MaskedTextBox)
							{
								if (aComparacion[iIndiceModificar] != Convert.ToString(LaConexion[iIndiceModificar].NombreControl.FormattedText()))
								{
									if (ComprobarNumericos(LaConexion[iIndiceModificar].NombreCampo))
									{
										if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
										{
											//                            RsManten2(i) = Null
											aActualizaSpread[iIndiceModificar] = "";
										}
										else
										{
											lNumerico = LaConexion[iIndiceModificar].NombreControl.FormattedText();
											//                            RsManten2(i) = lNumerico
											aActualizaSpread[iIndiceModificar] = Convert.ToString(lNumerico);
										}
									}
									else
									{

										if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
										{
											//                            RsManten2(i) = Null
											aActualizaSpread[iIndiceModificar] = "";
										}
										else
										{
											//                            RsManten2(i) = LaConexion(iIndiceModificar).NombreControl.FormattedText
											aActualizaSpread[iIndiceModificar] = Convert.ToString(LaConexion[iIndiceModificar].NombreControl.FormattedText());
										}
									}
								}
								else
								{
									//Son iguales
									aActualizaSpread[iIndiceModificar] = aComparacion[iIndiceModificar];
								}
							}
							else
							{
								if (aComparacion[iIndiceModificar] != Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text))
								{
									if (ComprobarNumericos(LaConexion[iIndiceModificar].NombreCampo))
									{
										if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
										{
											//                            RsManten2(i) = Null
											aActualizaSpread[iIndiceModificar] = "";
										}
										else
										{
											lNumerico = LaConexion[iIndiceModificar].NombreControl.Text;
											//                            RsManten2(i) = lNumerico
											aActualizaSpread[iIndiceModificar] = Convert.ToString(lNumerico);
										}
									}
									else
									{

										if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
										{
											//                            RsManten2(i) = Null
											aActualizaSpread[iIndiceModificar] = "";
										}
										else
										{
											//                            RsManten2(i) = LaConexion(iIndiceModificar).NombreControl.Text
											aActualizaSpread[iIndiceModificar] = Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text);
										}
									}
								}
								else
								{
									//Son iguales
									aActualizaSpread[iIndiceModificar] = aComparacion[iIndiceModificar];
								}
							}
							i++;
						}
						else
						{
							aActualizaSpread[iIndiceModificar] = Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text);
						}
						MiControl[iIndiceModificar] = null;
					}
					ModuloMantenim.ActualizarSpread(aActualizaSpread, this);
					result = true;
				}


				return result;
			}
			catch (Exception excep)
			{
				
			}
			return false;
		}

		private void SustituirEnSpreadFK(string StComando1, string StComando2, int iLongitudColumna, bool fDimensionarCombo, ModuloMantenim.StrFKCombo[] AStColFKCombo, ModuloMantenim.StrFK[] AStColFK, int iIndiceCombo)
		{
			DataSet RsFK = null;
			bool fAND = false;
			StringBuilder StComando = new StringBuilder();

			string StSelect = "Select ";

			int k = 1;

			while (k <= sprMultiple.MaxRows)
			{
				//Monto la sentencia WHERE para cada fila de la spread y
				//Oculto las columnas de la spread que son FK

				StComando = new StringBuilder(" where ");
				foreach (ModuloMantenim.StrFK AStColFK_item in AStColFK)
				{

					if (fAND)
					{
						StComando.Append(" and ");
					}
					fAND = true;

					//Ademas de a�adir cada columna al WHERE de la sentencia,
					//busco el n� de columna en el spread a la que corresponde
					//cada campo que es FK y la oculto.
					//Tambi�n oculto el control asociado a esa columna
					for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].gcolumncaman.Trim() == AStColFK_item.StColHija.Trim())
						{

							//La booleana fDimensionarCombo, me dice si tengo que redimensionar o no
							//el array, ya que la 1� vez est� dimensionado
							if (fDimensionarCombo)
							{
								AStColFKCombo = ArraysHelper.RedimPreserve(AStColFKCombo, new int[]{AStColFKCombo.GetUpperBound(0) + 2});
							}

							fDimensionarCombo = true;

							//Esto me sirve luego para poder identificar cada columna
							//que es clave a que combo pertenece y que n� de columna
							//ocupa en ese combo
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StColHija = AStColFK_item.StColHija.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].iIndiceCombo = (short) iIndiceCombo;

							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StLabelCombo = AStColFK_item.StLabelCombo.Trim();

							sprMultiple.Col = astrElConjunto[i].nordencoltab;
							sprMultiple.SetColHidden(sprMultiple.Col, true);
							astrElConjunto[i].NombreControl.Visible = false;

							StComando.Append("A." + AStColFK_item.StColPadre + "=");

							//SI ES "CHAR", "VARCHAR" O "DATETIME",
							//EL DATO VA ENTRE COMILLA SIMPLE
							if (AStColFK_item.stTipoDato.Trim() == "VARCHAR" || AStColFK_item.stTipoDato.Trim() == "CHAR")
							{
								StComando.Append("'" + sprMultiple.Text.Trim() + "'");
							}
							else if (AStColFK_item.stTipoDato.Trim() == "DATETIME")
							{ 
								StComando.Append("'" + DateTime.Parse(sprMultiple.Text).ToString("MM/dd/yyyy") + "'");
							}
							else
							{
								StComando.Append(sprMultiple.Text.Trim());
							}

							//Salgo del bucle FOR porque ya ha encontrado la columna
							break;
						}
					}
				}


				//Monto la consulta con la sentencia SELECT, el WHERE y el ORDER BY
				StComando = new StringBuilder(StSelect + StComando1 + StComando.ToString() + " " + StComando2);

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
				RsFK = new DataSet();
				tempAdapter.Fill(RsFK);

				//Inserto columnas con los datos que tienen que mostrarse
				//en vez de las FK

				sprMultiple.Col++;
				sprMultiple.setTypeEditLen(iLongitudColumna);
				sprMultiple.SetColWidth(sprMultiple.Col, AStColFK[0].StLabelCombo.Length);

				sprMultiple.Row = k;
				sprMultiple.Text = "";
				for (int i = 0; i <= RsFK.Tables[0].Columns.Count - 1; i++)
				{
					sprMultiple.Text = sprMultiple.Text + Convert.ToString(RsFK.Tables[0].Rows[k][i]) + " ";
				}

				RsFK.Close();

				k++;
			}

			//N� de �ndice de ControlCombo
			iIndiceCombo++;

		}

		private void tbTexto_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbTexto, eventSender);

			if (tbTexto[Index].Text.Trim() != "" && tbTexto[Index].Enabled)
			{
				cbBotones[2].Enabled = true;
			}

			//Comprobamos que est� la Spread habilitado porque es se�al
			//de que se ha disparado el evento Change al llenar los campos
			//desde la Spread.
			if (!tbTexto[Index].Enabled)
			{
				return;
			}

			//Si la variable vfCambiosPassword est� a true es se�al de
			//que hemos encriptado

			//***************************CRIS***********************************
			//Creo que lo que hace es:
			//Si vfCambiosPassword es falso, es que no se ha producido ning�n
			//cambio en la password, pero se est� produciendo ahora. Entonces
			//pone vfCambios = True para que luego pregunte si se quieren salvar
			//los cambios, y mira si el campo es password, ya que si tiene PassWordChar
			//es campo password. Si es password pone vfCambiosPassword = True
			//para que con la proxima letra que introduzcamos no se repita
			//todo el proceso
			//******************************************************************
			if (vfCambiosPassword)
			{
				return;
			}

			vfCambios = true;

			if (Convert.ToString(tbTexto[Index].PasswordChar.ToString()) == "*")
			{
				vfCambiosPassword = true;
			}


		}

		private void tbTexto_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbTexto, eventSender);
			tbTexto[Index].SelectionStart = 0;
			tbTexto[Index].SelectionLength = tbTexto[Index].MaxLength;
			if (ActiveControl == tbTexto[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}
		}


		public void CrearCampoFecha()
		{

			if (sdcFecha.GetUpperBound(0) == 0)
			{
				//vfComprFecha es un comprobador por si hemos creado ya
				//el primer control
				if (!vfComprFecha)
				{
					LaConexion[iIndice].NombreControl = sdcFecha[0];
					astrElConjunto[iIndice].NombreControl = sdcFecha[0];
					sdcFecha[0].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
					sdcFecha[0].Text = "";

					iIndiceFecha = 1;
					vfComprFecha = true;
					return;
				}
			}
			ControlArrayHelper.LoadControl(this, "sdcFecha", iIndiceFecha);
			LaConexion[iIndice].NombreControl = sdcFecha[iIndiceFecha];
			astrElConjunto[iIndice].NombreControl = sdcFecha[iIndiceFecha];
			sdcFecha[iIndiceFecha].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
			sdcFecha[iIndiceFecha].Text = "";
			iIndiceCampoFecha = iIndiceFecha;
			iIndiceFecha++;

		}

		public void CrearCampoMultilinea(int ILineas)
		{
			if (tbMultilinea.GetUpperBound(0) == 0)
			{
				if (!vfComprMultilinea)
				{
					//**************************CRIS***************************
					tbMultilinea[0].Width = (int) (((ModuloMantenim.COiTamCaractEtiqueta * ModuloMantenim.COiMaxTamLineaMultiline) + ModuloMantenim.COiTamBordes + ModuloMantenim.COiTamScrollBarMultiline) / 15);
					//*********************************************************
					tbMultilinea[0].MaxLength = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
					LaConexion[iIndice].NombreControl = tbMultilinea[0];
					astrElConjunto[iIndice].NombreControl = tbMultilinea[0];
					tbMultilinea[0].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
					//**************************CRIS*****************************
					//El Height que tiene el multil�nea por defecto es para 3 l�neas
					//luego si lo queremos pintar de dos l�neas lo dividimos entre
					//3 y lo multilicamos por 2
					if (ILineas == 2)
					{
						tbMultilinea[0].Height = (int) ((ModuloMantenim.COiHeightMultiline3 / ((int) 3)) * 2 / 15);
					}
					else
					{
						tbMultilinea[0].Height = (int) (ModuloMantenim.COiHeightMultiline3 / 15);
					}
					//************************************************************
					iIndiceMultilinea = 1;
					vfComprMultilinea = true;
					return;
				}
				iIndiceCampoMultilinea++;
			}

			ControlArrayHelper.LoadControl(this, "tbMultilinea", iIndiceMultilinea);
			//**************************CRIS***************************
			tbMultilinea[iIndiceMultilinea].Width = (int) (((ModuloMantenim.COiTamCaractEtiqueta * ModuloMantenim.COiMaxTamLineaMultiline) + ModuloMantenim.COiTamBordes + ModuloMantenim.COiTamScrollBarMultiline) / 15);
			//*********************************************************
			tbMultilinea[iIndiceMultilinea].MaxLength = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));

			LaConexion[iIndice].NombreControl = tbMultilinea[iIndiceMultilinea];
			astrElConjunto[iIndice].NombreControl = tbMultilinea[iIndiceMultilinea];
			tbMultilinea[iIndiceMultilinea].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;

			//**************************CRIS*****************************
			//El Height que tiene el multil�nea por defecto es para 3 l�neas
			//luego si lo queremos pintar de dos l�neas lo dividimos entre
			//3 y lo multilicamos por 2
			if (ILineas == 2)
			{
				tbMultilinea[iIndiceMultilinea].Height = (int) ((ModuloMantenim.COiHeightMultiline3 / ((int) 3)) * 2 / 15);
			}
			else
			{
				tbMultilinea[iIndiceMultilinea].Height = (int) (ModuloMantenim.COiHeightMultiline3 / 15);
			}
			//************************************************************

			iIndiceCampoMultilinea = iIndiceMultilinea;
			iIndiceMultilinea++;


		}

		public void CrearCampoTexto()
		{

			if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "S")
			{
				tbTexto[0].PasswordChar = '*';
			}
			if (tbTexto.GetUpperBound(0) == 0)
			{
				if (!vfComprTexto)
				{
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) == 1)
					{
						tbTexto[0].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
					}
					else
					{
						tbTexto[0].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
						tbTexto[0].MaxLength = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
						//Le pondremos 2 caracteres(signo y separaci�n de decimales
						//si es num�rico
						if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
						{
							tbTexto[0].MaxLength += 2;
						}
					}

					//**********************CRIS*******************************************
					//Para todos aquellos campos que tengan 10 caracters o menos
					//le a�adiremos a su ancho un caracter m�s, porque est� medido con el
					//ancho de la X
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) <= 10)
					{
						tbTexto[0].Width = (int) (tbTexto[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					}
					//*********************************************************************

					LaConexion[iIndice].NombreControl = tbTexto[0];
					astrElConjunto[iIndice].NombreControl = tbTexto[0];

					if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "S")
					{
						tbTexto[0].PasswordChar = '*';
					}

					tbTexto[0].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
					iIndiceCampoTexto = 1;
					vfComprTexto = true;
					return;
				}
				iIndiceTexto++;
			}
			ControlArrayHelper.LoadControl(this, "tbTexto", iIndiceCampoTexto);
			if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) == 1)
			{
				tbTexto[iIndiceCampoTexto].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
			}
			else
			{
				tbTexto[iIndiceCampoTexto].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
				tbTexto[iIndiceCampoTexto].MaxLength = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
				//Le pondremos 2 caracteres(signo y separaci�n de decimales
				//si es num�rico
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
				{
					tbTexto[iIndiceCampoTexto].MaxLength += 2;
				}

			}

			//**********************CRIS*******************************************
			//Para todos aquellos campos que tengan 10 caracters o menos
			//le a�adiremos a su ancho un caracter m�s, porque est� medido con el
			//ancho de la X
			if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) <= 10)
			{
				tbTexto[iIndiceCampoTexto].Width = (int) (tbTexto[iIndiceCampoTexto].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
			}
			//*********************************************************************

			LaConexion[iIndice].NombreControl = tbTexto[iIndiceCampoTexto];
			astrElConjunto[iIndice].NombreControl = tbTexto[iIndiceCampoTexto];

			if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "S")
			{
				tbTexto[iIndiceCampoTexto].PasswordChar = '*';
			}

			tbTexto[iIndiceCampoTexto].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
			iIndiceTexto = iIndiceCampoTexto;
			iIndiceCampoTexto++;

		}

		private void PintarCampoTexto()
		{
			int iValorTop = 0;

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				iValorTop = ModuloMantenim.COiSuperiorCampo;
			}
			else
			{
				iValorTop = ModuloMantenim.COiSeparacion;
			}

			if (tbTexto.GetUpperBound(0) == 0)
			{
				tbTexto[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				tbTexto[0].Left = (int) (viNuevoLeft / 15);
				tbTexto[0].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				//        Cbb_AlgoNada(iIndiceOption).Visible = AlgoNadaVisible
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (tbTexto[0].Top * 15)) + ((float) (tbTexto[0].Height * 15)));
			}
			else
			{
				tbTexto[iIndiceTexto].Top = (int) ((iValorTop + viNuevoTop) / 15);
				tbTexto[iIndiceTexto].Left = (int) (viNuevoLeft / 15);
				tbTexto[iIndiceTexto].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				//        Cbb_AlgoNada(iIndiceOption).Visible = AlgoNadaVisible
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (tbTexto[iIndiceTexto].Top * 15)) + ((float) (tbTexto[iIndiceTexto].Height * 15)));

			}
			//***************************CRIS**************************
			AlturaFrame(tbTexto[iIndiceTexto]);
			//***************************CRIS**************************
		}

		private void PintarCampoMultilinea(int ILineas)
		{
			int iValorTop = 0;

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				iValorTop = ModuloMantenim.COiSuperiorCampo;
			}
			else
			{
				iValorTop = ModuloMantenim.COiSeparacion;
			}

			if (tbMultilinea.GetUpperBound(0) == 0)
			{
				tbMultilinea[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				tbMultilinea[0].Left = (int) (viNuevoLeft / 15);
				tbMultilinea[0].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				//        Cbb_AlgoNada(iIndiceOption).Visible = AlgoNadaVisible
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (tbMultilinea[0].Top * 15)) + ((float) (tbMultilinea[0].Height * 15)));
			}
			else
			{

				tbMultilinea[iIndiceCampoMultilinea].Top = (int) ((iValorTop + viNuevoTop) / 15);
				tbMultilinea[iIndiceCampoMultilinea].Left = (int) (viNuevoLeft / 15);
				tbMultilinea[iIndiceCampoMultilinea].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				//        Cbb_AlgoNada(iIndiceOption).Visible = AlgoNadaVisible
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (tbMultilinea[iIndiceCampoMultilinea].Top * 15)) + ((float) (tbMultilinea[iIndiceCampoMultilinea].Height * 15)));
			}

			//***************************CRIS**************************
			AlturaFrame(tbMultilinea[iIndiceCampoMultilinea]);
			//*********************************************************
		}

		private void PintarCampoFecha()
		{
			int iValorTop = 0;

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				iValorTop = ModuloMantenim.COiSuperiorCampo;
			}
			else
			{
				iValorTop = ModuloMantenim.COiSeparacion;
			}

			if (sdcFecha.GetUpperBound(0) == 0)
			{
				sdcFecha[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				sdcFecha[0].Left = viNuevoLeft;
				sdcFecha[0].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				//        Cbb_AlgoNada(iIndiceOption).Visible = AlgoNadaVisible
				iIndiceOption++;
				//********************************************************

				viNuevoTop = Convert.ToInt32(((float) (sdcFecha[0].Top * 15)) + sdcFecha[0].Height);
			}
			else
			{
				sdcFecha[iIndiceCampoFecha].Top = (int) ((iValorTop + viNuevoTop) / 15);
				sdcFecha[iIndiceCampoFecha].Left = viNuevoLeft;
				sdcFecha[iIndiceCampoFecha].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				//        Cbb_AlgoNada(iIndiceOption).Visible = AlgoNadaVisible
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (sdcFecha[iIndiceCampoFecha].Top * 15)) + sdcFecha[iIndiceCampoFecha].Height);
			}
			sdcFecha[iIndiceCampoFecha].Text = "";
			//***************************CRIS**************************
			AlturaFrame(sdcFecha[iIndiceCampoFecha]);
			//***************************CRIS**************************
		}





		public void LimpiarCampos()
		{
			int iIndiceLimpiar = 0;

			int tempForVar = tbTexto.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar; iIndiceLimpiar++)
			{
				if (tbTexto[iIndiceLimpiar].Enabled)
				{
					tbTexto[iIndiceLimpiar].Text = "";
				}
			}

			iIndiceLimpiar = 0;
			int tempForVar2 = mebMascara.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar2; iIndiceLimpiar++)
			{
				if (mebMascara[iIndiceLimpiar].Enabled)
				{
					mebMascara[iIndiceLimpiar].Text = "";
				}
			}

			iIndiceLimpiar = 0;
			int tempForVar3 = sdcFecha.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar3; iIndiceLimpiar++)
			{
				if (sdcFecha[iIndiceLimpiar].Enabled)
				{
					sdcFecha[iIndiceLimpiar].Text = "";
				}
			}

			iIndiceLimpiar = 0;
			int tempForVar4 = tbMultilinea.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar4; iIndiceLimpiar++)
			{
				if (tbMultilinea[iIndiceLimpiar].Enabled)
				{
					tbMultilinea[iIndiceLimpiar].Text = "";
				}
			}

			iIndiceLimpiar = 0;
			int tempForVar5 = cbbCombo.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar5; iIndiceLimpiar++)
			{
				if (cbbCombo[iIndiceLimpiar].Enabled)
				{
					cbbCombo[iIndiceLimpiar].Text = "";
				}
			}


		}

		public void LlenarCampos()
		{
			bool fNumerico = false;
			if (sprMultiple.Row == 0)
			{
				return;
			}

			int iIndiceLlenado = 0;
			Control[] MiControl = null;
			//Inicializo a = para usar las mismas variales
			iIndiceTexto = 0;
			iIndiceCampoMultilinea = 0;
			iIndiceCampoFecha = 0;
			iIndiceCampoMascara = 0;
			string signo = ModuloMantenim.stSignoDecimal;

			//Bucle For para llenar los controles
			int iPosi = 0;
			int iPosicion = 0;
			string StEntero = String.Empty;
			int Idecimal = 0;
			int IPosiMask = 0;
			for (iIndiceLlenado = 0; iIndiceLlenado <= (sprMultiple.MaxCols - 1); iIndiceLlenado++)
			{
				fNumerico = false;
				sprMultiple.Col = iIndiceLlenado + 1;
				MiControl = new Control[iIndiceLlenado + 1];
				MiControl[iIndiceLlenado] = LaConexion[iIndiceLlenado].NombreControl;

				if (MiControl[iIndiceLlenado].Name == "sdcFecha")
				{
					LaConexion[iIndiceLlenado].NombreControl.Text = ModuloMantenim.FormatearFecha(aLongitud[iIndiceLlenado], sprMultiple.Text);
					//***************CRIS********************************************
					//Comprobamos si el control es Masked Edit
				}
				else if (MiControl[iIndiceLlenado].Name == "mebMascara")
				{ 

					//Si es Masked Edit,compruebo que el dato sea num�rico
					foreach (ModuloMantenim.strConjuntoResultados astrElConjunto_item in astrElConjunto)
					{
						if (astrElConjunto_item.NombreControl.Name == MiControl[iIndiceLlenado].Name && ControlArrayHelper.GetControlIndex(astrElConjunto_item.NombreControl) == ControlArrayHelper.GetControlIndex(MiControl[iIndiceLlenado]))
						{
							if (astrElConjunto_item.gtipodato.Trim() == "NUMERIC" || astrElConjunto_item.gtipodato.Trim() == "SMALLINT" || astrElConjunto_item.gtipodato.Trim() == "INT")
							{
								fNumerico = true;
								iPosi = (sprMultiple.Text.IndexOf('.') + 1);
								if (iPosi != 0)
								{
									signo = ".";
								}
								else
								{
									iPosi = (sprMultiple.Text.IndexOf(',') + 1);
									signo = ",";
								}
							}
							break;
						}
					}
					iPosi = 0;
					//Compruebo si es decimal buscando la posici�n del punto
					iPosi = (sprMultiple.Text.IndexOf(signo, StringComparison.CurrentCultureIgnoreCase) + 1);

					//Si el dato es num�rico y decimal
					if (iPosi != 0 && fNumerico)
					{

						iPosicion = (sprMultiple.Text.IndexOf(signo, StringComparison.CurrentCultureIgnoreCase) + 1);
						StEntero = sprMultiple.Text.Substring(0, Math.Min(iPosicion - 1, sprMultiple.Text.Length));
						IPosiMask = (Convert.ToString(LaConexion[iIndiceLlenado].NombreControl.Mask()).IndexOf(signo, StringComparison.CurrentCultureIgnoreCase) + 1);
						if (iPosicion + 1 == IPosiMask)
						{
							Idecimal = 0;
						}
						else
						{
							double dbNumericTemp = 0;
							if (Double.TryParse(sprMultiple.Text.Substring(iPosicion), NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
							{
								Idecimal = Convert.ToInt32(Double.Parse(sprMultiple.Text.Substring(iPosicion).Substring(0, Math.Min(4, sprMultiple.Text.Substring(iPosicion).Length)).Trim()));
							}
							else
							{
								Idecimal = 0;
							}
						}

						for (int i = 1; i <= (IPosiMask - StEntero.Length) - 1; i++)
						{
							StEntero = " " + StEntero;
						}
						LaConexion[iIndiceLlenado].NombreControl.Text = StEntero + ModuloMantenim.stSignoDecimal + Idecimal.ToString();

					}
					else
					{
						LaConexion[iIndiceLlenado].NombreControl.Text = sprMultiple.Text;
					}
				}
				else
				{
					//********************************************************************
					LaConexion[iIndiceLlenado].NombreControl.Text = sprMultiple.Text;
				}

			}

			for (iindice2 = 0; iindice2 <= (iIndiceLlenado - 1); iindice2++)
			{
				MiControl[iindice2] = null;
			}

			iindice2 = 0;
			iIndiceTexto = 0;
			iIndiceCampoMultilinea = 0;
			iIndiceCampoFecha = 0;
			iIndiceCampoMascara = 0;

		}

		private void DesactivarBotonesSpread()
		{

			for (int iIndiceBotones = 0; iIndiceBotones <= 4; iIndiceBotones++)
			{
				cbSpread[iIndiceBotones].Enabled = false;
			}

			//oscar 04/03/03
			sprMultiple.Enabled = false;
			//----------
		}

		public void ActivarBotonesSpread()
		{

			for (int iIndiceBotones = 0; iIndiceBotones <= 4; iIndiceBotones++)
			{
				cbSpread[iIndiceBotones].Enabled = true;
			}

			//oscar 04/03/03
			sprMultiple.Enabled = true;
			//-----------
		}

		private void ImprimirListado()
		{
			int i = 0;
			string stCamino = String.Empty;
			string stRPTVertical = String.Empty;
			string stRPTHorizontal = String.Empty;
			string stRPTHorizontal6 = String.Empty;
			int iLongitud = 0;
			string stListado = String.Empty;
			int Intentos = 0;
			//***************CRIS*************
			string StCabecera = String.Empty;
            //********************************
            int resumeStatement = 1;
            do
            {
                try
                {
                    Intentos = 0;
                    ModuloMantenim.RegistrarDSN();

                    stCamino = ModuloMantenim.vStPathReport;
                    stRPTVertical = "\\smt100f1v.rpt";
                    stRPTHorizontal = "\\smt100f1h.rpt";
                    stRPTHorizontal6 = "\\smt100f1h6.rpt";


                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    StringBuilder stFila = new StringBuilder();
                    string stCadaUno = String.Empty;

                    //Cargamos el array de las longitudes de los campos en un nuevo array
                    aLongitudListado = new int[aLongitud.GetUpperBound(0) + 1];

                    for (i = 0; i <= aLongitud.GetUpperBound(0); i++)
                    {
                        aLongitudListado[i] = aLongitud[i];
                    }

                    i = 0;

                    //Colocamos la spread en la primera celda.

                    // ralopezn TODO_X_5 19/05/2016
                    //ModuloMantenim.DBInforme = (Recordset) ModuloMantenim.DB.OpenRecordset("select * from listado where 2=1");

                    sprMultiple.Row = 0;
                    sprMultiple.Col = 1;
                    //For para ir desde la primera fila hasta la �ltima
                    for (int iIndiceInforme = 1; iIndiceInforme <= sprMultiple.MaxRows + 1; iIndiceInforme++)
                    { //He agregado +1
                      //For para concatenar todos los registros de cada fila
                        int tempForVar = sprMultiple.MaxCols;
                        for (int iIndiceFila = 1; iIndiceFila <= tempForVar; iIndiceFila++)
                        {

                            string tempRefParam = sprMultiple.Text.Trim();
                            stCadaUno = SumarBlancos(tempRefParam, sprMultiple.Text.Trim().Length, aLongitudListado[iIndiceFila - 1]);
                            if (stFila.ToString() == "")
                            { //Para evitar el primer blanco
                                stFila = new StringBuilder(stCadaUno);
                                sprMultiple.Col++;
                            }
                            else
                            {
                                stFila.Append(" " + stCadaUno);
                                sprMultiple.Col++;
                            }

                        }
                        //*********************
                        if (sprMultiple.Row == 0)
                        {
                            StCabecera = stFila.ToString().ToUpper();
                        }
                        else
                        {
                            // ralopezn TODO_X_5 19/05/2016
                            //ModuloMantenim.DBInforme.AddNew();
                            //ModuloMantenim.DBInforme["numero"] = (Recordset) sprMultiple.Row;
                            //ModuloMantenim.DBInforme["tipo"] = (Recordset) "L";
                            //ModuloMantenim.DBInforme["linea"] = (Recordset) stFila.ToString();
                            //ModuloMantenim.DBInforme["Cabecera"] = (Recordset) StCabecera;
                            //ModuloMantenim.DBInforme.Update();
                        }
                        //*********************
                        //comprobar si es el �ltimo registro
                        if (sprMultiple.Row == sprMultiple.MaxRows)
                        { //Le he quitado el sprMultiple.MaxRows-1
                            iLongitud = stFila.ToString().Length;
                        }

                        sprMultiple.Row++;
                        stFila = new StringBuilder("");
                        sprMultiple.Col = 1;
                    }
                    // ralopezn TODO_X_5 19/05/2016
                    //ModuloMantenim.DBInforme.Close();
                    //ModuloMantenim.DB.Close();
                    //ModuloMantenim.Wk.Close();

                    ModuloMantenim.cryCrystal.Connect = "dsn=" + ModuloMantenim.CoDSNTemp + ";UID=" + ModuloMantenim.gstUsuario + ";PWD=" + ModuloMantenim.gstPassword + "";

                    if (iLongitud <= 115)
                    {
                        ModuloMantenim.cryCrystal.ReportFileName = stCamino + stRPTVertical;
                    }
                    else if (iLongitud > 115 && iLongitud <= 160)
                    {
                        ModuloMantenim.cryCrystal.ReportFileName = stCamino + stRPTHorizontal;
                    }
                    else
                    {
                        ModuloMantenim.cryCrystal.ReportFileName = stCamino + stRPTHorizontal6;
                    }


                    //Recoger los datos del Hospital para Crystal
                    ModuloMantenim.RecogerDatosHospital();
                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //Incluir una Formula en Crystal para dar al listado la descripci�n
                    //de la tabla de la que crearemos el informe.
                    stListado = "Listado de " + vstDescrLarTabla;
                    ModuloMantenim.cryCrystal.Formulas["Tabla"] = "\"" + stListado + "\"";

                    // ralopezn TODO_X_5 19/05/2016 Crystal
                    //ModuloMantenim.cryCrystal.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
                    ModuloMantenim.cryCrystal.WindowShowPrintSetupBtn = true;
                    ModuloMantenim.cryCrystal.Action = 1;
                    ModuloMantenim.cryCrystal.PrinterStopPage = ModuloMantenim.cryCrystal.PageCount;
                    ModuloMantenim.cryCrystal.Reset();
                    //Set cryCrystal = Nothing
                    resumeStatement = 0;
                }
                catch
                {
                    if (Intentos < 3)
                    {
                        Intentos++;
                        resumeStatement = 1;
                    }
                    else
                    {
                        resumeStatement = 0;
                        return;
                    }
                }
            } while (resumeStatement != 0);
        }
		private string SumarBlancos(string stCadena, int iLongitudCabecera, int iLongitudColumna)
		{
			//Funci�n que ajusta el contenido de un campo al mayor valor
			//entre la etiqueta y el tama�o de la columna que soporta el campo,
			//ampliandolo al mayor de los dos con blancos.
			int iDiferencia = 0;

			if (iLongitudCabecera <= iLongitudColumna)
			{
				iDiferencia = iLongitudColumna - iLongitudCabecera;
				//Bucle para sumarle los espacios de diferencia
				for (int iIndiceSumar = 1; iIndiceSumar <= iDiferencia; iIndiceSumar++)
				{
					stCadena = stCadena + " ";
				}
			}
			else
			{
				aLongitudListado[this.sprMultiple.Col - 1] = iLongitudCabecera;
			}

			return stCadena;

		}

		public void CrearCampoMascara()
		{
			bool fMasUno = true;
			if (mebMascara.GetUpperBound(0) == 0)
			{
				if (!vfComprMascara)
				{
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) == 1)
					{

						mebMascara[0].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
                        mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])));
                        //Le pondremos 1 caracter (signo)
                        //si es num�rico. No va a tener
                        //punto ya que la suma de parte entera y decimal es 1
                        if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
						{
							mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
							mebMascara[0].Width = (int) (mebMascara[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
							fMasUno = false;
						}


					}
					else
					{
						mebMascara[0].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
						mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])));
						//Le pondremos 1 caracter para signo y otro para separaci�n de
						//decimales si la parte decimal<>0 si es num�rico
						if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
						{
							mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
							mebMascara[0].Width = (int) (mebMascara[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
							fMasUno = false;
							if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
							{
								mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
								mebMascara[0].Width = (int) (mebMascara[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
							}

						}
					}
					//**********************CRIS*******************************************
					//Para todos aquellos campos que tengan 10 caracters o menos
					//le a�adiremos a su ancho un caracter m�s, porque est� medido con el
					//ancho de la X. La boolena fMasUno quiere decir que si esta a verdadero
					//es porque el campo no es num�rico, ya que si lo es no hace falta sumarle
					//un caracter mas
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) <= 10 && fMasUno)
					{
						mebMascara[0].Width = (int) (mebMascara[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					}
					//*********************************************************************
					LaConexion[iIndice].NombreControl = mebMascara[0];
					astrElConjunto[iIndice].NombreControl = mebMascara[0];
					mebMascara[0].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
					iIndiceCampoMascara = 1;
					vfComprMascara = true;
					return;
				}
				iIndiceMascara++;
			}
			ControlArrayHelper.LoadControl(this, "mebMascara", iIndiceCampoMascara);
			mebMascara[iIndiceCampoMascara].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
			if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) == 1)
			{
				mebMascara[iIndiceCampoMascara].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
				mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])));
				//Le pondremos 1 caracter (signo)
				//si es num�rico. No va a tener
				//punto ya que la suma de parte entera y decimal es 1
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
				{
					mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
					mebMascara[iIndiceCampoMascara].Width = (int) (mebMascara[iIndiceCampoMascara].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					fMasUno = false;
				}
			}
			else
			{
				mebMascara[iIndiceCampoMascara].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
				mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])));
				//Le pondremos 1 caracter para signo y otro para separaci�n de
				//decimales si la parte decimal<>0 si es num�rico
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
				{
					mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
					mebMascara[iIndiceCampoMascara].Width = (int) (mebMascara[iIndiceCampoMascara].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					fMasUno = false;
					if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
					{
						mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
						mebMascara[iIndiceCampoMascara].Width = (int) (mebMascara[iIndiceCampoMascara].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					}

				}
			}

			//**********************CRIS*******************************************
			//Para todos aquellos campos que tengan 10 caracters o menos
			//le a�adiremos a su ancho un caracter m�s, porque est� medido con el
			//ancho de la X. La boolena fMasUno quiere decir que si esta a verdadero
			//es porque el campo no es num�rico, ya que si lo es no hace falta sumarle
			//un caracter mas
			if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) <= 10 && fMasUno)
			{
				mebMascara[iIndiceCampoMascara].Width = (int) (mebMascara[iIndiceCampoMascara].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
			}
			//*********************************************************************
			LaConexion[iIndice].NombreControl = mebMascara[iIndiceCampoMascara];
			astrElConjunto[iIndice].NombreControl = mebMascara[iIndiceCampoMascara];
			iIndiceMascara = iIndiceCampoMascara;
			iIndiceCampoMascara++;

		}

		private void PintarCampoMascara()
		{
			int iValorTop = 0;

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				iValorTop = ModuloMantenim.COiSuperiorCampo;
			}
			else
			{
				iValorTop = ModuloMantenim.COiSeparacion;
			}

			if (mebMascara.GetUpperBound(0) == 0)
			{
				mebMascara[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				mebMascara[0].Left = (int) (viNuevoLeft / 15);
				mebMascara[0].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				//        Cbb_AlgoNada(iIndiceOption).Visible = AlgoNadaVisible
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (mebMascara[0].Top * 15)) + ((float) (mebMascara[0].Height * 15)));
				ModuloMantenim.AsignarPropiedadControl(mebMascara[0]);
			}
			else
			{

				mebMascara[iIndiceMascara].Top = (int) ((iValorTop + viNuevoTop) / 15);
				mebMascara[iIndiceMascara].Left = (int) (viNuevoLeft / 15);
				mebMascara[iIndiceMascara].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				//        Cbb_AlgoNada(iIndiceOption).Visible = AlgoNadaVisible
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (mebMascara[iIndiceMascara].Top * 15)) + ((float) (mebMascara[iIndiceMascara].Height * 15)));
				ModuloMantenim.AsignarPropiedadControl(mebMascara[iIndiceMascara]);
			}
			//***************************CRIS**************************
			AlturaFrame(mebMascara[iIndiceMascara]);
			//***************************CRIS**************************
		}
		private void CargarEstrucResultados(DataSet Resultado)
		{
			int iIndiceEstructura = 0;

			foreach (DataRow iteration_row in Resultado.Tables[0].Rows)
			{
				astrElConjunto[iIndiceEstructura].gtablamanaut = Convert.ToString(iteration_row["gtablamanaut"]).Trim();
				astrElConjunto[iIndiceEstructura].dtablamancor = Convert.ToString(iteration_row["dtablamancor"]).Trim();
				astrElConjunto[iIndiceEstructura].gcolumncaman = Convert.ToString(iteration_row["gcolumncaman"]).Trim();
				astrElConjunto[iIndiceEstructura].dcoltabmanco = Convert.ToString(iteration_row["dcoltabmanco"]).Trim();
				astrElConjunto[iIndiceEstructura].dcoltabmanla = Convert.ToString(iteration_row["dcoltabmanla"]).Trim();
				astrElConjunto[iIndiceEstructura].nordencoltab = Convert.ToInt16(Double.Parse(Convert.ToString(iteration_row["nordencoltab"]).Trim()));
				astrElConjunto[iIndiceEstructura].gtipodato = Convert.ToString(iteration_row["gtipodato"]).Trim();
				astrElConjunto[iIndiceEstructura].gtipocolumna = Convert.ToString(iteration_row["gtipocolumna"]).Trim();
				astrElConjunto[iIndiceEstructura].nordencolpk = Convert.ToString(iteration_row["nordencolpk"]).Trim();
				astrElConjunto[iIndiceEstructura].nparteentera = Convert.ToInt16(Double.Parse(Convert.ToString(iteration_row["nparteentera"]).Trim()));
				astrElConjunto[iIndiceEstructura].npartedecima = Convert.ToInt16(Double.Parse(Convert.ToString(iteration_row["npartedecima"]).Trim()));
				astrElConjunto[iIndiceEstructura].iadmitenull = Convert.ToString(iteration_row["iadmitenull"]).Trim();

				if (Convert.IsDBNull(iteration_row["valordefecto"]))
				{
					astrElConjunto[iIndiceEstructura].valordefecto = "";
				}
				else
				{
					astrElConjunto[iIndiceEstructura].valordefecto = Convert.ToString(iteration_row["valordefecto"]).Trim();
				}

				astrElConjunto[iIndiceEstructura].itamaniofijo = Convert.ToString(iteration_row["itamaniofijo"]).Trim();

				if (Convert.IsDBNull(iteration_row["vformato"]))
				{
					astrElConjunto[iIndiceEstructura].vformato = "";
				}
				else
				{
					astrElConjunto[iIndiceEstructura].vformato = Convert.ToString(iteration_row["vformato"]).Trim();
				}

				if (Convert.IsDBNull(iteration_row["vmascara"]))
				{
					astrElConjunto[iIndiceEstructura].vmascara = "";
				}
				else
				{
					astrElConjunto[iIndiceEstructura].vmascara = Convert.ToString(iteration_row["vmascara"]).Trim();
				}

				astrElConjunto[iIndiceEstructura].ipassword = Convert.ToString(iteration_row["ipassword"]).Trim();
				astrElConjunto[iIndiceEstructura].imayusculas = Convert.ToString(iteration_row["imayusculas"]).Trim();
				astrElConjunto[iIndiceEstructura].ireqconsulta = Convert.ToString(iteration_row["ireqconsulta"]).Trim();
				astrElConjunto[iIndiceEstructura].icolformato = Convert.ToString(iteration_row["icolformato"]).Trim();
				astrElConjunto[iIndiceEstructura].icolmascara = Convert.ToString(iteration_row["icolmascara"]).Trim();
				astrElConjunto[iIndiceEstructura].nordenacion = Convert.ToInt16(Double.Parse(Convert.ToString(iteration_row["nordenacion"]).Trim()));

				if (Convert.IsDBNull(iteration_row["iascendente"]))
				{
					astrElConjunto[iIndiceEstructura].iascendente = "";
				}
				else
				{
					astrElConjunto[iIndiceEstructura].iascendente = Convert.ToString(iteration_row["iascendente"]).Trim();
				}

				astrElConjunto[iIndiceEstructura].icolfborrado = Convert.ToString(iteration_row["icolfborrado"]).Trim();
				iIndiceEstructura++;
			}
		}

		private void CaracterizarIncorporar()
		{
			//Debemos poner los campos con las caracter�sticas de incorporaci�n
			Control MiControl = null;
			for (int iCaracterizar = 0; iCaracterizar <= astrElConjunto.GetUpperBound(0) - 1; iCaracterizar++)
			{

				//Comprobar si tiene valores por defecto en las incorporaciones
				if (astrElConjunto[iCaracterizar].valordefecto != "")
				{
					MiControl = astrElConjunto[iCaracterizar].NombreControl;
					MiControl.Text = astrElConjunto[iCaracterizar].valordefecto;
				}
				//Comprobar el tipo de dato
				if (astrElConjunto[iCaracterizar].gtipocolumna == "SA" || astrElConjunto[iCaracterizar].icolfborrado.ToString().ToUpper() == "S")
				{
					//Or Trim(UCase(astrElConjunto(iCaracterizar).gcolumncaman)) = Trim(UCase(StFBORRADO)) Then
					astrElConjunto[iCaracterizar].NombreControl.Enabled = false;
				}

			}

		}

		private void CaracterizarModificar(bool fInhabilitarFBORRADO = false)
		{

			for (int iCaracterizar = 0; iCaracterizar <= astrElConjunto.GetUpperBound(0) - 1; iCaracterizar++)
			{

				//Comprobar el tipo de dato
				if (astrElConjunto[iCaracterizar].gtipocolumna == "EN" || astrElConjunto[iCaracterizar].gtipocolumna == "SA")
				{
					astrElConjunto[iCaracterizar].NombreControl.Enabled = false;
				}
				//***************************************CRIS********************************************
				//Miramos si el control es el de fecha de borrado. Si lo es miramos si la variable
				//de inhabilitar fecha de borrado es true, y si lo es lo inhabilitamos, ya que esto
				//significa que la fecha de borrado es nula y solo se puede modificar mediante el borrado
				//    If Trim(UCase(astrElConjunto(iCaracterizar).gcolumncaman)) = Trim(UCase(StFBORRADO))
				if (astrElConjunto[iCaracterizar].icolfborrado.ToString().ToUpper() == "S" && astrElConjunto[iCaracterizar].icolfborrado != Type.Missing)
				{
					if (fInhabilitarFBORRADO)
					{
						astrElConjunto[iCaracterizar].NombreControl.Enabled = false;
					}
				}
				//****************************************************************************************
			}

		}


		private void Descaracterizar()
		{
			//Quitamos todos los atributos definidos anteriormente para los controles

			for (int iCaracterizar = 0; iCaracterizar <= astrElConjunto.GetUpperBound(0) - 1; iCaracterizar++)
			{

				if (!astrElConjunto[iCaracterizar].NombreControl.Enabled)
				{
					astrElConjunto[iCaracterizar].NombreControl.Enabled = true;
				}

			}

		}

		public void SumarSpread()
		{
			//Le sumamos el �ltimo registro incorporado
			int iIndiceSuma = 0;
			Control[] MiControl = null;

			//Habilitamos un registro m�s en la grid
			sprMultiple.MaxRows++;
			sprMultiple.Row = sprMultiple.MaxRows;

			iindice2 = 0;

			//Bucle For para cargar el �ltimo registro de la grid
			for (iIndiceSuma = 0; iIndiceSuma <= (sprMultiple.MaxCols - 1); iIndiceSuma++)
			{
				sprMultiple.Col = iIndiceSuma + 1;
				if (viColumnaPassword == sprMultiple.Col)
				{
					this.sprMultiple.setTypeEditPassword(true);
				}
				MiControl = new Control[iIndiceSuma + 1];
				MiControl[iIndiceSuma] = LaConexion[iIndiceSuma].NombreControl;

				//Recorrer los campos e ir introduciendo los datos en la grid
				if (Convert.IsDBNull(LaConexion[iIndiceSuma].NombreControl.Text))
				{
					sprMultiple.Text = "";
				}
				else
				{
					//*******************************CRIS***********************************
					if (LaConexion[iIndiceSuma].NombreControl is MaskedTextBox)
					{
						sprMultiple.Text = Convert.ToString(LaConexion[iIndiceSuma].NombreControl.FormattedText());
					}
					else
					{
						sprMultiple.Text = Convert.ToString(LaConexion[iIndiceSuma].NombreControl.Text);
					}
					//**********************************************************************
				}

			}

			for (iindice2 = 0; iindice2 <= (iIndiceSuma - 1); iindice2++)
			{
				MiControl[iindice2] = null;
			}

			iindice2 = 0;

		}

		private void GuardarCambios()
		{

			//Se guardan los cambios pendientes antes de cerrar

			switch(stPulsado)
			{
				case "incorporar" : 
					cbBotones_Click(cbBotones[0], new EventArgs());  //Aceptar Incorporar 
					 
					break;
				case "modificar" : 
					cbBotones_Click(cbBotones[0], new EventArgs());  //Aceptar Modificar 
					 
					break;
			}

		}




		private void ComprobarModoTotal()
		{
			//Procedimiento que nos comprueba si es una tabla de s�lo consulta o
			//de acceso total a Incorporar, Modificar y Eliminar.

			if (vstActualizable.ToUpper() == "S")
			{
				//Puede acceder a todo
				return;
			}
			else
			{
				cbSpread[0].Visible = false;
				cbSpread[1].Visible = false;
				cbSpread[2].Visible = false;
			}

		}



		private void tbTexto_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			int Index = Array.IndexOf(this.tbTexto, eventSender);
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].NombreControl.Name == "tbTexto" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
				{
					if (astrElConjunto[i].imayusculas == "S")
					{
						Serrores.ValidarTecla(ref KeyAscii);
						KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]);
					}
					else if (astrElConjunto[i].icolformato == "S")
					{ 
						if (KeyAscii != 8 && KeyAscii != 9 && KeyAscii != 10 && KeyAscii != 13)
						{ //retroceso, tabulador,
							//avance de l�nea y retorno de carro
							ValidarFormato(KeyAscii, astrElConjunto[i].dcoltabmanco, astrElConjunto[i].gtipodato);
							if (!FFormatoValido)
							{
								KeyAscii = 13;
							}
						}
					}
					else if (astrElConjunto[i].icolmascara == "S")
					{ 
						if (KeyAscii != 8 && KeyAscii != 9 && KeyAscii != 10 && KeyAscii != 13)
						{ //retroceso, tabulador,
							//avance de l�nea y retorno de carro
							ValidarMascara(KeyAscii, astrElConjunto[i].dcoltabmanco, astrElConjunto[i].gtipodato);
							if (!FMascaraValida)
							{
								KeyAscii = 13;
							}
						}
					}

					break;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbTexto_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbTexto, eventSender);
			int IposiNegativo = 0;
			int IposiPositivo = 0;

			bool FEncriptar = true;

			if (this.ActiveControl.ToString() == cbBotones[1].Name)
			{
				if (!(ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[1]) && ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[2])) && tbTexto[Index].Text.Trim() != "" && stPulsado != "consultar")
				{ //Si no pulsa cancelar, ni limpiar
					//el campo no est� vacio y el bot�n pulsado no es el de consultar
					//se valida el tama�o fijo
					for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].NombreControl.Name == "tbTexto" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
						{
							if (astrElConjunto[i].itamaniofijo == "S")
							{
								if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
								{
									IposiNegativo = (tbTexto[Index].Text.IndexOf('-') + 1);
									IposiPositivo = (tbTexto[Index].Text.IndexOf('+') + 1);
									if (IposiNegativo != 0 || IposiPositivo != 0)
									{
										FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length + 1, i);
									}
									else
									{
										FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length, i);
									}
								}
								else
								{
									FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length, i);
								}
							}
							break;
						}
					}

					if (FEncriptar)
					{
						//Si ha habido cambios en el campo password encriptamos.
						if (vfCambiosPassword)
						{
							//03/06/2004
							if (tbTexto[Index].Text.Trim().Length < ModuloMantenim.LongitudMinimaPassword())
							{
								short tempRefParam = 1020;
								string[] tempRefParam2 = new string[]{"Longitud de contrase�a", "mayor o igual", ModuloMantenim.LongitudMinimaPassword().ToString()};
								ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
								tbTexto[Index].Focus();
								return;
							}
							//----------
							string tempRefParam3 = tbTexto[Index].Text;
							tbTexto[Index].Text = ModuloMantenim.CambiarContrase�a(ref tempRefParam3);
							vstContrase�a = tbTexto[Index].Text;
						}
					}

				}
			}
			else if (tbTexto[Index].Text.Trim() != "" && stPulsado != "consultar")
			{  //Si no pulsa cancelar, ni limpiar
				//el campo no est� vacio y el bot�n pulsado no es el de consultar
				//se valida el tama�o fijo
				for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
				{
					if (astrElConjunto[i].NombreControl.Name == "tbTexto" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
					{
						if (astrElConjunto[i].itamaniofijo == "S")
						{
							if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
							{
								IposiNegativo = (tbTexto[Index].Text.IndexOf('-') + 1);
								IposiPositivo = (tbTexto[Index].Text.IndexOf('+') + 1);
								if (IposiNegativo != 0 || IposiPositivo != 0)
								{
									FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length + 1, i);
								}
								else
								{
									FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length, i);
								}
							}
							else
							{
								FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length, i);
							}
						}
						break;
					}
				}

				if (FEncriptar)
				{
					//Si ha habido cambios en el campo password encriptamos.
					if (vfCambiosPassword)
					{
						//03/06/2004
						if (tbTexto[Index].Text.Trim().Length < ModuloMantenim.LongitudMinimaPassword())
						{
							short tempRefParam4 = 1020;
							string[] tempRefParam5 = new string[]{"Longitud de contrase�a", "mayor o igual", ModuloMantenim.LongitudMinimaPassword().ToString()};
							ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, ModuloMantenim.RcManten, tempRefParam5);
							tbTexto[Index].Focus();
							return;
						}
						//----------
						string tempRefParam6 = tbTexto[Index].Text;
						tbTexto[Index].Text = ModuloMantenim.CambiarContrase�a(ref tempRefParam6);
						vstContrase�a = tbTexto[Index].Text;
					}
				}

			}
		}

		private void proRecolocaControles()
		{
			this.Top = (int) 0;
			this.Left = (int) 0;
			this.Width = (int) Screen.PrimaryScreen.Bounds.Width;
			this.Height = (int) (Screen.PrimaryScreen.Bounds.Height - 33);
			frmSpread.Height = (int) (this.Height / 2);
			frmSpread.Width = (int) (this.Width - 7);
			cbSpread[0].Left = (int) (frmSpread.Width - cbSpread[0].Width - 13);
			for (int i = 1; i <= 8; i++)
			{
				cbSpread[i].Left = (int) cbSpread[0].Left;
			}
			sprMultiple.Width = (int) (frmSpread.Width - cbSpread[0].Width - 27);
			sprMultiple.Height = (int) (frmSpread.Height - 13);
			frmDetalleFondo.Top = (int) (frmSpread.Height + 7);
			frmDetalleFondo.Height = (int) (this.Height / 2);
			frmBotones.Top = (int) frmDetalleFondo.Top;
			frmBotones.Left = (int) (this.Width - frmBotones.Width - 7);
			frmBotones.Height = (int) frmDetalleFondo.Height;
			FrmDetalleMedio.Top = 10;
			FrmDetalleMedio.Height = (int) (frmDetalleFondo.Height - 13);
			scbvCampos.Height = (int) FrmDetalleMedio.Height;
			scbvCampos.Top = 10;
		}
		private void scbvCampos_Scroll(Object eventSender, ScrollEventArgs eventArgs)
		{
			switch(eventArgs.Type)
			{
				case ScrollEventType.EndScroll : 
					scbvCampos_Change(eventArgs.NewValue); 
					break;
			}
		}
	}
}