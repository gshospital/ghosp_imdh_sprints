using Microsoft.VisualBasic;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace MantenAutomatico
{
	public partial class SMT100F2
		: RadForm
	{

		private SqlCommand _RqSQL = null;
		public SMT100F2()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}


		SqlCommand RqSQL
		{
			get
			{
				if (_RqSQL == null)
				{
					_RqSQL = new SqlCommand();
				}
				return _RqSQL;
			}
			set
			{
				_RqSQL = value;
			}
		}


		int iIndice = 0; //indice para recorrer el Resultset
		int iindice2 = 0; //indice para recorrer las columnas de la Spread
		string stPulsado = String.Empty; //bot�n pulsado en la Spread

		int iIndiceTexto = 0; //<indices para crear cada uno de los controles
		int iIndiceCombo = 0; //<
		int iIndiceFecha = 0; //<
		int iIndiceMultilinea = 0; //<
		int iIndiceEtiqueta = 0; //<
		int iIndiceMascara = 0; //<

		string stCaptionForm = String.Empty; //< Nombre del formulario

		int iIndiceCampoTexto = 0; //<indices de apoyo para crear controles
		int iIndiceCampoFecha = 0; //<
		int iIndiceCampoCombo = 0; //<
		int iIndiceCampoMultilinea = 0; //<
		int iIndiceCampoMascara = 0; //<

		bool vfComprobador = false; //<Booleanos para salir de las compro-
		bool vfComprCombo = false; //<baciones del primer campo creado
		bool vfComprFecha = false; //<
		bool vfComprTexto = false; //<
		bool vfComprMultilinea = false; //<
		bool vfComprMascara = false; //<
		bool vfCambioScroll = false; //<


		int viNuevoTop = 0; //Nuevo Top al cambio de linea
		int viNuevoLeft = 0;

		int viControlarScroll = 0; //Controles del Scroll vertical del Detalle
		int viControlarValorScroll = 0;

		private struct Consultas
		{
			public string Campo;
			public string Busqueda;
			public static Consultas CreateInstance()
			{
					Consultas result = new Consultas();
					result.Campo = String.Empty;
					result.Busqueda = String.Empty;
					return result;
			}
		}

		private SMT100F2.Consultas[] LaConsulta = null;

		//*************************CRIS******************************
		//Variable que me da la diferencia entre la altura del frame contenedor
		//de los otros frames, y la del frame contenedor de los controles
		int iDiferencia = 0;
		//Boolena que me indica si el formato o la m�scara son validos o no
		bool FFormatoValido = false;
		bool FMascaraValida = false;
		//Cadenas que van a tener los caracteres v�lidos para formato y m�scara
		string StFormatoNumero = String.Empty;
		string StFormatoCadena = String.Empty;
		string StFormatoFechaHora = String.Empty;
		string StMascaraNumero = String.Empty;
		string StMascaraCadena = String.Empty;
		string StMascaraFechaHora = String.Empty;

		//Para saber que el vcampo fcha tiene que ser null
		bool fSoloNULL = false;
		//Para saber el indice del control fecha que solo puede ser null
		int iIndexFecha = 0;
		//Para saber si se ha cambiado una fecha de borrado a NULA
		bool fActivarRegistro = false;
		//N� de clicks que se tienen que hacer en la scrollbar para ver
		//todo el detalle de campos
		int lClicks = 0;
		//Integer para saber el indice de los controles OptionButton
		int iIndiceOption = 0;
		//Se encuentran en la tabla de constantes globales
		//Constante ALGONATO valores valfanu1, valfanu2 y valfanu3,
		//y sirven para saber los literales de los combos de consulta
		string StALGO = String.Empty;
		string StNADA = String.Empty;
		string StTODO = String.Empty;
		//Longitud m�xima de las tres variables anteriores
		int IMaxLen = 0;

		int[] aLongitudListado = null;

		//Booleana que indica si hay alg�n control activo
		bool fControlActivo = false;

		SqlCommand Rq = null;

		ModuloMantenim.StrFK[] AStColFK = null;

		ModuloMantenim.StrFKCombo[] AStColFKCombo = null;

		ModuloMantenim.StrOrdenFK[] AStOrdenFK = null;

		RadLabel[] LbControlEtiqueta = null;

		const string CoItemComboNulo = "";

		int viIndiceCombo = 0;
		int vIIndiceColEnCombo = 0;

		//**********************
		//Variable de formulario para crear instancias de �l mismo
		//cuando el programa se llame a s� mismo
		dynamic FrInstanciaFormulario = null;
		object FrInstanciaFormulario2 = null;
		//Variable de Clase para volver a llamarse el programa
		//a s� mismo
		object ClassMantenimiento = null;
		//Variable Control que guarda el nombre del combo que tiene
		//que recoger los datos de cuando se llama a otro mantenimiento
		Control CtrlCombo = null;
		//Columnas que nos pasa el Mantenimeinto Autom�tico cuando
		//viene de combos y sirven para saber que valores tenemos que devolver
		string[] AStColumnasFK = null;
		//Columnas que pasamos cuando llamamos a un Mantenimiento Autom�tico
		//desde un combo, y son las que utiliza para saber los valores que
		//nos tiene que devolver
		string[] AStColumnasFKPadre = null;
		//Valor de las Columnas que pasamos cuando llamamos a un Mantenimiento Autom�tico
		//desde un combo, por si el modo que se llama es MODIFICAR, poder elegir adem�s de
		//los valores activos, este valor actual aunque no est� activo
		string[] AStValorColumnasFKPadre = null;

		//Booleana que sirve para saber si un Mantenimiento Autom�tico viene
		//llamado por un combo o no
		bool fVieneDeCombos = false;

		//Variables que se van a devolver a un combo que
		//haya llamado a Mantenimiento Autom�tico

		//Descripci�n del combo
		string StDescripcion = String.Empty;
		//Valores de las columnas clave
		string[] AStClaves = null;
		//Nombre de las columnas clave
		string[] AStColumnas = null;
		//Tipo de dato de las columnas clave
		string[] AStTipoCol = null;

		//Variable para saber si se debe ejecutar el Form_Activate
		bool fForm_Activate = false;

		//Controles de la pantalla
		//Dim astrElConjunto() As strConjuntoResultados

		//Cantidad de registros que tiene la consulta
		int vlCantidad = 0;
		//Variable para el cambio de contrase�a
		string vstContrase�a = String.Empty;
		//Ser� OR � AND
		string vstCriterioConsulta = String.Empty; //para las consultas
		string[] aComparacion = null;
		string[] aActualizaSpread = null;
		int[] aNumeroColumna = null;
		int[] aLongitud = null;
		//Nombre de la tabla que se est� manteniendo
		public string vstNombreTabla = String.Empty;
		//Descripci�n larga de la tabla que se est� manteniendo
		public string vstDescrLarTabla = String.Empty;
		//Descripci�n corta de la tabla que se est� manteniendo
		public string vstDescrCortaTabla = String.Empty;
		//Variable que nos dice si la tabla es
		//actualizable o es solo de consulta
		public string vstActualizable = String.Empty;
		int viColumnaPassword = 0;
		ModuloMantenim.ConexionControles[] LaConexion = null;
		bool vfCambios = false;
		bool vfCambiosPassword = false;
		public bool vfTodo = false;
		ModuloMantenim.Clave[] AStPK = null;
		int Tope = 0;
		bool CargarMas = false;
		bool NuevaConsulta = false;
		ModuloMantenim.StrOrden[] AStOrden = null;
		bool fOrden = false;
		ModuloMantenim.StrParametro[] AStParametro = null;
		bool fUnicos = false;
		//HelpContextID de la ayuda de cada pantalla
		public int vlAyuda = 0;
		//Para no cerrar el Requery si no se ha abierto
		public bool vfCerrar = false;
		ModuloMantenim.StrColumnasFK[] aStrFK = null;
		ModuloMantenim.StrToolbar[] AStrToolbar = null;
		ModuloMantenim.StrFKUnicos[] AStrFKUnicos = null;

		//Variable que me dice el modo en el que estoy:
		//Incorporar, Modificar o Consultar.
		//La 1� vez que entro lo hago como si fuera a consultar,
		//Esto me sirve para saber la manera en que debo cargar
		//los combos. Con todos los registros o s�lo con los activos
		string StModo = String.Empty;
		//Contiene el nombre de la vista o de la tabla que
		//va a cargar el combo de valores activos
		string StTablaVista = String.Empty;
		string[, ] AStSpread = null;
		int LUltimaFilaSpread = 0;
		int viIndiceComboInhabilitado = 0;
		string StModoCombo = String.Empty;
		//Controles de la pantalla
		ModuloMantenim.strConjuntoResultados[] astrElConjunto = null;

		int iNumeroFilasVisibles = 0;

		public string CodigoClavesSpread(int Columna, int iCombo)
		{
			string result = String.Empty;
			int X = Columna;
			int y = X;
			X--;
			StringBuilder Clave = new StringBuilder();
			sprMultiple.Col = X;
			while (sprMultiple.GetColHidden(sprMultiple.Col) && sprMultiple.Text != "")
			{
				X--;
				sprMultiple.Col = X;
			}
			X++;
			for (int w = X; w <= y - 1; w++)
			{
				sprMultiple.Col = w;
				Clave.Append(sprMultiple.Text + ",");
			}
			if (Clave.ToString() != "")
			{
				Clave = new StringBuilder(Clave.ToString().Substring(0, Math.Min(Clave.ToString().Length - 1, Clave.ToString().Length)));
			}
			result = Clave.ToString();
			sprMultiple.Col = Columna;
			return result;
		}

		public string CodigosClaves(Control InCombo, int indice)
		{

			StringBuilder Clave = new StringBuilder();
            // ralopezn TODO_X_5 20/05/2016
			//int INumClaves = proNumPuntosYComa(1, Strings.Len(Convert.ToString(InCombo.ColumnWidths)), Convert.ToString(InCombo.ColumnWidths));
			//for (int X = 0; X <= INumClaves; X++)
			//{
			//	Clave.Append(Convert.ToString(InCombo.Column(X, indice)).Trim() + ",");
			//}
			return Clave.ToString().Substring(0, Math.Min(Clave.ToString().Length - 1, Clave.ToString().Length));
		}

		private void EncontrarDescripcionCombo(string[] AStValorClave, string[] AStNomColumna, string[] AStTipoCol, int iIndiceCombo)
		{
			//Conlos datos devueltos de la pantalla de MantenimientoAutomatico
			//que hemos llamado desde un combo, obtengo la descripci�n que se
			//corresponde con el valor de las columnas clave recibidas

			string StComando = String.Empty;
			DataSet RsCombos2 = null;
			int iLongitudColumna = 0;
			int iColumna = 0;
			string[] AStCol = null;


			for (int j = 0; j <= AStColFKCombo.GetUpperBound(0); j++)
			{
				if (AStNomColumna[0].Trim() == AStColFKCombo[j].StColPadre.Trim() && iIndiceCombo == AStColFKCombo[j].iIndiceCombo)
				{
					//Si la vista est� a null, los datos se sacan de la tabla
					//padre
					if (AStColFKCombo[j].fVistaNula)
					{
						StComando = "SELECT * from SPREVICMV1 " + "Where gtablacaman = " + "'" + AStColFKCombo[j].StTablaPadre.Trim() + "'" + "AND ISNULL(gvistacaman,'NULL') = 'NULL' " + "order by NORDENCOLTAB ";

						//Una de las condiciones de la select deber�a ser:
						//AND GVISTACAMAN IS NULL, pero esto no funciona con esta vista
						//y hemos tenido que sustituirlo por:
						//ISNULL(gvistacaman,'NULL') = 'NULL'
						SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
						RsCombos2 = new DataSet();
						tempAdapter.Fill(RsCombos2);


						//Montamos la select de las columnas que van a ir en el combo

						StComando = "";
						iLongitudColumna = 0;
						iColumna = 0;
						AStCol = new string[]{String.Empty};
						foreach (DataRow iteration_row in RsCombos2.Tables[0].Rows)
						{
							//S�lo participan en la select para cargar el combo
							//aquellas columnas que sean visibles.
							//Pero en el order by pueden participar tanto las visibles
							//como las no visibles. Como solo necesitamos un registro
							//no vamos a hacer el order by
							if (Convert.ToString(iteration_row["IVISIBCOMTAB"]) == "S")
							{
								StComando = StComando + "A." + Convert.ToString(iteration_row["GCOLUMNCAMAN"]).Trim() + ",";
								AStCol = ArraysHelper.RedimPreserve(AStCol, new int[]{iColumna + 1});
								AStCol[iColumna] = Convert.ToString(iteration_row["GCOLUMNCAMAN"]).Trim();
								//Calculamos la longitud m�xima que va a tener el campo en la spread
								//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
								//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
								//posici�n para el punto decimal

								if (Convert.ToString(iteration_row["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row["GTIPODATO"]).Trim() == "INT")
								{
									if (Convert.ToDouble(iteration_row["npartedecima"]) != 0)
									{
										iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row["nparteentera"]) + Convert.ToDouble(iteration_row["npartedecima"]) + 2);
									}
									else
									{
										iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row["nparteentera"]) + Convert.ToDouble(iteration_row["npartedecima"]) + 1);
									}
								}
								else
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row["nparteentera"]) + Convert.ToDouble(iteration_row["npartedecima"]));
								}
								iColumna++;
							}
						}

						RsCombos2.Close();

						//Quitamos la �ltima coma
						StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";

						SustituirDescripcionCombo(StComando, iLongitudColumna, AStCol, AStValorClave, AStNomColumna, AStTipoCol, iIndiceCombo, j);
						break;
					}
					else
					{
						//Si la vista no esta a null, los datos se sacan de la vista
						StComando = "SELECT * from SPREVICMV1 " + "Where gvistacaman = " + "'" + AStColFKCombo[j].StVistaCombo.Trim() + "'" + "order by NORDENVISTA ";

						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
						RsCombos2 = new DataSet();
						tempAdapter_2.Fill(RsCombos2);

						//Montamos la select de las columnas que van a ir en el combo

						StComando = "";
						iLongitudColumna = 0;
						AStCol = new string[]{String.Empty};
						iColumna = 0;
						foreach (DataRow iteration_row_2 in RsCombos2.Tables[0].Rows)
						{
							//S�lo participan en la select para cargar el combo
							//aquellas columnas que sean visibles.
							//Pero en el order by pueden participar tanto las visibles
							//como las no visibles.Como solo necesitamos un registro,
							//no vamos a montar el order
							if (Convert.ToString(iteration_row_2["IVISIBCOMVIS"]) == "S")
							{
								StComando = StComando + "A." + Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim() + ",";
								AStCol = ArraysHelper.RedimPreserve(AStCol, new int[]{iColumna + 1});
								AStCol[iColumna] = Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim();
								//Calculamos la longitud m�xima que va a tener el campo en la spread
								//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
								//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
								//posici�n para el punto decimal

								//Adem�s le a�adimos siempre uno mas de longitud, ya que
								//los campos ir�n separados por espacios
								if (Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "INT")
								{
									if (Convert.ToDouble(iteration_row_2["npartedecima"]) != 0)
									{
										iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]) + 2 + 1);
									}
									else
									{
										iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]) + 1 + 1);
									}
								}
								else
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]) + 1);
								}
								iColumna++;
							}
						}

						RsCombos2.Close();

						//Quitamos la �ltima coma
						StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";

						SustituirDescripcionCombo(StComando, iLongitudColumna, AStCol, AStValorClave, AStNomColumna, AStTipoCol, iIndiceCombo, j);
						break;
					}
				}
			}

		}

		private bool ModificaUpdate(DataSet Cursor_Renamed, SqlDataAdapter tempAdapter)
		{
			bool result = false;
			try
			{
				result = true;
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
				tempAdapter.Update(Cursor_Renamed, Cursor_Renamed.Tables[0].TableName);
				return result;
			}
			catch
			{
				result = false;
				RadMessageBox.Show("Existen incongruencias entre el tipo de campo y el valor introducido" + "\n" + "\r" + "Revise el contenido de los campos ", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
				return result;
			}
		}

		private void ReOrdenarArraySpread(int ICol, int IFila)
		{
			//Como se ha insertado una nueva columna en el array del spread
			//y queremos que �sta se encuentre en la misma posici�n que en
			//el spread, es decir, despues de su FK correspondiente,
			//tenemos que mover los datos una posicion hacia el final
			//partir de la columna que hemos insertado

			int i = AStSpread.GetUpperBound(1);
			while (i >= ICol)
			{
				//como la columna empieza por cero, habr� que
				//quitarle 1 a la columna
				AStSpread[IFila, i] = AStSpread[IFila, i - 1];
				i--;
			}

		}

		private void SustituirDescripcionCombo(string StComando1, int iLongitudColumna, string[] AStCol, string[] AStValorClave, string[] AStNomColumna, string[] AStTipoCol, int iIndiceCombo, int iIndiceArray)
		{
			//Conlos datos devueltos de la pantalla de MantenimientoAutomatico
			//que hemos llamado desde un combo, obtengo la descripci�n que se
			//corresponde con el valor de las columnas clave recibidas
			bool fActual = false;

			string StSelect = "Select ";

			//Concateno en la cla�sula WHERE las columnas que forman
			//parte de la clave primaria y su valor correspondiente
			//para el registro actual, para poder sacar el registro
			//actual en el resulset con una SELECT con UNION

			StringBuilder StWhere = new StringBuilder();
			StWhere.Append("Where ");
			for (int i = 0; i <= AStNomColumna.GetUpperBound(0); i++)
			{
				if (StWhere.ToString() != "Where ")
				{
					StWhere.Append(" AND ");
				}
				StWhere.Append(AStNomColumna[i] + "=");
				//Comprobamos el tipo de dato para saber
				switch(AStTipoCol[i].Trim())
				{
					case "DATETIME" : 
						System.DateTime TempDate = DateTime.FromOADate(0); 
						StWhere.Append("'" + ((DateTime.TryParse(AStValorClave[i].Trim(), out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : AStValorClave[i].Trim()) + "'"); 
						break;
					case "CHAR" : case "VARCHAR" : 
						StWhere.Append("'" + AStValorClave[i].Trim() + "'"); 
						break;
					default:
						StWhere.Append(AStValorClave[i].Trim()); 
						break;
				}
			}

			//Monto la consulta con la sentencia SELECT y el ORDER BY
			string StComando = StSelect + StComando1 + "from " + TablaVista(StModoCombo, 0, iIndiceArray).Trim() + " A " + StWhere.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsFK = new DataSet();
			tempAdapter.Fill(RsFK);

			//Comprobamos que el c�digo que nos ha llegado
			//de la pantalla de MantenimientoAutomatico, est�
			//activo para el modo seleccionado:CONSULTAR,MODIFICAR,INCORPORAR
			if (RsFK.Tables[0].Rows.Count == 0)
			{
				//Si el modo es MODIFICAR, se podr�n elegir cualquiera
				//de los registros activos y el registro que estaba ya de antes,
				//aunque �ste no est� activo
				if (StModoCombo.Trim() == "MODIFICAR")
				{
					fActual = true;
					for (int i = 0; i <= AStValorColumnasFKPadre.GetUpperBound(0); i++)
					{
						if (AStValorColumnasFKPadre[i].Trim() != AStValorClave[i].Trim())
						{
							AStValorClave[i] = "";
							fActual = false;
							break;
						}
					}
					RsFK.Close();
					if (!fActual)
					{
						RadMessageBox.Show("El elemento seleccionado no est� activo para el modo " + StModoCombo, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
						cbbCombo[iIndiceCombo].Text = "";
						cbbCombo[iIndiceCombo].SelectedIndex = -1;
						TbClaves[iIndiceCombo].Text = "";
						for (int i = 0; i <= AStNomColumna.GetUpperBound(0); i++)
						{
							foreach (ModuloMantenim.StrFKCombo AStColFKCombo_item in AStColFKCombo)
							{
								if (AStNomColumna[i].Trim() == AStColFKCombo_item.StColPadre.Trim() && iIndiceCombo == AStColFKCombo_item.iIndiceCombo)
								{
									for (int j = 0; j <= astrElConjunto.GetUpperBound(0); j++)
									{
										if (astrElConjunto[j].gcolumncaman.Trim() == AStColFKCombo_item.StColHija.Trim())
										{
											astrElConjunto[j].NombreControl.Text = AStValorClave[i].Trim();
											TbClaves[iIndiceCombo].Text = AStValorClave[i].Trim();
											break;
										}
									}
									break;
								}
							}
						}
						return;
					}
					else
					{
						//Concateno en la cla�sula WHERE las columnas que forman
						//parte de la clave primaria y su valor correspondiente
						//para el registro que ten�a anteriormente el registro
						//que quer�a ser modificado, que es un valor que ya no
						//est� activo

						StWhere = new StringBuilder("Where ");
						for (int i = 0; i <= AStNomColumna.GetUpperBound(0); i++)
						{
							if (StWhere.ToString() != "Where ")
							{
								StWhere.Append(" AND ");
							}
							StWhere.Append(AStNomColumna[i] + "=");
							//Comprobamos el tipo de dato para saber
							switch(AStTipoCol[i].Trim())
							{
								case "DATETIME" : 
									System.DateTime TempDate2 = DateTime.FromOADate(0); 
									StWhere.Append("'" + ((DateTime.TryParse(AStValorColumnasFKPadre[i].Trim(), out TempDate2)) ? TempDate2.ToString("MM/dd/yyyy") : AStValorColumnasFKPadre[i].Trim()) + "'"); 
									break;
								case "CHAR" : case "VARCHAR" : 
									StWhere.Append("'" + AStValorColumnasFKPadre[i].Trim() + "'"); 
									break;
								default:
									StWhere.Append(AStValorColumnasFKPadre[i].Trim()); 
									break;
							}
						}

						//Monto la consulta con la sentencia SELECT y el ORDER BY
						//Ahora para hallar la tabla o vista que corresponde, debemos
						//mandar el modo CONSULTAR, ya quer quiero que saque un registro
						//que no est� activo
						StComando = StSelect + StComando1 + "from " + TablaVista("CONSULTAR", 0, iIndiceArray).Trim() + " A " + StWhere.ToString();

						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
						RsFK = new DataSet();
						tempAdapter_2.Fill(RsFK);
						if (RsFK.Tables[0].Rows.Count == 0)
						{
							RsFK.Close();
							return;
						}
					}
				}
				else
				{
					RadMessageBox.Show("El elemento seleccionado no est� activo para el modo " + StModoCombo, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
					cbbCombo[iIndiceCombo].Text = "";
					cbbCombo[iIndiceCombo].SelectedIndex = -1;
					TbClaves[iIndiceCombo].Text = "";
					RsFK.Close();
					return;
				}
			}

			//Busco los campos FK que est�n ocultos y les pongo el
			//su valor correspondiente. Estos campos son los que guardan
			//la clave del valor referenciado en el combo
			TbClaves[iIndiceCombo].Text = "";
			for (int i = 0; i <= AStNomColumna.GetUpperBound(0); i++)
			{
				for (int k = 0; k <= AStColFKCombo.GetUpperBound(0); k++)
				{
					if (AStNomColumna[i].Trim() == AStColFKCombo[k].StColPadre.Trim() && iIndiceCombo == AStColFKCombo[k].iIndiceCombo)
					{
						for (int j = 0; j <= astrElConjunto.GetUpperBound(0); j++)
						{
							if (astrElConjunto[j].gcolumncaman.Trim() == AStColFKCombo[k].StColHija.Trim())
							{
								astrElConjunto[j].NombreControl.Text = AStValorClave[i].Trim();
								TbClaves[iIndiceCombo].Text = TbClaves[iIndiceCombo].Text + AStValorClave[i].Trim() + ",";
								break;
							}
						}
						break;
					}
				}
			}

			TbClaves[iIndiceCombo].Text = TbClaves[iIndiceCombo].Text.Substring(0, Math.Min(Strings.Len(TbClaves[iIndiceCombo].Text) - 1, TbClaves[iIndiceCombo].Text.Length));
			//Concatenamos los campos que se van a mostrar en el combo

			ConcatenarUltimosCampos(RsFK, 0, 0, iIndiceCombo, iLongitudColumna, AStColFKCombo[iIndiceArray].StTablaPadre, AStCol, AStColFKCombo[iIndiceArray].fVistaNula, AStColFKCombo[iIndiceArray].StVistaCombo, true);

			RsFK.Close();
		}

		private string TablaVista(string StModoACargar, int? j_optional = null, int indice = 0)
		{
			int j = (j_optional == null) ? 0 : j_optional.Value;
			string result = String.Empty;
			switch(StModoACargar)
			{
				case "INCORPORAR" : case "MODIFICAR" : 
					if (j_optional != null)
					{
						result = SelectActivos(j);
					}
					else
					{
						result = SelectActivos(0, indice);
					} 
					break;
				case "CONSULTAR" : 
					if (j_optional != null)
					{
						result = AStColFK[j].StTablaPadre;
					}
					else
					{
						result = AStColFKCombo[indice].StTablaPadre;
					} 
					break;
			}
			return result;
		}

		private bool IncorporarDB()
		{
			//Dar un alta en la Base de Datos
			bool result = false;
			string stSQLManten = String.Empty;
			Control[] MiControl = null;
			int iIndiceIncorporar = 0;
			object lNumerico = null; //As Long

			int i = 0;

			try
			{

				if (vfCambiosPassword)
				{
					SMT200F1 tempLoadForm = SMT200F1.DefInstance;
					SMT200F1.DefInstance.RecibirContrase�a(vstContrase�a);
					SMT200F1.DefInstance.ShowDialog();
				}
                ModuloMantenim.RcManten.BeginTrans();
				stSQLManten = "select * from " + vstNombreTabla;
				stSQLManten = stSQLManten + " where 2=1";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQLManten, ModuloMantenim.RcManten);
				ModuloMantenim.RsManten2 = new DataSet();
				tempAdapter.Fill(ModuloMantenim.RsManten2);
				ModuloMantenim.RsManten2.AddNew();

				//Bucle For para Incorporar en la DB.
				for (iIndiceIncorporar = 0; iIndiceIncorporar <= (sprMultiple.MaxCols - 1); iIndiceIncorporar++)
				{
					MiControl = new Control[iIndiceIncorporar + 1];
					MiControl[iIndiceIncorporar] = LaConexion[iIndiceIncorporar].NombreControl;

					if (LaConexion[iIndiceIncorporar].NombreControl.Name != "cbbCombo")
					{
						if (ComprobarNumericos(LaConexion[iIndiceIncorporar].NombreCampo))
						{
							if (Convert.ToString(LaConexion[iIndiceIncorporar].NombreControl.Text).Trim() == "")
							{
								ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
							}
							else
							{
								if (LaConexion[iIndiceIncorporar].NombreControl is RadMaskedEditBox)
								{
									lNumerico = LaConexion[iIndiceIncorporar].NombreControl.FormattedText(); //CLng(Trim(LaConexion(iIndiceIncorporar).NombreControl.text))
								}
								else
								{
									//lNumerico = Val(LaConexion(iIndiceIncorporar).NombreControl.Text)
									lNumerico = LaConexion[iIndiceIncorporar].NombreControl.Text;
								}

								//OSCAR C Diciembre 2009
								//En el caso de que el tipo de campo sea decimal, se deben considerar los decimales
								//RsManten2(i) = Val(lNumerico)
								if (ModuloMantenim.RsManten2.Tables[0].Rows[0][i].GetType() == typeof(decimal))
								{
									ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = lNumerico;
								}
								else
								{
									ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = Conversion.Val(Convert.ToString(lNumerico));
								}
								//--------------------

							}
						}
						else
						{
							if (Convert.ToString(LaConexion[iIndiceIncorporar].NombreControl.Text).Trim() == "")
							{
								ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
							}
							else
							{
								if (LaConexion[iIndiceIncorporar].NombreControl is RadMaskedEditBox)
								{
									ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = LaConexion[iIndiceIncorporar].NombreControl.FormattedText();
								}
								else
								{
									ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = LaConexion[iIndiceIncorporar].NombreControl.Text;
								}
							}
						}
						i++;
					}
				}


				//Oscar
				//Parece ser que se inserta un registro de la BD, por lo que se tiene
				//que auditar.Se hace antes sel update, porque en caso de producirse un error
				//en la actualizacion, se hace un rollback de la transaccion
				ModuloMantenim.AUDITARMOVIMIENTO("A", vstNombreTabla, Serrores.VVstUsuarioApli, ModuloMantenim.RsManten2);
				//---------------

				//oscar 04/06/2004
				string sql = String.Empty;
				if (vfCambiosPassword && vstNombreTabla == "SUSUARIO")
				{
					sql = "INSERT INTO SUSUCCON (FCAMBIO, GUSUARIO, NPASSWORD) " + 
					      " VALUES (GETDATE(), '" + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gusuario"]) + "','" + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["vpassword"]) + "')";
				}
				//-------

				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(ModuloMantenim.RsManten2, ModuloMantenim.RsManten2.Tables[0].TableName);

				//oscar 04/06/2004
				if (vfCambiosPassword && vstNombreTabla == "SUSUARIO")
				{
					SqlCommand tempCommand = new SqlCommand(sql, ModuloMantenim.RcManten);
					tempCommand.ExecuteNonQuery();
				}
				//----------------

				//Cargar el �ltimo registro de la grid
				SumarSpread();

				//''''''''''''''Visualizar el �ltimo registro
				ModuloMantenim.VisualizarRegistro(sprMultiple.MaxRows, this);

				for (int iindice2 = 0; iindice2 <= (iIndiceIncorporar - 1); iindice2++)
				{
					MiControl[iindice2] = null;
				}


				ModuloMantenim.RsManten2.Close();
				result = true;
                ModuloMantenim.RcManten.CommitTransScope();
				return result;
			}
			catch(Exception ex)
			{
				//En pruebas
				ModuloMantenim.ColeccionErrores(ex);
                ModuloMantenim.RcManten.RollbackTransScope();
				this.Cursor = Cursors.Default;


				return result;
			}
		}
		public bool ComprobarDependenciasTabla(string stNombreTabla, string stSQLBorrar, bool fBorrar, SqlDataAdapter tempAdapter_3)
		{
			//Cuando da un error al borrar porque la tabla tiene demasiadas CONSTRAINTS,
			//debo comprobar que ninguno de los valores del registro que se desea borrar
			//este siendo referenciado en las tablas que especifican las CONSTRAINTS.
			//Si es as�, entonces se deshabilitan las CONSTRAINTS, se borra el registro
			//y se vuelven a habilitar las CONSTRAINTS
			bool result = false;
			int i = 0;
			StringBuilder StFK = new StringBuilder();
			string StMensaje = String.Empty;
			bool fHayFK = false;
			DialogResult StRes = (DialogResult) 0;

			StringBuilder StComando = new StringBuilder();
			StComando.Append("select A.gtabcamanhij gtabcamanhij,A.gfkcaman gfkcaman," + "A.gcolcamanhij gcolcamanhij,B.gtipodato gtipodato," + "B.dcoltabmanco dcoltabmanco " + "from scolfkpkv1 A,SCOCACOLV1 B " + "where A.gtabcamanpad=" + "'" + stNombreTabla.Trim() + "'" + " and A.gtabcamanhij=B.gtablacaman" + " and A.gcolcamanhij=B.gcolumncaman" + " order by A.gtabcamanhij,A.gfkcaman ");

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
			DataSet RsFK = new DataSet();
			tempAdapter.Fill(RsFK);

			if (RsFK.Tables[0].Rows.Count != 0)
			{

				aStrFK = new ModuloMantenim.StrColumnasFK[RsFK.Tables[0].Rows.Count];
				i = 0;

				foreach (DataRow iteration_row in RsFK.Tables[0].Rows)
				{

					aStrFK[i].StTablaHija = Convert.ToString(iteration_row["gtabcamanhij"]).Trim();
					aStrFK[i].StColumnaHija = Convert.ToString(iteration_row["gcolcamanhij"]).Trim();
					aStrFK[i].StNombreGrupoFK = Convert.ToString(iteration_row["gfkcaman"]).Trim();
					aStrFK[i].Stgtipodato = Convert.ToString(iteration_row["gtipodato"]).Trim();
					aStrFK[i].Stdcoltabmanco = Convert.ToString(iteration_row["dcoltabmanco"]).Trim();
					aStrFK[i].fEsta = false;
					aStrFK[i].existe = false;
					foreach (ModuloMantenim.strConjuntoResultados astrElConjunto_item in astrElConjunto)
					{ //-1
						if (astrElConjunto_item.gcolumncaman.Trim() == aStrFK[i].StColumnaHija.Trim())
						{
							aStrFK[i].NombreControl = astrElConjunto_item.NombreControl;
							aStrFK[i].existe = true;
							break;
						}
					}

					i++;
				}
				RsFK.Close();

				for (i = 0; i <= aStrFK.GetUpperBound(0); i++)
				{
					if (!aStrFK[i].fEsta)
					{
						//aStrFK(i).fEsta nos indica si el campo est� ya
						//en el conjunto de grupos de FK

						StComando = new StringBuilder("select * from " + aStrFK[i].StTablaHija.Trim() + " where ");

						//Recorro el array en busca de otras columnas que
						//pertenezcan al mismo grupo de FK y la
						//a�ado al where
						fHayFK = false;

						for (int j = 0; j <= aStrFK.GetUpperBound(0); j++)
						{
							if (aStrFK[i].StNombreGrupoFK.Trim() == aStrFK[j].StNombreGrupoFK.Trim())
							{

								aStrFK[i].fEsta = true;
								aStrFK[j].fEsta = true;

								//Compruebo si ya ha habido anteriormente alguna columna
								//perteneciente a este grupo de FK
								if (fHayFK)
								{
									StComando.Append(" AND ");
								}

								//SI EL CAMPO ES DATETIME, NO SE TENDR� EN CUENTA LA HORA.
								//SE CONVIERTE LA FECHA-HORA A FECHA FORMATEADA A "DD/MM/YYYY"
								if (aStrFK[j].Stgtipodato.Trim() == "DATETIME")
								{
									StComando.Append("CONVERT(char(10)," + aStrFK[j].StColumnaHija + ",103)");
								}
								else
								{
									StComando.Append(aStrFK[j].StColumnaHija);
								}

								if (aStrFK[j].existe)
								{
									fHayFK = true;

									StComando.Append("=");

									//SI ES "CHAR", "VARCHAR" O "DATETIME",
									//EL DATO VA ENTRE COMILLA SIMPLE
									if (aStrFK[j].Stgtipodato.Trim() == "VARCHAR" || aStrFK[j].Stgtipodato.Trim() == "CHAR" || aStrFK[j].Stgtipodato.Trim() == "DATETIME")
									{
										StComando.Append("'");
									}

									//SI EL CONTROL ES M�SCARA, HABR� QUE COGER EL FORMATTEDTEXT
									//EN VEZ DEL TEXT PARA QUE COJA EL PUNTO EN LOS DECIMALES
									if (aStrFK[j].NombreControl.Name == "mebMascara")
									{
										StComando.Append(Convert.ToString(aStrFK[j].NombreControl.FormattedText()).Trim());
									}
									else
									{
										StComando.Append(Convert.ToString(aStrFK[j].NombreControl.Text).Trim());
									}

									//SI ES "CHAR", "VARCHAR" O "DATETIME",
									//EL DATO VA ENTRE COMILLA SIMPLE
									if (aStrFK[j].Stgtipodato.Trim() == "VARCHAR" || aStrFK[j].Stgtipodato.Trim() == "CHAR" || aStrFK[j].Stgtipodato.Trim() == "DATETIME")
									{
										StComando.Append("'");
									}

								}
								else
								{
									//SI EL CAMPO EST� EN BLANCO, SE COMPARA CON NULL
									fHayFK = true;
									StComando.Append(" IS NULL ");
								}
							}
						}


						if (fHayFK)
						{
							//Si hemos encontrado algun grupo de FK
							//Lanzamos el resulset y vemos si hay algun registro igual en la
							//tabla
							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
							RsFK = new DataSet();
							tempAdapter_2.Fill(RsFK);
							if (RsFK.Tables[0].Rows.Count != 0)
							{

								//Montamos el mensaje de error con el nombre del grupo
								//y el de las columnas que pertenecen al grupo de FK
								StFK = new StringBuilder("");
								if (aStrFK.GetUpperBound(0) != 0)
								{
									for (int j = 0; j <= aStrFK.GetUpperBound(0) - 1; j++)
									{
										if (aStrFK[i].StNombreGrupoFK.Trim() == aStrFK[j].StNombreGrupoFK.Trim())
										{
											if (StFK.ToString().Trim() != "")
											{
												StFK.Append(", ");
											}
											StFK.Append(aStrFK[j].Stdcoltabmanco);
										}
									}
								}
								else
								{
									StFK.Append(aStrFK[0].Stdcoltabmanco);
								}
								//Si viene de borrado
								if (fBorrar)
								{
									//*****************************************************
									//Compruebo que la tabla es una de las que se
									//puede realizar borrado l�gico

									if (ModuloMantenim.TablaBorradoLogico(vstNombreTabla, this))
									{
										RadMessageBox.Show("tabla borrado logico", Application.ProductName);
										if (Convert.IsDBNull(ModuloMantenim.RsManten2.Tables[0].Rows[0][ModuloMantenim.StFBORRADO]))
										{
											//Pregunto si desea borrar el registro l�gicamente
											StRes = RadMessageBox.Show("Se ha producido un error de integridad referencial. �Desea eliminar el registro l�gicamente?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question);
											if (StRes == System.Windows.Forms.DialogResult.Yes)
											{
												//Si la respuesta es s� actualizo la
												//fecha de borrado con la del sistema
												ModuloMantenim.RsManten2.Edit();
												ModuloMantenim.RsManten2.Tables[0].Rows[0][ModuloMantenim.StFBORRADO] = DateTime.Today.ToString("dd/MM/yyyy");
												SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
												tempAdapter_3.Update(ModuloMantenim.RsManten2, ModuloMantenim.RsManten2.Tables[0].TableName);
												ActualizarFechaBorradoSpread();
											}
										}
										else
										{
											//Se muestra que se ha detectado el error
											//de problemas de integridad referencial
											RadMessageBox.Show("Hay errores de integridad y el registro ya ha sido borrado l�gicamente", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
										}
									}
									else
									{
										StMensaje = "No se puede eliminar el registro porque se ha producido " + "un error de integridad referencial con el " + "Grupo de Foreign Key " + aStrFK[i].StNombreGrupoFK + " formado por " + StFK.ToString();
										RadMessageBox.Show(StMensaje, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
									}
									//*****************************************************
								}
								return result;
							}
							RsFK.Close();
						}
						fHayFK = false;
					}
				}
			}

			return true;
			//****************************************************************************
			//Si llega a esta parte es porque no ha encontrado ningun registro igual en las
			//tablas referenciadas.
			//Se desactivan las CONSTRAINTS, se borra el registro y se vuelven a activar
			//los constraints.
			//****************************************************************************
		}


		private string QuitaCeros(string Numero, string Tipodato)
		{

			if (Tipodato.Trim() == "NUMERIC" && Numero.Trim().IndexOf(ModuloMantenim.stSignoDecimal) >= 0)
			{
				Numero = Numero.Trim();
				for (int i = 1; i <= Numero.Trim().Length; i++)
				{
					if (Numero.EndsWith("0") || Numero.EndsWith(ModuloMantenim.stSignoDecimal))
					{
						Numero = Numero.Substring(0, Math.Min(Numero.Length - 1, Numero.Length));
						if (i > Numero.Trim().Length)
						{
							break;
						}
					}
					else
					{
						break;
					}
				}
			}
			return Numero;
		}

		private string SelectActivos(int? i_optional = null, int indice = 0)
		{
			int i = (i_optional == null) ? 0 : i_optional.Value;
			string result = String.Empty;
			string StComando = String.Empty;
			DataSet RsCombo = null;


			if (i_optional != null)
			{
				//Para sacar la vista de la que se cargan los combos
				//con los valores activos:

				//Si el combo se carga de una tabla
				if (AStColFK[i].fVistaNula)
				{
					StComando = "select GVISTACTTABB " + "from STABBLOG " + "where GTABLABLOG =" + "'" + AStColFK[i].StTablaPadre.Trim() + "'";

					SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombo = new DataSet();
					tempAdapter.Fill(RsCombo);

					if (RsCombo.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(RsCombo.Tables[0].Rows[0]["GVISTACTTABB"]))
						{
							result = Convert.ToString(RsCombo.Tables[0].Rows[0]["GVISTACTTABB"]).Trim();
						}
						else
						{
							//Si no hay ning�n registro, se carga de la tabla padre
							result = AStColFK[i].StTablaPadre.Trim();
						}
					}
					else
					{
						//Si no hay ning�n registro, se carga de la tabla padre
						result = AStColFK[i].StTablaPadre.Trim();
					}
					//Si el combo se carga de una vista
				}
				else
				{
					StComando = "Select GVISTACTVIST " + "from SVICAMAN " + "where GVISTACAMAN=" + "'" + AStColFK[i].StVistaCombo.Trim() + "'";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombo = new DataSet();
					tempAdapter_2.Fill(RsCombo);

					if (RsCombo.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(RsCombo.Tables[0].Rows[0]["GVISTACTVIST"]))
						{
							result = Convert.ToString(RsCombo.Tables[0].Rows[0]["GVISTACTVIST"]).Trim();
						}
						else
						{
							//Si no hay ning�n registro, se carga de la vista padre
							result = AStColFK[i].StVistaCombo.Trim();
						}
					}
					else
					{
						//Si no hay ning�n registro, se carga de la vista padre
						result = AStColFK[i].StVistaCombo.Trim();
					}
				}

			}
			else
			{
				//Para sacar la vista de la que se cargan los combos
				//con los valores activos:

				//Si el combo se carga de una tabla
				if (AStColFKCombo[indice].fVistaNula)
				{
					StComando = "select GVISTACTTABB " + "from STABBLOG " + "where GTABLABLOG =" + "'" + AStColFKCombo[indice].StTablaPadre.Trim() + "'";

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombo = new DataSet();
					tempAdapter_3.Fill(RsCombo);

					if (RsCombo.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(RsCombo.Tables[0].Rows[0]["GVISTACTTABB"]))
						{
							result = Convert.ToString(RsCombo.Tables[0].Rows[0]["GVISTACTTABB"]).Trim();
						}
						else
						{
							//Si no hay ning�n registro, se carga de la tabla padre
							result = AStColFKCombo[indice].StTablaPadre.Trim();
						}
					}
					else
					{
						//Si no hay ning�n registro, se carga de la tabla padre
						result = AStColFKCombo[indice].StTablaPadre.Trim();
					}
					//Si el combo se carga de una vista
				}
				else
				{
					StComando = "Select GVISTACTVIST " + "from SVICAMAN " + "where GVISTACAMAN=" + "'" + AStColFKCombo[indice].StVistaCombo.Trim() + "'";

					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombo = new DataSet();
					tempAdapter_4.Fill(RsCombo);

					if (RsCombo.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(RsCombo.Tables[0].Rows[0]["GVISTACTVIST"]))
						{
							result = Convert.ToString(RsCombo.Tables[0].Rows[0]["GVISTACTVIST"]).Trim();
						}
						else
						{
							//Si no hay ning�n registro, se carga de la vista padre
							result = AStColFKCombo[indice].StVistaCombo.Trim();
						}
					}
					else
					{
						//Si no hay ning�n registro, se carga de la vista padre
						result = AStColFKCombo[indice].StVistaCombo.Trim();
					}
				}
			}
			RsCombo.Close();
			return result;
		}



		private void SustituirColumnasFKCombo(string StComando1, string StComando2, int iLongitudColumna, string[] AStCol, bool fDimensionarCombo, string StModo)
		{
			int i = 0;
			int j = 0;
			string StComando = String.Empty;
			int iIndiceControl = 0;
			StringBuilder StColumnasOcultas = new StringBuilder();
			string StTablaActivos = String.Empty;
			StringBuilder StWhere = new StringBuilder();
			string StNombreFK = String.Empty;

			//Hago un COUNT para saber si el resulset que carga el combo
			//es > que la constante MAXFILAS, y si lo es, no cargo el combo
			//y lo inhabilito
			if (StModo.Trim() == "MODIFICAR")
			{
				//Monto la consulta con la sentencia SELECT y el ORDER BY
				StComando = "Select count(*) " + "from " + TablaVista(StModo, AStColFK.GetUpperBound(0)).Trim();

			}
			else
			{
				//Monto la consulta con la sentencia SELECT y el ORDER BY
				StComando = "Select count(*) " + "from " + TablaVista(StModo, AStColFK.GetUpperBound(0)).Trim();

			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsFK = new DataSet();
			tempAdapter.Fill(RsFK);

			if (Convert.ToDouble(RsFK.Tables[0].Rows[0][0]) > ModuloMantenim.vMaximoFilas)
			{
				RsFK.Close();
				cbbCombo[viIndiceCombo].Enabled = false;
				//N� de �ndice de ControlCombo
				foreach (ModuloMantenim.StrFK AStColFK_item in AStColFK)
				{
					StColumnasOcultas.Append("0;");
				}
				//Quitamos el �ltimo punto y coma
				StColumnasOcultas = new StringBuilder(StColumnasOcultas.ToString().Substring(0, Math.Min(StColumnasOcultas.ToString().Length - 1, StColumnasOcultas.ToString().Length)));
				//Oculta las columnas que son clave en el combo
				// ralopezn TODO_X_5 20/05/2016
                //cbbCombo[viIndiceCombo].ColumnWidths = StColumnasOcultas.ToString();
				viIndiceCombo++;

				return;
			}
			else
			{
				cbbCombo[viIndiceCombo].Enabled = true;
			}
			RsFK.Close();

			StringBuilder StSelect = new StringBuilder();
			StSelect.Append("Select ");

			//A�ado al Select los campos que son FK, para introducirlos en combo
			//y as� poder referenciarlos

			for (i = 0; i <= AStColFK.GetUpperBound(0); i++)
			{
				StSelect.Append("A." + AStColFK[i].StColPadre + ",");
			}

			//Si se est� realizando una modificaci�n,
			//habr� que cargar el combo con los valores activos
			//y con el valor actual del registro, aunque �ste sea de borrado
			//l�gico, ya que si el usuario se equivoca al modificar, debe volver
			//poder elegir el valor inicial aunque sea de borrado l�gico.
			//Para eso haremos una SELECT con los valores activos sacados
			//de la vista o tabla correspondiente UNION SELECT el valor
			//actual de la tabla o vista que contenga todos los registros

			if (StModo.Trim() == "MODIFICAR")
			{
				if (AStColFK[AStColFK.GetUpperBound(0)].fVistaNula)
				{
					StTablaActivos = AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre.Trim();
				}
				else
				{
					StTablaActivos = AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo.Trim();
				}

				//Concateno en la cla�sula WHERE las columnas que forman
				//parte de la clave primaria y su valor correspondiente
				//para el registro actual, para poder sacar el registro
				//actual en el resulset con una SELECT con UNION
				StNombreFK = AStColFK[AStColFK.GetUpperBound(0)].StNombreFK.Trim();
				StWhere = new StringBuilder("Where ");
				foreach (ModuloMantenim.StrFK AStColFK_item_3 in AStColFK)
				{
					if (StNombreFK.Trim() == AStColFK_item_3.StNombreFK.Trim())
					{
						for (j = 0; j <= astrElConjunto.GetUpperBound(0); j++)
						{
							if (AStColFK_item_3.StColHija.Trim() == astrElConjunto[j].gcolumncaman.Trim())
							{
								if (StWhere.ToString() != "Where ")
								{
									StWhere.Append(" AND ");
								}
								StWhere.Append(AStColFK_item_3.StColPadre + "=");
								//Comprobamos si es control fecha
								if (astrElConjunto[j].NombreControl.Name == "sdcFecha")
								{
									System.DateTime TempDate = DateTime.FromOADate(0);
									StWhere.Append("'" + ((DateTime.TryParse(Convert.ToString(astrElConjunto[j].NombreControl.Text).Trim(), out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : Convert.ToString(astrElConjunto[j].NombreControl.Text).Trim()) + "'");
									//Comprobamos si el control es Masked Edit
								}
								else if (astrElConjunto[j].NombreControl.Name == "mebMascara")
								{ 
									//Comprobamos el tipo de dato para saber
									//si va entre ncomilla simple
									switch(astrElConjunto[j].gtipodato.Trim())
									{
										case "CHAR" : case "VARCHAR" : 
											StWhere.Append("'" + Convert.ToString(astrElConjunto[j].NombreControl.FormattedText()).Trim() + "'"); 
											break;
										default:
											StWhere.Append(Convert.ToString(astrElConjunto[j].NombreControl.FormattedText()).Trim()); 
											break;
									}
								}
								else
								{
									//Comprobamos el tipo de dato para sbar
									//si va entre ncomilla simple
									switch(astrElConjunto[j].gtipodato.Trim())
									{
										case "CHAR" : case "VARCHAR" : 
											StWhere.Append("'" + Convert.ToString(astrElConjunto[j].NombreControl.Text).Trim() + "'"); 
											break;
										default:
											StWhere.Append(Convert.ToString(astrElConjunto[j].NombreControl.Text).Trim()); 
											break;
									}
								}
								break;
							}
						}
					}
				}
				if (StWhere.ToString().Substring(Math.Max(StWhere.ToString().Length - 1, 0)) == "=" || StWhere.ToString().Substring(Math.Max(StWhere.ToString().Length - 2, 0)) == "''")
				{
					StComando = StSelect.ToString() + StComando1 + "from " + TablaVista(StModo, AStColFK.GetUpperBound(0)).Trim() + " A " + StComando2;
				}
				else
				{
					//Monto la consulta con la sentencia SELECT y el ORDER BY
					StComando = "(" + StSelect.ToString() + StComando1 + "from " + TablaVista(StModo, AStColFK.GetUpperBound(0)).Trim() + " A " + "UNION " + StSelect.ToString() + StComando1 + "from " + StTablaActivos.Trim() + " A " + StWhere.ToString() + ")" + StComando2;
				}
			}
			else
			{
				//Monto la consulta con la sentencia SELECT y el ORDER BY
				StComando = StSelect.ToString() + StComando1 + "from " + TablaVista(StModo, AStColFK.GetUpperBound(0)).Trim() + " A " + StComando2;
			}


			//Tenemos que abrir una segunda conexi�n porque, al mandar la
			//select con UNION, si el resultado de la consulta es mayor a
			//determinado n� de registros, nos dice que la conexi�n est� ocupada

			ModuloMantenim.EstablecerSegundaConexion();

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, ModuloMantenim.Rc2);
			RsFK = new DataSet();
			tempAdapter_2.Fill(RsFK);


			//El n� de columnas de cada combo ser� igual a una columna por cada
			//columna que forme parte de la FK mas una, que ser� la que lleve
			//la descripci�n.
			//Como el Ubound nos da el m�ximo �ndice de un array y �ste n� es uno menos
			//que los elementos que tiene el array, le sumamos dos al Ubound y
			//nos dar� el n� de columnas que debe tener el combo
			if (AStColFK.GetUpperBound(0) <= 8)
			{
                // ralopezn TODO_X_5 20/05/2016
				//cbbCombo[viIndiceCombo].ColumnCount = AStColFK.GetUpperBound(0) + 2;
			}
			else
			{
				RadMessageBox.Show("El combo no puede soportar una FK formada por mas de 9 columnas.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
				this.Close();
				return;
			}

			//Limpio el combo
			cbbCombo[viIndiceCombo].Items.Clear();

			//Lleno el combo
			//La primeroa fila del combo va a tener todos sus valores igual al contenido
			//de la constante CoItemComboNulo, y servir� para poder grabar valores nulos
			j = 0;
			if (RsFK.Tables[0].Rows.Count != 0)
			{
				string tempRefParam = "";
				object tempRefParam2 = Type.Missing;
				cbbCombo[viIndiceCombo].Items.Add(new RadListDataItem(tempRefParam, tempRefParam2));
				for (i = 0; i <= AStColFK.GetUpperBound(0); i++)
				{
					//Primero creamos en el combo las columnas que forman parte de la
					//FK, y las ocultamos
                    // ralopezn TODO_X_5 20/05/2016
					//cbbCombo[viIndiceCombo].set_Column(i, j, CoItemComboNulo.ToUpper());
				}
                // ralopezn TODO_X_5 20/05/2016
                //cbbCombo[viIndiceCombo].set_Column(i, j, CoItemComboNulo.ToUpper());
            }

            for (j = 0; j <= astrElConjunto.GetUpperBound(0); j++)
			{
				if (astrElConjunto[j].NombreControl.Name == cbbCombo[viIndiceCombo].Name)
				{
					if (ControlArrayHelper.GetControlIndex(astrElConjunto[j].NombreControl) == viIndiceCombo)
					{
						iIndiceControl = j;
						break;
					}
				}
			}


			j = 1;
			foreach (DataRow iteration_row in RsFK.Tables[0].Rows)
			{
				string tempRefParam3 = "";
				object tempRefParam4 = Type.Missing;
				cbbCombo[viIndiceCombo].Items.Add(new RadListDataItem(tempRefParam3, tempRefParam4));

				for (i = 0; i <= AStColFK.GetUpperBound(0); i++)
				{
					//Primero creamos en el combo las columnas que forman parte de la
					//FK, y las ocultamos
					if (!Convert.IsDBNull(iteration_row[i]))
					{
						//'            iTama�o = Tama�oMaximo(AStColFK(i).StColPadre, AStColFK(i).fVistaNula, AStColFK(i).StTablaPadre, AStColFK(i).StVistaCombo)
						if (iteration_row[i].GetType() == typeof(DateTime))
						{
							//Si el tipo de dato es fecha, su longitud la sacaremos
							//con el tama�o que ocupe la fecha formateada a "dd/mm/yyyy"
						//ralopezn TODO_X_5 20/05/2016	
                        //cbbCombo[viIndiceCombo].set_Column(i, j, Convert.ToDateTime(iteration_row[i]).ToString("dd/MM/yyyy").ToUpper() + " ");
						}
						else
						{
                            // ralopezn TODO_X_5 20/05/2016
							//cbbCombo[viIndiceCombo].set_Column(i, j, Convert.ToString(iteration_row[i]).Trim().ToUpper() + " ");
						}
					}
					else
					{
                        //ralopezn TODO_X_5 20/05/2016
						//cbbCombo[viIndiceCombo].set_Column(i, j, " ");
					}
				}

				//Concatenamos los campos que se van a mostrar en el combo
				//en una sola columna visible
				ConcatenarUltimosCampos(RsFK, i, j, iIndiceControl, iLongitudColumna, AStColFK[i - 1].StTablaPadre, AStCol, AStColFK[i - 1].fVistaNula, AStColFK[i - 1].StVistaCombo);

				j++;
			}

			//Para ocultar columnas, hay que poner la propiedad ColumnWidths del combo
			//a ceros para cada una de las columnas que queramos ocultar,
			//separadas por comas.
			//As�, si queremos ocultar las dos primeras columnas, se pondr�:
			//COMBO.ColumnWidths="0;0"
			//Como nosotros queremos ocultar aquellas columnas que forman parte de
			//la FK, vamos concatenando en un string tantos ceros, separados por
			//puntos y coma, como columnas queramos ocultar
			for (i = 0; i <= AStColFK.GetUpperBound(0); i++)
			{
				StColumnasOcultas.Append("0;");
			}

			//Quitamos el �ltimo punto y coma
			StColumnasOcultas = new StringBuilder(StColumnasOcultas.ToString().Substring(0, Math.Min(StColumnasOcultas.ToString().Length - 1, StColumnasOcultas.ToString().Length)));
			//Oculta las columnas que son clave en el combo
			//cbbCombo[viIndiceCombo].ColumnWidths = StColumnasOcultas.ToString();

			RsFK.Close();
            ModuloMantenim.RcManten.Close();
			//N� de �ndice de ControlCombo
			viIndiceCombo++;
		}

		//private void ValidarNegritaCombo()
		//{
				//Debemos buscar en la estructura el control y comprobar si admite nulos
				//Control MiControl = null;
				//
				//for (int iIndiceControl = 0; iIndiceControl <= astrElConjunto.GetUpperBound(0) - 1; iIndiceControl++)
				//{
					//MiControl = astrElConjunto[iIndiceControl].NombreControl;
					//if (astrElConjunto[iIndiceControl].iadmitenull == "N")
					//{
						//buscar etiqueta del control y poner en negrita
						//MiControl.Font = MiControl.Font.Change(bold:true);
					//}
					//else
					//{
						//MiControl.Font = MiControl.Font.Change(bold:false);
					//}
					//MiControl = null;
				//}
		//}

		private bool ValidarTama�oFijo(int stTama�o, int iIndiceControl)
		{
			//Comprobaremos que el campo est� relleno al m�ximo

			if (stTama�o < (astrElConjunto[iIndiceControl].npartedecima) + (astrElConjunto[iIndiceControl].nparteentera) && astrElConjunto[iIndiceControl].NombreControl.Enabled)
			{
				//Mensaje para decir al usuario que debe rellenar todo el campo
				short tempRefParam = 1640;
				string[] tempRefParam2 = new string[]{astrElConjunto[iIndiceControl].dcoltabmanco};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
				//*************************CRIS**************************
				astrElConjunto[iIndiceControl].NombreControl.Focus();
				//*******************************************************
				return false;
			}
			return true;
		}
		private void GuardarColumnasPadre(int iIndiceCombo)
		{
			//Guardo estos datos para luego poder obtener las descripciones
			//correspondientes a los valores devueltos desde el MantenimientoAutomatico
			//que llamamos desde combo.
			//Tambien guardo el valor actual por si viene del modo MODIFICAR y el valor
			//actual no est� activo dejarle que lo elija
			int j = 0;
			bool fEsta = false;
			AStColumnasFKPadre = new string[]{String.Empty};
			AStValorColumnasFKPadre = new string[]{String.Empty};

			foreach (ModuloMantenim.StrFKCombo AStColFKCombo_item in AStColFKCombo)
			{
				if (iIndiceCombo == AStColFKCombo_item.iIndiceCombo)
				{
					//Comprobamos que no hemos introducido ya
					//esa columna en el array
					for (int k = 0; k <= AStColumnasFKPadre.GetUpperBound(0); k++)
					{
						if (AStColumnasFKPadre[k].Trim() == AStColFKCombo_item.StColPadre.Trim())
						{
							fEsta = true;
						}
					}
					if (!fEsta)
					{
						AStColumnasFKPadre = ArraysHelper.RedimPreserve(AStColumnasFKPadre, new int[]{j + 1});
						AStValorColumnasFKPadre = ArraysHelper.RedimPreserve(AStValorColumnasFKPadre, new int[]{j + 1});
						AStColumnasFKPadre[j] = AStColFKCombo_item.StColPadre;
						for (int p = 0; p <= astrElConjunto.GetUpperBound(0); p++)
						{
							if (astrElConjunto[p].gcolumncaman.Trim() == AStColFKCombo_item.StColPadre.Trim())
							{
								sprMultiple.Row = sprMultiple.ActiveRowIndex;
								//Comprobamos si es control Mascara
								if (astrElConjunto[p].NombreControl.Name == "mebMascara")
								{
									sprMultiple.Col = astrElConjunto[p].nordencoltab;
									AStValorColumnasFKPadre[j] = sprMultiple.Text.Trim(); //astrElConjunto(p).NombreControl.FormattedText()
									//Comprobamos si es control Fecha
								}
								else if (astrElConjunto[p].NombreControl.Name == "sdcFecha")
								{ 
									sprMultiple.Col = astrElConjunto[p].nordencoltab;
									AStValorColumnasFKPadre[j] = DateTime.Parse(sprMultiple.Text).ToString("dd/MM/yyyy"); //Format(astrElConjunto(p).NombreControl.Text, "dd/mm/yyyy")
								}
								else
								{
									sprMultiple.Col = astrElConjunto[p].nordencoltab;
									AStValorColumnasFKPadre[j] = sprMultiple.Text; //astrElConjunto(p).NombreControl.Text
								}
								break;
							}
						}
						j++;
					}
					fEsta = false;
				}
			}

		}

		public void InstanciaForm(int lNumRegistros, object FrInstancia, object varAStColumnasClave_optional, int? varIndiceCombo_optional)
		{
			string[] varAStColumnasClave = (varAStColumnasClave_optional == Type.Missing) ? null : varAStColumnasClave_optional as string[];
			int varIndiceCombo = (varIndiceCombo_optional == null) ? 0 : varIndiceCombo_optional.Value;

			vlCantidad = lNumRegistros;

			FrInstanciaFormulario = FrInstancia;

			if (varAStColumnasClave_optional != Type.Missing)
			{
				for (int Elemento = 0; Elemento <= varAStColumnasClave.GetUpperBound(0); Elemento++)
				{
					AStColumnasFK = ArraysHelper.RedimPreserve(AStColumnasFK, new int[]{Elemento + 1});
					AStColumnasFK[Elemento] = varAStColumnasClave[Elemento];
				}
				fVieneDeCombos = true;
			}
			else
			{
				fVieneDeCombos = false;
			}

			if (varIndiceCombo_optional != null)
			{
				viIndiceComboInhabilitado = varIndiceCombo;
			}
		}

		public void InstanciaForm(int lNumRegistros, object FrInstancia, object varAStColumnasClave_optional)
		{
			InstanciaForm(lNumRegistros, FrInstancia, varAStColumnasClave_optional, null);
		}

		public void InstanciaForm(int lNumRegistros, object FrInstancia)
		{
			InstanciaForm(lNumRegistros, FrInstancia, Type.Missing, null);
		}

		private void AlturaFrame(Control control)
		{
			//Movemos el frame que contiene los campos para que �stos se
			//puedan ir viendo seg�n se vaya moviendo el scroll
			frmDetalle.Height = (int) (frmDetalle.Height + control.Height + ModuloMantenim.COiSeparacion / 15);

		}

		private void CargarComboAlgoNada()
		{
			//carga los combos de consulta de cada campo con los valores
			//de consulta: ALGO, NADA, TODO, ETC
			int i = 0;
			//********************************************************
			return;
			//********************************************************
			Cbb_AlgoNada[i].Items.Clear();
			ObtenerConstantesCombo();
			int tempForVar = Cbb_AlgoNada.GetUpperBound(0);
			for (i = 1; i <= tempForVar; i++)
			{
				string tempRefParam = "";
				object tempRefParam2 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam, tempRefParam2));
				string tempRefParam3 = StALGO;
				object tempRefParam4 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam3, tempRefParam4));
				StALGO = Convert.ToString(tempRefParam3);
				string tempRefParam5 = StNADA;
				object tempRefParam6 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam5, tempRefParam6));
				StNADA = Convert.ToString(tempRefParam5);
				string tempRefParam7 = StTODO;
				object tempRefParam8 = Type.Missing;
				Cbb_AlgoNada[i].Items.Add(new RadListDataItem(tempRefParam7, tempRefParam8));
				StTODO = Convert.ToString(tempRefParam7);
                //5.4 son el n� de caracteres X en 1 cm
                //1 caracter X son 105 twips
                //Necesitamos saber los cm porque esa es la unidad de
                //la propiedad ListWidth
                Cbb_AlgoNada[i].DropDownListElement.DropDownWidth = (int.Parse(((IMaxLen + 2) / 5.4d).ToString()));// ((((IMaxLen + 2) / 5.4d).ToString()) + "cm");

            }
		}



		private void ComprobarCamposConsulta()
		{
			//Inserta en un array de consulta los valores de consulta de cada
			//campo por el que se pretende consultar, comprobando si son num�ricos
			//o no para ponerle las comillas simple y el parametro de consulta %,
			//excepto en el caso de que sean columnas que referencian a combos,
			//ya que estas columnas son FK y deben coincidir exactamente

			string stFecha = String.Empty;
			LaConsulta = new SMT100F2.Consultas[LaConexion.GetUpperBound(0) + 1];

			this.Cursor = Cursors.WaitCursor;

			//Recorrer los controles y comprobar si no est�n vac�os
			for (int iControles = 0; iControles <= LaConsulta.GetUpperBound(0) - 1; iControles++)
			{
				if (Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() != "" && LaConexion[iControles].NombreControl.Name != "cbbCombo")
				{
					//Deberiamos comprobar si los campos son num�ricos 0 fecha para convertirlos a cadenas
					if (ComprobarNumericos(LaConexion[iControles].NombreCampo))
					{

						LaConsulta[iControles].Campo = "CONVERT(VARCHAR," + LaConexion[iControles].NombreCampo + ")";
						if (LaConexion[iControles].NombreControl is RadMaskedEditBox)
						{
							if (LaConexion[iControles].NombreControl.Visible)
							{
								LaConsulta[iControles].Busqueda = "'%" + Convert.ToString(LaConexion[iControles].NombreControl.FormattedText()).Trim() + "%'" + " ";
							}
							else
							{
								LaConsulta[iControles].Busqueda = "'" + Convert.ToString(LaConexion[iControles].NombreControl.FormattedText()).Trim() + "'" + " ";
							}
						}
						else
						{
							if (LaConexion[iControles].NombreControl.Visible)
							{
								LaConsulta[iControles].Busqueda = "'%" + Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() + "%'" + " ";
							}
							else
							{
								LaConsulta[iControles].Busqueda = "'" + Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() + "'" + " ";
							}
						}

					}
					else if (ComprobarFechas(LaConexion[iControles].NombreCampo))
					{ 
						LaConsulta[iControles].Campo = LaConexion[iControles].NombreCampo;
						stFecha = (Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim());
						System.DateTime TempDate = DateTime.FromOADate(0);
						stFecha = (DateTime.TryParse(stFecha, out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : stFecha;
						LaConsulta[iControles].Busqueda = "'" + stFecha + "'" + " ";

					}
					else
					{

						LaConsulta[iControles].Campo = LaConexion[iControles].NombreCampo;
						if (LaConexion[iControles].NombreControl is RadMaskedEditBox)
						{
							if (LaConexion[iControles].NombreControl.Visible)
							{
								LaConsulta[iControles].Busqueda = "'%" + Convert.ToString(LaConexion[iControles].NombreControl.FormattedText()).Trim() + "%'" + " ";
							}
							else
							{
								LaConsulta[iControles].Busqueda = "'" + Convert.ToString(LaConexion[iControles].NombreControl.FormattedText()).Trim() + "'" + " ";
							}
						}
						else
						{
							if (LaConexion[iControles].NombreControl.Visible)
							{
								LaConsulta[iControles].Busqueda = "'%" + Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() + "%'" + " ";
							}
							else
							{
								LaConsulta[iControles].Busqueda = "'" + Convert.ToString(LaConexion[iControles].NombreControl.Text).Trim() + "'" + " ";
							}
						}

					}
				}
			}

			this.Cursor = Cursors.Default;

		}



		private void ControlarScroll()
		{
			int NewLargeChange = 0;

			//Ahora tenemos que comprobar si han cogido en los campos
			//o si tenemos que usar el scroll
			if (viNuevoTop > ((float) (frmDetalle.Height * 15)))
			{
				scbvCampos.Enabled = true;
				scbvCampos.Minimum = 255;
				scbvCampos.Maximum = (Convert.ToInt32((viNuevoTop - ((float) (frmDetalle.Height * 15))) - 255) + scbvCampos.LargeChange - 1);
				NewLargeChange = viNuevoTop - ( (frmDetalle.Height * 15));
				scbvCampos.Maximum = scbvCampos.Maximum + NewLargeChange - scbvCampos.LargeChange;
				scbvCampos.LargeChange = NewLargeChange;
				scbvCampos.SmallChange = Convert.ToInt32((Convert.ToInt32(viNuevoTop - ((float) (frmDetalle.Height * 15)))) / 10);
			}

			//Esta variable (iDiferencia) me sirve luego para poder controlar los
			//l�mites de la ScrollBar
			iDiferencia = Convert.ToInt32(((float) (frmDetalle.Height * 15)) - ((float) (frmDetalleFondo.Height * 15)));

			//Obtengo el n� de clicks que necesita la scroll para
			//que se vean todos los campos.
			//Esto lo saco restando el espacio total de los campos menos el espacio
			//de los campos que se ven, y dividiendo esto entre 150, ya que
			//este es el desplazamiento que se hace en cada click
			lClicks = Convert.ToInt32((((float) (frmDetalle.Height * 15)) - ((float) (FrmDetalleMedio.Height * 15))) / 150);

			//Si la variable lClick es <= que cero, significa que no necesito
			//la scroll, ya que todos los campos caben en el espacio disponible
			//para verlos
			if (lClicks <= 0)
			{
				//oculto la scroll
				scbvCampos.Visible = false;
			}
			else
			{
				//Si necesito la scroll, pongo el salto de la scroll, y esto lo hallo
				//dividiendo el valor m�ximo entre el n� de clicks
				scbvCampos.SmallChange = (scbvCampos.Maximum - (scbvCampos.LargeChange + 1)) / ((int) lClicks);
				NewLargeChange = scbvCampos.SmallChange;
				scbvCampos.Maximum = scbvCampos.Maximum + NewLargeChange - scbvCampos.LargeChange;
				scbvCampos.LargeChange = NewLargeChange;
			}

		}

		private void DejarHuecoParaCombo(int iIndiceControl)
		{
			//Dejamos un hueco en cada array que referencia a los controles para
			//introducir un nuevo control: un COMBO,

			//Tambi�n hago lo mismo con las etiquetas

			LaConexion = ArraysHelper.RedimPreserve(LaConexion, new int[]{LaConexion.GetUpperBound(0) + 2});
			astrElConjunto = ArraysHelper.RedimPreserve(astrElConjunto, new int[]{astrElConjunto.GetUpperBound(0) + 2});
			aLongitud = ArraysHelper.RedimPreserve(aLongitud, new int[]{aLongitud.GetUpperBound(0) + 2});
			aNumeroColumna = ArraysHelper.RedimPreserve(aNumeroColumna, new int[]{aNumeroColumna.GetUpperBound(0) + 2});
			LbControlEtiqueta = ArraysHelper.RedimPreserve(LbControlEtiqueta, new int[]{LbControlEtiqueta.GetUpperBound(0) + 2});

			int i = aLongitud.GetUpperBound(0) - 1;
			while (i > iIndiceControl - 1)
			{

				LaConexion[i + 1].NombreControl = LaConexion[i].NombreControl;
				LaConexion[i + 1].NombreCampo = LaConexion[i].NombreCampo;
				LaConexion[i + 1].NombreTabla = LaConexion[i].NombreTabla;
				LaConexion[i + 1].NumeroColumna = LaConexion[i].NumeroColumna;

				astrElConjunto[i + 1].NombreControl = astrElConjunto[i].NombreControl;
				astrElConjunto[i + 1].dcoltabmanco = astrElConjunto[i].dcoltabmanco;
				astrElConjunto[i + 1].dcoltabmanla = astrElConjunto[i].dcoltabmanla;
				astrElConjunto[i + 1].dtablamancor = astrElConjunto[i].dtablamancor;
				astrElConjunto[i + 1].fEsta = astrElConjunto[i].fEsta;
				astrElConjunto[i + 1].gcolumncaman = astrElConjunto[i].gcolumncaman;
				astrElConjunto[i + 1].ggrunimanaut = astrElConjunto[i].ggrunimanaut;
				astrElConjunto[i + 1].gtablamanaut = astrElConjunto[i].gtablamanaut;
				astrElConjunto[i + 1].gtipocolumna = astrElConjunto[i].gtipocolumna;
				astrElConjunto[i + 1].gtipodato = astrElConjunto[i].gtipodato;
				astrElConjunto[i + 1].iadmitenull = astrElConjunto[i].iadmitenull;
				astrElConjunto[i + 1].iascendente = astrElConjunto[i].iascendente;
				astrElConjunto[i + 1].icolfborrado = astrElConjunto[i].icolfborrado;
				astrElConjunto[i + 1].icolformato = astrElConjunto[i].icolformato;
				astrElConjunto[i + 1].icolmascara = astrElConjunto[i].icolmascara;
				astrElConjunto[i + 1].imayusculas = astrElConjunto[i].imayusculas;
				astrElConjunto[i + 1].ipassword = astrElConjunto[i].ipassword;
				astrElConjunto[i + 1].ireqconsulta = astrElConjunto[i].ireqconsulta;
				astrElConjunto[i + 1].itamaniofijo = astrElConjunto[i].itamaniofijo;
				astrElConjunto[i + 1].nordcolinun = astrElConjunto[i].nordcolinun;
				astrElConjunto[i + 1].nordenacion = astrElConjunto[i].nordenacion;
				astrElConjunto[i + 1].nordencolpk = astrElConjunto[i].nordencolpk;

				//como estamos introduciendo una nueva columna en el spread,
				//sumamos uno mas a este dato, que es el que nos dice el
				//n� de la columna  en el spread
				astrElConjunto[i + 1].nordencoltab = (short) (astrElConjunto[i].nordencoltab + 1);

				astrElConjunto[i + 1].npartedecima = astrElConjunto[i].npartedecima;
				astrElConjunto[i + 1].nparteentera = astrElConjunto[i].nparteentera;
				astrElConjunto[i + 1].valordefecto = astrElConjunto[i].valordefecto;
				astrElConjunto[i + 1].vformato = astrElConjunto[i].vformato;
				astrElConjunto[i + 1].vmascara = astrElConjunto[i].vmascara;

				aLongitud[i + 1] = aLongitud[i];
				aNumeroColumna[i + 1] = aNumeroColumna[i];
				LbControlEtiqueta[i + 1] = LbControlEtiqueta[i]; //lbEtiqueta(i)

				i--;
			}

		}

		private void HacerSelect(string StTabla, string StComando, bool fOrdenar)
		{
			int p = 0;
			bool fEsta = false;
			//stConsulta = StComando & " "
			ModuloMantenim.stConsulta = " ";
			string ConsultaTempo = StComando;
			string ConsultaFiltro = " ";
			if (fOrden)
			{
				AStParametro = new ModuloMantenim.StrParametro[AStOrden.GetUpperBound(0) + 1];
				//Montamos el WHERE necesario para hacer el ReQuery:
				//concatenando las columnas por las que se ordena y
				//las que forman parte de la clave primaria
				for (int i = 0; i <= AStOrden.GetUpperBound(0); i++)
				{
					switch(AStOrden[i].StTipo.Trim())
					{
						case "VARCHAR" : case "CHAR" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(" + AStOrden[i].iLongitud.ToString() + ")," + AStOrden[i].StColumna + ")"; 
							break;
						case "NUMERIC" : case "INT" : case "SMALLINT" : 
							//Para las columnas num�ricas tengo que coger el n�mero, 
							//concatenarle espacios por la izquierda hasta la longitud 
							//del campo 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " REPLICATE('0'," + AStOrden[i].iLongitud.ToString() + "-DATALENGTH(RTRIM(CONVERT(CHAR(" + AStOrden[i].iLongitud.ToString() + ")," + AStOrden[i].StColumna + ")))) + " + "RTRIM(CONVERT(CHAR(" + AStOrden[i].iLongitud.ToString() + ")," + AStOrden[i].StColumna + "))"; 
							break;
						case "DATETIME" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(10)," + AStOrden[i].StColumna + ",111)"; 
							break;
					}
					AStParametro[i].StTipo = AStOrden[i].StTipo;
					AStParametro[i].iLongitud = AStOrden[i].iLongitud;
					AStParametro[i].iOrdenColumnaSpread = AStOrden[i].iOrdenColumnaSpread;
					AStParametro[i].StColumna = AStOrden[i].StColumna;
					p = i;
					if (i != AStParametro.GetUpperBound(0))
					{
						ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " + ";
					}

				}
			}

			bool fDimensionar = true;

			if (!fOrden)
			{
				AStParametro = new ModuloMantenim.StrParametro[1];
				fDimensionar = false;
			}

			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{

				fEsta = false;
				foreach (ModuloMantenim.StrOrden AStOrden_item in AStOrden)
				{
					if (AStPK[i].StColumna.Trim() == AStOrden_item.StColumna.Trim())
					{
						fEsta = true;
						break;
					}
				}

				if (!fEsta)
				{
					if (fDimensionar)
					{
						AStParametro = ArraysHelper.RedimPreserve(AStParametro, new int[]{p + 2});
						ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " + ";
						if (fOrdenar)
						{
							ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + " , ";
						}
					}

					switch(AStPK[i].StTipo.Trim())
					{
						case "VARCHAR" : case "CHAR" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ")"; 
							break;
						case "NUMERIC" : case "INT" : case "SMALLINT" : 
							//Para las columnas num�ricas tengo que coger el n�mero, 
							//concatenarle espacios por la izquierda hasta la longitud 
							//del campo 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " SPACE(" + AStPK[i].iLongitud.ToString() + "-DATALENGTH(RTRIM(CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ")))) + " + "RTRIM(CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + "))"; 
							break;
						case "DATETIME" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ",112)" + " + " + " CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ",108)"; 
							 
							break;
					}

					if (fOrdenar)
					{
						ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + AStPK[i].StColumna;
					}

					p++;
					AStParametro[p].StTipo = AStPK[i].StTipo;
					AStParametro[p].iLongitud = AStPK[i].iLongitud;
					AStParametro[p].iOrdenColumnaSpread = AStPK[i].iOrdenColumnaSpread;
					AStParametro[p].StColumna = AStPK[i].StColumna;
					fDimensionar = true;
				}
			}
			if (ConsultaFiltro != " ")
			{
				ConsultaFiltro = ConsultaFiltro.Substring(0, Math.Min(ConsultaFiltro.Length - 4, ConsultaFiltro.Length));
			}
			ModuloMantenim.stConsulta = ConsultaTempo + ConsultaFiltro + ModuloMantenim.stConsulta;
			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " > @paramStr ";

			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + ModuloMantenim.stSQLOrder;

		}
		private void LlenarArrayEtiquetas()
		{

			LbControlEtiqueta = ArraysHelper.RedimPreserve(LbControlEtiqueta, new int[]{lbEtiqueta.GetUpperBound(0) + 1});

			int tempForVar = lbEtiqueta.GetUpperBound(0);
			for (int i = 0; i <= tempForVar; i++)
			{
				LbControlEtiqueta[i] = lbEtiqueta[i];
			}

		}

		private void DevolverDatos()
		{
			//Procedimiento que se utiliza cuando la pantalla de
			//MantenimientoAutomatico ha sido llamada desde un combo.
			//Se guardan en arrays el nombre de la columna, el valor de
			//la misma y el tipo de dato que vamos a devolver a la pantalla
			//que tiene el combo llamador


			AStClaves = ArraysHelper.InitializeArray<string>(AStColumnasFK.GetUpperBound(0) + 1);
			AStTipoCol = ArraysHelper.InitializeArray<string>(AStColumnasFK.GetUpperBound(0) + 1);


			for (int i = 0; i <= AStColumnasFK.GetUpperBound(0); i++)
			{
				foreach (ModuloMantenim.strConjuntoResultados astrElConjunto_item in astrElConjunto)
				{
					if (AStColumnasFK[i].Trim() == astrElConjunto_item.gcolumncaman.Trim())
					{
						//Comprobamos si es control fecha
						if (astrElConjunto_item.NombreControl.Name == "sdcFecha")
						{
							System.DateTime TempDate = DateTime.FromOADate(0);
							AStClaves[i] = (DateTime.TryParse(Convert.ToString(astrElConjunto_item.NombreControl.Text).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : Convert.ToString(astrElConjunto_item.NombreControl.Text).Trim();
							//Comprobamos si el control es Masked Edit
						}
						else if (astrElConjunto_item.NombreControl.Name == "mebMascara")
						{ 
							AStClaves[i] = Convert.ToString(astrElConjunto_item.NombreControl.FormattedText()).Trim();
						}
						else
						{
							AStClaves[i] = Convert.ToString(astrElConjunto_item.NombreControl.Text).Trim();
						}
						AStTipoCol[i] = astrElConjunto_item.gtipodato.Trim();
						break;
					}
				}
			}
		}

		private string Padl(string stCadena, int iEspacios, string stRelleno = " ")
		{
			//Funcion que concatena blancos u otro caracter por la izquierda
			return new string(stRelleno[0], iEspacios - stCadena.Trim().Length) + stCadena.Trim();
		}
		private void InhabilitarControles()
		{
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].icolfborrado.ToString().ToUpper() == "S")
				{
					astrElConjunto[i].NombreControl.Enabled = true;
					iIndexFecha = ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl);
				}
				else
				{
					astrElConjunto[i].NombreControl.Enabled = false;
				}
			}
		}

		private string MontarConsulta(string stTablaSQL, string stSQLSelect, bool fAND)
		{

			string result = String.Empty;
			bool fQuitarOr = false;

			this.Cursor = Cursors.WaitCursor;

			string stSQLFrom = " from " + stTablaSQL;
			StringBuilder stSQLWhere = new StringBuilder();
			stSQLWhere.Append(" where ");

			for (int iIndiceConsulta = 0; iIndiceConsulta <= LaConsulta.GetUpperBound(0) - 1; iIndiceConsulta++)
			{
				if (LaConsulta[iIndiceConsulta].Busqueda != "")
				{
					if (!ComprobarFechas(LaConsulta[iIndiceConsulta].Campo))
					{
						stSQLWhere.Append("(" + LaConsulta[iIndiceConsulta].Campo + " like " + 
						                  LaConsulta[iIndiceConsulta].Busqueda + ")" + vstCriterioConsulta);
					}
					else
					{
						stSQLWhere.Append("(" + LaConsulta[iIndiceConsulta].Campo + " = " + 
						                  LaConsulta[iIndiceConsulta].Busqueda + ")" + vstCriterioConsulta);
					}
					fQuitarOr = true;
				}
			}

			//Para quitar el �ltimo OR
			if (fQuitarOr)
			{ //Si ha rellenado la Where, se queda con un OR
				//de m�s
				stSQLWhere = new StringBuilder(stSQLWhere.ToString().Substring(0, Math.Min(stSQLWhere.ToString().Length - 4, stSQLWhere.ToString().Length)));
			}
			else
			{
				//Si no ha rellenado la Where, quito el "where"
				stSQLWhere = new StringBuilder("");
			}

			//**************************************
			if (fAND && fQuitarOr)
			{ //Si hay que a�adir un "and" y ha
				//rellenado la Where
				stSQLWhere.Append(" and ");
			}
			else if (fAND)
			{  //Si no ha rellenado la where pero hay que
				//poner un "and", se pone un "where"
				stSQLWhere = new StringBuilder(" where ");
			}
			//**************************************

			string stSqlBuscar = stSQLSelect + stSQLFrom + stSQLWhere.ToString();
			result = stSqlBuscar;

			this.Cursor = Cursors.Default;

			return result;
		}


		private void CrearEtiquetaCombo(int iIndiceControl)
		{

			//Crea una etiqueta nueva,(si no es la primera)

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				//vfComprobador es un comprobador por si hemos creado ya
				//el primer control
				if (!vfComprobador)
				{
					//Le pongo al caption de la etiqueta el campo VLABELCOMBO que he
					//obtenido anteriormente de la vista
					lbEtiqueta[0].Text = AStColFK[0].StLabelCombo.Trim() + " :";
					LbControlEtiqueta[iIndiceControl] = lbEtiqueta[0];
					iIndiceEtiqueta = 1;
					vfComprobador = true;
					return;
				}
			}
			ControlArrayHelper.LoadControl(this, "lbEtiqueta", iIndiceEtiqueta);
			//Le pongo al caption de la etiqueta el campo VLABELCOMBO que he
			//obtenido anteriormente de la vista
			lbEtiqueta[iIndiceEtiqueta].Text = AStColFK[0].StLabelCombo.Trim() + " :";
			LbControlEtiqueta[iIndiceControl] = lbEtiqueta[iIndiceEtiqueta];
			iIndiceEtiqueta++;

		}

		private void CrearEtiqueta()
		{
			//Crea una etiqueta nueva,(si no es la primera)

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				if (!vfComprobador)
				{
					iIndiceEtiqueta = 1;
					vfComprobador = true;
					return;
				}
			}

			ControlArrayHelper.LoadControl(this, "lbEtiqueta", iIndiceEtiqueta);
			iIndiceEtiqueta++;

		}
		private bool FBorradoNula()
		{

			bool result = false;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(ObtenerConsultaFBorrado(), ModuloMantenim.RcManten);
			DataSet RsFBorrado = new DataSet();
			tempAdapter.Fill(RsFBorrado);
			result = Convert.IsDBNull(RsFBorrado.Tables[0].Rows[0][ModuloMantenim.StFBORRADO]);
			RsFBorrado.Close();
			return result;
		}

		private void ObtenerConstantesCombo()
		{
			//Obtengo las constantes que van a estar el los combos
			//de consulta de campos:ALGO, NADA, TODO, ETC
			string StComando = "select VALFANU1, VALFANU2, VALFANU3 " + "from SCONSGLO " + "where GCONSGLO='ALGONATO'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsAlgo = new DataSet();
			tempAdapter.Fill(RsAlgo);
			if (RsAlgo.Tables[0].Rows.Count != 0)
			{
				StALGO = Convert.ToString(RsAlgo.Tables[0].Rows[0]["valfanu1"]).Trim();
				StNADA = Convert.ToString(RsAlgo.Tables[0].Rows[0]["valfanu2"]).Trim();
				StTODO = Convert.ToString(RsAlgo.Tables[0].Rows[0]["valfanu3"]).Trim();
			}
			RsAlgo.Close();
			IMaxLen = StALGO.Length;
			if (IMaxLen < StNADA.Length)
			{
				IMaxLen = StNADA.Length;
			}
			if (IMaxLen < StTODO.Length)
			{
				IMaxLen = StTODO.Length;
			}
		}

		private string ObtenerConsultaFBorrado()
		{
			StringBuilder stSQLBorrar = new StringBuilder();

			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{
				if (i == 0)
				{
					stSQLBorrar = new StringBuilder("SELECT " + ModuloMantenim.StFBORRADO + " FROM " + vstNombreTabla);
					stSQLBorrar.Append(" where ");
				}
				else
				{
					stSQLBorrar.Append(" and ");
				}

				switch(AStPK[i].StTipo)
				{
					case "DATETIME" :  //Campo fecha 
						//Formateamos la fecha DD/MM/YYYY 
						stSQLBorrar.Append("CONVERT(char(10)," + AStPK[i].StColumna + ",103)"); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							System.DateTime TempDate = DateTime.FromOADate(0);
							stSQLBorrar.Append("=" + "'" + ((DateTime.TryParse(AStPK[i].stTexto.Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : AStPK[i].stTexto.Trim()) + "'");
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
					case "VARCHAR" : case "CHAR" :  //Campo texto con o sin m�scara 
						stSQLBorrar.Append(AStPK[i].StColumna); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							stSQLBorrar.Append("=" + "'" + AStPK[i].stTexto.Trim() + "'");
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
					case "NUMERIC" : case "SMALLINT" : case "INT" :  //Campo num�rico 
						stSQLBorrar.Append(AStPK[i].StColumna); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							stSQLBorrar.Append("=" + AStPK[i].stTexto.Trim());
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
				}
			}
			return stSQLBorrar.ToString();
		}


		private void PintarEtiqueta()
		{
			//24/10/1997
			//Comprobar primero si existe espacio suficiente el el Frame

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				lbEtiqueta[0].Top = (int) ((ModuloMantenim.COiSupEtiqueta + viNuevoTop) / 15);
				lbEtiqueta[0].Left = (int) (ModuloMantenim.COiIzda / 15);
				lbEtiqueta[0].Visible = true;
				viNuevoLeft = Convert.ToInt32(((float) (lbEtiqueta[0].Left * 15)) + ((float) (lbEtiqueta[0].Width * 15))); //+ COiSeparacion
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["iadmitenull"]) == "N")
				{
					lbEtiqueta[0].Font = lbEtiqueta[0].Font.Change(bold:true);
				}
				else
				{
					lbEtiqueta[0].Font = lbEtiqueta[0].Font.Change(bold:false);
				}
			}
			else
			{
				lbEtiqueta[iIndice].Top = (int) ((ModuloMantenim.COiDiferenciaControl + viNuevoTop + ModuloMantenim.COiSeparacion) / 15);
				lbEtiqueta[iIndice].Left = (int) (ModuloMantenim.COiIzda / 15);
				lbEtiqueta[iIndice].Visible = true;
				viNuevoLeft = Convert.ToInt32(((float) (lbEtiqueta[iIndice].Left * 15)) + ((float) (lbEtiqueta[iIndice].Width * 15))); //+ COiSeparacion
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["iadmitenull"]) == "N")
				{
					lbEtiqueta[iIndice].Font = lbEtiqueta[iIndice].Font.Change(bold:true);
				}
				else
				{
					lbEtiqueta[iIndice].Font = lbEtiqueta[iIndice].Font.Change(bold:false);
				}

			}

		}

		public void RecogerDatosMantenimiento(string[] AStClaves, string[] AStColumnas, string[] AStTipoCol, int iIndiceCombo)
		{

			//AStColumnas --> Nombre de las columnas clave
			//AStClaves   --> Valor de las columnas clave

			//Recibo la Descripci�n que tiene que ir en el combo y dos arrays:
			//uno con los valores de claves y otro con el nombre de las columnas
			//que son clave

			int l = 0;

			bool fEncontrado = false;
			int INumColClaveEncontradas = 0;
			bool fComboHabilitado = false;

			int[] AINumColCombo = new int[AStClaves.GetUpperBound(0) + 1];

			fComboHabilitado = !(!cbbCombo[iIndiceCombo].Enabled || cbbCombo[iIndiceCombo].Items.Count - 1 < 0);

			//***************************************************************************
			StringBuilder CodigodeClave = new StringBuilder();
			if (fComboHabilitado)
			{
				for (int i = 0; i <= AStColumnas.GetUpperBound(0); i++)
				{
					foreach (ModuloMantenim.StrFKCombo AStColFKCombo_item in AStColFKCombo)
					{
						if (AStColumnas[i].Trim() == AStColFKCombo_item.StColPadre.Trim() && iIndiceCombo == AStColFKCombo_item.iIndiceCombo)
						{
							for (int k = 0; k <= astrElConjunto.GetUpperBound(0) - 1; k++)
							{
								if (AStColFKCombo_item.StColHija.Trim() == astrElConjunto[k].gcolumncaman.Trim())
								{
									astrElConjunto[k].NombreControl.Text = AStClaves[i].Trim();
									CodigodeClave.Append(AStClaves[i].Trim() + ",");
									AINumColCombo[i] = AStColFKCombo_item.IIndiceColEnCombo;
									break;
								}
							}
							break;
						}
					}
				}
				CodigodeClave = new StringBuilder(CodigodeClave.ToString().Substring(0, Math.Min(CodigodeClave.ToString().Length - 1, CodigodeClave.ToString().Length)));
				//Con esta variable cuento el n� de columnas clave que van coincidiendo
				//en el combo por cada registro.
				//En el momento que INumColClaveEncontradas sea igual al n� de columnas
				//clave del combo, significa que ese es el registro buscado
				INumColClaveEncontradas = 0;
				fEncontrado = false;
				for (int i = 0; i <= cbbCombo[iIndiceCombo].Items.Count - 1; i++)
				{
					object tempRefParam2 = AINumColCombo[0];
					object tempRefParam3 = i;
					string tempRefParam = Convert.ToString(cbbCombo[iIndiceCombo].Items[i].Value);
					string tempRefParam4 = AStClaves[0].Trim();
					if (QuitaCeros(tempRefParam, AStTipoCol[0]).Trim().ToUpper() == QuitaCeros(tempRefParam4, AStTipoCol[0]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam3);
						AINumColCombo[0] = Convert.ToInt32(tempRefParam2);
						for (int k = 0; k <= AINumColCombo.GetUpperBound(0); k++)
						{
							object tempRefParam6 = AINumColCombo[k];
							object tempRefParam7 = i;
							string tempRefParam5 = Convert.ToString(cbbCombo[iIndiceCombo].Items[i].Value);
							string tempRefParam8 = AStClaves[k].Trim();
							if (QuitaCeros(tempRefParam5, AStTipoCol[k]).Trim().ToUpper() == QuitaCeros(tempRefParam8, AStTipoCol[k]).Trim().ToUpper())
							{
								i = Convert.ToInt32(tempRefParam7);
								AINumColCombo[k] = Convert.ToInt32(tempRefParam6);
								if (INumColClaveEncontradas == AINumColCombo.GetUpperBound(0))
								{
									cbbCombo[iIndiceCombo].SelectedIndex = i;
									cbbCombo[iIndiceCombo].SelectionStart = 0;
									cbbCombo[iIndiceCombo].SelectionLength = cbbCombo[iIndiceCombo].MaxLength;
									fEncontrado = true;
									break;
								}
								INumColClaveEncontradas++;
							}
							else
							{
								i = Convert.ToInt32(tempRefParam7);
								AINumColCombo[k] = Convert.ToInt32(tempRefParam6);
							}
						}
						if (fEncontrado)
						{
							break;
						}
						else
						{
							INumColClaveEncontradas = 0;
						}

					}
					else
					{
						i = Convert.ToInt32(tempRefParam3);
						AINumColCombo[0] = Convert.ToInt32(tempRefParam2);
					}
				}
				if (!fEncontrado)
				{
					cbbCombo[iIndiceCombo].Text = "";
					cbbCombo[iIndiceCombo].SelectedIndex = -1;
					RadMessageBox.Show("C�digo '" + TbClaves[iIndiceCombo].Text + "' no encontrado", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
					TbClaves[iIndiceCombo].Text = "";
				}
				else
				{
					TbClaves[iIndiceCombo].Text = CodigodeClave.ToString();
				}
			}
			else
			{
				EncontrarDescripcionCombo(AStClaves, AStColumnas, AStTipoCol, iIndiceCombo);
			}
		}
		private void RecorrerRegistros()
		{
			//Recorro el resulset de la consulta y lleno la spread
			double xx = 0;
			int iRecorrerArray = 0;
			object stTexto = null;

			viColumnaPassword = ComprobarCamposPassword();

			sprMultiple.Col = 0;

			int tempForVar = ModuloMantenim.RsManten.Tables[0].Rows.Count;
			for (iIndice = 1; iIndice <= tempForVar; iIndice++)
			{ //Recorrer hasta final del Resultset
				//Siguiente registro de la spread
				sprMultiple.MaxRows++;
				sprMultiple.Row = sprMultiple.MaxRows;
				for (iindice2 = 0; iindice2 <= sprMultiple.MaxCols - 1; iindice2++)
				{
					sprMultiple.Col = iindice2 + 1;

					//**************************************************************
					//Comprobamos si la columna es password
					if (sprMultiple.Col == viColumnaPassword)
					{
						sprMultiple.setTypeEditPassword(true);
					}

					if (aNumeroColumna.GetUpperBound(0) > iRecorrerArray)
					{
						iRecorrerArray++;
					}

					stTexto = ModuloMantenim.RsManten.Tables[0].Rows[0][iindice2];
					if (Convert.IsDBNull(stTexto))
					{
						sprMultiple.Text = "";
					}
					else
					{
						//****************************CRIS***********************************
						for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
						{
							if (ModuloMantenim.RsManten.Tables[0].Columns[iindice2].ColumnName.ToUpper() == astrElConjunto[i].gcolumncaman.ToUpper())
							{
								//********************************CRIS********************************
								//Calculamos la longitud m�xima que va a tener el campo en la spread
								//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
								//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
								//posici�n para el punto decimal
								if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
								{
									if (astrElConjunto[i].nparteentera != 0)
									{
										sprMultiple.setTypeEditLen(astrElConjunto[i].nparteentera + astrElConjunto[i].npartedecima + 2);
									}
									else
									{
										sprMultiple.setTypeEditLen(astrElConjunto[i].nparteentera + astrElConjunto[i].npartedecima + 1);
									}
								}
								else
								{
									sprMultiple.setTypeEditLen(astrElConjunto[i].nparteentera + astrElConjunto[i].npartedecima);
								}
								//*******************************************************************
								//Miro si la columna es fecha para formatearla a DD/MM/YYYY
								//si no tiene formato
								if (ModuloMantenim.RsManten.Tables[0].Columns[iindice2].DataType == typeof(DateTime) && astrElConjunto[i].vformato.Trim() == "")
								{
									sprMultiple.Text = Convert.ToDateTime(stTexto).ToString("dd/MM/yyyy");
								}
								else
								{
									if (astrElConjunto[i].vformato != "")
									{
										sprMultiple.Text = StringsHelper.Format(stTexto, astrElConjunto[i].vformato);
									}
									else
									{
										if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
										{
											if (ModuloMantenim.RsManten.Tables[0].Columns[iindice2].DataType == typeof(decimal))
											{
												xx = Convert.ToDouble(stTexto);
											}
											else
											{
												xx = Conversion.Val(Convert.ToString(stTexto));
											}
											sprMultiple.Text = xx.ToString();
										}
										else
										{
											sprMultiple.Text = Convert.ToString(stTexto);
										}
									}
								}
								break;
							}
						}
						//*******************************************************************
					}
				}
				ModuloMantenim.RsManten.MoveNext();
				iRecorrerArray = 0;
			}
			//La consulta no se cierra, ya que se volver� a utilizar
			//en el ReQuery
		}




		private void VolcarArraySpread_EnSpread()
		{
			//Recorro el array de la spread y lleno el spread

			int Row = 0;

			//viColumnaPassword = ComprobarCamposPassword
			sprMultiple.Row = sprMultiple.MaxRows;
			sprMultiple.MaxRows = sprMultiple.MaxRows + AStSpread.GetUpperBound(0) + 1;
			for (Row = 0; Row <= AStSpread.GetUpperBound(0); Row++)
			{ //Recorro las filas del array
				sprMultiple.Row++;
				for (int col = 0; col <= AStSpread.GetUpperBound(1); col++)
				{ //Recorro las columnas del array
					sprMultiple.Col = col + 1;

					if (AStSpread[Row, col] != "")
					{
						if (sprMultiple.getTypeEditLen() < AStSpread[Row, col].Trim().Length)
						{
							sprMultiple.setTypeEditLen(AStSpread[Row, col].Trim().Length);
						}
						sprMultiple.Text = AStSpread[Row, col].Trim();
					}
					if (sprMultiple.Col == viColumnaPassword)
					{
						sprMultiple.setTypeEditPassword(true);
					}
				}
			}
		}

		private void VolcarRegistrosEnArraySpread()
		{
			//Recorro el resulset de la consulta y lleno el array de la spread


			//viColumnaPassword = ComprobarCamposPassword

			//Array de dos dimensiones: el primero para la FILA y el segundo
			//para la COLUMNA
			AStSpread = ArraysHelper.InitializeArray<string[, ]>(new int[]{ModuloMantenim.RsManten.Tables[0].Rows.Count, ModuloMantenim.RsManten.Tables[0].Columns.Count}, new int[]{0, 0});

			for (int Row = 0; Row <= ModuloMantenim.RsManten.Tables[0].Rows.Count - 1; Row++)
			{ //Recorrer hasta final del Resultset
				for (int col = 0; col <= ModuloMantenim.RsManten.Tables[0].Columns.Count - 1; col++)
				{
					if (Convert.IsDBNull(ModuloMantenim.RsManten.Tables[0].Columns[col]))
					{
						AStSpread[Row, col] = "";
					}
					else if (ModuloMantenim.RsManten.Tables[0].Columns[col].DataType == typeof(DateTime))
					{ 
						//Miro si la columna es fecha para formatearla a DD/MM/YYYY
						AStSpread[Row, col] = Convert.ToDateTime(ModuloMantenim.RsManten.Tables[0].Rows[0][col]).ToString("dd/MM/yyyy");
					}
					else
					{
						if (ModuloMantenim.RsManten.Tables[0].Columns[col].DataType == typeof(decimal) || ModuloMantenim.RsManten.Tables[0].Columns[col].DataType == typeof(int))
						{
							if (ModuloMantenim.RsManten.Tables[0].Columns[col].DataType == typeof(decimal))
							{
								AStSpread[Row, col] = Double.Parse(Convert.ToString(ModuloMantenim.RsManten.Tables[0].Rows[0][col]).Trim()).ToString();
							}
							else
							{
								AStSpread[Row, col] = Conversion.Val(Convert.ToString(ModuloMantenim.RsManten.Tables[0].Rows[0][col]).Trim()).ToString();
							}
						}
						else
						{
							AStSpread[Row, col] = Convert.ToString(ModuloMantenim.RsManten.Tables[0].Rows[0][col]).Trim();
						}
					}
				}
				ModuloMantenim.RsManten.MoveNext();
			}
			//La consulta no se cierra, ya que se volver� a utilizar
			//en el ReQuery
		}

		private void IntroducirFKEnArraySpread(string StComando1, string StComando2, bool fDimensionarCombo)
		{
			DataSet RsFK = null;
			bool fAND = false;
			StringBuilder StComando = new StringBuilder();
			int ICol = 0;

			//Inserta nuevas columnas en el array del
			//spread que van a contener el valor de las FK

			string StSelect = "Select ";

			for (int k = 0; k <= AStSpread.GetUpperBound(0); k++)
			{
				//Monto la sentencia WHERE para cada fila de la spread y
				//Oculto las columnas de la spread que son FK

				StComando = new StringBuilder(" where ");
				fAND = false;
				foreach (ModuloMantenim.StrFK AStColFK_item in AStColFK)
				{

					if (fAND)
					{
						StComando.Append(" and ");
					}
					fAND = true;

					//Ademas de a�adir cada columna al WHERE de la sentencia,
					//busco el n� de columna en el spread a la que corresponde
					//cada campo que es FK y la oculto.
					//Tambi�n oculto el control asociado a esa columna
					for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].gcolumncaman.Trim() == AStColFK_item.StColHija.Trim())
						{

							//La booleana fDimensionarCombo, me dice si tengo que redimensionar o no
							//el array, ya que la 1� vez est� dimensionado
							if (fDimensionarCombo)
							{
								AStColFKCombo = ArraysHelper.RedimPreserve(AStColFKCombo, new int[]{AStColFKCombo.GetUpperBound(0) + 2});
							}

							fDimensionarCombo = true;

							//Esto me sirve luego para poder identificar cada columna
							//que es clave a que combo pertenece y que n� de columna
							//ocupa en ese combo
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StColHija = AStColFK_item.StColHija.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].iIndiceCombo = (short) viIndiceCombo;
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].IIndiceColEnCombo = (short) vIIndiceColEnCombo;
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StLabelCombo = AStColFK_item.StLabelCombo.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StTablaPadre = AStColFK_item.StTablaPadre.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StColPadre = AStColFK_item.StColPadre.Trim();

							//*******************************************************************************
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].fVistaNula = AStColFK_item.fVistaNula;
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StNombreFK = AStColFK_item.StNombreFK.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StVistaCombo = AStColFK_item.StVistaCombo.Trim();
							//*******************************************************************************�

							vIIndiceColEnCombo++;

							astrElConjunto[i].NombreControl.Visible = false;

							ICol = astrElConjunto[i].nordencoltab - 1;

							StComando.Append("A." + AStColFK_item.StColPadre + "=");

							if (AStSpread[k, astrElConjunto[i].nordencoltab - 1].Trim() == "")
							{
								//Quitamos el �ltimo igual
								StComando = new StringBuilder(StComando.ToString().Substring(0, Math.Min(StComando.ToString().Length - 1, StComando.ToString().Length)) + " ");
								StComando.Append(" IS NULL");
							}
							else
							{
								//SI ES "CHAR", "VARCHAR" O "DATETIME",
								//EL DATO VA ENTRE COMILLA SIMPLE
								if (AStColFK_item.stTipoDato.Trim() == "VARCHAR" || AStColFK_item.stTipoDato.Trim() == "CHAR")
								{
									StComando.Append("'" + AStSpread[k, astrElConjunto[i].nordencoltab - 1].Trim() + "'");
								}
								else if (AStColFK_item.stTipoDato.Trim() == "DATETIME")
								{ 
									System.DateTime TempDate = DateTime.FromOADate(0);
									StComando.Append("'" + ((DateTime.TryParse(AStSpread[k, astrElConjunto[i].nordencoltab - 1].Trim(), out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : AStSpread[k, astrElConjunto[i].nordencoltab - 1].Trim()) + "'");
								}
								else
								{
									StComando.Append(AStSpread[k, astrElConjunto[i].nordencoltab - 1].Trim());
								}
							}
							//Salgo del bucle FOR porque ya ha encontrado la columna
							break;
						}
					}
				}


				//Monto la consulta con la sentencia SELECT, el WHERE y el ORDER BY
				StComando = new StringBuilder(StSelect + StComando1 + StComando.ToString() + " " + StComando2);

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
				RsFK = new DataSet();
				tempAdapter.Fill(RsFK);

				//Inserto columnas con los datos que tienen que mostrarse
				//en vez de las FK, pero s�lo una columna por campo, no por fila
				if (k == 0)
				{
					//Inserto una columna m�s en el array
					AStSpread = ArraysHelper.RedimPreserve<string[, ]>(AStSpread, new int[]{AStSpread.GetUpperBound(0) + 1, AStSpread.GetUpperBound(1) + 2});
				}

				ICol++;

				ReOrdenarArraySpread(ICol, k);

				//Introducimos los valores de FK en la posici�n delm array
				//de la spread que corresponda
				AStSpread[k, ICol] = "";
				if (RsFK.Tables[0].Rows.Count != 0)
				{
					for (int i = 0; i <= RsFK.Tables[0].Columns.Count - 1; i++)
					{
						AStSpread[k, ICol] = AStSpread[k, ICol] + Convert.ToString(RsFK.Tables[0].Columns[i]) + " ";
					}
				}
				RsFK.Close();

			}

			ReOrdenarColumnas(ICol);
		}

		private void EncontrarColumnasFK(object StModo_optional)
		{
			string StModo = (StModo_optional == Type.Missing) ? String.Empty : StModo_optional as string;

			string StComando2 = String.Empty;
			StringBuilder StOrden = new StringBuilder();
			DataSet RsCombos2 = null;
			bool fDimensionar = false, fDimensionarCombo = false;
			int iLongitudColumna = 0;
			string[] AStCol = null;
			int iColumna = 0;
			string StFrom = String.Empty;

			viIndiceCombo = 0;

			if (StModo_optional == Type.Missing)
			{
				LlenarArrayEtiquetas();
				AStColFKCombo = new ModuloMantenim.StrFKCombo[1];
			}

			//Obtengo los grupos de columnas de FK
			string StComando = "SELECT * FROM SCOLFKPKV1 " + "WHERE GTABCAMANHIJ= " + "'" + vstNombreTabla.Trim() + "'" + "ORDER BY gfkcaman,nordencolfk ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsCombos = new DataSet();
			tempAdapter.Fill(RsCombos);

			//Obtenemos las columnas FK que se van a sustituir

			string StNombreFK = Convert.ToString(RsCombos.Tables[0].Rows[0]["GFKCAMAN"]).Trim();

			while (RsCombos.Tables[0].Rows.Count != 0)
			{

				AStColFK = new ModuloMantenim.StrFK[1];
				fDimensionar = false;

				//Mientras la columna pertenezca al mismo grupo
				//de valores de FK, vamos a�adiendo sus datos a un array
				//de estructura
				foreach (DataRow iteration_row in RsCombos.Tables[0].Rows)
				{

					if (StNombreFK.Trim() == Convert.ToString(iteration_row["GFKCAMAN"]).Trim())
					{
						if (fDimensionar)
						{
							AStColFK = ArraysHelper.RedimPreserve(AStColFK, new int[]{AStColFK.GetUpperBound(0) + 2});
						}

						fDimensionar = true;

						AStColFK[AStColFK.GetUpperBound(0)].StNombreFK = Convert.ToString(iteration_row["GFKCAMAN"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StColHija = Convert.ToString(iteration_row["GCOLCAMANHIJ"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StColPadre = Convert.ToString(iteration_row["GCOLUMNCAMAN"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre = Convert.ToString(iteration_row["GTABCAMANPAD"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StLabelCombo = Convert.ToString(iteration_row["VLABELCOMBO"]).Trim();

						if (Convert.IsDBNull(iteration_row["GVISTACOMBO"]))
						{
							AStColFK[AStColFK.GetUpperBound(0)].fVistaNula = true;
						}
						else
						{
							AStColFK[AStColFK.GetUpperBound(0)].fVistaNula = false;
							AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo = Convert.ToString(iteration_row["GVISTACOMBO"]).Trim();
						}

						//Guardo el tipo de dato de cada columna
						for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
						{
							if (AStColFK[AStColFK.GetUpperBound(0)].StColHija.Trim() == astrElConjunto[i].gcolumncaman.Trim())
							{
								AStColFK[AStColFK.GetUpperBound(0)].stTipoDato = astrElConjunto[i].gtipodato.Trim();
								break;
							}
						}

					}
					else
					{
						//Si pasa por aqu�, significa que ya se
						//ha cambiado de grupo de columnas FK,
						//con lo que sadremos del Do While
						//y llenaremos el combo con los datos correspondientes
						//al anterior grupo de columnas FK
						break;
					}
				}


				//Si la vista est� a null, los datos se sacan de la tabla
				//padre
				if (AStColFK[AStColFK.GetUpperBound(0)].fVistaNula)
				{
					StComando = "SELECT * from SPREVICMV1 " + "Where gtablacaman = " + "'" + AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre.Trim() + "'" + " AND ISNULL(gvistacaman,'NULL') = 'NULL' " + "order by NORDENCOLTAB ";

					//Una de las condiciones de la select deber�a ser:
					//AND GVISTACAMAN IS NULL, pero esto no funciona con esta vista
					//y hemos tenido que sustituirlo por:
					//ISNULL(gvistacaman,'NULL') = 'NULL'
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombos2 = new DataSet();
					tempAdapter_2.Fill(RsCombos2);


					//Montamos la select de las columnas que van a ir en el combo

					StComando = "";
					AStOrdenFK = new ModuloMantenim.StrOrdenFK[1];
					iLongitudColumna = 0;
					iColumna = 0;
					AStCol = new string[]{String.Empty};
					foreach (DataRow iteration_row_2 in RsCombos2.Tables[0].Rows)
					{
						//S�lo participan en la select para cargar el combo
						//aquellas columnas que sean visibles.
						//Pero en el oreder by pueden participar tanto las visibles
						//como las no visibles
						if (Convert.ToString(iteration_row_2["IVISIBCOMTAB"]) == "S")
						{
							StComando = StComando + "A." + Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim() + ",";
							AStCol = ArraysHelper.RedimPreserve(AStCol, new int[]{iColumna + 1});
							AStCol[iColumna] = Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim();
							//Calculamos la longitud m�xima que va a tener el campo en la spread
							//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
							//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
							//posici�n para el punto decimal

							if (Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "INT")
							{
								if (Convert.ToDouble(iteration_row_2["npartedecima"]) != 0)
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]) + 2);
								}
								else
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]) + 1);
								}
							}
							else
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]));
							}
							iColumna++;
						}
						//Nos creamos un array para guardar el orden de columnas
						//y si �stas son ascendentes o no
						if (Convert.ToDouble(iteration_row_2["NORDENACTAB"]) != 0)
						{
							if (Convert.ToDouble(iteration_row_2["NORDENACTAB"]) > AStOrdenFK.GetUpperBound(0))
							{
								AStOrdenFK = ArraysHelper.RedimPreserve(AStOrdenFK, new int[]{Convert.ToInt32(iteration_row_2["NORDENACTAB"]) + 1});
							}
							AStOrdenFK[Convert.ToInt32(iteration_row_2["NORDENACTAB"])].StAscendente = Convert.ToString(iteration_row_2["IASCENDETAB"]).Trim();
							AStOrdenFK[Convert.ToInt32(iteration_row_2["NORDENACTAB"])].StColumna = Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim();
						}
					}

					RsCombos2.Close();

					//Quitamos la �ltima coma
					if (StModo_optional == Type.Missing)
					{
						StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
						StFrom = "from " + AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre.Trim() + " A";
					}
					else
					{
						StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
					}

					//Montamos la sentencia ORDER BY con el array
					//que nos hemos cargado anteriormente
					StComando2 = "";
					StOrden = new StringBuilder("");
					if (AStOrdenFK.GetUpperBound(0) != 0)
					{
						for (int j = 1; j <= AStOrdenFK.GetUpperBound(0); j++)
						{
							StOrden.Append("A." + AStOrdenFK[j].StColumna);
							if (AStOrdenFK[j].StAscendente.Trim() == "N")
							{
								StOrden.Append(" DESC");
							}
							StOrden.Append(",");
						}
						//Quitamos la �ltima coma
						StOrden = new StringBuilder(StOrden.ToString().Substring(0, Math.Min(StOrden.ToString().Length - 1, StOrden.ToString().Length)) + " ");
						StComando2 = " order by " + StOrden.ToString();
					}

					//Para saber en qu� columna va en el combo
					vIIndiceColEnCombo = 0;

					if (StModo_optional == Type.Missing)
					{
						//Sustituimos las columnas en la spread y llenamos el combo
						SustituirColumnasFK(StComando, StFrom, StComando2, iLongitudColumna, fDimensionarCombo, AStCol);
					}
					else
					{
						//Sustituimos las columnas en la spread y llenamos el combo
						SustituirColumnasFKCombo(StComando, StComando2, iLongitudColumna, AStCol, fDimensionarCombo, StModo);
					}
				}
				else
				{
					//Si la vista no esta a null, los datos se sacan de la vista
					StComando = "SELECT * from SPREVICMV1 " + "Where gvistacaman = " + "'" + AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo.Trim() + "'" + "order by NORDENVISTA ";

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombos2 = new DataSet();
					tempAdapter_3.Fill(RsCombos2);

					//Montamos la select de las columnas que van a ir en el combo

					StComando = "";
					AStOrdenFK = new ModuloMantenim.StrOrdenFK[1];
					iLongitudColumna = 0;
					AStCol = new string[]{String.Empty};
					iColumna = 0;
					foreach (DataRow iteration_row_3 in RsCombos2.Tables[0].Rows)
					{
						//S�lo participan en la select para cargar el combo
						//aquellas columnas que sean visibles.
						//Pero en el oreder by pueden participar tanto las visibles
						//como las no visibles
						if (Convert.ToString(iteration_row_3["IVISIBCOMVIS"]) == "S")
						{
							StComando = StComando + "A." + Convert.ToString(iteration_row_3["GCOLUMNCAMAN"]).Trim() + ",";
							AStCol = ArraysHelper.RedimPreserve(AStCol, new int[]{iColumna + 1});
							AStCol[iColumna] = Convert.ToString(iteration_row_3["GCOLUMNCAMAN"]).Trim();
							//Calculamos la longitud m�xima que va a tener el campo en la spread
							//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
							//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
							//posici�n para el punto decimal

							//Adem�s le a�adimos siempre uno mas de longitud, ya que
							//los campos ir�n separados por espacios
							if (Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "INT")
							{
								if (Convert.ToDouble(iteration_row_3["npartedecima"]) != 0)
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 2 + 1);
								}
								else
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 1 + 1);
								}
							}
							else
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 1);
							}
							iColumna++;
						}
						//Nos creamos un array para guardar el orden de columnas
						//y si �stas son ascendentes o no
						if (Convert.ToDouble(iteration_row_3["NORDENACVIS"]) != 0)
						{
							if (Convert.ToDouble(iteration_row_3["NORDENACVIS"]) > AStOrdenFK.GetUpperBound(0))
							{
								AStOrdenFK = ArraysHelper.RedimPreserve(AStOrdenFK, new int[]{Convert.ToInt32(iteration_row_3["NORDENACVIS"]) + 1});
							}
							AStOrdenFK[Convert.ToInt32(iteration_row_3["NORDENACVIS"])].StAscendente = Convert.ToString(iteration_row_3["IASCENDEVIS"]).Trim();
							AStOrdenFK[Convert.ToInt32(iteration_row_3["NORDENACVIS"])].StColumna = Convert.ToString(iteration_row_3["GCOLUMNCAMAN"]).Trim();
						}
					}

					RsCombos2.Close();

					//Quitamos la �ltima coma
					if (StModo_optional == Type.Missing)
					{
						StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
						StFrom = "from " + AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo.Trim() + " A";
					}
					else
					{
						StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
					}
					//Montamos la sentencia ORDER BY con el array
					//que nos hemos cargado anteriormente
					StOrden = new StringBuilder("");
					StComando2 = "";
					if (AStOrdenFK.GetUpperBound(0) != 0)
					{
						for (int j = 1; j <= AStOrdenFK.GetUpperBound(0); j++)
						{
							StOrden.Append("A." + AStOrdenFK[j].StColumna);
							if (AStOrdenFK[j].StAscendente.Trim() == "N")
							{
								StOrden.Append(" DESC");
							}
							StOrden.Append(",");
						}
						//Quitamos la �ltima coma
						StOrden = new StringBuilder(StOrden.ToString().Substring(0, Math.Min(StOrden.ToString().Length - 1, StOrden.ToString().Length)) + " ");
						StComando2 = " order by " + StOrden.ToString();
					}

					//Para saber en qu� columna va en el combo
					vIIndiceColEnCombo = 0;

					if (StModo_optional == Type.Missing)
					{
						//Sustituimos las columnas en la spread y llenamos el combo
						SustituirColumnasFK(StComando, StFrom, StComando2, iLongitudColumna, fDimensionarCombo, AStCol);
					}
					else
					{
						//Sustituimos las columnas en la spread y llenamos el combo
						SustituirColumnasFKCombo(StComando, StComando2, iLongitudColumna, AStCol, fDimensionarCombo, StModo);
					}
				}

				//Guardo el nombre del nuevo grupo de columnas FK
				if (RsCombos.Tables[0].Rows.Count != 0)
				{
					StNombreFK = Convert.ToString(RsCombos.Tables[0].Rows[0]["GFKCAMAN"]).Trim();
				}

			}

			RsCombos.Close();

		}

		private void EncontrarColumnasFK()
		{
			EncontrarColumnasFK(Type.Missing);
		}
		private void SustituirColumnasFK(string StComando1, string StFrom, string StComando2, int iLongitudColumna, bool fDimensionarCombo, string[] AStCol)
		{

			int i = 0;
			int j = 0;
			DataSet RsFK = null;
			bool fAND = false;
			StringBuilder StComando = new StringBuilder();
			StringBuilder StColumnasOcultas = new StringBuilder();
			int iTama�o = 0;

			StringBuilder StSelect = new StringBuilder();
			StSelect.Append("Select ");

			if (sprMultiple.MaxRows == 0)
			{

				foreach (ModuloMantenim.StrFK AStColFK_item in AStColFK)
				{

					//busco el n� de columna en el spread a la que corresponde
					//cada campo que es FK y la oculto.
					//Tambi�n oculto el control asociado a esa columna
					for (i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].gcolumncaman.Trim() == AStColFK_item.StColHija.Trim())
						{

							//La booleana fDimensionarCombo, me dice si tengo que redimensionar o no
							//el array, ya que la 1� vez est� dimensionado
							if (fDimensionarCombo)
							{
								AStColFKCombo = ArraysHelper.RedimPreserve(AStColFKCombo, new int[]{AStColFKCombo.GetUpperBound(0) + 2});
							}

							fDimensionarCombo = true;

							//Esto me sirve luego para poder identificar cada columna
							//que es clave a que combo pertenece y que n� de columna
							//ocupa en ese combo
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StColHija = AStColFK_item.StColHija.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].iIndiceCombo = (short) viIndiceCombo;
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].IIndiceColEnCombo = (short) vIIndiceColEnCombo;
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StLabelCombo = AStColFK_item.StLabelCombo.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StTablaPadre = AStColFK_item.StTablaPadre.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StColPadre = AStColFK_item.StColPadre.Trim();

							//*******************************************************************************
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].fVistaNula = AStColFK_item.fVistaNula;
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StNombreFK = AStColFK_item.StNombreFK.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StVistaCombo = AStColFK_item.StVistaCombo.Trim();
							//*******************************************************************************

							vIIndiceColEnCombo++;

							sprMultiple.Col = astrElConjunto[i].nordencoltab;
							sprMultiple.SetColHidden(sprMultiple.Col, true);
							astrElConjunto[i].NombreControl.Visible = false;

							//Salgo del bucle FOR porque ya ha encontrado la columna
							break;
						}
					}
				}


				//Inserto columnas con los datos que tienen que mostrarse
				//en vez de las FK, pero s�lo una columna por campo, no por fila

				//insertamos una columna nueva.
				//sino no insertamos columna, ya que ya la habremos insertado
				//cuando pas� por la primera fila

				sprMultiple.MaxCols++;
				sprMultiple.Col++;
				//sprMultiple.Action = 6; //INSERT COL
				sprMultiple.setTypeEditLen(iLongitudColumna);
				sprMultiple.Row = 0;
				sprMultiple.Text = AStColFK[0].StLabelCombo.Trim();
				sprMultiple.SetColWidth(sprMultiple.Col, AStColFK[0].StLabelCombo.Length);

			}

			int k = 1;

			while (k <= sprMultiple.MaxRows)
			{
				//Monto la sentencia WHERE para cada fila de la spread y
				//Oculto las columnas de la spread que son FK
				sprMultiple.Row = k;
				fAND = false;
				StComando = new StringBuilder(" where ");
				foreach (ModuloMantenim.StrFK AStColFK_item_2 in AStColFK)
				{

					if (fAND)
					{
						StComando.Append(" and ");
					}
					fAND = true;

					//Ademas de a�adir cada columna al WHERE de la sentencia,
					//busco el n� de columna en el spread a la que corresponde
					//cada campo que es FK y la oculto.
					//Tambi�n oculto el control asociado a esa columna
					for (i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].gcolumncaman.Trim() == AStColFK_item_2.StColHija.Trim())
						{

							//La booleana fDimensionarCombo, me dice si tengo que redimensionar o no
							//el array, ya que la 1� vez est� dimensionado
							if (fDimensionarCombo)
							{
								AStColFKCombo = ArraysHelper.RedimPreserve(AStColFKCombo, new int[]{AStColFKCombo.GetUpperBound(0) + 2});
							}

							fDimensionarCombo = true;

							//Esto me sirve luego para poder identificar cada columna
							//que es clave a que combo pertenece y que n� de columna
							//ocupa en ese combo
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StColHija = AStColFK_item_2.StColHija.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].iIndiceCombo = (short) viIndiceCombo;
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].IIndiceColEnCombo = (short) vIIndiceColEnCombo;
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StLabelCombo = AStColFK_item_2.StLabelCombo.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StTablaPadre = AStColFK_item_2.StTablaPadre.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StColPadre = AStColFK_item_2.StColPadre.Trim();

							//*******************************************************************************
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].fVistaNula = AStColFK_item_2.fVistaNula;
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StNombreFK = AStColFK_item_2.StNombreFK.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StVistaCombo = AStColFK_item_2.StVistaCombo.Trim();
							//*******************************************************************************

							vIIndiceColEnCombo++;

							sprMultiple.Col = astrElConjunto[i].nordencoltab;
							sprMultiple.SetColHidden(sprMultiple.Col, true);
							astrElConjunto[i].NombreControl.Visible = false;

							StComando.Append("A." + AStColFK_item_2.StColPadre + "=");

							if (sprMultiple.Text.Trim() == "")
							{
								//Quitamos el �ltimo igual
								StComando = new StringBuilder(StComando.ToString().Substring(0, Math.Min(StComando.ToString().Length - 1, StComando.ToString().Length)) + " ");
								StComando.Append(" IS NULL");
							}
							else
							{
								//SI ES "CHAR", "VARCHAR" O "DATETIME",
								//EL DATO VA ENTRE COMILLA SIMPLE
								if (AStColFK_item_2.stTipoDato.Trim() == "VARCHAR" || AStColFK_item_2.stTipoDato.Trim() == "CHAR")
								{
									StComando.Append("'" + sprMultiple.Text.Trim() + "'");
								}
								else if (AStColFK_item_2.stTipoDato.Trim() == "DATETIME")
								{ 
									StComando.Append("'" + DateTime.Parse(sprMultiple.Text).ToString("MM/dd/yyyy") + "'");
								}
								else
								{
									StComando.Append(sprMultiple.Text.Trim());
								}
							}

							//Salgo del bucle FOR porque ya ha encontrado la columna
							break;
						}
					}
				}


				//Monto la consulta con la sentencia SELECT, el WHERE y el ORDER BY
				StComando = new StringBuilder(StSelect.ToString() + StComando1 + StFrom + StComando.ToString() + " " + StComando2);

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
				RsFK = new DataSet();
				tempAdapter.Fill(RsFK);

				//Inserto columnas con los datos que tienen que mostrarse
				//en vez de las FK, pero s�lo una columna por campo, no por fila

				//si la fila es la primera, insertamos una columna nueva.
				//sino no insertamos columna, ya que ya la habremos insertado
				//cuando pas� por la primera fila
				if (k == 1)
				{
					sprMultiple.MaxCols++;
					sprMultiple.Col++;
					//sprMultiple.Action = 6; //INSERT COL
					sprMultiple.setTypeEditLen(iLongitudColumna);
					sprMultiple.Row = 0;
					sprMultiple.Text = AStColFK[0].StLabelCombo.Trim();
					sprMultiple.SetColWidth(sprMultiple.Col, AStColFK[0].StLabelCombo.Length);
				}
				else
				{
					sprMultiple.Col++;
					sprMultiple.setTypeEditLen(iLongitudColumna);
				}

				sprMultiple.Row = k;
				sprMultiple.Text = "";
				if (RsFK.Tables[0].Rows.Count != 0)
				{
					for (i = 0; i <= RsFK.Tables[0].Columns.Count - 1; i++)
					{
						sprMultiple.Text = sprMultiple.Text + Convert.ToString(RsFK.Tables[0].Rows[0][i]).Trim() + " ";
					}
				}

				RsFK.Close();

				k++;
			}

			int iIndiceControl = sprMultiple.Col - 1;

			//***********************CARGO EL COMBO********************************
			DejarHuecoParaCombo(iIndiceControl);
			CrearCampoCombo(iIndiceControl, iLongitudColumna);
			CrearEtiquetaCombo(iIndiceControl);
			PintarCampoCombo();

			//El n� de columnas de cada combo ser� igual a una columna por cada
			//columna que forme parte de la FK mas una, que ser� la que lleve
			//la descripci�n.
			//Como el Ubound nos da el m�ximo �ndice de un array y �ste n� es uno menos
			//que los elementos que tiene el array, le sumamos dos al Ubound y
			//nos dar� el n� de columnas que debe tener el combo
			if (AStColFK.GetUpperBound(0) <= 8)
			{
                // ralopezn TODO_X_5 20/05/2016
				//astrElConjunto[iIndiceControl].NombreControl.ColumnCount = AStColFK.GetUpperBound(0) + 2;
			}
			else
			{
				RadMessageBox.Show("El combo no puede soportar una FK formada por mas de 9 columnas.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
				this.Close();
				return;
			}

			//A�ado al Select los campos que son FK, para introducirlos en combo
			//y as� poder referenciarlos

			for (i = 0; i <= AStColFK.GetUpperBound(0); i++)
			{
				StSelect.Append("A." + AStColFK[i].StColPadre + ",");
			}

			//Hago un COUNT para saber si el resulset que carga el combo
			//es > que la constante MAXFILAS, y si lo es, no cargo el combo
			//y lo inhabilito
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter("Select count(*) " + StFrom, ModuloMantenim.RcManten);
			RsFK = new DataSet();
			tempAdapter_2.Fill(RsFK);

			if (Convert.ToDouble(RsFK.Tables[0].Rows[0][0]) > ModuloMantenim.vMaximoFilas)
			{
				RsFK.Close();
				astrElConjunto[iIndiceControl].NombreControl.Enabled = false;
				//N� de �ndice de ControlCombo
				foreach (ModuloMantenim.StrFK AStColFK_item_4 in AStColFK)
				{
					StColumnasOcultas.Append("0;");
				}

				//Quitamos el �ltimo punto y coma
				StColumnasOcultas = new StringBuilder(StColumnasOcultas.ToString().Substring(0, Math.Min(StColumnasOcultas.ToString().Length - 1, StColumnasOcultas.ToString().Length)));
                //Oculta las columnas que son clave en el combo
                // ralopezn TODO_X_5 20/05/2016
                //astrElConjunto[iIndiceControl].NombreControl.ColumnWidths = StColumnasOcultas.ToString();
                viIndiceCombo++;
				return;
			}
			else
			{
				astrElConjunto[iIndiceControl].NombreControl.Enabled = true;
			}
			RsFK.Close();

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSelect.ToString() + StComando1 + StFrom + StComando2, ModuloMantenim.RcManten);
			RsFK = new DataSet();
			tempAdapter_3.Fill(RsFK);


			//Lleno el combo
			//La primeroa fila del combo va a tener todos sus valores igual al contenido
			//de la constante CoItemComboNulo, y servir� para poder grabar valores nulos
			j = 0;
			if (RsFK.Tables[0].Rows.Count != 0)
			{
                // ralopezn TODO_X_5 20/05/2016
                //astrElConjunto[iIndiceControl].NombreControl.AddItem("");
                for (i = 0; i <= AStColFK.GetUpperBound(0); i++)
				{
                    //Primero creamos en el combo las columnas que forman parte de la
                    //FK, y las ocultamos
                    // ralopezn TODO_X_5 20/05/2016	
                    //astrElConjunto[iIndiceControl].NombreControl.Column[i, j] = CoItemComboNulo.ToUpper();
                }
                // ralopezn TODO_X_5 20/05/2016
                //astrElConjunto[iIndiceControl].NombreControl.Column[i, j] = CoItemComboNulo.ToUpper();
			}
			j = 1;
			foreach (DataRow iteration_row in RsFK.Tables[0].Rows)
			{
                // ralopezn TODO_X_5 20/05/2016
                //astrElConjunto[iIndiceControl].NombreControl.AddItem("");

                for (i = 0; i <= AStColFK.GetUpperBound(0); i++)
				{
					//Primero creamos en el combo las columnas que forman parte de la
					//FK, y las ocultamos
					if (!Convert.IsDBNull(iteration_row[i]))
					{
						//comentada y sustituida la linea siguiente por la siguiente
						//            iTama�o = Tama�oMaximo(AStColFK(i).StColPadre, AStColFK(i).fVistaNula, AStColFK(i).StTablaPadre, AStColFK(i).StVistaCombo)
						iTama�o = Strings.Len(iteration_row[i]);
						if (iteration_row[i].GetType() == typeof(DateTime))
						{
                            //Si el tipo de dato es fecha, su longitud la sacaremos
                            //con el tama�o que ocupe la fecha formateada a "dd/mm/yyyy"
                            // ralopezn TODO_X_5 20/05/2016	
                            //astrElConjunto[iIndiceControl].NombreControl.Column[i, j] = Padrr(Convert.ToDateTime(iteration_row[i]).ToString("dd/MM/yyyy"), Convert.ToDateTime(iteration_row[i]).ToString("dd/MM/yyyy").Length, iTama�o, iteration_row[i].getType()).ToUpper() + " ";
                        }
                        else
						{
                            // ralopezn TODO_X_5 20/05/2016	
                            //astrElConjunto[iIndiceControl].NombreControl.Column[i, j] = Convert.ToString(iteration_row[i]).Trim().ToUpper() + " ";
                        }
                    }
					else
					{
                        // ralopezn TODO_X_5 20/05/2016	
                        //astrElConjunto[iIndiceControl].NombreControl.Column[i, j] = " ";
                    }
                }

				//Concatenamos los campos que se van a mostrar en el combo
				//en una sola columna visible
				ConcatenarUltimosCampos(RsFK, i, j, iIndiceControl, iLongitudColumna, AStColFK[i - 1].StTablaPadre, AStCol, AStColFK[i - 1].fVistaNula, AStColFK[i - 1].StVistaCombo);
				j++;
			}
			//Para ocultar columnas, hay que poner la propiedad ColumnWidths del combo
			//a ceros para cada una de las columnas que queramos ocultar,
			//separadas por comas.
			//As�, si queremos ocultar las dos primeras columnas, se pondr�:
			//COMBO.ColumnWidths="0;0"
			//Como nosotros queremos ocultar aquellas columnas que forman parte de
			//la FK, vamos concatenando en un string tantos ceros, separados por
			//puntos y coma, como columnas queramos ocultar
			for (i = 0; i <= AStColFK.GetUpperBound(0); i++)
			{
				StColumnasOcultas.Append("0;");
			}

			//Quitamos el �ltimo punto y coma
			StColumnasOcultas = new StringBuilder(StColumnasOcultas.ToString().Substring(0, Math.Min(StColumnasOcultas.ToString().Length - 1, StColumnasOcultas.ToString().Length)));
            //Oculta las columnas que son clave en el combo
            // ralopezn TODO_X_5 20/05/2016
            //astrElConjunto[iIndiceControl].NombreControl.ColumnWidths = StColumnasOcultas.ToString();

            RsFK.Close();

			//N� de �ndice de ControlCombo
			viIndiceCombo++;

		}
		//private int Tama�oMaximo(string StColPadre, bool fVistaNula, string StTablaPadre = "", string StVista = "")
		//{
				//Funci�n que nos dice el m�ximo tama�o que puede ocupar un campo
				//
				//int result = 0;
				//string stConsulta = "select (nparteentera+npartedecima) " + "From SPREVICMV1 " + "where ";
				//
				//if (fVistaNula)
				//{
					//stConsulta = stConsulta + "gtablacaman=" + "'" + StTablaPadre.Trim() + "'" + " and gvistacaman is null" + " and gcolumncaman=" + "'" + StColPadre.Trim() + "'";
					//
				//}
				//else
				//{
					//stConsulta = stConsulta + "gvistacaman=" + "'" + StVista.Trim() + "'" + " and gcolumncaman=" + "'" + StColPadre.Trim() + "'";
				//}
				//
				//SqlDataAdapter tempAdapter = new SqlDataAdapter(stConsulta, ModuloMantenim.RcManten);
				//DataSet RsTama�o = new DataSet();
				//tempAdapter.Fill(RsTama�o);
				//
				//result = Convert.ToInt32(RsTama�o.Tables[0].Rows[0][0]);
				//
				//RsTama�o.Close();
				//
				//return result;
		//}

		private void OrdenarControles()
		{

			int INumClaves = 0;

			viNuevoTop = 0;
			int iValorTop = ModuloMantenim.COiSeparacion;
			int iTab = -1;
			for (int i = 0; i <= LbControlEtiqueta.GetUpperBound(0); i++)
			{

				if (!astrElConjunto[i].NombreControl.Visible)
				{
					LbControlEtiqueta[i].Visible = false;
				}
				else
				{
					iTab++;
					if (astrElConjunto[i].NombreControl.Name == "cbbCombo")
					{
                        // ralopezn TODO_X_5 20/05/2016
                        //INumClaves = proNumPuntosYComa(1, Strings.Len(cbbCombo[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].ColumnWidths), cbbCombo[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].ColumnWidths);
                        AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].ClaveAsociada.Visible = true;
						AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].ClaveAsociada.Top = (int) ((iValorTop + viNuevoTop) / 15);
						AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].ClaveAsociada.Left = (int) (viNuevoLeft / 15);
						AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].ClaveAsociada.TabIndex = iTab;
						if (INumClaves > 0)
						{
							//                AStrToolbar(astrElConjunto(i).NombreControl.Index).ClaveAsociada.Visible = False
						}
						astrElConjunto[i].NombreControl.Top = (int) ((iValorTop + viNuevoTop) / 15);
						astrElConjunto[i].NombreControl.Left = (int) (AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].ClaveAsociada.Left + AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].ClaveAsociada.Width);
						AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].NombreControl.Visible = true;
						AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].NombreControl.Top = (int) (astrElConjunto[i].NombreControl.Top - 2);
						AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl)].NombreControl.Left = (int) (astrElConjunto[i].NombreControl.Left + astrElConjunto[i].NombreControl.Width + 2);
					}
					else
					{
						astrElConjunto[i].NombreControl.Top = (int) ((iValorTop + viNuevoTop) / 15);
						astrElConjunto[i].NombreControl.Left = (int) (viNuevoLeft / 15);
						astrElConjunto[i].NombreControl.TabIndex = iTab;

					}
					LbControlEtiqueta[i].Visible = true;
					LbControlEtiqueta[i].Top = (int) ((iValorTop + viNuevoTop) / 15);
					LbControlEtiqueta[i].Left = (int) (ModuloMantenim.COiIzda / 15);
					viNuevoTop = Convert.ToInt32(((float) (astrElConjunto[i].NombreControl.Top * 15)) + ((float) (astrElConjunto[i].NombreControl.Height * 15)));

					AlturaFrame(astrElConjunto[i].NombreControl);
				}
			}
		}

		private void ConcatenarUltimosCampos(DataSet RsFK, int i, int j, int iIndiceControl, int iLongitudColumna, string StTabla, string[] AStCol, bool fVistaNula, string StVista = "", bool fComboDeshabilitado = false)
		{
			//Funci�n que lo que hace es concatenar los campos que
			//se van a visualizar en el combo. Para ello debe hacer que estos
			//campos aparezcan diferenciados como en columnas. Esto lo haremos
			//concatenando blancos a cada campo hasta alcanzar la longitud
			//m�xima de cada uno de ellos
			StringBuilder Concatena = new StringBuilder();
			int iTama�o = 0;


			int iIndice = 0;
			for (int indice2 = i; indice2 <= RsFK.Tables[0].Columns.Count - 1; indice2++)
			{
				//Funci�n para hallar el tama�o m�ximo que puede ocupar
				//un  campo, para despues poder rellenar este tama�o
				//con blancos
                
				//DIEGO 09-08-2006 - SE CAMBIA PARA ARREGLAR EL ERROR QUE SE PRODUCE CUANDO ES NULL - TABLA SCOCAMAN
				//iTama�o = LenB(RsFK(indice2))
				iTama�o = (Convert.IsDBNull(RsFK.Tables[0].Rows[0][indice2])) ? 0 : Strings.Len(RsFK.Tables[0].Rows[0][indice2]);
				if (RsFK.Tables[0].Rows[0][indice2].GetType() == typeof(DateTime))
				{
					//Si el tipo de dato es fecha, su longitud la sacamos
					//del tama�o que tenga la fecha formateada a "dd/mm/yyyy"
					if (Convert.IsDBNull(RsFK.Tables[0].Rows[0][indice2]))
					{
						Concatena.Append(Padrr("", 0, iTama�o, RsFK.Tables[0].Rows[0][indice2].GetType()).ToUpper() + " ");
					}
					else
					{
						Concatena.Append(Padrr(Convert.ToDateTime(RsFK.Tables[0].Rows[0][indice2]).ToString("dd/MM/yyyy"), Convert.ToDateTime(RsFK.Tables[0].Rows[0][indice2]).ToString("dd/MM/yyyy").Length, iTama�o, RsFK.Tables[0].Rows[0][indice2].GetType()).ToUpper() + " ");
					}
				}
				else
				{
					if (Convert.IsDBNull(RsFK.Tables[0].Rows[0][indice2]))
					{
						Concatena.Append(Padrr("", 0, iTama�o, RsFK.Tables[0].Rows[0][indice2].GetType()).ToUpper() + " ");
					}
					else
					{
						Concatena.Append(Convert.ToString(RsFK.Tables[0].Rows[0][indice2]).Trim().ToUpper() + " ");
					}
				}
				iIndice++;
			}
			//El m�ximo tama�o que va a tener el combo:
			//IMaxAnchoCombo = 4305
			int IMaxAnchoCombo = 4225;

			//Puede que venga de combo deshabilitado, con lo que
			//no habr� que cargar todo el combo, sino s�lo el text del combo
			if (false)
			{
                // ralopezn TODO_X_5 20/05/2016
				//astrElConjunto[iIndiceControl].NombreControl.Column[i, j] = Concatena.ToString();
				astrElConjunto[iIndiceControl].NombreControl.Width = (int) (IMaxAnchoCombo / 15);
			}
			else
			{
				//si el combo est� deshabilitado, en vez de mandar el indice del
				//control del array de controles, mando el �ndice del combo
				cbbCombo[iIndiceControl].Text = Concatena.ToString();
				cbbCombo[iIndiceControl].Width = (int) (IMaxAnchoCombo / 15);
			}
		}

		public string Padrr(string stCadena, int iLongitud, int iLongitudTotal, Type iTipo)
		{
			//Funci�n que concatena blancos a un campo hasta
			//rellenar una longitud determinada.
			//El n� de espacios que hay que concatenar ser�
			//la longitud m�xima - la longitud del campo
			int iEspacios = 0;

			if (iTipo == typeof(int))// || iTipo == UpgradeStubs.RDO_DataTypeConstants.getrdTypeINTEGER())
			{
				iEspacios = (iLongitudTotal + 1) - iLongitud;
				//Si el campo es numeric,
				//habr� que sumar dos posiciones mas
				//para el signo y el punto
			}
			else if (iTipo == typeof(int))
			{ 
				iEspacios = (iLongitudTotal + 2) - iLongitud;
			}
			else
			{
				iEspacios = iLongitudTotal - iLongitud;
				if (!Convert.IsDBNull(stCadena))
				{
					return stCadena.Trim() + new string(' ', iEspacios);
				}
				else
				{
					return new string(' ', iEspacios);
				}
			}

			//Si es num�rico, se concatenan los espacios por la izquierda
			if (!Convert.IsDBNull(stCadena))
			{
				return new string(' ', iEspacios) + stCadena.Trim();
			}
			else
			{
				return new string(' ', iEspacios);
			}
		}
		private void NombrarEtiqueta()
		{
			//Se nombran las Etiquetas y se les da tama�o
			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				lbEtiqueta[0].Text = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim() + " :";
			}
			else
			{
				lbEtiqueta[iIndice].Text = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim() + " :";
			}
		}

		private void LlenarSpread(string StTabla, string stQuery = "", bool fTodo = false, bool fMaxFilas = false)
		{


			ModuloMantenim.DesactivarCampos(this, astrElConjunto, AStrToolbar);
			if (!fTodo)
			{
				sprMultiple.MaxRows = 0;
				cbSpread[3].Enabled = true;
				return;
			}
			//Reloj arena
			this.Cursor = Cursors.WaitCursor;
			sprMultiple.MaxRows = 0;
			//**************************************
			Requery(stQuery);
			//**************************************

			RecorrerRegistros();

			this.Cursor = Cursors.Default;

		}
		private void Requery(string stQuery)
		{
			//Hacemos el requery de nuestra consulta


			//Si es una nueva consulta, cierro la consulta anterior
			if (NuevaConsulta)
			{
				if (vfTodo)
				{
					//        RsManten.Close
					NuevaConsulta = false;
				}
				else
				{
					NuevaConsulta = false;
				}
			}

			Rq = ModuloMantenim.RcManten.CreateQuery("", stQuery);

			Rq.CommandText = stQuery;
            //Rq.setMaxRows(ModuloMantenim.vMaximoFilas);
            Rq.Parameters.Add("@paramStr");
			Rq.Parameters[0].Value = "";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(Rq);
			ModuloMantenim.RsManten = new DataSet();
            // setMaxRows
            tempAdapter.Fill(ModuloMantenim.RsManten, 0, ModuloMantenim.vMaximoFilas, "Table1");

			if (ModuloMantenim.RsManten.Tables[0].Rows.Count == ModuloMantenim.vMaximoFilas)
			{
				CargarMas = true;
				Tope = ModuloMantenim.RsManten.Tables[0].Rows.Count - iNumeroFilasVisibles; //11
			}
			else if (ModuloMantenim.RsManten.Tables[0].Rows.Count < ModuloMantenim.vMaximoFilas)
			{ 
				CargarMas = false;
			}

			vfTodo = true;

			//Si no hay filas en la spread, deshabilitamos el bot�n de imprimir
			cbSpread[4].Enabled = sprMultiple.MaxRows != 0;
		}

		//private void PONER_NEGRITA(string StTabla)
		//{
				//
				//ModuloMantenim.stSQLOrder = ModuloMantenim.ObtenerOrdenColumnas();
				//
				//ModuloMantenim.stConsulta = "select * from SPRETBSPV1 ";
				//ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + "where GTABLAMANAUT='";
				//ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + StTabla + "'";
				//ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + ModuloMantenim.stSQLOrder;
				//SqlDataAdapter tempAdapter = new SqlDataAdapter(ModuloMantenim.stConsulta, ModuloMantenim.RcManten);
				//ModuloMantenim.RsManten2 = new DataSet();
				//tempAdapter.Fill(ModuloMantenim.RsManten2);
				//
				//if (ModuloMantenim.RsManten2.Tables[0].Rows.Count <= 0)
				//{
					//ModuloMantenim.RsManten2.Close();
					//this.Cursor = Cursors.Default;
					//return;
				//}
				//
				//
				//ModuloMantenim.RsManten2.MoveFirst();
				//
				//int INDICETEMP = 1;
				//foreach (DataRow iteration_row in ModuloMantenim.RsManten2.Tables[0].Rows)
				//{
					//if (Convert.ToString(iteration_row["iadmitenull"]) == "N")
					//{
						//lbEtiqueta[INDICETEMP].Font = lbEtiqueta[INDICETEMP].Font.Change(bold:true);
					//}
					//else
					//{
						//lbEtiqueta[INDICETEMP].Font = lbEtiqueta[INDICETEMP].Font.Change(bold:false);
					//}
					//INDICETEMP++;
				//}
				//
				//ModuloMantenim.RsManten2.Close();
				//
				//this.Cursor = Cursors.Default;
				//
		//}

		private void CrearSpread(string StTabla, string vstDescrLarTabla)
		{
			//Crea las columnas de la Spread
			Text = Text + " " + vstDescrLarTabla;
			//Vamos a crear el tama�o de las etiquetas
			Tama�oEtiqueta(StTabla);
			this.Cursor = Cursors.WaitCursor;

			ModuloMantenim.stSQLOrder = ModuloMantenim.ObtenerOrdenColumnas();

			ModuloMantenim.stConsulta = "select * from SPRETBSPV1 ";
			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + "where GTABLAMANAUT='";
			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + StTabla + "'";
			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + ModuloMantenim.stSQLOrder;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(ModuloMantenim.stConsulta, ModuloMantenim.RcManten);
			ModuloMantenim.RsManten2 = new DataSet();
			tempAdapter.Fill(ModuloMantenim.RsManten2);

			iIndice = ModuloMantenim.RsManten2.Tables[0].Rows.Count;
			//Ampliamos las columnas de la Spread al total de las columnas
			sprMultiple.MaxCols = iIndice;
			sprMultiple.MaxRows = 1;
			//Los siguientes arrays siempre estar�n dimensionados a un elemento mas
			//que los campos de la tabla a mantener (MAXCOL), aunque el �ltimo elemento
			//no se va a tratar nunca ya que los arrays siempre se recorren de 0 a UBOUND - 1
			LaConexion = new ModuloMantenim.ConexionControles[iIndice + 1];
			astrElConjunto = new ModuloMantenim.strConjuntoResultados[iIndice + 1];


			if (ModuloMantenim.RsManten2.Tables[0].Rows.Count <= 0)
			{
				ModuloMantenim.RsManten2.Close();
				this.Cursor = Cursors.Default;
				return;
			}

			CargarEstrucResultados(ModuloMantenim.RsManten2);

			ModuloMantenim.RsManten2.MoveFirst();

			sprMultiple.Col = 1;
			sprMultiple.Row = 0;
			iIndice = 0;


			while(ModuloMantenim.RsManten2.Tables[0].Rows.Count != 0)
			{
				sprMultiple.SetColWidth(iIndice + 1, Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim().Length);
				if (sprMultiple.GetColWidth(iIndice + 1) <= 4)
				{
					sprMultiple.SetColWidth(iIndice + 1, 6);
					sprMultiple.Text = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim();
					LaConexion[iIndice].NumeroColumna = (short) sprMultiple.Col;
					aNumeroColumna = ArraysHelper.RedimPreserve(aNumeroColumna, new int[]{iIndice + 1});
					aLongitud = ArraysHelper.RedimPreserve(aLongitud, new int[]{iIndice + 1});
					aNumeroColumna[iIndice] = sprMultiple.Col;
					//********************************CRIS********************************
					//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
					//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
					//posici�n para el punto decimal
					if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
					{
						if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
						{
							aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 2);
						}
						else
						{
							aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 1);
						}
					}
					else
					{
						aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
					}
					viColumnaPassword = ComprobarCamposPassword();
					if (sprMultiple.Col == viColumnaPassword)
					{
						sprMultiple.setTypeEditPassword(true);
					}

					//********************************************************************
					CrearEtiqueta();
					NombrarEtiqueta(); //Se nombran y se crean los tama�os
					PintarEtiqueta();
					//Nombre de la tabla que estamos pintando
					LaConexion[iIndice].NombreTabla = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtablamanaut"]).Trim();
					//Nombre del campo de la tabla
					LaConexion[iIndice].NombreCampo = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim();
					CrearControles();
					PintarControles();
					ModuloMantenim.RsManten2.MoveNext();
					iIndice++;
					sprMultiple.Col++;
				}
				else
				{
					sprMultiple.Text = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["dcoltabmanco"]).Trim();
					LaConexion[iIndice].NumeroColumna = (short) sprMultiple.Col;
					CrearEtiqueta();
					NombrarEtiqueta(); //Se nombran y se crean los tama�os
					PintarEtiqueta();
					//Nombre de la tabla que estamos pintando
					LaConexion[iIndice].NombreTabla = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtablamanaut"]).Trim();
					//Nombre del campo de la tabla
					LaConexion[iIndice].NombreCampo = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim();
					aNumeroColumna = ArraysHelper.RedimPreserve(aNumeroColumna, new int[]{iIndice + 1});
					aLongitud = ArraysHelper.RedimPreserve(aLongitud, new int[]{iIndice + 1});
					aNumeroColumna[iIndice] = sprMultiple.Col;
					//********************************CRIS********************************
					//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
					//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
					//posici�n para el punto decimal
					if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
					{
						if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
						{
							aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 2);
						}
						else
						{
							aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 1);
						}
					}
					else
					{
						aLongitud[iIndice] = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
					}
					//********************************************************************
					viColumnaPassword = ComprobarCamposPassword();
					if (sprMultiple.Col == viColumnaPassword)
					{
						sprMultiple.setTypeEditPassword(true);
					}
					CrearControles();
					PintarControles();
					ModuloMantenim.RsManten2.MoveNext();
					iIndice++;
					sprMultiple.Col++;
				}
			};

			ModuloMantenim.RsManten2.Close();

			this.Cursor = Cursors.Default;

		}

		private void CrearControles()
		{
			//Rutina para comprobar el tipo de dato y as�
			//crear el tipo de control que necesitamos



			switch(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim())
			{
				case "DATETIME" :  //Campo fecha 
					 
					CrearCampoFecha(); 
					 
					break;
				case "VARCHAR" : case "CHAR" :  //Campo texto con o sin m�scara 
					 
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) > 2 * ModuloMantenim.COiMaxTamLineaMultiline)
					{
						CrearCampoMultilinea(3);
					}
					else if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) > ModuloMantenim.COiMaxTamText)
					{ 
						CrearCampoMultilinea(2);
					}
					else
					{
						if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "N" && Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["icolformato"]) == "N" && Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["icolmascara"]) == "N")
						{
							CrearCampoMascara();
						}
						else
						{
							//S�lo se creer� campo texto no multilinea
							//cuando sea con password, formato o m�scara
							CrearCampoTexto();
						}
					} 
					 
					break;
				case "NUMERIC" : case "SMALLINT" : case "INT" :  //Campo num�rico 
					 
					CrearCampoMascara(); 
					 
					break;
			}

		}

		private void PintarControles()
		{
			//Pintar los controles en el Frame de detalle

			switch(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim())
			{
				case "DATETIME" :  //Campo fecha 
					PintarCampoFecha(); 
					 
					break;
				case "VARCHAR" : case "CHAR" :  //Campo texto con o sin m�scara 
					//Si el campo es muy largo se crea un multilinea directamente 
					//No se comprueba si tiene m�scara porque no es l�gico. 
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) > 2 * ModuloMantenim.COiMaxTamLineaMultiline)
					{
						PintarCampoMultilinea(3);
					}
					else if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) > ModuloMantenim.COiMaxTamText)
					{ 
						PintarCampoMultilinea(2);
					}
					else
					{
						//Si la columna "ipassword" nos dice que el campo no llevar� protecci�n con
						//caracteres para password
						if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "N" && Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["icolformato"]) == "N" && Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["icolmascara"]) == "N")
						{
							PintarCampoMascara();

						}
						else
						{
							//Si el Control debe llevar la propiedad password con un car�cter de protecci�n
							PintarCampoTexto();
						}

					} 
					 
					break;
				case "NUMERIC" : case "SMALLINT" : case "INT" :  //Campo num�rico 
					 
					PintarCampoMascara(); 
					 
					break;
			}

		}

		private void Tama�oEtiqueta(string StTabla)
		{

			//Obtengo el m�ximo tama�o de las etiquetas de los campos, y el m�ximo
			//tama�o de las etiquetas de los combos, y el que sea mayor es el que
			//utilizo

			string consulta = "select MAX(DATALENGTH(RTRIM(dcoltabmanco))) from SPRETBSPV1 " + "where gtablamanaut=" + "'" + StTabla.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(consulta, ModuloMantenim.RcManten);
			ModuloMantenim.RsManten2 = new DataSet();
			tempAdapter.Fill(ModuloMantenim.RsManten2);

			int iTama�oEtiqueta = Convert.ToInt32(Double.Parse(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0][0]).Trim()) * ModuloMantenim.COiTamCaractEtiqueta);

			ModuloMantenim.RsManten2.Close();

			consulta = "select MAX(DATALENGTH(RTRIM(vlabelcombo))) FROM SCOLFKPKV1 " + "where gtabcamanhij=" + "'" + StTabla.Trim() + "'";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(consulta, ModuloMantenim.RcManten);
			ModuloMantenim.RsManten2 = new DataSet();
			tempAdapter_2.Fill(ModuloMantenim.RsManten2);

			if (iTama�oEtiqueta < Double.Parse(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0][0]).Trim()) * ModuloMantenim.COiTamCaractEtiqueta)
			{
				iTama�oEtiqueta = Convert.ToInt32(Double.Parse(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0][0]).Trim()) * ModuloMantenim.COiTamCaractEtiqueta);
			}

			ModuloMantenim.RsManten2.Close();

			lbEtiqueta[0].Width = (int) ((iTama�oEtiqueta - 100) / 15);

		}



		private void ValidarFormato(int IAscii, string stCampo, string stTipoDato)
		{
			//Rutina para dejar s�lo introducir los caracteres de formato
			string stLetra = String.Empty;
			string StFormato = String.Empty;

			FFormatoValido = false;
			switch(stTipoDato)
			{
				case "CHAR" : case "VARCHAR" : 
					StFormato = StFormatoCadena + StFormatoFechaHora; 
					for (int i = 1; i <= StFormato.Length; i++)
					{
						stLetra = StFormato.Substring(i - 1, Math.Min(1, StFormato.Length - (i - 1)));
						if (IAscii == Strings.Asc(stLetra[0]) || IAscii == Strings.Asc(stLetra.ToUpper()[0]))
						{
							FFormatoValido = true;
							break;
						}
					} 
					 
					break;
				case "NUMERIC" : case "SMALLINT" : case "INT" : 
					StFormato = StFormatoNumero + StFormatoFechaHora; 
					for (int i = 1; i <= StFormato.Length; i++)
					{
						stLetra = StFormato.Substring(i - 1, Math.Min(1, StFormato.Length - (i - 1)));
						if (IAscii == Strings.Asc(stLetra[0]) || IAscii == Strings.Asc(stLetra.ToUpper()[0]))
						{
							FFormatoValido = true;
							break;
						}
					} 
					 
					break;
			}


			if (!FFormatoValido)
			{
				//Muestra al usuario los formatos v�lidos
				short tempRefParam = 1750;
				string[] tempRefParam2 = new string[]{"el valor de " + stCampo.Trim(), StFormato};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
			}
		}

		private void ValidarMascara(int IAscii, string stCampo, string stTipoDato)
		{
			//Rutina para dejar s�lo introducir los caracteres de m�scara
			string stLetra = String.Empty;
			string StMascara = String.Empty;

			FMascaraValida = false;
			switch(stTipoDato)
			{
				case "CHAR" : case "VARCHAR" : 
					StMascara = StMascaraNumero + StMascaraFechaHora; 
					for (int i = 1; i <= StMascara.Length; i++)
					{
						stLetra = StMascara.Substring(i - 1, Math.Min(1, StMascara.Length - (i - 1)));
						if (IAscii == Strings.Asc(stLetra[0]))
						{
							FMascaraValida = true;
							break;
						}
					} 
					break;
				case "NUMERIC" : case "SMALLINT" : case "INT" : 
					StMascara = StMascaraCadena + StMascaraFechaHora; 
					for (int i = 1; i <= StMascara.Length; i++)
					{
						stLetra = StMascara.Substring(i - 1, Math.Min(1, StMascara.Length - (i - 1)));
						if (IAscii == Strings.Asc(stLetra[0]))
						{
							FMascaraValida = true;
							break;
						}
					} 
					break;
			}
			if (!FMascaraValida)
			{
				//Muestra al usuario las m�scaras v�lidas
				short tempRefParam = 1750;
				string[] tempRefParam2 = new string[]{"el valor de " + stCampo.Trim(), StMascara};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
			}
		}

		private bool ValidarPrecision()
		{
			//Debemos buscar en la estructura el control
			//y comprobar si la precision de la parte entera
			//es la correcta
			Control MiControl = null;
			double varEntera = 0;
			bool fPrecision = false;

			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{

				fPrecision = true;

				if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
				{

					MiControl = astrElConjunto[i].NombreControl;

					if (MiControl.Name == "mebMascara")
					{
						varEntera = Math.Abs(Math.Floor(Conversion.Val(Convert.ToString(MiControl.FormattedText()))));
					}
					else
					{
						varEntera = Math.Abs(Math.Floor(Convert.ToDouble(MiControl.Text)));
					}


					switch(astrElConjunto[i].gtipodato)
					{
						case "SMALLINT" : 
							if (Math.Sign(Conversion.Val(Convert.ToString(MiControl.Text))) >= 0)
							{
								if (varEntera > Conversion.Val(ModuloMantenim.CoIntegerPosi) || Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
								{
									fPrecision = false;
								}
							}
							else
							{
								if (varEntera > Conversion.Val(ModuloMantenim.CoIntegerNega) || Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
								{
									fPrecision = false;
								}
							} 
							break;
						case "INT" : 
							if (Math.Sign(Conversion.Val(Convert.ToString(MiControl.Text))) >= 0)
							{
								if (varEntera > Conversion.Val(ModuloMantenim.CoLongPosi) || Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
								{
									fPrecision = false;
								}
							}
							else
							{
								if (varEntera > Conversion.Val(ModuloMantenim.CoLongNega) || Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
								{
									fPrecision = false;
								}
							} 
							break;
						case "NUMERIC" : 
							if (Marshal.SizeOf(varEntera) > astrElConjunto[i].nparteentera)
							{
								fPrecision = false;
							} 
							break;
					}

					if (!fPrecision)
					{
						//MENSAJE:V1 excede la Precisi�n definida.
						short tempRefParam = 1730;
						string[] tempRefParam2 = new string[]{astrElConjunto[i].dcoltabmanco};
						ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
						//            MiControl.SetFocus
						MiControl = null;
						return false;
					}

					MiControl = null;

				}
			}
			return true;
		}

		private void VisibleComboBox(bool fVisible)
		{
			//*************************************************
			return;
			//*************************************************
			if (fVisible)
			{
				int tempForVar = Cbb_AlgoNada.GetUpperBound(0);
				for (int i = 1; i <= tempForVar; i++)
				{
					Cbb_AlgoNada[i].Visible = true;
				}
			}
			else
			{
				int tempForVar2 = Cbb_AlgoNada.GetUpperBound(0);
				for (int i = 1; i <= tempForVar2; i++)
				{
					Cbb_AlgoNada[i].Visible = false;
				}
				//Tengo que refrescar el frame porque no desaparecen
				//los combos
				frmDetalle.Refresh();
			}
		}

		private void cbbCombo_Change(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cbbCombo, eventSender);
			if (cbbCombo[Index].Text.Trim() != "" && cbbCombo[Index].Enabled)
			{
				cbBotones[2].Enabled = true;
				if (sprMultiple.Enabled)
				{
					return;
				}
				vfCambios = true;
			}
		}

		private void cbbCombo_ClickEvent(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cbbCombo, eventSender);
			string Clave = String.Empty;
			bool fNumerico = false;

            //Cada vez que se elija un nuevo elemento del combo, habr� que obtener
            //los valores de clave de ese elemento en las cajas de texto ocultas
            //correspondientes.


            //Para saber el n� de columnas que forman la FK de cada combo,
            //lo hacemos sacando todos aquellos anchos de columna que sean cero.
            //Como los anchos que no son cero no se los hemos especificado al combo,
            //sino que hemos dejado que los calcule el combo autom�ticamente,
            //y la propiedad ColumnWidths del combo nos da los anchos separados
            //por punto y coma, con contar el n� de puntos y coma de la propiedad
            //ColumnWidths, sabremos cuantas columnas tienen de ancho cero.
            //As� si un combo tiene dos columnas de clave, la propiedad ColumnWidths
            //nos dar� "0 pt;0 pt". Contando el n� de comas nos da una, pero como luego
            //recorremos las columnas desde cero al n� de comas, recorreremos dos
            //columnas.
            // ralopezn TODO_X_5 20/05/2016
            int INumClaves = 0;// proNumPuntosYComa(1, Strings.Len(cbbCombo[Index].ColumnWidths), cbbCombo[Index].ColumnWidths);

			int iPosi = 0;
			int iPosicion = 0;
			string StEntero = String.Empty;
			int Idecimal = 0;
			int IPosiMask = 0;
			for (int k = 0; k <= INumClaves; k++)
			{
				for (int j = 0; j <= AStColFKCombo.GetUpperBound(0); j++)
				{
					if (AStColFKCombo[j].iIndiceCombo == Index && AStColFKCombo[j].IIndiceColEnCombo == k)
					{
						for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
						{
							if (astrElConjunto[i].gcolumncaman.Trim() == AStColFKCombo[j].StColHija.Trim())
							{
								//Comprobamos si es control fecha
								if (astrElConjunto[i].NombreControl.Name == "sdcFecha")
								{
									object tempRefParam = k;
									int tempRefParam2 = cbbCombo[Index].SelectedIndex;
									astrElConjunto[i].NombreControl.Text = Convert.ToDateTime(cbbCombo[Index].Items[tempRefParam2].Value).ToString("dd/MM/yyyy");
									k = Convert.ToInt32(tempRefParam);
									//Comprobamos si el control es Masked Edit
								}
								else if (astrElConjunto[i].NombreControl.Name == "mebMascara")
								{ 

									fNumerico = false;
									//Si es Masked Edit,compruebo que el dato sea num�rico
									if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
									{
										fNumerico = true;
									}

									//Compruebo si es decimal buscando la posici�n del punto
									object tempRefParam3 = k;
									int tempRefParam4 = cbbCombo[Index].SelectedIndex;
									iPosi = (Convert.ToString(cbbCombo[Index].Items[tempRefParam4].Value).Trim().IndexOf(ModuloMantenim.stSignoDecimal, StringComparison.CurrentCultureIgnoreCase) + 1);
									k = Convert.ToInt32(tempRefParam3);

									//Si el dato es num�rico y decimal
									if (iPosi != 0 && fNumerico)
									{

										object tempRefParam5 = k;
										int tempRefParam6 = cbbCombo[Index].SelectedIndex;
										iPosicion = (Convert.ToString(cbbCombo[Index].Items[tempRefParam6].Value).Trim().IndexOf(ModuloMantenim.stSignoDecimal, StringComparison.CurrentCultureIgnoreCase) + 1);
										k = Convert.ToInt32(tempRefParam5);
										object tempRefParam7 = k;
										int tempRefParam8 = cbbCombo[Index].SelectedIndex;
										StEntero = Convert.ToString(cbbCombo[Index].Items[tempRefParam8].Value).Trim().Substring(0, Math.Min(iPosicion - 1, Convert.ToString(cbbCombo[Index].Items[tempRefParam8].Value).Trim().Length));
										k = Convert.ToInt32(tempRefParam7);
										IPosiMask = (Convert.ToString(astrElConjunto[i].NombreControl.Mask()).IndexOf(ModuloMantenim.stSignoDecimal, StringComparison.CurrentCultureIgnoreCase) + 1);
										if (iPosicion + 1 == IPosiMask)
										{
											Idecimal = 0;
										}
										else
										{
											double dbNumericTemp = 0;
											object tempRefParam9 = k;
											int tempRefParam10 = cbbCombo[Index].SelectedIndex;
											if (Double.TryParse(Convert.ToString(cbbCombo[Index].Items[tempRefParam10].Value).Trim().Substring(iPosicion), NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
											{
												k = Convert.ToInt32(tempRefParam9);
												object tempRefParam11 = k;
												int tempRefParam12 = cbbCombo[Index].SelectedIndex;
												Idecimal = Convert.ToInt32(Double.Parse(Convert.ToString(cbbCombo[Index].Items[tempRefParam12].Value).Trim().Substring(iPosicion).Substring(0, Math.Min(4, Convert.ToString(cbbCombo[Index].Items[tempRefParam12].Value).Trim().Substring(iPosicion).Length)).Trim()));
												k = Convert.ToInt32(tempRefParam11);
											}
											else
											{
												k = Convert.ToInt32(tempRefParam9);
												Idecimal = 0;
											}
										}

										for (int p = 1; p <= (IPosiMask - StEntero.Length) - 1; p++)
										{
											StEntero = " " + StEntero;
										}
										astrElConjunto[i].NombreControl.Text = StEntero + ModuloMantenim.stSignoDecimal + Idecimal.ToString();

									}
									else
									{
										object tempRefParam13 = k;
										int tempRefParam14 = cbbCombo[Index].SelectedIndex;
										astrElConjunto[i].NombreControl.Text = Convert.ToString(cbbCombo[Index].Items[tempRefParam14].Value).Trim();
										k = Convert.ToInt32(tempRefParam13);
									}
								}
								else
								{
									//********************************************************************
									object tempRefParam15 = k;
									int tempRefParam16 = cbbCombo[Index].SelectedIndex;
									astrElConjunto[i].NombreControl.Text = Convert.ToString(cbbCombo[Index].Items[tempRefParam16].Value).Trim();
									k = Convert.ToInt32(tempRefParam15);
								}
								//                   TbClaves(Index).Text = astrElConjunto(i).NombreControl.Text
								TbClaves[Index].Text = CodigosClaves(cbbCombo[Index], Convert.ToInt32(cbbCombo[Index].SelectedIndex));
								break;
							}
						}
						break;
					}
				}
			}
		}

		private int proNumPuntosYComa(int Iinicio, int iFin, string cadena)
		{
			int ix = 0;
			for (int iy = Iinicio; iy <= iFin; iy++)
			{
				if (cadena.Substring(iy - 1, Math.Min(1, cadena.Length - (iy - 1))) == ";")
				{
					ix++;
				}
			}
			return ix;
		}
		private void cbbCombo_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cbbCombo, eventSender);
			cbbCombo[Index].SelectionStart = 0;
			cbbCombo[Index].SelectionLength = cbbCombo[Index].MaxLength;
			if (ActiveControl == cbbCombo[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}
		}

		private void cbbCombo_KeyDownEvent(Object eventSender, System.Windows.Forms.KeyEventArgs eventArgs)
		{
			if (eventArgs.KeyCode != Keys.Up && eventArgs.KeyCode != Keys.Down && eventArgs.KeyCode != Keys.Left && eventArgs.KeyCode != Keys.Right && eventArgs.KeyCode != Keys.Escape && eventArgs.KeyCode != Keys.End && eventArgs.KeyCode != Keys.Home)
			{
                //eventArgs.KeyValue = 0;
			}
		}

		private void cbbCombo_KeyPressEvent(Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
		{
			eventArgs.KeyChar = '0';
		}


		private void cbBotones_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cbBotones, eventSender);
			int lCantidad = 0;
			DialogResult iRespuesta = (DialogResult) 0;
			int i = 0;
			DialogResult StRes = (DialogResult) 0;
			DataSet RsCount = null;
			bool ErrorValidacion = false;
			vfCambios = false;
			this.Cursor = Cursors.WaitCursor;
			string consulta = String.Empty;

			if (Index == 0)
			{ //Aceptar


				switch(stPulsado)
				{
					case "incorporar" : 
						//Si no hemos buscado anteriormente los valores 
						//�nicos, los buscamos 
						if (!fUnicos)
						{
							BuscarUnicos();
						} 
						//********************************** 
						AStrFKUnicos = new ModuloMantenim.StrFKUnicos[AStColFKCombo.GetUpperBound(0) + 1]; 
						for (int k = 0; k <= AStColFKCombo.GetUpperBound(0); k++)
						{
							AStrFKUnicos[k].StColHija = AStColFKCombo[k].StColHija;
							AStrFKUnicos[k].StLabelCombo = AStColFKCombo[k].StLabelCombo;
						} 
						//********************************** 
						BuscarPK(true); 
						 
						ErrorValidacion = false; 
						if (ValidarRequeridoCombo(AStColFKCombo))
						{
							if (ValidarPrecision())
							{
								if (ValidarValoresUnicos(false, true))
								{
									if (ValidarPKUnicos())
									{
										if (!IncorporarDB())
										{
											return;
										}
										else
										{
											cbBotones[0].Enabled = true;
											cbBotones[1].Enabled = true;
											Descaracterizar();
										}
									}
									else
									{
										ErrorValidacion = true;
									}
								}
								else
								{
									ErrorValidacion = true;
								}
							}
							else
							{
								ErrorValidacion = true;
							}
						}
						else
						{
							ErrorValidacion = true;
						} 
						if (ErrorValidacion)
						{
							cbBotones[0].Enabled = true;
							cbBotones[1].Enabled = true;
							cbBotones[2].Enabled = true;
							this.Cursor = Cursors.Default;
							return;
						} 
						 
						LimpiarCampos(); 
						break;
					case "modificar" : 
						if (fActivarRegistro)
						{
							fActivarRegistro = false;
							StRes = RadMessageBox.Show("Si Fecha de Borrado no tiene valor, el c�digo volver� a estar activo." + "\r" + "�Desea continuar?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question);
							if (StRes == System.Windows.Forms.DialogResult.No)
							{
								Descaracterizar();
								cbSpread[0].Enabled = true;
								cbSpread[3].Enabled = true;
								cbSpread[4].Enabled = true;
								cbBotones[0].Enabled = false;
								cbBotones[1].Enabled = false;
								cbBotones[2].Enabled = false;
								VisibleComboBox(false);
								frmConsulta.Visible = false;
								vfCambiosPassword = false;
								rbConsulta[1].IsChecked = true;

								ModuloMantenim.DesactivarCampos(this, astrElConjunto, AStrToolbar);

								sprMultiple.Enabled = true;
                                sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
								this.Text = stCaptionForm;
								//Tengo que activar los frames para
								//poder limpiar los campos
								//*********************************
								frmDetalleFondo.Enabled = true;
								//*********************************
								LimpiarCampos();
								//Inhabilito los frames para dejarlos
								//como estaban
								//************************************
								frmDetalleFondo.Enabled = false;
								//************************************
								this.Cursor = Cursors.Default;
								return;
							}
						} 
						//Si no hemos buscado anteriormente los valores 
						//�nicos, los buscamos 
						if (!fUnicos)
						{
							BuscarUnicos();
						} 
						 
						//********************************** 
						AStrFKUnicos = new ModuloMantenim.StrFKUnicos[AStColFKCombo.GetUpperBound(0) + 1]; 
						for (int k = 0; k <= AStColFKCombo.GetUpperBound(0); k++)
						{
							AStrFKUnicos[k].StColHija = AStColFKCombo[k].StColHija;
							AStrFKUnicos[k].StLabelCombo = AStColFKCombo[k].StLabelCombo;
						} 
						//********************************** 

						 
						ErrorValidacion = false; 
						if (ValidarRequeridoCombo(AStColFKCombo))
						{
							if (ValidarPrecision())
							{
								if (ValidarValoresUnicos(true, true))
								{
									if (!ModificarDB())
									{
										this.Cursor = Cursors.Default;
										return;
									}
									else
									{
										Descaracterizar();
									}
								}
								else
								{
									ErrorValidacion = true;
								}
							}
							else
							{
								ErrorValidacion = true;
							}
						}
						else
						{
							ErrorValidacion = true;
						} 
						if (ErrorValidacion)
						{
							cbBotones[0].Enabled = true;
							cbBotones[1].Enabled = true;
							cbBotones[2].Enabled = true;
							this.Cursor = Cursors.Default;
							return;
						} 
						LimpiarCampos(); 
						break;
					case "consultar" : 
						 
						 
						//Recorrer todos los campos para ver si no est�n 
						//vac�os o tienen datos;comprobar campos requeridos en la 
						//consulta y, si hay, verificar que est�n introducidos 
						//y montar la Select, si no, enviar mensaje requiri�ndolos. 
						if (ValidarRequeridoConsultaCombo(AStColFKCombo))
						{
							//*********CRIS****************
							NuevaConsulta = true;
							//*****************************
							//Ver si los campos est�n vacios o con
							//datos y incluirlos para la consulta
							ComprobarCamposConsulta();
							//montar la consulta
							consulta = MontarConsulta(vstNombreTabla, "Select count(*) ", false);
							//Call HacerSelect(vstNombreTabla, consulta)
							//Dar mensaje de cantidad de registros
							SqlDataAdapter tempAdapter = new SqlDataAdapter(consulta, ModuloMantenim.RcManten);
							RsCount = new DataSet();
							tempAdapter.Fill(RsCount);
							//Call Requery(consulta)
							lCantidad = Convert.ToInt32(RsCount.Tables[0].Rows[0][0]);
							RsCount.Close();
							//MENSAJE:Se han encontrado V1 elementos.
							//''''''''�Desea continuar?.
							short tempRefParam = 1110;
							string[] tempRefParam2 = new string[]{lCantidad.ToString()};
							iRespuesta = ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);

							if (iRespuesta == System.Windows.Forms.DialogResult.Yes && lCantidad != 0)
							{ //"S�"
								vfCerrar = true;

								consulta = MontarConsulta(vstNombreTabla, "Select * ", true);

								//*****************
								ReHacerSelect(vstNombreTabla, consulta, false);
								//*****************

								//                       'Obtener el Order by de la tabla
								ModuloMantenim.stSQLOrder = ObtenerOrdenFilas(ModuloMantenim.RcManten, vstNombreTabla);
								ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + ModuloMantenim.stSQLOrder;

								sprMultiple.MaxRows = 0;
								Requery(ModuloMantenim.stConsulta);
								VolcarRegistrosEnArraySpread();
								ReEncontrarColumnasFK();
								VolcarArraySpread_EnSpread();

								frmSpread.Refresh();
								//*****************

								cbBotones[0].Enabled = true;
								cbBotones[1].Enabled = true;
								Descaracterizar();
							}
							else if (iRespuesta == System.Windows.Forms.DialogResult.No)
							{ 
								//********************
								NuevaConsulta = false;
								//********************
								this.Cursor = Cursors.Default;
								return;
							}
							else if (lCantidad == 0)
							{ 
								sprMultiple.MaxRows = 0;
								cbBotones[0].Enabled = true;
								cbBotones[1].Enabled = true;
								Descaracterizar();
							}

						}
						else
						{
							//Si la respuesta es No

							DesactivarBotonesSpread();
							cbBotones[0].Enabled = true;
							cbBotones[1].Enabled = true;
							Descaracterizar();
							this.Cursor = Cursors.Default;
							return;
						} 
						break;
				}
				cbSpread[0].Enabled = true;
				cbSpread[3].Enabled = true;
				cbSpread[4].Enabled = true;
				cbBotones[0].Enabled = false;
				cbBotones[1].Enabled = false;
				cbBotones[2].Enabled = false;

			}
			else if (Index == 1)
			{  //Cancelar

				switch(stPulsado)
				{
					case "incorporar" : 
						 
						LimpiarCampos(); 
						cbBotones[2].Enabled = false; 
						//VB6.SetDefault(cbBotones[0], false);
                        this.AcceptButton = null;
						vfCambios = false; 
						vfCambiosPassword = false; 
						 
						break;
					case "modificar" : case "consultar" : 
						 
						Descaracterizar(); 
						LimpiarCampos(); 
						cbBotones[2].Enabled = false;
                        this.AcceptButton = null;
                        vfCambios = false; 
						vfCambiosPassword = false; 
						 
						break;
				}
				//habilitar los botones correspondientes
				//Call ActivarBotonesSpread
				cbSpread[0].Enabled = true;
				cbSpread[3].Enabled = true;
				cbSpread[4].Enabled = true;
				cbSpread[5].Enabled = true;
				cbSpread[6].Enabled = true;
				cbBotones[0].Enabled = false;
				cbBotones[1].Enabled = false;
				cbBotones[2].Enabled = false;
			}
			else if (Index == 2)
			{  //Limpiar

				LimpiarCampos();
				cbSpread[1].Enabled = false;
				cbSpread[2].Enabled = false;
				this.Cursor = Cursors.Default;
				return;

			}

			vfCambiosPassword = false;
			rbConsulta[1].IsChecked = true;
			VisibleComboBox(false);
			frmConsulta.Visible = false;

			ModuloMantenim.DesactivarCampos(this, astrElConjunto, AStrToolbar);

			sprMultiple.Enabled = true;
            sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
			this.Text = stCaptionForm;

			//Si no hay filas en la spread, deshabilitamos el bot�n de imprimir
			cbSpread[4].Enabled = sprMultiple.MaxRows != 0;
			this.Cursor = Cursors.Default;
		}

		private bool ValidarPKUnicos()
		{
			//Si la tabla tiene valores �nicos, se valida si existe algun registro
			//con los valores �nicos iguales a los que quiero insertar

			bool result = false;
			StringBuilder StSelectPK = new StringBuilder();
			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{
				if (i == 0)
				{
					StSelectPK.Append(" (");
				}
				else
				{
					StSelectPK.Append(" AND ");
				}

				StSelectPK.Append(AStPK[i].StColumna.Trim() + "=");
				switch(AStPK[i].StTipo.Trim())
				{
					case "NUMERIC" : case "INT" : case "SMALLINT" : 
						StSelectPK.Append(AStPK[i].stTexto); 
						break;
					case "CHAR" : case "VARCHAR" : 
						StSelectPK.Append("'" + AStPK[i].stTexto.Trim() + "'"); 
						break;
					case "DATETIME" : 
						System.DateTime TempDate = DateTime.FromOADate(0); 
						StSelectPK.Append("'" + ((DateTime.TryParse(AStPK[i].stTexto, out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : AStPK[i].stTexto) + "'"); 
						break;
				}
			}

			StSelectPK.Append(")");
			string StComando = "select * from " + vstNombreTabla + " where " + StSelectPK.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsUnico = new DataSet();
			tempAdapter.Fill(RsUnico);
			if (RsUnico.Tables[0].Rows.Count > 0)
			{
				result = false;
				short tempRefParam = 1080;
				string[] tempRefParam2 = new string[]{"Elemento"};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
			}
			else
			{
				result = true;
			}
			RsUnico.Close();
			return result;
		}


		private void ReHacerSelect(string StTabla, string StComando, bool fOrdenar)
		{
			int p = 0;
			bool fEsta = false;

			ModuloMantenim.stConsulta = StComando + " ";

			if (fOrden)
			{
				AStParametro = new ModuloMantenim.StrParametro[AStOrden.GetUpperBound(0) + 1];
				//Montamos el WHERE necesario para hacer el ReQuery:
				//concatenando las columnas por las que se ordena y
				//las que forman parte de la clave primaria
				for (int i = 0; i <= AStOrden.GetUpperBound(0); i++)
				{
					switch(AStOrden[i].StTipo.Trim())
					{
						case "VARCHAR" : case "CHAR" : 
							//(maplaza)(18/12/2006)Si el campo es NULO, se produce error para BBDD con compatibilidad 70 
							//stConsulta = stConsulta & " CONVERT(CHAR(" _
							//'& AStOrden(i).iLongitud & ")," _
							//'& AStOrden(i).StColumna & ")" 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(" + AStOrden[i].iLongitud.ToString() + ")," + "ISNULL(" + AStOrden[i].StColumna + ",''))"; 
							//----- 
							break;
						case "NUMERIC" : case "INT" : case "SMALLINT" : 
							//Para las columnas num�ricas tengo que coger el n�mero, 
							//concatenarle espacios por la izquierda hasta la longitud 
							//del campo 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " REPLICATE('0'," + AStOrden[i].iLongitud.ToString() + "-DATALENGTH(RTRIM(CONVERT(CHAR(" + AStOrden[i].iLongitud.ToString() + ")," + AStOrden[i].StColumna + ")))) + " + "RTRIM(CONVERT(CHAR(" + AStOrden[i].iLongitud.ToString() + ")," + AStOrden[i].StColumna + "))"; 
							 
							break;
						case "DATETIME" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(8)," + AStOrden[i].StColumna + ",112)" + " + " + " CONVERT(CHAR(8)," + AStOrden[i].StColumna + ",108)"; 
							 
							break;
					}
					AStParametro[i].StTipo = AStOrden[i].StTipo;
					AStParametro[i].iLongitud = AStOrden[i].iLongitud;
					AStParametro[i].iOrdenColumnaSpread = AStOrden[i].iOrdenColumnaSpread;
					AStParametro[i].StColumna = AStOrden[i].StColumna;
					p = i;
					if (i != AStParametro.GetUpperBound(0))
					{
						ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " + ";
					}

				}
			}

			bool fDimensionar = true;

			if (!fOrden)
			{
				AStParametro = new ModuloMantenim.StrParametro[1];
				fDimensionar = false;
			}

			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{

				fEsta = false;
				foreach (ModuloMantenim.StrOrden AStOrden_item in AStOrden)
				{
					if (AStPK[i].StColumna.Trim() == AStOrden_item.StColumna.Trim())
					{
						fEsta = true;
						break;
					}
				}

				if (!fEsta)
				{
					if (fDimensionar)
					{
						AStParametro = ArraysHelper.RedimPreserve(AStParametro, new int[]{p + 2});
						ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " + ";
						if (fOrdenar)
						{
							ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + " , ";
						}
					}

					switch(AStPK[i].StTipo.Trim())
					{
						case "VARCHAR" : case "CHAR" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ")"; 
							break;
						case "NUMERIC" : case "INT" : case "SMALLINT" : 
							//Para las columnas num�ricas tengo que coger el n�mero, 
							//concatenarle espacios por la izquierda hasta la longitud 
							//del campo 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " SPACE(" + AStPK[i].iLongitud.ToString() + "-DATALENGTH(RTRIM(CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ")))) + " + "RTRIM(CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + "))"; 
							break;
						case "DATETIME" : 
							ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ",112)" + " + " + " CONVERT(CHAR(" + AStPK[i].iLongitud.ToString() + ")," + AStPK[i].StColumna + ",108)"; 
							 
							break;
					}

					if (fOrdenar)
					{
						ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + AStPK[i].StColumna;
					}

					p++;
					AStParametro[p].StTipo = AStPK[i].StTipo;
					AStParametro[p].iLongitud = AStPK[i].iLongitud;
					AStParametro[p].iOrdenColumnaSpread = AStPK[i].iOrdenColumnaSpread;
					AStParametro[p].StColumna = AStPK[i].StColumna;
					fDimensionar = true;
				}
			}

			ModuloMantenim.stConsulta = ModuloMantenim.stConsulta + " > @paramStr ";


		}
		private void ReEncontrarColumnasFK()
		{
			string StComando2 = String.Empty;
			StringBuilder StOrden = new StringBuilder();
			DataSet RsCombos2 = null;
			bool fDimensionar = false, fDimensionarCombo = false;
			int iLongitudColumna = 0;


			//Vuelvo a poner el orden de las columnas a cada elemento de mi array
			//de controles
			string StSQL = "select nordencoltab,gcolumncaman from SPRETBSPV1 " + "where GTABLAMANAUT='" + vstNombreTabla.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSQL, ModuloMantenim.RcManten);
			DataSet RsOrdenCol = new DataSet();
			tempAdapter.Fill(RsOrdenCol);

			foreach (DataRow iteration_row in RsOrdenCol.Tables[0].Rows)
			{
				for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
				{
					if (astrElConjunto[i].gcolumncaman.Trim() == Convert.ToString(iteration_row["gcolumncaman"]).Trim())
					{
						astrElConjunto[i].nordencoltab = (short) iteration_row["nordencoltab"];
						break;
					}
				}
			}

			RsOrdenCol.Close();

			viIndiceCombo = 0;

			//Obtengo los grupos de columnas de FK
			string StComando = "SELECT * FROM SCOLFKPKV1 " + "WHERE GTABCAMANHIJ= " + "'" + vstNombreTabla.Trim() + "'" + "ORDER BY gfkcaman,nordencolfk ";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsCombos = new DataSet();
			tempAdapter_2.Fill(RsCombos);

			//Obtenemos las columnas FK que se van a sustituir

			string StNombreFK = Convert.ToString(RsCombos.Tables[0].Rows[0]["GFKCAMAN"]).Trim();

			AStColFKCombo = new ModuloMantenim.StrFKCombo[1];

			while (RsCombos.Tables[0].Rows.Count != 0)
			{

				AStColFK = new ModuloMantenim.StrFK[1];
				fDimensionar = false;

				//Mientras la columna pertenezca al mismo grupo
				//de valores de FK, vamos a�adiendo sus datos a un array
				//de estructura
				foreach (DataRow iteration_row_2 in RsCombos.Tables[0].Rows)
				{

					if (StNombreFK.Trim() == Convert.ToString(iteration_row_2["GFKCAMAN"]).Trim())
					{
						if (fDimensionar)
						{
							AStColFK = ArraysHelper.RedimPreserve(AStColFK, new int[]{AStColFK.GetUpperBound(0) + 2});
						}

						fDimensionar = true;

						AStColFK[AStColFK.GetUpperBound(0)].StColHija = Convert.ToString(iteration_row_2["GCOLCAMANHIJ"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StColPadre = Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre = Convert.ToString(iteration_row_2["GTABCAMANPAD"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StLabelCombo = Convert.ToString(iteration_row_2["VLABELCOMBO"]).Trim();

						if (Convert.IsDBNull(iteration_row_2["GVISTACOMBO"]))
						{
							AStColFK[AStColFK.GetUpperBound(0)].fVistaNula = true;
						}
						else
						{
							AStColFK[AStColFK.GetUpperBound(0)].fVistaNula = false;
							AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo = Convert.ToString(iteration_row_2["GVISTACOMBO"]).Trim();
						}

						//Guardo el tipo de dato de cada columna
						for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
						{
							if (AStColFK[AStColFK.GetUpperBound(0)].StColHija.Trim() == astrElConjunto[i].gcolumncaman.Trim())
							{
								AStColFK[AStColFK.GetUpperBound(0)].stTipoDato = astrElConjunto[i].gtipodato.Trim();
								break;
							}
						}

					}
					else
					{
						//Si pasa por aqu�, significa que ya se
						//ha cambiado de grupo de columnas FK,
						//con lo que sadremos del Do While
						//y llenaremos el combo con los datos correspondientes
						//al anterior grupo de columnas FK
						break;
					}
				}


				//Si la vista est� a null, los datos se sacan de la tabla
				//padre
				if (AStColFK[AStColFK.GetUpperBound(0)].fVistaNula)
				{
					StComando = "SELECT * from SPREVICMV1 " + "Where gtablacaman = " + "'" + AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre.Trim() + "'" + "AND ISNULL(gvistacaman,'NULL') = 'NULL' " + "order by NORDENCOLTAB ";

					//Una de las condiciones de la select deber�a ser:
					//AND GVISTACAMAN IS NULL, pero esto no funciona con esta vista
					//y hemos tenido que sustituirlo por:
					//ISNULL(gvistacaman,'NULL') = 'NULL'
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombos2 = new DataSet();
					tempAdapter_3.Fill(RsCombos2);


					//Montamos la select de las columnas que van a ir en el combo

					StComando = "";
					AStOrdenFK = new ModuloMantenim.StrOrdenFK[1];
					iLongitudColumna = 0;
					foreach (DataRow iteration_row_3 in RsCombos2.Tables[0].Rows)
					{
						//S�lo participan en la select para cargar el combo
						//aquellas columnas que sean visibles.
						//Pero en el oreder by pueden participar tanto las visibles
						//como las no visibles
						if (Convert.ToString(iteration_row_3["IVISIBCOMTAB"]) == "S")
						{
							StComando = StComando + "A." + Convert.ToString(iteration_row_3["GCOLUMNCAMAN"]).Trim() + ",";

							//Calculamos la longitud m�xima que va a tener el campo en la spread
							//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
							//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
							//posici�n para el punto decimal

							if (Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "INT")
							{
								if (Convert.ToDouble(iteration_row_3["npartedecima"]) != 0)
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 2);
								}
								else
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 1);
								}
							}
							else
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]));
							}
						}
						//Nos creamos un array para guardar el orden de columnas
						//y si �stas son ascendentes o no
						if (Convert.ToDouble(iteration_row_3["NORDENACTAB"]) != 0)
						{
							if (Convert.ToDouble(iteration_row_3["NORDENACTAB"]) > AStOrdenFK.GetUpperBound(0))
							{
								AStOrdenFK = ArraysHelper.RedimPreserve(AStOrdenFK, new int[]{Convert.ToInt32(iteration_row_3["NORDENACTAB"]) + 1});
							}
							AStOrdenFK[Convert.ToInt32(iteration_row_3["NORDENACTAB"])].StAscendente = Convert.ToString(iteration_row_3["IASCENDETAB"]).Trim();
							AStOrdenFK[Convert.ToInt32(iteration_row_3["NORDENACTAB"])].StColumna = Convert.ToString(iteration_row_3["GCOLUMNCAMAN"]).Trim();
						}
					}

					RsCombos2.Close();

					//Quitamos la �ltima coma
					StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
					StComando = StComando + "from " + AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre.Trim() + " A";

					//Montamos la sentencia ORDER BY con el array
					//que nos hemos cargado anteriormente
					StComando2 = "";
					StOrden = new StringBuilder("");
					if (AStOrdenFK.GetUpperBound(0) != 0)
					{
						for (int j = 1; j <= AStOrdenFK.GetUpperBound(0); j++)
						{
							StOrden.Append("A." + AStOrdenFK[j].StColumna);
							if (AStOrdenFK[j].StAscendente.Trim() == "N")
							{
								StOrden.Append(" DESC");
							}
							StOrden.Append(",");
						}
						//Quitamos la �ltima coma
						StOrden = new StringBuilder(StOrden.ToString().Substring(0, Math.Min(StOrden.ToString().Length - 1, StOrden.ToString().Length)) + " ");
						StComando2 = " order by " + StOrden.ToString();
					}

					//Para saber en qu� columna va en el combo
					vIIndiceColEnCombo = 0;

					IntroducirFKEnArraySpread(StComando, StComando2, fDimensionarCombo);

				}
				else
				{
					//Si la vista no esta a null, los datos se sacan de la vista
					StComando = "SELECT * from SPREVICMV1 " + "Where gvistacaman = " + "'" + AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo.Trim() + "'" + "order by NORDENVISTA ";

					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombos2 = new DataSet();
					tempAdapter_4.Fill(RsCombos2);

					//Montamos la select de las columnas que van a ir en el combo

					StComando = "";
					AStOrdenFK = new ModuloMantenim.StrOrdenFK[1];
					iLongitudColumna = 0;
					foreach (DataRow iteration_row_4 in RsCombos2.Tables[0].Rows)
					{
						//S�lo participan en la select para cargar el combo
						//aquellas columnas que sean visibles.
						//Pero en el oreder by pueden participar tanto las visibles
						//como las no visibles
						if (Convert.ToString(iteration_row_4["IVISIBCOMVIS"]) == "S")
						{
							StComando = StComando + "A." + Convert.ToString(iteration_row_4["GCOLUMNCAMAN"]).Trim() + ",";

							//Calculamos la longitud m�xima que va a tener el campo en la spread
							//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
							//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
							//posici�n para el punto decimal

							//Adem�s le a�adimos siempre uno mas de longitud, ya que
							//los campos ir�n separados por espacios
							if (Convert.ToString(iteration_row_4["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row_4["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row_4["GTIPODATO"]).Trim() == "INT")
							{
								if (Convert.ToDouble(iteration_row_4["npartedecima"]) != 0)
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_4["nparteentera"]) + Convert.ToDouble(iteration_row_4["npartedecima"]) + 2 + 1);
								}
								else
								{
									iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_4["nparteentera"]) + Convert.ToDouble(iteration_row_4["npartedecima"]) + 1 + 1);
								}
							}
							else
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_4["nparteentera"]) + Convert.ToDouble(iteration_row_4["npartedecima"]) + 1);
							}
						}
						//Nos creamos un array para guardar el orden de columnas
						//y si �stas son ascendentes o no
						if (Convert.ToDouble(iteration_row_4["NORDENACVIS"]) != 0)
						{
							if (Convert.ToDouble(iteration_row_4["NORDENACVIS"]) > AStOrdenFK.GetUpperBound(0))
							{
								AStOrdenFK = ArraysHelper.RedimPreserve(AStOrdenFK, new int[]{Convert.ToInt32(iteration_row_4["NORDENACVIS"]) + 1});
							}
							AStOrdenFK[Convert.ToInt32(iteration_row_4["NORDENACVIS"])].StAscendente = Convert.ToString(iteration_row_4["IASCENDEVIS"]).Trim();
							AStOrdenFK[Convert.ToInt32(iteration_row_4["NORDENACVIS"])].StColumna = Convert.ToString(iteration_row_4["GCOLUMNCAMAN"]).Trim();
						}
					}

					RsCombos2.Close();

					//Quitamos la �ltima coma
					StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
					StComando = StComando + "from " + AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo.Trim() + " A";

					//Montamos la sentencia ORDER BY con el array
					//que nos hemos cargado anteriormente
					StOrden = new StringBuilder("");
					StComando2 = "";
					if (AStOrdenFK.GetUpperBound(0) != 0)
					{
						for (int j = 1; j <= AStOrdenFK.GetUpperBound(0); j++)
						{
							StOrden.Append("A." + AStOrdenFK[j].StColumna);
							if (AStOrdenFK[j].StAscendente.Trim() == "N")
							{
								StOrden.Append(" DESC");
							}
							StOrden.Append(",");
						}
						//Quitamos la �ltima coma
						StOrden = new StringBuilder(StOrden.ToString().Substring(0, Math.Min(StOrden.ToString().Length - 1, StOrden.ToString().Length)) + " ");
						StComando2 = " order by " + StOrden.ToString();
					}

					//Para saber en qu� columna va en el combo
					vIIndiceColEnCombo = 0;

					IntroducirFKEnArraySpread(StComando, StComando2, fDimensionarCombo);

				}

				//Guardo el nombre del nuevo grupo de columnas FK
				if (RsCombos.Tables[0].Rows.Count != 0)
				{
					StNombreFK = Convert.ToString(RsCombos.Tables[0].Rows[0]["GFKCAMAN"]).Trim();
				}
				viIndiceCombo++;
			}

			RsCombos.Close();


		}

		private void ReOrdenarColumnas(int iIndiceControl)
		{

			int i = aLongitud.GetUpperBound(0);
			while (i >= 0)
			{

				//como estamos introduciendo una nueva columna en el spread,
				//sumamos uno mas a este dato, que es el que nos dice el
				//n� de la columna  en el spread
				if (astrElConjunto[i].nordencoltab > iIndiceControl)
				{
					astrElConjunto[i].nordencoltab = (short) (astrElConjunto[i].nordencoltab + 1);
				}
				i--;
			}

		}



		private void cbSpread_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cbSpread, eventSender);

			DialogResult iRespuesta = (DialogResult) 0;
			int iIndiceModificar = 0;
			string stColumnas = String.Empty;
			int iEspacio = 0;
			string StComando = String.Empty;
			DataSet RsModificar = null;
			try
			{
				//****************************CRIS*************************
				//Antes de cambiar el modo hay que descaracterizar el detalle
				Descaracterizar();
				//*********************************************************
				cbBotones[2].Enabled = true;

				//********************************************************

				switch(Index)
				{
					case 0 :  //Incorporar 
						 
						if (StModo != "INCORPORAR")
						{
							this.Cursor = Cursors.WaitCursor;
							EncontrarColumnasFK("INCORPORAR");
							this.Cursor = Cursors.Default;
						} 
						 
						StModo = "INCORPORAR"; 
						this.Text = this.Text + " (Incorporar)"; 
						stPulsado = "incorporar"; 
						ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar); 
						LimpiarCampos(); 
						//Todos los botones de la Spread se han de inhabilitar 
						DesactivarBotonesSpread(); 
						//y habilitar los de Aceptar y cancelar con los campos. 
						cbBotones[0].Enabled = true; 
						cbBotones[1].Enabled = true; 
						ModuloMantenim.StFBORRADO = ObtenerColumnaFBorrado(); 
						CaracterizarIncorporar();  //Caracter�sticas de los campos 
                        this.AcceptButton = cbBotones[0];
                        vfCambios = false; 
						vfCambiosPassword = false; 
						ObtenerFocoCombo(AStColFKCombo); 
						cbBotones[2].Enabled = false; 
						//******************************************************** 
						 
						break;
					case 1 :  //Eliminar 
						this.Text = this.Text + " (Eliminar)"; 
						//Preguntar si est� seguro y si la respuesta es s� 
						//hacer las validaciones correspondientes y si todo es correcto 
						//ELIMINAR. 
						 
						DesactivarBotonesSpread(); 
						//Enviamos un mensaje pidiendo confirmaci�n 
						short tempRefParam = 1050; 
						iRespuesta = (DialogResult) Convert.ToInt32(ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, "")); 
						 
						//************************ 
						 
						if (iRespuesta == System.Windows.Forms.DialogResult.OK)
						{ //Aceptar

							//Borrar el registro activo
							//y quitarlo de la Spread
							ModuloMantenim.BorrarDB(this);

							this.Text = stCaptionForm;
							cbSpread[0].Enabled = true;
							cbSpread[3].Enabled = true;
							ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar);
							if (sprMultiple.MaxRows > 0)
							{
								cbSpread[4].Enabled = true;
							}
							//Tengo que activar los frames para
							//poder limpiar los campos
							//*********************************
							frmDetalleFondo.Enabled = true;
							//*********************************
							LimpiarCampos();
							sprMultiple.Enabled = true;
                            sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
							vfCambios = false;
							vfCambiosPassword = false;
							cbSpread[3].Focus();
							//Inhabilito los frames para dejarlos
							//como estaban
							//************************************
							frmDetalleFondo.Enabled = false;
							//************************************
							return;

						}
						else
						{
							//Cancelar Eliminar

							//No eliminar�a nada
							//Volver a la situaci�n anterior de botones
							this.Text = stCaptionForm;
							cbSpread[0].Enabled = true;
							cbSpread[3].Enabled = true;
							if (sprMultiple.MaxRows > 0)
							{
								cbSpread[4].Enabled = true;
							}
							//Tengo que activar los frames para
							//poder limpiar los campos
							//*********************************
							frmDetalleFondo.Enabled = true;
							//*********************************
							LimpiarCampos();
							sprMultiple.Enabled = true;
                            sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
							cbBotones[2].Enabled = false;
							vfCambios = false;
							vfCambiosPassword = false;
							cbSpread[3].Focus();
							//Inhabilito los frames para dejarlos
							//como estaban
							//************************************
							frmDetalleFondo.Enabled = false;
							//************************************
							return;
						} 
						 
						//******************************************************** 

					case 2 :  //Modificar 
						 
						if (StModo != "MODIFICAR")
						{
							this.Cursor = Cursors.WaitCursor;
							EncontrarColumnasFK("MODIFICAR");
							this.Cursor = Cursors.Default;
						} 
						StModo = "MODIFICAR"; 
						this.Text = this.Text + " (Modificar)"; 
						stPulsado = "modificar"; 
						 
						iIndiceModificar = 0; 
						 
						//*************************CRIS************************** 
						BuscarPK(); 
						//******************************************************* 
						ModuloMantenim.stSQLManten = MontarSelectConsulta();  //Para modificar 
						 
						if (!ModuloMantenim.PermitirBorrar(ModuloMantenim.stSQLManten))
						{
							//Borrar de la spread el registro eliminado
							//this.sprMultiple.Action = 5; //Borra de la Spread el registro actual
							this.sprMultiple.MaxRows--;
							return;
						} 
						//**************** 
						ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar); 
						DesactivarBotonesSpread(); 
						//Saber qu� columna hay que modificar y presentar en los campos 
						//''''''sprMultiple.Row = sprMultiple.ActiveRow (en el Click) 
						 
						LlenarCampos(); 
						 
						//habilitar los botones de Aceptar y Cancelar e 
						//inhabilitar Limpiar. 
						cbBotones[0].Enabled = true; 
						cbBotones[1].Enabled = true; 
                        this.AcceptButton = cbBotones[0];


                        if (ModuloMantenim.TablaBorradoLogico(vstNombreTabla, this))
						{
							if (!FBorradoNula())
							{
								RadMessageBox.Show("S�lo podr� modificar la columna de Fecha de Borrado a NULA " + "\r" + "ya que el registro ha sido borrado l�gicamente", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
								fSoloNULL = true;
								InhabilitarControles();
							}
							else
							{
								CaracterizarModificar(true); //Caracter�sticas de los campos
							}
						}
						else
						{
							CaracterizarModificar(); //Caracter�sticas de los campos
						} 
                        this.AcceptButton = cbBotones[0];

                        vfCambios = false; 
						vfCambiosPassword = false; 
						cbBotones[2].Enabled = false; 
						ObtenerFocoCombo(AStColFKCombo); 
						 
						//********************************************************* 
						 
						break;
					case 3 :  //Consultar 
						 
						if (StModo != "CONSULTAR")
						{
							this.Cursor = Cursors.WaitCursor;
							EncontrarColumnasFK("CONSULTAR");
							this.Cursor = Cursors.Default;
						} 
						StModo = "CONSULTAR"; 
						this.Text = this.Text + " (Consultar)"; 
						stPulsado = "consultar"; 
						 
						//Si est�n inhabilitados los botones de eliminar 
						//y modificar, significa que el �ltimo bot�n pulsado 
						//ha sido el consultar. Si esto ocurre, no limpio los 
						//campos para que tenga la consulta anterior 
						if (cbSpread[1].Enabled || cbSpread[2].Enabled)
						{
							LimpiarCampos();
						} 
						DesactivarBotonesSpread(); 
						//**************CRIS*************** 
						VisibleComboBox(true); 
						//********************************* 
						frmConsulta.Visible = true; 
						 
						cbBotones[0].Enabled = true; 
						cbBotones[1].Enabled = true; 
						ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar); 
						 
                        this.AcceptButton = cbBotones[0];

                        cbBotones[2].Enabled = true; 
						ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar); 
						cbBotones[0].Focus(); 
						ObtenerFocoCombo(AStColFKCombo); 
						 
						//******************************************************** 
						 
						break;
					case 4 :  //Imprimir 
						if (sprMultiple.MaxRows >= ModuloMantenim.vMaximoFilas)
						{
							//If vlCantidad >= vMaximoFilas Then
							if (CargarMas)
							{ //tiene que cargar mas
								short tempRefParam3 = 1660;
								string[] tempRefParam4 = new string[]{vlCantidad.ToString()};
								iRespuesta = (DialogResult) Convert.ToInt32(ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, ModuloMantenim.RcManten, tempRefParam4));
							}
							else
							{
								short tempRefParam5 = 1660;
								string[] tempRefParam6 = new string[]{sprMultiple.MaxRows.ToString()};
								iRespuesta = (DialogResult) Convert.ToInt32(ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, ModuloMantenim.RcManten, tempRefParam6)); //vlCantidad
							}
							if (iRespuesta == System.Windows.Forms.DialogResult.Yes)
							{
								//***************************
								while (CargarMas)
								{
									sprMultiple.Row = sprMultiple.MaxRows;
									sprMultiple.Col = sprMultiple.ActiveColumnIndex;
									sprMultiple.Action = 0;
								}
								//***************************
								DesactivarBotonesSpread();
								ModuloMantenim.CrearDBTemporal();
								ModuloMantenim.CrearTablaTemporal();
								ImprimirListado();
								cbSpread[0].Enabled = true;
								cbSpread[3].Enabled = true;
								cbSpread[4].Enabled = true;
								vfCambios = false;
								vfCambiosPassword = false;
							}
						}
						else
						{
							DesactivarBotonesSpread();
							ModuloMantenim.CrearDBTemporal();
							ModuloMantenim.CrearTablaTemporal();
							ImprimirListado();
							cbSpread[0].Enabled = true;
							cbSpread[3].Enabled = true;
							cbSpread[4].Enabled = true;
							vfCambios = false;
							vfCambiosPassword = false;
						} 
						//Tengo que activar los frames para 
						//poder limpiar los campos 
						//********************************* 
						frmDetalleFondo.Enabled = true; 
						//********************************* 
						LimpiarCampos(); 
						//Inhabilito los frames para dejarlos 
						//como estaban 
						//************************************ 
						frmDetalleFondo.Enabled = false; 
						//************************************ 
						//****************************************************************** 
						break;
					case 5 :  //Aceptar selecci�n 
						//Este bot�n y el siguiente solo aparecer�n en caso de que se halla 
						//llamado a este Active X desde un combo 
						if (sprMultiple.MaxRows == 0)
						{
							return;
						} 
						DevolverDatos(); 
						FrInstanciaFormulario.RecogerDatosMantenimiento(AStClaves, AStColumnasFK, AStTipoCol, viIndiceComboInhabilitado); 
						this.Close(); 
						return;
					case 6 :  //Cancelar selecci�n 
						this.Close(); 
						//oscar 30/09/03 
						return; 
						//-------------
					case 7 :  //Salir 
						this.Close(); 
						return; 

					case 8 : 
						//        MsgBox "Presenta la ayuda que no esta disponible" 
						// ralopezn TODO_X_5 20/05/2016
                        //CDAyuda.Instance.setHelpContext(this.getHelpContextID()); 
						//CDAyuda.setHelpCommand(0x1); 
						CDAyuda.Instance.ShowHelp(); 
						//****************************************************************** 
						break;
				}
                sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
				if (Index == 4)
				{
					return;
				}
				//sprMultiple.Enabled = False

				//Si no hay filas en la spread, deshabilitamos el bot�n de imprimir
				cbSpread[4].Enabled = sprMultiple.MaxRows != 0;
			}
			catch (System.Exception excep)
			{
				RadMessageBox.Show(excep.Message, Application.ProductName);
			}
		}



		private void SMT100F2_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				this.WindowState = FormWindowState.Maximized;
				if (fForm_Activate)
				{
					if (sprMultiple.MaxRows != 0)
					{
						this.cbSpread[3].Enabled = true;
						this.cbSpread[4].Enabled = true;
						this.cbSpread[3].Focus();
					}
					stCaptionForm = this.Text;

					OrdenarControles();

					ControlarScroll();

					fForm_Activate = false;
				}
				this.Cursor = Cursors.Default;
			}
		}

		private void SMT100F2_Load(Object eventSender, EventArgs eventArgs)
		{

			string sql = String.Empty;
			string lTotal = String.Empty;
			string iRespuesta = String.Empty;

			this.Top = (int) 0;
			this.Left = (int) 0;
			this.Cursor = Cursors.WaitCursor;
			// ralopezn TODO_X_5 20/05/2016
            //this.setHelpContextID(vlAyuda);

			fControlActivo = false;

			iIndiceOption = 1;

			proRecolocaControles();

			int RowHeightinTwips = 0;
			sprMultiple.RowHeightToTwips(0, (float) sprMultiple.GetRowHeight(0), RowHeightinTwips);
			iNumeroFilasVisibles = Convert.ToInt32(Conversion.Val(((((float) (sprMultiple.Height * 15)) / RowHeightinTwips) - 3).ToString()));
			//MsgBox iNumeroFilasVisibles

			//Primero comprobamos si s�lo puede hacer consultas
			//o puede hacer TODO.
			//Si viene de haberse llamado desde un combo,
			//entonces el modo es consulta
			if (fVieneDeCombos)
			{
				cbSpread[0].Visible = false;
				cbSpread[1].Visible = false;
				cbSpread[2].Visible = false;

				cbSpread[5].Visible = true;
				cbSpread[6].Visible = true;

			}
			else
			{
				ComprobarModoTotal();
			}
			//Inicializar la variable de consulta; por defecto 'AND'
			vstCriterioConsulta = " AND ";
			//Reloj de arena
			this.Cursor = Cursors.WaitCursor;
			//Incluir una Formula en Crystal para dar al listado la descripci�n
			//de la tabla de la que crearemos el informe.
			string stListado = "Listado de " + vstDescrLarTabla;
			ModuloMantenim.cryCrystal.Formulas["Tabla"] = "\"" + stListado + "\"";


			//**************************CRIS**********************
			frmDetalle.Height = (int) (ModuloMantenim.COiSuperiorCampo / 15);
			//***************************************************

			//Crea las cabeceras de la Spread y los controles del detalle.
			CrearSpread(vstNombreTabla, vstDescrLarTabla);

			//*****************CRIS***************************
			NuevaConsulta = false;
			//Obtener el Order by de la tabla
			ModuloMantenim.stSQLOrder = ObtenerOrdenFilas(ModuloMantenim.RcManten, vstNombreTabla);
			BuscarPK();
			HacerSelect(vstNombreTabla, "Select * from " + vstNombreTabla + " where ", true);
			//************************************************

			LlenarSpread(vstNombreTabla, ModuloMantenim.stConsulta, vfTodo);

			EncontrarColumnasFK();
			ModuloMantenim.DesactivarCampos(this, astrElConjunto, AStrToolbar);

			cbBotones[2].Enabled = false;
			this.Cursor = Cursors.Default;
			vfCambios = false;
			vfCambiosPassword = false;

			//Lleno las variables de caracteres v�lidos de formato y m�scara
			//Caracteres obtenidos seg�n la funci�n Format y propiedad
			//Mask de VB
			StFormatoNumero = "#0%e,.-+()$\\\"";
			StFormatoCadena = "@&<>!";
			StFormatoFechaHora = "acdhmnpqstwy/:";
			StMascaraNumero = "#.,\\&9C";
			StMascaraCadena = "\\&<>AaC?";
			StMascaraFechaHora = ":\\/&9C";


			//****************************************CRIS************************
			//Limpiamos los controles despues de crearlos,pintarlos y asignar todas
			//sus caracter�sticas porque, al ser arrays de controles, puede que se
			//hereden algunas propiedades de unos controles a otros
			LimpiarCampos();
			//********************************************************************

			CargarComboAlgoNada();

			fControlActivo = true;

			fForm_Activate = true;

			//Modo en el que debo cargar los combos
			StModo = "CONSULTAR";

			ReordenarParametros();

			CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

		}
		private void ReordenarParametros()
		{

			for (int i = 0; i <= AStParametro.GetUpperBound(0); i++)
			{
				foreach (ModuloMantenim.strConjuntoResultados astrElConjunto_item in astrElConjunto)
				{
					if (AStParametro[i].StColumna.Trim() == astrElConjunto_item.gcolumncaman.Trim())
					{
						AStParametro[i].iOrdenColumnaSpread = astrElConjunto_item.nordencoltab;
						break;
					}
				}
			}
		}

		private string ObtenerOrdenFilas(SqlConnection RcConexion, string stTablaOrden)
		{
			//Obtengo las columnas por las que se ordenan los registros
			//de las consultas y las caracter�sticas de las mismas

			string result = String.Empty;

			ModuloMantenim.stSQLOrder = "";

			string StSQL = "select gcolumncaman,nordenacion,iascendente," + "nparteentera,npartedecima,gtipodato,nordencoltab,valordefecto ";
			StSQL = StSQL + "from SPRETBSPV1 ";
			StSQL = StSQL + "where GTABLAMANAUT='";
			StSQL = StSQL + stTablaOrden + "'";
			StSQL = StSQL + " and nordenacion <> 0";
			StSQL = StSQL + " order by nordenacion";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSQL, ModuloMantenim.RcManten);
			ModuloMantenim.RsManten2 = new DataSet();
			tempAdapter.Fill(ModuloMantenim.RsManten2);
			if (ModuloMantenim.RsManten2.Tables[0].Rows.Count <= 0)
			{
				ModuloMantenim.RsManten2.Close();
				result = "";
				//**************************************
				fOrden = false;
				//**************************************
				return result;
			}
			else
			{
				//**************************************
				AStOrden = new ModuloMantenim.StrOrden[ModuloMantenim.RsManten2.Tables[0].Rows.Count];
				fOrden = true;
				//**************************************
				ModuloMantenim.stSQLOrder = " order by ";
				for (int iIndiceOrden = 0; iIndiceOrden <= ModuloMantenim.RsManten2.Tables[0].Rows.Count - 1; iIndiceOrden++)
				{
					if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["iascendente"]) == "S")
					{
						ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim() + " " + "ASC,";
					}
					else
					{
						//El orden siempre va a ser ascendente para poder hacer el
						//ReQuery ordenado
						ModuloMantenim.stSQLOrder = ModuloMantenim.stSQLOrder + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim() + " " + "ASC,"; //"DESC,"
					}
					//***********************CRIS***********************************
					AStOrden[iIndiceOrden].StColumna = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gcolumncaman"]).Trim();
					//*******************LUIS*******************
					if (!Convert.IsDBNull(ModuloMantenim.RsManten2.Tables[0].Rows[0]["valordefecto"]))
					{
						AStOrden[iIndiceOrden].vDefecto = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["valordefecto"]).Trim();
					}
					else
					{
						AStOrden[iIndiceOrden].vDefecto = "";
					}
					//*************Luis**************

					switch(Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim())
					{
						case "NUMERIC" :  //Un espacio m�s para el signo 
							AStOrden[iIndiceOrden].iLongitud = Convert.ToInt16(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 1); 
							//Un espacio m�s para el punto decimal 
							if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
							{
								AStOrden[iIndiceOrden].iLongitud = (short) (AStOrden[iIndiceOrden].iLongitud + 1);
							} 
							break;
						case "INT" : case "SMALLINT" :  //Un espacio m�s para el signo 
							AStOrden[iIndiceOrden].iLongitud = Convert.ToInt16(Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) + 1); 
							break;
						case "CHAR" : case "VARCHAR" : case "DATETIME" : 
							AStOrden[iIndiceOrden].iLongitud = (short) (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])); 
							 
							break;
					}

					AStOrden[iIndiceOrden].StTipo = Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim();
					AStOrden[iIndiceOrden].iOrdenColumnaSpread = (short) ModuloMantenim.RsManten2.Tables[0].Rows[0]["nordencoltab"];
					//********************************************************************************************************
					ModuloMantenim.RsManten2.MoveNext();
				}
			}
			ModuloMantenim.RsManten2.Close();
			//Para quitar la �ltima coma
			return ModuloMantenim.stSQLOrder.Substring(0, Math.Min(ModuloMantenim.stSQLOrder.Length - 1, ModuloMantenim.stSQLOrder.Length));
			//ObtenerOrdenFilas = stSQLOrder
		}



		private void SMT100F2_Closed(Object eventSender, CancelEventArgs eventArgs)
		{
			int iRespuesta = 0;

			if (vfCambios)
			{
				//MENSAJE:Antes de V1, �Desea guardar los cambios efectuados en V2?
				short tempRefParam = 1510;
				string[] tempRefParam2 = new string[]{"cerrar", vstDescrLarTabla};
				iRespuesta = Convert.ToInt32(ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2)); //1070

				//Comprobamos el valor del bot�n pulsado en el mensaje

				switch(iRespuesta)
				{
					case 2 :  //Cancelar 
						 
						eventArgs.Cancel = true; 
						//Exit Sub 
						 
						break;
					case 6 :  //Si 
						 
						GuardarCambios(); 
						vfCambios = false; 
						vfCambiosPassword = false; 
						//Set Mantenimiento = Nothing 
						//Exit Sub 
						 
						break;
					case 7 :  //No 
						 
						vfCambios = false; 
						vfCambiosPassword = false; 
						//Set Mantenimiento = Nothing 
						//Exit Sub 
						 
						break;
				}

			}

			if (vfCerrar && vfTodo)
			{
				Rq.Close();
			}
			if (NuevaConsulta)
			{
				//    RsManten.Close
			}



			MemoryHelper.ReleaseMemory();
		}



		private void mebMascara_Change(int Index)
		{
			if (mebMascara[Index].Text.Trim() != "" && mebMascara[Index].Enabled)
			{
				cbBotones[2].Enabled = true;
				if (sprMultiple.Enabled)
				{
					return;
				}
				vfCambios = true;
			}
		}

		private void mebMascara_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mebMascara, eventSender);

			mebMascara[Index].SelectionStart = 0;
			mebMascara[Index].SelectionLength = mebMascara[Index].MaskedEditBoxElement.TextBoxItem.MaxLength;
			if (ActiveControl == mebMascara[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}

		}



		private void mebMascara_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			int Index = Array.IndexOf(this.mebMascara, eventSender);
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].NombreControl.Name == "mebMascara" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
				{
					if (astrElConjunto[i].imayusculas == "S")
					{
						if (KeyAscii == 39)
						{
							KeyAscii = Serrores.SustituirComilla();
						}
						Serrores.ValidarTecla(ref KeyAscii);
						KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]);
					}
					if (KeyAscii == 39)
					{
						KeyAscii = Serrores.SustituirComilla();
					}
					if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC")
					{
						if ((KeyAscii < Strings.Asc('0') || KeyAscii > Strings.Asc('9')) && KeyAscii != Strings.Asc('-') && KeyAscii != Strings.Asc(',') || (mebMascara[Index].Text.IndexOf(',') >= 0 && KeyAscii == Strings.Asc(',')))
						{
							KeyAscii = 0;
						}
					}
					if (astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
					{
						if ((KeyAscii < Strings.Asc('0') || KeyAscii > Strings.Asc('9')) && KeyAscii != Strings.Asc('-'))
						{
							KeyAscii = 0;
						}
					}
					break;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void mebMascara_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.mebMascara, eventSender);
			int IposiNegativo = 0;
			int IposiPositivo = 0;
			bool FBool = false;

			if (this.ActiveControl.Name == cbBotones[1].Name)
			{
				if (!(ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[1]) && ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[2])) && mebMascara[Index].Text.Trim() != "" && stPulsado != "consultar")
				{ //Si no pulsa cancelar, ni limpiar
					//el campo no est� vacio y el bot�n pulsado no es el de consultar
					//se valida el tama�o fijo
					for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].NombreControl.Name == "mebMascara" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
						{
							if (astrElConjunto[i].itamaniofijo == "S")
							{
								IposiNegativo = (mebMascara[Index].Text.IndexOf('-') + 1);
								IposiPositivo = (mebMascara[Index].Text.IndexOf('+') + 1);
								if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
								{
									if (IposiNegativo != 0 || IposiPositivo != 0)
									{
										FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length - 1, i);
									}
									else
									{
										FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length, i);
									}
								}
								else
								{
									FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length, i);
								}
							}
							break;
						}
					}
				}
			}
			else if (mebMascara[Index].Text.Trim() != "" && stPulsado != "consultar")
			{  //Si no pulsa cancelar, ni limpiar
				//el campo no est� vacio y el bot�n pulsado no es el de consultar
				//se valida el tama�o fijo
				for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
				{
					if (astrElConjunto[i].NombreControl.Name == "mebMascara" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
					{
						if (astrElConjunto[i].itamaniofijo == "S")
						{
							IposiNegativo = (mebMascara[Index].Text.IndexOf('-') + 1);
							IposiPositivo = (mebMascara[Index].Text.IndexOf('+') + 1);
							if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
							{
								if (IposiNegativo != 0 || IposiPositivo != 0)
								{
									FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length - 1, i);
								}
								else
								{
									FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length, i);
								}
							}
							else
							{
								FBool = ValidarTama�oFijo(mebMascara[Index].Text.Trim().Length, i);
							}
						}
						break;
					}
				}
			}
		}


		private bool isInitializingComponent;
		private void rbConsulta_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.rbConsulta, eventSender);


				switch(Index)
				{
					case 0 :  //OR 
						vstCriterioConsulta = " OR "; 
						break;
					case 1 :  //AND 
						vstCriterioConsulta = " AND "; 
						break;
				}

			}
		}

		private void scbvCampos_Change(int newScrollValue)
		{
			//Mover los controles en el scrollbar

			//Comprobamos si el Scroll nos ordena
			//subir o bajar los controles
			viControlarValorScroll = Convert.ToInt32(Double.Parse(newScrollValue.ToString()));

			if (viControlarScroll < viControlarValorScroll)
			{
				//********************CRIS**************************
				//Mientras el top del frame sea mayor que la diferencia entre
				//las alturas de los frames DetalleFondo y Detalle (iDiferencia),
				//mas la separaci�n de campo con los l�mites del frame (COiSuperiorCampo),
				//dejo que se vayan subiendo los campos. Una vez que sean iguales ya
				//no subir�n mas, ya que querr� decir que se ve el �ltimo campo

				if (((float) (frmDetalle.Top * 15)) > -(iDiferencia + ModuloMantenim.COiSuperiorCampo))
				{
					//Compruebo que la barra no haya sido movida arrastrando
					if (Math.Abs(viControlarScroll - viControlarValorScroll) == scbvCampos.SmallChange)
					{
						frmDetalle.Top = (int) (frmDetalle.Top - 10); //esto
					}
					else
					{
						//Si se ha movido arrastrando, tendr� que saber el equivalente
						//de veces que se ha movido y eso lo obtengo restando la posicion
						//anterior y la actual y dividiendo esto entre el salto de la scroll
						//ya tengo el n� de veces que se ha movido.
						//Si multiplico eso por 150 es la cantidad que hay que sumarle al top
						//para que nos de el top actual
						frmDetalle.Top = (int) (frmDetalle.Top + 150 * ((viControlarScroll - viControlarValorScroll) / ((int) scbvCampos.SmallChange)) / 15);
					}
				}
				//**************************************************
			}
			else
			{
				//**********************CRIS**************************
				//Mientras el top del Frame Detalle  sea menor que cero puedo seguir
				//bajando los controles. Una vez que sea igual a cero ya no los bajar�
				//mas porque eso querr� decir que ya se ve el primero
				if (((float) (frmDetalle.Top * 15)) < 0)
				{
					//Compruebo que la barra no haya sido movida arrastrando
					if (Math.Abs(viControlarScroll - viControlarValorScroll) == scbvCampos.SmallChange)
					{
						frmDetalle.Top = (int) (frmDetalle.Top + 10); //esto
					}
					else
					{
						//Si se ha movido arrastrando, tendr� que saber el equivalente
						//de veces que se ha movido y eso lo obtengo restando la posicion
						//anterior y la actual y dividiendo esto entre el salto de la scroll
						//ya tengo el n� de veces que se ha movido.
						//Si multiplico eso por 150 es la cantidad que hay que sumarle al top
						//para que nos de el top actual.
						//Tanto el top anterior como la cantidad de veces multiplicada por 150
						//nos van a dar n�meros negativos, por eso se suman
						frmDetalle.Top = (int) (frmDetalle.Top + 150 * ((viControlarScroll - viControlarValorScroll) / ((int) scbvCampos.SmallChange)) / 15);
					}
				}
				//**************************************************
			}
			//Cogemos el �ltimo valor del Scroll
			//para poderlo comparar en el pr�ximo cambio
			viControlarScroll = Convert.ToInt32(Double.Parse(newScrollValue.ToString()));
		}

		private void sdcFecha_Change(int Index)
		{
			if (sprMultiple.Enabled)
			{
				return;
			}
			if (sdcFecha[Index].Text != "" && sdcFecha[Index].Enabled)
			{
				cbBotones[2].Enabled = true;
				vfCambios = true;
			}
		}

		private void sdcFecha_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.sdcFecha, eventSender);
			if (ActiveControl == sdcFecha[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}
		}


		private void sdcFecha_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.sdcFecha, eventSender);
			//Si no se ha ejecutado todavia el Form_Load
			//no habr� todav�a ning�n control activo, con
			//lo que cascar�a en este procedimiento.
			//Por eso inicializamos la varible fControlActivo=false
			//en el Form_Load y hasta que no termine no la ponemos a true
			if (fControlActivo)
			{
				//Miro si la fecha solo admite null,
				//si el control es el de fecha borrado
				//y no se ha pulsado cancelar ni limpiar
				if (fSoloNULL && Index == iIndexFecha && sdcFecha[Index].Text.Trim() != "")
				{
					if (this.ActiveControl.Name == cbBotones[1].Name)
					{
						if (ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[0]))
						{
							if (!(this.ActiveControl.Name == cbBotones[1].Name && ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[1])))
							{
								RadMessageBox.Show("S�lo admite cambio a null", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
								sdcFecha[Index].Focus();
								fActivarRegistro = false;
							}
						}
					}
					else if (fSoloNULL && Index == iIndexFecha && this.ActiveControl.Name != cbBotones[1].Name && sdcFecha[Index].Text.Trim() == "")
					{ 
						fActivarRegistro = true;
					}
				}
				else if (fSoloNULL && Index == iIndexFecha && sdcFecha[Index].Text.Trim() == "")
				{ 
					fActivarRegistro = true;
				}
			}
		}

        bool _isMouseRightDown = false;
        private void sprMultiple_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            _isMouseRightDown = (e.Button == MouseButtons.Right);
        }

        private void sprMultiple_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			if (!_isMouseRightDown)
			{
				int col = eventArgs.ColumnIndex;
				int Row = eventArgs.RowIndex;

				//Si lo que se ha pulsado es la cabecera del campo
				if (Row == 0)
				{
					return;
				}



				//Si el modo es de solo consulta
				if (!cbSpread[0].Visible)
				{
                    sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
					sprMultiple.Row = Row;
					LlenarCampos();
					frmDetalleFondo.Enabled = true;
					scbvCampos.Enabled = true;
					for (int i = 0; i <= LaConexion.GetUpperBound(0) - 1; i++)
					{
						LaConexion[i].NombreControl.Enabled = false;
					}
					cbSpread[5].Enabled = true;
					cbSpread[6].Enabled = true;

				}
				else
				{
					if (sprMultiple.MaxRows != 0)
					{
						if (cbSpread[3].Enabled)
						{
                            sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
							sprMultiple.Row = Row;
							ActivarBotonesSpread();
							LlenarCampos();
							ModuloMantenim.DesactivarCampos(this, astrElConjunto, AStrToolbar);
							cbSpread[2].Focus();
							cbBotones[2].Enabled = false;
							cbBotones[0].Enabled = false;
							cbBotones[1].Enabled = false;
							cbBotones[2].Enabled = false;
						}
					}
				}
			}
		}

		private void sprMultiple_CellDoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			if (Row == 0)
			{
				return;
			}
			if (!cbSpread[0].Visible)
			{
				return;
			}
			if (sprMultiple.MaxRows != 0)
			{
                sprMultiple.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
				sprMultiple.Row = Row;
				ActivarBotonesSpread();
				LlenarCampos();
				cbBotones[2].Enabled = false;
				//Ejecuta modificar
				cbSpread_Click(cbSpread[2], new EventArgs());
			}

		}


		private void sprMultiple_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;

			int FilaActiva = 0;
			int ColActiva = 0;

			if (KeyCode == ((int) Keys.Down))
			{
				FilaActiva = sprMultiple.ActiveRowIndex + 1;
				if (FilaActiva > sprMultiple.MaxRows)
				{
					FilaActiva = sprMultiple.ActiveRowIndex;
				}
				else
				{
                    var currentRow = sprMultiple.Rows[FilaActiva].Cells[ColActiva].RowInfo;
                    var currentCol = sprMultiple.Rows[FilaActiva].Cells[ColActiva].ColumnInfo;
                    sprMultiple_CellClick(sprMultiple, new GridViewCellEventArgs(currentRow, currentCol, null));
					LlenarCampos();
				}

			}
			else if (KeyCode == ((int) Keys.Up))
			{ 
				FilaActiva = sprMultiple.ActiveRowIndex - 1;
				if (FilaActiva == 0)
				{
					FilaActiva = sprMultiple.ActiveRowIndex;
				}
				else
				{
                    var currentRow = sprMultiple.Rows[FilaActiva].Cells[ColActiva].RowInfo;
                    var currentCol = sprMultiple.Rows[FilaActiva].Cells[ColActiva].ColumnInfo;
                    sprMultiple_CellClick(sprMultiple, new GridViewCellEventArgs(currentRow, currentCol, null));
                    LlenarCampos();
				}
			}

		}


		private void sprMultiple_TopLeftChange(object eventSender, UpgradeHelpers.Spread.TopLeftChangeEventArgs eventArgs)
		{
			//Cada vez que se baje la barra de la spread habr� que mirar
			//si hay m�s registros por cargar. Si los hay, habr� que hacer el
			//Requery comparando con la �ltima fila de la spread que cargamos en
			//nuestra variable par�metro

			StringBuilder StParametro = new StringBuilder();
			if (CargarMas)
			{
				if (eventArgs.NewTop >= Tope)
				{
					sprMultiple.Row = sprMultiple.MaxRows;
					LUltimaFilaSpread = sprMultiple.MaxRows;
					foreach (ModuloMantenim.StrParametro AStParametro_item in AStParametro)
					{
						sprMultiple.Col = AStParametro_item.iOrdenColumnaSpread;
						switch(AStParametro_item.StTipo)
						{
							case "NUMERIC" : case "INT" : case "SMALLINT" : 
								//Relleno con blancos por la izquierda 
								StParametro.Append(Padl(sprMultiple.Text.Trim(), AStParametro_item.iLongitud, "0")); 
								break;
							case "CHAR" : case "VARCHAR" : 
								//Relleno con blancos por la derecha 
								StParametro.Append(Padr(sprMultiple.Text.Trim(), AStParametro_item.iLongitud)); 
								break;
							case "DATETIME" : 
								//Relleno con blancos por la derecha 
								StParametro.Append(Padr(DateTime.Parse(sprMultiple.Text).ToString("yyyy/MM/dd").Trim(), AStParametro_item.iLongitud)); 
								break;
						}
					}

					Rq.Parameters[0].Value = StParametro.ToString();
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Rq);
					ModuloMantenim.RsManten = new DataSet();
					tempAdapter.Fill(ModuloMantenim.RsManten);
					if (ModuloMantenim.RsManten.Tables[0].Rows.Count < ModuloMantenim.vMaximoFilas)
					{
						CargarMas = false;
					}
					this.Cursor = Cursors.WaitCursor;
					VolcarRegistrosEnArraySpread();
					ReEncontrarColumnasFK();
					VolcarArraySpread_EnSpread();

					Tope = sprMultiple.MaxRows - iNumeroFilasVisibles; // 11
				}
			}

			this.Cursor = Cursors.Default;
		}

		private string Padr(string stCadena, int iEspacios, string stRelleno = " ")
		{
			//Funcion que concatena blancos u otro caracter por la derecha
			return stCadena.Trim() + new string(stRelleno[0], iEspacios - stCadena.Trim().Length);
		}

		private void TbClaves_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void TbClaves_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.TbClaves, eventSender);
			string[] Valorclave = null;
			string[] Nombrecolumna = null;
			string[] Tipodato = null;
			string[] NOMBREPADRE = null;
			int INumClaves = 0;
			int Coma = 0;
			string TextoClave = TbClaves[Index].Text;
			bool Encontrado = false;
			StModoCombo = StModo;
			int iCombo = 0;
			if (TbClaves[Index].Text != "")
			{
                // ralopezn TODO_X_5 20/05/2016
                INumClaves = 0;//proNumPuntosYComa(1, Strings.Len(cbbCombo[Index].ColumnWidths), cbbCombo[Index].ColumnWidths);
				for (int k = 0; k <= INumClaves; k++)
				{
					for (int j = 0; j <= AStColFKCombo.GetUpperBound(0); j++)
					{
						if (AStColFKCombo[j].iIndiceCombo == Index && AStColFKCombo[j].IIndiceColEnCombo == k)
						{
							for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
							{
								if (astrElConjunto[i].gcolumncaman.Trim() == AStColFKCombo[j].StColHija.Trim())
								{
									Valorclave = ArraysHelper.RedimPreserve(Valorclave, new int[]{iCombo + 1});
									Nombrecolumna = ArraysHelper.RedimPreserve(Nombrecolumna, new int[]{iCombo + 1});
									Tipodato = ArraysHelper.RedimPreserve(Tipodato, new int[]{iCombo + 1});
									NOMBREPADRE = ArraysHelper.RedimPreserve(NOMBREPADRE, new int[]{iCombo + 1});
									NOMBREPADRE[iCombo] = AStColFKCombo[j].StColPadre;
									cbbCombo[Index].Text = "";
                                    cbbCombo[Index].SelectedIndex = -1;
									Nombrecolumna[iCombo] = astrElConjunto[i].gcolumncaman;
									Tipodato[iCombo] = astrElConjunto[i].gtipodato;
									Coma = (TextoClave.IndexOf(',') + 1);
									if (Coma == 0)
									{
										Valorclave[iCombo] = TextoClave;
									}
									else
									{
										Valorclave[iCombo] = TextoClave.Substring(0, Math.Min(Coma - 1, TextoClave.Length));
										//                            If Len(Mid(TextoClave, Coma)) <= 1 Then
										//                                TextoClave = " "
										//                            Else
										TextoClave = TextoClave.Substring(Coma);
										//                            End If
									}
									if (Tipodato[iCombo].ToUpper() == "SMALLINT")
									{
										if (Conversion.Val(Valorclave[iCombo]) > StringsHelper.ToDoubleSafe(ModuloMantenim.CoIntegerPosi) || Conversion.Val(Valorclave[iCombo]) < -Double.Parse(ModuloMantenim.CoIntegerNega))
										{
											RadMessageBox.Show("El valor introducido debe estar comprendido entre -" + ModuloMantenim.CoIntegerNega + " y " + ModuloMantenim.CoIntegerPosi, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
											Encontrado = false;
											break;
										}
										if (Conversion.Val(Valorclave[iCombo]) == 0)
										{
											RadMessageBox.Show("Uno de los c�digos falta, o ha introducido un car�cter y debe ser n�merico. ", Application.ProductName);
											Encontrado = false;
											break;
										}
									}
									if (Tipodato[iCombo].ToUpper() == "INT")
									{
										if (Conversion.Val(Valorclave[iCombo]) > StringsHelper.ToDoubleSafe(ModuloMantenim.CoLongPosi) || Conversion.Val(Valorclave[iCombo]) < -Double.Parse(ModuloMantenim.CoLongNega) || Conversion.Str(Conversion.Val(Valorclave[iCombo])).Trim().Length != Valorclave[iCombo].Trim().Length)
										{
											RadMessageBox.Show("El valor introducido debe estar comprendido entre -" + ModuloMantenim.CoLongNega + " y " + ModuloMantenim.CoLongPosi, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
											TbClaves[Index].Focus();
											Encontrado = false;
											break;
										}
										if (Conversion.Val(Valorclave[iCombo]) == 0)
										{
											RadMessageBox.Show("Uno de los c�digos falta, o ha introducido un car�cter y debe ser n�merico. ", Application.ProductName);
											TbClaves[Index].Focus();
											Encontrado = false;
											break;
										}
									}
									Encontrado = true;
									iCombo++;
								}
							}
							break;
						}
					}
				}
				if (Encontrado)
				{
					for (int i = 0; i <= AStColFKCombo.GetUpperBound(0); i++)
					{
						if (ControlArrayHelper.GetControlIndex(AStrToolbar[Index].ComboAsociado) == AStColFKCombo[i].iIndiceCombo)
						{

							StModoCombo = StModo.Trim();
							//Hay que crear el array de columnas FK padre para mand�rselo al
							//Mantenimiento Autom�tico nuevo que se va a crear
							GuardarColumnasPadre(AStColFKCombo[i].iIndiceCombo);
							break;
						}
					}

					//el ultimo es el valor de la tabla que busco por eso le cambio el valor
					RecogerDatosMantenimiento(Valorclave, NOMBREPADRE, Tipodato, Index);
				}
				else
				{
					cbbCombo[Index].Text = "";
                    cbbCombo[Index].SelectedIndex = -1;
					TbClaves[Index].Text = "";
				}
			}
		}


		private void tbMultilinea_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbMultilinea, eventSender);
			if (tbMultilinea[Index].Text.Trim() != "" && tbMultilinea[Index].Enabled)
			{
				cbBotones[2].Enabled = true;
				if (sprMultiple.Enabled)
				{
					return;
				}
				vfCambios = true;
			}
		}

		private void tbMultilinea_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbMultilinea, eventSender);

			tbMultilinea[Index].SelectionStart = 0;
			tbMultilinea[Index].SelectionLength = tbMultilinea[Index].MaxLength;
			if (ActiveControl == tbMultilinea[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}
		}

		private void tbMultilinea_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			int Index = Array.IndexOf(this.tbMultilinea, eventSender);
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].NombreControl.Name == "tbMultilinea" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
				{
					//Oscar 25/06/04 Impedimos que se pulse "Ctrl+Intro" o "Shift+Intro"
					if (KeyAscii == 10 || KeyAscii == 13)
					{
						KeyAscii = 0;
					}
					//-------
					if (KeyAscii == 39)
					{
						KeyAscii = Serrores.SustituirComilla();
					}
					if (astrElConjunto[i].imayusculas == "S")
					{
						Serrores.ValidarTecla(ref KeyAscii);
						KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]);
					}
					break;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbMultilinea_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbMultilinea, eventSender);
			int IposiNegativo = 0;
			int IposiPositivo = 0;
			bool FBool = false;

			if (this.ActiveControl.Name == cbBotones[1].Name)
			{
				if (!(ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[1]) && ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[2])) && tbMultilinea[Index].Text.Trim() != "" && stPulsado != "consultar")
				{ //Si no pulsa cancelar, ni limpiar
					//el campo no est� vacio y el bot�n pulsado no es el de consultar
					//se valida el tama�o fijo
					for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].NombreControl.Name == "tbMultilinea" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
						{
							if (astrElConjunto[i].itamaniofijo == "S")
							{
								if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
								{
									IposiNegativo = (tbMultilinea[Index].Text.IndexOf('-') + 1);
									IposiPositivo = (tbMultilinea[Index].Text.IndexOf('+') + 1);
									if (IposiNegativo != 0 || IposiPositivo != 0)
									{
										FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length + 1, i);
									}
									else
									{
										FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length, i);
									}
								}
								else
								{
									FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length, i);
								}
							}
							break;
						}
					}
				}
			}
			else if (tbMultilinea[Index].Text.Trim() != "" && stPulsado != "consultar")
			{  //Si no pulsa cancelar, ni limpiar
				//el campo no est� vacio y el bot�n pulsado no es el de consultar
				//se valida el tama�o fijo
				for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
				{
					if (astrElConjunto[i].NombreControl.Name == "tbMultilinea" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
					{
						if (astrElConjunto[i].itamaniofijo == "S")
						{
							if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
							{
								IposiNegativo = (tbMultilinea[Index].Text.IndexOf('-') + 1);
								IposiPositivo = (tbMultilinea[Index].Text.IndexOf('+') + 1);
								if (IposiNegativo != 0 || IposiPositivo != 0)
								{
									FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length + 1, i);
								}
								else
								{
									FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length, i);
								}
							}
							else
							{
								FBool = ValidarTama�oFijo(tbMultilinea[Index].Text.Trim().Length, i);
							}
						}
						break;
					}
				}
			}
		}

		private void tbTexto_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbTexto, eventSender);

			if (tbTexto[Index].Text.Trim() != "" && tbTexto[Index].Enabled)
			{
				cbBotones[2].Enabled = true;
			}

			//Comprobamos que est� la Spread habilitado porque es se�al
			//de que se ha disparado el evento Change al llenar los campos
			//desde la Spread.
			if (!tbTexto[Index].Enabled)
			{
				return;
			}

			//Si la variable vfCambiosPassword est� a true es se�al de
			//que hemos encriptado

			//***************************CRIS***********************************
			//Creo que lo que hace es:
			//Si vfCambiosPassword es falso, es que no se ha producido ning�n
			//cambio en la password, pero se est� produciendo ahora. Entonces
			//pone vfCambios = True para que luego pregunte si se quieren salvar
			//los cambios, y mira si el campo es password, ya que si tiene PassWordChar
			//es campo password. Si es password pone vfCambiosPassword = True
			//para que con la proxima letra que introduzcamos no se repita
			//todo el proceso
			//******************************************************************
			if (vfCambiosPassword)
			{
				return;
			}

			vfCambios = true;

			if (Convert.ToString(tbTexto[Index].PasswordChar.ToString()) == "*")
			{
				vfCambiosPassword = true;
			}


		}

		private void tbTexto_Enter(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbTexto, eventSender);
			tbTexto[Index].SelectionStart = 0;
			tbTexto[Index].SelectionLength = tbTexto[Index].MaxLength;
			if (ActiveControl == tbTexto[Index])
			{
				ModuloMantenim.ActualizaBarraDesplazamiento(ActiveControl, scbvCampos, frmDetalle);
			}
		}


		private void CrearCampoFecha()
		{

			if (sdcFecha.GetUpperBound(0) == 0)
			{
				//vfComprFecha es un comprobador por si hemos creado ya
				//el primer control
				if (!vfComprFecha)
				{
					LaConexion[iIndice].NombreControl = sdcFecha[0];
					astrElConjunto[iIndice].NombreControl = sdcFecha[0];
					sdcFecha[0].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
					sdcFecha[0].Text = "";

					iIndiceFecha = 1;
					vfComprFecha = true;
					return;
				}
			}
			ControlArrayHelper.LoadControl(this, "sdcFecha", iIndiceFecha);
			LaConexion[iIndice].NombreControl = sdcFecha[iIndiceFecha];
			astrElConjunto[iIndice].NombreControl = sdcFecha[iIndiceFecha];
			sdcFecha[iIndiceFecha].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
			sdcFecha[iIndiceFecha].Text = "";
			iIndiceCampoFecha = iIndiceFecha;
			iIndiceFecha++;

		}

		public void CrearCampoMultilinea(int ILineas)
		{
			if (tbMultilinea.GetUpperBound(0) == 0)
			{
				if (!vfComprMultilinea)
				{
					//**************************CRIS***************************
					tbMultilinea[0].Width = (int) (((ModuloMantenim.COiTamCaractEtiqueta * ModuloMantenim.COiMaxTamLineaMultiline) + ModuloMantenim.COiTamBordes + ModuloMantenim.COiTamScrollBarMultiline) / 15);
					//*********************************************************
					tbMultilinea[0].MaxLength = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
					LaConexion[iIndice].NombreControl = tbMultilinea[0];
					astrElConjunto[iIndice].NombreControl = tbMultilinea[0];
					tbMultilinea[0].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
					//**************************CRIS*****************************
					//El Height que tiene el multil�nea por defecto es para 3 l�neas
					//luego si lo queremos pintar de dos l�neas lo dividimos entre
					//3 y lo multilicamos por 2
					if (ILineas == 2)
					{
						tbMultilinea[0].Height = (int) ((ModuloMantenim.COiHeightMultiline3 / ((int) 3)) * 2 / 15);
					}
					else
					{
						tbMultilinea[0].Height = (int) (ModuloMantenim.COiHeightMultiline3 / 15);
					}
					//************************************************************
					iIndiceMultilinea = 1;
					vfComprMultilinea = true;
					return;
				}
				iIndiceCampoMultilinea++;
			}

			ControlArrayHelper.LoadControl(this, "tbMultilinea", iIndiceMultilinea);
			//**************************CRIS***************************
			tbMultilinea[iIndiceMultilinea].Width = (int) (((ModuloMantenim.COiTamCaractEtiqueta * ModuloMantenim.COiMaxTamLineaMultiline) + ModuloMantenim.COiTamBordes + ModuloMantenim.COiTamScrollBarMultiline) / 15);
			//*********************************************************
			tbMultilinea[iIndiceMultilinea].MaxLength = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));

			LaConexion[iIndice].NombreControl = tbMultilinea[iIndiceMultilinea];
			astrElConjunto[iIndice].NombreControl = tbMultilinea[iIndiceMultilinea];
			tbMultilinea[iIndiceMultilinea].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;

			//**************************CRIS*****************************
			//El Height que tiene el multil�nea por defecto es para 3 l�neas
			//luego si lo queremos pintar de dos l�neas lo dividimos entre
			//3 y lo multilicamos por 2
			if (ILineas == 2)
			{
				tbMultilinea[iIndiceMultilinea].Height = (int) ((ModuloMantenim.COiHeightMultiline3 / ((int) 3)) * 2 / 15);
			}
			else
			{
				tbMultilinea[iIndiceMultilinea].Height = (int) (ModuloMantenim.COiHeightMultiline3 / 15);
			}
			//************************************************************

			iIndiceCampoMultilinea = iIndiceMultilinea;
			iIndiceMultilinea++;


		}

		public void CrearCampoTexto()
		{

			if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "S")
			{
				tbTexto[0].PasswordChar = '*';
			}
			if (tbTexto.GetUpperBound(0) == 0)
			{
				if (!vfComprTexto)
				{
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) == 1)
					{
						tbTexto[0].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
					}
					else
					{
						tbTexto[0].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
						tbTexto[0].MaxLength = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
						//Le pondremos 2 caracteres(signo y separaci�n de decimales
						//si es num�rico
						if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
						{
							tbTexto[0].MaxLength += 2;
						}
					}

					//**********************CRIS*******************************************
					//Para todos aquellos campos que tengan 10 caracteres o menos
					//le a�adiremos a su ancho un caracter m�s, porque est� medido con el
					//ancho de la X
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) <= 10)
					{
						tbTexto[0].Width = (int) (tbTexto[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					}
					//*********************************************************************

					LaConexion[iIndice].NombreControl = tbTexto[0];
					astrElConjunto[iIndice].NombreControl = tbTexto[0];

					if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "S")
					{
						tbTexto[0].PasswordChar = '*';
					}

					tbTexto[0].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
					iIndiceCampoTexto = 1;
					vfComprTexto = true;
					return;
				}
				iIndiceTexto++;
			}
			ControlArrayHelper.LoadControl(this, "tbTexto", iIndiceCampoTexto);
			if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) == 1)
			{
				tbTexto[iIndiceCampoTexto].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
			}
			else
			{
				tbTexto[iIndiceCampoTexto].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
				tbTexto[iIndiceCampoTexto].MaxLength = Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]));
				//Le pondremos 2 caracteres(signo y separaci�n de decimales
				//si es num�rico
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
				{
					tbTexto[iIndiceCampoTexto].MaxLength += 2;
				}

			}

			//**********************CRIS*******************************************
			//Para todos aquellos campos que tengan 10 caracteres o menos
			//le a�adiremos a su ancho un caracter m�s, porque est� medido con el
			//ancho de la X
			if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) <= 10)
			{
				tbTexto[iIndiceCampoTexto].Width = (int) (tbTexto[iIndiceCampoTexto].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
			}
			//*********************************************************************

			LaConexion[iIndice].NombreControl = tbTexto[iIndiceCampoTexto];
			astrElConjunto[iIndice].NombreControl = tbTexto[iIndiceCampoTexto];

			if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["ipassword"]) == "S")
			{
				tbTexto[iIndiceCampoTexto].PasswordChar = '*';
			}

			tbTexto[iIndiceCampoTexto].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
			iIndiceTexto = iIndiceCampoTexto;
			iIndiceCampoTexto++;

		}

		private void PintarCampoTexto()
		{
			int iValorTop = 0;

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				iValorTop = ModuloMantenim.COiSuperiorCampo;
			}
			else
			{
				iValorTop = ModuloMantenim.COiSeparacion;
			}

			if (tbTexto.GetUpperBound(0) == 0)
			{
				tbTexto[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				tbTexto[0].Left = (int) (viNuevoLeft / 15);
				tbTexto[0].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (tbTexto[0].Top * 15)) + ((float) (tbTexto[0].Height * 15)));
			}
			else
			{
				tbTexto[iIndiceTexto].Top = (int) ((iValorTop + viNuevoTop) / 15);
				tbTexto[iIndiceTexto].Left = (int) (viNuevoLeft / 15);
				tbTexto[iIndiceTexto].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (tbTexto[iIndiceTexto].Top * 15)) + ((float) (tbTexto[iIndiceTexto].Height * 15)));

			}
			//***************************CRIS**************************
			//Call AlturaFrame(tbTexto(iIndiceTexto))
			//***************************CRIS**************************
		}

		private void PintarCampoMultilinea(int ILineas)
		{
			int iValorTop = 0;

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				iValorTop = ModuloMantenim.COiSuperiorCampo;
			}
			else
			{
				iValorTop = ModuloMantenim.COiSeparacion;
			}

			if (tbMultilinea.GetUpperBound(0) == 0)
			{
				tbMultilinea[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				tbMultilinea[0].Left = (int) (viNuevoLeft / 15);
				tbMultilinea[0].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (tbMultilinea[0].Top * 15)) + ((float) (tbMultilinea[0].Height * 15)));
			}
			else
			{

				tbMultilinea[iIndiceCampoMultilinea].Top = (int) ((iValorTop + viNuevoTop) / 15);
				tbMultilinea[iIndiceCampoMultilinea].Left = (int) (viNuevoLeft / 15);
				tbMultilinea[iIndiceCampoMultilinea].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (tbMultilinea[iIndiceCampoMultilinea].Top * 15)) + ((float) (tbMultilinea[iIndiceCampoMultilinea].Height * 15)));
			}

			//***************************CRIS**************************
			//Call AlturaFrame(tbMultilinea(iIndiceCampoMultilinea))
			//*********************************************************
		}

		private void PintarCampoFecha()
		{
			int iValorTop = 0;

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				iValorTop = ModuloMantenim.COiSuperiorCampo;
			}
			else
			{
				iValorTop = ModuloMantenim.COiSeparacion;
			}

			if (sdcFecha.GetUpperBound(0) == 0)
			{
				sdcFecha[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				sdcFecha[0].Left = viNuevoLeft;
				sdcFecha[0].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************

				viNuevoTop = Convert.ToInt32(((float) (sdcFecha[0].Top * 15)) + sdcFecha[0].Height);
			}
			else
			{
				sdcFecha[iIndiceCampoFecha].Top = (int) ((iValorTop + viNuevoTop) / 15);
				sdcFecha[iIndiceCampoFecha].Left = viNuevoLeft;
				sdcFecha[iIndiceCampoFecha].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (sdcFecha[iIndiceCampoFecha].Top * 15)) + sdcFecha[iIndiceCampoFecha].Height);
			}
			sdcFecha[iIndiceCampoFecha].Text = "";
			//***************************CRIS**************************
			//Call AlturaFrame(sdcFecha(iIndiceCampoFecha))
			//***************************CRIS**************************
		}

		private void CrearCampoCombo(int iIndiceControl, int iLongitudColumna)
		{

			if (cbbCombo.GetUpperBound(0) == 0)
			{
				//vfComprCombo es un comprobador por si hemos creado ya
				//el primer control
				if (!vfComprCombo)
				{
					LaConexion[iIndiceControl].NombreControl = cbbCombo[0];
					LaConexion[iIndiceControl].NombreCampo = "";
					LaConexion[iIndiceControl].NombreTabla = "";
					LaConexion[iIndiceControl].NumeroColumna = 0;
					astrElConjunto[iIndiceControl].NombreControl = cbbCombo[0];
					astrElConjunto[iIndiceControl].dcoltabmanco = "";
					astrElConjunto[iIndiceControl].dcoltabmanla = "";
					astrElConjunto[iIndiceControl].dtablamancor = "";
					astrElConjunto[iIndiceControl].fEsta = false;
					astrElConjunto[iIndiceControl].gcolumncaman = "";
					astrElConjunto[iIndiceControl].ggrunimanaut = "";
					astrElConjunto[iIndiceControl].gtablamanaut = "";
					astrElConjunto[iIndiceControl].gtipocolumna = "";
					astrElConjunto[iIndiceControl].gtipodato = "";
					astrElConjunto[iIndiceControl].iadmitenull = "";
					astrElConjunto[iIndiceControl].iascendente = "";
					astrElConjunto[iIndiceControl].icolfborrado = "";
					astrElConjunto[iIndiceControl].icolformato = "";
					astrElConjunto[iIndiceControl].icolmascara = "";
					astrElConjunto[iIndiceControl].imayusculas = "";
					astrElConjunto[iIndiceControl].ipassword = "";
					astrElConjunto[iIndiceControl].ireqconsulta = "";
					astrElConjunto[iIndiceControl].itamaniofijo = "";
					astrElConjunto[iIndiceControl].nordcolinun = 0;
					astrElConjunto[iIndiceControl].nordenacion = 0;
					astrElConjunto[iIndiceControl].nordencolpk = "0";
					astrElConjunto[iIndiceControl].nordencoltab = 0;
					astrElConjunto[iIndiceControl].npartedecima = 0;
					astrElConjunto[iIndiceControl].nparteentera = 0;
					astrElConjunto[iIndiceControl].valordefecto = "";
					astrElConjunto[iIndiceControl].vformato = "";
					astrElConjunto[iIndiceControl].vmascara = "";

					aLongitud[iIndiceControl] = iLongitudColumna;
					//***********************************************
					AStrToolbar = new ModuloMantenim.StrToolbar[1];
					AStrToolbar[0].NombreControl = TlbBuscar[0];
					AStrToolbar[0].ComboAsociado = cbbCombo[0];
					AStrToolbar[0].ClaveAsociada = TbClaves[0];
					//***********************************************
					cbbCombo[0].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
					iIndiceCombo = 1;
					vfComprCombo = true;
					return;
				}
			}
			ControlArrayHelper.LoadControl(this, "cbbCombo", iIndiceCombo);
			ControlArrayHelper.LoadControl(this, "TlbBuscar", iIndiceCombo);
			ControlArrayHelper.LoadControl(this, "TbClaves", iIndiceCombo);
			LaConexion[iIndiceControl].NombreControl = cbbCombo[iIndiceCombo];
			LaConexion[iIndiceControl].NombreCampo = "";
			LaConexion[iIndiceControl].NombreTabla = "";
			LaConexion[iIndiceControl].NumeroColumna = 0;
			cbbCombo[iIndiceCombo].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
			astrElConjunto[iIndiceControl].NombreControl = cbbCombo[iIndiceCombo];
			astrElConjunto[iIndiceControl].dcoltabmanco = "";
			astrElConjunto[iIndiceControl].dcoltabmanla = "";
			astrElConjunto[iIndiceControl].dtablamancor = "";
			astrElConjunto[iIndiceControl].fEsta = false;
			astrElConjunto[iIndiceControl].gcolumncaman = "";
			astrElConjunto[iIndiceControl].ggrunimanaut = "";
			astrElConjunto[iIndiceControl].gtablamanaut = "";
			astrElConjunto[iIndiceControl].gtipocolumna = "";
			astrElConjunto[iIndiceControl].gtipodato = "";
			astrElConjunto[iIndiceControl].iadmitenull = "";
			astrElConjunto[iIndiceControl].iascendente = "";
			astrElConjunto[iIndiceControl].icolfborrado = "";
			astrElConjunto[iIndiceControl].icolformato = "";
			astrElConjunto[iIndiceControl].icolmascara = "";
			astrElConjunto[iIndiceControl].imayusculas = "";
			astrElConjunto[iIndiceControl].ipassword = "";
			astrElConjunto[iIndiceControl].ireqconsulta = "";
			astrElConjunto[iIndiceControl].itamaniofijo = "";
			astrElConjunto[iIndiceControl].nordcolinun = 0;
			astrElConjunto[iIndiceControl].nordenacion = 0;
			astrElConjunto[iIndiceControl].nordencolpk = "0";
			astrElConjunto[iIndiceControl].nordencoltab = 0;
			astrElConjunto[iIndiceControl].npartedecima = 0;
			astrElConjunto[iIndiceControl].nparteentera = 0;
			astrElConjunto[iIndiceControl].valordefecto = "";
			astrElConjunto[iIndiceControl].vformato = "";
			astrElConjunto[iIndiceControl].vmascara = "";

			aLongitud[iIndiceControl] = iLongitudColumna;
			//***********************************************
			AStrToolbar = ArraysHelper.RedimPreserve(AStrToolbar, new int[]{AStrToolbar.GetUpperBound(0) + 2});
			AStrToolbar[iIndiceCombo].NombreControl = TlbBuscar[iIndiceCombo];
			AStrToolbar[iIndiceCombo].ComboAsociado = cbbCombo[iIndiceCombo];
			AStrToolbar[iIndiceCombo].ClaveAsociada = TbClaves[iIndiceCombo];
			//***********************************************
			iIndiceCampoCombo = iIndiceCombo;
			iIndiceCombo++;

		}

		private void PintarCampoCombo()
		{
			int iValorTop = 0;

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				iValorTop = ModuloMantenim.COiSuperiorCampo;
			}
			else
			{
				iValorTop = ModuloMantenim.COiSeparacion;
			}

			if (cbbCombo.GetUpperBound(0) == 0)
			{

				cbbCombo[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				cbbCombo[0].Left = (int) (viNuevoLeft / 15);
				cbbCombo[0].Visible = true;
				//cbbCombo(0).Width = 6000

				//***********************************************
				TlbBuscar[0].Top = (int) (cbbCombo[0].Top - 2);
				TlbBuscar[0].Left = (int) (cbbCombo[0].Left + cbbCombo[0].Width + 2);
				TlbBuscar[0].Visible = true;
				//***********************************************
				TbClaves[0].Text = Conversion.Str(0);
				TbClaves[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				TbClaves[0].Left = (int) (TlbBuscar[0].Left + TlbBuscar[0].Width + 2);

				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (cbbCombo[0].Top * 15)) + ((float) (cbbCombo[0].Height * 15)));
			}
			else
			{

				cbbCombo[iIndiceCampoCombo].Top = (int) ((iValorTop + viNuevoTop) / 15);
				cbbCombo[iIndiceCampoCombo].Left = (int) (viNuevoLeft / 15);
				cbbCombo[iIndiceCampoCombo].Visible = true;
				//cbbCombo(iIndiceCampoCombo).Width = 6000

				//***********************************************
				TlbBuscar[iIndiceCampoCombo].Top = (int) (cbbCombo[iIndiceCampoCombo].Top - 2);
				TlbBuscar[iIndiceCampoCombo].Left = (int) (cbbCombo[iIndiceCampoCombo].Left + cbbCombo[iIndiceCampoCombo].Width + 2);
				TlbBuscar[iIndiceCampoCombo].Visible = true;
				//***********************************************
				TbClaves[iIndiceCampoCombo].Text = Conversion.Str(iIndiceCombo);
				TbClaves[iIndiceCampoCombo].Top = (int) ((iValorTop + viNuevoTop) / 15);
				TbClaves[iIndiceCampoCombo].Left = (int) (TlbBuscar[iIndiceCampoCombo].Left + TlbBuscar[iIndiceCampoCombo].Width + 2);

				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (cbbCombo[iIndiceCampoCombo].Top * 15)) + ((float) (cbbCombo[iIndiceCampoCombo].Height * 15)));
			}

			//***************************CRIS**************************
			//Call AlturaFrame(cbbCombo(iIndiceCampoCombo))
			//***************************CRIS**************************
		}



		public void LimpiarCampos()
		{
			int iIndiceLimpiar = 0;

			int tempForVar = tbTexto.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar; iIndiceLimpiar++)
			{
				if (tbTexto[iIndiceLimpiar].Enabled)
				{
					tbTexto[iIndiceLimpiar].Text = "";
				}
			}

			iIndiceLimpiar = 0;
			int tempForVar2 = mebMascara.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar2; iIndiceLimpiar++)
			{
				if (mebMascara[iIndiceLimpiar].Enabled)
				{
					mebMascara[iIndiceLimpiar].Text = "";
				}
			}

			iIndiceLimpiar = 0;
			int tempForVar3 = sdcFecha.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar3; iIndiceLimpiar++)
			{
				if (sdcFecha[iIndiceLimpiar].Enabled)
				{
					sdcFecha[iIndiceLimpiar].Text = "";
				}
			}

			iIndiceLimpiar = 0;
			int tempForVar4 = tbMultilinea.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar4; iIndiceLimpiar++)
			{
				if (tbMultilinea[iIndiceLimpiar].Enabled)
				{
					tbMultilinea[iIndiceLimpiar].Text = "";
				}
			}

			iIndiceLimpiar = 0;
			int tempForVar5 = cbbCombo.GetUpperBound(0);
			for (iIndiceLimpiar = 0; iIndiceLimpiar <= tempForVar5; iIndiceLimpiar++)
			{
				//If cbbCombo(iIndiceLimpiar).Enabled = True Then
				cbbCombo[iIndiceLimpiar].Text = "";
				TbClaves[iIndiceLimpiar].Text = "";
				//End If
			}


		}

		public void LlenarCampos()
		{
			bool fNumerico = false;
			if (sprMultiple.Row == 0)
			{
				return;
			}

			int iIndiceLlenado = 0;
			Control[] MiControl = null;
			//Inicializo a = para usar las mismas variales
			iIndiceTexto = 0;
			iIndiceCampoMultilinea = 0;
			iIndiceCampoFecha = 0;
			iIndiceCampoMascara = 0;
			string signo = ModuloMantenim.stSignoDecimal;


			//Bucle For para llenar los controles
			int iPosi = 0;
			int iPosicion = 0;
			string StEntero = String.Empty;
			int Idecimal = 0;
			int IPosiMask = 0;
			for (iIndiceLlenado = 0; iIndiceLlenado <= (sprMultiple.MaxCols - 1); iIndiceLlenado++)
			{
				fNumerico = false;
				sprMultiple.Col = iIndiceLlenado + 1;
				MiControl = new Control[iIndiceLlenado + 1];
				MiControl[iIndiceLlenado] = LaConexion[iIndiceLlenado].NombreControl;

				if (MiControl[iIndiceLlenado].Name == "sdcFecha")
				{
					LaConexion[iIndiceLlenado].NombreControl.Text = ModuloMantenim.FormatearFecha(aLongitud[iIndiceLlenado], sprMultiple.Text);
					//***************CRIS********************************************
					//Comprobamos si el control es Masked Edit
				}
				else if (MiControl[iIndiceLlenado].Name == "mebMascara")
				{ 

					//Si es Masked Edit,compruebo que el dato sea num�rico
					foreach (ModuloMantenim.strConjuntoResultados astrElConjunto_item in astrElConjunto)
					{
						if (astrElConjunto_item.NombreControl.Name == MiControl[iIndiceLlenado].Name && ControlArrayHelper.GetControlIndex(astrElConjunto_item.NombreControl) == ControlArrayHelper.GetControlIndex(MiControl[iIndiceLlenado]))
						{
							if (astrElConjunto_item.gtipodato.Trim() == "NUMERIC" || astrElConjunto_item.gtipodato.Trim() == "SMALLINT" || astrElConjunto_item.gtipodato.Trim() == "INT")
							{
								fNumerico = true;
								iPosi = (sprMultiple.Text.IndexOf('.') + 1);
								if (iPosi != 0)
								{
									signo = ".";
								}
								else
								{
									iPosi = (sprMultiple.Text.IndexOf(',') + 1);
									signo = ",";
								}

							}
							break;
						}
					}
					iPosi = 0;
					//Compruebo si es decimal buscando la posici�n del punto
					iPosi = (sprMultiple.Text.IndexOf(signo, StringComparison.CurrentCultureIgnoreCase) + 1);

					//Si el dato es num�rico y decimal
					if (iPosi != 0 && fNumerico)
					{

						iPosicion = (sprMultiple.Text.IndexOf(signo, StringComparison.CurrentCultureIgnoreCase) + 1);
						StEntero = sprMultiple.Text.Substring(0, Math.Min(iPosicion - 1, sprMultiple.Text.Length));
						IPosiMask = (Convert.ToString(LaConexion[iIndiceLlenado].NombreControl.Mask()).IndexOf(signo, StringComparison.CurrentCultureIgnoreCase) + 1);
						if (iPosicion + 1 == IPosiMask)
						{
							Idecimal = 0;
						}
						else
						{
							double dbNumericTemp = 0;
							if (Double.TryParse(sprMultiple.Text.Substring(iPosicion), NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
							{
								Idecimal = Convert.ToInt32(Double.Parse(sprMultiple.Text.Substring(iPosicion).Substring(0, Math.Min(4, sprMultiple.Text.Substring(iPosicion).Length)).Trim()));
							}
							else
							{
								Idecimal = 0;
							}
						}

						for (int i = 1; i <= (IPosiMask - StEntero.Length) - 1; i++)
						{
							StEntero = " " + StEntero;
						}
						LaConexion[iIndiceLlenado].NombreControl.Text = StEntero + ModuloMantenim.stSignoDecimal + Idecimal.ToString();

					}
					else
					{
						LaConexion[iIndiceLlenado].NombreControl.Text = sprMultiple.Text;
					}
					//Comprobamos si el control es Combo para rellenar en may�sculas
				}
				else if (MiControl[iIndiceLlenado].Name == "cbbCombo")
				{ 
					LaConexion[iIndiceLlenado].NombreControl.Text = sprMultiple.Text.ToUpper();

					//        TbClaves(LaConexion(iIndiceLlenado).NombreControl.Index).Text = UCase(Trim(sprMultiple.Text))
					TbClaves[ControlArrayHelper.GetControlIndex(LaConexion[iIndiceLlenado].NombreControl)].Text = CodigoClavesSpread(sprMultiple.Col, ControlArrayHelper.GetControlIndex(LaConexion[iIndiceLlenado].NombreControl));

				}
				else
				{
					//********************************************************************
					LaConexion[iIndiceLlenado].NombreControl.Text = sprMultiple.Text;
				}

			}

			for (iindice2 = 0; iindice2 <= (iIndiceLlenado - 1); iindice2++)
			{
				MiControl[iindice2] = null;
			}

			iindice2 = 0;
			iIndiceTexto = 0;
			iIndiceCampoMultilinea = 0;
			iIndiceCampoFecha = 0;
			iIndiceCampoMascara = 0;

		}

		private void DesactivarBotonesSpread()
		{

			for (int iIndiceBotones = 0; iIndiceBotones <= 6; iIndiceBotones++)
			{
				cbSpread[iIndiceBotones].Enabled = false;
			}

			//oscar 04/03/03
			sprMultiple.Enabled = false;
			//----------

		}

		private void ActivarBotonesSpread()
		{

			for (int iIndiceBotones = 0; iIndiceBotones <= 6; iIndiceBotones++)
			{
				cbSpread[iIndiceBotones].Enabled = true;
			}

			//oscar 04/03/03
			sprMultiple.Enabled = true;
			//-----------
		}

		private void ImprimirListado()
		{
			int i = 0;
			string stCamino = String.Empty;
			string stRPTVertical = String.Empty;
			string stRPTHorizontal = String.Empty;
			string stRPTHorizontal6 = String.Empty;
			int iLongitud = 0;
			string stListado = String.Empty;
			int Intentos = 0;
			//***************CRIS*************
			string StCabecera = String.Empty;
            //********************************
            int resumeStatement = 1;
            do
            {
                try
                {
                    Intentos = 0;
                    ModuloMantenim.RegistrarDSN();

                    stCamino = ModuloMantenim.vStPathReport;
                    stRPTVertical = "\\smt100f1v.rpt";
                    stRPTHorizontal = "\\smt100f1h.rpt";
                    stRPTHorizontal6 = "\\smt100f1h6.rpt";


                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    StringBuilder stFila = new StringBuilder();
                    string stCadaUno = String.Empty;

                    //Cargamos el array de las longitudes de los campos en un nuevo array
                    aLongitudListado = new int[aLongitud.GetUpperBound(0) + 1];

                    for (i = 0; i <= aLongitud.GetUpperBound(0); i++)
                    {
                        aLongitudListado[i] = aLongitud[i];
                    }

                    i = 0;

                    //Colocamos la spread en la primera celda.

                    // ralopezn TODO_X_5 20/05/2016
                    //ModuloMantenim.DBInforme = (Recordset) ModuloMantenim.DB.OpenRecordset("select * from listado where 2=1");

                    sprMultiple.Row = 0;
                    sprMultiple.Col = 1;
                    //For para ir desde la primera fila hasta la �ltima
                    for (int iIndiceInforme = 1; iIndiceInforme <= sprMultiple.MaxRows + 1; iIndiceInforme++)
                    { //He agregado +1
                      //For para concatenar todos los registros de cada fila
                        int tempForVar = sprMultiple.MaxCols;
                        for (int iIndiceFila = 1; iIndiceFila <= tempForVar; iIndiceFila++)
                        {
                            //S�lo salen las columnas que no est�n ocultas
                            if (!sprMultiple.GetColHidden(sprMultiple.Col))
                            {
                                string tempRefParam = sprMultiple.Text.Trim();
                                stCadaUno = SumarBlancos(tempRefParam, sprMultiple.Text.Trim().Length, aLongitudListado[iIndiceFila - 1]);
                                if (stFila.ToString() == "")
                                { //Para evitar el primer blanco
                                    stFila = new StringBuilder(stCadaUno);
                                }
                                else
                                {
                                    stFila.Append(" " + stCadaUno);
                                }
                            }

                            sprMultiple.Col++;
                        }
                        //*********************
                        if (sprMultiple.Row == 0)
                        {
                            StCabecera = stFila.ToString().ToUpper();
                        }
                        else
                        {
                            // ralopezn TODO_X_5 20/05/2016
                            //ModuloMantenim.DBInforme.AddNew();
                            //ModuloMantenim.DBInforme["numero"] = (Recordset) sprMultiple.Row;
                            //ModuloMantenim.DBInforme["tipo"] = (Recordset) "L";
                            //ModuloMantenim.DBInforme["linea"] = (Recordset) stFila.ToString();
                            //ModuloMantenim.DBInforme["Cabecera"] = (Recordset) StCabecera;
                            //ModuloMantenim.DBInforme.Update();
                        }
                        //*********************
                        //comprobar si es el �ltimo registro
                        if (sprMultiple.Row == sprMultiple.MaxRows)
                        { //Le he quitado el sprMultiple.MaxRows-1
                            iLongitud = stFila.ToString().Length;
                        }

                        sprMultiple.Row++;
                        stFila = new StringBuilder("");
                        sprMultiple.Col = 1;
                    }
                    // ralopezn TODO_X_5 20/05/2016
                    //ModuloMantenim.DBInforme.Close();
                    //ModuloMantenim.DB.Close();
                    //ModuloMantenim.Wk.Close();

                    ModuloMantenim.cryCrystal.Connect = "dsn=" + ModuloMantenim.CoDSNTemp + ";UID=" + ModuloMantenim.gstUsuario + ";PWD=" + ModuloMantenim.gstPassword + "";

                    if (iLongitud <= 115)
                    {
                        ModuloMantenim.cryCrystal.ReportFileName = stCamino + stRPTVertical;
                    }
                    else if (iLongitud > 115 && iLongitud <= 160)
                    {
                        ModuloMantenim.cryCrystal.ReportFileName = stCamino + stRPTHorizontal;
                    }
                    else
                    {
                        ModuloMantenim.cryCrystal.ReportFileName = stCamino + stRPTHorizontal6;
                    }


                    //Recoger los datos del Hospital para Crystal
                    ModuloMantenim.RecogerDatosHospital();
                    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //Incluir una Formula en Crystal para dar al listado la descripci�n
                    //de la tabla de la que crearemos el informe.
                    stListado = "Listado de " + vstDescrLarTabla;
                    ModuloMantenim.cryCrystal.Formulas["Tabla"] = "\"" + stListado + "\"";

                    // ralopezn TODO_X_5 20/05/2016 Crystal
                    //ModuloMantenim.cryCrystal.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
                    ModuloMantenim.cryCrystal.WindowShowPrintSetupBtn = true;
                    ModuloMantenim.cryCrystal.Action = 1;
                    ModuloMantenim.cryCrystal.PrinterStopPage = ModuloMantenim.cryCrystal.PageCount;
                    ModuloMantenim.cryCrystal.Reset();
                    //Set cryCrystal = Nothing
                    resumeStatement = 0;
                }
                catch
                {
                    if (Intentos < 3)
                    {
                        Intentos++;
                        resumeStatement = 1;
                    }
                    else
                    {
                        resumeStatement = 0;
                        return;
                    }
                }
            } while (resumeStatement != 0);
		}
		private string SumarBlancos(string stCadena, int iLongitudCabecera, int iLongitudColumna)
		{
			//Funci�n que ajusta el contenido de un campo al mayor valor
			//entre la etiqueta y el tama�o de la columna que soporta el campo,
			//ampliandolo al mayor de los dos con blancos.
			int iDiferencia = 0;

			if (iLongitudCabecera <= iLongitudColumna)
			{
				iDiferencia = iLongitudColumna - iLongitudCabecera;
				//Bucle para sumarle los espacios de diferencia
				for (int iIndiceSumar = 1; iIndiceSumar <= iDiferencia; iIndiceSumar++)
				{
					stCadena = stCadena + " ";
				}
			}
			else
			{
				aLongitudListado[this.sprMultiple.Col - 1] = iLongitudCabecera;
			}

			return stCadena;

		}

		public void CrearCampoMascara()
		{
			bool fMasUno = true;
			if (mebMascara.GetUpperBound(0) == 0)
			{
				if (!vfComprMascara)
				{
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) == 1)
					{

						mebMascara[0].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
						mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])));
						//Le pondremos 1 caracter (signo)
						//si es num�rico. No va a tener
						//punto ya que la suma de parte entera y decimal es 1
						if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
						{
							mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
							mebMascara[0].Width = (int) (mebMascara[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
							fMasUno = false;
						}


					}
					else
					{
						mebMascara[0].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
						mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])));
						//Le pondremos 1 caracter para signo y otro para separaci�n de
						//decimales si la parte decimal<>0 si es num�rico
						if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
						{
							mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
							mebMascara[0].Width = (int) (mebMascara[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
							fMasUno = false;
							if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
							{
								mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[0].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
								mebMascara[0].Width = (int) (mebMascara[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
							}

						}
					}
					//**********************CRIS*******************************************
					//Para todos aquellos campos que tengan 10 caracters o menos
					//le a�adiremos a su ancho un caracter m�s, porque est� medido con el
					//ancho de la X. La boolena fMasUno quiere decir que si esta a verdadero
					//es porque el campo no es num�rico, ya que si lo es no hace falta sumarle
					//un caracter mas
					if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) <= 10 && fMasUno)
					{
						mebMascara[0].Width = (int) (mebMascara[0].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					}
					//*********************************************************************
					LaConexion[iIndice].NombreControl = mebMascara[0];
					astrElConjunto[iIndice].NombreControl = mebMascara[0];
					mebMascara[0].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
					iIndiceCampoMascara = 1;
					vfComprMascara = true;
					return;
				}
				iIndiceMascara++;
			}
			ControlArrayHelper.LoadControl(this, "mebMascara", iIndiceCampoMascara);
			mebMascara[iIndiceCampoMascara].TabIndex = iIndice + ModuloMantenim.COiDesplazamTab;
			if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) == 1)
			{
				mebMascara[iIndiceCampoMascara].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
				mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])));
				//Le pondremos 1 caracter (signo)
				//si es num�rico. No va a tener
				//punto ya que la suma de parte entera y decimal es 1
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
				{
					mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
					mebMascara[iIndiceCampoMascara].Width = (int) (mebMascara[iIndiceCampoMascara].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					fMasUno = false;
				}
			}
			else
			{
				mebMascara[iIndiceCampoMascara].Width = (int) ((((Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]))) * ModuloMantenim.COiTamCaractEtiqueta) + ModuloMantenim.COiTamBordes) / 15);
				mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (Convert.ToInt32(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])));
				//Le pondremos 1 caracter para signo y otro para separaci�n de
				//decimales si la parte decimal<>0 si es num�rico
				if (Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "NUMERIC" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "SMALLINT" || Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim() == "INT")
				{
					mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
					mebMascara[iIndiceCampoMascara].Width = (int) (mebMascara[iIndiceCampoMascara].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					fMasUno = false;
					if (Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"]) != 0)
					{
						mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength = (mebMascara[iIndiceCampoMascara].MaskedEditBoxElement.TextBoxItem.MaxLength + 1);
						mebMascara[iIndiceCampoMascara].Width = (int) (mebMascara[iIndiceCampoMascara].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
					}

				}
			}

			//**********************CRIS*******************************************
			//Para todos aquellos campos que tengan 10 caracters o menos
			//le a�adiremos a su ancho un caracter m�s, porque est� medido con el
			//ancho de la X. La boolena fMasUno quiere decir que si esta a verdadero
			//es porque el campo no es num�rico, ya que si lo es no hace falta sumarle
			//un caracter mas
			if (Convert.ToDouble(Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["nparteentera"]) + Convert.ToDouble(ModuloMantenim.RsManten2.Tables[0].Rows[0]["npartedecima"])) <= 10 && fMasUno)
			{
				mebMascara[iIndiceCampoMascara].Width = (int) (mebMascara[iIndiceCampoMascara].Width + ModuloMantenim.COiTamCaractEtiqueta / 15);
			}
			//*********************************************************************
			LaConexion[iIndice].NombreControl = mebMascara[iIndiceCampoMascara];
			astrElConjunto[iIndice].NombreControl = mebMascara[iIndiceCampoMascara];
			iIndiceMascara = iIndiceCampoMascara;
			iIndiceCampoMascara++;

		}

		private void PintarCampoMascara()
		{
			int iValorTop = 0;

			if (lbEtiqueta.GetUpperBound(0) == 0)
			{
				iValorTop = ModuloMantenim.COiSuperiorCampo;
			}
			else
			{
				iValorTop = ModuloMantenim.COiSeparacion;
			}

			if (mebMascara.GetUpperBound(0) == 0)
			{
				mebMascara[0].Top = (int) ((iValorTop + viNuevoTop) / 15);
				mebMascara[0].Left = (int) (viNuevoLeft / 15);
				mebMascara[0].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (mebMascara[0].Top * 15)) + ((float) (mebMascara[0].Height * 15)));
				ModuloMantenim.AsignarPropiedadControl(mebMascara[0]);
			}
			else
			{

				mebMascara[iIndiceMascara].Top = (int) ((iValorTop + viNuevoTop) / 15);
				mebMascara[iIndiceMascara].Left = (int) (viNuevoLeft / 15);
				mebMascara[iIndiceMascara].Visible = true;
				//**************************CRIS**************************
				ControlArrayHelper.LoadControl(this, "Cbb_AlgoNada", iIndiceOption);
				Cbb_AlgoNada[iIndiceOption].Top = (int) ((iValorTop + viNuevoTop) / 15);
				iIndiceOption++;
				//********************************************************
				viNuevoTop = Convert.ToInt32(((float) (mebMascara[iIndiceMascara].Top * 15)) + ((float) (mebMascara[iIndiceMascara].Height * 15)));
				ModuloMantenim.AsignarPropiedadControl(mebMascara[iIndiceMascara]);
			}
			//***************************CRIS**************************
			//Call AlturaFrame(mebMascara(iIndiceMascara))
			//***************************CRIS**************************
		}
		private void CargarEstrucResultados(DataSet Resultado)
		{
			//Llena el array de controles con las caracter�sticas de cada campo
			//y las del control al que va a referenciar

			int iIndiceEstructura = 0;

			foreach (DataRow iteration_row in Resultado.Tables[0].Rows)
			{
				astrElConjunto[iIndiceEstructura].gtablamanaut = Convert.ToString(iteration_row["gtablamanaut"]).Trim();
				astrElConjunto[iIndiceEstructura].dtablamancor = Convert.ToString(iteration_row["dtablamancor"]).Trim();
				astrElConjunto[iIndiceEstructura].gcolumncaman = Convert.ToString(iteration_row["gcolumncaman"]).Trim();
				astrElConjunto[iIndiceEstructura].dcoltabmanco = Convert.ToString(iteration_row["dcoltabmanco"]).Trim();
				astrElConjunto[iIndiceEstructura].dcoltabmanla = Convert.ToString(iteration_row["dcoltabmanla"]).Trim();
				astrElConjunto[iIndiceEstructura].nordencoltab = Convert.ToInt16(Double.Parse(Convert.ToString(iteration_row["nordencoltab"]).Trim()));
				astrElConjunto[iIndiceEstructura].gtipodato = Convert.ToString(iteration_row["gtipodato"]).Trim();
				astrElConjunto[iIndiceEstructura].gtipocolumna = Convert.ToString(iteration_row["gtipocolumna"]).Trim();
				astrElConjunto[iIndiceEstructura].nordencolpk = Convert.ToString(iteration_row["nordencolpk"]).Trim();
				astrElConjunto[iIndiceEstructura].nparteentera = Convert.ToInt16(Double.Parse(Convert.ToString(iteration_row["nparteentera"]).Trim()));
				astrElConjunto[iIndiceEstructura].npartedecima = Convert.ToInt16(Double.Parse(Convert.ToString(iteration_row["npartedecima"]).Trim()));
				astrElConjunto[iIndiceEstructura].iadmitenull = Convert.ToString(iteration_row["iadmitenull"]).Trim();

				if (Convert.IsDBNull(iteration_row["valordefecto"]))
				{
					astrElConjunto[iIndiceEstructura].valordefecto = "";
				}
				else
				{
					astrElConjunto[iIndiceEstructura].valordefecto = Convert.ToString(iteration_row["valordefecto"]).Trim();
				}

				astrElConjunto[iIndiceEstructura].itamaniofijo = Convert.ToString(iteration_row["itamaniofijo"]).Trim();

				if (Convert.IsDBNull(iteration_row["vformato"]))
				{
					astrElConjunto[iIndiceEstructura].vformato = "";
				}
				else
				{
					astrElConjunto[iIndiceEstructura].vformato = Convert.ToString(iteration_row["vformato"]).Trim();
				}

				if (Convert.IsDBNull(iteration_row["vmascara"]))
				{
					astrElConjunto[iIndiceEstructura].vmascara = "";
				}
				else
				{
					astrElConjunto[iIndiceEstructura].vmascara = Convert.ToString(iteration_row["vmascara"]).Trim();
				}

				astrElConjunto[iIndiceEstructura].ipassword = Convert.ToString(iteration_row["ipassword"]).Trim();
				astrElConjunto[iIndiceEstructura].imayusculas = Convert.ToString(iteration_row["imayusculas"]).Trim();
				astrElConjunto[iIndiceEstructura].ireqconsulta = Convert.ToString(iteration_row["ireqconsulta"]).Trim();
				astrElConjunto[iIndiceEstructura].icolformato = Convert.ToString(iteration_row["icolformato"]).Trim();
				astrElConjunto[iIndiceEstructura].icolmascara = Convert.ToString(iteration_row["icolmascara"]).Trim();
				astrElConjunto[iIndiceEstructura].nordenacion = Convert.ToInt16(Double.Parse(Convert.ToString(iteration_row["nordenacion"]).Trim()));

				if (Convert.IsDBNull(iteration_row["iascendente"]))
				{
					astrElConjunto[iIndiceEstructura].iascendente = "";
				}
				else
				{
					astrElConjunto[iIndiceEstructura].iascendente = Convert.ToString(iteration_row["iascendente"]).Trim();
				}

				astrElConjunto[iIndiceEstructura].icolfborrado = Convert.ToString(iteration_row["icolfborrado"]).Trim();
				iIndiceEstructura++;
			}
		}

		private void CaracterizarIncorporar()
		{
			//Debemos poner los campos con las caracter�sticas de incorporaci�n
			Control MiControl = null;
			for (int iCaracterizar = 0; iCaracterizar <= astrElConjunto.GetUpperBound(0) - 1; iCaracterizar++)
			{

				//Comprobar si tiene valores por defecto en las incorporaciones
				if (astrElConjunto[iCaracterizar].valordefecto != "")
				{
					MiControl = astrElConjunto[iCaracterizar].NombreControl;
					MiControl.Text = astrElConjunto[iCaracterizar].valordefecto;
				}
				//Comprobar el tipo de dato
				if (astrElConjunto[iCaracterizar].gtipocolumna == "SA" || astrElConjunto[iCaracterizar].icolfborrado.ToString().ToUpper() == "S")
				{
					//Or Trim(UCase(astrElConjunto(iCaracterizar).gcolumncaman)) = Trim(UCase(StFBORRADO)) Then
					astrElConjunto[iCaracterizar].NombreControl.Enabled = false;
				}

			}

		}

		private void CaracterizarModificar(bool fInhabilitarFBORRADO = false)
		{
			//Debemos poner los campos con las caracter�sticas de modificaci�n

			for (int iCaracterizar = 0; iCaracterizar <= astrElConjunto.GetUpperBound(0) - 1; iCaracterizar++)
			{

				//Comprobar el tipo de dato
				if (astrElConjunto[iCaracterizar].gtipocolumna == "EN" || astrElConjunto[iCaracterizar].gtipocolumna == "SA")
				{
					astrElConjunto[iCaracterizar].NombreControl.Enabled = false;
				}
				//***************************************CRIS********************************************
				//Miramos si el control es el de fecha de borrado. Si lo es miramos si la variable
				//de inhabilitar fecha de borrado es true, y si lo es lo inhabilitamos, ya que esto
				//significa que la fecha de borrado es nula y solo se puede modificar mediante el borrado
				//    If Trim(UCase(astrElConjunto(iCaracterizar).gcolumncaman)) = Trim(UCase(StFBORRADO))
				if (astrElConjunto[iCaracterizar].icolfborrado.ToString().ToUpper() == "S" && astrElConjunto[iCaracterizar].icolfborrado != Type.Missing)
				{
					if (fInhabilitarFBORRADO)
					{
						astrElConjunto[iCaracterizar].NombreControl.Enabled = false;
					}
				}
				//****************************************************************************************
			}

		}


		private void Descaracterizar()
		{
			//Quitamos todos los atributos definidos anteriormente para los controles

			for (int iCaracterizar = 0; iCaracterizar <= astrElConjunto.GetUpperBound(0) - 1; iCaracterizar++)
			{

				if (!astrElConjunto[iCaracterizar].NombreControl.Enabled)
				{
					if (astrElConjunto[iCaracterizar].NombreControl.Name != "cbbCombo")
					{
						astrElConjunto[iCaracterizar].NombreControl.Enabled = true;
					}
					else
					{
						AStrToolbar[ControlArrayHelper.GetControlIndex(astrElConjunto[iCaracterizar].NombreControl)].NombreControl.Enabled = true;
					}
				}

			}

		}

		public void SumarSpread()
		{
			//Le sumamos el �ltimo registro incorporado
			int iIndiceSuma = 0;
			Control[] MiControl = null;

			//Habilitamos un registro m�s en la grid
			sprMultiple.MaxRows++;
			sprMultiple.Row = sprMultiple.MaxRows;

			iindice2 = 0;

			//Bucle For para cargar el �ltimo registro de la grid
			for (iIndiceSuma = 0; iIndiceSuma <= (sprMultiple.MaxCols - 1); iIndiceSuma++)
			{
				sprMultiple.Col = iIndiceSuma + 1;
				if (viColumnaPassword == sprMultiple.Col)
				{
					this.sprMultiple.setTypeEditPassword(true);
				}
				MiControl = new Control[iIndiceSuma + 1];
				MiControl[iIndiceSuma] = LaConexion[iIndiceSuma].NombreControl;

				//Recorrer los campos e ir introduciendo los datos en la grid
				if (Convert.IsDBNull(LaConexion[iIndiceSuma].NombreControl.Text))
				{
					sprMultiple.Text = "";
				}
				else
				{
					//*******************************CRIS***********************************
					if (LaConexion[iIndiceSuma].NombreControl is RadMaskedEditBox)
					{
						sprMultiple.Text = Convert.ToString(LaConexion[iIndiceSuma].NombreControl.FormattedText());
					}
					else
					{
						sprMultiple.Text = Convert.ToString(LaConexion[iIndiceSuma].NombreControl.Text);
					}
					//**********************************************************************
				}

			}

			for (iindice2 = 0; iindice2 <= (iIndiceSuma - 1); iindice2++)
			{
				MiControl[iindice2] = null;
			}

			iindice2 = 0;

		}

		private void GuardarCambios()
		{

			//Se guardan los cambios pendientes antes de cerrar

			switch(stPulsado)
			{
				case "incorporar" : 
					cbBotones_Click(cbBotones[0], new EventArgs());  //Aceptar Incorporar 
					 
					break;
				case "modificar" : 
					cbBotones_Click(cbBotones[0], new EventArgs());  //Aceptar Modificar 
					 
					break;
			}

		}




		private void ComprobarModoTotal()
		{
			//Procedimiento que nos comprueba si es una tabla de s�lo consulta o
			//de acceso total a Incorporar, Modificar y Eliminar.

			if (vstActualizable == "S")
			{
				//Puede acceder a todo
				return;
			}
			else
			{
				cbSpread[0].Visible = false;
				cbSpread[1].Visible = false;
				cbSpread[2].Visible = false;
			}

		}



		private void tbTexto_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			int Index = Array.IndexOf(this.tbTexto, eventSender);
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].NombreControl.Name == "tbTexto" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
				{
					if (astrElConjunto[i].imayusculas == "S")
					{
						Serrores.ValidarTecla(ref KeyAscii);
						KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]);
					}
					else if (astrElConjunto[i].icolformato == "S")
					{ 
						if (KeyAscii != 8 && KeyAscii != 9 && KeyAscii != 10 && KeyAscii != 13)
						{ //retroceso, tabulador,
							//avance de l�nea y retorno de carro
							ValidarFormato(KeyAscii, astrElConjunto[i].dcoltabmanco, astrElConjunto[i].gtipodato);
							if (!FFormatoValido)
							{
								KeyAscii = 13;
							}
						}
					}
					else if (astrElConjunto[i].icolmascara == "S")
					{ 
						if (KeyAscii != 8 && KeyAscii != 9 && KeyAscii != 10 && KeyAscii != 13)
						{ //retroceso, tabulador,
							//avance de l�nea y retorno de carro
							ValidarMascara(KeyAscii, astrElConjunto[i].dcoltabmanco, astrElConjunto[i].gtipodato);
							if (!FMascaraValida)
							{
								KeyAscii = 13;
							}
						}
					}

					break;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbTexto_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.tbTexto, eventSender);
			int IposiNegativo = 0;
			int IposiPositivo = 0;

			bool FEncriptar = true;

			if (this.ActiveControl.Name == cbBotones[1].Name)
			{
				if (!(ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[1]) && ControlArrayHelper.GetControlIndex(this.ActiveControl) == ControlArrayHelper.GetControlIndex(cbBotones[2])) && tbTexto[Index].Text.Trim() != "" && stPulsado != "consultar")
				{ //Si no pulsa cancelar, ni limpiar
					//el campo no est� vacio y el bot�n pulsado no es el de consultar
					//se valida el tama�o fijo
					for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].NombreControl.Name == "tbTexto" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
						{
							if (astrElConjunto[i].itamaniofijo == "S")
							{
								if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
								{
									IposiNegativo = (tbTexto[Index].Text.IndexOf('-') + 1);
									IposiPositivo = (tbTexto[Index].Text.IndexOf('+') + 1);
									if (IposiNegativo != 0 || IposiPositivo != 0)
									{
										FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length + 1, i);
									}
									else
									{
										FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length, i);
									}
								}
								else
								{
									FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length, i);
								}
							}
							break;
						}
					}

					if (FEncriptar)
					{
						//Si ha habido cambios en el campo password encriptamos.
						if (vfCambiosPassword)
						{
							//03/06/2004
							if (tbTexto[Index].Text.Trim().Length < ModuloMantenim.LongitudMinimaPassword())
							{
								short tempRefParam = 1020;
								string[] tempRefParam2 = new string[]{"Longitud de contrase�a", "mayor o igual", ModuloMantenim.LongitudMinimaPassword().ToString()};
								ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
								tbTexto[Index].Focus();
								return;
							}
							//----------
							string tempRefParam3 = tbTexto[Index].Text;
							tbTexto[Index].Text = ModuloMantenim.CambiarContrase�a(ref tempRefParam3);
							vstContrase�a = tbTexto[Index].Text;
						}
					}

				}
			}
			else if (tbTexto[Index].Text.Trim() != "" && stPulsado != "consultar")
			{  //Si no pulsa cancelar, ni limpiar
				//el campo no est� vacio y el bot�n pulsado no es el de consultar
				//se valida el tama�o fijo
				for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
				{
					if (astrElConjunto[i].NombreControl.Name == "tbTexto" && ControlArrayHelper.GetControlIndex(astrElConjunto[i].NombreControl) == Index)
					{
						if (astrElConjunto[i].itamaniofijo == "S")
						{
							if (astrElConjunto[i].gtipodato.Trim() == "NUMERIC" || astrElConjunto[i].gtipodato.Trim() == "SMALLINT" || astrElConjunto[i].gtipodato.Trim() == "INT")
							{
								IposiNegativo = (tbTexto[Index].Text.IndexOf('-') + 1);
								IposiPositivo = (tbTexto[Index].Text.IndexOf('+') + 1);
								if (IposiNegativo != 0 || IposiPositivo != 0)
								{
									FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length + 1, i);
								}
								else
								{
									FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length, i);
								}
							}
							else
							{
								FEncriptar = ValidarTama�oFijo(tbTexto[Index].Text.Trim().Length, i);
							}
						}
						break;
					}
				}

				if (FEncriptar)
				{
					//Si ha habido cambios en el campo password encriptamos.
					if (vfCambiosPassword)
					{
						//03/06/2004
						//oscar
						//Comprobamos que el campo nueva contrase�a tenga como minimo el numero de caracteres especificado
						//en la constante global CONTLMIN.
						if (tbTexto[Index].Text.Trim().Length < ModuloMantenim.LongitudMinimaPassword())
						{
							short tempRefParam4 = 1020;
							string[] tempRefParam5 = new string[]{"Longitud de contrase�a", "mayor o igual", ModuloMantenim.LongitudMinimaPassword().ToString()};
							ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, ModuloMantenim.RcManten, tempRefParam5);
							tbTexto[Index].Focus();
							return;
						}
						//----------
						string tempRefParam6 = tbTexto[Index].Text;
						tbTexto[Index].Text = ModuloMantenim.CambiarContrase�a(ref tempRefParam6);
						vstContrase�a = tbTexto[Index].Text;
					}
				}

			}

		}


		private void ObtenerFocoCombo(ModuloMantenim.StrFKCombo[] AStColFKCombo)
		{
			Control MiControl = null;
			bool primero = false;

			for (int iIndiceControl = 0; iIndiceControl <= astrElConjunto.GetUpperBound(0) - 1; iIndiceControl++)
			{

				MiControl = astrElConjunto[iIndiceControl].NombreControl;
				if (UpgradeHelpers.Gui.ControlHelper.GetEnabled(MiControl) && !primero && UpgradeHelpers.Gui.ControlHelper.GetVisible(MiControl))
				{
					MiControl.Focus();
					primero = true;
				}
				if (MiControl.TabIndex == ModuloMantenim.COiDesplazamTab && UpgradeHelpers.Gui.ControlHelper.GetEnabled(MiControl))
				{

					MiControl = null;
					return;
				}
				MiControl = null;

			}

		}



		public void BuscarFK()
		{
			string StComando2 = String.Empty;
			StringBuilder StOrden = new StringBuilder();
			DataSet RsCombos2 = null;
			bool fDimensionar = false, fDimensionarCombo = false;
			int iLongitudColumna = 0;


			ModuloMantenim.StrFK[] AStColFK = null;

			ModuloMantenim.StrOrdenFK[] AStOrdenFK = null;

			int iIndiceCombo = 0;

			//Obtengo los grupos de columnas de FK
			string StComando = "SELECT * FROM SCOLFKPKV1 " + "WHERE GTABCAMANHIJ= " + "'" + vstNombreTabla.Trim() + "'" + "ORDER BY gfkcaman,nordencolfk ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsCombos = new DataSet();
			tempAdapter.Fill(RsCombos);

			//Obtenemos las columnas FK que se van a sustituir

			string StNombreFK = Convert.ToString(RsCombos.Tables[0].Rows[0]["GFKCAMAN"]).Trim();

			ModuloMantenim.StrFKCombo[] AStColFKCombo = ArraysHelper.InitializeArray<ModuloMantenim.StrFKCombo>(1);

			while (RsCombos.Tables[0].Rows.Count != 0)
			{

				AStColFK = new ModuloMantenim.StrFK[1];
				fDimensionar = false;

				//Mientras la columna pertenezca al mismo grupo
				//de valores de FK, vamos a�adiendo sus datos a un array
				//de estructura
				foreach (DataRow iteration_row in RsCombos.Tables[0].Rows)
				{

					if (StNombreFK.Trim() == Convert.ToString(iteration_row["GFKCAMAN"]).Trim())
					{
						if (fDimensionar)
						{
							AStColFK = ArraysHelper.RedimPreserve(AStColFK, new int[]{AStColFK.GetUpperBound(0) + 2});
						}

						fDimensionar = true;

						AStColFK[AStColFK.GetUpperBound(0)].StColHija = Convert.ToString(iteration_row["GCOLCAMANHIJ"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StColPadre = Convert.ToString(iteration_row["GCOLUMNCAMAN"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre = Convert.ToString(iteration_row["GTABCAMANPAD"]).Trim();
						AStColFK[AStColFK.GetUpperBound(0)].StLabelCombo = Convert.ToString(iteration_row["VLABELCOMBO"]).Trim();
						if (Convert.IsDBNull(iteration_row["GVISTACOMBO"]))
						{
							AStColFK[AStColFK.GetUpperBound(0)].fVistaNula = true;
						}
						else
						{
							AStColFK[AStColFK.GetUpperBound(0)].fVistaNula = false;
							AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo = Convert.ToString(iteration_row["GVISTACOMBO"]).Trim();
						}

						//Guardo el tipo de dato de cada columna
						for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
						{
							if (AStColFK[AStColFK.GetUpperBound(0)].StColHija.Trim() == astrElConjunto[i].gcolumncaman.Trim())
							{
								AStColFK[AStColFK.GetUpperBound(0)].stTipoDato = astrElConjunto[i].gtipodato.Trim();
								break;
							}
						}

					}
					else
					{
						//Si pasa por aqu�, significa que ya se
						//ha cambiado de grupo de columnas FK,
						//con lo que sadremos del Do While
						//y llenaremos el combo con los datos correspondientes
						//al anterior grupo de columnas FK
						break;
					}
				}


				//Si la vista est� a null, los datos se sacan de la tabla
				//padre
				if (AStColFK[AStColFK.GetUpperBound(0)].fVistaNula)
				{
					StComando = "SELECT * from SPREVICMV1 " + "Where gtablacaman = " + "'" + AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre.Trim() + "'" + " AND IVISIBCOMTAB='S' " + "AND ISNULL(gvistacaman,'NULL') = 'NULL' " + "order by NORDENCOLTAB ";

					//Una de las condiciones de la select deber�a ser:
					//AND GVISTACAMAN IS NULL, pero esto no funciona con esta vista
					//y hemos tenido que sustituirlo por:
					//ISNULL(gvistacaman,'NULL') = 'NULL'
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombos2 = new DataSet();
					tempAdapter_2.Fill(RsCombos2);


					//Montamos la select de las columnas que van a ir en el combo

					StComando = "";
					AStOrdenFK = new ModuloMantenim.StrOrdenFK[1];
					iLongitudColumna = 0;
					foreach (DataRow iteration_row_2 in RsCombos2.Tables[0].Rows)
					{
						StComando = StComando + "A." + Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim() + ",";

						//Calculamos la longitud m�xima que va a tener el campo en la spread
						//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
						//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
						//posici�n para el punto decimal

						if (Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row_2["GTIPODATO"]).Trim() == "INT")
						{
							if (Convert.ToDouble(iteration_row_2["npartedecima"]) != 0)
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]) + 2);
							}
							else
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]) + 1);
							}
						}
						else
						{
							iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_2["nparteentera"]) + Convert.ToDouble(iteration_row_2["npartedecima"]));
						}

						//Nos creamos un array para guardar el orden de columnas
						//y si �stas son ascendentes o no
						if (Convert.ToDouble(iteration_row_2["NORDENACTAB"]) != 0)
						{
							if (Convert.ToDouble(iteration_row_2["NORDENACTAB"]) > AStOrdenFK.GetUpperBound(0))
							{
								AStOrdenFK = ArraysHelper.RedimPreserve(AStOrdenFK, new int[]{Convert.ToInt32(iteration_row_2["NORDENACTAB"]) + 1});
							}
							AStOrdenFK[Convert.ToInt32(iteration_row_2["NORDENACTAB"])].StAscendente = Convert.ToString(iteration_row_2["IASCENDETAB"]).Trim();
							AStOrdenFK[Convert.ToInt32(iteration_row_2["NORDENACTAB"])].StColumna = Convert.ToString(iteration_row_2["GCOLUMNCAMAN"]).Trim();
						}
					}

					RsCombos2.Close();

					//Quitamos la �ltima coma
					StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
					StComando = StComando + "from " + AStColFK[AStColFK.GetUpperBound(0)].StTablaPadre.Trim() + " A";

					//Montamos la sentencia ORDER BY con el array
					//que nos hemos cargado anteriormente
					StComando2 = "";
					StOrden = new StringBuilder("");
					if (AStOrdenFK.GetUpperBound(0) != 0)
					{
						for (int j = 1; j <= AStOrdenFK.GetUpperBound(0); j++)
						{
							StOrden.Append("A." + AStOrdenFK[j].StColumna);
							if (AStOrdenFK[j].StAscendente.Trim() == "N")
							{
								StOrden.Append(" DESC");
							}
							StOrden.Append(",");
						}
						StComando2 = StComando2 + " order by " + StOrden.ToString() + " ";
					}


					//Sustituimos las columnas en la spread y llenamos el combo
					SustituirEnSpreadFK(StComando, StComando2, iLongitudColumna, fDimensionarCombo, AStColFKCombo, AStColFK, iIndiceCombo);

				}
				else
				{
					//Si la vista no esta a null, los datos se sacan de la vista
					StComando = "SELECT * from SPREVICMV1 " + "Where gvistacaman = " + "'" + AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo.Trim() + "'" + " AND IVISIBCOMVIS='S' " + "order by NORDENVISTA ";

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
					RsCombos2 = new DataSet();
					tempAdapter_3.Fill(RsCombos2);

					//Montamos la select de las columnas que van a ir en el combo

					StComando = "";
					AStOrdenFK = new ModuloMantenim.StrOrdenFK[1];
					iLongitudColumna = 0;
					foreach (DataRow iteration_row_3 in RsCombos2.Tables[0].Rows)
					{
						StComando = StComando + "A." + Convert.ToString(iteration_row_3["GCOLUMNCAMAN"]).Trim() + ",";

						//Calculamos la longitud m�xima que va a tener el campo en la spread
						//Miramos si el tipo de dato es num�rico. Si lo es le a�adimos una posici�n mas
						//para el signo, y si tiene decimales (parte decimal <> 0), le a�adimos otra posici�n
						//posici�n para el punto decimal

						//Adem�s le a�adimos siempre uno mas de longitud, ya que
						//los campos ir�n separados por espacios
						if (Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "NUMERIC" || Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "SMALLINT" || Convert.ToString(iteration_row_3["GTIPODATO"]).Trim() == "INT")
						{
							if (Convert.ToDouble(iteration_row_3["npartedecima"]) != 0)
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 2 + 1);
							}
							else
							{
								iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 1 + 1);
							}
						}
						else
						{
							iLongitudColumna = Convert.ToInt32(iLongitudColumna + Convert.ToDouble(iteration_row_3["nparteentera"]) + Convert.ToDouble(iteration_row_3["npartedecima"]) + 1);
						}

						//Nos creamos un array para guardar el orden de columnas
						//y si �stas son ascendentes o no
						if (Convert.ToDouble(iteration_row_3["NORDENACVIS"]) != 0)
						{
							if (Convert.ToDouble(iteration_row_3["NORDENACVIS"]) > AStOrdenFK.GetUpperBound(0))
							{
								AStOrdenFK = ArraysHelper.RedimPreserve(AStOrdenFK, new int[]{Convert.ToInt32(iteration_row_3["NORDENACVIS"]) + 1});
							}
							AStOrdenFK[Convert.ToInt32(iteration_row_3["NORDENACVIS"])].StAscendente = Convert.ToString(iteration_row_3["IASCENDEVIS"]).Trim();
							AStOrdenFK[Convert.ToInt32(iteration_row_3["NORDENACVIS"])].StColumna = Convert.ToString(iteration_row_3["GCOLUMNCAMAN"]).Trim();
						}
					}

					RsCombos2.Close();

					//Quitamos la �ltima coma
					StComando = StComando.Substring(0, Math.Min(StComando.Length - 1, StComando.Length)) + " ";
					StComando = StComando + "from " + AStColFK[AStColFK.GetUpperBound(0)].StVistaCombo.Trim() + " A";

					//Montamos la sentencia ORDER BY con el array
					//que nos hemos cargado anteriormente
					StOrden = new StringBuilder("");
					StComando2 = "";
					if (AStOrdenFK.GetUpperBound(0) != 0)
					{
						for (int j = 1; j <= AStOrdenFK.GetUpperBound(0); j++)
						{
							StOrden.Append("A." + AStOrdenFK[j].StColumna);
							if (AStOrdenFK[j].StAscendente.Trim() == "N")
							{
								StOrden.Append(" DESC");
							}
							StOrden.Append(",");
						}
						//Quitamos la �ltima coma
						StOrden = new StringBuilder(StOrden.ToString().Substring(0, Math.Min(StOrden.ToString().Length - 1, StOrden.ToString().Length)) + " ");
						StComando2 = " order by " + StOrden.ToString();
					}


					//Sustituimos las columnas en la spread y llenamos el combo
					SustituirEnSpreadFK(StComando, StComando2, iLongitudColumna, fDimensionarCombo, AStColFKCombo, AStColFK, iIndiceCombo);

				}

				//Guardo el nombre del nuevo grupo de columnas FK
				if (RsCombos.Tables[0].Rows.Count != 0)
				{
					StNombreFK = Convert.ToString(RsCombos.Tables[0].Rows[0]["GFKCAMAN"]).Trim();
				}

			}

			RsCombos.Close();
		}


		private void BuscarUnicos()
		{
			//************************************************************************
			//Busca todas las columnas de valores �nicos de la tabla y establece en
			//el array de controles sus propiedades

			string StComando = "Select GGRUNIMANAUT,GCOLUMNCAMAN,NORDCOLINUN " + "from SCOGRMTBV1 " + "where GTABLACAMAN = " + "'" + vstNombreTabla.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, ModuloMantenim.RcManten);
			DataSet RsUnico = new DataSet();
			tempAdapter.Fill(RsUnico);

			foreach (DataRow iteration_row in RsUnico.Tables[0].Rows)
			{
				for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
				{
					if (astrElConjunto[i].gcolumncaman.Trim() == Convert.ToString(iteration_row["GCOLUMNCAMAN"]).Trim())
					{
						astrElConjunto[i].ggrunimanaut = Convert.ToString(iteration_row["GGRUNIMANAUT"]).Trim();
						astrElConjunto[i].nordcolinun = (short) iteration_row["NORDCOLINUN"];
						break;
					}
				}
			}

			RsUnico.Close();
			fUnicos = true;
			//************************************************************************
		}

		public void ActualizarFechaBorradoSpread()
		{
			//Una vez eliminada l�gicamente una fila, hacemos que aparezca
			//la fecha del sistema en la casilla de la spread correspondiente
			//a la fecha de borrado
			sprMultiple.Row = sprMultiple.ActiveRowIndex;
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].icolfborrado.ToString().ToUpper() == "S")
				{
					sprMultiple.Col = astrElConjunto[i].nordencoltab;
					sprMultiple.Text = DateTime.Today.ToString("dd/MM/yyyy");
					break;
				}
			}
		}


		public void BuscarPK(bool Parainsert = false)
		{
			//Busca todas las PK de la tabla y sus caracter�sticas
			bool FArrayCreado = false;

			ModuloMantenim.iIndiceBuscar = 0;
			AStPK = new ModuloMantenim.Clave[1];
			sprMultiple.Row = sprMultiple.ActiveRowIndex;
			for (ModuloMantenim.iIndiceBuscar = 0; ModuloMantenim.iIndiceBuscar <= astrElConjunto.GetUpperBound(0) - 1; ModuloMantenim.iIndiceBuscar++)
			{
				if (StringsHelper.ToDoubleSafe(astrElConjunto[ModuloMantenim.iIndiceBuscar].nordencolpk) != 0)
				{
					if (FArrayCreado)
					{
						AStPK = ArraysHelper.RedimPreserve(AStPK, new int[]{AStPK.GetUpperBound(0) + 2});
					}
					AStPK[AStPK.GetUpperBound(0)].StColumna = astrElConjunto[ModuloMantenim.iIndiceBuscar].gcolumncaman;
					AStPK[AStPK.GetUpperBound(0)].StTipo = astrElConjunto[ModuloMantenim.iIndiceBuscar].gtipodato;
					sprMultiple.Col = astrElConjunto[ModuloMantenim.iIndiceBuscar].nordencoltab;
					if (!Convert.IsDBNull(Parainsert))
					{
						//SI ES "CHAR", "VARCHAR" O "DATETIME",
						AStPK[AStPK.GetUpperBound(0)].stTexto = Convert.ToString(astrElConjunto[ModuloMantenim.iIndiceBuscar].NombreControl.Text);
					}
					else
					{
						AStPK[AStPK.GetUpperBound(0)].stTexto = sprMultiple.Text.Trim();
					}
					AStPK[AStPK.GetUpperBound(0)].iOrdenColumnaSpread = astrElConjunto[ModuloMantenim.iIndiceBuscar].nordencoltab;
					//***********************CRIS***********************************

					switch(astrElConjunto[ModuloMantenim.iIndiceBuscar].gtipodato.Trim())
					{
						case "NUMERIC" :  //Un espacio m�s para el signo 
							AStPK[AStPK.GetUpperBound(0)].iLongitud = (short) (astrElConjunto[ModuloMantenim.iIndiceBuscar].nparteentera + astrElConjunto[ModuloMantenim.iIndiceBuscar].npartedecima + 1); 
							//Un espacio m�s para el punto decimal 
							if (astrElConjunto[ModuloMantenim.iIndiceBuscar].npartedecima != 0)
							{
								AStPK[AStPK.GetUpperBound(0)].iLongitud = (short) (AStPK[AStPK.GetUpperBound(0)].iLongitud + 1);
							} 
							break;
						case "INT" : case "SMALLINT" :  //Un espacio m�s para el signo 
							AStPK[AStPK.GetUpperBound(0)].iLongitud = (short) (astrElConjunto[ModuloMantenim.iIndiceBuscar].nparteentera + astrElConjunto[ModuloMantenim.iIndiceBuscar].npartedecima + 1); 
							break;
						case "CHAR" : case "VARCHAR" : case "DATETIME" : 
							AStPK[AStPK.GetUpperBound(0)].iLongitud = (short) (astrElConjunto[ModuloMantenim.iIndiceBuscar].nparteentera + astrElConjunto[ModuloMantenim.iIndiceBuscar].npartedecima); 
							 
							break;
					}
					//********************************************************************************************************
					FArrayCreado = true;
				}
			}
		}

		private bool ComprobarFechas(string stCampo)
		{

			for (int iIndiceComprobacion = 0; iIndiceComprobacion <= astrElConjunto.GetUpperBound(0); iIndiceComprobacion++)
			{
				if (astrElConjunto[iIndiceComprobacion].gcolumncaman == stCampo)
				{
					return astrElConjunto[iIndiceComprobacion].gtipodato == "DATETIME";
				}
			}
			return false;
		}



		private bool ValidarRequeridoConsultaCombo(ModuloMantenim.StrFKCombo[] AStColFKCombo)
		{
			//Rutina donde tenemos que comprobar si hay campos
			//requeridos en la consulta, sac�ndolo de la estructura.
			//>>>>>>>>>ireqconsulta
			Control MiControl = null;

			for (int iIndiceControl = 0; iIndiceControl <= astrElConjunto.GetUpperBound(0) - 1; iIndiceControl++)
			{

				MiControl = astrElConjunto[iIndiceControl].NombreControl;
				if (Convert.ToString(MiControl.Text).Trim() == "" && astrElConjunto[iIndiceControl].ireqconsulta == "S")
				{
					ModuloMantenim.ActivarCampos(this, astrElConjunto, AStrToolbar);
					if (UpgradeHelpers.Gui.ControlHelper.GetVisible(MiControl))
					{
						short tempRefParam = 1040;
						string[] tempRefParam2 = new string[]{astrElConjunto[iIndiceControl].dcoltabmanco};
						ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
						MiControl.Focus();
					}
					else
					{
						ModuloMantenim.DarFoco(astrElConjunto[iIndiceControl].gcolumncaman, AStColFKCombo, true, this);
					}
					MiControl = null;
					return false;
				}
				MiControl = null;
			}
			return true;
		}


		private int ComprobarCamposPassword()
		{
			int result = 0;

			for (int iIndiceEstructura = 0; iIndiceEstructura <= astrElConjunto.GetUpperBound(0) - 1; iIndiceEstructura++)
			{
				if (astrElConjunto[iIndiceEstructura].ipassword == "S")
				{
					for (int iIndiceColumna = 0; iIndiceColumna <= LaConexion.GetUpperBound(0) - 1; iIndiceColumna++)
					{
						if (LaConexion[iIndiceColumna].NombreCampo == astrElConjunto[iIndiceEstructura].gcolumncaman)
						{
							result = LaConexion[iIndiceColumna].NumeroColumna;
						}
					}
				}

			}

			return result;
		}


		public bool ComprobarNumericos(string stCampo)
		{
			//S�lo comprobamos que el campo es un n�mero o no

			for (int iIndiceComprobacion = 0; iIndiceComprobacion <= astrElConjunto.GetUpperBound(0) - 1; iIndiceComprobacion++)
			{
				if (astrElConjunto[iIndiceComprobacion].gcolumncaman == stCampo)
				{
					return astrElConjunto[iIndiceComprobacion].gtipodato == "NUMERIC" || astrElConjunto[iIndiceComprobacion].gtipodato == "DECIMAL" || astrElConjunto[iIndiceComprobacion].gtipodato == "INT" || astrElConjunto[iIndiceComprobacion].gtipodato == "SMALLINT";
				}
			}

			return false;
		}

		public string ObtenerColumnaFBorrado()
		{
			//Buscamos el nombre de la columna que sea Fecha de Borrado:
			//ICOLFBORRADO="S"
			string result = String.Empty;
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].icolfborrado.ToString().ToUpper() == "S")
				{
					result = astrElConjunto[i].gcolumncaman.Trim();
					break;
				}
			}
			return result;
		}


		private bool ValidarValoresUnicos(bool? fModificar_optional = null, bool? fVieneDeCombos_optional = null)
		{
			bool fModificar = (fModificar_optional == null) ? false : fModificar_optional.Value;
			bool fVieneDeCombos = (fVieneDeCombos_optional == null) ? false : fVieneDeCombos_optional.Value;
			//Si la tabla tiene valores �nicos, se valida si existe algun registro
			//con los valores �nicos iguales a los que quiero insertar

			bool result = false;
			DataSet RsUnico = null;
			StringBuilder StComando = new StringBuilder();
			bool fHayValoresUnicos = false;
			StringBuilder StUnicos = new StringBuilder();
			string StMensaje = String.Empty;
			StringBuilder StSelectPK = new StringBuilder();

			//Monto lo que le voy a tener que a�adir al where para que, si
			//lo que estoy haciendo es modificar, compruebe que no hay valores �nicos,
			//pero para otra fila que no sea la que estoy modificando

			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{
				if (i == 0)
				{
					StSelectPK.Append(" AND (");
				}
				else
				{
					StSelectPK.Append(" OR ");
				}

				StSelectPK.Append(AStPK[i].StColumna.Trim() + "<>");
				switch(AStPK[i].StTipo.Trim())
				{
					case "NUMERIC" : case "INT" : case "SMALLINT" : 
						StSelectPK.Append(AStPK[i].stTexto); 
						break;
					case "CHAR" : case "VARCHAR" : 
						StSelectPK.Append("'" + AStPK[i].stTexto.Trim() + "'"); 
						break;
					case "DATETIME" : 
						System.DateTime TempDate = DateTime.FromOADate(0); 
						StSelectPK.Append("'" + ((DateTime.TryParse(AStPK[i].stTexto, out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : AStPK[i].stTexto) + "'"); 
						break;
				}
			}

			StSelectPK.Append(")");

			result = true;

			//astrElConjunto(j).fEsta nos indica si el campo est� ya
			//en el conjunto de grupos de valores �nicos
			//Inicializo la booleana de todo el array a falso
			for (int j = 0; j <= astrElConjunto.GetUpperBound(0) - 1; j++)
			{
				astrElConjunto[j].fEsta = false;
			}

			//Recorre el array buscando las columnas que tengan valores �nicos
			//Cuando encuentra una, recorre otra vez el array para ver
			//si hay alguna columna m�s que sea de valor �nico y pertenezca
			//a su grupo, y cuando ya no hay ninguna m�s del mismo grupo,
			//lanza el resulset.
			//Si encuentra que existe en la tabla alg�n registro con el grupo
			//de valores �nicos repetidos, lanza un mensaje de error y ya no
			//sigue buscando si existen otros grupos de valores �nicos.
			//Si no encuentra ning�n registro, comprueba si hay otro grupo de
			//valores �nicos y repite la operaci�n
			for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
			{
				if (astrElConjunto[i].nordcolinun != 0 && !astrElConjunto[i].fEsta)
				{
					//astrElConjunto(i).fEsta nos indica si el campo est� ya
					//en el conjunto de grupos de valores �nicos

					StComando = new StringBuilder("select * from " + vstNombreTabla + " where ");

					//Recorro el array en busca de otras columnas que
					//pertenezcan al mismo grupo de valores �nicos y la
					//a�ado al where
					for (int j = 0; j <= astrElConjunto.GetUpperBound(0) - 1; j++)
					{
						if (astrElConjunto[i].ggrunimanaut.Trim() == astrElConjunto[j].ggrunimanaut.Trim())
						{

							astrElConjunto[i].fEsta = true;
							astrElConjunto[j].fEsta = true;

							//Compruebo si ya ha habido anteriormente alguna columna
							//perteneciente a este grupo de valores �nicos
							if (fHayValoresUnicos)
							{
								StComando.Append(" AND ");
							}
							fHayValoresUnicos = true;

							//SI EL CAMPO ES DATETIME, NO SE TENDR� EN CUENTA LA HORA.
							//SE CONVIERTE LA FECHA-HORA A FECHA FORMATEADA A "DD/MM/YYYY"
							if (astrElConjunto[j].gtipodato.Trim() == "DATETIME")
							{
								StComando.Append("CONVERT(char(10)," + astrElConjunto[j].gcolumncaman + ",103)");
							}
							else
							{
								StComando.Append(astrElConjunto[j].gcolumncaman);
							}

							if (Convert.ToString(astrElConjunto[j].NombreControl.Text).Trim() != "")
							{

								StComando.Append("=");

								//SI ES "CHAR", "VARCHAR" O "DATETIME",
								//EL DATO VA ENTRE COMILLA SIMPLE
								if (astrElConjunto[j].gtipodato.Trim() == "VARCHAR" || astrElConjunto[j].gtipodato.Trim() == "CHAR" || astrElConjunto[j].gtipodato.Trim() == "DATETIME")
								{
									StComando.Append("'");
								}

								//SI EL CONTROL ES M�SCARA, HABR� QUE COGER EL FORMATTEDTEXT
								//EN VEZ DEL TEXT PARA QUE COJA EL PUNTO EN LOS DECIMALES
								if (astrElConjunto[j].NombreControl.Name == "mebMascara")
								{
									StComando.Append(Convert.ToString(astrElConjunto[j].NombreControl.FormattedText()).Trim());
								}
								else
								{
									StComando.Append(Convert.ToString(astrElConjunto[j].NombreControl.Text).Trim());
								}

								//SI ES "CHAR", "VARCHAR" O "DATETIME",
								//EL DATO VA ENTRE COMILLA SIMPLE
								if (astrElConjunto[j].gtipodato.Trim() == "VARCHAR" || astrElConjunto[j].gtipodato.Trim() == "CHAR" || astrElConjunto[j].gtipodato.Trim() == "DATETIME")
								{
									StComando.Append("'");
								}

							}
							else
							{
								//SI EL CAMPO EST� EN BLANCO, SE COMPARA CON NULL
								StComando.Append(" IS NULL ");
							}
						}
					}

					//**********************************************************************
					//        If fModificar Then
					//            If fHayValoresUnicos Then
					//                StComando = StComando & " AND "
					//            End If
					//
					//        End If
					//**********************************************************************
					if (fModificar_optional != null)
					{
						if (fModificar)
						{
							StComando.Append(StSelectPK.ToString());
						}
					}
					if (fHayValoresUnicos)
					{
						//Si hemos encontrado algun grupo de valores �nicos
						//Lanzamos el resulset y vemos si hay algun registro igual en la
						//tabla
						SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
						RsUnico = new DataSet();
						tempAdapter.Fill(RsUnico);
						if ((RsUnico.Tables[0].Rows.Count != 0 && !((fModificar_optional != null) ? fModificar : false)) || (RsUnico.Tables[0].Rows.Count > 1 && ((fModificar_optional != null) ? fModificar : false)))
						{

							result = false;

							//Montamos el mensaje de error con el nombre del grupo
							//y el de las columnas que pertenecen al grupo de valores
							//�nicos
							StUnicos = new StringBuilder("");
							for (int j = 0; j <= astrElConjunto.GetUpperBound(0) - 1; j++)
							{
								if (astrElConjunto[i].ggrunimanaut.Trim() == astrElConjunto[j].ggrunimanaut.Trim())
								{
									if (!astrElConjunto[j].NombreControl.Visible && fVieneDeCombos_optional != null)
									{
										foreach (ModuloMantenim.StrFKUnicos AStrFKUnicos_item in AStrFKUnicos)
										{
											if (astrElConjunto[j].gcolumncaman.Trim() == AStrFKUnicos_item.StColHija.Trim())
											{
												if (StUnicos.ToString().Trim() != "")
												{
													StUnicos.Append(", ");
												}
												StUnicos.Append(AStrFKUnicos_item.StLabelCombo.Trim());
												break;
											}
										}
									}
									else
									{
										if (StUnicos.ToString().Trim() != "")
										{
											StUnicos.Append(", ");
										}
										StUnicos.Append(astrElConjunto[j].dcoltabmanco.Trim());
									}
								}
							}

							StMensaje = "Grupo de valores �nicos " + astrElConjunto[i].ggrunimanaut + " formado por " + StUnicos.ToString();
							short tempRefParam = 1080;
							string[] tempRefParam2 = new string[]{StMensaje};
							ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);

							RsUnico.Close();
							return result;
						}
						RsUnico.Close();
					}
					fHayValoresUnicos = false;
				}
			}

			return result;
		}

		private bool ModificarDB()
		{
			bool errorborrar = false;
			bool errormodificar = false;
			//Modificar un registro en la Base de Datos
			bool result = false;
			object lNumerico = null;
			Control[] MiControl = null;
			int i = 0;
			DialogResult res = (DialogResult) 0;
			bool p = true;
            SqlDataAdapter tempAdapter_2 = null;
            //Se redimensiona para introducir los valores de la Spread
            aActualizaSpread = ArraysHelper.InitializeArray<string>(sprMultiple.MaxCols + 1);

			if (vfCambiosPassword)
			{
				SMT200F1 tempLoadForm = SMT200F1.DefInstance;
				SMT200F1.DefInstance.RecibirContrase�a(vstContrase�a);
				SMT200F1.DefInstance.ShowDialog();
			}

			//CRIS
			//*******************************
			aComparacion = ArraysHelper.InitializeArray<string>(astrElConjunto.GetUpperBound(0));

			//Introducir los datos del registro en un array para luego compararlo
			for (int iIndiceArray = 0; iIndiceArray <= astrElConjunto.GetUpperBound(0) - 1; iIndiceArray++)
			{
				if (astrElConjunto[iIndiceArray].dcoltabmanco.Trim() != "")
				{
					sprMultiple.Col = astrElConjunto[iIndiceArray].nordencoltab;
					aComparacion[iIndiceArray] = sprMultiple.Text;
				}
				else
				{
					//Si el orden de la columna en la tabla es cero, pongo la constante
					//CoEsCombo para luego mirar que, cada vez que tenga esta constante,
					//no lo comparo con la spread
					aComparacion[iIndiceArray] = ModuloMantenim.CoEsCombo;
				}
			}
			//*******************************

			int iIndiceModificar = 0;


			try
			{
				errormodificar = true;
				errorborrar = false;

				//stSQLManten = MontarSelectConsulta  'Para modificar

				//****************
				if (!ModuloMantenim.PermitirBorrar(ModuloMantenim.stSQLManten))
				{
					//Borrar de la spread el registro eliminado
					//sprMultiple.Action = 5; //Borra de la Spread el registro actual
					sprMultiple.MaxRows--;
					return false;
				}
				//****************
				if (!ModuloMantenim.PermitirModificar(ModuloMantenim.stSQLManten, aComparacion, this))
				{
					//No seguir pues no hay cambio
					return result;
				}
                //****************
                //Abro una transacci�n para borrar el registro que se quiere modificar.
                //Si da erores de integridad porque algunas de sus claves est� siendo
                //referenciada en otra tabla, avisaremos de que si modifica alguno
                //de los datos, puede cambiar el significado  del registro.
                //Si no da error seguiremos realizando la modificaci�n
                ModuloMantenim.RcManten.BeginTransScope();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(ModuloMantenim.stSQLManten, ModuloMantenim.RcManten);
				ModuloMantenim.RsManten2 = new DataSet();
				tempAdapter.Fill(ModuloMantenim.RsManten2);
				errorborrar = true;
				errormodificar = false;
                try
                {
                    ModuloMantenim.RsManten2.Delete(tempAdapter);
                }
                catch
                {
                    ModuloMantenim.RsManten2.Close();

                    if (ComprobarDependenciasTabla(vstNombreTabla, ModuloMantenim.stSQLManten, false, tempAdapter))
                    {
                        ModuloMantenim.RcManten.RollbackTransScope();
                    }
                    else
                    {
                        res = RadMessageBox.Show("Si modifica el elemento seleccionado, puede cambiar el significado del mismo. �Desea continuar?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question);
                        if (res == System.Windows.Forms.DialogResult.Yes)
                        {
                            ModuloMantenim.RcManten.RollbackTransScope();
                        }
                        else
                        {
                            return result;
                        }
                    }
                }
				errormodificar = true;
				errorborrar = false;
				tempAdapter_2 = new SqlDataAdapter(ModuloMantenim.stSQLManten, ModuloMantenim.RcManten);
				ModuloMantenim.RsManten2 = new DataSet();
				tempAdapter_2.Fill(ModuloMantenim.RsManten2);


				//Modificar los registros, porque no hay variaciones.
				ModuloMantenim.RsManten2.Edit();
				iIndiceModificar = 0;
				i = 0;

				//Bucle For para Modificar en la DB.
				for (iIndiceModificar = 0; iIndiceModificar <= sprMultiple.MaxCols - 1; iIndiceModificar++)
				{
					MiControl = new Control[iIndiceModificar + 1];
					MiControl[iIndiceModificar] = LaConexion[iIndiceModificar].NombreControl;

					//Si el control es combo no lo utilizo para grabar los datos en
					//la Base de Datos
					if (LaConexion[iIndiceModificar].NombreControl.Name != "cbbCombo")
					{
						//***********************************************************************
						//Aqu� comprobar las modificaciones con el array
						if (LaConexion[iIndiceModificar].NombreControl is RadMaskedEditBox)
						{
							if (aComparacion[iIndiceModificar] != Convert.ToString(LaConexion[iIndiceModificar].NombreControl.FormattedText()))
							{
								if (ComprobarNumericos(LaConexion[iIndiceModificar].NombreCampo))
								{
									if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
										//                        aActualizaSpread(iIndiceModificar) = ""
									}
									else
									{
										lNumerico = LaConexion[iIndiceModificar].NombreControl.FormattedText();
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = Convert.ToDouble(lNumerico);
										//                        aActualizaSpread(iIndiceModificar) = CStr(lNumerico)
									}
								}
								else
								{

									if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
										//                        aActualizaSpread(iIndiceModificar) = ""
									}
									else
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = LaConexion[iIndiceModificar].NombreControl.FormattedText();
										//                        aActualizaSpread(iIndiceModificar) = LaConexion(iIndiceModificar).NombreControl.FormattedText()
									}
								}
							}
							else
							{
								//Son iguales
								//                aActualizaSpread(iIndiceModificar) = aComparacion(iIndiceModificar)
							}
						}
						else
						{
							if (aComparacion[iIndiceModificar] != Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text))
							{
								if (ComprobarNumericos(LaConexion[iIndiceModificar].NombreCampo))
								{
									if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
										//                        aActualizaSpread(iIndiceModificar) = ""
									}
									else
									{
										lNumerico = LaConexion[iIndiceModificar].NombreControl.Text;
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = Convert.ToDouble(lNumerico);
										//                        aActualizaSpread(iIndiceModificar) = CStr(lNumerico)
									}
								}
								else
								{

									if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = DBNull.Value;
										//                        aActualizaSpread(iIndiceModificar) = ""
									}
									else
									{
										ModuloMantenim.RsManten2.Tables[0].Rows[0][i] = LaConexion[iIndiceModificar].NombreControl.Text;
										//                        aActualizaSpread(iIndiceModificar) = LaConexion(iIndiceModificar).NombreControl.Text
									}
								}
							}
							else
							{
								//Son iguales
								//                aActualizaSpread(iIndiceModificar) = aComparacion(iIndiceModificar)
							}
						}
						i++;
					}
					else
					{
						//        aActualizaSpread(iIndiceModificar) = LaConexion(iIndiceModificar).NombreControl.Text
					}
					MiControl[iIndiceModificar] = null;
				}

                try
                {
                    //oscar 04/06/2004
                    string sql = String.Empty;
                    if (vfCambiosPassword && vstNombreTabla == "SUSUARIO")
                    {
                        sql = "INSERT INTO SUSUCCON (FCAMBIO, GUSUARIO, NPASSWORD) " +
                              " VALUES (GETDATE(), '" + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["gusuario"]) + "','" + Convert.ToString(ModuloMantenim.RsManten2.Tables[0].Rows[0]["vpassword"]) + "')";
                        SqlCommand tempCommand = new SqlCommand(sql, ModuloMantenim.RcManten);
                        tempCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception excep)
                {
                    if (excep.InnerException is SqlException)
                    {
                        var errors = new RDO_rdoErrors(excep.InnerException as SqlException);
                        if (errors.rdoErrors.Count > 1)
                        {
                            //Si el error es que hay una clave
                            //referenci�ndose en otra tabla
                            if (errors.rdoErrors[1].Number == 547)
                            {
                                //            res = MsgBox("Si modifica el elemento seleccionado, puede cambiar el significado del mismo. �Desea continuar?", vbYesNo + vbQuestion)
                                res = RadMessageBox.Show("Existen incongruencias entre el tipo de campo y el valor introducido." + "\n" + "\r" + "Revise el contenido de los campos ", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                                ModuloMantenim.RcManten.RollbackTransScope();
                                return result;
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                                return result;
                            }
                        }
                        else
                        {
                            if (errors.rdoErrors[0].Number == 431)
                            {
                                if (!ModuloMantenim.DesHabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                    p = ModificaUpdate(ModuloMantenim.RsManten2, tempAdapter_2);
                                }
                                if (!ModuloMantenim.HabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                }
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                            }
                        }
                    }
                }
                //----------------

                //Oscar
                //Parece ser que se modifica un registro de la BD, por lo que se tiene
                //que auditar.Se hace antes sel update, porque en caso de producirse un error
                //en la actualizacion, se hace un rollback de la transaccion
                try
                {
                    ModuloMantenim.AUDITARMOVIMIENTO("M", vstNombreTabla, Serrores.VVstUsuarioApli, ModuloMantenim.RsManten2);
                }
                catch (Exception excep)
                {
                    if (excep.InnerException is SqlException)
                    {
                        var errors = new RDO_rdoErrors(excep.InnerException as SqlException);
                        if (errors.rdoErrors.Count > 1)
                        {
                            //Si el error es que hay una clave
                            //referenci�ndose en otra tabla
                            if (errors.rdoErrors[1].Number == 547)
                            {
                                //            res = MsgBox("Si modifica el elemento seleccionado, puede cambiar el significado del mismo. �Desea continuar?", vbYesNo + vbQuestion)
                                res = RadMessageBox.Show("Existen incongruencias entre el tipo de campo y el valor introducido." + "\n" + "\r" + "Revise el contenido de los campos ", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                                ModuloMantenim.RcManten.RollbackTransScope();
                                return result;
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                                return result;
                            }
                        }
                        else
                        {
                            if (errors.rdoErrors[0].Number == 431)
                            {
                                if (!ModuloMantenim.DesHabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                    p = ModificaUpdate(ModuloMantenim.RsManten2, tempAdapter_2);
                                }
                                if (!ModuloMantenim.HabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                }
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                            }
                        }
                    }
                }
                //---------------
                try
                {
                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                    tempAdapter_2.Update(ModuloMantenim.RsManten2, ModuloMantenim.RsManten2.Tables[0].TableName);
                    ModuloMantenim.RsManten2.Close();
                }
                catch (Exception excep)
                {
                    if (excep.InnerException is SqlException)
                    {
                        var errors = new RDO_rdoErrors(excep.InnerException as SqlException);
                        if (errors.rdoErrors.Count > 1)
                        {
                            //Si el error es que hay una clave
                            //referenci�ndose en otra tabla
                            if (errors.rdoErrors[1].Number == 547)
                            {
                                //            res = MsgBox("Si modifica el elemento seleccionado, puede cambiar el significado del mismo. �Desea continuar?", vbYesNo + vbQuestion)
                                res = RadMessageBox.Show("Existen incongruencias entre el tipo de campo y el valor introducido." + "\n" + "\r" + "Revise el contenido de los campos ", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
                                ModuloMantenim.RcManten.RollbackTransScope();
                                return result;
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                                return result;
                            }
                        }
                        else
                        {
                            if (errors.rdoErrors[0].Number == 431)
                            {
                                if (!ModuloMantenim.DesHabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                    p = ModificaUpdate(ModuloMantenim.RsManten2, tempAdapter_2);
                                }
                                if (!ModuloMantenim.HabilitarConstraintsModificacion(vstNombreTabla, ModuloMantenim.RsManten2))
                                {
                                    return result;
                                }
                                else
                                {
                                }
                            }
                            else
                            {
                                ModuloMantenim.RcManten.RollbackTransScope();
                                ModuloMantenim.ColeccionErrores(excep);
                            }
                        }
                    }
                }
				if (p)
				{
					iIndiceModificar = 0;
					i = 0;
					//Bucle For para Modificar en la DB.
					for (iIndiceModificar = 0; iIndiceModificar <= sprMultiple.MaxCols - 1; iIndiceModificar++)
					{
						MiControl = new Control[iIndiceModificar + 1];
						MiControl[iIndiceModificar] = LaConexion[iIndiceModificar].NombreControl;

						//Si el control es combo no lo utilizo para grabar los datos en
						//la Base de Datos
						if (LaConexion[iIndiceModificar].NombreControl.Name != "cbbCombo")
						{
							//***********************************************************************
							//Aqu� comprobar las modificaciones con el array
							if (LaConexion[iIndiceModificar].NombreControl is RadMaskedEditBox)
							{
								if (aComparacion[iIndiceModificar] != Convert.ToString(LaConexion[iIndiceModificar].NombreControl.FormattedText()))
								{
									if (ComprobarNumericos(LaConexion[iIndiceModificar].NombreCampo))
									{
										if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
										{
											//                            RsManten2(i) = Null
											aActualizaSpread[iIndiceModificar] = "";
										}
										else
										{
											lNumerico = LaConexion[iIndiceModificar].NombreControl.FormattedText();
											//                            RsManten2(i) = lNumerico
											aActualizaSpread[iIndiceModificar] = Convert.ToString(lNumerico);
										}
									}
									else
									{

										if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
										{
											//                            RsManten2(i) = Null
											aActualizaSpread[iIndiceModificar] = "";
										}
										else
										{
											//                            RsManten2(i) = LaConexion(iIndiceModificar).NombreControl.FormattedText()
											aActualizaSpread[iIndiceModificar] = Convert.ToString(LaConexion[iIndiceModificar].NombreControl.FormattedText());
										}
									}
								}
								else
								{
									//Son iguales
									aActualizaSpread[iIndiceModificar] = aComparacion[iIndiceModificar];
								}
							}
							else
							{
								if (aComparacion[iIndiceModificar] != Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text))
								{
									if (ComprobarNumericos(LaConexion[iIndiceModificar].NombreCampo))
									{
										if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
										{
											//                            RsManten2(i) = Null
											aActualizaSpread[iIndiceModificar] = "";
										}
										else
										{
											lNumerico = LaConexion[iIndiceModificar].NombreControl.Text;
											//                            RsManten2(i) = lNumerico
											aActualizaSpread[iIndiceModificar] = Convert.ToString(lNumerico);
										}
									}
									else
									{

										if (Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text).Trim() == "")
										{
											//                            RsManten2(i) = Null
											aActualizaSpread[iIndiceModificar] = "";
										}
										else
										{
											//                            RsManten2(i) = LaConexion(iIndiceModificar).NombreControl.Text
											aActualizaSpread[iIndiceModificar] = Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text);
										}
									}
								}
								else
								{
									//Son iguales
									aActualizaSpread[iIndiceModificar] = aComparacion[iIndiceModificar];
								}
							}
							i++;
						}
						else
						{
							aActualizaSpread[iIndiceModificar] = Convert.ToString(LaConexion[iIndiceModificar].NombreControl.Text);
						}
						MiControl[iIndiceModificar] = null;
					}
					ModuloMantenim.ActualizarSpread(aActualizaSpread, this);
					result = true;
				}
				return result;
			}
			catch (Exception excep)
			{
                RadMessageBox.Show(excep.Message, Application.ProductName);
            }
            return false;
		}


		public string MontarSelectConsulta()
		{

			StringBuilder stSQLBorrar = new StringBuilder();

			for (int i = 0; i <= AStPK.GetUpperBound(0); i++)
			{
				if (i == 0)
				{
					stSQLBorrar = new StringBuilder("SELECT * FROM " + vstNombreTabla);
					stSQLBorrar.Append(" where ");
				}
				else
				{
					stSQLBorrar.Append(" and ");
				}

				switch(AStPK[i].StTipo)
				{
					case "DATETIME" :  //Campo fecha 
						//Formateamos la fecha DD/MM/YYYY 
						stSQLBorrar.Append("CONVERT(char(10)," + AStPK[i].StColumna + ",103)"); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							System.DateTime TempDate = DateTime.FromOADate(0);
							stSQLBorrar.Append("=" + "'" + ((DateTime.TryParse(AStPK[i].stTexto.Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : AStPK[i].stTexto.Trim()) + "'");
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
					case "VARCHAR" : case "CHAR" :  //Campo texto con o sin m�scara 
						stSQLBorrar.Append(AStPK[i].StColumna); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							stSQLBorrar.Append("=" + "'" + AStPK[i].stTexto.Trim() + "'");
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
					case "NUMERIC" : case "SMALLINT" : case "INT" :  //Campo num�rico 
						stSQLBorrar.Append(AStPK[i].StColumna); 
						if (AStPK[i].stTexto.Trim() != "")
						{
							stSQLBorrar.Append("=" + AStPK[i].stTexto.Trim());
						}
						else
						{
							stSQLBorrar.Append(" IS NULL");
						} 
						 
						break;
				}
			}
			return stSQLBorrar.ToString();
		}



		private void SustituirEnSpreadFK(string StComando1, string StComando2, int iLongitudColumna, bool fDimensionarCombo, ModuloMantenim.StrFKCombo[] AStColFKCombo, ModuloMantenim.StrFK[] AStColFK, int iIndiceCombo)
		{
			DataSet RsFK = null;
			bool fAND = false;
			StringBuilder StComando = new StringBuilder();

			string StSelect = "Select ";

			int k = 1;

			while (k <= sprMultiple.MaxRows)
			{
				//Monto la sentencia WHERE para cada fila de la spread y
				//Oculto las columnas de la spread que son FK

				StComando = new StringBuilder(" where ");
				foreach (ModuloMantenim.StrFK AStColFK_item in AStColFK)
				{

					if (fAND)
					{
						StComando.Append(" and ");
					}
					fAND = true;

					//Ademas de a�adir cada columna al WHERE de la sentencia,
					//busco el n� de columna en el spread a la que corresponde
					//cada campo que es FK y la oculto.
					//Tambi�n oculto el control asociado a esa columna
					for (int i = 0; i <= astrElConjunto.GetUpperBound(0) - 1; i++)
					{
						if (astrElConjunto[i].gcolumncaman.Trim() == AStColFK_item.StColHija.Trim())
						{

							//La booleana fDimensionarCombo, me dice si tengo que redimensionar o no
							//el array, ya que la 1� vez est� dimensionado
							if (fDimensionarCombo)
							{
								AStColFKCombo = ArraysHelper.RedimPreserve(AStColFKCombo, new int[]{AStColFKCombo.GetUpperBound(0) + 2});
							}

							fDimensionarCombo = true;

							//Esto me sirve luego para poder identificar cada columna
							//que es clave a que combo pertenece y que n� de columna
							//ocupa en ese combo
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StColHija = AStColFK_item.StColHija.Trim();
							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].iIndiceCombo = (short) iIndiceCombo;

							AStColFKCombo[AStColFKCombo.GetUpperBound(0)].StLabelCombo = AStColFK_item.StLabelCombo.Trim();

							sprMultiple.Col = astrElConjunto[i].nordencoltab;
							sprMultiple.SetColHidden(sprMultiple.Col, true);
							astrElConjunto[i].NombreControl.Visible = false;

							StComando.Append("A." + AStColFK_item.StColPadre + "=");

							//SI ES "CHAR", "VARCHAR" O "DATETIME",
							//EL DATO VA ENTRE COMILLA SIMPLE
							if (AStColFK_item.stTipoDato.Trim() == "VARCHAR" || AStColFK_item.stTipoDato.Trim() == "CHAR")
							{
								StComando.Append("'" + sprMultiple.Text.Trim() + "'");
							}
							else if (AStColFK_item.stTipoDato.Trim() == "DATETIME")
							{ 
								StComando.Append("'" + DateTime.Parse(sprMultiple.Text).ToString("MM/dd/yyyy") + "'");
							}
							else
							{
								StComando.Append(sprMultiple.Text.Trim());
							}

							//Salgo del bucle FOR porque ya ha encontrado la columna
							break;
						}
					}
				}

				fAND = false;
				//Monto la consulta con la sentencia SELECT, el WHERE y el ORDER BY
				StComando = new StringBuilder(StSelect + StComando1 + StComando.ToString() + " " + StComando2);

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando.ToString(), ModuloMantenim.RcManten);
				RsFK = new DataSet();
				tempAdapter.Fill(RsFK);

				//Inserto columnas con los datos que tienen que mostrarse
				//en vez de las FK

				sprMultiple.Col++;
				sprMultiple.setTypeEditLen(iLongitudColumna);
				sprMultiple.SetColWidth(sprMultiple.Col, AStColFK[0].StLabelCombo.Length);

				sprMultiple.Row = k;
				sprMultiple.Text = "";
				for (int i = 0; i <= RsFK.Tables[0].Columns.Count - 1; i++)
				{
					sprMultiple.Text = sprMultiple.Text + Convert.ToString(RsFK.Tables[0].Rows[0][i]) + " ";
				}

				RsFK.Close();

				k++;
			}

			//N� de �ndice de ControlCombo
			iIndiceCombo++;

		}
		private bool ValidarRequeridoCombo(ModuloMantenim.StrFKCombo[] AStColFKCombo)
		{
			//Debemos buscar en la estructura el control y comprobar si admite nulos
			Control MiControl = null;

			for (int iIndiceControl = 0; iIndiceControl <= astrElConjunto.GetUpperBound(0) - 1; iIndiceControl++)
			{

				MiControl = astrElConjunto[iIndiceControl].NombreControl;
				if (Convert.ToString(MiControl.Text).Trim() == "" && astrElConjunto[iIndiceControl].iadmitenull == "N")
				{
					if (UpgradeHelpers.Gui.ControlHelper.GetVisible(MiControl))
					{
						//MENSAJE:Campo V1 requerido.
						short tempRefParam = 1040;
						string[] tempRefParam2 = new string[]{astrElConjunto[iIndiceControl].dcoltabmanco};
						ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
						if (UpgradeHelpers.Gui.ControlHelper.GetEnabled(MiControl))
						{
							MiControl.Focus();
						}
					}
					else
					{
						ModuloMantenim.DarFoco(astrElConjunto[iIndiceControl].gcolumncaman, AStColFKCombo, true, this);
					}
					MiControl = null;
					return false;
				}
				MiControl = null;
			}
			return true;
		}

		private void TlbBuscar_ButtonClick(int Index, ToolStripItem Button)
		{

			//Se llama a Mantenimiento Autom�tico.
			//Para eso debemos de pasarle la tabla Padre, y la buscamos sabiendo
			//que el n� de IndiceCombo es igual al del control ToolBar
			foreach (ModuloMantenim.StrFKCombo AStColFKCombo_item in AStColFKCombo)
			{
				if (ControlArrayHelper.GetControlIndex(AStrToolbar[Index].ComboAsociado) == AStColFKCombo_item.iIndiceCombo)
				{

					StModoCombo = StModo.Trim();
					//Hay que crear el array de columnas FK padre para mand�rselo al
					//Mantenimiento Autom�tico nuevo que se va a crear
					GuardarColumnasPadre(AStColFKCombo_item.iIndiceCombo);

					CtrlCombo = cbbCombo[AStColFKCombo_item.iIndiceCombo];
                    // ralopezn 20/05/2016 Verificar el funcionamiento.
                    var stTablaPadre = AStColFKCombo_item.StTablaPadre;
                    ModuloMantenim.LlamadaMantenimiento(ref stTablaPadre, this, AStColumnasFKPadre, ControlArrayHelper.GetControlIndex(AStrToolbar[Index].ComboAsociado));

					break;
				}
			}
		}

		private void proRecolocaControles()
		{
			this.Top = (int) 0;
			this.Left = (int) 0;
			this.Width = (int) Screen.PrimaryScreen.Bounds.Width;
			this.Height = (int) (Screen.PrimaryScreen.Bounds.Height - 33);
			frmSpread.Height = (int) (this.Height / 2);
			frmSpread.Width = (int) (this.Width - 7);
			cbSpread[0].Left = (int) (frmSpread.Width - cbSpread[0].Width - 13);
			for (int i = 1; i <= 8; i++)
			{
				cbSpread[i].Left = (int) cbSpread[0].Left;
			}
			sprMultiple.Width = (int) (frmSpread.Width - cbSpread[0].Width - 27);
			sprMultiple.Height = (int) (frmSpread.Height - 13);
			frmDetalleFondo.Top = (int) (frmSpread.Height + 7);
			frmDetalleFondo.Height = (int) (this.Height / 2);
			frmBotones.Top = (int) frmDetalleFondo.Top;
			frmBotones.Left = (int) (this.Width - frmBotones.Width - 7);
			frmBotones.Height = (int) frmDetalleFondo.Height;
			FrmDetalleMedio.Top = 10;
			FrmDetalleMedio.Height = (int) (frmDetalleFondo.Height - 13);
			scbvCampos.Height = (int) FrmDetalleMedio.Height;
			scbvCampos.Top = 10;
		}
		private void scbvCampos_Scroll(Object eventSender, ScrollEventArgs eventArgs)
		{
			switch(eventArgs.Type)
			{
				case ScrollEventType.EndScroll : 
					scbvCampos_Change(eventArgs.NewValue); 
					break;
			}
		}
	}
}