using Microsoft.VisualBasic;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace MantenAutomatico
{
	public partial class SMT200F1
		: RadForm
	{

		public SMT200F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		public void RecibirContraseña(string StContraseña)
		{
			tbNuevaContraseña.Text = StContraseña;
			tbNuevaContraseña.IsReadOnly = true;
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{

			ModuloMantenim.VerificarModoConexion();

			//Comprobamos que el campo no es nulo.
			if (tbRepetNContraseña.Text == "")
			{
				short tempRefParam = 1040;
                string[] tempRefParam2 = new string[]{lbRepNContraseña.Text};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
				tbRepetNContraseña.Focus();
				return;
			}

			//Comprobamos si los caracteres son válidos.
			string tempRefParam3 = tbRepetNContraseña.Text;
			if (!ModuloMantenim.ComprobarCaracteres(ref tempRefParam3))
			{
				short tempRefParam4 = 1230;
                string[] tempRefParam5 = new string[] { lbNContraseña.Text, "letras y números"};
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, ModuloMantenim.RcManten, tempRefParam5);
				tbRepetNContraseña.Focus();
				tbRepetNContraseña.SelectionStart = 0;
				tbRepetNContraseña.SelectionLength = Strings.Len(tbRepetNContraseña.Text);
				SendKeys.Send("{Home}+{End}");
			}
			else
			{
				string tempRefParam6 = tbRepetNContraseña.Text;
				tbRepetNContraseña.Text = ModuloMantenim.CambiarContraseña(ref tempRefParam6);
				//MENSAJE: El Administrador del Sistema debe conocer
				//su contraseña.
				if (ModuloMantenim.vstModo.Value == "N")
				{
					short tempRefParam7 = 1260;
                    string[] tempRefParam8 = new string[] { };
					ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, ModuloMantenim.RcManten, tempRefParam8);
				}
			}
			//Comprobamos que los campos sean iguales.
			if (tbNuevaContraseña.Text != tbRepetNContraseña.Text)
			{
				short tempRefParam9 = 1250;
                string[] tempRefParam10 = new string[] { };
				ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, ModuloMantenim.RcManten, tempRefParam10);
				tbRepetNContraseña.Text = "";
				return;
			}


			this.Close();

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{

			this.Close();

		}





		private void tbRepetNContraseña_Leave(Object eventSender, EventArgs eventArgs)
		{

			//Si pulsa cancelar no ejecuta la rutina

			//Comprobamos que el campo no es nulo.
			if (ActiveControl.Name != cbCancelar.Name)
			{ //Si no pulsa cancelar
				if (tbRepetNContraseña.Text == "")
				{
					short tempRefParam = 1040;
                    string[] tempRefParam2 = new string[] { lbRepNContraseña.Text};
					ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
					return;
				}
			}

		}
		private void SMT200F1_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}