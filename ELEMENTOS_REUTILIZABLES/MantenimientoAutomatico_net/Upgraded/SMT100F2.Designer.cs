using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace MantenAutomatico
{
	partial class SMT100F2
	{

		#region "Upgrade Support "
		private static SMT100F2 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static SMT100F2 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new SMT100F2();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "_TlbBuscar_0_Button1", "_TlbBuscar_0", "_TbClaves_0", "_tbMultilinea_0", "_tbTexto_0", "_mebMascara_0", "_sdcFecha_0", "ImageList1", "_Cbb_AlgoNada_0", "_cbbCombo_0", "_lbEtiqueta_0", "frmDetalle", "FrmDetalleMedio", "scbvCampos", "frmDetalleFondo", "_rbConsulta_1", "_rbConsulta_0", "frmConsulta", "_cbBotones_2", "_cbBotones_1", "_cbBotones_0", "frmBotones", "sprMultiple", "_cbSpread_0", "_cbSpread_3", "_cbSpread_4", "_cbSpread_7", "_cbSpread_8", "_cbSpread_6", "_cbSpread_5", "_cbSpread_1", "_cbSpread_2", "frmSpread", "Cbb_AlgoNada", "TbClaves", "TlbBuscar", "cbBotones", "cbSpread", "cbbCombo", "lbEtiqueta", "mebMascara", "rbConsulta", "sdcFecha", "tbMultilinea", "tbTexto", "sprMultiple_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public System.Windows.Forms.OpenFileDialog CDAyudaOpen;
		public System.Windows.Forms.SaveFileDialog CDAyudaSave;
		public System.Windows.Forms.FontDialog CDAyudaFont;
		public System.Windows.Forms.ColorDialog CDAyudaColor;
		public System.Windows.Forms.PrintDialog CDAyudaPrint;
		private System.Windows.Forms.ToolStripButton _TlbBuscar_0_Button1;
		private System.Windows.Forms.ToolStrip _TlbBuscar_0;
		private Telerik.WinControls.UI.RadTextBoxControl _TbClaves_0;
		private Telerik.WinControls.UI.RadTextBoxControl _tbMultilinea_0;
		private Telerik.WinControls.UI.RadTextBoxControl _tbTexto_0;
		private Telerik.WinControls.UI.RadMaskedEditBox _mebMascara_0;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcFecha_0;
		public System.Windows.Forms.ImageList ImageList1;
		private Telerik.WinControls.UI.RadDropDownList _Cbb_AlgoNada_0;
		private Telerik.WinControls.UI.RadDropDownList _cbbCombo_0;
		private Telerik.WinControls.UI.RadLabel _lbEtiqueta_0;
		public Telerik.WinControls.UI.RadPanel frmDetalle;
		public Telerik.WinControls.UI.RadPanel FrmDetalleMedio;
		public System.Windows.Forms.VScrollBar scbvCampos;
		public Telerik.WinControls.UI.RadGroupBox frmDetalleFondo;
		private Telerik.WinControls.UI.RadRadioButton _rbConsulta_1;
		private Telerik.WinControls.UI.RadRadioButton _rbConsulta_0;
		public Telerik.WinControls.UI.RadGroupBox frmConsulta;
		private Telerik.WinControls.UI.RadButton _cbBotones_2;
		private Telerik.WinControls.UI.RadButton _cbBotones_1;
		private Telerik.WinControls.UI.RadButton _cbBotones_0;
		public Telerik.WinControls.UI.RadGroupBox frmBotones;
		public UpgradeHelpers.Spread.FpSpread sprMultiple;
		private Telerik.WinControls.UI.RadButton _cbSpread_0;
		private Telerik.WinControls.UI.RadButton _cbSpread_3;
		private Telerik.WinControls.UI.RadButton _cbSpread_4;
		private Telerik.WinControls.UI.RadButton _cbSpread_7;
		private Telerik.WinControls.UI.RadButton _cbSpread_8;
		private Telerik.WinControls.UI.RadButton _cbSpread_6;
		private Telerik.WinControls.UI.RadButton _cbSpread_5;
		private Telerik.WinControls.UI.RadButton _cbSpread_1;
		private Telerik.WinControls.UI.RadButton _cbSpread_2;
		public Telerik.WinControls.UI.RadGroupBox frmSpread;
		public Telerik.WinControls.UI.RadDropDownList[] Cbb_AlgoNada = new Telerik.WinControls.UI.RadDropDownList[1];
		public Telerik.WinControls.UI.RadTextBoxControl[] TbClaves = new Telerik.WinControls.UI.RadTextBoxControl[1];
		public System.Windows.Forms.ToolStrip[] TlbBuscar = new System.Windows.Forms.ToolStrip[1];
		public Telerik.WinControls.UI.RadButton[] cbBotones = new Telerik.WinControls.UI.RadButton[3];
		public Telerik.WinControls.UI.RadButton[] cbSpread = new Telerik.WinControls.UI.RadButton[9];
		public Telerik.WinControls.UI.RadDropDownList[] cbbCombo = new Telerik.WinControls.UI.RadDropDownList[1];
		public Telerik.WinControls.UI.RadLabel[] lbEtiqueta = new Telerik.WinControls.UI.RadLabel[1];
		public Telerik.WinControls.UI.RadMaskedEditBox[] mebMascara = new Telerik.WinControls.UI.RadMaskedEditBox[1];
		public Telerik.WinControls.UI.RadRadioButton[] rbConsulta = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadDateTimePicker[] sdcFecha = new Telerik.WinControls.UI.RadDateTimePicker[1];
		public Telerik.WinControls.UI.RadTextBoxControl[] tbMultilinea = new Telerik.WinControls.UI.RadTextBoxControl[1];
		public Telerik.WinControls.UI.RadTextBoxControl[] tbTexto = new Telerik.WinControls.UI.RadTextBoxControl[1];
		//private FarPoint.Win.Spread.SheetView sprMultiple_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SMT100F2));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.CDAyudaOpen = new System.Windows.Forms.OpenFileDialog();
			this.CDAyudaSave = new System.Windows.Forms.SaveFileDialog();
			this.CDAyudaFont = new System.Windows.Forms.FontDialog();
			this.CDAyudaColor = new System.Windows.Forms.ColorDialog();
			this.CDAyudaPrint = new System.Windows.Forms.PrintDialog();
			this.frmDetalleFondo = new Telerik.WinControls.UI.RadGroupBox();
			this.FrmDetalleMedio = new Telerik.WinControls.UI.RadPanel();
			this.frmDetalle = new Telerik.WinControls.UI.RadPanel();
			this._TlbBuscar_0 = new System.Windows.Forms.ToolStrip();
			this._TlbBuscar_0_Button1 = new System.Windows.Forms.ToolStripButton();
			this._TbClaves_0 = new Telerik.WinControls.UI.RadTextBoxControl();
			this._tbMultilinea_0 = new Telerik.WinControls.UI.RadTextBoxControl();
			this._tbTexto_0 = new Telerik.WinControls.UI.RadTextBoxControl();
			this._mebMascara_0 = new Telerik.WinControls.UI.RadMaskedEditBox();
			this._sdcFecha_0 = new Telerik.WinControls.UI.RadDateTimePicker();
			this.ImageList1 = new System.Windows.Forms.ImageList();
			this._Cbb_AlgoNada_0 = new Telerik.WinControls.UI.RadDropDownList();
			this._cbbCombo_0 = new Telerik.WinControls.UI.RadDropDownList();
			this._lbEtiqueta_0 = new Telerik.WinControls.UI.RadLabel();
			this.scbvCampos = new System.Windows.Forms.VScrollBar();
			this.frmBotones = new Telerik.WinControls.UI.RadGroupBox();
			this.frmConsulta = new Telerik.WinControls.UI.RadGroupBox();
			this._rbConsulta_1 = new Telerik.WinControls.UI.RadRadioButton();
			this._rbConsulta_0 = new Telerik.WinControls.UI.RadRadioButton();
			this._cbBotones_2 = new Telerik.WinControls.UI.RadButton();
			this._cbBotones_1 = new Telerik.WinControls.UI.RadButton();
			this._cbBotones_0 = new Telerik.WinControls.UI.RadButton();
			this.frmSpread = new Telerik.WinControls.UI.RadGroupBox();
			this.sprMultiple = new UpgradeHelpers.Spread.FpSpread();
			this._cbSpread_0 = new Telerik.WinControls.UI.RadButton();
			this._cbSpread_3 = new Telerik.WinControls.UI.RadButton();
			this._cbSpread_4 = new Telerik.WinControls.UI.RadButton();
			this._cbSpread_7 = new Telerik.WinControls.UI.RadButton();
			this._cbSpread_8 = new Telerik.WinControls.UI.RadButton();
			this._cbSpread_6 = new Telerik.WinControls.UI.RadButton();
			this._cbSpread_5 = new Telerik.WinControls.UI.RadButton();
			this._cbSpread_1 = new Telerik.WinControls.UI.RadButton();
			this._cbSpread_2 = new Telerik.WinControls.UI.RadButton();
			this.frmDetalleFondo.SuspendLayout();
			this.FrmDetalleMedio.SuspendLayout();
			this.frmDetalle.SuspendLayout();
			this._TlbBuscar_0.SuspendLayout();
			this.frmBotones.SuspendLayout();
			this.frmConsulta.SuspendLayout();
			this.frmSpread.SuspendLayout();
			this.SuspendLayout();
			// 
			// frmDetalleFondo
			// 
			this.frmDetalleFondo.Controls.Add(this.FrmDetalleMedio);
			this.frmDetalleFondo.Controls.Add(this.scbvCampos);
			this.frmDetalleFondo.Enabled = true;
			this.frmDetalleFondo.Location = new System.Drawing.Point(0, 246);
			this.frmDetalleFondo.Name = "frmDetalleFondo";
			this.frmDetalleFondo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmDetalleFondo.Size = new System.Drawing.Size(655, 325);
			this.frmDetalleFondo.TabIndex = 6;
			this.frmDetalleFondo.Visible = true;
			// 
			// FrmDetalleMedio
			// 
			this.FrmDetalleMedio.Controls.Add(this.frmDetalle);
			this.FrmDetalleMedio.Cursor = System.Windows.Forms.Cursors.Default;
			this.FrmDetalleMedio.Enabled = true;
			this.FrmDetalleMedio.Location = new System.Drawing.Point(8, 8);
			this.FrmDetalleMedio.Name = "FrmDetalleMedio";
			this.FrmDetalleMedio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmDetalleMedio.Size = new System.Drawing.Size(627, 313);
			this.FrmDetalleMedio.TabIndex = 11;
			this.FrmDetalleMedio.Text = "Frame1";
			this.FrmDetalleMedio.Visible = true;
			// 
			// frmDetalle
			// 
			this.frmDetalle.Controls.Add(this._TlbBuscar_0);
			this.frmDetalle.Controls.Add(this._TbClaves_0);
			this.frmDetalle.Controls.Add(this._tbMultilinea_0);
			this.frmDetalle.Controls.Add(this._tbTexto_0);
			this.frmDetalle.Controls.Add(this._mebMascara_0);
			this.frmDetalle.Controls.Add(this._sdcFecha_0);
			this.frmDetalle.Controls.Add(this._Cbb_AlgoNada_0);
			this.frmDetalle.Controls.Add(this._cbbCombo_0);
			this.frmDetalle.Controls.Add(this._lbEtiqueta_0);
			this.frmDetalle.Cursor = System.Windows.Forms.Cursors.Default;
			this.frmDetalle.Enabled = true;
			this.frmDetalle.Location = new System.Drawing.Point(0, 0);
			this.frmDetalle.Name = "frmDetalle";
			this.frmDetalle.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmDetalle.Size = new System.Drawing.Size(626, 309);
			this.frmDetalle.TabIndex = 12;
			this.frmDetalle.Text = "Frame1";
			this.frmDetalle.Visible = true;
			// 
			// _TlbBuscar_0
			// 
			this._TlbBuscar_0.Dock = System.Windows.Forms.DockStyle.None;
			this._TlbBuscar_0.ImageList = ImageList1;
			this._TlbBuscar_0.Location = new System.Drawing.Point(164, 110);
			this._TlbBuscar_0.Name = "_TlbBuscar_0";
			this._TlbBuscar_0.ShowItemToolTips = true;
			this._TlbBuscar_0.Size = new System.Drawing.Size(28, 26);
			this._TlbBuscar_0.TabIndex = 20;
			this._TlbBuscar_0.Items.Add(this._TlbBuscar_0_Button1);
			// 
			// _TlbBuscar_0_Button1
			// 
			this._TlbBuscar_0_Button1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._TlbBuscar_0_Button1.ImageIndex = 0;
			this._TlbBuscar_0_Button1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._TlbBuscar_0_Button1.Name = "";
			this._TlbBuscar_0_Button1.Size = new System.Drawing.Size(24, 22);
			this._TlbBuscar_0_Button1.Tag = "";
			this._TlbBuscar_0_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._TlbBuscar_0_Button1.ToolTipText = "Buscar";
			// 
			// _TbClaves_0
			// 
			this._TbClaves_0.AcceptsReturn = true;
			this._TbClaves_0.Cursor = System.Windows.Forms.Cursors.IBeam;
			this._TbClaves_0.Location = new System.Drawing.Point(302, 39);
			this._TbClaves_0.MaxLength = 0;
			this._TbClaves_0.Name = "_TbClaves_0";
			this._TbClaves_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._TbClaves_0.Size = new System.Drawing.Size(64, 20);
			this._TbClaves_0.TabIndex = 30;
			this._TbClaves_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbClaves_KeyPress);
			this._TbClaves_0.Leave += new System.EventHandler(this.TbClaves_Leave);
			// 
			// _tbMultilinea_0
			// 
			this._tbMultilinea_0.AcceptsReturn = true;
			this._tbMultilinea_0.Cursor = System.Windows.Forms.Cursors.IBeam;
			this._tbMultilinea_0.Location = new System.Drawing.Point(192, 62);
			this._tbMultilinea_0.MaxLength = 0;
			this._tbMultilinea_0.Multiline = true;
			this._tbMultilinea_0.Name = "_tbMultilinea_0";
			this._tbMultilinea_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._tbMultilinea_0.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AutoHide;
			this._tbMultilinea_0.Size = new System.Drawing.Size(383, 49);
			this._tbMultilinea_0.TabIndex = 14;
			this._tbMultilinea_0.Visible = false;
			this._tbMultilinea_0.Enter += new System.EventHandler(this.tbMultilinea_Enter);
			this._tbMultilinea_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMultilinea_KeyPress);
			this._tbMultilinea_0.Leave += new System.EventHandler(this.tbMultilinea_Leave);
			this._tbMultilinea_0.TextChanged += new System.EventHandler(this.tbMultilinea_TextChanged);
			// 
			// _tbTexto_0
			// 
			this._tbTexto_0.AcceptsReturn = true;
			this._tbTexto_0.Cursor = System.Windows.Forms.Cursors.IBeam;
			this._tbTexto_0.Location = new System.Drawing.Point(368, 38);
			this._tbTexto_0.MaxLength = 0;
			this._tbTexto_0.Name = "_tbTexto_0";
			this._tbTexto_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._tbTexto_0.Size = new System.Drawing.Size(113, 21);
			this._tbTexto_0.TabIndex = 13;
			this._tbTexto_0.Visible = false;
			this._tbTexto_0.Enter += new System.EventHandler(this.tbTexto_Enter);
			this._tbTexto_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTexto_KeyPress);
			this._tbTexto_0.Leave += new System.EventHandler(this.tbTexto_Leave);
			this._tbTexto_0.TextChanged += new System.EventHandler(this.tbTexto_TextChanged);
			// 
			// _mebMascara_0
			// 
			this._mebMascara_0.Location = new System.Drawing.Point(240, 138);
			this._mebMascara_0.Name = "_mebMascara_0";
			this._mebMascara_0.PromptChar = '_';
			this._mebMascara_0.Size = new System.Drawing.Size(113, 21);
			this._mebMascara_0.TabIndex = 15;
            this._mebMascara_0.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
			this._mebMascara_0.Visible = false;
			this._mebMascara_0.Enter += new System.EventHandler(this.mebMascara_Enter);
			this._mebMascara_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mebMascara_KeyPress);
			this._mebMascara_0.Leave += new System.EventHandler(this.mebMascara_Leave);
			// 
			// _sdcFecha_0
			// 
			this._sdcFecha_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this._sdcFecha_0.CustomFormat = "dd/MM/yyyy";
			this._sdcFecha_0.Format =  System.Windows.Forms.DateTimePickerFormat.Custom;
			this._sdcFecha_0.Location = new System.Drawing.Point(354, 138);
			this._sdcFecha_0.MaxDate = System.DateTime.Parse("3000/12/31");
			this._sdcFecha_0.MinDate = System.DateTime.Parse("1000/1/1");
			this._sdcFecha_0.Name = "_sdcFecha_0";
			this._sdcFecha_0.Size = new System.Drawing.Size(105, 21);
			this._sdcFecha_0.TabIndex = 16;
			this._sdcFecha_0.Visible = false;
			this._sdcFecha_0.Enter += new System.EventHandler(this.sdcFecha_Enter);
			this._sdcFecha_0.Leave += new System.EventHandler(this.sdcFecha_Leave);
			// 
			// ImageList1
			// 
			this.ImageList1.ImageSize = new System.Drawing.Size(17, 16);
			this.ImageList1.ImageStream = (System.Windows.Forms.ImageListStreamer) resources.GetObject("ImageList1.ImageStream");
			this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
			// 
			// _Cbb_AlgoNada_0
			// 
			this._Cbb_AlgoNada_0.Location = new System.Drawing.Point(459, 10);
			this._Cbb_AlgoNada_0.Name = "_Cbb_AlgoNada_0";
			this._Cbb_AlgoNada_0.Size = new System.Drawing.Size(61, 20);
			this._Cbb_AlgoNada_0.TabIndex = 0;
			this._Cbb_AlgoNada_0.Visible = false;
			// 
			// _cbbCombo_0
			// 
			this._cbbCombo_0.Location = new System.Drawing.Point(193, 112);
			this._cbbCombo_0.Name = "_cbbCombo_0";
			this._cbbCombo_0.Size = new System.Drawing.Size(386, 21);
			this._cbbCombo_0.TabIndex = 18;
			this._cbbCombo_0.Visible = false;
			this._cbbCombo_0.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbCombo_Change);
			this._cbbCombo_0.Click += new System.EventHandler(this.cbbCombo_ClickEvent);
			this._cbbCombo_0.Enter += new System.EventHandler(this.cbbCombo_Enter);
            this._cbbCombo_0.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbbCombo_KeyDownEvent);
            this._cbbCombo_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbCombo_KeyPressEvent);
			// 
			// _lbEtiqueta_0
			// 
			this._lbEtiqueta_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._lbEtiqueta_0.Location = new System.Drawing.Point(366, 17);
			this._lbEtiqueta_0.Name = "_lbEtiqueta_0";
			this._lbEtiqueta_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._lbEtiqueta_0.Size = new System.Drawing.Size(112, 15);
			this._lbEtiqueta_0.TabIndex = 17;
			this._lbEtiqueta_0.Text = "lbEtiqueta";
			this._lbEtiqueta_0.Visible = false;
			// 
			// scbvCampos
			// 
			this.scbvCampos.CausesValidation = true;
			this.scbvCampos.Cursor = System.Windows.Forms.Cursors.Default;
			this.scbvCampos.Enabled = true;
			this.scbvCampos.LargeChange = 100;
			this.scbvCampos.Location = new System.Drawing.Point(636, 10);
			this.scbvCampos.Maximum = 32099;
			this.scbvCampos.Minimum = 0;
			this.scbvCampos.Name = "scbvCampos";
			this.scbvCampos.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.scbvCampos.Size = new System.Drawing.Size(17, 313);
			this.scbvCampos.SmallChange = 10;
			this.scbvCampos.TabIndex = 7;
			this.scbvCampos.TabStop = false;
			this.scbvCampos.Value = 0;
			this.scbvCampos.Visible = true;
			this.scbvCampos.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scbvCampos_Scroll);
			// 
			// frmBotones
			// 
			this.frmBotones.Controls.Add(this.frmConsulta);
			this.frmBotones.Controls.Add(this._cbBotones_2);
			this.frmBotones.Controls.Add(this._cbBotones_1);
			this.frmBotones.Controls.Add(this._cbBotones_0);
			this.frmBotones.Enabled = true;
			this.frmBotones.Location = new System.Drawing.Point(656, 248);
			this.frmBotones.Name = "frmBotones";
			this.frmBotones.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmBotones.Size = new System.Drawing.Size(99, 193);
			this.frmBotones.TabIndex = 5;
			this.frmBotones.Visible = true;
			// 
			// frmConsulta
			// 
			this.frmConsulta.Controls.Add(this._rbConsulta_1);
			this.frmConsulta.Controls.Add(this._rbConsulta_0);
			this.frmConsulta.Enabled = true;
			this.frmConsulta.Location = new System.Drawing.Point(10, 126);
			this.frmConsulta.Name = "frmConsulta";
			this.frmConsulta.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmConsulta.Size = new System.Drawing.Size(85, 59);
			this.frmConsulta.TabIndex = 8;
			this.frmConsulta.Text = "Modo consulta";
			this.frmConsulta.Visible = false;
			// 
			// _rbConsulta_1
			// 
			this._rbConsulta_1.CausesValidation = true;
			this._rbConsulta_1.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._rbConsulta_1.IsChecked = true;
			this._rbConsulta_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._rbConsulta_1.Enabled = true;
			this._rbConsulta_1.Location = new System.Drawing.Point(18, 14);
			this._rbConsulta_1.Name = "_rbConsulta_1";
			this._rbConsulta_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._rbConsulta_1.Size = new System.Drawing.Size(51, 21);
			this._rbConsulta_1.TabIndex = 10;
			this._rbConsulta_1.TabStop = true;
			this._rbConsulta_1.Text = "A y B";
			this._rbConsulta_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._rbConsulta_1.Visible = true;
			this._rbConsulta_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbConsulta_CheckedChanged);
			// 
			// _rbConsulta_0
			// 
			this._rbConsulta_0.CausesValidation = true;
			this._rbConsulta_0.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._rbConsulta_0.IsChecked = false;
			this._rbConsulta_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._rbConsulta_0.Enabled = true;
			this._rbConsulta_0.Location = new System.Drawing.Point(18, 38);
			this._rbConsulta_0.Name = "_rbConsulta_0";
			this._rbConsulta_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._rbConsulta_0.Size = new System.Drawing.Size(47, 15);
			this._rbConsulta_0.TabIndex = 9;
			this._rbConsulta_0.TabStop = true;
			this._rbConsulta_0.Text = "A � B";
			this._rbConsulta_0.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this._rbConsulta_0.Visible = true;
			this._rbConsulta_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbConsulta_CheckedChanged);
			// 
			// _cbBotones_2
			// 
			this._cbBotones_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbBotones_2.Enabled = false;
			this._cbBotones_2.Location = new System.Drawing.Point(14, 88);
			this._cbBotones_2.Name = "_cbBotones_2";
			this._cbBotones_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbBotones_2.Size = new System.Drawing.Size(81, 32);
			this._cbBotones_2.TabIndex = 3;
			this._cbBotones_2.Text = "&Limpiar";
			this._cbBotones_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cbBotones_2.Click += new System.EventHandler(this.cbBotones_Click);
            // 
            // _cbBotones_2
            // 
            this._cbBotones_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._cbBotones_2.Enabled = false;
            this._cbBotones_2.Location = new System.Drawing.Point(14, 88);
            this._cbBotones_2.Name = "_cbBotones_2";
            this._cbBotones_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._cbBotones_2.Size = new System.Drawing.Size(81, 32);
            this._cbBotones_2.TabIndex = 3;
            this._cbBotones_2.Text = "&Limpiar";
            this._cbBotones_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this._cbBotones_2.Click += new System.EventHandler(this.cbBotones_Click);
            // 
            // _cbBotones_1
            // 
            this._cbBotones_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbBotones_1.Enabled = false;
			this._cbBotones_1.Location = new System.Drawing.Point(14, 50);
			this._cbBotones_1.Name = "_cbBotones_1";
			this._cbBotones_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbBotones_1.Size = new System.Drawing.Size(81, 32);
			this._cbBotones_1.TabIndex = 2;
			this._cbBotones_1.Text = "&Cancelar";
			this._cbBotones_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cbBotones_1.Click += new System.EventHandler(this.cbBotones_Click);
			// 
			// _cbBotones_0
			// 
			this._cbBotones_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbBotones_0.Enabled = false;
			this._cbBotones_0.Location = new System.Drawing.Point(14, 12);
			this._cbBotones_0.Name = "_cbBotones_0";
			this._cbBotones_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbBotones_0.Size = new System.Drawing.Size(81, 32);
			this._cbBotones_0.TabIndex = 1;
			this._cbBotones_0.Text = "&Aceptar";
			this._cbBotones_0.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cbBotones_0.Click += new System.EventHandler(this.cbBotones_Click);
			// 
			// frmSpread
			// 
			this.frmSpread.Controls.Add(this.sprMultiple);
			this.frmSpread.Controls.Add(this._cbSpread_0);
			this.frmSpread.Controls.Add(this._cbSpread_3);
			this.frmSpread.Controls.Add(this._cbSpread_4);
			this.frmSpread.Controls.Add(this._cbSpread_7);
			this.frmSpread.Controls.Add(this._cbSpread_8);
			this.frmSpread.Controls.Add(this._cbSpread_6);
			this.frmSpread.Controls.Add(this._cbSpread_5);
			this.frmSpread.Controls.Add(this._cbSpread_1);
			this.frmSpread.Controls.Add(this._cbSpread_2);
			this.frmSpread.Enabled = true;
			this.frmSpread.Location = new System.Drawing.Point(1, -2);
			this.frmSpread.Name = "frmSpread";
			this.frmSpread.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmSpread.Size = new System.Drawing.Size(755, 248);
			this.frmSpread.TabIndex = 4;
			this.frmSpread.Visible = true;
			// 
			// sprMultiple
			// 
			this.sprMultiple.Location = new System.Drawing.Point(8, 12);
			this.sprMultiple.Name = "sprMultiple";
			this.sprMultiple.Size = new System.Drawing.Size(655, 225);
			this.sprMultiple.TabIndex = 19;
			//this.sprMultiple.CellClick += new UpgradeHelpers.FpSpread.CellClickEventHandler(this.sprMultiple_CellClick);
			//this.sprMultiple.CellDoubleClick += new FarPoint.Win.Spread.CellClickEventHandler(this.sprMultiple_CellDoubleClick);
			this.sprMultiple.KeyDown += new System.Windows.Forms.KeyEventHandler(this.sprMultiple_KeyDown);
            this.sprMultiple.TopLeftChange += new UpgradeHelpers.Spread.TopLeftChangeEventHandler(this.sprMultiple_TopLeftChange);
			// 
			// _cbSpread_0
			// 
			this._cbSpread_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbSpread_0.Location = new System.Drawing.Point(668, 11);
			this._cbSpread_0.Name = "_cbSpread_0";
			this._cbSpread_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbSpread_0.Size = new System.Drawing.Size(81, 26);
			this._cbSpread_0.TabIndex = 27;
			this._cbSpread_0.Text = "&Incorporar";
			this._cbSpread_0.Click += new System.EventHandler(this.cbSpread_Click);
			// 
			// _cbSpread_3
			// 
			this._cbSpread_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbSpread_3.Enabled = false;
			this._cbSpread_3.Location = new System.Drawing.Point(668, 108);
			this._cbSpread_3.Name = "_cbSpread_3";
			this._cbSpread_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbSpread_3.Size = new System.Drawing.Size(81, 26);
			this._cbSpread_3.TabIndex = 26;
			this._cbSpread_3.Text = "C&onsultar";
			this._cbSpread_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cbSpread_3.Click += new System.EventHandler(this.cbSpread_Click);
			// 
			// _cbSpread_4
			// 
			this._cbSpread_4.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbSpread_4.Enabled = false;
			this._cbSpread_4.Location = new System.Drawing.Point(668, 140);
			this._cbSpread_4.Name = "_cbSpread_4";
			this._cbSpread_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbSpread_4.Size = new System.Drawing.Size(81, 26);
			this._cbSpread_4.TabIndex = 25;
			this._cbSpread_4.Text = "Im&primir";
			this._cbSpread_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cbSpread_4.Click += new System.EventHandler(this.cbSpread_Click);
			// 
			// _cbSpread_7
			// 
			this._cbSpread_7.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbSpread_7.Location = new System.Drawing.Point(668, 173);
			this._cbSpread_7.Name = "_cbSpread_7";
			this._cbSpread_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbSpread_7.Size = new System.Drawing.Size(81, 26);
			this._cbSpread_7.TabIndex = 22;
			this._cbSpread_7.Text = "&Salir";
			this._cbSpread_7.Click += new System.EventHandler(this.cbSpread_Click);
			// 
			// _cbSpread_8
			// 
			this._cbSpread_8.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbSpread_8.Location = new System.Drawing.Point(668, 205);
			this._cbSpread_8.Name = "_cbSpread_8";
			this._cbSpread_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbSpread_8.Size = new System.Drawing.Size(81, 26);
			this._cbSpread_8.TabIndex = 21;
			this._cbSpread_8.Text = "&Ayuda";
			this._cbSpread_8.Click += new System.EventHandler(this.cbSpread_Click);
			// 
			// _cbSpread_6
			// 
			this._cbSpread_6.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbSpread_6.Location = new System.Drawing.Point(668, 43);
			this._cbSpread_6.Name = "_cbSpread_6";
			this._cbSpread_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbSpread_6.Size = new System.Drawing.Size(81, 26);
			this._cbSpread_6.TabIndex = 23;
			this._cbSpread_6.Text = "Ca&ncelar Selecci�n";
			this._cbSpread_6.Visible = false;
			this._cbSpread_6.Click += new System.EventHandler(this.cbSpread_Click);
			// 
			// _cbSpread_5
			// 
			this._cbSpread_5.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbSpread_5.Location = new System.Drawing.Point(668, 76);
			this._cbSpread_5.Name = "_cbSpread_5";
			this._cbSpread_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbSpread_5.Size = new System.Drawing.Size(81, 26);
			this._cbSpread_5.TabIndex = 24;
			this._cbSpread_5.Text = "Aceptar &Selecci�n";
			this._cbSpread_5.Visible = false;
			this._cbSpread_5.Click += new System.EventHandler(this.cbSpread_Click);
			// 
			// _cbSpread_1
			// 
			this._cbSpread_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbSpread_1.Enabled = false;
			this._cbSpread_1.Location = new System.Drawing.Point(668, 43);
			this._cbSpread_1.Name = "_cbSpread_1";
			this._cbSpread_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbSpread_1.Size = new System.Drawing.Size(81, 26);
			this._cbSpread_1.TabIndex = 29;
			this._cbSpread_1.Text = "&Eliminar";
			this._cbSpread_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cbSpread_1.Click += new System.EventHandler(this.cbSpread_Click);
			// 
			// _cbSpread_2
			// 
			this._cbSpread_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._cbSpread_2.Enabled = false;
			this._cbSpread_2.Location = new System.Drawing.Point(668, 77);
			this._cbSpread_2.Name = "_cbSpread_2";
			this._cbSpread_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cbSpread_2.Size = new System.Drawing.Size(81, 25);
			this._cbSpread_2.TabIndex = 28;
			this._cbSpread_2.Text = "&Modificar";
			this._cbSpread_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._cbSpread_2.Click += new System.EventHandler(this.cbSpread_Click);
			// 
			// SMT100F2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this._cbBotones_1;
			this.ClientSize = new System.Drawing.Size(759, 575);
			this.ControlBox = false;
			this.Controls.Add(this.frmDetalleFondo);
			this.Controls.Add(this.frmBotones);
			this.Controls.Add(this.frmSpread);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SMT100F2";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "SMT100F1 - Mantenimiento de :";
			this.Visible = false;
			this.Activated += new System.EventHandler(this.SMT100F2_Activated);
			base.Closing += new System.ComponentModel.CancelEventHandler(this.SMT100F2_Closed);
			this.Load += new System.EventHandler(this.SMT100F2_Load);
			this.frmDetalleFondo.ResumeLayout(false);
			this.FrmDetalleMedio.ResumeLayout(false);
			this.frmDetalle.ResumeLayout(false);
			this._TlbBuscar_0.ResumeLayout(false);
			this.frmBotones.ResumeLayout(false);
			this.frmConsulta.ResumeLayout(false);
			this.frmSpread.ResumeLayout(false);
			this.ResumeLayout(false);
		}

        void ReLoadForm(bool addEvents)
		{
			InitializetbTexto();
			InitializetbMultilinea();
			InitializesdcFecha();
			InitializerbConsulta();
			InitializemebMascara();
			InitializelbEtiqueta();
			InitializecbbCombo();
			InitializecbSpread();
			InitializecbBotones();
			InitializeTlbBuscar();
			InitializeTbClaves();
			InitializeCbb_AlgoNada();
		}
		void InitializetbTexto()
		{
			this.tbTexto = new Telerik.WinControls.UI.RadTextBoxControl[1];
			this.tbTexto[0] = _tbTexto_0;
		}
		void InitializetbMultilinea()
		{
			this.tbMultilinea = new Telerik.WinControls.UI.RadTextBoxControl[1];
			this.tbMultilinea[0] = _tbMultilinea_0;
		}
		void InitializesdcFecha()
		{
			this.sdcFecha = new Telerik.WinControls.UI.RadDateTimePicker[1];
			this.sdcFecha[0] = _sdcFecha_0;
		}
		void InitializerbConsulta()
		{
			this.rbConsulta = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbConsulta[1] = _rbConsulta_1;
			this.rbConsulta[0] = _rbConsulta_0;
		}
		void InitializemebMascara()
		{
			this.mebMascara = new Telerik.WinControls.UI.RadMaskedEditBox[1];
			this.mebMascara[0] = _mebMascara_0;
		}
		void InitializelbEtiqueta()
		{
			this.lbEtiqueta = new Telerik.WinControls.UI.RadLabel[1];
			this.lbEtiqueta[0] = _lbEtiqueta_0;
		}
		void InitializecbbCombo()
		{
			this.cbbCombo = new Telerik.WinControls.UI.RadDropDownList[1];
			this.cbbCombo[0] = _cbbCombo_0;
		}
		void InitializecbSpread()
		{
			this.cbSpread = new Telerik.WinControls.UI.RadButton[9];
			this.cbSpread[0] = _cbSpread_0;
			this.cbSpread[3] = _cbSpread_3;
			this.cbSpread[4] = _cbSpread_4;
			this.cbSpread[7] = _cbSpread_7;
			this.cbSpread[8] = _cbSpread_8;
			this.cbSpread[6] = _cbSpread_6;
			this.cbSpread[5] = _cbSpread_5;
			this.cbSpread[1] = _cbSpread_1;
			this.cbSpread[2] = _cbSpread_2;
		}
		void InitializecbBotones()
		{
			this.cbBotones = new Telerik.WinControls.UI.RadButton[3];
			this.cbBotones[2] = _cbBotones_2;
			this.cbBotones[1] = _cbBotones_1;
			this.cbBotones[0] = _cbBotones_0;
		}
		void InitializeTlbBuscar()
		{
			this.TlbBuscar = new System.Windows.Forms.ToolStrip[1];
			this.TlbBuscar[0] = _TlbBuscar_0;
		}
		void InitializeTbClaves()
		{
			this.TbClaves = new Telerik.WinControls.UI.RadTextBoxControl[1];
			this.TbClaves[0] = _TbClaves_0;
		}
		void InitializeCbb_AlgoNada()
		{
			this.Cbb_AlgoNada = new Telerik.WinControls.UI.RadDropDownList[1];
			this.Cbb_AlgoNada[0] = _Cbb_AlgoNada_0;
		}
		#endregion
	}
}