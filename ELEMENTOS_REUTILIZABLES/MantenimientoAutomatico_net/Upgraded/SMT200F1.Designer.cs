using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace MantenAutomatico
{
	partial class SMT200F1
	{

		#region "Upgrade Support "
		private static SMT200F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static SMT200F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new SMT200F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "tbNuevaContraseña", "cbAyuda", "tbRepetNContraseña", "cbCancelar", "cbAceptar", "lbDescripcion", "lbRepNContraseña", "lbNContraseña"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadTextBoxControl tbNuevaContraseña;
		public Telerik.WinControls.UI.RadButton cbAyuda;
		public Telerik.WinControls.UI.RadTextBoxControl tbRepetNContraseña;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadLabel lbDescripcion;
		public Telerik.WinControls.UI.RadLabel lbRepNContraseña;
		public Telerik.WinControls.UI.RadLabel lbNContraseña;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.tbNuevaContraseña = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbAyuda = new Telerik.WinControls.UI.RadButton();
            this.tbRepetNContraseña = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.lbDescripcion = new Telerik.WinControls.UI.RadLabel();
            this.lbRepNContraseña = new Telerik.WinControls.UI.RadLabel();
            this.lbNContraseña = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tbNuevaContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAyuda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRepetNContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRepNContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tbNuevaContraseña
            // 
            this.tbNuevaContraseña.AcceptsReturn = true;
            this.tbNuevaContraseña.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNuevaContraseña.Enabled = false;
            this.tbNuevaContraseña.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tbNuevaContraseña.Location = new System.Drawing.Point(188, 48);
            this.tbNuevaContraseña.MaxLength = 0;
            this.tbNuevaContraseña.Name = "tbNuevaContraseña";
            this.tbNuevaContraseña.PasswordChar = '*';
            this.tbNuevaContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNuevaContraseña.Size = new System.Drawing.Size(67, 19);
            this.tbNuevaContraseña.TabIndex = 7;
            // 
            // cbAyuda
            // 
            this.cbAyuda.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAyuda.Location = new System.Drawing.Point(193, 116);
            this.cbAyuda.Name = "cbAyuda";
            this.cbAyuda.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAyuda.Size = new System.Drawing.Size(81, 31);
            this.cbAyuda.TabIndex = 3;
            this.cbAyuda.Text = "A&yuda";
            // 
            // tbRepetNContraseña
            // 
            this.tbRepetNContraseña.AcceptsReturn = true;
            this.tbRepetNContraseña.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbRepetNContraseña.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tbRepetNContraseña.Location = new System.Drawing.Point(188, 80);
            this.tbRepetNContraseña.MaxLength = 8;
            this.tbRepetNContraseña.Name = "tbRepetNContraseña";
            this.tbRepetNContraseña.PasswordChar = '*';
            this.tbRepetNContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRepetNContraseña.Size = new System.Drawing.Size(67, 19);
            this.tbRepetNContraseña.TabIndex = 0;
            this.tbRepetNContraseña.Leave += new System.EventHandler(this.tbRepetNContraseña_Leave);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(106, 116);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 31);
            this.cbCancelar.TabIndex = 2;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(18, 116);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 31);
            this.cbAceptar.TabIndex = 1;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // lbDescripcion
            // 
            this.lbDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDescripcion.Location = new System.Drawing.Point(12, 12);
            this.lbDescripcion.Name = "lbDescripcion";
            this.lbDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDescripcion.Size = new System.Drawing.Size(269, 18);
            this.lbDescripcion.TabIndex = 6;
            this.lbDescripcion.Text = "Confirme la Nueva Contraseña tecleándola de nuevo";
            // 
            // lbRepNContraseña
            // 
            this.lbRepNContraseña.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbRepNContraseña.Location = new System.Drawing.Point(18, 82);
            this.lbRepNContraseña.Name = "lbRepNContraseña";
            this.lbRepNContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbRepNContraseña.Size = new System.Drawing.Size(139, 18);
            this.lbRepNContraseña.TabIndex = 5;
            this.lbRepNContraseña.Text = "Repetir Nueva Contraseña:";
            // 
            // lbNContraseña
            // 
            this.lbNContraseña.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNContraseña.Location = new System.Drawing.Point(18, 50);
            this.lbNContraseña.Name = "lbNContraseña";
            this.lbNContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNContraseña.Size = new System.Drawing.Size(100, 18);
            this.lbNContraseña.TabIndex = 4;
            this.lbNContraseña.Text = "Nueva Contraseña:";
            // 
            // SMT200F1
            // 
            this.AcceptButton = this.cbCancelar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 154);
            this.Controls.Add(this.tbNuevaContraseña);
            this.Controls.Add(this.cbAyuda);
            this.Controls.Add(this.tbRepetNContraseña);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.lbDescripcion);
            this.Controls.Add(this.lbRepNContraseña);
            this.Controls.Add(this.lbNContraseña);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SMT200F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CONFIRMAR CONTRASEÑA - SMT200F1";
            this.Closed += new System.EventHandler(this.SMT200F1_Closed);
            ((System.ComponentModel.ISupportInitialize)(this.tbNuevaContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAyuda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRepetNContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRepNContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}