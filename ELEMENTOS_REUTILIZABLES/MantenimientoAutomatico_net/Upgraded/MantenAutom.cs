using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using System.Transactions;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MantenAutomatico
{
    internal static class ModuloMantenim
    {


        public static SqlConnection RcManten = null;
        public static DataSet RsManten = null;
        public static DataSet RsManten2 = null;

        public static SqlConnection RcError = null;

        // ralopezn_TODO_X_5 18/05/2016
        //public static Workspace Wk = null;
        //public static Database DB = null;
        //public static Recordset DBInforme = null;
        public static string stInforme = String.Empty;
        //variable para recoger de SCONSGLO el m�x.de filas
        public static int vMaximoFilas = 0;
        //****************************
        //Tengo que mirarla para ver si es global
        public static string stSQLOrder = String.Empty;
        //****************************

        public static int iIndiceBuscar = 0;

        public static string vstUsuario_NT = String.Empty;
        public static FixedLengthString vstModo = new FixedLengthString(1);

        public static Mensajes.ClassMensajes Mensaje = null; //Set oClass = New Mensajes.ClassMensajes

        public struct StrMenus
        {
            public string StNombTabla;
            public string StNombreMenu;
            public short IIndiceModulo;
            public short IIndiceMenu;
            public string StDisponible;
            public static StrMenus CreateInstance()
            {
                StrMenus result = new StrMenus();
                result.StNombTabla = String.Empty;
                result.StNombreMenu = String.Empty;
                result.StDisponible = String.Empty;
                return result;
            }
        }

        public static string stConsulta = String.Empty;

        public static string vstGrupoHosp = String.Empty; //<<Grupo de Hospitales para informe
        public static string vstNombreHosp = String.Empty; //<<Nombre Hospital para el informe
        public static string vstDireccHosp = String.Empty; //<<Direcci�n del Hospital

        public struct ConexionControles
        {
            public string NombreTabla;
            public string NombreCampo;
            public Control NombreControl;
            public short NumeroColumna;
            public static ConexionControles CreateInstance()
            {
                ConexionControles result = new ConexionControles();
                result.NombreTabla = String.Empty;
                result.NombreCampo = String.Empty;
                return result;
            }
        }

        public struct strConjuntoResultados
        {

            public Control NombreControl;
            public string gtablamanaut;
            public string dtablamancor;
            public string gcolumncaman;
            public string dcoltabmanco;
            public string dcoltabmanla;
            public short nordencoltab;
            public string gtipodato;
            public string gtipocolumna;
            public string nordencolpk;
            public short nparteentera;
            public short npartedecima;
            public string iadmitenull;
            public string valordefecto;
            public string itamaniofijo;
            public string vformato;
            public string vmascara;
            public string ipassword;
            public string imayusculas;
            public string ireqconsulta;
            public string icolformato;
            public string icolmascara;
            public short nordenacion;
            public string iascendente;
            public object icolfborrado;
            public short nordcolinun;
            public string ggrunimanaut;
            public bool fEsta;
            public static strConjuntoResultados CreateInstance()
            {
                strConjuntoResultados result = new strConjuntoResultados();
                result.gtablamanaut = String.Empty;
                result.dtablamancor = String.Empty;
                result.gcolumncaman = String.Empty;
                result.dcoltabmanco = String.Empty;
                result.dcoltabmanla = String.Empty;
                result.gtipodato = String.Empty;
                result.gtipocolumna = String.Empty;
                result.nordencolpk = String.Empty;
                result.iadmitenull = String.Empty;
                result.valordefecto = String.Empty;
                result.itamaniofijo = String.Empty;
                result.vformato = String.Empty;
                result.vmascara = String.Empty;
                result.ipassword = String.Empty;
                result.imayusculas = String.Empty;
                result.ireqconsulta = String.Empty;
                result.icolformato = String.Empty;
                result.icolmascara = String.Empty;
                result.iascendente = String.Empty;
                result.icolfborrado = String.Empty;
                result.ggrunimanaut = String.Empty;
                return result;
            }
        }

        static bool fDBTemporal = false;
        static bool fTTemporal = false;

        public const string stSignoDecimal = ","; //Utilizado en los campos m�scara
        public const int COiMaxTamText = 44; //M�ximo de X permitido para que sea campo texto normal
        public const int COiMaxTamLineaMultiline = 41; //M�ximo n� de caracteres X para cada l�nea del multiline
        public const int COiTamCaractEtiqueta = 105; //Por cada car�cter 100 twips de etiquetas
        public const int COiTamBordes = 105; //Lo que ocupan los bordes de un control en twips
        public static readonly int COiTamScrollBarMultiline = 3 * COiTamCaractEtiqueta; //Tama�o de la scrollBar vertical del multiline
        public const int COiIzda = 0; //120           'Principio de linea
        public const int COiDcha = 120; //Final de linea
        public const int COiSuperiorCampo = 150; //Sumar Top superior en campos
        public const int COiSupEtiqueta = 225; //Sumar Top en etiquetas
        public const int COiSeparacion = 135; //Separaci�n entre campos y etiquetas
        public const int COiDiferenciaControl = 45; //Diferencia entre etiqueta y texto
        public const int COiDesplazamTab = 13; //Desplazamiento del TabIndex para los controles que se creen
        public const int COiHeightMultiline3 = 735; //Height del multiline de 3 l�neas

        //Array que me sirve para tener las columnas de clave primaria y el
        //control al que est�n asociadas
        public struct Clave
        {
            public string StColumna;
            public string stTexto;
            public string StTipo;
            public short iLongitud;
            public short iOrdenColumnaSpread;
            public static Clave CreateInstance()
            {
                Clave result = new Clave();
                result.StColumna = String.Empty;
                result.stTexto = String.Empty;
                result.StTipo = String.Empty;
                return result;
            }
        }

        public static string stSQLManten = String.Empty;
        public static string StFBORRADO = String.Empty;

        //Path donde va a estar la Base de Datos
        public static string vStPathBD = String.Empty;
        //Nombre DSN temporal ACCESS
        //Public Const CoDSNTemp = "GHOSP_ACCESS"

        public static string CoDSNTemp = String.Empty;
        public static string NOMBRE_DSN_SQL = String.Empty;

        //Nombre Base de Datos
        public static string vStBaseDatos = String.Empty;
        //Path de los report
        public static string vStPathReport = String.Empty;

        //Constantes p�blicas para saber la precision de cada tipo de datos
        public const string CoIntegerPosi = "32767";

        public const string CoIntegerNega = "32768";

        public const string CoLongPosi = "2147483647";

        public const string CoLongNega = "2147483648";

        //********************************
        public struct StrOrden
        {
            public string StColumna;
            public short iLongitud;
            public string StTipo;
            public short iOrdenColumnaSpread;
            public string vDefecto;
            public static StrOrden CreateInstance()
            {
                StrOrden result = new StrOrden();
                result.StColumna = String.Empty;
                result.StTipo = String.Empty;
                result.vDefecto = String.Empty;
                return result;
            }
        }

        public struct StrParametro
        {
            public short iLongitud;
            public short iOrdenColumnaSpread;
            public string StTipo;
            public string StColumna;
            public static StrParametro CreateInstance()
            {
                StrParametro result = new StrParametro();
                result.StTipo = String.Empty;
                result.StColumna = String.Empty;
                return result;
            }
        }

        public const string CoMetaUsuario = "METAUSUA";

        public struct StrColumnasFK
        {
            public string StTablaHija;
            public string StColumnaHija;
            public string StNombreGrupoFK;
            public bool fEsta;
            public string Stgtipodato;
            public string Stdcoltabmanco;
            public Control NombreControl;
            public bool existe;
            public static StrColumnasFK CreateInstance()
            {
                StrColumnasFK result = new StrColumnasFK();
                result.StTablaHija = String.Empty;
                result.StColumnaHija = String.Empty;
                result.StNombreGrupoFK = String.Empty;
                result.Stgtipodato = String.Empty;
                result.Stdcoltabmanco = String.Empty;
                return result;
            }
        }
        //*************************************************************************
        public struct StrFKCombo
        {
            public string StColHija;
            public short iIndiceCombo;
            public short IIndiceColEnCombo;
            public string StLabelCombo;
            public string StTablaPadre;
            public string StColPadre;
            public string StNombreFK;
            public bool fVistaNula;
            public string StVistaCombo;
            public static StrFKCombo CreateInstance()
            {
                StrFKCombo result = new StrFKCombo();
                result.StColHija = String.Empty;
                result.StLabelCombo = String.Empty;
                result.StTablaPadre = String.Empty;
                result.StColPadre = String.Empty;
                result.StNombreFK = String.Empty;
                result.StVistaCombo = String.Empty;
                return result;
            }
        }

        public const string CoEsCombo = "Es columna de combo y no la utilizo para comparar";

        public struct StrFK
        {
            public string StColHija;
            public string StColPadre;
            public string StTablaPadre;
            public bool fVistaNula;
            public string stTipoDato;
            public string StLabelCombo;
            public string StVistaCombo;
            public string StNombreFK;
            public static StrFK CreateInstance()
            {
                StrFK result = new StrFK();
                result.StColHija = String.Empty;
                result.StColPadre = String.Empty;
                result.StTablaPadre = String.Empty;
                result.stTipoDato = String.Empty;
                result.StLabelCombo = String.Empty;
                result.StVistaCombo = String.Empty;
                result.StNombreFK = String.Empty;
                return result;
            }
        }
        //*************************************************************************
        public struct StrOrdenFK
        {
            public string StColumna;
            public string StAscendente;
            public static StrOrdenFK CreateInstance()
            {
                StrOrdenFK result = new StrOrdenFK();
                result.StColumna = String.Empty;
                result.StAscendente = String.Empty;
                return result;
            }
        }

        public struct StrToolbar
        {
            public Control NombreControl;
            public Control ComboAsociado;
            public Control ClaveAsociada;
            public static StrToolbar CreateInstance()
            {
                StrToolbar result = new StrToolbar();
                return result;
            }
        }

        public static dynamic cryCrystal = null;
        public static ClassMantenimiento ClassManten = null;

        public struct StrFKUnicos
        {
            public string StColHija;
            public string StLabelCombo;
            public static StrFKUnicos CreateInstance()
            {
                StrFKUnicos result = new StrFKUnicos();
                result.StColHija = String.Empty;
                result.StLabelCombo = String.Empty;
                return result;
            }
        }

        //***********************************
        public const string CoLoginAdminSQL = "sa";
        public const string CoPasswordAdminSQL = "";
        static SqlConnection RcSA = null;
        public static SqlConnection Rc2 = null;

        public static string gstUsuario = String.Empty; //Usuario para SQL SERVER
        public static string gstPassword = String.Empty; //Password para SQL SERVER

        public static string gstPantallaOrigen = String.Empty; //para guardar la pantalla llamante

        internal static bool EstablecerSegundaConexion()
        {
            bool fError = false;

            string stLetra = "S";

            Conexion.Obtener_conexion Instancia2 = new Conexion.Obtener_conexion();
            string tempRefParam = String.Empty;
            string tempRefParam2 = String.Empty;
            string tempRefParam3 = String.Empty;
            string tempRefParam4 = String.Empty;
            Instancia2.Conexion(ref Rc2, stLetra, vstUsuario_NT, ref fError, ref gstUsuario, ref gstPassword, Serrores.VVstServerActual, Serrores.VVstDriver, Serrores.VVstBasedatosActual, Serrores.VVstUsuarioBD, Serrores.VVstPasswordBD, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4);

            if (!fError)
            {
                RadMessageBox.Show("ERROR DE CONEXION", Application.ProductName);
                return false;
            }
            else
            {
                return true;
            }

        }

        internal static void RegistrarDSN()
        {

            string tstConexion = "Description=" + "BASE TEMPORAL EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + vStPathBD + "\\" + vStBaseDatos;
            //UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(CoDSNTemp, "Microsoft Access Driver (*.mdb)", true, tstConexion);

        }

        private static bool EstablecerConexionConSA()
        {
            bool fError = false;

            string stLetra = "S";

            Conexion.Obtener_conexion Instancia = new Conexion.Obtener_conexion();
            string tempRefParam = CoLoginAdminSQL;
            string tempRefParam2 = CoPasswordAdminSQL;
            Instancia.ConexionAdminSQL(ref RcSA, stLetra, vstUsuario_NT, ref fError, tempRefParam, ref tempRefParam2);//, Serrores.VVstServerActual, Serrores.VVstDriver, Serrores.VVstBasedatosActual, Serrores.VVstUsuarioBD, Serrores.VVstPasswordBD);

            if (!fError)
            {
                RadMessageBox.Show("ERROR DE CONEXION", Application.ProductName);
                return false;
            }
            else
            {
                return true;
            }

        }


        internal static bool HabilitarConstraintsModificacion(string stNombreTabla, DataSet Cursor)
        {
            bool result = false;
            StringBuilder StSQL = new StringBuilder();
            DataSet RrsObtenerSelect = null;
            DataSet RrsMulticonsulta = null;
            result = true;
            //If Not EstablecerConexionConSA Then
            //    HabilitarConstraintsModificacion = False
            //    Exit Function
            //End If
            try
            {
                using (TransactionScope transaction = TransScopeBuilder.CreateReadCommited())
                {
                    //Habilito las constraints
                    StSQL = new StringBuilder("select " + "'ALTER TABLE ' +db_name(r.fkeydbid) + '.' + " + "isNull(rtrim( " + "(select user_name(o.uid) " + "from sysobjects o " + "where o.id = r.fkeyid) " + "),'') " + "+ '.' + object_name(r.fkeyid) " + "+ ' CHECK CONSTRAINT ' + object_name(r.constid)+';' " + "from      sysreferences r " + "where     r.rkeyid = " + "object_id('" + stNombreTabla + "') " + "order by  1 ");

                    SqlDataAdapter tempAdapter = new SqlDataAdapter(StSQL.ToString(), RcSA);
                    RrsObtenerSelect = new DataSet();
                    tempAdapter.Fill(RrsObtenerSelect);

                    StSQL = new StringBuilder("");
                    foreach (DataRow iteration_row in RrsObtenerSelect.Tables[0].Rows)
                    {
                        StSQL.Append(Convert.ToString(iteration_row[0]));
                    }

                    RrsObtenerSelect.Close();

                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSQL.ToString(), RcSA);
                    RrsMulticonsulta = new DataSet();
                    tempAdapter_2.Fill(RrsMulticonsulta);

                    RrsMulticonsulta.Close();


                    transaction.Complete();
                    RcSA.Close();

                    return result;
                }
            }
            catch (SqlException ex)
            {
                var errors = new RDO_rdoErrors(ex);
                foreach (SqlError er in errors.rdoErrors)
                {
                    //GestionErrorBaseDatos(rdoErrors.number)
                    RadMessageBox.Show(er.Number.ToString() + "-" + er.Message, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
                }
                errors.Clear();
                result = false;
                RcSA.Close();
                return result;
            }
            catch (Exception ex)
            {
                result = false;
                RadMessageBox.Show(ex.Message, Application.ProductName);
                RcSA.Close();
                return result;
            }
        }

        internal static bool DesHabilitarConstraintsModificacion(string stNombreTabla, DataSet Cursor)
        {
            bool result = false;
            StringBuilder StSQL = new StringBuilder();
            DataSet RrsObtenerSelect = null;
            DataSet RrsMulticonsulta = null;
            result = true;
            if (!EstablecerConexionConSA())
            {
                return false;
            }

            try
            {
                using (TransactionScope transaction = TransScopeBuilder.CreateReadCommited())
                {
                    //*******************************************************
                    //Deshabilito las Constraints
                    StSQL = new StringBuilder("select " + "'ALTER TABLE ' +db_name(r.fkeydbid) + '.' + " + "isNull(rtrim( " + "(select user_name(o.uid) " + "from sysobjects o " + "where o.id = r.fkeyid) " + "),'') " + "+ '.' + object_name(r.fkeyid) " + "+ ' NOCHECK CONSTRAINT ' + object_name(r.constid)+';' " + "from      sysreferences r " + "where     r.rkeyid = " + "object_id('" + stNombreTabla + "') " + "order by  1 ");

                    SqlDataAdapter tempAdapter = new SqlDataAdapter(StSQL.ToString(), RcSA);
                    RrsObtenerSelect = new DataSet();
                    tempAdapter.Fill(RrsObtenerSelect);

                    StSQL = new StringBuilder("");
                    foreach (DataRow iteration_row in RrsObtenerSelect.Tables[0].Rows)
                    {
                        StSQL.Append(Convert.ToString(iteration_row[0]));
                    }
                    RrsObtenerSelect.Close();

                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSQL.ToString(), RcSA);
                    RrsMulticonsulta = new DataSet();
                    tempAdapter_2.Fill(RrsMulticonsulta);
                    RrsMulticonsulta.Close();
                    transaction.Complete();
                    return result;
                }
            }
            catch (SqlException ex)
            {
                var errors = new RDO_rdoErrors(ex);
                foreach (SqlError er in errors.rdoErrors)
                {
                    //GestionErrorBaseDatos(rdoErrors.number)
                    RadMessageBox.Show(er.Number.ToString() + "-" + er.Message, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
                }
                errors.Clear();
                result = false;
                return result;
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(ex.Message, Application.ProductName);
                result = false;
                return result;
            }
        }

        internal static void DeshabilitarHabilitarConstraints(string stNombreTabla, string stSQLBorrar, dynamic Formulario)
        {
            StringBuilder StSQL = new StringBuilder();
            DataSet RrsObtenerSelect = null;
            DataSet RrsMulticonsulta = null;
            DataSet RrsBorrar = null;

            if (!EstablecerConexionConSA())
            {
                return;
            }

            try
            {
                using (TransactionScope transaction = TransScopeBuilder.CreateReadCommited())
                {

                    //*******************************************************
                    //Deshabilito las Constraints
                    StSQL = new StringBuilder("select " + "'ALTER TABLE ' +db_name(r.fkeydbid) + '.' + " + "isNull(rtrim( " + "(select user_name(o.uid) " + "from sysobjects o " + "where o.id = r.fkeyid) " + "),'') " + "+ '.' + object_name(r.fkeyid) " + "+ ' NOCHECK CONSTRAINT ' + object_name(r.constid)+';' " + "from      sysreferences r " + "where     r.rkeyid = " + "object_id('" + stNombreTabla + "') " + "order by  1 ");

                    SqlDataAdapter tempAdapter = new SqlDataAdapter(StSQL.ToString(), RcSA);
                    RrsObtenerSelect = new DataSet();
                    tempAdapter.Fill(RrsObtenerSelect);

                    StSQL = new StringBuilder("");
                    foreach (DataRow iteration_row in RrsObtenerSelect.Tables[0].Rows)
                    {
                        StSQL.Append(Convert.ToString(iteration_row[0]));
                    }

                    RrsObtenerSelect.Close();

                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSQL.ToString(), RcSA);
                    RrsMulticonsulta = new DataSet();
                    tempAdapter_2.Fill(RrsMulticonsulta);

                    RrsMulticonsulta.Close();

                    //*******************************************************
                    //Borro el registro que se hab�a seleccionado para borrar

                    SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSQLBorrar, RcSA);
                    RrsBorrar = new DataSet();
                    tempAdapter_3.Fill(RrsBorrar);

                    RrsBorrar.Delete(tempAdapter_3);

                    RrsBorrar.Close();

                    //*******************************************************
                    //Habilito las constraints

                    StSQL = new StringBuilder("select " + "'ALTER TABLE ' +db_name(r.fkeydbid) + '.' + " + "isNull(rtrim( " + "(select user_name(o.uid) " + "from sysobjects o " + "where o.id = r.fkeyid) " + "),'') " + "+ '.' + object_name(r.fkeyid) " + "+ ' CHECK CONSTRAINT ' + object_name(r.constid)+';' " + "from      sysreferences r " + "where     r.rkeyid = " + "object_id('" + stNombreTabla + "') " + "order by  1 ");

                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSQL.ToString(), RcSA);
                    RrsObtenerSelect = new DataSet();
                    tempAdapter_4.Fill(RrsObtenerSelect);

                    StSQL = new StringBuilder("");
                    foreach (DataRow iteration_row_2 in RrsObtenerSelect.Tables[0].Rows)
                    {
                        StSQL.Append(Convert.ToString(iteration_row_2[0]));
                    }

                    RrsObtenerSelect.Close();

                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSQL.ToString(), RcSA);
                    RrsMulticonsulta = new DataSet();
                    tempAdapter_5.Fill(RrsMulticonsulta);

                    RrsMulticonsulta.Close();


                    transaction.Complete();
                    RcSA.Close();

                    //Borrar de la spread el registro
                    Formulario.sprMultiple.Action = 5;
                    Formulario.sprMultiple.MaxRows = Convert.ToDouble(Formulario.sprMultiple.MaxRows) - 1;
                }
            }
            catch (SqlException ex)
            {
                var errors = new RDO_rdoErrors(ex);
                foreach (SqlError er in errors.rdoErrors)
                {
                    //GestionErrorBaseDatos(rdoErrors.number)
                    RadMessageBox.Show(er.Number.ToString() + "-" + er.Message, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
                }

                errors.Clear();

                RcSA.Close();
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(ex.Message, Application.ProductName);
                RcSA.Close();
            }

        }

        internal static void DarFoco(string StColumna, ModuloMantenim.StrFKCombo[] AStColFKCombo, bool fMensaje, SMT100F2 Mantenimiento)
        {
            object[] cbbCombo = null;

            foreach (ModuloMantenim.StrFKCombo AStColFKCombo_item in AStColFKCombo)
            {
                if (StColumna.Trim() == AStColFKCombo_item.StColHija.Trim())
                {
                    if (fMensaje)
                    {
                        //MENSAJE:Campo V1 requerido.
                        short tempRefParam = 1040;
                        string[] tempRefParam2 = new string[] { AStColFKCombo_item.StLabelCombo.Trim() };
                        Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcManten, tempRefParam2);
                    }
                    if (Mantenimiento.cbbCombo[AStColFKCombo_item.iIndiceCombo].Enabled)
                    {
                        Mantenimiento.cbbCombo[AStColFKCombo_item.iIndiceCombo].Focus();
                    }
                    break;
                }
            }

        }



        internal static bool ComprobarCaracteres(ref string stTexto)
        {
            //VALIDA QUE LOS CARACTERES DE PASSWORD SEAN ASCII EDITABLES
            int[] entrada = new int[stTexto.Length + 1];
            stTexto = stTexto.ToUpper();
            for (int iI = 1; iI <= stTexto.Length; iI++)
            {
                entrada[iI] = Strings.Asc(stTexto.Substring(iI - 1, Math.Min(1, stTexto.Length - (iI - 1)))[0]);
                if (!(entrada[iI] == 32 || (entrada[iI] >= 48 && entrada[iI] <= 57) || (entrada[iI] >= 65 && entrada[iI] <= 90) || entrada[iI] == 209))
                {
                    return false;
                }
            }
            return true;

        }

        internal static string CambiarContrase�a(ref string StContrase�a)
        {
            //Rutina para modificar la contrase�a del usuario de la aplicaci�n
            //encript�ndola.

            return EncriptarPassword(ref StContrase�a);


        }

        internal static string EncriptarPassword(ref string StContrase�a)
        {

            int[] arraydesp = new int[9];
            int[] entrada = new int[9];
            string[] salida = ArraysHelper.InitializeArray<string>(9);
            StringBuilder stDevolver = new StringBuilder();

            //el n�mero m�ximo de desplazamiento se debe fijar a 37
            //para que el cociente siempre sea 1 para poder
            //desencriptar partiendo del resto
            //(fijando el cociente siempre a 1).
            arraydesp[1] = 10;
            arraydesp[2] = 13;
            arraydesp[3] = 31;
            arraydesp[4] = 14;
            arraydesp[5] = 1;
            arraydesp[6] = 22;
            arraydesp[7] = 34;
            arraydesp[8] = 8;
            //Pasamos a may�sculas la contrase�a
            StContrase�a = StContrase�a.ToUpper();
            //metemos espacios a la derecha para almacenar como password siempre 8 caracteres
            if (StContrase�a.Length < 8)
            {
                StContrase�a = StContrase�a + new String(' ', 8 - StContrase�a.Length);
            }
            //vamos recortando la contrase�a palabra por palabra
            for (int i = 1; i <= 8; i++)
            {
                entrada[i] = Strings.Asc(StContrase�a.Substring(i - 1, Math.Min(1, StContrase�a.Length - (i - 1)))[0]);
                //'    'validar caracteres tecleados: solo se admite el espacio, n�meros, letras may�sculas, letras min�sculas, la � y la �
                //'    If Not (entrada(I) = 32 Or _
                //''        (entrada(I) >= 48 And entrada(I) <= 57) Or _
                //''        (entrada(I) >= 65 And entrada(I) <= 90) Or _
                //''        entrada(I) = 209) Then
                //'        Mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 1230, RcPrincipal, S01000F1.lbContrase�a, "letras y n�meros"
                //'        Exit Function
                //'    End If
                //se transforman los rangos de caracteres v�lidos en un rango de 1..38
                if (entrada[i] == 32)
                {
                    entrada[i] = 1;
                }
                else
                {
                    if (entrada[i] >= 48 && entrada[i] <= 57)
                    {
                        entrada[i] -= 46;
                    }
                    else
                    {
                        if (entrada[i] >= 65 && entrada[i] <= 90)
                        {
                            entrada[i] -= 53;
                        }
                        else
                        {
                            entrada[i] = 38;
                        }
                    }
                }
                //se le suma el desplazamiento y se obtiene el m�dulo 38
                entrada[i] += arraydesp[i];
                entrada[i] = entrada[i] % 38;
                //se transforma el ascii en su caracter
                if (entrada[i] == 1)
                {
                    entrada[i] = 32;
                }
                else
                {
                    if (entrada[i] >= 2 && entrada[i] <= 11)
                    {
                        entrada[i] += 46;
                    }
                    else
                    {
                        if (entrada[i] >= 12 && entrada[i] <= 37)
                        {
                            entrada[i] += 53;
                        }
                        else
                        {
                            entrada[i] = 209;
                        }
                    }
                }
                salida[i] = Strings.Chr(entrada[i]).ToString();
                stDevolver.Append(salida[i]);
            }

            return stDevolver.ToString();

        }
        internal static string DesencriptarPassword(string StContrase�a)
        {

            int[] arraydesp = new int[9];
            int[] entrada = new int[9];
            StringBuilder stDevolucion = new StringBuilder();

            arraydesp[1] = 10;
            arraydesp[2] = 13;
            arraydesp[3] = 31;
            arraydesp[4] = 14;
            arraydesp[5] = 1;
            arraydesp[6] = 22;
            arraydesp[7] = 34;
            arraydesp[8] = 8;

            //A PARTIR DE AQUI, LA DESENCRIPTACION
            for (int i = 1; i <= 8; i++)
            {
                entrada[i] = Strings.Asc(StContrase�a.Substring(i - 1, Math.Min(1, StContrase�a.Length - (i - 1)))[0]);
                if (entrada[i] == 32)
                {
                    entrada[i] = 1;
                }
                else
                {
                    if (entrada[i] >= 48 && entrada[i] <= 57)
                    {
                        entrada[i] -= 46;
                    }
                    else
                    {
                        if (entrada[i] >= 65 && entrada[i] <= 90)
                        {
                            entrada[i] -= 53;
                        }
                        else
                        {
                            entrada[i] = 38;
                        }
                    }
                }
                entrada[i] -= arraydesp[i];
                if (entrada[i] < 0)
                {
                    entrada[i] += 38;
                }
                if (entrada[i] == 1)
                {
                    entrada[i] = 32;
                }
                else
                {
                    if (entrada[i] >= 2 && entrada[i] <= 11)
                    {
                        entrada[i] += 46;
                    }
                    else
                    {
                        if (entrada[i] >= 12 && entrada[i] <= 37)
                        {
                            entrada[i] += 53;
                        }
                        else
                        {
                            entrada[i] = 209;
                        }
                    }
                }
                stDevolucion.Append(Strings.Chr(entrada[i]).ToString());

            }

            return stDevolucion.ToString();

        }




        private static void LlenarArray(ref string[] AStColFK, string[] varAStColFK)
        {
            AStColFK = new string[] { String.Empty };
            for (int i = 0; i <= varAStColFK.GetUpperBound(0); i++)
            {
                if (i != 0)
                {
                    AStColFK = ArraysHelper.RedimPreserve(AStColFK, new int[] { i + 1 });
                }
                AStColFK[i] = varAStColFK[i];
            }
        }




        internal static bool TablaBorradoLogico(string StTabla, dynamic Mantenimiento)
        {

            bool result = false;
            string StComando = "Select * from STABBLOG where GTABLABLOG=";
            StComando = StComando + "'" + StTabla.Trim() + "'";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RcManten);
            DataSet RsBorrado = new DataSet();
            tempAdapter.Fill(RsBorrado);

            if (RsBorrado.Tables[0].Rows.Count != 0)
            {
                StFBORRADO = Convert.ToString(Mantenimiento.ObtenerColumnaFBorrado);
                result = true;
            }
            else
            {
                result = false;
            }
            RsBorrado.Close();
            return result;
        }



        internal static string FormatearFecha(int iLongitud, string stCadenaFecha)
        {
            //Funci�n que devuelve la cadena de la fecha formateada
            string stDevolver = String.Empty;
            switch (iLongitud)
            {
                case 19:
                    System.DateTime TempDate = DateTime.FromOADate(0);
                    stDevolver = (DateTime.TryParse(stCadenaFecha, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : stCadenaFecha;
                    break;
                case 16:
                    System.DateTime TempDate2 = DateTime.FromOADate(0);
                    stDevolver = (DateTime.TryParse(stCadenaFecha, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm") : stCadenaFecha;
                    break;
                case 10:
                    System.DateTime TempDate3 = DateTime.FromOADate(0);
                    stDevolver = (DateTime.TryParse(stCadenaFecha, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : stCadenaFecha;
                    break;
                case 8:
                    System.DateTime TempDate4 = DateTime.FromOADate(0);
                    stDevolver = (DateTime.TryParse(stCadenaFecha, out TempDate4)) ? TempDate4.ToString("HH:mm:ss") : stCadenaFecha;
                    break;
                case 5:
                    System.DateTime TempDate5 = DateTime.FromOADate(0);
                    stDevolver = (DateTime.TryParse(stCadenaFecha, out TempDate5)) ? TempDate5.ToString("HH:mm") : stCadenaFecha;
                    break;
                default:
                    RadMessageBox.Show("Valor del campo no v�lido", Application.ProductName);
                    break;
            }
            return stDevolver;
        }

        internal static void ValidarTipoColumna()
        {

        }

        internal static void DesactivarCampos(dynamic Mantenimiento, ModuloMantenim.strConjuntoResultados[] MatrizControles, ModuloMantenim.StrToolbar[] MatrizTolBar)
        {
            //Desactiva los campos del detalle
            //Mantenimiento.frmDetalleFondo.Enabled = False 'Comentado por luis
            if (MatrizControles.GetUpperBound(0) != 0)
            {
                for (int iCaracterizar = 0; iCaracterizar <= MatrizControles.GetUpperBound(0) - 1; iCaracterizar++)
                {
                    MatrizControles[iCaracterizar].NombreControl.Enabled = false;
                    if (MatrizControles[iCaracterizar].NombreControl.Name == "cbbCombo")
                    {
                        MatrizTolBar[ControlArrayHelper.GetControlIndex(MatrizControles[iCaracterizar].NombreControl)].NombreControl.Enabled = false;
                        MatrizTolBar[ControlArrayHelper.GetControlIndex(MatrizControles[iCaracterizar].NombreControl)].ClaveAsociada.Enabled = false;
                    }
                }
                Mantenimiento.frmDetalleFondo.Enabled = true;
            }
            else
            {
                Mantenimiento.frmDetalleFondo.Enabled = false;
            }

            //si el max es igual al min significa que no se puede cambiar
            //el value de la barra, entonces no tengo que inicializar el value.
            //sino si lo inicializo
            if (Mantenimiento.scbvCampos.Max != Mantenimiento.scbvCampos.Min)
            {
                Mantenimiento.scbvCampos.Value = 0;
            }
        }



        internal static void ActivarCampos(dynamic Mantenimiento, ModuloMantenim.strConjuntoResultados[] MatrizControles, ModuloMantenim.StrToolbar[] MatrizTolBar)
        {
            //    Activa los campos del detalle para poder editarlos,
            //    modificarlos, etc.
            if (MatrizControles.GetUpperBound(0) != 0)
            {
                for (int iCaracterizar = 0; iCaracterizar <= MatrizControles.GetUpperBound(0) - 1; iCaracterizar++)
                {
                    MatrizControles[iCaracterizar].NombreControl.Enabled = true;
                    if (MatrizControles[iCaracterizar].NombreControl.Name == "cbbCombo")
                    {
                        MatrizTolBar[ControlArrayHelper.GetControlIndex(MatrizControles[iCaracterizar].NombreControl)].NombreControl.Enabled = true;
                        MatrizTolBar[ControlArrayHelper.GetControlIndex(MatrizControles[iCaracterizar].NombreControl)].ClaveAsociada.Enabled = true;
                    }
                }
            }
            Mantenimiento.frmDetalleFondo.Enabled = true;
            Mantenimiento.frmDetalle.Top = 0;
        }

        internal static void RecogerDatosHospital()
        {

            string stSQLInforme = String.Empty;

            if (vstGrupoHosp == "" && vstNombreHosp == "" && vstDireccHosp == "")
            {
                stSQLInforme = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo='GRUPOHO' ";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQLInforme, RcManten);
                RsManten2 = new DataSet();
                tempAdapter.Fill(RsManten2);
                vstGrupoHosp = Convert.ToString(RsManten2.Tables[0].Rows[0]["valfanu1"]).Trim();
                RsManten2.Close();
                stSQLInforme = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo='NOMBREHO' ";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSQLInforme, RcManten);
                RsManten2 = new DataSet();
                tempAdapter_2.Fill(RsManten2);
                vstNombreHosp = Convert.ToString(RsManten2.Tables[0].Rows[0]["valfanu1"]).Trim();
                RsManten2.Close();
                stSQLInforme = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo='DIRECCHO' ";
                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSQLInforme, RcManten);
                RsManten2 = new DataSet();
                tempAdapter_3.Fill(RsManten2);
                vstDireccHosp = Convert.ToString(RsManten2.Tables[0].Rows[0]["valfanu1"]).Trim();
                RsManten2.Close();
            }
            //cryCrystal.Formulas[0] = "Empresa= \"" + vstGrupoHosp + "\"";
            cryCrystal.Formulas["Empresa"] = "\"" + vstGrupoHosp + "\"";
            //cryCrystal.Formulas[1] = "Hospital= \"" + vstNombreHosp + "\"";
            cryCrystal.Formulas["Hospital"] = "\"" + vstNombreHosp + "\"";
            //cryCrystal.Formulas[2] = "Direccion= \"" + vstDireccHosp + "\"";
            cryCrystal.Formulas["Direccion"] = "\"" + vstDireccHosp + "\"";

        }

        internal static void CrearDBTemporal()
        {
            object dbLangSpanish = null;
            object DBEngine = null;
            bool yaexiste = false;

            if (fDBTemporal)
            {
                return;
            }
            // ************************************************************************
            // Creamos aqui la BASE DE DATOS TEMPORAL
            // para el informe solo tenemos que crear la TABLA listado
            try
            {
                stInforme = vStPathBD + "\\" + vStBaseDatos;
                yaexiste = false;
                if (FileSystem.Dir(stInforme, FileAttribute.Normal) != "")
                {
                    File.Delete(stInforme);
                } // comprueba que esta eliminada
            }
            catch
            {
                yaexiste = true;
            }
            if (!yaexiste)
            {
                // ralopezn_TODO_X_5 18/05/2016
                //Wk = (Workspace) DBEngine.Workspaces(0);
                //DB = (Database) Wk.CreateDatabase(stInforme, dbLangSpanish);
            }
            else
            {
                // ralopezn_TODO_X_5 18/05/2016
                //Wk = (Workspace) DBEngine.Workspaces(0);
                //        Set DB = Wk.OpenDatabase(stInforme, dbLangSpanish)
            }
            fDBTemporal = true;
            // ************************************************************************
        }

		internal static void CrearTablaTemporal()
		{
			string StSQL = String.Empty;
			try
			{
                // ralopezn_TODO_X_5 18/05/2016
                //DB = (Database) Wk.OpenDatabase(stInforme);

                //TableDef tdfNuevo = null;
                if (fTTemporal)
				{ //tabla ya creada
                  //DB.Execute "delete from listado;"
                  // ralopezn_TODO_X_5 18/05/2016
                  //DB.Execute("drop table listado;");
                    StSQL = "";
					StSQL = "CREATE TABLE listado ";
					StSQL = StSQL + "(numero INTEGER, tipo CHAR(1) , Cabecera MEMO, ";
					StSQL = StSQL + "linea MEMO); ";
                    // ralopezn_TODO_X_5 18/05/2016	
                    //DB.Execute(StSQL);
                    //*****************************************************
                }
                else
				{
					//******************************
                    // ralopezn_TODO_X_5 18/05/2016
					//foreach (TableDef tdfBucle in DB.TableDefs)
					//{
					//	if (Convert.ToString(tdfBucle.Name) == "listado")
					//	{
					//		DB.Execute("drop table listado;");
					//	}
					//}
					//*****************************
					StSQL = "";
					StSQL = "CREATE TABLE listado ";
					StSQL = StSQL + "(numero INTEGER, tipo CHAR(1) , Cabecera MEMO, ";
					StSQL = StSQL + "linea MEMO); ";
                    // ralopezn_TODO_X_5 18/05/2016
                    //DB.Execute(StSQL);
                    fTTemporal = true;
					//****************************************************
				}
			}
			catch
			{
				//   Resume Next
			}
		}

        internal static void AsignarPropiedadControl(RadMaskedEditBox ControlMascara)
		{

			//Guardamos el maxlength en una variable porque
			//al cambiarle la m�scara lo pierde
			int ILen = Convert.ToInt32(ControlMascara.MaskedEditBoxElement.TextBoxItem.MaxLength);
            ControlMascara.MaskType = MaskType.None;

            if (!Convert.IsDBNull(RsManten2.Tables[0].Rows[0]["vmascara"]))
			{
				ControlMascara.Mask = RsManten2.Tables[0].Rows[0]["vmascara"].ToString();

			}
			else
			{
				//        ControlMascara.Mask = PrepararMascara(Trim(RsManten2("gtipodato")), _
				//'                                            RsManten2("nparteentera"), _
				//'                                            RsManten2("npartedecima"), RsManten2("vmascara"))
				//****************CRIS*************************************
				ControlMascara.Mask = "";

				//Ponemos el prompt de la m�scara a blanco para los datos
				//CHAR o VARCHAR, y a cero si el campo es num�rico

				switch(Convert.ToString(RsManten2.Tables[0].Rows[0]["gtipodato"]).Trim())
				{
					case "CHAR" : case "VARCHAR" : case "NUMERIC" : case "SMALLINT" : case "INT" : 
						 
						ControlMascara.PromptChar = ' '; 
						 
						break;
				}
				//**************************************************************
			}
            //    If Not IsNull(RsManten2("vformato")) Then
            //        ControlMascara.Format = RsManten2("vformato")
            //    Else
            //        ControlMascara.Format = ""
            //    End If
            ControlMascara.MaskedEditBoxElement.TextBoxItem.MaxLength = ILen;
		}

		internal static string PrepararMascara(string stTipoDato, int iparteentera, int ipartedecima, object VarMascara_optional)
		{
			string VarMascara = (VarMascara_optional == Type.Missing) ? String.Empty : VarMascara_optional as string;


			string result = String.Empty;
			switch(stTipoDato)
			{
				case "CHAR" : case "VARCHAR" : 
					if (VarMascara_optional != Type.Missing)
					{
						result = LlenarMascara(iparteentera, "&", Convert.ToInt32(DBNull.Value), VarMascara);
					}
					else
					{
						result = LlenarMascara(iparteentera, "&", Convert.ToInt32(DBNull.Value), Convert.ToString(DBNull.Value));
					} 
					 
					break;
				case "NUMERIC" : case "SMALLINT" : case "INT" : 
					 
					if (VarMascara_optional != Type.Missing)
					{
						result = LlenarMascara(iparteentera, "9", ipartedecima, VarMascara);
					}
					else
					{
						result = LlenarMascara(iparteentera, "9", ipartedecima, Convert.ToString(DBNull.Value));
					} 
					 
					break;
			}

			return result;
		}

		internal static string PrepararMascara(string stTipoDato, int iparteentera, int ipartedecima)
		{
			return PrepararMascara(stTipoDato, iparteentera, ipartedecima, Type.Missing);
		}

		internal static string LlenarMascara(int Ientero, string stSigno, int Idecimal = 0, string VarMascara = "")
		{

			string result = String.Empty;
			int iIndiceMascara = 0;
			StringBuilder stCadena = new StringBuilder();

			if (!Convert.IsDBNull(VarMascara))
			{
				result = VarMascara.Trim();
			}
			else
			{
				if (Idecimal > 0)
				{ //ES NUM�RICO CON DECIMALES

					for (iIndiceMascara = 1; iIndiceMascara <= Ientero; iIndiceMascara++)
					{
						stCadena.Append(stSigno);
					}

					stCadena.Append(stSignoDecimal);
					iIndiceMascara = 0;
					for (iIndiceMascara = 1; iIndiceMascara <= Idecimal; iIndiceMascara++)
					{
						stCadena.Append("9");
					}


					//**************************CRIS***********************
					//A�adimos un d�gito mas a la m�scara para poner el signo si
					//�sta es num�rica, y lo es si se llama a la funci�n mandando
					//la parte decimal (aunque sea �sta cero)

					stCadena = new StringBuilder(stSigno + stCadena.ToString());

				}
				else if (Idecimal == 0)
				{  //ES NUM�RICO SIN DECIMALES
					for (iIndiceMascara = 1; iIndiceMascara <= Ientero; iIndiceMascara++)
					{
						stCadena.Append(stSigno);
					}

					//**************************CRIS***********************
					//A�adimos un d�gito mas a la m�scara para poner el signo si
					//�sta es num�rica, y lo es si se llama a la funci�n mandando
					//la parte decimal (aunque sea �sta cero)

					stCadena = new StringBuilder(stSigno + stCadena.ToString());
				}
				else if (Convert.IsDBNull(Idecimal))
				{ 
					stCadena = new StringBuilder("");
				}
				//*****************************************************
			}

			return stCadena.ToString();
		}



		internal static void LlamadaMantenimiento(ref string StTablas, SMT100F2 Formulario, object varAStColFK_optional, int varIndiceCombo)
		{
			string[] varAStColFK = (varAStColFK_optional == Type.Missing) ? null : varAStColFK_optional as string[];
			string[] AStColFK = null;

			ClassManten = new MantenAutomatico.ClassMantenimiento();
			if (varAStColFK_optional != Type.Missing)
			{
				LlenarArray(ref AStColFK, varAStColFK);
				string tempRefParam = String.Empty;
				ClassManten.Load(gstUsuario, gstPassword, vStBaseDatos, vStPathBD, vStPathReport, Formulario, cryCrystal, RcManten, vMaximoFilas, ref StTablas, tempRefParam, AStColFK, varIndiceCombo, gstPantallaOrigen);
			}
			else
			{
				string tempRefParam2 = String.Empty;
				ClassManten.Load(gstUsuario, gstPassword, vStBaseDatos, vStPathBD, vStPathReport, Formulario, cryCrystal, RcManten, vMaximoFilas, ref StTablas, tempRefParam2, null, 0, gstPantallaOrigen);
			}

		}

		internal static void LlamadaMantenimiento(ref string StTablas, SMT100F2 Formulario, object varAStColFK_optional)
		{
			LlamadaMantenimiento(ref StTablas, Formulario, varAStColFK_optional, 0);
		}

		internal static void LlamadaMantenimiento(ref string StTablas, SMT100F2 Formulario)
		{
			LlamadaMantenimiento(ref StTablas, Formulario, Type.Missing, 0);
		}


		internal static object BorrarDB(dynamic Mantenimiento)
		{
			//Borrar un registro en la Base de Datos
			//*******************************************************
			Mantenimiento.BuscarPK();

			try
			{

				//Llamada a la funci�n que prepara la consulta para el borrado
				stSQLManten = Convert.ToString(Mantenimiento.MontarSelectConsulta);

				//*******************************************
				if (!PermitirBorrar(stSQLManten))
				{
					//Borrar de la spread el registro eliminado
					Mantenimiento.sprMultiple.Action = 5; //Borra de la Spread el registro actual
					Mantenimiento.sprMultiple.MaxRows = Convert.ToDouble(Mantenimiento.sprMultiple.MaxRows) - 1;
					return null;
				}
				//*******************************************

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQLManten, RcManten);
				RsManten2 = new DataSet();
				tempAdapter.Fill(RsManten2);


				//Oscar
				//Parece ser que se borra un registro de la BD, por lo que se tiene
				//que auditar. Se hace antes del delete, porque en caso de producirse un error
				//en la actualizacion, se hace un rollback de la transaccion
				AUDITARMOVIMIENTO("B", Convert.ToString(Mantenimiento.vstNombreTabla), Serrores.VVstUsuarioApli, RsManten2);
				//---------------

				RsManten2.Delete(tempAdapter);
				RsManten2.Close();




				//Borrar de la spread
				Mantenimiento.sprMultiple.Action = 5;
				Mantenimiento.sprMultiple.MaxRows = Convert.ToDouble(Mantenimiento.sprMultiple.MaxRows) - 1;

				return null;
			}
			catch(Exception ex)
			{

				//MsgBox Err.Number & " " & Err.Description
				//RsManten2.Close

				//****************************CRIS************************************

				string StRes = String.Empty;

                if (ex.InnerException is SqlException)
                {
                    SqlException sqlex = ex.InnerException as SqlException;
                    var errors = new RDO_rdoErrors(sqlex);



                    if (errors.rdoErrors.Count > 1)
                    {
                        //Si no se puede borrar porque
                        //hay problemas de integridad
                        //referencial ERROR=547
                        if (errors.rdoErrors[1].Number == 547)
                        {
                            //Compruebo que la tabla es una de las que se
                            //puede realizar borrado l�gico

                            if (TablaBorradoLogico(Convert.ToString(Mantenimiento.vstNombreTabla), Mantenimiento))
                            {
                                if (Convert.IsDBNull(RsManten2.Tables[0].Rows[0][StFBORRADO]))
                                {
                                    //Pregunto si desea borrar el registro l�gicamente
                                    StRes = ((int)RadMessageBox.Show("Se ha producido un error de integridad referencial. �Desea eliminar el registro l�gicamente?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Question)).ToString();
                                    if (StRes == ((int)System.Windows.Forms.DialogResult.Yes).ToString())
                                    {
                                        //Si la respuesta es s� actualizo la
                                        //fecha de borrado con la del sistema
                                        RsManten2.Edit();
                                        RsManten2.Tables[0].Rows[0][StFBORRADO] = DateTime.Today.ToString("dd/MM/yyyy");

                                        //Oscar
                                        //Parece ser que se borra de forma logica un registro de la BD,
                                        //por lo que se tiene que auditar
                                        AUDITARMOVIMIENTO("M", Convert.ToString(Mantenimiento.vstNombreTabla), Serrores.VVstUsuarioApli, RsManten2);
                                        //---------------

                                        string tempQuery = RsManten2.Tables[0].TableName;
                                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                                        tempAdapter_2.Update(RsManten2, RsManten2.Tables[0].TableName);
                                        Mantenimiento.ActualizarFechaBorradoSpread();
                                    }
                                }
                                else
                                {
                                    //Se muestra que se ha detectado el error
                                    //de problemas de integridad referencial
                                    RadMessageBox.Show("Hay errores de integridad y el registro ya ha sido borrado l�gicamente", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                                }
                            }
                            else
                            {
                                //Aunque el error sea de integridad referencial, se controlar�
                                //como si se tratase de cualquier otro error
                                foreach (SqlError er in errors.rdoErrors)
                                {
                                    //GestionErrorBaseDatos(rdoErroRrs.number)
                                    if (er.Number == 547)
                                    {
                                        RadMessageBox.Show("Imposible eliminar existen referencias en otras tablas", Application.ProductName);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(er.Number.ToString() + "-" + er.Message, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                    else if (errors.rdoErrors.Count == 1)
                    {
                        //Si no se puede borrar porque la tabla
                        //tiene demasiadas CONSTRAINTS
                        //ERROR=431
                        if (errors.rdoErrors[0].Number == 431)
                        {
                            if (Convert.ToBoolean(Mantenimiento.ComprobarDependenciasTabla(Mantenimiento.vstNombreTabla, stSQLManten, true)))
                            {
                                //****************************************************************************
                                //Si llega a esta parte es porque no ha encontrado ningun registro igual en las
                                //tablas referenciadas.
                                //Se desactivan las CONSTRAINTS, se borra el registro y se vuelven a activar
                                //los constraints.
                                //****************************************************************************
                                DeshabilitarHabilitarConstraints(Convert.ToString(Mantenimiento.vstNombreTabla), stSQLManten, Mantenimiento);
                            }
                        }
                        else
                        {
                            //Si el error no es de integridad referencial, se controlar�
                            foreach (SqlError er in errors.rdoErrors)
                            {
                                //GestionErrorBaseDatos(rdoErroRrs.number)
                                if (er.Number == 547)
                                {
                                    RadMessageBox.Show("Imposible eliminar existen referencias en otras tablas", Application.ProductName);
                                }
                                else
                                {
                                    RadMessageBox.Show(er.Number.ToString() + "-" + er.Message, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
                                }
                            }
                        }
                    }
                    errors.Clear();
                }
				//Rc.RollbackTrans
				return null;
				//***************************************************************************
			}
		}
		internal static void ColeccionErrores(Exception exception)
		{
			//Recorre la colecci�n de errores y llama a la rutina
			//que gestiona los errores propios.Despu�s de las pruebas
			//se pasar�n al m�dulo SERRORES.
			int lError = 0;
			//De momento estoy gestionando aqu� los errores que despu�s
			//pasar� al m�dulo SERRORES.
			int iDevolver = 0;

            if (exception != null && exception.InnerException is SqlException)
            {
                var errors = new RDO_rdoErrors(exception.InnerException as SqlException);
                foreach (SqlError er in errors.rdoErrors)
                {
                    lError = er.Number;
                    switch (lError)
                    {
                        case 233:  //La columna no puede ser nula 
                            short tempRefParam = 1040;
                            string[] tempRefParam2 = new string[] { };
                            Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcManten, tempRefParam2);

                            break;
                        case 3621:  //El comando ha sido abortado 
                                    //                MsgBox "El comando ha sido abortado" 
                            iDevolver = 1;
                            break;
                        case 2627:  //Clave duplicada 
                            short tempRefParam3 = 1080;
                            string[] tempRefParam4 = new string[] { "Elemento" };
                            Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RcManten, tempRefParam4);
                            iDevolver = 1;
                            break;
                        case 547:
                            RadMessageBox.Show("Existen campos que contienen valores incoherentes con el dato que se espera ", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                            break;
                        default:
                            RadMessageBox.Show(er.Number.ToString() + " " + er.Message, Application.ProductName);
                            break;
                    }
                }
            }


		}
		internal static object ControlErrores(int lError, SqlConnection RcConexion)
		{
            /*
            ralopezn TODO_X_5 18/05/2016
			foreach (SqlError er in UpgradeSupport.RDO_rdoEngine_definst.rdoErrors)
			{
				//GestionErrorBaseDatos(rdoErrors.number)
				RadMessageBox.Show(er.Number.ToString() + "-" + er.Message, Application.ProductName, RadMessageBoxButtons.OK, RadMessageBoxIcon.Error);
			}

			UpgradeSupport.RDO_rdoEngine_definst.rdoErrors.Clear();
            */
			return null;
		}

		internal static bool ComprobarActualizacion(DataSet RsActualizacion, string[] aComparacion)
		{

			for (int iActualizar = 0; iActualizar <= aComparacion.GetUpperBound(0) - 1; iActualizar++)
			{

				if (Convert.ToString(RsActualizacion.Tables[0].Rows[0][iActualizar]).Trim() != aComparacion[iActualizar])
				{

					return false;
				}

			}

			//Est�n todos los registros igual que en la spread
			return true;

		}

		internal static void ActualizarSpread(string[] aActualizaSpread, dynamic Mantenimiento)
		{
			//Actualiza el registro activo

			Mantenimiento.sprMultiple.col = 1;

			for (int iIndiceSpread = 0; iIndiceSpread <= Convert.ToInt32(Convert.ToDouble(Mantenimiento.sprMultiple.MaxCols) - 1); iIndiceSpread++)
			{

				if (aActualizaSpread[iIndiceSpread].Trim() != CoEsCombo)
				{
					if (aActualizaSpread[iIndiceSpread].Trim() == "")
					{
						Mantenimiento.sprMultiple.col = iIndiceSpread + 1;
						Mantenimiento.sprMultiple.Text = "";
						//Mantenimiento.sprMultiple.Col = Mantenimiento.sprMultiple.Col + 1
					}
					else
					{
						Mantenimiento.sprMultiple.col = iIndiceSpread + 1;
						Mantenimiento.sprMultiple.Text = aActualizaSpread[iIndiceSpread];
						//Mantenimiento.sprMultiple.Col = Mantenimiento.sprMultiple.Col + 1
					}
				}

			}

		}

		internal static void VisualizarRegistro(int iRegistro, dynamic Mantenimiento)
		{

			Mantenimiento.sprMultiple.Row = iRegistro;
			Mantenimiento.sprMultiple.col = 1;
			Mantenimiento.sprMultiple.Action = 0;

		}





		internal static string ObtenerOrdenColumnas()
		{

			string StSQL = "";
			StSQL = " order by nordencoltab ";
			return StSQL;

		}











		internal static int VerificarCantidadRegistros(string Tabla)
		{

			//Contaremos los registros con los que vamos a llenar la Spread
			string sql = "select count(*) from " + Tabla;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcManten);
			RsManten2 = new DataSet();
			tempAdapter.Fill(RsManten2);
			string lTotal = Convert.ToString(RsManten2.Tables[0].Rows[0][0]);
			RsManten2.Close();

			return Convert.ToInt32(Double.Parse(lTotal));

		}

		internal static void VerificarModoConexion()
		{

			string sql = "select valfanu1 from SCONSGLO where gconsglo='MODCONEX'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcManten);
			RsManten2 = new DataSet();
			tempAdapter.Fill(RsManten2);
			vstModo.Value = Convert.ToString(RsManten2.Tables[0].Rows[0][0]);
			RsManten2.Close();

		}

		internal static bool PermitirBorrar(string StSelect)
		{

			bool result = false;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSelect, RcManten);
			RsManten2 = new DataSet();
			tempAdapter.Fill(RsManten2);
			if (RsManten2.Tables[0].Rows.Count == 1)
			{
				result = true;
			}
			else
			{
				//MENSAJE: V1 seleccionado ha sido Eliminado por otro Usuario.
				short tempRefParam = 1700;
				string[] tempRefParam2 = new string[]{"El registro"};
				Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcManten, tempRefParam2);
				result = false;
			}
			RsManten2.Close();
			return result;
		}

		internal static bool PermitirModificar(string StSelect, string[] aComparar, dynamic Mantenimiento)
		{
			bool result = false;
			int iRespuesta = 0;
			string VarComparacion = String.Empty;

			int i = 0;

			result = true;

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSelect, RcManten);
			RsManten2 = new DataSet();
			tempAdapter.Fill(RsManten2);

			for (int iIndiceComparacion = 0; iIndiceComparacion <= aComparar.GetUpperBound(0); iIndiceComparacion++)
			{ //- 1
				//Si lo que contiene el array es igual a la constante CoEsCombo,
				//quiere decir que es un combo y no lo utilizo para comparar
				if (aComparar[iIndiceComparacion].Trim() != CoEsCombo.Trim())
				{
					//*********************************+CRIS***************************
					//Si es fecha lo formateo para que est� como DD/MM/YYYY, como en el
					//spread
					if (RsManten2.Tables[0].Rows[0].getType(i) == typeof(DateTime))
					{
						System.DateTime TempDate = DateTime.FromOADate(0);
						VarComparacion = (DateTime.TryParse(Convert.ToString(RsManten2.Tables[0].Rows[0][i]).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : Convert.ToString(RsManten2.Tables[0].Rows[0][i]).Trim();
					}
					else if (RsManten2.Tables[0].Rows[0].getType(i) == typeof(int) || RsManten2.Tables[0].Rows[0].getType(i) == typeof(decimal))
					{ 
						if (!Convert.IsDBNull(RsManten2.Tables[0].Rows[0][i]))
						{
							if (RsManten2.Tables[0].Rows[0].getType(i) == typeof(decimal))
							{
								VarComparacion = Convert.ToDouble(RsManten2.Tables[0].Rows[0][i]).ToString();
							}
							else
							{
								VarComparacion = Conversion.Val(Convert.ToString(RsManten2.Tables[0].Rows[0][i])).ToString();
							}
							aComparar[iIndiceComparacion] = Double.Parse(aComparar[iIndiceComparacion]).ToString();
						}
						else
						{
							VarComparacion = "";
						}
					}
					else
					{
						if (!Convert.IsDBNull(RsManten2.Tables[0].Rows[0][i]))
						{
							VarComparacion = Convert.ToString(RsManten2.Tables[0].Rows[0][i]).Trim();
						}
						else
						{
							VarComparacion = "";
						}
					}
					//*****************************************************************
					if (aComparar[iIndiceComparacion].Trim() != VarComparacion.Trim())
					{
						result = false;
						//MENSAJE 1710
						short tempRefParam = 1710;
						string[] tempRefParam2 = new string[]{"El registro"};
						iRespuesta = Convert.ToInt32(Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcManten, tempRefParam2));
						if (iRespuesta == 6)
						{ //S�
							//Refrescar Spread y
							ActualizarCambios(RsManten2, aComparar);
							ActualizarSpread(aComparar, Mantenimiento);
							if (Mantenimiento.Name == "SMT100F2")
							{
								Mantenimiento.BuscarFK();
							}
							Mantenimiento.LimpiarCampos();
							//Refrescar Detalle
							Mantenimiento.LlenarCampos();
						}
						else
						{
							//No
							//No hace nada, no se muestra el cambio en la spread y
							//Se limpian los campos del detalle.
							Mantenimiento.LimpiarCampos();
						}
					}
					i++;
				}
			}
			RsManten2.Close();

			return result;
		}

		internal static void ActualizarCambios(DataSet RsCambios, string[] aComparacion)
		{

			int i = 0;

			for (int iIndiceActualizarCambios = 0; iIndiceActualizarCambios <= aComparacion.GetUpperBound(0); iIndiceActualizarCambios++)
			{ //- 1

				if (aComparacion[iIndiceActualizarCambios].Trim() != CoEsCombo.Trim())
				{
					if (Convert.IsDBNull(RsCambios.Tables[0].Rows[0][i]))
					{
						aComparacion[iIndiceActualizarCambios] = "";
					}
					else
					{
						aComparacion[iIndiceActualizarCambios] = Convert.ToString(RsCambios.Tables[0].Rows[0][i]);
					}
					i++;
				}

			}

		}

		internal static void ActualizaBarraDesplazamiento(Control ControlActivo, VScrollBar ControlBarra, RadPanel ControlFrame)
		{
			int ControlEnfoque = 0;
			if (ControlBarra.Visible)
			{
				ControlEnfoque = (Convert.ToInt32((((float) (ControlActivo.Top * 15)) + ((float) (ControlActivo.Height * 15))) * (ControlBarra.Maximum - (ControlBarra.LargeChange + 1)) / ((float) (ControlFrame.Height * 15))));
				ControlBarra.Value = (Convert.ToInt32(Math.Floor(Convert.ToDecimal(ControlEnfoque / ((int) ControlBarra.SmallChange))) * ControlBarra.SmallChange));
			}
		}

		//Oscar
		internal static object AUDITARMOVIMIENTO(string PTipoAccion, string PNombTabla, string PUsuario, DataSet PRAUX)
		{

			string[] arrayPK = null;
			int i = 1;

			string sql = " SELECT gcolumncaman from scocaman" + 
			             " WHERE gtablacaman='" + PNombTabla + "' AND nordencolpk > 0 order by nordencolpk";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcManten);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			StringBuilder stPK = new StringBuilder();
			if (rr.Tables[0].Rows.Count != 0)
			{
				foreach (DataRow iteration_row in rr.Tables[0].Rows)
				{
					arrayPK = ArraysHelper.RedimPreserve(arrayPK, new int[]{i + 1});
					arrayPK[i] = Convert.ToString(iteration_row[0]).Trim();
					i++;
				}
				stPK = new StringBuilder("");
				for (i = 1; i <= arrayPK.GetUpperBound(0); i++)
				{
					stPK.Append(Convert.ToString(PRAUX.Tables[0].Rows[0]["" + arrayPK[i] + ""]).Trim().ToUpper() + ",");
				}
				stPK = new StringBuilder(stPK.ToString().Substring(0, Math.Min(stPK.ToString().Length - 1, stPK.ToString().Length)));
			}
			else
			{
				stPK = new StringBuilder("");
			}
			rr.Close();

			sql = "INSERT INTO SAUDISIS (factuali, gusuario, itipactu, gnombtab, dideregi)" + 
			      " VALUES(GETDATE(), '" + PUsuario + "', '" + PTipoAccion + "', '" + PNombTabla + "', '" + stPK.ToString() + "')";

			SqlCommand tempCommand = new SqlCommand(sql, RcManten);
			tempCommand.ExecuteNonQuery();

			return null;
		}

		//oscar
		internal static int LongitudMinimaPassword()
		{
			//Comprobamos que el campo nueva contrase�a tenga como minimo el numero de caracteres especificado
			//en la constante global CONTLMIN.
			int result = 0;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT ISNULL(NNUMERI1,0) FROM SCONSGLO WHERE GCONSGLO='CONTLMIN'", RcManten);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				result = (Convert.ToInt32(rr.Tables[0].Rows[0][0]));
			}
			rr.Close();
			return result;
		}
		//----------
	}
}