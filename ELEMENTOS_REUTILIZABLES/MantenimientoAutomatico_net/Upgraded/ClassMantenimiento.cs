using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using System.Transactions;
using Telerik.WinControls;

namespace MantenAutomatico
{
	public class ClassMantenimiento
	{



		public void Load(string StUsuSQL, string StPassSQL, string StNombreBD, string StPathBD, string StPathReport, dynamic FrForm, object cryCrystalReport, SqlConnection Rc, int MaximoFilas, ref string NombreTabla_optional, string DescrCortaTabla, object varAStColumnas_optional, int iIndiceArrayCombo, object stp_optional)
		{
			object stp = (stp_optional == Type.Missing) ? null : stp_optional as object;
			string NombreTabla = (NombreTabla_optional == Type.Missing) ? String.Empty : NombreTabla_optional as string;
			string[] varAStColumnas = (varAStColumnas_optional == Type.Missing) ? null : varAStColumnas_optional as string[];
			try
			{

				int iRespuesta = 0;
				string stConsulta = String.Empty;
				SMT100F1 FrSinCombos = null;
				SMT100F2 FrConCombos = null;
				bool fCombos = false;
				int lAyuda = 0;
				bool fCerrar = false;
				ModuloMantenim.gstUsuario = StUsuSQL;
				ModuloMantenim.gstPassword = StPassSQL;
				ModuloMantenim.vStBaseDatos = StNombreBD.Trim();
				ModuloMantenim.vStPathBD = StPathBD.Trim();
				ModuloMantenim.vMaximoFilas = MaximoFilas;
				ModuloMantenim.vStPathReport = StPathReport.Trim();
				if (stp_optional == Type.Missing)
				{

					ModuloMantenim.gstPantallaOrigen = Convert.ToString(FrForm.Name); //guardo la pantalla origen
				}
				else
				{
					ModuloMantenim.gstPantallaOrigen = ModuloMantenim.gstPantallaOrigen;
				}

				ModuloMantenim.cryCrystal = cryCrystalReport;
				ModuloMantenim.RcManten = Rc;

				Serrores.CrearCadenaDSN(ref ModuloMantenim.NOMBRE_DSN_SQL, ref ModuloMantenim.CoDSNTemp);

				Serrores.Datos_Conexion(ModuloMantenim.RcManten);
				Serrores.AjustarRelojCliente(ModuloMantenim.RcManten);
				//Establecer la DLL para los mensajes
				ModuloMantenim.Mensaje = new Mensajes.ClassMensajes();

				//fTodo es una variable booleana que para indicar que hay que llenar
				//la Spread con todos los registros.
				bool fTodo = false;

				if (NombreTabla_optional != Type.Missing)
				{
					stConsulta = "select gtablacaman,dtablalargo,dtablamancor,iactualizar," + "nayudatabla from STACATABV1 where gtablacaman='";
					stConsulta = stConsulta + NombreTabla + "'";
				}
				else
				{
					stConsulta = "select gtablacaman,dtablalargo,dtablamancor,iactualizar," + "nayudatabla from STACATABV1 where dtablamancor='";
					stConsulta = stConsulta + DescrCortaTabla + "'";
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stConsulta, ModuloMantenim.RcManten);
				DataSet RsManten = new DataSet();
				tempAdapter.Fill(RsManten);

				NombreTabla = Convert.ToString(RsManten.Tables[0].Rows[0]["gtablacaman"]).Trim();
				string DescrLarTabla = Convert.ToString(RsManten.Tables[0].Rows[0]["dtablalargo"]).Trim();
				DescrCortaTabla = Convert.ToString(RsManten.Tables[0].Rows[0]["dtablamancor"]).Trim();
				string Actualizable = Convert.ToString(RsManten.Tables[0].Rows[0]["iactualizar"]).Trim();
				if (!Convert.IsDBNull(RsManten.Tables[0].Rows[0]["nayudatabla"]))
				{
					lAyuda = Convert.ToInt32(RsManten.Tables[0].Rows[0]["nayudatabla"]);
				}
				RsManten.Close();

				int lNumRegistros = ModuloMantenim.VerificarCantidadRegistros(NombreTabla);
				//**********************************************
				//verificar que hay datos en la vista para crear la Spread y los campos
				//necesarios en la pantalla de Mantenimiento Autom�tico
				stConsulta = "";
				stConsulta = "select dtablamancor from SPRETBSPV1 ";
				stConsulta = stConsulta + "where GTABLAMANAUT='";
				stConsulta = stConsulta + NombreTabla + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stConsulta, ModuloMantenim.RcManten);
				RsManten = new DataSet();
				tempAdapter_2.Fill(RsManten);

				if (RsManten.Tables[0].Rows.Count < 1)
				{
					short tempRefParam = 1760;
                    string[] tempRefParam2 = new string[]{DescrLarTabla};
					ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, ModuloMantenim.RcManten, tempRefParam2);
					RsManten.Close();
					return;
				}
				RsManten.Close();
				//******************CRIS***************************
				//Compruebo si el mantenimiento se har� con combos referenciando a FK

				stConsulta = "SELECT * FROM SCOLFKPKV1 " + "WHERE GTABCAMANHIJ=" + "'" + NombreTabla.Trim() + "'";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stConsulta, ModuloMantenim.RcManten);
				RsManten = new DataSet();
				tempAdapter_3.Fill(RsManten);

				//**********************************************
				//Si el m�ximo de filas es superior al permitido
				if (lNumRegistros > MaximoFilas)
				{
					//Mensaje-->Se han encontrado V1 elementos.�Desea visualizarlos?
					short tempRefParam3 = 1660;
                    string[] tempRefParam4 = new string[] { lNumRegistros.ToString()};
					iRespuesta = Convert.ToInt32(ModuloMantenim.Mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, ModuloMantenim.RcManten, tempRefParam4));


					switch(iRespuesta)
					{
						case 6 :  //S� 
							fCerrar = true; 
							fTodo = true; 
							 
							//Si la tabla no es hija de ninguna otra 
							//no tendr� combos asociados 
							fCombos = RsManten.Tables[0].Rows.Count != 0; 
							RsManten.Close(); 

							 
							break;
						case 7 :  //No 
							fCerrar = false; 
							fTodo = false; 
							//Si la tabla no es hija de ninguna otra 
							//no tendr� combos asociados 
							fCombos = RsManten.Tables[0].Rows.Count != 0; 
							RsManten.Close(); 
							 
							break;
						case 2 :  //Cancelar 
							fCerrar = false; 
							return;
					}
				}
				else
				{

					fTodo = true;

					//Si la tabla no es hija de ninguna otra
					//no tendr� combos asociados
					fCombos = RsManten.Tables[0].Rows.Count != 0;
					RsManten.Close();

				}
				//****************************************************************

				if (fCombos)
				{
					FrConCombos = new SMT100F2();
					FrConCombos.vstNombreTabla = NombreTabla;
					FrConCombos.vstDescrLarTabla = DescrLarTabla;
					FrConCombos.vstDescrCortaTabla = DescrCortaTabla;
					FrConCombos.vstActualizable = Actualizable;
					FrConCombos.vfTodo = fTodo;
					FrConCombos.vlAyuda = lAyuda;
					FrConCombos.vfCerrar = fCerrar;
					if (varAStColumnas_optional != Type.Missing)
					{
						FrConCombos.InstanciaForm(lNumRegistros, FrForm, varAStColumnas, iIndiceArrayCombo);
					}
					else
					{
						FrConCombos.InstanciaForm(lNumRegistros, FrForm);
					}
					if (ModuloMantenim.gstPantallaOrigen == "PROBLEMAS")
					{
						FrConCombos.ShowDialog();
					}
					else
					{
						FrConCombos.Show();
					}
				}
				else
				{
					FrSinCombos = new SMT100F1();
					FrSinCombos.vstNombreTabla = NombreTabla;
					FrSinCombos.vstDescrLarTabla = DescrLarTabla;
					FrSinCombos.vstDescrCortaTabla = DescrCortaTabla;
					FrSinCombos.vstActualizable = Actualizable;
					FrSinCombos.vfTodo = fTodo;
					FrSinCombos.vlAyuda = lAyuda;
					FrSinCombos.vfCerrar = fCerrar;
					if (varAStColumnas_optional != Type.Missing)
					{
						FrSinCombos.InstanciaForm(lNumRegistros, FrForm, varAStColumnas, iIndiceArrayCombo);
					}
					else
					{
						FrSinCombos.InstanciaForm(lNumRegistros, FrForm);
					}
					if (ModuloMantenim.gstPantallaOrigen == "PROBLEMAS")
					{
						FrSinCombos.ShowDialog();
					}
					else
					{
						FrSinCombos.Show();
					}
				}
			}
			finally
			{
				NombreTabla_optional = NombreTabla;
			}

		}

		public void Load(string StUsuSQL, string StPassSQL, string StNombreBD, string StPathBD, string StPathReport, dynamic FrForm, object cryCrystalReport, SqlConnection Rc, int MaximoFilas, ref string NombreTabla_optional, string DescrCortaTabla, object varAStColumnas_optional, int iIndiceArrayCombo)
		{
			Load(StUsuSQL, StPassSQL, StNombreBD, StPathBD, StPathReport, FrForm, cryCrystalReport, Rc, MaximoFilas, ref NombreTabla_optional, DescrCortaTabla, varAStColumnas_optional, iIndiceArrayCombo, Type.Missing);
		}

        public void Load(string StUsuSQL, string StPassSQL, string StNombreBD, string StPathBD, string StPathReport, dynamic FrForm, object cryCrystalReport, SqlConnection Rc, int MaximoFilas,ref  string NombreTabla_optional, string DescrCortaTabla, object varAStColumnas_optional)
		{
			Load(StUsuSQL, StPassSQL, StNombreBD, StPathBD, StPathReport, FrForm, cryCrystalReport, Rc, MaximoFilas, ref NombreTabla_optional, DescrCortaTabla, varAStColumnas_optional, 0, Type.Missing);
		}

        public void Load(string StUsuSQL, string StPassSQL, string StNombreBD, string StPathBD, string StPathReport, dynamic FrForm, object cryCrystalReport, SqlConnection Rc, int MaximoFilas, ref string NombreTabla_optional, string DescrCortaTabla)
		{
			Load(StUsuSQL, StPassSQL, StNombreBD, StPathBD, StPathReport, FrForm, cryCrystalReport, Rc, MaximoFilas, ref NombreTabla_optional, DescrCortaTabla, Type.Missing, 0, Type.Missing);
		}

        public void Load(string StUsuSQL, string StPassSQL, string StNombreBD, string StPathBD, string StPathReport, dynamic FrForm, object cryCrystalReport, SqlConnection Rc, int MaximoFilas,ref  string NombreTabla_optional)
		{
			string tempRefParam = String.Empty;
			Load(StUsuSQL, StPassSQL, StNombreBD, StPathBD, StPathReport, FrForm, cryCrystalReport, Rc, MaximoFilas, ref NombreTabla_optional, tempRefParam, Type.Missing, 0, Type.Missing);
		}

        public void Load(string StUsuSQL, string StPassSQL, string StNombreBD, string StPathBD, string StPathReport, dynamic FrForm, object cryCrystalReport, SqlConnection Rc, int MaximoFilas)
		{
			string tempRefParam2 = string.Empty;
			string tempRefParam3 = String.Empty;
			Load(StUsuSQL, StPassSQL, StNombreBD, StPathBD, StPathReport, FrForm, cryCrystalReport, Rc, MaximoFilas, ref tempRefParam2, tempRefParam3, Type.Missing, 0, Type.Missing);
		}


		public void INSERT_DPERSONA_itiporec(dynamic Form, SqlConnection RcManten, int lgpersona, string Stdnompers, string Stdap1pers, string Stdap2pers, object iccamasco, string Stitiporec, string Stncolegia)
		{


			string StComando = String.Empty;
			DataSet RsInsertar = null;

            try
            {
                using (TransactionScope transaction = TransScopeBuilder.CreateReadCommited())
                {
                    StComando = "Select * from DPERSONA where 2=1";

                    SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RcManten);
                    RsInsertar = new DataSet();
                    tempAdapter.Fill(RsInsertar);

                    RsInsertar.AddNew();

                    RsInsertar.Tables[0].Rows[0]["GPERSONA"] = lgpersona;
                    RsInsertar.Tables[0].Rows[0]["DNOMPERS"] = Stdnompers.Trim().ToUpper();
                    RsInsertar.Tables[0].Rows[0]["DAP1PERS"] = Stdap1pers.Trim().ToUpper();

                    if (!Convert.IsDBNull(Stdap2pers))
                    {
                        RsInsertar.Tables[0].Rows[0]["DAP2PERS"] = Stdap2pers.Trim().ToUpper();
                    }
                    else
                    {
                        RsInsertar.Tables[0].Rows[0]["DAP2PERS"] = DBNull.Value;
                    }

                    if (!Convert.IsDBNull(iccamasco))
                    {
                        RsInsertar.Tables[0].Rows[0]["CCAMASCO"] = iccamasco;
                    }
                    else
                    {
                        RsInsertar.Tables[0].Rows[0]["CCAMASCO"] = DBNull.Value;
                    }

                    RsInsertar.Tables[0].Rows[0]["FBORRADO"] = DBNull.Value;
                    RsInsertar.Tables[0].Rows[0]["ITIPOREC"] = Stitiporec.Trim().ToUpper();

                    if (!Convert.IsDBNull(Stncolegia))
                    {
                        RsInsertar.Tables[0].Rows[0]["NCOLEGIA"] = Stncolegia.Trim().ToUpper();
                    }
                    else
                    {
                        RsInsertar.Tables[0].Rows[0]["NCOLEGIA"] = DBNull.Value;
                    }

                    string tempQuery = RsInsertar.Tables[0].TableName;
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                    tempAdapter_2.Update(RsInsertar, RsInsertar.Tables[0].TableName);
                    RsInsertar.Close();
                    transaction.Complete();
                    Form.RecogerExitoError(false);
                }
            }
            catch(SqlException ex)
            {
                var errors = new RDO_rdoErrors(ex);
                foreach (SqlError er in errors.rdoErrors)
                {
                    RadMessageBox.Show(er.Number.ToString() + "-" + er.Message, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
                }
                Form.RecogerExitoError(true);
                return;
            }
            catch (Exception ex)
            {
                RadMessageBox.Show(ex.Message, Application.ProductName);
                return;
            }
        }

		public void UPDATE_DPERSONA_itiporec(dynamic Form, SqlConnection RcManten, int lgpersona, string Stdnompers, string Stdap1pers, string Stdap2pers, object iccamasco, string Stitiporec, string Stncolegia)
		{


			string StComando = String.Empty;
			DataSet RsInsertar = null;

			try
			{
                using (TransactionScope transaction = TransScopeBuilder.CreateReadCommited())
                {
                    StComando = "Select * from DPERSONA where GPERSONA=" + lgpersona.ToString();

                    SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RcManten);
                    RsInsertar = new DataSet();
                    tempAdapter.Fill(RsInsertar);

                    RsInsertar.Edit();

                    RsInsertar.Tables[0].Rows[0]["DNOMPERS"] = Stdnompers.Trim().ToUpper();
                    RsInsertar.Tables[0].Rows[0]["DAP1PERS"] = Stdap1pers.Trim().ToUpper();

                    if (!Convert.IsDBNull(Stdap2pers))
                    {
                        RsInsertar.Tables[0].Rows[0]["DAP2PERS"] = Stdap2pers.Trim().ToUpper();
                    }
                    else
                    {
                        RsInsertar.Tables[0].Rows[0]["DAP2PERS"] = DBNull.Value;
                    }

                    if (!Convert.IsDBNull(iccamasco))
                    {
                        RsInsertar.Tables[0].Rows[0]["CCAMASCO"] = iccamasco;
                    }
                    else
                    {
                        RsInsertar.Tables[0].Rows[0]["CCAMASCO"] = DBNull.Value;
                    }

                    RsInsertar.Tables[0].Rows[0]["ITIPOREC"] = Stitiporec.Trim().ToUpper();

                    if (!Convert.IsDBNull(Stncolegia))
                    {
                        RsInsertar.Tables[0].Rows[0]["NCOLEGIA"] = Stncolegia.Trim().ToUpper();
                    }
                    else
                    {
                        RsInsertar.Tables[0].Rows[0]["NCOLEGIA"] = DBNull.Value;
                    }

                    string tempQuery = RsInsertar.Tables[0].TableName;
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                    tempAdapter_2.Update(RsInsertar, RsInsertar.Tables[0].TableName);
                    RsInsertar.Close();
                    transaction.Complete();
                    Form.RecogerExitoError(false);
                }
			}
			catch(SqlException ex)
			{
                var errors = new RDO_rdoErrors(ex);
                foreach (SqlError er in errors.rdoErrors)
				{
					RadMessageBox.Show(er.Number.ToString() + "-" + er.Message, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
				}
				Form.RecogerExitoError(true);
				return;
			}
            catch (Exception ex)
            {
                RadMessageBox.Show(ex.Message, Application.ProductName);
                return;
            }
        }

		public void DELETE_DPERSONA_itiporec(dynamic Form, SqlConnection RcManten, int lgpersona)
		{


			string StComando = String.Empty;
			DataSet RsInsertar = null;

			try
			{
                using (TransactionScope transaction = TransScopeBuilder.CreateReadCommited())
                {
                    StComando = "Select * from DPERSONA where GPERSONA=" + lgpersona.ToString();

                    SqlDataAdapter tempAdapter = new SqlDataAdapter(StComando, RcManten);
                    RsInsertar = new DataSet();
                    tempAdapter.Fill(RsInsertar);

                    RsInsertar.Edit();

                    RsInsertar.Tables[0].Rows[0]["FBORRADO"] = DateTime.Today.ToString("dd/MM/yyyy");

                    string tempQuery = RsInsertar.Tables[0].TableName;
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                    tempAdapter_2.Update(RsInsertar, RsInsertar.Tables[0].TableName);
                    RsInsertar.Close();
                    transaction.Complete();
                    Form.RecogerExitoError(false);
                }
			}
			catch(SqlException ex)
			{
                var errors = new RDO_rdoErrors(ex);
				foreach (SqlError er in errors.rdoErrors)
				{
					RadMessageBox.Show(er.Number.ToString() + "-" + er.Message, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
				}
				Form.RecogerExitoError(true);
				return;
			}
            catch(Exception ex)
            {
                RadMessageBox.Show(ex.Message, Application.ProductName);
                return;
            }
		}
	}
}