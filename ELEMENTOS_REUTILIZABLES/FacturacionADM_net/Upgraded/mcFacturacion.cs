using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Microsoft.CSharp;

namespace FacturacionADM
{
	public class mcFacturacion
	{

		public void proDatos_Paciente(int iGanoregi, int lGnumregi, string stFechaini, string stFechaFin, ref SqlConnection RcCon, dynamic Formulario, string stFechaSis, bool bMensual = false, object esConsulta = null, int iSegursoc = 0, int iServUle = 0, string TablaFacturacion = "")
		{
			//llamo al procedimento del m�dulo bas
			try
			{

				Serrores.oClass = new Mensajes.ClassMensajes();
				Serrores.Obtener_TipoBD_FormatoFechaBD(ref RcCon);
				mbFacturacion.proRecibe_Pac(iGanoregi, lGnumregi, stFechaini, stFechaFin, RcCon, Formulario, stFechaSis, bMensual, esConsulta, iSegursoc, iServUle, TablaFacturacion);

				Serrores.oClass = null;

				Formulario.proReciboError(mbFacturacion.fRetorno);
			}
			catch
			{
				mbFacturacion.fRetorno = true;
				Formulario.proReciboError(mbFacturacion.fRetorno);
			}
		}
	}
}