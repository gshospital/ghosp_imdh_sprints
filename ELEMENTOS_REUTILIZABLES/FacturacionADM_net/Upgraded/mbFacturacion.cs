using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace FacturacionADM
{
	internal static class mbFacturacion
	{

		//creamos un ARRAY DE DOS DIMENSIONES CON LOS CAMPOS Y SI SON
		//OBLIGATORIOS
		//en el primer lugar el valor del campo y
		//en el segundo una 'S' si es obligatorio
		public static string[, ] VaCampos = ArraysHelper.InitializeArray<string[, ]>(new int[]{11, 2}, new int[]{0, 0});
		public static int viGanoregi = 0; //ganoregi del episodio pedido
		public static int vlGnumregi = 0; //gnumregi del episodio pedido
		public static string vstFechaIni = String.Empty; //fecha inicio de la petici�n
		public static string vstFechaFin = String.Empty; //fecha fin de la petici�n
		public static string vstFechaCambio = String.Empty; //fecha de cambio de alg�n dato
		public static string vstFechaSis = String.Empty; //fecha Sistema
		public static string vstGidenpac = String.Empty; //identificador del paciente
		public static string vstIreingre = String.Empty; //indicador de reingreso
		public static string vstFaltplan = String.Empty; //fecha de alta en planta
		public static string vstFechaLLegada = String.Empty; // fecha del ingreso en admisi�n
		public static string vstDiagalt = String.Empty; //diagn�stico al alta del paciente
		public static string vstDNI = String.Empty; //DNI/NIF DEL PACIENTE
		public static string vstExitus = String.Empty; //INDICADOR DE EXITUS
		public static string vstGmotalta = String.Empty; //INDICADOR DE GMOTALTA
		public static SqlConnection vRcConex = null;
		public static string sTipoFacturacion = String.Empty;
		public static DataSet vRrUnion = null;
		public static string stUnion = String.Empty;
		public static string stTablaFacturacion = String.Empty;

		public static bool bPrimerApunteDmovfact = false; // para colocar a cap�n 1� de mes en ULE y fact. mixta
		public static DataSet RrBorrar = null;

		public static bool fRetorno = false; //si es true ha ocurrido algun error

		public static int viSeguS = 0; //c�digo de la Seguridad Social
		public static int viUle = 0; // c�digo del servicio de Ule para facturaci�n
		public static bool bMensual = false; // indicador de si le ha llamado la fact mensual
		public static bool bTemp = false; //indicador de si se llama en modo consulta o para facturar definitivo

		static bool fCambio = false; //me marca si se ha cambiado algo ademas de la cama

		static DataSet tRrGrabar = null; //el que graba en dmovfact

		internal static string procambiadia_mes(string ffecha)
		{
			string stdia = ffecha.Substring(0, Math.Min(2, ffecha.Length));
			string stmes = ffecha.Substring(3, Math.Min(2, ffecha.Length - 3));
			return stmes + "/" + stdia + "/" + ffecha.Substring(6);
		}

		internal static void proGrabar_Apunte(bool bUltimo)
		{

			string tstGrabar = String.Empty;

			if (!bPrimerApunteDmovfact)
			{
				bPrimerApunteDmovfact = true;
			}

			if (bTemp)
			{
				// si es solo para consulta grabo en tabla temporal
				// tstGrabar = "SELECT * FROM #DMOVFACTTEMP WHERE 2=1"
				// el 5/7/99 se graba en tabla f�sica para poder hacer consultas posteriores
				tstGrabar = "SELECT * FROM DMOVFACC WHERE 2=1";
			}
			else
			{
				//tstGrabar = "SELECT * FROM DMOVFACT WHERE 2=1"
				//Grabamos en una tabla temporal con lo mismo que se graba en DMOVFACT
				tstGrabar = "SELECT * FROM " + stTablaFacturacion + " WHERE 2=1";
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstGrabar, vRcConex);
			tRrGrabar = new DataSet();
			tempAdapter.Fill(tRrGrabar);
			tRrGrabar.AddNew();
			tRrGrabar.Tables[0].Rows[0]["GIDENPAC"] = vstGidenpac;
			// 17/2/2000: todas las estancias son externas porque no afectan a las
			// condiciones de facturaci�n
			tRrGrabar.Tables[0].Rows[0]["ITPACFAC"] = "E";
			tRrGrabar.Tables[0].Rows[0]["ITIPOSER"] = "H";
			tRrGrabar.Tables[0].Rows[0]["GANOREGI"] = viGanoregi;
			tRrGrabar.Tables[0].Rows[0]["GNUMREGI"] = vlGnumregi;
			tRrGrabar.Tables[0].Rows[0]["itiporig"] = tRrGrabar.Tables[0].Rows[0]["itiposer"];
			tRrGrabar.Tables[0].Rows[0]["ganoorig"] = tRrGrabar.Tables[0].Rows[0]["ganoregi"];
			tRrGrabar.Tables[0].Rows[0]["gnumorig"] = tRrGrabar.Tables[0].Rows[0]["gnumregi"];
			if (VaCampos[9, 0] != "")
			{
				tRrGrabar.Tables[0].Rows[0]["GCODEVEN"] = "AU";
			}
			else
			{
				tRrGrabar.Tables[0].Rows[0]["GCODEVEN"] = "ES";
			}
			tRrGrabar.Tables[0].Rows[0]["GSERREAL"] = Conversion.Val(VaCampos[2, 0]);
			// 17/2/2000: el servicio solicitante siempre es nulo
			tRrGrabar.Tables[0].Rows[0]["IREINGRE"] = vstIreingre;
			tRrGrabar.Tables[0].Rows[0]["GTIPOCAM"] = VaCampos[4, 0];
			tRrGrabar.Tables[0].Rows[0]["IREGALOJ"] = VaCampos[5, 0];
			tRrGrabar.Tables[0].Rows[0]["GPERSONA"] = VaCampos[3, 0];
			if (bUltimo && vstFaltplan != "")
			{
				System.DateTime TempDate2 = DateTime.FromOADate(0);
				System.DateTime TempDate = DateTime.FromOADate(0);
                //if (DateTime.Parse((DateTime.TryParse(VaCampos[1, 0], out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : VaCampos[1, 0]) == DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm:ss") : vstFaltplan))
                if(DateTime.Parse(VaCampos[1,0].ToString()).ToString("dd/MM/yyyy HH:mm:ss") == DateTime.Parse(vstFaltplan.ToString()).ToString("dd/MM/yyyy HH:mm:ss"))
                {
					tRrGrabar.Tables[0].Rows[0]["IEXITUSP"] = vstExitus;
					if (vstGmotalta != "")
					{
						tRrGrabar.Tables[0].Rows[0]["GMOTALTA"] = vstGmotalta;
					}
					else
					{
						tRrGrabar.Tables[0].Rows[0]["GMOTALTA"] = DBNull.Value;
					}
					if (vstDiagalt != "")
					{
						tRrGrabar.Tables[0].Rows[0]["GDIAGALT"] = (vstDiagalt);
					}
					else
					{
						tRrGrabar.Tables[0].Rows[0]["GDIAGALT"] = DBNull.Value;
					}
				}
			}
			tRrGrabar.Tables[0].Rows[0]["GSOCIEDA"] = Conversion.Val(VaCampos[6, 0]);
			tRrGrabar.Tables[0].Rows[0]["FSISTEMA"] = DateTime.Parse(vstFechaSis);
			tRrGrabar.Tables[0].Rows[0]["FFINICIO"] = DateTime.Parse(VaCampos[0, 0]);
			tRrGrabar.Tables[0].Rows[0]["FFECHFIN"] = DateTime.Parse(VaCampos[1, 0]);
			if (VaCampos[8, 0] != "")
			{
				tRrGrabar.Tables[0].Rows[0]["ginspecc"] = VaCampos[8, 0];
			}
			else
			{
				tRrGrabar.Tables[0].Rows[0]["GINSPECC"] = DBNull.Value;
			}
			if (VaCampos[7, 0] != "")
			{
				tRrGrabar.Tables[0].Rows[0]["NAFILIAC"] = VaCampos[7, 0];
			}
			else
			{
				tRrGrabar.Tables[0].Rows[0]["NAFILIAC"] = DBNull.Value;
			}
			tRrGrabar.Tables[0].Rows[0]["IREGIQUI"] = VaCampos[10, 0];
			if (vstDNI != "")
			{
				tRrGrabar.Tables[0].Rows[0]["NDNINIFP"] = vstDNI;
			}
			else
			{
				tRrGrabar.Tables[0].Rows[0]["NDNINIFP"] = DBNull.Value;
			}
			string sFechaBisiesto = String.Empty;
			if (bMensual)
			{
				//pongo el �ltimo d�a de mes porque si no me graba la fecha de alta
				System.DateTime TempDate6 = DateTime.FromOADate(0);
				System.DateTime TempDate5 = DateTime.FromOADate(0);
				System.DateTime TempDate4 = DateTime.FromOADate(0);
				System.DateTime TempDate3 = DateTime.FromOADate(0);
                //if (DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : vstFechaFin).Month == 11 || DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy") : vstFechaFin).Month == 4 || DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy") : vstFechaFin).Month == 6 || DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy") : vstFechaFin).Month == 9)
                if(DateTime.Parse(vstFechaFin.ToString()).Month == 11 || DateTime.Parse(vstFechaFin.ToString()).Month == 4 || DateTime.Parse(vstFechaFin.ToString()).Month == 6 || DateTime.Parse(vstFechaFin.ToString()).Month == 9)
                {
					System.DateTime TempDate8 = DateTime.FromOADate(0);
					System.DateTime TempDate7 = DateTime.FromOADate(0);
					//tRrGrabar.Tables[0].Rows[0]["FPERFACT"] = DateTime.Parse("30/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate8)) ? TempDate8.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Year.ToString() + " 23:59:59");
                    tRrGrabar.Tables[0].Rows[0]["FPERFACT"] = DateTime.Parse("30/" + DateTime.Parse(vstFechaFin.ToString()).Month + DateTime.Parse(vstFechaFin.ToString()).Year + " 23:59:59");
                }
				else
				{
					System.DateTime TempDate9 = DateTime.FromOADate(0);
					//if (DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate9)) ? TempDate9.ToString("dd/MM/yyyy") : vstFechaFin).Month == 2)
                      if (DateTime.Parse(vstFechaFin.ToString()).Month == 2)
                        {
						System.DateTime TempDate11 = DateTime.FromOADate(0);
						System.DateTime TempDate10 = DateTime.FromOADate(0);
						//sFechaBisiesto = "29/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate10)) ? TempDate10.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate11)) ? TempDate11.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Year.ToString() + " 23:59:59";
                        sFechaBisiesto = "29/" + DateTime.Parse(vstFechaFin.ToString()).Month + "/" + DateTime.Parse(vstFechaFin.ToString()).Year + " 23:59:59";
                        if (Information.IsDate(sFechaBisiesto))
						{
							System.DateTime TempDate13 = DateTime.FromOADate(0);
							System.DateTime TempDate12 = DateTime.FromOADate(0);
							//tRrGrabar.Tables[0].Rows[0]["FPERFACT"] = DateTime.Parse("29/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate12)) ? TempDate12.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate13)) ? TempDate13.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Year.ToString() + " 23:59:59");
                            tRrGrabar.Tables[0].Rows[0]["FPERFACT"] = DateTime.Parse("29/" + DateTime.Parse(vstFechaFin.ToString()).Month + "/" + DateTime.Parse(vstFechaFin.ToString()).Year + " 23:59:59");
                        }
						else
						{
							System.DateTime TempDate15 = DateTime.FromOADate(0);
							System.DateTime TempDate14 = DateTime.FromOADate(0);
							//tRrGrabar.Tables[0].Rows[0]["FPERFACT"] = DateTime.Parse("28/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate14)) ? TempDate14.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate15)) ? TempDate15.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Year.ToString() + " 23:59:59");
                            tRrGrabar.Tables[0].Rows[0]["FPERFACT"] = DateTime.Parse("28/" + DateTime.Parse(vstFechaFin.ToString()).Month + "/" + DateTime.Parse(vstFechaFin.ToString()).Year + " 23:59:59");
                        }
					}
					else
					{
						System.DateTime TempDate17 = DateTime.FromOADate(0);
						System.DateTime TempDate16 = DateTime.FromOADate(0);
						//tRrGrabar.Tables[0].Rows[0]["FPERFACT"] = DateTime.Parse("31/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate16)) ? TempDate16.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate17)) ? TempDate17.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Year.ToString() + " 23:59:59");
                        tRrGrabar.Tables[0].Rows[0]["FPERFACT"] = DateTime.Parse("31/" + DateTime.Parse(vstFechaFin.ToString()).Month + "/" + DateTime.Parse(vstFechaFin.ToString()).Year + " 23:59:59");
                    }
				}
			}
			else
			{
				tRrGrabar.Tables[0].Rows[0]["FPERFACT"] = DateTime.Parse(vstFechaFin);
			}
			string tempQuery = tRrGrabar.Tables[0].TableName;
			SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
            tempAdapter.Update(tRrGrabar, tRrGrabar.Tables[0].TableName);
			tRrGrabar.Close();


		}
		internal static void proRecibe_Pac(int iGanoregi, int lGnumregi, string stFechaini, string stFechaFin, SqlConnection RcCon, object Formu, string stFeSistema, bool? esMensual_optional, object esConsulta_optional, int? iSegursoc_optional, int iServiUle, object TablaFacturacion_optional)
		{
			string TablaFacturacion = (TablaFacturacion_optional == Type.Missing) ? String.Empty : TablaFacturacion_optional as string;
			bool esMensual = (esMensual_optional == null) ? false : esMensual_optional.Value;
			object esConsulta = (esConsulta_optional == Type.Missing) ? null : esConsulta_optional as object;
			int iSegursoc = (iSegursoc_optional == null) ? 0 : iSegursoc_optional.Value;


			vRcConex = RcCon;

			VaCampos[0, 0] = "";
			VaCampos[1, 0] = "";
			VaCampos[2, 0] = "";
			VaCampos[3, 0] = "";
			VaCampos[4, 0] = "";
			VaCampos[5, 0] = "";
			VaCampos[6, 0] = "";
			VaCampos[7, 0] = "";
			VaCampos[8, 0] = "";
			VaCampos[9, 0] = "";
			VaCampos[10, 0] = "";

			//asigno los valores a la Matriz
			//VaCampos(1, 1) = "FMOVIMIE"
			VaCampos[0, 1] = "S";
			//VaCampos(2, 1) = "FFINMOVI"
			VaCampos[1, 1] = "N";
			//VaCampos(3, 1) = "GSERVIOR"
			VaCampos[2, 1] = "S";
			//VaCampos(4, 1) = "GMEDICOR"
			VaCampos[3, 1] = "S";
			//VaCampos(5, 1) = "GTIPOCAM"
			VaCampos[4, 1] = "S";
			//VaCampos(6, 1) = "IREGALOJ"
			VaCampos[5, 1] = "S";
			//VaCampos(7, 1) = "GSOCIEDA"
			VaCampos[6, 1] = "S";
			//VaCampos(8, 1) = "NAFILIAC"
			VaCampos[7, 1] = "N";
			//VaCampos(9, 1) = "GINSPECC"
			VaCampos[8, 1] = "N";
			//VaCampos(10, 1) = "GMOTAUSE"
			VaCampos[9, 1] = "N";
			//VaCampos(11, 1) = "IREGIMEN"
			VaCampos[10, 1] = "S";

			viGanoregi = iGanoregi;
			vlGnumregi = lGnumregi;
			//CUIDADO CON EL FORMATO TIENE QUE SER dd/mm/yyyy HH:NN:SS
			vstFechaIni = stFechaini;
			vstFechaFin = stFechaFin;
			vstFechaSis = stFeSistema;

			if (TablaFacturacion_optional == Type.Missing)
			{
				stTablaFacturacion = "DMOVFACT";
			}
			else
			{
				stTablaFacturacion = TablaFacturacion;
			}

			if (esMensual_optional != null)
			{
				bMensual = esMensual;
				System.DateTime TempDate = DateTime.FromOADate(0);
				vstFechaIni = (DateTime.TryParse(vstFechaIni, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaIni;
                //vstFechaIni = (DateTime.TryParse(vstFechaIni));
                System.DateTime TempDate2 = DateTime.FromOADate(0);
				vstFechaFin = (DateTime.TryParse(vstFechaFin, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin;
			}
			else
			{
				bMensual = false;
			}

			bTemp = esConsulta_optional != Type.Missing;
			fRetorno = false;

			// en la facturaci�n mensual se manda como par�metro el
			// c�digo de la SS y de la ULE por lo que no hace falta leerlos
			if (iSegursoc_optional == null)
			{
				Sociedad_SS();
			}
			else
			{
				viSeguS = iSegursoc;
				viUle = iServiUle;
			}
			proAepisadm(iGanoregi, lGnumregi);
			//REALIZO LA CONSULTA UNION Y LA DEJO ABIERTA
			//es la variable vRrUnion
			proUnion();
			//compruebo que hay algo en la union (hay movimientos sin facturar en mi periodo

			if (vRrUnion.Tables[0].Rows.Count == 0)
			{
				vRrUnion.Close();
				// si no hay nada para facturar me salgo de la dll
				return;
			}

			// guardo la fecha de partida para grabar en dmovfact
			VaCampos[0, 0] = vstFechaIni;
			VaCampos[1, 0] = vstFechaFin;

			// relleno en un array los datos de partida para dmovfact
			// para ello tengo que leer al menos la primera fila
			// de la union de cada tabla
			int iContador = 1;

			while(iContador != 5)
			{
				Rellenar_Array();
				vRrUnion.MoveNext();
				iContador++;
			};
			if (vRrUnion.Tables[0].Rows.Count != 0)
			{
				// me guardo la primera fecha en que ha habido un cambio
				vstFechaCambio = Convert.ToDateTime(vRrUnion.Tables[0].Rows[0]["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss");
			}

			// inicializo la variable para controlar el primer apunte que he grabado en DMOVFACT
			bPrimerApunteDmovfact = false;


			while(vRrUnion.Tables[0].Rows.Count != 0)
			{
				// comprueba si los datos le�dos en la Uni�n no
				// han variado respecto a lo que afecta a DMOVFACT
				if (fnCompararUnionArray())
				{
					// si ha variado se graba un apunte con lo guardado
					// si la financiadora y tipo de facturaci�n lo indican
					VaCampos[1, 0] = vstFechaCambio;
					if (fnHayQueFacturar())
					{
						proGrabar_Apunte(false);
					}
					// guardo los datos nuevos le�dos en el array
					Rellenar_Array();
					VaCampos[0, 0] = vstFechaCambio;
				}
				vRrUnion.MoveNext();
				if (vRrUnion.Tables[0].Rows.Count != 0)
				{
					// me guardo la fecha en que ha habido un cambio
					vstFechaCambio = Convert.ToDateTime(vRrUnion.Tables[0].Rows[0]["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss");
				}
			};
			// siempre queda un apunte pendiente por grabar en dmovfact
			// con la fecha de cierre

			VaCampos[1, 0] = vstFechaFin;
			if (fnHayQueFacturar())
			{
				proGrabar_Apunte(true);
			}
			if (bPrimerApunteDmovfact)
			{
				// Modificaci�n del 14/2/00
				// si he grabado estancias, grabo los periodos de ausencias
				proAusencias(viGanoregi, vlGnumregi);
			}

			vRrUnion.Close();

			return;


			fRetorno = true;
		}

		internal static void proRecibe_Pac(int iGanoregi, int lGnumregi, string stFechaini, string stFechaFin, SqlConnection RcCon, object Formu, string stFeSistema, bool? esMensual_optional, object esConsulta_optional, int? iSegursoc_optional, int iServiUle)
		{
			proRecibe_Pac(iGanoregi, lGnumregi, stFechaini, stFechaFin, RcCon, Formu, stFeSistema, esMensual_optional, esConsulta_optional, iSegursoc_optional, iServiUle, Type.Missing);
		}

		internal static void proRecibe_Pac(int iGanoregi, int lGnumregi, string stFechaini, string stFechaFin, SqlConnection RcCon, object Formu, string stFeSistema, bool? esMensual_optional, object esConsulta_optional, int? iSegursoc_optional)
		{
			proRecibe_Pac(iGanoregi, lGnumregi, stFechaini, stFechaFin, RcCon, Formu, stFeSistema, esMensual_optional, esConsulta_optional, iSegursoc_optional, 0, Type.Missing);
		}

		internal static void proRecibe_Pac(int iGanoregi, int lGnumregi, string stFechaini, string stFechaFin, SqlConnection RcCon, object Formu, string stFeSistema, bool? esMensual_optional, object esConsulta_optional)
		{
			proRecibe_Pac(iGanoregi, lGnumregi, stFechaini, stFechaFin, RcCon, Formu, stFeSistema, esMensual_optional, esConsulta_optional, null, 0, Type.Missing);
		}

		internal static void proRecibe_Pac(int iGanoregi, int lGnumregi, string stFechaini, string stFechaFin, SqlConnection RcCon, object Formu, string stFeSistema, bool? esMensual_optional)
		{
			proRecibe_Pac(iGanoregi, lGnumregi, stFechaini, stFechaFin, RcCon, Formu, stFeSistema, esMensual_optional, Type.Missing, null, 0, Type.Missing);
		}

		internal static void proRecibe_Pac(int iGanoregi, int lGnumregi, string stFechaini, string stFechaFin, SqlConnection RcCon, object Formu, string stFeSistema)
		{
			proRecibe_Pac(iGanoregi, lGnumregi, stFechaini, stFechaFin, RcCon, Formu, stFeSistema, null, Type.Missing, null, 0, Type.Missing);
		}

		internal static void proAepisadm(int ganoregi, int gnumregi)
		{
			//busca el gidenpac, el reingreso y la fecha de alta en planta de la
			//tabla aepisadm y los guarda en unas variables
			string tstDmotalta = String.Empty;
			DataSet tRrDmotalta = null;
			string sItipingr = String.Empty;
			int iGanrelur = 0;
			int lGnurelur = 0;

			vstExitus = "N";
			string tstAepisadm = "SELECT FLLEGADA, GIDENPAC,IREINGRE,FALTPLAN,GDIAGALT,GMOTALTA, ITIPINGR, GANRELUR, GNURELUR " + " FROM AEPISADM " + " WHERE ganoadme=" + ganoregi.ToString() + " AND " + " gnumadme=" + gnumregi.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstAepisadm, vRcConex);
			DataSet tRrAepisadm = new DataSet();
			tempAdapter.Fill(tRrAepisadm);
			if (tRrAepisadm.Tables[0].Rows.Count != 0)
			{
				vstGidenpac = Convert.ToString(tRrAepisadm.Tables[0].Rows[0]["gidenpac"]);
				vstFechaLLegada = Convert.ToString(tRrAepisadm.Tables[0].Rows[0]["fllegada"]);
				if (!Convert.IsDBNull(tRrAepisadm.Tables[0].Rows[0]["IREINGRE"]))
				{
					vstIreingre = Convert.ToString(tRrAepisadm.Tables[0].Rows[0]["ireingre"]);
				}
				else
				{
					vstIreingre = "";
				}
				if (!Convert.IsDBNull(tRrAepisadm.Tables[0].Rows[0]["faltplan"]))
				{
					vstFaltplan = Convert.ToDateTime(tRrAepisadm.Tables[0].Rows[0]["faltplan"]).ToString("dd/MM/yyyy HH:mm:ss");
				}
				else
				{
					vstFaltplan = "";
				}
				if (!Convert.IsDBNull(tRrAepisadm.Tables[0].Rows[0]["gdiagalt"]))
				{
					vstDiagalt = Convert.ToString(tRrAepisadm.Tables[0].Rows[0]["gdiagalt"]);
				}
				else
				{
					vstDiagalt = "";
				}
				if (!Convert.IsDBNull(tRrAepisadm.Tables[0].Rows[0]["gMOTALTA"]))
				{
					vstGmotalta = Convert.ToString(tRrAepisadm.Tables[0].Rows[0]["gMOTALTA"]);
					tstDmotalta = "SELECT icexitus FROM DMOTALTA " + " WHERE GMOTALTA =" + Convert.ToString(tRrAepisadm.Tables[0].Rows[0]["gMOTALTA"]);
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstDmotalta, vRcConex);
					tRrDmotalta = new DataSet();
					tempAdapter_2.Fill(tRrDmotalta);
					if (tRrDmotalta.Tables[0].Rows.Count != 0)
					{
						vstExitus = Convert.ToString(tRrDmotalta.Tables[0].Rows[0]["icexitus"]);
					}
					tRrDmotalta.Close();
				}
				else
				{
					vstGmotalta = "";
				}
				sItipingr = Convert.ToString(tRrAepisadm.Tables[0].Rows[0]["itipingr"]);
				if (!Convert.IsDBNull(tRrAepisadm.Tables[0].Rows[0]["ganrelur"]))
				{
					iGanrelur = Convert.ToInt32(tRrAepisadm.Tables[0].Rows[0]["ganrelur"]);
					lGnurelur = Convert.ToInt32(tRrAepisadm.Tables[0].Rows[0]["gnurelur"]);
				}
				else
				{
					iGanrelur = 0;
				}
			}
			tRrAepisadm.Close();
			//AHORA SACO LOS DATOS DE DPACIENT
			tstAepisadm = "SELECT NDNINIFP,IEXITUSP" + " FROM DPACIENT " + " WHERE GIDENPAC='" + vstGidenpac + "'";

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstAepisadm, vRcConex);
			tRrAepisadm = new DataSet();
			tempAdapter_3.Fill(tRrAepisadm);
			if (!Convert.IsDBNull(tRrAepisadm.Tables[0].Rows[0]["ndninifp"]))
			{
				vstDNI = Convert.ToString(tRrAepisadm.Tables[0].Rows[0]["ndninifp"]);
			}
			else
			{
				vstDNI = "";
			}
			// cambio de 25/2/99: cojo el ind. exitus de AEPISADM en principio
			// vstExitus = tRrAepisadm("iexitusp")
			tRrAepisadm.Close();

			// 17/2/2000 : en estancias ya no se distingue entre tipo interno
			// y externo. Todos son externos y nunca hay servicio solicitante

		}
		internal static void proAusencias(int ganoregi, int gnumregi)
		{
			// Grabo una fila en dmovfact seg�n las ausencias que ha tenido
			// y la financiadora y datos m�dicos del inicio de la ausencia
			string tstAmovifin = String.Empty;
			DataSet tRrAmovifin = null;
			//29/01/2002 Pongo orden a todas las select porque habia casos en los que las ausencias
			//no se facturaba bien. Al hacer la select habia veces que se recuperaban varios registro
			//y al hacer el MoveLast el ultimo no siempre coincidia con el adecuado

			System.DateTime TempDate = DateTime.FromOADate(0);
			object tempRefParam = ((DateTime.TryParse(vstFechaFin, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : vstFechaFin) + " 23:59:59";
			object tempRefParam2 = vstFechaIni;
			string tstAusenci = "SELECT * " + " FROM AAUSENCI " + " WHERE ganoadme=" + ganoregi.ToString() + " AND " + " gnumadme =" + gnumregi.ToString() + " AND " + " fausenci<=" + Serrores.FormatFechaHMS(tempRefParam) + " and " + " (freavuel>=" + Serrores.FormatFechaHMS(tempRefParam2) + "" + "  or freavuel is null) order by fausenci ";
			vstFechaIni = Convert.ToString(tempRefParam2);

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstAusenci, vRcConex);
			DataSet tRrAusenci = new DataSet();
			tempAdapter.Fill(tRrAusenci);
			if (tRrAusenci.Tables[0].Rows.Count != 0)
			{
				tRrAusenci.MoveFirst();
				foreach (DataRow iteration_row in tRrAusenci.Tables[0].Rows)
				{
					// cargo las fechas por defecto porque si no las pierdo
					// al grabar apunte las modifico seg�n tipo de facturaci�n
					//06/05/2009 graba la estancia completa sin tener en cuenta el periodo de facturaci�n
					//        VaCampos(1, 1) = Format(tRrAusenci("fausenci"), "DD/MM/YYYY HH:NN:SS")
					//        If Not IsNull(tRrAusenci("freavuel")) Then
					//            VaCampos(2, 1) = Format(tRrAusenci("freavuel"), "DD/MM/YYYY HH:NN:SS")
					//        Else
					//            VaCampos(2, 1) = Format(vstFechaFin, "DD/MM/YYYY HH:NN:SS")
					//        End If
					System.DateTime TempDate2 = DateTime.FromOADate(0);
					if (DateTime.Parse((DateTime.TryParse(vstFechaIni, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaIni) > DateTime.Parse(Convert.ToDateTime(iteration_row["fausenci"]).ToString("dd/MM/yyyy HH:mm:ss")))
                    {
						System.DateTime TempDate3 = DateTime.FromOADate(0);
						VaCampos[0, 0] = (DateTime.TryParse(vstFechaIni, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaIni;
					}
					else
					{
						VaCampos[0, 0] = Convert.ToDateTime(iteration_row["fausenci"]).ToString("dd/MM/yyyy HH:mm:ss");
					}
					if (!Convert.IsDBNull(iteration_row["freavuel"]))
					{
						System.DateTime TempDate4 = DateTime.FromOADate(0);
						if (DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin) < DateTime.Parse(Convert.ToDateTime(iteration_row["freavuel"]).ToString("dd/MM/yyyy HH:mm:ss")))
						{
							System.DateTime TempDate5 = DateTime.FromOADate(0);
							VaCampos[1, 0] = (DateTime.TryParse(vstFechaFin, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin;
						}
						else
						{
							VaCampos[1, 0] = Convert.ToDateTime(iteration_row["freavuel"]).ToString("dd/MM/yyyy HH:mm:ss");
						}
					}
					else
					{
						System.DateTime TempDate6 = DateTime.FromOADate(0);
						VaCampos[1, 0] = (DateTime.TryParse(vstFechaFin, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin;
					}

					VaCampos[9, 0] = Convert.ToString(iteration_row["gmotause"]);

					// busco los datos en las tablas de movimientos
					// de la fecha en que se inicia la ausencia
					object tempRefParam3 = iteration_row["fausenci"];
					tstAmovifin = "SELECT max(fmovimie), gservior, gmedicor, gtipocam FROM AMOVIMIE,DCAMASBO " + " WHERE " + " itiposer = 'H' AND " + " ganoregi =" + ganoregi.ToString() + " AND " + " gnumregi =" + gnumregi.ToString() + " AND " + " fmovimie < " + Serrores.FormatFechaHMS(tempRefParam3) + " AND " + " gplantas = gplantor and " + " ghabitac = ghabitor and " + " gcamasbo = gcamaori " + " group by gservior, gmedicor, gtipocam  order by  max(fmovimie)";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstAmovifin, vRcConex);
					tRrAmovifin = new DataSet();
					tempAdapter_2.Fill(tRrAmovifin);
					tRrAmovifin.MoveLast(null);
					VaCampos[2, 0] = Convert.ToString(tRrAmovifin.Tables[0].Rows[0]["gservior"]);
					VaCampos[3, 0] = Convert.ToString(tRrAmovifin.Tables[0].Rows[0]["gmedicor"]);
					VaCampos[4, 0] = Convert.ToString(tRrAmovifin.Tables[0].Rows[0]["gtipocam"]);

					object tempRefParam4 = iteration_row["fausenci"];
					tstAmovifin = "SELECT max(fmovimie), iregaloj FROM AMOVREGA " + " WHERE " + " itiposer = 'H' AND " + " ganoregi =" + ganoregi.ToString() + " AND " + " gnumregi =" + gnumregi.ToString() + " AND " + " fmovimie < " + Serrores.FormatFechaHMS(tempRefParam4) + " group by iregaloj order by  max(fmovimie) ";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstAmovifin, vRcConex);
					tRrAmovifin = new DataSet();
					tempAdapter_3.Fill(tRrAmovifin);
					tRrAmovifin.MoveLast(null);
					VaCampos[5, 0] = Convert.ToString(tRrAmovifin.Tables[0].Rows[0]["iregaloj"]);

					object tempRefParam5 = iteration_row["fausenci"];
					tstAmovifin = "SELECT max(fmovimie), iregimen FROM AREGIMEN " + " WHERE " + " ganoadme =" + ganoregi.ToString() + " AND " + " gnumadme =" + gnumregi.ToString() + " AND " + " fmovimie < " + Serrores.FormatFechaHMS(tempRefParam5) + " group by iregimen order by  max(fmovimie) ";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tstAmovifin, vRcConex);
					tRrAmovifin = new DataSet();
					tempAdapter_4.Fill(tRrAmovifin);
					tRrAmovifin.MoveLast(null);
					VaCampos[10, 0] = Convert.ToString(tRrAmovifin.Tables[0].Rows[0]["iregimen"]);

					object tempRefParam6 = iteration_row["fausenci"];
					tstAmovifin = "SELECT max(fmovimie), gsocieda,ginspecc,nafiliac FROM AMOVIFIN " + " WHERE " + " itiposer = 'H' AND " + " ganoregi =" + ganoregi.ToString() + " AND " + " gnumregi =" + gnumregi.ToString() + " AND " + " fmovimie < " + Serrores.FormatFechaHMS(tempRefParam6) + " group by gsocieda, ginspecc, nafiliac order by  max(fmovimie) ";
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tstAmovifin, vRcConex);
					tRrAmovifin = new DataSet();
					tempAdapter_5.Fill(tRrAmovifin);
					tRrAmovifin.MoveLast(null);
					VaCampos[6, 0] = Convert.ToString(tRrAmovifin.Tables[0].Rows[0]["gsocieda"]);
					if (!Convert.IsDBNull(tRrAmovifin.Tables[0].Rows[0]["ginspecc"]))
					{
						VaCampos[8, 0] = Convert.ToString(tRrAmovifin.Tables[0].Rows[0]["ginspecc"]);
					}
					else
					{
						VaCampos[8, 0] = "";
					}
					if (!Convert.IsDBNull(tRrAmovifin.Tables[0].Rows[0]["nafiliac"]))
					{
						VaCampos[7, 0] = Convert.ToString(tRrAmovifin.Tables[0].Rows[0]["nafiliac"]);
					}
					else
					{
						VaCampos[7, 0] = "";
					}
					// los campos del alta los dejo a nulos
					vstExitus = "N";
					vstGmotalta = "";
					vstDiagalt = "";
					// grabo en dmovfact
					if (fnHayQueFacturar())
					{
						proGrabar_Apunte(false);
					}
				}
			}
			tRrAusenci.Close();

		}

		internal static void proUnion()
		{

			// cambio de Diciembre por facturarse distinto lo mensual. Ya no se va a poner la marca
			// Se coger�n los apuntes del periodo de facturaci�n
			// y los que est�n todav�a abiertos
			object tempRefParam = vstFechaFin;
			object tempRefParam2 = vstFechaIni;
			string tstVar = "  Ganoregi = " + viGanoregi.ToString() + " And Gnumregi = " + vlGnumregi.ToString() + "" + " and fmovimie <=" + Serrores.FormatFechaHMS(tempRefParam) + " " + " and (ffinmovi>=" + Serrores.FormatFechaHMS(tempRefParam2) + " " + " or ffinmovi is null) ";
			vstFechaIni = Convert.ToString(tempRefParam2);
			vstFechaFin = Convert.ToString(tempRefParam);

			//parte de amovimie y dcamasbo
			stUnion = " select fmovimie,ffinmovi,gservior,gmedicor,gtipocam, " + " null as iregaloj,null as gsocieda,null as nafiliac ,null as ginspecc,null as gmotause,null as iregimen " + " from amovimie,dcamasbo " + " where gplantor=gplantas and ghabitor=ghabitac and gcamaori=gcamasbo " + " and " + tstVar;
			//ahora la uno con la select de amovrega
			stUnion = stUnion + " union all ";
			stUnion = stUnion + " select fmovimie,ffinmovi,null,null,null,iregaloj,null,null,null,null,null ";
			stUnion = stUnion + " FROM AMOVREGA WHERE ";
			stUnion = stUnion + tstVar;
			//union con amovifin
			stUnion = stUnion + " union all ";
			stUnion = stUnion + " select fmovimie,ffinmovi,null,null,null,null,gsocieda,nafiliac,ginspecc,null,null ";
			stUnion = stUnion + " FROM amovifin WHERE ";
			stUnion = stUnion + tstVar;
			//union con AREGIMEN
			stUnion = stUnion + " union all ";
			stUnion = stUnion + " select fmovimie,ffinmovi,null,null,null,null,null,null,null,null,iregimen ";
			stUnion = stUnion + " FROM aregimen WHERE ";
			stUnion = stUnion + " ganoadme=" + viGanoregi.ToString() + " and gnumadme=" + vlGnumregi.ToString();
			object tempRefParam3 = vstFechaFin;
			stUnion = stUnion + " and fmovimie <=" + Serrores.FormatFechaHMS(tempRefParam3) + " ";
			vstFechaFin = Convert.ToString(tempRefParam3);
			object tempRefParam4 = vstFechaIni;
			stUnion = stUnion + " and (ffinmovi>=" + Serrores.FormatFechaHMS(tempRefParam4) + " ";
			vstFechaIni = Convert.ToString(tempRefParam4);
			stUnion = stUnion + " or ffinmovi is null) ";
			stUnion = stUnion + " order by fmovimie";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stUnion, vRcConex);
			vRrUnion = new DataSet();
			tempAdapter.Fill(vRrUnion);
			if (vRrUnion.Tables[0].Rows.Count != 0)
			{
				vRrUnion.MoveFirst();
			}


		}
		internal static bool fnHayQueFacturar()
		{
			//Funci�n que comprueba si realmente hay que facturar los
			// datos guardados en el array en funci�n del tipo de llamada a la dll
			// Mensual/Al alta y en funci�n del tipo de facturaci�n (imensual):
			// M=mensual, X=mixta, A=al alta

			bool result = false;
			string tstLeer = String.Empty;
			string sPrimeroMes = String.Empty;

			sTipoFacturacion = "A";
			// Modificaci�n 2/3/00: si la sociedad es Seguridad Social el
			// indicador de imensual hay que leerlo de la Inspecci�n
			if (Conversion.Val(VaCampos[6, 0]) == viSeguS)
			{
				// la sociedad es Seguridad Social
				tstLeer = "Select imensual from dinspecc where ginspecc = '" + VaCampos[8, 0] + "'";
			}
			else
			{
				tstLeer = "Select imensual from dsocieda where gsocieda = " + Conversion.Val(VaCampos[6, 0]).ToString();
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstLeer, vRcConex);
			DataSet tRrLeer = new DataSet();
			tempAdapter.Fill(tRrLeer);
			if (tRrLeer.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(tRrLeer.Tables[0].Rows[0]["imensual"]))
				{
					sTipoFacturacion = Convert.ToString(tRrLeer.Tables[0].Rows[0]["imensual"]).Trim();
				}
			}

			if (bMensual)
			{
				if (Conversion.Val(VaCampos[2, 0]) == viUle || sTipoFacturacion == "X")
				{
					result = true;
					if (vstFaltplan != "")
					{
						System.DateTime TempDate2 = DateTime.FromOADate(0);
						System.DateTime TempDate = DateTime.FromOADate(0);
						sPrimeroMes = "01/" + DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : vstFaltplan).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm:ss") : vstFaltplan).Year.ToString() + " 00:00:00";
						System.DateTime TempDate6 = DateTime.FromOADate(0);
						System.DateTime TempDate5 = DateTime.FromOADate(0);
						System.DateTime TempDate4 = DateTime.FromOADate(0);
						System.DateTime TempDate3 = DateTime.FromOADate(0);
						if (DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : vstFaltplan).Month == DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy") : vstFechaFin).Month && DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy") : vstFaltplan).Year == DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy") : vstFechaFin).Year)
						{
							// si es la facturaci�n mensual de apuntes del mes de alta de ULE o Mixta
							// no grabo porque se factura al alta
							result = false;
						}
					}
				}
				else
				{
					if (sTipoFacturacion == "A")
					{
						result = false;
					}
					else
					{
						// el tipo de facturaci�n es mensual
						result = true;
					}
				}
				if (VaCampos[9, 0] != "")
				{
					// si es una ausencia
					System.DateTime TempDate8 = DateTime.FromOADate(0);
					System.DateTime TempDate7 = DateTime.FromOADate(0);
					if (DateTime.Parse((DateTime.TryParse(VaCampos[0, 0], out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy HH:mm:ss") : VaCampos[0, 0]) < DateTime.Parse((DateTime.TryParse(vstFechaIni, out TempDate8)) ? TempDate8.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaIni))
					{
						// si se ha ausentado en el periodo anterior de facturaci�n
						// le facturo desde el primero de mes de la fecha fin
						System.DateTime TempDate10 = DateTime.FromOADate(0);
						System.DateTime TempDate9 = DateTime.FromOADate(0);
						VaCampos[0, 0] = "01/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate9)) ? TempDate9.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate10)) ? TempDate10.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Year.ToString() + " 00:00:00";
					}
					System.DateTime TempDate12 = DateTime.FromOADate(0);
					System.DateTime TempDate11 = DateTime.FromOADate(0);
					if (DateTime.Parse((DateTime.TryParse(VaCampos[1, 0], out TempDate11)) ? TempDate11.ToString("dd/MM/yyyy HH:mm:ss") : VaCampos[1, 0]) > DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate12)) ? TempDate12.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin))
					{
						// si ha vuelto despu�s del periodo de facturaci�n
						// le facturo hasta fin de mes
						System.DateTime TempDate13 = DateTime.FromOADate(0);
						VaCampos[1, 0] = (DateTime.TryParse(vstFechaFin, out TempDate13)) ? TempDate13.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin;
					}
				}
			}
			else
			{
				// si no es la facturaci�n mensual
				if (Conversion.Val(VaCampos[2, 0]) == viUle || sTipoFacturacion == "X")
				{
					result = true;
					System.DateTime TempDate15 = DateTime.FromOADate(0);
					System.DateTime TempDate14 = DateTime.FromOADate(0);
					sPrimeroMes = "01/" + DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate14)) ? TempDate14.ToString("dd/MM/yyyy HH:mm:ss") : vstFaltplan).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate15)) ? TempDate15.ToString("dd/MM/yyyy HH:mm:ss") : vstFaltplan).Year.ToString() + " 00:00:00";
					System.DateTime TempDate17 = DateTime.FromOADate(0);
					System.DateTime TempDate16 = DateTime.FromOADate(0);
					if (DateTime.Parse((DateTime.TryParse(VaCampos[1, 0], out TempDate16)) ? TempDate16.ToString("dd/MM/yyyy HH:mm:ss") : VaCampos[1, 0]) < DateTime.Parse((DateTime.TryParse(sPrimeroMes, out TempDate17)) ? TempDate17.ToString("dd/MM/yyyy HH:mm:ss") : sPrimeroMes))
					{
						result = false;
						// los apuntes de meses anteriores van en la mensual
					}
					else
					{
						// s�lo le cobro lo que le queda del mes
						if (VaCampos[9, 0] == "")
						{

							if (!bPrimerApunteDmovfact)
							{
								// cambio la fecha de inicio cuando es el primer apunte y ha ingresado en otro mes
								System.DateTime TempDate19 = DateTime.FromOADate(0);
								System.DateTime TempDate18 = DateTime.FromOADate(0);
								if (DateTime.Parse((DateTime.TryParse(vstFechaLLegada, out TempDate18)) ? TempDate18.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaLLegada) < DateTime.Parse((DateTime.TryParse(sPrimeroMes, out TempDate19)) ? TempDate19.ToString("dd/MM/yyyy HH:mm:ss") : sPrimeroMes))
								{
									VaCampos[0, 0] = sPrimeroMes;
								}
							}
						}
						else
						{
							// si es una ausencia
							System.DateTime TempDate25 = DateTime.FromOADate(0);
							System.DateTime TempDate24 = DateTime.FromOADate(0);
							System.DateTime TempDate23 = DateTime.FromOADate(0);
							System.DateTime TempDate22 = DateTime.FromOADate(0);
							System.DateTime TempDate21 = DateTime.FromOADate(0);
							System.DateTime TempDate20 = DateTime.FromOADate(0);
							if (DateTime.Parse((DateTime.TryParse(VaCampos[0, 0], out TempDate20)) ? TempDate20.ToString("dd/MM/yyyy HH:mm:ss") : VaCampos[0, 0]).Year < DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate21)) ? TempDate21.ToString("dd/MM/yyyy HH:mm:ss") : vstFaltplan).Year || (DateTime.Parse((DateTime.TryParse(VaCampos[0, 0], out TempDate22)) ? TempDate22.ToString("dd/MM/yyyy HH:mm:ss") : VaCampos[0, 0]).Year == DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate23)) ? TempDate23.ToString("dd/MM/yyyy HH:mm:ss") : vstFaltplan).Year && DateTime.Parse((DateTime.TryParse(VaCampos[0, 0], out TempDate24)) ? TempDate24.ToString("dd/MM/yyyy HH:mm:ss") : VaCampos[0, 0]).Month < DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate25)) ? TempDate25.ToString("dd/MM/yyyy HH:mm:ss") : vstFaltplan).Month))
							{

								// si el inicio de ausencia es del periodo anterior
								// grabo el periodo pendiente en dmovfact
								System.DateTime TempDate27 = DateTime.FromOADate(0);
								System.DateTime TempDate26 = DateTime.FromOADate(0);
								VaCampos[0, 0] = "01/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate26)) ? TempDate26.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate27)) ? TempDate27.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Year.ToString() + " 00:00:00";
							}
							System.DateTime TempDate29 = DateTime.FromOADate(0);
							System.DateTime TempDate28 = DateTime.FromOADate(0);
							if (DateTime.Parse((DateTime.TryParse(VaCampos[1, 0], out TempDate28)) ? TempDate28.ToString("dd/MM/yyyy HH:mm:ss") : VaCampos[1, 0]) != DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate29)) ? TempDate29.ToString("dd/MM/yyyy") : vstFaltplan))
							{
								// si ha vuelto de la ausencia
								System.DateTime TempDate31 = DateTime.FromOADate(0);
								System.DateTime TempDate30 = DateTime.FromOADate(0);
								sPrimeroMes = "01/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate30)) ? TempDate30.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(vstFechaFin, out TempDate31)) ? TempDate31.ToString("dd/MM/yyyy HH:mm:ss") : vstFechaFin).Year.ToString() + " 00:00:00";
								System.DateTime TempDate33 = DateTime.FromOADate(0);
								System.DateTime TempDate32 = DateTime.FromOADate(0);
								if (DateTime.Parse((DateTime.TryParse(VaCampos[1, 0], out TempDate32)) ? TempDate32.ToString("dd/MM/yyyy HH:mm:ss") : VaCampos[1, 0]) < DateTime.Parse((DateTime.TryParse(sPrimeroMes, out TempDate33)) ? TempDate33.ToString("dd/MM/yyyy HH:mm:ss") : sPrimeroMes))
								{
									// si fin de ausencia < primero de mes de alta
									// no grabo nada
									result = false;
								}
							}
						}
					} // fin de que no se factura por ser de periodo anterior
				}
				else
				{
					if (sTipoFacturacion == "A")
					{
						result = true;
					}
					else
					{
						// el tipo de facturaci�n es mensual
						result = false;
					}
				}
			}

			return result;
		}
		internal static bool fnCompararUnionArray()
		{
			// compara lo le�do en la Union con lo guardado en el array
			// para ver si el cambio afecta a DMOVFACT

			bool result = false;

			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["gservior"]))
			{
				if (VaCampos[2, 0] != Convert.ToString(vRrUnion.Tables[0].Rows[0]["gservior"]))
				{
					return true;
				}
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["gmedicor"]))
			{
				if (VaCampos[3, 0] != Convert.ToString(vRrUnion.Tables[0].Rows[0]["gmedicor"]))
				{
					return true;
				}
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["gtipocam"]))
			{
				if (VaCampos[4, 0] != Convert.ToString(vRrUnion.Tables[0].Rows[0]["gtipocam"]))
				{
					return true;
				}
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["iregaloj"]))
			{
				if (VaCampos[5, 0] != Convert.ToString(vRrUnion.Tables[0].Rows[0]["iregaloj"]))
				{
					return true;
				}
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["gsocieda"]))
			{
				if (VaCampos[6, 0] != Convert.ToString(vRrUnion.Tables[0].Rows[0]["gsocieda"]))
				{
					return true;
				}
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["nafiliac"]))
			{
				if (VaCampos[7, 0] != Convert.ToString(vRrUnion.Tables[0].Rows[0]["nafiliac"]))
				{
					return true;
				}
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["ginspecc"]))
			{
				if (VaCampos[8, 0] != Convert.ToString(vRrUnion.Tables[0].Rows[0]["ginspecc"]))
				{
					return true;
				}
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["iregimen"]))
			{
				if (VaCampos[10, 0] != Convert.ToString(vRrUnion.Tables[0].Rows[0]["iregimen"]))
				{
					result = true;
				}
			}

			return result;
		}
		internal static void Rellenar_Array()
		{

			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["gservior"]))
			{
				VaCampos[2, 0] = Convert.ToString(vRrUnion.Tables[0].Rows[0]["gservior"]);
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["gmedicor"]))
			{
				VaCampos[3, 0] = Convert.ToString(vRrUnion.Tables[0].Rows[0]["gmedicor"]);
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["gtipocam"]))
			{
				VaCampos[4, 0] = Convert.ToString(vRrUnion.Tables[0].Rows[0]["gtipocam"]);
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["iregaloj"]))
			{
				VaCampos[5, 0] = Convert.ToString(vRrUnion.Tables[0].Rows[0]["iregaloj"]);
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["gsocieda"]))
			{
				VaCampos[6, 0] = Convert.ToString(vRrUnion.Tables[0].Rows[0]["gsocieda"]);
				// arrastro los campos de la sociedad para que  no deje
				// la p�liza de la sociedad anterior
				if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["nafiliac"]))
				{
					VaCampos[7, 0] = Convert.ToString(vRrUnion.Tables[0].Rows[0]["nafiliac"]);
				}
				else
				{
					VaCampos[7, 0] = "";
				}
				if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["ginspecc"]))
				{
					VaCampos[8, 0] = Convert.ToString(vRrUnion.Tables[0].Rows[0]["ginspecc"]);
				}
				else
				{
					VaCampos[8, 0] = "";
				}
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["gmotause"]))
			{
				VaCampos[9, 0] = Convert.ToString(vRrUnion.Tables[0].Rows[0]["gmotause"]);
			}
			if (!Convert.IsDBNull(vRrUnion.Tables[0].Rows[0]["iregimen"]))
			{
				VaCampos[10, 0] = Convert.ToString(vRrUnion.Tables[0].Rows[0]["iregimen"]);
			}

		}

		internal static void Sociedad_SS()
		{
			//saco el identificador de la Seguridad Social
			string tStSS = "select nnumeri1 from sconsglo where gconsglo = 'SEGURSOC'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tStSS, vRcConex);
			DataSet tRrSS = new DataSet();
			tempAdapter.Fill(tRrSS);
			viSeguS = Convert.ToInt32(tRrSS.Tables[0].Rows[0]["nnumeri1"]);
			tStSS = "select nnumeri1 from sconsglo where gconsglo = 'SERVIULE'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tStSS, vRcConex);
			tRrSS = new DataSet();
			tempAdapter_2.Fill(tRrSS);
			viUle = Convert.ToInt32(tRrSS.Tables[0].Rows[0]["nnumeri1"]);
			tRrSS.Close();

		}
	}
}