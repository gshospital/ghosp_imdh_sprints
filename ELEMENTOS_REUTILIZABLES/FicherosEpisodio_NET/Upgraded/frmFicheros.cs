using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace FicherosEpisodio
{
    public partial class frmFicheros : Telerik.WinControls.UI.RadForm
	{


		private int iFilaArchivos = 0;
		private bool bHayCambios = false;
		private bool bHayInicial = false;
		public frmFicheros()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

        
		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			StringBuilder stTemp = new StringBuilder();
			string stSql = String.Empty;
			int iNumDocu = 0;
			bool bRecuperado = false;
			DataSet RrDatos = null;

			// Si no hay cambios, simplemente salimos

            // Iniciamos la transacci�n
            SqlTransaction transaction = basFicherosEpisodio.rcConexion.BeginTrans();
            try
            {
                if (bHayCambios)
                {

                    // Si ya hab�a algo y ha cambiado, confirmamos la acci�n

                    if (bHayInicial)
                    {

                        if (RadMessageBox.Show("�Desea confirmar los cambios realizados (se borrar�n los datos iniciales)?", "Ficheros Episodio", MessageBoxButtons.YesNo, RadMessageIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                        {
                            return;
                        }

                    }

                    // Primero tenemos que eliminar lo que hubieran desmarcado
                    sprFicheros.Col = basFicherosEpisodio.COL_INUMDOCU;

                    if (sprFicheros.MaxRows == 0)
                    {

                        stSql = "DELETE FROM DDOCUPDF WHERE " +
                                "gnombdoc = '" + basFicherosEpisodio.stNombreDocumento + "'";

                        SqlCommand tempCommand = new SqlCommand(stSql, basFicherosEpisodio.rcConexion);
                        tempCommand.ExecuteNonQuery();

                    }
                    else
                    {
                        int tempForVar = sprFicheros.MaxRows;
                        for (int I = 1; I <= tempForVar; I++)
                        {
                            sprFicheros.Row = I;
                            if (sprFicheros.Text.Trim() != "")
                            {
                                stTemp.Append(((stTemp.ToString().Trim() == "") ? "" : ", ") + sprFicheros.Text);
                            }

                        }

                        if (stTemp.ToString().Trim() != "")
                        {
                            stSql = "DELETE FROM DDOCUPDF WHERE " +
                                    "gnombdoc = '" + basFicherosEpisodio.stNombreDocumento + "' AND " +
                                    "inumdocu NOT IN (" + stTemp.ToString() + ")";

                            SqlCommand tempCommand_2 = new SqlCommand(stSql, basFicherosEpisodio.rcConexion, transaction);
                            tempCommand_2.ExecuteNonQuery();
                        }
                    }
                    // Ahora tomamos el iNumDocu m�s alto para guardar el resto a partir de �se
                    stSql = "SELECT isNull(MAX(inumdocu), 0) FROM DDOCUPDF WHERE gnombdoc = '" + basFicherosEpisodio.stNombreDocumento + "'";
                    var command = new SqlCommand(stSql, basFicherosEpisodio.rcConexion, transaction);
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
                    RrDatos = new DataSet();
                    tempAdapter.Fill(RrDatos);

                    if (RrDatos.Tables[0].Rows.Count == 0)
                    {
                        iNumDocu = 1;
                    }
                    else
                    {                        
                        iNumDocu = Convert.ToInt32(Convert.ToDouble(RrDatos.Tables[0].Rows[0][0]) + 1);
                    }

                    RrDatos.Close();

                    // Ahora grabamos los que no estaban

                    int tempForVar2 = sprFicheros.MaxRows;
                    for (int I = 1; I <= tempForVar2; I++)
                    {

                        sprFicheros.Col = basFicherosEpisodio.COL_RECUPERADO;
                        bRecuperado = sprFicheros.Text.Trim().ToUpper() == "S";

                        sprFicheros.Col = basFicherosEpisodio.COL_PATH;
                        sprFicheros.Row = I;

                        if (sprFicheros.Text.Trim() != "" && !bRecuperado)
                        {

                            string tempRefParam = sprFicheros.Text;
                            if (!basFicherosEpisodio.GrabaDatosArchivo(tempRefParam, iNumDocu, transaction))
                            {
                                basFicherosEpisodio.rcConexion.RollbackTrans(transaction);
                                return;
                            }
                            else
                            {
                                iNumDocu++;
                            }
                        }
                    }
                }

                basFicherosEpisodio.rcConexion.CommitTrans(transaction);
                bHayCambios = false;
                this.Close();
            }
            catch (SqlException ex)
            {
                basFicherosEpisodio.rcConexion.RollbackTrans(transaction);
            }finally
            {
                transaction.Dispose();
            }
		}

		private void cbA�adir_Click(Object eventSender, EventArgs eventArgs)
		{
			string stRutaFichero = String.Empty;
			string stFichero = String.Empty;			
			DialogoFicherosOpen.Filter = "Documentos PDF (*.pdf)|*.pdf";			
			DialogoFicherosOpen.FileName = "";
			DialogoFicherosOpen.ShowDialog();

			if (DialogoFicherosOpen.FileName != "")
			{
				stRutaFichero = DialogoFicherosOpen.FileName;
				// Comprobamos si el fichero ya est� incluido
				sprFicheros.Col = basFicherosEpisodio.COL_PATH;

				int tempForVar = sprFicheros.MaxRows;
				for (int I = 1; I <= tempForVar; I++)
				{
					sprFicheros.Row = I;
					if (sprFicheros.Text == stRutaFichero)
					{
						return;
					}
				}

				sprFicheros.MaxRows++;
				sprFicheros.Row = sprFicheros.MaxRows;
				sprFicheros.Col = basFicherosEpisodio.COL_NOMBRE;
				sprFicheros.Text = basFicherosEpisodio.extraeFichero(ref stRutaFichero);
				sprFicheros.Col = basFicherosEpisodio.COL_PATH;
				sprFicheros.Text = stRutaFichero;
                //CAMAYA SPRINT_X_2
				//sprFicheros.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly;
				iFilaArchivos = -1;
				bHayCambios = true;
			}

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbEliminar_Click(Object eventSender, EventArgs eventArgs)
		{

			string tmpValor = String.Empty;
                        
            if (sprFicheros.ActiveRowIndex <= 0 || sprFicheros.MaxRows <= 0)
            {
                return;
            }
			// Simulamos el click

            //CAMAYA SPRINT_X_2
            //sprFicheros_CellClick(sprFicheros, new EventArgs());

			// Vamos cogiendo los valores de las siguientes hasta que no queden m�s y luego quitamos la �ltima
			int FilaInicial = sprFicheros.ActiveRowIndex;

			for (int I = FilaInicial; I <= sprFicheros.MaxRows - 1; I++)
			{
				int tempForVar = sprFicheros.MaxCols;
				for (int J = 1; J <= tempForVar; J++)
				{
					sprFicheros.Col = J;
					sprFicheros.Row = I + 1;
					tmpValor = sprFicheros.Text;
					sprFicheros.Row = I;
					sprFicheros.Text = tmpValor;
				}
			}

			sprFicheros.MaxRows--;

			cbEliminar.Enabled = sprFicheros.MaxRows > 0;

			if (sprFicheros.MaxRows > 0)
			{
                //CAMAYA SPRINT_X_2
				//sprFicheros_CellClick(sprFicheros, new EventArgs());
			}

			bHayCambios = true;

		}
		
		private void frmFicheros_Load(Object eventSender, EventArgs eventArgs)
		{            
			Serrores.proBorraPDFs(Path.GetDirectoryName(Application.ExecutablePath));

			proInicializaTabla();            
            
			proCargaFicherosEpisodio();

			bHayCambios = false;

			// Si es modo lectura hay que ocultar elementos
			//UPGRADE_ISSUE: (2070) Constant App was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
			//UPGRADE_ISSUE: (2064) App property App.HelpFile was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //CAMAYA SPRINT_X_2
			//UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\AYUDA_DLLS.HLP");


			if (basFicherosEpisodio.bModoConsulta)
			{
				cbA�adir.Visible = false;
				cbEliminar.Visible = false;
				cbAceptar.Visible = false;
				cbCancelar.Text = "Cerrar";
			}
			else
			{
				cbA�adir.Visible = true;
				cbEliminar.Visible = true;
				cbAceptar.Visible = true;
				cbCancelar.Text = "Cancelar";
			}
		}

		private void proCargaFicherosEpisodio()
		{
			string stSql = String.Empty;
			DataSet RrDatos = null;

			try
			{
				basFicherosEpisodio.stNombreDocumento = basFicherosEpisodio.stItiposer + basFicherosEpisodio.iGanoregi.ToString() + StringsHelper.Format(basFicherosEpisodio.lGnumregi, "0000000000") + "RX" + 
				                                        ((basFicherosEpisodio.bNoRealizados) ? "NO" : "");

				stSql = "SELECT inumdocu, dnombpdf FROM DDOCUPDF WHERE " + 
				        "gnombdoc = '" + basFicherosEpisodio.stNombreDocumento + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, basFicherosEpisodio.rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				bHayInicial = RrDatos.Tables[0].Rows.Count != 0;

				sprFicheros.MaxRows = 0;

				foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
				{

					// Incrementamos una fila
					sprFicheros.MaxRows++;
					sprFicheros.Row = sprFicheros.MaxRows;

					// Completamos cada columna
					sprFicheros.Col = basFicherosEpisodio.COL_NOMBRE;
					sprFicheros.Text = Convert.ToString(iteration_row["dnombpdf"]);

					sprFicheros.Col = basFicherosEpisodio.COL_PATH;
					sprFicheros.Text = "";

					sprFicheros.Col = basFicherosEpisodio.COL_INUMDOCU;
					sprFicheros.Text = Convert.ToString(iteration_row["inumdocu"]);

				}
				
				RrDatos.Close();
			}
			catch
			{
			}
		}

		private void proInicializaTabla()
		{
			sprFicheros.MaxRows = 0;
			sprFicheros.MaxCols = 4;

			sprFicheros.Row = 0;

			sprFicheros.Col = basFicherosEpisodio.COL_NOMBRE;
			sprFicheros.Text = "Archivos";
			sprFicheros.SetColHidden(sprFicheros.Col, false);

			sprFicheros.Col = basFicherosEpisodio.COL_PATH;
			sprFicheros.Text = "Path";
			sprFicheros.SetColHidden(sprFicheros.Col, true);

			sprFicheros.Col = basFicherosEpisodio.COL_INUMDOCU;
			sprFicheros.Text = "inumdocu";
			sprFicheros.SetColHidden(sprFicheros.Col, true);

			sprFicheros.Col = basFicherosEpisodio.COL_RECUPERADO;
			sprFicheros.Text = "Recuperado";
			sprFicheros.SetColHidden(sprFicheros.Col, true);

		}

		private void frmFicheros_Closed(Object eventSender, CancelEventArgs eventArgs)
		{
			if (bHayCambios)
			{
				if (RadMessageBox.Show("Realiz� algunos cambios. �Desea salir sin guardar?", "Ficheros episodio", MessageBoxButtons.YesNo, RadMessageIcon.Question) != System.Windows.Forms.DialogResult.Yes)
				{
					eventArgs.Cancel = true;
					return;
				}
			}
			basFicherosEpisodio.bHayDatos = (sprFicheros.MaxRows > 0);
		}

        private void sprFicheros_CellClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            //CAMAYA SPRINT_X_2
            //if (eventArgs.Button == MouseButtons.Left)
            ///{
                int Col = eventArgs.ColumnIndex+1;
                int Row = eventArgs.RowIndex+1;
                //CAMAYA SPRINT_X_2
                //if (Row == 0 || sprFicheros.ActiveRowIndex == 0 || Col == 0 || sprFicheros.ActiveColumnIndex == 0)
                if (Row == 0 || sprFicheros.ActiveRowIndex == 0 || Col == 0)
                {
                    return;
                }

                if (sprFicheros.ActiveRowIndex == iFilaArchivos)
                {
                    // Quitamos la selecci�n
                    iFilaArchivos = -1;
                    //CAMAYA SPRINT_X_2
                    //sprFicheros.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly;
                    sprFicheros.Row = -1;
                    cbEliminar.Enabled = false;
                }
                else
                {
                    // Ponemos la selecci�n
                    iFilaArchivos = Row;
                    //CAMAYA SPRINT_X_2
                    //UPGRADE_WARNING: (6021) Casting 'int' to Enum may cause different behaviour. More Information: http://www.vbtonet.com/ewis/ewi6021.aspx
                    //sprFicheros.ActiveSheet.OperationMode = (FarPoint.Win.Spread.OperationMode)(((int)FarPoint.Win.Spread.OperationMode.ReadOnly) + ((int)FarPoint.Win.Spread.OperationMode.RowMode));
                    sprFicheros.Row = Row;
                    cbEliminar.Enabled = true;
                }

            //}
        }

        private void sprFicheros_CellDoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;

            string stSql = String.Empty;
            DataSet RrDatos = null;
            string stRutaPDF = String.Empty;

            if (Row == 0 || Col == 0)
            {
                return;
            }

            sprFicheros.Row = Row;
            sprFicheros.Col = basFicherosEpisodio.COL_PATH;

            if (sprFicheros.Text.Trim() == "")
            {
                // Es uno de los de la BD. Lo traemos

                sprFicheros.Col = basFicherosEpisodio.COL_INUMDOCU;

                stRutaPDF = Path.GetDirectoryName(Application.ExecutablePath) + "\\TMP_" + Serrores.NumeroAleatorio().ToString() + ".pdf";

                stSql = "SELECT odocupdf FROM DDOCUPDF WHERE " +
                        "gnombdoc = '" + basFicherosEpisodio.stNombreDocumento + "' AND " +
                        "inumdocu = " + sprFicheros.Text;

                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, basFicherosEpisodio.rcConexion);
                RrDatos = new DataSet();
                tempAdapter.Fill(RrDatos);

                if (RrDatos.Tables[0].Rows.Count == 0)
                {
                    RadMessageBox.Show("No se pudo recuperar el archivo de la Base de Datos", "Ficheros Episodio", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
                else
                {
                    this.Cursor = Cursors.WaitCursor;
                    lbRecuperando.Visible = true;
                    Application.DoEvents();

                    //UPGRADE_ISSUE: (2068) rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
                    //CAMAYA SPRINT_X_2
                    if (Serrores.RecuperarFicheroArchivo_b(RrDatos.Tables[0].Rows[0]["odocupdf"], stRutaPDF))
                    {
                        sprFicheros.Col = basFicherosEpisodio.COL_PATH;
                        sprFicheros.Text = stRutaPDF;

                        sprFicheros.Col = basFicherosEpisodio.COL_RECUPERADO;
                        sprFicheros.Text = "S";

                        string tempRefParam = "open";
                        string tempRefParam2 = "";
                        string tempRefParam3 = "";
                        //CAMAYA SPRINT_X_2
                        //FicheroEpisodiosSupport.PInvoke.SafeNative.shell32.ShellExecute(this.Handle.ToInt32(), ref tempRefParam, ref stRutaPDF, ref tempRefParam2, ref tempRefParam3, 1);

                    }

                    lbRecuperando.Visible = false;
                    this.Cursor = Cursors.Default;
                }                
            }
            else
            {
                // Abrimos el fichero
                string tempRefParam4 = "open";
                string tempRefParam5 = sprFicheros.Text;
                string tempRefParam6 = "";
                string tempRefParam7 = "";
                //CAMAYA SPRINT_X_2
                //FicheroEpisodiosSupport.PInvoke.SafeNative.shell32.ShellExecute(this.Handle.ToInt32(), ref tempRefParam4, ref tempRefParam5, ref tempRefParam6, ref tempRefParam7, 1);

            }

        }
	}
}