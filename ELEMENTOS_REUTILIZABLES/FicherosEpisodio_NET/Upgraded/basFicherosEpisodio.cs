using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace FicherosEpisodio
{
	internal static class basFicherosEpisodio
	{
		public const int COL_NOMBRE = 1;
		public const int COL_PATH = 2;
		public const int COL_INUMDOCU = 3;
		public const int COL_RECUPERADO = 4;

		public static SqlConnection rcConexion = null;
		public static object FormOrigen = null;
		public static bool bModoConsulta = false;
		public static bool bNoRealizados = false;
		public static string stItiposer = String.Empty;
		public static int iGanoregi = 0;
		public static int lGnumregi = 0;
		public static string gUsuario = String.Empty;
		public static string stNombreDocumento = String.Empty;

		public static bool bHayDatos = false;

		internal static string extraeFichero(ref string stRuta)
		{
			try
			{
				int pos = 0;
				pos = 1;

				while ((Strings.InStr(pos + 1, stRuta, "\\", CompareMethod.Binary) > 0))
				{
					pos = Strings.InStr(pos + 1, stRuta, "\\", CompareMethod.Binary);
				}                
				return stRuta.Substring(pos);
			}
			catch
			{
				return "";
			}
		}

		internal static bool GrabaDatosArchivo(string stPath, int iNumero, SqlTransaction transaction)
		{
			bool result = false;
			string stSql = String.Empty;
			DataSet RrDatos = null;
			string stNombre = String.Empty;
			byte[] Binario = null;

            if (transaction == null)
            {
                transaction = rcConexion.BeginTrans();
            }

			try
			{
				Binario = (byte[]) fSacaBinario(stPath);
				stNombre = extraeFichero(ref stPath);

				stSql = "SELECT * FROM DDOCUPDF WHERE 1 = 2";
                var command = new SqlCommand(stSql, rcConexion, transaction);
                SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				RrDatos.AddNew();
				
				RrDatos.Tables[0].Rows[0]["gnombdoc"] = stNombreDocumento;				
				RrDatos.Tables[0].Rows[0]["iNumDocu"] = iNumero;				
				RrDatos.Tables[0].Rows[0]["ddescrip"] = stNombre.Trim();				
				RrDatos.Tables[0].Rows[0]["fechalta"] = DateTime.Now;				
				RrDatos.Tables[0].Rows[0]["odocupdf"] = Binario;				
				RrDatos.Tables[0].Rows[0]["dnombpdf"] = stNombre.Trim();				
				RrDatos.Tables[0].Rows[0]["gUsuario"] = gUsuario;
				
                tempAdapter.Update(RrDatos, RrDatos.Tables[0].TableName);

				return true;
			}
            catch (SqlException ex)
			{
				result = false;                
				Serrores.AnalizaError("FicherosEpisodio", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "GrabaDatosArchivo", ex);

				return result;
			}
		}

		private static byte[] fSacaBinario(string strFichero)
		{
			bool error = false;

			byte[] result = null;
			int intFichero = 0;
			int lngLongitud = 0; //Variable a almacenar en Fichero

			try
			{
				error = true;

				intFichero = FileSystem.FreeFile();

				FileSystem.FileOpen(intFichero, strFichero, OpenMode.Binary, OpenAccess.Default, OpenShare.LockRead, -1);

				lngLongitud = (int) FileSystem.LOF(intFichero);
				byte[] bytFichero = new byte[lngLongitud + 2];
                //CAMAYA SPRINT_X_2
				//UPGRADE_WARNING: (2080) Get was upgraded to FileGet and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				Array TempArray = Array.CreateInstance(bytFichero.GetType().GetElementType(), bytFichero.Length);
				FileSystem.FileGet(intFichero, ref TempArray, -1, false, false);
				Array.Copy(TempArray, bytFichero, TempArray.Length);

				FileSystem.FileClose(intFichero);
				result = bytFichero;

				error = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!error)
				{
					throw excep;
				}

				if (error)
				{
					RadMessageBox.Show(excep.Message, "Error al descomponer el fichero " + strFichero, MessageBoxButtons.OK, RadMessageIcon.Info);
					return result;
				}
			}
			return new byte[1];
		}
	}
}