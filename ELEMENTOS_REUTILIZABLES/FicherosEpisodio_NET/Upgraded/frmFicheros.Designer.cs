using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace FicherosEpisodio
{
	partial class frmFicheros
	{

		#region "Upgrade Support "
		private static frmFicheros m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmFicheros DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmFicheros();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "DialogoFicherosOpen", "sprFicheros", "cbCancelar", "cbA�adir", "cbEliminar", "cbAceptar", "lbRecuperando", "frmGrupo", "sprFicheros_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public System.Windows.Forms.OpenFileDialog DialogoFicherosOpen;
		public UpgradeHelpers.Spread.FpSpread sprFicheros;		
        public Telerik.WinControls.UI.RadButton cbCancelar;
        public Telerik.WinControls.UI.RadButton cbA�adir;
        public Telerik.WinControls.UI.RadButton cbEliminar;
        public Telerik.WinControls.UI.RadButton cbAceptar;		
        public Telerik.WinControls.UI.RadLabel lbRecuperando;
        public Telerik.WinControls.UI.RadGroupBox frmGrupo;	
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFicheros));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.DialogoFicherosOpen = new System.Windows.Forms.OpenFileDialog();
            this.frmGrupo = new Telerik.WinControls.UI.RadGroupBox();            
			this.sprFicheros = new UpgradeHelpers.Spread.FpSpread();			
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbA�adir = new Telerik.WinControls.UI.RadButton();
            this.cbEliminar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();			
            this.lbRecuperando = new Telerik.WinControls.UI.RadLabel();
			this.frmGrupo.SuspendLayout();
			this.SuspendLayout();
			// 
			// DialogoFicherosOpen
			// 
			this.DialogoFicherosOpen.Title = "Seleccione fichero para incorporar";
			// 
			// frmGrupo
			// 			
			this.frmGrupo.Controls.Add(this.sprFicheros);
			this.frmGrupo.Controls.Add(this.cbCancelar);
			this.frmGrupo.Controls.Add(this.cbA�adir);
			this.frmGrupo.Controls.Add(this.cbEliminar);
			this.frmGrupo.Controls.Add(this.cbAceptar);
			this.frmGrupo.Controls.Add(this.lbRecuperando);
			this.frmGrupo.Enabled = true;			
			this.frmGrupo.Location = new System.Drawing.Point(4, 0);
			this.frmGrupo.Name = "frmGrupo";
			this.frmGrupo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmGrupo.Size = new System.Drawing.Size(509, 257);
			this.frmGrupo.TabIndex = 0;
			this.frmGrupo.Text = "Inclusi�n PDFs ";
			this.frmGrupo.Visible = true;
			// 
			// sprFicheros
			// 
			this.sprFicheros.Location = new System.Drawing.Point(8, 20);
			this.sprFicheros.Name = "sprFicheros";
			this.sprFicheros.Size = new System.Drawing.Size(417, 194);
			this.sprFicheros.TabIndex = 4;            
            this.sprFicheros.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprFicheros_CellClick);
            this.sprFicheros.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprFicheros_CellDoubleClick);			
			// 
			// cbCancelar
			// 			
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cbCancelar.Location = new System.Drawing.Point(432, 220);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(69, 29);
			this.cbCancelar.TabIndex = 5;
			this.cbCancelar.Text = "Cancelar";
			this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;			
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbA�adir
			// 			
			this.cbA�adir.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cbA�adir.Location = new System.Drawing.Point(432, 20);
			this.cbA�adir.Name = "cbA�adir";
			this.cbA�adir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbA�adir.Size = new System.Drawing.Size(69, 29);
			this.cbA�adir.TabIndex = 3;
			this.cbA�adir.Text = "A�adir";
			this.cbA�adir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;			
			this.cbA�adir.Click += new System.EventHandler(this.cbA�adir_Click);
			// 
			// cbEliminar
			// 			
			this.cbEliminar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbEliminar.Enabled = false;			
			this.cbEliminar.Location = new System.Drawing.Point(432, 56);
			this.cbEliminar.Name = "cbEliminar";
			this.cbEliminar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbEliminar.Size = new System.Drawing.Size(69, 29);
			this.cbEliminar.TabIndex = 2;
			this.cbEliminar.Text = "Eliminar";
			this.cbEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbEliminar.Click += new System.EventHandler(this.cbEliminar_Click);
			// 
			// cbAceptar
			// 			
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cbAceptar.Location = new System.Drawing.Point(356, 220);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(69, 29);
			this.cbAceptar.TabIndex = 1;
			this.cbAceptar.Text = "Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;			
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// lbRecuperando
			// 		
			this.lbRecuperando.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbRecuperando.Font = new System.Drawing.Font("Microsoft Sans Serif", 12f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);			
			this.lbRecuperando.Location = new System.Drawing.Point(108, 224);
			this.lbRecuperando.Name = "lbRecuperando";
			this.lbRecuperando.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbRecuperando.Size = new System.Drawing.Size(229, 21);
			this.lbRecuperando.TabIndex = 6;
			this.lbRecuperando.Text = "Recuperando archivo....";
			this.lbRecuperando.Visible = false;
			// 
			// frmFicheros
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;			
			this.ClientSize = new System.Drawing.Size(516, 258);
			this.Controls.Add(this.frmGrupo);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmFicheros";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Ficheros episodio";
			base.Closing += new System.ComponentModel.CancelEventHandler(this.frmFicheros_Closed);
			this.Load += new System.EventHandler(this.frmFicheros_Load);
			this.frmGrupo.ResumeLayout(false);
			this.ResumeLayout(false);
		}        
		#endregion
	}
}