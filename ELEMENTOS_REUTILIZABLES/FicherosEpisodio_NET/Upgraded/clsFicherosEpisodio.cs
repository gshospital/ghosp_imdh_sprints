using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace FicherosEpisodio
{
	public class clsFicherosEpisodio
	{
		public bool hayFicherosEpisodio(SqlConnection pConexion, string pEpisodio, bool pNoRealizados)
		{

			bool result = false;
			string stSql = String.Empty;
			DataSet RrDatos = null;

			try
			{

				basFicherosEpisodio.stItiposer = pEpisodio.Substring(0, Math.Min(1, pEpisodio.Length));
				basFicherosEpisodio.iGanoregi = Convert.ToInt32(Double.Parse(pEpisodio.Substring(1, Math.Min(4, pEpisodio.Length - 1))));
				basFicherosEpisodio.lGnumregi = Convert.ToInt32(Double.Parse(pEpisodio.Substring(5)));

				basFicherosEpisodio.stNombreDocumento = basFicherosEpisodio.stItiposer + basFicherosEpisodio.iGanoregi.ToString() + StringsHelper.Format(basFicherosEpisodio.lGnumregi, "0000000000") + "RX" + 
				                                        ((basFicherosEpisodio.bNoRealizados) ? "NO" : "");

				stSql = "SELECT COUNT(*) FROM DDOCUPDF WHERE " + 
				        "gnombdoc = '" + basFicherosEpisodio.stNombreDocumento + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, pConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				
				result = (Convert.ToDouble(RrDatos.Tables[0].Rows[0][0]) > 0);
				
				RrDatos.Close();

				return result;
			}
			catch
			{

				return false;
			}
		}

		public bool proFicherosEpisodio(SqlConnection pConexion, object pFormulario, string pEpisodio, string pUsuario, bool pModoConsulta, bool pNoRealizados)
		{
			try
			{
				basFicherosEpisodio.rcConexion = pConexion;
				basFicherosEpisodio.FormOrigen = pFormulario;
				basFicherosEpisodio.stItiposer = pEpisodio.Substring(0, Math.Min(1, pEpisodio.Length));
				basFicherosEpisodio.iGanoregi = Convert.ToInt32(Double.Parse(pEpisodio.Substring(1, Math.Min(4, pEpisodio.Length - 1))));
				basFicherosEpisodio.lGnumregi = Convert.ToInt32(Double.Parse(pEpisodio.Substring(5)));
				basFicherosEpisodio.gUsuario = pUsuario;
				basFicherosEpisodio.bModoConsulta = pModoConsulta;
				basFicherosEpisodio.bNoRealizados = pNoRealizados;

				frmFicheros tempLoadForm = frmFicheros.DefInstance;

				frmFicheros.DefInstance.ShowDialog();

				return basFicherosEpisodio.bHayDatos;
			}
			catch
			{
				return false;
			}
		}

		public void proBorraArchivos(SqlConnection pConexion, string pEpisodio, bool pNoRealizados)
		{
			try
			{
				basFicherosEpisodio.stItiposer = pEpisodio.Substring(0, Math.Min(1, pEpisodio.Length));
				basFicherosEpisodio.iGanoregi = Convert.ToInt32(Double.Parse(pEpisodio.Substring(1, Math.Min(4, pEpisodio.Length - 1))));
				basFicherosEpisodio.lGnumregi = Convert.ToInt32(Double.Parse(pEpisodio.Substring(5)));

				basFicherosEpisodio.stNombreDocumento = basFicherosEpisodio.stItiposer + basFicherosEpisodio.iGanoregi.ToString() + StringsHelper.Format(basFicherosEpisodio.lGnumregi, "0000000000") + "RX" + 
				                                        ((basFicherosEpisodio.bNoRealizados) ? "NO" : "");

				SqlCommand tempCommand = new SqlCommand("DELETE FROM DDOCUPDF WHERE gnombdoc = '" + basFicherosEpisodio.stNombreDocumento + "'", pConexion);
				tempCommand.ExecuteNonQuery();
			}
			catch
			{
			}
		}
	}
}