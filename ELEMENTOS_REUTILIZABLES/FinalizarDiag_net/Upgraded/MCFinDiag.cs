using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace FinalizarDiag
{
	public class MCFinDiag
	{


		//Aqu� no se realizan actualizaciones en la Base de Datos, pero se obtiene la informaci�n necesaria para saber
		//d�nde hay que actualizar posteriormente en Base de Datos. Y tambi�n se recoge la informaci�n de si el usuario
		//desea abortar el proceso. Todos estos datos se asignan a variables p�blicas de los formularios desde los que
		//se realiza la llamada.
		public void proFinalizarDiagnostico(dynamic nForm, SqlConnection nConnet, string stUsuario, string stCodPlan, string stCodDiag, string stTipoServ, int iA�oReg, int lNumReg, string stFecha, bool bRealizarPregunta = true)
		{

			string stListaCuidados = String.Empty;
			string stTexto = String.Empty;
			string stDesPlan = String.Empty;

            //UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\ayuda_dlls.hlp");
            CDAyuda.Instance.setHelpFile(nForm, CDAyuda.FICHERO_AYUDA_OTRASDLLS);


            mbFinDiag.gForm = nForm;
			mbFinDiag.gConexion = nConnet;
			mbFinDiag.gstUsuario = stUsuario.Trim();
			mbFinDiag.gstCodPlan = stCodPlan.Trim();
			mbFinDiag.gstCodDiag = stCodDiag.Trim();
			mbFinDiag.gstTipoServ = stTipoServ.Trim();
			mbFinDiag.gintA�oReg = iA�oReg;
			mbFinDiag.glNumReg = lNumReg;
			mbFinDiag.gstFecha = stFecha.Trim();

			if (mbFinDiag.fnCuidadosActivosAsociados(mbFinDiag.gstCodDiag))
			{
				//Al finalizar un diagnostico con cuidados asociados si la constante global IGESPLAN en el alfanum�rico 2
				//tiene valor "S" se tendr�n que finalizar los cuidados asociados al diagn�stico que vamos a finalizar con la
				//misma fecha y motivo seleccionado en la pantalla (representada por la variable gForm). Antes de finalizarlo
				//mostraremos en pantalla el mensaje: Al finalizar el diagn�stico se finalizar�n los cuidados (descripci�n de
				//los cuidados separados por comas) �Desea continuar?. Si responde "SI" se dar� de baja el diagnostico y se
				//actualizar� la tabla ECUIDADO con la misma fecha y motivo de finalizaci�n que la del diagn�stico. Si se
				//responde "NO" se dejar� todo como estaba
				if (Serrores.ObternerValor_CTEGLOBAL(mbFinDiag.gConexion, "IGESPLAN", "VALFANU2").ToUpper() == "S")
				{
					//If Not bRealizarPregunta Then
					//    gForm.bFinalizarCuidadosDiag = True  'para m�s adelante, actualizar en ECUIDADO los cuidados asociados al diagn�stico.
					//Else
					stListaCuidados = mbFinDiag.fnListaCuidados(mbFinDiag.gstCodDiag);
					if (stListaCuidados.Trim() != "")
					{
						stTexto = "Al finalizar el registro se finalizar�n los cuidados " + stListaCuidados + ". �Desea  continuar?";
						if (MessageBox.Show(stTexto, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
						{
							
							mbFinDiag.gForm.bContinuarFinalizacion = false;
							return;
						}
						else
						{
						
							mbFinDiag.gForm.bFinalizarCuidadosDiag = true; //para m�s adelante, actualizar en ECUIDADO los cuidados asociados al diagn�stico.
						}
					}
					//cuidado no se finalizan los cuidados que pertenecen a otro diagnostico
					if (mbFinDiag.gstCodPlan != "")
					{
						//Si el diagn�stico a finalizar pertenece a un plan y el paciente no tiene m�s diagn�sticos y cuidados
						//activos que el que vamos a finalizar mostraremos el mensaje: Al finalizar el diagn�stico se finalizar�
						//el plan (descripci�n del plan) �Desea continuar?. Si responde "SI" se dar� de baja el diagnostico y se
						//actualizar� la tabla EPLANPAC con la misma fecha y motivo de finalizaci�n que la del diagn�stico (estas
						//actualizaciones se realizar�n m�s adelante). Si se responde "NO" se dejar� todo como estaba.
						if (mbFinDiag.fnUltDiagActivoPlan(mbFinDiag.gstCodDiag, mbFinDiag.gstCodPlan))
						{
							if (!mbFinDiag.fnPlanConCuidadosActivos(mbFinDiag.gstCodPlan))
							{
								if (!bRealizarPregunta)
								{
									
									mbFinDiag.gForm.bUltimoDiagPlan = true; //para m�s adelante, actualizar la tabla EPLANPAC
								}
								else
								{
									stDesPlan = mbFinDiag.fnObtenerDesPlan(mbFinDiag.gstCodPlan);
									stTexto = "Al finalizar este registro se finalizar� el plan " + stDesPlan + ". �Desea  continuar?";
									if (MessageBox.Show(stTexto, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
									{
										
										mbFinDiag.gForm.bContinuarFinalizacion = false;
										return;
									}
									else
									{
									
										mbFinDiag.gForm.bUltimoDiagPlan = true; //para m�s adelante, actualizar la tabla EPLANPAC
									}
								}
							}
						}
					}
					//End If
					//End If
				}
				else
				{
					stTexto = "No se puede finalizar el registro, tiene cuidados asociados activos";
					MessageBox.Show(stTexto, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			
					mbFinDiag.gForm.bContinuarFinalizacion = false;
					return;
				}
			}
			else
			{
				if (mbFinDiag.gstCodPlan != "")
				{
					//Si el diagn�stico a finalizar pertenece a un plan y el paciente no tiene m�s diagn�sticos y cuidados
					//activos que el que vamos a finalizar mostraremos el mensaje: Al finalizar el diagn�stico se finalizar�
					//el plan (descripci�n del plan) �Desea continuar?. Si responde "SI" se dar� de baja el diagnostico y se
					//actualizar� la tabla EPLANPAC con la misma fecha y motivo de finalizaci�n que la del diagn�stico (estas
					//actualizaciones se realizar�n m�s adelante). Si se responde "NO" se dejar� todo como estaba.
					if (mbFinDiag.fnUltDiagActivoPlan(mbFinDiag.gstCodDiag, mbFinDiag.gstCodPlan))
					{
						if (!mbFinDiag.fnPlanConCuidadosActivos(mbFinDiag.gstCodPlan))
						{
							if (!bRealizarPregunta)
							{
								
								mbFinDiag.gForm.bUltimoDiagPlan = true; //para m�s adelante, actualizar la tabla EPLANPAC
							}
							else
							{
								stDesPlan = mbFinDiag.fnObtenerDesPlan(mbFinDiag.gstCodPlan);
								stTexto = "Al finalizar este registro se finalizar� el plan " + stDesPlan + ". �Desea  continuar?";
								if (MessageBox.Show(stTexto, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
								{
								
									mbFinDiag.gForm.bContinuarFinalizacion = false;
									return;
								}
								else
								{
								
									mbFinDiag.gForm.bUltimoDiagPlan = true; //para m�s adelante, actualizar la tabla EPLANPAC
								}
							}
						}
					}
				}
			}

		}

		//aqu� se actualizan los valores correspondientes en Base de Datos
		public void proActualizar(SqlConnection nConnet, string stUsuario, string stCodPlan, string stCodDiag, string stTipoServ, int intA�oReg, int lngNumReg, string stFecha, bool blnUltimoDiagPlan, bool blnFinalizarCuidadosDiag, int intMotivoFin, string stFechaFin)
		{

			DataSet RrPlan = null;
			string stPlan = String.Empty;
			StringBuilder stCuidados = new StringBuilder();


            CDAyuda.Instance.setHelpFile(mbFinDiag.gForm, CDAyuda.FICHERO_AYUDA_OTRASDLLS);


            SqlConnection Conexion = nConnet;
			Serrores.AjustarRelojCliente(Conexion);
			string sUsuario = stUsuario.Trim();
			string sCodPlan = stCodPlan.Trim();
			string sTipoServ = stTipoServ.Trim();
			int iA�oReg = intA�oReg;
			int lNumReg = lngNumReg;
			bool bUltDiagPlan = Convert.ToBoolean(mbFinDiag.gForm.bUltimoDiagPlan);
			bool bFinCuidDiag = blnFinalizarCuidadosDiag;
			int iMotFin = intMotivoFin;

			DateTime sFechaFin = new DateTime();

			if (bFinCuidDiag)
			{
				//si se tienen que finalizar los cuidados asociados al diagnostico, tenemos que finalizar unicamente aquellos que
				//pertenecen unicamente al diagnostico seleccionado
				for (int aik = 1; aik <= mbFinDiag.ArrayCuidados.GetUpperBound(0); aik++)
				{
					if (mbFinDiag.ArrayCuidados[aik].Estado == "S")
					{
						//ponemos fecha de borrado a los cuidados que pertenecen unicaqmnete al problema seleccionado

						//para los cuidados asociados al diagn�stico, se actualizar� en la tabla ECUIDADO con la misma fecha y motivo
						//de finalizaci�n que la del diagn�stico
						stCuidados = new StringBuilder("Select gmotifin,ffintrat, gusuario, finitrat From ecuidado Where itiposer='" + sTipoServ + "' ");
						stCuidados.Append("And ganoregi=" + iA�oReg.ToString() + " And gnumregi=" + lNumReg.ToString() + " ");
						stCuidados.Append(" And gcuidado = '" + mbFinDiag.ArrayCuidados[aik].Codigo + "' And (ffintrat is null or (ffintrat is not null and ffintrat >= getdate()))");


						SqlDataAdapter tempAdapter = new SqlDataAdapter(stCuidados.ToString(), Conexion);
						RrPlan = new DataSet();
						tempAdapter.Fill(RrPlan);

						foreach (DataRow iteration_row in RrPlan.Tables[0].Rows)
						{
							
							RrPlan.Edit();
							iteration_row["gmotifin"] = iMotFin;
                            iteration_row["ffintrat"] = sFechaFin;
                            DateTime sFincial = new DateTime() ;
                            iteration_row["ffintrat"] = sFincial;
                            //cuando el cuidado todav�a no se ha iniciado no puede ser que la fecha de fin sea menor que la de inicio

                            if (sFechaFin < sFincial)
							{
								iteration_row["finitrat"] = iteration_row["ffintrat"];
							}
							iteration_row["gusuario"] = sUsuario;
							string tempQuery = RrPlan.Tables[0].TableName;
							SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                            tempAdapter.Update(RrPlan, RrPlan.Tables[0].TableName);
                        }
						
						RrPlan.Close();
					}
					else
					{
						//si es "N" tendriamos que actualizar el problema asociado al cuidado
						//ya que el problema lo vamos a finalizar
						stCuidados = new StringBuilder("update ecuidado set gprobenf = '" + mbFinDiag.ArrayCuidados[aik].Problema + "' Where itiposer='" + sTipoServ + "' ");
						stCuidados.Append("And ganoregi=" + iA�oReg.ToString() + " And gnumregi=" + lNumReg.ToString() + " ");
						stCuidados.Append(" And gcuidado = '" + mbFinDiag.ArrayCuidados[aik].Codigo + "' And (ffintrat is null or (ffintrat is not null and ffintrat >= getdate())) ");

						SqlCommand tempCommand = new SqlCommand(stCuidados.ToString(), Conexion);
						tempCommand.ExecuteNonQuery();
					}
				}
			}
			//
			if (bUltDiagPlan)
			{
				//Si se trataba del �ltimo diagn�stico activo del plan, se finalizar� el plan (EPLANPAC),
				//actualizando la tabla EPLANPAC con la misma fecha y motivo de finalizaci�n que los del diagn�stico.
				stPlan = "Select gmotifin, ffinplan, gusuario From Eplanpac Where itiposer='" + sTipoServ + "' ";
				stPlan = stPlan + "And ganoregi=" + iA�oReg.ToString() + " And gnumregi=" + lNumReg.ToString() + " ";
				stPlan = stPlan + "And gplantip='" + sCodPlan + "' And ffinplan is null ";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stPlan, Conexion);
				RrPlan = new DataSet();
				tempAdapter_3.Fill(RrPlan);

				if (RrPlan.Tables[0].Rows.Count != 0)
				{
					
					RrPlan.Edit();
					RrPlan.Tables[0].Rows[0]["gmotifin"] = iMotFin;
					RrPlan.Tables[0].Rows[0]["ffinplan"] = (sFechaFin);
					RrPlan.Tables[0].Rows[0]["gusuario"] = sUsuario;
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);
					tempAdapter_3.Update(RrPlan, RrPlan.Tables[0].TableName);
				}

				
				RrPlan.Close();
			}

		}

		public MCFinDiag()
		{

			try
			{

				mbFinDiag.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{

				RadMessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}
		}


		~MCFinDiag()
		{
			mbFinDiag.clase_mensaje = null;
		}
	}
}