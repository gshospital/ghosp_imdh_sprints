using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace FinalizarDiag
{
	internal static class mbFinDiag
	{
		public static dynamic gForm = null;
		public static SqlConnection gConexion = null;
		public static Mensajes.ClassMensajes clase_mensaje = null;
		public static string gstUsuario = String.Empty;
		public static string gstCodPlan = String.Empty;
		public static string gstCodDiag = String.Empty;
		public static string gstTipoServ = String.Empty;
		public static int gintA�oReg = 0;
		public static int glNumReg = 0;
		public static string gstFecha = String.Empty;

		public struct Cuidados
		{
			public string Codigo;
			public string Descripcion;
			public string Estado;
			public string Problema;
			public static Cuidados CreateInstance()
			{
					Cuidados result = new Cuidados();
					result.Codigo = String.Empty;
					result.Descripcion = String.Empty;
					result.Estado = String.Empty;
					result.Problema = String.Empty;
					return result;
			}
		}

		public static mbFinDiag.Cuidados[] ArrayCuidados = null;


		//Esta funci�n devuelve TRUE si el diagn�stico que se pasa como argumento tiene cuidados activos asociados.
		internal static bool fnCuidadosActivosAsociados(string stProbEnf)
		{

			bool result = false;
			DataSet RrSQL1 = null;

			ArrayCuidados = new mbFinDiag.Cuidados[1];
			int iCuidados = 0;

			//comprobamos si el problema que vamos a finalizar tiene cuidados activos y estos se pueden terminar
			StringBuilder StSql = new StringBuilder();
			StSql.Append("Select ECODCUID.gcuidenf,ECODCUID.dcuidado From ECUIDADO ");
			StSql.Append(" inner join ECODCUID on ecuidado.gcuidado = ECODCUID.gcuidenf ");
			StSql.Append(" left join ECUIPROB on ecuidado.gcuidado = ECUIPROB.gcuidenf and ECUIPROB.gprobenf='" + stProbEnf + "' ");
			StSql.Append(" Where itiposer='" + gstTipoServ + "' And ganoregi= " + gintA�oReg.ToString() + " And gnumregi= " + glNumReg.ToString() + " ");
			StSql.Append(" And ECUIDADO.gprobenf='" + stProbEnf + "' And (ffintrat is null or (ffintrat is not null and ffintrat >= getdate()))");

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql.ToString(), gConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//se recuperan registros
				//por cada uno de los registros recuperados hay que ver si ese cuidado esta entre los cuidados
				//de otro problema asociado al paciente
				result = true;
				//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
				RrSql.MoveFirst();
				foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
				{
					StSql = new StringBuilder("Select EPROBENF.*  From EPROBENF ");
					StSql.Append(" inner join ECUIPROB on ECUIPROB.gprobenf = EPROBENF.gprobenf ");
					StSql.Append(" Where itiposer='" + gstTipoServ + "' And ganoregi= " + gintA�oReg.ToString() + " And gnumregi= " + glNumReg.ToString() + " ");
					StSql.Append(" And ECUIPROB.gprobenf <> '" + stProbEnf + "' and ECUIPROB.gcuidenf = '" + Convert.ToString(iteration_row["gcuidenf"]) + "'");
					StSql.Append(" And (ffinprob is null or (ffinprob is not null and ffinprob >= getdate()))");

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql.ToString(), gConexion);
					RrSQL1 = new DataSet();
					tempAdapter_2.Fill(RrSQL1);
					if (RrSQL1.Tables[0].Rows.Count != 0)
					{
						//si hay filas el cuidado no se podra finalizar pero si se podra finalizar el
						//problema
						iCuidados = ArrayCuidados.GetUpperBound(0) + 1;
						ArrayCuidados = ArraysHelper.RedimPreserve(ArrayCuidados, new int[]{ArrayCuidados.GetUpperBound(0) + 2});
						ArrayCuidados[iCuidados].Codigo = Convert.ToString(iteration_row["gcuidenf"]);
						ArrayCuidados[iCuidados].Descripcion = Convert.ToString(iteration_row["dcuidado"]);
						ArrayCuidados[iCuidados].Estado = "N";
						ArrayCuidados[iCuidados].Problema = Convert.ToString(RrSQL1.Tables[0].Rows[0]["gprobenf"]).Trim();
					}
					else
					{
						//si no hay ninguna fila el cuidado se podra finalizar
						iCuidados = ArrayCuidados.GetUpperBound(0) + 1;
						ArrayCuidados = ArraysHelper.RedimPreserve(ArrayCuidados, new int[]{ArrayCuidados.GetUpperBound(0) + 2});
						ArrayCuidados[iCuidados].Codigo = Convert.ToString(iteration_row["gcuidenf"]);
						ArrayCuidados[iCuidados].Descripcion = Convert.ToString(iteration_row["dcuidado"]);
						ArrayCuidados[iCuidados].Estado = "S";
						ArrayCuidados[iCuidados].Problema = "";
					}

					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSQL1.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrSQL1.Close();

				}

			}
			else
			{
				//no se recuperan registros
				result = false;
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();


			return result;
		}

		//Esta funci�n devuelve, separados por comas, los cuidados asociados al problema que se pasa como argumento.
		internal static string fnListaCuidados(string stProbEnf)
		{
			string result = String.Empty;

			//tenemos guardados los cuidados recuperados en un array
			for (int aik = 1; aik <= ArrayCuidados.GetUpperBound(0); aik++)
			{
				if (ArrayCuidados[aik].Estado == "S")
				{
					result = result + ArrayCuidados[aik].Descripcion.Trim() + ",";
				}
			}

			//se quita la �ltima coma
			if (result != "")
			{
				result = result.Substring(0, Math.Min(Strings.Len(result) - 1, result.Length));
			}


			return result;
		}

		//Esta funci�n devuelve TRUE si el plan que se pasa como argumento tiene cuidados activos para el paciente que corresponde.
		internal static bool fnPlanConCuidadosActivos(string sCodPlan)
		{


			bool result = false;
			string StSql = "Select itiposer From ecuidado Where itiposer='" + gstTipoServ + "' ";
			StSql = StSql + "And ganoregi=" + gintA�oReg.ToString() + " And gnumregi=" + glNumReg.ToString() + " ";
			StSql = StSql + "And gplantip='" + sCodPlan + "' And gprobenf is null and (ffintrat is null or (ffintrat is not null and ffintrat >= getdate()))";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, gConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{
				result = true;
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();

			return result;
		}

		//Esta funci�n devuelve TRUE si el diagn�stico que se pasa como argumento es el �ltimo diagn�stico
		//activo para el plan de cuidados que se pasa como argumento. Y todo ello, referido al paciente actual.
		internal static bool fnUltDiagActivoPlan(string sCodDiagnostico, string sCodPlan)
		{


			bool result = false;
			string StSql = "Select itiposer From Eprobenf Where itiposer='" + gstTipoServ + "' ";
			StSql = StSql + "And ganoregi=" + gintA�oReg.ToString() + " And gnumregi=" + glNumReg.ToString() + " ";
			StSql = StSql + "And gplantip='" + sCodPlan + "' And gprobenf<>'" + sCodDiagnostico + "' ";
			StSql = StSql + "And ffinprob is null ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, gConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count == 0)
			{ //si no existen m�s diagn�sticos activos del mismo plan
				result = true;
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();

			return result;
		}

		//Esta funci�n devuelve la descripci�n del plan cuyo c�digo se pasa como argumento
		internal static string fnObtenerDesPlan(string sCodPlan)
		{


			string result = String.Empty;
			string StSql = "Select dplantip From Eplantip Where gplantip='" + sCodPlan + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, gConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = Convert.ToString(RrSql.Tables[0].Rows[0]["dplantip"]).Trim();
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();
			return result;
		}
	}
}