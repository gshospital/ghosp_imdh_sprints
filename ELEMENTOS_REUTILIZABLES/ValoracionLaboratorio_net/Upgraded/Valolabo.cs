using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ValoLaboracion
{
	public class ValoLabo
	{


		public void CreaValoracionLaboratorio(SqlConnection CnConexion, string itiposer, int ganoregi, int gnumregi, int gservici, string gusuario, string texto, string fapunte, bool ErrorValo)
		{
			string sql = String.Empty;
			DataSet cursor = null;
			int CodigoConcepto = 0;
			int CodigoServicioValoracion = 0;
			string TipoValoracion = string.Empty;
			string TipodeHoja = string.Empty;
			string CodigoDelDocumento = string.Empty;
			string stUsuarioValoracion = string.Empty;
			string stSinComillas = string.Empty;

			bool errorvuelta = false;
			try
			{
				ErrorValo = errorvuelta;

				CodigoServicioValoracion = gservici;

				sql = "select * from sconsglo where gconsglo='VALOLABO'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, CnConexion);
				cursor = new DataSet();
				tempAdapter.Fill(cursor);
				if (cursor.Tables[0].Rows.Count != 0)
				{
					CodigoConcepto = Convert.ToInt32(cursor.Tables[0].Rows[0]["nnumeri1"]);
					TipoValoracion = Convert.ToString(cursor.Tables[0].Rows[0]["valfanu1"]);
					TipodeHoja = Convert.ToString(cursor.Tables[0].Rows[0]["valfanu2"]);
				}
				cursor.Close();

				//para recuperar el usuario responsable de laboratorio
				//OSCAR C FEB 2006
				//El usuario de laboratorio debe salir de la tabla DOPENSER
				//sql = "select * from sconsglo where gconsglo='PRESPROD'"
				sql = "SELECT gusuario FROM DOPENSER WHERE gservici = " + gservici.ToString();
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, CnConexion);
				cursor = new DataSet();
				tempAdapter_2.Fill(cursor);
				if (cursor.Tables[0].Rows.Count != 0)
				{
					//stUsuarioValoracion = IIf(IsNull(cursor("VALFANU2")), "", Trim(cursor("VALFANU2")))
					stUsuarioValoracion = (Convert.IsDBNull(cursor.Tables[0].Rows[0]["gusuario"])) ? "" : Convert.ToString(cursor.Tables[0].Rows[0]["gusuario"]).Trim();
				}
				cursor.Close();

				sql = "select * from dvaloram where 1=2";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, CnConexion);
				cursor = new DataSet();
				tempAdapter_3.Fill(cursor);
				cursor.AddNew();
				cursor.Tables[0].Rows[0]["itiposer"] = itiposer;
				cursor.Tables[0].Rows[0]["ganoregi"] = ganoregi;
				cursor.Tables[0].Rows[0]["gnumregi"] = gnumregi;
				cursor.Tables[0].Rows[0]["itipvalm"] = TipoValoracion;
				cursor.Tables[0].Rows[0]["ggruhoja"] = TipodeHoja;
				cursor.Tables[0].Rows[0]["gcodconc"] = CodigoConcepto;
				cursor.Tables[0].Rows[0]["gservalm"] = CodigoServicioValoracion;
				cursor.Tables[0].Rows[0]["gusuario"] = stUsuarioValoracion; //gusuario
				cursor.Tables[0].Rows[0]["nsecuval"] = 1;
				cursor.Tables[0].Rows[0]["fapuntes"] = DateTime.Parse(fapunte);
				CodigoDelDocumento = CreaCodigoDelDocumento(itiposer, ganoregi, gnumregi, DateTime.Parse(fapunte).ToString("ddMMyyyy") + DateTime.Parse(fapunte).ToString("HHmmss") + TipoValoracion.Trim() + TipodeHoja.Trim() + CodigoConcepto.ToString().Trim());
				cursor.Tables[0].Rows[0]["ovallcon"] = CodigoDelDocumento;
				cursor.Tables[0].Rows[0]["ibajconc"] = "N";
				string tempQuery = cursor.Tables[0].TableName;
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tempQuery, "");
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_4);
				tempAdapter_4.Update(cursor, cursor.Tables[0].TableName);
				cursor.Close();
				string tempRefParam = texto + "";
				stSinComillas = ReemplazarComillaSimple(ref tempRefParam);
				//sql = "insert into document values('" & CodigoDelDocumento & "' ,'" & texto & "','Arial',0,0,0,9.75,'" & gusuario & "')"
				sql = "insert into document (gnomdocu,contdocu,fontname,fontbold,Fontital,fontline,FontSize,gusuario) values('" + 
				      CodigoDelDocumento + "' ,'" + stSinComillas + "','Arial',0,0,0,9.75,'" + stUsuarioValoracion + "')";

				SqlCommand tempCommand = new SqlCommand(sql, CnConexion);
				tempCommand.ExecuteNonQuery();

				// hay que hacer un insert en DOCUMMOD. REQUERIMIENTO 1972
				sql = "insert into DOCUMMOD (fmodific,gnomdocu,contdocu, fontname, fontbold, Fontital ,fontline, FontSize,gusuario,DDOCWORD)  select getdate(),gnomdocu,contdocu, fontname, fontbold, Fontital ,fontline, FontSize,gusuario,DDOCWORD  from document where gnomdocu = '" + CodigoDelDocumento + "'";

				SqlCommand tempCommand_2 = new SqlCommand(sql, CnConexion);
				tempCommand_2.ExecuteNonQuery();


				//a continuacion insertamos en la tabla DVALOCAB que lleva la cabecera de la valoracion
				sql = "select * from DVALOCAB where 1=2";
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, CnConexion);
				cursor = new DataSet();
				tempAdapter_5.Fill(cursor);
				cursor.AddNew();
				cursor.Tables[0].Rows[0]["itiposer"] = itiposer;
				cursor.Tables[0].Rows[0]["ganoregi"] = ganoregi;
				cursor.Tables[0].Rows[0]["gnumregi"] = gnumregi;
				cursor.Tables[0].Rows[0]["fapuntes"] = DateTime.Parse(fapunte);
				cursor.Tables[0].Rows[0]["itipvalm"] = TipoValoracion;
				cursor.Tables[0].Rows[0]["ggruhoja"] = TipodeHoja;
				cursor.Tables[0].Rows[0]["gservalm"] = CodigoServicioValoracion;
				cursor.Tables[0].Rows[0]["gusuario"] = stUsuarioValoracion; //gusuario
				cursor.Tables[0].Rows[0]["fmecaniz"] = DateTime.Now;
				cursor.Tables[0].Rows[0]["oanotaci"] = DBNull.Value;
				string tempQuery_2 = cursor.Tables[0].TableName;
				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(tempQuery_2, "");
				SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_6);
				tempAdapter_6.Update(cursor, cursor.Tables[0].TableName);
				cursor.Close();
			}
			catch
			{
				errorvuelta = true;
				ErrorValo = errorvuelta;
			}



		}

		public string CreaCodigoDelDocumento(string itiposer, int ganoregi, int gnumregi, string fecha)
		{
			return itiposer.Trim() + Conversion.Str(ganoregi).Trim() + Conversion.Str(gnumregi).Trim() + fecha;
		}

		private string ReemplazarComillaSimple(ref string stEntrada)
		{


			int i = (stEntrada.IndexOf('\'') + 1);

			while (i != 0)
			{
				stEntrada = stEntrada.Substring(0, Math.Min(i - 1, stEntrada.Length)) + "�" + stEntrada.Substring(i);
				i = (stEntrada.IndexOf('\'') + 1);
			}

			return stEntrada;

		}
	}
}