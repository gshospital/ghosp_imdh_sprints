﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMDH_Servicios_Test
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            byte[] bytes = new byte[30];
            string msg;
            try
            {
                msg = textBox1.Text;
                msg = msg.PadRight(30);
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(msg);

                soc.Send(byData);

               int bytesRec = soc.Receive(bytes);

               MessageBox.Show("Respuesta: "+ Encoding.ASCII.GetString(bytes, 0, bytesRec));

                button5.PerformClick();

            }
            catch (Exception ex)
            {           

                MessageBox.Show(ex.Message);
            }
        

        }
        private Socket soc;

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                soc = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                System.Net.IPAddress ipAdd = System.Net.IPAddress.Parse("127.0.0.1");
                System.Net.IPEndPoint remoteEP = new IPEndPoint(ipAdd, 5000);
                soc.Connect(remoteEP);
                button2.Enabled = true;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            const int RESTART = 129;
            string[] args  = new string[]{"--port:","5000","--bytes:","30","--debug:","false"};

            TimeSpan timeout = TimeSpan.FromMilliseconds(2000);

            ServiceController service = new ServiceController("IMDH_Servicios",Environment.MachineName);
            while (service.Status != ServiceControllerStatus.Running)
            {
                service = new ServiceController("IMDH_Servicios", Environment.MachineName);
                if (service.Status != ServiceControllerStatus.Running && service.Status != ServiceControllerStatus.StartPending)

                    service.Start(args);
                
            }
            try
            {
                service.ExecuteCommand(RESTART);
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
         

        }

         private void button5_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            // Release the socket.
            soc.Shutdown(SocketShutdown.Both);
            soc.Close();
        }
    }
}
