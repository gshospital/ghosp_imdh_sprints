﻿using IMDH_Servicios;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IMDH_Conexion
{
    class Program
    {

        static uint _port = 5000;
        static uint _bytes = 30;
        static bool _debug = true;
        static EventLog _eventLog;
        // Controla la actividad de escucha
        static bool _parar = false;
        static string data;

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                _port = uint.Parse(args[1]);
                if (args.Length > 2)
                {
                    _bytes = uint.Parse(args[3]);
                    if (args.Length > 4)
                    {
                        _debug = bool.Parse(args[5]);
                    }
                }

            }
            if (!System.Diagnostics.EventLog.SourceExists("IMDH_Servicios"))
            {
                System.Diagnostics.EventLog.CreateEventSource("IMDH_Servicios", "");
            }

            _eventLog = new EventLog();

            _eventLog.Source = "IMDH_Servicios";
            _eventLog.Log = "";
            _eventLog.WriteEntry("IMDH_Conexion Start TEST");

            Console.WriteLine("IMDH_Conexion Start port:" + _port.ToString() + ", bytes:" + _bytes.ToString() + ", debug:" + _debug.ToString());

            Thread thread = new Thread(StartListenning);
            thread.Start();

        }

        public static void StartListenning()
        {
            try
            {
                IPAddress ipAddr = System.Net.IPAddress.Loopback;
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, (int)_port);

                Socket listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(ipEndPoint);
                listener.Listen(10);
                byte[] bytes = new byte[_bytes];

                #region Escucha  while (!_parar)
                while (!_parar)
                {
                    if (_debug)
                        Console.WriteLine("IMDH_Conexion Esperando conexión...");

                    Socket handler = listener.Accept();
                    data = null;
                    while (true)
                    {
                        bytes = new byte[_bytes];
                        int bytesRec = handler.Receive(bytes);
                        data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        //if (data.IndexOf("\0") > -1)
                        //{
                        //    break;
                        //}
                        if (data.Length >= _bytes)
                            break;

                    }
                    byte[] msg;
                    string respuesta = "";

                    IServicio servicio = Servicio.getServicio(data, _eventLog, _debug);
                    if (servicio != null)
                        respuesta = servicio.Procesar();
                    else
                        respuesta = "False";
                    if (_debug)
                        Console.WriteLine("Datos Recibidos: " + data);

                    respuesta = respuesta.PadRight((int)_bytes);

                    msg = Encoding.ASCII.GetBytes(respuesta);

                    handler.Send(msg);


                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                    handler.Dispose();

                }
                #endregion

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.Read();
            }
        }
    }
}
