﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IMDH_Servicios
{

    public class ServicioSetLocaleInfo : IServicio
    {
        [DllImport("kernel32.dll")]
        internal static extern bool SetLocaleInfo(uint Locale, uint LCType, string lpLCData);
        [DllImport("user32.dll")]
        internal static extern IntPtr SendMessage(IntPtr hWnd, uint wMsg,UIntPtr wParam, IntPtr lParam);

        const int HWND_BROADCAST = 0xFFFF;
        const uint WM_SETTINGCHANGE = 0x001A;

        // CONSTANTES UTILIZADAS PARA OBTENER VALORES DE LA CONFIGURACION
        // REGIONAL DEL ORDENADOR
        const uint LOCALE_SYSTEM_DEFAULT = 0x800;
        const uint LOCALE_SMONTHOUSANDSEP = 0x17; //  separador de miles en moneda
        const uint LOCALE_ICENTURY = 0x24; //  especificador de formato de siglo
        const uint LOCALE_SMONDECIMALSEP = 0x16; //  separador de decimales en moneda
        const uint LOCALE_STIMEFORMAT = 0x1003; //  cadena de formato de hora
        const uint LOCALE_SSHORTDATE = 0x1F; //  cadena de formato de fecha corta
        const uint LOCALE_SDATE = 0x1D; //  separador de fecha
        const uint LOCALE_STIME = 0x1E; //  separador de hora


        const string SEPARADOR_HORA = ":";
        const string SEPARADOR_FECHA = "/";
        const string FORMATO_FECHA = "dd/MM/yyyy";
        const string FORMATO_HORA = "HH:mm:ss";
        const string SEPARADOR_MILES = ".";
        const string SEPARADOR_DECIMAL = ",";
        const int MOSTRAR_SIGLO4CIFRAS = 1;

        EventLog _eventLog;
        bool _debug;
        public ServicioSetLocaleInfo(EventLog eventLog, bool debug)
        {
            _eventLog = eventLog;
            _debug = debug;
        }
        public string Procesar()
        {
            try
            {
                return ConfigurarEquipo().ToString();
            }
            catch (Exception)
            {
                return "False";
            }

        }
   

        public bool ConfigurarEquipo()
        {
            string tempRefParam = SEPARADOR_HORA;
            bool codError;
            string tempRefParam2 = SEPARADOR_FECHA;
            string tempRefParam3 = FORMATO_FECHA;
            string tempRefParam4 = FORMATO_HORA;
            try
            {
                codError = SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIME, tempRefParam);
                if (codError == false) return false;
                codError = SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDATE, tempRefParam2);
                if (codError == false) return false;
                codError = SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SSHORTDATE, tempRefParam3);
                if (codError == false) return false;
                codError = SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIMEFORMAT,tempRefParam4);
                if (codError == false) return false;

                SendMessage((IntPtr)HWND_BROADCAST, WM_SETTINGCHANGE, UIntPtr.Zero, IntPtr.Zero);

                return true;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
