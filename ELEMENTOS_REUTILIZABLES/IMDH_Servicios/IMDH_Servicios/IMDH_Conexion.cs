﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IMDH_Servicios
{
    public class IMDH_Conexion
    {
        Socket socket;
        string data = null;

        //Valores parámetros Inicio --port:5000 --bytes:30 --debug:false
        uint _port = 5000;
        uint _bytes = 30;
        bool _debug = false;
        // Controla la actividad de escucha
        bool _parar = false;

        EventLog _eventLog;

        Thread thread;

        public uint Port
        {
            get { return _port; }
            set { _port = value; }
        }
        public uint Bytes
        {
            get { return _bytes; }
            set { _bytes = value; }
        }
        public bool Debug
        {
            get { return _debug; }
            set { _debug = value; }
        }
        public IMDH_Conexion(string[] args, EventLog eventLog)
        {
            try
            {
                _eventLog = eventLog;
                if (args.Length > 0)
                {
                    _port = uint.Parse(args[1]);
                    if (args.Length > 2)
                    {
                        _bytes = uint.Parse(args[3]);
                        if (args.Length > 4)
                        {
                            _debug = bool.Parse(args[5]);
                        }
                    }

                }
            }
            catch
            {
                _eventLog.WriteEntry("IMDH_Conexion Error parámetros entrada");
            }
        }
        public void Start()
        {
            _eventLog.WriteEntry("IMDH_Conexion Start port:" + _port.ToString() + ", bytes:" + _bytes.ToString() + ", debug:" + _debug.ToString());

            thread = new Thread(StartListenning);
            thread.Start();

        }
        public void ReStart()
        {
            _parar = true;
            thread.Join();
            _eventLog.WriteEntry("IMDH_Conexion ReStart port:" + _port.ToString() + ", bytes:" + _bytes.ToString() + ", debug:" + _debug.ToString());
        }
        public void StartListenning()
        {
            try
            {
                IPAddress ipAddr = System.Net.IPAddress.Loopback;
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, (int)_port);

                Socket listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(ipEndPoint);
                listener.Listen(10);
                byte[] bytes = new byte[_bytes];

                #region Escucha  while (!_parar)
                while (!_parar)
                {
                    if (_debug)
                        _eventLog.WriteEntry("IMDH_Conexion Esperando conexión...");

                    Socket handler = listener.Accept();
                    data = null;
                    while (true)
                    {
                        bytes = new byte[_bytes];
                        int bytesRec = handler.Receive(bytes);
                        data += Encoding.ASCII.GetString(bytes, 0, bytesRec);

                        if (data.Length >= _bytes)
                            break;

                    }
                    byte[] msg;
                    string respuesta = "";

                    IServicio servicio = Servicio.getServicio(data, _eventLog, _debug);
                    if (servicio != null)
                        respuesta = servicio.Procesar();
                    else
                        respuesta = "False";
                    if (_debug)
                        _eventLog.WriteEntry("Datos Recibidos: " + data);

                    respuesta = respuesta.PadRight((int)_bytes);

                    msg = Encoding.ASCII.GetBytes(respuesta);

                    handler.Send(msg);


                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                    handler.Dispose();

                }
                #endregion

            }
            catch (Exception ex)
            {
                _eventLog.WriteEntry(ex.Message);

            }
        }

    }
}
