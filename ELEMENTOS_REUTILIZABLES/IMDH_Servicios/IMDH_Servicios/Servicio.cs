﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDH_Servicios
{
    public class Servicio
    {
        static public IServicio getServicio(string datos, EventLog eventLog, bool debug)
        {
            string tipo="";
            string parametros="";
            int posEspacio = datos.IndexOf(" ");
            if (posEspacio > 0 && datos.Length > 0)
            {
                tipo = datos.Substring(0, posEspacio).ToUpper();
                parametros = datos.Substring(posEspacio + 1);
            }
            else if (datos.Length > 0)
            {
                tipo = datos;
            }
            switch (tipo)
            {
                case "SETDATETIME":
                    return new ServicioSetDateTime(parametros, eventLog, debug);
                case "SETLOCALE":
                    return new ServicioSetLocaleInfo(eventLog, debug);
                default:
                    if (debug)
                        eventLog.WriteEntry("Error: " + tipo + " no implementado.");
                    return null;
            }
        }
    }
}
