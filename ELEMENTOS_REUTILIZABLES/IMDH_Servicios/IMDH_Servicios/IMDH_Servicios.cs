﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;


namespace IMDH_Servicios
{
    public partial class IMDH_Servicios : ServiceBase
    {
        //COMANDOS
        const int RESTART = 129;

        IMDH_Conexion conexion;
        public IMDH_Servicios()
        {
            InitializeComponent();
            if (!System.Diagnostics.EventLog.SourceExists("IMDH_Servicios"))
            {
                System.Diagnostics.EventLog.CreateEventSource("IMDH_Servicios", "");
            }

            eventLog1.Source = "IMDH_Servicios";
            eventLog1.Log = "";
        }

        protected override void OnStart(string[] args)
        {
            //Valores parámetros Inicio --port: 5000 --bytes: 30 --debug: false
            eventLog1.WriteEntry("IMDH_Servicios OnStart");
            conexion = new IMDH_Conexion(args, eventLog1);
            conexion.Start();
        }
        protected override void OnCustomCommand(int command)
        {
            switch (command)
            {

                case RESTART:
                    conexion.ReStart();
                    break;
            }
        }
        protected override void OnStop()
        {
            eventLog1.WriteEntry("IMDH_Servicios OnStop");
        }
  
    }

}
