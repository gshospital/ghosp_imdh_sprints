﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IMDH_Servicios
{
    public class ServicioSetDateTime : IServicio
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern bool SetLocalTime(ref SystemTime lpSystemTime);

        public struct SystemTime
        {
            public ushort Year;
            public ushort Month;
            public ushort DayOfWeek;
            public ushort Day;
            public ushort Hour;
            public ushort Minute;
            public ushort Second;
            public ushort Millisecond;
        };

        EventLog _eventLog;
        bool _debug;
        string _parametros;
        public ServicioSetDateTime(string parametros, EventLog eventLog, bool debug)
        {
            _eventLog = eventLog;
            _debug = debug;
            _parametros = parametros;
        }
        public string Procesar()
        {
            try
            {
                return SetMLocalTime().ToString();
            }
            catch (Exception)
            {
                return "False";
            }


        }

        public bool SetMLocalTime()
        {
            DateTime fecha;
            bool result =false;

            if (DateTime.TryParse(_parametros, out fecha))
            {
                SystemTime fechaHora = new SystemTime();
                try
                {
                    fechaHora.Year = (ushort)fecha.Year;
                    fechaHora.Month = (ushort)fecha.Month;
                    fechaHora.Day = (ushort)fecha.Day;
                    fechaHora.Hour = (ushort)fecha.Hour;
                    fechaHora.Minute = (ushort)fecha.Minute;
                    fechaHora.Second = (ushort)fecha.Second;

                    result = SetLocalTime(ref fechaHora);
                }
                catch 
                {
                    result = false;
                }
                return result;
            }
            else

                return false;
        }
    }
}
