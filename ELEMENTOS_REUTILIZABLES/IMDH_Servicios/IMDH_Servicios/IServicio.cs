﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDH_Servicios
{
    public interface IServicio
    {   
        string Procesar();
    }
}
