using ElementosCompartidos;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ImpTratamiento
{
	internal static class BListado
	{

		//declaraci�n de variables globales

		public static SqlConnection rcConexion = null;
		public static object Listado_general = null;

		public static string gUsuario = String.Empty;
		public static string GstTiposervicio = String.Empty;
		public static string GiA�oRegistro = String.Empty;
		public static string GiNumeroRegistro = String.Empty;
		public static string GstFechaConsulta = String.Empty;
		public static string GstGidenpac = String.Empty;

		public static string vstPathAplicacion = String.Empty;
		public static string vstNombreBaseDatos = String.Empty;
		public static string vstDSN = String.Empty;

		public static Mensajes.ClassMensajes clase_mensaje = null;
		public static string vstSeparadorDecimal = String.Empty;
		public static string stDecimalBD = String.Empty;

		//Global vstFechaHoraSis    As String
		public static string vstAlergias = String.Empty;
		public static string vstNombrePAciente = String.Empty;

		public static bool vfHistoriaClinica = false;

		public static bool vBMostrarTodos = false;
		public static string vSTablaMostrarTodos = String.Empty;

		public static string vstUsuarioCrystal = String.Empty;
		public static string vstContrase�aCrystal = String.Empty;

        //Para el listado de las recetas
        //INDRA jproche  TODO_X_5 - USO BASE DATOS ACCES 23/05/2016
        //UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        //public static Database bs = null;
        //INDRA jproche  TODO_X_5 23/05/2016
        //UPGRADE_ISSUE: (2068) Recordset object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
        //public static Recordset Rs = null;

		//Declaro una estructura para el listado de farmacos
		public struct DosisFarmacos
		{
			public string dosis;
			public string CadenaHoras;
			public static DosisFarmacos CreateInstance()
			{
					DosisFarmacos result = new DosisFarmacos();
					result.dosis = String.Empty;
					result.CadenaHoras = String.Empty;
					return result;
			}
		}

		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public string VstPoblaciHo;
			public bool VfCabecera;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					result.VstPoblaciHo = String.Empty;
					return result;
			}
		}
		public static BListado.CabCrystal VstrCrystal = BListado.CabCrystal.CreateInstance();
		public static bool bEpisodiodeAlta = false;


		internal static string UnidadDosis(int iCodigo)
		{
			string result = String.Empty;
			string sql = String.Empty;
			DataSet RrDescripcion = null;
			//para recuperar la unidad de la dosis
			result = "";

			if (!Convert.IsDBNull(iCodigo))
			{
				sql = "select dunitoma from DUNITOMAVA where gunitoma = " + iCodigo.ToString() + " ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
				RrDescripcion = new DataSet();
				tempAdapter.Fill(RrDescripcion);
				if (RrDescripcion.Tables[0].Rows.Count != 0)
				{	
					result = Convert.ToString(RrDescripcion.Tables[0].Rows[0]["dunitoma"]).Trim();
				}
				RrDescripcion.Close();
			}
			return result;
		}

		internal static string HorasFarmacos(DataSet Registro, string UnidadDosis)
		{
			//funcion que devuelve la linea de prescripcion que se va a insertar en el listado
			string result = String.Empty;
			bool fNoencontrado = false;
			int iPosicion = 0;
			string stCadenaAux = String.Empty;

			BListado.DosisFarmacos[] DosisFarmacos = null;

			result = "";

			DosisFarmacos = new BListado.DosisFarmacos[1];
			for (int aik = 0; aik <= 23; aik++)
			{
				fNoencontrado = false;
				if (!Convert.IsDBNull(Registro.Tables[0].Rows[0]["cdosisfa" + StringsHelper.Format(aik, "00") + ""]))
				{
					for (int aij = 0; aij <= DosisFarmacos.GetUpperBound(0); aij++)
					{
						if (DosisFarmacos[aij].dosis == Convert.ToString(Registro.Tables[0].Rows[0]["cdosisfa" + StringsHelper.Format(aik, "00") + ""]))
						{
							DosisFarmacos[aij].CadenaHoras = DosisFarmacos[aij].CadenaHoras + "," + aik.ToString();
							fNoencontrado = true;
							break;
						}
					}
					if (!fNoencontrado)
					{
						DosisFarmacos = ArraysHelper.RedimPreserve(DosisFarmacos, new int[]{DosisFarmacos.GetUpperBound(0) + 2});
						DosisFarmacos[DosisFarmacos.GetUpperBound(0)].dosis = Convert.ToString(Registro.Tables[0].Rows[0]["cdosisfa" + StringsHelper.Format(aik, "00") + ""]);
						DosisFarmacos[DosisFarmacos.GetUpperBound(0)].CadenaHoras = Conversion.Str(aik);
					}
				}
				else
				{
					//cuando la dosis es nula
				}
			}
			//una vez que ha leido todas las dosis se compone el campo del listado
			StringBuilder stPrescripcion = new StringBuilder();
			for (int aik = 1; aik <= DosisFarmacos.GetUpperBound(0); aik++)
			{
				if (stPrescripcion.ToString().Trim() != "")
				{
					stPrescripcion.Append("-- ");
				}
				//antes de escribir en la linea del listado controlar la ultima para cambiar la , por la y
				if (Convert.ToString(Registro.Tables[0].Rows[0]["iprecisa"]) == "S")
				{
					if (!Convert.IsDBNull(Registro.Tables[0].Rows[0]["gfretoma"]))
					{
						DosisFarmacos[aik].CadenaHoras = DescripcionFrecuencia(Convert.ToInt32(Registro.Tables[0].Rows[0]["gfretoma"]));
					}
					
					string tempRefParam = (Convert.IsDBNull(DosisFarmacos[aik].dosis)) ? "" : DosisFarmacos[aik].dosis;
					stPrescripcion.Append("Si Precisa  " + Serrores.ConvertirDecimales(ref tempRefParam, ref vstSeparadorDecimal, 2));
					stPrescripcion.Append(" " + UnidadDosis + " ");
					stPrescripcion.Append(DosisFarmacos[aik].CadenaHoras);
				}
				else
				{
					if (DosisFarmacos[aik].CadenaHoras.IndexOf(',') >= 0)
					{
						for (int aij = 1; aij <= DosisFarmacos[aik].CadenaHoras.Length; aij++)
						{
							if (DosisFarmacos[aik].CadenaHoras.Substring(aij - 1, Math.Min(1, DosisFarmacos[aik].CadenaHoras.Length - (aij - 1))) == ",")
							{
								iPosicion = aij;
							}
						}
						stCadenaAux = DosisFarmacos[aik].CadenaHoras.Substring(0, Math.Min(iPosicion - 1, DosisFarmacos[aik].CadenaHoras.Length)) + " y " + DosisFarmacos[aik].CadenaHoras.Substring(iPosicion);
						DosisFarmacos[aik].CadenaHoras = stCadenaAux;
					}
					string tempRefParam2 = (Convert.IsDBNull(DosisFarmacos[aik].dosis)) ? "" : DosisFarmacos[aik].dosis;
					stPrescripcion.Append(Serrores.ConvertirDecimales(ref tempRefParam2, ref vstSeparadorDecimal, 2));
					stPrescripcion.Append(" " + UnidadDosis + " a las");
					stPrescripcion.Append(DosisFarmacos[aik].CadenaHoras + " horas");
				}
			}

			return stPrescripcion.ToString();
		}

		internal static string MotivoTrat(int iCodigo)
		{
			string result = String.Empty;
			string sql = String.Empty;
			DataSet RrDescripcion = null;
			try
			{
				result = "";

				if (!Convert.IsDBNull(iCodigo))
				{
					sql = "select dmotisus from EMOTSUSP where gmsuspro = " + iCodigo.ToString() + " ";

					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
					RrDescripcion = new DataSet();
					tempAdapter.Fill(RrDescripcion);
					if (RrDescripcion.Tables[0].Rows.Count != 0 && RrDescripcion.Tables[0].Rows.Count > 0)
					{
						result = (Convert.IsDBNull(RrDescripcion.Tables[0].Rows[0]["dmotisus"])) ? "" : Convert.ToString(RrDescripcion.Tables[0].Rows[0]["dmotisus"]).Trim();
					}
					RrDescripcion.Close();
				}
				return result;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("TerapeuticaDll", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "MotivoTrat:PROGRAMACION_HORARIA", ex);
				return result;
			}
		}
		internal static string ViaAdminis(int iCodigo)
		{
			string result = String.Empty;
			string sql = String.Empty;
			DataSet RrDescripcion = null;
			//recuperar la via de administracion del farmaco
			result = "";

			if (!Convert.IsDBNull(iCodigo))
			{
				sql = "select dviaadmi from DVIASADM " + "where DVIASADM.gviadmin = " + iCodigo.ToString() + " ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
				RrDescripcion = new DataSet();
				tempAdapter.Fill(RrDescripcion);
				if (RrDescripcion.Tables[0].Rows.Count != 0)
				{
					result = Convert.ToString(RrDescripcion.Tables[0].Rows[0]["dviaadmi"]);
				}
				RrDescripcion.Close();
			}
			return result;
		}

		internal static void GestionListado()
		{
			object DBEngine = null;


			//Listado para ver los farmacos activos salen los de estado iniciado y aquellos que estan suspendidos
			//con todos sus detalles

			BuscarDatosPaciente(GstGidenpac);

			string stNombreTabla = "TRATAMIENTO";
			//Se presenta en pantalla un listado con los farmacos que estan activos
			//se genera una tabla temporal
			CrearTabla(stNombreTabla);

			//llenar la tabla temporal
			string sqlTemporal = "select * from " + stNombreTabla + " where 1=2";

            //INDRA jproche  TODO_X_5 23/05/2016
            //UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //Rs = (Recordset) bs.OpenRecordset(sqlTemporal);

			if (GstTiposervicio == "C")
			{
				BuscarDatosTratamiento(); // stFMaxFarmacos
			}
			else
			{
				BuscarDatosTratamiento();
			}

            //INDRA jproche  TODO_X_5 23/05/2016
            //UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //Rs.Close(); //se cierra el registro que escribe en tratamientos
            //UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            /*INDRA jproche  TODO_X_5 23/05/2016
            bs.Close(); //se cierra la base de datos
			//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			DBEngine.Workspaces(0).Close();
            */

			//despues hay que confeccionar el listado
			if (!VstrCrystal.VfCabecera)
			{
				proCabCrystal();
			}

            //llenado de formulas
            /*INDRA jproche  TODO_X_5 23/05/2016
			//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.ReportFileName = vstPathAplicacion + "\\..\\ElementosComunes\\RPT\\EGI327R1.rpt";

			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.Formulas["Grupo"] = "\"" + VstrCrystal.VstGrupoHo + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.Formulas["DireccionH"] = "\"" + VstrCrystal.VstDireccHo + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.Formulas["NombreH"] = "\"" + VstrCrystal.VstNombreHo + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.Formulas["NInforme"] = "\"" + "EGI327R1" + "\"";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.Formulas["ALERGICO"] = "\"" + vstAlergias + "\"";
            

			string LitPersona = Serrores.ObtenerLiteralPersona(rcConexion);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1) + ":";
			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.Formulas["LitPers"] = "\"" + LitPersona + "\"";
			if (vBMostrarTodos)
			{
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Listado_general.Formulas["titulo"] = "\"" + "Tratamiento farmacol�gico completo" + "\"";
			}
			else
			{
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Listado_general.Formulas["titulo"] = "\"" + "Tratamiento farmacol�gico actual" + "\"";
			}


			//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.Destination = 0; //crptToWindow 'se muestra por ventana
			//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.WindowState = (int) FormWindowState.Maximized; //se muestra maximizado
			//UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.DataFiles[0] = vstPathAplicacion + "\\" + vstNombreBaseDatos;
			//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.SQLQuery = " SELECT * From " + stNombreTabla;

			//se registra el dsn para que no de problemas
			//stBaseTemporal = Mid(vstNombreBaseDatos, 1, (InStr(1, vstNombreBaseDatos, ".")) - 1)
			//RegistrarDsnAccess stBaseTemporal, vstPathAplicacion 'para que no de error al lanzar el listado
			//Listado_general.Connect = "DSN=" & vstDSN & ""

			//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.WindowTitle = "Tratamiento farmacol�gico ";
			//UPGRADE_TODO: (1067) Member WindowShowPrintSetupBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.WindowShowPrintSetupBtn = true;
			//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.Action = 1;
			//para que salga el n�mero de p�ginas totales

			//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.PageCount();
			//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.PrinterStopPage = Listado_general.PageCount;

			vacia_formulas(Listado_general, 6); //limpio las formulas

			//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Listado_general.Reset();
            */
        }

        internal static void CrearTabla(string NombreTabla)
		{
             /*INDRA jproche  TODO_X_5 23/05/2016
			object dbLangSpanish = null;
			object DBEngine = null;
			bool vbBase = false;
			//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			string vstPathTemp = String.Empty;

			//comprueba si existe la tabla

			try
			{
				//Se crea una base de datos temporal para realizar el informe
				vstPathTemp = vstPathAplicacion + "\\" + vstNombreBaseDatos;
				if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
				{
					vbBase = true;
				}

				if (!vbBase)
				{ // SI est� dado
					//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					bs = (Database) DBEngine.Workspaces(0).CreateDatabase(vstPathTemp, dbLangSpanish);
					vbBase = true;
				}
				else
				{
					//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					bs = (Database) DBEngine.Workspaces(0).OpenDatabase(vstPathTemp);
					//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					foreach (TableDef Tabla in bs.TableDefs)
					{
						//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						if (Convert.ToString(Tabla.Name) == NombreTabla.ToUpper())
						{
							//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							bs.Execute("DROP TABLE " + NombreTabla.ToUpper());
						}
					}
				}

				//crear la tabla temporal
				//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				bs.Execute("CREATE TABLE " + NombreTabla.ToUpper() + " (GIDENPAC text, PACIENTE Text,FARMACO Text, " + 
				           " MEDICO TEXT, VIA TEXT, ETIQUETA TEXT, MOTIVO TEXT, DURACION TEXT, FRECUENCIA TEXT, " + 
				           " FECHAINICIO TEXT, DIAS TEXT, ALERGICO TEXT ,OBSERVACIONES TEXT,Prescripcion MEMO, CAMA text) ");
			}
			catch
			{
			}
            */

		}
		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************

			DataSet tRrCrystal = null;
			string tstCab = String.Empty;
			try
			{
				tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, rcConexion);
				tRrCrystal = new DataSet();
				tempAdapter.Fill(tRrCrystal);
				VstrCrystal.VstGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
				tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, rcConexion);
				tRrCrystal = new DataSet();
				tempAdapter_2.Fill(tRrCrystal);
				VstrCrystal.VstNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
				tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, rcConexion);
				tRrCrystal = new DataSet();
				tempAdapter_3.Fill(tRrCrystal);
				VstrCrystal.VstDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
				tstCab = "SELECT valfanu2 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tstCab, rcConexion);
				tRrCrystal = new DataSet();
				tempAdapter_4.Fill(tRrCrystal);
				VstrCrystal.VstPoblaciHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU2"]).Trim();
				tRrCrystal.Close();
				VstrCrystal.VfCabecera = true;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("TerapeuticaDll", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "proCabCrystal:MEnfermeria.bas", ex);
				VstrCrystal.VfCabecera = false;
			}
		}


		internal static void RegistrarDsnAccess(string base_de_datos, string PathAplicacion)
		{
			// DSN PARA ESTABLECER EL ORIGEN DE DATOS EN ACCESS PARA LAS TABLAS TEMPORALES
			//una conexi�n DSN para los controles Crystal de la aplicaci�n para los RPT
			//VstCrystal = "GHOSP_ACCESS"
			string tstconexion = "Description=" + "Microsoft Access para Enfermeria" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "" + "\r" + "dbq=" + PathAplicacion + "\\" + base_de_datos.Trim() + "";
            /*INDRA jproche  TODO_X_5 23/05/2016
			//UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(vstDSN, "MicroSoft Access Driver (*.mdb)", true, tstconexion);
            */
		}

		internal static void vacia_formulas(object Listado, int Numero)
		{

			for (int tiForm = 0; tiForm <= Numero; tiForm++)
			{
                 /*INDRA jproche  TODO_X_5 23/05/2016
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Listado.Formulas[tiForm] = "";
                */
			}

		}

		internal static void BuscarDatosTratamiento()
		{
			string sql = String.Empty;
			DataSet RrListado = null;
			DataSet RrCursor = null;

			string stUnidadDosis = String.Empty;

			try
			{

				//segun el GstTiposervicio tiene que sacar los datos de un sitio o de otro
				//esta select es diferente segun el gsttiposervicio
				switch(GstTiposervicio)
				{
					case "C" : 
						//hay dos posibilidades 
						//que queramos saber todos los farmacos de consultas o solo los 
						//farmacos del episodio 
						if (StringsHelper.ToDoubleSafe(GiA�oRegistro) == 0 && StringsHelper.ToDoubleSafe(GiNumeroRegistro) == 0)
						{
							sql = "select EFARMACO.*, DCODFARMVT.dnomfarm ";
							sql = sql + " from EFARMACO, DCODFARMVT, CCONSULT where ";
							sql = sql + "CCONSULT.gidenpac = '" + GstGidenpac + "' ";
							sql = sql + "and EFARMACO.itiposer = 'C' ";
							sql = sql + "and CCONSULT.ganoregi = EFARMACO.ganoregi ";
							sql = sql + "and CCONSULT.gnumregi = EFARMACO.gnumregi ";
							sql = sql + "and (iesttrat = 'I' or iesttrat = 'S') ";
							sql = sql + "and ffinmovi is null ";
							sql = sql + "and EFARMACO.gfarmaco= DCODFARMVT.gfarmaco ";
							sql = sql + " order by EFARMACO.finitrat ";

						}
						else
						{
							//los farmaco de largo tratamiento
							sql = "select EFARMACO.*, DCODFARMVT.dnomfarm ";
							sql = sql + " from EFARMACO, DCODFARMVT, CCONSULT where ";
							sql = sql + "CCONSULT.gidenpac = '" + GstGidenpac + "' ";
							sql = sql + "and ilargtra = 'S' ";
							sql = sql + "and EFARMACO.itiposer = 'C' ";
							sql = sql + "and CCONSULT.ganoregi = EFARMACO.ganoregi ";
							sql = sql + "and CCONSULT.gnumregi = EFARMACO.gnumregi ";
							sql = sql + "and (iesttrat = 'I' or iesttrat = 'S') ";
							sql = sql + "and ffinmovi is null ";
							//sql = sql & "and fapuntes <= " & FormatFechaHMS(GstFechaConsulta) & " "
							sql = sql + "and EFARMACO.gfarmaco= DCODFARMVT.gfarmaco ";

							sql = sql + "Union all ";

							//los farmacos del episodio de consultas
							sql = sql + "select EFARMACO.*, DCODFARMVT.dnomfarm ";
							sql = sql + "from EFARMACO, DCODFARMVT where ";
							sql = sql + "EFARMACO.itiposer = '" + GstTiposervicio + "' ";
							sql = sql + "and EFARMACO.ganoregi = " + GiA�oRegistro + " ";
							sql = sql + "and EFARMACO.gnumregi = " + GiNumeroRegistro + " ";
							sql = sql + "and (ilargtra <> 'S' or ilargtra is null) ";
							sql = sql + "and (iesttrat = 'I' or iesttrat = 'S') ";
							sql = sql + "and ffinmovi is null ";
							sql = sql + "and EFARMACO.gfarmaco= DCODFARMVT.gfarmaco ";
							sql = sql + " order by EFARMACO.finitrat ";

						} 						 
						break;
					case "H" : case "U" : 
						if (vBMostrarTodos)
						{
							//si es este caso se supone que tienen que venir los datos del episodio
							//saldra el detalle de los todos los farmacos del episodio

							sql = "select DCODFARMVT.dnomfarm,  case " + vSTablaMostrarTodos + ".iesttrat when 'I' then 1 when 'S' then 2 when 'F' then 3 else 4 end estado," + vSTablaMostrarTodos + ".* ";
							sql = sql + "from " + vSTablaMostrarTodos + ", DCODFARMVT ";
							sql = sql + "Where " + vSTablaMostrarTodos + ".itiposer = '" + GstTiposervicio + "' ";
							sql = sql + "and " + vSTablaMostrarTodos + ".ganoregi = " + GiA�oRegistro + " ";
							sql = sql + "and " + vSTablaMostrarTodos + ".gnumregi = " + GiNumeroRegistro + " ";
							sql = sql + "and (" + vSTablaMostrarTodos + ".iesttrat = 'I' or " + vSTablaMostrarTodos + ".iesttrat = 'S' or " + vSTablaMostrarTodos + ".iesttrat = 'F') ";
							sql = sql + "and " + vSTablaMostrarTodos + ".ffinmovi is NULL ";
							sql = sql + "and " + vSTablaMostrarTodos + ".gfarmaco = DCODFARMVT.gfarmaco ";
							sql = sql + " order by estado," + vSTablaMostrarTodos + ".finitrat ";
						}
						else
						{
							//si es este caso se supone que tienen que venir los datos del episodio
							//saldra el detalle de los farmacos del episodio que estan activos
							sql = "select DCODFARMVT.dnomfarm,EFARMACO.* ";
							sql = sql + "from EFARMACO, DCODFARMVT ";
							sql = sql + "Where EFARMACO.itiposer = '" + GstTiposervicio + "' ";
							sql = sql + "and EFARMACO.ganoregi = " + GiA�oRegistro + " ";
							sql = sql + "and EFARMACO.gnumregi = " + GiNumeroRegistro + " ";
							sql = sql + "and (EFARMACO.iesttrat = 'I' or EFARMACO.iesttrat = 'S') ";
							sql = sql + "and EFARMACO.ffinmovi is NULL ";
							sql = sql + "and EFARMACO.gfarmaco = DCODFARMVT.gfarmaco ";
							sql = sql + " order by EFARMACO.finitrat ";
						} 
						break;
					case "" : 
						//cuando solo viene el gidenpac hay que buscar en CCONSULT,AEPISADM,UEPISURG 
						sql = "select EFARMACO.*, DCODFARMVT.dnomfarm "; 
						sql = sql + " from EFARMACO, DCODFARMVT, CCONSULT where "; 
						sql = sql + "CCONSULT.gidenpac = '" + GstGidenpac + "' "; 
						sql = sql + "and EFARMACO.itiposer = 'C' "; 
						sql = sql + "and CCONSULT.ganoregi = EFARMACO.ganoregi "; 
						sql = sql + "and CCONSULT.gnumregi = EFARMACO.gnumregi "; 
						sql = sql + "and (iesttrat = 'I' or iesttrat = 'S') "; 
						sql = sql + "and ffinmovi is null "; 
						sql = sql + "and EFARMACO.gfarmaco= DCODFARMVT.gfarmaco "; 
						 
						sql = sql + "Union all "; 
						 
						sql = sql + "select EFARMACO.*, DCODFARMVT.dnomfarm "; 
						sql = sql + " from EFARMACO, DCODFARMVT, AEPISADM where "; 
						sql = sql + "AEPISADM.gidenpac = '" + GstGidenpac + "' "; 
						sql = sql + "and EFARMACO.itiposer = 'H' "; 
						sql = sql + "and AEPISADM.ganoadme = EFARMACO.ganoregi "; 
						sql = sql + "and AEPISADM.gnumadme = EFARMACO.gnumregi "; 
						sql = sql + "and (iesttrat = 'I' or iesttrat = 'S') "; 
						sql = sql + "and ffinmovi is null "; 
						sql = sql + "and EFARMACO.gfarmaco= DCODFARMVT.gfarmaco "; 
						 
						sql = sql + "Union all "; 
						 
						sql = sql + "select EFARMACO.*, DCODFARMVT.dnomfarm "; 
						sql = sql + " from EFARMACO, DCODFARMVT, UEPISURG where "; 
						sql = sql + "UEPISURG.gidenpac = '" + GstGidenpac + "' "; 
						sql = sql + "and EFARMACO.itiposer = 'U' "; 
						sql = sql + "and UEPISURG.ganourge = EFARMACO.ganoregi "; 
						sql = sql + "and UEPISURG.gnumurge = EFARMACO.gnumregi "; 
						sql = sql + "and (iesttrat = 'I' or iesttrat = 'S') "; 
						sql = sql + "and ffinmovi is null "; 
						sql = sql + "and EFARMACO.gfarmaco= DCODFARMVT.gfarmaco "; 
						 
						sql = sql + " order by EFARMACO.finitrat "; 
						break;
				}

				if (vfHistoriaClinica)
				{
					string tempRefParam = "CCONSULT";
					Serrores.PonerVistas(ref sql, ref tempRefParam, rcConexion);
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
				RrListado = new DataSet();
				tempAdapter.Fill(RrListado);

				if (RrListado.Tables[0].Rows.Count != 0)
				{
                    /*INDRA jproche  TODO_X_5 23/05/2016 Uso de Recordset
					foreach (DataRow iteration_row in RrListado.Tables[0].Rows)
					{
						//UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						Rs.AddNew();
						//NombrePaciente
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["gidenpac"] = (Recordset) GstGidenpac;
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["paciente"] = (Recordset) vstNombrePAciente;
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["alergico"] = (Recordset) vstAlergias;
						//Farmaco
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(farmaco). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["farmaco"] = (Recordset) ((Convert.IsDBNull(iteration_row["dnomfarm"])) ? "" : Convert.ToString(iteration_row["dnomfarm"]).Trim());
						//medico
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(iteration_row["gpersona"]))
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(medico). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["medico"] = (Recordset) Facultativo(Convert.ToInt32(iteration_row["gpersona"]));
						}
						else
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["medico"] = (Recordset) "";
						}
						//via de administracion
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(iteration_row["gviadmin"]))
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(via). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["via"] = (Recordset) ViaAdminis(Convert.ToInt32(iteration_row["gviadmin"]));
						}
						else
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["via"] = (Recordset) "";
						}
						//se a�ade el campo observaciones
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (Convert.IsDBNull(iteration_row["oobserva"]))
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["observaciones"] = (Recordset) "";
						}
						else
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(observaciones). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["observaciones"] = (Recordset) Convert.ToString(iteration_row["oobserva"]).Trim();
						}
						//se carga la unidad de dosis del farmaco en stUnidadDosis
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(iteration_row["gunitoma"]))
						{
							stUnidadDosis = UnidadDosis(Convert.ToInt32(iteration_row["gunitoma"]));
						}
						else
						{
							stUnidadDosis = "";
						}
						if (!bEpisodiodeAlta)
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(cama). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["cama"] = (Recordset) CamaPaciente(GstGidenpac);
						}
						else
						{
							//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
							Rs["cama"] = (Recordset) "";
						}
						//solo vamos a recuperar los farmacos activos el resto lo dejamos porsi
						switch(Convert.ToString(iteration_row["iesttrat"]))
						{
							case "I" : 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["etiqueta"] = (Recordset) ""; 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Motivo"] = (Recordset) ""; 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								if (!Convert.IsDBNull(iteration_row["cdiastra"]))
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(duracion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["duracion"] = (Recordset) Convert.ToString(iteration_row["cdiastra"]).Trim();
								}
								else
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["duracion"] = (Recordset) "";
								} 
								//frecuencia 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								if (!Convert.IsDBNull(iteration_row["periodic"]))
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(frecuencia). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["frecuencia"] = (Recordset) ("Cada " + Conversion.Str(iteration_row["periodic"]).Trim() + " d�as");
								}
								else
								{
									//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
									if (!Convert.IsDBNull(iteration_row["vdiasfre"]))
									{
										//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(frecuencia). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
										//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
										Rs["frecuencia"] = (Recordset) CalculaFrecuenciaEnDias(Convert.ToString(iteration_row["vdiasfre"]));
									}
									else
									{
										//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
										Rs["frecuencia"] = (Recordset) "";
									}
								} 
								//llenar el campo de prescripcion 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(prescripcion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["prescripcion"] = (Recordset) HorasFarmacos(RrListado, stUnidadDosis); 
								if (Convert.ToString(iteration_row["idounica"]) == "S")
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(prescripcion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1068) Rs() of type Recordset is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["prescripcion"] = (Recordset) (((string) Rs("prescripcion")) + " Dosis �nica");
								} 
								//calcular el numero de dias 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								if (!Convert.IsDBNull(iteration_row["finitrat"]))
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(FechaInicio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["FechaInicio"] = (Recordset) Convert.ToDateTime(iteration_row["finitrat"]).ToString("dd/MM/yyyy HH:mm");
									//Si hay que mostrar todos los f�rmacos y el paciente est� dado de alta, mostramos
									//los d�as de tratamiento desde el ingreso al alta.
									if (vBMostrarTodos && vSTablaMostrarTodos.Trim().ToUpper() == "EFARMALT")
									{
										sql = "";
										//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
										Rs["dias"] = (Recordset) "";
										if (GstTiposervicio == "H")
										{
											sql = "select faltplan Fechaalta from aepisadm where ganoadme =  " + GiA�oRegistro + " and gnumadme = " + GiNumeroRegistro;
										}
										if (GstTiposervicio == "U")
										{
											sql = "select faltaurg Fechaalta from uepisurg where ganourge =  " + GiA�oRegistro + " and gnumurge = " + GiNumeroRegistro;
										}
										if (sql != "")
										{
											SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, rcConexion);
											RrCursor = new DataSet();
											tempAdapter_2.Fill(RrCursor);
											if (RrCursor.Tables[0].Rows.Count != 0)
											{
												//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
												if (!Convert.IsDBNull(RrCursor.Tables[0].Rows[0]["Fechaalta"]))
												{
													//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dias). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
													//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
													Rs["dias"] = (Recordset) DateAndTime.DateDiff("d", Convert.ToDateTime(iteration_row["finitrat"]), Convert.ToDateTime(RrCursor.Tables[0].Rows[0]["Fechaalta"]), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
												}
											}
											//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrCursor.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
											RrCursor.Close();
										}
									}
									else
									{
										//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dias). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
										//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
										Rs["dias"] = (Recordset) DateAndTime.DateDiff("d", Convert.ToDateTime(iteration_row["finitrat"]), DateTime.Parse(GstFechaConsulta), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
									}
								} 
								 
								break;
							case "S" : 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(etiqueta). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["etiqueta"] = (Recordset) ("SUSPENDIDO " + Convert.ToDateTime(iteration_row["fsuspens"]).ToString("dd/MM/yyyy HH:mm")); 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								if (!Convert.IsDBNull(iteration_row["gmsuspro"]))
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Motivo). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["Motivo"] = (Recordset) MotivoTrat(Convert.ToInt32(iteration_row["gmsuspro"]));
								}
								else
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["Motivo"] = (Recordset) "";
								} 
								//Rs("duracion") = "" 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Frecuencia"] = (Recordset) ""; 
								//Rs("fechainicio") = "" 
								//llenar el campo de prescripcion 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(prescripcion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["prescripcion"] = (Recordset) HorasFarmacos(RrListado, stUnidadDosis); 
								//Fecha de incio 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								if (!Convert.IsDBNull(iteration_row["finitrat"]))
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(FechaInicio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["FechaInicio"] = (Recordset) Convert.ToDateTime(iteration_row["finitrat"]).ToString("dd/MM/yyyy HH:mm");
								} 
								//calcular el numero de dias 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								if (!Convert.IsDBNull(iteration_row["finitrat"]) && !Convert.IsDBNull(iteration_row["fsuspens"]))
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dias). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["dias"] = (Recordset) DateAndTime.DateDiff("d", Convert.ToDateTime(iteration_row["finitrat"]), Convert.ToDateTime(iteration_row["fsuspens"]), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
								} 
								break;
							case "F" : 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(etiqueta). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["etiqueta"] = (Recordset) ("FINALIZADO " + Convert.ToDateTime(iteration_row["ffintrat"]).ToString("dd/MM/yyyy HH:mm")); 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								if (!Convert.IsDBNull(iteration_row["gmsuspro"]))
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Motivo). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["Motivo"] = (Recordset) MotivoTrat(Convert.ToInt32(iteration_row["gmsuspro"]));
								}
								else
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["Motivo"] = (Recordset) "";
								} 
								//Rs("duracion") = "" 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Frecuencia"] = (Recordset) ""; 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Prescripcion"] = (Recordset) ""; 
								//Rs("fechainicio") = "" 
								//Fecha de incio 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								if (!Convert.IsDBNull(iteration_row["finitrat"]))
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(FechaInicio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["FechaInicio"] = (Recordset) Convert.ToDateTime(iteration_row["finitrat"]).ToString("dd/MM/yyyy HH:mm");
								} 
								//calcular el numero de dias 
								 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								if (!Convert.IsDBNull(iteration_row["finitrat"]) && !Convert.IsDBNull(iteration_row["ffintrat"]))
								{
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(dias). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
									Rs["dias"] = (Recordset) DateAndTime.DateDiff("d", Convert.ToDateTime(iteration_row["finitrat"]), Convert.ToDateTime(iteration_row["ffintrat"]), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
								} 
								break;
						}
						//UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						Rs.Update();
					}
                    */
				}
				else
				{

				}
				RrListado.Close();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("TerapeuticaDll", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "Listado_Prescripcion:PROGRAMACION_HORARIA", ex);
			}
		}
		private static string CamaPaciente(string stGidenpac)
		{

			string stCama = "";
			string sql = "select gplantas,ghabitac,gcamasbo from DCAMASBO ";
			sql = sql + "where gidenpac = '" + stGidenpac + "' and iestcama = 'O' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{ //si hay datos
				stCama = StringsHelper.Format(RrSql.Tables[0].Rows[0]["gplantas"], "00") + StringsHelper.Format(RrSql.Tables[0].Rows[0]["ghabitac"], "00") + StringsHelper.Format(RrSql.Tables[0].Rows[0]["gcamasbo"], "00");
			}

			RrSql.Close();

			return stCama;

		}
		private static string CalculaFrecuenciaEnDias(string cadena)
		{
			string[] m = new string[]{String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty};
			m[0] = "L";
			m[1] = "M";
			m[2] = "X";
			m[3] = "J";
			m[4] = "V";
			m[5] = "S";
			m[6] = "D";
			StringBuilder retorno = new StringBuilder();
			for (int x = 1; x <= 7; x++)
			{
				if (cadena.Substring(x - 1, Math.Min(1, cadena.Length - (x - 1))) == "*")
				{
					if (retorno.ToString() != "")
					{
						retorno.Append("-");
					}
					retorno.Append(m[x - 1]);
				}
			}
			return retorno.ToString();
		}

		internal static string DescripcionFrecuencia(int iCodigoFrecuencia)
		{

			string stDescripcion = "";
			string sql = "select dfretoma from DFRECUENVA ";
			sql = sql + "where gfretoma = " + iCodigoFrecuencia.ToString() + "";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{ //si hay datos
			  stDescripcion = (Convert.IsDBNull("dfretoma")) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dfretoma"]).Trim();
			}

			RrSql.Close();

			return stDescripcion;

		}

		internal static void BuscarDatosPaciente(string stPaciente)
		{
			string sql = String.Empty;
			DataSet RrSql = null;

			try
			{
				sql = "Select * from DPACIENT " + " where gidenpac = '" + stPaciente + "' ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcConexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
					vstNombrePAciente = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dnombpac"]).Trim() + " ";
					vstNombrePAciente = vstNombrePAciente + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape1pac"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dape1pac"]).Trim() + " ");
					vstNombrePAciente = vstNombrePAciente + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape2pac"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dape2pac"]).Trim());

					//si recupera algun valor hay que buscar el valor que tiene
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ialermed"]))
					{
						if (Convert.ToString(RrSql.Tables[0].Rows[0]["ialermed"]) == "S")
						{
							if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["oalermed"]))
							{
								vstAlergias = "ALERGICO A: " + Convert.ToString(RrSql.Tables[0].Rows[0]["oalermed"]).Trim().ToUpper();
							}
							else
							{
								vstAlergias = "ALERGICO ";
							}
						}
						else
						{
							vstAlergias = "";
						}
					}
					else
					{
						vstAlergias = "";
					}
				}
				else
				{
					vstAlergias = "";
				}
				RrSql.Close();
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("Sueros.dll", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "Farmacos_sueros: BuscarAlergias", ex);
			}
		}

		internal static string Facultativo(int iCodigoNombre)
		{
			//funcion que devuelve el nombre de un medico...
			string result = String.Empty;
			DataSet RrNombre = null;
			string sqlNombre = String.Empty;
			string stCadena = String.Empty;

			try
			{
				result = "";

				if (!Convert.IsDBNull(iCodigoNombre))
				{
					sqlNombre = "select dnompers,dap1pers,dap2pers " + "from dpersona where gpersona =" + iCodigoNombre.ToString();

					SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlNombre, rcConexion);
					RrNombre = new DataSet();
					tempAdapter.Fill(RrNombre);
					if (RrNombre.Tables[0].Rows.Count != 0)
					{
						stCadena = (Convert.IsDBNull(RrNombre.Tables[0].Rows[0]["dap1pers"])) ? "" : Convert.ToString(RrNombre.Tables[0].Rows[0]["dap1pers"]).Trim();
						stCadena = stCadena + " " + ((Convert.IsDBNull(RrNombre.Tables[0].Rows[0]["dap2pers"])) ? "" : Convert.ToString(RrNombre.Tables[0].Rows[0]["dap2pers"]).Trim());
						stCadena = stCadena + ((Convert.IsDBNull(RrNombre.Tables[0].Rows[0]["dnompers"])) ? "" : ", " + Convert.ToString(RrNombre.Tables[0].Rows[0]["dnompers"]).Trim());
						result = stCadena;
					}
					RrNombre.Close();
				}
				return result;
			}
			catch
			{
				return result;
			}
		}
	}
}
 