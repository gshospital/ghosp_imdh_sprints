using ElementosCompartidos;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using Telerik.WinControls;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ImpTratamiento
{
	public class McImprimir
	{
		public McImprimir()
		{
			try
			{
				BListado.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{
				RadMessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}
		}

		~McImprimir()
		{
			BListado.clase_mensaje = null;
		}

		public void ProLoad(SqlConnection nConnet, string gidenpac, string stUsuario, object ObjetoCrystal, string PathAplicacion, string DSN, string BaseDatos, object stiTipoSer_optional, object Episodio_optional, ref object UsuarioCrystal_optional, bool HistoriaClinica, bool MostrarHistoricos, bool MostrarTodos, string TablaMostrarTodos)
		{
			string stiTipoSer = (stiTipoSer_optional == Type.Missing) ? String.Empty : stiTipoSer_optional as string;
			string Episodio = (Episodio_optional == Type.Missing) ? String.Empty : Episodio_optional as string;
			string UsuarioCrystal = (UsuarioCrystal_optional == Type.Missing) ? String.Empty : UsuarioCrystal_optional as string;
			try
			{

				//asignamos los parametros de entrada a las variablesd globales
				int iPosicion = 0;
				Serrores.AjustarRelojCliente(nConnet);
				BListado.vfHistoriaClinica = HistoriaClinica;
				Serrores.vfMostrandoHistoricos = MostrarHistoricos;
				BListado.vBMostrarTodos = MostrarTodos;
				BListado.vSTablaMostrarTodos = TablaMostrarTodos;
				BListado.bEpisodiodeAlta = TablaMostrarTodos == "EFARMALT";

				BListado.rcConexion = nConnet;

				BListado.GstGidenpac = gidenpac;

				BListado.Listado_general = ObjetoCrystal;

				BListado.gUsuario = stUsuario;

				BListado.vstPathAplicacion = PathAplicacion;
				BListado.vstNombreBaseDatos = BaseDatos;
				BListado.vstDSN = DSN;
				//hay tres posibilidades de llamada
				//si no viene itiposer hay que buscar con Gidenpac en todas las tablas
				//si viene itiposer
				//puede que venga episodio
				//no viene episodio
				if (stiTipoSer_optional != Type.Missing)
				{
					//si viene itiposer
					BListado.GstTiposervicio = stiTipoSer;
					if (Episodio_optional != Type.Missing)
					{
						//si se han transferido datos
						//GstTiposervicio = Mid$(Episodio, 1, 1)
						BListado.GiAñoRegistro = Convert.ToInt32(Double.Parse(Episodio.Substring(1, Math.Min(4, Episodio.Length - 1)))).ToString();
						BListado.GiNumeroRegistro = Convert.ToInt32(Double.Parse(Episodio.Substring(5, Math.Min(7, Episodio.Length - 5)))).ToString();
					}
					else
					{
						//si no se pasa el episodio se supone que son las consultas
						BListado.GiAñoRegistro = "0";
						BListado.GiNumeroRegistro = "0";
					}
				}
				else
				{
					BListado.GstTiposervicio = "";
				}

				if (UsuarioCrystal_optional != Type.Missing)
				{
					//siempre se tiene que tranferir el usuario y la contraseña
					if (UsuarioCrystal.Trim() != "")
					{
						iPosicion = (UsuarioCrystal.IndexOf('/') + 1);
						BListado.vstUsuarioCrystal = UsuarioCrystal.Substring(0, Math.Min(iPosicion - 1, UsuarioCrystal.Length));
						BListado.vstContraseñaCrystal = UsuarioCrystal.Substring(iPosicion);
					}
				}


				BListado.GstFechaConsulta = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

				Serrores.Obtener_TipoBD_FormatoFechaBD(ref BListado.rcConexion);

				Serrores.ObtenerNombreAplicacion(BListado.rcConexion);
				Serrores.AjustarRelojCliente(BListado.rcConexion);
				BListado.vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(BListado.rcConexion);
				BListado.stDecimalBD = Serrores.ObtenerSeparadorDecimalBD(BListado.rcConexion);

				//llamamos al procedimeiento que genera todo el proceso
				BListado.GestionListado();
			}
			finally
			{
				UsuarioCrystal_optional = UsuarioCrystal;
			}
		}

		public void ProLoad(SqlConnection nConnet, string gidenpac, string stUsuario, object ObjetoCrystal, string PathAplicacion, string DSN, string BaseDatos, object stiTipoSer_optional, object Episodio_optional, ref object UsuarioCrystal_optional, bool HistoriaClinica, bool MostrarHistoricos, bool MostrarTodos)
		{
			ProLoad(nConnet, gidenpac, stUsuario, ObjetoCrystal, PathAplicacion, DSN, BaseDatos, stiTipoSer_optional, Episodio_optional, ref UsuarioCrystal_optional, HistoriaClinica, MostrarHistoricos, MostrarTodos, "EFARMACO");
		}

		public void ProLoad(SqlConnection nConnet, string gidenpac, string stUsuario, object ObjetoCrystal, string PathAplicacion, string DSN, string BaseDatos, object stiTipoSer_optional, object Episodio_optional, ref object UsuarioCrystal_optional, bool HistoriaClinica, bool MostrarHistoricos)
		{
			ProLoad(nConnet, gidenpac, stUsuario, ObjetoCrystal, PathAplicacion, DSN, BaseDatos, stiTipoSer_optional, Episodio_optional, ref UsuarioCrystal_optional, HistoriaClinica, MostrarHistoricos, false, "EFARMACO");
		}

		public void ProLoad(SqlConnection nConnet, string gidenpac, string stUsuario, object ObjetoCrystal, string PathAplicacion, string DSN, string BaseDatos, object stiTipoSer_optional, object Episodio_optional, ref object UsuarioCrystal_optional, bool HistoriaClinica)
		{
			ProLoad(nConnet, gidenpac, stUsuario, ObjetoCrystal, PathAplicacion, DSN, BaseDatos, stiTipoSer_optional, Episodio_optional, ref UsuarioCrystal_optional, HistoriaClinica, false, false, "EFARMACO");
		}

		public void ProLoad(SqlConnection nConnet, string gidenpac, string stUsuario, object ObjetoCrystal, string PathAplicacion, string DSN, string BaseDatos, object stiTipoSer_optional, object Episodio_optional, ref object UsuarioCrystal_optional)
		{
			ProLoad(nConnet, gidenpac, stUsuario, ObjetoCrystal, PathAplicacion, DSN, BaseDatos, stiTipoSer_optional, Episodio_optional, ref UsuarioCrystal_optional, false, false, false, "EFARMACO");
		}

		public void ProLoad(SqlConnection nConnet, string gidenpac, string stUsuario, object ObjetoCrystal, string PathAplicacion, string DSN, string BaseDatos, object stiTipoSer_optional, object Episodio_optional)
		{
			object tempRefParam = Type.Missing;
			ProLoad(nConnet, gidenpac, stUsuario, ObjetoCrystal, PathAplicacion, DSN, BaseDatos, stiTipoSer_optional, Episodio_optional, ref tempRefParam, false, false, false, "EFARMACO");
		}

		public void ProLoad(SqlConnection nConnet, string gidenpac, string stUsuario, object ObjetoCrystal, string PathAplicacion, string DSN, string BaseDatos, object stiTipoSer_optional)
		{
			object tempRefParam2 = Type.Missing;
			ProLoad(nConnet, gidenpac, stUsuario, ObjetoCrystal, PathAplicacion, DSN, BaseDatos, stiTipoSer_optional, Type.Missing, ref tempRefParam2, false, false, false, "EFARMACO");
		}

		public void ProLoad(SqlConnection nConnet, string gidenpac, string stUsuario, object ObjetoCrystal, string PathAplicacion, string DSN, string BaseDatos)
		{
			object tempRefParam3 = Type.Missing;
			ProLoad(nConnet, gidenpac, stUsuario, ObjetoCrystal, PathAplicacion, DSN, BaseDatos, Type.Missing, Type.Missing, ref tempRefParam3, false, false, false, "EFARMACO");
		}
	}
}