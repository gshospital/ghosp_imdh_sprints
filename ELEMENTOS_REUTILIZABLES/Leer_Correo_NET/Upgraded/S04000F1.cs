using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Media;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace LeerCorreoDLL
{
    public partial class S04000F1
        : Telerik.WinControls.UI.RadForm
    {

        public bool LeerCorreoDirecto = false; //oscar 04/12/2003
        public S04000F1()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
        }

        private void CB_cancelar_Click(Object eventSender, EventArgs eventArgs)
        {
            DataSet cursor = null;
            string sql = String.Empty;

            //If stModo = "R" Then
            //oscar 04/12/2003
            //sql = "select * from smensusu where nmensaje= " & S06000F1.Numensaje & " and smensusu.gusuario='" & vstUsuario & " ' and FECHENVI = " & FormatFechaHMS(S06000F1.Fenvio)
            //Set cursor = RcPrincipal.OpenResultset(sql, 1, 2)
            //If Not cursor.EOF Then
            //     cursor.Edit
            //     cursor("fechlect") = CDate(Now)
            //     cursor.Update
            //End If
            
            if (!LeerCorreoDirecto)
            {                
                object tempRefParam = S06000F1.DefInstance.Fenvio;
                sql = "select * from smensusu " +
                      " where nmensaje= " + S06000F1.DefInstance.Numensaje.ToString() + " and " +
                      " amensaje= " + S06000F1.DefInstance.Anomensaje.ToString() + " and " +
                      " smensusu.gusuario='" + MLeerCorreo.vstUsuario + " ' and " +
                      " FECHENVI = " + Serrores.FormatFechaHMS(tempRefParam);                      

                //S06000F1.DefInstance.Fenvio = Convert.ToDateTime(tempRefParam);
                
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MLeerCorreo.RcPrincipal);
                cursor = new DataSet();
                tempAdapter.Fill(cursor);
                if (cursor.Tables[0].Rows.Count != 0)
                {                    
                    cursor.Tables[0].Rows[0]["fechlect"] = DateTime.Now;                    
                    string tempQuery = cursor.Tables[0].TableName;                    
                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(cursor, tempQuery);
                }
            }            
            this.Close();
        }
        
        private void S04000F1_Load(Object eventSender, EventArgs eventArgs)
        {
            SystemSounds.Beep.Play();
        }

        private void TxtAsunto_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {
            int keyascii = Strings.Asc(eventArgs.KeyChar);
            keyascii = 0;
            if (keyascii == 0)
            {
                eventArgs.Handled = true;
            }
            eventArgs.KeyChar = Convert.ToChar(keyascii);
        }

        private void TxtTextomensaje_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {            
            int keyascii = Strings.Asc(eventArgs.KeyChar);
            keyascii = 0;
            if (keyascii == 0)
            {
                eventArgs.Handled = true;
            }
            eventArgs.KeyChar = Convert.ToChar(keyascii);
        }

        private void S04000F1_Closed(Object eventSender, EventArgs eventArgs)
        {
        }
    }
}