using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace LeerCorreoDLL
{
    public class MCLeerCorreo
    {

        //Global vstUsuario As String
        //Global RcPrincipal As rdoConnection

        //oscar 04/12/2003
        //Public Sub Load(Conexion As rdoConnection, Modo As String, usuario As String, Optional bform400 As Boolean)
        public void Load(SqlConnection Conexion, string Modo, string usuario, bool bform400, object PAsunto, object PTexto)
        {
            //----------

            MLeerCorreo.RcPrincipal = Conexion;            
            Serrores.Obtener_TipoBD_FormatoFechaBD(ref MLeerCorreo.RcPrincipal);
            MLeerCorreo.stModo = Modo;
            MLeerCorreo.vstUsuario = usuario;
            if (MLeerCorreo.stModo.Trim().ToUpper() == "R")
            {
                MLeerCorreo.stCADSQL = " and irayosmen= 'S' ";
            }
            else if (MLeerCorreo.stModo.Trim().ToUpper() != "G")
            {
                MLeerCorreo.stCADSQL = "";
            }

            //UPGRADE_ISSUE: (2070) Constant App was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
            //UPGRADE_ISSUE: (2064) App property App.HelpFile was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx            
            //UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\ayuda_dlls.hlp");

            if (PAsunto != Type.Missing && PTexto != Type.Missing)
            {
                if (bform400)
                {
                    //oscar 04/12/2003
                    //S04000F1.Show 1
                    S04000F1.DefInstance.LeerCorreoDirecto = true;                    
                    S04000F1.DefInstance.TxtAsunto.Text = PAsunto + "";
                    S04000F1.DefInstance.TxtTextomensaje.Text = PTexto + "";
                    S04000F1.DefInstance.Top = 7;
                    S04000F1.DefInstance.Left = 7;
                    S04000F1.DefInstance.ShowDialog();
                    //----------                    
                }
                else
                {
                    S06000F1.DefInstance.ShowDialog();
                }
            }
            else
            {
                S06000F1.DefInstance.ShowDialog();
            }
        }
    }
}