using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace LeerCorreoDLL
{
    partial class S06000F1
    {

        #region "Upgrade Support "
        private static S06000F1 m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static S06000F1 DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new S06000F1();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "sprMensajes", "CbLeer", "CB_cancelar", "ImageList1", "sprMensajes_Sheet1" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread sprMensajes;
        public Telerik.WinControls.UI.RadButton CbLeer;
        public Telerik.WinControls.UI.RadButton CB_cancelar;
        public System.Windows.Forms.ImageList ImageList1;
        //private FarPoint.Win.Spread.SheetView sprMensajes_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S06000F1));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.sprMensajes = new UpgradeHelpers.Spread.FpSpread();
            this.CbLeer = new Telerik.WinControls.UI.RadButton();
            this.CB_cancelar = new Telerik.WinControls.UI.RadButton();
            this.ImageList1 = new System.Windows.Forms.ImageList();
            this.SuspendLayout();
            // 
            // sprMensajes
            // 
            this.sprMensajes.Location = new System.Drawing.Point(2, 0);
            this.sprMensajes.Name = "sprMensajes";
            this.sprMensajes.Size = new System.Drawing.Size(635, 254);
            this.sprMensajes.TabIndex = 0;
            //this.sprMensajes.ButtonClicked += new FarPoint.Win.Spread.EditorNotifyEventHandler(this.sprMensajes_ButtonClicked);
            //his.sprMensajes.ButtonClicked += new FarPoint.Win.Spread.EditorNotifyEventHandler(this.sprMensajes_ButtonClicked);
            //this.sprMensajes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(sprMensajes_CellClick);
            this.sprMensajes.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(sprMensajes_CellDoubleClick);
            this.sprMensajes.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.sprMensajes_CellFormatting);
            
            // 
            // CbLeer
            // 
            //this.CbLeer.BackColor = System.Drawing.SystemColors.Control;
            this.CbLeer.Cursor = System.Windows.Forms.Cursors.Default;
            //this.CbLeer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CbLeer.Location = new System.Drawing.Point(462, 258);
            this.CbLeer.Name = "CbLeer";
            this.CbLeer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbLeer.Size = new System.Drawing.Size(81, 29);
            this.CbLeer.TabIndex = 2;
            this.CbLeer.Text = "&Leer";
            //this.CbLeer.UseVisualStyleBackColor = false;
            this.CbLeer.Visible = false;
            this.CbLeer.Click += new System.EventHandler(this.CbLeer_Click);
            // 
            // CB_cancelar
            // 
            //this.CB_cancelar.BackColor = System.Drawing.SystemColors.Control;
            this.CB_cancelar.Cursor = System.Windows.Forms.Cursors.Default;
            //this.CB_cancelar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CB_cancelar.Location = new System.Drawing.Point(551, 258);
            this.CB_cancelar.Name = "CB_cancelar";
            this.CB_cancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CB_cancelar.Size = new System.Drawing.Size(81, 29);
            this.CB_cancelar.TabIndex = 1;
            this.CB_cancelar.Text = "&Salir";
            //this.CB_cancelar.UseVisualStyleBackColor = false;
            this.CB_cancelar.Click += new System.EventHandler(this.CB_cancelar_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.ImageList1.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ImageList1.ImageStream");
            this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            // 
            // S06000F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(640, 294);
            this.Controls.Add(this.sprMensajes);
            this.Controls.Add(this.CbLeer);
            this.Controls.Add(this.CB_cancelar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "S06000F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Consulta de Mensajes S06000F1";
            this.Closed += new System.EventHandler(this.S06000F1_Closed);
            this.Load += new System.EventHandler(this.S06000F1_Load);
            this.ResumeLayout(false);
        }
        #endregion
    }
}