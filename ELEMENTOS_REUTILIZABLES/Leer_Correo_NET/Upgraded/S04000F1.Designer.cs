using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace LeerCorreoDLL
{
    partial class S04000F1
    {

        #region "Upgrade Support "
        private static S04000F1 m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static S04000F1 DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new S04000F1();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "TxtAsunto", "TxtTextomensaje", "CB_cancelar", "Label1", "Label2", "Image1", "Image2" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;
        public System.Windows.Forms.PictureBox Image1;
        public System.Windows.Forms.PictureBox Image2;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S04000F1));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.TxtAsunto = new Telerik.WinControls.UI.RadTextBoxControl();
            this.TxtTextomensaje = new Telerik.WinControls.UI.RadTextBoxControl();
            this.CB_cancelar = new Telerik.WinControls.UI.RadButton();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Image1 = new System.Windows.Forms.PictureBox();
            this.Image2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsunto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTextomensaje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CB_cancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtAsunto
            // 
            this.TxtAsunto.AcceptsReturn = true;
            this.TxtAsunto.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtAsunto.Location = new System.Drawing.Point(86, 11);
            this.TxtAsunto.MaxLength = 254;
            this.TxtAsunto.Multiline = true;
            this.TxtAsunto.Name = "TxtAsunto";
            this.TxtAsunto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.TxtAsunto.RootElement.ControlBounds = new System.Drawing.Rectangle(86, 11, 125, 20);
            this.TxtAsunto.Size = new System.Drawing.Size(400, 41);
            this.TxtAsunto.TabIndex = 2;
            this.TxtAsunto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtAsunto_KeyPress);
            // 
            // TxtTextomensaje
            // 
            this.TxtTextomensaje.AcceptsReturn = true;
            this.TxtTextomensaje.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtTextomensaje.Location = new System.Drawing.Point(86, 60);
            this.TxtTextomensaje.MaxLength = 0;
            this.TxtTextomensaje.Multiline = true;
            this.TxtTextomensaje.Name = "TxtTextomensaje";
            this.TxtTextomensaje.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.TxtTextomensaje.RootElement.ControlBounds = new System.Drawing.Rectangle(86, 60, 125, 20);
            this.TxtTextomensaje.Size = new System.Drawing.Size(400, 185);
            this.TxtTextomensaje.TabIndex = 1;
            this.TxtTextomensaje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtTextomensaje_KeyPress);
            // 
            // CB_cancelar
            // 
            this.CB_cancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CB_cancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CB_cancelar.Location = new System.Drawing.Point(405, 254);
            this.CB_cancelar.Name = "CB_cancelar";
            this.CB_cancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.CB_cancelar.RootElement.ControlBounds = new System.Drawing.Rectangle(405, 254, 110, 24);
            this.CB_cancelar.Size = new System.Drawing.Size(81, 29);
            this.CB_cancelar.TabIndex = 0;
            this.CB_cancelar.Text = "&Aceptar";
            this.CB_cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CB_cancelar.Click += new System.EventHandler(this.CB_cancelar_Click);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(16, 10);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.Label1.RootElement.ControlBounds = new System.Drawing.Rectangle(16, 10, 100, 18);
            this.Label1.Size = new System.Drawing.Size(44, 18);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "Asunto:";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(2, 75);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.Label2.RootElement.ControlBounds = new System.Drawing.Rectangle(2, 75, 100, 18);
            this.Label2.Size = new System.Drawing.Size(79, 18);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "Texto Mensaje";
            // 
            // Image1
            // 
            this.Image1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(21, 99);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(32, 32);
            this.Image1.TabIndex = 5;
            this.Image1.TabStop = false;
            // 
            // Image2
            // 
            this.Image2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Image2.Image = ((System.Drawing.Image)(resources.GetObject("Image2.Image")));
            this.Image2.Location = new System.Drawing.Point(21, 25);
            this.Image2.Name = "Image2";
            this.Image2.Size = new System.Drawing.Size(32, 32);
            this.Image2.TabIndex = 6;
            this.Image2.TabStop = false;
            // 
            // S04000F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 305);
            this.Controls.Add(this.TxtAsunto);
            this.Controls.Add(this.TxtTextomensaje);
            this.Controls.Add(this.CB_cancelar);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Image1);
            this.Controls.Add(this.Image2);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "S04000F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Tiene un mensaje Nuevo";
            this.Closed += new System.EventHandler(this.S04000F1_Closed);
            this.Load += new System.EventHandler(this.S04000F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsunto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTextomensaje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CB_cancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        public Telerik.WinControls.UI.RadTextBoxControl TxtAsunto;
        public Telerik.WinControls.UI.RadTextBoxControl TxtTextomensaje;
        private Telerik.WinControls.UI.RadButton CB_cancelar;
        private Telerik.WinControls.UI.RadLabel Label1;
        private Telerik.WinControls.UI.RadLabel Label2;
    }
}