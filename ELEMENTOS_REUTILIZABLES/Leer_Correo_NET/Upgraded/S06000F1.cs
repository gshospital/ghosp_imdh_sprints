using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace LeerCorreoDLL
{
    public partial class S06000F1
        : Telerik.WinControls.UI.RadForm
    {

        bool Primera = false;
        //oscar 04/12/2003
        //Public Numensaje As Integer
        public int Numensaje = 0;
        public int Anomensaje = 0;
        //-------
        //public System.DateTime Fenvio = DateTime.FromOADate(0);
        public string Fenvio = string.Empty;
        int Rowselect = 0;
        string TextMensaje = null;



        /// indra - pario- 18/03/2016
        /// Se inicializa en 0 las colecciones en C# 
        public int Col=0;
        public int Row = 0;

        public S06000F1()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
        }


        private void CB_cancelar_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }

        private void CbLeer_Click(Object eventSender, EventArgs eventArgs)
        {
            Row = sprMensajes.CurrentCell.RowIndex;
            Col = sprMensajes.CurrentCell.ColumnIndex;
            sprMensajes_CellDoubleClick(eventSender, null);
        }
        
        private void S06000F1_Load(Object eventSender, EventArgs eventArgs)
        {
            rellena_grid();
            if (sprMensajes.MaxRows > 0)
            {
                Primera = true;                
                sprMensajes_CellClick(eventSender, null);
            }
        }

        private void rellena_grid()
        {
            //oscar 04/12/2003
            //sql = "select * from smensusu,stexmens where smensusu.gusuario='" & vstUsuario & _
            //"' and smensusu.nmensaje=stexmens.nmensaje " & stCADSQL & " order by fechenvi"
            string sql = "select * from smensusu,stexmens where " +
                         " smensusu.gusuario='" + MLeerCorreo.vstUsuario + "' and " +
                         " smensusu.nmensaje = stexmens.nmensaje " + " and " +
                         " smensusu.amensaje = stexmens.amensaje " + MLeerCorreo.stCADSQL +
                         " order by fechenvi";
            //-------
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MLeerCorreo.RcPrincipal);
            DataSet cursor = new DataSet();
            tempAdapter.Fill(cursor);
            sprMensajes.AutoScroll = true;
            sprMensajes.Row = 1;
            sprMensajes.Col = 1;
            int alto = Convert.ToInt32(sprMensajes.GetRowHeight(1) + 10);
            int ancho = Convert.ToInt32(sprMensajes.GetColWidth(1));
            sprMensajes.MaxRows = 0;

            DateTime dt = new DateTime();

            if (cursor.Tables[0].Rows.Count != 0)
            {    
                //INDRA DGMORENOG 07-03-2016 
                //Se crea Para inicializar la cantidad de columnas y los nombres de la cabecera
                //Se crea Para agregar la columna de tipo Boton y sus propiedades.
                if (sprMensajes.Columns.Count == 0)
                {                                        
                    Telerik.WinControls.UI.GridViewCommandColumn dgvCmdCol = new Telerik.WinControls.UI.GridViewCommandColumn();
                    dgvCmdCol.Name = "LeerCorreo";
                    dgvCmdCol.UseDefaultText = false;
                    dgvCmdCol.FieldName = "LeerCorreo";
                    dgvCmdCol.Image = ImageList1.Images[1];
                    sprMensajes.MasterTemplate.Columns.Add(dgvCmdCol);
                   
                    
                    sprMensajes.MaxCols = 6;
                    sprMensajes.Row = 0;
                    sprMensajes.Col = 2;
                    sprMensajes.Text = "Fecha Env�o";

                    sprMensajes.Col = 3;
                    sprMensajes.Text = "Asunto";
                    sprMensajes.Col = 4;
                    sprMensajes.Text = "Fecha Lectura";
                    sprMensajes.ReadOnly = true;

                    //DGMORENOG - AJUSTAR TAMA�O DE LAS COLUMNAS PARA MEJORAR APARIENCIA DEL GRID
                    sprMensajes.Columns[0].Width = 10;
                    sprMensajes.Columns[1].Width = 30;
                    sprMensajes.Columns[3].Width = 30;
                }
                
                foreach (DataRow iteration_row in cursor.Tables[0].Rows)
                {          
                    
                    sprMensajes.FontBold = Convert.IsDBNull(iteration_row["fechlect"]);
                    sprMensajes.MaxRows++;
                    sprMensajes.Row = sprMensajes.MaxRows;
                                                            
                    sprMensajes.Col = 2;
                    sprMensajes.Lock = true;
                    dt = Convert.ToDateTime(iteration_row["fechenvi"]);
                    sprMensajes.Text = dt.ToString("yyyy-MM-dd HH:mm:ss:fff");
                    //sprMensajes.Text = Convert.ToString(iteration_row["fechenvi"]) + "";                    
                    sprMensajes.FontBold = Convert.IsDBNull(iteration_row["fechlect"]);

                    sprMensajes.Col = 3;
                    sprMensajes.Lock = true;
                    sprMensajes.Text = Convert.ToString(iteration_row["asunto"]) + "";                    
                    sprMensajes.FontBold = Convert.IsDBNull(iteration_row["fechlect"]);

                    sprMensajes.Col = 4;
                    sprMensajes.Lock = true;
                    sprMensajes.Text = !Convert.IsDBNull(iteration_row["fechlect"]) ? Convert.ToDateTime(iteration_row["fechlect"]).ToString("yyyy-MM-dd HH:mm:ss:fff") : "";
                    sprMensajes.FontBold = Convert.IsDBNull(iteration_row["fechlect"]);

                    sprMensajes.Col = 5;
                    sprMensajes.Lock = true;
                    sprMensajes.SetColHidden(sprMensajes.Col, true);
                    sprMensajes.Text = Convert.ToString(iteration_row["nmensaje"]) + "";                    
                    sprMensajes.FontBold = Convert.IsDBNull(iteration_row["fechlect"]);

                    //oscar 04/12/2003
                    sprMensajes.Col = 6;
                    sprMensajes.Lock = true;
                    sprMensajes.SetColHidden(sprMensajes.Col, true);
                    sprMensajes.Text = Convert.ToString(iteration_row["amensaje"]) + "";                    
                    sprMensajes.FontBold = Convert.IsDBNull(iteration_row["fechlect"]);                    
                }
            }
            
            //sprMensajes.OperationMode = OperationModeRead + OperationModeRow
            //If Rowselect <> 0 Then sprMensajes.Row = Rowselect
            sprMensajes.Refresh();
        }

        private void sprMensajes_ButtonClicked(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            sprMensajes_CellDoubleClick(eventSender, eventArgs);
        }

        private void sprMensajes_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            sprMensajes.Col = Col;
            sprMensajes.Row = Row;
            if (Col == 1 && Row > 0 && Primera)            
            {
                Primera = false;
                sprMensajes_CellDoubleClick(eventSender, eventArgs);
            }
            else if (Row == 0 && Col > 1)
            {
                //proOrdenGrid(Col);
            }                       
        }

        private void sprMensajes_CellDoubleClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            string asunto = String.Empty;
            string Texto = String.Empty;
            string sql = String.Empty;
            DataSet cursor = null;

            TextMensaje = String.Empty;
            sprMensajes.Row = Row;
            Numensaje = 0;
            //oscar 04/12/2003
            Anomensaje = 0;
            //-------
            sprMensajes.Col = Col;
            if (Row > 0)
            {
                Rowselect = Row;
                sprMensajes.Col = 2;
                //Fenvio = DateTime.Parse(sprMensajes.Text);
                Fenvio = sprMensajes.Text;
                sprMensajes.Col = 5;
                Numensaje = Convert.ToInt32(Double.Parse(sprMensajes.Text));
                sprMensajes.Col = 3;
                asunto = sprMensajes.Text;
                Texto = "";
                sprMensajes.Col = Col;
                //oscar 04/12/2003
                //sql = "select * from stexmens where nmensaje=" & Numensaje
                sprMensajes.Col = 6;
                Anomensaje = Convert.ToInt32(Double.Parse(sprMensajes.Text));
                sql = "select * from stexmens where " +
                        " nmensaje=" + Numensaje.ToString() + " and " +
                        " amensaje=" + Anomensaje.ToString();
                //-------
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MLeerCorreo.RcPrincipal);
                cursor = new DataSet();
                tempAdapter.Fill(cursor);
                if (cursor.Tables[0].Rows.Count != 0)
                {
                    TextMensaje = cursor.Tables[0].Rows[0]["textomen"].ToString();
                    if (!Convert.IsDBNull(TextMensaje))
                    {
                        Texto = Convert.ToString(TextMensaje);
                    }
                    else
                    {
                        Texto = "";
                    }
                }

                S04000F1 tempLoadForm = S04000F1.DefInstance;
                //oscar 04/12/2003
                S04000F1.DefInstance.LeerCorreoDirecto = false;
                //-------                
                S04000F1.DefInstance.TxtAsunto.Text = asunto + "";
                S04000F1.DefInstance.TxtTextomensaje.Text = Texto + "";
                S04000F1.DefInstance.Top = 7;
                S04000F1.DefInstance.Left = 7;
                S04000F1.DefInstance.ShowDialog();
                rellena_grid();
            }            
        }

        private void proOrdenGrid(int iCol)
        {
            sprMensajes.Row = 1;
            sprMensajes.Col = 1;
            sprMensajes.Row2 = sprMensajes.MaxRows;
            sprMensajes.Col2 = sprMensajes.MaxCols;

            sprMensajes.SortBy = UpgradeHelpers.Spread.SortByConstants.SortByRow; //Ordeno por columna
            sprMensajes.SetSortKey(1, iCol);

            if (sprMensajes.GetSortKeyOrder(1) == UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderAscending || sprMensajes.GetSortKeyOrder(1) == UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderNone)
            {
                sprMensajes.SetSortKeyOrder(1, UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderDescending);
            }
            else if (sprMensajes.GetSortKeyOrder(1) == UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderDescending)
            {
                sprMensajes.SetSortKeyOrder(1, UpgradeHelpers.Spread.SortKeyOrderConstants.SortKeyOrderAscending);
            }
            sprMensajes.Action = UpgradeHelpers.Spread.ActionConstants.ActionSort;
        }

        private void sprMensajes_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.CellElement.ColumnInfo is GridViewCommandColumn && e.CellElement.RowInfo.Cells[3].Value == "")
            {
                ((RadButtonElement)e.CellElement.Children[0]).Image = ImageList1.Images[0];
                e.CellElement.RowInfo.Cells[1].Style.Font = new Font(sprMensajes.Font.FontFamily, sprMensajes.Font.Size, FontStyle.Bold);
                e.CellElement.RowInfo.Cells[2].Style.Font = new Font(sprMensajes.Font.FontFamily, sprMensajes.Font.Size, FontStyle.Bold);
            }
        }            

        private void S06000F1_Closed(Object eventSender, EventArgs eventArgs)
        {
        }
    }
}