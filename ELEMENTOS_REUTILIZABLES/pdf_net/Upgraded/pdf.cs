using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.VB;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Microsoft.CSharp;
using ElementosCompartidos;
using UpgradeHelpers;
using Telerik.WinControls;


namespace Pdf
{
	public class ClasePdf
	{

		private object crystal = null;
		private string Impresora = String.Empty;
		IWshRuntimeLibrary.WshNetwork obj_Wsh = null;
		SqlConnection ConexionField = null;
		private object crystalField = null;
		private bool MostrarField = false;
		private string RutaImpresoraField = String.Empty;
		private bool Existe_ImpresoraField = false;
		private bool Agregar_ImpresoraField = false;
		private string NombreImpresoraField = String.Empty;
		private string FicheTempField = String.Empty;
		private bool RetornoField = false;


		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static void Sleep(int dwMilliseconds);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("shell32.dll", EntryPoint = "ShellExecuteA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int ShellExecute(int hWnd, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpOperation, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpFile, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpParameters, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpDirectory, int nShowCmd);
		private void Agregar_Impresora(string Path)
		{
			IWshRuntimeLibrary.WshNetwork obj_Wsh = null;

			try
			{
				// Nuevo objeto WshNetwork
				obj_Wsh = new IWshRuntimeLibrary.WshNetwork();

				// agrega la impresora de red
				// obj_Wsh.AddWindowsPrinterConnection Path, nombrecontrolador
				//Ejecutar los archivos de RutaImpresora

				ComprobarSiExiste(Path, "gs860w32.exe");
				if (!RetornoField)
				{
					return;
				}
				ComprobarSiExiste(Path, "BullzipPDFPrinter.exe");
				if (!RetornoField)
				{
					return;
				}
				int tempRefParam = 4033;
				MensajeError(ref tempRefParam, ref RutaImpresoraField);

				string tempRefParam2 = "Open";
				string tempRefParam3 = RutaImpresoraField + "gs860w32.exe";
				string tempRefParam4 = null;
				string tempRefParam5 = null;
                UpgradeSupportHelper.PInvoke.SafeNative.shell32.ShellExecute(IntPtr.Zero, ref tempRefParam2, ref tempRefParam3, ref tempRefParam4, ref tempRefParam5, 1);
                
				if (RadMessageBox.Show("�Desea continuar con la instalaci�n?, si no lo hace no se generara el PDF", "Atencion", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
				{
					RetornoField = false;
					Agregar_ImpresoraField = false;
					return;
				}
				string tempRefParam6 = "Open";
				string tempRefParam7 = RutaImpresoraField + "BullzipPDFPrinter.exe";
				string tempRefParam8 = null;
				string tempRefParam9 = null;
                UpgradeSupportHelper.PInvoke.SafeNative.shell32.ShellExecute(IntPtr.Zero, ref tempRefParam6, ref tempRefParam7, ref tempRefParam8, ref tempRefParam9, 0);
				if (RadMessageBox.Show("�Desea continuar con la instalaci�n?, si no lo hace no se generara el PDF", "Atencion", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
				{
					RetornoField = false;
					Agregar_ImpresoraField = false;
					return;
				}
                //camaya todo_x_5: no esta el.exe de BullzipPDFPrinter
                //Bullzip.PDFPrinterSettings obullzip = null;
                //obullzip = new Bullzip.PDFPrinterSettings();
                //obullzip.SetValue("Output", FicheTempField);
                //obullzip.SetValue("ConfirmOverwrite", "no");
                //obullzip.SetValue("ShowSaveAS", "never");
                //obullzip.SetValue("ShowSettings", "never");
                //obullzip.SetValue("ShowPDF", "no");
                //obullzip.WriteSettings(true);
                //obullzip.LoadSettings(true);
                //obullzip = null;
                // fin camaya todo_x_5

                // Verifica si hubo error				
                Agregar_ImpresoraField = Information.Err().Number == 0;
				obj_Wsh = null;
			}
			catch (System.Exception excep)
			{
				RetornoField = false;
				//MensajeError 4028, RutaImpresoraField				
				RadMessageBox.Show(Information.Err().Number.ToString() + " - " + excep.Message, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
			}
		}
		public void ComprobarSiExiste(string Ruta, string Fichero)
		{
			string Unidad = String.Empty;
            FileInfo fso;
            
			IWshRuntimeLibrary.WshNetwork oNet = new IWshRuntimeLibrary.WshNetwork();

			Ruta = Ruta.Substring(0, Math.Min(Ruta.Length - 1, Ruta.Length));
			try
			{
                fso = new FileInfo("J:");
                if (!fso.Directory.Exists)
				{
					Unidad = "J:";
				}
                fso = new FileInfo("K:");
                if (!fso.Directory.Exists)
				{
					Unidad = "K:";
				}
                fso = new FileInfo("L:");
                if (!fso.Directory.Exists)
				{
					Unidad = "L:";
				}
                fso = new FileInfo("M:");
                if (!fso.Directory.Exists)
				{
					Unidad = "M:";
				}
                fso = new FileInfo("N:");
                if (!fso.Directory.Exists)
				{
					Unidad = "N:";
				}
                fso = new FileInfo("O:");
                if (!fso.Directory.Exists)
				{
					Unidad = "O:";
				}
                fso = new FileInfo("P:");
                if (!fso.Directory.Exists)
				{
					Unidad = "P:";
				}


				object tempRefParam = Type.Missing;
				object tempRefParam2 = Type.Missing;
				object tempRefParam3 = Type.Missing;
				oNet.MapNetworkDrive(Unidad, Ruta, ref tempRefParam, ref tempRefParam2, ref tempRefParam3);
                fso = new FileInfo((Unidad + "\\" + Fichero).Trim());
                if (!fso.Exists)
				{
					int tempRefParam4 = 4034;
					string tempRefParam5 = Ruta + Fichero;
					MensajeError(ref tempRefParam4, ref tempRefParam5);
					RetornoField = false;
					return;
				}


				object tempRefParam6 = Type.Missing;
				object tempRefParam7 = Type.Missing;
				oNet.RemoveNetworkDrive(Unidad, ref tempRefParam6, ref tempRefParam7);
			}
			catch
			{
				fso = null;
				oNet = null;
				object tempRefParam8 = Type.Missing;
				object tempRefParam9 = Type.Missing;
				oNet.RemoveNetworkDrive(Unidad, ref tempRefParam8, ref tempRefParam9);
				int tempRefParam10 = 4034;
				string tempRefParam11 = Ruta + Fichero;
				MensajeError(ref tempRefParam10, ref tempRefParam11);
				RetornoField = false;
			}
		}
		public bool GenerPdf(string itipinf)
		{
			//Oscar C Abril 2010
			//Ahora en funcion del informe que se desee generar PFD, se utilizan constantes diferentes.
			//AM--> Vendra desde la dll InformesMedicos.dll
			//AU--> Vendra desde la dll AltasUrg.dll (Visualizar/generar Informe)
			//EN--> Vendra desde la dll InrormesEnfermeria.dll
			//RX--> Vendra desde Radiodiagnostico, Informe Radiologico
			//Este parametro no vendra cuando se acceda desde el ejecutable que genera PDFs
			//a partir de los informes de enfermeria no generados por lo que si no viene, se supone que se desean
			//generar PDFs de informes de enfermeria y se sigue manteniendo la misma funcionalidad que siempre.

			//'Dim sqlTemporal As String
			//'Dim Rs As rdoResultset
			//'GenerPdf = False
			//'sqlTemporal = "select valfanu1 from sconsglo where gconsglo='GENERPDF'"
			//'Set Rs = ConexionField.OpenResultset(sqlTemporal, rdOpenKeyset, 1)
			//'If Rs.RowCount > 0 Then
			//'    If Trim(Rs("valfanu1")) = "N" Then GenerPdf = False
			//'    If Trim(Rs("valfanu1")) = "S" Then GenerPdf = True
			//'Else
			//'GenerPdf = False
			//'End If
			bool result = false;
			string sqlTemporal = String.Empty;
			switch(itipinf)
			{
				case "AM" : 
					sqlTemporal = "select valfanu1 from sconsglo where gconsglo='GENPDFAH'"; 
					break;
				case "AU" : 
					sqlTemporal = "select valfanu1 from sconsglo where gconsglo='GENPDFAU'"; 
					break;
				case "ENF" : 
					sqlTemporal = "select valfanu1 from sconsglo where gconsglo='GENERPDF'"; 
					break;
				case "RX" : 
					sqlTemporal = "select valfanu1 from sconsglo where gconsglo='GENPDFRX'"; 
					break;
				default:
					sqlTemporal = "select valfanu1 from sconsglo where gconsglo='GENERPDF'"; 
					break;
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTemporal, ConexionField);
			DataSet Rs = new DataSet();
			tempAdapter.Fill(Rs);
			if (Rs.Tables[0].Rows.Count > 0)
			{				
				if (Convert.ToString(Rs.Tables[0].Rows[0]["valfanu1"]).Trim() == "N")
				{
					result = false;
				}				
				if (Convert.ToString(Rs.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
				{
					result = true;
				}
			}
			else
			{
				result = false;
			}
			return result;
		}

		public void DameNombreImpresora(string gnombdoc)
		{
			string sqlTemporal = String.Empty;
			DataSet Rs = null;
			try
			{
				sqlTemporal = "select valfanu1,valfanu2,valfanu3 from sconsglo where gconsglo='IMPREPDF'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTemporal, ConexionField);
				Rs = new DataSet();
				tempAdapter.Fill(Rs);
				if (Rs.Tables[0].Rows.Count > 0)
				{					
					RutaImpresoraField = Convert.ToString(Rs.Tables[0].Rows[0]["valfanu1"]).Trim();					
					NombreImpresoraField = Convert.ToString(Rs.Tables[0].Rows[0]["valfanu2"]).Trim();					
					FicheTempField = Convert.ToString(Rs.Tables[0].Rows[0]["valfanu3"]).Trim() + gnombdoc + ".pdf";
					RetornoField = true;
				}
				else
				{
					RutaImpresoraField = "";
					NombreImpresoraField = "";
					FicheTempField = "";
					RetornoField = false;
					throw new Exception();
				}
				if (RutaImpresoraField == "" || NombreImpresoraField == "" || FicheTempField == "")
				{
					RetornoField = false;
				}
			}
			catch
			{
				RetornoField = false;				
				int tempRefParam = Information.Err().Number;
				string tempRefParam2 = "Error en la variable IMPREPDF";
				MensajeError(ref tempRefParam, ref tempRefParam2);
			}

		}

		private void Existe_Impresora(string gnombdoc)
		{
			Existe_ImpresoraField = false;

			try
			{
				DameNombreImpresora(gnombdoc);
				if (RutaImpresoraField == "")
				{
					Existe_ImpresoraField = false;					
					return ;
				}

				IWshRuntimeLibrary.WshNetwork obj_Wsh = null;

				// Nuevo objeto WshNetwork
				obj_Wsh = new IWshRuntimeLibrary.WshNetwork();
				for (int i = 1; i <= obj_Wsh.EnumPrinterConnections().Count() - 1; i += 2)
				{					
					object tempRefParam = i;
					if (Convert.ToString(obj_Wsh.EnumPrinterConnections().Item(ref tempRefParam)).ToUpper().IndexOf(NombreImpresoraField.ToUpper()) >= 0)
					{
						i = Convert.ToInt32(tempRefParam);
						Existe_ImpresoraField = true;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam);
					}
				}
				if (!Existe_ImpresoraField)
				{
					Agregar_Impresora(RutaImpresoraField);
					if (!RetornoField)
					{
						return;
					}
					if (!Agregar_ImpresoraField)
					{
						RetornoField = false;
						int tempRefParam2 = 4028;
						MensajeError(ref tempRefParam2, ref RutaImpresoraField);
						return;
					}
				}



				//Elimina la referencia
				obj_Wsh = null;
			}
			catch
			{
				Existe_ImpresoraField = false;
				RetornoField = false;
				int tempRefParam3 = 4029;
				MensajeError(ref tempRefParam3, ref RutaImpresoraField);
				//Elimina la referencia
			}


		}

		private void ImprimirCrystalPDF(dynamic crystal)
		{
			//Seleccionar la impresora como predeterminada
			bool Cambiado = false;
			string defecto = String.Empty;
			try
			{
				defecto = PrinterHelper.Printer.DeviceName;
				obj_Wsh = new IWshRuntimeLibrary.WshNetwork();
				foreach (PrinterHelper tPrinter2 in PrinterHelper.Printers)
				{
					if (PrinterHelper.Printer.DeviceName.IndexOf(NombreImpresoraField) >= 0)
					{

						Cambiado = true;
						obj_Wsh.SetDefaultPrinter(PrinterHelper.Printer.DeviceName);
						break;
					}
				}
                //crystal.documentname = GuardarComo
                //GuardarComo = crystal
                //camaya todo_x_5 no esta el.exe de BullzipPDFPrinter
                //Bullzip.PDFPrinterSettings obullzip = null;
                //obullzip = new Bullzip.PDFPrinterSettings();
                //obullzip.SetValue("Output", FicheTempField);
                //obullzip.SetValue("ConfirmOverwrite", "no");
                //obullzip.SetValue("ShowSaveAS", "never");
                //obullzip.SetValue("ShowSettings", "never");
                //obullzip.SetValue("ShowPDF", "no");
                //obullzip.WriteSettings(true);
                //obullzip.LoadSettings(true);
                //obullzip = null;
                //fin camaya todo_x_5

                //camaya todo_x_5
                //crystal.Destination = (int) Crystal.DestinationConstants.crptToPrinter;				
                //crystal.WindowShowPrintSetupBtn = false;				
				crystal.Destination = 1;				
				crystal.Action = 1;

				if (Cambiado)
				{
					obj_Wsh.SetDefaultPrinter(defecto);
				}
				obj_Wsh = null;
			}
			catch
			{
				RetornoField = false;
				if (Cambiado)
				{
					obj_Wsh.SetDefaultPrinter(defecto);
				}
				obj_Wsh = null;
			}
		}


		public bool Pdf(SqlConnection Conexion, object crystal, ref string gnombdoc, System.DateTime fechaalta, string ddescrip, string gusuario, string itipinf, bool Borrar)
		{
			ConexionField = Conexion;
			System.DateTime Inicio = DateTime.FromOADate(0);
            FileInfo Obj_Fso;
			bool Existe = false;
			
			//camaya todo_x_5
			//UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Ayuda_DLLS.hlp");
            
            bool Mostrar = false;
			bool Guardar = true;
			try
			{
				if (!GenerPdf(itipinf))
				{
					return false;
				}
				crystalField = crystal;
				MostrarField = Mostrar;
				Existe_Impresora(gnombdoc);
				if (!RetornoField)
				{
					throw new Exception();
				}
                Obj_Fso = new FileInfo(FicheTempField.Trim());
				if (Obj_Fso.Exists)
				{
                    Obj_Fso.Delete();
				}
				if (Existe_ImpresoraField || (!Existe_ImpresoraField && Agregar_ImpresoraField))
				{
					ImprimirCrystalPDF(crystal);
					if (!RetornoField)
					{
						throw new Exception();
					}
				}
				else
				{
					RetornoField = false;
					int tempRefParam = 4029;
					MensajeError(ref tempRefParam, ref RutaImpresoraField);
					if (!RetornoField)
					{
						throw new Exception();
					}
				}
				Inicio = DateTime.Now;
				while (!Existe && (Math.Abs((int) DateAndTime.DateDiff("s", DateTime.Now, Inicio, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) < 15))
				{
                    Obj_Fso = new FileInfo(FicheTempField.Trim());
                    Existe = Obj_Fso.Exists;
					Application.DoEvents();
				}

				if (!Existe)
				{
					RetornoField = false;
					int tempRefParam2 = 4031;
					MensajeError(ref tempRefParam2, ref gnombdoc);
					if (!RetornoField)
					{
						throw new Exception();
					}
				}
				else
				{
					if (Guardar && Existe)
					{
						GuardaPdf(ref gnombdoc, ddescrip, fechaalta, gusuario);
						if (!RetornoField)
						{
							throw new Exception();
						}
					}
					if (Mostrar && Existe)
					{
						MuestraPdf();
						if (!RetornoField)
						{
							throw new Exception();
						}
					}
				}
                Obj_Fso = new FileInfo(FicheTempField.Trim());
				if (Obj_Fso.Exists && Borrar)
				{
                    Obj_Fso.Delete();
				}
				Obj_Fso = null;
				obj_Wsh = null;
				return RetornoField;
			}
			catch
			{
                Obj_Fso = new FileInfo(FicheTempField.Trim());
                if (Obj_Fso.Exists)
				{
                    Obj_Fso.Delete();
				}
				Obj_Fso = null;
				obj_Wsh = null;
				RetornoField = false;
				return RetornoField;
				//  MensajeError 4031, gnombdoc
			}
		}

		public bool Pdf(SqlConnection Conexion, object crystal, ref string gnombdoc, System.DateTime fechaalta, string ddescrip, string gusuario, string itipinf)
		{
			return Pdf(Conexion, crystal, ref gnombdoc, fechaalta, ddescrip, gusuario, itipinf, true);
		}

		public bool Pdf(SqlConnection Conexion, object crystal, ref string gnombdoc, System.DateTime fechaalta, string ddescrip, string gusuario)
		{
			return Pdf(Conexion, crystal, ref gnombdoc, fechaalta, ddescrip, gusuario, String.Empty, true);
		}

		public bool Pdf(SqlConnection Conexion, object crystal, ref string gnombdoc, System.DateTime fechaalta, string ddescrip)
		{
			return Pdf(Conexion, crystal, ref gnombdoc, fechaalta, ddescrip, String.Empty, String.Empty, true);
		}

		public bool Pdf(SqlConnection Conexion, object crystal, ref string gnombdoc, System.DateTime fechaalta)
		{
			return Pdf(Conexion, crystal, ref gnombdoc, fechaalta, String.Empty, String.Empty, String.Empty, true);
		}

		public bool Pdf(SqlConnection Conexion, object crystal, ref string gnombdoc)
		{
			return Pdf(Conexion, crystal, ref gnombdoc, DateTime.FromOADate(0), String.Empty, String.Empty, String.Empty, true);
		}

		public bool Pdf(SqlConnection Conexion, object crystal)
		{
			string tempRefParam5 = String.Empty;
			return Pdf(Conexion, crystal, ref tempRefParam5, DateTime.FromOADate(0), String.Empty, String.Empty, String.Empty, true);
		}
		public object fPasaBinario(string strFichero)
		{
			bool error = false;
			object result = null;
			int intFichero = 0;
			int lngLongitud = 0; //Variable a almacenar en Fichero

			try
			{
				error = true;
				//lblTextos(2).Caption = "Proceso de conversi�n"
				//lblTextos(2).Refresh

				intFichero = FileSystem.FreeFile();
                UpgradeSupportHelper.PInvoke.SafeNative.kernel32.Sleep(500);				
				FileSystem.FileOpen(intFichero, strFichero, OpenMode.Binary, OpenAccess.Default, OpenShare.LockRead, -1);


				lngLongitud = (int) FileSystem.LOF(intFichero);
				byte[] bytFichero = new byte[lngLongitud + 2];				
				Array TempArray = Array.CreateInstance(bytFichero.GetType().GetElementType(), bytFichero.Length);
				FileSystem.FileGet(intFichero, ref TempArray, -1, false, false);
				Array.Copy(TempArray, bytFichero, TempArray.Length);

				FileSystem.FileClose(intFichero);
				result = bytFichero;

				//'If lngLongitud > 0 Then
				//'    ProgressBar1.Min = 0
				//'    ProgressBar1.Max = lngLongitud
				//'    ProgressBar1.Visible = True
				//'End If
				//''txtDescripcion.Visible = False
				//'
				//'For lngContador = 0 To lngLongitud
				//'    If lngLongitud > 0 Then
				//'        ProgressBar1.Value = lngContador
				//'    End If
				//'    Get #intFichero, , bytFichero(lngContador)
				//'Next
				//'
				//'ProgressBar1.Visible = False
				//''txtDescripcion.Visible = True
				//lblTextos(2).Caption = "Descripci�n:"
				//lblTextos(2).Refresh

				error = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!error)
				{
					throw excep;
				}

				if (error)
				{

					RadMessageBox.Show(excep.Message, "Error al descomponer el fichero " + strFichero, MessageBoxButtons.OK, RadMessageIcon.Info);


					return result;
				}
			}
			return null;
		}



		private void GuardaPdf(ref string gnombdoc, string ddescrip, System.DateTime fechaalta, string gusuario)
		{
			string Sql = String.Empty;
			DataSet Rs = null;
			try
			{
				Sql = "Select * from ddocupdf where gnombdoc='" + gnombdoc + "' and inumdocu=1";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ConexionField);
				Rs = new DataSet();
				tempAdapter.Fill(Rs);

				if (Rs.Tables[0].Rows.Count == 0)
				{					
					Rs.AddNew();
					
					Rs.Tables[0].Rows[0]["gnombdoc"] = gnombdoc;
					
					Rs.Tables[0].Rows[0]["inumdocu"] = 1;
					
					Rs.Tables[0].Rows[0]["ddescrip"] = ddescrip;
					
					Rs.Tables[0].Rows[0]["fechalta"] = fechaalta;					
					
					Rs.Tables[0].Rows[0]["odocupdf"] = fPasaBinario(FicheTempField);
				
					Rs.Tables[0].Rows[0]["dnombpdf"] = gnombdoc + ".pdf";
				
					Rs.Tables[0].Rows[0]["gusuario"] = gusuario;					
								
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(Rs, Rs.Tables[0].TableName);
				}
				else
				{
					
					Rs.Edit();				
					Rs.Tables[0].Rows[0]["odocupdf"] = fPasaBinario(FicheTempField);					
					Rs.Tables[0].Rows[0]["fechalta"] = fechaalta;					
					Rs.Tables[0].Rows[0]["gusuario"] = gusuario;					
					
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(Rs, Rs.Tables[0].TableName);
				}
				Rs = null;
			}
			catch
			{

				RetornoField = false;
				int tempRefParam = 4035;
				MensajeError(ref tempRefParam, ref gnombdoc);
			}
		}
		public void MuestraPdf()
		{
			string tempRefParam = "Open";
			string tempRefParam2 = null;
			string tempRefParam3 = null;           
            UpgradeSupportHelper.PInvoke.SafeNative.shell32.ShellExecute(IntPtr.Zero, ref tempRefParam, ref FicheTempField, ref tempRefParam2, ref tempRefParam3, 0);
            
		}

		private void MensajeError(ref int error, ref string Mensaje)
		{
			Mensajes.ClassMensajes oClassErrores = new Mensajes.ClassMensajes();		
			
			string tempRefParam = AssemblyHelper.GetTitle(System.Reflection.Assembly.GetExecutingAssembly());
            string tempRefParam2 = CDAyuda.Instance.getHelpFile();
			short tempRefParam3 = (short) error;
			string[] tempRefParam4 = new string[] { Mensaje};
			oClassErrores.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, ConexionField, tempRefParam4);
			error = tempRefParam3;
		}
	}
}