using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace FacturacionQuirofanos
{
	public class clsFacturacion
	{

		string stSql = String.Empty;
		DataSet rsIntervencion = null;
		DataSet rsFactura = null;
		DataSet rsPrivado = null;
		string OrigenQuirurgico = String.Empty;
		string FechaSistema = String.Empty;
		public string FechaPerfact = String.Empty;
		string FechaPer2 = String.Empty;
		string sRegiqui = String.Empty;
		// indicador de si la llamada a la DLL es en modo consulta o no
		public bool bTemp = false;
		public bool GestionLotes = false;


		DataSet rsProgramacion = null;
		DataSet rsIngreso = null;
		DataSet rsProtesis = null;
		DataSet rsMaterial = null;
		DataSet rsDcodpres = null;
		string stSql2 = String.Empty;
		string tStSS = String.Empty;
		DataSet tRrSS = null;
		int iFarmacia = 0;
		int iAlmacen = 0;
		bool ExisteProtocolo = false;
		string sql = String.Empty;
		bool Continua_Bucle = false;
		string PrestacionFactura = String.Empty;
		DataSet rsDanalsol = null;
		string stsqllot = String.Empty;

		//OSCAR C
		string iquirofa = String.Empty;
		bool bAñadirHM = false;
		bool bAñadirAY = false;
		//----------

		//OSCAR C 15/07/2005
		bool bCapturarResponsable = false;
		//-------
		string stTablaFacturacion = String.Empty;
		string stTablaFacturacion_W = String.Empty;
		//Dim bEncontrado As Boolean
		DataSet RsAux = null;

		//OSCAR C Abril 2009
		//Estas variables que venian como parametro en la llamada GenerarMovimientosFacturacion,
		//se hacen visibles a nivel de modulo para que los diferentes procedimientos  puedan serguir consultando su valor.
		int AñoRegistro = 0;
		int NumeroRegistro = 0;
		SqlConnection Conexion = null;
		//Dim FechaFact As String
		//Dim TablaFacturacion As Variant
		//-----


		//************************************************************************************************************
		//*                                                                                                          *
		//*  Nombre: GenerarMovimientosFacturacion                                                                   *
		//*                                                                                                          *
		//*  Modificaciones:                                                                                         *
		//*                                                                                                          *
		//*      O.Frias (12/01/2007) Realizo la inserción de los nuevos datos ( gcomadr2, gpediat1 y gpediat2 )     *
		//*                                                                                                          *
		//************************************************************************************************************
		public void GenerarMovimientosFacturacion(int pAñoRegistro, int pNumeroRegistro, SqlConnection pConexion, dynamic Formulario, ref int lErr, ref string stErr, string FechaFact, object Consulta_optional, object TablaFacturacion_optional)
		{
			string TablaFacturacion = (TablaFacturacion_optional == Type.Missing) ? String.Empty : TablaFacturacion_optional as string;
			object Consulta = (Consulta_optional == Type.Missing) ? null : Consulta_optional as object;
			//Dim rsProgramacion As rdoResultset
			//Dim rsIngreso As rdoResultset
			//Dim rsProtesis As rdoResultset
			//Dim rsMaterial As rdoResultset
			//Dim rsDcodpres As rdoResultset
			//Dim stSql2 As String
			//Dim tStSS As String
			//Dim tRrSS As rdoResultset
			//Dim iFarmacia As Integer
			//Dim iAlmacen As Integer
			//Dim ExisteProtocolo As Boolean
			//Dim sql As String
			//Dim Continua_Bucle As Boolean
			//Dim PrestacionFactura As String
			//Dim rsDanalsol As rdoResultset
			//Dim stsqllot As String
			//
			//'OSCAR C
			//Dim iquirofa As String
			//Dim bAñadirHM As Boolean
			//Dim bAñadirAY As Boolean
			//'----------
			//
			//'OSCAR C 15/07/2005
			//Dim bCapturarResponsable As Boolean
			//'-------
			//Dim stTablaFacturacion As String
			//Dim stTablaFacturacion_W As String
			//Dim bEncontrado As Boolean
			//Dim RsAux As rdoResultset
			//------
			try
			{
				lErr = 0;
				stErr = "";
				AñoRegistro = pAñoRegistro;
				NumeroRegistro = pNumeroRegistro;
				Conexion = pConexion;
				//TablaFacturacion = TablaFacturacion


				Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
				GestionLotes = gestionporlotes(Conexion);


				//OSCAR C Abril 2009
				// Si no existe la tabla temporal correspondiente al episodio de quirofanos a facturar, se debera crear dicha tabla.
				// Si ya existiera, significara que se ha cargada previamente en la funcion GENERADO de esta dll.

				stTablaFacturacion_W = "W_DMOVFACT_Q" + Conversion.Str(AñoRegistro).Trim() + Conversion.Str(NumeroRegistro).Trim();

				stSql = "SELECT '' FROM sysobjects where name='" + stTablaFacturacion_W + "' and type='U'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
				rsPrivado = new DataSet();
				tempAdapter.Fill(rsPrivado);
				if (rsPrivado.Tables[0].Rows.Count == 0)
				{
					proCrearTablaFacturacion(stTablaFacturacion_W, Conexion);
				}
				rsPrivado.Close();
				//-----------------------

				if (TablaFacturacion_optional == Type.Missing)
				{
					stTablaFacturacion = "DMOVFACT";
				}
				else
				{
					stTablaFacturacion = TablaFacturacion;
				}

				if (FechaFact == "")
				{
					FechaPerfact = "";
				}
				else
				{
					FechaPerfact = FechaFact;
				}

				if (Consulta_optional == Type.Missing)
				{
					// llaman a la facturación definitiva
					bTemp = false;
				}
				else
				{
					bTemp = true;
				}


				FechaSistema = DateTimeHelper.ToString(DateTime.Now);

				// Se obtiene el codigo de financiadora correspondiente a privado
				stSql = "SELECT nnumeri1 " + 
				        "FROM sconsglo " + 
				        "WHERE gconsglo = 'PRIVADO'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion);
				rsPrivado = new DataSet();
				tempAdapter_2.Fill(rsPrivado);

				stSql = "SELECT qintquir.* " + 
				        "FROM qintquir " + 
				        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
				        "gnumregi = " + NumeroRegistro.ToString();
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
				rsIntervencion = new DataSet();
				tempAdapter_3.Fill(rsIntervencion);
				if (rsIntervencion.Tables[0].Rows.Count != 0)
				{

					proFacturacionProcedimientoPostOperatorio(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"

					proFacturacionHonorarioMedico(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"

					proFacturacionDerechosQuirofano(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"

					proFacturacionAyudantesIntervencion(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"

					proFacturacionAnestesistayAyudante(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"

					proFacturacionComadronayAyudante(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"

					proFacturacionPediatrayAyudante(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"

					proFacturacionMonitorista();

					rsIntervencion.Edit();
					rsIntervencion.Tables[0].Rows[0]["fpasfact"] = FechaSistema;
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
                    tempAdapter_3.Update(rsIntervencion, rsIntervencion.Tables[0].TableName);


					// BUSCO EL SERVICIO DE FARMACIA Y ALMACEN PARA GRABAR LUEGO EN DMOVFACT
					tStSS = "select nnumeri1, nnumeri3 from sconsglo where gconsglo = 'SERVREAL'";
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tStSS, Conexion);
					tRrSS = new DataSet();
					tempAdapter_5.Fill(tRrSS);
					iFarmacia = Convert.ToInt32(tRrSS.Tables[0].Rows[0]["nnumeri1"]);
					iAlmacen = Convert.ToInt32(tRrSS.Tables[0].Rows[0]["nnumeri3"]);
					tRrSS.Close();


					//If GestionLotes Then
					//    stsqllot = ",gcodlote "
					//Else
					//    stsqllot = " "
					//End If

					proFacturacionProtesis(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"

					proFacturacioFarmacosyMateriales(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"


					///*****************/
					//guardo la fecha de facturacion del episodio por si no tenia
					if (FechaPerfact == "")
					{
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
						{
							FechaPerfact = Convert.ToString(rsIntervencion.Tables[0].Rows[0]["hhsalida"]);
						}
						else
						{
							FechaPerfact = Convert.ToString(rsIntervencion.Tables[0].Rows[0]["finterve"]);
						}
					}

					proFacturacionOtrasPrestacionesRealizadas(); //Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"



					//Oscar C Abril 2009
					//Se saca a un procedimiento porque daba error de compilacion "Procedimiento demasiado largo"
					proActualiza_DMOVFACT(stTablaFacturacion_W);
					//--------------

				}

				rsPrivado.Close();
				rsIntervencion.Close();

				if (Formulario != null)
				{
					Formulario.ProRecibirError(null, null);
				}
			}
			catch (System.Exception excep)
			{

				Serrores.GrabaLog(DateTimeHelper.ToString(DateTime.Now) + " GENERAR MOVIMIENTOS FACTURACION - " + Information.Err().Number.ToString() + " - " + excep.Message, Path.GetDirectoryName(Application.ExecutablePath) + "\\ErroresFacturacionQuirofanos.log");
				if (Formulario == null)
				{
                    lErr = excep.HResult;
					stErr = excep.Message;
				}
				else
				{
					Formulario.ProRecibirError(Information.Err().Number, excep.Message);
				}
			}
		}

		public void GenerarMovimientosFacturacion(int pAñoRegistro, int pNumeroRegistro, SqlConnection pConexion, object Formulario, ref int lErr, ref string stErr, string FechaFact, object Consulta_optional)
		{
			GenerarMovimientosFacturacion(pAñoRegistro, pNumeroRegistro, pConexion, Formulario, ref lErr, ref stErr, FechaFact, Consulta_optional, Type.Missing);
		}

		public void GenerarMovimientosFacturacion(int pAñoRegistro, int pNumeroRegistro, SqlConnection pConexion, object Formulario, ref int lErr, ref string stErr, string FechaFact)
		{
			GenerarMovimientosFacturacion(pAñoRegistro, pNumeroRegistro, pConexion, Formulario, ref lErr, ref stErr, FechaFact, Type.Missing, Type.Missing);
		}

		public void GenerarMovimientosFacturacion(int pAñoRegistro, int pNumeroRegistro, SqlConnection pConexion, object Formulario, ref int lErr, ref string stErr)
		{
			GenerarMovimientosFacturacion(pAñoRegistro, pNumeroRegistro, pConexion, Formulario, ref lErr, ref stErr, String.Empty, Type.Missing, Type.Missing);
		}

		public void GenerarMovimientosFacturacion(int pAñoRegistro, int pNumeroRegistro, SqlConnection pConexion, object Formulario, ref int lErr)
		{
			string tempRefParam5 = String.Empty;
			GenerarMovimientosFacturacion(pAñoRegistro, pNumeroRegistro, pConexion, Formulario, ref lErr, ref tempRefParam5, String.Empty, Type.Missing, Type.Missing);
		}

		public void GenerarMovimientosFacturacion(int pAñoRegistro, int pNumeroRegistro, SqlConnection pConexion, object Formulario)
		{
			int tempRefParam6 = 0;
			string tempRefParam7 = String.Empty;
			GenerarMovimientosFacturacion(pAñoRegistro, pNumeroRegistro, pConexion, Formulario, ref tempRefParam6, ref tempRefParam7, String.Empty, Type.Missing, Type.Missing);
		}

		public void GenerarMovimientosFacturacion(int pAñoRegistro, int pNumeroRegistro, SqlConnection pConexion)
		{
			int tempRefParam8 = 0;
			string tempRefParam9 = String.Empty;
			GenerarMovimientosFacturacion(pAñoRegistro, pNumeroRegistro, pConexion, null, ref tempRefParam8, ref tempRefParam9, String.Empty, Type.Missing, Type.Missing);
		}

		private void proFacturacioFarmacosyMateriales()
		{
			bool bEncontrado = false;
			//Facturacion de los conceptos facturables asociados al posible material utilizado en la intervencion--------OK
			if (GestionLotes)
			{
				stsqllot = ",gcodlote ";
			}
			else
			{
				stsqllot = " ";
			}
			//stSql = "SELECT qmatutil.gtipoart, qmatutil.gcodiart, qmatutil.cservida, " & _
			//'                  "qmatutil.fpasfact, 'S' " & _
			//'              "FROM qmatutil " & _
			//'              "WHERE ganoregi = " & AñoRegistro & " AND " & _
			//'                    "gnumregi = " & NumeroRegistro
			stSql = "SELECT qmatutil.gtipoart, qmatutil.gcodiart, qmatutil.cservida, " + 
			        "qmatutil.fpasfact, dtipoart.ifarmaco " + stsqllot + 
			        "FROM qmatutil, dtipoart " + 
			        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
			        "gnumregi = " + NumeroRegistro.ToString() + " AND " + 
			        "qmatutil.gtipoart = dtipoart.gtipoart";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
			rsMaterial = new DataSet();
			tempAdapter.Fill(rsMaterial);
			//, 2, 4)

			foreach (DataRow iteration_row in rsMaterial.Tables[0].Rows)
			{
				if (GestionLotes)
				{
					stsqllot = ",gcodlote ";
				}
				else
				{
					stsqllot = " ";
				}
				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "INSERT INTO dmovfacc " + 
						        "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
						        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, " + 
						        "ncantida, fsistema, ffinicio, ffechfin, ginspecc, iregiqui, ndninifp, fperfact, gcodiart" + stsqllot + 
						        ",itiporig, ganoorig, gnumorig ) " + 
						        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'," + 
						        "'I',";
					}
					else
					{
						//            StSql = "INSERT INTO dmovfact " & _
						//'                       "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " & _
						//'                        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, " & _
						//'                        "ncantida, fsistema, ffinicio, ffechfin, ginspecc, iregiqui, ndninifp, fperfact, gcodiart" & stsqllot & _
						//'                        ",itiporig, ganoorig, gnumorig ) " & _
						//'                       "VALUES('" & rsIntervencion!gidenpac & "'," & _
						//'                               "'I',"
						stSql = "INSERT INTO " + stTablaFacturacion + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
						        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, " + 
						        "ncantida, fsistema, ffinicio, ffechfin, ginspecc, iregiqui, ndninifp, fperfact, gcodiart" + stsqllot + 
						        ",itiporig, ganoorig, gnumorig ) " + 
						        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'," + 
						        "'I',";

					}
					// Si el paciente no estaba hospitalizado la prestación principal
					// se graba como externa. Si estaba hospitalizado se graba como interna
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + ",";
					}
					else
					{
						stSql = stSql + 
						        "'Q'," + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + ",";
					}
					if (Convert.ToString(iteration_row["ifarmaco"]) == "S")
					{
						stSql = stSql + 
						        "'FA',";
					}
					else
					{
						stSql = stSql + 
						        "'AL',";
					}
					// Modificación: 10/11/99 el servicio solicitante va a nulos
					// cuando es un ambulante

					//
					//      If IsNull(rsIntervencion!itiposer) Then
					//            stSql = stSql & _
					//'                                "(Null) ,"
					//      Else
					// modificacion 31/3/2000
					stSql = stSql + 
					        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
					//      End If
					if (Convert.ToString(iteration_row["ifarmaco"]) == "S")
					{
						stSql = stSql + iFarmacia.ToString() + ",";
					}
					else
					{
						stSql = stSql + iAlmacen.ToString() + ",";
					}
					stSql = stSql + 
					        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + 
						        "(Null),";
					}
					else
					{
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
					}
					//      If rsMaterial!ifarmaco = "S" Then
					stSql = stSql + 
					        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "',";
					//      Else
					//         stSql = stSql & _
					//'                         "'" & rsMaterial!gtipoart & rsMaterial!gcodiart & "',"
					//      End If
					object tempRefParam = FechaSistema;
					object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + 
					        Convert.ToString(iteration_row["cservida"]) + "," + 
					        "" + Serrores.FormatFechaHMS(tempRefParam) + "," + 
					        "" + Serrores.FormatFechaHMS(tempRefParam2) + "," + 
					        "" + Serrores.FormatFechaHMS(tempRefParam3) + ",";
					FechaSistema = Convert.ToString(tempRefParam);
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + 
						        "(Null),";
					}
					else
					{
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
					}
					stSql = stSql + "'" + sRegiqui + "',";
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + 
						        "(Null),";
					}
					else
					{
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
					}
					if (FechaPerfact == "")
					{
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
						{
							object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
							stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam4) + "";
						}
						else
						{
							object tempRefParam5 = rsIntervencion.Tables[0].Rows[0]["finterve"];
							stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam5) + "";
						}
					}
					else
					{
						object tempRefParam6 = FechaPerfact;
						stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam6) + "";
						FechaPerfact = Convert.ToString(tempRefParam6);
					}
					stSql = stSql + ",'" + Convert.ToString(iteration_row["gcodiart"]) + "'";
					if (GestionLotes)
					{
						stsqllot = ",'" + Convert.ToString(iteration_row["gcodlote"]) + "'";
					}
					else
					{
						stsqllot = " ";
					}
					stSql = stSql + stsqllot;
					//oscar 29/10/03
					stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + " )";
					//------

					//Debug.Print stSql
					SqlCommand tempCommand = new SqlCommand(stSql, Conexion);
					tempCommand.ExecuteNonQuery();

					//      rsMaterial.Edit
					//      rsMaterial!fpasfact = FechaSistema
					//      rsMaterial.Update

				}
				else
				{

					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
					}
					if (Convert.ToString(iteration_row["ifarmaco"]) == "S")
					{
						stSql = stSql + " AND gcodeven ='FA'";
					}
					else
					{
						stSql = stSql + " AND gcodeven ='AL'";
					}
					stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					if (Convert.ToString(iteration_row["ifarmaco"]) == "S")
					{
						stSql = stSql + " AND gserreal=" + iFarmacia.ToString();
					}
					else
					{
						stSql = stSql + " AND gserreal=" + iAlmacen.ToString();
					}
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					stSql = stSql + " AND ncantida=" + Convert.ToString(iteration_row["cservida"]);
					object tempRefParam7 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam7);
					object tempRefParam8 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam8);
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					stSql = stSql + " AND iregiqui='" + sRegiqui + "'";
					stSql = stSql + " AND gcodiart='" + Convert.ToString(iteration_row["gcodiart"]) + "'";
					if (GestionLotes)
					{
						stSql = stSql + " AND gcodlote='" + Convert.ToString(iteration_row["gcodlote"]) + "'";
					}
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_2.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}


					if (bEncontrado)
					{

						stSql = " UPDATE " + stTablaFacturacion_W + " SET ";
						stSql = stSql + "  gidenpac= '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'";
						stSql = stSql + ", itpacfac='I'";
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
						{
							stSql = stSql + " , itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
							stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
							stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						}
						else
						{
							stSql = stSql + " , itiposer='Q'";
							stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
							stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						}
						if (Convert.ToString(iteration_row["ifarmaco"]) == "S")
						{
							stSql = stSql + " , gcodeven='FA'";
						}
						else
						{
							stSql = stSql + " , gcodeven='AL'";
						}
						stSql = stSql + " , gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
						if (Convert.ToString(iteration_row["ifarmaco"]) == "S")
						{
							stSql = stSql + " , gserreal=" + iFarmacia.ToString();
						}
						else
						{
							stSql = stSql + " , gserreal=" + iAlmacen.ToString();
						}
						stSql = stSql + " , gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
						{
							stSql = stSql + " , nafiliac = (NULL) ";
						}
						else
						{
							stSql = stSql + " , nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
						}
						stSql = stSql + " , gconcfac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
						stSql = stSql + " , ncantida=" + Convert.ToString(iteration_row["cservida"]);
						object tempRefParam9 = FechaSistema;
						stSql = stSql + " , fsistema=" + Serrores.FormatFechaHMS(tempRefParam9);
						FechaSistema = Convert.ToString(tempRefParam9);
						object tempRefParam10 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHMS(tempRefParam10);
						object tempRefParam11 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHMS(tempRefParam11);
						if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
						{
							stSql = stSql + " , ginspecc = (Null) ";
						}
						else
						{
							stSql = stSql + " , ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
						}
						stSql = stSql + " , iregiqui='" + sRegiqui + "'";
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
						{
							stSql = stSql + " , ndninifp = (Null)";
						}
						else
						{
							stSql = stSql + " , ndninifp='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
						}
						if (FechaPerfact == "")
						{
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
							{
								object tempRefParam12 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
								stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam12) + "";
							}
							else
							{
								object tempRefParam13 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam13) + "";
							}
						}
						else
						{
							object tempRefParam14 = FechaPerfact;
							stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam14) + "";
							FechaPerfact = Convert.ToString(tempRefParam14);
						}
						stSql = stSql + " , gcodiart='" + Convert.ToString(iteration_row["gcodiart"]) + "'";
						if (GestionLotes)
						{
							stSql = stSql + " , gcodlote='" + Convert.ToString(iteration_row["gcodlote"]) + "'";
						}
						stSql = stSql + " , itiporig='Q'";
						stSql = stSql + " , ganoorig=" + AñoRegistro.ToString();
						stSql = stSql + " , gnumorig=" + NumeroRegistro.ToString();
						stSql = stSql + " , IBORRADO=(NULL) ";

						stSql = stSql + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);

						SqlCommand tempCommand_2 = new SqlCommand(stSql, Conexion);
						tempCommand_2.ExecuteNonQuery();

					}
					else
					{

						stSql = "INSERT INTO " + stTablaFacturacion_W + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
						        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, " + 
						        "ncantida, fsistema, ffinicio, ffechfin, ginspecc, iregiqui, ndninifp, fperfact, gcodiart" + stsqllot + 
						        ",itiporig, ganoorig, gnumorig ) " + 
						        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'," + 
						        "'I',";

						// Si el paciente no estaba hospitalizado la prestación principal
						// se graba como externa. Si estaba hospitalizado se graba como interna
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + ",";
						}
						else
						{
							stSql = stSql + 
							        "'Q'," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + ",";
						}
						if (Convert.ToString(iteration_row["ifarmaco"]) == "S")
						{
							stSql = stSql + 
							        "'FA',";
						}
						else
						{
							stSql = stSql + 
							        "'AL',";
						}
						// Modificación: 10/11/99 el servicio solicitante va a nulos
						// cuando es un ambulante

						//
						//      If IsNull(rsIntervencion!itiposer) Then
						//            stSql = stSql & _
						//'                                "(Null) ,"
						//      Else
						// modificacion 31/3/2000
						stSql = stSql + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
						//      End If
						if (Convert.ToString(iteration_row["ifarmaco"]) == "S")
						{
							stSql = stSql + iFarmacia.ToString() + ",";
						}
						else
						{
							stSql = stSql + iAlmacen.ToString() + ",";
						}
						stSql = stSql + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
						{
							stSql = stSql + 
							        "(Null),";
						}
						else
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
						}
						//      If rsMaterial!ifarmaco = "S" Then
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "',";
						//      Else
						//         stSql = stSql & _
						//'                         "'" & rsMaterial!gtipoart & rsMaterial!gcodiart & "',"
						//      End If
						object tempRefParam15 = FechaSistema;
						object tempRefParam16 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						object tempRefParam17 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + 
						        Convert.ToString(iteration_row["cservida"]) + "," + 
						        "" + Serrores.FormatFechaHMS(tempRefParam15) + "," + 
						        "" + Serrores.FormatFechaHMS(tempRefParam16) + "," + 
						        "" + Serrores.FormatFechaHMS(tempRefParam17) + ",";
						FechaSistema = Convert.ToString(tempRefParam15);
						if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
						{
							stSql = stSql + 
							        "(Null),";
						}
						else
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
						}
						stSql = stSql + "'" + sRegiqui + "',";
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
						{
							stSql = stSql + 
							        "(Null),";
						}
						else
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
						}
						if (FechaPerfact == "")
						{
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
							{
								object tempRefParam18 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
								stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam18) + "";
							}
							else
							{
								object tempRefParam19 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam19) + "";
							}
						}
						else
						{
							object tempRefParam20 = FechaPerfact;
							stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam20) + "";
							FechaPerfact = Convert.ToString(tempRefParam20);
						}
						stSql = stSql + ",'" + Convert.ToString(iteration_row["gcodiart"]) + "'";
						if (GestionLotes)
						{
							stsqllot = ",'" + Convert.ToString(iteration_row["gcodlote"]) + "'";
						}
						else
						{
							stsqllot = " ";
						}
						stSql = stSql + stsqllot;
						//oscar 29/10/03
						stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + " )";
						//------

						//Debug.Print stSql
						SqlCommand tempCommand_3 = new SqlCommand(stSql, Conexion);
						tempCommand_3.ExecuteNonQuery();

					}
					RsAux.Close();
				}


			}

			object tempRefParam21 = FechaSistema;
			stSql = "update qmatutil set fpasfact = " + Serrores.FormatFechaHMS(tempRefParam21) + " " + 
			        " WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
			        "gnumregi = " + NumeroRegistro.ToString();
			FechaSistema = Convert.ToString(tempRefParam21);

			SqlCommand tempCommand_4 = new SqlCommand(stSql, Conexion);
			tempCommand_4.ExecuteNonQuery();
			//-----FIN Facturacion de los conceptos facturables asociados al posible material utilizado en la intervencion--------
		}

		private void proFacturacionComadronayAyudante()
		{
			bool bEncontrado = false;
			//--------facturacion movimiento para la comadrona----------------------------------------------OK
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gcomadro"]))
			{
				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "SELECT * " + 
						        "FROM dmovfacc " + 
						        "WHERE 1 = 2";
					}
					else
					{
						//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
						stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
					}

					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter.Fill(rsFactura);
					rsFactura.AddNew();
					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gcomadro"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}
				else
				{

					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						stSql = stSql + " AND gsersoli IS NULL";
					}
					stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
					{
						object tempRefParam = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam);
					}
					else
					{
						object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam2);
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
					{
						object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam3);
					}
					else
					{
						object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam4);
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
					{
						stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
					}
					else
					{
						stSql = stSql + " AND gdiagalt IS NULL";
					}
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
					{
						stSql = stSql + " AND iregiqui='C'";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
						{
							stSql = stSql + " AND iregiqui='A'";
						}
						else
						{
							stSql = stSql + " AND iregiqui='S'";
						}
					}
					stSql = stSql + " AND gcodeven ='AY'";
					stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gcomadro"]);
					stSql = stSql + " AND ncantida =1";
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_3.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}

                    SqlDataAdapter tempAdapter_4 = null;

                    if (bEncontrado)
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
						tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_4.Fill(rsFactura);
						rsFactura.Edit();
						rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
					}
					else
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
						tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
                        tempAdapter_4.Fill(rsFactura);
						rsFactura.AddNew();
					}

					RsAux.Close();

					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gcomadro"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
                    tempAdapter_4.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}

			}
			//--------FIN facturacion movimiento para la comadrona---------------------------------------------


			//--------facturacion movimiento para la ayundante de la comadrona------------------------------OK
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gcomadr2"]))
			{
				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "SELECT * " + 
						        "FROM dmovfacc " + 
						        "WHERE 1 = 2";
					}
					else
					{
						//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
						stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
					}

					SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_7.Fill(rsFactura);
					rsFactura.AddNew();
					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gcomadr2"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_7);
                    tempAdapter_7.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}
				else
				{

					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						stSql = stSql + " AND gsersoli IS NULL";
					}
					stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
					{
						object tempRefParam5 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam5);
					}
					else
					{
						object tempRefParam6 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam6);
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
					{
						object tempRefParam7 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam7);
					}
					else
					{
						object tempRefParam8 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam8);
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
					{
						stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
					}
					else
					{
						stSql = stSql + " AND gdiagalt IS NULL";
					}
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
					{
						stSql = stSql + " AND iregiqui='C'";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
						{
							stSql = stSql + " AND iregiqui='A'";
						}
						else
						{
							stSql = stSql + " AND iregiqui='S'";
						}
					}
					stSql = stSql + " AND gcodeven ='AY'";
					stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gcomadr2"]);
					stSql = stSql + " AND ncantida =1";
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_9.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}

                    SqlDataAdapter tempAdapter_10 = null;

                    if (bEncontrado)
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
						tempAdapter_10 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_10.Fill(rsFactura);
						rsFactura.Edit();
						rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
					}
					else
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
                        tempAdapter_10 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_10.Fill(rsFactura);
						rsFactura.AddNew();
					}

					RsAux.Close();

					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gcomadr2"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_10);
                    tempAdapter_10.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}

			}
			//--------FIN facturacion movimiento para la ayudante de la comadrona------------------------------
		}

		private void proFacturacionDerechosQuirofano()
		{
			bool bEncontrado = false;
			//--------facturacion movimiento derechos de quirofano -----------------------------------------OK
			//OSCAR C Abril 2009
			//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
			//de facturacion.
			//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
			//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
			//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
			//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
			//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
			//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
			if (stTablaFacturacion != "DMOVFACT")
			{

				if (bTemp)
				{
					stSql = "SELECT * " + 
					        "FROM dmovfacc " + 
					        "WHERE 1 = 2";
				}
				else
				{
					//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
					stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
				rsFactura = new DataSet();
				tempAdapter.Fill(rsFactura);
				rsFactura.AddNew();
				InicializarDatos();
				rsFactura.Tables[0].Rows[0]["gcodeven"] = "DQ";
				rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gmedicoc"];
				//   rsFactura!ncantida = 1  Modificado el 21/4/99
				//   solamente se factura si han rellenado las horas. Si no se pone 0
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]) && !Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
				{
					rsFactura.Tables[0].Rows[0]["ncantida"] = (int) DateAndTime.DateDiff("n", Convert.ToDateTime(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]), Convert.ToDateTime(rsIntervencion.Tables[0].Rows[0]["hhsalida"]), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
				}
				else
				{
					rsFactura.Tables[0].Rows[0]["ncantida"] = 0;
				}
				//oscar 29/10/03
				rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
				rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
				rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
				//-----
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(rsFactura, rsFactura.Tables[0].TableName);
				rsFactura.Close();

			}
			else
			{

				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				// NOTA:
				// La condicion de busqueda sera por:
				// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
				// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
				// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
				// y que ademas tuviera la marca de borrado logico (iborrado='S')
				//* No aplica en Quirofanos en este caso
				bEncontrado = false;

				stSql = "SELECT * FROM " + stTablaFacturacion_W;
				stSql = stSql + " WHERE ";
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
				{
					stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
					stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
					stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
					stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
				}
				else
				{
					stSql = stSql + "  itiposer='Q'";
					stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
					stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
					stSql = stSql + " AND gsersoli IS NULL";
				}
				stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
				stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
				if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
				{
					stSql = stSql + " AND nafiliac IS NULL";
				}
				else
				{
					stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
				}
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
				{
					object tempRefParam = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
					stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam);
				}
				else
				{
					object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam2);
				}
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
				{
					object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
					stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam3);
				}
				else
				{
					object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam4);
				}
				stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
				{
					stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
				}
				else
				{
					stSql = stSql + " AND gdiagalt IS NULL";
				}
				if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
				{
					stSql = stSql + " AND ginspecc IS NULL";
				}
				else
				{
					stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
				}
				if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
				{
					stSql = stSql + " AND iregiqui='C'";
				}
				else
				{
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
					{
						stSql = stSql + " AND iregiqui='A'";
					}
					else
					{
						stSql = stSql + " AND iregiqui='S'";
					}
				}
				stSql = stSql + " AND gcodeven ='DQ'";
				stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]) && !Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
				{
					stSql = stSql + " AND ncantida =" + ((int) DateAndTime.DateDiff("n", Convert.ToDateTime(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]), Convert.ToDateTime(rsIntervencion.Tables[0].Rows[0]["hhsalida"]), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)).ToString();
				}
				else
				{
					stSql = stSql + " AND ncantida =0";
				}
				stSql = stSql + " AND itiporig ='Q'";
				stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
				stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
				stSql = stSql + " AND iborrado='S'";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
				RsAux = new DataSet();
				tempAdapter_3.Fill(RsAux);
				if (RsAux.Tables[0].Rows.Count != 0)
				{
					RsAux.MoveFirst();
					bEncontrado = true;
				}

                SqlDataAdapter tempAdapter_4 = null;

                if (bEncontrado)
				{
					stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
					tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_4.Fill(rsFactura);
					rsFactura.Edit();
					rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
				}
				else
				{
					stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
					tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_4.Fill(rsFactura);
					rsFactura.AddNew();
				}

				RsAux.Close();

				InicializarDatos();
				rsFactura.Tables[0].Rows[0]["gcodeven"] = "DQ";
				rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gmedicoc"];
				//   rsFactura!ncantida = 1  Modificado el 21/4/99
				//   solamente se factura si han rellenado las horas. Si no se pone 0
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]) && !Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
				{
					rsFactura.Tables[0].Rows[0]["ncantida"] = (int) DateAndTime.DateDiff("n", Convert.ToDateTime(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]), Convert.ToDateTime(rsIntervencion.Tables[0].Rows[0]["hhsalida"]), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
				}
				else
				{
					rsFactura.Tables[0].Rows[0]["ncantida"] = 0;
				}
				//oscar 29/10/03
				rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
				rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
				rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
				//-----
				SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
                tempAdapter_4.Update(rsFactura, rsFactura.Tables[0].TableName);
				rsFactura.Close();

			}
			//--------FIN facturacion movimiento derechos de quirofano ----------------------------------------
		}

		private void proFacturacionOtrasPrestacionesRealizadas()
		{
			bool bEncontrado = false;
			//Facturacion de otras prestaciones asociadas en la intervencion----------------------------------
			//grabamos si tiene otras prestaciones 03/01/2000
			//buscamos en epresalt si tiene prestaciones asociadas
			stSql = "select * from epresalt where itiposer='q'" + " and ganoregi=" + AñoRegistro.ToString() + " and gnumregi=" + NumeroRegistro.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
			rsDcodpres = new DataSet();
			tempAdapter.Fill(rsDcodpres);

			//necesitamos el servicio de la prestacion para grabar el realizador
			DataSet tRr1 = null;
			int tiServicio = 0;
			if (rsDcodpres.Tables[0].Rows.Count != 0)
			{
				object tempRefParam = rsDcodpres.Tables[0].Rows[0]["FREALIZA"];
				object tempRefParam2 = rsDcodpres.Tables[0].Rows[0]["FREALIZA"];
				sql = "select gservici from dcodpres where " + " gprestac='" + Convert.ToString(rsDcodpres.Tables[0].Rows[0]["gprestac"]) + "' and " + " finivali <=" + Serrores.FormatFechaHMS(tempRefParam) + " AND " + " (ffinvali>= " + Serrores.FormatFechaHMS(tempRefParam2) + " or ffinvali is null)";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
				tRr1 = new DataSet();
				tempAdapter_2.Fill(tRr1);

				if (tRr1.Tables[0].Rows.Count != 0)
				{ //anda que como no lo encuentre
					tiServicio = Convert.ToInt32(tRr1.Tables[0].Rows[0]["gservici"]);
				}
				tRr1.Close();
			}

			//oscar 17/12/2004
			bAñadirHM = false;
			sql = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO ='QOTROSHM'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, Conexion);
			tRr1 = new DataSet();
			tempAdapter_3.Fill(tRr1);
			if (tRr1.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(tRr1.Tables[0].Rows[0]["VALFANU1"]))
				{
					if (Convert.ToString(tRr1.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "S")
					{
						bAñadirHM = true;
					}
				}
			}
			tRr1.Close();
			bAñadirAY = false;
			sql = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='QOTROSAY'";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion);
			tRr1 = new DataSet();
			tempAdapter_4.Fill(tRr1);
			if (tRr1.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(tRr1.Tables[0].Rows[0]["VALFANU1"]))
				{
					if (Convert.ToString(tRr1.Tables[0].Rows[0]["VALFANU1"]).Trim().ToUpper() == "S")
					{
						bAñadirAY = true;
					}
				}
			}
			tRr1.Close();
			//----------------

			//OSCAR C 15/07/2005
			//Ahora y para que otros centros no protesten, lo de capturar el responsable
			//de la prestacion en la pestaña de otras prestaciones depende de constante global (como no)
			bCapturarResponsable = Serrores.ObternerValor_CTEGLOBAL(Conexion, "RESOTRPR", "VALFANU1") == "S";
            //----------
            int iMoveFirst = 0;
			//bucle por los registros
			foreach (DataRow iteration_row in rsDcodpres.Tables[0].Rows)
			{

				//si es un protocolo facturamos lo de danalsol
				if (HayProtocolo(Convert.ToString(iteration_row["gprestac"]), Conexion))
				{
					ExisteProtocolo = true;
					sql = "select * from danalsol where ganoregi=" + AñoRegistro.ToString() + " and " + "gnumregi=" + NumeroRegistro.ToString() + " AND GPRESTAC='" + Convert.ToString(iteration_row["gprestac"]) + "' and itiposer='Q'";
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, Conexion);
					rsDanalsol = new DataSet();
					tempAdapter_5.Fill(rsDanalsol);
					if (rsDanalsol.Tables[0].Rows.Count != 0)
					{
                        iMoveFirst = rsDanalsol.MoveFirst();
					}
				}
				else
				{
					ExisteProtocolo = false;
					PrestacionFactura = Convert.ToString(iteration_row["gprestac"]);
				}
				///*****************************

				Continua_Bucle = true;

				while (Continua_Bucle)
				{

					if (ExisteProtocolo)
					{
                        PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[iMoveFirst]["gpruelab"]);
					}
					else
					{
						Continua_Bucle = false;
					}
					object tempRefParam3 = iteration_row["FREALIZA"];
					object tempRefParam4 = iteration_row["FREALIZA"];
					sql = "select gservici from dcodpres where " + " gprestac='" + Convert.ToString(iteration_row["gprestac"]) + "' and " + " finivali <=" + Serrores.FormatFechaHMS(tempRefParam3) + " AND " + " (ffinvali>= " + Serrores.FormatFechaHMS(tempRefParam4) + " or ffinvali is null)";
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql, Conexion);
					tRr1 = new DataSet();
					tempAdapter_6.Fill(tRr1);

					if (tRr1.Tables[0].Rows.Count != 0)
					{ //anda que como no lo encuentre
						tiServicio = Convert.ToInt32(tRr1.Tables[0].Rows[0]["gservici"]);
					}

					sql = "SELECT iquirofa from DSERVICI where gservici=" + Convert.ToString(tRr1.Tables[0].Rows[0]["gservici"]);
					SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql, Conexion);
					tRr1 = new DataSet();
					tempAdapter_7.Fill(tRr1);
					iquirofa = Convert.ToString(tRr1.Tables[0].Rows[0]["iquirofa"]).Trim().ToUpper();

					tRr1.Close();


					//oscar 17/12/2004
					//**************************************************************
					//Si la el indicador de quirofa de la prestacion ="S" y la constante global QOTROSHM
					//esta establecida a "S", Hay que grabar un registro en dmovfact de HM

					//Ademas, si la constante global QOTROSAY esta establecida a "S, hay que grabar un
					//registro en DMOVFACT de AY para cada ayudante que este relleno (gayudant, gayudant2)


					if (iquirofa == "S" && bAñadirHM)
					{
						//-------FACTURACION Movimiento del Honorario Medico ------------------------------------------------OK

						//OSCAR C Abril 2009
						//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
						//de facturacion.
						//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
						//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
						//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
						//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
						//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
						//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
						if (stTablaFacturacion != "DMOVFACT")
						{

							if (bTemp)
							{
								stSql = "INSERT INTO dmovfacc " + 
								        "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
								        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
								        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
								        "ginspecc, iregiqui, iautosoc, " + 
								        "iexiauto, ndninifp, fperfact, " + 
								        "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " + 
								        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";
							}
							else
							{
								//                          StSql = "INSERT INTO dmovfact " & _
								//'                                     "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " & _
								//'                                      "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " & _
								//'                                      "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " & _
								//'                                      "ginspecc, iregiqui, iautosoc, " & _
								//'                                      "iexiauto, ndninifp, fperfact, " & _
								//'                                      "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " & _
								//'                                     "VALUES('" & rsIntervencion!gidenpac & "','I',"

								stSql = "INSERT INTO " + stTablaFacturacion + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
								        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
								        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
								        "ginspecc, iregiqui, iautosoc, " + 
								        "iexiauto, ndninifp, fperfact, " + 
								        "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " + 
								        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";
							}

							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
							{
								stSql = stSql + 
								        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
								        "'HM',";
							}
							else
							{
								stSql = stSql + 
								        "'Q'," + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
								        "'HM',";
							}

							//servicio solicitante
							stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
							//servicio realizador de la prestacion
							stSql = stSql + tiServicio.ToString() + ",";

							// si no ha traído la autorización necesaria
							// la prestación se cobra como privado
							if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
							{
								stSql = stSql + 
								        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
							}
							else
							{
								stSql = stSql + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
							}

							//numero de afiliacion
							if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
							{
								stSql = stSql + "(Null),";
							}
							else
							{
								stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
							}

							//prestacion, fecha sistema, fecha inicio,fecha fin
							object tempRefParam5 = FechaSistema;
							stSql = stSql + 
							        "'" + PrestacionFactura + "'," + 
							        Convert.ToString(iteration_row["ncantida"]) + "," + 
							        "" + Serrores.FormatFechaHMS(tempRefParam5) + ",";
							FechaSistema = Convert.ToString(tempRefParam5); //& |                                            '"" & FormatFechaHM(rsIntervencion!finterve) & "," & |                                            '"" & FormatFechaHM(rsIntervencion!finterve) & ","

							//OSCAR C: 15/07/2005
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
							{
								object tempRefParam6 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
								stSql = stSql + Serrores.FormatFechaHM(tempRefParam6) + ", ";
							}
							else
							{
								object tempRefParam7 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + Serrores.FormatFechaHM(tempRefParam7) + ", ";
							}
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
							{
								object tempRefParam8 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
								stSql = stSql + Serrores.FormatFechaHM(tempRefParam8) + ", ";
							}
							else
							{
								object tempRefParam9 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + Serrores.FormatFechaHM(tempRefParam9) + ", ";
							}
							//--------------

							//inspeccion
							if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
							{
								stSql = stSql + "(Null),";
							}
							else
							{
								stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
							}

							//regimen quirurgico
							stSql = stSql + "'" + sRegiqui + "',";

							//autorizacion
							if (!Convert.IsDBNull(iteration_row["iautosoc"]))
							{
								stSql = stSql + "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
							}
							else
							{
								stSql = stSql + "(Null),";
							}

							if (!Convert.IsDBNull(iteration_row["iexiauto"]))
							{
								stSql = stSql + "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
							}
							else
							{
								stSql = stSql + "(Null),";
							}

							//NDNINIFP
							if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
							{
								stSql = stSql + "(Null),";
							}
							else
							{
								stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
							}

							//fperfact
							object tempRefParam10 = FechaPerfact;
							stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam10);
							FechaPerfact = Convert.ToString(tempRefParam10);

							stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ",";

							//OSCAR C 15/07/2005
							if (bCapturarResponsable)
							{
								stSql = stSql + Convert.ToString(iteration_row["gperreal"]);
							}
							else
							{
								stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
							}
							//----------

							stSql = stSql + ",'" + OrigenQuirurgico + "')";

							SqlCommand tempCommand = new SqlCommand(stSql, Conexion);
							tempCommand.ExecuteNonQuery();

						}
						else
						{

							//OSCAR C Abril 2009
							//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
							//de facturacion.
							//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
							//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
							//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
							//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
							//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
							//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
							// NOTA:
							// La condicion de busqueda sera por:
							// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
							// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
							// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
							// y que ademas tuviera la marca de borrado logico (iborrado='S')
							//* No aplica en Quirofanos en este caso
							bEncontrado = false;

							stSql = "SELECT * FROM " + stTablaFacturacion_W;
							stSql = stSql + " WHERE ";
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
							{
								stSql = stSql + " itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
								stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
								stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
							}
							else
							{
								stSql = stSql + " itiposer='Q'";
								stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
								stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
							}
							stSql = stSql + " AND gcodeven ='HM'";
							stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
							stSql = stSql + " AND gserreal=" + tiServicio.ToString();
							if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
							{
								stSql = stSql + " AND gsocieda=" + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
							}
							else
							{
								stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
							}
							if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
							{
								stSql = stSql + " AND nafiliac IS NULL";
							}
							else
							{
								stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
							}
							stSql = stSql + " AND gconcfac ='" + PrestacionFactura + "'";
							stSql = stSql + " AND ncantida=" + Convert.ToString(iteration_row["ncantida"]);
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
							{
								object tempRefParam11 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
								stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHM(tempRefParam11);
							}
							else
							{
								object tempRefParam12 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHM(tempRefParam12);
							}
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
							{
								object tempRefParam13 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
								stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHM(tempRefParam13);
							}
							else
							{
								object tempRefParam14 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHM(tempRefParam14);
							}
							//stSql = stSql & " AND gdiagalt="  No Aplica
							if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
							{
								stSql = stSql + " AND ginspecc IS NULL";
							}
							else
							{
								stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
							}
							stSql = stSql + " AND iregiqui='" + sRegiqui + "'";
							//stSql = stSql & " AND gserresp"  No Aplica.
							//stSql = stSql & " AND gcodiart=" No Aplica
							//stSql = stSql & " AND gcodlote=" No Aplica
							stSql = stSql + " AND itiporig ='Q'";
							stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
							stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
							if (bCapturarResponsable)
							{
								stSql = stSql + " AND gpersona =" + Convert.ToString(iteration_row["gperreal"]);
							}
							else
							{
								stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
							}
							stSql = stSql + " AND iborrado='S'";

							SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(stSql, Conexion);
							RsAux = new DataSet();
							tempAdapter_8.Fill(RsAux);
							if (RsAux.Tables[0].Rows.Count != 0)
							{
								RsAux.MoveFirst();
								bEncontrado = true;
							}


							if (bEncontrado)
							{

								stSql = " UPDATE " + stTablaFacturacion_W + " SET ";
								stSql = stSql + "  gidenpac= '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'";
								stSql = stSql + ", itpacfac='I'";
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
								{
									stSql = stSql + " , itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
									stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
									stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
								}
								else
								{
									stSql = stSql + " , itiposer='Q'";
									stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
									stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
								}
								stSql = stSql + " , gcodeven='HM'";
								stSql = stSql + " , gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
								stSql = stSql + " , gserreal=" + tiServicio.ToString();
								if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + " , gsocieda=" + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
								}
								else
								{
									stSql = stSql + " , gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
								}
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
								{
									stSql = stSql + " , nafiliac = (NULL) ";
								}
								else
								{
									stSql = stSql + " , nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
								}

								stSql = stSql + " , gconcfac='" + PrestacionFactura + "'";
								stSql = stSql + " , ncantida=" + Convert.ToString(iteration_row["ncantida"]);
								object tempRefParam15 = FechaSistema;
								stSql = stSql + " , fsistema=" + Serrores.FormatFechaHMS(tempRefParam15);
								FechaSistema = Convert.ToString(tempRefParam15);
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
								{
									object tempRefParam16 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
									stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHM(tempRefParam16);
								}
								else
								{
									object tempRefParam17 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHM(tempRefParam17);
								}
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
								{
									object tempRefParam18 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
									stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHM(tempRefParam18);
								}
								else
								{
									object tempRefParam19 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHM(tempRefParam19);
								}
								if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
								{
									stSql = stSql + " , ginspecc = (Null) ";
								}
								else
								{
									stSql = stSql + " , ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
								}
								stSql = stSql + " , iregiqui='" + sRegiqui + "'";
								if (!Convert.IsDBNull(iteration_row["iautosoc"]))
								{
									stSql = stSql + " , iautosoc='" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "'";
								}
								else
								{
									stSql = stSql + " , iautosoc=(Null)";
								}
								if (!Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + " , iexiauto='" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "'";
								}
								else
								{
									stSql = stSql + " , iexiauto=(Null)";
								}
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
								{
									stSql = stSql + " , ndninifp = (Null)";
								}
								else
								{
									stSql = stSql + " , ndninifp='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
								}
								if (FechaPerfact == "")
								{
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
									{
										object tempRefParam20 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
										stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam20) + "";
									}
									else
									{
										object tempRefParam21 = rsIntervencion.Tables[0].Rows[0]["finterve"];
										stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam21) + "";
									}
								}
								else
								{
									object tempRefParam22 = FechaPerfact;
									stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam22) + "";
									FechaPerfact = Convert.ToString(tempRefParam22);
								}
								stSql = stSql + " , itiporig='Q'";
								stSql = stSql + " , ganoorig=" + AñoRegistro.ToString();
								stSql = stSql + " , gnumorig=" + NumeroRegistro.ToString();
								if (bCapturarResponsable)
								{
									stSql = stSql + " , gpersona=" + Convert.ToString(iteration_row["gperreal"]);
								}
								else
								{
									stSql = stSql + " , gpersona=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
								}
								stSql = stSql + " , IBORRADO=(NULL) ";

								stSql = stSql + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);

								SqlCommand tempCommand_2 = new SqlCommand(stSql, Conexion);
								tempCommand_2.ExecuteNonQuery();

							}
							else
							{

								stSql = "INSERT INTO " + stTablaFacturacion_W + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
								        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
								        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
								        "ginspecc, iregiqui, iautosoc, " + 
								        "iexiauto, ndninifp, fperfact, " + 
								        "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " + 
								        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";

								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
								{
									stSql = stSql + 
									        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
									        "'HM',";
								}
								else
								{
									stSql = stSql + 
									        "'Q'," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
									        "'HM',";
								}

								//servicio solicitante
								stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
								//servicio realizador de la prestacion
								stSql = stSql + tiServicio.ToString() + ",";

								// si no ha traído la autorización necesaria
								// la prestación se cobra como privado
								if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + 
									        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
								}
								else
								{
									stSql = stSql + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
								}

								//numero de afiliacion
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
								{
									stSql = stSql + "(Null),";
								}
								else
								{
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
								}

								//prestacion, fecha sistema, fecha inicio,fecha fin
								object tempRefParam23 = FechaSistema;
								stSql = stSql + 
								        "'" + PrestacionFactura + "'," + 
								        Convert.ToString(iteration_row["ncantida"]) + "," + 
								        "" + Serrores.FormatFechaHMS(tempRefParam23) + ",";
								FechaSistema = Convert.ToString(tempRefParam23); //& |                                            '"" & FormatFechaHM(rsIntervencion!finterve) & "," & |                                            '"" & FormatFechaHM(rsIntervencion!finterve) & ","

								//OSCAR C: 15/07/2005
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
								{
									object tempRefParam24 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam24) + ", ";
								}
								else
								{
									object tempRefParam25 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam25) + ", ";
								}
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
								{
									object tempRefParam26 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam26) + ", ";
								}
								else
								{
									object tempRefParam27 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam27) + ", ";
								}
								//--------------

								//inspeccion
								if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
								{
									stSql = stSql + "(Null),";
								}
								else
								{
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
								}

								//regimen quirurgico
								stSql = stSql + "'" + sRegiqui + "',";

								//autorizacion
								if (!Convert.IsDBNull(iteration_row["iautosoc"]))
								{
									stSql = stSql + "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
								}
								else
								{
									stSql = stSql + "(Null),";
								}

								if (!Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
								}
								else
								{
									stSql = stSql + "(Null),";
								}

								//NDNINIFP
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
								{
									stSql = stSql + "(Null),";
								}
								else
								{
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
								}

								//fperfact
								object tempRefParam28 = FechaPerfact;
								stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam28);
								FechaPerfact = Convert.ToString(tempRefParam28);

								stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ",";

								//OSCAR C 15/07/2005
								if (bCapturarResponsable)
								{
									stSql = stSql + Convert.ToString(iteration_row["gperreal"]);
								}
								else
								{
									stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
								}
								//----------

								stSql = stSql + ",'" + OrigenQuirurgico + "')";

								SqlCommand tempCommand_3 = new SqlCommand(stSql, Conexion);
								tempCommand_3.ExecuteNonQuery();

							}


							RsAux.Close();

						}

					}


					if (iquirofa == "S" && bAñadirAY)
					{

						//-------FACTURACION Movimiento del Anestesista ----------------------------------------------------OK
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gayudant"]))
						{

							//OSCAR C Abril 2009
							//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
							//de facturacion.
							//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
							//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
							//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
							//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
							//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
							//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL

							if (stTablaFacturacion != "DMOVFACT")
							{

								if (bTemp)
								{
									stSql = "INSERT INTO dmovfacc " + 
									        "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
									        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
									        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
									        "ginspecc, iregiqui, iautosoc, " + 
									        "iexiauto, ndninifp, fperfact, " + 
									        "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " + 
									        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";
								}
								else
								{
									//                              StSql = "INSERT INTO dmovfact " & _
									//'                                         "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " & _
									//'                                          "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " & _
									//'                                          "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " & _
									//'                                          "ginspecc, iregiqui, iautosoc, " & _
									//'                                          "iexiauto, ndninifp, fperfact, " & _
									//'                                          "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " & _
									//'                                         "VALUES('" & rsIntervencion!gidenpac & "','I',"

									stSql = "INSERT INTO " + stTablaFacturacion + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
									        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
									        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
									        "ginspecc, iregiqui, iautosoc, " + 
									        "iexiauto, ndninifp, fperfact, " + 
									        "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " + 
									        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";
								}


								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
								{
									stSql = stSql + 
									        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
									        "'AY',";
								}
								else
								{
									stSql = stSql + 
									        "'Q'," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
									        "'AY',";
								}

								//servicio solicitante
								stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
								//servicio realizador de la prestacion
								stSql = stSql + tiServicio.ToString() + ",";

								//si no ha traído la autorización necesaria
								//la prestación se cobra como privado
								if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + 
									        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
								}
								else
								{
									stSql = stSql + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
								}

								//numero de afiliacion
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
								{
									stSql = stSql + "(Null),";
								}
								else
								{
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
								}

								//prestacion, fecha sistema, fecha inicio,fecha fin
								object tempRefParam29 = FechaSistema;
								stSql = stSql + 
								        "'" + PrestacionFactura + "'," + 
								        Convert.ToString(iteration_row["ncantida"]) + "," + 
								        "" + Serrores.FormatFechaHMS(tempRefParam29) + ",";
								FechaSistema = Convert.ToString(tempRefParam29); //& |                                             '"" & FormatFechaHM(rsIntervencion!finterve) & "," & |                                             '"" & FormatFechaHM(rsIntervencion!finterve) & ","

								//OSCAR C: 15/07/2005
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
								{
									object tempRefParam30 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam30) + ", ";
								}
								else
								{
									object tempRefParam31 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam31) + ", ";
								}
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
								{
									object tempRefParam32 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam32) + ", ";
								}
								else
								{
									object tempRefParam33 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam33) + ", ";
								}
								//--------------


								//inspeccion
								if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
								{
									stSql = stSql + "(Null),";
								}
								else
								{
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
								}

								//regimen quirurgico
								stSql = stSql + "'" + sRegiqui + "',";

								//autorizacion
								if (!Convert.IsDBNull(iteration_row["iautosoc"]))
								{
									stSql = stSql + "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
								}
								else
								{
									stSql = stSql + "(Null),";
								}

								if (!Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
								}
								else
								{
									stSql = stSql + "(Null),";
								}

								//NDNINIFP
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
								{
									stSql = stSql + "(Null),";
								}
								else
								{
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
								}

								//fperfact
								object tempRefParam34 = FechaPerfact;
								stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam34);
								FechaPerfact = Convert.ToString(tempRefParam34);

								stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ",";
								stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudant"]) + "','" + OrigenQuirurgico + "')";

								SqlCommand tempCommand_4 = new SqlCommand(stSql, Conexion);
								tempCommand_4.ExecuteNonQuery();

							}
							else
							{

								//OSCAR C Abril 2009
								//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
								//de facturacion.
								//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
								//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
								//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
								//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
								//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
								//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
								// NOTA:
								// La condicion de busqueda sera por:
								// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
								// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
								// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
								// y que ademas tuviera la marca de borrado logico (iborrado='S')
								//* No aplica en Quirofanos en este caso
								bEncontrado = false;

								stSql = "SELECT * FROM " + stTablaFacturacion_W;
								stSql = stSql + " WHERE ";
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
								{
									stSql = stSql + " itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
									stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
									stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
								}
								else
								{
									stSql = stSql + " itiposer='Q'";
									stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
									stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
								}
								stSql = stSql + " AND gcodeven ='AY'";
								stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
								stSql = stSql + " AND gserreal=" + tiServicio.ToString();
								if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + " AND gsocieda=" + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
								}
								else
								{
									stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
								}
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
								{
									stSql = stSql + " AND nafiliac IS NULL";
								}
								else
								{
									stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
								}
								stSql = stSql + " AND gconcfac ='" + PrestacionFactura + "'";
								stSql = stSql + " AND ncantida=" + Convert.ToString(iteration_row["ncantida"]);
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
								{
									object tempRefParam35 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
									stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHM(tempRefParam35);
								}
								else
								{
									object tempRefParam36 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHM(tempRefParam36);
								}
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
								{
									object tempRefParam37 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
									stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHM(tempRefParam37);
								}
								else
								{
									object tempRefParam38 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHM(tempRefParam38);
								}
								if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
								{
									stSql = stSql + " AND ginspecc IS NULL";
								}
								else
								{
									stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
								}
								stSql = stSql + " AND iregiqui='" + sRegiqui + "'";
								stSql = stSql + " AND itiporig ='Q'";
								stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
								stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
								stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudant"]);
								stSql = stSql + " AND iborrado='S'";

								SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql, Conexion);
								RsAux = new DataSet();
								tempAdapter_9.Fill(RsAux);
								if (RsAux.Tables[0].Rows.Count != 0)
								{
									RsAux.MoveFirst();
									bEncontrado = true;
								}

								if (bEncontrado)
								{

									stSql = " UPDATE " + stTablaFacturacion_W + " SET ";
									stSql = stSql + "  gidenpac= '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'";
									stSql = stSql + ", itpacfac='I'";
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
									{
										stSql = stSql + " , itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
										stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
										stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
									}
									else
									{
										stSql = stSql + " , itiposer='Q'";
										stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
										stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
									}
									stSql = stSql + " , gcodeven='AY'";
									stSql = stSql + " , gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
									stSql = stSql + " , gserreal=" + tiServicio.ToString();
									if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
									{
										stSql = stSql + " , gsocieda=" + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
									}
									else
									{
										stSql = stSql + " , gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
									}
									if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
									{
										stSql = stSql + " , nafiliac = (NULL) ";
									}
									else
									{
										stSql = stSql + " , nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
									}

									stSql = stSql + " , gconcfac='" + PrestacionFactura + "'";
									stSql = stSql + " , ncantida=" + Convert.ToString(iteration_row["ncantida"]);
									object tempRefParam39 = FechaSistema;
									stSql = stSql + " , fsistema=" + Serrores.FormatFechaHMS(tempRefParam39);
									FechaSistema = Convert.ToString(tempRefParam39);
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
									{
										object tempRefParam40 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
										stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHM(tempRefParam40);
									}
									else
									{
										object tempRefParam41 = rsIntervencion.Tables[0].Rows[0]["finterve"];
										stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHM(tempRefParam41);
									}
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
									{
										object tempRefParam42 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
										stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHM(tempRefParam42);
									}
									else
									{
										object tempRefParam43 = rsIntervencion.Tables[0].Rows[0]["finterve"];
										stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHM(tempRefParam43);
									}
									if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
									{
										stSql = stSql + " , ginspecc = (Null) ";
									}
									else
									{
										stSql = stSql + " , ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
									}
									stSql = stSql + " , iregiqui='" + sRegiqui + "'";
									if (!Convert.IsDBNull(iteration_row["iautosoc"]))
									{
										stSql = stSql + " , iautosoc='" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "'";
									}
									else
									{
										stSql = stSql + " , iautosoc=(Null)";
									}
									if (!Convert.IsDBNull(iteration_row["iexiauto"]))
									{
										stSql = stSql + " , iexiauto='" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "'";
									}
									else
									{
										stSql = stSql + " , iexiauto=(Null)";
									}
									if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
									{
										stSql = stSql + " , ndninifp = (Null)";
									}
									else
									{
										stSql = stSql + " , ndninifp='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
									}
									object tempRefParam44 = FechaPerfact;
									stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam44) + "";
									FechaPerfact = Convert.ToString(tempRefParam44);
									stSql = stSql + " , itiporig='Q'";
									stSql = stSql + " , ganoorig=" + AñoRegistro.ToString();
									stSql = stSql + " , gnumorig=" + NumeroRegistro.ToString();
									stSql = stSql + " , gpersona=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudant"]);
									stSql = stSql + " , IBORRADO=(NULL) ";

									stSql = stSql + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);

									SqlCommand tempCommand_5 = new SqlCommand(stSql, Conexion);
									tempCommand_5.ExecuteNonQuery();

								}
								else
								{

									stSql = "INSERT INTO " + stTablaFacturacion_W + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
									        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
									        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
									        "ginspecc, iregiqui, iautosoc, " + 
									        "iexiauto, ndninifp, fperfact, " + 
									        "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " + 
									        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";

									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
									{
										stSql = stSql + 
										        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
										        "'AY',";
									}
									else
									{
										stSql = stSql + 
										        "'Q'," + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
										        "'AY',";
									}

									//servicio solicitante
									stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
									//servicio realizador de la prestacion
									stSql = stSql + tiServicio.ToString() + ",";

									//si no ha traído la autorización necesaria
									//la prestación se cobra como privado
									if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
									{
										stSql = stSql + 
										        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
									}
									else
									{
										stSql = stSql + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
									}

									//numero de afiliacion
									if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
									{
										stSql = stSql + "(Null),";
									}
									else
									{
										stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
									}

									//prestacion, fecha sistema, fecha inicio,fecha fin
									object tempRefParam45 = FechaSistema;
									stSql = stSql + 
									        "'" + PrestacionFactura + "'," + 
									        Convert.ToString(iteration_row["ncantida"]) + "," + 
									        "" + Serrores.FormatFechaHMS(tempRefParam45) + ",";
									FechaSistema = Convert.ToString(tempRefParam45); //& |                                                 '"" & FormatFechaHM(rsIntervencion!finterve) & "," & |                                                 '"" & FormatFechaHM(rsIntervencion!finterve) & ","

									//OSCAR C: 15/07/2005
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
									{
										object tempRefParam46 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
										stSql = stSql + Serrores.FormatFechaHM(tempRefParam46) + ", ";
									}
									else
									{
										object tempRefParam47 = rsIntervencion.Tables[0].Rows[0]["finterve"];
										stSql = stSql + Serrores.FormatFechaHM(tempRefParam47) + ", ";
									}
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
									{
										object tempRefParam48 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
										stSql = stSql + Serrores.FormatFechaHM(tempRefParam48) + ", ";
									}
									else
									{
										object tempRefParam49 = rsIntervencion.Tables[0].Rows[0]["finterve"];
										stSql = stSql + Serrores.FormatFechaHM(tempRefParam49) + ", ";
									}
									//--------------


									//inspeccion
									if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
									{
										stSql = stSql + "(Null),";
									}
									else
									{
										stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
									}

									//regimen quirurgico
									stSql = stSql + "'" + sRegiqui + "',";

									//autorizacion
									if (!Convert.IsDBNull(iteration_row["iautosoc"]))
									{
										stSql = stSql + "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
									}
									else
									{
										stSql = stSql + "(Null),";
									}

									if (!Convert.IsDBNull(iteration_row["iexiauto"]))
									{
										stSql = stSql + "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
									}
									else
									{
										stSql = stSql + "(Null),";
									}

									//NDNINIFP
									if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
									{
										stSql = stSql + "(Null),";
									}
									else
									{
										stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
									}

									//fperfact
									object tempRefParam50 = FechaPerfact;
									stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam50);
									FechaPerfact = Convert.ToString(tempRefParam50);

									stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ",";
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudant"]) + "','" + OrigenQuirurgico + "')";

									SqlCommand tempCommand_6 = new SqlCommand(stSql, Conexion);
									tempCommand_6.ExecuteNonQuery();

								}

								RsAux.Close();

							}

						} //if not IsNull(rsIntervencion!gayudan)
						//-------FIN FACTURACION Movimiento del Anestesista ----------------------------------------------------


						//-------FACTURACION Movimiento del Ayudante del Anestesista ---------------------------------------OK
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gayudan2"]))
						{
							//OSCAR C Abril 2009
							//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
							//de facturacion.
							//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
							//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
							//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
							//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
							//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
							//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
							if (stTablaFacturacion != "DMOVFACT")
							{

								if (bTemp)
								{
									stSql = "INSERT INTO dmovfacc " + 
									        "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
									        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
									        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
									        "ginspecc, iregiqui, iautosoc, " + 
									        "iexiauto, ndninifp, fperfact, " + 
									        "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " + 
									        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";
								}
								else
								{
									//                              StSql = "INSERT INTO dmovfact " & _
									//'                                         "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " & _
									//'                                          "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " & _
									//'                                          "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " & _
									//'                                          "ginspecc, iregiqui, iautosoc, " & _
									//'                                          "iexiauto, ndninifp, fperfact, " & _
									//'                                          "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " & _
									//'                                         "VALUES('" & rsIntervencion!gidenpac & "','I',"

									stSql = "INSERT INTO " + stTablaFacturacion + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
									        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
									        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
									        "ginspecc, iregiqui, iautosoc, " + 
									        "iexiauto, ndninifp, fperfact, " + 
									        "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " + 
									        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";
								}


								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
								{
									stSql = stSql + 
									        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
									        "'AY',";
								}
								else
								{
									stSql = stSql + 
									        "'Q'," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
									        "'AY',";
								}

								//servicio solicitante
								stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
								//servicio realizador de la prestacion
								stSql = stSql + tiServicio.ToString() + ",";

								//si no ha traído la autorización necesaria
								//la prestación se cobra como privado
								if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + 
									        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
								}
								else
								{
									stSql = stSql + 
									        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
								}

								//numero de afiliacion
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
								{
									stSql = stSql + "(Null),";
								}
								else
								{
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
								}

								//prestacion, fecha sistema, fecha inicio,fecha fin
								object tempRefParam51 = FechaSistema;
								stSql = stSql + 
								        "'" + PrestacionFactura + "'," + 
								        Convert.ToString(iteration_row["ncantida"]) + "," + 
								        "" + Serrores.FormatFechaHMS(tempRefParam51) + ",";
								FechaSistema = Convert.ToString(tempRefParam51); //& |                                                 '"" & FormatFechaHM(rsIntervencion!finterve) & "," & |                                                 '"" & FormatFechaHM(rsIntervencion!finterve) & ","


								//OSCAR C: 15/07/2005
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
								{
									object tempRefParam52 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam52) + ", ";
								}
								else
								{
									object tempRefParam53 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam53) + ", ";
								}
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
								{
									object tempRefParam54 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam54) + ", ";
								}
								else
								{
									object tempRefParam55 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + Serrores.FormatFechaHM(tempRefParam55) + ", ";
								}
								//--------------


								//inspeccion
								if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
								{
									stSql = stSql + "(Null),";
								}
								else
								{
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
								}

								//regimen quirurgico
								stSql = stSql + "'" + sRegiqui + "',";

								//autorizacion
								if (!Convert.IsDBNull(iteration_row["iautosoc"]))
								{
									stSql = stSql + "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
								}
								else
								{
									stSql = stSql + "(Null),";
								}

								if (!Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
								}
								else
								{
									stSql = stSql + "(Null),";
								}

								//NDNINIFP
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
								{
									stSql = stSql + "(Null),";
								}
								else
								{
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
								}

								//fperfact
								object tempRefParam56 = FechaPerfact;
								stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam56);
								FechaPerfact = Convert.ToString(tempRefParam56);

								stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ",";
								stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudan2"]) + "','" + OrigenQuirurgico + "')";

								SqlCommand tempCommand_7 = new SqlCommand(stSql, Conexion);
								tempCommand_7.ExecuteNonQuery();

							}
							else
							{

								//OSCAR C Abril 2009
								//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
								//de facturacion.
								//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
								//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
								//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
								//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
								//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
								//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
								// NOTA:
								// La condicion de busqueda sera por:
								// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
								// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
								// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
								// y que ademas tuviera la marca de borrado logico (iborrado='S')
								//* No aplica en Quirofanos en este caso
								bEncontrado = false;
								stSql = "SELECT * FROM " + stTablaFacturacion_W;
								stSql = stSql + " WHERE ";
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
								{
									stSql = stSql + " itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
									stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
									stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
								}
								else
								{
									stSql = stSql + " itiposer='Q'";
									stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
									stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
								}
								stSql = stSql + " AND gcodeven ='AY'";
								stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
								stSql = stSql + " AND gserreal=" + tiServicio.ToString();
								if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									stSql = stSql + " AND gsocieda=" + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
								}
								else
								{
									stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
								}
								if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
								{
									stSql = stSql + " AND nafiliac IS NULL";
								}
								else
								{
									stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
								}
								stSql = stSql + " AND gconcfac ='" + PrestacionFactura + "'";
								stSql = stSql + " AND ncantida=" + Convert.ToString(iteration_row["ncantida"]);
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
								{
									object tempRefParam57 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
									stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHM(tempRefParam57);
								}
								else
								{
									object tempRefParam58 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHM(tempRefParam58);
								}
								if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
								{
									object tempRefParam59 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
									stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHM(tempRefParam59);
								}
								else
								{
									object tempRefParam60 = rsIntervencion.Tables[0].Rows[0]["finterve"];
									stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHM(tempRefParam60);
								}
								if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
								{
									stSql = stSql + " AND ginspecc IS NULL";
								}
								else
								{
									stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
								}
								stSql = stSql + " AND iregiqui='" + sRegiqui + "'";
								stSql = stSql + " AND itiporig ='Q'";
								stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
								stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
								stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudan2"]);
								stSql = stSql + " AND iborrado='S'";

								SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(stSql, Conexion);
								RsAux = new DataSet();
								tempAdapter_10.Fill(RsAux);
								if (RsAux.Tables[0].Rows.Count != 0)
								{
									RsAux.MoveFirst();
									bEncontrado = true;
								}

								if (bEncontrado)
								{

									stSql = " UPDATE " + stTablaFacturacion_W + " SET ";
									stSql = stSql + "  gidenpac= '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'";
									stSql = stSql + ", itpacfac='I'";
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
									{
										stSql = stSql + " , itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
										stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
										stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
									}
									else
									{
										stSql = stSql + " , itiposer='Q'";
										stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
										stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
									}
									stSql = stSql + " , gcodeven='AY'";
									stSql = stSql + " , gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
									stSql = stSql + " , gserreal=" + tiServicio.ToString();
									if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
									{
										stSql = stSql + " , gsocieda=" + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
									}
									else
									{
										stSql = stSql + " , gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
									}
									if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
									{
										stSql = stSql + " , nafiliac = (NULL) ";
									}
									else
									{
										stSql = stSql + " , nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
									}

									stSql = stSql + " , gconcfac='" + PrestacionFactura + "'";
									stSql = stSql + " , ncantida=" + Convert.ToString(iteration_row["ncantida"]);
									object tempRefParam61 = FechaSistema;
									stSql = stSql + " , fsistema=" + Serrores.FormatFechaHMS(tempRefParam61);
									FechaSistema = Convert.ToString(tempRefParam61);
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
									{
										object tempRefParam62 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
										stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHM(tempRefParam62);
									}
									else
									{
										object tempRefParam63 = rsIntervencion.Tables[0].Rows[0]["finterve"];
										stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHM(tempRefParam63);
									}
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
									{
										object tempRefParam64 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
										stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHM(tempRefParam64);
									}
									else
									{
										object tempRefParam65 = rsIntervencion.Tables[0].Rows[0]["finterve"];
										stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHM(tempRefParam65);
									}
									if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
									{
										stSql = stSql + " , ginspecc = (Null) ";
									}
									else
									{
										stSql = stSql + " , ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
									}
									stSql = stSql + " , iregiqui='" + sRegiqui + "'";
									if (!Convert.IsDBNull(iteration_row["iautosoc"]))
									{
										stSql = stSql + " , iautosoc='" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "'";
									}
									else
									{
										stSql = stSql + " , iautosoc=(Null)";
									}
									if (!Convert.IsDBNull(iteration_row["iexiauto"]))
									{
										stSql = stSql + " , iexiauto='" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "'";
									}
									else
									{
										stSql = stSql + " , iexiauto=(Null)";
									}
									if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
									{
										stSql = stSql + " , ndninifp = (Null)";
									}
									else
									{
										stSql = stSql + " , ndninifp='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
									}
									object tempRefParam66 = FechaPerfact;
									stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam66) + "";
									FechaPerfact = Convert.ToString(tempRefParam66);
									stSql = stSql + " , itiporig='Q'";
									stSql = stSql + " , ganoorig=" + AñoRegistro.ToString();
									stSql = stSql + " , gnumorig=" + NumeroRegistro.ToString();
									stSql = stSql + " , gpersona=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudan2"]);
									stSql = stSql + " , IBORRADO=(NULL) ";

									stSql = stSql + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);

									SqlCommand tempCommand_8 = new SqlCommand(stSql, Conexion);
									tempCommand_8.ExecuteNonQuery();

								}
								else
								{

									stSql = "INSERT INTO " + stTablaFacturacion_W + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
									        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
									        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
									        "ginspecc, iregiqui, iautosoc, " + 
									        "iexiauto, ndninifp, fperfact, " + 
									        "itiporig, ganoorig, gnumorig, gpersona, iorigqui) " + 
									        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";

									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
									{
										stSql = stSql + 
										        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
										        "'AY',";
									}
									else
									{
										stSql = stSql + 
										        "'Q'," + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
										        "'AY',";
									}

									//servicio solicitante
									stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
									//servicio realizador de la prestacion
									stSql = stSql + tiServicio.ToString() + ",";

									//si no ha traído la autorización necesaria
									//la prestación se cobra como privado
									if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
									{
										stSql = stSql + 
										        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
									}
									else
									{
										stSql = stSql + 
										        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
									}

									//numero de afiliacion
									if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
									{
										stSql = stSql + "(Null),";
									}
									else
									{
										stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
									}

									//prestacion, fecha sistema, fecha inicio,fecha fin
									object tempRefParam67 = FechaSistema;
									stSql = stSql + 
									        "'" + PrestacionFactura + "'," + 
									        Convert.ToString(iteration_row["ncantida"]) + "," + 
									        "" + Serrores.FormatFechaHMS(tempRefParam67) + ",";
									FechaSistema = Convert.ToString(tempRefParam67); //& |                                                 '"" & FormatFechaHM(rsIntervencion!finterve) & "," & |                                                 '"" & FormatFechaHM(rsIntervencion!finterve) & ","

									//OSCAR C: 15/07/2005
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
									{
										object tempRefParam68 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
										stSql = stSql + Serrores.FormatFechaHM(tempRefParam68) + ", ";
									}
									else
									{
										object tempRefParam69 = rsIntervencion.Tables[0].Rows[0]["finterve"];
										stSql = stSql + Serrores.FormatFechaHM(tempRefParam69) + ", ";
									}
									if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
									{
										object tempRefParam70 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
										stSql = stSql + Serrores.FormatFechaHM(tempRefParam70) + ", ";
									}
									else
									{
										object tempRefParam71 = rsIntervencion.Tables[0].Rows[0]["finterve"];
										stSql = stSql + Serrores.FormatFechaHM(tempRefParam71) + ", ";
									}
									//--------------

									//inspeccion
									if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
									{
										stSql = stSql + "(Null),";
									}
									else
									{
										stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
									}

									//regimen quirurgico
									stSql = stSql + "'" + sRegiqui + "',";

									//autorizacion
									if (!Convert.IsDBNull(iteration_row["iautosoc"]))
									{
										stSql = stSql + "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
									}
									else
									{
										stSql = stSql + "(Null),";
									}

									if (!Convert.IsDBNull(iteration_row["iexiauto"]))
									{
										stSql = stSql + "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
									}
									else
									{
										stSql = stSql + "(Null),";
									}

									//NDNINIFP
									if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
									{
										stSql = stSql + "(Null),";
									}
									else
									{
										stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
									}

									//fperfact
									object tempRefParam72 = FechaPerfact;
									stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam72);
									FechaPerfact = Convert.ToString(tempRefParam72);

									stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ",";
									stSql = stSql + "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudan2"]) + "','" + OrigenQuirurgico + "')";

									SqlCommand tempCommand_9 = new SqlCommand(stSql, Conexion);
									tempCommand_9.ExecuteNonQuery();


								}
								RsAux.Close();

							}


						}
						//-------FIN FACTURACION Movimiento del Ayudante del Anestesista ------------------------------------------------
					}

					//-------FACTURACION Movimiento de la Prestancion ----------------------------------------------------OK
					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					if (stTablaFacturacion != "DMOVFACT")
					{

						if (bTemp)
						{
							stSql = "INSERT INTO dmovfacc " + 
							        "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
							        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
							        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
							        "ginspecc, iregiqui, iautosoc, " + 
							        "iexiauto, ndninifp, fperfact, " + 
							        "itiporig, ganoorig, gnumorig,gpersona) " + 
							        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";
						}
						else
						{
							//                StSql = "INSERT INTO dmovfact " & _
							//'                           "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " & _
							//'                            "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " & _
							//'                            "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " & _
							//'                            "ginspecc, iregiqui, iautosoc, " & _
							//'                            "iexiauto, ndninifp, fperfact, " & _
							//'                            "itiporig, ganoorig, gnumorig,gpersona) " & _
							//'                           "VALUES('" & rsIntervencion!gidenpac & "','I',"

							stSql = "INSERT INTO " + stTablaFacturacion + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
							        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
							        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
							        "ginspecc, iregiqui, iautosoc, " + 
							        "iexiauto, ndninifp, fperfact, " + 
							        "itiporig, ganoorig, gnumorig,gpersona) " + 
							        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";
						}


						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
							        "'PR',";
						}
						else
						{
							stSql = stSql + 
							        "'Q'," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
							        "'PR',";
						}

						//servicio solicitante
						stSql = stSql + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
						//servicio realizador de la prestacion

						stSql = stSql + 
						        tiServicio.ToString() + ",";
						// si no ha traído la autorización necesaria
						// la prestación se cobra como privado
						if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
						{
							stSql = stSql + 
							        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
						}
						else
						{
							stSql = stSql + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
						}
						//numero de afiliacion

						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
						{
							stSql = stSql + 
							        "(Null),";
						}
						else
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
						}
						//prestacion, fecha sistema, fecha inicio,fecha fin

						object tempRefParam73 = FechaSistema;
						stSql = stSql + 
						        "'" + PrestacionFactura + "'," + 
						        Convert.ToString(iteration_row["ncantida"]) + "," + 
						        "" + Serrores.FormatFechaHMS(tempRefParam73) + ",";
						FechaSistema = Convert.ToString(tempRefParam73); //& |                                           '"" & FormatFechaHM(rsIntervencion!finterve) & "," & |                                           '"" & FormatFechaHM(rsIntervencion!finterve) & ","

						//OSCAR C: 15/07/2005
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
						{
							object tempRefParam74 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
							stSql = stSql + Serrores.FormatFechaHM(tempRefParam74) + ", ";
						}
						else
						{
							object tempRefParam75 = rsIntervencion.Tables[0].Rows[0]["finterve"];
							stSql = stSql + Serrores.FormatFechaHM(tempRefParam75) + ", ";
						}
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
						{
							object tempRefParam76 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
							stSql = stSql + Serrores.FormatFechaHM(tempRefParam76) + ", ";
						}
						else
						{
							object tempRefParam77 = rsIntervencion.Tables[0].Rows[0]["finterve"];
							stSql = stSql + Serrores.FormatFechaHM(tempRefParam77) + ", ";
						}
						//--------------


						//inspeccion
						if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
						{
							stSql = stSql + 
							        "(Null),";
						}
						else
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
						}

						//regimen quirurgico
						stSql = stSql + 
						        "'" + sRegiqui + "',";

						//autorizacion
						if (!Convert.IsDBNull(iteration_row["iautosoc"]))
						{
							stSql = stSql + 
							        "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
						}
						else
						{
							stSql = stSql + 
							        "(Null),";
						}

						if (!Convert.IsDBNull(iteration_row["iexiauto"]))
						{
							stSql = stSql + 
							        "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
						}
						else
						{
							stSql = stSql + 
							        "(Null),";
						}

						//NDNINIFP
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
						{
							stSql = stSql + 
							        "(Null),";
						}
						else
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
						}

						//fperfact
						object tempRefParam78 = FechaPerfact;
						stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam78);
						FechaPerfact = Convert.ToString(tempRefParam78);

						//oscar 29/10/03
						stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ",";
						//-------

						//OSCAR C 15/07/2005
						if (bCapturarResponsable)
						{
							stSql = stSql + Convert.ToString(iteration_row["gperreal"]);
						}
						else
						{
							stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
						}
						stSql = stSql + ")";
						//----------


						SqlCommand tempCommand_10 = new SqlCommand(stSql, Conexion);
						tempCommand_10.ExecuteNonQuery();

					}
					else
					{

						//OSCAR C Abril 2009
						//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
						//de facturacion.
						//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
						//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
						//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
						//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
						//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
						//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
						// NOTA:
						// La condicion de busqueda sera por:
						// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
						// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
						// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
						// y que ademas tuviera la marca de borrado logico (iborrado='S')
						//* No aplica en Quirofanos en este caso
						bEncontrado = false;
						stSql = "SELECT * FROM " + stTablaFacturacion_W;
						stSql = stSql + " WHERE ";
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
						{
							stSql = stSql + " itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
							stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
							stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						}
						else
						{
							stSql = stSql + " itiposer='Q'";
							stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
							stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						}
						stSql = stSql + " AND gcodeven ='PR'";
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
						stSql = stSql + " AND gserreal=" + tiServicio.ToString();
						if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
						{
							stSql = stSql + " AND gsocieda=" + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
						}
						else
						{
							stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
						}
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
						{
							stSql = stSql + " AND nafiliac IS NULL";
						}
						else
						{
							stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
						}
						stSql = stSql + " AND gconcfac ='" + PrestacionFactura + "'";
						stSql = stSql + " AND ncantida=" + Convert.ToString(iteration_row["ncantida"]);
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
						{
							object tempRefParam79 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
							stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHM(tempRefParam79);
						}
						else
						{
							object tempRefParam80 = rsIntervencion.Tables[0].Rows[0]["finterve"];
							stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHM(tempRefParam80);
						}
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
						{
							object tempRefParam81 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
							stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHM(tempRefParam81);
						}
						else
						{
							object tempRefParam82 = rsIntervencion.Tables[0].Rows[0]["finterve"];
							stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHM(tempRefParam82);
						}
						if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
						{
							stSql = stSql + " AND ginspecc IS NULL";
						}
						else
						{
							stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
						}
						stSql = stSql + " AND iregiqui='" + sRegiqui + "'";
						stSql = stSql + " AND itiporig ='Q'";
						stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
						stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
						if (bCapturarResponsable)
						{
							stSql = stSql + " AND gpersona =" + Convert.ToString(iteration_row["gperreal"]);
						}
						else
						{
							stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
						}
						stSql = stSql + " AND iborrado='S'";

						SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(stSql, Conexion);
						RsAux = new DataSet();
						tempAdapter_11.Fill(RsAux);
						if (RsAux.Tables[0].Rows.Count != 0)
						{
							RsAux.MoveFirst();
							bEncontrado = true;
						}


						if (bEncontrado)
						{

							stSql = " UPDATE " + stTablaFacturacion_W + " SET ";
							stSql = stSql + "  gidenpac= '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'";
							stSql = stSql + ", itpacfac='I'";
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
							{
								stSql = stSql + " , itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
								stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
								stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
							}
							else
							{
								stSql = stSql + " , itiposer='Q'";
								stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
								stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
							}
							stSql = stSql + " , gcodeven='PR'";
							stSql = stSql + " , gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
							stSql = stSql + " , gserreal=" + tiServicio.ToString();
							if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
							{
								stSql = stSql + " , gsocieda=" + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
							}
							else
							{
								stSql = stSql + " , gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
							}
							if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
							{
								stSql = stSql + " , nafiliac = (NULL) ";
							}
							else
							{
								stSql = stSql + " , nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
							}

							stSql = stSql + " , gconcfac='" + PrestacionFactura + "'";
							stSql = stSql + " , ncantida=" + Convert.ToString(iteration_row["ncantida"]);
							object tempRefParam83 = FechaSistema;
							stSql = stSql + " , fsistema=" + Serrores.FormatFechaHMS(tempRefParam83);
							FechaSistema = Convert.ToString(tempRefParam83);
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
							{
								object tempRefParam84 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
								stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHM(tempRefParam84);
							}
							else
							{
								object tempRefParam85 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHM(tempRefParam85);
							}
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
							{
								object tempRefParam86 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
								stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHM(tempRefParam86);
							}
							else
							{
								object tempRefParam87 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHM(tempRefParam87);
							}
							if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
							{
								stSql = stSql + " , ginspecc = (Null) ";
							}
							else
							{
								stSql = stSql + " , ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
							}
							stSql = stSql + " , iregiqui='" + sRegiqui + "'";
							if (!Convert.IsDBNull(iteration_row["iautosoc"]))
							{
								stSql = stSql + " , iautosoc='" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "'";
							}
							else
							{
								stSql = stSql + " , iautosoc=(Null)";
							}
							if (!Convert.IsDBNull(iteration_row["iexiauto"]))
							{
								stSql = stSql + " , iexiauto='" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "'";
							}
							else
							{
								stSql = stSql + " , iexiauto=(Null)";
							}
							if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
							{
								stSql = stSql + " , ndninifp = (Null)";
							}
							else
							{
								stSql = stSql + " , ndninifp='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
							}
							object tempRefParam88 = FechaPerfact;
							stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam88) + "";
							FechaPerfact = Convert.ToString(tempRefParam88);
							stSql = stSql + " , itiporig='Q'";
							stSql = stSql + " , ganoorig=" + AñoRegistro.ToString();
							stSql = stSql + " , gnumorig=" + NumeroRegistro.ToString();
							if (bCapturarResponsable)
							{
								stSql = stSql + " , gpersona=" + Convert.ToString(iteration_row["gperreal"]);
							}
							else
							{
								stSql = stSql + " , gpersona=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
							}
							stSql = stSql + " , IBORRADO=(NULL) ";

							stSql = stSql + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);

							SqlCommand tempCommand_11 = new SqlCommand(stSql, Conexion);
							tempCommand_11.ExecuteNonQuery();


						}
						else
						{


							stSql = "INSERT INTO " + stTablaFacturacion_W + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
							        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
							        "gconcfac, ncantida, fsistema, ffinicio, ffechfin, " + 
							        "ginspecc, iregiqui, iautosoc, " + 
							        "iexiauto, ndninifp, fperfact, " + 
							        "itiporig, ganoorig, gnumorig,gpersona) " + 
							        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "','I',";


							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
							{
								stSql = stSql + 
								        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
								        "'PR',";
							}
							else
							{
								stSql = stSql + 
								        "'Q'," + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
								        "'PR',";
							}

							//servicio solicitante
							stSql = stSql + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + ",";
							//servicio realizador de la prestacion

							stSql = stSql + 
							        tiServicio.ToString() + ",";
							// si no ha traído la autorización necesaria
							// la prestación se cobra como privado
							if (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"]))
							{
								stSql = stSql + 
								        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
							}
							else
							{
								stSql = stSql + 
								        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";
							}
							//numero de afiliacion

							if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) || (!Convert.IsDBNull(iteration_row["iautosoc"]) && Convert.IsDBNull(iteration_row["iexiauto"])))
							{
								stSql = stSql + 
								        "(Null),";
							}
							else
							{
								stSql = stSql + 
								        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
							}
							//prestacion, fecha sistema, fecha inicio,fecha fin

							object tempRefParam89 = FechaSistema;
							stSql = stSql + 
							        "'" + PrestacionFactura + "'," + 
							        Convert.ToString(iteration_row["ncantida"]) + "," + 
							        "" + Serrores.FormatFechaHMS(tempRefParam89) + ",";
							FechaSistema = Convert.ToString(tempRefParam89); //& |                                           '"" & FormatFechaHM(rsIntervencion!finterve) & "," & |                                           '"" & FormatFechaHM(rsIntervencion!finterve) & ","

							//OSCAR C: 15/07/2005
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
							{
								object tempRefParam90 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
								stSql = stSql + Serrores.FormatFechaHM(tempRefParam90) + ", ";
							}
							else
							{
								object tempRefParam91 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + Serrores.FormatFechaHM(tempRefParam91) + ", ";
							}
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
							{
								object tempRefParam92 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
								stSql = stSql + Serrores.FormatFechaHM(tempRefParam92) + ", ";
							}
							else
							{
								object tempRefParam93 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + Serrores.FormatFechaHM(tempRefParam93) + ", ";
							}
							//--------------


							//inspeccion
							if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
							{
								stSql = stSql + 
								        "(Null),";
							}
							else
							{
								stSql = stSql + 
								        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
							}

							//regimen quirurgico
							stSql = stSql + 
							        "'" + sRegiqui + "',";

							//autorizacion
							if (!Convert.IsDBNull(iteration_row["iautosoc"]))
							{
								stSql = stSql + 
								        "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
							}
							else
							{
								stSql = stSql + 
								        "(Null),";
							}

							if (!Convert.IsDBNull(iteration_row["iexiauto"]))
							{
								stSql = stSql + 
								        "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
							}
							else
							{
								stSql = stSql + 
								        "(Null),";
							}

							//NDNINIFP
							if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
							{
								stSql = stSql + 
								        "(Null),";
							}
							else
							{
								stSql = stSql + 
								        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
							}

							//fperfact
							object tempRefParam94 = FechaPerfact;
							stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam94);
							FechaPerfact = Convert.ToString(tempRefParam94);

							//oscar 29/10/03
							stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ",";
							//-------

							//OSCAR C 15/07/2005
							if (bCapturarResponsable)
							{
								stSql = stSql + Convert.ToString(iteration_row["gperreal"]);
							}
							else
							{
								stSql = stSql + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
							}
							stSql = stSql + ")";
							//----------


							SqlCommand tempCommand_12 = new SqlCommand(stSql, Conexion);
							tempCommand_12.ExecuteNonQuery();

						}

						RsAux.Close();

					}
					//-------FIN FACTURACION Movimiento de la Prestacion ----------------------------------------------------

					if (ExisteProtocolo)
					{
						iMoveFirst = rsDanalsol.MoveNext();
						if (rsDanalsol.Tables[0].Rows.Count != 0)
						{
                            PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[iMoveFirst]["gpruelab"]);
						}
						else
						{
							Continua_Bucle = false;
							rsDanalsol.Close();
						}
					}

				}

				rsDcodpres.Edit();
				iteration_row["fpasfact"] = FechaPerfact;
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(rsDcodpres, rsDcodpres.Tables[0].TableName);

				///****************************


			}

			rsDcodpres.Close();

		}

		private void proFacturacionPediatrayAyudante()
		{
			bool bEncontrado = false;
			//--------facturacion movimiento para el pediatra ----------------------------------------------OK
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gpediat1"]))
			{
				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "SELECT * " + 
						        "FROM dmovfacc " + 
						        "WHERE 1 = 2";
					}
					else
					{
						//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
						stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
					}

					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter.Fill(rsFactura);
					rsFactura.AddNew();
					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gpediat1"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
					tempAdapter.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}
				else
				{

					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						stSql = stSql + " AND gsersoli IS NULL";
					}
					stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
					{
						object tempRefParam = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam);
					}
					else
					{
						object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam2);
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
					{
						object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam3);
					}
					else
					{
						object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam4);
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
					{
						stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
					}
					else
					{
						stSql = stSql + " AND gdiagalt IS NULL";
					}
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
					{
						stSql = stSql + " AND iregiqui='C'";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
						{
							stSql = stSql + " AND iregiqui='A'";
						}
						else
						{
							stSql = stSql + " AND iregiqui='S'";
						}
					}
					stSql = stSql + " AND gcodeven ='AY'";
					stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gpediat1"]);
					stSql = stSql + " AND ncantida =1";
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_3.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}
                    SqlDataAdapter tempAdapter_4 = null;

                    if (bEncontrado)
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
						tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_4.Fill(rsFactura);
						rsFactura.Edit();
						rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
					}
					else
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
						tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_4.Fill(rsFactura);
						rsFactura.AddNew();
					}

					RsAux.Close();

					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gpediat1"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
					tempAdapter_4.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}

			}
			//--------FIN facturacion movimiento para el pediatra ---------------------------------------------

			//--------facturacion movimiento para el ayudante del pediatra ---------------------------------OK
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gpediat2"]))
			{
				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "SELECT * " + 
						        "FROM dmovfacc " + 
						        "WHERE 1 = 2";
					}
					else
					{
						//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
						stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
					}

					SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_7.Fill(rsFactura);
					rsFactura.AddNew();
					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gpediat2"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_7);
                    tempAdapter_7.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}
				else
				{

					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						stSql = stSql + " AND gsersoli IS NULL";
					}
					stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
					{
						object tempRefParam5 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam5);
					}
					else
					{
						object tempRefParam6 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam6);
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
					{
						object tempRefParam7 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam7);
					}
					else
					{
						object tempRefParam8 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam8);
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
					{
						stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
					}
					else
					{
						stSql = stSql + " AND gdiagalt IS NULL";
					}
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
					{
						stSql = stSql + " AND iregiqui='C'";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
						{
							stSql = stSql + " AND iregiqui='A'";
						}
						else
						{
							stSql = stSql + " AND iregiqui='S'";
						}
					}
					stSql = stSql + " AND gcodeven ='AY'";
					stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gpediat2"]);
					stSql = stSql + " AND ncantida =1";
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_9.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}
                    SqlDataAdapter tempAdapter_10 = null;

                    if (bEncontrado)
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
						tempAdapter_10 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_10.Fill(rsFactura);
						rsFactura.Edit();
						rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
					}
					else
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
						tempAdapter_10 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_10.Fill(rsFactura);
						rsFactura.AddNew();
					}

					RsAux.Close();

					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gpediat2"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_10);
                    tempAdapter_10.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}

			}
			//--------FIN facturacion movimiento para el ayudante del pediatra --------------------------------

		}


		private void proFacturacionProcedimientoPostOperatorio()
		{
			bool bEncontrado = false;
			//-----------------facturacion procedimiento postoperatorio de la intervencion------------------ OK
			//OSCAR C Abril 2009
			//COMENTARIOS GENERALES
			//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
			//de facturacion.
			//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
			//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
			//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
			//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
			//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
			//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
			if (stTablaFacturacion != "DMOVFACT")
			{

				if (bTemp)
				{
					stSql = "SELECT * " + 
					        "FROM dmovfacc " + 
					        "WHERE 1 = 2";
				}
				else
				{
					//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
					stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
				}

				//Si la intervencion estaba programada y la programacion fue anterior
				//al ingreso o es ambulante se considera programado. En caso contrario
				//se considera inmediato
				OrigenQuirurgico = "H";
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ganoprog"]))
				{
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						OrigenQuirurgico = "P";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) == "H")
						{
							stSql2 = "SELECT fentrada " + 
							         "FROM qprogqui " + 
							         "WHERE ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprog"]) + " AND " + 
							         "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprog"]);
							SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql2, Conexion);
							rsProgramacion = new DataSet();
							tempAdapter.Fill(rsProgramacion);
							stSql2 = "SELECT fllegada " + 
							         "FROM aepisadm " + 
							         "WHERE ganoadme = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
							         "gnumadme = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql2, Conexion);
							rsIngreso = new DataSet();
							tempAdapter_2.Fill(rsIngreso);
							if (rsIngreso.Tables[0].Rows.Count != 0)
							{
								if (Convert.ToDateTime(rsProgramacion.Tables[0].Rows[0]["fentrada"]) < Convert.ToDateTime(rsIngreso.Tables[0].Rows[0]["fllegada"]))
								{
									OrigenQuirurgico = "P";
								}
							}
						}
					}
				}

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
				rsFactura = new DataSet();
				tempAdapter_3.Fill(rsFactura);
				rsFactura.AddNew();
				InicializarDatos();
				rsFactura.Tables[0].Rows[0]["gcodeven"] = "PR";
				rsFactura.Tables[0].Rows[0]["gconcfac"] = rsIntervencion.Tables[0].Rows[0]["gprocpos"];
				rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gmedicoc"];
				rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
				//oscar 29/10/03
				rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
				rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
				rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
				//-----
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
                tempAdapter_3.Update(rsFactura, rsFactura.Tables[0].TableName);
				rsFactura.Close();
			}
			else
			{

				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				// NOTA:
				// La condicion de busqueda sera por:
				// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
				// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
				// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
				// y que ademas tuviera la marca de borrado logico (iborrado='S')
				//* No aplica en Quirofanos en este caso
				bEncontrado = false;

				stSql = "SELECT * FROM " + stTablaFacturacion_W;
				stSql = stSql + " WHERE ";
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
				{
					stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
					stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
					stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
					stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
				}
				else
				{
					stSql = stSql + "  itiposer='Q'";
					stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
					stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
					stSql = stSql + " AND gsersoli IS NULL";
				}
				stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
				stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
				if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
				{
					stSql = stSql + " AND nafiliac IS NULL";
				}
				else
				{
					stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
				}
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
				{
					object tempRefParam = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
					stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam);
				}
				else
				{
					object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam2);
				}
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
				{
					object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
					stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam3);
				}
				else
				{
					object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam4);
				}
				stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
				{
					stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
				}
				else
				{
					stSql = stSql + " AND gdiagalt IS NULL";
				}
				if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
				{
					stSql = stSql + " AND ginspecc IS NULL";
				}
				else
				{
					stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
				}
				if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
				{
					stSql = stSql + " AND iregiqui='C'";
				}
				else
				{
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
					{
						stSql = stSql + " AND iregiqui='A'";
					}
					else
					{
						stSql = stSql + " AND iregiqui='S'";
					}
				}
				stSql = stSql + " AND gcodeven ='PR'";
				stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
				stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
				stSql = stSql + " AND ncantida =1";
				stSql = stSql + " AND itiporig ='Q'";
				stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
				stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
				stSql = stSql + " AND iborrado='S'";

				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSql, Conexion);
				RsAux = new DataSet();
				tempAdapter_5.Fill(RsAux);
				if (RsAux.Tables[0].Rows.Count != 0)
				{
					RsAux.MoveFirst();
					bEncontrado = true;
				}
                SqlDataAdapter tempAdapter_6 = null;

                if (bEncontrado)
				{
					stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
					tempAdapter_6 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_6.Fill(rsFactura);
					rsFactura.Edit();
					rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
				}
				else
				{
					stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
                    tempAdapter_6 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
                    tempAdapter_6.Fill(rsFactura);
					rsFactura.AddNew();
				}

				RsAux.Close();


				//Si la intervencion estaba programada y la programacion fue anterior
				//al ingreso o es ambulante se considera programado. En caso contrario
				//se considera inmediato
				OrigenQuirurgico = "H";
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ganoprog"]))
				{
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						OrigenQuirurgico = "P";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) == "H")
						{
							stSql2 = "SELECT fentrada " + 
							         "FROM qprogqui " + 
							         "WHERE ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprog"]) + " AND " + 
							         "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprog"]);
							SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(stSql2, Conexion);
							rsProgramacion = new DataSet();
							tempAdapter_8.Fill(rsProgramacion);
							stSql2 = "SELECT fllegada " + 
							         "FROM aepisadm " + 
							         "WHERE ganoadme = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
							         "gnumadme = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
							SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql2, Conexion);
							rsIngreso = new DataSet();
							tempAdapter_9.Fill(rsIngreso);
							if (rsIngreso.Tables[0].Rows.Count != 0)
							{
								if (Convert.ToDateTime(rsProgramacion.Tables[0].Rows[0]["fentrada"]) < Convert.ToDateTime(rsIngreso.Tables[0].Rows[0]["fllegada"]))
								{
									OrigenQuirurgico = "P";
								}
							}
						}
					}
				}

				InicializarDatos();
				rsFactura.Tables[0].Rows[0]["gcodeven"] = "PR";
				rsFactura.Tables[0].Rows[0]["gconcfac"] = rsIntervencion.Tables[0].Rows[0]["gprocpos"];
				rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gmedicoc"];
				rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
				//oscar 29/10/03
				rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
				rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
				rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
				//-----
				SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_6);
                tempAdapter_6.Update(rsFactura, rsFactura.Tables[0].TableName);
				rsFactura.Close();

			}
			//-------FIN facturacion procedimiento postoperatorio de la intervencion-------------------------------
		}
		private bool HayProtocolo(string Prestacion, SqlConnection Conexion)
		{
			string sql = "select * from dprotlab where gprestac='" + Prestacion + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet rsProtocolo = new DataSet();
			tempAdapter.Fill(rsProtocolo);
			if (rsProtocolo.Tables[0].Rows.Count != 0)
			{
				sql = "select * from dcodpres where gprestac='" + Prestacion + "' and iprotcer = 'C' ";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
				rsProtocolo = new DataSet();
				tempAdapter_2.Fill(rsProtocolo);
				if (rsProtocolo.Tables[0].Rows.Count != 0)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return false;
			}

		}
		private void InicializarDatos()
		{

			rsFactura.Tables[0].Rows[0]["gidenpac"] = rsIntervencion.Tables[0].Rows[0]["gidenpac"];

			// Si el paciente no estaba hospitalizado la prestación principal
			// se graba como externa. Si estaba hospitalizado se graba como interna
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
			{
				rsFactura.Tables[0].Rows[0]["itpacfac"] = "I";
				rsFactura.Tables[0].Rows[0]["itiposer"] = rsIntervencion.Tables[0].Rows[0]["itiposer"];
				rsFactura.Tables[0].Rows[0]["ganoregi"] = rsIntervencion.Tables[0].Rows[0]["ganoprin"];
				rsFactura.Tables[0].Rows[0]["gnumregi"] = rsIntervencion.Tables[0].Rows[0]["gnumprin"];
				rsFactura.Tables[0].Rows[0]["gsersoli"] = rsIntervencion.Tables[0].Rows[0]["gservici"]; // en principio el servicio que opera
				// pendiente de confirmar
			}
			else
			{
				rsFactura.Tables[0].Rows[0]["itpacfac"] = "E";
				rsFactura.Tables[0].Rows[0]["itiposer"] = "Q";
				rsFactura.Tables[0].Rows[0]["ganoregi"] = rsIntervencion.Tables[0].Rows[0]["ganoregi"];
				rsFactura.Tables[0].Rows[0]["gnumregi"] = rsIntervencion.Tables[0].Rows[0]["gnumregi"];
				// 4/11/1999: en los ambulantes no hay servicio solicitante
				rsFactura.Tables[0].Rows[0]["gsersoli"] = DBNull.Value;
			}
			if (FechaPerfact == "")
			{
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
				{
					rsFactura.Tables[0].Rows[0]["fperfact"] = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
				}
				else
				{
					rsFactura.Tables[0].Rows[0]["fperfact"] = rsIntervencion.Tables[0].Rows[0]["finterve"];
				}
			}
			else
			{
				rsFactura.Tables[0].Rows[0]["fperfact"] = DateTime.Parse(FechaPerfact); //procambiadia_mes(FechaPerfact)
			}
			rsFactura.Tables[0].Rows[0]["gserreal"] = rsIntervencion.Tables[0].Rows[0]["gservici"];
			rsFactura.Tables[0].Rows[0]["gsocieda"] = rsIntervencion.Tables[0].Rows[0]["gsocieda"];
			if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
			{
				rsFactura.Tables[0].Rows[0]["nafiliac"] = DBNull.Value;
			}
			else
			{
				rsFactura.Tables[0].Rows[0]["nafiliac"] = rsIntervencion.Tables[0].Rows[0]["nafiliac"];
			}

			rsFactura.Tables[0].Rows[0]["fsistema"] = FechaSistema;
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
			{
				rsFactura.Tables[0].Rows[0]["ffinicio"] = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
			}
			else
			{
				rsFactura.Tables[0].Rows[0]["ffinicio"] = rsIntervencion.Tables[0].Rows[0]["finterve"];
			}
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
			{
				rsFactura.Tables[0].Rows[0]["ffechfin"] = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
			}
			else
			{
				rsFactura.Tables[0].Rows[0]["ffechfin"] = rsIntervencion.Tables[0].Rows[0]["finterve"];
			}
			rsFactura.Tables[0].Rows[0]["gconcfac"] = rsIntervencion.Tables[0].Rows[0]["gprocpos"];
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
			{
				rsFactura.Tables[0].Rows[0]["gdiagalt"] = rsIntervencion.Tables[0].Rows[0]["gdiagpos"];
			}
			else
			{
				rsFactura.Tables[0].Rows[0]["gdiagalt"] = DBNull.Value;
			}
			if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
			{
				rsFactura.Tables[0].Rows[0]["ginspecc"] = DBNull.Value;
			}
			else
			{
				rsFactura.Tables[0].Rows[0]["ginspecc"] = rsIntervencion.Tables[0].Rows[0]["ginspecc"];
			}

			rsFactura.Tables[0].Rows[0]["iorigqui"] = OrigenQuirurgico;

			if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
			{
				rsFactura.Tables[0].Rows[0]["iregiqui"] = "C";
			}
			else
			{
				if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
				{
					rsFactura.Tables[0].Rows[0]["iregiqui"] = "A";
				}
				else
				{
					rsFactura.Tables[0].Rows[0]["iregiqui"] = "S";
				}
			}
			sRegiqui = Convert.ToString(rsFactura.Tables[0].Rows[0]["iregiqui"]);
			if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
			{
				rsFactura.Tables[0].Rows[0]["ndninifp"] = DBNull.Value;
			}
			else
			{
				rsFactura.Tables[0].Rows[0]["ndninifp"] = rsIntervencion.Tables[0].Rows[0]["nafiliac"];
			}

		}

		public bool Facturado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, dynamic Formulario, ref int lErr, ref string stErr)
		{
			bool result = false;
			DataSet rsMaterial = null;

			try
			{

				Serrores.vfMostrandoHistoricos = true;
				lErr = 0;
				stErr = "";

				Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
				// Se obtiene los datos de la intervencion
				stSql = "SELECT qintquir.* " + 
				        "FROM qintquir " + 
				        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
				        "gnumregi = " + NumeroRegistro.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
				rsIntervencion = new DataSet();
				tempAdapter.Fill(rsIntervencion);

				// Si el procedimiento postoperatorio se ha pasado a facturacion se
				// comprueba si esta marcada ya como facturada
				if (rsIntervencion.Tables[0].Rows.Count != 0 && !Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["fpasfact"]))
				{

					stSql = "SELECT ffactura " + 
					        "FROM dmovfact ";

					// Si el paciente estaba hospitalizado se accede con el episodio
					// principal. En caso contrario se accede con el de quirofanos
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + 
						        "WHERE itiposer = '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + " ' AND " + 
						        "ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
						        "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + " AND ";
					}
					else
					{
						stSql = stSql + 
						        "WHERE itiposer = 'Q' AND " + 
						        "ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
						        "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + " AND ";
					}
					object tempRefParam = rsIntervencion.Tables[0].Rows[0]["fpasfact"];
					object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + 
					        "gcodeven = 'PR' AND " + 
					        "gconcfac = '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "' AND " + 
					        "fsistema = " + Serrores.FormatFechaHMS(tempRefParam) + " AND " + 
					        "ffinicio = " + Serrores.FormatFechaHMS(tempRefParam2) + "";

					string tempRefParam3 = "DMOVFACT";
					Serrores.PonerVistas(ref stSql, ref tempRefParam3, Conexion);

					if (Serrores.proExisteTabla("DMOVFACH", Conexion))
					{
						string tempRefParam4 = "DMOVFACT";
						stSql = stSql + " UNION ALL " + Serrores.proReplace(ref stSql, tempRefParam4, "DMOVFACH");
					}

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_2.Fill(rsFactura);
					//If Not rsFactura.EOF And Not IsNull(rsFactura!ffactura) Then
					//   Facturado = True
					//   rsFactura.Close
					//   Exit Function
					//End If
					foreach (DataRow iteration_row in rsFactura.Tables[0].Rows)
					{
						if (!Convert.IsDBNull(iteration_row["ffactura"]))
						{
							result = true;
							rsFactura.Close();
							return result;
						}
					}

				}
				else
				{

					// Se comprueba si los materiales utilizados en la intervencion
					// estan marcados como facturados

					stSql = "SELECT qprotesi.gtipoart, qprotesi.gprotesi AS articulo, qprotesi.fpasfact, 'N' AS ifarmaco " + 
					        "FROM qprotesi " + 
					        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
					        "gnumregi = " + NumeroRegistro.ToString() + " AND " + 
					        "iproreal = 'R' " + 
					        "UNION ALL " + 
					        "SELECT qmatutil.gtipoart, qmatutil.gcodiart AS articulo, qmatutil.fpasfact, " + 
					        "dtipoart.ifarmaco " + 
					        "FROM qmatutil, dtipoart " + 
					        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
					        "gnumregi = " + NumeroRegistro.ToString() + " AND " + 
					        "qmatutil.gtipoart = dtipoart.gtipoart";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
					rsMaterial = new DataSet();
					tempAdapter_3.Fill(rsMaterial);

					foreach (DataRow iteration_row_2 in rsMaterial.Tables[0].Rows)
					{

						if (!Convert.IsDBNull(iteration_row_2["fpasfact"]))
						{

							stSql = "SELECT ffactura " + 
							        "FROM dmovfact ";

							// Si el paciente estaba hospitalizado se accede con el episodio
							// principal. En caso contrario se accede con el de consultas
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
							{
								stSql = stSql + 
								        "WHERE itiposer = '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + " ' AND " + 
								        "ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
								        "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + " AND ";
							}
							else
							{
								stSql = stSql + 
								        "WHERE itiposer = 'Q' AND " + 
								        "ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
								        "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + " AND ";
							}
							if (Convert.ToString(iteration_row_2["ifarmaco"]) == "S")
							{
								stSql = stSql + 
								        "gcodeven = 'FA' AND " + 
								        "gcodiart = '" + Convert.ToString(iteration_row_2["articulo"]) + "' AND ";
							}
							else
							{
								stSql = stSql + 
								        "gcodeven = 'AL' AND " + 
								        "gcodiart = '" + Convert.ToString(iteration_row_2["articulo"]) + "' AND ";

								//               stSql = stSql & _
								//'                            "gcodeven = 'AL' AND " & _
								//'                            "gconcfac = '" & rsMaterial!gtipoart & rsMaterial!articulo & "' AND "
							}
							object tempRefParam5 = iteration_row_2["fpasfact"];
							object tempRefParam6 = rsIntervencion.Tables[0].Rows[0]["finterve"];
							stSql = stSql + 
							        "fsistema = " + Serrores.FormatFechaHMS(tempRefParam5) + " AND " + 
							        "ffinicio = " + Serrores.FormatFechaHMS(tempRefParam6) + "";

							string tempRefParam7 = "DMOVFACT";
							Serrores.PonerVistas(ref stSql, ref tempRefParam7, Conexion);

							if (Serrores.proExisteTabla("DMOVFACH", Conexion))
							{
								string tempRefParam8 = "DMOVFACT";
								stSql = stSql + " UNION ALL " + Serrores.proReplace(ref stSql, tempRefParam8, "DMOVFACH");
							}

							SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
							rsFactura = new DataSet();
							tempAdapter_4.Fill(rsFactura);
							//If Not rsFactura.EOF Then
							//    If Not IsNull(rsFactura!ffactura) Then
							//       Facturado = True
							//       rsFactura.Close
							//       Exit Function
							//    End If
							//End If
							foreach (DataRow iteration_row_3 in rsFactura.Tables[0].Rows)
							{
								if (!Convert.IsDBNull(iteration_row_3["ffactura"]))
								{
									result = true;
									rsFactura.Close();
									return result;
								}
							}
						}


					}

				}


				if (Formulario != null)
				{
					Formulario.ProRecibirError(null, null);
				}
				return result;
			}
			catch (System.Exception excep)
			{

				Serrores.GrabaLog(DateTimeHelper.ToString(DateTime.Now) + " FACTURADO - " + Information.Err().Number.ToString() + " - " + excep.Message, Path.GetDirectoryName(Application.ExecutablePath) + "\\ErroresFacturacionQuirofanos.log");
				if (Formulario == null)
				{
					lErr = Information.Err().Number;
					stErr = excep.Message;
				}
				else
				{
					Formulario.ProRecibirError(Information.Err().Number, excep.Message);
				}

				return result;
			}
		}

		public bool Facturado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario, ref int lErr)
		{
			string tempRefParam10 = String.Empty;
			return Facturado(AñoRegistro, NumeroRegistro, Conexion, Formulario, ref lErr, ref tempRefParam10);
		}

		public bool Facturado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario)
		{
			int tempRefParam11 = 0;
			string tempRefParam12 = String.Empty;
			return Facturado(AñoRegistro, NumeroRegistro, Conexion, Formulario,ref tempRefParam11,ref tempRefParam12);
		}

		public bool Facturado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion)
		{
			int tempRefParam13 = 0;
			string tempRefParam14 = String.Empty;
			return Facturado(AñoRegistro, NumeroRegistro, Conexion, null,ref  tempRefParam13,ref tempRefParam14);
		}

		//Public Function Estado1(AñoRegistro As Integer, NumeroRegistro As Long, Conexion As rdoConnection, Optional Formulario As Object, Optional BylErr As Long, Optional BystErr As String) As String
		//
		//Dim rsIntervencion As rdoResultset
		//Dim rsFactura As rdoResultset
		//Dim rsMaterial As rdoResultset
		//Dim rsOtrasPR  As rdoResultset
		//
		//   On Error GoTo etiqueta
		//   Obtener_TipoBD_FormatoFechaBD Conexion
		//   Estado = "PENDIENTE"
		//
		//   ' Se obtiene los datos de la intervencion
		//   stSql = "SELECT qintquir.* " & _
		//'              "FROM qintquir " & _
		//'              "WHERE ganoregi = " & AñoRegistro & " AND " & _
		//'                    "gnumregi = " & NumeroRegistro
		//   Set rsIntervencion = Conexion.OpenResultset(stSql, rdOpenForwardOnly)
		//
		//   ' Si el procedimiento postoperatorio se ha pasado a facturacion se
		//   ' comprueba si esta marcada ya como facturada. Si no lo esta se borra
		//   If Not rsIntervencion.EOF And Not IsNull(rsIntervencion!fpasfact) Then
		//
		//      stSql = "SELECT ffactura " & _
		//'                 "FROM dmovfact "
		//
		//      ' Si el paciente estaba hospitalizado se accede con el episodio
		//      ' principal. En caso contrario se accede con el de consultas
		//      If Not IsNull(rsIntervencion!itiposer) Then
		//         stSql = stSql & _
		//'                 "WHERE itiposer = '" & rsIntervencion!itiposer & "' AND " & _
		//'                       "ganoregi = " & rsIntervencion!ganoprin & " AND " & _
		//'                       "gnumregi = " & rsIntervencion!gnumprin & " AND "
		//          stSql = stSql & _
		//'                      "fsistema = " & FormatFechaHMS(rsIntervencion!fpasfact) & " AND " & _
		//'                      "ffinicio = " & FormatFechaHMS(rsIntervencion!hpasoqui) & " and " & _
		//'                      "gconcfac = '" & Trim(rsIntervencion!gprocpos) & "'"
		//
		//      Else
		//         stSql = stSql & _
		//'                "WHERE itiposer = 'Q' AND " & _
		//'                      "ganoregi = " & rsIntervencion!ganoregi & " AND " & _
		//'                      "gnumregi = " & rsIntervencion!gnumregi & "  "
		//'        stSql = stSql & _
		//''                      "and fsistema = " & FormatFechaHMS(rsIntervencion!fpasfact) & " and " & _
		//''                      "gconcfac = '" & Trim(rsIntervencion!gprocpos) & "'"
		//
		//      End If
		//
		//      Set rsFactura = Conexion.OpenResultset(stSql, rdOpenKeyset, rdConcurRowVer, rdExecDirect)
		//      While Not rsFactura.EOF
		//         If Not IsNull(rsFactura!ffactura) Then
		//            Estado = "FACTURADO"
		//            Exit Function
		//         Else
		//            Estado = "GENERADO"
		//         End If
		//         rsFactura.MoveNext
		//      Wend
		//
		//      rsFactura.Close
		//
		//      ' Se borran las filas anteriores
		//      stSql = "DELETE FROM dmovfact "
		//      ' Si el paciente estaba hospitalizado se accede con el episodio
		//      ' principal. En caso contrario se accede con el de consultas
		//      If Not IsNull(rsIntervencion!itiposer) Then
		//         stSql = stSql & _
		//'                 "WHERE itiposer = '" & rsIntervencion!itiposer & " ' AND " & _
		//'                       "ganoregi = " & rsIntervencion!ganoprin & " AND " & _
		//'                       "gnumregi = " & rsIntervencion!gnumprin & " AND "
		//        stSql = stSql & _
		//'                      "fsistema = " & FormatFechaHMS(rsIntervencion!fpasfact) & " AND " & _
		//'                      "ffinicio = " & FormatFechaHMS(rsIntervencion!hpasoqui) & " and " & _
		//'                      "gconcfac = '" & Trim(rsIntervencion!gprocpos) & "'"
		//      Else
		//         stSql = stSql & _
		//'                "WHERE itiposer = 'Q' AND " & _
		//'                      "ganoregi = " & rsIntervencion!ganoregi & " AND " & _
		//'                      "gnumregi = " & rsIntervencion!gnumregi & " "
		//'      stSql = stSql & _
		//''                      "and fsistema = " & FormatFechaHMS(rsIntervencion!fpasfact) & " and " & _
		//''                      "gconcfac = '" & Trim(rsIntervencion!gprocpos) & "'"
		//      End If
		//      Conexion.Execute stSql
		//      'habria que borrar las otras prestaciones
		//
		//   End If
		//
		//   ' añadido Concha 02/10/03
		//   ' borraba DMOVFACT todas y tiene que borrar por prestacion
		//   ' hay que comprobar si existen otras prestaciones de la intervencion principal
		//   stSql = "SELECT * FROM epresalt " & _
		//'              "WHERE itiposer = 'Q' and " & _
		//'                " ganoregi = " & AñoRegistro & " AND " & _
		//'                "gnumregi = " & NumeroRegistro & " "
		//
		//   Set rsOtrasPR = Conexion.OpenResultset(stSql, rdOpenKeyset, rdConcurValues)
		//   If Not rsOtrasPR.EOF Then
		//      'si hay Otras prestaciones
		//   Do Until rsOtrasPR.EOF
		//
		//      If Not IsNull(rsOtrasPR!fpasfact) Then
		//         stSql = "SELECT ffactura FROM dmovfact "
		//         ' Si el paciente estaba hospitalizado se accede con el episodio
		//         ' principal. En caso contrario se accede con el de consultas
		//         If Not IsNull(rsIntervencion!itiposer) Then
		//            stSql = stSql & _
		//'                    "WHERE itiposer = '" & rsIntervencion!itiposer & "' AND " & _
		//'                          "ganoregi = " & rsIntervencion!ganoprin & " AND " & _
		//'                          "gnumregi = " & rsIntervencion!gnumprin & " AND "
		//         Else
		//            stSql = stSql & _
		//'                   "WHERE itiposer = 'Q' AND " & _
		//'                         "ganoregi = " & rsIntervencion!ganoregi & " AND " & _
		//'                         "gnumregi = " & rsIntervencion!gnumregi & " AND "
		//         End If
		//         'stSql = stSql & _
		//'         '                "gcodeven = 'PR' AND " & _
		//'         '                "gconcfac = '" & Trim(rsOtrasPR!gprestac) & "' AND " & _
		//'         '                "fsistema = " & FormatFechaHMS(rsIntervencion!fpasfact) & " AND " & _
		//'         '                "ffinicio = " & FormatFechaHMS(rsIntervencion!finterve) & ""
		//        stSql = stSql & _
		//'                         "gcodeven = 'PR' AND " & _
		//'                         "fsistema = " & FormatFechaHMS(rsIntervencion!fpasfact) & " AND " & _
		//'                         "ffinicio = " & FormatFechaHMS(rsIntervencion!finterve) & ""
		//
		//         Set rsFactura = Conexion.OpenResultset(stSql, rdOpenKeyset, rdConcurRowVer, rdExecDirect)
		//         If Not rsFactura.EOF Then
		//            Do Until rsFactura.EOF
		//                If Not IsNull(rsFactura!ffactura) Then
		//                    Estado = "FACTURADO"
		//                    Exit Function
		//                Else
		//                    Estado = "GENERADO"
		//    '               rsFactura.Edit
		//                   rsFactura.Delete
		//                End If
		//                rsFactura.MoveNext
		//            Loop
		//         End If
		//         rsFactura.Close
		//      End If
		//
		//      rsOtrasPR.MoveNext
		//
		//   Loop
		//   Else
		//      'no hay otras prestaciones
		//   End If
		//
		//
		//   '***************
		//   ' Se comprueba si el material utilizado en la intervencion estan
		//   ' generados sus movimientos pero no facturados
		//
		//   stSql = "SELECT qprotesi.gtipoart, qprotesi.gprotesi AS articulo, " & _
		//'                   "qprotesi.fpasfact, 'N' AS ifarmaco " & _
		//'              "FROM qprotesi " & _
		//'              "WHERE ganoregi = " & AñoRegistro & " AND " & _
		//'                    "gnumregi = " & NumeroRegistro & " AND " & _
		//'                    "iproreal = 'R'" & _
		//'           "UNION ALL " & _
		//'           "SELECT qmatutil.gtipoart, qmatutil.gcodiart AS articulo, " & _
		//'                  "qmatutil.fpasfact, dtipoart.ifarmaco " & _
		//'              "FROM qmatutil, dtipoart " & _
		//'              "WHERE ganoregi = " & AñoRegistro & " AND " & _
		//'                    "gnumregi = " & NumeroRegistro & " AND " & _
		//'                    "qmatutil.gtipoart = dtipoart.gtipoart"
		//   Set rsMaterial = Conexion.OpenResultset(stSql, rdOpenKeyset, rdConcurValues)
		//
		//   Do Until rsMaterial.EOF
		//
		//      If Not IsNull(rsMaterial!fpasfact) Then
		//         stSql = "SELECT ffactura " & _
		//'                    "FROM dmovfact "
		//
		//         ' Si el paciente estaba hospitalizado se accede con el episodio
		//         ' principal. En caso contrario se accede con el de consultas
		//         If Not IsNull(rsIntervencion!itiposer) Then
		//            stSql = stSql & _
		//'                    "WHERE itiposer = '" & rsIntervencion!itiposer & " ' AND " & _
		//'                          "ganoregi = " & rsIntervencion!ganoprin & " AND " & _
		//'                          "gnumregi = " & rsIntervencion!gnumprin & " AND "
		//         Else
		//            stSql = stSql & _
		//'                   "WHERE itiposer = 'Q' AND " & _
		//'                         "ganoregi = " & rsIntervencion!ganoregi & " AND " & _
		//'                         "gnumregi = " & rsIntervencion!gnumregi & " AND "
		//         End If
		//         If rsMaterial!ifarmaco = "S" Then
		//            stSql = stSql & _
		//'                         "gcodeven = 'FA' AND " & _
		//'                         "gcodiart = '" & rsMaterial!articulo & "' AND "
		//         Else
		//                    stSql = stSql & _
		//'                         "gcodeven = 'AL' AND " & _
		//'                         "gcodiart = '" & rsMaterial!articulo & "' AND "
		// '           stSql = stSql & _
		//' '                        "gcodeven = 'AL' AND " & _
		//' '                        "gconcfac = '" & rsMaterial!gtipoart & rsMaterial!articulo & "' AND "
		//         End If
		//
		//
		//
		//         stSql = stSql & _
		//'                         "fsistema = " & FormatFechaHMS(rsMaterial!fpasfact) & " AND " & _
		//'                         "ffinicio = " & FormatFechaHMS(rsIntervencion!finterve) & ""
		//
		//         Set rsFactura = Conexion.OpenResultset(stSql, rdOpenKeyset, rdConcurRowVer, rdExecDirect)
		//         If Not rsFactura.EOF Then
		//            If Not IsNull(rsFactura!ffactura) Then
		//               Estado = "FACTURADO"
		//               Exit Function
		//            Else
		//               Estado = "GENERADO"
		//               rsFactura.Edit
		//               rsFactura.Delete
		//            End If
		//         End If
		//
		//         rsFactura.Close
		//
		//      End If
		//
		//      rsMaterial.MoveNext
		//
		//   Loop
		//
		//   If Not Formulario Is Nothing Then
		//      Formulario.ProRecibirError Empty, Empty
		//   End If
		//   Exit Function
		//
		//etiqueta:
		//   If Formulario Is Nothing Then
		//      lErr = Err.Number
		//      stErr = Err.Description
		//   Else
		//      Formulario.ProRecibirError Err.Number, Err.Description
		//   End If
		//
		//End Function

		private bool gestionporlotes(SqlConnection LotConexion)
		{
			bool result = false;
			string sql = "select * from sconsglo where gconsglo='geslotes'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, LotConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
				{
					result = true;
				}
			}
			RrSql.Close();
			return result;
		}

		//Oscar C ABRIL 2009
		//El nuevo parametro bAnular indica que se ha accedido a la funcion estado desde la operacion de anular intervencion.
		//No desde modificacion de actividad, por lo que en este caso si queremos que se borre DMOVFACT.
		public string Estado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, dynamic Formulario, int lErr, string stErr, bool bAnular, bool bProcCambiado, string sProcAnterior, bool bFecHorCambiada, string sFecHorAnterior)
		{

			string result = String.Empty;
			DataSet rsIntervencion = null;
			DataSet rsFactura = null;
			DataSet rsDanalsol = null;
			DataSet rsOtrasPR = null;
			bool ExisteProtocolo = false;
			string sql = String.Empty;
			string PrestacionFactura = String.Empty;
			bool Continua_Bucle = false;

			try
			{

				Serrores.vfMostrandoHistoricos = true;

				Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
				result = "PENDIENTE";
				lErr = 0;
				stErr = "";
				// Se obtiene los datos de la intervencion
				stSql = "SELECT qintquir.* " + 
				        "FROM qintquir " + 
				        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
				        "gnumregi = " + NumeroRegistro.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
				rsIntervencion = new DataSet();
				tempAdapter.Fill(rsIntervencion);

				// Si el procedimiento postoperatorio se ha pasado a facturacion se
				// comprueba si esta marcada ya como facturada. Si no lo esta se borra
				string stTabla = String.Empty;
				if (rsIntervencion.Tables[0].Rows.Count != 0 && !Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["fpasfact"]))
				{
					stSql = "select * from DMOVFACT ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + 
						        "WHERE itiposer = '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "' AND " + 
						        "ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
						        "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + " AND ";
						object tempRefParam = rsIntervencion.Tables[0].Rows[0]["fpasfact"];
						stSql = stSql + 
						        "fsistema = " + Serrores.FormatFechaHMS(tempRefParam) + " AND ";
						if (bFecHorCambiada)
						{
							object tempRefParam2 = sFecHorAnterior;
							stSql = stSql + "ffinicio = " + Serrores.FormatFechaHMS(tempRefParam2) + " and ";
							sFecHorAnterior = Convert.ToString(tempRefParam2);
						}
						else
						{
							object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
							stSql = stSql + "ffinicio = " + Serrores.FormatFechaHMS(tempRefParam3) + " and ";
						}
						if (bProcCambiado)
						{
							stSql = stSql + "gconcfac = '" + sProcAnterior + "'";
						}
						else
						{
							stSql = stSql + "gconcfac = '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]).Trim() + "'";
						}

					}
					else
					{
						stSql = stSql + 
						        "WHERE itiposer = 'Q' AND " + 
						        "ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
						        "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "  ";

					}

					string tempRefParam4 = "DMOVFACT";
					Serrores.PonerVistas(ref stSql, ref tempRefParam4, Conexion);

					if (Serrores.proExisteTabla("DMOVFACH", Conexion))
					{
						string tempRefParam5 = "DMOVFACT";
						stSql = stSql + " UNION ALL " + Serrores.proReplace(ref stSql, tempRefParam5, "DMOVFACH");
					}

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_2.Fill(rsFactura);
					foreach (DataRow iteration_row in rsFactura.Tables[0].Rows)
					{
						if (!Convert.IsDBNull(iteration_row["ffactura"]))
						{
							result = "FACTURADO";
							rsIntervencion.Close();
							return result;
						}
						else
						{
							result = "GENERADO";
						}
					}
					rsFactura.Close();

					if (result == "GENERADO")
					{

						//OSCAR C Abril 2009
						//BORRAMOS LA TABLA TEMPORAL QUE SE UTILIZARA PARA REALIZAR LOS MOVIMIENTOS DE FACTURACION EN LA FUNCION
						//GENERARMOVIMIENTOSFACTURACION DE ESTA MISMA DLL
						stTabla = "W_DMOVFACT_Q" + Conversion.Str(AñoRegistro).Trim() + Conversion.Str(NumeroRegistro).Trim();

						stSql = "if exists (select * from sysobjects where name='" + stTabla + "'  and type='U') drop table " + stTabla;
						SqlCommand tempCommand = new SqlCommand(stSql, Conexion);
						tempCommand.ExecuteNonQuery();

						proCrearTablaFacturacion(stTabla, Conexion);
						//-------------------------------

						//borramos DMOVFACT
						//si no hay ninguna linea facturada se procede a borrar las lineas correspondientes.

						//OSCAR C Abril 2009
						//EN LUGAR DE BORRAR LOS REGISTROS FISICOS DE DMOVFACT, SE TRASPASAN A LA NUEVA TABLA TEMPORAL.
						//LOS REGISTROS FISICOS DE DMOVFACT SE BORRARAN CUANTO TERMINE TODO EL PROCESO

						//StSql = "Delete FROM dmovfact "
						//StSql = StSql & " where itiporig = 'Q' and ganoorig = " & AñoRegistro & " "
						//StSql = StSql & "and gnumorig = " & NumeroRegistro & " "

						//Conexion.Execute StSql

						stSql = "INSERT INTO " + stTabla + "(gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
						        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
						        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
						        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
						        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
						        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,IBORRADO)";
						stSql = stSql + " select gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
						        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
						        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
						        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
						        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
						        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,'S'";
						stSql = stSql + " FROM DMOVFACT where " + 
						        "  itiporig = 'Q' and ganoorig = " + AñoRegistro.ToString() + " and gnumorig = " + NumeroRegistro.ToString() + " ";
						stSql = stSql + "  AND fila_id NOT IN (SELECT fila_id from " + stTabla + ")";

						SqlCommand tempCommand_2 = new SqlCommand(stSql, Conexion);
						tempCommand_2.ExecuteNonQuery();




						//hasta aqui se borraran todas las lineas correspondientes al episodio de quirofanos
						//de todos las intervenciones con los campos itiporig, ganoorig y gnumorig rellenos
						//si el episodio no se ha borrado porque los campos itiporig, ganoorig y gnumorig
						//estan rellenos con el episodio principal(registros antiguos)
						//seguimos borrando
						// Si el paciente estaba hospitalizado se accede con el episodio principal.
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
						{

							//OSCAR C Abril 2009
							//EN LUGAR DE BORRAR LOS REGISTROS FISICOS DE DMOVFACT, SE TRASPASAN A LA NUEVA TABLA TEMPORAL.
							//LOS REGISTROS FISICOS DE DMOVFACT SE BORRARAN CUANTO TERMINE TODO EL PROCESO
							//StSql = "DELETE FROM dmovfact "

							stSql = "INSERT INTO " + stTabla + "(gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
							        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
							        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
							        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
							        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
							        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,IBORRADO)";
							stSql = stSql + " select gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
							        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
							        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
							        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
							        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
							        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,'S'";
							stSql = stSql + " FROM DMOVFACT ";

							stSql = stSql + 
							        "WHERE itiposer = '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "' AND " + 
							        "ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
							        "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + " AND ";
							object tempRefParam6 = rsIntervencion.Tables[0].Rows[0]["fpasfact"];
							object tempRefParam7 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
							stSql = stSql + 
							        "fsistema = " + Serrores.FormatFechaHMS(tempRefParam6) + " AND " + 
							        "ffinicio = " + Serrores.FormatFechaHMS(tempRefParam7) + " and " + 
							        "gconcfac = '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]).Trim() + "'";
							stSql = stSql + "  AND fila_id NOT IN (SELECT fila_id from " + stTabla + ")";
							SqlCommand tempCommand_3 = new SqlCommand(stSql, Conexion);
							tempCommand_3.ExecuteNonQuery();

							//si esta generado hay que actualizar fpasfact a null para que posteriormente
							//vuelva a generar las lineas correspondientes
							stSql = "Update EPRESTAC set fpasfact = null ";
							stSql = stSql + 
							        "WHERE itiprela = 'Q' AND " + 
							        "ganorela = " + AñoRegistro.ToString() + " AND " + 
							        "gnumrela = " + NumeroRegistro.ToString() + " AND " + 
							        "gprestac = '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]).Trim() + "' ";

							SqlCommand tempCommand_4 = new SqlCommand(stSql, Conexion);
							tempCommand_4.ExecuteNonQuery();

							//Para las otras prestaciones y protocolos
							stSql = "SELECT * FROM epresalt " + 
							        "WHERE itiposer = 'Q' and " + 
							        " ganoregi = " + AñoRegistro.ToString() + " AND " + 
							        "gnumregi = " + NumeroRegistro.ToString() + " ";

							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
							rsOtrasPR = new DataSet();
							tempAdapter_3.Fill(rsOtrasPR);
							if (rsOtrasPR.Tables[0].Rows.Count != 0)
							{
                                int iMoveFirst = 0;
								foreach (DataRow iteration_row_2 in rsOtrasPR.Tables[0].Rows)
								{
									if (HayProtocolo(Convert.ToString(iteration_row_2["gprestac"]), Conexion))
									{
										ExisteProtocolo = true;
										sql = "select * from danalsol where itiposer = 'Q' and ganoregi=" + Convert.ToString(iteration_row_2["ganoregi"]) + " and " + "gnumregi=" + Convert.ToString(iteration_row_2["gnumregi"]);

										SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion);
										rsDanalsol = new DataSet();
										tempAdapter_4.Fill(rsDanalsol);
										if (rsDanalsol.Tables[0].Rows.Count != 0)
										{
                                            iMoveFirst = rsDanalsol.MoveFirst();
										}
										else
										{
											//no encuentra nada en los protocolo porque lo ha borrado previament
											ExisteProtocolo = false;
										}
									}
									else
									{
										ExisteProtocolo = false;
										PrestacionFactura = Convert.ToString(iteration_row_2["gprestac"]);
									}
									Continua_Bucle = true;
									while (Continua_Bucle)
									{
										if (ExisteProtocolo)
										{
                                            PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[iMoveFirst]["gpruelab"]);
										}
										else
										{
											Continua_Bucle = false;
										}

										//OSCAR C Abril 2009
										//EN LUGAR DE BORRAR LOS REGISTROS FISICOS DE DMOVFACT, SE TRASPASAN A LA NUEVA TABLA TEMPORAL.
										//LOS REGISTROS FISICOS DE DMOVFACT SE BORRARAN CUANTO TERMINE TODO EL PROCESO
										//StSql = "Delete FROM dmovfact "

										stSql = "INSERT INTO " + stTabla + "(gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
										        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
										        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
										        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
										        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
										        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,IBORRADO)";
										stSql = stSql + " select gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
										        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
										        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
										        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
										        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
										        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,'S'";
										stSql = stSql + " FROM DMOVFACT ";

										stSql = stSql + 
										        "WHERE itiposer = '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "' AND " + 
										        "ganoregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
										        "gnumregi = " + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + " AND ";
										object tempRefParam8 = iteration_row_2["fpasfact"];
										object tempRefParam9 = iteration_row_2["frealiza"];
										stSql = stSql + 
										        "gcodeven = 'PR' AND " + 
										        "gconcfac = '" + PrestacionFactura + "' AND " + 
										        "fsistema = " + Serrores.FormatFechaHMS(tempRefParam8) + " AND " + 
										        "ffinicio = " + Serrores.FormatFechaHM(tempRefParam9) + "";
										stSql = stSql + "  AND fila_id NOT IN (SELECT fila_id from " + stTabla + ")";
										SqlCommand tempCommand_5 = new SqlCommand(stSql, Conexion);
										tempCommand_5.ExecuteNonQuery();
										if (ExisteProtocolo)
										{
											iMoveFirst = rsDanalsol.MoveNext();
											if (rsDanalsol.Tables[0].Rows.Count != 0)
											{
                                                PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[iMoveFirst]["gpruelab"]);
											}
											else
											{
												Continua_Bucle = false;
												rsDanalsol.Close();
											}
										}
									}
								}
							}
							rsOtrasPR.Close();
						}

						//OSCAR C Abril 2009
						//SI SE HA ACCEDIDO DESDE UNA OPERACION DE ANULAR (bANULAR=true)
						//SE BORRARAN LOS REGISTROS FISICOS DE DMOVFACT QUE ESTEN EN LA NUEVA TABLA TEMPORAL.
						//bAnular viene a true desde:
						//    - Funcion BorraIntervencion de la pantalla de Captura de Actividad Quirurgica.
						if (bAnular)
						{
							stSql = " DELETE DMOVFACT WHERE fila_id IN (SELECT fila_id from " + stTabla + ")";
							SqlCommand tempCommand_6 = new SqlCommand(stSql, Conexion);
							tempCommand_6.ExecuteNonQuery();

							stSql = " DROP TABLE " + stTabla;
							SqlCommand tempCommand_7 = new SqlCommand(stSql, Conexion);
							tempCommand_7.ExecuteNonQuery();
						}
						//-----------------------

					} //generado
				}
				rsIntervencion.Close();

				if (Formulario != null)
				{
					Formulario.ProRecibirError(null, null);
				}

				return result;
			}
			catch (System.Exception excep)
			{

				Serrores.GrabaLog(DateTimeHelper.ToString(DateTime.Now) + " ESTADO - " + Information.Err().Number.ToString() + " - " + excep.Message, Path.GetDirectoryName(Application.ExecutablePath) + "\\ErroresFacturacionQuirofanos.log");

				if (Formulario == null)
				{
					lErr = Information.Err().Number;
					stErr = excep.Message;
				}
				else
				{
					Formulario.ProRecibirError(Information.Err().Number, excep.Message);
				}

				return result;
			}
		}

		public string Estado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario, int lErr, string stErr, bool bAnular, bool bProcCambiado, string sProcAnterior, bool bFecHorCambiada)
		{
			string tempRefParam15 = String.Empty;
			return Estado(AñoRegistro, NumeroRegistro, Conexion, Formulario, lErr, stErr, bAnular, bProcCambiado, sProcAnterior, bFecHorCambiada, tempRefParam15);
		}

		public string Estado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario, int lErr, string stErr, bool bAnular, bool bProcCambiado, string sProcAnterior)
		{
			string tempRefParam16 = String.Empty;
			return Estado(AñoRegistro, NumeroRegistro, Conexion, Formulario, lErr, stErr, bAnular, bProcCambiado, sProcAnterior, false, tempRefParam16);
		}

		public string Estado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario, int lErr, string stErr, bool bAnular, bool bProcCambiado)
		{
			string tempRefParam17 = String.Empty;
			return Estado(AñoRegistro, NumeroRegistro, Conexion, Formulario, lErr, stErr, bAnular, bProcCambiado, String.Empty, false, tempRefParam17);
		}

		public string Estado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario, int lErr, string stErr, bool bAnular)
		{
			string tempRefParam18 = String.Empty;
			return Estado(AñoRegistro, NumeroRegistro, Conexion, Formulario, lErr, stErr, bAnular, false, String.Empty, false, tempRefParam18);
		}

		public string Estado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario, int lErr, string stErr)
		{
			string tempRefParam19 = String.Empty;
			return Estado(AñoRegistro, NumeroRegistro, Conexion, Formulario, lErr, stErr, false, false, String.Empty, false, tempRefParam19);
		}

		public string Estado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario, int lErr)
		{
			string tempRefParam20 = String.Empty;
			string tempRefParam21 = String.Empty;
			return Estado(AñoRegistro, NumeroRegistro, Conexion, Formulario, lErr, tempRefParam20, false, false, String.Empty, false, tempRefParam21);
		}

		public string Estado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario)
		{
			int tempRefParam22 = 0;
			string tempRefParam23 = String.Empty;
			string tempRefParam24 = String.Empty;
			return Estado(AñoRegistro, NumeroRegistro, Conexion, Formulario, tempRefParam22, tempRefParam23, false, false, String.Empty, false, tempRefParam24);
		}

		public string Estado(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion)
		{
			int tempRefParam25 = 0;
			string tempRefParam26 = String.Empty;
			string tempRefParam27 = String.Empty;
			return Estado(AñoRegistro, NumeroRegistro, Conexion, null, tempRefParam25, tempRefParam26, false, false, String.Empty, false, tempRefParam27);
		}

		private void proCrearTablaFacturacion(string stTabla, SqlConnection Conexion)
		{

			string sql = " CREATE TABLE " + stTabla + "(" + 
			             "gidenpac char (13)  NOT NULL , " + 
			             "itpacfac char (1)  NULL , " + 
			             "itiposer char (1)  NOT NULL ," + 
			             "ganoregi smallint NOT NULL ," + 
			             "gnumregi int NOT NULL ," + 
			             "gcodeven char (2)  NULL ," + 
			             "gsersoli smallint NULL ," + 
			             "gserreal smallint NULL ," + 
			             "gsocieda smallint NOT NULL ," + 
			             "nafiliac char (20)  NULL ," + 
			             "gconcfac char (10)  NULL ," + 
			             "ncantida numeric(14, 2) NULL ," + 
			             "fsistema datetime NOT NULL ," + 
			             "ffinicio datetime NULL ," + 
			             "ffechfin datetime NULL ," + 
			             "ireingre char (1)  NULL ," + 
			             "gtipocam smallint NULL ," + 
			             "gtipodie smallint NULL ," + 
			             "iregaloj char (1)  NULL ," + 
			             "gdiagalt char (8)  NULL ," + 
			             "ginspecc char (4)  NULL ," + 
			             "iorigqui char (1)  NULL ," + 
			             "iregiqui char (1)  NULL ," + 
			             "gpersona int NULL ,";
			sql = sql + 
			      "ndninifp char (9)  NULL ," + 
			      "iexitusp char (1)  NULL ," + 
			      "iprimsuc char (1)  NULL ," + 
			      "gmotalta smallint NULL ," + 
			      "ipaseobs char (1)  NULL ," + 
			      "ffactura datetime NULL ," + 
			      "iautosoc char (1)  NULL ," + 
			      "iexiauto char (1)  NULL ," + 
			      "gserresp smallint NULL ," + 
			      "fperfact datetime NOT NULL ," + 
			      "fila_id int  NULL ," + 
			      "area char (1)  NULL ," + 
			      "gcodiart varchar (10)  NULL ," + 
			      "numfact varchar (10)  NULL ," + 
			      "anof char (4)  NULL ," + 
			      "NUMFACTSOM varchar (10)  NULL ," + 
			      "anofsom char (4)  NULL ," + 
			      "irepetid varchar (1)  NULL ," + 
			      "idlinc int NULL ," + 
			      "gcodlote varchar (10)  NULL ," + 
			      "itiporig varchar (1)  NULL ," + 
			      "ganoorig smallint NULL ," + 
			      "gnumorig int NULL, " + 
			      "IBORRADO char (1)  NULL)";

			SqlCommand tempCommand = new SqlCommand(sql, Conexion);
			tempCommand.ExecuteNonQuery();
		}

		private void proActualiza_DMOVFACT(string stTablaFacturacion_W)
		{
			//OSCAR C Abril 2009:
			//Una vez generados todos los movimientos de facturacion en la tabla temporal del tipo W_DMOVFACT_Q20081,
			//se debera realizar lo siguiente, siempre y cuando la tabla temporal tenga registros:
			//    - Se deberá pasar de la tabla temporal de facturacion a la tabla de facturacion real (DMOVFACT), de acuerdo a:
			//        * Se borrara de DMOVFACT aquellos registros que coincidan con W_DMOVFACT por fila_id y que tengan la marca de borrado (iborrado='S')
			//        * Se modificara DMOVFACT actualizando todos los campos de aquellos registros que coincidan con la tabla temporal por fila_id y
			//            cuyo indicador bde borrado logico estubiera a NULO (iborrado = NULL)
			//        * Se insertatan los registrso en DMOVFACT de la tabla temporal aquellos registros cuya fila_id estubiera a NUL
			//    - Se borrará la tabla temporal

			stSql = "SELECT * FROM " + stTablaFacturacion_W;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
			rsFactura = new DataSet();
			tempAdapter.Fill(rsFactura);
			if (rsFactura.Tables[0].Rows.Count != 0)
			{

				stSql = " DELETE DMOVFACT WHERE Fila_id IN" + 
				        "   (SELECT fila_id FROM " + stTablaFacturacion_W + " WHERE iborrado='S' and fila_id is NOT null)";
				SqlCommand tempCommand = new SqlCommand(stSql, Conexion);
				tempCommand.ExecuteNonQuery();

				stSql = " UPDATE DMOVFACT SET " + 
				        " gidenpac = " + stTablaFacturacion_W + ".gidenpac, " + 
				        " itpacfac = " + stTablaFacturacion_W + ".itpacfac, " + 
				        " itiposer = " + stTablaFacturacion_W + ".itiposer, " + 
				        " ganoregi = " + stTablaFacturacion_W + ".ganoregi, " + 
				        " gnumregi = " + stTablaFacturacion_W + ".gnumregi, " + 
				        " gcodeven = " + stTablaFacturacion_W + ".gcodeven, " + 
				        " gsersoli = " + stTablaFacturacion_W + ".gsersoli, " + 
				        " gserreal = " + stTablaFacturacion_W + ".gserreal, " + 
				        " gsocieda = " + stTablaFacturacion_W + ".gsocieda, " + 
				        " nafiliac = " + stTablaFacturacion_W + ".nafiliac, " + 
				        " gconcfac = " + stTablaFacturacion_W + ".gconcfac, " + 
				        " ncantida = " + stTablaFacturacion_W + ".ncantida, " + 
				        " fsistema = " + stTablaFacturacion_W + ".fsistema, " + 
				        " ffinicio = " + stTablaFacturacion_W + ".ffinicio, " + 
				        " ffechfin = " + stTablaFacturacion_W + ".ffechfin, " + 
				        " ireingre = " + stTablaFacturacion_W + ".ireingre, ";
				stSql = stSql + 
				        " gtipocam = " + stTablaFacturacion_W + ".gtipocam, " + 
				        " gtipodie = " + stTablaFacturacion_W + ".gtipodie, " + 
				        " iregaloj = " + stTablaFacturacion_W + ".iregaloj, " + 
				        " gdiagalt = " + stTablaFacturacion_W + ".gdiagalt, " + 
				        " ginspecc = " + stTablaFacturacion_W + ".ginspecc, " + 
				        " iorigqui = " + stTablaFacturacion_W + ".iorigqui, " + 
				        " iregiqui = " + stTablaFacturacion_W + ".iregiqui, " + 
				        " gpersona = " + stTablaFacturacion_W + ".gpersona, " + 
				        " ndninifp = " + stTablaFacturacion_W + ".ndninifp, " + 
				        " iexitusp = " + stTablaFacturacion_W + ".iexitusp, " + 
				        " iprimsuc = " + stTablaFacturacion_W + ".iprimsuc, " + 
				        " gmotalta = " + stTablaFacturacion_W + ".gmotalta, " + 
				        " ipaseobs = " + stTablaFacturacion_W + ".ipaseobs, " + 
				        " ffactura = " + stTablaFacturacion_W + ".ffactura, " + 
				        " iautosoc = " + stTablaFacturacion_W + ".iautosoc, " + 
				        " iexiauto = " + stTablaFacturacion_W + ".iexiauto, " + 
				        " gserresp = " + stTablaFacturacion_W + ".gserresp, " + 
				        " fperfact = " + stTablaFacturacion_W + ".fperfact, ";
				stSql = stSql + 
				        " area = " + stTablaFacturacion_W + ".area, " + 
				        " gcodiart = " + stTablaFacturacion_W + ".gcodiart, " + 
				        " numfact = " + stTablaFacturacion_W + ".numfact, " + 
				        " anof = " + stTablaFacturacion_W + ".anof, " + 
				        " NUMFACTSOM = " + stTablaFacturacion_W + ".NUMFACTSOM, " + 
				        " anofsom = " + stTablaFacturacion_W + ".anofsom, " + 
				        " irepetid = " + stTablaFacturacion_W + ".irepetid, " + 
				        " idlinc = " + stTablaFacturacion_W + ".idlinc, " + 
				        " gcodlote = " + stTablaFacturacion_W + ".gcodlote, " + 
				        " itiporig = " + stTablaFacturacion_W + ".itiporig, " + 
				        " ganoorig = " + stTablaFacturacion_W + ".ganoorig, " + 
				        " gnumorig = " + stTablaFacturacion_W + ".gnumorig ";
				stSql = stSql + 
				        " FROM DMOVFACT INNER JOIN " + stTablaFacturacion_W + " ON DMOVFACT.fila_id = " + stTablaFacturacion_W + ".fila_id " + 
				        " and " + stTablaFacturacion_W + ".iborrado Is Null AND " + 
				        stTablaFacturacion_W + ".fila_id Is NOT Null ";
				SqlCommand tempCommand_2 = new SqlCommand(stSql, Conexion);
				tempCommand_2.ExecuteNonQuery();


				stSql = " INSERT INTO DMOVFACT " + 
				        "  (gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal,gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre,gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona,ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto,gserresp,fperfact,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom,irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig)" + 
				        " SELECT " + 
				        "   gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal,gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre,gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona,ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto,gserresp,fperfact,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom,irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig" + 
				        " FROM " + stTablaFacturacion_W + 
				        " WHERE fila_id IS NULL ";
				SqlCommand tempCommand_3 = new SqlCommand(stSql, Conexion);
				tempCommand_3.ExecuteNonQuery();

			}
			rsFactura.Close();

			stSql = " DROP TABLE " + stTablaFacturacion_W;
			SqlCommand tempCommand_4 = new SqlCommand(stSql, Conexion);
			tempCommand_4.ExecuteNonQuery();

			// -------------- FIN OSCAR C Abril 2009 --------------------------

		}


		private void proFacturacionHonorarioMedico()
		{
			bool bEncontrado = false;
			//------- facturacion movimiento para el cirujano----------------------------------------------------OK
			//OSCAR C Abril 2009
			//COMENTARIOS GENERALES
			//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
			//de facturacion.
			//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
			//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
			//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
			//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
			//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
			//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL

			if (stTablaFacturacion != "DMOVFACT")
			{

				if (bTemp)
				{
					stSql = "SELECT * " + 
					        "FROM dmovfacc " + 
					        "WHERE 1 = 2";
				}
				else
				{
					//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
					stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
				rsFactura = new DataSet();
				tempAdapter.Fill(rsFactura);
				// Se graba un movimiento para el cirujano
				rsFactura.AddNew();
				InicializarDatos();
				rsFactura.Tables[0].Rows[0]["gcodeven"] = "HM";
				rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gmedicoc"];
				rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
				//oscar 29/10/03
				rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
				rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
				rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
				//-----
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(rsFactura, rsFactura.Tables[0].TableName);
				rsFactura.Close();
			}
			else
			{
				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				// NOTA:
				// La condicion de busqueda sera por:
				// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
				// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
				// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
				// y que ademas tuviera la marca de borrado logico (iborrado='S')
				//* No aplica en Quirofanos en este caso
				bEncontrado = false;

				stSql = "SELECT * FROM " + stTablaFacturacion_W;
				stSql = stSql + " WHERE ";
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
				{
					stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
					stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
					stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
					stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
				}
				else
				{
					stSql = stSql + "  itiposer='Q'";
					stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
					stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
					stSql = stSql + " AND gsersoli IS NULL";
				}
				stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
				stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
				if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
				{
					stSql = stSql + " AND nafiliac IS NULL";
				}
				else
				{
					stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
				}
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
				{
					object tempRefParam = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
					stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam);
				}
				else
				{
					object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam2);
				}
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
				{
					object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
					stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam3);
				}
				else
				{
					object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam4);
				}
				stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
				if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
				{
					stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
				}
				else
				{
					stSql = stSql + " AND gdiagalt IS NULL";
				}
				if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
				{
					stSql = stSql + " AND ginspecc IS NULL";
				}
				else
				{
					stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
				}
				if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
				{
					stSql = stSql + " AND iregiqui='C'";
				}
				else
				{
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
					{
						stSql = stSql + " AND iregiqui='A'";
					}
					else
					{
						stSql = stSql + " AND iregiqui='S'";
					}
				}
				stSql = stSql + " AND gcodeven ='HM'";
				stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmedicoc"]);
				stSql = stSql + " AND ncantida =1";
				stSql = stSql + " AND itiporig ='Q'";
				stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
				stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
				stSql = stSql + " AND iborrado='S'";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
				RsAux = new DataSet();
				tempAdapter_3.Fill(RsAux);
				if (RsAux.Tables[0].Rows.Count != 0)
				{
					RsAux.MoveFirst();
					bEncontrado = true;
				}
                SqlDataAdapter tempAdapter_4 = null;

                if (bEncontrado)
				{
					stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
					tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_4.Fill(rsFactura);
					rsFactura.Edit();
					rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
				}
				else
				{
					stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
					tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_4.Fill(rsFactura);
					rsFactura.AddNew();
				}

				RsAux.Close();

				InicializarDatos();
				rsFactura.Tables[0].Rows[0]["gcodeven"] = "HM";
				rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gmedicoc"];
				rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
				//oscar 29/10/03
				rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
				rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
				rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
				//----

				SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
				tempAdapter_4.Update(rsFactura, rsFactura.Tables[0].TableName);
				rsFactura.Close();
			}
			//-------FIN facturacion movimiento para el cirujano-------------------------------End Sub
		}

		private void proFacturacionAyudantesIntervencion()
		{
			bool bEncontrado = false;
			//--------facturacion movimiento para el ayudante----------------------------------OK

			//OSCAR C Abril 2009
			//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
			//de facturacion.
			//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
			//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
			//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
			//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
			//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
			//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gayudant"]))
			{

				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "SELECT * " + 
						        "FROM dmovfacc " + 
						        "WHERE 1 = 2";
					}
					else
					{
						//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
						stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
					}

					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter.Fill(rsFactura);
					rsFactura.AddNew();
					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gayudant"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}
				else
				{
					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						stSql = stSql + " AND gsersoli IS NULL";
					}
					stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
					{
						object tempRefParam = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam);
					}
					else
					{
						object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam2);
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
					{
						object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam3);
					}
					else
					{
						object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam4);
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
					{
						stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
					}
					else
					{
						stSql = stSql + " AND gdiagalt IS NULL";
					}
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
					{
						stSql = stSql + " AND iregiqui='C'";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
						{
							stSql = stSql + " AND iregiqui='A'";
						}
						else
						{
							stSql = stSql + " AND iregiqui='S'";
						}
					}
					stSql = stSql + " AND gcodeven ='AY'";
					stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudant"]);
					stSql = stSql + " AND ncantida =1";
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_3.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}
                    SqlDataAdapter tempAdapter_4 = null;

                    if (bEncontrado)
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
						tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_4.Fill(rsFactura);
						rsFactura.Edit();
						rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
					}
					else
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
                        tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
                        tempAdapter_4.Fill(rsFactura);
						rsFactura.AddNew();
					}

					RsAux.Close();

					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gayudant"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
                    tempAdapter_4.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}

			}
			//---------FIN facturacion movimiento para el ayudante-----------------------------


			//--------facturacion movimiento para el ayudante 2---------------------------------OK
			//OSCAR C Abril 2009
			//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
			//de facturacion.
			//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
			//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
			//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
			//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
			//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
			//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL

			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gayudan2"]))
			{

				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "SELECT * " + 
						        "FROM dmovfacc " + 
						        "WHERE 1 = 2";
					}
					else
					{
						//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
						stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
					}

					SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_7.Fill(rsFactura);
					rsFactura.AddNew();
					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gayudan2"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_7);
                    tempAdapter_7.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}
				else
				{
					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						stSql = stSql + " AND gsersoli IS NULL";
					}
					stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
					{
						object tempRefParam5 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam5);
					}
					else
					{
						object tempRefParam6 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam6);
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
					{
						object tempRefParam7 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam7);
					}
					else
					{
						object tempRefParam8 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam8);
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
					{
						stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
					}
					else
					{
						stSql = stSql + " AND gdiagalt IS NULL";
					}
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
					{
						stSql = stSql + " AND iregiqui='C'";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
						{
							stSql = stSql + " AND iregiqui='A'";
						}
						else
						{
							stSql = stSql + " AND iregiqui='S'";
						}
					}
					stSql = stSql + " AND gcodeven ='AY'";
					stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gayudan2"]);
					stSql = stSql + " AND ncantida =1";
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_9.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}
                    SqlDataAdapter tempAdapter_10 = null;

                    if (bEncontrado)
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
						tempAdapter_10 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_10.Fill(rsFactura);
						rsFactura.Edit();
						rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
					}
					else
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
                        tempAdapter_10 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
                        tempAdapter_10.Fill(rsFactura);
						rsFactura.AddNew();
					}

					RsAux.Close();

					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gayudan2"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_10);
                    tempAdapter_10.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}

			}
			//--------FIN facturacion movimiento para el ayudante 2----------------------------------

		}

		private void proFacturacionAnestesistayAyudante()
		{
			bool bEncontrado = false;
			//--------facturacion movimiento para el anestesista----------------------------------OK
			//OSCAR C Abril 2009
			//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
			//de facturacion.
			//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
			//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
			//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
			//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
			//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
			//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL

			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gperanes"]))
			{

				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "SELECT * " + 
						        "FROM dmovfacc " + 
						        "WHERE 1 = 2";
					}
					else
					{
						//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
						stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
					}

					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter.Fill(rsFactura);
					rsFactura.AddNew();
					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AN";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gperanes"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}
				else
				{
					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso

					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						stSql = stSql + " AND gsersoli IS NULL";
					}
					stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
					{
						object tempRefParam = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam);
					}
					else
					{
						object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam2);
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
					{
						object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam3);
					}
					else
					{
						object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam4);
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
					{
						stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
					}
					else
					{
						stSql = stSql + " AND gdiagalt IS NULL";
					}
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
					{
						stSql = stSql + " AND iregiqui='C'";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
						{
							stSql = stSql + " AND iregiqui='A'";
						}
						else
						{
							stSql = stSql + " AND iregiqui='S'";
						}
					}
					stSql = stSql + " AND gcodeven ='AN'";
					stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gperanes"]);
					stSql = stSql + " AND ncantida =1";
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_3.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}
                    SqlDataAdapter tempAdapter_4 = null;

                    if (bEncontrado)
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
						tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_4.Fill(rsFactura);
						rsFactura.Edit();
						rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
					}
					else
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
                        tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
                        tempAdapter_4.Fill(rsFactura);
						rsFactura.AddNew();
					}

					RsAux.Close();

					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AN";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gperanes"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
                    tempAdapter_4.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}

			}
			//--------FIN facturacion movimiento para el anestesista----------------------------------


			//--------facturacion movimiento para el ayudante del anestesista-------------------------------OK
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gperane2"]))
			{
				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "SELECT * " + 
						        "FROM dmovfacc " + 
						        "WHERE 1 = 2";
					}
					else
					{
						//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
						stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
					}

					SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter_7.Fill(rsFactura);
					rsFactura.AddNew();
					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gperane2"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_7);
                    tempAdapter_7.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}
				else
				{

					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						stSql = stSql + " AND gsersoli IS NULL";
					}
					stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
					{
						object tempRefParam5 = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam5);
					}
					else
					{
						object tempRefParam6 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam6);
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
					{
						object tempRefParam7 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam7);
					}
					else
					{
						object tempRefParam8 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam8);
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
					{
						stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
					}
					else
					{
						stSql = stSql + " AND gdiagalt IS NULL";
					}
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
					{
						stSql = stSql + " AND iregiqui='C'";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
						{
							stSql = stSql + " AND iregiqui='A'";
						}
						else
						{
							stSql = stSql + " AND iregiqui='S'";
						}
					}
					stSql = stSql + " AND gcodeven ='AY'";
					stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gperane2"]);
					stSql = stSql + " AND ncantida =1";
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_9.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}
                    SqlDataAdapter tempAdapter_10 = null;

                    if (bEncontrado)
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
                        tempAdapter_10 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_10.Fill(rsFactura);
						rsFactura.Edit();
						rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
					}
					else
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
                        tempAdapter_10 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
                        tempAdapter_10.Fill(rsFactura);
						rsFactura.AddNew();
					}

					RsAux.Close();

					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gperane2"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_10);
                    tempAdapter_10.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}

			}
			//--------FIN facturacion movimiento para el ayudante del anestesista------------------------------
		}


		private void proFacturacionProtesis()
		{
			bool bEncontrado = false;
			//-----Facturacion de los conceptos facturables asociados a las posibles protesis utilizadas -----------OK
			if (GestionLotes)
			{
				stsqllot = ",gcodlote ";
			}
			else
			{
				stsqllot = " ";
			}
			stSql = "SELECT qprotesi.gtipoart, qprotesi.gprotesi, qprotesi.fpasfact,qprotesi.ncantida" + stsqllot + 
			        "FROM qprotesi " + 
			        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
			        "gnumregi = " + NumeroRegistro.ToString() + " AND " + 
			        "iproreal = 'R'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
			rsProtesis = new DataSet();
			tempAdapter.Fill(rsProtesis);
			foreach (DataRow iteration_row in rsProtesis.Tables[0].Rows)
			{
				if (GestionLotes)
				{
					stsqllot = ",gcodlote ";
				}
				else
				{
					stsqllot = " ";
				}

				//OSCAR C Abril 2009
				//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
				//de facturacion.
				//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
				//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
				//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
				//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
				//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
				//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "INSERT INTO dmovfacc " + 
						        "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
						        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, " + 
						        "ncantida, fsistema, ffinicio, ffechfin, ginspecc, iregiqui, ndninifp, fperfact, gcodiart" + stsqllot + 
						        ",itiporig, ganoorig, gnumorig ) " + 
						        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'," + 
						        "'I',";
					}
					else
					{
						//            StSql = "INSERT INTO dmovfact " & _
						//'                       "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " & _
						//'                        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, " & _
						//'                        "ncantida, fsistema, ffinicio, ffechfin, ginspecc, iregiqui, ndninifp, fperfact, gcodiart" & stsqllot & _
						//'                        ",itiporig, ganoorig, gnumorig ) " & _
						//'                       "VALUES('" & rsIntervencion!gidenpac & "'," & _
						//'                               "'I',"
						//lo sustituimos por la tabla temporal de facturacion
						stSql = "INSERT INTO " + stTablaFacturacion + "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
						        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, " + 
						        "ncantida, fsistema, ffinicio, ffechfin, ginspecc, iregiqui, ndninifp, fperfact, gcodiart" + stsqllot + 
						        ",itiporig, ganoorig, gnumorig ) " + 
						        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'," + 
						        "'I',";
					}
					// Si el paciente no estaba hospitalizado la prestación principal
					// se graba como externa. Si estaba hospitalizado se graba como interna
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
						        "'AL',";
					}
					else
					{
						stSql = stSql + 
						        "'Q'," + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
						        "'AL',";
					}

					// Modificación: 10/11/99 el servicio solicitante va a nulos
					// cuando es un ambulante

					//      If IsNull(rsIntervencion!itiposer) Then
					//            stSql = stSql & _
					//'                               "(Null) ," & _
					//'                               iAlmacen & "," & _
					//'                               rsIntervencion!gsocieda & ","
					//      Else

					// Modificacion 31/3/2000
					stSql = stSql + 
					        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + "," + 
					        iAlmacen.ToString() + "," + 
					        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";

					//      End If
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + 
						        "(Null),";
					}
					else
					{
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
					}
					object tempRefParam = FechaSistema;
					object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + 
					        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'," + 
					        Convert.ToString(iteration_row["ncantida"]) + "," + 
					        "" + Serrores.FormatFechaHMS(tempRefParam) + "," + 
					        "" + Serrores.FormatFechaHMS(tempRefParam2) + "," + 
					        "" + Serrores.FormatFechaHMS(tempRefParam3) + ",";
					FechaSistema = Convert.ToString(tempRefParam);

					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + 
						        "(Null),";
					}
					else
					{
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
					}
					stSql = stSql + "'" + sRegiqui + "',";
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + 
						        "(Null),";
					}
					else
					{
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
					}
					if (FechaPerfact == "")
					{
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
						{
							object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
							stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam4) + "";
						}
						else
						{
							object tempRefParam5 = rsIntervencion.Tables[0].Rows[0]["finterve"];
							stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam5) + "";
						}
					}
					else
					{
						object tempRefParam6 = FechaPerfact;
						stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam6) + "";
						FechaPerfact = Convert.ToString(tempRefParam6);
					}
					stSql = stSql + ",'" + Convert.ToString(iteration_row["gprotesi"]) + "'";

					if (GestionLotes)
					{
						stsqllot = ",'" + Convert.ToString(iteration_row["gcodlote"]) + "'";
					}
					else
					{
						stsqllot = " ";
					}
					stSql = stSql + stsqllot;

					//oscar 29/10/03
					stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ")";
					//---------

					//Debug.Print stSql
					SqlCommand tempCommand = new SqlCommand(stSql, Conexion);
					tempCommand.ExecuteNonQuery();

				}
				else
				{

					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
					}
					stSql = stSql + " AND gcodeven ='AL'";
					stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gserreal=" + iAlmacen.ToString();
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					stSql = stSql + " AND ncantida=" + Convert.ToString(iteration_row["ncantida"]);
					object tempRefParam7 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam7);
					object tempRefParam8 = rsIntervencion.Tables[0].Rows[0]["finterve"];
					stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam8);
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					stSql = stSql + " AND iregiqui='" + sRegiqui + "'";
					stSql = stSql + " AND gcodiart='" + Convert.ToString(iteration_row["gprotesi"]) + "'";
					if (GestionLotes)
					{
						stSql = stSql + " AND gcodlote='" + Convert.ToString(iteration_row["gcodlote"]) + "'";
					}
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_2.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}


					if (bEncontrado)
					{

						stSql = " UPDATE " + stTablaFacturacion_W + " SET ";
						stSql = stSql + "  gidenpac= '" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'";
						stSql = stSql + ", itpacfac='I'";
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
						{
							stSql = stSql + " , itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
							stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
							stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						}
						else
						{
							stSql = stSql + " , itiposer='Q'";
							stSql = stSql + " , ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
							stSql = stSql + " , gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						}
						stSql = stSql + " , gcodeven='AL'";
						stSql = stSql + " , gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
						stSql = stSql + " , gserreal=" + iAlmacen.ToString();
						stSql = stSql + " , gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
						{
							stSql = stSql + " , nafiliac = (NULL) ";
						}
						else
						{
							stSql = stSql + " , nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
						}
						stSql = stSql + " , gconcfac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
						stSql = stSql + " , ncantida=" + Convert.ToString(iteration_row["ncantida"]);
						object tempRefParam9 = FechaSistema;
						stSql = stSql + " , fsistema=" + Serrores.FormatFechaHMS(tempRefParam9);
						FechaSistema = Convert.ToString(tempRefParam9);
						object tempRefParam10 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " , ffinicio=" + Serrores.FormatFechaHMS(tempRefParam10);
						object tempRefParam11 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " , ffechfin=" + Serrores.FormatFechaHMS(tempRefParam11);
						if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
						{
							stSql = stSql + " , ginspecc = (Null) ";
						}
						else
						{
							stSql = stSql + " , ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
						}
						stSql = stSql + " , iregiqui='" + sRegiqui + "'";
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
						{
							stSql = stSql + " , ndninifp = (Null)";
						}
						else
						{
							stSql = stSql + " , ndninifp='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
						}
						if (FechaPerfact == "")
						{
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
							{
								object tempRefParam12 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
								stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam12) + "";
							}
							else
							{
								object tempRefParam13 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam13) + "";
							}
						}
						else
						{
							object tempRefParam14 = FechaPerfact;
							stSql = stSql + " , fperfact=" + Serrores.FormatFechaHM(tempRefParam14) + "";
							FechaPerfact = Convert.ToString(tempRefParam14);
						}
						stSql = stSql + " , gcodiart='" + Convert.ToString(iteration_row["gprotesi"]) + "'";
						if (GestionLotes)
						{
							stSql = stSql + " , gcodlote='" + Convert.ToString(iteration_row["gcodlote"]) + "'";
						}
						stSql = stSql + " , itiporig='Q'";
						stSql = stSql + " , ganoorig=" + AñoRegistro.ToString();
						stSql = stSql + " , gnumorig=" + NumeroRegistro.ToString();
						stSql = stSql + " , IBORRADO=(NULL) ";

						stSql = stSql + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);

						SqlCommand tempCommand_2 = new SqlCommand(stSql, Conexion);
						tempCommand_2.ExecuteNonQuery();

					}
					else
					{

						stSql = "INSERT INTO " + stTablaFacturacion_W + "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
						        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, " + 
						        "ncantida, fsistema, ffinicio, ffechfin, ginspecc, iregiqui, ndninifp, fperfact, gcodiart" + stsqllot + 
						        ",itiporig, ganoorig, gnumorig ) " + 
						        "VALUES('" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gidenpac"]) + "'," + 
						        "'I',";

						// Si el paciente no estaba hospitalizado la prestación principal
						// se graba como externa. Si estaba hospitalizado se graba como interna
						if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]) + "," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]) + "," + 
							        "'AL',";
						}
						else
						{
							stSql = stSql + 
							        "'Q'," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]) + "," + 
							        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]) + "," + 
							        "'AL',";
						}

						// Modificación: 10/11/99 el servicio solicitante va a nulos
						// cuando es un ambulante

						//      If IsNull(rsIntervencion!itiposer) Then
						//            stSql = stSql & _
						//'                               "(Null) ," & _
						//'                               iAlmacen & "," & _
						//'                               rsIntervencion!gsocieda & ","
						//      Else

						// Modificacion 31/3/2000
						stSql = stSql + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]) + "," + 
						        iAlmacen.ToString() + "," + 
						        Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]) + ",";

						//      End If
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
						{
							stSql = stSql + 
							        "(Null),";
						}
						else
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
						}
						object tempRefParam15 = FechaSistema;
						object tempRefParam16 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						object tempRefParam17 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + 
						        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'," + 
						        Convert.ToString(iteration_row["ncantida"]) + "," + 
						        "" + Serrores.FormatFechaHMS(tempRefParam15) + "," + 
						        "" + Serrores.FormatFechaHMS(tempRefParam16) + "," + 
						        "" + Serrores.FormatFechaHMS(tempRefParam17) + ",";
						FechaSistema = Convert.ToString(tempRefParam15);

						if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
						{
							stSql = stSql + 
							        "(Null),";
						}
						else
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "',";
						}
						stSql = stSql + "'" + sRegiqui + "',";
						if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
						{
							stSql = stSql + 
							        "(Null),";
						}
						else
						{
							stSql = stSql + 
							        "'" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "',";
						}
						if (FechaPerfact == "")
						{
							if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
							{
								object tempRefParam18 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
								stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam18) + "";
							}
							else
							{
								object tempRefParam19 = rsIntervencion.Tables[0].Rows[0]["finterve"];
								stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam19) + "";
							}
						}
						else
						{
							object tempRefParam20 = FechaPerfact;
							stSql = stSql + "" + Serrores.FormatFechaHM(tempRefParam20) + "";
							FechaPerfact = Convert.ToString(tempRefParam20);
						}
						stSql = stSql + ",'" + Convert.ToString(iteration_row["gprotesi"]) + "'";

						if (GestionLotes)
						{
							stsqllot = ",'" + Convert.ToString(iteration_row["gcodlote"]) + "'";
						}
						else
						{
							stsqllot = " ";
						}
						stSql = stSql + stsqllot;

						//oscar 29/10/03
						stSql = stSql + ", 'Q' ," + AñoRegistro.ToString() + ", " + NumeroRegistro.ToString() + ")";
						//---------

						//Debug.Print stSql
						SqlCommand tempCommand_3 = new SqlCommand(stSql, Conexion);
						tempCommand_3.ExecuteNonQuery();


					}
					RsAux.Close();
				}


			}

			object tempRefParam21 = FechaSistema;
			stSql = "UPDATE qprotesi " + 
			        "SET fpasfact = " + Serrores.FormatFechaHMS(tempRefParam21) + " " + 
			        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
			        "gnumregi = " + NumeroRegistro.ToString() + " AND " + 
			        "iproreal = 'R'";
			FechaSistema = Convert.ToString(tempRefParam21);
			SqlCommand tempCommand_4 = new SqlCommand(stSql, Conexion);
			tempCommand_4.ExecuteNonQuery();

			//---FIN Facturacion de los conceptos facturables asociados a las posibles protesis utilizadas--------
		}


		private void proFacturacionMonitorista()
		{
			bool bEncontrado = false;
			//--------facturacion movimiento para el Monitorista----------------------------------OK

			//OSCAR C Abril 2009
			//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
			//de facturacion.
			//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
			//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
			//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
			//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
			//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
			//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
			if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gmonitor"]))
			{

				if (stTablaFacturacion != "DMOVFACT")
				{

					if (bTemp)
					{
						stSql = "SELECT * " + 
						        "FROM dmovfacc " + 
						        "WHERE 1 = 2";
					}
					else
					{
						//StSql = "SELECT * FROM dmovfact WHERE 1 = 2"
						stSql = "SELECT * FROM " + stTablaFacturacion + " WHERE 1 = 2";
					}

					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
					rsFactura = new DataSet();
					tempAdapter.Fill(rsFactura);
					rsFactura.AddNew();
					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gmonitor"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}
				else
				{
					//OSCAR C Abril 2009
					//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
					//de facturacion.
					//Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
					//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
					//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
					//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
					//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
					//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
					// NOTA:
					// La condicion de busqueda sera por:
					// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
					// ffechfin, gdiagalt, ginspecc, iregiqui, gpersona, gserresp*, gcodiart*, gcodlote,
					// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
					// y que ademas tuviera la marca de borrado logico (iborrado='S')
					//* No aplica en Quirofanos en este caso
					bEncontrado = false;

					stSql = "SELECT * FROM " + stTablaFacturacion_W;
					stSql = stSql + " WHERE ";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["itiposer"]))
					{
						stSql = stSql + "  itiposer='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["itiposer"]) + "'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoprin"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumprin"]);
						stSql = stSql + " AND gsersoli=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					}
					else
					{
						stSql = stSql + "  itiposer='Q'";
						stSql = stSql + " AND ganoregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ganoregi"]);
						stSql = stSql + " AND gnumregi=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gnumregi"]);
						stSql = stSql + " AND gsersoli IS NULL";
					}
					stSql = stSql + " AND gserreal=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gservici"]);
					stSql = stSql + " AND gsocieda=" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gsocieda"]);
					if (rsIntervencion.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["nafiliac"]))
					{
						stSql = stSql + " AND nafiliac IS NULL";
					}
					else
					{
						stSql = stSql + " AND nafiliac='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["nafiliac"]) + "'";
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hpasoqui"]))
					{
						object tempRefParam = rsIntervencion.Tables[0].Rows[0]["hpasoqui"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam);
					}
					else
					{
						object tempRefParam2 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffinicio=" + Serrores.FormatFechaHMS(tempRefParam2);
					}
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["hhsalida"]))
					{
						object tempRefParam3 = rsIntervencion.Tables[0].Rows[0]["hhsalida"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam3);
					}
					else
					{
						object tempRefParam4 = rsIntervencion.Tables[0].Rows[0]["finterve"];
						stSql = stSql + " AND ffechfin=" + Serrores.FormatFechaHMS(tempRefParam4);
					}
					stSql = stSql + " AND gconcfac ='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gprocpos"]) + "'";
					if (!Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]))
					{
						stSql = stSql + " AND gdiagalt='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gdiagpos"]) + "'";
					}
					else
					{
						stSql = stSql + " AND gdiagalt IS NULL";
					}
					if (Convert.IsDBNull(rsIntervencion.Tables[0].Rows[0]["ginspecc"]))
					{
						stSql = stSql + " AND ginspecc IS NULL";
					}
					else
					{
						stSql = stSql + " AND ginspecc='" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ginspecc"]) + "'";
					}
					if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["iregciru"]) == "C")
					{
						stSql = stSql + " AND iregiqui='C'";
					}
					else
					{
						if (Convert.ToString(rsIntervencion.Tables[0].Rows[0]["ireganes"]) == "A")
						{
							stSql = stSql + " AND iregiqui='A'";
						}
						else
						{
							stSql = stSql + " AND iregiqui='S'";
						}
					}
					stSql = stSql + " AND gcodeven ='AY'";
					stSql = stSql + " AND gpersona =" + Convert.ToString(rsIntervencion.Tables[0].Rows[0]["gmonitor"]);
					stSql = stSql + " AND ncantida =1";
					stSql = stSql + " AND itiporig ='Q'";
					stSql = stSql + " AND ganoorig =" + AñoRegistro.ToString();
					stSql = stSql + " AND gnumorig =" + NumeroRegistro.ToString();
					stSql = stSql + " AND iborrado='S'";

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
					RsAux = new DataSet();
					tempAdapter_3.Fill(RsAux);
					if (RsAux.Tables[0].Rows.Count != 0)
					{
						RsAux.MoveFirst();
						bEncontrado = true;
					}
                    SqlDataAdapter tempAdapter_4 = null;

                    if (bEncontrado)
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE fila_id=" + Convert.ToString(RsAux.Tables[0].Rows[0]["fila_id"]);
                        tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
						tempAdapter_4.Fill(rsFactura);
						rsFactura.Edit();
						rsFactura.Tables[0].Rows[0]["iborrado"] = DBNull.Value;
					}
					else
					{
						stSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE 1 = 2";
                        tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
						rsFactura = new DataSet();
                        tempAdapter_4.Fill(rsFactura);
						rsFactura.AddNew();
					}

					RsAux.Close();

					InicializarDatos();
					rsFactura.Tables[0].Rows[0]["gcodeven"] = "AY";
					rsFactura.Tables[0].Rows[0]["gpersona"] = rsIntervencion.Tables[0].Rows[0]["gmonitor"];
					rsFactura.Tables[0].Rows[0]["ncantida"] = 1;
					//oscar 29/10/03
					rsFactura.Tables[0].Rows[0]["itiporig"] = "Q";
					rsFactura.Tables[0].Rows[0]["ganoorig"] = AñoRegistro;
					rsFactura.Tables[0].Rows[0]["gnumorig"] = NumeroRegistro;
					//-----
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_4);
                    tempAdapter_4.Update(rsFactura, rsFactura.Tables[0].TableName);
					rsFactura.Close();

				}

			}
			//---------FIN facturacion movimiento para el Monitorista-----------------------------


		}
	}
}