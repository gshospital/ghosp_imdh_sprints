using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeHelpers.DB.DAO;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace DLLExitus
{
	public partial class DFI170F1
		: Telerik.WinControls.UI.RadForm
	{
		string SQLT = String.Empty;
		string SQLF = String.Empty;
		string SQLTEXT = String.Empty;
		DataSet RrSQLTEXT = null;
		string stGrupoHo = String.Empty;
		string stNombreHo = String.Empty;
		string stDireccHo = String.Empty;
		string stFormfili = String.Empty;
		string camino = String.Empty;
		DataSet RrSQLF = null;
		int i = 0;
		bool vbBase = false;
		DAODatabaseHelper bs = null;
		DAORecordSetHelper Rs = null;
		string vstPathTemp = String.Empty;
		bool espera = false;
		public DFI170F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//Oscar C Marzo 2010
			if (!OptionEntidad.IsChecked && !OptionServicio.IsChecked)
			{
				string tempRefParam = "";
				short tempRefParam2 = 1040;
				String[] tempRefParam3 = new String[] {"Tipo de informe"};
				MExitus.clasemensaje.RespuestaMensaje(MExitus.NombreApli, tempRefParam, tempRefParam2, MExitus.GConexion, tempRefParam3);
				return;
			}
			//----------
			if (OptionEntidad.IsChecked)
			{
				if (rbTListado[0].IsChecked)
				{
					ConsultaPorHospital();
				}
				else
				{
					if (rbTListado[1].IsChecked)
					{
						ConsultaPorFiliacion();
					}
				}
			}
			else if (OptionServicio.IsChecked)
			{ 
				ConsultaPorHospital_Servicio();
			}
		}

		private void CbbInspeccion_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			if (CbbInspeccion.Text == "")
			{                
                CbbInspeccion.SelectedIndex = -1;
			}
			Activar_Aceptar();
		}

		private void CbbInspeccion_ClickEvent(Object eventSender, EventArgs eventArgs)
		{
			Activar_Aceptar();
		}
		
		private bool isInitializingComponent;
		private void cbbSociedad_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			if (cbbSociedad.Text == "")
			{
				cbbSociedad.SelectedIndex = -1;
			}
			Activar_Aceptar();
		}

		private void cbbSociedad_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			if (cbbSociedad.SelectedIndex != -1)
			{
				if (Convert.ToInt32(cbbSociedad.SelectedValue) == MExitus.CodSS)
				{
					if (CbbInspeccion.Visible)
					{
						CbbInspeccion.Enabled = true;
					}
					Activar_Aceptar();
				}
				else
				{
					if (CbbInspeccion.Visible)
					{                        
                        CbbInspeccion.Enabled = false;
                        CbbInspeccion.SelectedIndex = -1;
                    }					
				}
			}
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			string PathBasedatos = String.Empty;
			if (this.Cursor != Cursors.WaitCursor)
			{
				try
				{
					PathBasedatos = MExitus.camino2 + "\\EXITUS.mdb";
				}
				catch
				{
				}
				if (ErrorHandlingHelper.ResumeNextExpr<bool>(() => {return FileSystem.Dir(PathBasedatos, FileAttribute.Normal) != "";}))
					{
						try
						{
							File.Delete(PathBasedatos); // borro la base de datos
						}
						catch
						{
						}
					}
					try
					{
						this.Close();
					}
					catch
					{
					}
				}
			}

		private void DFI170F1_Activated(Object eventSender, EventArgs eventArgs)
		{
            if (ActivateHelper.myActiveForm != eventSender)
            {
                ActivateHelper.myActiveForm = (Form)eventSender;
                this.Left = (int)((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
                this.Top = (int)((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
            }
        }
        			
		private void DFI170F1_Load(Object eventSender, EventArgs eventArgs)
		{
					//UPGRADE_ISSUE: (2070) Constant App was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
					//UPGRADE_ISSUE: (2064) App property App.HelpFile was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Ayuda_DLLS.hlp");
					string SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SEGURSOC'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLENTIDAD, MExitus.GConexion);
					DataSet RrSQLENTIDAD = new DataSet();
					tempAdapter.Fill(RrSQLENTIDAD);
					//codigo de la SEGURIDAD SOCIAL					
					MExitus.CodSS = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
					//texto de la seguridad social					
					MExitus.TexSS = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
					SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'PRIVADO'";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLENTIDAD, MExitus.GConexion);
					RrSQLENTIDAD = new DataSet();
					tempAdapter_2.Fill(RrSQLENTIDAD);
					//codigo de regimen economco privado					
					MExitus.CodPriv = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
					//texto de regimen economco privado					
					MExitus.TexPriv = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
					
					RrSQLENTIDAD.Close();
					//CARGA DE LAS SOCIEDADES
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter("select * from DSOCIEDA order by dsocieda", MExitus.GConexion);
					MExitus.RrSQL = new DataSet();
					tempAdapter_3.Fill(MExitus.RrSQL);					
					foreach (DataRow iteration_row in MExitus.RrSQL.Tables[0].Rows)
					{
						cbbSociedad.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row["DSOCIEDA"]).Trim(), VbStrConv.ProperCase, 0), Convert.ToInt32(iteration_row["GSOCIEDA"])));
					}
					
					MExitus.RrSQL.Close();
					SQLTEXT = "select valfanu1 from sconsglo where gconsglo = 'FORMFILI'";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLTEXT, MExitus.GConexion);
					RrSQLTEXT = new DataSet();
					tempAdapter_4.Fill(RrSQLTEXT);
					
					stFormfili = Convert.ToString(RrSQLTEXT.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
					//CARGA DE LAS INSPECCIONES
					int i = 0;
					if (stFormfili != "P")
					{
						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter("select * from DINSPECC order by dinspecc", MExitus.GConexion);
						MExitus.RrSQL = new DataSet();
						tempAdapter_5.Fill(MExitus.RrSQL);
						CbbInspeccion.Items.Clear();
						i = 0;
                        
						if (MExitus.RrSQL.Tables[0].Rows.Count != 0)
						{							
							foreach (DataRow iteration_row_2 in MExitus.RrSQL.Tables[0].Rows)
							{								
								
								CbbInspeccion.Items.Add(Strings.StrConv(Convert.ToString(iteration_row_2["DINSPECC"]).Trim(), VbStrConv.ProperCase, 0));								
								//camaya todo_x_4
                                ///CbbInspeccion.set_Column(1, i, iteration_row_2["GINSPECC"]);

								//CbbInspeccion.ItemData(CbbInspeccion.NewIndex) = Trim(Rrsql("GINSPECC"))
								i++;
							}
						}						
						MExitus.RrSQL.Close();
					}
					else
					{
						Label2.Visible = false;
						CbbInspeccion.Visible = false;
					}
					//DATOS DEL HOSPITAL
					SQLTEXT = "select valfanu1 from sconsglo where gconsglo = 'GrupoHo'";
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQLTEXT, MExitus.GConexion);
					RrSQLTEXT = new DataSet();
					tempAdapter_6.Fill(RrSQLTEXT);					
					stGrupoHo = Convert.ToString(RrSQLTEXT.Tables[0].Rows[0]["valfanu1"]).Trim();

					SQLTEXT = "select valfanu1 from sconsglo where gconsglo = 'NOMBREHO'";
					SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(SQLTEXT, MExitus.GConexion);
					RrSQLTEXT = new DataSet();
					tempAdapter_7.Fill(RrSQLTEXT);					
					stNombreHo = Convert.ToString(RrSQLTEXT.Tables[0].Rows[0]["valfanu1"]).Trim();

					SQLTEXT = "select valfanu1 from sconsglo where gconsglo = 'direccho'";
					SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SQLTEXT, MExitus.GConexion);
					RrSQLTEXT = new DataSet();
					tempAdapter_8.Fill(RrSQLTEXT);
					
					stDireccHo = Convert.ToString(RrSQLTEXT.Tables[0].Rows[0]["valfanu1"]).Trim();

					SQLTEXT = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO = 'NOMAPLIC'";
					SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(SQLTEXT, MExitus.GConexion);
					RrSQLTEXT = new DataSet();
					tempAdapter_9.Fill(RrSQLTEXT);					
					MExitus.NombreApli = Convert.ToString(RrSQLTEXT.Tables[0].Rows[0]["valfanu1"]).Trim();
            					
					RrSQLTEXT.Close();
					sdcDFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    //camaya todo_x_4					
					//UPGRADE_ISSUE: (2064) SSCalendarWidgets_A.SSDateCombo property sdcHFecha.Mask was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//sdcHFecha.setMask(UpgradeStubs.SSCalendarWidgets_A_MaskDateConstants.getDayMonthYear());
					sdcHFecha.CustomFormat = "dd/MM/yyyy";
					sdcHFecha.Value = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
					Activar_Aceptar();

					string LitPersona = Serrores.ObtenerLiteralPersona(MExitus.GConexion);
					LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + "s";
					rbTListado[0].Text = LitPersona + " fallecidos en el centro";
					rbTListado[1].Text = LitPersona + " con �xitus en filiaci�n";

					//Oscar C Marzo 2010
					OptionServicio.Text = LitPersona + " fallecidos en el centro por Servicio";
					rbTListado[0].Text = LitPersona + " fallecidos en el centro";
					rbTListado[1].Text = LitPersona + " con �xitus en filiaci�n";
					//CARGA DE LOS SERVICIOS
					cbbServicio.Items.Add("<<Todos>>");
					SqlDataAdapter tempAdapter_10 = new SqlDataAdapter("select * from DSERVICI where iurgenci='S' or ihospita='S' order by dnomserv", MExitus.GConexion);
					MExitus.RrSQL = new DataSet();
					tempAdapter_10.Fill(MExitus.RrSQL);					
					foreach (DataRow iteration_row_3 in MExitus.RrSQL.Tables[0].Rows)
					{
						cbbServicio.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row_3["dnomserv"]).Trim(), VbStrConv.ProperCase, 0), Convert.ToInt32(iteration_row_3["gservici"])));						
					}
					
					MExitus.RrSQL.Close();
					//FrameEntidad.Enabled = False
					//FrameServicio.Enabled = False
					OptionEntidad.IsChecked = true;
					//---------
			}

		private void ConsultaPorHospital()
			{
            //		this.Cursor = Cursors.WaitCursor;
            //		DataSet RrDiag = null; //'
            //		string HospitalOIngreso = String.Empty;

            //		vstPathTemp = MExitus.camino2 + "\\" + MExitus.stbasedatos;
            //		if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
            //		{
            //			vbBase = true;
            //		}
            //		if (!vbBase)
            //		{ // SI est� dado
            //			//UPGRADE_ISSUE: (2064) DAO.Workspace method DBEngine.Workspaces.CreateDatabase was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //			bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].CreateDatabase(vstPathTemp, DAO.LanguageConstants.dbLangSpanish, null);
            //			vbBase = true;
            //		}
            //		else
            //		{
            //			//UPGRADE_WARNING: (2065) DAO.Workspace method DBEngine.Workspaces.OpenDatabase has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
            //			bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].OpenDatabase("<Connection-String>");
            //			foreach (TableDefHelper TABLA in bs.TableDefs)
            //			{
            //				if (TABLA.Name.ToUpper() == "EXITUS")
            //				{
            //					DbCommand TempCommand = bs.Connection.CreateCommand();
            //					TempCommand.CommandText = "DROP TABLE EXITUS";
            //					TempCommand.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
            //					TempCommand.ExecuteNonQuery();
            //				}
            //			}
            //		}

            //		DbCommand TempCommand_2 = bs.Connection.CreateCommand();
            //		TempCommand_2.CommandText = "CREATE TABLE EXITUS (campo1 text, " + 
            //		                            " campo2 text, campo3 text ,campo4 text, campo5 text, " + 
            //		                            " campo6 text, campo7 text, campo8 text, campo9 text, campo10 text, campo11 text)";
            //		TempCommand_2.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
            //		TempCommand_2.ExecuteNonQuery();
            //		string sqltemporal = "select * from EXITUS where 1=2";
            //		Rs = bs.OpenRecordset(sqltemporal);
            //		string SQLU = " SELECT GMOTALTA FROM DMOTALTA WHERE ICEXITUS = 'S'";
            //		SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLU, MExitus.GConexion);
            //		DataSet RrSQLU = new DataSet();
            //		tempAdapter.Fill(RrSQLU);
            //		//CODIGO DE EXITUS					
            //		int EXITUS = Convert.ToInt32(RrSQLU.Tables[0].Rows[0]["GMOTALTA"]);

            //		RrSQLU.Close();
            //		//minimas condiciones para que sea fallecido en el hospital

            //		SQLT = "SELECT DISTINCT DPACIENT.GIDENPAC, DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " + 
            //		       " DPACIENT.itipsexo, DPACIENT.ddirepac, DPACIENT.gcodipos, DPACIENT.nafiliac, " + 
            //		       " DPACIENT.iexitusp, HDOSSIER.ghistoria, DPOBLACI.dpoblaci, " + 
            //		       " DSOCIEDA.DSOCIEDA, AEPISADM.faltplan, AEPISADM.gdiagalt, AEPISADM.odiagalt " + 
            //		       " From ((((AEPISADM INNER JOIN AMOVIFIN ON " + 
            //		       " AEPISADM.ganoadme = AMOVIFIN.ganoregi AND " + 
            //		       " AEPISADM.gnumadme = AMOVIFIN.gnumregi AND " + 
            //		       " AEPISADM.faltplan = AMOVIFIN.ffinmovi) " + 
            //		       " INNER JOIN DPACIENT ON AEPISADM.gidenpac = DPACIENT.gidenpac) " + 
            //		       " LEFT OUTER JOIN DPOBLACI ON DPACIENT.gpoblaci = DPOBLACI.gpoblaci) " + 
            //		       " LEFT OUTER JOIN DSOCIEDA ON AMOVIFIN.gsocieda = DSOCIEDA.gsocieda) " + 
            //		       " LEFT OUTER JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac " + 
            //		       " Where aepisadm.gmotalta = " + EXITUS.ToString() + " ";
            //		//SI TIENE SOCIEDAD
            //		if (cbbSociedad.SelectedIndex != -1)
            //		{
            //			//si no es privado
            //			SQLT = SQLT + " and amovifin.gsocieda = " + cbbSociedad.GetItemData(cbbSociedad.SelectedIndex).ToString() + " ";
            //			if (cbbSociedad.GetItemData(cbbSociedad.SelectedIndex) == MExitus.CodSS)
            //			{
            //				if (CbbInspeccion.Visible)
            //				{
            //					if (Convert.ToDouble(CbbInspeccion.get_ListIndex()) != -1)
            //					{
            //						//UPGRADE_WARNING: (1068) CbbInspeccion.Column() of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
            //						object tempRefParam = 1;
            //						object tempRefParam2 = CbbInspeccion.get_ListIndex();
            //						SQLT = SQLT + " and amovifin.GINSPECC = '" + Convert.ToString(CbbInspeccion.get_Column(ref tempRefParam, ref tempRefParam2)) + "' ";
            //						//SQLT = SQLT & " and amovifin.GINSPECC = '" & CbbInspeccion.ItemData(CbbInspeccion.ListIndex) & "' "
            //					}
            //				}
            //			}
            //		}
            //		string tempRefParam3 = sdcDFecha.Text;
            //		object tempRefParam4 = sdcHFecha.Text + " 23:59:59";
            //		SQLT = SQLT + " and (AEPISADM.faltplan >= " + Serrores.FormatFecha(ref tempRefParam3) + " and " + 
            //		       " AEPISADM.faltplan <= " + Serrores.FormatFechaHMS(ref tempRefParam4) + ")";
            //		if (rbOListado[0].IsChecked)
            //		{
            //			SQLT = SQLT + " ORDER BY DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac";
            //		}
            //		if (rbOListado[1].IsChecked)
            //		{
            //			SQLT = SQLT + " ORDER BY AEPISADM.faltplan";
            //		}
            //		SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLT, MExitus.GConexion);
            //		MExitus.RrSQL = new DataSet();
            //		tempAdapter_2.Fill(MExitus.RrSQL);
            //		if (MExitus.RrSQL.Tables[0].Rows.Count != 0)
            //		{
            //			//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
            //			MExitus.RrSQL.MoveFirst();
            //			foreach (DataRow iteration_row in MExitus.RrSQL.Tables[0].Rows)
            //			{
            //				Rs.AddNew();
            //				Rs["campo1"] = iteration_row["ghistoria"];

            //				if (!Convert.IsDBNull(iteration_row["dnombpac"]))
            //				{
            //					Rs["campo2"] = Convert.ToString(iteration_row["dnombpac"]).Trim();
            //				}

            //				if (!Convert.IsDBNull(iteration_row["dape1pac"]))
            //				{
            //					Rs["campo2"] = Convert.ToString(Rs["campo2"]).Trim() + " " + Convert.ToString(iteration_row["dape1pac"]).Trim();
            //				}

            //				if (!Convert.IsDBNull(iteration_row["dape2pac"]))
            //				{
            //					Rs["campo2"] = Convert.ToString(Rs["campo2"]).Trim() + " " + Convert.ToString(iteration_row["dape2pac"]).Trim();
            //				}
            //				if (Convert.ToString(iteration_row["itipsexo"]) == "H")
            //				{
            //					Rs["campo3"] = "Hombre";
            //				}
            //				if (Convert.ToString(iteration_row["itipsexo"]) == "M")
            //				{
            //					Rs["campo3"] = "Mujer";
            //				}
            //				if (Convert.ToString(iteration_row["itipsexo"]) == "I")
            //				{
            //					Rs["campo3"] = "Indeterminado";
            //				}
            //				Rs["campo4"] = Convert.ToDateTime(iteration_row["faltplan"]).ToString("dd/MM/yyyy");
            //				Rs["campo5"] = iteration_row["ddirepac"];
            //				Rs["campo6"] = iteration_row["gcodipos"];
            //				Rs["campo7"] = iteration_row["dpoblaci"];
            //				Rs["campo8"] = iteration_row["DSOCIEDA"];
            //				if (Convert.ToString(iteration_row["iexitusp"]) == "N")
            //				{
            //					Rs["campo9"] = "No";
            //				}
            //				if (Convert.ToString(iteration_row["iexitusp"]) == "S")
            //				{
            //					Rs["campo9"] = "Si";
            //				}

            //				if (!Convert.IsDBNull(iteration_row["gdiagalt"]))
            //				{ //'
            //					SQLT = "SELECT dnombdia FROM DDIAGNOS " + 
            //					       "WHERE gcodidia = '" + Convert.ToString(iteration_row["gdiagalt"]) + "'";
            //					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLT, MExitus.GConexion);
            //					RrDiag = new DataSet();
            //					tempAdapter_3.Fill(RrDiag);
            //					Rs["campo10"] = RrDiag.Tables[0].Rows[0]["dnombdia"];

            //					RrDiag.Close();
            //				}
            //				else
            //				{
            //					Rs["campo10"] = iteration_row["odiagalt"];
            //				}

            //				Rs["campo11"] = (Convert.IsDBNull(iteration_row["nafiliac"])) ? "" : Convert.ToString(iteration_row["nafiliac"]).Trim();

            //				Rs.Update();
            //			}
            //		}

            //		MExitus.RrSQL.Close();
            //		Rs.Close();
            //		UpgradeHelpers.DB.TransactionManager.DeEnlist(bs.Connection);
            //		bs.Close();
            //		DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].Close();

            //                 //camaya todo_x_4
            //		//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.ReportFileName = MExitus.camino2 + "\\..\\ElementosComunes\\RPT\\dfi170r1.rpt";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["Grupo"] = "\"" + stGrupoHo + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["DireccionH"] = "\"" + stDireccHo + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["NombreH"] = "\"" + stNombreHo + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["FechaDesde"] = "\"" + DateTime.Parse(sdcDFecha.Value.Date).ToString("dd/MM/yyyy") + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["FechaHasta"] = "\"" + DateTime.Parse(sdcHFecha.Value.Date).ToString("dd/MM/yyyy") + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["SOCIEDAD"] = "\"" + cbbSociedad.Text + "\"";
            //		if (CbbInspeccion.Visible)
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			//MExitus.objeto_crystal.Formulas["lbINSPECCION"] = "\"" + "Inspecci�n:" + "\"";
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			//MExitus.objeto_crystal.Formulas["INSPECCION"] = "\"" + CbbInspeccion.Text + "\"";
            //		}
            //		else
            //		{
            //                     //camaya todo_x_4
            //                     //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //                     //MExitus.objeto_crystal.Formulas["lbINSPECCION"] = "\"" + "" + "\"";
            //                     //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //                     //MExitus.objeto_crystal.Formulas["INSPECCION"] = "\"" + "" + "\"";
            //         }

            //         //LISTADO DE PACIENTES FALLECIDOS EN EL HOSPITAL
            //         string LitPersona = Serrores.ObtenerLiteralPersona(MExitus.GConexion);
            //		LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["LitPers"] = "\"" + LitPersona + "\"";

            //		string HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(MExitus.GConexion);
            //		if (HospitalOCentro == "S")
            //		{
            //			HospitalOIngreso = "HOSPITAL";
            //		}
            //		else
            //		{
            //			HospitalOIngreso = "CENTRO";
            //		}
            //		string StrTitulo = "LISTADO DE " + LitPersona.ToUpper() + "S" + " FALLECIDOS EN EL " + HospitalOIngreso;

            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["TITULO"] = "\"" + StrTitulo + "\"";
            //		if (rbOListado[0].IsChecked)
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			//MExitus.objeto_crystal.Formulas["ORDEN"] = "\"" + "nombre" + "\"";
            //		}
            //		else
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			//MExitus.objeto_crystal.Formulas["ORDEN"] = "\"" + "exitus en filiaci�n" + "\"";
            //		}
            //                 //camaya todo_x_4
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["ORIGEN"] = "\"" + "(HOSPITALIZACI�N)" + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["NINFORME"] = "\"" + "DFI170R1" + "\"";
            //		if (Serrores.ExisteAseguradora(MExitus.GConexion))
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			//MExitus.objeto_crystal.Formulas["IASEGURA"] = "'S'";
            //		}
            //		else
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			//MExitus.objeto_crystal.Formulas["IASEGURA"] = "'N'";
            //		}

            //                 //camaya todo_x_4
            //		//UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.DataFiles[0] = MExitus.camino2 + "\\" + MExitus.stbasedatos;
            //		//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Connect = "DSN=" + MExitus.NOMBRE_DSN_ACCESS + "";
            //		//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
            //		IMPRIMIR_ADMISION();

            //		//AQUI HACE EL INFORME PARA URGENCIAS
            //		SQLT = "SELECT distinct dpacient.gidenpac, UEPISURG.faltaurg, UEPISURG.gdiagnos, UEPISURG.odiagnos, HDOSSIER.ghistoria, DSOCIEDA.dsocieda, " + 
            //		       " DPACIENT.gidenpac, DPACIENT.dnombpac, DPACIENT.dape1pac, " + 
            //		       " DPACIENT.dape2pac, DPACIENT.itipsexo, DPACIENT.ddirepac, " + 
            //		       " DPACIENT.gcodipos, DPACIENT.iexitusp, DPACIENT.nafiliac, DPOBLACI.DPOBLACI " + 
            //		       " From (((UEPISURG LEFT OUTER JOIN DSOCIEDA ON " + 
            //		       " UEPISURG.gsocieda = DSOCIEDA.gsocieda) " + 
            //		       " INNER JOIN DPACIENT ON UEPISURG.gidenpac = DPACIENT.gidenpac) " + 
            //		       " LEFT OUTER JOIN HDOSSIER ON UEPISURG.gidenpac = HDOSSIER.gidenpac) " + 
            //		       " LEFT JOIN DPOBLACI ON DPACIENT.gpoblaci = DPOBLACI.gpoblaci " + 
            //		       " Where UEPISURG.FALTAURG IS NOT NULL  and " + 
            //		       " UEPISURG.gmotalta = " + EXITUS.ToString() + " ";

            //		if (cbbSociedad.SelectedIndex != -1)
            //		{
            //			SQLT = SQLT + " and uepisurg.gsocieda = " + cbbSociedad.GetItemData(cbbSociedad.SelectedIndex).ToString() + " ";
            //			if (cbbSociedad.GetItemData(cbbSociedad.SelectedIndex) == MExitus.CodSS)
            //			{
            //				if (CbbInspeccion.Visible)
            //				{
            //					if (Convert.ToDouble(CbbInspeccion.get_ListIndex()) != -1)
            //					{
            //						//UPGRADE_WARNING: (1068) CbbInspeccion.Column() of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
            //						object tempRefParam5 = 1;
            //						object tempRefParam6 = CbbInspeccion.get_ListIndex();
            //						SQLT = SQLT + " and uepisurg.ginspecc = '" + Convert.ToString(CbbInspeccion.get_Column(ref tempRefParam5, ref tempRefParam6)) + "' ";
            //						//SQLT = SQLT & " and uepisurg.ginspecc = '" & CbbInspeccion.ItemData(CbbInspeccion.ListIndex) & "' "
            //					}
            //				}
            //			}
            //		}
            //		string tempRefParam7 = sdcDFecha.Text;
            //		object tempRefParam8 = sdcHFecha.Text + " 23:59:59";
            //		SQLT = SQLT + " and (uepisurg.faltaurg >= " + Serrores.FormatFecha(ref tempRefParam7) + " and " + 
            //		       " uepisurg.faltaurg <= " + Serrores.FormatFechaHMS(ref tempRefParam8) + ")";
            //		if (rbOListado[0].IsChecked)
            //		{
            //			SQLT = SQLT + " ORDER BY DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac";
            //		}
            //		if (rbOListado[1].IsChecked)
            //		{
            //			SQLT = SQLT + " ORDER BY UEPISURG.faltaurg";
            //		}

            //		vstPathTemp = MExitus.camino2 + "\\" + MExitus.stbasedatos;
            //		if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
            //		{
            //			vbBase = true;
            //		}
            //		if (!vbBase)
            //		{ // SI est� dado
            //			//UPGRADE_ISSUE: (2064) DAO.Workspace method DBEngine.Workspaces.CreateDatabase was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //			bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].CreateDatabase(vstPathTemp, DAO.LanguageConstants.dbLangSpanish, null);
            //			vbBase = true;
            //		}
            //		else
            //		{
            //			//UPGRADE_WARNING: (2065) DAO.Workspace method DBEngine.Workspaces.OpenDatabase has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
            //			bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].OpenDatabase("<Connection-String>");
            //			foreach (TableDefHelper TABLA in bs.TableDefs)
            //			{
            //				if (TABLA.Name.ToUpper() == "EXITUS")
            //				{
            //					DbCommand TempCommand_3 = bs.Connection.CreateCommand();
            //					TempCommand_3.CommandText = "DROP TABLE EXITUS";
            //					TempCommand_3.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
            //					TempCommand_3.ExecuteNonQuery();
            //				}
            //			}
            //		}

            //		DbCommand TempCommand_4 = bs.Connection.CreateCommand();
            //		TempCommand_4.CommandText = "CREATE TABLE EXITUS (campo1 text, " + 
            //		                            " campo2 text, campo3 text ,campo4 text, campo5 text, " + 
            //		                            " campo6 text, campo7 text, campo8 text, campo9 text, campo10 text, campo11 text)";
            //		TempCommand_4.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
            //		TempCommand_4.ExecuteNonQuery();

            //		sqltemporal = "select * from EXITUS where 1=2";
            //		Rs = bs.OpenRecordset(sqltemporal);
            //		SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLT, MExitus.GConexion);
            //		MExitus.RrSQL = new DataSet();
            //		tempAdapter_4.Fill(MExitus.RrSQL);
            //		if (MExitus.RrSQL.Tables[0].Rows.Count != 0)
            //		{
            //			//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
            //			MExitus.RrSQL.MoveFirst();
            //			foreach (DataRow iteration_row_2 in MExitus.RrSQL.Tables[0].Rows)
            //			{
            //				Rs.AddNew();
            //				Rs["campo1"] = iteration_row_2["ghistoria"];

            //				if (!Convert.IsDBNull(iteration_row_2["dnombpac"]))
            //				{
            //					Rs["campo2"] = Convert.ToString(iteration_row_2["dnombpac"]).Trim();
            //				}

            //				if (!Convert.IsDBNull(iteration_row_2["dape1pac"]))
            //				{
            //					Rs["campo2"] = Convert.ToString(Rs["campo2"]).Trim() + " " + Convert.ToString(iteration_row_2["dape1pac"]).Trim();
            //				}

            //				if (!Convert.IsDBNull(iteration_row_2["dape2pac"]))
            //				{
            //					Rs["campo2"] = Convert.ToString(Rs["campo2"]).Trim() + " " + Convert.ToString(iteration_row_2["dape2pac"]).Trim();
            //				}
            //				if (Convert.ToString(iteration_row_2["itipsexo"]) == "H")
            //				{
            //					Rs["campo3"] = "Hombre";
            //				}
            //				if (Convert.ToString(iteration_row_2["itipsexo"]) == "M")
            //				{
            //					Rs["campo3"] = "Mujer";
            //				}
            //				if (Convert.ToString(iteration_row_2["itipsexo"]) == "I")
            //				{
            //					Rs["campo3"] = "Indeterminado";
            //				}
            //				Rs["campo4"] = Convert.ToDateTime(iteration_row_2["faltaurg"]).ToString("dd/MM/yyyy");
            //				Rs["campo5"] = iteration_row_2["ddirepac"];
            //				Rs["campo6"] = iteration_row_2["gcodipos"];
            //				Rs["campo7"] = iteration_row_2["dpoblaci"];
            //				Rs["campo8"] = iteration_row_2["DSOCIEDA"];
            //				if (Convert.ToString(iteration_row_2["iexitusp"]) == "N")
            //				{
            //					Rs["campo9"] = "No";
            //				}
            //				if (Convert.ToString(iteration_row_2["iexitusp"]) == "S")
            //				{
            //					Rs["campo9"] = "Si";
            //				}

            //				if (!Convert.IsDBNull(iteration_row_2["gdiagnos"]))
            //				{ //'
            //					SQLT = "SELECT dnombdia FROM DDIAGNOS " + 
            //					       "WHERE gcodidia = '" + Convert.ToString(iteration_row_2["gdiagnos"]) + "'";
            //					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQLT, MExitus.GConexion);
            //					RrDiag = new DataSet();
            //					tempAdapter_5.Fill(RrDiag);
            //					Rs["campo10"] = RrDiag.Tables[0].Rows[0]["dnombdia"];

            //					RrDiag.Close();
            //				}
            //				else
            //				{
            //					Rs["campo10"] = iteration_row_2["odiagnos"];
            //				} //'
            //				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
            //				Rs["campo11"] = (Convert.IsDBNull(iteration_row_2["nafiliac"])) ? "" : Convert.ToString(iteration_row_2["nafiliac"]).Trim();
            //				Rs.Update();
            //			}

            //		}

            //		MExitus.RrSQL.Close();
            //		Rs.Close();
            //		UpgradeHelpers.DB.TransactionManager.DeEnlist(bs.Connection);
            //		bs.Close();
            //		DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].Close();
            //        //camaya todo_x_4		
            //                 //UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.ReportFileName = MExitus.camino2 + "\\..\\ElementosComunes\\RPT\\dfi170r1.rpt";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["Grupo"] = "\"" + stGrupoHo + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["DireccionH"] = "\"" + stDireccHo + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["NombreH"] = "\"" + stNombreHo + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["FechaDesde"] = "\"" + DateTime.Parse(sdcDFecha.Value.Date).ToString("dd/MM/yyyy") + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["FechaHasta"] = "\"" + DateTime.Parse(sdcHFecha.Value.Date).ToString("dd/MM/yyyy") + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		//MExitus.objeto_crystal.Formulas["SOCIEDAD"] = "\"" + cbbSociedad.Text + "\"";
            //		if (CbbInspeccion.Visible)
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			//MExitus.objeto_crystal.Formulas["lbINSPECCION"] = "\"" + "Inspecci�n:" + "\"";
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			//MExitus.objeto_crystal.Formulas["INSPECCION"] = "\"" + CbbInspeccion.Text + "\"";
            //		}
            //		else
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			((MExitus.objeto_crystal.Formulas["lbINSPECCION"] = "\"" + "" + "\"";
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			((MExitus.objeto_crystal.Formulas["INSPECCION"] = "\"" + "" + "\"";
            //		}

            //		LitPersona = Serrores.ObtenerLiteralPersona(MExitus.GConexion);
            //		LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		((MExitus.objeto_crystal.Formulas["LitPers"] = "\"" + LitPersona + "\"";

            //		HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(MExitus.GConexion);
            //		if (HospitalOCentro == "S")
            //		{
            //			HospitalOIngreso = "HOSPITAL";
            //		}
            //		else
            //		{
            //			HospitalOIngreso = "CENTRO";
            //		}
            //		StrTitulo = "LISTADO DE " + LitPersona.ToUpper() + "S" + " FALLECIDOS EN EL " + HospitalOIngreso;


            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		MExitus.objeto_crystal.Formulas["TITULO"] = "\"" + StrTitulo + "\"";
            //		if (rbOListado[0].Checked)
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			MExitus.objeto_crystal.Formulas["ORDEN"] = "\"" + "nombre" + "\"";
            //		}
            //		else
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			MExitus.objeto_crystal.Formulas["ORDEN"] = "\"" + "exitus en filiaci�n" + "\"";
            //		}
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		MExitus.objeto_crystal.Formulas["ORIGEN"] = "\"" + "(URGENCIAS)" + "\"";
            //		//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		MExitus.objeto_crystal.Formulas["NINFORME"] = "\"" + "DFI170R2" + "\"";
            //		if (Serrores.ExisteAseguradora(MExitus.GConexion))
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			MExitus.objeto_crystal.Formulas["IASEGURA"] = "'S'";
            //		}
            //		else
            //		{
            //			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //			MExitus.objeto_crystal.Formulas["IASEGURA"] = "'N'";
            //		}

            //		//objeto_crystal.DataFiles(0) = camino2 & "\Exitus.mdb"
            //		//UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		MExitus.objeto_crystal.DataFiles[0] = MExitus.camino2 + "\\" + MExitus.stbasedatos;
            //		//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		MExitus.objeto_crystal.Connect = "DSN=" + MExitus.NOMBRE_DSN_ACCESS + "";
            //		//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //		MExitus.objeto_crystal.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
            //		IMPRIMIR_URGENCIAS();
            //		//UPGRADE_ISSUE: (2036) Form property DFI170F1.MousePointer does not support custom mousepointers. More Information: http://www.vbtonet.com/ewis/ewi2036.aspx
            this.Cursor = Cursors.Default;
			}

		private void sdcDFecha_Enter(Object eventSender, EventArgs eventArgs)
		{
            //Para que no supere la fecha actual
            MExitus.FechaHoraSistema();            
            sdcDFecha.MaxDate = Convert.ToDateTime(DateTime.Parse(MExitus.vstFechaHoraSis).ToString("dd/MM/yyyy"));
            //sdcDFecha.setMask(UpgradeStubs.SSCalendarWidgets_A_MaskDateConstants.getDayMonthYear());
            sdcDFecha.Value = sdcDFecha.MaxDate;
            sdcDFecha.CustomFormat = "dd/MM/yyyy"; //Para que el formato de fecha sea D�a/Mes/A�o
        }

		private void sdcDFecha_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_Aceptar();
		}

		private void sdcHFecha_Enter(Object eventSender, EventArgs eventArgs)
		{
            //Para que no supere la fecha actual
            MExitus.FechaHoraSistema();            
            sdcHFecha.MaxDate = Convert.ToDateTime(DateTime.Parse(MExitus.vstFechaHoraSis).ToString("dd/MM/yyyy"));
            //Para que el formato de fecha sea D�a/Mes/A�o            
            //sdcHFecha.setMask(UpgradeStubs.SSCalendarWidgets_A_MaskDateConstants.getDayMonthYear());
            sdcHFecha.Value = sdcHFecha.MaxDate;
            sdcHFecha.CustomFormat = "dd/MM/yyyy";
        }

		private void Activar_Aceptar()
		{
			if (sdcDFecha.Text == "" || sdcHFecha.Text == "")
			{
				//si no existen fechas en cualquiera de los controles
				cbAceptar.Enabled = false;
				return;
			}
			else
			{
				if (sdcDFecha.Value.Date > sdcHFecha.Value.Date)
				{
					//Si la fecha de inicio es mayor que la de final
					cbAceptar.Enabled = false;
					string tempRefParam = "";
					short tempRefParam2 = 1020;
					string[] tempRefParam3 = new string[] { Label4.Text, "mayor", Label3.Text};
					MExitus.clasemensaje.RespuestaMensaje(MExitus.NombreApli, tempRefParam, tempRefParam2, MExitus.GConexion, tempRefParam3);
				}
				else
				{
					cbAceptar.Enabled = true;
				}
			}
		}

		private void ConsultaPorFiliacion()
			{
					this.Cursor = Cursors.WaitCursor;

					SQLF = " SELECT GMOTALTA FROM DMOTALTA WHERE ICEXITUS = 'S'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLF, MExitus.GConexion);
					RrSQLF = new DataSet();
					tempAdapter.Fill(RrSQLF);
					//CODIGO DE EXITUS
					
					RrSQLF.Close();
					vstPathTemp = MExitus.camino2 + "\\" + MExitus.stbasedatos;
					if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
					{
						vbBase = true;
					}
					if (!vbBase)
					{ // SI est� dado
                        //camaya todo_x_4
						//UPGRADE_ISSUE: (2064) DAO.Workspace method DBEngine.Workspaces.CreateDatabase was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						//bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].CreateDatabase(vstPathTemp, DAO.LanguageConstants.dbLangSpanish, null);
						vbBase = true;
					}
					else
					{
						
						bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].OpenDatabase("<Connection-String>");
						foreach (TableDefHelper TABLA in bs.TableDefs)
						{
							if (TABLA.Name.ToUpper() == "EXITUS")
							{
								DbCommand TempCommand = bs.Connection.CreateCommand();
								TempCommand.CommandText = "DROP TABLE EXITUS";
								TempCommand.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
								TempCommand.ExecuteNonQuery();
							}
						}
					}
					DbCommand TempCommand_2 = bs.Connection.CreateCommand();
					TempCommand_2.CommandText = "CREATE TABLE EXITUS (campo1 text, " + 
					                            " campo2 text, campo3 text ,campo4 text, campo5 text, " + 
					                            " campo6 text, campo7 text, campo8 text, campo9 text)";
					TempCommand_2.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
					TempCommand_2.ExecuteNonQuery();
					string sqltemporal = "select * from EXITUS where 1=2";
					Rs = bs.OpenRecordset(sqltemporal);
					SQLF = "SELECT DISTINCT dpacient.gidenpac," + 
					       " DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " + 
					       " DPACIENT.fnacipac, DPACIENT.itipsexo, DPACIENT.ddirepac, " + 
					       " DPACIENT.gcodipos, DPACIENT.iexitusp, DPACIENT.ffalleci," + 
					       " DPOBLACI.dpoblaci, DSOCIEDA.dsocieda, HDOSSIER.ghistoria " + 
					       " From " + 
					       " DPACIENT ";
					if (CbbInspeccion.Visible)
					{
						if (Convert.ToInt32(CbbInspeccion.SelectedIndex) != -1)
						{
							SQLF = SQLF + " inner join DENTIPAC on dpacient.gidenpac = DENTIPAC.gidenpac ";
						}
					}
					SQLF = SQLF + " left  join HDOSSIER on dpacient.gidenpac = hdossier.gidenpac " + 
					       " left  join DPOBLACI on dpacient.gpoblaci = dpoblaci.gpoblaci " + 
					       " left  join DSOCIEDA on dpacient.gsocieda = dsocieda.gsocieda " + 
					       " Where ";

					//si se selecciona alguno
					if (cbbSociedad.SelectedIndex != -1)
					{
						//si es distinto de privado
						SQLF = SQLF + " dpacient.gsocieda = " + Convert.ToInt32(cbbSociedad.SelectedValue).ToString() + " and ";
						if (CbbInspeccion.Visible)
						{
							if (Convert.ToInt32(CbbInspeccion.SelectedIndex) != -1)
							{
								//UPGRADE_WARNING: (1068) CbbInspeccion.Column() of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
								object tempRefParam = 1;
								object tempRefParam2 = CbbInspeccion.SelectedIndex;
                                //camaya todo_x_4
								//SQLF = SQLF + " DENTIPAC.GINSPECC = '" + Convert.ToString(CbbInspeccion.get_Column(ref tempRefParam, ref tempRefParam2)) + "' AND  DENTIPAC.gsocieda = " + Convert.ToInt32(cbbSociedad.SelectedValue) + " and ";
								//SQLF = SQLF & " DENTIPAC.GINSPECC = '" & CbbInspeccion.ItemData(CbbInspeccion.ListIndex) & "' AND  DENTIPAC.gsocieda = " & cbbSociedad.ItemData(cbbSociedad.ListIndex) & " and ";
							}
						}
					}
					else
					{
						if (cbbSociedad.SelectedIndex != -1)
						{
							SQLF = SQLF + " dpacient.gsocieda = " + Convert.ToInt32(cbbSociedad.SelectedValue).ToString() + " and ";
						}
					}

					string tempRefParam3 = sdcDFecha.Text;
					string tempRefParam4 = sdcHFecha.Text;
					SQLF = SQLF + " dpacient.ffalleci BETWEEN " + Serrores.FormatFecha(tempRefParam3) + " and " + Serrores.FormatFecha(tempRefParam4) + " AND DPACIENT.IEXITUSP = 'S'";

					if (rbOListado[0].IsChecked)
					{
						SQLF = SQLF + " ORDER BY DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac";
					}
					if (rbOListado[1].IsChecked)
					{
						SQLF = SQLF + " ORDER BY dpacient.ffalleci ";
					}

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLF, MExitus.GConexion);
					MExitus.RrSQL = new DataSet();
					tempAdapter_2.Fill(MExitus.RrSQL);
					if (MExitus.RrSQL.Tables[0].Rows.Count != 0)
					{						
						foreach (DataRow iteration_row in MExitus.RrSQL.Tables[0].Rows)
						{
							Rs.AddNew();
							Rs["campo1"] = iteration_row["ghistoria"];
							
							if (!Convert.IsDBNull(iteration_row["dnombpac"]))
							{
								Rs["campo2"] = Convert.ToString(iteration_row["dnombpac"]).Trim();
							}
							
							if (!Convert.IsDBNull(iteration_row["dape1pac"]))
							{
								Rs["campo2"] = Convert.ToString(Rs["campo2"]).Trim() + " " + Convert.ToString(iteration_row["dape1pac"]).Trim();
							}
							
							if (!Convert.IsDBNull(iteration_row["dape2pac"]))
							{
								Rs["campo2"] = Convert.ToString(Rs["campo2"]).Trim() + " " + Convert.ToString(iteration_row["dape2pac"]).Trim();
							}
							if (Convert.ToString(iteration_row["itipsexo"]) == "H")
							{
								Rs["campo3"] = "Hombre";
							}
							if (Convert.ToString(iteration_row["itipsexo"]) == "M")
							{
								Rs["campo3"] = "Mujer";
							}
							if (Convert.ToString(iteration_row["itipsexo"]) == "I")
							{
								Rs["campo3"] = "Indeterminado";
							}
							Rs["campo4"] = Convert.ToDateTime(iteration_row["ffalleci"]).ToString("dd/MM/yyyy");
							Rs["campo5"] = iteration_row["ddirepac"];
							Rs["campo6"] = iteration_row["gcodipos"];
							Rs["campo7"] = iteration_row["dpoblaci"];
							Rs["campo8"] = Convert.ToString(iteration_row["DSOCIEDA"]).Trim();
							if (Convert.ToString(iteration_row["iexitusp"]) == "N")
							{
								Rs["campo9"] = "No";
							}
							if (Convert.ToString(iteration_row["iexitusp"]) == "S")
							{
								Rs["campo9"] = "Si";
							}
							Rs.Update();
						}
					}
					
					MExitus.RrSQL.Close();
					Rs.Close();
					UpgradeHelpers.DB.TransactionManager.DeEnlist(bs.Connection);
					bs.Close();
					DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].Close();
            //camaya todo_x_4		
            ////UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.ReportFileName = MExitus.camino2 + "\\..\\ElementosComunes\\RPT\\dfi170r2.rpt";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["Grupo"] = "\"" + stGrupoHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["DireccionH"] = "\"" + stDireccHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["NombreH"] = "\"" + stNombreHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["FechaDesde"] = "\"" + DateTime.Parse(sdcDFecha.Value.Date).ToString("dd/MM/yyyy") + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["FechaHasta"] = "\"" + DateTime.Parse(sdcHFecha.Value.Date).ToString("dd/MM/yyyy") + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["SOCIEDAD"] = "\"" + cbbSociedad.Text + "\"";
            if (CbbInspeccion.Visible)
					{
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MExitus.objeto_crystal.Formulas["lbINSPECCION"] = "\"" + "Inspecci�n:" + "\"";
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MExitus.objeto_crystal.Formulas["INSPECCION"] = "\"" + CbbInspeccion.Text + "\"";
            }
            else
					{
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MExitus.objeto_crystal.Formulas["lbINSPECCION"] = "\"" + "" + "\"";
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MExitus.objeto_crystal.Formulas["INSPECCION"] = "\"" + "" + "\"";
            }

            string LitPersona = Serrores.ObtenerLiteralPersona(MExitus.GConexion);
					LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
            //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["LitPers"] = "\"" + LitPersona + "\"";
            //LISTADO DE PACIENTES FALLECIDOS EN FILIACION
            string StrTitulo = "LISTADO DE " + LitPersona.ToUpper() + "S" + " FALLECIDOS EN FILIACION";

            //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["TITULO"] = "\"" + StrTitulo + "\"";
            if (rbOListado[0].IsChecked)
					{
                //camaya todo_x_4
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MExitus.objeto_crystal.Formulas["ORDEN"] = "\"" + "nombre" + "\"";
            }
            else
					{
                //camaya todo_x_4
                //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //MExitus.objeto_crystal.Formulas["ORDEN"] = "\"" + "exitus en filiaci�n" + "\"";
            }
            //camaya todo _x_4
            //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["ORIGEN"] = "\"" + "" + "\"";
            //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["NINFORME"] = "\"" + "DFI170R3" + "\"";
            //objeto_crystal.DataFiles(0) = camino2 & "\Exitus.mdb"
            //UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.DataFiles[0] = MExitus.camino2 + "\\" + MExitus.stbasedatos;
            //UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Connect = "DSN=" + MExitus.NOMBRE_DSN_ACCESS + "";
            //UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
            IMPRIMIR_FILIACION();
					//UPGRADE_ISSUE: (2036) Form property DFI170F1.MousePointer does not support custom mousepointers. More Information: http://www.vbtonet.com/ewis/ewi2036.aspx
					//this.Cursor = vbCustom;
			}

		public void REFERENCIAS(MCExitus Class, object OBJETO)
		{
			MExitus.CLASE = Class;
			MExitus.OBJETO2 = OBJETO;
		}

		private void IMPRIMIR_ADMISION()
			{
            //camaya todo _x_4
            ////UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.SQLQuery = "Select * from EXITUS";
            ////UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.WindowTitle = "Pacientes fallecidos en el hospital (Admisi�n)";
            ////UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Action = 1;
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.PageCount();
            ////UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.PrinterStopPage = MExitus.objeto_crystal.PageCount;

            //for (i = 0; i <= 11; i++)
            //{
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	MExitus.objeto_crystal.Formulas[i] = "";
            //}
            ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Reset();
        }

        private void IMPRIMIR_URGENCIAS()
			{
            //camaya todo _x_4
            ////UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.SQLQuery = "select * from EXITUS";
            ////UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.WindowTitle = "Pacientes fallecidos en el hospital (Urgencias)";
            //espera = false;
            //Timer1.Interval = 5000;
            //Timer1.Enabled = true;
            //Timer1.Enabled = true;
            //while (!espera)
            //{
            //	Application.DoEvents();
            //}
            //Timer1.Enabled = false;
            ////UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Action = 1;
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.PageCount();
            ////UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.PrinterStopPage = MExitus.objeto_crystal.PageCount;

            //for (i = 0; i <= 11; i++)
            //{
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	MExitus.objeto_crystal.Formulas[i] = "";
            //}
            ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Reset();
        }

        private void IMPRIMIR_FILIACION()
			{
            //camaya todo _x_4
            ////UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.SQLQuery = "SELECT * FROM EXITUS";
            ////UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.WindowTitle = "Pacientes fallecidos en filiaci�n";
            //espera = false;
            //Timer1.Interval = 5000;
            //Timer1.Enabled = true;
            //Timer1.Enabled = true;
            //while (!espera)
            //{
            //	Application.DoEvents();
            //}
            //Timer1.Enabled = false;
            ////UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Action = 1;
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.PageCount();
            ////UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.PrinterStopPage = MExitus.objeto_crystal.PageCount;

            //for (i = 0; i <= 10; i++)
            //{
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	MExitus.objeto_crystal.Formulas[i] = "";
            //}
            ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Reset();
        }

        private void sdcHFecha_Leave(Object eventSender, EventArgs eventArgs)
		{
		   Activar_Aceptar();
		}

	    private void Timer1_Tick(Object eventSender, EventArgs eventArgs)
		{
			espera = true;
		}

		//Oscar C Marzo 2010
		private void OptionEntidad_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
            if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                FrameEntidad.Enabled = true;
                FrameServicio.Enabled = false;
                cbbServicio.SelectedIndex = -1;
                Activar_Aceptar();
            }
        }

		private void OptionServicio_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
            if (((Telerik.WinControls.UI.RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                FrameEntidad.Enabled = false;
                FrameServicio.Enabled = true;
                cbbSociedad.SelectedIndex = -1;
                CbbInspeccion.SelectedIndex = -1;
                cbbServicio.SelectedIndex = 0;
                Activar_Aceptar();
            }
        }

		private void ConsultaPorHospital_Servicio()
			{
					this.Cursor = Cursors.WaitCursor;
					DataSet RrDiag = null; //'
					string sqltemporal = String.Empty;
					string HospitalOIngreso = String.Empty;


					vstPathTemp = MExitus.camino2 + "\\" + MExitus.stbasedatos;
					if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
					{
						vbBase = true;
					}
					if (!vbBase)
					{ // SI est� dado
						//UPGRADE_ISSUE: (2064) DAO.Workspace method DBEngine.Workspaces.CreateDatabase was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						//bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].CreateDatabase(vstPathTemp, DAO.LanguageConstants.dbLangSpanish, null);
						vbBase = true;
					}
					else
					{
						//UPGRADE_WARNING: (2065) DAO.Workspace method DBEngine.Workspaces.OpenDatabase has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
						bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].OpenDatabase("<Connection-String>");
						foreach (TableDefHelper TABLA in bs.TableDefs)
						{
							if (TABLA.Name.ToUpper() == "EXITUS")
							{
								DbCommand TempCommand = bs.Connection.CreateCommand();
								TempCommand.CommandText = "DROP TABLE EXITUS";
								TempCommand.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
								TempCommand.ExecuteNonQuery();
							}
						}
					}

					string SQLU = " SELECT GMOTALTA FROM DMOTALTA WHERE ICEXITUS = 'S'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLU, MExitus.GConexion);
					DataSet RrSQLU = new DataSet();
					tempAdapter.Fill(RrSQLU);
					//CODIGO DE EXITUS
					
					int EXITUS = Convert.ToInt32(RrSQLU.Tables[0].Rows[0]["GMOTALTA"]);
					
					RrSQLU.Close();

					string LitPersona = Serrores.ObtenerLiteralPersona(MExitus.GConexion);
					LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();


					string HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(MExitus.GConexion);
					if (HospitalOCentro == "S")
					{
						HospitalOIngreso = "HOSPITAL";
					}
					else
					{
						HospitalOIngreso = "CENTRO";
					}
					string StrTitulo = "LISTADO DE " + LitPersona.ToUpper() + "S" + " FALLECIDOS EN EL " + HospitalOIngreso;

					DbCommand TempCommand_2 = bs.Connection.CreateCommand();
					TempCommand_2.CommandText = "CREATE TABLE EXITUS (campo1 text, " + 
					                            " campo2 text, campo3 text ,campo4 text, campo5 text, " + 
					                            " campo6 text, campo7 text, campo8 text, campo9 text, campo10 text, campo11 text, " + 
					                            " campo12 text, campo13 text)";
					TempCommand_2.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
					TempCommand_2.ExecuteNonQuery();





					//minimas condiciones para que sea fallecido en el hospital
					SQLT = "SELECT DISTINCT DPACIENT.GIDENPAC, DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " + 
					       " DPACIENT.itipsexo, DPACIENT.ddirepac, DPACIENT.gcodipos, DPACIENT.nafiliac, " + 
					       " DPACIENT.iexitusp, HDOSSIER.ghistoria, DPOBLACI.dpoblaci, " + 
					       " DSOCIEDA.DSOCIEDA, AEPISADM.faltplan, AEPISADM.gdiagalt, AEPISADM.odiagalt, " + 
					       " AEPISADM.gserulti, AEPISADM.gperulti, " + 
					       " DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers, " + 
					       " DSERVICI.dnomserv " + 
					       " From AEPISADM " + 
					       " INNER JOIN AMOVIFIN ON AEPISADM.ganoadme = AMOVIFIN.ganoregi AND AEPISADM.gnumadme = AMOVIFIN.gnumregi AND AEPISADM.faltplan = AMOVIFIN.ffinmovi " + 
					       " INNER JOIN DPACIENT ON AEPISADM.gidenpac = DPACIENT.gidenpac " + 
					       " INNER JOIN DPERSONA ON AEPISADM.gperulti = DPERSONA.gpersona " + 
					       " INNER JOIN DSERVICI ON AEPISADM.gserulti = DSERVICI.gservici " + 
					       " LEFT OUTER JOIN DPOBLACI ON DPACIENT.gpoblaci = DPOBLACI.gpoblaci " + 
					       " LEFT OUTER JOIN DSOCIEDA ON AMOVIFIN.gsocieda = DSOCIEDA.gsocieda " + 
					       " LEFT OUTER JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac and  HDOSSIER.icontenido='H' " + 
					       " Where aepisadm.gmotalta = " + EXITUS.ToString() + " and aepisadm.faltplan is not null";
					//SI SE HA ELEGIDO SERVICIO
					if (cbbServicio.SelectedIndex > 0)
					{
						SQLT = SQLT + " and AEPISADM.gserulti = " + Convert.ToInt32(cbbServicio.SelectedValue).ToString() + " ";
					}
					string tempRefParam = sdcDFecha.Text;
					object tempRefParam2 = sdcHFecha.Text + " 23:59:59";
					SQLT = SQLT + " and (AEPISADM.faltplan >= " + Serrores.FormatFecha(tempRefParam) + " and " + 
					       "      AEPISADM.faltplan <= " + Serrores.FormatFechaHMS(tempRefParam2) + ")";

					SQLT = SQLT + " UNION ALL ";

					//PARA URGENCIAS
					SQLT = SQLT + 
					       " SELECT DISTINCT DPACIENT.GIDENPAC, DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " + 
					       " DPACIENT.itipsexo, DPACIENT.ddirepac, DPACIENT.gcodipos, DPACIENT.nafiliac, " + 
					       " DPACIENT.iexitusp, HDOSSIER.ghistoria, DPOBLACI.dpoblaci, " + 
					       " DSOCIEDA.DSOCIEDA, UEPISURG.faltaurg faltplan, UEPISURG.gdiagnos gdiagalt, UEPISURG.odiagnos odiagalt, " + 
					       " UEPISURG.gseralta gserulti, UEPISURG.gpersona gperulti, " + 
					       " DPERSONA.dap1pers, DPERSONA.dap2pers, DPERSONA.dnompers, " + 
					       " DSERVICI.dnomserv " + 
					       " From UEPISURG " + 
					       " INNER JOIN DPACIENT ON UEPISURG.gidenpac = DPACIENT.gidenpac " + 
					       " INNER JOIN DPERSONA ON UEPISURG.gpersona = DPERSONA.gpersona " + 
					       " INNER JOIN DSERVICI ON UEPISURG.gseralta = DSERVICI.gservici " + 
					       " LEFT OUTER JOIN DPOBLACI ON DPACIENT.gpoblaci = DPOBLACI.gpoblaci " + 
					       " LEFT OUTER JOIN DSOCIEDA ON UEPISURG.gsocieda = DSOCIEDA.gsocieda " + 
					       " LEFT OUTER JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac and HDOSSIER.icontenido='H' " + 
					       " Where UEPISURG.gmotalta = " + EXITUS.ToString() + " and UEPISURG.faltaurg is not null";
					//SI SE HA ELEGIDO SERVICIO
					if (cbbServicio.SelectedIndex > 0)
					{
						SQLT = SQLT + " and UEPISURG.gseralta = " + Convert.ToInt32(cbbServicio.SelectedValue).ToString() + " ";
					}
					string tempRefParam3 = sdcDFecha.Text;
					object tempRefParam4 = sdcHFecha.Text + " 23:59:59";
					SQLT = SQLT + " and (UEPISURG.faltaurg >= " + Serrores.FormatFecha(tempRefParam3) + " and " + 
					       "      UEPISURG.faltaurg <= " + Serrores.FormatFechaHMS(tempRefParam4) + ")";

					SQLT = SQLT + " ORDER BY DSERVICI.DNOMSERV, DPERSONA.DAP1PERS, DPERSONA.DAP2PERS, DPERSONA.DNOMPERS ";
					if (rbOListado[0].IsChecked)
					{
						SQLT = SQLT + " , DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac";
					}
					if (rbOListado[1].IsChecked)
					{
						SQLT = SQLT + " , faltplan, DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac";
					}

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLT, MExitus.GConexion);
					MExitus.RrSQL = new DataSet();
					tempAdapter_2.Fill(MExitus.RrSQL);
					if (MExitus.RrSQL.Tables[0].Rows.Count != 0)
					{
						sqltemporal = "select * from EXITUS where 1=2";
						Rs = bs.OpenRecordset(sqltemporal);
						
						foreach (DataRow iteration_row in MExitus.RrSQL.Tables[0].Rows)
						{
							Rs.AddNew();
							Rs["campo1"] = iteration_row["ghistoria"];
							
							if (!Convert.IsDBNull(iteration_row["dnombpac"]))
							{
								Rs["campo2"] = Convert.ToString(iteration_row["dnombpac"]).Trim();
							}
							
							if (!Convert.IsDBNull(iteration_row["dape1pac"]))
							{
								Rs["campo2"] = Convert.ToString(Rs["campo2"]).Trim() + " " + Convert.ToString(iteration_row["dape1pac"]).Trim();
							}
							
							if (!Convert.IsDBNull(iteration_row["dape2pac"]))
							{
								Rs["campo2"] = Convert.ToString(Rs["campo2"]).Trim() + " " + Convert.ToString(iteration_row["dape2pac"]).Trim();
							}
							if (Convert.ToString(iteration_row["itipsexo"]) == "H")
							{
								Rs["campo3"] = "Hombre";
							}
							if (Convert.ToString(iteration_row["itipsexo"]) == "M")
							{
								Rs["campo3"] = "Mujer";
							}
							if (Convert.ToString(iteration_row["itipsexo"]) == "I")
							{
								Rs["campo3"] = "Indeterminado";
							}
							Rs["campo4"] = Convert.ToDateTime(iteration_row["faltplan"]).ToString("dd/MM/yyyy");
							Rs["campo5"] = iteration_row["ddirepac"];
							Rs["campo6"] = iteration_row["gcodipos"];
							Rs["campo7"] = iteration_row["dpoblaci"];
							Rs["campo8"] = iteration_row["DSOCIEDA"];
							if (Convert.ToString(iteration_row["iexitusp"]) == "N")
							{
								Rs["campo9"] = "No";
							}
							if (Convert.ToString(iteration_row["iexitusp"]) == "S")
							{
								Rs["campo9"] = "Si";
							}
							
							if (!Convert.IsDBNull(iteration_row["gdiagalt"]))
							{ //'
								SQLT = "SELECT dnombdia FROM DDIAGNOS " + 
								       "WHERE gcodidia = '" + Convert.ToString(iteration_row["gdiagalt"]) + "'";
								SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLT, MExitus.GConexion);
								RrDiag = new DataSet();
								tempAdapter_3.Fill(RrDiag);
								Rs["campo10"] = RrDiag.Tables[0].Rows[0]["dnombdia"];
								
								RrDiag.Close();
							}
							else
							{
								Rs["campo10"] = iteration_row["odiagalt"];
							}
							
							Rs["campo11"] = (Convert.IsDBNull(iteration_row["nafiliac"])) ? "" : Convert.ToString(iteration_row["nafiliac"]).Trim();
							
							Rs["campo12"] = (Convert.IsDBNull(iteration_row["dnomserv"])) ? "" : Convert.ToString(iteration_row["dnomserv"]).Trim();
							
							if (!Convert.IsDBNull(iteration_row["DAP1PERS"]))
							{
								Rs["campo13"] = Convert.ToString(iteration_row["DAP1PERS"]).Trim();
							}
							
							if (!Convert.IsDBNull(iteration_row["DAP2PERS"]))
							{
								Rs["campo13"] = Convert.ToString(Rs["campo13"]).Trim() + " " + Convert.ToString(iteration_row["DAP2PERS"]).Trim();
							}
							
							if (!Convert.IsDBNull(iteration_row["DNOMPERS"]))
							{
								Rs["campo13"] = Convert.ToString(Rs["campo13"]).Trim() + ", " + Convert.ToString(iteration_row["DNOMPERS"]).Trim();
							}
							Rs.Update();
						}
						Rs.Close();
					}
					else
					{
						RadMessageBox.Show("No hay " + LitPersona + "s" + " fallecidos en el " + HospitalOIngreso + " para las condiciones especificadas.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Info);				        	
                        //camaya todo_x_4
						//this.Cursor = vbCustom;
						return;
					}

					
					MExitus.RrSQL.Close();
					UpgradeHelpers.DB.TransactionManager.DeEnlist(bs.Connection);
					bs.Close();
					DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].Close();

            //camaya todo _x_4
            ////UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.ReportFileName = MExitus.camino2 + "\\..\\ElementosComunes\\RPT\\dfi170r3.rpt";
            ////UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.DataFiles[0] = MExitus.camino2 + "\\" + MExitus.stbasedatos;

            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["Grupo"] = "\"" + stGrupoHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["DireccionH"] = "\"" + stDireccHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["NombreH"] = "\"" + stNombreHo + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["FechaDesde"] = "\"" + DateTime.Parse(sdcDFecha.Value.Date).ToString("dd/MM/yyyy") + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["FechaHasta"] = "\"" + DateTime.Parse(sdcHFecha.Value.Date).ToString("dd/MM/yyyy") + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["SERVICIO"] = "\"" + cbbServicio.Text + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["LitPers"] = "\"" + LitPersona + "\"";
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["TITULO"] = "\"" + StrTitulo + "\"";
            //if (rbOListado[0].Checked)
            //{
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	MExitus.objeto_crystal.Formulas["ORDEN"] = "\"" + "Nombre" + "\"";
            //}
            //else
            //{
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	MExitus.objeto_crystal.Formulas["ORDEN"] = "\"" + "Fecha de exitus" + "\"";
            //}
            ////objeto_crystal.Formulas(9) = "ORIGEN = """ & "(HOSPITALIZACI�N)" & """"
            ////UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Formulas["NINFORME"] = "\"" + "DFI170R4" + "\"";
            //if (Serrores.ExisteAseguradora(MExitus.GConexion))
            //{
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	MExitus.objeto_crystal.Formulas["IASEGURA"] = "'S'";
            //}
            //else
            //{
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	MExitus.objeto_crystal.Formulas["IASEGURA"] = "'N'";
            //}
            ////objeto_crystal.DataFiles(0) = camino2 & "\" & stbasedatos
            ////objeto_crystal.Connect = "DSN=" & NOMBRE_DSN_ACCESS & ""
            ////UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
            ////UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.SQLQuery = "Select * from EXITUS";
            ////UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.WindowTitle = StrTitulo;
            ////UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Action = 1;
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.PageCount();
            ////UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            ////UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.PrinterStopPage = MExitus.objeto_crystal.PageCount;

            //for (i = 0; i <= 11; i++)
            //{
            //	//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //	MExitus.objeto_crystal.Formulas[i] = "";
            //}
            ////UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            //MExitus.objeto_crystal.Reset();

            ////UPGRADE_ISSUE: (2036) Form property DFI170F1.MousePointer does not support custom mousepointers. More Information: http://www.vbtonet.com/ewis/ewi2036.aspx
            //this.Cursor = vbCustom;
        }
        //---------------------
        private void DFI170F1_Closed(Object eventSender, EventArgs eventArgs)
			{
					MemoryHelper.ReleaseMemory();
			}
	}
}