using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace DLLExitus
{
	internal static class MExitus
	{

		public static SqlConnection GConexion = null;
		public static DFI170F1 NuevoDFI170F1 = null;
		public static DLLExitus.MCExitus CLASE = null;
		public static object OBJETO2 = null;
		public static int CodSS = 0; //codigo de la seguridad social
		public static string TexSS = String.Empty; //texto de la seguridad social
		public static int CodPriv = 0; //codigo de privado
		public static string TexPriv = String.Empty; //texto de privado
		public static DataSet RrSQL = null;
		public static string camino2 = String.Empty;
		public static Mensajes.ClassMensajes clasemensaje = null;
		public static string NombreApli = String.Empty;
		public static string vstFechaHoraSis = String.Empty;
		public static object objeto_crystal = null;
		public static string NOMBRE_DSN_ACCESS = String.Empty;
		public static string stbasedatos = String.Empty;


		internal static void FechaHoraSistema()
		{
			try
			{
				//    sql = "select getdate() go"
				//    Set RsFechaSis = GConexion.OpenResultset(sql, rdOpenKeyset, 3, rdExecDirect)
				//    stFechaHoraSis = RsFechaSis(0)
				//    RsFechaSis.Close
				//    vstFechaHoraSis = stFechaHoraSis
				vstFechaHoraSis = DateTimeHelper.ToString(DateTime.Now);
			}
			catch (SqlException ex)
			{				
				Serrores.GestionErrorBaseDatos(ex, GConexion); //Falta ver que se hace con el resume
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }
	}
}