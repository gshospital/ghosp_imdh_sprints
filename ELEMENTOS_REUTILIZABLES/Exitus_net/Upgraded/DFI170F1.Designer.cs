using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLExitus
{
	partial class DFI170F1
	{

		#region "Upgrade Support "
		private static DFI170F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI170F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI170F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "OptionEntidad", "OptionServicio", "cbbServicio", "Label6", "FrameServicio", "_rbTListado_0", "_rbTListado_1", "Frame1", "cbbSociedad", "Label2", "CbbInspeccion", "Label1", "FrameEntidad", "Timer1", "cbCancelar", "cbAceptar", "_rbOListado_1", "_rbOListado_0", "Frame3", "sdcDFecha", "sdcHFecha", "Label4", "Label3", "Frame2", "rbOListado", "rbTListado"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadRadioButton OptionEntidad;
		public Telerik.WinControls.UI.RadRadioButton OptionServicio;
		public Telerik.WinControls.UI.RadDropDownList cbbServicio;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadGroupBox FrameServicio;
		private Telerik.WinControls.UI.RadRadioButton _rbTListado_0;
		private Telerik.WinControls.UI.RadRadioButton _rbTListado_1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadDropDownList cbbSociedad;
		public Telerik.WinControls.UI.RadLabel Label2;
        //public AxMSForms.AxComboBox CbbInspeccion;dropdownlist
        public Telerik.WinControls.UI.RadDropDownList CbbInspeccion;
        public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox FrameEntidad;
		public System.Windows.Forms.Timer Timer1;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		private Telerik.WinControls.UI.RadRadioButton _rbOListado_1;
		private Telerik.WinControls.UI.RadRadioButton _rbOListado_0;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadDateTimePicker sdcDFecha;
		public Telerik.WinControls.UI.RadDateTimePicker sdcHFecha;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadRadioButton[] rbOListado = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbTListado = new Telerik.WinControls.UI.RadRadioButton[2];
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.OptionEntidad = new Telerik.WinControls.UI.RadRadioButton();
            this.OptionServicio = new Telerik.WinControls.UI.RadRadioButton();
            this.FrameServicio = new Telerik.WinControls.UI.RadGroupBox();
            this.cbbServicio = new Telerik.WinControls.UI.RadDropDownList();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.FrameEntidad = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbTListado_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbTListado_1 = new Telerik.WinControls.UI.RadRadioButton();
            this.cbbSociedad = new Telerik.WinControls.UI.RadDropDownList();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.CbbInspeccion = new Telerik.WinControls.UI.RadDropDownList();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbOListado_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbOListado_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcDFecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.sdcHFecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.OptionEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OptionServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrameServicio)).BeginInit();
            this.FrameServicio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrameEntidad)).BeginInit();
            this.FrameEntidad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbTListado_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbTListado_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSociedad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbInspeccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbOListado_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbOListado_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcHFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // OptionEntidad
            // 
            this.OptionEntidad.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OptionEntidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.OptionEntidad.Location = new System.Drawing.Point(4, 2);
            this.OptionEntidad.Name = "OptionEntidad";
            this.OptionEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OptionEntidad.Size = new System.Drawing.Size(78, 18);
            this.OptionEntidad.TabIndex = 1;
            this.OptionEntidad.Text = "Por Entidad";
            this.OptionEntidad.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.OptionEntidad.CheckStateChanged += new System.EventHandler(this.OptionEntidad_CheckedChanged);
            // 
            // OptionServicio
            // 
            this.OptionServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.OptionServicio.Location = new System.Drawing.Point(4, 128);
            this.OptionServicio.Name = "OptionServicio";
            this.OptionServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OptionServicio.Size = new System.Drawing.Size(79, 18);
            this.OptionServicio.TabIndex = 6;
            this.OptionServicio.Text = "Por Servicio";
            this.OptionServicio.CheckStateChanged += new System.EventHandler(this.OptionServicio_CheckedChanged);
            // 
            // FrameServicio
            // 
            this.FrameServicio.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrameServicio.Controls.Add(this.cbbServicio);
            this.FrameServicio.Controls.Add(this.Label6);
            this.FrameServicio.HeaderText = "";
            this.FrameServicio.Location = new System.Drawing.Point(4, 152);
            this.FrameServicio.Name = "FrameServicio";
            this.FrameServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrameServicio.Size = new System.Drawing.Size(665, 44);
            this.FrameServicio.TabIndex = 21;
            // 
            // cbbServicio
            // 
            this.cbbServicio.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbServicio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbServicio.Location = new System.Drawing.Point(68, 11);
            this.cbbServicio.Name = "cbbServicio";
            this.cbbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbServicio.Size = new System.Drawing.Size(333, 20);
            this.cbbServicio.TabIndex = 7;
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Location = new System.Drawing.Point(8, 15);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(47, 18);
            this.Label6.TabIndex = 22;
            this.Label6.Text = "Servicio:";
            // 
            // FrameEntidad
            // 
            this.FrameEntidad.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrameEntidad.Controls.Add(this.Frame1);
            this.FrameEntidad.Controls.Add(this.cbbSociedad);
            this.FrameEntidad.Controls.Add(this.Label2);
            this.FrameEntidad.Controls.Add(this.CbbInspeccion);
            this.FrameEntidad.Controls.Add(this.Label1);
            this.FrameEntidad.HeaderText = "";
            this.FrameEntidad.Location = new System.Drawing.Point(4, 24);
            this.FrameEntidad.Name = "FrameEntidad";
            this.FrameEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrameEntidad.Size = new System.Drawing.Size(665, 98);
            this.FrameEntidad.TabIndex = 17;
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this._rbTListado_0);
            this.Frame1.Controls.Add(this._rbTListado_1);
            this.Frame1.HeaderText = "Tipo de listado";
            this.Frame1.Location = new System.Drawing.Point(416, 3);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(241, 81);
            this.Frame1.TabIndex = 20;
            this.Frame1.Text = "Tipo de listado";
            // 
            // _rbTListado_0
            // 
            this._rbTListado_0.CheckState = System.Windows.Forms.CheckState.Checked;
            this._rbTListado_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbTListado_0.Location = new System.Drawing.Point(32, 20);
            this._rbTListado_0.Name = "_rbTListado_0";
            this._rbTListado_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbTListado_0.Size = new System.Drawing.Size(179, 18);
            this._rbTListado_0.TabIndex = 4;
            this._rbTListado_0.Text = "Pacientes fallecidos en el centro";
            this._rbTListado_0.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // _rbTListado_1
            // 
            this._rbTListado_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbTListado_1.Location = new System.Drawing.Point(30, 48);
            this._rbTListado_1.Name = "_rbTListado_1";
            this._rbTListado_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbTListado_1.Size = new System.Drawing.Size(178, 18);
            this._rbTListado_1.TabIndex = 5;
            this._rbTListado_1.Text = "Pacientes con �xitus en filiaci�n";
            // 
            // cbbSociedad
            // 
            this.cbbSociedad.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbSociedad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbSociedad.Location = new System.Drawing.Point(68, 23);
            this.cbbSociedad.Name = "cbbSociedad";
            this.cbbSociedad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbSociedad.Size = new System.Drawing.Size(333, 20);
            this.cbbSociedad.TabIndex = 2;
            this.cbbSociedad.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbSociedad_SelectedIndexChanged);
            this.cbbSociedad.TextChanged += new System.EventHandler(this.cbbSociedad_TextChanged);
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(4, 59);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(62, 18);
            this.Label2.TabIndex = 19;
            this.Label2.Text = "Inspecci�n:";
            // 
            // CbbInspeccion
            // 
            this.CbbInspeccion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CbbInspeccion.Location = new System.Drawing.Point(68, 55);
            this.CbbInspeccion.Name = "CbbInspeccion";
            this.CbbInspeccion.Size = new System.Drawing.Size(333, 20);
            this.CbbInspeccion.TabIndex = 3;
            this.CbbInspeccion.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbInspeccion_SelectedIndexChanged);
            this.CbbInspeccion.Click += new System.EventHandler(this.CbbInspeccion_ClickEvent);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(4, 27);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(54, 18);
            this.Label1.TabIndex = 18;
            this.Label1.Text = "Sociedad:";
            // 
            // Timer1
            // 
            this.Timer1.Interval = 1;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(588, 278);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 13;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Enabled = false;
            this.cbAceptar.Location = new System.Drawing.Point(500, 278);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 12;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this._rbOListado_1);
            this.Frame3.Controls.Add(this._rbOListado_0);
            this.Frame3.HeaderText = "Orden listado";
            this.Frame3.Location = new System.Drawing.Point(4, 199);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(243, 73);
            this.Frame3.TabIndex = 16;
            this.Frame3.Text = "Orden listado";
            // 
            // _rbOListado_1
            // 
            this._rbOListado_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbOListado_1.Location = new System.Drawing.Point(48, 44);
            this._rbOListado_1.Name = "_rbOListado_1";
            this._rbOListado_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbOListado_1.Size = new System.Drawing.Size(97, 18);
            this._rbOListado_1.TabIndex = 9;
            this._rbOListado_1.Text = "Fecha de exitus";
            // 
            // _rbOListado_0
            // 
            this._rbOListado_0.CheckState = System.Windows.Forms.CheckState.Checked;
            this._rbOListado_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbOListado_0.Location = new System.Drawing.Point(48, 20);
            this._rbOListado_0.Name = "_rbOListado_0";
            this._rbOListado_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbOListado_0.Size = new System.Drawing.Size(62, 18);
            this._rbOListado_0.TabIndex = 8;
            this._rbOListado_0.Text = "Nombre";
            this._rbOListado_0.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.sdcDFecha);
            this.Frame2.Controls.Add(this.sdcHFecha);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.HeaderText = "Fechas";
            this.Frame2.Location = new System.Drawing.Point(252, 199);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(417, 73);
            this.Frame2.TabIndex = 0;
            this.Frame2.Text = "Fechas";
            // 
            // sdcDFecha
            // 
            this.sdcDFecha.CustomFormat = "dd/MM/yyyy";
            this.sdcDFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcDFecha.Location = new System.Drawing.Point(84, 20);
            this.sdcDFecha.Name = "sdcDFecha";
            this.sdcDFecha.Size = new System.Drawing.Size(121, 20);
            this.sdcDFecha.TabIndex = 10;
            this.sdcDFecha.TabStop = false;
            this.sdcDFecha.Text = "10/05/2016";
            this.sdcDFecha.Value = new System.DateTime(2016, 5, 10, 0, 0, 0, 0);
            this.sdcDFecha.Enter += new System.EventHandler(this.sdcDFecha_Enter);
            this.sdcDFecha.Leave += new System.EventHandler(this.sdcDFecha_Leave);
            // 
            // sdcHFecha
            // 
            this.sdcHFecha.CustomFormat = "dd/MM/yyyy";
            this.sdcHFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcHFecha.Location = new System.Drawing.Point(84, 44);
            this.sdcHFecha.Name = "sdcHFecha";
            this.sdcHFecha.Size = new System.Drawing.Size(121, 20);
            this.sdcHFecha.TabIndex = 11;
            this.sdcHFecha.TabStop = false;
            this.sdcHFecha.Text = "10/05/2016";
            this.sdcHFecha.Value = new System.DateTime(2016, 5, 10, 0, 0, 0, 0);
            this.sdcHFecha.Enter += new System.EventHandler(this.sdcHFecha_Enter);
            this.sdcHFecha.Leave += new System.EventHandler(this.sdcHFecha_Leave);
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(44, 44);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(37, 18);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "Hasta:";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(44, 20);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(40, 18);
            this.Label3.TabIndex = 14;
            this.Label3.Text = "Desde:";
            // 
            // DFI170F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 313);
            this.Controls.Add(this.OptionEntidad);
            this.Controls.Add(this.OptionServicio);
            this.Controls.Add(this.FrameServicio);
            this.Controls.Add(this.FrameEntidad);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.Frame2);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(127, 143);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI170F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Informes de �xitus  - DFI170F1";
            this.Activated += new System.EventHandler(this.DFI170F1_Activated);
            this.Closed += new System.EventHandler(this.DFI170F1_Closed);
            this.Load += new System.EventHandler(this.DFI170F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OptionEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OptionServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrameServicio)).EndInit();
            this.FrameServicio.ResumeLayout(false);
            this.FrameServicio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbServicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrameEntidad)).EndInit();
            this.FrameEntidad.ResumeLayout(false);
            this.FrameEntidad.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbTListado_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbTListado_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbSociedad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbInspeccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbOListado_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbOListado_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDFecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcHFecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		void ReLoadForm(bool addEvents)
		{
			InitializerbTListado();
			InitializerbOListado();
		}
		void InitializerbTListado()
		{
			this.rbTListado = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbTListado[0] = _rbTListado_0;
			this.rbTListado[1] = _rbTListado_1;
		}
		void InitializerbOListado()
		{
			this.rbOListado = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbOListado[1] = _rbOListado_1;
			this.rbOListado[0] = _rbOListado_0;
		}
		#endregion
	}
}