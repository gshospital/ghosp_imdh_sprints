using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace DLLExitus
{
	public class MCExitus
	{

		public void load(MCExitus VAR1, object VAR2,SqlConnection Conexion, string camino, object crystal, string dsn_temporal, string BaseDatos)
		{
			MExitus.GConexion = Conexion;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			Serrores.AjustarRelojCliente(Conexion);
			MExitus.camino2 = camino;
			MExitus.objeto_crystal = crystal;
			MExitus.NOMBRE_DSN_ACCESS = dsn_temporal;
			MExitus.stbasedatos = BaseDatos;
			//Crea un formulario
			MExitus.NuevoDFI170F1 = new DFI170F1();
			MExitus.NuevoDFI170F1.REFERENCIAS(VAR1, VAR2);			
			MExitus.NuevoDFI170F1.ShowDialog();
		}

		public MCExitus()
		{
			MExitus.clasemensaje = new Mensajes.ClassMensajes();
		}
	}
}