using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Poblaciones
{
	public class MCPoblaciones
	{

        public void llamada(object LLAMADOR, SqlConnection Conexion, ref UpgradeHelpers.MSForms.MSCombobox ComboAlfa)
		{
			MPoblaciones.GConexion = Conexion;
			MPoblaciones.oLLAMADOR = LLAMADOR;
			MPoblaciones.Comborecibir(ComboAlfa);
			DFI111F2.DefInstance.ShowDialog();
		}

		public MCPoblaciones()
		{
			MPoblaciones.clasemensaje = new Mensajes.ClassMensajes();
		}
	}
}