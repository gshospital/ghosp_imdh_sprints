using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Poblaciones
{
	partial class DFI111F2
	{

		#region "Upgrade Support "
		private static DFI111F2 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI111F2 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI111F2();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbCancelar", "cbAceptar", "cbbCodProv", "cbbProv", "cbbCorregimiento", "cbbCodCorregimiento", "Label5", "Label3", "Label2", "Label1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
        public Telerik.WinControls.RadToolTip ToolTipMain;
        public Telerik.WinControls.UI.RadButton cbCancelar;
        public Telerik.WinControls.UI.RadButton cbAceptar;
        public UpgradeHelpers.MSForms.MSCombobox cbbCodProv;
        public UpgradeHelpers.MSForms.MSCombobox cbbProv;
        public UpgradeHelpers.MSForms.MSCombobox cbbCorregimiento;
        public UpgradeHelpers.MSForms.MSCombobox cbbCodCorregimiento;
        public Telerik.WinControls.UI.RadLabel Label5;
        public Telerik.WinControls.UI.RadLabel Label3;
        public Telerik.WinControls.UI.RadLabel Label2;
        public Telerik.WinControls.UI.RadLabel Label1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbbCodProv = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbProv = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbCorregimiento = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbCodCorregimiento = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCodProv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbProv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCorregimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCodCorregimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(481, 61);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 1;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(394, 61);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 0;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cbbCodProv
            // 
            this.cbbCodProv.ColumnWidths = "";
            this.cbbCodProv.Location = new System.Drawing.Point(115, 6);
            this.cbbCodProv.Name = "cbbCodProv";
            this.cbbCodProv.Size = new System.Drawing.Size(94, 20);
            this.cbbCodProv.TabIndex = 6;
            this.cbbCodProv.Enter += new System.EventHandler(this.cbbCodProv_Enter);
            this.cbbCodProv.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbCodProv_Change);
            this.cbbCodProv.Click += new System.EventHandler(this.cbbCodProv_ClickEvent);
            this.cbbCodProv.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            // 
            // cbbProv
            // 
            this.cbbProv.ColumnWidths = "";
            this.cbbProv.Location = new System.Drawing.Point(309, 4);
            this.cbbProv.Name = "cbbProv";
            this.cbbProv.Size = new System.Drawing.Size(252, 20);
            this.cbbProv.TabIndex = 7;
            this.cbbProv.Enter += new System.EventHandler(this.cbbProv_Enter);
            this.cbbProv.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbProv_Change);
            this.cbbProv.Click += new System.EventHandler(this.cbbProv_ClickEvent);
            this.cbbProv.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            // 
            // cbbCorregimiento
            // 
            this.cbbCorregimiento.ColumnWidths = "";
            this.cbbCorregimiento.Location = new System.Drawing.Point(309, 32);
            this.cbbCorregimiento.Name = "cbbCorregimiento";
            this.cbbCorregimiento.Size = new System.Drawing.Size(252, 20);
            this.cbbCorregimiento.TabIndex = 9;
            this.cbbCorregimiento.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbCorregimiento_Change);
            this.cbbCorregimiento.Click += new System.EventHandler(this.cbbCorregimiento_ClickEvent);
            this.cbbCorregimiento.Enter += new System.EventHandler(this.cbbCorregimiento_Enter);
            this.cbbCorregimiento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            // 
            // cbbCodCorregimiento
            // 
            this.cbbCodCorregimiento.ColumnWidths = "";
            this.cbbCodCorregimiento.Location = new System.Drawing.Point(115, 28);
            this.cbbCodCorregimiento.Name = "cbbCodCorregimiento";
            this.cbbCodCorregimiento.Size = new System.Drawing.Size(94, 20);
            this.cbbCodCorregimiento.TabIndex = 8;
            this.cbbCodCorregimiento.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbCodCorregimiento_Change);
            this.cbbCodCorregimiento.Click += new System.EventHandler(this.cbbCodCorregimiento_ClickEvent);
            this.cbbCodCorregimiento.Enter += new System.EventHandler(this.cbbCodCorregimiento_Enter);
            this.cbbCodCorregimiento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            // 
            // Label5
            // 
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Location = new System.Drawing.Point(215, 33);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(81, 18);
            this.Label5.TabIndex = 5;
            this.Label5.Text = "Corregimiento:";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(2, 34);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(107, 18);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "C�d. Corregimiento:";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(215, 7);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(54, 18);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "Provincia:";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(0, 8);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(80, 18);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "C�d. Provincia:";
            // 
            // DFI111F2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 102);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cbbCodProv);
            this.Controls.Add(this.cbbProv);
            this.Controls.Add(this.cbbCorregimiento);
            this.Controls.Add(this.cbbCodCorregimiento);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(39, 175);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI111F2";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "B�squeda de poblaci�n por provincia - DFI111F2";
            this.Closed += new System.EventHandler(this.DFI111F2_Closed);
            this.Load += new System.EventHandler(this.DFI111F2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCodProv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbProv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCorregimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCodCorregimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}