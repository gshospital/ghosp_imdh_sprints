using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Poblaciones
{
	public partial class DFI111F2
		: Telerik.WinControls.UI.RadForm
	{

		string NombreApli = String.Empty;
		public DFI111F2()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (validar_datos())
			{
				object tempRefParam = 1;
				object tempRefParam2 = cbbProv.get_ListIndex();
				CargarComboAlfanumerico("SELECT gpoblaci, Dpoblaci FROM Dpoblaci WHERE gpoblaci LIKE '" + Convert.ToString(cbbProv.get_Column(tempRefParam, tempRefParam2)).Trim().ToUpper() + "%'", "Gpoblaci", "Dpoblaci", MPoblaciones.cbbComboRecibir);
				for (int I = 0; I <= cbbCodCorregimiento.ListCount - 1; I++)
				{
					object tempRefParam3 = 1;
					object tempRefParam4 = cbbCodCorregimiento.get_ListIndex();
                    if (Convert.ToString(MPoblaciones.cbbComboRecibir.get_Column(1, I)).Trim().ToUpper() == Convert.ToString(cbbCodCorregimiento.get_Column(tempRefParam3, tempRefParam4)).Trim().ToUpper())
					{
						MPoblaciones.cbbComboRecibir.SelectedIndex = I;
						break;
					}
				}
				this.Close();
			}
		}

		private void cbbCodCorregimiento_Change(Object eventSender, EventArgs eventArgs)
		{
			if (Convert.ToDouble(cbbCodCorregimiento.get_ListIndex()) != -1)
			{
                cbbCodCorregimiento_ClickEvent(eventSender, eventArgs);
			}
			else
			{
				cbbCorregimiento.set_ListIndex(-1);
			}
		}

		private void cbbCodCorregimiento_ClickEvent(Object eventSender, EventArgs eventArgs)
		{
            for (int I = 0; I <= cbbCodCorregimiento.ListCount - 1; I++)
			{
				object tempRefParam = 1;
				object tempRefParam2 = I;
				object tempRefParam3 = 1;
				object tempRefParam4 = cbbCodCorregimiento.get_ListIndex();
				if (Convert.ToString(cbbCorregimiento.get_Column( tempRefParam, tempRefParam2)).Trim().ToUpper() == Convert.ToString(cbbCodCorregimiento.get_Column( tempRefParam3, tempRefParam4)).Trim().ToUpper())
				{
					I = Convert.ToInt32(tempRefParam2);
					cbbCorregimiento.set_ListIndex(I);
					break;
				}
				else
				{
					I = Convert.ToInt32(tempRefParam2);
				}
			}
		}

		private void cbbCodCorregimiento_Enter(Object eventSender, EventArgs eventArgs)
		{
            cbbCodCorregimiento.SelectionStart = 0;
            cbbCodCorregimiento.SelectionLength = cbbCodCorregimiento.Text.Trim().Length;
		}



		private void cbbCodProv_Change(Object eventSender, EventArgs eventArgs)
		{
			if (Convert.ToDouble(cbbCodProv.get_ListIndex()) != -1)
			{
                cbbCodProv_ClickEvent(eventSender, eventArgs);
			}
			else
			{
				cbbProv.set_ListIndex(-1);
				cbbCodCorregimiento.Clear();
				cbbCorregimiento.Clear();
			}
		}

		private void cbbCodProv_ClickEvent(Object eventSender, EventArgs eventArgs)
		{
            for (int I = 0; I <= cbbProv.ListCount - 1; I++)
			{
				object tempRefParam = 1;
				object tempRefParam2 = I;
				object tempRefParam3 = 1;
				object tempRefParam4 = cbbCodProv.get_ListIndex();
				if (Convert.ToString(cbbProv.get_Column( tempRefParam,  tempRefParam2)).Trim().ToUpper() == Convert.ToString(cbbCodProv.get_Column( tempRefParam3,  tempRefParam4)).Trim().ToUpper())
				{
					I = Convert.ToInt32(tempRefParam2);
					cbbProv.set_ListIndex(I);
					break;
				}
				else
				{
					I = Convert.ToInt32(tempRefParam2);
				}
			}
		}

		private void cbbCodProv_Enter(Object eventSender, EventArgs eventArgs)
		{
            cbbCodProv.SelectionStart = 0;
            cbbCodProv.SelectionLength = cbbCodProv.Text.Trim().Length;
		}

		private void cbbCorregimiento_Change(Object eventSender, EventArgs eventArgs)
		{
			if (Convert.ToDouble(cbbCorregimiento.get_ListIndex()) != -1)
			{
                cbbCorregimiento_ClickEvent(eventSender, eventArgs);
			}
			else
			{
				cbbCodCorregimiento.set_ListIndex(-1);
			}
		}

		private void cbbCorregimiento_ClickEvent(Object eventSender, EventArgs eventArgs)
		{
            for (int I = 0; I <= cbbCorregimiento.ListCount - 1; I++)
			{
				int tempRefParam = I;
				object tempRefParam2 = Type.Missing;
				object tempRefParam3 = 1;
				object tempRefParam4 = cbbCorregimiento.get_ListIndex();
				if (cbbCodCorregimiento.get_List( tempRefParam,  0).Trim().ToUpper() == Convert.ToString(cbbCorregimiento.get_Column( tempRefParam3,  tempRefParam4)).Trim().ToUpper())
				{
					I = tempRefParam;
					cbbCodCorregimiento.set_ListIndex(I);
					break;
				}
				else
				{
					I = Convert.ToInt32(tempRefParam);
				}
			}
		}

		private void cbbCorregimiento_Enter(Object eventSender, EventArgs eventArgs)
		{
            cbbCorregimiento.SelectionStart = 0;
            cbbCorregimiento.SelectionLength = cbbCorregimiento.Text.Trim().Length;
		}




		private void cbbProv_Change(Object eventSender, EventArgs eventArgs)
		{
			if (Convert.ToDouble(cbbProv.get_ListIndex()) != -1)
			{
                cbbProv_ClickEvent(eventSender, eventArgs);
			}
			else
			{
				cbbCodProv.set_ListIndex(-1);
				cbbCodCorregimiento.Clear();
				cbbCorregimiento.Clear();
			}
		}

		private void cbbProv_ClickEvent(Object eventSender, EventArgs eventArgs)
		{
            for (int I = 0; I <= cbbCodProv.ListCount - 1; I++)
			{
				int tempRefParam = I;
				object tempRefParam2 = Type.Missing;
				object tempRefParam3 = 1;
				object tempRefParam4 = cbbProv.get_ListIndex();
				if (cbbCodProv.get_List(tempRefParam,  0).Trim().ToUpper() == Convert.ToString(cbbProv.get_Column( tempRefParam3,  tempRefParam4)).Trim().ToUpper())
				{
					I = tempRefParam;
					cbbCodProv.set_ListIndex(I);
					break;
				}
				else
				{
					I = Convert.ToInt32(tempRefParam);
				}
			}
			object tempRefParam5 = 1;
			object tempRefParam6 = cbbProv.get_ListIndex();
			CargarComboAlfanumerico("SELECT gpoblaci, Dpoblaci FROM Dpoblaci WHERE gpoblaci LIKE '" + Convert.ToString(cbbProv.get_Column( tempRefParam5,  tempRefParam6)).Trim().ToUpper() + "%' order by Dpoblaci", "Gpoblaci", "Dpoblaci", cbbCorregimiento);
			object tempRefParam7 = 1;
			object tempRefParam8 = cbbProv.get_ListIndex();
			CargarComboAlfanumerico("SELECT gpoblaci FROM Dpoblaci WHERE gpoblaci LIKE '" + Convert.ToString(cbbProv.get_Column( tempRefParam7,  tempRefParam8)).Trim().ToUpper() + "%' order by gpoblaci", "gpoblaci", "gpoblaci", cbbCodCorregimiento);
		}

		private void cbbProv_Enter(Object eventSender, EventArgs eventArgs)
		{
            cbbProv.SelectionStart = 0;
            cbbProv.SelectionLength = cbbProv.Text.Trim().Length;
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void DFI111F2_Load(Object eventSender, EventArgs eventArgs)
		{

			this.Text = "B�squeda de poblaci�n por Estado - DFI111F2";
			string SQLConst = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO = 'NOMAPLIC'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLConst, MPoblaciones.GConexion);
			DataSet RrConst = new DataSet();
			tempAdapter.Fill(RrConst);
			NombreApli = Convert.ToString(RrConst.Tables[0].Rows[0]["valfanu1"]).Trim();
			RrConst.Close();



			//CARGAR LAS TABLAS DE PROVINCIAS, LAS TABLAS DE DISTRITOS y LAS TABLAS DE CORREGIMIENTOS
			CargarComboAlfanumerico("select gprovinc, dnomprov from DPROVINC order by dnomprov", "gprovinc", "dnomprov", cbbProv);
			CargarComboAlfanumerico("select gprovinc from DPROVINC order by gprovinc", "gprovinc", "gprovinc", cbbCodProv);
			cbbProv.set_ListIndex(0);

			Label1.Text = "C�d. Provincia:";
			Label2.Text = "Provincia:";
			Label3.Text = "C�d. Poblaci�n:";
			Label5.Text = "Poblaci�n:";

			object tempRefParam = 1;
			object tempRefParam2 = cbbProv.get_ListIndex();
			CargarComboAlfanumerico("SELECT gpoblaci FROM Dpoblaci WHERE gpoblaci LIKE '" + Convert.ToString(cbbProv.get_Column( tempRefParam,  tempRefParam2)).Trim().ToUpper() + "%' order by Gpoblaci", "gpoblaci", "gpoblaci", cbbCodCorregimiento);
			object tempRefParam3 = 1;
			object tempRefParam4 = cbbProv.get_ListIndex();
			CargarComboAlfanumerico("SELECT gpoblaci, Dpoblaci FROM Dpoblaci WHERE gpoblaci LIKE '" + Convert.ToString(cbbProv.get_Column( tempRefParam3,  tempRefParam4)).Trim().ToUpper() + "%' order by Dpoblaci", "gpoblaci", "dpoblaci", cbbCorregimiento);
			if (cbbCorregimiento.ListCount > 0)
			{
				cbbCorregimiento.set_ListIndex(0);
			}


			//Oscar C Marzo 2012
			//Se parametriza la provincia por defecto
			SQLConst = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO ='CODPROVI'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLConst, MPoblaciones.GConexion);
			RrConst = new DataSet();
			tempAdapter_2.Fill(RrConst);
			if (RrConst.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(RrConst.Tables[0].Rows[0]["valfanu1"]))
				{
					for (int I = 0; I <= cbbCodProv.ListCount - 1; I++)
					{
						object tempRefParam5 = 1;
						object tempRefParam6 = I;
						if (Convert.ToString(RrConst.Tables[0].Rows[0]["valfanu1"]).Trim() == Convert.ToString(cbbCodProv.get_Column( tempRefParam5,  tempRefParam6)))
						{
							I = Convert.ToInt32(tempRefParam6);
							cbbCodProv.set_ListIndex(I);
							break;
						}
						else
						{
							I = Convert.ToInt32(tempRefParam6);
						}
					}
				}
			}
			RrConst.Close();
			//-----

		}
        private void CargarComboAlfanumerico(string SQL, string CampoCod, string campoDes, UpgradeHelpers.MSForms.MSCombobox comboacargar, object comboacargar2 = null)
		{
			this.Cursor = Cursors.WaitCursor;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQL, MPoblaciones.GConexion);
			DataSet RrCombo = new DataSet();
			tempAdapter.Fill(RrCombo);
			comboacargar.Items.Clear();
            //comboacargar.Column = 2;
			comboacargar.ColumnWidths = "15;0";
			foreach (DataRow iteration_row in RrCombo.Tables[0].Rows)
			{
                comboacargar.Items.Add(new Telerik.WinControls.UI.RadListDataItem(
                    Strings.StrConv(Convert.ToString(iteration_row[campoDes]).Trim(), VbStrConv.ProperCase, 0), 
                    iteration_row[CampoCod]));

			}
			RrCombo.Close();
			this.Cursor = Cursors.Default;
		}
        public void Comborecibir(UpgradeHelpers.MSForms.MSCombobox Combo)
		{
			MPoblaciones.cbbComboRecibir = Combo;
		}
		public bool validar_datos()
		{
			bool result = false;
			result = true;
			if (Convert.ToDouble(cbbProv.get_ListIndex()) == -1)
			{
				string tempRefParam = "";
				short tempRefParam2 = 1040;
				string [] tempRefParam3 = new string []{Label2.Text};
				MPoblaciones.clasemensaje.RespuestaMensaje( NombreApli,  tempRefParam,  tempRefParam2,  MPoblaciones.GConexion,  tempRefParam3);
				return false;
			}

			if (Convert.ToDouble(cbbCorregimiento.get_ListIndex()) == -1)
			{
				string tempRefParam4 = "";
				short tempRefParam5 = 1040;
				string[] tempRefParam6 = new string[] {Label5.Text};
				MPoblaciones.clasemensaje.RespuestaMensaje( NombreApli,  tempRefParam4,  tempRefParam5,  MPoblaciones.GConexion,  tempRefParam6);
				return false;
			}
			return result;
		}
		private void DFI111F2_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}