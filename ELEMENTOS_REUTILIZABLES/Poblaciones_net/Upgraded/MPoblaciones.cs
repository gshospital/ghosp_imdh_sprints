using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Poblaciones
{
	internal static class MPoblaciones
	{

		public static SqlConnection GConexion = null;
		public static Mensajes.ClassMensajes clasemensaje = null;
		public static object oLLAMADOR = null;
        public static UpgradeHelpers.MSForms.MSCombobox cbbComboRecibir = null;


        internal static void Comborecibir(UpgradeHelpers.MSForms.MSCombobox Combo)
		{
			cbbComboRecibir = Combo;
		}
	}
}