using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using ElementosCompartidos;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Notificaciones
{
	internal static class mNotificaciones
	{
		public static SqlConnection RcNotif = null;

		//oscar 16/03/04
		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public bool VfCabecera;
			public string VstTelefoHo;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					result.VstTelefoHo = String.Empty;
					return result;
			}
		}
		public static mNotificaciones.CabCrystal VstrCrystal = mNotificaciones.CabCrystal.CreateInstance();
		//------------



		internal static string ObtenerDiagnostico(string StCodigo, string stfecha)
		{
			//Obtengo la descripci�n de un diagn�stico sabiendo su c�digo y la
			//fecha en la que se asign� a un episodio
			//************************ CRIS *****************************************

			string result = String.Empty;
			string stsql = "Select DNOMBDIA from DDIAGNOS ";
			stsql = stsql + "where GCODIDIA=" + "'" + StCodigo.Trim() + "'";
			System.DateTime TempDate = DateTime.FromOADate(0);
			stsql = stsql + " and FINVALDI<=" + "'" + ((DateTime.TryParse(stfecha, out TempDate)) ? TempDate.ToString("MM/dd/yyyy") : stfecha) + "'";
			System.DateTime TempDate2 = DateTime.FromOADate(0);
			stsql = stsql + " and (FFIVALDI>=" + "'" + ((DateTime.TryParse(stfecha, out TempDate2)) ? TempDate2.ToString("MM/dd/yyyy") : stfecha) + "'";
			stsql = stsql + " or FFIVALDI IS NULL)";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, RcNotif);
			DataSet rrDiag = new DataSet();
			tempAdapter.Fill(rrDiag);

			result = Convert.ToString(rrDiag.Tables[0].Rows[0]["DNOMBDIA"]).Trim();

			rrDiag.Close();

			return result;
		}


		internal static string probuscar_provincia(string Codigo)
		{
			string result = String.Empty;
			string sql = String.Empty;
			DataSet Rrsql = null;
			string PROVINCIA = String.Empty;
			try
			{
				sql = "select dnomprov from dprovinc where gprovinc = '" + Codigo + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcNotif);
				Rrsql = new DataSet();
				tempAdapter.Fill(Rrsql);
				if (Rrsql.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(Rrsql.Tables[0].Rows[0]["dnomprov"]))
					{
						PROVINCIA = Convert.ToString(Rrsql.Tables[0].Rows[0]["dnomprov"]).Trim();
					}
					else
					{
						PROVINCIA = "";
					}
				}
				else
				{
					PROVINCIA = "";
				}
				result = PROVINCIA;
				Rrsql.Close();
				return result;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), "Desconocido", "Menu_Admision:probuscar_provincia", ex);
				return result;
			}
		}

		//oscar 16/03/04
		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************


			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RcNotif);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);

			VstrCrystal.VstGrupoHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1, valfanu2 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, RcNotif);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			
			VstrCrystal.VstNombreHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			
			VstrCrystal.VstTelefoHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU2"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU2"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, RcNotif);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			
			VstrCrystal.VstDireccHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tRrCrystal.Close();
			VstrCrystal.VfCabecera = true;
		}
		//-------------

		internal static string probuscar_poblacion(string Codigo)
		{
			string result = String.Empty;
			string sql = String.Empty;
			DataSet Rrsql = null;
			string stPoblacion = String.Empty;
			try
			{
				sql = "select dpoblaci from dpoblaci where gpoblaci = '" + Codigo + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcNotif);
				Rrsql = new DataSet();
				tempAdapter.Fill(Rrsql);
				if (Rrsql.Tables[0].Rows.Count != 0)
				{
					
					if (!Convert.IsDBNull(Rrsql.Tables[0].Rows[0]["dpoblaci"]))
					{
						stPoblacion = Convert.ToString(Rrsql.Tables[0].Rows[0]["dpoblaci"]).Trim();
					}
					else
					{
						stPoblacion = "";
					}
				}
				else
				{
					stPoblacion = "";
				}
				result = stPoblacion;
				Rrsql.Close();
				return result;
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("admision", Path.GetDirectoryName(Application.ExecutablePath), "Desconocido", "Menu_Admision:probuscar_poblacion", ex);
				return result;
			}
		}
	}
}