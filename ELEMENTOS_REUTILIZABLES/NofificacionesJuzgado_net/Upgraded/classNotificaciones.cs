using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using ElementosCompartidos;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using CrystalWrapper;

namespace Notificaciones
{
	public class classNotificaciones
	{



		Crystal Listado = null; // objecto crystal report
		int iNumeroReg = 0;
		int iAñoReg = 0;
		string VstIdentificador = String.Empty;
		string UsuarioCrystal = String.Empty;
		string ContraseñaCrystal = String.Empty;
		string Path = String.Empty;
		string PathInformes = String.Empty;
		dynamic FormularioQueLlama = null;
		string NOMBRE_DSN_SQLSERVER = String.Empty;
		PrintDialog Ventana = null; // objecto commondialog


		// variables locales comunes a todos los informes
		string NOMBREHOSPITAL = String.Empty;
		string DIRECCIONHOSPITAL = String.Empty;
		string NOMBREDIRECTOR = String.Empty;
		string NOMBREJUZGADODECANO = String.Empty;
		string NUMEROJUZGADODECANO = String.Empty;
		string DIRECCIONJUZGADODECANO = String.Empty;
		string PROVINCIA = String.Empty;


		//oscar 16/03/04
		string DNICEDULA = String.Empty;
		string LITPERSO = String.Empty;
		//----


		// recogida de los valores comunes a todos los informes de notificaciones
		public void RecogerParametrosComunes(dynamic FormularioLLamador, SqlConnection Conexion, dynamic ObjectoCrystal, string Numero, string Año, string Identificador, string usuario, string ContraseñaBBDD, string PathAplic, string PathReport, string DsnSqlServer, object VentanaDialogo)
		{
			mNotificaciones.RcNotif = Conexion;
			Listado = ObjectoCrystal;
			FormularioQueLlama = FormularioLLamador;
			Serrores.AjustarRelojCliente(mNotificaciones.RcNotif);
			iNumeroReg = Convert.ToInt32(Double.Parse(Numero));
			iAñoReg = Convert.ToInt32(Double.Parse(Año));
			VstIdentificador = Identificador;
			UsuarioCrystal = usuario;
			ContraseñaCrystal = ContraseñaBBDD;
			Path = PathAplic;
			PathInformes = PathReport;
			NOMBRE_DSN_SQLSERVER = DsnSqlServer;
			Ventana = (PrintDialog)VentanaDialogo;
			proObtenerDatosComunes();
		}
		// fin de recogida de los valores comunes


		// funciones asociadas a cada una de las notificaciones a imprimir :

		public void proImprimirIncapacidadJuzgado()
		{
			string sql = String.Empty;
			DataSet RrSql = null;
			string diagnostico_paciente = String.Empty;
			try
			{
                //Set Ventana = MIN000F1.cdAyuda
                //Ventana.ShowPrinter
                //Set Listado = Listado_general
                //Me.MousePointer = 11
                Listado.ReportFileName = PathInformes + "\\agi171r1_CR11.rpt";

                sql = "select nnumexp1,odiaging,gdiaging,fllegada,aepisadm.gmotingr,dmotingr,ddirjuzg,dnomnum,dnomjuzg" + 
				      " from aepisadm " + 
				      " inner join dmotingr on aepisadm.gmotingr=dmotingr.gmotingr " + 
				      " left  join djuzgado on aepisadm.gjuzgad1= djuzgado.gjuzgado " + 
				      " Where " + 
				      " gidenpac = '" + VstIdentificador + "'and " + 
				      " ganoadme =  " + iAñoReg.ToString() + " and " + 
				      " gnumadme = " + iNumeroReg.ToString() + " ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["odiaging"]))
					{
						diagnostico_paciente = Convert.ToString(RrSql.Tables[0].Rows[0]["odiaging"]).Trim();
					}
					else
					{
						if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gdiaging"]))
						{
							diagnostico_paciente = mNotificaciones.ObtenerDiagnostico(Convert.ToString(RrSql.Tables[0].Rows[0]["gdiaging"]), Convert.ToString(RrSql.Tables[0].Rows[0]["fllegada"]));
						}
					}
					
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["nnumexp1"]))
					{
                        //Listado.Formulas["{@Juzgado}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomjuzg"]) + "\"";
                        Listado.Formulas["{@Juzgado}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomjuzg"]) + "\"";
                        //Listado.Formulas["{@Direccion}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["ddirjuzg"]) + "\"";
                        Listado.Formulas["{@Direccion}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["ddirjuzg"]) + "\"";
                        
					}
					else
					{
                        //Listado.Formulas["{@Juzgado}"] = "\"" + NOMBREJUZGADODECANO + "\"";
                        Listado.Formulas["{@Juzgado}"] = "\"" + NOMBREJUZGADODECANO + "\"";
                        //Listado.Formulas["{@Direccion}"] = "\"" + DIRECCIONJUZGADODECANO + "\"";
                        Listado.Formulas["{@Direccion}"] = "\"" + DIRECCIONJUZGADODECANO + "\"";
                        
					}
				}
				RrSql.Close();

                //Listado.Formulas["{@Nombre_Director}"] = " \"" + NOMBREDIRECTOR + "\"";
                Listado.Formulas["{@Nombre_Director}"] = "\"" + NOMBREDIRECTOR + "\"";
                //Listado.Formulas["{@NHospit}"] = " \"" + NOMBREHOSPITAL + "\"";
                Listado.Formulas["{@NHospit}"] = "\"" + NOMBREHOSPITAL + "\"";
                //Listado.Formulas["{@DDireccion}"] = " \"" + DIRECCIONHOSPITAL + "\"";
                Listado.Formulas["{@DDireccion}"] = "\"" + DIRECCIONHOSPITAL + "\"";
                //Listado.Formulas["{@Diagnostico}"] = " \"" + diagnostico_paciente + "\"";
                Listado.Formulas["{@Diagnostico}"] = "\"" + diagnostico_paciente + "\"";


                //oscar 22/03/04
                //SQL = "select dnomprov From sconsglo , dprovinc Where sconsglo.gconsglo = 'CODPROVI' and " & _
                //'" substring(valfanu1,1,2)=gprovinc"
                //Set RRSQL = RcNotif.OpenResultset(SQL, rdOpenKeyset)
                //Listado.Formulas(6) = "Poblacion= """ & Trim(RRSQL("dnomprov")) & """"
                //Listado.Formulas["{@Poblacion}"] = " \"" + PROVINCIA + "\"";
                Listado.Formulas["{@Poblacion}"] = "\"" + PROVINCIA + "\"";
                //----------

                //oscar 24/03/04
                if (!mNotificaciones.VstrCrystal.VfCabecera)
				{
					mNotificaciones.proCabCrystal();
				}
                //Listado.Formulas["{@Nombre_Director}"]
                Listado.Formulas["{@Nombre_Director}"] = "\"" + mNotificaciones.VstrCrystal.VstGrupoHo + "\"";
                //Listado.Formulas["{@NHospit}"]
                Listado.Formulas["{@NHospit}"] = "\"" + mNotificaciones.VstrCrystal.VstNombreHo + "\"";
                //Listado.Formulas["{@DDireccion}"]
                Listado.Formulas["{@DDireccion}"] = "\"" + mNotificaciones.VstrCrystal.VstDireccHo + "\"";
                //Listado.Formulas["{@Poblacion}"]
                Listado.Formulas["{@Poblacion}"] = "\"" + Serrores.ExisteLogo(mNotificaciones.RcNotif) + "\"";
                //Listado.Formulas["{@FORM1}"]
                Listado.Formulas["{@FORM1}"] = "\"" + DNICEDULA + "\"";
                //Listado.Formulas["{@FORM2}"]
                Listado.Formulas["{@FORM2}"] = "\"" + LITPERSO.ToLower() + "\"";
                

				string stPaciente = String.Empty;
				sql = "SELECT * From DPACIENT where gidenpac = '" + VstIdentificador + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter_2.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnombpac"]))
					{
						stPaciente = Convert.ToString(RrSql.Tables[0].Rows[0]["dnombpac"]).Trim();
					}
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape1pac"]))
					{
						stPaciente = stPaciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape1pac"]).Trim();
					}
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape2pac"]))
					{
						stPaciente = stPaciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape2pac"]).Trim();
					}
					Listado.Formulas["{@FORM3}"] = "\"" + stPaciente + "\"";
				}
				RrSql.Close();
				//-----------

				Listado.SQLQuery = "SELECT dnombpac, dape1pac, dape2pac, ndninifp From DPACIENT where gidenpac = '" + VstIdentificador + "'";
				
				Listado.WindowTitle = "Notificación de la incapacidad al juzgado";
				Listado.WindowState = (Crystal.WindowStateConstants)2; // crptMaximized
				Listado.Connect = "" + NOMBRE_DSN_SQLSERVER + ";" + UsuarioCrystal + ";" + ContraseñaCrystal + "";

				//oscar 24/03/04
				Listado.SubreportToChange = "LogotipoIDC";
				Listado.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                Listado.Connect = "" + NOMBRE_DSN_SQLSERVER + ";" + UsuarioCrystal + ";" + ContraseñaCrystal + "" + "";
				//------

				Listado.Destination = (Crystal.DestinationConstants)1; //crptToPrinter
				Listado.WindowShowPrintSetupBtn = true;
				Listado.CopiesToPrinter = Ventana.PrinterSettings.Copies;
				Ventana.PrinterSettings.Copies = 1;
				Listado.Action = 1;

				//una vez emitida la notificación de incapacidad al juzgado se rellena
				//la fecha de emisión de la notificacion en aepisadm

				sql = "select * from aepisadm where " + 
				      " aepisadm.ganoadme = " + iAñoReg.ToString() + " and  " + 
				      " aepisadm.gnumadme = " + iNumeroReg.ToString();
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter_3.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count > 0)
				{
					RrSql.Edit();
					RrSql.Tables[0].Rows[0]["fnotifincap"] = DateTime.Now;
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
					tempAdapter_3.Update(RrSql, RrSql.Tables[0].TableName);
				}
				RrSql.Close();
				Listado.PrinterStopPage = Listado.PageCount;
				Listado.Reset();
			}
			catch(Exception ex)
			{
				FormularioQueLlama.MousePointer = 99;
				Serrores.AnalizaError("Notificaciones", System.IO.Path.GetDirectoryName(Application.ExecutablePath), UsuarioCrystal, "ProImprimirIncapacidadJuzgado", ex);
				/*INDRA JPROCHE TODO_X_4 Reporte Crystal 03/05/2016
				Listado.Reset();
                */
			}
		}

		public void proImprimirInternamientoJudicial()
		{
			string sql = String.Empty;
			DataSet RrSql = null;
			string diagnostico_paciente = String.Empty;
			try
			{
				Listado.Reset();
                Listado.ReportFileName = PathInformes + "\\agi175r1_CR11.rpt";
                //Listado.Formulas["{@DDireccion}"]
                Listado.Formulas["{@DDireccion}"] = "\"" + NOMBREDIRECTOR + "\"";
                //Listado.Formulas["{@Diagnostico}"]
                Listado.Formulas["{@Diagnostico}"] = "\"" + NOMBREHOSPITAL + "\"";
                //Listado.Formulas["{@Poblacion}"]
                Listado.Formulas["{@Poblacion}"] = "\"" + DIRECCIONHOSPITAL + "\"";

				//oscar 16/03/04
				if (!mNotificaciones.VstrCrystal.VfCabecera)
				{
					mNotificaciones.proCabCrystal();
				}
                //Listado.Formulas["{@DDireccion}"]
                Listado.Formulas["{@DDireccion}"] = "\"" + mNotificaciones.VstrCrystal.VstGrupoHo + "\"";
                //Listado.Formulas["{@Poblacion}"]
                Listado.Formulas["{@Poblacion}"] = "\"" + mNotificaciones.VstrCrystal.VstNombreHo + "\"";
                //Listado.Formulas["{@FORM1}"]
                Listado.Formulas["{@FORM1}"] = "\"" + mNotificaciones.VstrCrystal.VstDireccHo + "\"";
				//-----------

				sql = "select nnumexp1,odiaging,gdiaging,fllegada,aepisadm.gmotingr,dmotingr,ddirjuzg,dnomnum,dnomjuzg" + 
				      " from aepisadm " + 
				      " inner join dmotingr on aepisadm.gmotingr=dmotingr.gmotingr " + 
				      " left  join djuzgado on aepisadm.gjuzgad1= djuzgado.gjuzgado " + 
				      " Where " + 
				      " gidenpac = '" + VstIdentificador + "'and " + 
				      " ganoadme =  " + iAñoReg.ToString() + " and " + 
				      " gnumadme = " + iNumeroReg.ToString() + " ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gmotingr"]))
					{
						diagnostico_paciente = Convert.ToString(RrSql.Tables[0].Rows[0]["dmotingr"]).Trim();
					}
					else
					{
						diagnostico_paciente = " ";
					}
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["nnumexp1"]))
					{
                        //Listado.Formulas["{@Juzgado}"]
                        Listado.Formulas["{@Juzgado}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomjuzg"]) + "\"";
                        //Listado.Formulas["{@Direccion}"]
                        Listado.Formulas["{@Direccion}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["ddirjuzg"]) + "\"";
                        //Listado.Formulas["{@Nombre_Director}"]
                        Listado.Formulas["{@Nombre_Director}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["nnumexp1"]).Trim() + "\"";
                        //Listado.Formulas["{@NJuzgado1}"]
                        Listado.Formulas["{@NJuzgado1}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomnum"]).Trim() + "\"";
					}
					else
					{
                        
						Listado.Formulas["{@Juzgado}"] = "\"" + NOMBREJUZGADODECANO + "\"";
						Listado.Formulas["{@Direccion}"] = "\"" + DIRECCIONJUZGADODECANO + "\"";
					}
				}

                //Listado.Formulas["{@Nombre_Director}"]
                Listado.Formulas["{@Nombre_Director}"] = "\"" + diagnostico_paciente + "\"";

				//oscar
				//SQL = "select dnomprov From sconsglo , dprovinc " & _
				//'      " Where sconsglo.gconsglo = 'CODPROVI' and " & _
				//'      " substring(valfanu1,1,2)=gprovinc"
				//Set RRSQL = RcNotif.OpenResultset(SQL, rdOpenKeyset)
				//Listado.Formulas(8) = "Poblacion= """ & Trim(RRSQL("dnomprov")) & """"
				//RRSQL.Close
				Listado.Formulas["{@Poblacion}"] = "\"" + PROVINCIA + "\"";
                //-------

                //oscar 16/03/04
                //Listado.Formulas["{@FORM2}"]
                Listado.Formulas["{@FORM2}"] = "\"" + Serrores.ExisteLogo(mNotificaciones.RcNotif) + "\"";
                //Listado.Formulas["{@FORM3}"]
                Listado.Formulas["{@FORM3}"] = "\"" + DNICEDULA + "\"";

				string stPaciente = String.Empty;
				sql = "SELECT * From DPACIENT where gidenpac = '" + VstIdentificador + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter_2.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnombpac"]))
					{
						stPaciente = Convert.ToString(RrSql.Tables[0].Rows[0]["dnombpac"]).Trim();
					}
					
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape1pac"]))
					{
						stPaciente = stPaciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape1pac"]).Trim();
					}
					
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape2pac"]))
					{
						stPaciente = stPaciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape2pac"]).Trim();
					}
                    //Listado.Formulas["{@LogoIDC}"]
                    Listado.Formulas["{@LogoIDC}"] = "\"" + stPaciente + "\"";
				}
				RrSql.Close();
                //-------
                
                
                

				Listado.WindowTitle = "Notificación del ingreso con internamiento judicial";
				Listado.WindowState = (Crystal.WindowStateConstants)2; //crptMaximized

				Listado.Connect = "" + NOMBRE_DSN_SQLSERVER + ";" + UsuarioCrystal + ";" + ContraseñaCrystal + "";
				Listado.SQLQuery = "SELECT dnombpac,dape1pac,dape2pac,ndninifp From DPACIENT where gidenpac = '" + VstIdentificador + "'";

				Listado.Destination = (Crystal.DestinationConstants)1; // crptToPrinter 'crptToWindow
				//Listado.WindowShowPrintSetupBtn = True
				Listado.CopiesToPrinter = Ventana.PrinterSettings.Copies;
				Ventana.PrinterSettings.Copies = 1;

				//oscar 16/03/04
				Listado.SubreportToChange = "LogotipoIDC";
				Listado.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                Listado.Connect = "" + NOMBRE_DSN_SQLSERVER + ";" + UsuarioCrystal + ";" + ContraseñaCrystal + "" + "";
				//------

				Listado.Action = 1;
				Listado.PrinterStopPage = Listado.PageCount;
				Listado.Reset();
			}
			catch(Exception ex)
			{
				FormularioQueLlama.MousePointer = 99;
				Serrores.AnalizaError("Notificaciones", System.IO.Path.GetDirectoryName(Application.ExecutablePath), UsuarioCrystal, "ProImprimirInternamientoJudicial", ex);
				Listado.Reset();
                
			}

		}


		public void proImprimirDuplicidadExpediente()
		{
			string cod_juzgado1 = String.Empty;
			string cod_juzgado2 = String.Empty;
			string stPoblacion = String.Empty;
			string sql = String.Empty;
			DataSet RrSql = null;
			try
			{
				Listado.Reset();
                Listado.ReportFileName = PathInformes + "\\agi173r1_CR11.rpt";
                sql = "select * from aepisadm where " + 
				      " gidenpac = '" + VstIdentificador + "'and " + 
				      " ganoadme =  " + iAñoReg.ToString() + " and " + 
				      " gnumadme = " + iNumeroReg.ToString() + " ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					cod_juzgado1 = Convert.ToString(RrSql.Tables[0].Rows[0]["gjuzgad1"]);
					cod_juzgado2 = Convert.ToString(RrSql.Tables[0].Rows[0]["gjuzgad2"]);

                    //Listado.Formulas["{@Juzgado}"]
                    Listado.Formulas["{@Juzgado}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["nnumexp1"]).Trim() + "\"";
                    //Listado.Formulas["{@Direccion}"]
                    Listado.Formulas["{@Direccion}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["nnumexp2"]).Trim() + "\"";
				}
				RrSql.Close();

				sql = " select * from djuzgado where gjuzgado = '" + cod_juzgado1 + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter_2.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
                    //Listado.Formulas["{@Nombre_Director}"]
                    Listado.Formulas["{@Nombre_Director}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomjuzg"]).Trim() + "\"";
                    //Listado.Formulas["{@NHospit}"]
                    Listado.Formulas["{@NHospit}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomnum"]).Trim() + "\"";
                    //Listado.Formulas["{@DDireccion}"]
                    Listado.Formulas["{@DDireccion}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["ddirjuzg"]).Trim() + "\"";

					stPoblacion = "" + Convert.ToString(RrSql.Tables[0].Rows[0]["gcodipos"]).Trim();
					//stPoblacion = stPoblacion & " " & probuscar_provincia("" & Rrsql("gprovinc"))
					stPoblacion = stPoblacion + " " + mNotificaciones.probuscar_poblacion("" + Convert.ToString(RrSql.Tables[0].Rows[0]["gpoblaci"]));
                    
                    Listado.Formulas["{@Diagnostico}"] = "\"" + stPoblacion + "\"";
            
				}
				RrSql.Close();

				sql = " select dnomjuzg, dnomnum from djuzgado where gjuzgado = '" + cod_juzgado2 + "'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter_3.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
                    //Listado.Formulas["{@Poblacion}"]
                    Listado.Formulas["{@Poblacion}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomnum"]).Trim() + "\"";
                    //Listado.Formulas["{@Juzgado2}"]
                    Listado.Formulas["{@Juzgado2}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomjuzg"]).Trim() + "\"";
                
				}
				
				RrSql.Close();

                //Listado.Formulas["{@Nombre_Director}"]
                Listado.Formulas["{@Nombre_Director}"] = "\"" + NOMBREDIRECTOR + "\"";
                //Listado.Formulas["{@NHospit}"]
                Listado.Formulas["{@NHospit}"] = "\"" + NOMBREHOSPITAL + "\"";
                //Listado.Formulas["{@DDireccion}"]
                Listado.Formulas["{@DDireccion}"] = "\"" + DIRECCIONHOSPITAL + "\"";

                //oscar
                //SQL = "select dnomprov From sconsglo , dprovinc " & _
                //'      " Where sconsglo.gconsglo = 'CODPROVI' and " & _
                //'      " substring(valfanu1,1,2)=gprovinc"
                //Set RRSQL = RcNotif.OpenResultset(SQL, rdOpenKeyset)
                //If Not RRSQL.EOF Then Listado.Formulas(10) = "Poblacion= """ & Trim(RRSQL("dnomprov")) & """"
                //RRSQL.Close
                //Listado.Formulas["{@Poblacion}"]
                Listado.Formulas["{@Poblacion}"] = "\"" + PROVINCIA + "\"";
				//------
              

				//oscar 16/03/04
				if (!mNotificaciones.VstrCrystal.VfCabecera)
				{
					mNotificaciones.proCabCrystal();
				}

                //Listado.Formulas["{@FORM1}"]
                Listado.Formulas["{@FORM1}"] = "\"" + mNotificaciones.VstrCrystal.VstGrupoHo + "\"";
                //Listado.Formulas["{@FORM2}"]
                Listado.Formulas["{@FORM2}"] = "\"" + mNotificaciones.VstrCrystal.VstNombreHo + "\"";
                //Listado.Formulas["{@FORM3}"]
                Listado.Formulas["{@FORM3}"] = "\"" + mNotificaciones.VstrCrystal.VstDireccHo + "\"";
                //Listado.Formulas["{@LogoIDC}"]
                Listado.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(mNotificaciones.RcNotif) + "\"";
                //Listado.Formulas["{@DNICEDULA}"]
                Listado.Formulas["{@DNICEDULA}"] = "\"" + DNICEDULA + "\"";
				//-----------

				Listado.SQLQuery = "SELECT dnombpac,dape1pac,dape2pac,ndninifp From DPACIENT where gidenpac = '" + VstIdentificador + "'";
				Listado.Connect = "" + NOMBRE_DSN_SQLSERVER + ";" + UsuarioCrystal + ";" + ContraseñaCrystal + "";


				
				Listado.WindowTitle = "Notificación de duplicidad de expediente";
				Listado.WindowState = (Crystal.WindowStateConstants)2; // crptMaximized
				Listado.Destination = (Crystal.DestinationConstants)1; // crptToPrinter
				Listado.WindowShowPrintSetupBtn = true;
				Listado.CopiesToPrinter = Ventana.PrinterSettings.Copies;
				Ventana.PrinterSettings.Copies = 1;

				//oscar 16/03/04
				Listado.SubreportToChange = "LogotipoIDC";
				Listado.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                Listado.Connect = "" + NOMBRE_DSN_SQLSERVER + ";" + UsuarioCrystal + ";" + ContraseñaCrystal + "" + "";
				//------

				Listado.Action = 1;
				Listado.PrinterStopPage = Listado.PageCount;
				Listado.Reset();
			}
			catch(Exception ex)
			{
				FormularioQueLlama.MousePointer = 99;
				Serrores.AnalizaError("Notificaciones", System.IO.Path.GetDirectoryName(Application.ExecutablePath), UsuarioCrystal, "ProImprimirDuplicidadExpediente", ex);
                
				Listado.Reset();
                
			}

		}

		public void proImprimirAltaIncapacitado()
		{
			string stPoblacion_Juzgado = String.Empty;
			string stPaciente = String.Empty;
			string Poblacion = String.Empty;
			string sql = String.Empty;
			DataSet RrSql = null;
			string fecha_alta = String.Empty;
			try
			{
				//oscar
				//SQL = "select dnomprov From sconsglo , dprovinc Where sconsglo.gconsglo = 'CODPROVI' and " & _
				//'" substring(valfanu1,1,2)=gprovinc"
				//Set RRSQL = RcNotif.OpenResultset(SQL, rdOpenKeyset)
				//stPoblacion_Juzgado = Trim(RRSQL("dnomprov"))
				stPoblacion_Juzgado = PROVINCIA;
				//---

				sql = "select " + 
				      " djuzgado.dnomjuzg, djuzgado.dnomnum,  djuzgado.ddirjuzg, " + 
				      " djuzgado.gcodipos, dpoblaci.dpoblaci, dprovinc.dnomprov, " + 
				      " dpacient.dape1pac, dpacient.dape2pac, dpacient.dnombpac, " + 
				      " aepisadm.nnumexp1, aepisadm.fllegada, dpacient.ndninifp," + 
				      " aepisadm.faltplan , dmotalta.dmotalta " + 
				      " From " + 
				      " aepisadm " + 
				      " inner join dpacient on aepisadm.gidenpac = dpacient.gidenpac " + 
				      " inner join djuzgado on aepisadm.gjuzgad1 = djuzgado.gjuzgado " + 
				      " inner join dmotalta on aepisadm.gmotalta = dmotalta.gmotalta " + 
				      " left  join dpoblaci on djuzgado.gpoblaci = dpoblaci.gpoblaci " + 
				      " left  join dprovinc on djuzgado.gprovinc = dprovinc.gprovinc " + 
				      " Where " + 
				      " aepisadm.ganoadme = " + iAñoReg.ToString() + " and  " + 
				      " aepisadm.gnumadme = " + iNumeroReg.ToString() + " and  " + 
				      " aepisadm.gjuzgad1 is not null ";


				// QUITO LA CONDICION ESTA PARA QUE
				// SE PUEDA SACAR DESDE SERVICIOS MEDICOS Y DESDE ADMISION
				//" aepisadm.faltplan is not null and  " & _
				//
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
                    Listado.ReportFileName = PathInformes + "\\aga227r1_CR11.rpt";
                    //Listado.Formulas["{@Juzgado1}"]
                    Listado.Formulas["{@Juzgado1}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomjuzg"]).Trim() + "\"";
                    //Listado.Formulas["{@NJuzgado1}"]
                    Listado.Formulas["{@NJuzgado1}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dnomnum"]).Trim() + "\"";
                    //Listado.Formulas["{@DirJuzgado1}"]
                    Listado.Formulas["{@DirJuzgado1}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["ddirjuzg"]).Trim() + "\"";

					bool tempBool = false;
					string auxVar = Convert.ToString(RrSql.Tables[0].Rows[0]["gcodipos"]).Trim();
					Poblacion = (((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))) ? Convert.ToString(RrSql.Tables[0].Rows[0]["gcodipos"]).Trim() : "";
					Poblacion = Poblacion + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dpoblaci"]).Trim();

                    //Listado.Formulas["{@PobJuzgado1}"]
                    Listado.Formulas["{@PobJuzgado1}"] = "\"" + Poblacion + "\"";
                    //Listado.Formulas["{@Nombre_Director}"]
                    Listado.Formulas["{@Nombre_Director}"] = "\"" + NOMBREDIRECTOR + "\"";
                    //Listado.Formulas["{@NHospit}"]
                    Listado.Formulas["{@NHospit}"] = "\"" + NOMBREHOSPITAL + "\"";
                    //Listado.Formulas["{@DDireccion}"]
                    Listado.Formulas["{@DDireccion}"] = "\"" + DIRECCIONHOSPITAL + "\"";
                    //Listado.Formulas["{@MOTIVO}"]
                    Listado.Formulas["{@MOTIVO}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["dmotalta"]).Trim() + "\"";

					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnombpac"]))
					{
						stPaciente = Convert.ToString(RrSql.Tables[0].Rows[0]["dnombpac"]).Trim();
					}
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape1pac"]))
					{
						stPaciente = stPaciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape1pac"]).Trim();
					}
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape2pac"]))
					{
						stPaciente = stPaciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape2pac"]).Trim();
					}

                    //Listado.Formulas["{@Paciente}"]
                    Listado.Formulas["{@Paciente}"] = "\"" + stPaciente + "\"";
                    //Listado.Formulas["{@DNI}"]
                    Listado.Formulas["{@DNI}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["ndninifp"]).Trim() + "\"";
                    //Listado.Formulas["{@NExpediente1}"]
                    Listado.Formulas["{@NExpediente1}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["nnumexp1"]).Trim() + "\"";
                    //Listado.Formulas["{@FECHA_LLEGADA}"]
                    Listado.Formulas["{@FECHA_LLEGADA}"] = "\"" + Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy") + "\"";
                    //Listado.Formulas["{@Poblacion_juzgado}"]
                    Listado.Formulas["{@Poblacion_juzgado}"] = "\"" + stPoblacion_Juzgado + "\"";

					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["faltplan"]))
					{
						fecha_alta = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["faltplan"]).ToString("dd") + " de " + 
						             Convert.ToDateTime(RrSql.Tables[0].Rows[0]["faltplan"]).ToString("MMMM") + " de " + Convert.ToDateTime(RrSql.Tables[0].Rows[0]["faltplan"]).ToString("yyyy");
					}
					else
					{
						fecha_alta = DateTime.Now.ToString("dd") + " de " + 
						             DateTime.Now.ToString("MMMM") + " de " + DateTime.Now.ToString("yyyy");
					}

                    //Listado.Formulas["{@Fecha_Alta}"]
                    Listado.Formulas["{@Fecha_Alta}"] = "\"" + fecha_alta + "\"";

					


					//oscar 22/03/04
					if (!mNotificaciones.VstrCrystal.VfCabecera)
					{
						mNotificaciones.proCabCrystal();
					}

                    //Listado.Formulas["{@FORM1}"]
                    Listado.Formulas["{@FORM1}"] = "\"" + mNotificaciones.VstrCrystal.VstGrupoHo + "\"";
                    //Listado.Formulas["{@FORM2}"]
                    Listado.Formulas["{@FORM2}"] = "\"" + mNotificaciones.VstrCrystal.VstNombreHo + "\"";
                    //Listado.Formulas["{@FORM3}"]
                    Listado.Formulas["{@FORM3}"] = "\"" + mNotificaciones.VstrCrystal.VstDireccHo + "\"";
                    //Listado.Formulas["{@LogoIDC}"]
                    Listado.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(mNotificaciones.RcNotif) + "\"";
                    //Listado.Formulas["{@DNICEDULA}"]
                    Listado.Formulas["{@DNICEDULA}"] = "\"" + DNICEDULA + "\"";

					Listado.SubreportToChange = "LogotipoIDC";
					Listado.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
					Listado.Connect = "" + NOMBRE_DSN_SQLSERVER + ";" + UsuarioCrystal + ";" + ContraseñaCrystal + "" + "";
					//------


					Listado.WindowTitle = "Notificación al Juzgado del alta del incapacitado";
					Listado.WindowState = (Crystal.WindowStateConstants)2; // crptMaximized
					Listado.Destination = (Crystal.DestinationConstants)1; // toprinter
					Listado.WindowShowPrintSetupBtn = true;
					Listado.CopiesToPrinter = Ventana.PrinterSettings.Copies;
					Ventana.PrinterSettings.Copies = 1;
					Listado.Action = 1;
					Listado.PrinterStopPage = Listado.PageCount;
					Listado.Reset();

				}
				RrSql.Close();
			}
			catch(Exception ex)
			{
				
				FormularioQueLlama.MousePointer = 99;
				Serrores.AnalizaError("Notificaciones", System.IO.Path.GetDirectoryName(Application.ExecutablePath), UsuarioCrystal, "ProImprimirAltaIncapacitado", ex);
                
				Listado.Reset();
			}
		}


		public void proImprimirFallecimientoIncapacitado()
		{
			//OSCAR 23/03/04
			//SE HA TRAIDO AQUI PARA UNIFIAR TODOS LAS NOTIFICACIONES EN UNA UNICA DLL
			//ANTES SE LLAMABA A LA DLL EINFORFALLECIMIENTO

			//''''''On Error GoTo error
			//''''''Dim objecto As Object
			//''''''Dim Salida_listado As Integer
			//'''''''If Parasalida = crptToPrinter Then
			//''''''    Salida_listado = 1   ' crpttoprinter
			//'''''''Else
			//'''''''    Salida_listado = 0
			//'''''''End If
			//''''''Set objecto = CreateObject("einforfallecimiento.inforfallecimiento")
			//''''''objecto.proload FormularioQueLlama, RcNotif, Listado, Salida_listado, iNumeroReg, iAñoReg, Path & "\..\ElementosComunes"
			//''''''Set objecto = Nothing
			//''''''Exit Function
			//''''''error:
			//''''''    MsgBox "Error llamando a informe de fallecimiento"
			//''''''
			//'''''''ChbFallecInc.Value = 0
			//'''''''cbImprimir.Enabled = False

			string stNombre_Director = String.Empty;
			string stNHospit = String.Empty;
			string stDDireccion = String.Empty;
			string stPoblacion_Juzgado = String.Empty;
			string stPaciente = String.Empty;
			string stPoblacion = String.Empty;
			string sql = String.Empty;
			DataSet RrSql = null;

			try
			{


				stNombre_Director = NOMBREDIRECTOR;
				stNHospit = NOMBREHOSPITAL;
				stDDireccion = DIRECCIONHOSPITAL;

				//oscar
				//SQL = "select dnomprov From sconsglo , dprovinc Where sconsglo.gconsglo = 'CODPROVI' and " & _
				//'" substring(valfanu1,1,2)=gprovinc"
				//Set RRSQL = RcAdmision.OpenResultset(SQL, rdOpenKeyset)
				//stPoblacion_Juzgado = Trim(RRSQL("dnomprov"))
				stPoblacion_Juzgado = PROVINCIA;
				//----------

				// MODIFICACION AL 21/08/1998 ahora puede QUE NO TENGA JUZGADO

				sql = "select AEPISADM.GJUZGAD1, " + 
				      " dmotalta.dmotalta, dpacient.dnombpac, dpacient.dape1pac, " + 
				      " dpacient.dape2pac, dpacient.ndninifp, aepisadm.nnumexp1, " + 
				      " aepisadm.fllegada, aepisadm.ocinmexi, aepisadm.ocpriexi, " + 
				      " DPACIENT.FFALLECI From " + 
				      " aepisadm , dmotalta, dpacient " + 
				      " Where " + 
				      " aepisadm.ganoadme = " + iAñoReg.ToString() + " and " + 
				      " aepisadm.gnumadme = " + iNumeroReg.ToString() + " and " + 
				      " aepisadm.gmotalta = dmotalta.gmotalta and " + 
				      " dmotalta.icexitus = 'S' and " + 
				      " aepisadm.gidenpac = dpacient.gidenpac";


				// MODIFICACION
				DataSet Rrsql2 = null;
				string sql2 = String.Empty;

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
                    Listado.ReportFileName = PathInformes + "\\aga228r1_CR11.rpt";
					
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gjuzgad1"]))
					{ // obtener juzgado
						sql2 = " select djuzgado.dnomjuzg, djuzgado.dnomnum,dnomprov,dpoblaci,djuzgado.ddirjuzg, " + 
						       " djuzgado.gcodipos " + 
						       " from djuzgado " + 
						       " left  join dpoblaci on djuzgado.gpoblaci = dpoblaci.gpoblaci " + 
						       " left  join dprovinc on djuzgado.gprovinc = dprovinc.gprovinc " + 
						       " Where " + 
						       " gjuzgado='" + Convert.ToString(RrSql.Tables[0].Rows[0]["gjuzgad1"]).Trim() + "'";
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql2, mNotificaciones.RcNotif);
						Rrsql2 = new DataSet();
						tempAdapter_2.Fill(Rrsql2);
						if (Rrsql2.Tables[0].Rows.Count == 1)
						{
                            string auxVar = Convert.ToString(Rrsql2.Tables[0].Rows[0]["gcodipos"]).Trim();
                            //Listado.Formulas["{@Juzgado1}"]
                            Listado.Formulas["{@Juzgado1}"] = "\"" + Convert.ToString(Rrsql2.Tables[0].Rows[0]["dnomjuzg"]).Trim() + "\"";
                            //Listado.Formulas["{@NJuzgado1}"]
                            Listado.Formulas["{@NJuzgado1}"] = "\"" + Convert.ToString(Rrsql2.Tables[0].Rows[0]["dnomnum"]).Trim() + "\"";
                            //Listado.Formulas["{@DirJuzgado1}"]
                            Listado.Formulas["{@DirJuzgado1}"] = "\"" + Convert.ToString(Rrsql2.Tables[0].Rows[0]["ddirjuzg"]).Trim() + "\"";
                            
							bool tempBool = false;
							stPoblacion = (((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))) ? Convert.ToString(Rrsql2.Tables[0].Rows[0]["gcodipos"]).Trim() : "";
							stPoblacion = stPoblacion + " " + Convert.ToString(Rrsql2.Tables[0].Rows[0]["dpoblaci"]).Trim();
                            //Listado.Formulas["{@PobJuzgado1}"]
                            Listado.Formulas["{@PobJuzgado1}"] = "\"" + stPoblacion + "\"";

						}
						Rrsql2.Close();
					}
					else
					{
                        //Listado.Formulas["{@Juzgado1}"]
                        Listado.Formulas["{@Juzgado1}"] = "\"" + NOMBREJUZGADODECANO + "\"";
                        //Listado.Formulas["{@NJuzgado1}"]
                        Listado.Formulas["{@NJuzgado1}"] = "\"" + NUMEROJUZGADODECANO + "\"";
                        //Listado.Formulas["{@DirJuzgado1}"]
                        Listado.Formulas["{@DirJuzgado1}"] = "\"" + DIRECCIONJUZGADODECANO + "\"";
                        
					}


                    //Listado.Formulas["{@Nombre_Director}"]
                    Listado.Formulas["{@Nombre_Director}"] = "\"" + stNombre_Director + "\"";
                    //Listado.Formulas["{@NHospit}"]
                    Listado.Formulas["{@NHospit}"] = "\"" + stNHospit + "\"";
                    //Listado.Formulas["{@DDireccion}"]
                    Listado.Formulas["{@DDireccion}"] = "\"" + stDDireccion + "\"";
                    
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnombpac"]))
					{
						stPaciente = Convert.ToString(RrSql.Tables[0].Rows[0]["dnombpac"]).Trim();
					}
					
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape1pac"]))
					{
						stPaciente = stPaciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape1pac"]).Trim();
					}
					
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dape2pac"]))
					{
						stPaciente = stPaciente + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["dape2pac"]).Trim();
					}

                    //Listado.Formulas["{@Paciente}"]
                    Listado.Formulas["{@Paciente}"] = "\"" + stPaciente + "\"";
                    //Listado.Formulas["{@DNI}"]
                    Listado.Formulas["{@DNI}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["ndninifp"]).Trim() + "\"";
                    //Listado.Formulas["{@NExpediente1}"]
                    Listado.Formulas["{@NExpediente1}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["nnumexp1"]).Trim() + "\"";
                    //Listado.Formulas["{@FECHA_LLEGADA}"]
                    Listado.Formulas["{@FECHA_LLEGADA}"] = "\"" + Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy") + "\"";
                    //Listado.Formulas["{@CAUSA_INMEDIATA}"]
                    Listado.Formulas["{@CAUSA_INMEDIATA}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["ocinmexi"]).Trim() + "\"";
                    //Listado.Formulas["{@CAUSA_PRINCIPAL}"]
                    Listado.Formulas["{@CAUSA_PRINCIPAL}"] = "\"" + Convert.ToString(RrSql.Tables[0].Rows[0]["ocpriexi"]).Trim() + "\"";
                    //Listado.Formulas["{@Poblacion_juzgado}"]
                    Listado.Formulas["{@Poblacion_juzgado}"] = "\"" + stPoblacion_Juzgado + "\"";
                    //Listado.Formulas["{@FECHAFALLECIMIENTO}"]
                    if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["FFALLECI"]))
                    {
                        Listado.Formulas["{@FECHAFALLECIMIENTO}"] = "\"" + Convert.ToDateTime(RrSql.Tables[0].Rows[0]["FFALLECI"]).ToString("dd/MM/yyyy") + "\"";
                    } else
                    {
                        Listado.Formulas["{@FECHAFALLECIMIENTO}"] = "\"";
                    }
                    
					//Listado.Formulas(15) = "HORAFALLECIMIENTO= """ & Format$(Rrsql("FFALLECI"), "HH:NN") & """"
					//Dim fecha_alta As String
					//fecha_alta = Format(RRSQL("faltplan"), "dd") & " de " & Format(RRSQL("faltplan"), "mmmm") & " de " & Format(RRSQL("faltplan"), "yyyy")
					//listado.Formulas(13) = "Fecha_Alta= """ & fecha_alta & """"

					
					Listado.WindowTitle = "Notificación al Juzgado del fallecimiento del incapacitado";
					Listado.WindowState = (Crystal.WindowStateConstants)2;


                    if (!mNotificaciones.VstrCrystal.VfCabecera)
					{
						mNotificaciones.proCabCrystal();
					}
                    //Listado.Formulas["{@FORM1}"]
                    Listado.Formulas["{@FORM1}"] = "\"" + mNotificaciones.VstrCrystal.VstGrupoHo + "\"";
                    //Listado.Formulas["{@FORM2}"]
                    Listado.Formulas["{@FORM2}"] = "\"" + mNotificaciones.VstrCrystal.VstNombreHo + "\"";
                    //Listado.Formulas["{@FORM3}"]
                    Listado.Formulas["{@FORM3}"] = "\"" + mNotificaciones.VstrCrystal.VstDireccHo + "\"";
                    //Listado.Formulas["{@LogoIDC}"]
                    Listado.Formulas["{@LogoIDC}"] = "\"" + Serrores.ExisteLogo(mNotificaciones.RcNotif) + "\"";
                    //Listado.Formulas["{@DNICEDULA}"]
                    Listado.Formulas["{@DNICEDULA}"] = "\"" + DNICEDULA + "\"";

					Listado.SubreportToChange = "LogotipoIDC";
					Listado.SQLQuery = "SELECT slogodoc FROM SLOGODOC";
                    Listado.Connect = "" + NOMBRE_DSN_SQLSERVER + ";" + UsuarioCrystal + ";" + ContraseñaCrystal + "" + "";


					Listado.Destination = (Crystal.DestinationConstants)1;
					Listado.WindowShowPrintSetupBtn = true;
					Listado.Action = 1;
					Listado.PrinterStopPage = Listado.PageCount;
					Listado.Reset();
                    

				}
				RrSql.Close();
			}
			catch(Exception ex)
			{
				FormularioQueLlama.MousePointer = 99;
				Serrores.AnalizaError("Notificaciones", System.IO.Path.GetDirectoryName(Application.ExecutablePath), UsuarioCrystal, "ProImprimirFallecimientoIncapacitado", ex);
                Listado.Reset();
			}
		}
		//-------

		// rellena los datos comunes para todos los informes
		private void proObtenerDatosComunes()
		{
			string sql = "select valfanu1, valfanu2,valfanu3  from sconsglo where gconsglo = 'JUZGDECA'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				NOMBREJUZGADODECANO = "" + Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper(); //djuzgado
				NUMEROJUZGADODECANO = ""; //& Trim(UCase(Rrsql("valfanu2"))) 'dnomnum
				DIRECCIONJUZGADODECANO = "" + Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper(); //valfanu3
			}
			sql = "select valfanu1  from sconsglo where gconsglo = 'NDIREJUD'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
			RrSql = new DataSet();
			tempAdapter_2.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				NOMBREDIRECTOR = "" + Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
			}
			sql = "select valfanu1  from sconsglo where gconsglo = 'nombreho'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
			RrSql = new DataSet();
			tempAdapter_3.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				NOMBREHOSPITAL = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim();
			}
			sql = "select valfanu1,valfanu2 from sconsglo where gconsglo = 'direccho'";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
			RrSql = new DataSet();
			tempAdapter_4.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				DIRECCIONHOSPITAL = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim() + " " + Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu2"]).Trim();
			}
			RrSql.Close();

			//oscar 16/03/04
			sql = "select valfanu1 from sconsglo where gconsglo= 'idenpers' ";
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
			RrSql = new DataSet();
			tempAdapter_5.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				DNICEDULA = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
			}
			RrSql.Close();

			sql = "select dnomprov " + 
			      " From sconsglo , dprovinc Where sconsglo.gconsglo = 'CODPROVI' and " + 
			      " substring(valfanu1,1,2)=gprovinc";
			SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
			RrSql = new DataSet();
			tempAdapter_6.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				PROVINCIA = Convert.ToString(RrSql.Tables[0].Rows[0]["dnomprov"]).Trim();
			}
			RrSql.Close();

			//24/03/04
			sql = "select valfanu1 from sconsglo where gconsglo= 'LITPERSO' ";
			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql, mNotificaciones.RcNotif);
			RrSql = new DataSet();
			tempAdapter_7.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				LITPERSO = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
			}
			RrSql.Close();
			//-----
		}
	}
}