using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace BuscarCie
{
	public partial class frmBusGruTerapeutico
		: Telerik.WinControls.UI.RadForm
    {


		DataSet rsPrestaciones = null;
		dynamic frmFormularioDevolverDatos = null; //a qui�n devolver los datos (formulario que
		//   contenga el m�todo RecogerPrestacion).
		SqlConnection cnPrestaciones = null; //conexi�n sobre la que hacer consultas.
		string stCondicionFiltroLlamada = String.Empty; //se aplica en el Where que muestra prestaciones.

		bool bPrincipioActivo = false;
		//

		public frmBusGruTerapeutico()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		public void EstablecerVariables(Form frmFormularioRetorno, SqlConnection cnConexionLlamante, string stCondicionFiltro, bool buscarPrincipioActivo = false)
		{

			frmFormularioDevolverDatos = frmFormularioRetorno;
			cnPrestaciones = cnConexionLlamante;
			stCondicionFiltroLlamada = stCondicionFiltro;
			bPrincipioActivo = buscarPrincipioActivo;
		}

		private void frmBusGruTerapeutico_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				txtContenga.Focus();
			}
		}

		private void frmBusGruTerapeutico_Load(Object eventSender, EventArgs eventArgs)
		{
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
            //    Me.HelpContextID = 163104
			sprPrestaciones.MaxRows = 0;
			this.Text = "B�squeda de Grupo Terap�utico.";
		}
        private void sprPrestaciones_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
        {
			int Col = eventArgs.ColumnIndex +1;
			int Row = eventArgs.RowIndex + 1;

			if (Row >= 0)
			{
				cbAceptar.Enabled = true;
			}
		}

		private void txtContenga_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			txtEmpiecepor.Text = "";
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void txtEmpiecepor_TextChanged(Object eventSender, EventArgs eventArgs)
		{

			string stComp = String.Empty;
			string stBusqueda = String.Empty;
			string stSql = String.Empty;
			string fecha = String.Empty;
			string stCriterio = String.Empty;

			string stCondicionesSql = String.Empty; //se ir� construyendo de derecha a izquierda.
			if (txtEmpiecepor.Text != "")
			{
				if (rbDesc.IsChecked)
				{
					stCriterio = "dgrutera";
				}
				else
				{
					stCriterio = "ggrutera";
				}
				txtContenga.Text = "";
				stBusqueda = txtEmpiecepor.Text.Trim();
				stComp = stCriterio + " like '" + stBusqueda + "%'";

				// Construir la sentencia Sql de selecci�n de registros:
				//

				if (stCondicionFiltroLlamada.Length > 0)
				{
					stCondicionesSql = stCondicionFiltroLlamada + " And (" + stComp + ")";
				}
				else
				{
					stCondicionesSql = "(" + stComp + ")";
				}

				stSql = "Select ggrutera as ggrutera, dgrutera as dgrutera, getDate() as finivali from DGRUTERAVT Where " + 
				        stCondicionesSql + " Order By " + stCriterio;

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnPrestaciones);
				rsPrestaciones = new DataSet();
				tempAdapter.Fill(rsPrestaciones);

				if (rsPrestaciones.Tables[0].Rows.Count != 0)
				{
					sprPrestaciones.MaxRows = rsPrestaciones.Tables[0].Rows.Count;
					carga_grid();
				}
				else
				{
					sprPrestaciones.MaxRows = 0;
					txtEmpiecepor.SelectionStart = 0;
					txtEmpiecepor.SelectionLength = Strings.Len(txtEmpiecepor.Text);
					txtEmpiecepor.Focus();
					return;
				}
			}
			else
			{
				sprPrestaciones.MaxRows = 0;
			}
		}

		private void txtEmpiecepor_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			txtContenga.Text = "";
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void cbBuscarCadena_Click(Object eventSender, EventArgs eventArgs)
		{

			string fecha = String.Empty;

			txtEmpiecepor.Text = "";

			StringBuilder busca = new StringBuilder();
			string cadena = txtContenga.Text.Trim();

			int iCorte = (cadena.IndexOf(',') + 1);
			if (iCorte == 0)
			{
				busca = new StringBuilder("dgrutera like '%" + cadena + "%'");
			}
			else
			{
				while (iCorte != 0)
				{

					busca = new StringBuilder("dgrutera like '%" + cadena.Substring(0, Math.Min(iCorte - 1, cadena.Length)).Trim() + "%'");

					cadena = cadena.Substring(iCorte, Math.Min(cadena.Length, cadena.Length - iCorte)).Trim();
					iCorte = (cadena.IndexOf(',') + 1);
					if (iCorte != 0)
					{
						if (rbY.IsChecked)
						{
							busca.Append(" and ");
						}
						else
						{
							busca.Append(" or ");
						}
					}
				}
                
				if (rbY.IsChecked)
				{
					busca.Append(" and dgrutera like '%" + cadena + "%'");
				}
				else
				{
					busca.Append(" or dgrutera like '%" + cadena + "%'");
				}
			}

			// Construir la sentencia Sql de selecci�n de registros:
			//
			string stCondicionesSql = String.Empty; //se ir� construyendo de derecha a izquierda.

			if (stCondicionFiltroLlamada.Length > 0)
			{
				stCondicionesSql = stCondicionFiltroLlamada + " And (" + busca.ToString() + ")";
			}
			else
			{
				stCondicionesSql = "(" + busca.ToString() + ")";
			}

			string stSql = "select ggrutera as ggrutera, dgrutera as dgrutera, getDate() as finivali FROM DGRUTERAVT WHERE " + 
			               stCondicionesSql + " order by dgrutera";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnPrestaciones);
			rsPrestaciones = new DataSet();
			tempAdapter.Fill(rsPrestaciones);
            
			if (rsPrestaciones.Tables[0].Rows.Count != 0)
			{
				sprPrestaciones.MaxRows = rsPrestaciones.Tables[0].Rows.Count;
				carga_grid();
			}
			else
			{
				sprPrestaciones.MaxRows = 0;
				txtContenga.SelectionStart = 0;
				txtContenga.SelectionLength = Strings.Len(txtContenga.Text);
				txtContenga.Focus();
				return;
			}
		}

		private void carga_grid()
		{
			sprPrestaciones.Row = 0;
		
			foreach (DataRow iteration_row in rsPrestaciones.Tables[0].Rows)
			{

				sprPrestaciones.Row++;
				sprPrestaciones.Col = 1;
				sprPrestaciones.Text = Convert.ToString(iteration_row["ggrutera"]).Trim();

				sprPrestaciones.Col = 2;
				sprPrestaciones.Text = Convert.ToString(iteration_row["dgrutera"]).Trim();

				sprPrestaciones.Col = 3;
				sprPrestaciones.Text = Convert.ToDateTime(iteration_row["finivali"]).ToString("MM/dd/yyyy HH:mm:ss");
			}
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{


			if (sprPrestaciones.ActiveRowIndex <= 0)
			{
				cbAceptar.Enabled = false;
				return;
			}

			sprPrestaciones.Row = sprPrestaciones.ActiveRowIndex;

			sprPrestaciones.Col = 1;
			string stCodigoPrestacion = sprPrestaciones.Text.Trim();
			sprPrestaciones.Col = 2;
			string stDescripcionPrestacion = sprPrestaciones.Text.Trim();

			sprPrestaciones.Col = 3;
			string stFechaPrestacion = sprPrestaciones.Text.Trim(); //va en formato "MM/DD/YYYY HH:NN:SS"

			frmFormularioDevolverDatos.Recoger_datosGrupoTerapeutico(stCodigoPrestacion, stDescripcionPrestacion);

			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			frmFormularioDevolverDatos.Recoger_datosGrupoTerapeutico("", "");
			this.Close();
		}

		private void frmBusGruTerapeutico_FormClosing(Object eventSender, FormClosingEventArgs eventArgs)
		{
			int Cancel = (eventArgs.Cancel) ? 1 : 0;
			int UnloadMode = (int) eventArgs.CloseReason;

			if (UnloadMode == ((int)CloseReason.UserClosing))
			{ //el usuario pinch� el bot�n de cerrar ventana.
				frmFormularioDevolverDatos.Recoger_datosGrupoTerapeutico("", "");
			}

			eventArgs.Cancel = Cancel != 0;
		}

		private void frmBusGruTerapeutico_Closed(Object eventSender, EventArgs eventArgs)
		{
			cnPrestaciones = null;
		}
	}
}