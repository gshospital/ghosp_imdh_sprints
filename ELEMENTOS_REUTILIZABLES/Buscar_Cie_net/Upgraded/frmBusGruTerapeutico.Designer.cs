using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BuscarCie
{
	partial class frmBusGruTerapeutico
	{

		#region "Upgrade Support "
		private static frmBusGruTerapeutico m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmBusGruTerapeutico DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmBusGruTerapeutico();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprPrestaciones", "txtContenga", "rbY", "rbO", "FrmbuscarContengaCriterio", "cbBuscarCadena", "LbBusquedaContenga", "FrmbuscarContenga", "cbCancelar", "cbAceptar", "txtEmpiecepor", "rbCod", "rbDesc", "FrmbuscarInicialesCriterio", "LbBusquedaIniciales", "FrmbuscarIniciales", "commandButtonHelper1", "sprPrestaciones_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprPrestaciones;
		public Telerik.WinControls.UI.RadTextBoxControl txtContenga;
		public Telerik.WinControls.UI.RadRadioButton rbY;
		public Telerik.WinControls.UI.RadRadioButton rbO;
		public Telerik.WinControls.UI.RadGroupBox FrmbuscarContengaCriterio;
		public Telerik.WinControls.UI.RadButton cbBuscarCadena;
		public Telerik.WinControls.UI.RadLabel LbBusquedaContenga;
		public Telerik.WinControls.UI.RadGroupBox FrmbuscarContenga;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadTextBoxControl txtEmpiecepor;
		public Telerik.WinControls.UI.RadRadioButton rbCod;
		public Telerik.WinControls.UI.RadRadioButton rbDesc;
		public Telerik.WinControls.UI.RadGroupBox FrmbuscarInicialesCriterio;
		public Telerik.WinControls.UI.RadLabel LbBusquedaIniciales;
		public Telerik.WinControls.UI.RadGroupBox FrmbuscarIniciales;
		
        //private FarPoint.Win.Spread.SheetView sprPrestaciones_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBusGruTerapeutico));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.sprPrestaciones = new UpgradeHelpers.Spread.FpSpread();
			this.FrmbuscarContenga = new Telerik.WinControls.UI.RadGroupBox();
			this.txtContenga = new Telerik.WinControls.UI.RadTextBoxControl();
			this.FrmbuscarContengaCriterio = new Telerik.WinControls.UI.RadGroupBox();
			this.rbY = new Telerik.WinControls.UI.RadRadioButton();
			this.rbO = new Telerik.WinControls.UI.RadRadioButton();
			this.cbBuscarCadena = new Telerik.WinControls.UI.RadButton();
			this.LbBusquedaContenga = new Telerik.WinControls.UI.RadLabel();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.FrmbuscarIniciales = new Telerik.WinControls.UI.RadGroupBox();
			this.txtEmpiecepor = new Telerik.WinControls.UI.RadTextBoxControl();
			this.FrmbuscarInicialesCriterio = new Telerik.WinControls.UI.RadGroupBox();
			this.rbCod = new Telerik.WinControls.UI.RadRadioButton();
			this.rbDesc = new Telerik.WinControls.UI.RadRadioButton();
			this.LbBusquedaIniciales = new Telerik.WinControls.UI.RadLabel();
			this.FrmbuscarContenga.SuspendLayout();
			this.FrmbuscarContengaCriterio.SuspendLayout();
			this.FrmbuscarIniciales.SuspendLayout();
			this.FrmbuscarInicialesCriterio.SuspendLayout();
			this.SuspendLayout();
            // 
            // sprPrestaciones
            // 
            gridViewTextBoxColumn1.HeaderText = "C�digo";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 40;
            gridViewTextBoxColumn2.HeaderText = "Descripci�n";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 250;
            gridViewTextBoxColumn3.HeaderText = "Fecha";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 40;
            gridViewTextBoxColumn3.IsVisible = false;
            this.sprPrestaciones.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1, gridViewTextBoxColumn2, gridViewTextBoxColumn3});

            this.sprPrestaciones.ReadOnly = true;
            this.sprPrestaciones.EnableSorting = false;
            this.sprPrestaciones.Location = new System.Drawing.Point(6, 84);
            this.sprPrestaciones.Name = "sprPrestaciones";
            this.sprPrestaciones.Size = new System.Drawing.Size(697, 243);
            this.sprPrestaciones.TabIndex = 0;
            this.sprPrestaciones.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(sprPrestaciones_CellClick);
            // 
            // FrmbuscarContenga
            // 
            this.FrmbuscarContenga.Controls.Add(this.txtContenga);
			this.FrmbuscarContenga.Controls.Add(this.FrmbuscarContengaCriterio);
			this.FrmbuscarContenga.Controls.Add(this.cbBuscarCadena);
			this.FrmbuscarContenga.Controls.Add(this.LbBusquedaContenga);
			this.FrmbuscarContenga.Enabled = true;
			this.FrmbuscarContenga.Location = new System.Drawing.Point(6, 6);
			this.FrmbuscarContenga.Name = "FrmbuscarContenga";
			this.FrmbuscarContenga.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmbuscarContenga.Size = new System.Drawing.Size(398, 74);
			this.FrmbuscarContenga.TabIndex = 9;
			this.FrmbuscarContenga.Text = "B�squeda por contenido";
			this.FrmbuscarContenga.Visible = true;
			// 
			// txtContenga
			// 
			this.txtContenga.AcceptsReturn = true;
			this.txtContenga.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtContenga.Location = new System.Drawing.Point(5, 32);
			this.txtContenga.MaxLength = 0;
			this.txtContenga.Name = "txtContenga";
			this.txtContenga.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtContenga.Size = new System.Drawing.Size(210, 22);
			this.txtContenga.TabIndex = 14;
			this.txtContenga.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContenga_KeyPress);
			// 
			// FrmbuscarContengaCriterio
			// 
			this.FrmbuscarContengaCriterio.Controls.Add(this.rbY);
			this.FrmbuscarContengaCriterio.Controls.Add(this.rbO);
			this.FrmbuscarContengaCriterio.Enabled = true;
			this.FrmbuscarContengaCriterio.Location = new System.Drawing.Point(261, 17);
			this.FrmbuscarContengaCriterio.Name = "FrmbuscarContengaCriterio";
			this.FrmbuscarContengaCriterio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmbuscarContengaCriterio.Size = new System.Drawing.Size(130, 49);
			this.FrmbuscarContengaCriterio.TabIndex = 11;
			this.FrmbuscarContengaCriterio.Text = "Criterio de selecci�n";
			this.FrmbuscarContengaCriterio.Visible = true;
			// 
			// rbY
			// 
			this.rbY.CausesValidation = true;
			this.rbY.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbY.IsChecked = false;
			this.rbY.Cursor = System.Windows.Forms.Cursors.Default;
			this.rbY.Enabled = true;
			this.rbY.Location = new System.Drawing.Point(6, 20);
			this.rbY.Name = "rbY";
			this.rbY.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.rbY.Size = new System.Drawing.Size(47, 14);
			this.rbY.TabIndex = 13;
			this.rbY.TabStop = true;
			this.rbY.Text = "A y B";
			this.rbY.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbY.Visible = true;
			// 
			// rbO
			// 
			this.rbO.CausesValidation = true;
			this.rbO.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbO.IsChecked = false;
			this.rbO.Cursor = System.Windows.Forms.Cursors.Default;
			this.rbO.Enabled = true;
			this.rbO.Location = new System.Drawing.Point(58, 20);
			this.rbO.Name = "rbO";
			this.rbO.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.rbO.Size = new System.Drawing.Size(47, 14);
			this.rbO.TabIndex = 12;
			this.rbO.TabStop = true;
			this.rbO.Text = "A � B";
			this.rbO.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbO.Visible = true;
			// 
			// cbBuscarCadena
			// 
			this.cbBuscarCadena.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbBuscarCadena.Image = (System.Drawing.Image) resources.GetObject("cbBuscarCadena.Image");
			this.cbBuscarCadena.Location = new System.Drawing.Point(220, 29);
			this.cbBuscarCadena.Name = "cbBuscarCadena";
			this.cbBuscarCadena.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbBuscarCadena.Size = new System.Drawing.Size(32, 29);
			this.cbBuscarCadena.TabIndex = 10;
            this.cbBuscarCadena.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbBuscarCadena.Click += new System.EventHandler(this.cbBuscarCadena_Click);
			// 
			// LbBusquedaContenga
			// 
			this.LbBusquedaContenga.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbBusquedaContenga.Location = new System.Drawing.Point(5, 16);
			this.LbBusquedaContenga.Name = "LbBusquedaContenga";
			this.LbBusquedaContenga.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbBusquedaContenga.Size = new System.Drawing.Size(58, 13);
			this.LbBusquedaContenga.TabIndex = 15;
			this.LbBusquedaContenga.Text = "Contenga :";
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(622, 334);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 29);
			this.cbCancelar.TabIndex = 8;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Location = new System.Drawing.Point(534, 334);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 7;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// FrmbuscarIniciales
			// 
			this.FrmbuscarIniciales.Controls.Add(this.txtEmpiecepor);
			this.FrmbuscarIniciales.Controls.Add(this.FrmbuscarInicialesCriterio);
			this.FrmbuscarIniciales.Controls.Add(this.LbBusquedaIniciales);
			this.FrmbuscarIniciales.Enabled = true;
			this.FrmbuscarIniciales.Location = new System.Drawing.Point(407, 6);
			this.FrmbuscarIniciales.Name = "FrmbuscarIniciales";
			this.FrmbuscarIniciales.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmbuscarIniciales.Size = new System.Drawing.Size(296, 74);
			this.FrmbuscarIniciales.TabIndex = 1;
			this.FrmbuscarIniciales.Text = "B�squeda por caracteres iniciales";
			this.FrmbuscarIniciales.Visible = true;
			// 
			// txtEmpiecepor
			// 
			this.txtEmpiecepor.AcceptsReturn = true;
			this.txtEmpiecepor.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtEmpiecepor.Location = new System.Drawing.Point(7, 32);
			this.txtEmpiecepor.MaxLength = 0;
			this.txtEmpiecepor.Name = "txtEmpiecepor";
			this.txtEmpiecepor.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtEmpiecepor.Size = new System.Drawing.Size(165, 22);
			this.txtEmpiecepor.TabIndex = 5;
			this.txtEmpiecepor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmpiecepor_KeyPress);
			this.txtEmpiecepor.TextChanged += new System.EventHandler(this.txtEmpiecepor_TextChanged);
			// 
			// FrmbuscarInicialesCriterio
			// 
			this.FrmbuscarInicialesCriterio.Controls.Add(this.rbCod);
			this.FrmbuscarInicialesCriterio.Controls.Add(this.rbDesc);
			this.FrmbuscarInicialesCriterio.Enabled = true;
			this.FrmbuscarInicialesCriterio.Location = new System.Drawing.Point(177, 17);
			this.FrmbuscarInicialesCriterio.Name = "FrmbuscarInicialesCriterio";
			this.FrmbuscarInicialesCriterio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmbuscarInicialesCriterio.Size = new System.Drawing.Size(116, 49);
			this.FrmbuscarInicialesCriterio.TabIndex = 2;
			this.FrmbuscarInicialesCriterio.Text = "Criterio de selecci�n";
			this.FrmbuscarInicialesCriterio.Visible = true;
			// 
			// rbCod
			// 
			this.rbCod.CausesValidation = true;
			this.rbCod.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbCod.IsChecked = false;
			this.rbCod.Cursor = System.Windows.Forms.Cursors.Default;
			this.rbCod.Enabled = true;
			this.rbCod.Location = new System.Drawing.Point(6, 20);
			this.rbCod.Name = "rbCod";
			this.rbCod.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.rbCod.Size = new System.Drawing.Size(47, 14);
			this.rbCod.TabIndex = 4;
			this.rbCod.TabStop = true;
			this.rbCod.Text = "C�d.";
			this.rbCod.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbCod.Visible = true;
			// 
			// rbDesc
			// 
			this.rbDesc.CausesValidation = true;
			this.rbDesc.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbDesc.IsChecked = true;
			this.rbDesc.Cursor = System.Windows.Forms.Cursors.Default;
			this.rbDesc.Enabled = true;
			this.rbDesc.Location = new System.Drawing.Point(58, 20);
			this.rbDesc.Name = "rbDesc";
			this.rbDesc.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.rbDesc.Size = new System.Drawing.Size(48, 14);
			this.rbDesc.TabIndex = 3;
			this.rbDesc.TabStop = true;
			this.rbDesc.Text = "Desc.";
			this.rbDesc.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbDesc.Visible = true;
			// 
			// LbBusquedaIniciales
			// 
			this.LbBusquedaIniciales.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbBusquedaIniciales.Location = new System.Drawing.Point(7, 16);
			this.LbBusquedaIniciales.Name = "LbBusquedaIniciales";
			this.LbBusquedaIniciales.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbBusquedaIniciales.Size = new System.Drawing.Size(70, 16);
			this.LbBusquedaIniciales.TabIndex = 6;
			this.LbBusquedaIniciales.Text = "Comienzo por:";
			// 
			// frmBusGruTerapeutico
			// 
			this.AcceptButton = this.cbBuscarCadena;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(711, 370);
			this.Controls.Add(this.sprPrestaciones);
			this.Controls.Add(this.FrmbuscarContenga);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.FrmbuscarIniciales);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location = new System.Drawing.Point(4, 23);
			this.MaximizeBox = true;
			this.MinimizeBox = true;
			this.Name = "frmBusGruTerapeutico";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "B�squeda Grupo Terap�utico.";
			this.ToolTipMain.SetToolTip(this.cbBuscarCadena, "Llevar a cabo la b�squeda en base de datos");
			this.Activated += new System.EventHandler(this.frmBusGruTerapeutico_Activated);
			this.Closed += new System.EventHandler(this.frmBusGruTerapeutico_Closed);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBusGruTerapeutico_FormClosing);
			this.Load += new System.EventHandler(this.frmBusGruTerapeutico_Load);
			this.FrmbuscarContenga.ResumeLayout(false);
			this.FrmbuscarContengaCriterio.ResumeLayout(false);
			this.FrmbuscarIniciales.ResumeLayout(false);
			this.FrmbuscarInicialesCriterio.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}