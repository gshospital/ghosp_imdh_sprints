using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace BuscarCie
{
	public class clsBuscarCie
	{



		public void Ejecutar(Form frmFormularioDevolverDatos, SqlConnection cnConexion, ref object stCondicionFiltro_optional, bool buscarPrincipioActivo, string nCaso)
		{
			string stCondicionFiltro = (stCondicionFiltro_optional == Type.Missing) ? String.Empty : stCondicionFiltro_optional as string;
			try
			{

				//frmFormularioDevolverDatos debe contener un m�todo RecogerPrestacion; este
				//     m�todo ser� ejecutado cuando se descargue el formulario de esta Dll.
				//cnConexion es la conexi�n que se utilizar� para consultar la tabla o vista
				//     (adem�s de otras tablas que se pueden indicar en subselects
				//     de stCondicionFiltro).
				//stCondicionFiltro tiene el formato de una cl�usula Where de Sql (pero
				//     NO incluir� la palabra 'Where').

				// se da a la condici�n de filtrado el formato apropiado para un futuro Where:
				//
				//UPGRADE_WARNING: (2080) IsEmpty was upgraded to a comparison and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				if (stCondicionFiltro_optional == Type.Missing)
				{
					stCondicionFiltro = "";
				}
				else if (!String.IsNullOrEmpty(stCondicionFiltro))
				{ 
					stCondicionFiltro = "(" + stCondicionFiltro + ")";
				}
				// se inicializa y lanza la ejecuci�n de una pantalla de b�squeda:
				//

				frmBuscarCie.DefInstance.EstablecerVariables(frmFormularioDevolverDatos, cnConexion, stCondicionFiltro, buscarPrincipioActivo, nCaso);
				frmBuscarCie.DefInstance.ShowDialog();
			}
			finally
			{
				stCondicionFiltro_optional = stCondicionFiltro;
			}

		}

		public void Ejecutar(Form frmFormularioDevolverDatos, SqlConnection cnConexion, ref object stCondicionFiltro_optional, bool buscarPrincipioActivo)
		{
			Ejecutar(frmFormularioDevolverDatos, cnConexion, ref stCondicionFiltro_optional, buscarPrincipioActivo, String.Empty);
		}

		public void Ejecutar(Form frmFormularioDevolverDatos, SqlConnection cnConexion, ref object stCondicionFiltro_optional)
		{
			Ejecutar(frmFormularioDevolverDatos, cnConexion, ref stCondicionFiltro_optional, false, String.Empty);
		}

		public void Ejecutar(Form frmFormularioDevolverDatos, SqlConnection cnConexion)
		{
			object tempRefParam = Type.Missing;
			Ejecutar(frmFormularioDevolverDatos, cnConexion, ref tempRefParam, false, String.Empty);
		}

		public void grupoTerapeutico(Form frmFormularioDevolverDatos, SqlConnection cnConexion, ref object stCondicionFiltro_optional, bool buscarPrincipioActivo)
		{
			string stCondicionFiltro = (stCondicionFiltro_optional == Type.Missing) ? String.Empty : stCondicionFiltro_optional as string;
			try
			{

				//frmFormularioDevolverDatos debe contener un m�todo RecogerPrestacion; este
				//     m�todo ser� ejecutado cuando se descargue el formulario de esta Dll.
				//cnConexion es la conexi�n que se utilizar� para consultar la tabla o vista
				//     (adem�s de otras tablas que se pueden indicar en subselects
				//     de stCondicionFiltro).
				//stCondicionFiltro tiene el formato de una cl�usula Where de Sql (pero
				//     NO incluir� la palabra 'Where').

				// se da a la condici�n de filtrado el formato apropiado para un futuro Where:
				//
				//UPGRADE_WARNING: (2080) IsEmpty was upgraded to a comparison and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				if (stCondicionFiltro_optional == Type.Missing)
				{
					stCondicionFiltro = "";
				}
				else if (!String.IsNullOrEmpty(stCondicionFiltro))
				{ 
					stCondicionFiltro = "(" + stCondicionFiltro + ")";
				}
				// se inicializa y lanza la ejecuci�n de una pantalla de b�squeda:
				//

				frmBusGruTerapeutico.DefInstance.EstablecerVariables(frmFormularioDevolverDatos, cnConexion, stCondicionFiltro, buscarPrincipioActivo);
				frmBusGruTerapeutico.DefInstance.ShowDialog();
			}
			finally
			{
				stCondicionFiltro_optional = stCondicionFiltro;
			}

		}

		public void grupoTerapeutico(Form frmFormularioDevolverDatos, SqlConnection cnConexion, ref object stCondicionFiltro_optional)
		{
			grupoTerapeutico(frmFormularioDevolverDatos, cnConexion, ref stCondicionFiltro_optional, false);
		}

		public void grupoTerapeutico(Form frmFormularioDevolverDatos, SqlConnection cnConexion)
		{
			object tempRefParam2 = Type.Missing;
			grupoTerapeutico(frmFormularioDevolverDatos, cnConexion, ref tempRefParam2, false);
		}
	}
}