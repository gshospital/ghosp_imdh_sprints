using System;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace LLamarFacturacion
{
	public partial class OperacionPrivados
		: System.Windows.Forms.Form
	{


		clsLLamadaFactur ClaseFactura = null;
		public OperacionPrivados()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}




		public void ProCargarFormulario(clsLLamadaFactur Clase)
		{
			ClaseFactura = Clase;
			this.ShowDialog();
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			ClaseFactura.OperacionPrivado = "FAPR5";
			ClaseFactura.EsPrivadoGenerico = !rbPrivadoObra.Checked;
			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			ClaseFactura.OperacionPrivado = "";
			this.Close();
		}

		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void OperacionPrivados_Load(Object eventSender, EventArgs eventArgs)
		{
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
		}

		private bool isInitializingComponent;
		private void rbPrivado_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				cbAceptar.Enabled = ((((rbPrivado.Checked) ? -1 : 0) | ((rbPrivadoObra.Checked) ? -1 : 0)) != 0);
			}
		}

		private void rbPrivadoObra_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadioButton) eventSender).Checked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				cbAceptar.Enabled = ((((rbPrivado.Checked) ? -1 : 0) | ((rbPrivadoObra.Checked) ? -1 : 0)) != 0);
			}
		}
		private void OperacionPrivados_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}