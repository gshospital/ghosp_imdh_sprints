using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace LLamarFacturacion
{
	public class clsLLamadaFactur
	{


		SqlConnection NombreConexion = null;
		FixedLengthString NombreModulo = new FixedLengthString(1);
		string OperacionDevuelta = String.Empty;
		bool Beneficiencia = false;



		public bool EsPrivadoGenerico
		{
			get
			{
				return !Beneficiencia;
			}
			set
			{
				Beneficiencia = !value;
			}
		}




		public SqlConnection Conexion
		{
			get
			{
				return NombreConexion;
			}
			set
			{
				NombreConexion = value;
			}
		}



		public string Modulo
		{
			get
			{
				return NombreModulo.Value;
			}
			set
			{
				NombreModulo.Value = value;
			}
		}



		public string OperacionPrivado
		{
			get
			{
				return OperacionDevuelta;
			}
			set
			{
				OperacionDevuelta = value;
			}
		}




		public string DevolverOperacionPrivados(string OperacionNormalPrivado, SqlConnection Conexion, string LetraModulo, clsLLamadaFactur ClaseOrigen)
		{


			ClaseOrigen.Conexion = Conexion;
			ClaseOrigen.Modulo = LetraModulo;
			string sqlConstante = "select * from SCONSGLO where gconsglo = 'FACTBENE'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlConstante, ClaseOrigen.Conexion);
			DataSet RrConstante = new DataSet();
			tempAdapter.Fill(RrConstante);
			if (RrConstante.Tables[0].Rows.Count == 1)
			{
				ClaseOrigen.OperacionPrivado = OperacionNormalPrivado;
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(RrConstante.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					OperacionPrivados.DefInstance.ProCargarFormulario(ClaseOrigen);

				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrConstante.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrConstante.Close();
			return String.Empty;
		}
	}
}