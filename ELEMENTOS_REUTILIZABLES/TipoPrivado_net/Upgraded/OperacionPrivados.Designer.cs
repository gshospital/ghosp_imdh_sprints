using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace LLamarFacturacion
{
	partial class OperacionPrivados
	{

		#region "Upgrade Support "
		private static OperacionPrivados m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static OperacionPrivados DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new OperacionPrivados();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "rbPrivadoObra", "rbPrivado", "Frame1", "cbAceptar", "cbCancelar"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public System.Windows.Forms.RadioButton rbPrivadoObra;
		public System.Windows.Forms.RadioButton rbPrivado;
		public System.Windows.Forms.GroupBox Frame1;
		public System.Windows.Forms.Button cbAceptar;
		public System.Windows.Forms.Button cbCancelar;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperacionPrivados));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.Frame1 = new System.Windows.Forms.GroupBox();
			this.rbPrivadoObra = new System.Windows.Forms.RadioButton();
			this.rbPrivado = new System.Windows.Forms.RadioButton();
			this.cbAceptar = new System.Windows.Forms.Button();
			this.cbCancelar = new System.Windows.Forms.Button();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// Frame1
			// 
			this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			this.Frame1.Controls.Add(this.rbPrivadoObra);
			this.Frame1.Controls.Add(this.rbPrivado);
			this.Frame1.Enabled = true;
			this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(9, 6);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(298, 94);
			this.Frame1.TabIndex = 2;
			this.Frame1.Text = "Tipo de factura";
			this.Frame1.Visible = true;
			// 
			// rbPrivadoObra
			// 
			this.rbPrivadoObra.Appearance = System.Windows.Forms.Appearance.Normal;
			this.rbPrivadoObra.BackColor = System.Drawing.SystemColors.Control;
			this.rbPrivadoObra.CausesValidation = true;
			this.rbPrivadoObra.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbPrivadoObra.Checked = false;
			this.rbPrivadoObra.Cursor = System.Windows.Forms.Cursors.Default;
			this.rbPrivadoObra.Enabled = true;
			this.rbPrivadoObra.ForeColor = System.Drawing.SystemColors.ControlText;
			this.rbPrivadoObra.Location = new System.Drawing.Point(30, 51);
			this.rbPrivadoObra.Name = "rbPrivadoObra";
			this.rbPrivadoObra.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.rbPrivadoObra.Size = new System.Drawing.Size(238, 25);
			this.rbPrivadoObra.TabIndex = 4;
			this.rbPrivadoObra.TabStop = true;
			this.rbPrivadoObra.Text = "Privado de obra ben�fica";
			this.rbPrivadoObra.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbPrivadoObra.Visible = true;
			this.rbPrivadoObra.CheckedChanged += new System.EventHandler(this.rbPrivadoObra_CheckedChanged);
			// 
			// rbPrivado
			// 
			this.rbPrivado.Appearance = System.Windows.Forms.Appearance.Normal;
			this.rbPrivado.BackColor = System.Drawing.SystemColors.Control;
			this.rbPrivado.CausesValidation = true;
			this.rbPrivado.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbPrivado.Checked = false;
			this.rbPrivado.Cursor = System.Windows.Forms.Cursors.Default;
			this.rbPrivado.Enabled = true;
			this.rbPrivado.ForeColor = System.Drawing.SystemColors.ControlText;
			this.rbPrivado.Location = new System.Drawing.Point(30, 27);
			this.rbPrivado.Name = "rbPrivado";
			this.rbPrivado.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.rbPrivado.Size = new System.Drawing.Size(238, 25);
			this.rbPrivado.TabIndex = 3;
			this.rbPrivado.TabStop = true;
			this.rbPrivado.Text = "Privado";
			this.rbPrivado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbPrivado.Visible = true;
			this.rbPrivado.CheckedChanged += new System.EventHandler(this.rbPrivado_CheckedChanged);
			// 
			// cbAceptar
			// 
			this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Enabled = false;
			this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAceptar.Location = new System.Drawing.Point(141, 108);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 1;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// cbCancelar
			// 
			this.cbCancelar.BackColor = System.Drawing.SystemColors.Control;
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCancelar.Location = new System.Drawing.Point(225, 108);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 29);
			this.cbCancelar.TabIndex = 0;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbCancelar.UseVisualStyleBackColor = false;
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// OperacionPrivados
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(312, 142);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCancelar);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "OperacionPrivados";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Facturaci�n a Privados (Obra ben�fica)";
			this.Closed += new System.EventHandler(this.OperacionPrivados_Closed);
			this.Load += new System.EventHandler(this.OperacionPrivados_Load);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}