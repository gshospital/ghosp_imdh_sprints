using System;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace InforInterco
{
	internal static class MInforInterco
	{
		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public string VstPoblaciHo;
			public bool VfCabecera;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					result.VstPoblaciHo = String.Empty;
					return result;
			}
		}
	}
}