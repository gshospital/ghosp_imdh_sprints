using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace InforInterco
{
	partial class Pacientes_Interco
	{

		#region "Upgrade Support "
		private static Pacientes_Interco m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Pacientes_Interco DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Pacientes_Interco();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sdcFini", "SdcFfin", "LbFfin", "LbFini", "Frame1", "RbResumen", "RbListado", "Frame2", "CbbMedico", "FrmMedico", "LsbLista1", "LsbLista2", "CbInsTodo", "CbInsUno", "CbDelTodo", "CbDelUno", "Frame3", "cbPantalla", "cbCerrar", "cbImpresora", "CbUnoEntida", "CbTodoEntia", "CbUnoEntide", "CbTodoEntide", "LsbEntidada", "LsbEntidadde", "Frame4", "listBoxHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFini;
		public Telerik.WinControls.UI.RadDateTimePicker SdcFfin;
		public Telerik.WinControls.UI.RadLabel LbFfin;
		public Telerik.WinControls.UI.RadLabel  LbFini;
        public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadRadioButton RbResumen;
		public Telerik.WinControls.UI.RadRadioButton RbListado;
        public Telerik.WinControls.UI.RadGroupBox Frame2;
		public UpgradeHelpers.MSForms.MSCombobox CbbMedico;
		public Telerik.WinControls.UI.RadGroupBox FrmMedico;
		public Telerik.WinControls.UI.RadListControl LsbLista1;
		public Telerik.WinControls.UI.RadListControl LsbLista2;
		public Telerik.WinControls.UI.RadButton CbInsTodo;
		public Telerik.WinControls.UI.RadButton CbInsUno;
		public Telerik.WinControls.UI.RadButton CbDelTodo;
		public Telerik.WinControls.UI.RadButton  CbDelUno;
        public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadButton cbPantalla;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadButton cbImpresora;
		public Telerik.WinControls.UI.RadButton CbUnoEntida;
		public Telerik.WinControls.UI.RadButton CbTodoEntia;
		public Telerik.WinControls.UI.RadButton CbUnoEntide;
		public Telerik.WinControls.UI.RadButton CbTodoEntide;
		public Telerik.WinControls.UI.RadListControl LsbEntidada;
		public Telerik.WinControls.UI.RadListControl LsbEntidadde;
        public Telerik.WinControls.UI.RadGroupBox Frame4;
		private UpgradeHelpers.Gui.ListBoxHelper listBoxHelper1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcFini = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SdcFfin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.LbFfin = new Telerik.WinControls.UI.RadLabel();
            this.LbFini = new Telerik.WinControls.UI.RadLabel();
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.RbResumen = new Telerik.WinControls.UI.RadRadioButton();
            this.RbListado = new Telerik.WinControls.UI.RadRadioButton();
            this.FrmMedico = new Telerik.WinControls.UI.RadGroupBox();
            this.CbbMedico = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.LsbLista1 = new Telerik.WinControls.UI.RadListControl();
            this.LsbLista2 = new Telerik.WinControls.UI.RadListControl();
            this.CbInsTodo = new Telerik.WinControls.UI.RadButton();
            this.CbInsUno = new Telerik.WinControls.UI.RadButton();
            this.CbDelTodo = new Telerik.WinControls.UI.RadButton();
            this.CbDelUno = new Telerik.WinControls.UI.RadButton();
            this.cbPantalla = new Telerik.WinControls.UI.RadButton();
            this.cbCerrar = new Telerik.WinControls.UI.RadButton();
            this.cbImpresora = new Telerik.WinControls.UI.RadButton();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.CbUnoEntida = new Telerik.WinControls.UI.RadButton();
            this.CbTodoEntia = new Telerik.WinControls.UI.RadButton();
            this.CbUnoEntide = new Telerik.WinControls.UI.RadButton();
            this.CbTodoEntide = new Telerik.WinControls.UI.RadButton();
            this.LsbEntidada = new Telerik.WinControls.UI.RadListControl();
            this.LsbEntidadde = new Telerik.WinControls.UI.RadListControl();
            this.listBoxHelper1 = new UpgradeHelpers.Gui.ListBoxHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbResumen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbListado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmMedico)).BeginInit();
            this.FrmMedico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbbMedico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsTodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelTodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImpresora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbUnoEntida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbTodoEntia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbUnoEntide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbTodoEntide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbEntidada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbEntidadde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.sdcFini);
            this.Frame1.Controls.Add(this.SdcFfin);
            this.Frame1.Controls.Add(this.LbFfin);
            this.Frame1.Controls.Add(this.LbFini);
            this.Frame1.HeaderText = "Seleccione rango de fechas";
            this.Frame1.Location = new System.Drawing.Point(281, 232);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(320, 42);
            this.Frame1.TabIndex = 22;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Seleccione rango de fechas";
            // 
            // sdcFini
            // 
            this.sdcFini.CustomFormat = "dd/MM/yyyy";
            this.sdcFini.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFini.Location = new System.Drawing.Point(47, 17);
            this.sdcFini.Name = "sdcFini";
            this.sdcFini.Size = new System.Drawing.Size(112, 20);
            this.sdcFini.TabIndex = 15;
            this.sdcFini.TabStop = false;
            this.sdcFini.Text = "20/05/16";
            this.sdcFini.Value = new System.DateTime(2016, 5, 20, 15, 34, 5, 417);
            // 
            // SdcFfin
            // 
            this.SdcFfin.CustomFormat = "dd/MM/yyyy";
            this.SdcFfin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFfin.Location = new System.Drawing.Point(200, 17);
            this.SdcFfin.Name = "SdcFfin";
            this.SdcFfin.Size = new System.Drawing.Size(112, 20);
            this.SdcFfin.TabIndex = 16;
            this.SdcFfin.TabStop = false;
            this.SdcFfin.Text = "20/05/16";
            this.SdcFfin.Value = new System.DateTime(2016, 5, 20, 15, 34, 5, 436);
            // 
            // LbFfin
            // 
            this.LbFfin.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFfin.Location = new System.Drawing.Point(174, 19);
            this.LbFfin.Name = "LbFfin";
            this.LbFfin.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFfin.Size = new System.Drawing.Size(23, 18);
            this.LbFfin.TabIndex = 24;
            this.LbFfin.Text = "Fin:";
            // 
            // LbFini
            // 
            this.LbFini.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbFini.Location = new System.Drawing.Point(10, 19);
            this.LbFini.Name = "LbFini";
            this.LbFini.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbFini.Size = new System.Drawing.Size(35, 18);
            this.LbFini.TabIndex = 23;
            this.LbFini.Text = "Inicio:";
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.RbResumen);
            this.Frame2.Controls.Add(this.RbListado);
            this.Frame2.HeaderText = "Seleccione tipo de consulta";
            this.Frame2.Location = new System.Drawing.Point(2, 275);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(599, 36);
            this.Frame2.TabIndex = 25;
            this.Frame2.TabStop = false;
            this.Frame2.Text = "Seleccione tipo de consulta";
            // 
            // RbResumen
            // 
            this.RbResumen.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbResumen.Location = new System.Drawing.Point(308, 13);
            this.RbResumen.Name = "RbResumen";
            this.RbResumen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbResumen.Size = new System.Drawing.Size(226, 18);
            this.RbResumen.TabIndex = 1;
            this.RbResumen.Text = "Resumen de totales de las Interconsultas.";
            // 
            // RbListado
            // 
            this.RbListado.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RbListado.Cursor = System.Windows.Forms.Cursors.Default;
            this.RbListado.Location = new System.Drawing.Point(17, 13);
            this.RbListado.Name = "RbListado";
            this.RbListado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RbListado.Size = new System.Drawing.Size(213, 18);
            this.RbListado.TabIndex = 0;
            this.RbListado.Text = "Listado detallado de las interconsultas.";
            this.RbListado.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // FrmMedico
            // 
            this.FrmMedico.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmMedico.Controls.Add(this.CbbMedico);
            this.FrmMedico.HeaderText = "Seleccione M�dico";
            this.FrmMedico.Location = new System.Drawing.Point(2, 232);
            this.FrmMedico.Name = "FrmMedico";
            this.FrmMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmMedico.Size = new System.Drawing.Size(273, 42);
            this.FrmMedico.TabIndex = 21;
            this.FrmMedico.TabStop = false;
            this.FrmMedico.Text = "Seleccione M�dico";
            // 
            // CbbMedico
            // 
            this.CbbMedico.ColumnWidths = "";
            this.CbbMedico.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbbMedico.Location = new System.Drawing.Point(6, 14);
            this.CbbMedico.Name = "CbbMedico";
            this.CbbMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbbMedico.Size = new System.Drawing.Size(260, 20);
            this.CbbMedico.TabIndex = 14;
            this.CbbMedico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbbMedico_KeyPress);
            this.CbbMedico.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CbbMedico_KeyDown);
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.LsbLista1);
            this.Frame3.Controls.Add(this.LsbLista2);
            this.Frame3.Controls.Add(this.CbInsTodo);
            this.Frame3.Controls.Add(this.CbInsUno);
            this.Frame3.Controls.Add(this.CbDelTodo);
            this.Frame3.Controls.Add(this.CbDelUno);
            this.Frame3.HeaderText = "Seleccione Servicio";
            this.Frame3.Location = new System.Drawing.Point(2, 115);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(599, 117);
            this.Frame3.TabIndex = 20;
            this.Frame3.TabStop = false;
            this.Frame3.Text = "Seleccione Servicio";
            // 
            // LsbLista1
            // 
            this.LsbLista1.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista1.Location = new System.Drawing.Point(8, 14);
            this.LsbLista1.Name = "LsbLista1";
            this.LsbLista1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista1.Size = new System.Drawing.Size(260, 95);
            this.LsbLista1.TabIndex = 2;
            this.LsbLista1.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            this.LsbLista1.DoubleClick += new System.EventHandler(this.LsbLista1_DoubleClick);
            // 
            // LsbLista2
            // 
            this.LsbLista2.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbLista2.Location = new System.Drawing.Point(331, 15);
            this.LsbLista2.Name = "LsbLista2";
            this.LsbLista2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbLista2.Size = new System.Drawing.Size(260, 95);
            this.LsbLista2.TabIndex = 7;
            this.LsbLista2.DoubleClick += new System.EventHandler(this.LsbLista2_DoubleClick);
            // 
            // CbInsTodo
            // 
            this.CbInsTodo.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbInsTodo.Location = new System.Drawing.Point(285, 14);
            this.CbInsTodo.Name = "CbInsTodo";
            this.CbInsTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsTodo.Size = new System.Drawing.Size(34, 17);
            this.CbInsTodo.TabIndex = 3;
            this.CbInsTodo.Text = ">>";
            this.CbInsTodo.Click += new System.EventHandler(this.CbInsTodo_Click);
            // 
            // CbInsUno
            // 
            this.CbInsUno.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbInsUno.Location = new System.Drawing.Point(285, 40);
            this.CbInsUno.Name = "CbInsUno";
            this.CbInsUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbInsUno.Size = new System.Drawing.Size(34, 17);
            this.CbInsUno.TabIndex = 4;
            this.CbInsUno.Text = ">";
            this.CbInsUno.Click += new System.EventHandler(this.CbInsUno_Click);
            // 
            // CbDelTodo
            // 
            this.CbDelTodo.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDelTodo.Location = new System.Drawing.Point(285, 67);
            this.CbDelTodo.Name = "CbDelTodo";
            this.CbDelTodo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelTodo.Size = new System.Drawing.Size(34, 17);
            this.CbDelTodo.TabIndex = 5;
            this.CbDelTodo.Text = "<<";
            this.CbDelTodo.Click += new System.EventHandler(this.CbDelTodo_Click);
            // 
            // CbDelUno
            // 
            this.CbDelUno.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDelUno.Location = new System.Drawing.Point(285, 93);
            this.CbDelUno.Name = "CbDelUno";
            this.CbDelUno.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDelUno.Size = new System.Drawing.Size(34, 17);
            this.CbDelUno.TabIndex = 6;
            this.CbDelUno.Text = "<";
            this.CbDelUno.Click += new System.EventHandler(this.CbDelUno_Click);
            // 
            // cbPantalla
            // 
            this.cbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbPantalla.Location = new System.Drawing.Point(425, 315);
            this.cbPantalla.Name = "cbPantalla";
            this.cbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbPantalla.Size = new System.Drawing.Size(81, 29);
            this.cbPantalla.TabIndex = 18;
            this.cbPantalla.Text = "&Pantalla";
            this.cbPantalla.Click += new System.EventHandler(this.cbPantalla_Click);
            // 
            // cbCerrar
            // 
            this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCerrar.Location = new System.Drawing.Point(519, 315);
            this.cbCerrar.Name = "cbCerrar";
            this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCerrar.Size = new System.Drawing.Size(81, 29);
            this.cbCerrar.TabIndex = 19;
            this.cbCerrar.Text = "&Cerrar";
            this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // cbImpresora
            // 
            this.cbImpresora.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImpresora.Location = new System.Drawing.Point(331, 315);
            this.cbImpresora.Name = "cbImpresora";
            this.cbImpresora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImpresora.Size = new System.Drawing.Size(81, 29);
            this.cbImpresora.TabIndex = 17;
            this.cbImpresora.Text = "&Impresora";
            this.cbImpresora.Click += new System.EventHandler(this.cbImpresora_Click);
            // 
            // Frame4
            // 
            this.Frame4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame4.Controls.Add(this.CbUnoEntida);
            this.Frame4.Controls.Add(this.CbTodoEntia);
            this.Frame4.Controls.Add(this.CbUnoEntide);
            this.Frame4.Controls.Add(this.CbTodoEntide);
            this.Frame4.Controls.Add(this.LsbEntidada);
            this.Frame4.Controls.Add(this.LsbEntidadde);
            this.Frame4.HeaderText = "Seleccione Entidad";
            this.Frame4.Location = new System.Drawing.Point(2, -1);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(599, 116);
            this.Frame4.TabIndex = 26;
            this.Frame4.TabStop = false;
            this.Frame4.Text = "Seleccione Entidad";
            // 
            // CbUnoEntida
            // 
            this.CbUnoEntida.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbUnoEntida.Location = new System.Drawing.Point(285, 93);
            this.CbUnoEntida.Name = "CbUnoEntida";
            this.CbUnoEntida.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbUnoEntida.Size = new System.Drawing.Size(34, 17);
            this.CbUnoEntida.TabIndex = 12;
            this.CbUnoEntida.Text = "<";
            this.CbUnoEntida.Click += new System.EventHandler(this.CbUnoEntida_Click);
            // 
            // CbTodoEntia
            // 
            this.CbTodoEntia.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbTodoEntia.Location = new System.Drawing.Point(285, 67);
            this.CbTodoEntia.Name = "CbTodoEntia";
            this.CbTodoEntia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbTodoEntia.Size = new System.Drawing.Size(34, 20);
            this.CbTodoEntia.TabIndex = 11;
            this.CbTodoEntia.Text = "<<";
            this.CbTodoEntia.Click += new System.EventHandler(this.CbTodoEntia_Click);
            // 
            // CbUnoEntide
            // 
            this.CbUnoEntide.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbUnoEntide.Location = new System.Drawing.Point(285, 40);
            this.CbUnoEntide.Name = "CbUnoEntide";
            this.CbUnoEntide.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbUnoEntide.Size = new System.Drawing.Size(34, 17);
            this.CbUnoEntide.TabIndex = 10;
            this.CbUnoEntide.Text = ">";
            this.CbUnoEntide.Click += new System.EventHandler(this.CbUnoEntide_Click);
            // 
            // CbTodoEntide
            // 
            this.CbTodoEntide.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbTodoEntide.Location = new System.Drawing.Point(285, 14);
            this.CbTodoEntide.Name = "CbTodoEntide";
            this.CbTodoEntide.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbTodoEntide.Size = new System.Drawing.Size(34, 17);
            this.CbTodoEntide.TabIndex = 9;
            this.CbTodoEntide.Text = ">>";
            this.CbTodoEntide.Click += new System.EventHandler(this.CbTodoEntide_Click);
            // 
            // LsbEntidada
            // 
            this.LsbEntidada.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbEntidada.ForeColor = System.Drawing.SystemColors.WindowText;
            this.LsbEntidada.Location = new System.Drawing.Point(331, 15);
            this.LsbEntidada.Name = "LsbEntidada";
            this.LsbEntidada.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbEntidada.Size = new System.Drawing.Size(260, 95);
            this.LsbEntidada.TabIndex = 13;
            this.LsbEntidada.DoubleClick += new System.EventHandler(this.LsbEntidada_DoubleClick);
            // 
            // LsbEntidadde
            // 
            this.LsbEntidadde.Cursor = System.Windows.Forms.Cursors.Default;
            this.LsbEntidadde.Location = new System.Drawing.Point(8, 14);
            this.LsbEntidadde.Name = "LsbEntidadde";
            this.LsbEntidadde.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LsbEntidadde.Size = new System.Drawing.Size(260, 95);
            this.LsbEntidadde.TabIndex = 8;
            this.LsbEntidadde.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;            
            this.LsbEntidadde.DoubleClick += new System.EventHandler(this.LsbEntidadde_DoubleClick);
            // 
            // Pacientes_Interco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 347);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.FrmMedico);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.cbPantalla);
            this.Controls.Add(this.cbCerrar);
            this.Controls.Add(this.cbImpresora);
            this.Controls.Add(this.Frame4);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Pacientes_Interco";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Pacientes de Interconsultas - AGI470F1";
            //this.Closed += new System.EventHandler(this.Pacientes_Interco_Closed);
            this.Load += new System.EventHandler(this.Pacientes_Interco_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SdcFfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbFini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RbResumen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RbListado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmMedico)).EndInit();
            this.FrmMedico.ResumeLayout(false);
            this.FrmMedico.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbbMedico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbLista2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsTodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbInsUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelTodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbDelUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImpresora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CbUnoEntida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbTodoEntia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbUnoEntide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbTodoEntide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbEntidada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LsbEntidadde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}