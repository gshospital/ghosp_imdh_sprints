using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.DB.DAO;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;
using Telerik.WinControls;
using System.Linq;

namespace InforInterco
{
	public partial class Pacientes_Interco
        : Telerik.WinControls.UI.RadForm
    {

		WorkspaceHelper Wk1 = null;
		DAODatabaseHelper Dbt1 = null;
		string VstNomtabla = String.Empty;
		SqlConnection Rc = null;
		dynamic LISTADO = null;
		string sttiposer = String.Empty;
		string NOMBRE_DSN_ACCESS = String.Empty;
		string NombreBaseAccess = String.Empty;
		string gUsuario_Crystal = String.Empty;
		string gContrase�a_Crystal = String.Empty;
		string stPathAplicacion = String.Empty;
		MInforInterco.CabCrystal VstrCrystal = MInforInterco.CabCrystal.CreateInstance();
		dynamic Cuadro_Diag_impre = null; // object
		public Pacientes_Interco()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		public void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************

			DataSet tRrCrystal = null;
			string tstCab = String.Empty;
			try
			{
				tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, Rc);
				tRrCrystal = new DataSet();
				tempAdapter.Fill(tRrCrystal);
				
				VstrCrystal.VstGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
				tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, Rc);
				tRrCrystal = new DataSet();
				tempAdapter_2.Fill(tRrCrystal);
				
				VstrCrystal.VstNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
				tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, Rc);
				tRrCrystal = new DataSet();
				tempAdapter_3.Fill(tRrCrystal);
				
				VstrCrystal.VstDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
				tstCab = "SELECT valfanu2 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tstCab, Rc);
				tRrCrystal = new DataSet();
				tempAdapter_4.Fill(tRrCrystal);
			
				VstrCrystal.VstPoblaciHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU2"]).Trim();
			
				tRrCrystal.Close();
				VstrCrystal.VfCabecera = true;
			}
			catch
			{
				RadMessageBox.Show("Error al cargar la cabecera del informe", Application.ProductName);
			}
		}

		public void fnrellena_medicos()
		{
			string sqlMedicos = String.Empty;
			DataSet rrMedicos = null;
			int X = 0;
			StringBuilder vstConsulta = new StringBuilder();
			int vstIndice = 0;
			string vstMedico = String.Empty;

			try
			{
				vstIndice = 0;
				vstMedico = "";
				if (LsbLista2.Items.Count > 0)
				{
					//seleccionar el que esta
					if (CbbMedico.SelectedIndex != -1)
					{
						vstIndice = Convert.ToInt32(CbbMedico.SelectedValue);
					}
					vstMedico = CbbMedico.Text;
					CbbMedico.Items.Clear();
					LsbLista2.SelectedIndex = 0;
                    LsbLista2.SelectedValue = 0;
     //               for (X = 0; X <= LsbLista2.Items.Count - 1; X++)
					//{ 

     //                   if (vstConsulta.ToString() != "")
					//	{
     //                       vstConsulta.Append("," + LsbLista2.Items[X].Value);
                                
					//	}
					//	else
					//	{
     //                       vstConsulta.Append( "(" + LsbLista2.Items[X].Value);
     //                   }
					//}

                    var strIn = "(" + string.Join(",", LsbLista2.Items.Select(x => x.Value).ToArray()) + ")";

					vstConsulta.Append(")");
					sqlMedicos = "select distinct dperserv.gpersona,dnompers,dap1pers,dap2pers from dpersona,dperserv " + " where gservici in " + strIn + " and dperserv.gpersona = " + " dpersona.gpersona order by dap1pers,dap2pers,dnompers";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlMedicos, Rc);
					rrMedicos = new DataSet();
					tempAdapter.Fill(rrMedicos);
					if (rrMedicos.Tables[0].Rows.Count != 0)
					{
						
						foreach (DataRow iteration_row in rrMedicos.Tables[0].Rows)
						{
							CbbMedico.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dap1pers"]).Trim() + " " + Convert.ToString(iteration_row["dap2pers"]).Trim() + "," + Convert.ToString(iteration_row["dnompers"]).Trim(), Convert.ToInt32(iteration_row["gpersona"])));							
						}
						CbbMedico.Items.Add(new RadListDataItem("SIN M�DICO ASIGNADO",-1));
						
						if (vstMedico == "")
						{
							CbbMedico.SelectedIndex = -1;
						}
						else
						{
							for (X = 0; X <= CbbMedico.Items.Count - 1; X++)
							{
								if (Convert.ToInt32(CbbMedico.Items[X].Text) == vstIndice)
								{

                                    vstMedico = (CbbMedico.Items[X].Text);
                                    

                                    break;
								}
							}
							if (X != CbbMedico.Items.Count)
							{
								CbbMedico.SelectedIndex = X;
							}
							else
							{
								CbbMedico.SelectedIndex = -1;
							}
						}
					}
				}
				else
				{
					CbbMedico.Items.Clear();
					CbbMedico.SelectedIndex = -1;
				}
			}
			catch
			{
				RadMessageBox.Show("Error al cargar la lista de m�dicos", Application.ProductName);
			}
		}
		public void proCrear_tabla()
		{
			bool bExiste_base = false;
			string tstConexion = String.Empty;
               /*
			try
			{
                // PARIO TODO _X_5
				//HAGO LA CONEXION DE CRISTAL
				tstConexion = "Description=" + "BASE TEMPORAL PARA CUADRO MANDO EN ACCESS" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=(local)" + "\r" + "dbq=" + stPathAplicacion + "\\" + NombreBaseAccess + "";
				//UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(NOMBRE_DSN_ACCESS, "Microsoft Access Driver (*.mdb)", true, tstConexion);
				//SI NO EXISTE LA BASE DE DATOS CREARLA
				bExiste_base = false;
				Wk1 = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0];
				if (FileSystem.Dir(stPathAplicacion + "\\" + NombreBaseAccess, FileAttribute.Normal) == "")
				{
					//UPGRADE_ISSUE: (2064) DAO.Workspace method Wk1.CreateDatabase was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					Dbt1 = Wk1.CreateDatabase(stPathAplicacion + "\\" + NombreBaseAccess, DAO.LanguageConstants.dbLangSpanish, null);
				}
				else
				{
					//SI EXISTE LA TABLA BORRAR
					//UPGRADE_WARNING: (2065) DAO.Workspace method Wk1.OpenDatabase has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
					Dbt1 = Wk1.OpenDatabase("<Connection-String>");
					foreach (TableDefHelper dTabla in Dbt1.TableDefs)
					{
						if (dTabla.Name == VstNomtabla)
						{
							bExiste_base = true;
							break;
						}
					}
					//si existe borrarla
					if (bExiste_base)
					{
						DbCommand TempCommand = Dbt1.Connection.CreateCommand();
						TempCommand.CommandText = "drop table " + VstNomtabla;
						TempCommand.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(Dbt1.Connection);
						TempCommand.ExecuteNonQuery();
					}
				}
				//crear tabla
				DbCommand TempCommand_2 = Dbt1.Connection.CreateCommand();
				TempCommand_2.CommandText = "CREATE TABLE " + VstNomtabla + " (MedRealiza  STRING, Sociedad  STRING, PACIENTE  STRING, Historia  STRING, " + "Poliza  STRING, SerPide  STRING, MedPide  STRING, SerRealiza  STRING, Estado  STRING, Fecha STRING);";
				TempCommand_2.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(Dbt1.Connection);
				TempCommand_2.ExecuteNonQuery();
			}
			catch
			{
				RadMessageBox.Show("Error al crear las tablas temporales del informe", Application.ProductName);
			}*/
		}
		public string proMedico(string stCod)
		{
			DataSet rrConsult = null;
			string stTemp = String.Empty;
			string stDesc = String.Empty;

			try
			{
				if (stCod != "")
				{
					stTemp = "select dnompers, dap1pers, dap2pers from dpersona where gpersona   = " + stCod;
					SqlDataAdapter tempAdapter = new SqlDataAdapter(stTemp, Rc);
					rrConsult = new DataSet();
					tempAdapter.Fill(rrConsult);
					if (rrConsult.Tables[0].Rows.Count > 0)
					{
						
						stDesc = (Convert.ToString(rrConsult.Tables[0].Rows[0]["dap1pers"]) + "").Trim() + " " + (Convert.ToString(rrConsult.Tables[0].Rows[0]["dap2pers"]) + "").Trim() + ", " + (Convert.ToString(rrConsult.Tables[0].Rows[0]["dnompers"]) + "").Trim();
					}
					else
					{
						stDesc = "";
					}
					
					rrConsult.Close();
				}
				else
				{
					stDesc = "SIN M�DICO ASIGNADO";
				}
				return stDesc;
			}
			catch
			{
				return "";
			}
		}
		public string proServicio(string stCod)
		{
			string result = String.Empty;
			DataSet rrConsult = null;
			string stTemp = String.Empty;
			string stDesc = String.Empty;

			try
			{
				stTemp = "select dnomserv from dservici where gservici = " + stCod;
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stTemp, Rc);
				rrConsult = new DataSet();
				tempAdapter.Fill(rrConsult);
				if (rrConsult.Tables[0].Rows.Count > 0)
				{
					
					stDesc = Convert.ToString(rrConsult.Tables[0].Rows[0]["dnomserv"]) + "";
				}
				else
				{
					stDesc = " ";
				}
				result = stDesc;
				
				rrConsult.Close();
				return result;
			}
			catch
			{
				return "";
			}
		}

		public void proRellena_tabla(string stSQL, string VstTabla)
		{
			DAORecordSetHelper rtemp = null;
			string stTemp = String.Empty;
			DataSet rrIngre = null;

			try
			{
                //PARIO TODO_X_5
                /*
				stTemp = "SELECT * FROM " + VstTabla;
				rtemp = Dbt1.OpenRecordset(stTemp);
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQL, Rc);
				rrIngre = new DataSet();
				tempAdapter.Fill(rrIngre);
				if (rrIngre.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
					rrIngre.MoveFirst();
					foreach (DataRow iteration_row in rrIngre.Tables[0].Rows)
					{
						rtemp.AddNew();
						//**********************
						rtemp["MedRealiza"] = proMedico((Convert.ToString(iteration_row["gperinte"]) + "").Trim());
						rtemp["Sociedad"] = Convert.ToString(iteration_row["dsocieda"]).Trim();
						rtemp["PACIENTE"] = Convert.ToString(iteration_row["dape1pac"]).Trim() + " " + Convert.ToString(iteration_row["dape2pac"]).Trim() + ", " + Convert.ToString(iteration_row["dnombpac"]).Trim() + "";
						rtemp["Historia"] = Convert.ToString(iteration_row["ghistoria"]).Trim();
						rtemp["Poliza"] = (Convert.ToString(iteration_row["nafiliac"]) + "").Trim();
						rtemp["SerPide"] = proServicio(Convert.ToString(iteration_row["gsersoli"]).Trim());
						rtemp["MedPide"] = proMedico(Convert.ToString(iteration_row["gpersoli"]).Trim());
						rtemp["SerRealiza"] = proServicio(Convert.ToString(iteration_row["gservici"]).Trim());
						switch(Convert.ToString(iteration_row["iestsoli"]).Trim().ToUpper())
						{
							case "P" : 
								rtemp["Estado"] = "Pendiente"; 
								rtemp["Fecha"] = Convert.ToString(iteration_row["fpeticio"]).Trim(); 
								break;
							case "C" : 
								rtemp["Estado"] = "Comunicada"; 
								rtemp["Fecha"] = Convert.ToString(iteration_row["fprevista"]).Trim(); 
								break;
							case "R" : 
								rtemp["Estado"] = "Realizada"; 
								rtemp["Fecha"] = Convert.ToString(iteration_row["frealiza"]).Trim(); 
								break;
							case "A" : 
								rtemp["Estado"] = "Anulada"; 
								rtemp["Fecha"] = Convert.ToString(iteration_row["fanulaci"]).Trim(); 
								break;
						}
						//**********************
						rtemp.Update();
					}
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rrIngre.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rrIngre.Close();
				rtemp.Close();*/
			}
			catch
			{
				RadMessageBox.Show("Error al rellenar las tablas temporales del informe", Application.ProductName);
			}
		}


		private void CbbMedico_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			if (KeyCode == 46 || KeyCode == 110)
			{
				CbbMedico.SelectedIndex = -1;
			}
		}

		private void CbbMedico_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 8)
			{
				CbbMedico.SelectedIndex = -1;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}
		private void CbDelTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			Paso_Lisa_Lisb(LsbLista2, LsbLista1);
			fnrellena_medicos();
			proAceptar();
		}
		private void CbDelUno_Click(Object eventSender, EventArgs eventArgs)
		{
			//pongo el deseleccionado de la 2 en la 1 y le quito de 2
			if ((LsbLista2.SelectedIndex) != -1)
			{
                LsbLista1.Items.Add(new RadListDataItem(LsbLista2.Items[LsbLista2.SelectedIndex].Text, LsbLista2.Items[LsbLista2.SelectedIndex].Value));
				LsbLista2.Items.RemoveAt(LsbLista2.SelectedIndex);
                LsbLista2.SelectedIndex = -1;
                fnrellena_medicos();
			}
            
            proAceptar();
		}
		private void cbImpresora_Click(Object eventSender, EventArgs eventArgs)
		{
			if (sdcFini.Value.Date > SdcFfin.Value.Date)
			{
				MessageBox.Show("La fecha de inicio debe ser menor o igual que la de fin", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
            // PARIO TODO _X_5
		/*	proImprimir(Crystal.DestinationConstants.crptToPrinter);*/
		}

		private void CbInsTodo_Click(Object eventSender, EventArgs eventArgs)
		{
			//quito todos de 1 y los pongo en 2
			if (LsbLista1.Items.Count > 0)
			{
				Paso_Lisa_Lisb(LsbLista1, LsbLista2);
				fnrellena_medicos();
			}
			proAceptar();
		}
		public void Paso_Lisa_Lisb(RadListControl listaa, RadListControl listab)
		{
			//indice de las listas
			if (listaa.Items.Count > 0)
			{
              
                listaa.SelectedIndex = 0;
				for (int viIn = 0; viIn <= listaa.Items.Count - 1; viIn++)
				{ // Bucle por la lista.
					listab.Items.Add(new RadListDataItem(listaa.Items[viIn].Text, listaa.Items[viIn].Value)); 
				}
				listaa.Items.Clear();
			}
		}
		private void CbInsUno_Click(Object eventSender, EventArgs eventArgs)
		{
			//pongo el seleccionado en la segunda lista y le quito de la primera
			if (LsbLista1.SelectedIndex != -1)
			{
                LsbLista2.Items.Add(new RadListDataItem(LsbLista1.Items[LsbLista1.SelectedIndex].Text, LsbLista1.Items[LsbLista1.SelectedIndex].Value));
                LsbLista1.Items.RemoveAt(LsbLista1.SelectedIndex);
                LsbLista1.SelectedIndex = -1;
                fnrellena_medicos();
			}
			proAceptar();
		}
		private void cbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{
			if (sdcFini.Value.Date > SdcFfin.Value.Date)
			{
				MessageBox.Show("La fecha de inicio debe ser menor o igual que la de fin", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
            //PARIO
			/*proImprimir(Crystal.DestinationConstants.crptToWindow);*/
		}
		private void CbTodoEntia_Click(Object eventSender, EventArgs eventArgs)
		{
			if (LsbEntidada.Items.Count > 0)
			{
				Paso_Lisa_Lisb(LsbEntidada, LsbEntidadde);
			}
			proAceptar();
		}
		private void CbTodoEntide_Click(Object eventSender, EventArgs eventArgs)
		{
			if (LsbEntidadde.Items.Count > 0)
			{
				Paso_Lisa_Lisb(LsbEntidadde, LsbEntidada);
			}
			proAceptar();
		}

		private void CbUnoEntida_Click(Object eventSender, EventArgs eventArgs)
		{
			if (LsbEntidada.SelectedIndex != -1)//ListBoxHelper.GetSelectedIndex(LsbEntidada)
            {
                LsbEntidadde.Items.Add(new RadListDataItem(LsbEntidada.Items[LsbEntidada.SelectedIndex].Text, LsbEntidada.Items[LsbEntidada.SelectedIndex].Value));
                LsbEntidada.Items.RemoveAt(LsbEntidada.SelectedIndex);
                LsbEntidada.SelectedIndex = -1;


            }
			proAceptar();
		}
		private void CbUnoEntide_Click(Object eventSender, EventArgs eventArgs)
		{
			if (LsbEntidadde.SelectedIndex != -1)
			{
                LsbEntidada.Items.Add(new RadListDataItem(Strings.StrConv(LsbEntidadde.Items[LsbEntidadde.SelectedIndex].Text,VbStrConv.ProperCase), LsbEntidadde.Items[LsbEntidadde.SelectedIndex].Value));
				LsbEntidadde.Items.RemoveAt(LsbEntidadde.SelectedIndex);
                LsbEntidadde.SelectedIndex = -1;

            }
			proAceptar();
		}
		public void RecogerDatosDeLaClase(SqlConnection Conexion, string sttipo, object LISTADO_GENERAL, string DSN_ACCESS, string NombreBase, string gUsuario, string gContrase�a, string stPath, object Diag_impre)
		{
			Rc = Conexion;
			sttiposer = sttipo;
			LISTADO = LISTADO_GENERAL;
			NOMBRE_DSN_ACCESS = DSN_ACCESS;
			NombreBaseAccess = NombreBase;
			gUsuario_Crystal = gUsuario;
			gContrase�a_Crystal = gContrase�a;
			stPathAplicacion = stPath;
			Cuadro_Diag_impre = Diag_impre;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Rc);
		}
		
		private void Pacientes_Interco_Load(Object eventSender, EventArgs eventArgs)
		{
			SqlCommand tRqServicio = new SqlCommand();
			DataSet tRrServicio = null;
			string tstSqServ = String.Empty;
			try
			{
				RbListado.IsChecked = true;
				//cargo la lista con los servicios
				tstSqServ = "select dnomserv,gservici from dservici ";
				tstSqServ = tstSqServ + "WHERE iurgenci = 'S' OR ihospita = 'S' OR ";
				tstSqServ = tstSqServ + "iconsult = 'S' ORDER BY dnomserv ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstSqServ, Rc);
				tRrServicio = new DataSet();
				tempAdapter.Fill(tRrServicio);
				foreach (DataRow iteration_row in tRrServicio.Tables[0].Rows)
				{
					
					if (!Convert.IsDBNull(iteration_row["dnomserv"]))
					{
                        LsbLista1.Items.Add(new RadListDataItem (Convert.ToString(iteration_row["dnomserv"]).Trim() , Convert.ToInt32(iteration_row["gservici"])));    

                    }
				}
			
				tRrServicio.Close();
				tRqServicio = null;
				//cargo la lista con las entidades
				tstSqServ = "select *  from dsocieda";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstSqServ, Rc);
				tRrServicio = new DataSet();
				tempAdapter_2.Fill(tRrServicio);
				foreach (DataRow iteration_row_2 in tRrServicio.Tables[0].Rows)
				{
					
					if (!Convert.IsDBNull(iteration_row_2["gsocieda"]))
					{

                        LsbEntidadde.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row_2["dsocieda"]).Trim().ToLower(),VbStrConv.ProperCase), Convert.ToInt32(iteration_row_2["gsocieda"])));
                     
                    }
				}
				
				tRrServicio.Close();
				tRqServicio = null;

				//deshabilito los botones
				cbImpresora.Enabled = false;
				cbPantalla.Enabled = false;

				VstNomtabla = "PacienteInterco";

                CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
                string LitPersona = String.Empty;
				LitPersona = Serrores.ObtenerLiteralPersona(Rc);
				this.Text = "Interconsultas de " + LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + "s" + " - AGI470F1";
			}
			catch(SqlException ex )
			{
				//**********
				
				Serrores.GestionErrorBaseDatos(ex, Rc);
			}

		}
		public string MontarSQL(string stTabla, string stSociedades, string stServicios)
		{
			string stCondicion = String.Empty;
			string stUrgAdm = String.Empty;
			string stNumero = String.Empty;
			string stA�o = String.Empty;
			string stCondicionSociedad = String.Empty;
			string stNafiliac = String.Empty;

			if (sttiposer == "U")
			{
				stCondicion = "";
				stUrgAdm = "UEPISURG";
				stNumero = "gnumurge";
				stA�o = "ganourge";
				stNafiliac = stUrgAdm;
				stCondicionSociedad = "";
			}
			else
			{
				if (stTabla == "Einterco")
				{
					stCondicion = "amovifin.ffinmovi  is null and ";
				}
				else
				{
					stCondicion = " amovifin.ffinmovi = ( select max(ffinmovi) from amovifin as A where A.ganoregi = eintealt.ganoregi and ";
					stCondicion = stCondicion + " A.gnumregi = eintealt.gnumregi and A.itiposer = '" + sttiposer + "') and ";
				}
				stUrgAdm = "AEPISADM";
				stNumero = "gnumadme";
				stA�o = "ganoadme";
				stNafiliac = "amovifin";
				stCondicionSociedad = "inner join amovifin on (" + stTabla + ".itiposer = amovifin.itiposer) and " + 
				                      "(" + stTabla + ".ganoregi = amovifin.ganoregi) and (" + stTabla + ".gnumregi = amovifin.gnumregi)";
			}
			string stSQL = "select " + stTabla + ".gservici, " + stTabla + ".gsersoli, " + stTabla + ".gpersoli, " + stTabla + ".gperinte, ghistoria, ";
			stSQL = stSQL + "fpeticio, fprevista, frealiza,fanulaci, iestsoli, " + stNafiliac + ".nafiliac, dnombpac, dape1pac , dape2pac, dsocieda ";
			stSQL = stSQL + "From ((((" + stTabla + " inner join " + stUrgAdm + " on (" + stTabla + ".ganoregi = " + stUrgAdm + "." + stA�o + ") and ";
			stSQL = stSQL + "(" + stTabla + ".gnumregi = " + stUrgAdm + "." + stNumero + ")) left join hdossier on (" + stUrgAdm + ".gidenpac = hdossier.gidenpac)) ";
			stSQL = stSQL + "inner join dpacient on (" + stUrgAdm + ".gidenpac = dpacient.gidenpac)) " + stCondicionSociedad + ") left join dsocieda on ";
			stSQL = stSQL + "(" + stNafiliac + ".gsocieda = dsocieda.gsocieda) ";
			string tempRefParam = sdcFini.Text;
			object tempRefParam2 = SdcFfin.Text + " 23:59:59";
			stSQL = stSQL + "Where " + stTabla + ".itiposer = '" + sttiposer + "' and " + stCondicion + " fpeticio >= " + Serrores.FormatFecha( tempRefParam) + " and fpeticio <= " + Serrores.FormatFechaHMS( tempRefParam2);

			if (stSociedades != "")
			{
				stSQL = stSQL + " and " + stNafiliac + ".gsocieda " + stSociedades;
			}
			if (stServicios != "")
			{
				stSQL = stSQL + " and " + stTabla + ".gservici " + stServicios;
			}

			if (CbbMedico.SelectedIndex != -1)
			{
				if (Convert.ToInt32(CbbMedico.SelectedValue) == -1)
				{
					stSQL = stSQL + " and " + stTabla + ".gperinte is null ";
				}
				else
				{
					stSQL = stSQL + " and " + stTabla + ".gperinte = " + CbbMedico.GetItemData(CbbMedico.SelectedIndex).ToString();
				}
			}
			return stSQL;
		}
		public void proSociedadServicio(ref string stSociedades, ref string stServicios)
		{
			//indice de las listas

			if (LsbLista1.Items.Count != 0)
			{
				if (LsbLista1.Items.Count > LsbLista2.Items.Count)
				{
                    
                   LsbLista2.SelectedIndex = 0;
                    stServicios = "in (";
					for (int viIn = 0; viIn <= LsbLista2.Items.Count - 1; viIn++)
					{ // Bucle por la lista
						if (viIn < LsbLista2.Items.Count - 1)
						{
							stServicios = stServicios + LsbLista2.Items[viIn].Text + ", ";
						}
						else if (viIn == LsbLista2.Items.Count - 1)
						{ 
							stServicios = stServicios + LsbLista2.Items[viIn].Text + ")";
						}
					}
				}
				else
				{
                    
                 LsbLista1.SelectedIndex = 0;
                    stServicios = "not in (";
					for (int viIn = 0; viIn <= LsbLista1.Items.Count - 1; viIn++)
					{ // Bucle por la lista
						if (viIn < LsbLista1.Items.Count - 1)
						{
							stServicios = stServicios + LsbLista1.Items[viIn].Text + ", ";
                        }
						else if (viIn == LsbLista1.Items.Count - 1)
						{ 
							stServicios = stServicios + LsbLista1.Items[viIn].Text + ")";
						}
					}
				}
			}

			if (LsbEntidadde.Items.Count != 0)
			{
				if (LsbEntidadde.Items.Count > LsbEntidada.Items.Count)
				{
					stSociedades = "In (";

                    LsbEntidada.SelectedIndex = 0;
                    for (int viIn = 0; viIn <= LsbEntidada.Items.Count - 1; viIn++)
					{ // Bucle por la lista
						if (viIn < LsbEntidada.Items.Count - 1)
						{
							stSociedades = stSociedades + LsbEntidada.Items[viIn].Text + ", ";
						}
						else if (viIn == LsbEntidada.Items.Count - 1)
						{ 
							stSociedades = stSociedades + LsbEntidada.Items[viIn].Text + ")";
						}
					}
				}
				else
				{
					stSociedades = "Not In (";
				LsbEntidadde. SelectedIndex = 0;
                    
                    for (int viIn = 0; viIn <= LsbEntidadde.Items.Count - 1; viIn++)
					{ // Bucle por la lista
						if (viIn < LsbEntidadde.Items.Count - 1)
						{
							stSociedades = stSociedades + LsbEntidadde.Items[viIn].Text + ", ";
						}
						else if (viIn == LsbEntidadde.Items.Count - 1)
						{ 
							stSociedades = stSociedades + LsbEntidadde.Items[viIn].Text + ")";
						}
					}
				}
			}
		}
	     public void proImprimir (dynamic stSitio ) 
            // pario TODO _X_5
        /*	public void proImprimir(Crystal.DestinationConstants stSitio)*/
		{
			string tstSqlReg = String.Empty;
			string stMedico = String.Empty;
			string stCabecera = String.Empty;
			string stFechas = String.Empty;
			string stServicios = String.Empty;
			string stSociedades = String.Empty;
			string LitPersona = String.Empty;
			//indice de las listas

			//**************
			try
			{
				//****************

				this.Cursor = Cursors.WaitCursor;

				//(maplaza)(16/10/2007)Se resetea el objeto Crystal
				LISTADO.Reset();
				//-----

				//*********cabeceras
				if (!VstrCrystal.VfCabecera)
				{
					proCabCrystal();
				}
                //RELLENA FORMULAS
                //LISTADO.Formulas[0] = "Nom_sociedad= \"" + VstrCrystal.VstGrupoHo + "\"";
                LISTADO.Formulas["Nom_sociedad"] = "\"" + VstrCrystal.VstGrupoHo + "\"";
				//LISTADO.Formulas[1] = "Nom_hospital= \"" + VstrCrystal.VstNombreHo + "\"";
                LISTADO.Formulas["Nom_hospital"] = "\"" + VstrCrystal.VstNombreHo + "\"";
                //LISTADO.Formulas[2] = "Dir_hospital= \"" + VstrCrystal.VstDireccHo + "\"";
                LISTADO.Formulas["Dir_hospital"] = "\"" + VstrCrystal.VstDireccHo + "\"";
                stFechas = "Fecha inicio : " + DateTime.Parse(sdcFini.Text).ToString("dd/MM/yyyy") + "  Fecha fin : " + DateTime.Parse(SdcFfin.Text).ToString("dd/MM/yyyy");
				if (CbbMedico.SelectedIndex > -1)
				{
					stMedico = "Interconsultas realizadas por el Doctor : " + CbbMedico.Text;
				}
				else
				{
					stMedico = "";
				}
				stServicios = "Servicios seleccionados: ";


                LsbLista2.SelectedIndex = 0;
				for (int viIn = 0; viIn <= LsbLista2.Items.Count - 1; viIn++)
				{ // Bucle por la lista
					if (viIn < LsbLista2.Items.Count - 1 && viIn <= 2)
					{
						stServicios = stServicios + LsbLista2.Items[viIn].Text + ", ";
					}
					else
					{
						if (viIn > 2 && viIn < LsbLista2.Items.Count - 1)
						{
							stServicios = stServicios + "...";
							break;
						}
						else
						{
							stServicios = stServicios + LsbLista2.Items[viIn].Text;
                        }
                        //GetListItem(viIn);
                    }
					}
				
			
				//LISTADO.Formulas[3] = "medico= \"" + stMedico + "\"";
                LISTADO.Formulas["medico"] = "\"" + stMedico + "\"";
                //LISTADO.Formulas[4] = "Rango_fechas=\"" + stFechas + "\"";
                LISTADO.Formulas["Rango_fechas"] = "\"" + stFechas + "\"";
                //LISTADO.Formulas[5] = "ServSelec=\"" + stServicios + "\"";
                LISTADO.Formulas["ServSelec"] = "\"" + stServicios + "\"";
                LISTADO.DataFiles[0] = "" + stPathAplicacion + "\\" + NombreBaseAccess;
				LISTADO.SQLQuery = "select * from " + VstNomtabla;
                // PARIO TODO _X_5
                /*
                LISTADO.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;*/
				LISTADO.Connect = "DSN=" + NOMBRE_DSN_ACCESS + ";UID=" + gUsuario_Crystal + ";PWD=" + gContrase�a_Crystal + "";
				LISTADO.Destination = (int) stSitio;
				LISTADO.WindowShowPrintSetupBtn = true;
                //UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                // PARIO TODO _X_5
                /*
                if (LISTADO.Destination == ((int) Crystal.DestinationConstants.crptToPrinter))
				{
					proDialogo_impre(LISTADO);
				}
				proCrear_tabla();
				stServicios = ""; //Esta variable la reutilizao
				//meto las sociedades y los servicios en string para generar la query
				proSociedadServicio(ref stSociedades, ref stServicios);

				tstSqlReg = MontarSQL("Einterco", stSociedades, stServicios);
				proRellena_tabla(tstSqlReg, VstNomtabla);
				tstSqlReg = MontarSQL("Eintealt", stSociedades, stServicios);
				proRellena_tabla(tstSqlReg, VstNomtabla);

				UpgradeHelpers.DB.TransactionManager.DeEnlist(Dbt1.Connection);
				Dbt1.Close();
				Wk1.Close();*/
                Application.DoEvents();

				LitPersona = Serrores.ObtenerLiteralPersona(Rc);
				LitPersona = LitPersona.ToUpper();

				if (RbListado.IsChecked)
				{
					stCabecera = "LISTADO DE INTERCONSULTAS POR M�DICO/ENTIDAD";
					//LISTADO.Formulas[6] = "cabecera=\"" + stCabecera + "\"";
                    LISTADO.Formulas["cabecera"] = "\"" + stCabecera + "\"";
                    //LISTADO.Formulas[7] = "LitPers= \"" + LitPersona + "\"";
                    LISTADO.Formulas["LitPers"] = "\"" + LitPersona + "\"";
                    LISTADO.WindowTitle = stCabecera;
					LISTADO.ReportFileName = "" + stPathAplicacion + "\\..\\elementosComunes\\rpt\\AGI470R1.rpt";
					LISTADO.Action = 1;
					LISTADO.PrinterStopPage = LISTADO.PageCount;
				}
				else
				{
					stCabecera = "LISTADO RESUMEN DE INTERCONSULTAS POR M�DICO/ENTIDAD";
					//LISTADO.Formulas[6] = "cabecera=\"" + stCabecera + "\"";
                    LISTADO.Formulas["cabecera"] = "\"" + stCabecera + "\"";
                    LISTADO.ReportFileName = "" + stPathAplicacion + "\\..\\elementosComunes\\rpt\\AGI470R2.rpt";
					LISTADO.WindowTitle = stCabecera;
					LISTADO.Action = 1;
					LISTADO.PrinterStopPage = LISTADO.PageCount;
					stCabecera = "LISTADO RESUMEN DE INTERCONSULTAS POR ENTIDAD/M�DICO";
					LISTADO.Formulas["cabecera"] = "\"" + stCabecera + "\"";
					LISTADO.ReportFileName = "" + stPathAplicacion + "\\..\\elementosComunes\\rpt\\AGI470R3.rpt";
					LISTADO.WindowTitle = stCabecera;
					LISTADO.Action = 1;
					LISTADO.PrinterStopPage = LISTADO.PageCount;
					stCabecera = "LISTADO RESUMEN DE INTERCONSULTAS POR ESPECIALIDAD";
				
					LISTADO.Formulas["cabecera"] = "\"" + stCabecera + "\"";
					LISTADO.ReportFileName = "" + stPathAplicacion + "\\..\\elementosComunes\\rpt\\AGI470R4.rpt";
					LISTADO.WindowTitle = stCabecera;
					LISTADO.Action = 1;
					LISTADO.PrinterStopPage = LISTADO.PageCount;
				}

                //LISTADO.Formulas[0]
                LISTADO.Formulas["Nom_sociedad"] = "";
                //LISTADO.Formulas[1]
                LISTADO.Formulas["Nom_hospital"] = "";
                //LISTADO.Formulas[2]
                LISTADO.Formulas["Dir_hospital"] = "";
                //LISTADO.Formulas[3]
                LISTADO.Formulas["medico"] = "";
                //LISTADO.Formulas[4]
                LISTADO.Formulas["Rango_fechas"] = "";
                //LISTADO.Formulas[5]
                LISTADO.Formulas["ServSelec"] = "";
                //LISTADO.Formulas[6]
                LISTADO.Formulas["cabecera"] = "";
                //LISTADO.Formulas[7]
                LISTADO.Formulas["LitPers"] = "";
				Paso_Lisa_Lisb(LsbLista2, LsbLista1);
				Paso_Lisa_Lisb(LsbEntidada, LsbEntidadde);
				cbImpresora.Enabled = false;
				cbPantalla.Enabled = false;
				CbbMedico.Items.Clear();
				CbbMedico.SelectedIndex = -1;
				sdcFini.Value = DateTime.Today;
				SdcFfin.Value = DateTime.Today;
				this.Cursor = Cursors.Default;
			}
			catch (Exception e)
			{
				//*****
			
				LISTADO.Formulas["Nom_sociedad"] = "";
				LISTADO.Formulas["Nom_hospital"] = "";
				LISTADO.Formulas["Dir_hospital"] = "";
				LISTADO.Formulas["medico"] = "";
				LISTADO.Formulas["Rango_fechas"] = "";
				LISTADO.Formulas["ServSelec"] = "";
				LISTADO.Formulas["cabecera"] = "";
				LISTADO.Formulas["LitPers"] = "";
				Paso_Lisa_Lisb( LsbLista2, LsbLista1);
				Paso_Lisa_Lisb(LsbEntidada, LsbEntidadde);
				cbImpresora.Enabled = false;
				cbPantalla.Enabled = false;
				CbbMedico.Items.Clear();
				CbbMedico.SelectedIndex = -1;
				sdcFini.Value = DateTime.Today;
				SdcFfin.Value = DateTime.Today;
				this.Cursor = Cursors.Default;
				
				if (Information.Err().Number == 32755)
				{
					//CANCELADA IMPRESION
				}
				else
				{
					RadMessageBox.Show(e.Message, Application.ProductName);
				}
				Dbt1 = null;
				Wk1 = null;
			}
		}
		public void proDialogo_impre(object Pcrystal)
		{
			
			Cuadro_Diag_impre.Copies = 1;
			Cuadro_Diag_impre.CancelError = true;
			Cuadro_Diag_impre.PrinterDefault = true;
			Cuadro_Diag_impre.ShowPrinter();

            // PARIO TODO _X_5
           /* Pcrystal.CopiesToPrinter = Cuadro_Diag_impre.Copies;*/
		}

		private void Pacientes_Interco_Closed(Object eventSender, EventArgs eventArgs)
		{
			try
			{
				if (FileSystem.Dir(stPathAplicacion + "\\" + NombreBaseAccess, FileAttribute.Normal) != "")
				{
					File.Delete(stPathAplicacion + "\\" + NombreBaseAccess);
				}
				this.Close();
			}
			catch
			{
				RadMessageBox.Show("Error al cerrar las tablas temporales del informe", Application.ProductName);
			}
		}
		private void LsbEntidada_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbUnoEntida_Click(CbUnoEntida, new EventArgs());
		}

		private void LsbEntidadde_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbUnoEntide_Click(CbUnoEntide, new EventArgs());
		}

		private void LsbLista1_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbInsUno_Click(CbInsUno, new EventArgs());
		}


		private void LsbLista2_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			CbDelUno_Click(CbDelUno, new EventArgs());
		}
		private void proAceptar()
		{

			bool tfEnable = true;
			if (LsbLista2.Items.Count == 0)
			{
				tfEnable = false;
			}
			if (LsbEntidada.Items.Count == 0)
			{
				tfEnable = false;
			}

			if (!tfEnable)
			{
				//deshabilito los botones
				cbImpresora.Enabled = false;
				cbPantalla.Enabled = false;
			}
			else
			{
				//deshabilito los botones
				cbImpresora.Enabled = true;
				cbPantalla.Enabled = true;
			}

		}
	}
}