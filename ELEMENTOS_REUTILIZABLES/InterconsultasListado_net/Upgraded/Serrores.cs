using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.DB.DAO;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeHelpers.VB;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace InforInterco
{
	internal static class Serrores
	{


		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("shell32.dll", EntryPoint = "ShellExecuteA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int ShellExecute(int hwnd, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpOperation, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpFile, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpParameters, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpDirectory, int nShowCmd);

		private static bool fcomprobado = false;
		private static bool fexiste = false;

		private const int WebMultiUse = -1; //True
		private const int WebPersistable = 0; //NotPersistable
		private const int WebDataBindingBehavior = 0; //vbNone
		private const int WebDataSourceBehavior = 0; //vbNone
		private const int WebMTSTransactionMode = 0; //NotAnMTSObject

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("urlmon.dll", EntryPoint = "URLDownloadToFileA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int URLDownloadToFile(int pCaller, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szURL, [MarshalAs(UnmanagedType.VBByRefStr)] ref string szFileName, int dwReserved, int lpfnCB);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("wininet.dll", EntryPoint = "InternetOpenA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int InternetOpen([MarshalAs(UnmanagedType.VBByRefStr)] ref string sAgent, int lAccessType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sProxyName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sProxyBypass, int lFlags);


		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("wininet.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static short InternetCloseHandle(int hInet);


		const int INTERNET_OPEN_TYPE_PRECONFIG = 0;
		const int INTERNET_FLAG_EXISTING_CONNECT = 0x20000000;
		const int INTERNET_OPEN_TYPE_DIRECT = 1;
		const int INTERNET_OPEN_TYPE_PROXY = 3;
		static readonly int INTERNET_FLAG_RELOAD = unchecked((int) 0x80000000);

		// JavierC   19/07/2000
		public static string vstFormatoSOLOFechaBD = String.Empty; //formato de solo la fecha en la base de datos sin hora
		public static string vstFormatoFechaHoraMinBD = String.Empty; //formato de la fecha en la base de datos con hh:nn
		public static string vstFormatoFechaHoraMinSegBD = String.Empty; //formato de la fecha en la base de datos con hh:nn:ss
		public static string VstTipoBD = String.Empty; //Tipo de base de datos con la que se trabaja

		//oscar 10/10/2002
		//Diferenciacion estre Sintaxis SQL y ORACLE
		public static string QLongitud = String.Empty; //(len / length)
		public static string QFechaBD = String.Empty; //(getdate() / SYSDATE)
		public static string QEsNulo = String.Empty; // (Isnull / NVL)
		//------

		public static bool vfMostrandoHistoricos = false;

		public static string vNomAplicacion = String.Empty;
		public static string vAyuda = String.Empty;
		public static DataSet RsError = null; //�es necesario que sea Public?
		public static Mensajes.ClassMensajes oClassErrores = null; //en ejecuci�n se le asignar� CreateObject("Mensajes.ClassMensajes")
		public static object oClass = null;
		public const int MAX_COMPUTERNAME_LENGTH = 20;

		//*********************
		static int iDevolver = 0;

		//0   Exit (Sub,Function,...)
		//1   Resume next
		//2   Resume
		//********************
		public static int iresume = 0;
		static string stPresentacion = String.Empty;
		static int lvarNum = 0;
		static string stvarDes = String.Empty;
		static int lCodigoError = 0;
		static DataSet Rrtitulo = null;
		static SqlConnection RcError = null;

		//*********************CRIS*******************

		//Constantes de errores de disquete
		const int mnErrDeviceUnavailable = 68;
		const int mnErrDiskNotReady = 71;
		const int mnErrDeviceIO = 57;
		const int mnErrDiskFull = 61;
		const int mnErrBadFileName = 64;
		const int mnErrBadFileNameOrNumber = 52;
		const int mnErrPathDoesNotExist = 76;
		const int mnErrBadFileMode = 54;
		const int mnErrFileAlreadyOpen = 55;
		const int mnErrInputPastEndOfFile = 62;
		const int mnErrAccesoRutaArchivo = 75;
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", EntryPoint = "DeleteFileA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int DeleteFile([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpFileName);
		public const int Tama�oFicheroLog = 512000;
		public static readonly string ERRORES_DE_MSRDO20_LOCK = "172.606.365.801.816.817.818.819.1110." + "1113.1114.1115.1201.1202.1203.1204.1205.1206.1207.1208.1210.1211.1219. " + "1510.2613.2621.2625.2755.2807.3306.3307.3308.3309.5147.5805.5904.7108." + "7112.16941.16946.16964";
		public static readonly string ERRORES_DE_MSRDO20_INSERT = "109.110.120.121.199.213.273.286." + "434.511.513.515.530.544.546.550.619.640.1009.1020.1030.1538.2109.2601." + "2603.2615.2619.2622.2624.2626.2627.2710.3009.4609.4801.4802.4805.8101.8107.8164.15056.15095.16826";
		public const string ERRORES_DE_MSRDO20_DELETE = "286.619.629.631.640.1020.1507.2109.4609.7917.7918.14061";
		public static readonly string ERRORES_DE_MSRDO20_UPDATE = "140.157.190.259.272.277.286.412.511.513.532.550.611.619." + "620.627.641.642.1706.1758.1764.2589.2610.2710.3206.3430.3609.3734.5808.6204.6205.8102.14025.14053" + "14054.14060.14069.14070.14077.14217.14232.14246.15099.15143.15265.15286.15357.15372.15391.16851.16928.16932.16941";
		public static readonly string ERRORES_DE_MSRDO20_INTEGRIDAD = "427.428.429.433.434.440.547.549.1756.1759.1782.1783.1784.2627.3723.3725." + "3730.3733.3735.4917.4918.4919.6231.8111.8135.8136.8141.8148.8151.8166";
		public static readonly string ERRORES_DE_DAO_ARCHIVO = "3024.3176.3179.3180.3182.3256.3418.3436.3441.3442.3549.20477.31036.31037" + "3120.3290.3293.3295";
		public const string ERRORES_DE_DAO_BLOQUEO = "455.3052.3225.3009.3418.536.541.3211.3212.3254.3262";
		public const string ERRORES_DE_DAO_INSERT = "3084.3107.3127.3129.3134.3155.3352";
		public const string ERRORES_DE_DAO_DELETE = "3109.3129.3130.3156";
		public const string ERRORES_DE_DAO_UPDATE = "3020.3106.3108.3129.3144.3157";
		public const string ERRORES_DE_DAO_INDICES = "3015.3264.3269.3291.3294.3295.3374.3393";
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", EntryPoint = "GetComputerNameA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetComputerName([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpbuffer, ref int nSize);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("advapi32.dll", EntryPoint = "GetUserNameA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetUserName([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpbuffer, ref int nSize);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", EntryPoint = "GetProfileStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetProfileString([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpAppName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpKeyName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpDefault, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpReturnedString, int nSize);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", EntryPoint = "WriteProfileStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int WriteProfileString([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszSection, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszKeyName, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszString);

		private const int HWND_BROADCAST = 0xFFFF;
		private const int WM_WININICHANGE = 0x1A;
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int SendMessage(int hwnd, int wMsg, int wParam, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lParam);




		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int ProcessIdToSessionId(int dwProcessId, ref int pSessionId);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetCurrentProcessId();
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("ODBCCP32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int SQLConfigDataSource(int hwndParent, int fRequest, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszDriver, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszAttributes);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("gdi32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int Escape(int hdc, int nEscape, int nCount, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpInData, System.IntPtr lpOutData);

		private const string COMANDO_IDIOMA = "I8,A,034";
		private const string COMANDO_INICIO = "N";
		private const string COMANDO_HOSPITAL = "A210,50,1,4,1,1,N,";
		private const string COMANDO_PACIENTE = "A180,50,1,4,1,1,N,";
		private const string COMANDO_HISTORIA = "A150,50,1,3,1,1,N,";
		private const string COMANDO_EDAD = "A120,50,1,3,1,1,N,";
		private const string COMANDO_SEXO = "A120,190,1,3,1,1,N,";
		private const string COMANDO_LITERAL_ALERTAS = "A80,50,1,3,1,1,N,";
		private const string COMANDO_ALERTAS = "A80,190,1,3,1,1,N,";
		private const string COMANDO_FIN = "P1";

		//carlos: para las dlls que se hagan conexiones aparte. las defino como VV porque supongo
		//que no habr� ninguna as�

		public static string VVstDriver = String.Empty;
		public static string VVstBasedatosActual = String.Empty;
		public static string VVstServerActual = String.Empty;
		public static string VVstUsuarioBD = String.Empty;
		public static string VVstPasswordBD = String.Empty;
		public static string VVstUsuarioApli = String.Empty;

		public static string[] VarRPT = null; //PARA GARBAR LOS MENSAJES DEL MULTILENGUAJE
		public static string[] VarFECHA = null; // PARA GARABAR LOS DIAS DE LA SEMANA Y LOS MESES
		public static string CampoIdioma = String.Empty; //sconsglo = 'idioma' . para ver q idioma esta en la aplicacion

		//******************************************************************************************
		//*******************      AYUDA A LA CODIFICACI�N EN ARCHIVO       ************************
		//******************************************************************************************

		public static string stCMD = String.Empty;
		public static string stPAY = String.Empty;
		public static string stSexHombre = String.Empty;
		public static string stSexMujer = String.Empty;


		public static int Codefinder = 0;
		public static int DLLInstance = 0;
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("CFINT32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static string sVBReadPacket(int Instance);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("CFINT32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int nReleaseInstance(int Instance);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("KERNEL32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int FreeLibrary(int hLibModule);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("advapi32.dll", EntryPoint = "RegQueryValueExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int RegQueryValueExNull(int hKey, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpValueName, int lpReserved, ref int lpType, int lpdata, ref int lpcbData);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("advapi32.dll", EntryPoint = "RegOpenKeyExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int RegOpenKeyEx(int hKey, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpSubKey, int ulOptions, int samDesired, ref int phkResult);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("advapi32.dll", EntryPoint = "RegQueryValueExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int RegQueryValueExStr(int hKey, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpValueName, int lpReserved, ref int lpType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpdata, ref int lpcbData);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("advapi32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int RegCloseKey(int hKey);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("KERNEL32.DLL", EntryPoint = "LoadLibraryA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int LoadLibrary([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLibFileName);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("CFINT32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int nVBInitInstance(int hCmdButton, int EventMessage);
		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("CFINT32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int nWritePacket([MarshalAs(UnmanagedType.VBByRefStr)] ref string InPacket, int Instance);
		//******************************************************************************************
		//******************* Variables utilizadas en la codificaci�n con el CodFinder *************

		public static double dPesoGDR = 0;
		public static int iGRD = 0;
		private struct diagnostico
		{
			public string CodDiag;
			public string codPOA;
			public static diagnostico CreateInstance()
			{
					diagnostico result = new diagnostico();
					result.CodDiag = String.Empty;
					result.codPOA = String.Empty;
					return result;
			}
		}
		//******************************************************************************************

		// Literales de la edad
		private static string litAno = String.Empty;
		private static string litAnos = String.Empty;
		private static string litMes = String.Empty;
		private static string litMeses = String.Empty;
		private static string litDia = String.Empty;
		private static string litDias = String.Empty;
		//--------------------------
		public struct stEpisodios
		{
			public string stiTipoSer;
			public short iA�o;
			public int lNum;
			public string stFecha;
			public string stPrestacion;
			public string sttipoEstsoli;
			public short iA�oNew;
			public int lNumNew;
			public bool bPetHist;
			public string stPrestActual;
			public static stEpisodios CreateInstance()
			{
					stEpisodios result = new stEpisodios();
					result.stiTipoSer = String.Empty;
					result.stFecha = String.Empty;
					result.stPrestacion = String.Empty;
					result.sttipoEstsoli = String.Empty;
					result.stPrestActual = String.Empty;
					return result;
			}
		}

		private static Serrores.stEpisodios[] Episodios = null;

		private static bool bSemillaIniciada = false;

		public static string[] stRutasPDF = null;

		internal static int AnalizaError(string Modulo, string Camino, string usuario, string Comentario, SqlConnection Conexion, bool bOcultarMensajes = false)
		{
			string texto = String.Empty;

			//bOcultarMensajes = false (por defecto desde todos los lugares) hace que se muestren los mensajes de error
			//bOcultarMensajes = true (desde generaPDF_InfAltaEnfermeria) hace que no se muestre los mensajes de error ya que es un .exe desatendido

			// *************  CON ESTO NOS ASEGURAMOS QUE UN ERROR PRODUCIDO POR UN COMMONDIALOG
			// *************  AL PULSAR CANCELAR NO GENERE UN FICHERO DE LOG
			//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
			if (Information.Err().Number == 32755 || Information.Err().Number == 20545 || Information.Err().Number == 0)
			{
				return 0;
			}
			int vuelta = 1;
			bool primera = true;

			string Fichero = ObtenerFichero(Modulo, Camino);
			string Fuente = Information.Err().Source;
			int Encontrado = 0;
			//comprobar que no tiene una clase
			Encontrado = (Fuente.IndexOf('.') + 1);
			if (Encontrado != 0)
			{
				Fuente = Fuente.Substring(0, Math.Min(Encontrado - 1, Fuente.Length));
			}
			if (Fuente == "DAO")
			{
				AnalizaErrorDAO(Modulo, Camino, usuario, Comentario, Fuente, Fichero, bOcultarMensajes);
			}
			else if (Fuente == "MSRDO20")
			{ 
				AnalizaErrorRDO(Modulo, Camino, usuario, Comentario, Fuente, Fichero, bOcultarMensajes);
			}
			else if (Fuente == "MicrosoftRDO")
			{ 
				AnalizaErrorRDO(Modulo, Camino, usuario, Comentario, Fuente, Fichero, bOcultarMensajes);
			}
			else if (Fuente == Modulo.ToUpper())
			{ 
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				texto = CreaError(usuario, Fuente, Information.Err().Number, Information.Err().Description, Comentario);
				GrabaLog(texto, Fichero);
				if (primera && !bOcultarMensajes)
				{
					//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
					MessageBox.Show("Error de aplicaci�n, N�mero :" + Conversion.Str(Information.Err().Number) + ":" + Information.Err().Description + "\r" + " Consulte el fichero de log " + Fichero, Application.ProductName);
				}
			}
			else if (Fuente == "CrystalReport")
			{ 
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				texto = CreaError(usuario, Fuente, Information.Err().Number, Information.Err().Description, Comentario);
				GrabaLog(texto, Fichero);
				if (primera && !bOcultarMensajes)
				{
					//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
					primera = BuscaError(Information.Err().Number);
					//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
					MessageBox.Show("Error de CrystalReport, N�mero :" + Conversion.Str(Information.Err().Number) + ":" + Information.Err().Description + "\r" + " Consulte el fichero de log " + Fichero + "\r", Application.ProductName);
				}
			}
			else
			{
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				texto = CreaError(usuario, Fuente, Information.Err().Number, Information.Err().Description, Comentario);
				GrabaLog(texto, Fichero);
				if (primera && !bOcultarMensajes)
				{
					//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
					MessageBox.Show("Error de aplicaci�n, N�mero :" + Conversion.Str(Information.Err().Number) + ":" + Information.Err().Description + "\r" + " Consulte el fichero de log " + Fichero, Application.ProductName);
				}
			}
			return vuelta;
		}
		//UPGRADE_ISSUE: (2068) error object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		internal static void AnalizaErrorDAO(string Modulo, string Camino, string usuario, string Comentario, string Fuente, string Fichero, bool bOcultarMensajes = false)
		{
			string texto = String.Empty;
			//UPGRADE_ISSUE: (2068) error object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			//fichero = ObtenerFichero(Modulo, Camino)
			bool primera = true;


			//bOcultarMensajes = false (por defecto desde todos los lugares) hace que se muestren los mensajes de error
			//bOcultarMensajes = true (desde generaPDF_InfAltaEnfermeria) hace que no se muestre los mensajes de error
			if (bOcultarMensajes)
			{
				//UPGRADE_ISSUE: (2064) DAO.Errors method DBEngine.Errors was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				foreach (UpgradeStubs.DAO_Error DAOERR in UpgradeStubs.UpgradeHelpers_DB_DAO_DBEngineHelper.getErrors())
				{
					//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Description was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Number was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					texto = CreaError(usuario, Fuente, DAOERR.getNumber(), DAOERR.getDescription(), Comentario);
					GrabaLog(texto, Fichero);
				}
				return;
			}
			//------

			//UPGRADE_ISSUE: (2064) DAO.Errors method DBEngine.Errors was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			foreach (UpgradeStubs.DAO_Error DAOERR in UpgradeStubs.UpgradeHelpers_DB_DAO_DBEngineHelper.getErrors())
			{
				//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Description was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Number was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				texto = CreaError(usuario, Fuente, DAOERR.getNumber(), DAOERR.getDescription(), Comentario);
				GrabaLog(texto, Fichero);
				//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Number was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				primera = BuscaError(DAOERR.getNumber());
				//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Number was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				if (ERRORES_DE_DAO_ARCHIVO.IndexOf(Conversion.Str(DAOERR.getNumber()).Trim()) >= 0)
				{
					MessageBox.Show("Error al crear o modificar la tabla temporal , Consulte el fichero de log " + Fichero, Application.ProductName);
					primera = false;
				}
				//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Number was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				if (ERRORES_DE_DAO_BLOQUEO.IndexOf(Conversion.Str(DAOERR.getNumber()).Trim()) >= 0)
				{
					MessageBox.Show("Error de bloqueo en la tabla temporal, Consulte el fichero de log " + Fichero, Application.ProductName);
					primera = false;
				}
				//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Number was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				if (ERRORES_DE_DAO_INSERT.IndexOf(Conversion.Str(DAOERR.getNumber()).Trim()) >= 0)
				{
					MessageBox.Show("Error insertando en la tabla temporal, Consulte el fichero de log " + Fichero, Application.ProductName);
					primera = false;
				}
				//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Number was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				if (ERRORES_DE_DAO_DELETE.IndexOf(Conversion.Str(DAOERR.getNumber()).Trim()) >= 0)
				{
					MessageBox.Show("Error de borrado de la tabla temporal, Consulte el fichero de log " + Fichero, Application.ProductName);
					primera = false;
				}
				//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Number was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				if (ERRORES_DE_DAO_UPDATE.IndexOf(Conversion.Str(DAOERR.getNumber()).Trim()) >= 0)
				{
					MessageBox.Show("Error de modificaci�n en la tabla temporal, Consulte el fichero de log " + Fichero, Application.ProductName);
					primera = false;
				}
				//UPGRADE_ISSUE: (2064) DAO.Error property DAOERR.Number was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				if (ERRORES_DE_DAO_INDICES.IndexOf(Conversion.Str(DAOERR.getNumber()).Trim()) >= 0)
				{
					MessageBox.Show("Error de Indices en la tabla temporal, Consulte el fichero de log " + Fichero, Application.ProductName);
					primera = false;
				}
			}
			if (primera)
			{
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				MessageBox.Show("Error de aplicaci�n, N�mero :" + Conversion.Str(Information.Err().Number) + ":" + Information.Err().Description + "\r" + " Consulte el fichero de log " + Fichero, Application.ProductName);
			}
		}

		internal static void AnalizaErrorRDO(string Modulo, string Camino, string usuario, string Comentario, string Fuente, string Fichero, bool bOcultarMensajes = false)
		{
			string texto = String.Empty;
			//fichero = ObtenerFichero(Modulo, Camino)

			//bOcultarMensajes = false (por defecto desde todos los lugares) hace que se muestren los mensajes de error
			//bOcultarMensajes = true (desde generaPDF_InfAltaEnfermeria) hace que no se muestre los mensajes de error
			if (bOcultarMensajes)
			{
				foreach (SqlError RDOerr in UpgradeSupport.RDO_rdoEngine_definst.rdoErrors)
				{
					texto = CreaError(usuario, Fuente, RDOerr.Number, RDOerr.Message, Comentario);
					GrabaLog(texto, Fichero);
				}
				return;
			}
			//------

			bool primera = true;
			foreach (SqlError RDOerr in UpgradeSupport.RDO_rdoEngine_definst.rdoErrors)
			{
				texto = CreaError(usuario, Fuente, RDOerr.Number, RDOerr.Message, Comentario);
				GrabaLog(texto, Fichero);
				if (primera)
				{
					primera = BuscaError(RDOerr.Number);
					if (ERRORES_DE_MSRDO20_LOCK.IndexOf(Conversion.Str(RDOerr.Number).Trim()) >= 0)
					{
						MessageBox.Show("Error de bloqueo, Consulte el fichero de log " + Fichero, Application.ProductName);
						primera = false;
					}
					if (ERRORES_DE_MSRDO20_INSERT.IndexOf(Conversion.Str(RDOerr.Number).Trim()) >= 0)
					{
						MessageBox.Show("Error de insert, Consulte el fichero de log " + Fichero, Application.ProductName);
						primera = false;
					}
					if (ERRORES_DE_MSRDO20_DELETE.IndexOf(Conversion.Str(RDOerr.Number).Trim()) >= 0)
					{
						MessageBox.Show("Error de delete, Consulte el fichero de log " + Fichero, Application.ProductName);
						primera = false;
					}
					if (ERRORES_DE_MSRDO20_UPDATE.IndexOf(Conversion.Str(RDOerr.Number).Trim()) >= 0)
					{
						MessageBox.Show("Error de update, Consulte el fichero de log " + Fichero, Application.ProductName);
						primera = false;
					}
					if (ERRORES_DE_MSRDO20_INTEGRIDAD.IndexOf(Conversion.Str(RDOerr.Number).Trim()) >= 0)
					{
						MessageBox.Show("Error de integridad, Consulte el fichero de log " + Fichero, Application.ProductName);
						primera = false;
					}
				}
			}
			if (primera)
			{
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				MessageBox.Show("Error de aplicaci�n, N�mero :" + Conversion.Str(Information.Err().Number) + ":" + Information.Err().Description + "\r" + " Consulte el fichero de log " + Fichero, Application.ProductName);
			}
		}

		internal static void Borrar_DSN(string DSN_SQL, string DSN_ACCESS)
		{
			//procedimiento que borra los dsn
			int ODBC_REMOVE_SYS_DSN = 0;
			int lRet = 0;
			string sdrv = String.Empty;
			int vbAPINull = 0;
			try
			{
				vbAPINull = 0;
				ODBC_REMOVE_SYS_DSN = 3;

				sdrv = "SQL Server";

				string tempRefParam = "DSN=" + DSN_SQL + Strings.Chr(0).ToString();
				lRet = InterconsultasListadoSupport.PInvoke.SafeNative.odbccp32.SQLConfigDataSource(vbAPINull, ODBC_REMOVE_SYS_DSN, ref sdrv, ref tempRefParam);

				sdrv = "Microsoft Access Driver (*.mdb)";

				string tempRefParam2 = "DSN=" + DSN_ACCESS + Strings.Chr(0).ToString();
				lRet = InterconsultasListadoSupport.PInvoke.SafeNative.odbccp32.SQLConfigDataSource(vbAPINull, ODBC_REMOVE_SYS_DSN, ref sdrv, ref tempRefParam2);
			}
			catch
			{

				//UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
				UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
			}

		}

		internal static int ConexionTS()
		{
			int result = 0;
			int pid = 0;
			int sid = 0;
			int ret = 0;
			try
			{
				//esta funci�n devolver� la conexion TS o "0" si es local
				pid = InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.GetCurrentProcessId();

				ret = InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.ProcessIdToSessionId(pid, ref sid);

				return sid;
			}
			catch (Exception e)
			{
				//si el error es 453 no estamos en TS y devolvemos un 0
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 453)
				{
					result = 0;
				}

				return result;
			}
		}

		internal static void CrearCadenaDSN(ref string DSN_SQL, ref string DSN_ACCESS, bool registrarDSN = false, string BaseAccess = "", string nombreDSN = "")
		{

			//devuelve los valores de las cadenas de conexion de los DSN

			string tstMAquina = String.Empty;
			string cadenaConexion = String.Empty;
			string Servidor = String.Empty;
			string driver = String.Empty;
			string BaseDatos = String.Empty;
			string usuario = String.Empty;
			string Password = String.Empty;
			int digitoLlave = 0;

			//GrabaLog "Se crea el objeto Conexion.", "C:\ListadoValoracion.txt"

			Conexion.Obtener_conexion oIns = new Conexion.Obtener_conexion();


			if (nombreDSN == "")
			{
				tstMAquina = oIns.NonMaquina();
				DSN_SQL = "G_" + ConexionTS().ToString() + "_" + tstMAquina;
				DSN_ACCESS = "GA_" + ConexionTS().ToString() + "_" + tstMAquina;
			}
			else
			{
				DSN_SQL = "G_" + nombreDSN;
				DSN_ACCESS = "GA_" + nombreDSN;
			}

			//GrabaLog "Los nombre de los DSN son " & DSN_SQL & " / " & DSN_ACCESS, "C:\ListadoValoracion.txt"


			if (registrarDSN)
			{

				//' Registramos el DSN de SQL
				string tempRefParam = "SERVIDOR";
				Servidor = oIns.fnQueryRegistro(ref tempRefParam);
				string tempRefParam2 = "DRIVER";
				driver = oIns.fnQueryRegistro(ref tempRefParam2);
				string tempRefParam3 = "BASEDATOS";
				BaseDatos = oIns.fnQueryRegistro(ref tempRefParam3);
				string tempRefParam4 = "INICIO";
				usuario = oIns.fnQueryRegistro(ref tempRefParam4);
				Password = "Indra";

				//    GrabaLog "Los datos para la creacion del DSN son:", "C:\ListadoValoracion.txt"
				//    GrabaLog "Los datos para la creacion del servidor " & servidor, "C:\ListadoValoracion.txt"
				//    GrabaLog "Los datos para la creacion del driver " & driver, "C:\ListadoValoracion.txt"
				//    GrabaLog "Los datos para la creacion del BaseDatos " & BaseDatos, "C:\ListadoValoracion.txt"
				//    GrabaLog "Los datos para la creacion del usuario " & usuario, "C:\ListadoValoracion.txt"
				//    GrabaLog "Los datos para la creacion del password " & password, "C:\ListadoValoracion.txt"
				//


				cadenaConexion = "Description=SQL Server" + "\r" + 
				                 "OemToAnsi=No" + "\r" + 
				                 "SERVER=" + Servidor + "" + "\r" + 
				                 "Database=" + BaseDatos + "";

				//    GrabaLog "La cadena de conexion SQL es " & cadenaConexion, "C:\ListadoValoracion.txt"


				digitoLlave = (driver.IndexOf('{') + 1);
				driver = driver.Substring(digitoLlave);
				digitoLlave = (driver.IndexOf('}') + 1);
				driver = driver.Substring(0, Math.Min(digitoLlave - 1, driver.Length));

				//UPGRADE_TODO: (1065) Error handling statement (On Error Goto) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
				UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (ErrDsn)");

				//    GrabaLog Format(Now, "dd/mm/yyyy hh:nn:ss") & " Voy a crear el ODB SQL ", "C:\ListadoValoracion.txt"

				//UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(DSN_SQL, driver, true, cadenaConexion);


				//' Registramos el DSN de ACCESS
				cadenaConexion = "Description=Microsoft Access" + "\r" + 
				                 "OemToAnsi=No" + "\r" + 
				                 "SERVER=(local)" + "" + "\r" + 
				                 "dbq=" + BaseAccess + "";

				//    GrabaLog "La cadena de conexion ACCES es  " & cadenaConexion, "C:\ListadoValoracion.txt"

				//UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(DSN_ACCESS, "MicroSoft Access Driver (*.mdb)", true, cadenaConexion);

			}

			oIns = null;

ErrDsn:;
			//    GrabaLog Format(Now, "dd/mm/yyyy hh:nn:ss") & "  " & Err.Description, "C:\ListadoValoracion.txt"

		}



		internal static void EliminaDSN(string nombreDSN)
		{


			int ODBC_REMOVE_DSN = 3; // 3 ' Remove data source // Remove data source
			int vbAPINull = 0; // 0& ' NULL Pointer // NULL Pointer

			string strAttributes = "DSN=" + "G_" + nombreDSN + Strings.Chr(0).ToString();
			string tempRefParam = "SQL Server";
			int intRet = InterconsultasListadoSupport.PInvoke.SafeNative.odbccp32.SQLConfigDataSource(vbAPINull, ODBC_REMOVE_DSN, ref tempRefParam, ref strAttributes);

			strAttributes = "DSN=" + "GA_" + nombreDSN + Strings.Chr(0).ToString();
			string tempRefParam2 = "MicroSoft Access Driver (*.mdb)";
			intRet = InterconsultasListadoSupport.PInvoke.SafeNative.odbccp32.SQLConfigDataSource(vbAPINull, ODBC_REMOVE_DSN, ref tempRefParam2, ref strAttributes);
		}



		internal static string DevuelveNombreAccess(string Modulo)
		{
			//devuelve los valores de las cadenas de conexion de los DSN
			Conexion.Obtener_conexion oIns = new Conexion.Obtener_conexion();
			string tstMAquina = oIns.NonMaquina();
			return tstMAquina + Modulo + ".MDB";


		}

		internal static void Datos_Conexion(SqlConnection vConexion)
		{
			//Procedimiento que buscara en la tabla SSESUSUA CON LOS DATOS DE SESION TS, USUARIO NT, PC
			//los datos necesarios para establecer la conexion
			//para la busqueda   idconexi
			//                   dusuarnt
			//                   puestopc
			//saco               gusuario
			//                   basedato
			//                   servidor
			//                   usuariobd
			//                   pwbaseda

			string tstDatos = String.Empty;
			string usuarioconectado = String.Empty;

			//obtengo idconexi
			int tlIdConexion = ConexionTS();
			//obtengo UsuarioNT
			string tstUsuNT = obtener_usuario();
			//obtengo nombre maquina

			Conexion.Obtener_conexion tInstancia = new Conexion.Obtener_conexion();
			string tstMAquina = tInstancia.NonMaquina();
			tInstancia = null;

			string tmpUsuario = String.Empty;
			int indice = 0;
			bool Salir = false;

			//UPGRADE_ISSUE: (2064) RDO.rdoConnection property vConexion.Connect was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			if (vConexion.getConnect().IndexOf("APP=") >= 0)
			{
				//UPGRADE_ISSUE: (2064) RDO.rdoConnection property vConexion.Connect was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				usuarioconectado = vConexion.getConnect().Substring(Strings.InStr(vConexion.getConnect().IndexOf("APP=") + 1, vConexion.getConnect(), "\\", CompareMethod.Binary), Math.Min(Strings.InStr(vConexion.getConnect().IndexOf("APP=") + 1, vConexion.getConnect(), ";", CompareMethod.Binary) - Strings.InStr(vConexion.getConnect().IndexOf("APP=") + 1, vConexion.getConnect(), "\\", CompareMethod.Binary) - 1, vConexion.getConnect().Length - Strings.InStr(vConexion.getConnect().IndexOf("APP=") + 1, vConexion.getConnect(), "\\", CompareMethod.Binary)));
				//UPGRADE_ISSUE: (2064) RDO.rdoConnection property vConexion.Connect was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				tmpUsuario = vConexion.getConnect().Substring(vConexion.getConnect().IndexOf("APP=") + 4, Math.Min(Strings.InStr(vConexion.getConnect().IndexOf("APP=") + 1, vConexion.getConnect(), ";", CompareMethod.Binary) - (vConexion.getConnect().IndexOf("APP=") + 1) - 4, vConexion.getConnect().Length - (vConexion.getConnect().IndexOf("APP=") + 4)));
				indice = 1;
				Salir = false;

				if (tmpUsuario.IndexOf('\\') >= 0)
				{
					// Buscamos la �ltima \

					while (!Salir)
					{
						if (Strings.InStr(indice, tmpUsuario, "\\", CompareMethod.Binary) > 0)
						{
							indice = Strings.InStr(indice, tmpUsuario, "\\", CompareMethod.Binary) + 1;
						}
						else
						{
							Salir = true;
						}
					}

					usuarioconectado = tmpUsuario.Substring(indice - 1);
				}
				else if (tmpUsuario.IndexOf('.') >= 0)
				{ 
					// Buscamos la �ltima \

					while (!Salir)
					{
						if (Strings.InStr(indice, tmpUsuario, ".", CompareMethod.Binary) > 0)
						{
							indice = Strings.InStr(indice, tmpUsuario, ".", CompareMethod.Binary) + 1;
						}
						else
						{
							Salir = true;
						}
					}

					usuarioconectado = tmpUsuario.Substring(indice - 1);
				}
				else
				{
					usuarioconectado = "";
				}

				if (tstMAquina == "")
				{
					tstMAquina = usuarioconectado;
				}

				tstDatos = "select * from SSESUSUA where idconexi=" + tlIdConexion.ToString() + " and dusuarnt='" + tstUsuNT + "' " + " and puestopc='" + tstMAquina + "'" + " and gusuario = '" + usuarioconectado + "'";
			}
			else
			{
				tstDatos = "select * from SSESUSUA where idconexi=" + tlIdConexion.ToString() + " and dusuarnt='" + tstUsuNT + "' " + " and puestopc='" + tstMAquina + "'";
			}




			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstDatos, vConexion);
			DataSet tRrDatos = new DataSet();
			tempAdapter.Fill(tRrDatos);
			if (tRrDatos.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VVstDriver = Convert.ToString(tRrDatos.Tables[0].Rows[0]["ddriver"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VVstBasedatosActual = Convert.ToString(tRrDatos.Tables[0].Rows[0]["basedato"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VVstServerActual = Convert.ToString(tRrDatos.Tables[0].Rows[0]["servidor"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VVstUsuarioBD = Convert.ToString(tRrDatos.Tables[0].Rows[0]["usuaribd"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VVstPasswordBD = Convert.ToString(tRrDatos.Tables[0].Rows[0]["pwbaseda"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VVstUsuarioApli = Convert.ToString(tRrDatos.Tables[0].Rows[0]["gusuario"]).Trim();
			}

			//desencripto la pasword
			VVstPasswordBD = General_DesencriptarPassword(ref VVstPasswordBD).Trim();

		}
		internal static string General_DesencriptarPassword(ref string stContrase�a)
		{

			int[] arraydesp = new int[9];
			int[] entrada = new int[9];
			StringBuilder stDevolucion = new StringBuilder();

			arraydesp[1] = 10;
			arraydesp[2] = 13;
			arraydesp[3] = 31;
			arraydesp[4] = 14;
			arraydesp[5] = 1;
			arraydesp[6] = 22;
			arraydesp[7] = 34;
			arraydesp[8] = 8;

			if (stContrase�a.Length < 8)
			{
				stContrase�a = stContrase�a + new String(' ', 8 - stContrase�a.Length);
			}

			//A PARTIR DE AQUI, LA DESENCRIPTACION
			for (int I = 1; I <= 8; I++)
			{
				entrada[I] = Strings.Asc(stContrase�a.Substring(I - 1, Math.Min(1, stContrase�a.Length - (I - 1)))[0]);
				if (entrada[I] == 32)
				{
					entrada[I] = 1;
				}
				else
				{
					if (entrada[I] >= 48 && entrada[I] <= 57)
					{
						entrada[I] -= 46;
					}
					else
					{
						if (entrada[I] >= 65 && entrada[I] <= 90)
						{
							entrada[I] -= 53;
						}
						else
						{
							entrada[I] = 38;
						}
					}
				}
				entrada[I] -= arraydesp[I];
				if (entrada[I] < 0)
				{
					entrada[I] += 38;
				}
				if (entrada[I] == 1)
				{
					entrada[I] = 32;
				}
				else
				{
					if (entrada[I] >= 2 && entrada[I] <= 11)
					{
						entrada[I] += 46;
					}
					else
					{
						if (entrada[I] >= 12 && entrada[I] <= 37)
						{
							entrada[I] += 53;
						}
						else
						{
							entrada[I] = 209;
						}
					}
				}
				stDevolucion.Append(Strings.Chr(entrada[I]).ToString());

			}

			return stDevolucion.ToString();

		}
		internal static string General_EncriptarPassword(ref string stContrase�a)
		{

			int[] arraydesp = new int[9];
			int[] entrada = new int[9];
			string[] salida = ArraysHelper.InitializeArray<string>(9);
			StringBuilder stDevolver = new StringBuilder();

			//el n�mero m�ximo de desplazamiento se debe fijar a 37
			//para que el cociente siempre sea 1 para poder
			//desencriptar partiendo del resto
			//(fijando el cociente siempre a 1).
			arraydesp[1] = 10;
			arraydesp[2] = 13;
			arraydesp[3] = 31;
			arraydesp[4] = 14;
			arraydesp[5] = 1;
			arraydesp[6] = 22;
			arraydesp[7] = 34;
			arraydesp[8] = 8;
			//Pasamos a may�sculas la contrase�a
			stContrase�a = stContrase�a.ToUpper();
			//metemos espacios a la derecha para almacenar como password siempre 8 caracteres
			if (stContrase�a.Length < 8)
			{
				stContrase�a = stContrase�a + new String(' ', 8 - stContrase�a.Length);
			}
			//vamos recortando la contrase�a palabra por palabra
			for (int I = 1; I <= 8; I++)
			{
				entrada[I] = Strings.Asc(stContrase�a.Substring(I - 1, Math.Min(1, stContrase�a.Length - (I - 1)))[0]);
				//'    'validar caracteres tecleados: solo se admite el espacio, n�meros, letras may�sculas, letras min�sculas, la � y la �
				//'    If Not (entrada(I) = 32 Or _
				//''        (entrada(I) >= 48 And entrada(I) <= 57) Or _
				//''        (entrada(I) >= 65 And entrada(I) <= 90) Or _
				//''        entrada(I) = 209) Then
				//'        Mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 1230, RcPrincipal, S01000F1.lbContrase�a, "letras y n�meros"
				//'        Exit Function
				//'    End If
				//se transforman los rangos de caracteres v�lidos en un rango de 1..38
				if (entrada[I] == 32)
				{
					entrada[I] = 1;
				}
				else
				{
					if (entrada[I] >= 48 && entrada[I] <= 57)
					{
						entrada[I] -= 46;
					}
					else
					{
						if (entrada[I] >= 65 && entrada[I] <= 90)
						{
							entrada[I] -= 53;
						}
						else
						{
							entrada[I] = 38;
						}
					}
				}
				//se le suma el desplazamiento y se obtiene el m�dulo 38
				entrada[I] += arraydesp[I];
				entrada[I] = entrada[I] % 38;
				//se transforma el ascii en su caracter
				if (entrada[I] == 1)
				{
					entrada[I] = 32;
				}
				else
				{
					if (entrada[I] >= 2 && entrada[I] <= 11)
					{
						entrada[I] += 46;
					}
					else
					{
						if (entrada[I] >= 12 && entrada[I] <= 37)
						{
							entrada[I] += 53;
						}
						else
						{
							entrada[I] = 209;
						}
					}
				}
				salida[I] = Strings.Chr(entrada[I]).ToString();
				stDevolver.Append(salida[I]);
			}

			return stDevolver.ToString();

		}


		internal static void GrabaLog(string paraTexto, string paraFichero, bool bConHora = false)
		{

			try
			{

				FileSystem.FileOpen(1, paraFichero, OpenMode.Append, OpenAccess.Default, OpenShare.Default, -1);

				FileSystem.WriteLine(1, ((bConHora) ? DateTime.Now.ToString("dd/MM/yyyy HH:NN:SS") + " ----> " : "") + paraTexto);

				FileSystem.FileClose(1);
			}
			catch
			{
			}


		}

		internal static string ObtenerFichero(string Paramodulo, string ParaCamino)
		{
			int X = 0;
			string Nomfichero = String.Empty;
			string[, ] Ficheroslog = ArraysHelper.InitializeArray<string[, ]>(new int[]{6, 3}, new int[]{0, 0});
			bool Encontrado = false;
			string NomCamino = ParaCamino + "\\" + Paramodulo;
			string Maxfecha = "01/01/1990 00:00:00";
			string MinFecha = "01/01/3000 00:00:00";
			for (X = 1; X <= 5; X++)
			{
				Nomfichero = NomCamino + Conversion.Str(X).Trim() + ".log";
				if (FileSystem.Dir(Nomfichero, FileAttribute.Normal) != "")
				{
					Encontrado = true;
					Ficheroslog[X, 1] = Nomfichero;
					Ficheroslog[X, 2] = DateTimeHelper.ToString((new FileInfo(Nomfichero)).LastWriteTime);
					if (DateTime.Parse(MinFecha) > DateTime.Parse(Ficheroslog[X, 2]))
					{
						MinFecha = Ficheroslog[X, 2];
					}
					if (DateTime.Parse(Maxfecha) < DateTime.Parse(Ficheroslog[X, 2]))
					{
						Maxfecha = Ficheroslog[X, 2];
					}
				}
				else
				{
					Ficheroslog[X, 2] = " ";
					Ficheroslog[X, 1] = " ";
				}
			}
			//si no existe ninguno es el primero
			//busca el fichero de mayor fecha
			//si el tama�o es mayor que 4000
			//busca el que no exista
			//si no hay ninguno busca el de menor fecha
			//lo borra y lo selecciona
			if (!Encontrado)
			{
				X = 1;
			}
			else
			{
				X = BuscaCadena(Ficheroslog, Maxfecha, 5, 2);
				if (((int) (new FileInfo(Ficheroslog[X, 1])).Length) > Tama�oFicheroLog)
				{
					X = BuscaCadena(Ficheroslog, " ", 5, 1);
					if (X == 0)
					{
						X = BuscaCadena(Ficheroslog, MinFecha, 5, 2);
						InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.DeleteFile(ref Ficheroslog[X, 1]);
					}
				}
			}
			Nomfichero = NomCamino + Conversion.Str(X).Trim();
			return Nomfichero + ".log";
		}
		internal static int BuscaCadena(string[, ] matriz, string cadena, int Tama�o, int posicion)
		{
			int X = 0;
			bool Encontrado = false;
			for (X = 1; X <= Tama�o; X++)
			{
				if (matriz[X, posicion] == cadena)
				{
					Encontrado = true;
					break;
				}
			}
			if (Encontrado)
			{
				return X;
			}
			else
			{
				return 0;
			}
		}

		internal static string CreaError(string ParaUsua, string ParaFuente, int ParaNumError, string ParaDesError, string ParaComentario)
		{
			return "Fecha : " + DateTime.Now.ToString("dd/MM/yyyy HH:NN:SS") + "\r" + "\n" + "Usuario : " + ParaUsua + "\r" + "\n" + "Fuente : " + ParaFuente + "\r" + "\n" + "Numero : " + Conversion.Str(ParaNumError) + "\r" + "\n" + "Descripci�n : " + ParaDesError + "\r" + "\n" + ParaComentario;
		}


		internal static string Sacar_PWD_FMS(ref string PWD_En)
		{
			//recibira una cadena con la pwd encriptada y devolver� la pwd en cristiano
			//troceo el par�metro PWD_En en trozos busacndo blancos
			StringBuilder tstPWD = new StringBuilder();
			int xx = 0;
			string pp = String.Empty;
			if (PWD_En.Substring(Math.Max(PWD_En.Length - 1, 0)) != " ")
			{
				PWD_En = PWD_En + " ";
			}
			int cc = 1;
			for (int tiFor = 1; tiFor <= PWD_En.Length; tiFor += 5)
			{
				if (xx < PWD_En.Length)
				{
					xx = Strings.InStr(cc, PWD_En, " ", CompareMethod.Binary);
					pp = PWD_En.Substring(cc - 1, Math.Min(xx - cc + 1, PWD_En.Length - (cc - 1)));
					tstPWD.Append(Decodificar_frase(ref pp));
					cc = xx + 1;
				}
			}
			return tstPWD.ToString();
		}

		internal static object ValidarTecla(ref int KeyAscii)
		{
			//Rutina para sustituir los acentos a la hora de guardar
			//en la base de datos y b�squeda
			switch(KeyAscii)
			{
				case 224 : case 225 : case 226 : case 227 : case 228 : case 229 : 
					KeyAscii = 97;  //a 
					//Beep 
					break;
				case 192 : case 193 : case 194 : case 195 : case 196 : case 197 : 
					KeyAscii = 65;  //A 
					//Beep 
					break;
				case 232 : case 233 : case 234 : case 235 : 
					KeyAscii = 101;  //e 
					//Beep 
					break;
				case 200 : case 201 : case 202 : case 203 : 
					KeyAscii = 69;  //E 
					//Beep 
					break;
				case 236 : case 237 : case 238 : case 239 : 
					KeyAscii = 105;  //i 
					//Beep 
					break;
				case 204 : case 205 : case 206 : case 207 : 
					KeyAscii = 73;  //I 
					//Beep 
					break;
				case 242 : case 243 : case 244 : case 245 : case 246 : 
					KeyAscii = 111;  //o 
					//Beep 
					break;
				case 210 : case 211 : case 212 : case 213 : case 214 : 
					KeyAscii = 79;  //O 
					//Beep 
					break;
				case 249 : case 250 : case 251 : 
					KeyAscii = 117;  //u 
					//Beep 
					break;
				case 217 : case 218 : case 219 : 
					KeyAscii = 85;  //U 
					//Beep 
					break;
				case 35 : case 37 : case 39 : case 63 : case 191 : 
					KeyAscii = 0;  //# % ' ? � 
					//Beep 
					break;
			}
			return null;
		}


		internal static int GestionErrorServidor(int lCodigoError, SqlConnection rcConexion)
		{
			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();

			lvarNum = lCodigoError;
			stvarDes = Conversion.ErrorToString(lCodigoError);
			short tempRefParam = 1010;
			object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorServidor"};
			oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2);
			Information.Err().Clear();
			iDevolver = 1;

			result = iDevolver;
			oClassErrores = null;

			return result;
		}

		internal static int GestionErrorRed(int lCodigoError, SqlConnection rcConexion)
		{

			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();

			lvarNum = lCodigoError;
			stvarDes = Conversion.ErrorToString(lCodigoError);
			short tempRefParam = 1010;
			object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorRed"};
			oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2);
			Information.Err().Clear();
			iDevolver = 1;

			result = iDevolver;
			oClassErrores = null;

			return result;
		}

		//Function AbrirBaseDatos()

		//On Error GoTo AbrirBaseDatosErr
		//***************************************************

		//Espacio reservado para escribir la B.D. buscada, si fuera necesario.

		//***************************************************
		//Desactiva el manejador de errores
		//On Error GoTo 0

		//Exit Function

		//AbrirBaseDatosErr:

		//   lCodigoError = Err
		//   iResume=GestionErrorBaseDatos (lCodigoError,"Conexi�n activa")
		//   Select Case iResume
		//   Case 0 ' resume
		//    Resume Next
		//   Case 1 ' resume next
		//    Resume
		//   End Select

		//Exit Function

		//End Function

		//Function CerrarBaseDatos()

		//On Error GoTo CerrarBaseDatosErr

		//Por ejemplo: RsError.Close

		//Desactiva el manejador de errores
		//On Error GoTo 0

		//Exit Function

		//CerrarBaseDatosErr:
		// lCodigoError = Err
		// iResume=GestionErrorBaseDatos (lCodigoError,"Conexi�n activa")
		//  Select Case iResume
		//   Case 0 ' resume
		//    Resume Next
		//   Case 1 ' resume next
		//    Resume
		//  End Select
		// Exit Function

		//End Function


		internal static int GestionErrorBaseDatos(int lCodigoError, SqlConnection rcConexion)
		{
			// Funcion que controla errores de base de datos
			// si queremos que luego haga resume next se manda un 1
			// si queremos que luego reintente (resume) se manda un 2
			// si queremos que finalize la ejecuci�n 0
			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();


			//************************ Ejemplos demostrativos *******************

			//Case 3004, 3007, 3263  'B.D. no encontrada
			// oClassErrores.RespuestaMensaje vNomAplicacion, vAyuda, 4500, RcError
			// Err = 0
			// idevolver = 1 'resume next

			//Case 3006, 3278, 3279 'Bloqueos o Permisos
			// oClassErrores.RespuestaMensaje vNomAplicacion, vAyuda,4600, RcError
			// Err = 0
			// idevolver = 1 'resume next

			switch(lCodigoError)
			{
				//********************************************************************
				//*********************************CRIS******************************
				case 208 :  //La tabla no existe en la Base de Datos 
					MessageBox.Show("La tabla no est� creada en la Base de Datos", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					//UPGRADE_ISSUE: (2064) RDO.rdoErrors method rdoEngine.rdoErrors.Clear was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
					UpgradeSupport.RDO_rdoEngine_definst.rdoErrors.Clear(); 
					iDevolver = 0;  //Exit 
					break;
				case 229 :  //La tabla existe en la Base de Datos, pero 
					//el usuario no tiene permisos 
					MessageBox.Show("No tiene permisos sobre la tabla", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					//UPGRADE_ISSUE: (2064) RDO.rdoErrors method rdoEngine.rdoErrors.Clear was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
					UpgradeSupport.RDO_rdoEngine_definst.rdoErrors.Clear(); 
					iDevolver = 0; 
					//******************************************************************** 
					break;
				default:
					lvarNum = lCodigoError; 
					stvarDes = Conversion.ErrorToString(lCodigoError); 
					short tempRefParam = 1010; 
					object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorBaseDatos"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2); 
					//UPGRADE_ISSUE: (2064) RDO.rdoErrors method rdoEngine.rdoErrors.Clear was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
					UpgradeSupport.RDO_rdoEngine_definst.rdoErrors.Clear(); 
					iDevolver = 1; 
					break;
			}

			result = iDevolver;
			oClassErrores = null;

			return result;
		}

		internal static int GestionErrorBloqueo(int lCodigoError, SqlConnection rcConexion)
		{
			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();

			//Ejemplos *************************

			lvarNum = lCodigoError;
			stvarDes = Conversion.ErrorToString(lCodigoError);
			short tempRefParam = 1010;
			object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorBloqueo"};
			oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2);
			Information.Err().Clear();
			iDevolver = 1;

			result = iDevolver;
			oClassErrores = null;
			return result;
		}

		internal static int GestionErrorDelete(int lCodigoError, SqlConnection rcConexion)
		{
			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();

			// Ejemplos***************************

			// Case 3086 'No puede borrar
			// oClassErrores.RespuestaMensaje vNomAplicacion, vAyuda, 4700, RcError
			// Err = 0

			//Case 3109 'Sin permiso para borrar
			//oClassErrores.RespuestaMensaje vNomAplicacion, vAyuda, 4800, RcError
			//Err = 0

			//Case 3167 ' Registro ya borrado.
			//oClassErrores.RespuestaMensaje vNomAplicacion, vAyuda, 4400, RcError
			//Err = 0

			lvarNum = lCodigoError;
			stvarDes = Conversion.ErrorToString(lCodigoError);
			short tempRefParam = 1010;
			object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorDelete"};
			oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2);
			Information.Err().Clear();
			iDevolver = 1;

			result = iDevolver;
			oClassErrores = null;

			return result;
		}

		internal static int GestionErrorDisco(int lCodigoError, SqlConnection rcConexion)
		{
			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();

			//Ejemplos *************************

			lvarNum = lCodigoError;
			stvarDes = Conversion.ErrorToString(lCodigoError);
			short tempRefParam = 1010;
			object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorDisco"};
			oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2);
			Information.Err().Clear();
			iDevolver = 1;
			result = iDevolver;
			oClassErrores = null;

			return result;
		}

		internal static int GestionErrorDisquete(int lCodigoError, string StDescError, SqlConnection rcConexion)
		{
			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();


			switch(lCodigoError)
			{
				case mnErrDeviceUnavailable :  // Error 68 
					//            strMsg = "El dispositivo no est� disponible." 
					//            intMsgType = vbExclamation + vbOKCancel 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					break;
				case mnErrDiskNotReady :  // Error 71 
					//            strMsg = "El disco no est� listo." 
					//            intMsgType = vbExclamation + vbOKCancel 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					break;
				case mnErrDeviceIO :  // Error 57 
					//            strMsg = "Error de E/S de dispositivo." 
					//            intMsgType = vbExclamation + vbOKOnly 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					break;
				case mnErrDiskFull :  // Error 61 
					//            strMsg = "Disco lleno. �Desea continuar?" 
					//            intMsgType = vbExclamation + vbAbortRetryIgnore 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					break;
				case mnErrBadFileName : case mnErrBadFileNameOrNumber :  // Error 64 y 52 
					//            strMsg = "Nombre de archivo no v�lido." 
					//            intMsgType = vbExclamation + vbOKCancel 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					break;
				case mnErrPathDoesNotExist :  // Error 76 
					//            strMsg = "No se ha encontrado la ruta de acceso." 
					//            intMsgType = vbExclamation + vbOKCancel 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					break;
				case mnErrBadFileMode :  // Error 54 
					//            strMsg = "Imposible abrir el archivo para este tipo de acceso." 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					break;
				case mnErrFileAlreadyOpen :  // Error 55 
					//            strMsg = "El archivo ya est� abierto." 
					//            intMsgType = vbExclamation + vbOKOnly 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					break;
				case mnErrInputPastEndOfFile :  // Error 62 
					//            strMsg = "Este archivo tiene un marcador no est�ndar de fin de archivo, " 
					//            strMsg = strMsg & "o se ha intentado leer m�s all� " 
					//            strMsg = strMsg & "del marcador de final de archivo." 
					//            intMsgType = vbExclamation + vbAbortRetryIgnore 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					 
					break;
				case mnErrAccesoRutaArchivo :  //error 75 
					MessageBox.Show(lCodigoError.ToString() + " - " + StDescError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
					iDevolver = 0; 
					 
					break;
				default:
					lvarNum = lCodigoError; 
					stvarDes = Conversion.ErrorToString(lCodigoError); 
					short tempRefParam = 1010; 
					object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorDisquete"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2); 
					iDevolver = 0; 
					break;
			}
			Information.Err().Clear();
			result = iDevolver;
			oClassErrores = null;

			return result;
		}


		internal static int GestionErrorIntegridad(int lCodigoError, SqlConnection rcConexion)
		{
			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();

			//Ejemplos *************************

			//       Case 3200 'Borrado o modificacion
			//       oClassErrores.RespuestaMensaje vNomAplicacion, vAyuda,5100, RcError
			//       Err = 0

			lvarNum = lCodigoError;
			stvarDes = Conversion.ErrorToString(lCodigoError);
			short tempRefParam = 1010;
			object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorIntegridad"};
			oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2);
			Information.Err().Clear();
			iDevolver = 1;

			result = iDevolver;
			oClassErrores = null;

			return result;
		}

		internal static int GestionErrorUpdate(int lCodigoError, SqlConnection rcConexion)
		{
			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();

			//Ejemplos *************************

			lvarNum = lCodigoError;
			stvarDes = Conversion.ErrorToString(lCodigoError);
			short tempRefParam = 1010;
			object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorUpdate"};
			oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2);
			Information.Err().Clear();
			iDevolver = 1;

			result = iDevolver;
			oClassErrores = null;

			return result;
		}

		internal static int GestionOtrosErrores(int lCodigoError, SqlConnection rcConexion)
		{
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();

			// Ejemplos *************************

			switch(lCodigoError)
			{
				//     Case 3175 ' Fecha con formato erroneo
				//      oClassErrores.RespuestaMensaje vNomAplicacion, vAyuda,5000, RcError
				//      Err = 0
				case 450 :  // falta alg�n par�metro en la llamada a una DLL 
					short tempRefParam = 1040; 
					object tempRefParam2 = new object{"Falta de par�metros en llamada a la dll"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					break;
				case 32006 :  // Fecha con formato err�neo 
					short tempRefParam3 = 1020; 
					object tempRefParam4 = new object{"La Previsi�n", ">", "la fecha actual"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam3, ref RcError, ref tempRefParam4); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					break;
				case 429 :  // ActiveX (DLL,OCX ..) no registrado 
					short tempRefParam5 = 1040; 
					object tempRefParam6 = new object{"Registro del ActiveX"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam5, ref RcError, ref tempRefParam6); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					break;
				default:
					lvarNum = lCodigoError; 
					stvarDes = Conversion.ErrorToString(lCodigoError); 
					short tempRefParam7 = 1010; 
					object tempRefParam8 = new object{lvarNum, stvarDes, "GestionErrorOtrosErrores"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam7, ref RcError, ref tempRefParam8); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					break;
			}
			oClassErrores = null;
			return iDevolver;
		}
		internal static string ObtenerNombreAplicacion(SqlConnection rcConexion)
		{
			//Buscando en la tabla de constantes globales el nombre de la aplicaci�n
			//(valfanu1),el path de la aplicaci�n (valfanu2) y el nombre del grupo
			//(valfanu3)
			RcError = rcConexion;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select valfanu1 from sconsglo where gconsglo='NOMAPLIC'", RcError);
			Rrtitulo = new DataSet();
			tempAdapter.Fill(Rrtitulo);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			vNomAplicacion = Convert.ToString(Rrtitulo.Tables[0].Rows[0]["valfanu1"]);
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method Rrtitulo.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			Rrtitulo.Close();
			return vNomAplicacion.Trim();
		}

		internal static string ObtenerFicheroAyuda()
		{
			//Como ejemplo se usa la de calculadora
			vAyuda = "c:\\WINNT\\system32\\calc.hlp";
			return vAyuda;
		}
		internal static bool FnSelecciona_Impresora(object Paraobjeto, string Pararpt, SqlConnection ParaConexion)
		{
			bool result = false;
			string vstNombrePc = String.Empty;
			int iLongitudNombrePc = 0;
			bool Bexisteimpresora = false;
			string sqlTemp = String.Empty;
			DataSet RrTemp = null;
			int itempo = 0;
			try
			{

				Bexisteimpresora = false;
				vstNombrePc = new string(' ', MAX_COMPUTERNAME_LENGTH + 1);
				iLongitudNombrePc = MAX_COMPUTERNAME_LENGTH;
				//buscar el nombre del PC
				itempo = InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.GetComputerName(ref vstNombrePc, ref iLongitudNombrePc);
				if (itempo != 0)
				{
					//buscar el la tabla la impresora
					sqlTemp = "select * from dimpdocu where gnombrpc='" + vstNombrePc.Trim().Substring(0, Math.Min(vstNombrePc.Trim().Length - 1, vstNombrePc.Trim().Length)).ToUpper() + "' and gdocumen='" + Pararpt.ToUpper() + "'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTemp, ParaConexion);
					RrTemp = new DataSet();
					tempAdapter.Fill(RrTemp);
					if (RrTemp.Tables[0].Rows.Count == 0)
					{
						//Si no existe impresora predeterminada, el objeto PRINTER no et� definido y salta a la rutina de error.
						if (PrinterHelper.Printer.DeviceName != "")
						{
							//UPGRADE_TODO: (1067) Member Impresora is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.Impresora = PrinterHelper.Printer.DeviceName;
							//UPGRADE_TODO: (1067) Member driver is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.driver = PrinterHelper.Printer.DeviceName;
							//UPGRADE_TODO: (1067) Member Puerto is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.Puerto = PrinterHelper.Printer.Port;
							Bexisteimpresora = true;
						}
					}
					else
					{
						//comprobar que esta dad de alta en el sistema
						foreach (PrinterHelper vimpresora in PrinterHelper.Printers)
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							if (PrinterHelper.Printer.DeviceName == Convert.ToString(RrTemp.Tables[0].Rows[0]["dimpreso"]))
							{
								PrinterHelper.Printer = vimpresora;
								Bexisteimpresora = true;
								break;
							}
						}
						if (Bexisteimpresora)
						{
							//devolver valor
							//UPGRADE_TODO: (1067) Member Impresora is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							Paraobjeto.Impresora = RrTemp.Tables[0].Rows[0]["dimpreso"];
							//UPGRADE_TODO: (1067) Member driver is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.driver = PrinterHelper.Printer.DeviceName;
							//UPGRADE_TODO: (1067) Member Puerto is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.Puerto = PrinterHelper.Printer.Port;
						}
						else
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							MessageBox.Show("No esta dada de alta la impresora " + Convert.ToString(RrTemp.Tables[0].Rows[0]["dimpreso"]) + " en el sistema", Application.ProductName);
						}
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrTemp.Close();
				}
				else
				{
					MessageBox.Show("Error al obtener el nombre del PC", Application.ProductName);
				}
				return Bexisteimpresora;
			}
			catch (System.Exception excep)
			{
				MessageBox.Show(excep.Message, Application.ProductName);
				return result;
			}
		}

		internal static string CortarCadena(ref string stCamino)
		{
			int lCortar = 0;

			string varBuscar = "\\";
			int lComienzo = (stCamino.IndexOf(varBuscar) + 1);

			while(lComienzo > 0)
			{
				lCortar = lComienzo;
				lComienzo = Strings.InStr(lComienzo + 1, stCamino, varBuscar, CompareMethod.Binary);
			};
			stCamino = stCamino.Substring(0, Math.Min(lCortar, stCamino.Length));

			return stCamino;
		}

		internal static int GestionErrorCrystal(int lCodigoError, ref SqlConnection rcConexion)
		{
			int result = 0;
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();

			switch(lCodigoError)
			{
				//Recuperables
				case 20520 : case 20524 :  //Ya se est� imprimiendo un informe 
					short tempRefParam = 2070; 
					object tempRefParam2 = new object{}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref rcConexion, ref tempRefParam2); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					 
					break;
				case 20500 :  //Memoria insuficiente para imprimir el informe 
					short tempRefParam3 = 2080; 
					object tempRefParam4 = new object{}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam3, ref rcConexion, ref tempRefParam4); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					 
					break;
				case 20527 :  //Fallo en la conexi�n ODBC 
					short tempRefParam5 = 2090; 
					object tempRefParam6 = new object{}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam5, ref rcConexion, ref tempRefParam6); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					 
					//Dise�o 
					break;
				case 20502 : case 20503 : case 20505 : case 20506 : case 20507 : case 20508 : case 20511 : case 20514 : case 20515 : case 20521 : case 20539 : case 20547 : case 20548 : case 20553 : case 20554 : case 20555 : case 20557 : 
					short tempRefParam7 = 2060; 
					object tempRefParam8 = new object{lCodigoError.ToString() + " - " + Information.Err().Description, "Dise�o"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam7, ref rcConexion, ref tempRefParam8); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					 
					//Sistema 
					break;
				case 20513 : case 20522 : case 20526 : case 20529 : case 20530 : case 20546 : 
					short tempRefParam9 = 2060; 
					object tempRefParam10 = new object{lCodigoError.ToString() + " - " + Information.Err().Description, null, "Sistema"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam9, ref rcConexion, ref tempRefParam10); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					 
					//Base de datos 
					break;
				case 20531 : case 20532 : case 20533 : case 20534 : case 20535 : case 20536 : case 20537 : case 20552 : 
					short tempRefParam11 = 2060; 
					object tempRefParam12 = new object{lCodigoError.ToString() + " - " + Information.Err().Description, "Base de datos"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam11, ref rcConexion, ref tempRefParam12); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					 
					//InterNos 
					break;
				//UPGRADE_NOTE: (7001) The following case (switch) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
				//case 20508 : 
					//break;
				case 20501 : case 20512 : case 20517 : case 20538 : case 20999 : 
					short tempRefParam13 = 2060; 
					object tempRefParam14 = new object{lCodigoError.ToString() + " - " + Information.Err().Description, null, "Interno"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam13, ref rcConexion, ref tempRefParam14); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					 
					//RPT 
					break;
				case 20504 : case 20510 : case 20525 : case 20540 : case 20541 : case 20544 : case 20545 : case 20550 : 
					short tempRefParam15 = 2060; 
					object tempRefParam16 = new object{Conversion.Str(lCodigoError) + " - " + Information.Err().Description, "RPT"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam15, ref rcConexion, ref tempRefParam16); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					 
					break;
				default:
					lvarNum = lCodigoError; 
					stvarDes = Conversion.ErrorToString(lCodigoError); 
					short tempRefParam17 = 1010; 
					object tempRefParam18 = new object{lvarNum, stvarDes, "GestionErrorCrystal"}; 
					oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam17, ref RcError, ref tempRefParam18); 
					Information.Err().Clear(); 
					iDevolver = 1; 
					break;
			}
			result = iDevolver;
			oClassErrores = null;
			return result;
		}

		internal static int GestionErrorLectura(int lCodigoError, SqlConnection rcConexion)
		{
			RcError = rcConexion;
			oClassErrores = new Mensajes.ClassMensajes();


			lvarNum = lCodigoError;
			stvarDes = Conversion.ErrorToString(lCodigoError);
			short tempRefParam = 1010;
			object tempRefParam2 = new object{lvarNum, stvarDes, "GestionErrorLectura"};
			oClassErrores.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref RcError, ref tempRefParam2);
			Information.Err().Clear();
			iDevolver = 1;

			return iDevolver;

		}

		internal static bool BuscaError(int ParaError)
		{
			bool result = false;
			result = true;
			if (Conversion.Str(ParaError).Trim() == "2627")
			{
				MessageBox.Show("Error, esta insertando en la tabla un valor que ya existe", Application.ProductName);
				result = false;
			}
			if (Conversion.Str(ParaError).Trim() == "40060")
			{
				MessageBox.Show("Error,esta intentando asignar un valor que no coincide con el tipo de dato del campo", Application.ProductName);
				result = false;
			}
			if (Conversion.Str(ParaError).Trim() == "547")
			{
				MessageBox.Show("Error,esta intentando modificar un dato referenciado en otras partes de la aplicaci�n", Application.ProductName);
				result = false;
			}
			return result;
		}
		internal static object Obtener_TipoBD_FormatoFechaBD(ref SqlConnection rc)
		{
			// JavierC   19/07/2000
			// Funcion para obtener el formato de fecha de la base de datos
			// y el tipo de base de datos con la que estamos trabajando
			Conexion.Obtener_conexion objConexion = new Conexion.Obtener_conexion();
			vstFormatoSOLOFechaBD = objConexion.FormatoSoloFechaBD(ref rc);
			vstFormatoFechaHoraMinBD = objConexion.FormatoFechaHoraMinBD(ref rc);
			vstFormatoFechaHoraMinSegBD = objConexion.FormatoFechaHoraMinSegBD(ref rc);

			//OSCAR 09/10/2002
			VstTipoBD = objConexion.ObtenerTipoBD();
			//Ahora el tipo de BD depende de una variable global y no del registro
			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO ='GTIPOBD'", rc);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0][0]))
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					VstTipoBD = Convert.ToString(RR.Tables[0].Rows[0][0]).Trim().ToUpper();
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();

			return null;
		}
		internal static string FormatFecha(ref string fecha)
		{
			string result = String.Empty;
			Conexion.Obtener_conexion objConexion = new Conexion.Obtener_conexion();
			//UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
			object tempRefParam = fecha;
			result = objConexion.FormatearFecha(ref VstTipoBD, ref tempRefParam, ref vstFormatoSOLOFechaBD);
			fecha = Convert.ToString(tempRefParam);
			return result;
		}
		internal static string FormatFechaHM(ref object fecha)
		{
			Conexion.Obtener_conexion objConexion = new Conexion.Obtener_conexion();
			return objConexion.FormatearFecha(ref VstTipoBD, ref fecha, ref vstFormatoFechaHoraMinBD);
		}
		internal static string FormatFechaHMS(ref object fecha)
		{
			Conexion.Obtener_conexion objConexion = new Conexion.Obtener_conexion();
			return objConexion.FormatearFecha(ref VstTipoBD, ref fecha, ref vstFormatoFechaHoraMinSegBD);
		}
		internal static string ObtenerSeparadorDecimalBD(SqlConnection RrConexion)
		{
			//Recupera el separador decimal con el que se tiene que grabar en las tablas
			//por defecto va a ser "."

			string result = String.Empty;
			string sql = "select valfanu2 from sconsglo where gconsglo='PUNTOCOM'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RrConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["valfanu2"])) ? "." : Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu2"]).Trim();
			}
			else
			{
				result = ".";
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();
			return result;
		}
		internal static string ConvertirDecimales(ref string dNumero, ref string SeparadorDecimal, int? nDecimales_optional = null)
		{
			int nDecimales = (nDecimales_optional == null) ? 0 : nDecimales_optional.Value;
			//''funcion que formatea un numero segun el separador de decimales que se le pasa
			//adem�s de el numero de decimales

			string result = String.Empty;
			int iPosicion = 0;
			int iDecimales = 0;
			string stNumeroFormateado = String.Empty;
			bool bNumeroNegativo = false;

			string stNumero = "";
			result = "";

			//buscamos la posicion en la que viene el separador de decimales puede ser punto o coma
			if (dNumero.Trim() != "")
			{

				if (dNumero.StartsWith("-"))
				{
					bNumeroNegativo = true;
					dNumero = dNumero.Substring(1);
				}

				//buscamos la posicion del separador decimal dentro del n�mero
				//si numero tiene valor hay que comprobar que tiene si punto o coma como separador
				if ((dNumero.IndexOf(',') + 1) == 0)
				{ //si no tiene "," comprobamos que tiene punto
					if ((dNumero.IndexOf('.') + 1) == 0)
					{
						iPosicion = 0;
					}
					else
					{
						iPosicion = (dNumero.IndexOf('.') + 1);
					}
				}
				else
				{
					iPosicion = (dNumero.IndexOf(',') + 1);
				}
				//una vez que tenemos la posicion formateamos el numero
				switch(iPosicion)
				{
					case 0 :  //no tiene separador de decimales 
						stNumeroFormateado = dNumero; 
						break;
					case 1 :  //separador en primera posicion 
						stNumeroFormateado = "0" + SeparadorDecimal + dNumero.Substring(iPosicion); 
						break;
					default:
						if (dNumero.Substring(iPosicion, Math.Min(2, dNumero.Length - iPosicion)).Trim() == "")
						{
							stNumeroFormateado = dNumero.Substring(0, Math.Min(iPosicion - 1, dNumero.Length));
						}
						else
						{
							stNumeroFormateado = dNumero.Substring(0, Math.Min(iPosicion - 1, dNumero.Length)) + SeparadorDecimal + dNumero.Substring(iPosicion);
						} 
						break;
				}

				//volvemos a recuperar la posicion una vez formateado el numero
				iPosicion = (stNumeroFormateado.IndexOf(SeparadorDecimal) + 1);



				//una vez que tenemos formateado el n�mero tenemos que ver si es correcto el numero de decimales
				if (nDecimales_optional == null)
				{
					//si no hay decimales dejamos el n�mero como este
					result = ((bNumeroNegativo) ? "-" : "") + stNumeroFormateado;
				}
				else
				{
					if (nDecimales == 0)
					{
						//no debe tener decimales
						if (iPosicion > 0)
						{
							stNumero = stNumeroFormateado.Substring(0, Math.Min(iPosicion - 1, stNumeroFormateado.Length));
						}
						else
						{
							stNumero = stNumeroFormateado;
						}
						result = ((bNumeroNegativo) ? "-" : "") + stNumero;

					}
					else
					{
						if (iPosicion == 0)
						{
							iDecimales = nDecimales;
							result = ((bNumeroNegativo) ? "-" : "") + stNumeroFormateado + SeparadorDecimal + new string('0', iDecimales);
						}
						else
						{
							if (stNumeroFormateado.Substring(iPosicion).Length < nDecimales)
							{
								iDecimales = nDecimales - (stNumeroFormateado.Length - iPosicion);
								result = ((bNumeroNegativo) ? "-" : "") + stNumeroFormateado + new string('0', iDecimales);
							}
							else
							{
								result = ((bNumeroNegativo) ? "-" : "") + stNumeroFormateado.Substring(0, Math.Min(iPosicion - 1, stNumeroFormateado.Length)) + SeparadorDecimal + stNumeroFormateado.Substring(iPosicion, Math.Min(nDecimales, stNumeroFormateado.Length - iPosicion));
							}
							//If iDecimales < 0 Then
							//    ConvertirDecimales = Mid(stNumeroFormateado, 1, iPosicion - 1) & SeparadorDecimal & Mid(stNumeroFormateado, iPosicion + 1, Abs(iDecimales))
							//Else
							//    ConvertirDecimales = stNumeroFormateado & String(iDecimales, "0")
							//End If
						}
					}
				}



			}

			return result;
		}

		internal static string ObtenerSeparadorDecimal(SqlConnection RrConexion)
		{
			//Devuelve el separador decimal que se tiene que visualizar en la aplicacion
			//seg�n lo que tenga SCONSGLO si es "P" tiene que mostrar "." en otro caso ","

			string stSeparador = String.Empty;

			string sql = "select valfanu1 from sconsglo where gconsglo='PUNTOCOM'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RrConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				stSeparador = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["valfanu1"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim();
			}
			else
			{
				stSeparador = "";
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();

			if (stSeparador.Trim() == "P")
			{
				return ".";
			}
			else
			{
				return ",";
			}


		}

		internal static void AjustarRelojCliente(SqlConnection RcAjuste)
		{
			//'    *************** CURIAE: A REVISAR POR EL EQUIPO DE MIGRACION ********************
			//'              NUEVA FORMA DE SINCRONIZAR LA FECHA y HORA  DEL PC CON LA FECHA/HORA DEL SERVIDOR
			//'    *************************************************************************
			//Ajusta la fecha y hora en el equipo cliente
			//'Dim StSql As String
			//'Dim RsAjuste As rdoResultset
			//'
			//'StSql = "select * from sconsglo where gconsglo = 'AJURELOJ'"
			//'Set RsAjuste = RcAjuste.OpenResultset(StSql, rdOpenStatic, rdConcurReadOnly)
			//'If Not RsAjuste.EOF Then
			//'    If UCase(Trim(RsAjuste("valfanu1"))) = "N" Then
			//'        'si la constante tiene valor N, no actualizo la hora en el equipo.
			//'        Exit Sub
			//'    End If
			//'End If
			//'RsAjuste.Close
			//'
			//'Set RsAjuste = RcAjuste.OpenResultset("select getdate()", rdOpenStatic, rdConcurReadOnly)
			//'
			//'Date = Format(RsAjuste(0), "dd/mm/yyyy HH:NN:ss")
			//'Time = Format(RsAjuste(0), "dd/mm/yyyy HH:NN:ss")
			//'RsAjuste.Close

		}
		internal static string ExisteLogo(SqlConnection rcConexion)
		{
			//para ver si tengo q pintar en el report el logo de IDC
			string stExiste = "N";

			string stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'EXISLOGO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, rcConexion);
			DataSet RrTemp = new DataSet();
			tempAdapter.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
				{
					stExiste = "S";
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrTemp.Close();
			return stExiste;
		}

		internal static bool ExisteAseguradora(SqlConnection rcConexion)
		{
			//para saber si tengo q sacar el nafiliac y la aseguradora

			bool result = false;

			string stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'IASEGURA' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, rcConexion);
			DataSet RrTemp = new DataSet();
			tempAdapter.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
				{
					result = true;
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrTemp.Close();

			return result;
		}


		internal static bool FactQuirAntigua(int A�oRegistro, int NumeroRegistro, SqlConnection Conexion, ref int DiasAntigua)
		{
			bool result = false;
			System.DateTime dia = DateTime.FromOADate(0);


			string StSql = "SELECT nnumeri1 " + 
			               "FROM sconsglo " + 
			               "WHERE gconsglo='NDIAMACT'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet rsConsulta = new DataSet();
			tempAdapter.Fill(rsConsulta);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nnumeri1"]))
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				DiasAntigua = Convert.ToInt32(Convert.ToDateTime(rsConsulta.Tables[0].Rows[0]["nnumeri1"]).ToOADate());
			}

			StSql = "SELECT finterve " + 
			        "FROM qintquir " + 
			        "WHERE ganoregi = " + A�oRegistro.ToString() + " AND " + 
			        "gnumregi = " + NumeroRegistro.ToString();

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
			rsConsulta = new DataSet();
			tempAdapter_2.Fill(rsConsulta);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["finterve"]))
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				dia = Convert.ToDateTime(rsConsulta.Tables[0].Rows[0]["finterve"]);
				if (dia < DateTime.Today.AddDays(-DiasAntigua))
				{
					return true;
				}
			}





			return result;
		}



		internal static bool FactConsAntigua(int A�oRegistro, int NumeroRegistro, SqlConnection Conexion, ref int DiasAntigua)
		{
			bool result = false;
			System.DateTime dia = DateTime.FromOADate(0);


			string StSql = "SELECT nnumeri1 " + 
			               "FROM sconsglo " + 
			               "WHERE gconsglo='NDIAMACT'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet rsConsulta = new DataSet();
			tempAdapter.Fill(rsConsulta);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nnumeri1"]))
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				DiasAntigua = Convert.ToInt32(Convert.ToDateTime(rsConsulta.Tables[0].Rows[0]["nnumeri1"]).ToOADate());
			}

			StSql = "SELECT ffeccita " + 
			        "FROM cconsult " + 
			        "WHERE ganoregi = " + A�oRegistro.ToString() + " AND " + 
			        "gnumregi = " + NumeroRegistro.ToString();

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
			rsConsulta = new DataSet();
			tempAdapter_2.Fill(rsConsulta);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ffeccita"]))
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				dia = Convert.ToDateTime(rsConsulta.Tables[0].Rows[0]["ffeccita"]);
				if (dia < DateTime.Today.AddDays(-DiasAntigua))
				{
					return true;
				}
			}

			return result;
		}


		internal static string obtener_usuario()
		{

			string stBuffer = new String(' ', 256);
			int lSize = stBuffer.Length;
			InterconsultasListadoSupport.PInvoke.SafeNative.advapi32.GetUserName(ref stBuffer, ref lSize);
			int lpos = (stBuffer.IndexOf(Strings.Chr(0).ToString()) + 1);
			if (lSize > 0)
			{
				return stBuffer.Substring(0, Math.Min(lpos - 1, stBuffer.Length));
			}
			else
			{
				return "";
			}
		}
		internal static bool FnSelecciona_Brazalete(object Paraobjeto, string Pararpt, SqlConnection ParaConexion)
		{
			bool result = false;
			string vstNombrePc = String.Empty;
			int iLongitudNombrePc = 0;
			bool Bexisteimpresora = false;
			string sqlTemp = String.Empty;
			DataSet RrTemp = null;
			int itempo = 0;
			try
			{

				Bexisteimpresora = false;
				vstNombrePc = new string(' ', MAX_COMPUTERNAME_LENGTH + 1);
				iLongitudNombrePc = MAX_COMPUTERNAME_LENGTH;
				//buscar el nombre del PC
				itempo = InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.GetComputerName(ref vstNombrePc, ref iLongitudNombrePc);
				if (itempo != 0)
				{
					//buscar el la tabla la impresora
					sqlTemp = "select * from dimpdocu where gnombrpc='" + vstNombrePc.Trim().Substring(0, Math.Min(vstNombrePc.Trim().Length - 1, vstNombrePc.Trim().Length)).ToUpper() + "' and gdocumen='" + Pararpt.ToUpper() + "'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTemp, ParaConexion);
					RrTemp = new DataSet();
					tempAdapter.Fill(RrTemp);
					if (RrTemp.Tables[0].Rows.Count == 0)
					{
						//Si no existe impresora predeterminada, el objeto PRINTER no et� definido y salta a la rutina de error.
						if (PrinterHelper.Printer.DeviceName != "")
						{
							//            Paraobjeto.PrinterName = Printer.DeviceName
							//            Paraobjeto.PrinterDriver = Printer.DriverName
							//            Paraobjeto.PrinterPort = Printer.Port
							Bexisteimpresora = true;
						}
					}
					else
					{
						//comprobar que esta dada de alta en el sistema
						foreach (PrinterHelper vimpresora in PrinterHelper.Printers)
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							if (PrinterHelper.Printer.DeviceName.ToUpper() == Convert.ToString(RrTemp.Tables[0].Rows[0]["dimpreso"]).ToUpper())
							{
								PrinterHelper.Printer = vimpresora;
								Bexisteimpresora = true;
								break;
							}
						}
						if (Bexisteimpresora)
						{
							//devolver valor

							//            Paraobjeto.PrinterName = RrTemp("dimpreso")
							//
							//            Paraobjeto.PrinterDriver = Printer.DriverName
							//            Paraobjeto.PrinterPort = Printer.Port
						}
						else
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							MessageBox.Show("No esta dada de alta la impresora " + Convert.ToString(RrTemp.Tables[0].Rows[0]["dimpreso"]) + " en el sistema", Application.ProductName);
						}
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrTemp.Close();
				}
				else
				{
					MessageBox.Show("Error al obtener el nombre del PC", Application.ProductName);
				}
				return Bexisteimpresora;
			}
			catch (System.Exception excep)
			{
				MessageBox.Show(excep.Message, Application.ProductName);
				return result;
			}
		}


		internal static bool FnSelecciona_ImpresoraCrystal(object Paraobjeto, string Pararpt, SqlConnection ParaConexion)
		{
			bool result = false;
			string vstNombrePc = String.Empty;
			int iLongitudNombrePc = 0;
			bool Bexisteimpresora = false;
			string sqlTemp = String.Empty;
			DataSet RrTemp = null;
			int itempo = 0;
			try
			{

				Bexisteimpresora = false;
				vstNombrePc = new string(' ', MAX_COMPUTERNAME_LENGTH + 1);
				iLongitudNombrePc = MAX_COMPUTERNAME_LENGTH;
				//buscar el nombre del PC
				itempo = InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.GetComputerName(ref vstNombrePc, ref iLongitudNombrePc);
				if (itempo != 0)
				{
					//buscar el la tabla la impresora
					sqlTemp = "select * from dimpdocu where gnombrpc='" + vstNombrePc.Trim().Substring(0, Math.Min(vstNombrePc.Trim().Length - 1, vstNombrePc.Trim().Length)).ToUpper() + "' and gdocumen='" + Pararpt.ToUpper() + "'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTemp, ParaConexion);
					RrTemp = new DataSet();
					tempAdapter.Fill(RrTemp);
					if (RrTemp.Tables[0].Rows.Count == 0)
					{
						//Si no existe impresora predeterminada, el objeto PRINTER no et� definido y salta a la rutina de error.
						if (PrinterHelper.Printer.DeviceName != "")
						{
							//UPGRADE_TODO: (1067) Member PrinterName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.PrinterName = PrinterHelper.Printer.DeviceName;
							//UPGRADE_TODO: (1067) Member PrinterDriver is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.PrinterDriver = PrinterHelper.Printer.DeviceName;
							//UPGRADE_TODO: (1067) Member PrinterPort is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.PrinterPort = PrinterHelper.Printer.Port;
							Bexisteimpresora = true;
						}
					}
					else
					{
						//comprobar que esta dada de alta en el sistema
						foreach (PrinterHelper vimpresora in PrinterHelper.Printers)
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							if (PrinterHelper.Printer.DeviceName.ToUpper() == Convert.ToString(RrTemp.Tables[0].Rows[0]["dimpreso"]).ToUpper())
							{
								PrinterHelper.Printer = vimpresora;
								Bexisteimpresora = true;
								break;
							}
						}
						if (Bexisteimpresora)
						{
							//devolver valor

							//UPGRADE_TODO: (1067) Member PrinterName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							Paraobjeto.PrinterName = RrTemp.Tables[0].Rows[0]["dimpreso"];
							//UPGRADE_TODO: (1067) Member PrinterDriver is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.PrinterDriver = PrinterHelper.Printer.DeviceName;
							//UPGRADE_TODO: (1067) Member PrinterPort is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Paraobjeto.PrinterPort = PrinterHelper.Printer.Port;
						}
						else
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							MessageBox.Show("No esta dada de alta la impresora " + Convert.ToString(RrTemp.Tables[0].Rows[0]["dimpreso"]) + " en el sistema", Application.ProductName);
						}
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrTemp.Close();
				}
				else
				{
					MessageBox.Show("Error al obtener el nombre del PC", Application.ProductName);
				}
				return Bexisteimpresora;
			}
			catch (System.Exception excep)
			{
				MessageBox.Show(excep.Message, Application.ProductName);
				return result;
			}
		}

		internal static string Codificar_frase(string frase)
		{
			string Intermedia = String.Empty;
			StringBuilder primera = new StringBuilder();
			double Cual = 0;

			for (int f = 1; f <= frase.Length; f++)
			{
				Intermedia = ("00" + Strings.Asc(frase.Substring(f - 1, Math.Min(1, frase.Length - (f - 1)))[0]).ToString()).Substring(Math.Max(("00" + Strings.Asc(frase.Substring(f - 1, Math.Min(1, frase.Length - (f - 1)))[0]).ToString()).Length - 3, 0));
				primera.Append(Intermedia);
			}
			Intermedia = "";
			double tal = primera.ToString().Length % 13;
			primera = new StringBuilder(new string('0', Convert.ToInt32(13 - tal)) + primera.ToString());
			for (int f = 1; f <= primera.ToString().Length; f += 13)
			{
				tal = Math.Floor(Conversion.Val(primera.ToString().Substring(f - 1, Math.Min(13, primera.ToString().Length - (f - 1)))) / 65535);
				Cual = Conversion.Val(primera.ToString().Substring(f - 1, Math.Min(13, primera.ToString().Length - (f - 1)))) - (tal * 65535);
				Intermedia = Intermedia + Convert.ToInt32(tal).ToString("X") + ("0000" + Convert.ToInt32(Cual).ToString("X")).Substring(Math.Max(("0000" + Convert.ToInt32(Cual).ToString("X")).Length - 4, 0)) + " ";
			}
			return Intermedia;
		}


		internal static string Decodificar_frase(ref string frase)
		{
			string Fin = String.Empty;
			string pp = String.Empty;
			string X = String.Empty;
			double num = 0;
			double num2 = 0;
			string pp1 = String.Empty;
			string pp2 = String.Empty;
			StringBuilder Intermedia = new StringBuilder();
			int cc = 1;
			int xx = Strings.InStr(cc, frase, " ", CompareMethod.Binary);
			while (xx != 0)
			{
				pp = frase.Substring(cc - 1, Math.Min(xx - cc, frase.Length - (cc - 1)));
				pp1 = pp.Substring(Math.Max(pp.Length - 4, 0));
				pp2 = pp.Substring(0, Math.Min(pp.Length - 4, pp.Length));
				num = Double.Parse("&H" + pp1);
				num2 = Double.Parse("&H" + pp2) * 65535;
				num += num2;
				cc = xx + 1;
				xx = Strings.InStr(cc, frase, " ", CompareMethod.Binary);
				if (xx != 0)
				{
					Intermedia.Append(("0000000000000" + num.ToString()).Substring(Math.Max(("0000000000000" + num.ToString()).Length - 13, 0)));
				}
				else
				{
					Intermedia.Append(("0000000000000" + num.ToString()).Substring(Math.Max(("0000000000000" + num.ToString()).Length - 13, 0)));
				}
			}
			for (xx = Intermedia.ToString().Length - 2; xx >= 1; xx -= 3)
			{
				X = Intermedia.ToString().Substring(xx - 1, Math.Min(3, Intermedia.ToString().Length - (xx - 1)));
				if (X != "000")
				{
					X = Strings.Chr(Convert.ToInt32(Double.Parse(X))).ToString();
					Fin = X + Fin;
				}
			}
			return Fin;

		}

		//oscar
		internal static void Obtener_Funciones_BD()
		{
			switch(VstTipoBD)
			{
				case "SQLSERVER" : 
					QLongitud = "DATALENGTH"; 
					QFechaBD = "GETDATE()"; 
					QEsNulo = "ISNULL"; 
					break;
				case "ORACLE" : 
					QLongitud = "LENGTH"; 
					QFechaBD = "SYSDATE"; 
					QEsNulo = "NVL"; 
					break;
			}
		}

		//23/01/23003
		//Obtencion del Calendario General del Centro
		internal static int ObtenerCalendarioGeneral(SqlConnection cn)
		{
			int result = 0;
			result = -1;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT NNUMERI1 FROM SCONSGLO WHERE GCONSGLO='CTIPCALE'", cn);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0][0]))
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					result = Convert.ToInt32(RR.Tables[0].Rows[0][0]);
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			return result;
		}
		//--------

		internal static double ToDOUBLE(ref string Numero)
		{
			//Funcion que recibe un string con formato numerico y devuelve el mismo numero
			//convertido a Double sin tener que utilizar la funcion de visual CDBL para evitar
			//incompatibilidades con las diferentes configuraciones regionales utilizando el
			//tratamiento de numeros en dotacion decimal
			//Ej: 3,33 = (333 / 100)
			//     3.3 = (33  / 10)
			int posdec = 0;
			int CuantosDec = 0;
			double Constante = 0;
			bool YaHay = false;
			double cantidad = 0;

			Numero = Numero.Trim();

			// Comprobamos que el par�metro sea num�rico
			if (Numero.StartsWith(".") || Numero.StartsWith(","))
			{
				Numero = "0" + Numero;
			}

			double dbNumericTemp = 0;
			if (!Double.TryParse(Numero, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
			{
				return 0;
			}

			// Comprobamos que el par�metro tenga un �nico separador decimal

			for (int I = 1; I <= Numero.Length; I++)
			{
				if (Numero.Substring(I - 1, Math.Min(1, Numero.Length - (I - 1))) == "." || Numero.Substring(I - 1, Math.Min(1, Numero.Length - (I - 1))) == ",")
				{
					if (YaHay)
					{
						return 0;
					}
					else
					{
						YaHay = true;
					}
				}
			}

			if (Numero != "")
			{
				posdec = (Numero.IndexOf('.') + 1);
				if (posdec == 0)
				{
					posdec = (Numero.IndexOf(',') + 1);
				}
				if (posdec == 0)
				{
					cantidad = Double.Parse(Numero);
				}
				else
				{
					CuantosDec = Numero.Substring(posdec).Length;
					Constante = Double.Parse("1" + new string('0', CuantosDec));
					cantidad = Double.Parse(Numero.Substring(0, Math.Min(posdec - 1, Numero.Length)) + Numero.Substring(posdec)) / Constante;
				}
			}
			return cantidad;
		}


		//30/09/23003
		//Validacion de teclas pulsadas (Para pantallas de Filiacion)
		//True -- > tecla pulsada OK de acuerdo con el formato
		//False --> tecla pulsada no OK de acuerdo con el formato
		internal static bool Valida_Tecla_Pulsada(int PCodTecla, string PFormato)
		{
			bool bError = true;
			if (PFormato == "POLIZA" || PFormato == "NSS")
			{
				if (PCodTecla >= 48 && PCodTecla <= 57)
				{
					bError = true; //Numeros
				}
				else if (PCodTecla >= 65 && PCodTecla <= 90)
				{ 
					bError = true; //Letras mayusculas
				}
				else if (PCodTecla >= 97 && PCodTecla <= 122)
				{ 
					bError = true; //Letras minusculas
				}
				else if (PCodTecla == 32 || PCodTecla == 8 || PCodTecla == 92 || PCodTecla == 46 || PCodTecla == 47 || PCodTecla == 45 || PCodTecla == 209 || PCodTecla == 241)
				{ 
					bError = true; //espacio, retroceso, \, .,/, -,�,�
				}
				else
				{
					bError = false;
				}
			}
			else if (PFormato == "MASCARA")
			{ 
				switch(PCodTecla)
				{
					case 32 : case 8 : case 92 : case 46 : case 47 : case 45 : case 63 : case 35 :  
						bError = true;  //espacio, retroceso, \, .,/, -, #, ? 
						break;
					default:
						bError = false; 
						break;
				}
			}
			else if (PFormato == "GENERICOSINCOMILLAS")
			{ 
			}
			else if (StringsHelper.ToDoubleSafe(PFormato) == 34 || StringsHelper.ToDoubleSafe(PFormato) == 39 || StringsHelper.ToDoubleSafe(PFormato) == 96 || StringsHelper.ToDoubleSafe(PFormato) == 180)
			{ 
				bError = false;
			}
			else
			{
				bError = true;
			}
			return bError;
		}
		//-----------


		internal static string ObtenerDescripcionUsuario(string CodigoUsuario, SqlConnection RrConexion)
		{
			//Devuelve la descripcion del usuario del que se manda el codigo

			string stCadena = "";

			string sql = "select dnompers,dap1pers,dap2pers From DPERSONA ,susuario ";
			sql = sql + "where susuario.gusuario = '" + CodigoUsuario + "' ";
			sql = sql + "and dpersona.gpersona = susuario.gpersona ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RrConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				stCadena = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dap1pers"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dap1pers"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				stCadena = stCadena + " " + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dap2pers"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dap2pers"]).Trim());
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				stCadena = stCadena + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnompers"])) ? "" : ", " + Convert.ToString(RrSql.Tables[0].Rows[0]["dnompers"]).Trim());
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();

			return stCadena;

		}
		internal static string ObtenerNColegiadoUsuario(string CodigoUsuario, SqlConnection RrConexion)
		{
			//Devuelve la descripcion del usuario del que se manda el codigo

			string stCadena = "";

			string sql = "select ncolegia From DPERSONA ,susuario ";
			sql = sql + "where susuario.gusuario = '" + CodigoUsuario + "' ";
			sql = sql + "and dpersona.gpersona = susuario.gpersona ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RrConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				stCadena = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ncolegia"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["ncolegia"]).Trim();
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();

			return stCadena;

		}
		internal static string ObtenerLiteralPersona(SqlConnection RrConexion, string ParaIdioma = "")
		{
			//para ver que tengo q pintar en el literal de paciente
			string result = String.Empty;
			string lIdioma = "IDIOMA0";
			if (ParaIdioma != "")
			{
				lIdioma = ParaIdioma.Trim().ToUpper();
			}

			result = "paciente";

			string stsqltemp = "select valfanu1, valfanu1i1, valfanu1i2 from sconsglo where gconsglo= 'LITPERSO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, RrConexion);
			DataSet RrTemp = new DataSet();
			tempAdapter.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				//oscar C
				//ObtenerLiteralPersona = Trim(rrtemp("valfanu1"))
				switch(lIdioma)
				{
					case "IDIOMA0" :  
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						result = Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim() + ""; 
						break;
					case "IDIOMA1" :  
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						result = Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1i1"]).Trim() + ""; 
						break;
					case "IDIOMA2" :  
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						result = Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1i2"]).Trim() + ""; 
						break;
				}
				//------
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrTemp.Close();

			return result;
		}

		internal static string ObtenerSiHospitalOCentro(SqlConnection RrConexion)
		{
			//para ver que tengo q pintar en el literal de paciente

			string result = String.Empty;
			result = "S";

			string stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'HOSPITAL' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, RrConexion);
			DataSet RrTemp = new DataSet();
			tempAdapter.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim();
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrTemp.Close();

			return result;
		}

		internal static object Obtener_Multilenguaje(SqlConnection Conexion, string stGidenpac, bool bImprimirEnPDF = false)
		{

			//por defecto tomo el idioma0 y dps lo busco en la constante
			CampoIdioma = "IDIOMA0";
			string sql = "Select * from SCONSGLO where gconsglo = 'IDIOMA'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				CampoIdioma = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["valfanu1"])) ? CampoIdioma : Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim();
			}

			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			object Seleccion_Idioma = null;
			if (Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper() == "P")
			{ //El paciete elige el idioma
				sql = "Select iidioma from DPACIENT where gidenpac = '" + stGidenpac + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
				RrSql = new DataSet();
				tempAdapter_2.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					CampoIdioma = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iidioma"])) ? CampoIdioma : Convert.ToString(RrSql.Tables[0].Rows[0]["iidioma"]).Trim(); //Campo idioma paciente
				}
			}
			else if (Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper() == "L" && !bImprimirEnPDF)
			{  //Pantalla para elegir idioma
				Seleccion_Idioma = new SeleccionIdioma.ClaseIdioma();
				//UPGRADE_TODO: (1067) Member Recibir_Datos is not defined in type object. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Seleccion_Idioma.Recibir_Datos(Conexion);
				//UPGRADE_TODO: (1067) Member ActualizarIdioma is not defined in type object. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				CampoIdioma = Convert.ToString(Seleccion_Idioma.ActualizarIdioma);
			}
			else if (Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper() == "G")
			{ 
				//se queda como esta
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();

			CampoIdioma = CampoIdioma.ToUpper();

			return null;
		}
		internal static object ObtenerTextosRPT(SqlConnection Conexion, string Modulo, string NombreRpt)
		{

			string sql = "Select max(Variable) numero from DDICCION where modulo='" + Modulo + "' AND formula = '" + NombreRpt + "' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["numero"]))
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					VarRPT = ArraysHelper.InitializeArray<string>(Convert.ToInt32(RrSql.Tables[0].Rows[0]["numero"]) + 1);
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();
			//Obtener_Multilenguaje Conexion
			//Busco todos los textos y los cargo en una array
			sql = "Select * from DDICCION where modulo = '" + Modulo + "' AND formula = '" + NombreRpt + "'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
			RrSql = new DataSet();
			tempAdapter_2.Fill(RrSql);
			VarRPT[0] = CampoIdioma;

			//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
			RrSql.MoveFirst();

			foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
			{
				VarRPT[Convert.ToInt32(iteration_row["variable"])] = Convert.ToString(iteration_row["" + CampoIdioma + ""]).Trim();
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();
			return null;
		}
		internal static object ObtenerMesesSemanas(SqlConnection Conexion)
		{

			//Busco todos los meses y dias de la semana y los cargo en una array
			string sql = "Select max(Variable) numero from DDICCION where modulo='ADMISION' AND formula = 'FECHA' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["numero"]))
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					VarFECHA = ArraysHelper.InitializeArray<string>(Convert.ToInt32(RrSql.Tables[0].Rows[0]["numero"]) + 1);
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();
			//Obtener_Multilenguaje Conexion

			sql = "Select * from DDICCION where modulo = 'ADMISION' AND formula = 'FECHA'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
			RrSql = new DataSet();
			tempAdapter_2.Fill(RrSql);
			//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
			RrSql.MoveFirst();

			foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
			{
				VarFECHA[Convert.ToInt32(iteration_row["variable"])] = Convert.ToString(iteration_row["" + CampoIdioma + ""]).Trim();
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();

			return null;
		}
		internal static object LLenarFormulas(SqlConnection Conexion, object INFORMES, string NomRpt, string NomSubrpt, string Subvst = "", string SubUsuario = "", string SubContrase�a = "")
		{
			string TextoTraducido = String.Empty;
			string NumFormula = String.Empty;

			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			INFORMES.Formulas[99] = "MLFORM000= \"" + CampoIdioma + "\"";

			//UPGRADE_WARNING: (2080) IsEmpty was upgraded to a comparison and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
			if (!String.IsNullOrEmpty(Subvst))
			{
				//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				INFORMES.SubreportToChange = NomSubrpt;
				//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				INFORMES.Connect = "DSN=" + Subvst + ";UID=" + SubUsuario + ";PWD=" + SubContrase�a + "";
			}
			int indice = 100;

			string sql = "select * from DFORMRPT where report = '" + NomRpt + "' and subreport = '" + NomSubrpt + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
			{
				TextoTraducido = Convert.ToString(iteration_row["" + CampoIdioma + ""]).Trim();
				NumFormula = Convert.ToString(iteration_row["formula"]).Trim();
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				INFORMES.Formulas[indice + 1] = NumFormula + " = \"" + TextoTraducido + "\"";
				indice++;
			}

			//UPGRADE_WARNING: (2080) IsEmpty was upgraded to a comparison and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
			if (!String.IsNullOrEmpty(Subvst))
			{
				//UPGRADE_TODO: (1067) Member SubreportToChange is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				INFORMES.SubreportToChange = "";
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSql.Close();

			return null;
		}
		internal static string CapturarFechaHoy(string MayuMinus)
		{
			string result = String.Empty;
			result = (DateTimeHelper.ToString(DateTime.Now).Substring(0, Math.Min(2, DateTimeHelper.ToString(DateTime.Now).Length)) + " " + VarFECHA[20] + " " + VarFECHA[Convert.ToInt32(Double.Parse(DateTimeHelper.ToString(DateTime.Now).Substring(3, Math.Min(2, DateTimeHelper.ToString(DateTime.Now).Length - 3))))] + " " + VarFECHA[20] + " " + DateTimeHelper.ToString(DateTime.Now).Substring(6, Math.Min(4, DateTimeHelper.ToString(DateTime.Now).Length - 6))).ToLower();
			if (MayuMinus == "m")
			{
				return result.ToLower();
			}
			else
			{
				return result.ToUpper();
			}

			return result;
		}
		internal static int SustituirComilla()
		{
			return 180;
		}
		internal static object RecogerIdioma(string stIdioma)
		{
			CampoIdioma = stIdioma;
			return null;
		}


		internal static void RecibeDatosAyudaCod(object formCod, object rc, string stSepDeci)
		{
			//'    *************** CURIAE: A REVISAR POR EL EQUIPO DE MIGRACION ********************
			//'              NUEVA FORMA DE INTEGRARSE CON SISTEMA CODIFICADOR
			//'    *************************************************************************

			//'    Dim OutputBuffer    As String
			//'    Dim StatusMsg       As Long
			//'    Dim stErr(1)        As String
			//'
			//'    Dim stDiag()        As diagnostico
			//'    Dim stProc()        As String
			//'    Dim stGRD           As String
			//'    Dim stMDC           As String
			//'    Dim stPeso          As String
			//'    Dim bExistePeso     As Boolean
			//'    Dim bEntornoWeb     As Boolean
			//'    Dim icont           As Integer
			//'    Dim stSeparador     As String
			//'
			//'    bExistePeso = (UCase(Trim(ObternerValor_CTEGLOBAL(rc, "PESOSCOD", "VALFANU1"))) = "S")
			//'
			//'    bEntornoWeb = (UCase(Trim(ObternerValor_CTEGLOBAL(rc, "AYUDACOD", "VALFANU1I1"))) = "S")
			//'
			//'    stSeparador = vbLf
			//'
			//'    ReDim stDiag(0)
			//'    ReDim stProc(0)
			//'    'Read packet after Codefinder has processed the inputData
			//'    OutputBuffer = sVBReadPacket(Codefinder)
			//'
			//'    If UCase(Trim(ObternerValor_CTEGLOBAL(rc, "PESOSCOD", "valfanu3i2"))) = "S" Then
			//'        Dim stHHMMSS As String
			//'        stHHMMSS = Trim(Time)
			//'        Do
			//'            stHHMMSS = Mid(stHHMMSS, 1, InStr(1, stHHMMSS, ":") - 1) & Mid(stHHMMSS, InStr(1, stHHMMSS, ":") + 1)
			//'        Loop Until InStr(1, stHHMMSS, ":") = 0
			//'        Open App.Path & Format(Trim(Year(Date)), "0000") & Format(Trim(Month(Date)), "00") & Format(Trim(Day(Date)), "00") & stHHMMSS & ".txt" For Append As #1
			//'        Print #1, OutputBuffer
			//'        Close #1
			//'    End If
			//'
			//'    ' Si hemos obtenido un error no capturado, tambi�n cuenta como error
			//'
			//'    If InStr(OutputBuffer, "ERR:") <= 0 Then
			//'        stErr(0) = "-1"
			//'        stErr(1) = ""
			//'    Else
			//'
			//'        ' Obtenemos el n�mero del error
			//'
			//'        stErr(0) = Mid(OutputBuffer, InStr(OutputBuffer, "ERR:") + 4, InStr(OutputBuffer, Chr(10)) - 1 - 4)
			//'
			//'        ' Obtenemos la descripci�n del error
			//'
			//'        OutputBuffer = Mid(OutputBuffer, InStr(OutputBuffer, Chr(10)) + 1)
			//'
			//'        stErr(1) = Mid(OutputBuffer, InStr(OutputBuffer, "ETX:") + 4, InStr(OutputBuffer, Chr(10)) - 1 - 4)
			//'
			//'    End If
			//'
			//'    ' Si existe error, no seguimos.
			//'
			//'    If stErr(0) = "0" Then
			//'        'Eliminamos la morralla anterior a los c�digos
			//'        OutputBuffer = Mid(OutputBuffer, InStr(OutputBuffer, "COD:"))
			//'        'Obtenemos los diagn�sticos y las prestaciones.
			//'        Do While InStr(OutputBuffer, "COD:")
			//'            'Comprobamos el tipo de c�digo
			//'
			//'            ' Vemos si hay GRD
			//'
			//'            If bEntornoWeb And InStr(OutputBuffer, "R" & stPAY & ".") < InStr(OutputBuffer, vbLf) And InStr(OutputBuffer, "R" & stPAY & ".") > 0 Then
			//'                stGRD = Mid(OutputBuffer, InStr(OutputBuffer, "R" & stPAY & ".") + Len("R" & stPAY & "."), InStr(OutputBuffer, vbLf) - (InStr(OutputBuffer, "R" & stPAY & ".") + Len("R" & stPAY & ".")))
			//'            Else
			//'                Select Case UCase(Mid(OutputBuffer, InStr(OutputBuffer, "COD:") + 5, 2))
			//'                    Case "R." 'Para el GRD
			//'                        If Not bEntornoWeb Then
			//'                            stGRD = Mid(OutputBuffer, InStr(OutputBuffer, "COD:") + 7, InStr(OutputBuffer, Chr(10)) - 1 - 7)
			//'                        End If
			//'                    Case "I.", "M." 'Para los diagn�sticos
			//'                        ReDim Preserve stDiag(UBound(stDiag) + 1)
			//'                        stDiag(UBound(stDiag)).CodDiag = Mid(OutputBuffer, InStr(OutputBuffer, "COD:") + 7, InStr(OutputBuffer, Chr(10)) - 1 - 7)
			//'                        stDiag(UBound(stDiag)).CodDiag = PonerPuntoEnDiagnostico(stDiag(UBound(stDiag)).CodDiag)
			//'                        'Si el siguiente COD: est� m�s all� que el siguiente POA:, es que el diagn�stico que estamos tratando s� tiene indicaci�nd del POA.
			//'                        If InStr(InStr(OutputBuffer, "COD:") + 3, OutputBuffer, "COD:") > InStr(OutputBuffer, "POA:") Then
			//'                            'stDiag(UBound(stDiag)).codPOA = Mid(OutputBuffer, InStr(OutputBuffer, "POA:") + 4, 1)
			//'                            If Trim(Mid(OutputBuffer, InStr(OutputBuffer, "POA:") + 4, 1)) <> ":" Then stDiag(UBound(stDiag)).codPOA = Mid(OutputBuffer, InStr(OutputBuffer, "POA:") + 4, 1)
			//'                        End If
			//'                    Case "S." 'Para los procedimientos
			//'                        ReDim Preserve stProc(UBound(stProc) + 1)
			//'                        stProc(UBound(stProc)) = Mid(OutputBuffer, InStr(OutputBuffer, "COD:") + 7, InStr(OutputBuffer, Chr(10)) - 1 - 7)
			//'                        stProc(UBound(stProc)) = PonerPuntoEnProcedimiento(stProc(UBound(stProc)))
			//'                    Case "MD" ' Para la categor�a diagn�stica mayor
			//'                        stMDC = Mid(OutputBuffer, InStr(OutputBuffer, "COD:") + 9, InStr(OutputBuffer, Chr(10)) - 1 - 9)
			//'                End Select
			//'            End If
			//'
			//'            OutputBuffer = Mid(OutputBuffer, InStr(InStr(OutputBuffer, "TEXT"), OutputBuffer, Chr(10)) + 1)
			//'        Loop
			//'
			//'        stPeso = ""
			//'        icont = 1
			//'
			//'        If bEntornoWeb Then
			//'
			//'            Dim stTemp      As String
			//'            'Dim stResultado As String
			//'
			//'            Do While InStr(OutputBuffer, stSeparador)
			//'
			//'                stTemp = Mid(OutputBuffer, 1, InStr(OutputBuffer, stSeparador) - 1)
			//'
			//'                If InStr(stTemp, "DRGWT") Then
			//'
			//'                    ' Peso
			//'
			//'                    stPeso = Mid(stTemp, InStr(stTemp, ":") + 1)
			//'
			//'                ElseIf InStr(stTemp, "ALOS") Then
			//'
			//'                    ' E. Medio
			//'
			//'                    'stResultado = Mid(stTemp, InStr(stTemp, ":") + 1)
			//'
			//'                ElseIf InStr(stTemp, "LOWT") Then
			//'
			//'                    ' P. Crte. Bajo
			//'
			//'                    'stResultado = Mid(stTemp, InStr(stTemp, ":") + 1)
			//'
			//'                ElseIf InStr(stTemp, "HIGT") Then
			//'
			//'                    ' P. Crte. Alto
			//'
			//'                    'stResultado = Mid(stTemp, InStr(stTemp, ":") + 1)
			//'
			//'                End If
			//'
			//'                OutputBuffer = Mid(OutputBuffer, InStr(OutputBuffer, stSeparador) + 1)
			//'
			//'            Loop
			//'
			//'        Else
			//'
			//'            If bExistePeso And InStr(UCase(OutputBuffer), "PESO") Then
			//'                OutputBuffer = Trim(Mid(OutputBuffer, InStr(UCase(OutputBuffer), "PESO") + 5))
			//'                Do While IsNumeric(Mid(OutputBuffer, icont, 1)) Or Mid(OutputBuffer, icont, 1) = "."
			//'                    stPeso = stPeso & Mid(OutputBuffer, icont, 1)
			//'                    icont = icont + 1
			//'                Loop
			//'            End If
			//'
			//'        End If
			//'
			//'        DevolverDatodGRD formCod, rc, stErr, stDiag, stProc, stGRD, stMDC, stSepDeci, stPeso
			//'
			//'    End If
			//'
			//'    ' Release the instance of Codefinder. This will terminate the 3M Coding and Reimbursement application
			//'
			//'    StatusMsg = nReleaseInstance(Codefinder)
			//'
			//'    ' Release CFINT32.DLL from memory
			//'
			//'    StatusMsg = FreeLibrary(DLLInstance)
			//'
			//'    Codefinder = 0
			//'    DLLInstance = 0
		}

		internal static string PonerPuntoEnDiagnostico(string Cod)
		{
			if (Cod.Substring(0, Math.Min(1, Cod.Length)).ToUpper() != "M")
			{
				//La variable Cod viene con cinco caracteres y se devuelve con seis.
				if (Cod.Substring(0, Math.Min(1, Cod.Length)).ToUpper() == "E")
				{
					//Si es un c�digo del tipo E, el punto va en la quinta posici�n
					if (Cod.Trim().Length > 4)
					{
						//Hay c�digos con cuatro carcteres y c�digos con cinco. A los �ltimos les colocamos el punto
						Cod = Cod.Substring(0, Math.Min(4, Cod.Length)) + "." + Cod.Substring(4);
					}
				}
				else
				{
					//El resto, el punto va en la cuarta posici�n
					if (Cod.Trim().Length > 3)
					{
						//S�lo si tiene m�s de tres posiciones, le colocamos el punto
						Cod = Cod.Substring(0, Math.Min(3, Cod.Length)) + "." + Cod.Substring(3);
					}
				}
			}
			return Cod;
		}
		internal static string PonerPuntoEnProcedimiento(string Cod)
		{
			//La variable Cod viene con cuatro caracteres y se devuelve con cinco.
			//Los procedimientos tienen el punto en la tercera posici�n.
			if (Cod.Trim().Length > 2)
			{
				//S�lo si tiene m�s de tres posiciones, le colocamos el punto
				Cod = Cod.Substring(0, Math.Min(2, Cod.Length)) + "." + Cod.Substring(2);
			}
			return Cod;
		}
		internal static string PrepararCadenas(ref string stcodigos, string stDiferenciador, string stSepar)
		{
			string stCadenaparcial = String.Empty;
			int iPosicionPrimeraArroba = 0;
			int iPosicionSegundaArroba = 0;
			string stDiferReal = String.Empty;
			//Cuando no exista el car�cter "@" y el car�cter ".", salimos

			while(stcodigos.IndexOf('@') >= 0)
			{
				iPosicionPrimeraArroba = (stcodigos.IndexOf('@') + 1);
				//Buscamos la segunda @
				if (Strings.InStr(iPosicionPrimeraArroba + 1, stcodigos, "@", CompareMethod.Binary) == 0)
				{
					//Es el �ltimo diagnostico
					iPosicionSegundaArroba = stcodigos.Length + 1;
				}
				else
				{
					iPosicionSegundaArroba = Strings.InStr(iPosicionPrimeraArroba + 1, stcodigos, "@", CompareMethod.Binary);
				}
				//Es la cadena correspondiente a un c�digo
				stCadenaparcial = stcodigos.Substring(iPosicionPrimeraArroba - 1, Math.Min(iPosicionSegundaArroba - iPosicionPrimeraArroba, stcodigos.Length - (iPosicionPrimeraArroba - 1)));
				//Si esa cadena parcial no tiene punto, pos no se quita.....joder
				if (stCadenaparcial.IndexOf('.') >= 0)
				{
					stCadenaparcial = stCadenaparcial.Substring(0, Math.Min(stCadenaparcial.IndexOf('.'), stCadenaparcial.Length)) + stCadenaparcial.Substring(stCadenaparcial.IndexOf('.') + 1);
				}
				//Si el c�digo del diagn�tico es morfol�gico, no se pone difenciador
				if (((stCadenaparcial.IndexOf('M') + 1) & ((stDiferenciador == "I") ? -1 : 0)) != 0)
				{
					stDiferReal = "M";
				}
				else
				{
					stDiferReal = stDiferenciador;
				}
				//Quita el car�cter "@" y lo sustituye por la cadena "COD:"
				stCadenaparcial = "COD:" + stDiferReal + "." + stCadenaparcial.Substring(1) + stSepar;
				//Buscamos la segunda @ otra vez
				if (Strings.InStr(iPosicionPrimeraArroba + 1, stcodigos, "@", CompareMethod.Binary) == 0)
				{
					//Es el �ltimo diagnostico
					iPosicionSegundaArroba = stcodigos.Length + 1;
				}
				else
				{
					iPosicionSegundaArroba = Strings.InStr(iPosicionPrimeraArroba + 1, stcodigos, "@", CompareMethod.Binary);
				}
				stcodigos = stcodigos.Substring(0, Math.Min(Strings.InStr(iPosicionPrimeraArroba, stcodigos, "@", CompareMethod.Binary) - 1, stcodigos.Length)) + stCadenaparcial + stcodigos.Substring(iPosicionSegundaArroba - 1);
			};
			return stcodigos;
		}

		internal static void DevolverDatodGRD(object formCod, object rc, string[] error, Serrores.diagnostico[] Diag, string[] Proce, string stGRD, string stMDC, string stSepDeci, string stPeso)
		{
			//'    *************** CURIAE: A REVISAR POR EL EQUIPO DE MIGRACION ********************
			//'              NUEVA FORMA DE INTEGRARSE CON SISTEMA CODIFICADOR
			//'    *************************************************************************

			//'    Dim RrSql As rdoResultset
			//'    Dim icont As Integer
			//'    Dim StSql As String
			//'    Dim iNumColBorr As Integer
			//'    Dim oClassErrores As Object
			//'    Dim FORMQUIROFANOS As String
			//'    Dim stCodTipIng As String
			//'    Dim stDesTipIng As String
			//'    ReDim stCodProc(2, 0) As String
			//'    ReDim stCodDiagn(2, 0) As String
			//'    Dim jCont As Integer
			//'    Dim btest As Boolean
			//'    Dim sqlAux As String
			//'    Dim RRAux As rdoResultset
			//'
			//'    FORMQUIROFANOS = "QAQ330F1"
			//'    Set oClassErrores = CreateObject("Mensajes.ClassMensajes")
			//'
			//'    If error(0) = "0" Then
			//'      ''Borramos los diagn�sticos de tipo 'A'(Principal), 'O'(secundarios) y 'M'(morfol�gicos) que no est�n borrados.
			//'        If UCase(formCod.Name) = FORMQUIROFANOS Then
			//'           'Para la funcionalidad de Quir�fanos (borramos el diagn�stico principal y secundario)
			//'           formCod.tbCodDiag(1).Text = ""
			//'           formCod.tbNomDiag(1).Text = ""
			//'           formCod.tbDiagnosSec.Text = ""
			//'           formCod.txtDesDiagnosSec.Text = ""
			//'        Else 'Para la funcionalidad de Archivo
			//'            iNumColBorr = 0
			//'            formCod.SprDiagnostico.MaxRows = 0
			//'            For icont = 1 To formCod.SprDiagnostico.MaxRows
			//'                If icont > formCod.SprDiagnostico.MaxRows - iNumColBorr Then Exit For
			//'                formCod.SprDiagnostico.Row = icont
			//'                formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_CODIGO_TIPO '5
			//'                If (UCase(formCod.SprDiagnostico.Text) = "O" Or UCase(formCod.SprDiagnostico.Text) = "A" Or UCase(formCod.SprDiagnostico.Text) = "M") And formCod.SprDiagnostico.ForeColor <> RGB(223, 223, 223) Then
			//'
			//'                    'C�digo
			//'                    formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_CODIGO ' 2
			//'                    stCodDiagn(0, UBound(stCodDiagn, 2)) = Trim(formCod.SprDiagnostico.Text)
			//'                    'Tipo
			//'                    formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_CODIGO_TIPO_ING
			//'                    stCodDiagn(1, UBound(stCodDiagn, 2)) = Trim(formCod.SprDiagnostico.Text)
			//'                    'Prioridad
			//'                    formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_PRIORIDAD
			//'                    stCodDiagn(2, UBound(stCodDiagn, 2)) = Trim(formCod.SprDiagnostico.Text)
			//'
			//'                    'Aumentamos
			//'                    ReDim Preserve stCodDiagn(2, UBound(stCodDiagn, 2) + 1)
			//'                    'Borramos la fila
			//'
			//'                    formCod.SprDiagnostico.Row = icont
			//'                    formCod.SprDiagnostico.Action = 5
			//'                    iNumColBorr = iNumColBorr + 1
			//'                    'Cuando se borra una fila, la siguiete se viene a la que hemos borrado.
			//'                    icont = icont - 1
			//'                End If
			//'            Next
			//'            formCod.SprDiagnostico.MaxRows = formCod.SprDiagnostico.MaxRows - iNumColBorr
			//'            If UBound(stCodDiagn, 2) > 1 Then ReDim Preserve stCodDiagn(2, UBound(stCodDiagn, 2) - 1)
			//'        End If
			//'
			//'        'borramos los procedimientos que no est�n borrados.
			//'        If UCase(formCod.Name) = FORMQUIROFANOS Then
			//'           formCod.tbProcCIE1.Text = ""
			//'           formCod.TxtDesProcCIE1.Text = ""
			//'           formCod.tbProcCIE2.Text = ""
			//'           formCod.TxtDesProcCIE2.Text = ""
			//'        Else
			//'            iNumColBorr = 0
			//'            For icont = 1 To formCod.Sprcie.MaxRows
			//'                If icont > formCod.Sprcie.MaxRows - iNumColBorr Then Exit For
			//'                formCod.Sprcie.Row = icont
			//'                formCod.Sprcie.Col = 1
			//'                If formCod.Sprcie.ForeColor <> RGB(223, 223, 223) Then
			//'                    'C�digo
			//'                    stCodProc(0, UBound(stCodProc, 2)) = Trim(formCod.Sprcie.Text)
			//'                    'Prioridad
			//'                    formCod.Sprcie.Col = 2
			//'                    stCodProc(1, UBound(stCodProc, 2)) = Trim(formCod.Sprcie.Text)
			//'                    'Lateralidad
			//'                    formCod.Sprcie.Col = 8
			//'                    stCodProc(2, UBound(stCodProc, 2)) = Trim(formCod.Sprcie.Text)
			//'
			//'                    'Aumentamos
			//'                    ReDim Preserve stCodProc(2, UBound(stCodProc, 2) + 1)
			//'                    'Borramos la fila
			//'                    formCod.Sprcie.Row = icont
			//'                    formCod.Sprcie.Action = 5
			//'                    iNumColBorr = iNumColBorr + 1
			//'                    'Cuando se borra una fila, la siguiete se viene a la que hemos borrado.
			//'                    icont = icont - 1
			//'                End If
			//'            Next
			//'            formCod.Sprcie.MaxRows = formCod.Sprcie.MaxRows - iNumColBorr
			//'            If UBound(stCodProc, 2) > 1 Then ReDim Preserve stCodProc(2, UBound(stCodProc, 2) - 1)
			//'        End If
			//'
			//'
			//'        'Introducimos los diagn�sticos que vienen.
			//'        For icont = 1 To UBound(Diag())
			//'            StSql = "select dnombdia from ddiagnos where gcodidia='" & Diag(icont).CodDiag & "'"
			//'            Set RrSql = rc.OpenResultset(StSql, rdOpenKeyset)
			//'            If Not RrSql.EOF Then
			//'               If UCase(formCod.Name) = FORMQUIROFANOS Then
			//'                  'Como en quir�fanos no existe POA, no se pone....
			//'                  Select Case icont
			//'                     Case 1:
			//'                        formCod.tbCodDiag(1).Text = Diag(icont).CodDiag
			//'                        formCod.tbNomDiag(1).Text = RrSql("dnombdia")
			//'                     Case 2:
			//'                        formCod.tbDiagnosSec.Text = Diag(icont).CodDiag
			//'                        formCod.txtDesDiagnosSec.Text = RrSql("dnombdia")
			//'                  End Select
			//'               Else  'De nombre de formulario=FORMQUIROFANOS
			//'                  formCod.SprDiagnostico.MaxRows = formCod.SprDiagnostico.MaxRows + 1
			//'                  formCod.SprDiagnostico.Row = formCod.SprDiagnostico.MaxRows
			//'                  formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_DESCRIPCION '3
			//'                  formCod.SprDiagnostico.ForeColor = RGB(0, 150, 0)
			//'                  formCod.SprDiagnostico.Text = RrSql("dnombdia") & ""
			//'                  formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_CODIGO_TIPO '5
			//'                  formCod.SprDiagnostico.ForeColor = RGB(0, 150, 0)
			//'                  If icont = 1 Then
			//'                      StSql = "select dtipdiag from dtipdiag where gtipdiag='A'"
			//'                      formCod.SprDiagnostico.Text = "A"
			//'                  Else
			//'                      If Left(Diag(icont).CodDiag, 1) = "M" Then
			//'                          StSql = "select dtipdiag from dtipdiag where gtipdiag='M'"
			//'                          formCod.SprDiagnostico.Text = "M"
			//'                      Else
			//'                          StSql = "select dtipdiag from dtipdiag where gtipdiag='O'"
			//'                          formCod.SprDiagnostico.Text = "O"
			//'                      End If
			//'                  End If
			//'                  Set RrSql = rc.OpenResultset(StSql, rdOpenKeyset)
			//'                  formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_TIPO '1
			//'                  formCod.SprDiagnostico.ForeColor = RGB(0, 150, 0)
			//'                  If Not RrSql.EOF Then formCod.SprDiagnostico.Text = RrSql("dtipdiag") & ""
			//'                  formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_CODIGO ' 2
			//'                  formCod.SprDiagnostico.ForeColor = RGB(0, 150, 0)
			//'                  formCod.SprDiagnostico.Text = Diag(icont).CodDiag
			//'
			//'
			//'
			//'                  btest = False
			//'
			//'                  '------------------
			//'                  '- David 17/06/2014
			//'                  '---- Se quita la comprobaci�n de si estaba antes. Se pondr� la misma prioridad que se establezca en el
			//'                  '---- DRG Finder.
			//'                  '------------------
			//'
			//'                  'Comprobamos si el c�digo estaba antes
			//''                  For jCont = 0 To UBound(stCodDiagn, 2)
			//''                        btest = (formCod.SprDiagnostico.Text = stCodDiagn(0, jCont))
			//''                        If btest Then Exit For
			//''                  Next
			//'
			//'
			//'                  'OSCAR C FEB 2006
			//'                  formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_PRIORIDAD
			//'                  formCod.SprDiagnostico.ForeColor = RGB(0, 150, 0)
			//'                  formCod.SprDiagnostico.Value = icont = 1
			//'
			//'                  If btest Then
			//'                        formCod.SprDiagnostico.Text = stCodDiagn(2, jCont)
			//'                  Else
			//'                        formCod.SprDiagnostico.Text = icont
			//'                  End If
			//'                  '----
			//'                  'RUB�N S ABR 2011
			//'                  stCodTipIng = ""
			//'                  stDesTipIng = ""
			//'                  'Si viene c�digo POA o hemos encontrado el diagn�stico.
			//'                  If Diag(icont).codPOA <> "" Or btest Then
			//'                        'Prima el que venga el c�digo.
			//'                        If Diag(icont).codPOA <> "" Then
			//'                            StSql = "select * from DDIAGEIN where gcodipoa='" & Diag(icont).codPOA & "'"
			//'                        Else
			//'                            StSql = "select * from DDIAGEIN where GDIAGEIN='" & stCodDiagn(1, jCont) & "'"
			//'                        End If
			//'                        Set RrSql = rc.OpenResultset(StSql, rdOpenKeyset)
			//'                        If Not RrSql.EOF Then
			//'                            stCodTipIng = RrSql("gdiagein")
			//'                            stDesTipIng = RrSql("ddiagein")
			//'                        End If
			//'                  End If
			//'                  '----
			//'                  'OSCAR C Enero 2010
			//'                  formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_TIPO_ING
			//'                  formCod.SprDiagnostico.ForeColor = RGB(0, 150, 0)
			//'                  formCod.SprDiagnostico.Text = stDesTipIng
			//'                  formCod.SprDiagnostico.Col = formCod.FCOL_DIAG_CODIGO_TIPO_ING
			//'                  formCod.SprDiagnostico.ForeColor = RGB(0, 150, 0)
			//'                  formCod.SprDiagnostico.Text = stCodTipIng
			//'                  '-------
			//'
			//'               End If 'De nombre de formulario=FORMQUIROFANOS
			//'            Else
			//'                oClassErrores.RespuestaMensaje vNomAplicacion, vAyuda, 1100, rc, "Diagn�stico" & " " & Diag(icont).CodDiag
			//'            End If
			//'            RrSql.Close
			//'        Next
			//'
			//'        'Introducimos los procedimientos que vienen.
			//'        For icont = 1 To UBound(Proce())
			//'            StSql = "select dnombcie, iquirurg from dproccie where gcodicie='" & Proce(icont) & "'"
			//'            Set RrSql = rc.OpenResultset(StSql, rdOpenKeyset)
			//'            If Not RrSql.EOF Then
			//'               If UCase(formCod.Name) = FORMQUIROFANOS Then
			//'                  Select Case icont
			//'                     Case 1:
			//'                        formCod.tbProcCIE1.Text = Proce(icont)
			//'                        formCod.TxtDesProcCIE1.Text = RrSql("dnombcie") & ""
			//'                     Case 2:
			//'                        formCod.tbProcCIE2.Text = Proce(icont)
			//'                        formCod.TxtDesProcCIE2.Text = RrSql("dnombcie") & ""
			//'                  End Select
			//'               Else  ' De si el nombre del formulario es FORMQUIROFANOS
			//'                  formCod.Sprcie.MaxRows = formCod.Sprcie.MaxRows + 1
			//'                  formCod.Sprcie.Row = formCod.Sprcie.MaxRows
			//'                  formCod.Sprcie.Col = 1
			//'                  formCod.Sprcie.ForeColor = RGB(0, 150, 0)
			//'                  formCod.Sprcie.Text = Proce(icont)
			//'
			//'                  btest = False
			//'                  'Comprobamos si el c�digo estaba antes
			//'
			//'                  '------------------
			//'                  '- David 17/06/2014
			//'                  '---- Se quita la comprobaci�n de si estaba antes. Se pondr� la misma prioridad que se establezca en el
			//'                  '---- DRG Finder.
			//'                  '------------------
			//'
			//''                  For jCont = 0 To UBound(stCodProc, 2)
			//''                        btest = (formCod.Sprcie.Text = stCodProc(0, jCont))
			//''                        If btest Then Exit For
			//''                  Next
			//'
			//'                  formCod.Sprcie.Col = 2
			//'                  formCod.Sprcie.ForeColor = RGB(0, 150, 0)
			//'                  formCod.Sprcie.Value = icont = 1
			//'
			//'                  If btest Then
			//'                    formCod.Sprcie.Text = stCodProc(1, jCont)
			//'                  Else
			//'                    formCod.Sprcie.Text = icont
			//'                  End If
			//'
			//'                  formCod.Sprcie.Col = 3
			//'                  formCod.Sprcie.ForeColor = RGB(0, 150, 0)
			//'                  formCod.Sprcie.Text = RrSql("dnombcie") & ""
			//'                  formCod.Sprcie.Col = 4
			//'                  formCod.Sprcie.ForeColor = RGB(0, 150, 0)
			//'                  formCod.Sprcie.Text = RrSql("iquirurg") & ""
			//'
			//'
			//'                  'Nos posicionamos en la fecha de realizaci�n.
			//'                  formCod.Sprcie.Col = 5
			//'                  'Si la fecha de realizaci�n de la pesta�a de procedimientos CIE tiene datos, lo incorporamos ..
			//'                  If formCod.SDCFechaRealizacion.Text <> "" Then
			//'                      formCod.Sprcie.ForeColor = RGB(0, 150, 0)
			//'                      formCod.Sprcie.Text = formCod.SDCFechaRealizacion.Text
			//'                  Else
			//'                      'Si la fecha de alta tiene dato, lo incorporamos
			//'                      If Trim(formCod.LB_fec_alta.Caption) <> "" Then
			//'                          formCod.Sprcie.ForeColor = RGB(0, 150, 0)
			//'                          formCod.Sprcie.Text = Format(formCod.LB_fec_alta.Caption, "dd/mm/yyyy")
			//'                      End If
			//'                  End If
			//'
			//'
			//'                  formCod.Sprcie.Col = 1
			//'                  For jCont = 0 To UBound(stCodProc, 2)
			//'                        btest = (formCod.Sprcie.Text = stCodProc(0, jCont))
			//'                        If btest Then Exit For
			//'                  Next
			//'                  If btest Then
			//'                        formCod.Sprcie.Col = 8
			//'                        formCod.Sprcie.Text = stCodProc(2, jCont)
			//'                        sqlAux = "select dlateral from DLATERAL where glateral = '" & stCodProc(2, jCont) & "'"
			//'                        Set RRAux = rc.OpenResultset(sqlAux, 1, 1)
			//'
			//'                        If Not RRAux.EOF Then
			//'                             formCod.Sprcie.Col = 7
			//'                             formCod.Sprcie.Text = RRAux("dlateral") & ""
			//'                        End If
			//'                        RRAux.Close
			//'                  Else
			//'                        formCod.Sprcie.Col = 8
			//'                        formCod.Sprcie.Text = ""
			//'                        formCod.Sprcie.Col = 7
			//'                        formCod.Sprcie.Text = ""
			//'                  End If
			//'
			//'
			//'               End If ' De si el nombre del formulario es FORMQUIROFANOS
			//'            Else
			//'                oClassErrores.RespuestaMensaje vNomAplicacion, vAyuda, 1100, rc, "Procedimiento" & " " & Proce(icont)
			//'            End If
			//'            RrSql.Close
			//'        Next
			//'
			//''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			//'        If UCase(formCod.Name) <> FORMQUIROFANOS Then
			//'            iGRD = Val(stGRD)
			//'            If stPeso = "" Then
			//'                If Trim(stGRD) <> "" Then
			//'                    StSql = "select npesogrd from dpesogrd where ggrd = " & stGRD
			//'                    Set RrSql = rc.OpenResultset(StSql, rdOpenKeyset)
			//'                    If Not RrSql.EOF Then
			//'                        dPesoGDR = ConvertirDecimales(IIf(IsNull(RrSql("npesogrd")), "", RrSql("npesogrd")), stSepDeci, 4)
			//'                    End If
			//'                End If
			//'                formCod.LB_Peso.Caption = IIf(Not IsNull(dPesoGDR), CStr(dPesoGDR), "")
			//'            Else
			//'                formCod.LB_Peso.Caption = IIf(Not IsNull(stPeso), CStr(stPeso), "")
			//'                dPesoGDR = ConvertirDecimales(stPeso, stSepDeci, 4)
			//'            End If
			//'            formCod.LB_GRD.Caption = IIf(Not IsNull(iGRD), CStr(iGRD), "")
			//'
			//'        End If
			//''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			//'    Else
			//'        'Esto es un error que devuelve 3M
			//'        MsgBox error(1)
			//'    End If
			//'     Set oClassErrores = Nothing
		}

		private static void LiteralesEdad(SqlConnection ParaConexion, string idioma)
		{

			string lIdioma = String.Empty;

			if (litAno.Trim() != "" || litAnos.Trim() != "" || litMes.Trim() != "" || litMeses.Trim() != "" || litDia.Trim() != "" || litDias.Trim() != "")
			{
				return;
			}

			if (idioma == "")
			{
				lIdioma = "IDIOMA0";
			}
			else
			{
				lIdioma = idioma.Trim().ToUpper();
			}

			string sql = "SELECT * FROM SCONSGLO WHERE gconsglo='EDADPACS'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				switch(lIdioma)
				{
					case "IDIOMA0" : 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litAno = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litMes = Convert.ToString(RR.Tables[0].Rows[0]["valfanu2"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litDia = Convert.ToString(RR.Tables[0].Rows[0]["valfanu3"]).Trim() + ""; 
						break;
					case "IDIOMA1" : 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litAno = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1i1"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litMes = Convert.ToString(RR.Tables[0].Rows[0]["valfanu2i1"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litDia = Convert.ToString(RR.Tables[0].Rows[0]["valfanu3i1"]).Trim() + ""; 
						break;
					case "IDIOMA2" : 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litAno = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1i2"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litMes = Convert.ToString(RR.Tables[0].Rows[0]["valfanu2i2"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litDia = Convert.ToString(RR.Tables[0].Rows[0]["valfanu3i2"]).Trim() + ""; 
						break;
				}
			}

			sql = "SELECT * FROM SCONSGLO WHERE gconsglo='EDADPACP'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, ParaConexion);
			RR = new DataSet();
			tempAdapter_2.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				switch(lIdioma)
				{
					case "IDIOMA0" : 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litAnos = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litMeses = Convert.ToString(RR.Tables[0].Rows[0]["valfanu2"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litDias = Convert.ToString(RR.Tables[0].Rows[0]["valfanu3"]).Trim() + ""; 
						break;
					case "IDIOMA1" : 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litAnos = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1i1"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litMeses = Convert.ToString(RR.Tables[0].Rows[0]["valfanu2i1"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litDias = Convert.ToString(RR.Tables[0].Rows[0]["valfanu3i1"]).Trim() + ""; 
						break;
					case "IDIOMA2" : 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litAnos = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1i2"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litMeses = Convert.ToString(RR.Tables[0].Rows[0]["valfanu2i2"]).Trim() + ""; 
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx 
						litDias = Convert.ToString(RR.Tables[0].Rows[0]["valfanu3i2"]).Trim() + ""; 
						break;
				}
			}

		}

		internal static string CalcularEdad(System.DateTime Fecha1, System.DateTime Fecha2, SqlConnection ParaConexion, string ParaIdioma = "")
		{

			LiteralesEdad(ParaConexion, ParaIdioma);

			int dias = (int) DateAndTime.DateDiff("d", Fecha1, Fecha2, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
			int meses = (int) DateAndTime.DateDiff("m", Fecha1, Fecha2, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
			int a�os = (int) DateAndTime.DateDiff("yyyy", Fecha1, Fecha2, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
			if (meses > 0)
			{
				if (Fecha1.AddMonths(meses) > Fecha2)
				{
					meses--;
				}
			}
			if (a�os > 0)
			{
				if (Fecha1.AddYears(a�os) > Fecha2)
				{
					a�os--;
				}
			}
			if (meses > 23)
			{
				if (a�os == 1)
				{
					return "1 " + litAno;
				}
				else
				{
					return a�os.ToString() + " " + litAnos;
				}
			}
			else
			{
				if (meses > 0)
				{
					if (meses == 1)
					{
						return "1 " + litMes;
					}
					else
					{
						return meses.ToString() + " " + litMeses;
					}
				}
				else
				{
					if (dias == 1)
					{
						return "1 " + litDia;
					}
					else
					{
						return dias.ToString() + " " + litDias;
					}
				}
			}

		}

		//Oscar C 28/10/04
		//SOLO FUNCIONA PARA PACIENTES CUYA SOCIEDAD EN DPACIENT SEA PRIVADO
		//TRUE --> Paciente bloqueado
		//FALSE --> Paciente No bloqueado
		internal static bool bPacienteBloqueado(ref SqlConnection ParaConexion, string ParaGidenpac)
		{
			bool result = false;
			Mensajes.ClassMensajes obML = null;


			string sql = "SELECT nnumeri1 from sconsglo where gconsglo='PRIVADO'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			int lCodPriv = Convert.ToInt32(RR.Tables[0].Rows[0]["nnumeri1"]);
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			sql = "SELECT gsocieda, isnull(ibloqueo,'N') as ibloqueo from DPACIENT Where gidenpac='" + ParaGidenpac + "'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, ParaConexion);
			RR = new DataSet();
			tempAdapter_2.Fill(RR);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			if (Convert.ToDouble(RR.Tables[0].Rows[0]["gsocieda"]) == lCodPriv)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(RR.Tables[0].Rows[0]["iBloqueo"]).Trim().ToUpper() == "S")
				{
					obML = new Mensajes.ClassMensajes();
					short tempRefParam = 3920;
					object tempRefParam2 = new object{};
					obML.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref ParaConexion, ref tempRefParam2);
					obML = null;
					result = true;
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			return result;
		}
		//---------------

		internal static object ActualizaVariable(SqlConnection ParaConexion, string stCampo, string stTabla, string stCondicion)
		{
			object result = null;
			string sql = "SELECT " + stCampo + " from " + stTabla + stCondicion;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (!Convert.IsDBNull(RR.Tables[0].Rows[0][" & stcampo & "]))
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = RR.Tables[0].Rows[0][" & stcampo & "];
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			return result;
		}
		//---------------
		internal static string LiteralOtrasOpcionesCliente(int NValfanu, SqlConnection ParaConexion)
		{
			string result = String.Empty;
			result = "";
			string sql = "SELECT valfanu" + NValfanu.ToString() + " from sconsglo where gconsglo = 'LITOPCLI'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0][0]))
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					result = Strings.StrConv(Convert.ToString(RR.Tables[0].Rows[0][0]), VbStrConv.ProperCase, 0).Trim();
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			return result;
		}

		internal static void PonerVistas(ref string sqlOriginal, ref string Tabla, SqlConnection ParaConexion)
		{


			// Si la base de datos no est� historizada, no cambiamos nada

			if (!BDHisto(ParaConexion) || !vfMostrandoHistoricos)
			{
				return;
			}

			string strTmp = sqlOriginal.ToUpper();
			Tabla = Tabla.ToUpper();
			string strVista = "VH";

			int intPos = (strTmp.IndexOf(Tabla) + 1);


			while(intPos > 0)
			{
				strTmp = (strTmp.Substring(0, Math.Min(intPos + Tabla.Length - 1, strTmp.Length)) + strVista + strTmp.Substring(intPos + Tabla.Length - 1));
				intPos = Strings.InStr(intPos + Tabla.Length, strTmp, Tabla, CompareMethod.Binary);
			};

			sqlOriginal = strTmp;

		}

		internal static bool BDHisto(SqlConnection ParaConexion)
		{


			bool result = false;

			// Vemos si la BD est� historizada

			string sql = "select isNull(valfanu1,'N') as valfanu1 from sconsglo where gconsglo = 'FHISTO'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RrHisto = new DataSet();
			tempAdapter.Fill(RrHisto);

			if (RrHisto.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = Convert.ToString(RrHisto.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S";
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrHisto.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrHisto.Close();

			return result;
		}

		internal static string TipoHisto(SqlConnection ParaConexion)
		{


			string result = String.Empty;
			result = "N";

			// Vemos si la BD est� historizada

			string sql = "select isNull(valfanu3,'N') as valfanu3 from sconsglo where gconsglo = 'FHISTO'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RrHisto = new DataSet();
			tempAdapter.Fill(RrHisto);

			if (RrHisto.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = Convert.ToString(RrHisto.Tables[0].Rows[0]["valfanu3"]).Trim().ToUpper();
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrHisto.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrHisto.Close();

			return result;
		}


		internal static string ObternerValor_CTEGLOBAL(SqlConnection ParaConexion, string ParaCTE, string ParaValor)
		{

			string result = String.Empty;
			result = "";

			string sql = "SELECT " + ParaValor + " FROM SCONSGLO WHERE GCONSGLO='" + ParaCTE + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = (Convert.ToString(RR.Tables[0].Rows[0][0]) + "").Trim().ToUpper();
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();

			return result;
		}


		//Devuelve el ItemData asociado a un texto en un combo
		internal static int fnBuscaItemdata(ComboBox ParaCombo, string paraTexto)
		{
			for (int I = 0; I <= ParaCombo.Items.Count - 1; I++)
			{
				if (ParaCombo.GetListItem(I).Trim() == paraTexto.Trim())
				{
					return ParaCombo.GetItemData(I);
				}
			}
			return 0;
		}

		//devuelve el ListIndex asociado a un texto en un combo
		internal static int fnBuscaListIndexT(ComboBox ParaCombo, string paraTexto)
		{
			for (int I = 0; I <= ParaCombo.Items.Count - 1; I++)
			{
				if (ParaCombo.GetListItem(I).Trim() == paraTexto.Trim())
				{
					return I;
				}
			}
			return 0;
		}

		//devuelve el ListIndex asociado a un itemdata en un combo
		internal static int fnBuscaListIndexID(ComboBox ParaCombo, string paraItemData)
		{
			for (int I = 0; I <= ParaCombo.Items.Count - 1; I++)
			{
				if (ParaCombo.GetItemData(I).ToString().Trim() == paraItemData.Trim())
				{
					return I;
				}
			}
			return 0;
		}

		//FUNCION QUE RECIBE UN STRING Y LE QUITA TODOS LOS CARACTERES NO NUMERICOS
		internal static string fQuitaCaracteresNoNumericos(string PARA)
		{
			string result = String.Empty;
			result = "";
			if (PARA.Trim() == "")
			{
				return result;
			}

			string c = String.Empty;
			StringBuilder CadenaGuena = new StringBuilder();
			for (int I = 1; I <= PARA.Length; I++)
			{
				c = PARA.Substring(I - 1, Math.Min(1, PARA.Length - (I - 1)));
				double dbNumericTemp = 0;
				if (Double.TryParse(c, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
				{
					CadenaGuena.Append(c);
				}
			}

			return CadenaGuena.ToString();
		}


		//Procedimiento que devuelve un array de n elementos en funcion de la cadena
		//y separador enviados.
		//Ejemplo:
		//Si viene AA|BB|CC,
		//Devolveria un ARRAY de este tipo
		//elemento(0): AA
		//elemento(1): BB
		//elemento(2): CC

		internal static void proDevolverARRAY(ref string entrada, string separador, ref string[] salida)
		{

			StringBuilder tmpStr = new StringBuilder();
			int I = 0;
			bool Salir = false;

			salida = new string[]{String.Empty};

			while (!Salir)
			{

				if ((entrada.StartsWith(separador)) || entrada.Length == 0)
				{
					if (tmpStr.ToString().Trim() != "")
					{
						salida = ArraysHelper.RedimPreserve(salida, new int[]{I + 2});
						salida[I] = tmpStr.ToString();
						tmpStr = new StringBuilder("");
						entrada = entrada.Substring(1);
						I++;
					}
					else
					{
						entrada = entrada.Substring(1);
					}

					if (entrada.Length == 0)
					{
						Salir = true;
					}
				}
				else if (!entrada.StartsWith(" "))
				{ 
					tmpStr.Append(entrada.Substring(0, Math.Min(1, entrada.Length)));
					entrada = entrada.Substring(1);
				}
				else
				{
					entrada = entrada.Substring(1);
				}

			}

		}

		internal static int fPerfilCitacionUsuario(SqlConnection ParaConexion, string paraUsuario)
		{
			int result = 0;
			string StSql = "select ISNULL(gpercita,0) gpercita from SUSUARIO where gusuario='" + paraUsuario + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, ParaConexion);
			DataSet rsd = new DataSet();
			tempAdapter.Fill(rsd);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			result = Convert.ToInt32(rsd.Tables[0].Rows[0]["gpercita"]);
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rsd.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rsd.Close();
			return result;
		}

		internal static bool fPermisoUsuarioParaModificarCita(SqlConnection ParaConexion, string paraUsuario, int ParaAnoCita, int ParaNumCita)
		{

			//Funcion que mira si el usuario tiene el perfil de citacion correcto para poder
			//cambiar la cita
			//FALSE --  NO TIENE PERFIL DE CITACION ADECUADO PARA MODIFICAR LA CITA
			//TRUE  -- TIENE PERFIL DE CITACION ADECUADO PARA MODIFICAR LA CITA
			bool result = false;
			result = true;


			int PerfilCitacion = fPerfilCitacionUsuario(ParaConexion, paraUsuario);
			string StSql = "select count(*) FROM CCITPROG " + 
			               " INNER JOIN CPECITPR on CCITPROG.gagendas = CPECITPR.gagendas AND " + 
			               " CCITPROG.gbloques = CPECITPR.gbloques AND " + 
			               " CCITPROG.gprestac = CPECITPR.gprestac AND " + 
			               " ( CPECITPR.gpercita = " + PerfilCitacion.ToString() + " OR " + 
			               "   CPECITPR.gpercita IN (SELECT gpercita FROM CUSUPCIT where gusuario='" + paraUsuario + "') " + 
			               " ) " + 
			               " WHERE ganoregi = " + ParaAnoCita.ToString() + " AND gnumregi = " + ParaNumCita.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, ParaConexion);
			DataSet rsd = new DataSet();
			tempAdapter.Fill(rsd);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			if (Convert.ToDouble(rsd.Tables[0].Rows[0][0]) == 0)
			{
				result = false;
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rsd.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rsd.Close();

			return result;
		}

		//En Valdemoro han solicitado que el sistema avise y
		//(seg�n usuario) no deje registrar una cita cuando
		//entre la Fecha de Solicitud y la de la Cita Propuesta haya m�s de X d�as
		//(30 para la Comunidad de Madrid. Se pondr� en Constante global).
		//El tema es controlarlo en el Aceptar de Cita R�pida o Cita Simple o m�ltiple:
		//al restar la Fecha de la Cita de la Fecha de Solicitud, si da m�s de X d�as aparecer�:
		//"No debe registrar la Cita con Demora de m�s de X d�as. Revise Fecha de Solicitud y Tipo de Demora. Si est�n bien esos datos, p�ngase en contacto con el Gestor de Agendas".
		//Si el indicador de poder citar con demora = 'S', se podr� continuar a pesar de que esto ocurra.
		//Si el indicador est� a "N" o nulo, no se podr� registrar esa Cita.
		//TRUE: Puede seguir con el proceso de la cita
		//FALE: No se puede seguir con el proceso de la cia

		internal static bool fAvisoPacienteSiDemora(SqlConnection ParaConexion, string paraUsuario, int paraPerfilCitacion, System.DateTime paraFechaSolicitud, System.DateTime ParaFechaCita)
		{

			bool result = false;
			DialogResult irespuesta = (DialogResult) 0;

			bool bPermitirCitarConDemora = false;
			bool bCitaConDemora = false;
			result = true;

			int nDias = 0;

			string sql = "SELECT NNUMERI1 FROM SCONSGLO WHERE GCONSGLO='NDIAFSCP'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				nDias = Convert.ToInt32(Conversion.Val(Convert.ToString(RR.Tables[0].Rows[0]["nnumeri1"]) + ""));
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			string sMensaje = " Cita con Demora de m�s de " + nDias.ToString() + " d�as.";

			sql = "SELECT ISNULL(icitadem,'N') icitadem FROM SUSUARIO where gusuario='" + paraUsuario + "'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, ParaConexion);
			RR = new DataSet();
			tempAdapter_2.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(RR.Tables[0].Rows[0]["icitadem"]).Trim().ToUpper() == "S")
				{
					bPermitirCitarConDemora = true;
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();


			sql = "SELECT dmensaje dmensaje FROM cpercita where gpercita=" + paraPerfilCitacion.ToString();
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, ParaConexion);
			RR = new DataSet();
			tempAdapter_3.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["dmensaje"]))
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					sMensaje = Convert.ToString(RR.Tables[0].Rows[0]["dmensaje"]).Trim();
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();

			if (((int) DateAndTime.DateDiff("d", paraFechaSolicitud, ParaFechaCita, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) > nDias)
			{
				bCitaConDemora = true;
			}

			if (bCitaConDemora && nDias > 0)
			{
				if (bPermitirCitarConDemora)
				{
					//Esta citando una cita con demora aunque el indicador del usuario lo permite,
					//por lo que le damos la oportunidad de continuar o no.
					irespuesta = MessageBox.Show(sMensaje + Environment.NewLine + "� Desea Continuar ?", vNomAplicacion, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
					if (irespuesta == System.Windows.Forms.DialogResult.No)
					{
						result = false;
					}
				}
				else
				{
					//Esta citando una cita con demora y el indicador del usuario no lo permite,
					//por lo que no se deja continuar con el proceso de citacion.
					MessageBox.Show(sMensaje, vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
					result = false;
				}
			}

			return result;
		}

		internal static void fAvisoPaciente_Cita(SqlConnection ParaConexion, string parastIdenpac, int paraSociedad, ref string paraInspeccion)
		{

			//Solo se mostrara el mensaje si el paciente no pertenece a la comunidad del Hospital.
			//Para ello, compararemos el gcomauto (codigo INE de la Comunidad Autonoma) obtendio a traves
			//de la provincia del paciente, con el valor valfanu3 de la constante CODPROVI que es
			//el codigo INE de la comunidad autonoma del hospital.



			string stgcomauto_hosp = "";
			string stgcomauto_paci = "";
			ObtenerNombreAplicacion(ParaConexion);

			//1. Obtenemos la Comunidad Autonoma del Hospital (INE)
			stgcomauto_hosp = ObternerValor_CTEGLOBAL(ParaConexion, "CODPROVI", "VALFANU3");

			//2. Obtenemos la Comunidad Autonoma del paciente a traves de la provincia de su direccion.
			string StSql = "SELECT gprovinc FROM DPACIENT where gidenpac='" + parastIdenpac + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, ParaConexion);
			DataSet rsd = new DataSet();
			tempAdapter.Fill(rsd);
			if (rsd.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(rsd.Tables[0].Rows[0]["gprovinc"]))
				{

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					StSql = " SELECT DCOMAUTO.gcomauto from DCOMAUTO " + 
					        " INNER JOIN DPROVINC on DPROVINC.gcomauto = DCOMAUTO.gcomauto " + 
					        " Where gprovinc = '" + Convert.ToString(rsd.Tables[0].Rows[0]["gprovinc"]) + "'";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, ParaConexion);
					rsd = new DataSet();
					tempAdapter_2.Fill(rsd);
					if (rsd.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(rsd.Tables[0].Rows[0]["gcomauto"]))
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							stgcomauto_paci = Convert.ToString(rsd.Tables[0].Rows[0]["gcomauto"]).Trim();
						}
					}
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rsd.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rsd.Close();

			//3. Comparamos las Comunidades Autonomas, y si son difetentes, se muestra el correspondiente
			//mensaje de aviso en funcion de lo que se parametrize en la tabla CAVICITA para la socieda
			//e ispeccion
			if (paraInspeccion.Trim() == "")
			{
				paraInspeccion = "0";
			}

			if (stgcomauto_hosp.Trim() != stgcomauto_paci.Trim())
			{
				StSql = "SELECT iavicita , otexavis FROM CAVICITA WHERE " + 
				        "   gsocieda = " + paraSociedad.ToString() + " AND " + 
				        "   ginspecc = '" + paraInspeccion + "'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, ParaConexion);
				rsd = new DataSet();
				tempAdapter_3.Fill(rsd);
				if (rsd.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToString(rsd.Tables[0].Rows[0]["iavicita"]).Trim().ToUpper() == "S")
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						MessageBox.Show(Convert.ToString(rsd.Tables[0].Rows[0]["otexavis"]).Trim(), vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rsd.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rsd.Close();
			}

		}

		internal static bool fObligar_Procedencia_Cita(SqlConnection ParaConexion, int paraSociedad, ref string paraInspeccion)
		{
			bool result = false;
			if (paraInspeccion.Trim() == "")
			{
				paraInspeccion = "0";
			}
			string StSql = "SELECT ISNULL(ioblproc,'N') ioblproc FROM CAVICITA WHERE " + 
			               "   gsocieda = " + paraSociedad.ToString() + " AND " + 
			               "   ginspecc = '" + paraInspeccion + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, ParaConexion);
			DataSet rsd = new DataSet();
			tempAdapter.Fill(rsd);
			if (rsd.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(rsd.Tables[0].Rows[0]["ioblproc"]).Trim().ToUpper() == "S")
				{
					result = true;
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rsd.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rsd.Close();
			return result;
		}

		internal static string proReplace(ref string stTexto, ref string stOrigen, string stDestino)
		{

			try
			{

				int I = 0;

				I = (stTexto.IndexOf(stOrigen, StringComparison.CurrentCultureIgnoreCase) + 1);


				while(I > 0)
				{
					stTexto = (stTexto.Substring(0, Math.Min(I - 1, stTexto.Length)) + stDestino + stTexto.Substring(I + stOrigen.Length - 1));
					I = Strings.InStr(I + stOrigen.Length, stTexto, stOrigen, CompareMethod.Text);
				};


				return stTexto;
			}
			catch
			{

				return "";
			}
		}

		internal static bool proExisteTabla(string stTabla, SqlConnection rcConexion)
		{

			bool result = false;
			string StSql = String.Empty;
			DataSet RrDatos = null;

			// Si ya est� comprobado, nos ahorramos el hacerlo de nuevo

			if (fcomprobado)
			{
				result = fexiste;
			}
			else
			{
				StSql = "select '' from SYSOBJECTS where xtype = 'U' and name = '" + stTabla.Trim() + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				result = RrDatos.Tables[0].Rows.Count != 0;
				fcomprobado = true;
				fexiste = result;

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();
			}

			return result;
		}

		internal static string fOtenerIndicadorServicio(SqlConnection ParaCn, string ParaPrestacion, System.DateTime ParaFecha)
		{
			string result = String.Empty;
			result = "";
			//UPGRADE_WARNING: (1068) tempRefParam2 of type Variant is being forced to DateTime. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
			//UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to DateTime. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
			object tempRefParam = ParaFecha;
			object tempRefParam2 = ParaFecha;
			string sql = "SELECT ISNULL(ioblproc,'N') as ioblproc " + " FROM DSERVICI INNER JOIN DCODPRES ON DCODPRES.gservici = DSERVICI.gservici" + " WHERE " + " DCODPRES.gprestac='" + ParaPrestacion + "' and " + " DCODPRES.finivali<= " + FormatFechaHMS(ref tempRefParam) + " and " + " (DCODPRES.ffinvali>=" + FormatFechaHMS(ref tempRefParam2) + " or DCODPRES.ffinvali is null)";
			ParaFecha = Convert.ToDateTime(tempRefParam2);
			ParaFecha = Convert.ToDateTime(tempRefParam);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaCn);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = Convert.ToString(RR.Tables[0].Rows[0]["ioblproc"]).ToUpper() + "";
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			return result;
		}
		internal static bool fCompletarOrigen(SqlConnection cnConex, RadioButton optPropioCentro, RadioButton optCentroExterno, string stPrestacion, System.DateTime dtFechaCita, ComboBox cbbServicioSol, ComboBox cbbMedicoSol, ComboBox CbbCentro, TextBox txMediServi, object[] armensa)
		{


			Mensajes.ClassMensajes objClass = new Mensajes.ClassMensajes();
			string ioblproc = fOtenerIndicadorServicio(cnConex, stPrestacion, dtFechaCita);
			if (ioblproc != "N")
			{
				if (!optPropioCentro.Checked && !optCentroExterno.Checked)
				{
					short tempRefParam = 1040;
					object tempRefParam2 = new object{armensa[0]};
					objClass.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam, ref cnConex, ref tempRefParam2);
					return false;
				}

				if (optCentroExterno.Checked)
				{
					if (CbbCentro.SelectedIndex < 1)
					{
						short tempRefParam3 = 1040;
						object tempRefParam4 = new object{"Centro origen"};
						objClass.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam3, ref cnConex, ref tempRefParam4);
						return false;
					}
				}

				switch(ioblproc)
				{
					case "A" : 
						if (cbbServicioSol.Enabled && cbbServicioSol.SelectedIndex <= 0)
						{
							short tempRefParam5 = 1040;
							object tempRefParam6 = new object{armensa[1]};
							objClass.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam5, ref cnConex, ref tempRefParam6);
							return false;
						} 
						if (cbbMedicoSol.Enabled && cbbMedicoSol.SelectedIndex <= 0)
						{
							short tempRefParam7 = 1040;
							object tempRefParam8 = new object{armensa[2]};
							objClass.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam7, ref cnConex, ref tempRefParam8);
							return false;
						} 
						if (txMediServi.Enabled && txMediServi.Text.Trim() == "")
						{
							short tempRefParam9 = 1040;
							object tempRefParam10 = new object{"Servicio/Medico"};
							objClass.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam9, ref cnConex, ref tempRefParam10);
							return false;
						} 
						break;
					case "S" : 
						if (cbbServicioSol.Enabled && cbbServicioSol.SelectedIndex <= 0)
						{
							short tempRefParam11 = 1040;
							object tempRefParam12 = new object{armensa[1]};
							objClass.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam11, ref cnConex, ref tempRefParam12);
							return false;
						} 
						if (txMediServi.Enabled && txMediServi.Text.Trim() == "")
						{
							short tempRefParam13 = 1040;
							object tempRefParam14 = new object{"Servicio/Medico"};
							objClass.RespuestaMensaje(ref vNomAplicacion, ref vAyuda, ref tempRefParam13, ref cnConex, ref tempRefParam14);
							return false;
						} 
						break;
				}
			}
			objClass = null;
			return true;
		}

		internal static bool PoderRegistrarConsulta(SqlConnection cnConexion, string stServicio)
		{
			bool result = false;
			string StSql = String.Empty;
			DataSet RrSql = null;
			result = true;

			string StSqlServ = "select iservlab from DSERVICI where gservici= '" + stServicio + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSqlServ, cnConexion);
			DataSet RrSqlServ = new DataSet();
			tempAdapter.Fill(RrSqlServ);
			if (RrSqlServ.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(RrSqlServ.Tables[0].Rows[0]["iservlab"]) == "S")
				{

					StSql = "select valfanu1,valfanu2 from sconsglo where gconsglo='OPENLAB'";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, cnConexion);
					RrSql = new DataSet();
					tempAdapter_2.Fill(RrSql);
					if (RrSql.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToString(RrSql.Tables[0].Rows[0]["VALFANU1"]).Trim() == "S" && Convert.ToString(RrSql.Tables[0].Rows[0]["VALFANU2"]).Trim() == "N")
						{
							result = false;
						}
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrSql.Close();
					RrSql = null;
				}
				//    'Comprobamos que si el servicio de la prueba es de BS y si est� activa la conexi�n con delphyn
				//    stSql = "select isnull(nnumeri1,0) nnumeri1 from sconsglo where gconsglo='ISERVBS'"
				//    Set RrSql = cnConexion.OpenResultset(stSql, 1, 1)
				//    If Not RrSql.EOF Then
				//        If Trim(stServicio) = Trim(RrSql("nnumeri1")) Then
				//            stSql = "select isnull(valfanu1,'N') valfanu1 from sconsglo where gconsglo='IDELFIN'"
				//            Set RrSql = cnConexion.OpenResultset(stSql, 1, 1)
				//            If Not RrSql.EOF Then
				//                If "S" = UCase(Trim(RrSql("valfanu1"))) Then
				//                    PoderRegistrarConsulta = False
				//                End If
				//            End If
				//        End If
				//    End If
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSqlServ.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrSqlServ.Close();

			return result;
		}


		internal static string fUsuarInfor(string stUsuario, SqlConnection cnConexion)
		{
			string result = String.Empty;

			result = "";


			string StSql = "SELECT ISNULL(valfanu1, 'N') as valfanu1 FROM SCONSGLO WHERE gconsglo = 'IUSUDOCU'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, cnConexion);
			DataSet rdTemp = new DataSet();
			tempAdapter.Fill(rdTemp);
			if (rdTemp.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(rdTemp.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
				{
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					rdTemp.Close();
					StSql = "SELECT ISNULL(valfanu1, 'N') as valfanu1 FROM SCONSGLO WHERE gconsglo = 'IUSULARG'";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, cnConexion);
					rdTemp = new DataSet();
					tempAdapter_2.Fill(rdTemp);
					if (rdTemp.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToString(rdTemp.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
						{
							//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							rdTemp.Close();
							StSql = "SELECT gusularg  FROM SUSUARIO WHERE gusuario = '" + stUsuario + "'";
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, cnConexion);
							rdTemp = new DataSet();
							tempAdapter_3.Fill(rdTemp);
							if (rdTemp.Tables[0].Rows.Count != 0)
							{
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								result = Convert.ToString(rdTemp.Tables[0].Rows[0]["gusularg"]).Trim();
							}
						}
						else
						{
							result = stUsuario;
						}
					}
				}
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rdTemp.Close();

			return result;
		}

		internal static byte[] fPasaBinario(string strFichero)
		{
			bool error = false;
			byte[] result = null;
			int intFichero = 0;
			int lngLongitud = 0; //Variable a almacenar en Fichero

			try
			{
				error = true;
				//lblTextos(2).Caption = "Proceso de conversi�n"
				//lblTextos(2).Refresh

				intFichero = FileSystem.FreeFile();

				FileSystem.FileOpen(intFichero, strFichero, OpenMode.Binary, OpenAccess.Default, OpenShare.LockRead, -1);


				lngLongitud = (int) FileSystem.LOF(intFichero);
				byte[] bytFichero = new byte[lngLongitud + 2];
				//UPGRADE_WARNING: (2080) Get was upgraded to FileGet and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				Array TempArray = Array.CreateInstance(bytFichero.GetType().GetElementType(), bytFichero.Length);
				FileSystem.FileGet(intFichero, ref TempArray, -1, false, false);
				Array.Copy(TempArray, bytFichero, TempArray.Length);

				FileSystem.FileClose(intFichero);
				result = bytFichero;


				error = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!error)
				{
					throw excep;
				}

				if (error)
				{

					MessageBox.Show(excep.Message, "Error al descomponer el fichero " + strFichero, MessageBoxButtons.OK, MessageBoxIcon.Information);


					return result;
				}
			}
			return new byte[1];
		}

		//UPGRADE_NOTE: (7001) The following declaration (ComponerComando) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string ComponerComando(string comando, ref string valor)
		//{
				//if (valor == "")
				//{
					//valor = " ";
				//}
				//
				//return comando + "\"" + valor + "\"" + "\n";
				//
		//}
		//End Function


		internal static bool proImprimeBrazaleteZebra(string stImpresoraEtiquetas, string stHospital, ref string stPaciente, ref string stHistoria, string stEdad, string stSexo, SqlConnection ParaConexion)
		{

			PrinterHelper objPrinter = null;

			string PRUEBAIMPR = String.Empty;
			int result = 0;

			//para la impresora predeterminada

			string DeviceNamePre = String.Empty;

			try
			{

				string vstNombrePc = String.Empty;

				int iLongitudNombrePc = 0;
				bool Bexisteimpresora = false;
				string sqlTemp = String.Empty;
				DataSet RrTemp = null;
				int itempo = 0;


				Bexisteimpresora = false;
				vstNombrePc = new string(' ', MAX_COMPUTERNAME_LENGTH + 1);
				iLongitudNombrePc = MAX_COMPUTERNAME_LENGTH;
				//buscar el nombre del PC
				itempo = InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.GetComputerName(ref vstNombrePc, ref iLongitudNombrePc);
				if (itempo != 0)
				{
					//buscar el la tabla la impresora
					sqlTemp = "select * from dimpdocu where gnombrpc='" + vstNombrePc.Trim().Substring(0, Math.Min(vstNombrePc.Trim().Length - 1, vstNombrePc.Trim().Length)).ToUpper() + "' and gdocumen='" + ("ZEBRA2824").ToUpper() + "'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlTemp, ParaConexion);
					RrTemp = new DataSet();
					tempAdapter.Fill(RrTemp);
					if (RrTemp.Tables[0].Rows.Count == 0)
					{
						//Si no existe impresora predeterminada, el objeto PRINTER no et� definido y salta a la rutina de error.
						if (PrinterHelper.Printer.DeviceName != "")
						{
							Bexisteimpresora = true;
						}
					}
					else
					{
						//comprobar que esta dada de alta en el sistema
						foreach (PrinterHelper vimpresoraX in PrinterHelper.Printers)
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							if (PrinterHelper.Printer.DeviceName.ToUpper() == Convert.ToString(RrTemp.Tables[0].Rows[0]["dimpreso"]).ToUpper())
							{
								PrinterHelper.Printer = vimpresoraX;

								Bexisteimpresora = true;
								break;
							}
						}
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrTemp.Close();
				}
				else
				{
					MessageBox.Show("Error al obtener el nombre del PC", Application.ProductName);
				}



				//
				//se obtiene la impresora predeterminada de Windows.

				objPrinter = seGetDefaultPrinter();

				if (objPrinter == null)
				{
					return false;
				}
				//
				//    'se guarda la impresora predeterminada de Windows en una variable
				//
				DeviceNamePre = PrinterHelper.Printer.DeviceName;
				//'PRUEBAIMPR = "SIZE 30 mm, 247.1 mm"
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "GAP 6 mm, 10 mm"
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "^XA"
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "^SZ2^JMA^MFN,N^JUS"
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "^PW195^PR2,2,2"
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "^MCY^PMN^MD05^MTD^MNW~JSO^MMT"
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "^JZY"
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "^LH0,0^LRN"
				//'
				//'
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "^FO,70^FS^FT75,948^A0B,56,55^FH\^FD" & stHistoria & "FS"
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "^FO,48^FS^FT185,946^A0B,39,28^FH\^FD" & stPaciente & "^FS"
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "^PQ1,0,1,Y^XZ"
				//'PRUEBAIMPR = "SIZE 30 mm, 247.1 mm" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "GAP 6 mm, 0 mm" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "SPEED 2" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "DENSITY 15" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "SET RIBBON OFF" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "DIRECTION 0, 0" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "REFERENCE 0, 0" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "OFFSET -10.4 mm" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "VSHIFT 0" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "SET PEEL OFF" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "SET CUTTER OFF" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "SET PARTIAL_CUTTER OFF" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "SET TEAR ON" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "CLS" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "CODEPAGE 850" + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "TEXT 52, 1639, " + Chr(34) + "0" + Chr(34) + ", 270, 10, 11, " + Chr(34) + stHospital + Chr(34) + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "BARCODE 175, 1255, " + Chr(34) + "25" + Chr(34) + ", 45, 0, 180, 1, 3, " + Chr(34) + stHistoria + Chr(34) + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "TEXT 175, 1201, " + Chr(34) + "1" + Chr(34) + ", 180, 1, 1, " + Chr(34) + stEdad + Chr(34) + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "TEXT 100, 1638, " + Chr(34) + "0" + Chr(34) + ", 270, 10, 11, " + Chr(34) + stPaciente + Chr(34) + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "Text 150, 1638," + Chr(34) + "0" + Chr(34) + ", 270, 15, 16, " + Chr(34) + "NHC:" + Chr(34) + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "TEXT 150, 1638, " + Chr(34) + "0" + Chr(34) + ", 270, 15, 16, " + Chr(34) + stHistoria + Chr(34) + Chr(10)
				//'
				//'PRUEBAIMPR = PRUEBAIMPR + "Print 1, 1"

				//'''''PRUEBAIMPR = "SIZE 30 mm, 247.1 mm " + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "GAP 6 mm, 0 mm" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "SPEED 2" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "DENSITY 15" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "SET RIBBON OFF" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "DIRECTION 0, 0" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "REFERENCE 0, 0" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "OFFSET -10.4 mm" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "VSHIFT 0" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "SET PEEL OFF" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "SET CUTTER OFF" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "SET PARTIAL_CUTTER OFF" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "SET TEAR ON" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "CLS" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "CODEPAGE 850" + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "TEXT 52, 1591, " + Chr(34) + "0" + Chr(34) + ", 270, 10, 11, " + Chr(34) + stHospital + Chr(34) + Chr(10)
				//''''''PRUEBAIMPR = PRUEBAIMPR + "TEXT 52, 1639, " + Chr(34) + "0" + Chr(34) + ", 270, 10, 11, " + Chr(34) + stHospital + Chr(34) + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "BARCODE 47, 1000, " + Chr(34) + "25" + Chr(34) + ", 102, 0, 270, 2, 5, " + Chr(34) + stHistoria + Chr(34) + Chr(10)
				//''''''PRUEBAIMPR = PRUEBAIMPR + "BARCODE 175, 1255, " + Chr(34) + "25" + Chr(34) + ", 45, 0, 180, 1, 3, " + Chr(34) + stHistoria + Chr(34) + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "TEXT 151, 1000, " + Chr(34) + "3" + Chr(34) + ", 270, 1, 1, " + Chr(34) + stEdad + Chr(34) + Chr(10)
				//''''''PRUEBAIMPR = PRUEBAIMPR + "TEXT 175, 1201, " + Chr(34) + "1" + Chr(34) + ", 180, 1, 1, " + Chr(34) + stEdad + Chr(34) + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "TEXT 100, 1590, " + Chr(34) + "0" + Chr(34) + ", 270, 10, 11, " + Chr(34) + stPaciente + Chr(34) + Chr(10)
				//''''''PRUEBAIMPR = PRUEBAIMPR + "TEXT 100, 1638, " + Chr(34) + "0" + Chr(34) + ", 270, 10, 11, " + Chr(34) + stPaciente + Chr(34) + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "TEXT 149, 1591, " + Chr(34) + "0" + Chr(34) + ", 270, 15, 16, " + Chr(34) + stHistoria + Chr(34) + Chr(10)
				//''''''PRUEBAIMPR = PRUEBAIMPR + "TEXT 150, 1638, " + Chr(34) + "0" + Chr(34) + ", 270, 15, 16, " + Chr(34) + stHistoria + Chr(34) + Chr(10)
				//'''''PRUEBAIMPR = PRUEBAIMPR + "PRINT 1, 1"
				//Dice LAcollado que quite estoy y ponga lo de abajo, que ya lo ha probado el.
				string apepac = String.Empty;
				string nompac = String.Empty;
				string apepac2 = String.Empty;
				string fechaNac = String.Empty;
				string iniciales = String.Empty;
				int offset = 0;
				string stNombreApellidos = String.Empty;
				int iCodigoBarras = 0;
				string sql = String.Empty;
				DataSet cursor = null;
				string historiasql = String.Empty;
				string sqltemporal = String.Empty;

				if (stHistoria.Length > 0)
				{
					historiasql = stHistoria;
					sql = "select dnombpac,dape1pac,dape2pac,fnacipac from hdossier a, dpacient b where a.gidenpac=b.gidenpac and a.ghistoria=" + historiasql;
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, ParaConexion);
					cursor = new DataSet();
					tempAdapter_2.Fill(cursor);
					if (cursor.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						apepac = Convert.ToString(cursor.Tables[0].Rows[0]["dape1pac"]);
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						apepac2 = Convert.ToString(cursor.Tables[0].Rows[0]["dape2pac"]) + "";
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						nompac = Convert.ToString(cursor.Tables[0].Rows[0]["dnombpac"]);
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						if (!Convert.IsDBNull(cursor.Tables[0].Rows[0]["fnacipac"]))
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							fechaNac = Convert.ToDateTime(cursor.Tables[0].Rows[0]["fnacipac"]).ToString("dd/MM/yyyy");
						}
						else
						{
							fechaNac = "";
						}
					}
					else
					{
						apepac = stPaciente.Substring(0, Math.Min(stPaciente.IndexOf(','), stPaciente.Length)).Trim();
						apepac2 = " ";
						nompac = stPaciente.Substring(stPaciente.IndexOf(',') + 1).Trim();
						fechaNac = "";
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method cursor.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					cursor.Close();
					stHistoria = historiasql;
				}
				else
				{
					apepac = stPaciente.Substring(0, Math.Min(stPaciente.IndexOf(','), stPaciente.Length)).Trim();
					apepac2 = " ";
					nompac = stPaciente.Substring(stPaciente.IndexOf(',') + 1).Trim();
				}

				// Tomamos las iniciales del hospital

				sql = "SELECT valfanu3, isNull(nnumeri1, 0) nnumeri1, isNull(nnumeri2, 25) nnumeri2 FROM SCONSGLO WHERE gconsglo = 'HOSPITAL'";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, ParaConexion);
				cursor = new DataSet();
				tempAdapter_3.Fill(cursor);

				if (cursor.Tables[0].Rows.Count == 0)
				{
					iniciales = " ";
					offset = 0;
					iCodigoBarras = 25;
				}
				else
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(cursor.Tables[0].Rows[0]["valfanu3"]))
					{
						iniciales = " ";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						iniciales = Convert.ToString(cursor.Tables[0].Rows[0]["valfanu3"]).Substring(0, Math.Min(4, Convert.ToString(cursor.Tables[0].Rows[0]["valfanu3"]).Length));
					}

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					offset = Convert.ToInt32(cursor.Tables[0].Rows[0]["nnumeri1"]);
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					iCodigoBarras = Convert.ToInt32(cursor.Tables[0].Rows[0]["nnumeri2"]);
				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method cursor.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				cursor.Close();

				stNombreApellidos = Strings.StrConv(nompac.Trim() + " " + apepac.Trim() + " " + apepac2.Trim(), VbStrConv.ProperCase, 0);

				// Parametrizaci�n de la impresora

				//        PRUEBAIMPR = "SIZE 19 mm, 292 mm " & vbCrLf
				//        'PRUEBAIMPR = PRUEBAIMPR + "GAP 6 mm, 0 mm" + Chr(10)
				//        PRUEBAIMPR = PRUEBAIMPR + "BLINE 3" & vbCrLf
				//        PRUEBAIMPR = PRUEBAIMPR + "SPEED 4" & vbCrLf
				//        PRUEBAIMPR = PRUEBAIMPR + "DENSITY 7" & vbCrLf
				//        PRUEBAIMPR = PRUEBAIMPR + "SET RIBBON OFF" & vbCrLf
				//        'PRUEBAIMPR = PRUEBAIMPR + "DIRECTION 0, 0" + Chr(10)
				//        'PRUEBAIMPR = PRUEBAIMPR + "REFERENCE 0, 0" + Chr(10)
				//        PRUEBAIMPR = PRUEBAIMPR + "OFFSET 0 mm" & vbCrLf
				//        'PRUEBAIMPR = PRUEBAIMPR + "VSHIFT 0" + Chr(10)
				//        PRUEBAIMPR = PRUEBAIMPR + "SET PEEL OFF" & vbCrLf
				//        PRUEBAIMPR = PRUEBAIMPR + "SET CUTTER OFF" & vbCrLf
				//        PRUEBAIMPR = PRUEBAIMPR + "SET PARTIAL_CUTTER OFF" & vbCrLf
				//        PRUEBAIMPR = PRUEBAIMPR + "SET TEAR ON" & vbCrLf
				//        PRUEBAIMPR = PRUEBAIMPR + "CLS" & vbCrLf
				//        PRUEBAIMPR = PRUEBAIMPR + "CODEPAGE 850" & vbCrLf

				//'''PRUEBAIMPR = "GAP 281.25 mm, 0 mm" & vbCrLf & _
				//''''             "SPEED 4" & vbCrLf & _
				//''''             "DENSITY 7" & vbCrLf & _
				//''''             "SIZE 19 mm, 4.75 mm" & vbCrLf & _
				//''''             "CLS" & vbCrLf & _
				//''''             "DIRECTION 0, 0" & vbCrLf & _
				//''''             "REFERENCE 0, 0" & vbCrLf & _
				//''''             "OFFSET 0 mm" & vbCrLf & _
				//''''             "SET CUTTER OFF" & vbCrLf & _
				//''''             "SET TEAR OFF" & vbCrLf & _
				//''''             "SET PEEL OFF" & vbCrLf & _
				//''''             "SET RIBBON OFF" & vbCrLf & _
				//''''             "CODEPAGE 1252" & vbCrLf & _
				//''''             "COUNTRY 003" & vbCrLf & _
				//''''             "SET HEAD ON" & vbCrLf & _
				//''''             "SET REPRINT OFF" & vbCrLf & _
				//''''             "SET GAP 31" & vbCrLf & _
				//''''             "BLINE 3" & vbCrLf
				//'''
				//'''' Contenido
				//'''
				//'''PRUEBAIMPR = PRUEBAIMPR + "TEXT 25, " & (offset + 0) & ", " & Chr(34) & "0" & Chr(34) & ", 0, 17, 18, " & Chr(34) & iniciales & Chr(34) & vbCrLf
				//'''PRUEBAIMPR = PRUEBAIMPR & "BARCODE 140, " & (offset + 70) & ", " & Chr(34) & iCodigoBarras & Chr(34) & ", 102, 0, 90, 2, 5, " & Chr(34) & stHistoria & Chr(34) & vbCrLf
				//'''PRUEBAIMPR = PRUEBAIMPR & "TEXT 30, " & (offset + 70) & ", " & Chr(34) & "0" & Chr(34) & ", 90, 10, 11, " & Chr(34) & stHistoria & Chr(34) & vbCrLf
				//'''PRUEBAIMPR = PRUEBAIMPR & "TEXT 140, " & (offset + 350) & ", " & Chr(34) & "0" & Chr(34) & ", 90, 12, 13, " & Chr(34) & stNombreApellidos & Chr(34) & vbCrLf
				//'''PRUEBAIMPR = PRUEBAIMPR & "TEXT 95, " & (offset + 350) & ", " & Chr(34) & "0" & Chr(34) & ", 90, 12, 13, " & Chr(34) & "NHC: " & stHistoria & Chr(34) & vbCrLf
				//'''PRUEBAIMPR = PRUEBAIMPR & "TEXT 50, " & (offset + 350) & ", " & Chr(34) & "0" & Chr(34) & ", 90, 12, 13, " & Chr(34) & "Fecha Nacimiento: " & fechaNac & Chr(34) & vbCrLf
				//'''PRUEBAIMPR = PRUEBAIMPR + "PRINT 1, 1" & vbCrLf
				//'''PRUEBAIMPR = PRUEBAIMPR + "EOP" & vbCrLf
				//'''
				//'''    PRUEBAIMPR = Chr$(Len(PRUEBAIMPR) Mod 256) & Chr$(Len(PRUEBAIMPR) \ 256) + PRUEBAIMPR
				//'''
				// ********************************************************************************************************
				// Jes�s 21/08/2013
				// Sacar la obtencion de la variable a un procedimiento almacenado para que as� sea m�s f�cil de modificar
				SqlCommand Rq = new SqlCommand();
				sqltemporal = "{Call SP_ParametroImpresoraBrazalete(?,?,?,?)}";
				//UPGRADE_ISSUE: (2064) RDO.rdoConnection method ParaConexion.CreateQuery was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Rq = UpgradeStubs.System_Data_SqlClient_SqlConnection.CreateQuery("COMPRUEBA", sqltemporal);
				Rq.Parameters[0].Direction = ParameterDirection.Input;
				Rq.Parameters[1].Direction = ParameterDirection.Input;
				Rq.Parameters[2].Direction = ParameterDirection.Input;
				Rq.Parameters[3].Direction = ParameterDirection.Output;
				Rq.Parameters[0].Value = stHistoria.Trim();
				Rq.Parameters[1].Value = stNombreApellidos.Trim();
				Rq.Parameters[2].Value = fechaNac.Trim();
				Rq.ExecuteNonQuery(null);

				PRUEBAIMPR = Convert.ToString(Rq.Parameters[3].Value);

				PRUEBAIMPR = Strings.Chr(PRUEBAIMPR.Length % 256).ToString() + Strings.Chr(PRUEBAIMPR.Length / 256).ToString() + PRUEBAIMPR;
				// ********************************************************************************************************


				PrinterHelper.Printer.Copies = 1;
				PrinterHelper.Printer.Print("");
				string tempRefParam = PRUEBAIMPR.Trim();
				result = InterconsultasListadoSupport.PInvoke.SafeNative.gdi32.Escape(PrinterHelper.Printer.Hdc, 19, 0, ref tempRefParam, 0);
				if (result < 0)
				{
					MessageBox.Show("Impresora no soportada", Application.ProductName); //The PASSTHROUGH Escape is not supported by this printer driver.', 48: End
				}
				else if (result == 0)
				{ 
					MessageBox.Show("Error enviando informaci�n a la impresora", Application.ProductName); //An error occurred sending the escape sequence.', 48: End
				}

				PrinterHelper.Printer.EndDoc();



				//se restaura en el objeto Printer la impresora predeterminada
				foreach (PrinterHelper vimpresora in PrinterHelper.Printers)
				{
					if (PrinterHelper.Printer.DeviceName.ToUpper() == DeviceNamePre.ToUpper())
					{
						PrinterHelper.Printer = vimpresora;
						break;
					}
				}


				return true;
			}
			catch
			{
			}

			return false;
		}

		//UPGRADE_NOTE: (7001) The following declaration (ImageToString) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string ImageToString(string ImagePath)
		//{
				//
				//
				//FileSystem.FileOpen(1, ImagePath, OpenMode.Binary, OpenAccess.Default, OpenShare.Default, -1);
				//
				//byte[] bImage = new byte[((int) (new FileInfo(ImagePath)).Length) + 1];
				//
				////UPGRADE_WARNING: (2080) Get was upgraded to FileGet and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
				//Array TempArray = Array.CreateInstance(bImage.GetType().GetElementType(), bImage.Length);
				//FileSystem.FileGet(1, ref TempArray, -1, false, false);
				//Array.Copy(TempArray, bImage, TempArray.Length);
				//
				//FileSystem.FileClose(1);
				//
				////UPGRADE_WARNING: (1059) Code was upgraded to use UpgradeHelpers.Helpers.StringsHelper.ByteArrayToString() which may not have the same behavior. More Information: http://www.vbtonet.com/ewis/ewi1059.aspx
				//return StringsHelper.StrConv(StringsHelper.ByteArrayToString(bImage), StringsHelper.VbStrConvEnum.vbUnicode, 0);
				//
		//}

		internal static PrinterHelper seGetDefaultPrinter()
		{

			FixedLengthString strBuffer = new FixedLengthString(254);
			int iRetValue = 0;
			string strDefaultPrinterInfo = String.Empty;
			PrinterHelper objPrinter = null;

			try
			{

				string tempRefParam = "windows";
				string tempRefParam2 = "device";
				string tempRefParam3 = ",,,";
				string tempRefParam4 = strBuffer.Value;
				iRetValue = InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.GetProfileString(ref tempRefParam, ref tempRefParam2, ref tempRefParam3, ref tempRefParam4, 254);
				strBuffer.Value = tempRefParam4;
				strDefaultPrinterInfo = strBuffer.Value.Substring(0, Math.Min(strBuffer.Value.IndexOf(Strings.Chr(0).ToString()), strBuffer.Value.Length));
				//tblDefaultPrinterInfo = Split(",", strDefaultPrinterInfo)
				strDefaultPrinterInfo = strDefaultPrinterInfo.Substring(0, Math.Min(strDefaultPrinterInfo.IndexOf(','), strDefaultPrinterInfo.Length));


				foreach (PrinterHelper objPrinter2 in PrinterHelper.Printers)
				{
					objPrinter = objPrinter2;
					if (PrinterHelper.Printer.DeviceName == strDefaultPrinterInfo)
					{
						//Impresora detectada
						break;
					}
					objPrinter = null;
				}


				//Si no se encuentra devuelve Nothing
				if (PrinterHelper.Printer.DeviceName != strDefaultPrinterInfo)
				{
					objPrinter = null;
				}


				return objPrinter;
			}
			catch
			{
			}
			return null;
		}

		//(maplaza)(12/03/2008)
		//UPGRADE_NOTE: (7001) The following declaration (seSetDefaultPrinter) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void seSetDefaultPrinter(string PrinterName, string DriverName, string PrinterPort)
		//{
				//
				//string DeviceLine = String.Empty;
				//int r = 0;
				//int l = 0;
				//
				//try
				//{
					//
					//DeviceLine = PrinterName + "," + DriverName + "," + PrinterPort;
					//string tempRefParam = "windows";
					//string tempRefParam2 = "Device";
					//r = InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.WriteProfileString(ref tempRefParam, ref tempRefParam2, ref DeviceLine);
					//string tempRefParam3 = "windows";
					//l = InterconsultasListadoSupport.PInvoke.SafeNative.user32.SendMessage(HWND_BROADCAST, WM_WININICHANGE, 0, ref tempRefParam3);
				//}
				//catch
				//{
				//}
				//
		//}

		//(maplaza)(12/03/2008)
		//UPGRADE_NOTE: (7001) The following declaration (seGetDriverAndPort) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void seGetDriverAndPort(string buffer, ref string DriverName, ref string PrinterPort)
		//{
				//
				//int iDriver = 0;
				//int iPort = 0;
				//
				//try
				//{
					//
					//DriverName = "";
					//PrinterPort = "";
					//iDriver = (buffer.IndexOf(',') + 1);
					//
					//if (iDriver > 0)
					//{
						//DriverName = buffer.Substring(0, Math.Min(iDriver - 1, buffer.Length));
						//iPort = Strings.InStr(iDriver + 1, buffer, ",", CompareMethod.Binary);
						//if (iPort > 0)
						//{
							//PrinterPort = buffer.Substring(iDriver, Math.Min(iPort - iDriver - 1, buffer.Length - iDriver));
						//}
					//}
				//}
				//catch
				//{
				//}
				//
		//}

		internal static void GrabarDCONSUHC(string stGidenpac, string DatosConsultados, string stUsuDCONSUHC, SqlConnection ParaConexion)
		{
			string sql = String.Empty;
			DataSet RrGrabar = null;
			string stFechaConsulta = String.Empty;
			try
			{
				stFechaConsulta = DateTime.Now.ToString("dd/MM/yyyy HH:NN:SS");

				//sql = "select * from DCONSUHC where gidenpac = '" & stGidenpac & "' and gusuario = '" & stUsuDCONSUHC & "' "
				//sql = sql & " and fconsult = " & FormatFechaHMS(stFechaConsulta) & ""

				sql = "select * from DCONSUHC where 2=1 ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
				RrGrabar = new DataSet();
				tempAdapter.Fill(RrGrabar);

				//If Not RrGrabar.EOF Then
				//si ya hay un registro no se hace nada
				//Else
				//se tiene que grabar un registro
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrGrabar.AddNew was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrGrabar.AddNew();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrGrabar.Tables[0].Rows[0]["gidenpac"] = stGidenpac;
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrGrabar.Tables[0].Rows[0]["gusuario"] = stUsuDCONSUHC;
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrGrabar.Tables[0].Rows[0]["fconsult"] = DateTime.Parse(stFechaConsulta);
				if (DatosConsultados.Trim() == "")
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					RrGrabar.Tables[0].Rows[0]["idatosme"] = DBNull.Value;
				}
				else
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					RrGrabar.Tables[0].Rows[0]["idatosme"] = "S";
				}
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				string tempQuery = RrGrabar.Tables(0).TableName;
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
				tempAdapter_2.Update(RrGrabar, RrGrabar.Tables(0).TableName);
				//End If
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrGrabar.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrGrabar.Close();
			}
			catch
			{
			}


		}
		internal static bool fExisteFotoPaciente(string stId, SqlConnection cn)
		{
			bool result = false;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT COUNT(*) FROM DFOTOPAC WHERE gidenpac='" + stId + "'", cn);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			if (Convert.ToDouble(RR.Tables[0].Rows[0][0]) > 0)
			{
				result = true;
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			return result;
		}

		internal static bool fControlPDF(ref PDFCreator.clsPDFCreator miObjPdf, string Fichero = "")
		{
			bool result = false;
			try
			{

				miObjPdf = new PDFCreator.clsPDFCreator();
				result = true;
				miObjPdf.cStart(String.Empty, false);
				miObjPdf.cVisible = false;
				miObjPdf.cClearCache();
				miObjPdf.cDefaultPrinter = "PDFCreator";
				miObjPdf.cPrinterStop = false;
				return result;
			}
			catch (System.Exception excep)
			{

				if ((Fichero + "").Trim() != "")
				{
					GrabaLog("Error en fControlPDF ( " + excep.Message + ")", Fichero, true);
				}
				return false;
			}
		}

		internal static void proMultilenguaje(SqlConnection cnMultilenguaje, string stApp, string stForm, Form fForm, string stUsuario, ref string stIdioma)
		{

			try
			{

				string StSql = String.Empty;
				DataSet RrDatos = null;
				bool esArray = false;
				int RowAnterior = 0;
				int ColAnterior = 0;
				string stIdiomaPorDefecto = String.Empty;
				bool bSobreescribir = false;

				// Comprobamos si est� activado multilenguaje

				StSql = "SELECT ISNULL(VALFANU1, 'N') VALFANU1, ISNULL(VALFANU2, 'IDIOMA0') VALFANU2, " + 
				        "ISNULL(VALFANU3, 'N') VALFANU3 FROM SCONSGLO WHERE GCONSGLO = 'MULTILEN'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, cnMultilenguaje);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrDatos.Close();
					return;
				}
				else
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "N")
					{
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrDatos.Close();
						return;
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stIdiomaPorDefecto = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper();
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						bSobreescribir = (Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3"]).Trim().ToUpper() == "S");
					}
				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				// Si no se env�a idioma, ponemos el que tenga por defecto el usuario

				if (stIdioma == "")
				{

					StSql = "SELECT ISNULL(gidioma, '" + stIdiomaPorDefecto + "') IDIOMA FROM SUSUARIO WHERE gusuario = '" + stUsuario + "'";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, cnMultilenguaje);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);

					if (RrDatos.Tables[0].Rows.Count == 0)
					{
						stIdioma = stIdiomaPorDefecto;
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stIdioma = Convert.ToString(RrDatos.Tables[0].Rows[0]["IDIOMA"]).Trim();
					}

					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrDatos.Close();

				}

				if (stIdioma == stIdiomaPorDefecto && !bSobreescribir)
				{
					return;
				} // Si es el idioma por defecto, no hacemos nada

				// Buscamos los Spreads del Formulario

				//UPGRADE_WARNING: (2065) Form property fForm.Controls has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
				foreach (Control MiControl in ContainerHelper.Controls(fForm))
				{

					if (MiControl is vaSpread)
					{

						// Comprobamos si es un array

						//UPGRADE_TODO: (1067) Member Name is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						//UPGRADE_TODO: (1067) Member Parent is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						//UPGRADE_WARNING: (2081) TypeName has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
						esArray = MiControl.GetType().Name != MiControl.Parent.Controls(MiControl.Name).GetType().Name;

						// Hay uno. Traemos los valores de la tabla de traducci�n

						if (esArray)
						{
							//UPGRADE_TODO: (1067) Member Index is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							//UPGRADE_TODO: (1067) Member Name is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							StSql = "SELECT * FROM SFRMDICC WHERE gejecutable = '" + stApp + "' AND gformulario = '" + stForm + "' AND " + 
							        "gcontrol LIKE '" + Convert.ToString(MiControl.Name) + "." + Convert.ToString(MiControl.Index) + "'";
						}
						else
						{
							//UPGRADE_TODO: (1067) Member Name is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							StSql = "SELECT * FROM SFRMDICC WHERE gejecutable = '" + stApp + "' AND gformulario = '" + stForm + "' AND " + 
							        "gcontrol LIKE '" + Convert.ToString(MiControl.Name) + "%'";
						}

						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, cnMultilenguaje);
						RrDatos = new DataSet();
						tempAdapter_3.Fill(RrDatos);

						//UPGRADE_TODO: (1067) Member Row is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						RowAnterior = Convert.ToInt32(MiControl.Row);
						//UPGRADE_TODO: (1067) Member Col is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						ColAnterior = Convert.ToInt32(MiControl.Col);

						foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
						{

							//UPGRADE_TODO: (1067) Member Row is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							MiControl.Row = 0;
							//UPGRADE_TODO: (1067) Member Col is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							MiControl.Col = Math.Floor(Convert.ToDouble(iteration_row["nindice"]));
							//UPGRADE_TODO: (1067) Member Text is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							MiControl.Text = (Convert.IsDBNull(iteration_row[stIdioma])) ? "" : iteration_row[stIdioma];


						}

						//UPGRADE_TODO: (1067) Member Row is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						MiControl.Row = RowAnterior;
						//UPGRADE_TODO: (1067) Member Col is not defined in type vaSpread. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						MiControl.Col = ColAnterior;

						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrDatos.Close();

					}

				}

				StSql = "SELECT * FROM SFRMDICC WHERE gejecutable = '" + stApp + "' AND gformulario = '" + stForm + "'";

				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSql, cnMultilenguaje);
				RrDatos = new DataSet();
				tempAdapter_4.Fill(RrDatos);

				foreach (DataRow iteration_row_2 in RrDatos.Tables[0].Rows)
				{

					if (Convert.ToString(iteration_row_2["gcontrol"]) == ".FORMULARIO.")
					{

						// Establecemos el t�tulo de la pantalla

						//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
						fForm.Text = (Convert.IsDBNull(iteration_row_2[stIdioma])) ? "" : Convert.ToString(iteration_row_2[stIdioma]);

					}
					else
					{

						//UPGRADE_WARNING: (2065) Form property fForm.Controls has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
						foreach (Control MiControl in ContainerHelper.Controls(fForm))
						{

							if (MiControl.Name == Convert.ToString(iteration_row_2["gcontrol"]))
							{
								if (Convert.ToString(iteration_row_2["nindice"]) == "#")
								{

									// No es un array. Cambiamos el nombre al conjunto

									//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
									MiControl.Text = (Convert.IsDBNull(iteration_row_2[stIdioma])) ? "" : Convert.ToString(iteration_row_2[stIdioma]);

								}
								else
								{

									// Cambiamos el nombre al elemento correspondiente del array

									if (MiControl is ToolStrip)
									{
										int tempForVar = ((ToolStrip) MiControl).Items.Count;
										for (int X = 1; X <= tempForVar; X++)
										{
											if (X == Math.Floor(Convert.ToDouble(iteration_row_2["nindice"])))
											{
												//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
												((ToolStrip) MiControl).Items[X - 1].ToolTipText = (Convert.IsDBNull(iteration_row_2[stIdioma])) ? "" : Convert.ToString(iteration_row_2[stIdioma]);
											}
										}
									}
									else if (MiControl is StatusStrip)
									{ 
										int tempForVar2 = ((StatusStrip) MiControl).Items.Count;
										for (int X = 1; X <= tempForVar2; X++)
										{
											if (X == Math.Floor(Convert.ToDouble(iteration_row_2["nindice"])))
											{
												//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
												((StatusStrip) MiControl).Items[X - 1].Text = (Convert.IsDBNull(iteration_row_2[stIdioma])) ? "" : Convert.ToString(iteration_row_2[stIdioma]);
											}
										}
									}
									else if (!(MiControl is vaSpread))
									{ 
										if (ControlArrayHelper.GetControlIndex(MiControl) == Math.Floor(Convert.ToDouble(iteration_row_2["nindice"])))
										{
											//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
											MiControl.Text = (Convert.IsDBNull(iteration_row_2[stIdioma])) ? "" : Convert.ToString(iteration_row_2[stIdioma]);
										}
									}

								}
							}

						}

					}


				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("Error recuperando multilenguaje: " + excep.Message, Application.ProductName);
				return;
			}

		}

		internal static void proMultilenguaje(SqlConnection cnMultilenguaje, string stApp, string stForm, Form fForm, string stUsuario)
		{
			string tempRefParam = "";
			proMultilenguaje(cnMultilenguaje, stApp, stForm, fForm, stUsuario, ref tempRefParam);
		}

		internal static string Replace(string Expresion, string Encontrar, string ReemplazarCon)
		{
			//---------------------------------------------------------------
			// Funci�n Replace para usar con VB5 o anteriores
			//---------------------------------------------------------------
			int I = 0;
			//
			int j = 1;
			do 
			{
				// Buscar la cadena indicada en la expresi�n
				I = Strings.InStr(j, Expresion, Encontrar, CompareMethod.Binary);
				// Si la hemos hallado, quitamos dicha cadena y ponemos la nueva
				if (I != 0)
				{
					Expresion = Expresion.Substring(0, Math.Min(I - 1, Expresion.Length)) + ReemplazarCon + Expresion.Substring(I + Encontrar.Length - 1);
					j = I + 1;
				}
			}
			while(I != 0);
			// Devolver la cadena
			return Expresion;
		}

		//Oscar C Agosto 2011. Copago
		//Para la b�squeda del COPAGO que tiene que pagar el paciente que viene por la Sociedad X (Cliente Asistencial o Plan X), creo que se realizar�a de la siguiente forma, pero me gustar�a que Ana, Mar�a o Celia nos lo confirmen.
		//Desde la Consulta de I-MDH con la Sociedad (gsocieda) = X y la Prestaci�n (gprestac)= �P�,
		//Lo normal es que el contrato se asocie a un tercero, y se guarda en la tabla IFMSFA_CLICONTR, pero a veces, los clientes asistenciales de un mismo tercero, pueden tener diferentes tarifas, en cuyo caso, se asociar�a el cliente asistencial a un contrato, y se guarda en la tabla IFMSFA_SOCICONTR, pero si no fuera as�, esta �ltima tabla estar� vac�a y el contrato estar� por tercero, en IFMSFA_CLICONTR.
		//Por tanto, al igual que se hace al prefacturar, el orden de b�squeda de un contrato es:
		//primero mirar si el cliente asistencial tiene un contrato en IFMSFA_SOCICONTR, utilizando la sentencia que os ha mandado Celia, que es la que usa facturaci�n:
		//  select  @ccontr = cl.ccontr
		//     from  IFMSFA_SOCICONTR cl,
		//           IFMSFA_CONTRATO co
		//    where  cl.ccontr = co.ccontr and
		//           IsNull(co.iborrado,'N') = 'N' and
		//           cl.gsocieda = @gsocieda  and
		//    cl.ginspecc = IsNull(@ginspecc,'0')  and
		//           @fechap between cl.fini and IsNull(dateadd(day,1,cl.ffin),@fechap)
		//     order by fini desc
		//Si la sentencia anterior NO devuelve nada, es porque todos los clientes asistenciales del tercero, utilizan el mismo contrato, en cuyo caso est� en IFMSFA_CLICONTR y se utiliza un procedimiento m�s complejo porque la vigencia del contrato, depende tambi�n de unos indicadores, el procedimiento se llama ifmsfa_buscacontrato, no s� si deber�ais llamarlo directamente o qu酅
		//2.Buscar el importe que debe pagar el paciente para realizarse la prestaci�n gprestac = �P� en la sociedad X, con CONTRATO.

		internal static string fObtenerImporteCopagoPrestacion(SqlConnection ParaConexion, string stCodSociedad, string stCodPrestacion, System.DateTime ffeccita)
		{
			string result = String.Empty;
			string stContrato = String.Empty;
			double volacu = 0;
			SqlCommand ConsultaAlmacenada = new SqlCommand();
			result = "";

			//UPGRADE_WARNING: (1068) tempRefParam2 of type Variant is being forced to DateTime. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
			//UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to DateTime. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
			object tempRefParam = ffeccita;
			object tempRefParam2 = ffeccita;
			string sql = "select cl.ccontr contrato " + 
			             "  from  IFMSFA_SOCICONTR cl, " + 
			             "  IFMSFA_CONTRATO co " + 
			             "  where  cl.ccontr = co.ccontr and " + 
			             "         IsNull(co.iborrado,'N') = 'N' and " + 
			             "         cl.gsocieda = " + stCodSociedad + " and " + 
			             "         cl.ginspecc = '0' and " + 
			             FormatFechaHMS(ref tempRefParam) + " between cl.fini and IsNull(dateadd(day,1,cl.ffin), " + FormatFechaHMS(ref tempRefParam2) + ")" + 
			             " order by fini desc ";
			ffeccita = Convert.ToDateTime(tempRefParam2);
			ffeccita = Convert.ToDateTime(tempRefParam);

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["contrato"]))
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					stContrato = Convert.ToString(RR.Tables[0].Rows[0]["contrato"]);
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();

			if (stContrato.Trim() == "")
			{
				sql = "SELECT RTERC, ETERC FROM IFMSFA_DSOCIEDA WHERE gsocieda=" + stCodSociedad;
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, ParaConexion);
				RR = new DataSet();
				tempAdapter_2.Fill(RR);
				if (RR.Tables[0].Rows.Count != 0)
				{
					ConsultaAlmacenada.Connection = ParaConexion;
					ConsultaAlmacenada.CommandText = "{ call IFMSFA_BUSCACONTRATO (?, ?, ?, ?, ?) }";
					ConsultaAlmacenada.Parameters[0].Direction = ParameterDirection.Input;
					ConsultaAlmacenada.Parameters[1].Direction = ParameterDirection.Input;
					ConsultaAlmacenada.Parameters[2].Direction = ParameterDirection.Input;
					ConsultaAlmacenada.Parameters[3].Direction = ParameterDirection.Output;
					ConsultaAlmacenada.Parameters[4].Direction = ParameterDirection.Output;
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					ConsultaAlmacenada.Parameters[0].Value = RR.Tables[0].Rows[0]["RTERC"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					ConsultaAlmacenada.Parameters[1].Value = RR.Tables[0].Rows[0]["ETERC"];
					ConsultaAlmacenada.Parameters[2].Value = ffeccita;
					ConsultaAlmacenada.ExecuteNonQuery(null);
					volacu = Conversion.Val(Convert.ToString(ConsultaAlmacenada.Parameters[4].Value) + "");
					stContrato = Convert.ToString(ConsultaAlmacenada.Parameters[3].Value);
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RR.Close();
			}

			if (stContrato.Trim() != "")
			{
				sql = " SELECT isnull(cpagpaci,0) cpagpaci from IFMSFA_VALORCONC where ccontr ='" + stContrato + "' and rconc = 'PR' and Econc = '" + stCodPrestacion + "'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, ParaConexion);
				RR = new DataSet();
				tempAdapter_3.Fill(RR);
				if (RR.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToDouble(RR.Tables[0].Rows[0]["cpagpaci"]) != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						string tempRefParam3 = Convert.ToString(RR.Tables[0].Rows[0]["cpagpaci"]);
						string tempRefParam4 = ObtenerSeparadorDecimal(ParaConexion);
						result = ConvertirDecimales(ref tempRefParam3, ref tempRefParam4, 2);
					}
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RR.Close();
			}


			return result;
		}
		//---------------

		internal static string ObtenerValor_CTEGLOBAL(SqlConnection ParaConexion, string ParaCTE, string ParaValor)
		{
			string result = String.Empty;
			result = "";
			string sql = "SELECT " + ParaValor + " FROM SCONSGLO WHERE GCONSGLO='" + ParaCTE + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = (Convert.ToString(RR.Tables[0].Rows[0][0]) + "").Trim();
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			return result;
		}

		//Oscar C Enero 2012
		//Al pulsar el bot�n imprimir para el caso de las consultas (primera consulta o sucesiva)
		//hay que comprobar si se han solicitado prestaciones desde la consulta, si se ha dado
		//alguna cita para el mismo servicio que la consulta o tiene sesiones programadas para el mismo Servicio de la consulta.
		//(Tiene que ser parametrizable por sociedad e inspecci�n la realizaci�n o no de estas comprobaciones).

		//fImprimirCierreProcesoConsulta
		//TRUE--> SE CUMPLEN LAS VALIDACIONES PARA PODER EMITIR EL INFORME
		//FALSE--> NO SE CUMPLEN LAS VALIDACIONES PARA PODER EMITIR EL INFORME

		//Si la sociedad/inspeccion indica que se deben realizar las comprobaciones
		//y es una primera/sucesiva
		//       -Si es un proceso cerrado (ifinproc="S")
		//           - Si tiene prestaciones asociadas a la consulta (<>A y <>R) o
		//                tiene citas dadas para el mismo servicio que la consulta o
		//                tiene sesiones programadas para el mismo servicio que la consulta
		//                   - Mensaje de aviso parametrizado en smensaje
		//                   - exit sub
		//       - Si es un proceso abierto (ifinproc="N")
		//          - Si no tiene prestaciones asociadas a la consulta(<>A y <>R) con indicador iprimsuc='S' y
		//               no tiene citas dadas para el mismo servicio que la consulta y
		//               no tiene sesiones programadas para el mismo servicio que la consulta
		//                   - Mensaje de aviso parametrizado
		//                   - exit sub

		internal static bool fImprimirCierreProcesoConsulta(SqlConnection RcValoracion, string GstTipoServicioEpisodio, int GiA�oRegistro, int GiNumeroRegistro, bool bCierreProcesoConsulta, bool bMostrarMensaje, ref string DescError)
		{
			bool result = false;
			string sqltemporal = String.Empty;
			//Dim RrDatos As rdoResultset
			//Dim loclase_mensaje As Object
			//Set loclase_mensaje = CreateObject("Mensajes.ClassMensajes") 'dll de mensajes

			SqlCommand Rq = null;
			if (GstTipoServicioEpisodio == "C" && bCierreProcesoConsulta)
			{

				vNomAplicacion = ObtenerNombreAplicacion(RcValoracion);


				//        sqlTemporal = " SELECT CCONSULT.ffeccita, CCONSULT.ifinproc, CCONSULT.gservici, CCONSULT.gidenpac " & _
				//'             " FROM CCONSULT " & _
				//'             " INNER JOIN DCODPRES ON DCODPRES.gprestac = CCONSULT.gprestac AND " & _
				//'             "                        DCODPRES.iprimsuc  IN ('P','S') " & _
				//'             " INNER JOIN DVALCCON ON DVALCCON.gsocieda = CCONSULT.gsocieda AND " & _
				//'             "                        DVALCCON.ginspecc = ISNULL(CCONSULT.ginspecc,'0') AND " & _
				//'             "                        DVALCCON.ivalidar='S' " & _
				//'             " WHERE itiposer='C' and " & _
				//'             "       ganoregi=" & GiA�oRegistro & " AND " & _
				//'             "       gnumregi=" & GiNumeroRegistro
				//        Set RrDatos = RcValoracion.OpenResultset(sqlTemporal, 1, 1)
				//
				//        If Not RrDatos.EOF Then
				//
				//            If UCase(RrDatos!ifinproc) & "" = "S" Then
				//
				//                sqlTemporal = _
				//'                    "SELECT SUM (T1.Cuenta) FROM (" & _
				//'                    "   SELECT count(*) Cuenta FROM EPRESTAC WHERE " & _
				//'                            "     itiposer = 'C'" & _
				//'                            " AND ganoregi = " & GiA�oRegistro & _
				//'                            " AND gnumregi = " & GiNumeroRegistro & _
				//'                            " AND (iestsoli <> 'A' AND iestsoli<>'R') " & _
				//'                    "   UNION ALL " & _
				//'                    "   SELECT count(*) Cuenta FROM CCITPROG WHERE " & _
				//'                            "    gidenpac = '" & RrDatos!GIDENPAC & "'" & _
				//'                            " AND ffeccita >= " & FormatFechaHMS(RrDatos!ffeccita) & _
				//'                            " AND gservici = " & RrDatos!gservici & _
				//'                    "   UNION ALL " & _
				//'                    "   SELECT count(*) Cuenta FROM CSESIONE " & _
				//'                            " INNER JOIN CSOLCITA ON CSOLCITA.ganoregi = CSESIONE.ganoregi " & _
				//'                            "                    AND CSOLCITA.gnumregi = CSESIONE.gnumregi " & _
				//'                            "                    AND CSOLCITA.gidenpac = '" & RrDatos!GIDENPAC & "'" & _
				//'                            "                    AND CSOLCITA.gservici = " & RrDatos!gservici & _
				//'                            "                    AND CSESIONE.ffeccita >= " & FormatFechaHMS(RrDatos!ffeccita) & _
				//'                            "                    AND (CSESIONE.iestsesi <> 'A'  and CSESIONE.iestsesi <> 'R') " & _
				//'                    " ) T1"
				//                Set RrDatos = RcValoracion.OpenResultset(sqlTemporal, 1, 1)
				//                If RrDatos(0) > 0 Then
				//                    If bMostrarMensaje = True Then loclase_mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 5060, RcValoracion
				//                    Exit Function
				//                End If
				//
				//            ElseIf UCase(RrDatos!ifinproc) & "" = "N" Then
				//
				//                sqlTemporal = _
				//'                    "SELECT SUM (T1.Cuenta) FROM (" & _
				//'                    "   SELECT count(*) Cuenta FROM EPRESTAC " & _
				//'                        "   INNER JOIN DCODPRES on DCODPRES.gprestac = EPRESTAC.gprestac AND iprimsuc = 'S' " & _
				//'                    "   WHERE " & _
				//'                            "     itiposer = 'C'" & _
				//'                            " AND ganoregi = " & GiA�oRegistro & _
				//'                            " AND gnumregi = " & GiNumeroRegistro & _
				//'                            " AND (iestsoli <> 'A' AND iestsoli<>'R') " & _
				//'                    "   UNION ALL " & _
				//'                    "   SELECT count(*) Cuenta FROM CCITPROG WHERE " & _
				//'                            "    gidenpac = '" & RrDatos!GIDENPAC & "'" & _
				//'                            " AND ffeccita >= " & FormatFechaHMS(RrDatos!ffeccita) & _
				//'                            " AND gservici = " & RrDatos!gservici & _
				//'                    "   UNION ALL " & _
				//'                    "   SELECT count(*) Cuenta FROM CSESIONE " & _
				//'                            " INNER JOIN CSOLCITA ON CSOLCITA.ganoregi = CSESIONE.ganoregi " & _
				//'                            "                    AND CSOLCITA.gnumregi = CSESIONE.gnumregi " & _
				//'                            "                    AND CSOLCITA.gidenpac = '" & RrDatos!GIDENPAC & "'" & _
				//'                            "                    AND CSOLCITA.gservici = " & RrDatos!gservici & _
				//'                            "                    AND CSESIONE.ffeccita >= " & FormatFechaHMS(RrDatos!ffeccita) & _
				//'                            "                    AND (CSESIONE.iestsesi <> 'A' AND CSESIONE.iestsesi <>'R') " & _
				//'                    " ) T1"
				//                Set RrDatos = RcValoracion.OpenResultset(sqlTemporal, 1, 1)
				//                If RrDatos(0) = 0 Then
				//                    If bMostrarMensaje = True Then loclase_mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 5061, RcValoracion
				//                    Exit Function
				//                End If
				//
				//            End If
				//
				//        End If
				//        RrDatos.Close

				//        Oscar C Marzo 2012
				//        Las validaciones se sacan al SP SP_CompruebaImpresionValoracion para que CAPIO tenga mas libertad
				//        a la hora de especificar los casos de uso en los que no se puede imprimir la valoracion
				Rq = new SqlCommand();
				sqltemporal = "{Call SP_CompruebaImpresionValoracion(?,?,?,?)}";
				//UPGRADE_ISSUE: (2064) RDO.rdoConnection method RcValoracion.CreateQuery was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				Rq = UpgradeStubs.System_Data_SqlClient_SqlConnection.CreateQuery("COMPRUEBA", sqltemporal);
				Rq.Parameters[0].Direction = ParameterDirection.Input;
				Rq.Parameters[1].Direction = ParameterDirection.Input;
				Rq.Parameters[2].Direction = ParameterDirection.Output;
				Rq.Parameters[3].Direction = ParameterDirection.Output;
				Rq.Parameters[0].Value = GiA�oRegistro;
				Rq.Parameters[1].Value = GiNumeroRegistro;
				Rq.ExecuteNonQuery(null);

				if (Convert.ToString(Rq.Parameters[2].Value) == "N")
				{
					if (bMostrarMensaje)
					{
						MessageBox.Show(Convert.ToString(Rq.Parameters[3].Value), vNomAplicacion, MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					DescError = Convert.ToString(Rq.Parameters[3].Value);
					return result;
				}
			}

			//Set loclase_mensaje = Nothing
			return true;
		}

		internal static bool fImprimirCierreProcesoConsulta(SqlConnection RcValoracion, string GstTipoServicioEpisodio, int GiA�oRegistro, int GiNumeroRegistro, bool bCierreProcesoConsulta, bool bMostrarMensaje)
		{
			string tempRefParam2 = "";
			return fImprimirCierreProcesoConsulta(RcValoracion, GstTipoServicioEpisodio, GiA�oRegistro, GiNumeroRegistro, bCierreProcesoConsulta, bMostrarMensaje, ref tempRefParam2);
		}
		//------------------

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr, string stIdPac, string stControlDuplicados, string stProcedenciaInical, bool fIngresado, int iA�oProg, int iNumProg, System.DateTime dfprevista, string stGpresant, string stMensError)
		{

			return false;


		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr, string stIdPac, string stControlDuplicados, string stProcedenciaInical, bool fIngresado, int iA�oProg, int iNumProg, System.DateTime dfprevista, string stGpresant)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, stFPrevIngr, stIdPac, stControlDuplicados, stProcedenciaInical, fIngresado, iA�oProg, iNumProg, dfprevista, stGpresant, "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr, string stIdPac, string stControlDuplicados, string stProcedenciaInical, bool fIngresado, int iA�oProg, int iNumProg, System.DateTime dfprevista)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, stFPrevIngr, stIdPac, stControlDuplicados, stProcedenciaInical, fIngresado, iA�oProg, iNumProg, dfprevista, "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr, string stIdPac, string stControlDuplicados, string stProcedenciaInical, bool fIngresado, int iA�oProg, int iNumProg)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, stFPrevIngr, stIdPac, stControlDuplicados, stProcedenciaInical, fIngresado, iA�oProg, iNumProg, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr, string stIdPac, string stControlDuplicados, string stProcedenciaInical, bool fIngresado, int iA�oProg)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, stFPrevIngr, stIdPac, stControlDuplicados, stProcedenciaInical, fIngresado, iA�oProg, 0, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr, string stIdPac, string stControlDuplicados, string stProcedenciaInical, bool fIngresado)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, stFPrevIngr, stIdPac, stControlDuplicados, stProcedenciaInical, fIngresado, 0, 0, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr, string stIdPac, string stControlDuplicados, string stProcedenciaInical)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, stFPrevIngr, stIdPac, stControlDuplicados, stProcedenciaInical, false, 0, 0, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr, string stIdPac, string stControlDuplicados)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, stFPrevIngr, stIdPac, stControlDuplicados, "", false, 0, 0, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr, string stIdPac)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, stFPrevIngr, stIdPac, "", "", false, 0, 0, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum, string stFPrevIngr)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, stFPrevIngr, "", "", "", false, 0, 0, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o, int iNum)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, iNum, "", "", "", "", false, 0, 0, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode, int iA�o)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, iA�o, 0, "", "", "", "", false, 0, 0, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo, string stVengode)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, stVengode, 0, 0, "", "", "", "", false, 0, 0, DateTime.FromOADate(0), "", "");
		}

		internal static bool fLlamadaWebServiceLES(Serrores.stEpisodios[] Episodio, string stProceso, bool blnReserva, SqlConnection rcCursor, string stMetodo)
		{
			return fLlamadaWebServiceLES(Episodio, stProceso, blnReserva, rcCursor, stMetodo, "0", 0, 0, "", "", "", "", false, 0, 0, DateTime.FromOADate(0), "", "");
		}

		internal static string NoContinuarSiExistenDuplicadosListaEspera(SqlConnection rcCursor, string stGidenpac, string stPrestac, string stLateral, int vModo, string stTipoSer, string stCodSoc = "", string stCodIns = "")
		{
			return "-1";
		}
		internal static bool ComprobarSiYaEnviado(SqlConnection rcCursor, int A�oRegistro, int NumeroRegistro)
		{
			return false;
		}

		internal static string ProcedenciaLes(SqlConnection rcCursor, int A�oRegistro, int NumeroRegistro)
		{
			return "NO";
		}

		internal static string DimeElEpisodioLESChaval(SqlConnection rcCursor, string stItipRela, string stA�oRela, string stNumRela, string stPreRela, string stPetRela, string stTipo)
		{
			return "0";
		}


		internal static string PrellamadaAfLlamadaWebServiceLES(int ivModoEd, string stControlDuplicados, SqlConnection rcCursor, bool fIngresado, string stDonde, int iA�oPrin, int lNumPrin, string stGprestac, int ivAnoInt, int lvNumInt, int ivAnoProg, int lvNumProg, System.DateTime dFecha_Intervencion, string sUsuario, string tbDiagnosSec, string txtDesDiagnosSec, string tbCodDiag, string tbNomDiag, string IndIngAmb, string IndCMcm, string stPriorid, string stLater, string txtAATRM, string IndTurno, System.DateTime FMenor, bool bCambioASS = false, string stGpresant = "", string tbProcSec = "", string tbDescProcSec = "", string stCreadaYaSolicitud = "")
		{

			return "N";
		}


		internal static bool CambioAgrupador(string stPresAnt, string stPresNue, SqlConnection rcCursor)
		{
			DataSet RrPro = null;
			string StSql = String.Empty;
			bool btest = false;
			if (stPresAnt != stPresNue)
			{
				StSql = "select COUNT(*) from DCODPRES inner join dproccie on dcodpres.gcodicie = dproccie.gcodicie where gprestac='" + stPresAnt.Trim() + "' and gcodagru = ( " + 
				        "select gcodagru from DCODPRES inner join dproccie on dcodpres.gcodicie = dproccie.gcodicie where gprestac='" + stPresNue.Trim() + "') ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcCursor);
				RrPro = new DataSet();
				tempAdapter.Fill(RrPro);
				//Si no hay registro, es que los agrupadores son distintos.
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				btest = (Convert.ToDouble(RrPro.Tables[0].Rows[0][0]) == 0);


			}
			return btest;

		}

		internal static bool CambioSociedadLE(string stGano, string stGnum, SqlConnection rc, string vstCodSoc, string vstCodInspec, string vstNafiliac)
		{

			string StSql = String.Empty;
			string VstGidenpac = String.Empty;
			string vstProgramaMarco = String.Empty;
			string stGanoProg = String.Empty;
			string stGnumProg = String.Empty;
			string stGanoInte = String.Empty;
			string stGnumInte = String.Empty;
			string vstCodSocAnt = String.Empty;
			string vstInspecAnt = String.Empty;
			int icodSEgurSoc = 0;
			bool bEnvicat = false;
			bool bTransaccion = false;
			DataSet RrSql = null;

			try
			{

				// Inicializamos los valores

				bTransaccion = false;
				bEnvicat = false;

				// Vemos si ENVICAT est� activo

				StSql = "SELECT 'S' FROM SCONSGLO WHERE gconsglo = 'IENVICAT' AND valfanu1 = 'S'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rc);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);

				bEnvicat = RrSql.Tables[0].Rows.Count != 0;

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrSql.Close();

				// Tomamos el c�digo de Seguridad Social

				StSql = "SELECT nnumeri1 FROM SCONSGLO WHERE gconsglo = 'SEGURSOC' AND nnumeri1 IS NOT NULL";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, rc);
				RrSql = new DataSet();
				tempAdapter_2.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count == 0)
				{
					icodSEgurSoc = -1;
				}
				else
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					icodSEgurSoc = Convert.ToInt32(RrSql.Tables[0].Rows[0]["nnumeri1"]);
				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrSql.Close();

				// Obtenemos los datos actuales

				StSql = "SELECT " + 
				        "gidenpac, gsocieda, gpromarq, ganoprog, gnumprog, ganointe, gnuminte, gsocieda, ginspecc " + 
				        "FROM AEPISLEQ WHERE " + 
				        "ganoregi = " + stGano + " AND " + 
				        "gnumregi = " + stGnum;

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, rc);
				RrSql = new DataSet();
				tempAdapter_3.Fill(RrSql);

				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				VstGidenpac = Convert.ToString(RrSql.Tables[0].Rows[0]["gidenpac"]);
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vstProgramaMarco = (Convert.ToString(RrSql.Tables[0].Rows[0]["gpromarq"]) + "").Trim();

				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				stGanoProg = (Convert.ToString(RrSql.Tables[0].Rows[0]["ganoprog"]) + "").Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				stGnumProg = (Convert.ToString(RrSql.Tables[0].Rows[0]["gnumprog"]) + "").Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				stGanoInte = (Convert.ToString(RrSql.Tables[0].Rows[0]["ganointe"]) + "").Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				stGnumInte = (Convert.ToString(RrSql.Tables[0].Rows[0]["gnuminte"]) + "").Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vstCodSocAnt = (Convert.ToString(RrSql.Tables[0].Rows[0]["gsocieda"]) + "").Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vstInspecAnt = (Convert.ToString(RrSql.Tables[0].Rows[0]["ginspecc"]) + "").Trim();

				// Si est� activo el env�o, es de Seguridad Social y antes no era.....

				if (Conversion.Val(vstCodSoc) != Conversion.Val(vstCodSocAnt) || vstCodInspec.Trim() != vstInspecAnt.Trim())
				{

					// Actualizamos los datos si hemos cambiado sociedad y/o inspecci�n

					//UPGRADE_ISSUE: (2064) RDO.rdoConnection method rc.BeginTrans was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();

					bTransaccion = true;

					StSql = "UPDATE AEPISLEQ SET " + 
					        "gsocieda = " + vstCodSoc + ", " + 
					        "ginspecc = " + ((vstCodInspec.Trim() == "") ? "Null" : "'" + vstCodInspec + "'") + ", " + 
					        "nafiliac = '" + vstNafiliac + "'";

					if (bEnvicat)
					{

						if (Conversion.Val(vstCodSoc) != icodSEgurSoc)
						{
							StSql = StSql + ", gpromarq = (NULL)";
						}
						if (Conversion.Val(vstCodSoc) == icodSEgurSoc && vstProgramaMarco == "")
						{
							StSql = StSql + ", gpromarq ='" + ObternerValor_CTEGLOBAL(rc, "RULEQDIA", "VALFANU2") + "'";
						}

					}

					StSql = StSql + " WHERE ganoregi = " + stGano + " AND gnumregi = " + stGnum;

					SqlCommand tempCommand = new SqlCommand(StSql, rc);
					tempCommand.ExecuteNonQuery();


					//UPGRADE_ISSUE: (2064) RDO.rdoConnection method rc.CommitTrans was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					UpgradeStubs.System_Data_SqlClient_SqlConnection.CommitTrans();

				}


				return true;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("Error CambioSociedadLE. " + excep.Message, "I-MDH", MessageBoxButtons.OK, MessageBoxIcon.Error);

				if (bTransaccion)
				{
					//UPGRADE_ISSUE: (2064) RDO.rdoConnection method rc.RollbackTrans was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
				}

				return false;
			}
		}



		internal static bool hayConceptosPDF(SqlConnection rcConexion, string stTipoServicio, int iGanoregi, int lGnumregi)
		{

			bool result = false;
			try
			{

				string StSql = String.Empty;
				DataSet RrDatos = null;

				StSql = "SELECT COUNT(*) " + 
				        "FROM " + 
				        "DVALORAM A " + 
				        "INNER JOIN DCONCLIN B ON B.gcodconc = A.gcodconc " + 
				        "INNER JOIN DDOCUPDF C ON C.gnombdoc = A.ovallcon AND C.inumdocu = 1 AND C.dnombpdf LIKE '%.pdf' " + 
				        "WHERE " + 
				        "A.itiposer = '" + stTipoServicio + "' AND " + 
				        "A.ganoregi = " + iGanoregi.ToString() + " AND " + 
				        "A.gnumregi = " + lGnumregi.ToString() + " AND " + 
				        "A.fapuntes = (SELECT MAX(fapuntes) FROM DVALORAM WHERE " + 
				        "itiposer = '" + stTipoServicio + "' AND " + 
				        "ganoregi = " + iGanoregi.ToString() + " AND " + 
				        "gnumregi = " + lGnumregi.ToString() + ") AND " + 
				        "B.iticocli = 'A' AND " + 
				        "isNull(A.iprivado, 'N') = 'N'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = (Convert.ToDouble(RrDatos.Tables[0].Rows[0][0]) > 0);

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				// Comprobamos que haya alg�n concepto de tipo URL con un PDF

				StSql = "SELECT COUNT(*) FROM " + 
				        "DVALORAM A " + 
				        "INNER JOIN DCONCLIN B ON " + 
				        "B.gcodconc = A.gcodconc AND " + 
				        "B.gcodconc = (SELECT nnumeri3 FROM SCONSGLO WHERE gconsglo = 'IVALOECG') " + 
				        "INNER JOIN DOCUMENT C ON " + 
				        "C.gnomdocu = A.ovallcon AND C.contdocu LIKE '%.pdf'" + 
				        " WHERE " + 
				        "A.itiposer = '" + stTipoServicio + "' AND " + 
				        "A.ganoregi = " + iGanoregi.ToString() + " AND " + 
				        "A.gnumregi = " + lGnumregi.ToString() + " AND " + 
				        "A.fapuntes = (SELECT MAX(fapuntes) FROM DVALORAM WHERE " + 
				        "itiposer = '" + stTipoServicio + "' AND " + 
				        "ganoregi = " + iGanoregi.ToString() + " AND " + 
				        "gnumregi = " + lGnumregi.ToString() + ") AND " + 
				        "B.iticocli = 'V' AND " + 
				        "isNull(A.iprivado, 'N') = 'N'";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter_2.Fill(RrDatos);

				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = result || (Convert.ToDouble(RrDatos.Tables[0].Rows[0][0]) > 0);

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				return result;
			}
			catch
			{

				//MsgBox "ERROR hayConceptosPDF: " & Err.Description, vbCritical + vbOKOnly, "hayConceptosPDF"
				return false;
			}
		}

		internal static void proBorraPDFs(string stPath)
		{



			string Directorios = String.Empty;
			try
			{
				Directorios = FileSystem.Dir(stPath + "\\*.pdf", FileAttribute.Normal);
			}
			catch
			{
			}


			while(Directorios != "")
			{
				ErrorHandlingHelper.ResumeNext(
					() => {Application.DoEvents();}, 
					() => {File.Delete(stPath + "\\" + Directorios);}, 
					() => {Directorios = FileSystem.Dir();});
			};

		}

		internal static void proImprimePDFs(SqlConnection rcConexion, string stTipoServicio, int iGanoregi, int lGnumregi, string stPath, bool bDeRX = false, bool pNoRealizados = false, bool PGenerarPDFDesdeInformesMasivos = false, string PIdProcesoMasivo = "")
		{

			try
			{

				string StSql = String.Empty;
				DataSet RrDatos = null;
				string stRutaPDF = String.Empty;
				string loNombreReport = String.Empty;
				VBMath.Randomize();

				if (bDeRX)
				{

					StSql = "SELECT odocupdf FROM DDOCUPDF WHERE " + 
					        "gnombdoc = '" + stTipoServicio + iGanoregi.ToString() + StringsHelper.Format(lGnumregi, "0000000000") + "RX" + 
					        ((pNoRealizados) ? "NO" : "") + "'";

				}
				else
				{

					StSql = "SELECT " + 
					        "C.dnombpdf, C.odocupdf " + 
					        "FROM " + 
					        "DVALORAM A " + 
					        "INNER JOIN DCONCLIN B ON B.gcodconc = A.gcodconc " + 
					        "INNER JOIN DDOCUPDF C ON C.gnombdoc = A.ovallcon AND C.inumdocu = 1 AND " + 
					        "(C.dnombpdf LIKE '%.pdf' OR C.dnombpdf LIKE '%.jpg' OR C.dnombpdf LIKE '%.bmp')" + 
					        " WHERE " + 
					        "A.itiposer = '" + stTipoServicio + "' AND " + 
					        "A.ganoregi = " + iGanoregi.ToString() + " AND " + 
					        "A.gnumregi = " + lGnumregi.ToString() + " AND " + 
					        "A.fapuntes = (SELECT MAX(fapuntes) FROM DVALORAM WHERE " + 
					        "itiposer = '" + stTipoServicio + "' AND " + 
					        "ganoregi = " + iGanoregi.ToString() + " AND " + 
					        "gnumregi = " + lGnumregi.ToString() + ") AND " + 
					        "B.iticocli = 'A' AND " + 
					        "isNull(A.iprivado, 'N') = 'N'";

				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
				{

					stRutaPDF = stPath + "\\TMP_" + NumeroAleatorio().ToString() + ".pdf";

					//UPGRADE_ISSUE: (2068) rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
					if (RecuperarFicheroArchivo_b((UpgradeStubs.RDO_rdoColumn) iteration_row["odocupdf"], stRutaPDF))
					{

						if (PGenerarPDFDesdeInformesMasivos && PIdProcesoMasivo.Trim() != "")
						{
							loNombreReport = PIdProcesoMasivo + DateTime.Now.ToString("yyyyMMddHHmmss") + fGET_HASH(5) + ".pdf";
							File.Copy(stRutaPDF, stPath + "\\" + loNombreReport); //Guarda el nombre
							File.Delete(stRutaPDF);
						}
						else
						{
							string tempRefParam = "open";
							string tempRefParam2 = "";
							string tempRefParam3 = "";
							InterconsultasListadoSupport.PInvoke.SafeNative.shell32.ShellExecute(-1, ref tempRefParam, ref stRutaPDF, ref tempRefParam2, ref tempRefParam3, 1); //Me.hwnd
						}

					}


				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();
			}
			catch
			{

				//MsgBox "ERROR proImprimePDFs: " & Err.Description, vbCritical + vbOKOnly, "proImprimePDFs"
			}

		}

		internal static int NumeroAleatorio(int maximo = 100000000)
		{

			try
			{

				// Si no se inicializ� la semilla de aleatoriedad, la inicializamos

				if (!bSemillaIniciada)
				{
					VBMath.Randomize();
					bSemillaIniciada = true;
				}

				// Genera un n�mero aleatorio positivo entre 0 y maximo


				return Convert.ToInt32(VBMath.Rnd() * maximo);
			}
			catch
			{

				return 0;
			}
		}

		internal static bool RecuperarFicheroWeb(ref string stDirURL, ref string strNomFichero)
		{

			int lRet = 0;

			try
			{

				string tempRefParam = "";
				string tempRefParam2 = null;
				string tempRefParam3 = null;
				lRet = InterconsultasListadoSupport.PInvoke.SafeNative.wininet.InternetOpen(ref tempRefParam, INTERNET_OPEN_TYPE_DIRECT, ref tempRefParam2, ref tempRefParam3, 0);

				lRet = InterconsultasListadoSupport.PInvoke.SafeNative.urlmon.URLDownloadToFile(0, ref stDirURL, ref strNomFichero, 0, 0);


				return true;
			}
			catch
			{

				Debug.WriteLine(VB6.TabLayout(Information.Err().LastDllError.ToString(), lRet.ToString()));
				return false;
			}
		}

		//UPGRADE_ISSUE: (2068) rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		internal static bool RecuperarFicheroArchivo_b(UpgradeStubs.RDO_rdoColumn varFichero, string strNomFichero)
		{

			int intFichero = 0;
			int lngLongitud = 0;
			byte[] bytFichero = null; //Variable a almacenar en Fichero

			try
			{


				//UPGRADE_ISSUE: (2064) RDO.rdoColumn property varFichero.Value was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				bytFichero = (byte[]) ArraysHelper.DeepCopy(varFichero.getValue());

				if (FileSystem.Dir(strNomFichero, FileAttribute.Normal) != "")
				{
					File.Delete(strNomFichero);
				}

				intFichero = FileSystem.FreeFile();

				FileSystem.FileOpen(intFichero, strNomFichero, OpenMode.Binary, OpenAccess.Default, OpenShare.Default, -1);

				lngLongitud = bytFichero.GetUpperBound(0);

				for (int lngContador = 0; lngContador <= lngLongitud; lngContador++)
				{
					//UPGRADE_WARNING: (2080) Put was upgraded to FilePutObject and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
					FileSystem.FilePutObject(intFichero, bytFichero[lngContador], 0);
				}

				FileSystem.FileClose(intFichero);


				return true;
			}
			catch
			{

				//MsgBox "ERROR RecuperarFicheroArchivo_b: " & Err.Description, vbCritical + vbOKOnly, "RecuperarFicheroArchivo_b"
				return false;
			}
		}

		internal static void proImprimeYConcatenaPDFs(SqlConnection rcConexion, string stTipoServicio, int iGanoregi, int lGnumregi, string stPath, bool bDeRX = false)
		{

			try
			{

				string StSql = String.Empty;
				DataSet RrDatos = null;
				int iArchivos = 0;

				VBMath.Randomize();

				if (bDeRX)
				{

					StSql = "SELECT odocupdf FROM DDOCUPDF WHERE " + 
					        "gnombdoc = '" + stTipoServicio + iGanoregi.ToString() + StringsHelper.Format(lGnumregi, "0000000000") + "RX'";

				}
				else
				{

					StSql = "SELECT " + 
					        "C.dnombpdf, C.odocupdf " + 
					        "FROM " + 
					        "DVALORAM A " + 
					        "INNER JOIN DCONCLIN B ON B.gcodconc = A.gcodconc " + 
					        "INNER JOIN DDOCUPDF C ON C.gnombdoc = A.ovallcon AND C.inumdocu = 1 AND " + 
					        "(C.dnombpdf LIKE '%.pdf' OR C.dnombpdf LIKE '%.jpg' OR C.dnombpdf LIKE '%.bmp')" + 
					        " WHERE " + 
					        "A.itiposer = '" + stTipoServicio + "' AND " + 
					        "A.ganoregi = " + iGanoregi.ToString() + " AND " + 
					        "A.gnumregi = " + lGnumregi.ToString() + " AND " + 
					        "A.fapuntes = (SELECT MAX(fapuntes) FROM DVALORAM WHERE " + 
					        "itiposer = '" + stTipoServicio + "' AND " + 
					        "ganoregi = " + iGanoregi.ToString() + " AND " + 
					        "gnumregi = " + lGnumregi.ToString() + ") AND " + 
					        "B.iticocli = 'A' AND " + 
					        "isNull(A.iprivado, 'N') = 'N'";

				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				iArchivos = -1;

				if (bArrayInicializado(stRutasPDF))
				{
					iArchivos = stRutasPDF.GetUpperBound(0);
				}

				foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
				{

					iArchivos++;

					stRutasPDF = ArraysHelper.RedimPreserve(stRutasPDF, new int[]{iArchivos + 1});

					if (Convert.ToString(iteration_row["dnombpdf"]).Substring(Strings.Len(Convert.ToString(iteration_row["dnombpdf"])) - 3).Trim().ToUpper() == "PDF")
					{
						stRutasPDF[iArchivos] = stPath + "\\TMP_" + NumeroAleatorio().ToString() + ".pdf";
					}
					else
					{
						stRutasPDF[iArchivos] = stPath + "\\TMP_" + NumeroAleatorio().ToString() + "_" + Convert.ToString(iteration_row["dnombpdf"]).Trim();
					}

					//UPGRADE_ISSUE: (2068) rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
					if (!RecuperarFicheroArchivo_b((UpgradeStubs.RDO_rdoColumn) iteration_row["odocupdf"], stRutasPDF[iArchivos]))
					{

						// Si no se recuper� con �xito, no lo tendremos en cuenta para la fusi�n de PDFs

						iArchivos--;

						if (iArchivos >= 0)
						{
							stRutasPDF = ArraysHelper.RedimPreserve(stRutasPDF, new int[]{iArchivos + 1});
						}
						else
						{
							//UPGRADE_NOTE: (1061) Erase was upgraded to System.Array.Clear. More Information: http://www.vbtonet.com/ewis/ewi1061.aspx
							if (stRutasPDF != null)
							{
								Array.Clear(stRutasPDF, 0, stRutasPDF.Length);
							}
						}

					}


				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				// Ahora traemos las URLs que apunten a PDFs

				if (!bDeRX)
				{

					StSql = "SELECT C.contdocu FROM " + 
					        "DVALORAM A " + 
					        "INNER JOIN DCONCLIN B ON " + 
					        "B.gcodconc = A.gcodconc AND " + 
					        "B.gcodconc = (SELECT nnumeri3 FROM SCONSGLO WHERE gconsglo = 'IVALOECG') " + 
					        "INNER JOIN DOCUMENT C ON " + 
					        "C.gnomdocu = A.ovallcon AND C.contdocu LIKE '%.pdf'" + 
					        " WHERE " + 
					        "A.itiposer = '" + stTipoServicio + "' AND " + 
					        "A.ganoregi = " + iGanoregi.ToString() + " AND " + 
					        "A.gnumregi = " + lGnumregi.ToString() + " AND " + 
					        "A.fapuntes = (SELECT MAX(fapuntes) FROM DVALORAM WHERE " + 
					        "itiposer = '" + stTipoServicio + "' AND " + 
					        "ganoregi = " + iGanoregi.ToString() + " AND " + 
					        "gnumregi = " + lGnumregi.ToString() + ") AND " + 
					        "B.iticocli = 'V' AND " + 
					        "isNull(A.iprivado, 'N') = 'N'";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, rcConexion);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);

					if (bArrayInicializado(stRutasPDF))
					{
						iArchivos = stRutasPDF.GetUpperBound(0);
					}
					else
					{
						iArchivos = -1;
					}

					foreach (DataRow iteration_row_2 in RrDatos.Tables[0].Rows)
					{

						iArchivos++;

						stRutasPDF = ArraysHelper.RedimPreserve(stRutasPDF, new int[]{iArchivos + 1});

						stRutasPDF[iArchivos] = stPath + "\\TMP_" + NumeroAleatorio().ToString() + ".pdf";

						string tempRefParam = ("" + Convert.ToString(iteration_row_2["contdocu"])).Trim();
						if (!RecuperarFicheroWeb(ref tempRefParam, ref stRutasPDF[iArchivos]))
						{

							// Si no se recuper� con �xito, no lo tendremos en cuenta para la fusi�n de PDFs

							iArchivos--;

							if (iArchivos >= 0)
							{
								stRutasPDF = ArraysHelper.RedimPreserve(stRutasPDF, new int[]{iArchivos + 1});
							}
							else
							{
								//UPGRADE_NOTE: (1061) Erase was upgraded to System.Array.Clear. More Information: http://www.vbtonet.com/ewis/ewi1061.aspx
								if (stRutasPDF != null)
								{
									Array.Clear(stRutasPDF, 0, stRutasPDF.Length);
								}
							}

						}


					}

					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrDatos.Close();

				}

				if (iArchivos < 0)
				{

					//UPGRADE_NOTE: (1061) Erase was upgraded to System.Array.Clear. More Information: http://www.vbtonet.com/ewis/ewi1061.aspx
					if (stRutasPDF != null)
					{
						Array.Clear(stRutasPDF, 0, stRutasPDF.Length);
					}

				}
			}
			catch
			{

				//MsgBox "ERROR ImprimeYConcatenaPDFs: " & Err.Description, vbCritical + vbOKOnly, "ImprimeYConcatenaPDFs"
				//UPGRADE_NOTE: (1061) Erase was upgraded to System.Array.Clear. More Information: http://www.vbtonet.com/ewis/ewi1061.aspx
				if (stRutasPDF != null)
				{
					Array.Clear(stRutasPDF, 0, stRutasPDF.Length);
				}
			}

		}

		internal static string tipoAccionPDF(SqlConnection rcConexion)
		{

			string result = String.Empty;
			string StSql = String.Empty;
			DataSet RrDatos = null;

			try
			{

				StSql = "SELECT rTrim(Upper(isNull(valfanu1, 'N'))) FROM SCONSGLO WHERE gconsglo = 'IMPRIPDF'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					result = "N";
				}
				else
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					switch(Convert.ToString(RrDatos.Tables[0].Rows[0][0]))
					{
						case "P" : 
							result = "P"; 
							break;
						case "S" : 
							result = "S"; 
							break;
						default:
							result = "N"; 
							break;
					}
				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();

				return result;
			}
			catch
			{

				return "N";
			}
		}

		internal static bool bArrayInicializado(string[] oArray)
		{

			try
			{


				return oArray.GetUpperBound(0) >= 0;
			}
			catch
			{

				return false;
			}
		}

		internal static string fObtenerAlertaPaciente(SqlConnection ParaConexion, string ParaIdPaciente)
		{
			string result = String.Empty;
			result = "";
			string sql = "SELECT iverobse, oobserva from DPACIENT where gidenpac='" + ParaIdPaciente + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ParaConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(RR.Tables[0].Rows[0]["iverobse"]) + "" == "S")
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					result = (Convert.ToString(RR.Tables[0].Rows[0]["oobserva"]) + "").Trim();
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RR.Close();
			return result;
		}

		internal static string obtieneIDENTIFICADOR(string NIF)
		{

			string cadena = String.Empty;

			for (int contador = 1; contador <= 9; contador++)
			{
				double dbNumericTemp = 0;
				if (!Double.TryParse(NIF.Substring(contador - 1, Math.Min(1, NIF.Length - (contador - 1))), NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
				{
					switch(NIF.Substring(contador - 1, Math.Min(1, NIF.Length - (contador - 1))))
					{
						case "A" : 
							cadena = "10"; 
							break;
						case "B" : 
							cadena = "11"; 
							break;
						case "C" : 
							cadena = "12"; 
							break;
						case "D" : 
							cadena = "13"; 
							break;
						case "E" : 
							cadena = "14"; 
							break;
						case "F" : 
							cadena = "15"; 
							break;
						case "G" : 
							cadena = "16"; 
							break;
						case "H" : 
							cadena = "17"; 
							break;
						case "I" : 
							cadena = "18"; 
							break;
						case "J" : 
							cadena = "19"; 
							break;
						case "K" : 
							cadena = "20"; 
							break;
						case "L" : 
							cadena = "21"; 
							break;
						case "M" : 
							cadena = "22"; 
							break;
						case "N" : 
							cadena = "23"; 
							break;
						case "O" : 
							cadena = "24"; 
							break;
						case "P" : 
							cadena = "25"; 
							break;
						case "Q" : 
							cadena = "26"; 
							break;
						case "R" : 
							cadena = "27"; 
							break;
						case "S" : 
							cadena = "28"; 
							break;
						case "T" : 
							cadena = "29"; 
							break;
						case "U" : 
							cadena = "30"; 
							break;
						case "V" : 
							cadena = "31"; 
							break;
						case "W" : 
							cadena = "32"; 
							break;
						case "X" : 
							cadena = "33"; 
							break;
						case "Y" : 
							cadena = "34"; 
							break;
						case "Z" : 
							cadena = "35"; 
							break;
					}
					if (contador == 1)
					{
						cadena = cadena + NIF.Substring(1, Math.Min(8, NIF.Length - 1));
					}
					else
					{
						cadena = cadena + NIF.Substring(0, Math.Min(8, NIF.Length));
					}
					break;
				}

			}

			cadena = cadena + "142800";
			string digitoControl = Decimal.Parse(cadena, NumberStyles.Currency | NumberStyles.AllowExponent).ToString();
			digitoControl = (Conversion.Val(digitoControl) - (Conversion.Val((Double.Parse(digitoControl) / 97).ToString()) * 97)).ToString();


			return "ES" + (98 - Double.Parse(digitoControl)).ToString() + "000" + NIF;



		}

		internal static void proObtenerDatosFinanciadora_CONSUMO(SqlConnection PRc, string PTip, int Pano, int PNum, ref string PFec, ref string stSoc, ref string stIns)
		{
			string sql = String.Empty;
			DataSet RR = null;

			switch(PTip)
			{
				case "LE" : 
					sql = " select gsocieda, ginspecc from AEPISLEQ " + 
					      " where ganoregi = " + Pano.ToString() + " and gnumregi = " + PNum.ToString(); 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, PRc); 
					RR = new DataSet(); 
					tempAdapter.Fill(RR); 
					if (RR.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stSoc = Convert.ToString(RR.Tables[0].Rows[0]["gsocieda"]) + "";
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stIns = Convert.ToString(RR.Tables[0].Rows[0]["ginspecc"]) + "";
					} 
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
					RR.Close(); 
					break;
				case "PR" : 
					sql = " select gsocieda, ginspecc from QPROGQUI " + 
					      " where ganoregi = " + Pano.ToString() + " and gnumregi = " + PNum.ToString(); 
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, PRc); 
					RR = new DataSet(); 
					tempAdapter_2.Fill(RR); 
					if (RR.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stSoc = Convert.ToString(RR.Tables[0].Rows[0]["gsocieda"]) + "";
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stIns = Convert.ToString(RR.Tables[0].Rows[0]["ginspecc"]) + "";
					} 
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
					RR.Close(); 
					break;
				case "U" : 
					sql = " select gsocieda, ginspecc from UEPISURG " + 
					      " where ganourge = " + Pano.ToString() + " and gnumurge = " + PNum.ToString(); 
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, PRc); 
					RR = new DataSet(); 
					tempAdapter_3.Fill(RR); 
					if (RR.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stSoc = Convert.ToString(RR.Tables[0].Rows[0]["gsocieda"]) + "";
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stIns = Convert.ToString(RR.Tables[0].Rows[0]["ginspecc"]) + "";
					} 
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
					RR.Close(); 
					break;
				case "C" : 
					sql = " select gsocieda, ginspecc from CCONSULT " + 
					      " where ganoregi = " + Pano.ToString() + " and gnumregi = " + PNum.ToString(); 
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, PRc); 
					RR = new DataSet(); 
					tempAdapter_4.Fill(RR); 
					if (RR.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stSoc = Convert.ToString(RR.Tables[0].Rows[0]["gsocieda"]) + "";
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stIns = Convert.ToString(RR.Tables[0].Rows[0]["ginspecc"]) + "";
					} 
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
					RR.Close(); 
					break;
				case "Q" : 
					sql = " select gsocieda, ginspecc from QINTQUIR " + 
					      " where ganoregi = " + Pano.ToString() + " and gnumregi = " + PNum.ToString(); 
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, PRc); 
					RR = new DataSet(); 
					tempAdapter_5.Fill(RR); 
					if (RR.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stSoc = Convert.ToString(RR.Tables[0].Rows[0]["gsocieda"]) + "";
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stIns = Convert.ToString(RR.Tables[0].Rows[0]["ginspecc"]) + "";
					} 
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
					RR.Close(); 
					break;
				case "H" : 
					//UPGRADE_WARNING: (1068) tempRefParam2 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx 
					//UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx 
					object tempRefParam = PFec; 
					object tempRefParam2 = PFec; 
					sql = " select gsocieda , ginspecc from AMOVIFIN A " + 
					      " where A.itiposer ='" + PTip + "' and A.ganoregi =  " + Pano.ToString() + " and A.gnumregi =" + PNum.ToString() + 
					      "   and A.fmovimie <=" + FormatFechaHMS(ref tempRefParam) + " and " + 
					      "       A.fmovimie = (select max(fmovimie) from AMOVIFIN " + 
					      " where itiposer = A.itiposer and ganoregi = A.ganoregi and gnumregi = A.gnumregi " + 
					      " and AMOVIFIN.fmovimie<=" + FormatFechaHMS(ref tempRefParam2) + ")"; 
					PFec = Convert.ToString(tempRefParam2); 
					PFec = Convert.ToString(tempRefParam); 
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql, PRc); 
					RR = new DataSet(); 
					tempAdapter_6.Fill(RR); 
					if (RR.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stSoc = Convert.ToString(RR.Tables[0].Rows[0]["gsocieda"]) + "";
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stIns = Convert.ToString(RR.Tables[0].Rows[0]["ginspecc"]) + "";
					} 
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx 
					RR.Close(); 
					 
					//Debido a un error reportado desde CURIA, se generaban apuntes en FCONSPAC sin codigo de sociedad. IMPOSIBLE!!! 
					//Esto se producia porque existe un desfase entre la fecha del ultimo movimiento  y la fecha de imputacion ya que no puede 
					//haber imputaciones de consumos anteriores a la fecha de movimientos (y esto es lo que se produce). 
					//Para eso se a�ade, que en estos casos, se coga la ultima sociedad existente en amovifin 
					if (stSoc.Trim() == "")
					{
						sql = " select gsocieda , ginspecc from AMOVIFIN " + 
						      " where itiposer='" + PTip + "' and ganoregi =  " + Pano.ToString() + " and gnumregi =" + PNum.ToString() + 
						      "  order by fmovimie desc ";
						SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql, PRc);
						RR = new DataSet();
						tempAdapter_7.Fill(RR);
						if (RR.Tables[0].Rows.Count != 0)
						{
							//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
							RR.MoveFirst();
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							stSoc = Convert.ToString(RR.Tables[0].Rows[0]["gsocieda"]) + "";
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							stIns = Convert.ToString(RR.Tables[0].Rows[0]["ginspecc"]) + "";
						}
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RR.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RR.Close();
					} 
					break;
			}
		}

		internal static bool fCompletarDiagnostico(ref SqlConnection pConexion, string pPrestacion, string pSociedad, string pCodDiag, string pDesDiag)
		{

			bool result = false;
			string StSql = String.Empty;
			DataSet RrDatos = null;
			string stOblSoc = String.Empty;
			Mensajes.ClassMensajes oClass = null;

			try
			{

				oClass = new Mensajes.ClassMensajes();

				StSql = "SELECT iobldiag FROM DSOCIEDA WHERE gsocieda = " + pSociedad + " AND iobldiag IS NOT NULL";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, pConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{

					// Es correcto ya que, seg�n el Cliente asistencial, no tiene por qu� completar el diagn�stico

					result = true;

				}
				else
				{

					// En funci�n del valor, se le exigir� una cosa u otra

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					stOblSoc = Convert.ToString(RrDatos.Tables[0].Rows[0]["iobldiag"]).Trim().ToUpper();

					StSql = "SELECT isNull(iobldiag, 'N') iobldiag FROM DCODPRES WHERE gprestac = '" + pPrestacion + "'";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, pConexion);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);

					if (RrDatos.Tables[0].Rows.Count == 0)
					{

						// No deber�a poder ser porque la prestaci�n no existir�a.

						string tempRefParam = "Citaci�n";
						string tempRefParam2 = "";
						short tempRefParam3 = 1100;
						object tempRefParam4 = new object{"Prestaci�n"};
						oClass.RespuestaMensaje(ref tempRefParam, ref tempRefParam2, ref tempRefParam3, ref pConexion, ref tempRefParam4);
						result = false;

					}
					else
					{

						// En funci�n de ese valor, vemos qu� hacemos

						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToString(RrDatos.Tables[0].Rows[0]["iobldiag"]).Trim().ToUpper() == "N")
						{

							// Se permite continuar porque seg�n la prestaci�n no es necesario registrar el diagn�stico

							result = true;

						}
						else
						{

							// Tenemos en cuenta el valor de la sociedad


							switch(stOblSoc)
							{
								case "O" : 
									 
									result = true; 
									 
									break;
								case "R" : 
									 
									result = (pDesDiag.Trim() != ""); 
									 
									if (!result)
									{
										string tempRefParam5 = "Citaci�n";
										string tempRefParam6 = "";
										short tempRefParam7 = 1040;
										object tempRefParam8 = new object{"Diagn�stico"};
										oClass.RespuestaMensaje(ref tempRefParam5, ref tempRefParam6, ref tempRefParam7, ref pConexion, ref tempRefParam8);
									} 
									 
									break;
								case "RC" : 
									 
									result = (pDesDiag.Trim() != "") && (pCodDiag.Trim() != ""); 
									 
									if (!result)
									{
										string tempRefParam9 = "Citaci�n";
										string tempRefParam10 = "";
										short tempRefParam11 = 1040;
										object tempRefParam12 = new object{"Diagn�stico codificado"};
										oClass.RespuestaMensaje(ref tempRefParam9, ref tempRefParam10, ref tempRefParam11, ref pConexion, ref tempRefParam12);
									} 
									 
									break;
							}

						}

					}

				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDatos.Close();
				oClass = null;

				return result;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("Error en fCompletarDiagnostico: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		internal static bool llamarAyudaDiagnostico(SqlConnection pConexion, string iAccion, string usuario, string Formulario, string fecha, string gidenpac, int iSociedad, string stInspeccion, string Prestacion, int tipoCita, int tipoOrigen, ref string stCod, ref string stDes, ref string stObs)
		{

			bool result = false;
			SqlCommand RqSugerencia = null;

			try
			{

				//UPGRADE_ISSUE: (2064) RDO.rdoConnection method pConexion.CreateQuery was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RqSugerencia = UpgradeStubs.System_Data_SqlClient_SqlConnection.CreateQuery("Sugerencia", "{Call sugerenciaDiagnostico (?,?,?,?,?,?,?,?,?,?,?,?,?)}");

				// Direcci�n de los par�metros

				RqSugerencia.Parameters[0].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[1].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[2].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[3].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[4].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[5].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[6].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[7].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[8].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[9].Direction = ParameterDirection.Input;
				RqSugerencia.Parameters[10].Direction = ParameterDirection.Output;
				RqSugerencia.Parameters[11].Direction = ParameterDirection.Output;
				RqSugerencia.Parameters[12].Direction = ParameterDirection.Output;

				// Valores de los par�metros

				RqSugerencia.Parameters[0].Value = iAccion;
				RqSugerencia.Parameters[1].Value = usuario;
				RqSugerencia.Parameters[2].Value = Formulario;

				if (fecha.Trim() != "")
				{
					RqSugerencia.Parameters[3].Value = fecha;
				}
				else
				{
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					RqSugerencia.Parameters[3].Value = DBNull.Value;
				}

				RqSugerencia.Parameters[4].Value = gidenpac;

				if (iSociedad <= 0)
				{
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					RqSugerencia.Parameters[5].Value = DBNull.Value;
				}
				else
				{
					RqSugerencia.Parameters[5].Value = iSociedad;
				}

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				RqSugerencia.Parameters[6].Value = (stInspeccion.Trim() == "") ? DBNull.Value : stInspeccion.Trim();

				if (Prestacion.Trim() != "")
				{
					RqSugerencia.Parameters[7].Value = Prestacion;
				}
				else
				{
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					RqSugerencia.Parameters[7].Value = DBNull.Value;
				}

				if (tipoCita < 0)
				{
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					RqSugerencia.Parameters[8].Value = DBNull.Value;
				}
				else
				{
					RqSugerencia.Parameters[8].Value = tipoCita;
				}

				if (tipoOrigen < 0)
				{
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					RqSugerencia.Parameters[9].Value = DBNull.Value;
				}
				else
				{
					RqSugerencia.Parameters[9].Value = tipoOrigen;
				}

				// Ejecutamos el procedimiento

				RqSugerencia.ExecuteNonQuery(null);

				// Devolvemos los resultados

				stCod = ("" + Convert.ToString(RqSugerencia.Parameters[10].Value)).Trim();
				stDes = ("" + Convert.ToString(RqSugerencia.Parameters[11].Value)).Trim();
				stObs = ("" + Convert.ToString(RqSugerencia.Parameters[12].Value)).Trim();

				// Cerramos y salimos

				//UPGRADE_ISSUE: (2064) RDO.rdoQuery method RqSugerencia.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				UpgradeStubs.System_Data_SqlClient_SqlCommand.Close();


				return true;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show(excep.Message, "Sugerencia de Diagn�stico", MessageBoxButtons.OK, MessageBoxIcon.Error);
				stCod = "";
				stDes = "";
				stObs = "";

				return result;
			}
		}


		internal static string obtieneAlergiasPaciente(SqlConnection conexionActiva, string stPaciente, string gUsuario)
		{
			StringBuilder result = new StringBuilder();
			string StSql = String.Empty;
			DataSet rdoTemp = null;

			try
			{

				result = new StringBuilder("");

				StSql = "SELECT ialermed, oalermed FROM DPACIENT WHERE gidenpac = '" + stPaciente + "' ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, conexionActiva);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					//si recupera algun valor hay que buscar el valor que tiene
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["ialermed"]))
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["ialermed"]) == "S")
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["oalermed"]))
							{
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								result = new StringBuilder(Convert.ToString(rdoTemp.Tables[0].Rows[0]["oalermed"]).Trim().ToUpper());
							}
						}
					}

				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rdoTemp.Close();


				StSql = "SELECT DPACTIVO.dpactivo FROM DPACALPA " + 
				        "INNER JOIN DPACTIVO ON DPACALPA.gpactivo = DPACTIVO.gpactivo " + 
				        "WHERE (DPACALPA.gidenpac = '" + stPaciente + "') AND (DPACALPA.fborrado IS NULL) ";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, conexionActiva);
				rdoTemp = new DataSet();
				tempAdapter_2.Fill(rdoTemp);
				foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
				{
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (!Convert.IsDBNull(iteration_row["dpactivo"]))
					{
						if (result.ToString().Trim() == "")
						{
							result.Append(Convert.ToString(iteration_row["dpactivo"]).Trim().ToUpper());
						}
						else
						{
							result.Append(" -- " + Convert.ToString(iteration_row["dpactivo"]).Trim().ToUpper());
						}
					}
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rdoTemp.Close();


				StSql = "SELECT IFMSAL_GTERAP.tgterap from DPACALGT " + 
				        "INNER JOIN IFMSAL_GTERAP ON DPACALGT.ggrutera = IFMSAL_GTERAP.gterap " + 
				        "WHERE DPACALGT.fborrado is null and DPACALGT.gidenpac = '" + stPaciente + "'";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, conexionActiva);
				rdoTemp = new DataSet();
				tempAdapter_3.Fill(rdoTemp);
				foreach (DataRow iteration_row_2 in rdoTemp.Tables[0].Rows)
				{
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (!Convert.IsDBNull(iteration_row_2["tgterap"]))
					{
						if (result.ToString().Trim() == "")
						{
							result.Append(Convert.ToString(iteration_row_2["tgterap"]).Trim().ToUpper());
						}
						else
						{
							result.Append(" -- " + Convert.ToString(iteration_row_2["tgterap"]).Trim().ToUpper());
						}
					}
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rdoTemp.Close();

				return result.ToString();
			}
			catch
			{

				result = new StringBuilder("");
				AnalizaError("serrores", Path.GetDirectoryName(Application.ExecutablePath), gUsuario, "obtieneAlergiasPaciente", conexionActiva);

				return result.ToString();
			}
		}
		internal static void proMandaJustificante(SqlConnection pConexion, int pGanoregi, int pGnumregi, string pTipo, string pUsuario)
		{

			string StSql = String.Empty;
			DataSet RrDatos = null;
			DataSet RrDestino = null;

			try
			{

				// Control de errores

				if (pGanoregi <= 0 || pGnumregi <= 0 || (pTipo != "E" && pTipo != "S"))
				{

					MessageBox.Show("Par�metros para enviar justificante incorrectos", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;

				}

				//borramos la anterior que est� pendiente
				SqlCommand tempCommand = new SqlCommand("DELETE CMAILSMS WHERE  WHERE GANOREGI = " + pGanoregi.ToString() + " AND GNUMREGI = " + pGnumregi.ToString() + " AND  FENVIADO IS NULL", pConexion);
				tempCommand.ExecuteNonQuery();


				//Si el canal prevalente es mail o sms,.....
				if (pTipo == "E" || pTipo == "S")
				{
					StSql = "SELECT B.ddiemail, B.ntelefo3 FROM " + 
					        "CCITPROG A " + 
					        "INNER JOIN DPACIENT B ON " + 
					        "B.gidenpac = A.gidenpac " + 
					        "WHERE " + 
					        "A.ganoregi = " + pGanoregi.ToString() + " AND " + 
					        "A.gnumregi = " + pGnumregi.ToString();

					SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, pConexion);
					RrDatos = new DataSet();
					tempAdapter.Fill(RrDatos);

					if (RrDatos.Tables[0].Rows.Count != 0)
					{

						StSql = "SELECT * FROM CMAILSMS WHERE 1 = 2";

						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, pConexion);
						RrDestino = new DataSet();
						tempAdapter_2.Fill(RrDestino);

						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDestino.AddNew was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrDestino.AddNew();

						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						RrDestino.Tables[0].Rows[0]["Tipo"] = pTipo;

						// Si es e-mail, como destino ponemos el correo. Si no, el m�vil

						if (pTipo == "E")
						{

							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ddiemail"]))
							{
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								RrDestino.Tables[0].Rows[0]["Destino"] = Convert.ToString(RrDatos.Tables[0].Rows[0]["ddiemail"]).Trim();
							}

						}
						else
						{

							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ntelefo3"]))
							{
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								RrDestino.Tables[0].Rows[0]["Destino"] = Convert.ToString(RrDatos.Tables[0].Rows[0]["ntelefo3"]).Trim();
							}

						}

						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						RrDestino.Tables[0].Rows[0]["ganoregi"] = pGanoregi;
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						RrDestino.Tables[0].Rows[0]["gnumregi"] = pGnumregi;
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						RrDestino.Tables[0].Rows[0]["gUsuario"] = pUsuario;

						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						string tempQuery = RrDestino.Tables(0).TableName;
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
						tempAdapter_3.Update(RrDestino, RrDestino.Tables(0).TableName);

						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDestino.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						RrDestino.Close();

					}

					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrDatos.Close();
				}
			}
			catch
			{

				AnalizaError("serrores", Path.GetDirectoryName(Application.ExecutablePath), pUsuario, "proMandaJustificante", pConexion);
			}

		}
		internal static void proGrabarHistoricoJustificante(SqlConnection pConexion, int pGanoregi, int pGnumregi, string pTipo, string pEstado, string pMotEnvi, string pUsuario, string pMaquina, string pObservaciones, System.DateTime pCitaAntigua)
		{

			string StSql = String.Empty;
			DataSet RrDatos = null;
			DataSet RrDestino = null;
			System.DateTime ffecitant = DateTime.FromOADate(0);

			if (pGanoregi <= 0 || pGnumregi <= 0)
			{

				MessageBox.Show("Par�metros para grabar en el hist�rico de env�os es incorrectos", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;

			}

			//Si es postal
			if (pTipo == "P")
			{
				//Comprobamos si ha habido env�o anterior
				StSql = "SELECT top 1 * from CACTENVI WHERE ganoregi = " + pGanoregi.ToString() + " AND gnumregi = " + pGnumregi.ToString() + " AND iestados = 'M' and tipoenvi in ('P','E','S') order by fmecaniz desc";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, pConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);
				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					//'tSql = "SELECT  top 1  convert(char(8),ffeccita,112) + ' ' + convert(char(8),ffeccita,108) ffeccita CCITPMOD WHERE ganoregi = " & Trim(RrDatos("ganoregi")) & " AND gnumregi = " & Trim(RrDatos("gnumregi")) & " AND  fmodific< " & FormatFechaHMS(RrDatos("fmecaniz")) & " order by fmodific desc"
					//UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					object tempRefParam = RrDatos.Tables[0].Rows[0]["festados"];
					StSql = "SELECT  top 1  ffeccita from CCITPMOD WHERE ganoregi = " + Convert.ToString(RrDatos.Tables[0].Rows[0]["ganoregi"]).Trim() + " AND gnumregi = " + Convert.ToString(RrDatos.Tables[0].Rows[0]["gnumregi"]).Trim() + " AND  fmodific> " + FormatFechaHMS(ref tempRefParam) + " order by fmodific asc";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, pConexion);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);
					if (RrDatos.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						ffecitant = Convert.ToDateTime(RrDatos.Tables[0].Rows[0]["ffeccita"]);
					}
				}
			}
			StSql = "SELECT B.ddiemail, B.ntelefo3, B.ddirepac, A.gidenpac, A.gprestac, A.gagendas, A.iorigcit FROM " + 
			        "CCITPROG A WITH(NOLOCK) " + 
			        "INNER JOIN DPACIENT B WITH(NOLOCK) ON " + 
			        "B.gidenpac = A.gidenpac " + 
			        "WHERE " + 
			        "A.ganoregi = " + pGanoregi.ToString() + " AND " + 
			        "A.gnumregi = " + pGnumregi.ToString();

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, pConexion);
			RrDatos = new DataSet();
			tempAdapter_3.Fill(RrDatos);

			if (RrDatos.Tables[0].Rows.Count != 0)
			{
				StSql = "SELECT * FROM CACTENVI WHERE 1 = 2";

				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSql, pConexion);
				RrDestino = new DataSet();
				tempAdapter_4.Fill(RrDestino);
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDestino.AddNew was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDestino.AddNew();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["gidenpac"] = Convert.ToString(RrDatos.Tables[0].Rows[0]["gidenpac"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["fmecaniz"] = DateTime.Now;
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["gUsuario"] = pUsuario;
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["ganoregi"] = pGanoregi;
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["gnumregi"] = pGnumregi;
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["gprestac"] = Convert.ToString(RrDatos.Tables[0].Rows[0]["gprestac"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["gagendas"] = (Convert.ToString(RrDatos.Tables[0].Rows[0]["gagendas"]) + "").Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["tipoenvi"] = pTipo; //''', M-anual, S-MS,E-mail,P-ostal,N-oEnvio
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["iestados"] = pEstado; //''', P-ENDIENTE, A-NULADO, E-RR�NEA y M -AMADA         '''' QU� COCHINA LA T�A GUARRA, ES MANDADA
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["imotenvi"] = pMotEnvi; //''', O para citas nuevas, R para reprogramaciones y C para cambios
				//Si es c. elect�nico ponemos el correo
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ddiemail"]) && pTipo == "E")
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					RrDestino.Tables[0].Rows[0]["lugarenv"] = Convert.ToString(RrDatos.Tables[0].Rows[0]["ddiemail"]).Trim();
				}

				//Si es m�vil ponemos el tel�fono
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ntelefo3"]) && pTipo == "S")
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					RrDestino.Tables[0].Rows[0]["lugarenv"] = Convert.ToString(RrDatos.Tables[0].Rows[0]["ntelefo3"]).Trim();
				}

				//Si es correo postal ponemos la direcci�n
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ddirepac"]) && pTipo == "P")
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					RrDestino.Tables[0].Rows[0]["lugarenv"] = Convert.ToString(RrDatos.Tables[0].Rows[0]["ddirepac"]).Trim();
				}

				//Si es no enviable o es impresi�n manual....
				if (pTipo == "M" || pTipo == "N")
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					RrDestino.Tables[0].Rows[0]["lugarenv"] = pMaquina.Trim();
				}

				//            If IsNull(RrDatos("iorigcit")) Then
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				RrDestino.Tables[0].Rows[0]["apliorig"] = "I";
				//            Else
				//                RrDestino!apliorig = Trim(RrDatos("iorigcit"))
				//            End If
				if (pObservaciones.Trim() != "")
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					RrDestino.Tables[0].Rows[0]["observac"] = pObservaciones.Trim();
				}

				if (ffecitant != DateTime.Parse("00:00:00") && ffecitant != pCitaAntigua)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					RrDestino.Tables[0].Rows[0]["ffeciant"] = ffecitant;
				}

				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				string tempQuery = RrDestino.Tables(0).TableName;
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery, "");
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_5);
				tempAdapter_5.Update(RrDestino, RrDestino.Tables(0).TableName);
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDestino.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrDestino.Close();

			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrDatos.Close();


		}

		internal static int PrinterIndex(string PrinterName)
		{
			int result = 0;
			for (int I = 0; I <= PrinterHelper.Printers.Count - 1; I++)
			{
				if (PrinterHelper.Printers[I].DeviceName.ToUpper() == PrinterName.ToUpper())
				{
					result = I;
					break;
				}
			}
			return result;
		}

		internal static string fImpresoraDef()
		{
			string result = String.Empty;
			string buffer = new String(' ', 255);
			string tempRefParam = "Windows";
			string tempRefParam2 = "device";
			string tempRefParam3 = "";
			int ret = InterconsultasListadoSupport.PInvoke.SafeNative.kernel32.GetProfileString(ref tempRefParam, ref tempRefParam2, ref tempRefParam3, ref buffer, buffer.Length);
			if (ret != 0)
			{
				result = buffer.Substring(0, Math.Min(buffer.IndexOf(','), buffer.Length)).ToUpper();
			}
			return result;
		}

		internal static string fGET_HASH(int Length)
		{

			double varNum = 0;
			double varAlpha = 0;
			int I = 0;

			StringBuilder stringNum = new StringBuilder();
			StringBuilder stringAlpha = new StringBuilder();
			StringBuilder stringConcat = new StringBuilder();

			VBMath.Randomize();

			for (I = 0; I <= (Length / ((int) 2)); I++)
			{
				varNum = (float) Math.Floor(10 * VBMath.Rnd());
				stringNum.Append(varNum.ToString());
			}

			VBMath.Randomize();

			I = 0;

			do 
			{
				varAlpha = (float) Math.Floor(100 * VBMath.Rnd());
				if (varAlpha > 65 && varAlpha < 90)
				{
					stringAlpha.Append(Strings.Chr(Convert.ToInt32(varAlpha)).ToString());
					I++;
				}
			}
			while(I < (Length / ((int) 2)));


			for (I = 1; I <= (stringNum.ToString() + stringAlpha.ToString()).Length; I++)
			{
				stringConcat.Append(stringNum.ToString().Substring(I - 1, Math.Min(1, stringNum.ToString().Length - (I - 1))) + stringAlpha.ToString().Substring(I - 1, Math.Min(1, stringAlpha.ToString().Length - (I - 1))));
			}

			return stringConcat.ToString();

		}

		internal static bool fPropiedadesPDF(ref object pObjPDFCreator)
		{
			bool result = false;
			result = true;
			try
			{
				if (pObjPDFCreator != null)
				{
					//UPGRADE_TODO: (1067) Member cclose is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					pObjPDFCreator.cclose();
					pObjPDFCreator = null;
				}
				pObjPDFCreator = new PDFCreator.clsPDFCreator();
				//UPGRADE_TODO: (1067) Member cStart is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				pObjPDFCreator.cStart();
				//UPGRADE_TODO: (1067) Member cVisible is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				pObjPDFCreator.cVisible = false;
				//UPGRADE_TODO: (1067) Member cClearCache is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				pObjPDFCreator.cClearCache();
				//UPGRADE_TODO: (1067) Member cOption is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				pObjPDFCreator.cOption["UseAutosave"] = 1; //Nuevo indica que se va auto guardar
				//UPGRADE_TODO: (1067) Member cOption is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				pObjPDFCreator.cOption["AutosaveFormat"] = 0; // 0 = PDF 2= jpg
				//UPGRADE_TODO: (1067) Member cOption is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				pObjPDFCreator.cOption["UseAutosaveDirectory"] = 1; //Nuevo indica que se va auto guardar en una carpeta predeterminada
				//UPGRADE_TODO: (1067) Member cDefaultPrinter is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				pObjPDFCreator.cDefaultPrinter = "PDFCreator";
				//UPGRADE_TODO: (1067) Member cPrinterStop is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				pObjPDFCreator.cPrinterStop = false;
				return result;
			}
			catch
			{
				return false;
			}
		}


		internal static void proEstablecerImpresoraPredeterminada(string Nombre_Impresora)
		{
			//    Dim Prt As Printer
			//    For Each Prt In Printers
			//        If (UCase(Prt.DeviceName) = UCase(Nombre_Impresora)) Then
			//            Set Printer = Prt
			//            Exit For
			//        End If
			//    Next
			IWshRuntimeLibrary.WshNetwork obj_Impresora = new IWshRuntimeLibrary.WshNetwork();
			obj_Impresora.SetDefaultPrinter(Nombre_Impresora);
		}
	}
}