using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace InforInterco
{
	public class ClInforInterco
	{

		public void LoadClase(SqlConnection Conexion, string sttiposer, object LISTADO_GENERAL, string NOMBRE_DSN_ACCESS, string NombreBaseAccess, string gUsuario_Crystal, string gContrase�a_Crystal, string CaminoAplicacion, object Cuadro_Diag_impre)
		{
			Pacientes_Interco.DefInstance.RecogerDatosDeLaClase(Conexion, sttiposer, LISTADO_GENERAL, NOMBRE_DSN_ACCESS, NombreBaseAccess, gUsuario_Crystal, gContrase�a_Crystal, CaminoAplicacion, Cuadro_Diag_impre);
			Pacientes_Interco tempLoadForm = Pacientes_Interco.DefInstance;
			Pacientes_Interco.DefInstance.ShowDialog();
			Pacientes_Interco.DefInstance = null;
		}
	}
}