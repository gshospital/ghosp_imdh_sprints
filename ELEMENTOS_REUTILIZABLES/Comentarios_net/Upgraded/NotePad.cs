using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using System.Globalization;
using System.IO;
using Telerik.WinForms.Documents.Proofing;
using Microsoft.CSharp;




namespace Comentarios
{
	public partial class frmNotePad
		: Telerik.WinControls.UI.RadForm
	{
		bool bDarContenido = false;
		bool bConsult = false;
		bool bMostrarFuentes = false;

		//Variables de la fuente y el texto para guardar los valores iniciales
		string stFuente = String.Empty;
		bool bNegrita = false;
		bool bCursiva = false;
		bool bSubrayado = false;
		double dSize = 0;

		string vTextoDocumento = String.Empty;

		string stDoc = String.Empty;
		string stCaptionVentana = String.Empty;

		int resultado = 0;

		SqlConnection Conexion = null;
		dynamic obVentana = null;

		public frmNotePad()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
            this.LoadDictionary();
        }


        //INDRA CAMAYA JUNIO 03 DEL 2016
        private void LoadDictionary()
        {
            string tdfFileStream = Path.GetDirectoryName(Application.ExecutablePath) + @"\\es-ES.tdf";
            FileStream gg = new FileStream(tdfFileStream, FileMode.Open);
            RadDictionary dictionary = new RadDictionary();
            dictionary.Load(gg);
            ((DocumentSpellChecker)this.txtContenido.SpellChecker).AddDictionary(dictionary, CultureInfo.InvariantCulture);
        }

        private void frmNotePad_Load(Object eventSender, EventArgs eventArgs)
		{

			//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (0)");
			txtContenido.Text = vTextoDocumento;

			if (!bConsult)
			{
                // Si estamos en modo de consulta, mostramos los resultados en Courier New, tama�o 10
                // Si no lo estamos, ponemos la fuente que nos venga.               
                txtContenido.Font = UpgradeHelpers.Helpers.FontHelper.Change(txtContenido.Font ,name:stFuente, size:(float) dSize);
			}

			txtContenido.Font = txtContenido.Font.Change(bold:bNegrita, underline:bSubrayado, italic:bCursiva);

			CD1Font.Font = CD1Font.Font.Change(name:stFuente);
			CD1Font.Font = CD1Font.Font.Change(bold:bNegrita);
			CD1Font.Font = CD1Font.Font.Change(underline:bSubrayado);
			CD1Font.Font = CD1Font.Font.Change(size:(float) dSize);
			CD1Font.Font = CD1Font.Font.Change(italic:bCursiva);

			// Comprueba en que caso estoy
			if (bConsult)
			{
				// Estoy en consulta, solo muestro el contenido del archivo
				for (int i = 0; i <= 6; i++)
				{
                    //camaya todo_x_5
					//Toolbar1.Items[i - 1].Visibility = false;
				}

				//Toolbar1.Buttons(1).Visibility = False
				//DIEGO 17/10/2006 - Hacemos Visibility el bot�n de copiar al portapapeles en modo edici�n				
                _Toolbar1_Button4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
				mnuFileSave.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;

				//DIEGO 17/10/2006 - Hacemos Visibility el bot�n de copiar al portapapeles en modo edici�n
				mnuEditCopy.Visibility = Telerik.WinControls.ElementVisibility.Visible;
				mnuEditCut.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
				mnuEditDelete.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
				mnuEditPaste.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
				mnuEditSelectAll.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
				mnuEditTime.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
				mnuESep1.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
				mnuESep2.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
				mnuOpciones.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;

				// Corrector ortogr�fico

				mnuHerramientas.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
				mnuCorrector.Enabled = false;				
                _Toolbar1_Button9.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
			}
			else
			{
				// Estoy en modo de escritura
				if (!bMostrarFuentes)
				{
					mnuOpciones.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;                    
                    _Toolbar1_Button6.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
				}

				// Corrector ortogr�fico

				mnuHerramientas.Visibility = Telerik.WinControls.ElementVisibility.Visible;
				mnuCorrector.Enabled = true;				
                _Toolbar1_Button9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            }
			this.Text = stCaptionVentana;

			//txtContenido.SelStart = Len(txtContenido.Text)
			txtContenido.IsReadOnly = bConsult;

			//Inicializamos las variables de b�squeda.
			frmbuscar.DefInstance.iCond = 8;


			//    InitializeMe

		}

		private bool isInitializingComponent;
		private void frmNotePad_Resize(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			// Expande el cuadro de texto para que ocupe el �rea interna del formulario hijo actual.
			if ((this.ClientRectangle.Height - ((float) VB6.FromPixelsUserY(txtContenido.Top, 0, 5670, 378)) > 0))
			{
				txtContenido.Height = (int) VB6.ToPixelsUserHeight(this.ClientRectangle.Height - ((float) VB6.FromPixelsUserY(txtContenido.Top, 0, 5670, 378)), 5670, 378);
			}
			if ((this.ClientRectangle.Width - ((float) VB6.FromPixelsUserX(txtContenido.Left, 0, 8085, 539)) > 0))
			{
				txtContenido.Width = (int) VB6.ToPixelsUserWidth(this.ClientRectangle.Width - ((float) VB6.FromPixelsUserX(txtContenido.Left, 0, 8085, 539)), 144, 539);
			}
		}

		private void frmNotePad_Closed(Object eventSender, EventArgs eventArgs)
		{
			if ((!bConsult && CambiosProducidos()) || bComentario.HayPlantillas)
			{
				if (RadMessageBox.Show("�Desea guardar los cambios?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Exclamation) == System.Windows.Forms.DialogResult.Yes)
				{
					mnuFileSave_Click(mnuFileSave, new EventArgs());
				}
				else
				{
					return;
				}
			}

			// Tengo que saber al final si hay o no algo en el fichero

			if (txtContenido.Text != "")
			{
				if (bDarContenido)
				{					
					obVentana.prorecogercomentario(true, txtContenido.Text);
				}
				else
                {                     
                    obVentana.prorecogercomentario(true, "");
				}
			}
		}

		public void mnuCorrector_Click(Object eventSender, EventArgs eventArgs)
		{
			string tmpContenido = txtContenido.Text;
			txtContenido.Text = bComentario.SpellMe(txtContenido.Text);
			this.Activate();
			return;
            
			RadMessageBox.Show("Corrector ortogr�fico no disponible", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);

			txtContenido.Text = tmpContenido;
			return;
    	}

		public void mnuEditBuscar_Click(Object eventSender, EventArgs eventArgs)
		{
			frmbuscar tempLoadForm = frmbuscar.DefInstance;
			if (txtContenido.Text != "")
			{
				frmbuscar.DefInstance.txtbuscar.Text = txtContenido.Text;
			}
			frmbuscar.DefInstance.ShowDialog();
		}

		public void mnuEditCopy_Click(Object eventSender, EventArgs eventArgs)
		{
			Clipboard.Clear();
			if (txtContenido.Text != "")
			{			
				Clipboard.SetText(txtContenido.Text);
			} // Copia el texto seleccionado en el Portapapeles.
		}

		public void mnuEditCut_Click(Object eventSender, EventArgs eventArgs)
		{
			Clipboard.Clear();
			if (txtContenido.Text != "")
			{			
				Clipboard.SetText(txtContenido.Text); // Copia el texto seleccionado en el Portapapeles.
				txtContenido.Text = ""; // Elimina el texto seleccionado.
			}
		}

		public void mnuEditDelete_Click(Object eventSender, EventArgs eventArgs)
		{
            //camaya todo_x_5
            // Si el puntero del mouse no se encuentra al final del bloc de notas ...			
            // if (Convert.ToDouble(VB6.GetActiveControl().SelStart) != Strings.Len(Convert.ToString(VB6.GetActiveControl().Text)))
			//{
			//	// Si no hay texto seleccionado, extiende la selecci�n en uno.			
			//	if (Convert.ToDouble(VB6.GetActiveControl().SelLength) == 0)
			//	{			
			//		VB6.GetActiveControl().SelLength = 1;
			//		// Si el puntero del mouse se encuentra en una l�nea en blanco, extiende la selecci�n en dos.
			//    	if (Strings.Asc(Convert.ToString(VB6.GetActiveControl().SelText)[0]) == 13)
			//		{			
			//			VB6.GetActiveControl().SelLength = 2;
			//		}
			//	}
			//	// Elimina el texto seleccionado.			
			//	VB6.GetActiveControl().SelText = "";
			//}
		}

		public void mnuEditPaste_Click(Object eventSender, EventArgs eventArgs)
		{
			txtContenido.Text = Clipboard.GetText(); // Coloca el texto del Portapapeles en el control activo.
		}

		public void mnuEditSelectAll_Click(Object eventSender, EventArgs eventArgs)
		{
			// Utiliza SelStart y SelLength para seleccionar el texto.
			txtContenido.TabIndex = 0;
            //camaya todo_x_5
			//txtContenido.SelectionLength = Strings.Len(txtContenido.Text);
		}

		public void mnuEditSiguiente_Click(Object eventSender, EventArgs eventArgs)
		{
			if (txtContenido.Text != "")
			{
				frmbuscar tempLoadForm = frmbuscar.DefInstance;
				if (txtContenido.Text != "")
				{
					frmbuscar.DefInstance.txtbuscar.Text = txtContenido.Text;
				}
			
				frmbuscar.DefInstance.cbBuscar_Click(frmbuscar.DefInstance.cbBuscar, new EventArgs());
			}
			else
			{
				RadMessageBox.Show("Debe seleccionar Texto.", "BUSCAR", MessageBoxButtons.OK, RadMessageIcon.Info);
			}
		}

		public void mnuEditTime_Click(Object eventSender, EventArgs eventArgs)
		{
			// Inserta la hora y la fecha actual.
			txtContenido.Text = DateTimeHelper.ToString(DateTime.Now);
		}

		public void mnuFileExit_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}
		public void mnuFileSave_Click(Object eventSender, EventArgs eventArgs)
		{
			string strFilename = String.Empty;
			if (!bConsult)
			{ //Si estoy en modo informe
				grabartexto(); // Reinicializo de nuevo los indicadores para sacar o no la pregunta de guardar cambios
			}
		}

		public void proRecoge(object oVentana, SqlConnection cnConexion, bool bConsulta, string Archivo, string stNomVentana, bool bfuentes, bool bDevolver, string vTextDoc, string stFuenteDoc, bool bNegritaDoc, bool bCursivaDoc, bool bSubrayadoDoc, double dSizeDoc)
		{
			vTextoDocumento = vTextDoc;
			stFuente = stFuenteDoc;
			bNegrita = bNegritaDoc;
			bCursiva = bCursivaDoc;
			bSubrayado = bSubrayadoDoc;
			dSize = dSizeDoc;

			bConsult = bConsulta;
			stDoc = Archivo;
			bMostrarFuentes = bfuentes;
			bDarContenido = bDevolver;
			stCaptionVentana = stNomVentana;

			Conexion = cnConexion;
			obVentana = oVentana;

		}

		public void mnuFuentes_Click(Object eventSender, EventArgs eventArgs)
		{
            //camaya todoX_x_5
            //txtContenido.SelectionLength = 0;			
			//CD1.Flags = 0x2;
			CD1Font.ShowDialog();
			txtContenido.Font = txtContenido.Font.Change(name:CD1Font.Font.Name, size:CD1Font.Font.Size, bold:CD1Font.Font.Bold, underline:CD1Font.Font.Underline, italic:CD1Font.Font.Italic);
		}

		private void Toolbar1_ButtonClick(Object eventSender, EventArgs eventArgs)
		{
			ToolStripItem Button = (ToolStripItem) eventSender;
			switch(Button.Owner.Items.IndexOf(Button) + 1)
			{
				case 1 : 
					mnuFileSave_Click(mnuFileSave, new EventArgs()); 
					break;
				case 3 : case 4 : 
					Clipboard.Clear(); 
					if (txtContenido.Text != "")
					{					
						Clipboard.SetText(txtContenido.Text); // Copia el texto seleccionado en el Portapapeles.
						if (Button.Owner.Items.IndexOf(Button) + 1 == 3)
						{
							txtContenido.Text = "";
						} // Elimina el texto seleccionado.
					} 
					break;
				case 5 : 
					txtContenido.Text = Clipboard.GetText();  // Coloca el texto del Portapapeles en el control activo. 
					break;
				case 6 : 
					mnuFuentes_Click(mnuFuentes, new EventArgs()); 
					 
					break;
				case 7 : 
					mnuEditBuscar_Click(mnuEditBuscar, new EventArgs()); 
					break;
				case 9 : 
					mnuCorrector_Click(mnuCorrector, new EventArgs()); 					
					this.BringToFront(); 
					break;
				case 12 : 
					this.Close(); 
					 
					break;
			}
		}
		private void grabartexto()
		{
			string sql = String.Empty;
			string sql1 = String.Empty;
			bool fNuevo = false;

			DataSet RrSql = null;
			try
			{
				sql = "select * from DOCUMENT where gnomdocu = '" + stDoc + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				//, rdOpenKeyset, rdConcurValues)
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					//If Rrsql.RowCount > 0 Then
					sql1 = "Insert into DOCUMMOD select getdate(),* from DOCUMENT where gnomdocu = '" + stDoc + "'";

					SqlCommand tempCommand = new SqlCommand(sql1, Conexion);
					tempCommand.ExecuteNonQuery();
                    					
					RrSql.Edit();
					fNuevo = false;
				}
				else
				{					
					RrSql.AddNew();					
					RrSql.Tables[0].Rows[0]["gnomdocu"] = stDoc;
					fNuevo = true;
				}
				
				RrSql.Tables[0].Rows[0]["contdocu"] = txtContenido.Text;				
				RrSql.Tables[0].Rows[0]["FontName"] = txtContenido.Font.Name;				
				RrSql.Tables[0].Rows[0]["fontbold"] = txtContenido.Font.Bold;				
				RrSql.Tables[0].Rows[0]["Fontital"] = txtContenido.Font.Italic;				
				RrSql.Tables[0].Rows[0]["fontline"] = txtContenido.Font.Underline;				
				RrSql.Tables[0].Rows[0]["FontSize"] = txtContenido.Font.SizeInPoints;
				if (bComentario.gUsuarioConectado.Trim() != "")
				{					
					RrSql.Tables[0].Rows[0]["gusuario"] = bComentario.gUsuarioConectado;
				}
				
				string tempQuery = RrSql.Tables[0].TableName;
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
				tempAdapter_2.Update(RrSql, RrSql.Tables[0].TableName);
				
				RrSql.Close();

				if (fNuevo)
				{
					// Pedido por Luis desde la Fundaci�n, quieren que se grabe en DOCUMMOD cada vez que se grabe en DOCUMENT
					sql1 = "Insert into DOCUMMOD select getdate(),* from DOCUMENT where gnomdocu = '" + stDoc + "'";
					SqlCommand tempCommand_2 = new SqlCommand(sql1, Conexion);
					tempCommand_2.ExecuteNonQuery();
				}

				stFuente = txtContenido.Font.Name;
				bNegrita = txtContenido.Font.Bold;
				bCursiva = txtContenido.Font.Italic;
				bSubrayado = txtContenido.Font.Underline;
				//dSize = txtContenido.Font.Size
				vTextoDocumento = txtContenido.Text;
			}
			catch
			{
				RadMessageBox.Show("Error al guardar el documento", Application.ProductName);
			}

		}
		private bool CambiosProducidos()
		{
			bool bText = false;
			if (stFuente != txtContenido.Font.Name || bNegrita != txtContenido.Font.Bold || bCursiva != txtContenido.Font.Italic || bSubrayado != txtContenido.Font.Underline || dSize != ((double) txtContenido.Font.SizeInPoints) || vTextoDocumento != txtContenido.Text)
			{

				bText = true;

			}
			return bText;
		}

		private void txtContenido_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 124)
			{
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
	}
}