using Microsoft.VisualBasic;
using System;
using System.Runtime.InteropServices;
using UpgradeHelpers.Helpers;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Comentarios
{
	internal static class bComentario
	{

		public const int LOCALE_SYSTEM_DEFAULT = 0x800;
		public const int LOCALE_SDECIMAL = 0xE; //  separador de decimales
		public const int LOCALE_STHOUSAND = 0xF; //  separador de miles

		public static object moApp = null; //Word.Application


		public static string lSep_Decimal = String.Empty;
		public static bool HayPlantillas = false;
		public static string vstSeparadorDecimal = String.Empty;
		public static string vstSeparadorBD = String.Empty;
		public static string gUsuarioConectado = String.Empty; //usuario conectado

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", EntryPoint = "GetLocaleInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLCData, int cchData);

		public static string DatosPlantilla = String.Empty;

		// Extracts a VB string from a buffer containing a null terminated
		// string
		//UPGRADE_NOTE: (7001) The following declaration (LPSTRToVBString) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string LPSTRToVBString(string s)
		//{
				//int nullpos = (s.IndexOf(Strings.Chr(0).ToString()) + 1);
				//if (nullpos > 0)
				//{
					//return s.Substring(0, Math.Min(nullpos - 1, s.Length));
				//}
				//else
				//{
					//return "";
				//}
		//}
		internal static void ObtenerLocale()
		{
			//'    Dim buffer As String * 100
			//'    Dim dl&
			//'
			//'    'SysInfo.Cls
			//'
			//'    #If Win32 Then
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDECIMAL, buffer, 99)
			//'        lSep_Decimal = LPSTRToVBString(buffer)
			//'    #Else
			//'        Print " Not implemented under Win16"
			//'    #End If
			//' CURIAE EQUIPO DE MIGRACION - NUEVA FORMA DE OBTENER LA CONFIGURACION LOCAL
		}

        //-------------------------------------------------------------------------
        //-------------------------------------------------------------------------
        //-------------------------------------------------------------------------
        //-------------------------------------------------------------------------
        //-------------------------------------------------------------------------
        //-------------------------------------------------------------------------

       
		internal static void InitializeMe()
		{

			//UPGRADE_TODO: (1069) Error handling statement (On Error Resume Next) was converted to a pattern that might have a different behavior. More Information: http://www.vbtonet.com/ewis/ewi1069.aspx
			//' CURIAE EQUIPO DE MIGRACION - NUEVA FORMA DE EJECUTAR EL CORRECTOR ORTOGRADICO MEDIANTE SOFTWARE LIBRE
			//    Set moApp = GetObject(, "Word.Application")
			//
			//    If TypeName(moApp) <> "Nothing" Then
			//
			//        Set moApp = GetObject(, "Word.Application")
			//
			//    Else
			//
			//        Set moApp = CreateObject("Word.Application")
			//
			//        mbKillMe = True
			//
			//    End If

		}

		internal static string SpellMe(string msSpell)
		{
			//' CURIAE EQUIPO DE MIGRACION - NUEVA FORMA DE EJECUTAR EL CORRECTOR ORTOGRADICO MEDIANTE SOFTWARE LIBRE
			//    On Error GoTo No_Bugs
			//
			//    Dim oDoc As Word.Document
			//    Dim iWSE As Integer
			//    Dim iWGE As Integer
			//    Dim sReplace As String
			//    Dim lResp As Long
			//
			//    If msSpell = vbNullString Then Exit Function
			//
			//    'InitializeMe
			//    Set moApp = CreateObject("Word.Application")
			//
			//    Select Case moApp.Version
			//
			//        Case "9.0", "10.0", "11.0"
			//
			//            Set oDoc = moApp.Documents.Add(, , 1, True)
			//
			//        Case Else '"8.0"
			//
			//            Set oDoc = moApp.Documents.Add
			//
			//    End Select
			//
			//    Screen.MousePointer = vbHourglass
			//
			//    oDoc.Words.First.InsertBefore msSpell
			//
			//    iWSE = oDoc.SpellingErrors.Count
			//
			//    iWGE = oDoc.GrammaticalErrors.Count
			//
			//    If iWSE > 0 Or iWGE > 0 Then
			//
			//        moApp.WindowState = wdWindowStateMinimize
			//
			//        moApp.Visible = True
			//
			//        moApp.Activate
			//
			//        lResp = moApp.Dialogs(wdDialogToolsSpellingAndGrammar).Display
			//
			//        If lResp < 0 Then
			//
			//            moApp.Visible = False
			//
			//            Clipboard.Clear
			//
			//            oDoc.Select
			//
			//            oDoc.Range.Copy
			//
			//            sReplace = Clipboard.GetText(1)
			//
			//            SpellMe = sReplace
			//
			//        ElseIf lResp = 0 Then
			//
			//            SpellMe = msSpell
			//
			//        End If
			//
			//    Else
			//
			//        MsgBox "No se encontraron errores", vbOKOnly + vbInformation
			//
			//        SpellMe = msSpell
			//
			//    End If
			//
			//    oDoc.Close False
			//
			//    moApp.Visible = False
			//
			//    Set oDoc = Nothing
			//
			//    moApp.Quit False
			//
			//    Set moApp = Nothing
			//
			//    Screen.MousePointer = vbNormal
			//
			//    Exit Function
			//
			//No_Bugs:
			//
			//    If Err.Number = 91 Then
			//
			//        Resume Next
			//
			//    ElseIf Err.Number = 462 Then
			//
			//        Screen.MousePointer = vbNormal
			//
			//    ElseIf Err.Number = 429 Then
			//
			//        Set moApp = Nothing
			//
			//        Resume Next
			//
			//    End If
			//
			//    ' Si hemos obtenido alg�n error, para evitar devolver un texto vac�o y que el usuario pierda el texto
			//    ' sobre el que estuviera intentando comprobar la ortograf�a, devolvemos el texto original
			//
			//    MsgBox "Se encontr� un error ejecutando el corrector ortogr�fico", vbInformation + vbOKOnly
			//
			//    Screen.MousePointer = vbNormal
			//
			//    If Trim(SpellMe) = "" Then SpellMe = msSpell

			return String.Empty;
		}
	}
}