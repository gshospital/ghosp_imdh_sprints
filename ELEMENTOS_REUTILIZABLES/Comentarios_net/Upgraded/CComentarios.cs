using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using UpgradeHelpers.DB.DAO;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Microsoft.CSharp;

namespace Comentarios
{
	public class Class1
	{

		bool bCabecera = false;
		int resultado = 0;

		public void proRecogeDatos(dynamic Ventana, SqlConnection Conexion, dynamic CRis, bool bConsulta, string stPlantilla, string stArchivo, string stNombreVentana, bool bfuentes, bool bImprime, bool bDevolverContenido, string RutaAplicacion, object matrizplantillas_optional, object usuarioconectado_optional, bool InformeRayos, string[] matrizAdendas, bool imprimirAdendas, string dFechaValoracion)
		{
			string[] matrizplantillas = (matrizplantillas_optional == Type.Missing) ? null : matrizplantillas_optional as string[];
			string usuarioconectado = (usuarioconectado_optional == Type.Missing) ? String.Empty : usuarioconectado_optional as string;

			string sql = String.Empty;
			DataSet RrSql = null;
			string[] matriz = null;
			int tam = 0; 

			bComentario.ObtenerLocale();
			bComentario.vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(Conexion);
			bComentario.vstSeparadorBD = Serrores.ObtenerSeparadorDecimalBD(Conexion);
			Serrores.Datos_Conexion(Conexion);
			Serrores.AjustarRelojCliente(Conexion);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);

			string vTextDoc = "";
			bComentario.DatosPlantilla = "";
			string stFuenteDoc = "Arial";
			bool bNegritaDoc = false;
			bool bCursivaDoc = false;
			bool bSubrayadoDoc = false;
			double dSizeDoc = 9;
			if (matrizplantillas_optional == Type.Missing)
			{
				matriz = new string[]{String.Empty, String.Empty};
				matriz[1] = stPlantilla;
			}
			else
			{
				matriz = matrizplantillas;
			}

			if (InformeRayos)
			{
				// Si ya ten�a datos, lo que hacemos es a�adir a lo que ya tengamos

				sql = "select * from DOCUMENT where gnomdocu = '" + stArchivo + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{					
					vTextDoc = Convert.ToString(RrSql.Tables[0].Rows[0]["contdocu"]) + "";
					tam = vTextDoc.Length;
				}
			
				RrSql.Close();
			}
			else
			{
				tam = 0;
			}

			for (int X = 1; X <= matriz.GetUpperBound(0); X++)
			{
				stPlantilla = matriz[X];
				if ((stPlantilla.IndexOf("@#") + 1) == 0)
				{
					if (dFechaValoracion != "")
					{										
						sql = "select top 1 * from docummod where gnomdocu = '" + stArchivo + "' and fmodific >= " + Serrores.FormatFechaHMS(dFechaValoracion) + " order by fmodific asc";
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
						RrSql = new DataSet();
						tempAdapter_2.Fill(RrSql);
						if (RrSql.Tables[0].Rows.Count == 0)
						{
							sql = "select * from DOCUMENT where gnomdocu = '" + stArchivo + "'";
						}
						
						RrSql.Close();
					}
					else
					{
						sql = "select * from DOCUMENT where gnomdocu = '" + stArchivo + "'";
					}
					bComentario.HayPlantillas = false;
				}
				else
				{
					sql = "select ofichinf as contdocu from XINFPLAN where ginfplan = " + stPlantilla.Substring(0, Math.Min(stPlantilla.IndexOf("@#"), stPlantilla.Length));
					bComentario.HayPlantillas = true;
				}
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, Conexion);
				RrSql = new DataSet();
				tempAdapter_3.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count == 0)
				{
					if (bConsulta)
					{
						if (bDevolverContenido)
						{
                            Ventana.prorecogercomentario(false, "");
						}
						RrSql = null;
						frmNotePad.DefInstance = null;
						return;
					}
				}
				else
				{
					//Si ya existe en tabla pero el valor es nulo, al cargar la DLL, el dato se pierde si se pregunta si es null. Es una gracia del tipo de campo memo.
					if (vTextDoc == "")
					{
						ErrorHandlingHelper.ResumeNext(								
							() => {vTextDoc = Convert.ToString(RrSql.Tables[0].Rows[0]["contdocu"]) + "";}, 								
							() => {bComentario.DatosPlantilla = Convert.ToString(RrSql.Tables[0].Rows[0]["contdocu"]) + "";});
					}
					else
					{
						if (InformeRayos)
						{
							if (bComentario.HayPlantillas)
							{
								try
								{								
									vTextDoc = vTextDoc + "\n" + "\n" + Convert.ToString(RrSql.Tables[0].Rows[0]["contdocu"]);
								}
								catch
								{
								}
							}
						}
						else
						{
							try
							{								
								vTextDoc = vTextDoc + "\n" + "\n" + Convert.ToString(RrSql.Tables[0].Rows[0]["contdocu"]);
							}
							catch
							{
							}
						}
					}
					if ((stPlantilla.IndexOf("@#") + 1) == 0)
					{					
						stFuenteDoc = Convert.ToString(RrSql.Tables[0].Rows[0]["fontname"]);					
						bNegritaDoc = Convert.ToBoolean(RrSql.Tables[0].Rows[0]["fontbold"]);					
						bCursivaDoc = Convert.ToBoolean(RrSql.Tables[0].Rows[0]["Fontital"]);					
						bSubrayadoDoc = Convert.ToBoolean(RrSql.Tables[0].Rows[0]["fontline"]);					
						dSizeDoc = Double.Parse(Convert.ToString(RrSql.Tables[0].Rows[0]["FontSize"]).Substring(0, Math.Min(Convert.ToString(RrSql.Tables[0].Rows[0]["FontSize"]).IndexOf(bComentario.vstSeparadorBD), Convert.ToString(RrSql.Tables[0].Rows[0]["FontSize"]).Length)));
					}
				}
			}
			if (usuarioconectado_optional != Type.Missing)
			{
				bComentario.gUsuarioConectado = usuarioconectado.Trim().ToUpper();
			}
			else
			{
				bComentario.gUsuarioConectado = Serrores.VVstUsuarioApli;
			}

			//OSCAR C Febrero 2009
			//Se anexan las Adendas
			if (imprimirAdendas)
			{
				vTextDoc = vTextDoc + "\n" + "\n" + "ADENDAS AL INFORME RADIOLOGICO";
				vTextDoc = vTextDoc + "\n" + "==============================";
				for (int X = 1; X <= matrizAdendas.GetUpperBound(0); X++)
				{
					if (matrizAdendas[X].Trim() != "")
					{
						sql = "select contdocu from DOCUMENT where gnomdocu = '" + matrizAdendas[X] + "'";
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion);
						RrSql = new DataSet();
						tempAdapter_4.Fill(RrSql);
						if (RrSql.Tables[0].Rows.Count != 0)
						{							
							vTextDoc = vTextDoc + "\n" + "\n" + Convert.ToString(RrSql.Tables[0].Rows[0]["contdocu"]);
						}
					
						RrSql.Close();
					}
				}
			}
			//----------------------

			if (bImprime)
			{
				if (bDevolverContenido)
				{
					if (RrSql.Tables[0].Rows.Count != 0)
					{
                        //If Rrsql.RowCount > 0 Then                      
                        Ventana.prorecogercomentario(true, vTextDoc);
                    }
                    else
					{                       
                        Ventana.prorecogercomentario(false, "");
					}
				}
				else
				{
					proImprimir(RutaAplicacion, Conexion, vTextDoc, stNombreVentana, CRis); // Llega que hay que imprimir
				}
			}
			else
			{
				if (tam > 0)
				{
					bComentario.DatosPlantilla = vTextDoc.Substring(tam);
				}

				frmNotePad.DefInstance.proRecoge(Ventana, Conexion, bConsulta, stArchivo, stNombreVentana, bfuentes, bDevolverContenido, vTextDoc, stFuenteDoc, bNegritaDoc, bCursivaDoc, bSubrayadoDoc, dSizeDoc);
				frmNotePad.DefInstance.ShowDialog();
			}
			
			RrSql.Close();
			frmNotePad.DefInstance = null;

		}

		public void proRecogeDatos(object Ventana, SqlConnection Conexion, object CRis, bool bConsulta, string stPlantilla, string stArchivo, string stNombreVentana, bool bfuentes, bool bImprime, bool bDevolverContenido, string RutaAplicacion, object matrizplantillas_optional, object usuarioconectado_optional, bool InformeRayos, string[] matrizAdendas, bool imprimirAdendas)
		{
			string tempRefParam = "";
			proRecogeDatos(Ventana, Conexion, CRis, bConsulta, stPlantilla, stArchivo, stNombreVentana, bfuentes, bImprime, bDevolverContenido, RutaAplicacion, matrizplantillas_optional, usuarioconectado_optional, InformeRayos, matrizAdendas, imprimirAdendas, tempRefParam);
		}

		public void proRecogeDatos(object Ventana, SqlConnection Conexion, object CRis, bool bConsulta, string stPlantilla, string stArchivo, string stNombreVentana, bool bfuentes, bool bImprime, bool bDevolverContenido, string RutaAplicacion, object matrizplantillas_optional, object usuarioconectado_optional, bool InformeRayos, string[] matrizAdendas)
		{
			string tempRefParam2 = "";
			proRecogeDatos(Ventana, Conexion, CRis, bConsulta, stPlantilla, stArchivo, stNombreVentana, bfuentes, bImprime, bDevolverContenido, RutaAplicacion, matrizplantillas_optional, usuarioconectado_optional, InformeRayos, matrizAdendas, false, tempRefParam2);
		}

		public void proRecogeDatos(object Ventana, SqlConnection Conexion, object CRis, bool bConsulta, string stPlantilla, string stArchivo, string stNombreVentana, bool bfuentes, bool bImprime, bool bDevolverContenido, string RutaAplicacion, object matrizplantillas_optional, object usuarioconectado_optional, bool InformeRayos)
		{
			string tempRefParam3 = "";
			proRecogeDatos(Ventana, Conexion, CRis, bConsulta, stPlantilla, stArchivo, stNombreVentana, bfuentes, bImprime, bDevolverContenido, RutaAplicacion, matrizplantillas_optional, usuarioconectado_optional, InformeRayos, null, false, tempRefParam3);
		}

		public void proRecogeDatos(object Ventana, SqlConnection Conexion, object CRis, bool bConsulta, string stPlantilla, string stArchivo, string stNombreVentana, bool bfuentes, bool bImprime, bool bDevolverContenido, string RutaAplicacion, object matrizplantillas_optional, object usuarioconectado_optional)
		{
			string tempRefParam4 = "";
			proRecogeDatos(Ventana, Conexion, CRis, bConsulta, stPlantilla, stArchivo, stNombreVentana, bfuentes, bImprime, bDevolverContenido, RutaAplicacion, matrizplantillas_optional, usuarioconectado_optional, false, null, false, tempRefParam4);
		}

		public void proRecogeDatos(object Ventana, SqlConnection Conexion, object CRis, bool bConsulta, string stPlantilla, string stArchivo, string stNombreVentana, bool bfuentes, bool bImprime, bool bDevolverContenido, string RutaAplicacion, object matrizplantillas_optional)
		{
			string tempRefParam5 = "";
			proRecogeDatos(Ventana, Conexion, CRis, bConsulta, stPlantilla, stArchivo, stNombreVentana, bfuentes, bImprime, bDevolverContenido, RutaAplicacion, matrizplantillas_optional, Type.Missing, false, null, false, tempRefParam5);
		}

		public void proRecogeDatos(object Ventana, SqlConnection Conexion, object CRis, bool bConsulta, string stPlantilla, string stArchivo, string stNombreVentana, bool bfuentes, bool bImprime, bool bDevolverContenido, string RutaAplicacion)
		{
			string tempRefParam6 = "";
			proRecogeDatos(Ventana, Conexion, CRis, bConsulta, stPlantilla, stArchivo, stNombreVentana, bfuentes, bImprime, bDevolverContenido, RutaAplicacion, Type.Missing, Type.Missing, false, null, false, tempRefParam6);
		}

		private void proImprimir(string stPathString, SqlConnection Conexion, string vTextDoc, string stCaptionVentana, dynamic CRis)
		{
			WorkspaceHelper WkTemporal = null;
			DAODatabaseHelper BaseTemporal = null;
			DAORecordSetHelper RsTemporal = null;
			string stBaseDatos = String.Empty;
			string stNombreHo = String.Empty;
			string stGrupoHo = String.Empty;
			string stDireccionHo = String.Empty;
			string StSql = String.Empty;

			try
			{
				// No se a creado la base de datos
				stBaseDatos = stPathString + "\\comentarios.mdb";
				//Kill (stBaseDatos)
				if (FileSystem.Dir(stBaseDatos, FileAttribute.Normal) == "")
				{
					// No existe la Base de Datos la crea
					WkTemporal = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0];
					
					//camaya todo_x_5
                    //BaseTemporal = WkTemporal.CreateDatabase(stBaseDatos, DAO.LanguageConstants.dbLangSpanish, null);
					DbCommand TempCommand = BaseTemporal.Connection.CreateCommand();
					TempCommand.CommandText = "CREATE TABLE TABLA1 (Comentario text, NombreHo text, " + " GrupoHo text, DireccionHo text, Titulo text);";
					TempCommand.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(BaseTemporal.Connection);
					TempCommand.ExecuteNonQuery();
				}
				else
				{
					WkTemporal = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0];					
					BaseTemporal = WkTemporal.OpenDatabase("<Connection-String>");
					DbCommand TempCommand_2 = BaseTemporal.Connection.CreateCommand();
					TempCommand_2.CommandText = "DROP TABLE TABLA1";
					TempCommand_2.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(BaseTemporal.Connection);
					TempCommand_2.ExecuteNonQuery();
					DbCommand TempCommand_3 = BaseTemporal.Connection.CreateCommand();
					TempCommand_3.CommandText = "CREATE TABLE TABLA1 (Comentario memo, NombreHo text, " + " GrupoHo text, DireccionHo text, Titulo text);";
					TempCommand_3.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(BaseTemporal.Connection);
					TempCommand_3.ExecuteNonQuery();
				}
				StSql = " SELECT * FROM TABLA1";
				RsTemporal = BaseTemporal.OpenRecordset(StSql);
				// Veo si tengo que hacer la consulta de las cabeceras
				if (!bCabecera)
				{
					proCabecera(Conexion, ref stNombreHo, ref stGrupoHo, ref stDireccionHo);
				}
				RsTemporal.AddNew();
				RsTemporal["comentario"] = vTextDoc.Trim();
				RsTemporal["nombreho"] = stNombreHo;
				RsTemporal["grupoho"] = stGrupoHo;
				RsTemporal["direccionho"] = stDireccionHo;
				RsTemporal["titulo"] = stCaptionVentana.ToUpper();
				RsTemporal.Update();
				RsTemporal.Close();

                //camaya todo_x_5
                //CRis.Destination = (int) Crystal.DestinationConstants.crptToWindow;				
                //CRis.ReportFileName = "" + stPathString + "\\..\\ElementosComunes\\rpt\\COMENTARIOS.rpt";				
				//CRis.WindowTitle = stCaptionVentana;        
               //CRis.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;                
               //CRis.DataFiles[0] = "" + stPathString + "\\comentarios.mdb";				
				//CRis.Action = 1;
			}
			catch
			{
			}
		}
		private void proCabecera(SqlConnection Conexion, ref string stNombreHo, ref string stGrupoHo, ref string stDireccionHo)
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************
			string stCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stCab, Conexion);
			DataSet RrCrystal = new DataSet();
			tempAdapter.Fill(RrCrystal);			
			stGrupoHo = Convert.ToString(RrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();

            stCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stCab, Conexion);
			RrCrystal = new DataSet();
			tempAdapter_2.Fill(RrCrystal);			
			stNombreHo = Convert.ToString(RrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();

            stCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stCab, Conexion);
			RrCrystal = new DataSet();
			tempAdapter_3.Fill(RrCrystal);		
			stDireccionHo = Convert.ToString(RrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();		
			RrCrystal.Close();
			bCabecera = true;
		}

		public string BuscarUsuarioConectado(SqlConnection Conexion)
		{
			return String.Empty;
		}
	}
}