using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;

namespace Comentarios
{
	partial class frmNotePad
	{

		#region "Upgrade Support "
		private static frmNotePad m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmNotePad DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmNotePad();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "mnuFileSave", "mnuFSep", "mnuFileExit", "mnuFile", "mnuEditCut", "mnuEditCopy", "mnuEditPaste", "mnuEditDelete", "mnuESep1", "mnuEditSelectAll", "mnuEditTime", "mnuESep2", "mnuEditBuscar", "mnuEditSiguiente", "mnuEdit", "mnuFuentes", "mnuOpciones", "mnuCorrector", "mnuHerramientas", "MainMenu1", "_Toolbar1_Button1", "_Toolbar1_Button2", "_Toolbar1_Button3", "_Toolbar1_Button4", "_Toolbar1_Button5", "_Toolbar1_Button6", "_Toolbar1_Button7", "_Toolbar1_Button8", "_Toolbar1_Button9", "_Toolbar1_Button10", "_Toolbar1_Button11", "_Toolbar1_Button12", "Toolbar1", "txtLog", "txtContenido", "CD1Font", "ImageList1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadMenuItem mnuFileSave;
		public Telerik.WinControls.UI.RadMenuSeparatorItem mnuFSep;
		public Telerik.WinControls.UI.RadMenuItem mnuFileExit;
		public Telerik.WinControls.UI.RadMenuItem mnuFile;
		public Telerik.WinControls.UI.RadMenuItem mnuEditCut;
		public Telerik.WinControls.UI.RadMenuItem mnuEditCopy;
		public Telerik.WinControls.UI.RadMenuItem mnuEditPaste;
		public Telerik.WinControls.UI.RadMenuItem mnuEditDelete;
		public Telerik.WinControls.UI.RadMenuSeparatorItem mnuESep1;
		public Telerik.WinControls.UI.RadMenuItem mnuEditSelectAll;
		public Telerik.WinControls.UI.RadMenuItem mnuEditTime;
		public Telerik.WinControls.UI.RadMenuSeparatorItem mnuESep2;
		public Telerik.WinControls.UI.RadMenuItem mnuEditBuscar;
		public Telerik.WinControls.UI.RadMenuItem mnuEditSiguiente;
		public Telerik.WinControls.UI.RadMenuItem mnuEdit;
		public Telerik.WinControls.UI.RadMenuItem mnuFuentes;
		public Telerik.WinControls.UI.RadMenuItem mnuOpciones;
		public Telerik.WinControls.UI.RadMenuItem mnuCorrector;
		public Telerik.WinControls.UI.RadMenuItem mnuHerramientas;
		public Telerik.WinControls.UI.RadMenu MainMenu1;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button1;
		private Telerik.WinControls.UI.RadMenuSeparatorItem _Toolbar1_Button2;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button3;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button4;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button5;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button6;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button7;
		private Telerik.WinControls.UI.RadMenuSeparatorItem _Toolbar1_Button8;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button9;
		private Telerik.WinControls.UI.RadMenuSeparatorItem _Toolbar1_Button10;
		private Telerik.WinControls.UI.RadMenuSeparatorItem _Toolbar1_Button11;
		private Telerik.WinControls.UI.CommandBarButton _Toolbar1_Button12;
		public Telerik.WinControls.UI.RadCommandBar Toolbar1;
		public Telerik.WinControls.UI.RadTextBoxControl txtLog;
		public Telerik.WinControls.UI.RadRichTextEditor txtContenido;
		public System.Windows.Forms.FontDialog CD1Font;
		public System.Windows.Forms.ImageList ImageList1;
        private Telerik.WinControls.UI.CommandBarRowElement radCommandBarLineElement1;
        private Telerik.WinControls.UI.CommandBarStripElement radCommandBarStripElement1;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNotePad));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.MainMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.mnuFile = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuFileSave = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuFSep = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuFileExit = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuEdit = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuEditCut = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuEditCopy = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuEditPaste = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuEditDelete = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuESep1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuEditSelectAll = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuEditTime = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuESep2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.mnuEditBuscar = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuEditSiguiente = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuOpciones = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuFuentes = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuHerramientas = new Telerik.WinControls.UI.RadMenuItem();
            this.mnuCorrector = new Telerik.WinControls.UI.RadMenuItem();
            this.Toolbar1 = new Telerik.WinControls.UI.RadCommandBar();
            this._Toolbar1_Button1 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this._Toolbar1_Button3 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button4 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button5 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button6 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button7 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button8 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this._Toolbar1_Button9 = new Telerik.WinControls.UI.CommandBarButton();
            this._Toolbar1_Button10 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this._Toolbar1_Button11 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this._Toolbar1_Button12 = new Telerik.WinControls.UI.CommandBarButton();
            this.radCommandBarLineElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.radCommandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.txtLog = new Telerik.WinControls.UI.RadTextBoxControl();
            this.txtContenido = new Telerik.WinControls.UI.RadRichTextEditor();
            this.CD1Font = new System.Windows.Forms.FontDialog();
            this.ImageList1 = new System.Windows.Forms.ImageList();
            this.Toolbar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu1
            // 
            this.MainMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] { this.mnuFile, this.mnuEdit, this.mnuOpciones, this.mnuHerramientas });
            // 
            // mnuFile
            // 		
            this.mnuFile.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuFile.IsChecked = false;
            this.mnuFile.Enabled = true;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "&Archivo";
            this.mnuFile.Items.AddRange(new Telerik.WinControls.RadItem[] { this.mnuFileSave, this.mnuFSep, this.mnuFileExit });
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuFileSave.IsChecked = false;
            this.mnuFileSave.Enabled = true;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Text = "&Guardar";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFSep
            //
            this.mnuFSep.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuFSep.Enabled = true;
            this.mnuFSep.Name = "mnuFSep";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuFileExit.IsChecked = false;
            this.mnuFileExit.Enabled = true;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "&Salir";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // mnuEdit
            // 		
            this.mnuEdit.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuEdit.IsChecked = false;
            this.mnuEdit.Enabled = true;
            this.mnuEdit.Name = "mnuEdit";
            this.mnuEdit.Text = "&Edici�n";
            this.mnuEdit.Items.AddRange(new Telerik.WinControls.RadItem[] { this.mnuEditCut, this.mnuEditCopy, this.mnuEditPaste, this.mnuEditDelete, this.mnuESep1, this.mnuEditSelectAll, this.mnuEditTime, this.mnuESep2, this.mnuEditBuscar, this.mnuEditSiguiente });
            // 
            // mnuEditCut
            // 	
            this.mnuEditCut.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuEditCut.IsChecked = false;
            this.mnuEditCut.Enabled = true;
            this.mnuEditCut.Name = "mnuEditCut";
            this.mnuEditCut.Text = "C&ortar";
            this.mnuEditCut.Click += new System.EventHandler(this.mnuEditCut_Click);
            // 
            // mnuEditCopy
            // 	
            this.mnuEditCopy.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuEditCopy.IsChecked = false;
            this.mnuEditCopy.Enabled = true;
            this.mnuEditCopy.Name = "mnuEditCopy";
            this.mnuEditCopy.Text = "&Copiar";
            this.mnuEditCopy.Click += new System.EventHandler(this.mnuEditCopy_Click);
            // 
            // mnuEditPaste
            // 	
            this.mnuEditPaste.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuEditPaste.IsChecked = false;
			this.mnuEditPaste.Enabled = true;
			this.mnuEditPaste.Name = "mnuEditPaste";
			this.mnuEditPaste.Text = "&Pegar";
			this.mnuEditPaste.Click += new System.EventHandler(this.mnuEditPaste_Click);
            // 
            // mnuEditDelete
            // 	
            this.mnuEditDelete.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuEditDelete.IsChecked = false;
			this.mnuEditDelete.Enabled = true;
			this.mnuEditDelete.Name = "mnuEditDelete";
			//this.mnuEditDelete.ShortcutKeys = (System.Windows.Forms.Keys) System.Windows.Forms.Keys.Delete;
			this.mnuEditDelete.Text = "Eli&minar";
			this.mnuEditDelete.Click += new System.EventHandler(this.mnuEditDelete_Click);
            // 
            // mnuESep1
            // 	
            this.mnuESep1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuESep1.Enabled = true;
			this.mnuESep1.Name = "mnuESep1";
            // 
            // mnuEditSelectAll
            // 
            this.mnuEditSelectAll.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuEditSelectAll.IsChecked = false;
			this.mnuEditSelectAll.Enabled = true;
			this.mnuEditSelectAll.Name = "mnuEditSelectAll";
			this.mnuEditSelectAll.Text = "Seleccionar &todo";
			this.mnuEditSelectAll.Click += new System.EventHandler(this.mnuEditSelectAll_Click);
            // 
            // mnuEditTime
            // 	
            this.mnuEditTime.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuEditTime.IsChecked = false;
			this.mnuEditTime.Enabled = true;
			this.mnuEditTime.Name = "mnuEditTime";
			this.mnuEditTime.Text = "&Hora/Fecha";
			this.mnuEditTime.Click += new System.EventHandler(this.mnuEditTime_Click);
            // 
            // mnuESep2
            // 	
            this.mnuESep2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuESep2.Enabled = true;
			this.mnuESep2.Name = "mnuESep2";
            // 
            // mnuEditBuscar
            // 
            this.mnuEditBuscar.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuEditBuscar.IsChecked = false;
			this.mnuEditBuscar.Enabled = true;
			this.mnuEditBuscar.Name = "mnuEditBuscar";
			this.mnuEditBuscar.Text = "&Buscar";
			this.mnuEditBuscar.Click += new System.EventHandler(this.mnuEditBuscar_Click);
            // 
            // mnuEditSiguiente
            // 	
            this.mnuEditSiguiente.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuEditSiguiente.IsChecked = false;
			this.mnuEditSiguiente.Enabled = true;
			this.mnuEditSiguiente.Name = "mnuEditSiguiente";
			//this.mnuEditSiguiente.ShortcutKeys = (System.Windows.Forms.Keys) System.Windows.Forms.Keys.F3;
			this.mnuEditSiguiente.Text = "Buscar &Siguiente";
			this.mnuEditSiguiente.Click += new System.EventHandler(this.mnuEditSiguiente_Click);
            // 
            // mnuOpciones
            //
            this.mnuOpciones.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuOpciones.IsChecked = false;
			this.mnuOpciones.Enabled = true;
			this.mnuOpciones.Name = "mnuOpciones";
			this.mnuOpciones.Text = "&Opciones";
			this.mnuOpciones.Items.AddRange(new Telerik.WinControls.RadItem[] { this.mnuFuentes});
            // 
            // mnuFuentes
            //	
            this.mnuFuentes.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuFuentes.IsChecked = false;
			this.mnuFuentes.Enabled = true;
			this.mnuFuentes.Name = "mnuFuentes";
			this.mnuFuentes.Text = "&Fuentes";
			this.mnuFuentes.Click += new System.EventHandler(this.mnuFuentes_Click);
            // 
            // mnuHerramientas
            // 		
            this.mnuHerramientas.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuHerramientas.IsChecked = false;
			this.mnuHerramientas.Enabled = true;
			this.mnuHerramientas.Name = "mnuHerramientas";
			this.mnuHerramientas.Text = "&Herramientas";
			this.mnuHerramientas.Items.AddRange(new Telerik.WinControls.RadItem[] { this.mnuCorrector});
            // 
            // mnuCorrector
            // 	
            this.mnuCorrector.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mnuCorrector.IsChecked = false;
			this.mnuCorrector.Enabled = true;
			this.mnuCorrector.Name = "mnuCorrector";
			this.mnuCorrector.Text = "Corrector ortogr�fico";
			this.mnuCorrector.Click += new System.EventHandler(this.mnuCorrector_Click);
			// 
			// Toolbar1
			// 			
			this.Toolbar1.Dock = System.Windows.Forms.DockStyle.Top;
			this.Toolbar1.ImageList = ImageList1;
			this.Toolbar1.Location = new System.Drawing.Point(0, 24);
			this.Toolbar1.Name = "Toolbar1";
			//this.Toolbar1.ShowItemToolTips = true;
            this.Toolbar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.radCommandBarLineElement1});
            this.Toolbar1.Size = new System.Drawing.Size(539, 28);
			this.Toolbar1.TabIndex = 0;
            // 
            // radCommandBarLineElement1
            // 
            this.radCommandBarLineElement1.BorderLeftShadowColor = System.Drawing.Color.Empty;
            this.radCommandBarLineElement1.DisplayName = null;
            this.radCommandBarLineElement1.MinSize = new System.Drawing.Size(25, 25);
            this.radCommandBarLineElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.radCommandBarStripElement1});
            this.radCommandBarStripElement1.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.radCommandBarLineElement1.Text = "";
            // 
            // radCommandBarStripElement1
            // 
            this.radCommandBarStripElement1.DisplayName = "Commands Strip";
            this.radCommandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this._Toolbar1_Button1,
            this._Toolbar1_Button3,
            this._Toolbar1_Button4,
            this._Toolbar1_Button5,
            this._Toolbar1_Button6,
            this._Toolbar1_Button7,
            this._Toolbar1_Button9,
            this._Toolbar1_Button12});
            this.radCommandBarStripElement1.Name = "radCommandBarStripElement1";
            // 
            // _Toolbar1_Button1
            //           
			this._Toolbar1_Button1.ImageIndex = 0;
			//this._Toolbar1_Button1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._Toolbar1_Button1.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button1.Tag = "";
			this._Toolbar1_Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button1.ToolTipText = "Guardar";
			this._Toolbar1_Button1.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button2
			// 
			this._Toolbar1_Button2.Size = new System.Drawing.Size(6, 22);
			this._Toolbar1_Button2.Tag = "";
			this._Toolbar1_Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button2.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button3
			//			
			this._Toolbar1_Button3.ImageIndex = 2;
			//this._Toolbar1_Button3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._Toolbar1_Button3.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button3.Tag = "";
			this._Toolbar1_Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button3.ToolTipText = "Cortar";
			this._Toolbar1_Button3.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button4
			// 			
			this._Toolbar1_Button4.ImageIndex = 1;
			//this._Toolbar1_Button4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this._Toolbar1_Button4.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button4.Tag = "";
			this._Toolbar1_Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button4.ToolTipText = "Copiar";
			this._Toolbar1_Button4.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button5
			// 
			//this._Toolbar1_Button5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.ImageAndText;
			this._Toolbar1_Button5.ImageIndex = 3;			
			this._Toolbar1_Button5.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button5.Tag = "";
			this._Toolbar1_Button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button5.ToolTipText = "Pegar";
			this._Toolbar1_Button5.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button6
			//			
			this._Toolbar1_Button6.ImageIndex = 5;			
			this._Toolbar1_Button6.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button6.Tag = "";
			this._Toolbar1_Button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button6.ToolTipText = "Fuentes";
			this._Toolbar1_Button6.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button7
			//			
			this._Toolbar1_Button7.ImageIndex = 4;			
			this._Toolbar1_Button7.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button7.Tag = "";
			this._Toolbar1_Button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button7.ToolTipText = "Buscar";
			this._Toolbar1_Button7.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button8
			// 
			this._Toolbar1_Button8.Size = new System.Drawing.Size(6, 22);
			this._Toolbar1_Button8.Tag = "";
			this._Toolbar1_Button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button8.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button9
			// 			
			this._Toolbar1_Button9.ImageIndex = 11;			
			this._Toolbar1_Button9.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button9.Tag = "";
			this._Toolbar1_Button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button9.ToolTipText = "Corrector";
			this._Toolbar1_Button9.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button10
			// 
			this._Toolbar1_Button10.Size = new System.Drawing.Size(6, 22);
			this._Toolbar1_Button10.Tag = "";
			this._Toolbar1_Button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button10.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button11
			// 
			this._Toolbar1_Button11.Size = new System.Drawing.Size(6, 22);
			this._Toolbar1_Button11.Tag = "";
			this._Toolbar1_Button11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button11.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// _Toolbar1_Button12
			// 			
			this._Toolbar1_Button12.ImageIndex = 6;			
			this._Toolbar1_Button12.Size = new System.Drawing.Size(24, 22);
			this._Toolbar1_Button12.Tag = "";
			this._Toolbar1_Button12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this._Toolbar1_Button12.ToolTipText = "Salir";
			this._Toolbar1_Button12.Click += new System.EventHandler(this.Toolbar1_ButtonClick);
			// 
			// txtLog
			// 
			this.txtLog.AcceptsReturn = true;
			this.txtLog.BackColor = System.Drawing.SystemColors.Window;			
			this.txtLog.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtLog.ForeColor = System.Drawing.SystemColors.WindowText;
			this.txtLog.Location = new System.Drawing.Point(456, 360);
			this.txtLog.MaxLength = 0;
			this.txtLog.Multiline = true;
			this.txtLog.Name = "txtLog";
			this.txtLog.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtLog.Size = new System.Drawing.Size(81, 33);
			this.txtLog.TabIndex = 2;
			this.txtLog.Visible = false;
			// 
			// txtContenido
			// 			
			this.txtContenido.Font = new System.Drawing.Font("Courier New", 9f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.txtContenido.Location = new System.Drawing.Point(2, 54);
			this.txtContenido.Name = "txtContenido";
           // this.txtContenido.Rtf = resources.GetString("txtContenido.TextRTF");
            //this.txtContenido.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtContenido.RichTextBoxElement.VerticalScrollBarVisibility = Telerik.WinControls.RichTextEditor.UI.ScrollBarVisibility.Visible;
            this.txtContenido.Size = new System.Drawing.Size(301, 188);
			this.txtContenido.TabIndex = 1;
			this.txtContenido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContenido_KeyPress);
            this.txtContenido.IsSpellCheckingEnabled = true;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageSize = new System.Drawing.Size(16, 15);
			this.ImageList1.ImageStream = (System.Windows.Forms.ImageListStreamer) resources.GetObject("ImageList1.ImageStream");
			this.ImageList1.TransparentColor = System.Drawing.Color.Silver;
			this.ImageList1.Images.SetKeyName(0, "");
			this.ImageList1.Images.SetKeyName(1, "");
			this.ImageList1.Images.SetKeyName(2, "");
			this.ImageList1.Images.SetKeyName(3, "");
			this.ImageList1.Images.SetKeyName(4, "");
			this.ImageList1.Images.SetKeyName(5, "");
			this.ImageList1.Images.SetKeyName(6, "");
			this.ImageList1.Images.SetKeyName(7, "");
			this.ImageList1.Images.SetKeyName(8, "");
			this.ImageList1.Images.SetKeyName(9, "");
			this.ImageList1.Images.SetKeyName(10, "");
			this.ImageList1.Images.SetKeyName(11, "");
			// 
			// frmNotePad
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(0, 0);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 402);
			this.Controls.Add(this.Toolbar1);
			this.Controls.Add(this.txtLog);
			this.Controls.Add(this.txtContenido);
			this.Controls.Add(MainMenu1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.Location = new System.Drawing.Point(101, 221);
			this.MaximizeBox = true;
			this.MinimizeBox = true;
			this.Name = "frmNotePad";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Closed += new System.EventHandler(this.frmNotePad_Closed);
			this.Load += new System.EventHandler(this.frmNotePad_Load);
			this.Resize += new System.EventHandler(this.frmNotePad_Resize);
			this.Toolbar1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}