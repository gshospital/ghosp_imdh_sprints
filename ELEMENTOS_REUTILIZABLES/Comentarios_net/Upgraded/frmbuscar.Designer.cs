using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Comentarios
{
	partial class frmbuscar
	{

		#region "Upgrade Support "
		private static frmbuscar m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static frmbuscar DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new frmbuscar();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbBuscar", "_Restriccion_4", "_Restriccion_2", "cbCancelar", "txtbuscar", "lblBuscar", "Restriccion"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbBuscar;
		private Telerik.WinControls.UI.RadCheckBox _Restriccion_4;
		private Telerik.WinControls.UI.RadCheckBox _Restriccion_2;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadTextBoxControl txtbuscar;
		public Telerik.WinControls.UI.RadLabel lblBuscar;
		public Telerik.WinControls.UI.RadCheckBox[] Restriccion = new Telerik.WinControls.UI.RadCheckBox[5];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmbuscar));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.cbBuscar = new Telerik.WinControls.UI.RadButton();
			this._Restriccion_4 = new Telerik.WinControls.UI.RadCheckBox();
			this._Restriccion_2 = new Telerik.WinControls.UI.RadCheckBox();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.txtbuscar = new Telerik.WinControls.UI.RadTextBoxControl();
			this.lblBuscar = new Telerik.WinControls.UI.RadLabel();
			this.SuspendLayout();
			// 
			// cbBuscar
			// 			
			this.cbBuscar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cbBuscar.Location = new System.Drawing.Point(123, 112);
			this.cbBuscar.Name = "cbBuscar";
			this.cbBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbBuscar.Size = new System.Drawing.Size(85, 28);
			this.cbBuscar.TabIndex = 3;
			this.cbBuscar.Text = "&Buscar";			
			this.cbBuscar.Click += new System.EventHandler(this.cbBuscar_Click);
			// 
			// _Restriccion_4
			//			
			this._Restriccion_4.CausesValidation = true;			
			this._Restriccion_4.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._Restriccion_4.Cursor = System.Windows.Forms.Cursors.Default;
			this._Restriccion_4.Enabled = true;
			this._Restriccion_4.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Restriccion_4.Location = new System.Drawing.Point(7, 76);
			this._Restriccion_4.Name = "_Restriccion_4";
			this._Restriccion_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Restriccion_4.Size = new System.Drawing.Size(207, 24);
			this._Restriccion_4.TabIndex = 2;
			this._Restriccion_4.TabStop = true;
			this._Restriccion_4.Text = "Coincidir &may�sculas/min�sculas";
			this._Restriccion_4.Visible = true;
			this._Restriccion_4.CheckStateChanged += new System.EventHandler(this.Restriccion_CheckStateChanged);
			// 
			// _Restriccion_2
			// 			
			this._Restriccion_2.CausesValidation = true;			
			this._Restriccion_2.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this._Restriccion_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._Restriccion_2.Enabled = true;			
			this._Restriccion_2.Location = new System.Drawing.Point(7, 45);
			this._Restriccion_2.Name = "_Restriccion_2";
			this._Restriccion_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Restriccion_2.Size = new System.Drawing.Size(207, 24);
			this._Restriccion_2.TabIndex = 1;
			this._Restriccion_2.TabStop = true;
			this._Restriccion_2.Text = "Buscar &palabra completa";
			this._Restriccion_2.Visible = true;
			this._Restriccion_2.CheckStateChanged += new System.EventHandler(this.Restriccion_CheckStateChanged);
			// 
			// cbCancelar
			// 			
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cbCancelar.Location = new System.Drawing.Point(222, 112);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(85, 28);
			this.cbCancelar.TabIndex = 4;
			this.cbCancelar.Text = "&Cancelar";			
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// txtbuscar
			// 
			this.txtbuscar.AcceptsReturn = true;			
			this.txtbuscar.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtbuscar.ForeColor = System.Drawing.SystemColors.WindowText;
			this.txtbuscar.Location = new System.Drawing.Point(55, 11);
			this.txtbuscar.MaxLength = 0;
			this.txtbuscar.Name = "txtbuscar";
			this.txtbuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtbuscar.Size = new System.Drawing.Size(250, 26);
			this.txtbuscar.TabIndex = 0;
			this.txtbuscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbuscar_KeyPress);
			// 
			// lblBuscar
			// 			
			this.lblBuscar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.lblBuscar.Location = new System.Drawing.Point(7, 17);
			this.lblBuscar.Name = "lblBuscar";
			this.lblBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblBuscar.Size = new System.Drawing.Size(38, 14);
			this.lblBuscar.TabIndex = 5;
			this.lblBuscar.Text = "Buscar";
			// 
			// frmbuscar
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;			
			this.ClientSize = new System.Drawing.Size(322, 151);
			this.Controls.Add(this.cbBuscar);
			this.Controls.Add(this._Restriccion_4);
			this.Controls.Add(this._Restriccion_2);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.txtbuscar);
			this.Controls.Add(this.lblBuscar);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmbuscar";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "BUSCAR";
			this.Activated += new System.EventHandler(this.frmbuscar_Activated);
			this.Closed += new System.EventHandler(this.frmbuscar_Closed);
			this.Load += new System.EventHandler(this.frmbuscar_Load);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeRestriccion();
		}
		void InitializeRestriccion()
		{
			this.Restriccion = new Telerik.WinControls.UI.RadCheckBox[5];
			this.Restriccion[4] = _Restriccion_4;
			this.Restriccion[2] = _Restriccion_2;
		}
		#endregion
	}
}