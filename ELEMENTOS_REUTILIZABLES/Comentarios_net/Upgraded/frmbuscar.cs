using Microsoft.VisualBasic;
using System;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace Comentarios
{
	public partial class frmbuscar
		: Telerik.WinControls.UI.RadForm
	{

		public int iCond = 0;
		bool bFormVisible = false;
		public frmbuscar()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}

		public void cbBuscar_Click(Object eventSender, EventArgs eventArgs)
		{
			//Actualizamos el inicio de la b�squeda
			int iPosIni = frmNotePad.DefInstance.txtContenido.TabIndex;
			if (frmNotePad.DefInstance.txtContenido.Text != "")
			{
				iPosIni += Strings.Len(txtbuscar.Text);
			}
            // Busca el texto especificado en el control TextBox.
            int iPos = 0;//frmNotePad.DefInstance.txtContenido.Find(txtbuscar.Text, iPosIni, iCond);
			// Muestra un mensaje si ha encontrado el texto.

			if (iPos == -1)
			{
				if (RadMessageBox.Show("No aparecen coincidencias. �Desea empezar desde el principio?", "BUSCAR", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation) == System.Windows.Forms.DialogResult.Yes)
				{
					// Busca el texto especificado en el control TextBox.
                    //camaya todo_X_5
					//iPos = frmNotePad.DefInstance.txtContenido.Find(txtbuscar.Text, 0, iCond);
					if (iPos == -1)
					{
						RadMessageBox.Show("Palabra no encontrada.", "BUSCAR", MessageBoxButtons.OK, RadMessageIcon.Info);
					}
					else
					{
						frmNotePad.DefInstance.txtContenido.TabIndex = iPos;
                        //camaya todo_X_5
                        //frmNotePad.DefInstance.txtContenido.SelectionLength = Strings.Len(txtbuscar.Text);
                    }
                }
			}
			else
			{
				frmNotePad.DefInstance.txtContenido.TabIndex = iPos;
                //camaya todo_X_5
                //frmNotePad.DefInstance.txtContenido.SelectionLength = Strings.Len(txtbuscar.Text);
            }
            this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void frmbuscar_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				bFormVisible = true;
			}
		}
        		
		private void frmbuscar_Load(Object eventSender, EventArgs eventArgs)
		{
			bFormVisible = false;

			switch(iCond)
			{
				case 8 : 
					break;
				case 10 : case 12 : 
					Restriccion[iCond - 8].CheckState = CheckState.Checked; 
					break;
				case 14 : 
					Restriccion[2].CheckState = CheckState.Checked; 
					Restriccion[4].CheckState = CheckState.Checked; 
					break;
			}

		}

		private void Restriccion_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.Restriccion, eventSender);
			if (bFormVisible)
			{
				if (Restriccion[Index].CheckState != CheckState.Unchecked)
				{
					iCond += Index;
				}
				else
				{
					iCond -= Index;
				}
			}
		}

		private void txtbuscar_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void frmbuscar_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}