using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;

namespace BlackBoxWS
{
	public class clsBlackBox
	{
		public bool Activo
		{
			get
			{
				return basBlackBox.bBlackBoxActivo;
			}
		}

		public string Respuesta
		{
			get
			{
				return basBlackBox.oRespuesta.Respuesta;
			}
		}


		public string CodigoError
		{
			get
			{
				return basBlackBox.oRespuesta.CodigoError;
			}
		}

		public string DescripcionRespuesta
		{
			get
			{
				return basBlackBox.oRespuesta.DescripcionRespuesta;
			}
		}

		public string Gestor
		{
			get
			{
				return basBlackBox.oRespuesta.Gestor;
			}
		}

		public string Terminal
		{
			get
			{ // Sólo válido para Autorización
				return basBlackBox.oRespuesta.Terminal;

			}
		}

		public string Timestamp
		{
			get
			{ // Sólo válido para Autorización
				return basBlackBox.oRespuesta.Timestamp;

			}
		}

		public string FechaAutorizacion
		{
			get
			{ // Sólo válido para Autorización
				return basBlackBox.oRespuesta.FechaAutorizacion;

			}
		}

		public string Referencia
		{
			get
			{ // Sólo válido para Autorización
				return basBlackBox.oRespuesta.Referencia;
			}
		}

		public string Nfactura
		{
			get
			{ // Sólo válido para Autorización
				return basBlackBox.oRespuesta.Nfactura;

			}
		}

		public bool AutorizacionManual
		{
			get
			{
				return basBlackBox.bAutorizacionManual;
			}
		}
		//---------------------------------------------------------------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------------------------------------

		public bool proIniciaBlackBox(SqlConnection pConexion)
		{
			// Guardamos las variables globales
			basBlackBox.cnConexion = pConexion;
			basBlackBox.bInicioCorrecto = false; // Valor inicial
			basBlackBox.bBlackBoxActivo = false; // Valor inicial
			basBlackBox.bAutorizacionManual = false; // Valor inicial

			// Obtenemos los datos correspondientes

			if (!basBlackBox.proObtenerParametros())
			{
				return false;
			}

			// Todo OK
			basBlackBox.bInicioCorrecto = true;
			return true;

		}

		public bool proSolicitarAutorizacion(int iSociedad, int iEspecialidad, int lMedico, string stGidenpac, string[] stPrestaciones)
		{
            if (!basBlackBox.bInicioCorrecto)
			{
                RadMessageBox.Show("Antes de realizar la autorización debe iniciarse la comunicación.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
				return false;
			}
			basBlackBox.stGidenpacPaciente = stGidenpac;

			return basBlackBox.bSolicitarAutorizacion(iSociedad, iEspecialidad, lMedico, stPrestaciones);

		}

		public bool proSolicitarAnulacion(string stGidenpac, string stTimeStamp)
		{
			if (!basBlackBox.bInicioCorrecto)
			{
				RadMessageBox.Show("Antes de realizar la autorización debe iniciarse la comunicación.", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
				return false;
			}
			basBlackBox.stGidenpacPaciente = stGidenpac;
			return basBlackBox.bSolicitarAnulacion(stTimeStamp);

		}
	}
}