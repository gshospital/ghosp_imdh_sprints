using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using System.Xml;

namespace BlackBoxWS
{
	internal static class basBlackBox
	{

		// Constantes

		private static readonly string stCabecera = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + 
		                                            "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + 
		                                            "<soap:Body>";

		private static readonly string stParametro = "<KeyValueOfstringstring xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">" + 
		                                             "<Key>CLAVE</Key><Value>VALOR</Value></KeyValueOfstringstring>";

		private const string stPrestacion = "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">PRESTACION</string>";

		// Estructuras

		public struct TipoRespuesta
		{
			public string MensajeCompleto;
			public string Respuesta;
			public string CodigoError;
			public string DescripcionRespuesta;
			public string Gestor;
			public string Terminal; // S�lo v�lido para Autorizaci�n
			public string Timestamp; // S�lo v�lido para Autorizaci�n
			public string FechaAutorizacion; // S�lo v�lido para Autorizaci�n
			public string Referencia; // S�lo v�lido para Autorizaci�n
			public string Nfactura; // S�lo v�lido para Autorizaci�n
			public static TipoRespuesta CreateInstance()
			{
					TipoRespuesta result = new TipoRespuesta();
					result.MensajeCompleto = String.Empty;
					result.Respuesta = String.Empty;
					result.CodigoError = String.Empty;
					result.DescripcionRespuesta = String.Empty;
					result.Gestor = String.Empty;
					result.Terminal = String.Empty;
					result.Timestamp = String.Empty;
					result.FechaAutorizacion = String.Empty;
					result.Referencia = String.Empty;
					result.Nfactura = String.Empty;
					return result;
			}
		}

		private struct TipoParametrizacion
		{
			public string URL;
			public string HOST;
			public string Centro;
			public string Terminal;
			public string AccionAutorizacion;
			public string AccionAnulacion;
			public static TipoParametrizacion CreateInstance()
			{
					TipoParametrizacion result = new TipoParametrizacion();
					result.URL = String.Empty;
					result.HOST = String.Empty;
					result.Centro = String.Empty;
					result.Terminal = String.Empty;
					result.AccionAutorizacion = String.Empty;
					result.AccionAnulacion = String.Empty;
					return result;
			}
		}

		// Variables globales

		public static SqlConnection cnConexion = null;
		public static bool bInicioCorrecto = false;
		public static bool bBlackBoxActivo = false;
		public static bool bAutorizacionManual = false;
		public static string stGidenpacPaciente = String.Empty;

		// Variables

		public static basBlackBox.TipoRespuesta oRespuesta = basBlackBox.TipoRespuesta.CreateInstance();
		private static basBlackBox.TipoParametrizacion oParametrizacion = basBlackBox.TipoParametrizacion.CreateInstance();
		private static string stHash = String.Empty;

		internal static bool bSolicitarAutorizacion(int iSociedad, int iEspecialidad, int lMedico, string[] stPrestaciones)
		{

			bool result = false;
			string stMensaje = String.Empty;
			string stSql = String.Empty;
			string stBandaTarjeta = String.Empty;
			string stNumeroTarjeta = String.Empty;
			DataSet RrDatos = null;

			try
			{

				// Tomamos un nuevo hash
				stHash = GET_HASH();

				// Obtenemos la banda de la tarjeta y el n�mero de asegurado
				stSql = "SELECT isNull(B.dbandtar, '') BANDA, isNull(B.nafiliac, '') NUMERO FROM " + 
				        "DPACIENT A " + 
				        "INNER JOIN DENTIPAC B ON " + 
				        "B.gidenpac = A.gidenpac AND " + 
				        "B.gsocieda = " + iSociedad.ToString() + 
				        " WHERE " + 
				        "A.gidenpac = '" + stGidenpacPaciente + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{					
					stBandaTarjeta = Convert.ToString(RrDatos.Tables[0].Rows[0]["BANDA"]).Trim();					
					stNumeroTarjeta = Convert.ToString(RrDatos.Tables[0].Rows[0]["NUMERO"]).Trim();
				}				
				RrDatos.Close();

				// Componemos el mensaje de solicitud
				string tempRefParam2 = stParametro;
				string tempRefParam3 = "CLAVE";
				string tempRefParam = Serrores.proReplace(ref tempRefParam2, tempRefParam3, "Centro");
				string tempRefParam4 = "VALOR";
				string tempRefParam6 = stParametro;
				string tempRefParam7 = "CLAVE";
				string tempRefParam5 = Serrores.proReplace(ref tempRefParam6, tempRefParam7, "Sociedad");
				string tempRefParam8 = "VALOR";
				string tempRefParam10 = stParametro;
				string tempRefParam11 = "CLAVE";
				string tempRefParam9 = Serrores.proReplace(ref tempRefParam10, tempRefParam11, "Especialidad");
				string tempRefParam12 = "VALOR";
				string tempRefParam14 = stParametro;
				string tempRefParam15 = "CLAVE";
				string tempRefParam13 = Serrores.proReplace(ref tempRefParam14, tempRefParam15, "Medico");
				string tempRefParam16 = "VALOR";
				stMensaje = stCabecera + 
				            "<SolicitarAutorizacion xmlns=\"http://BlackBox\">" + 
				            "<Parametros>" + 
				            Serrores.proReplace(ref tempRefParam, tempRefParam4, "" + oParametrizacion.Centro) + 
				            Serrores.proReplace(ref tempRefParam5, tempRefParam8, "" + iSociedad.ToString()) + 
				            Serrores.proReplace(ref tempRefParam9, tempRefParam12, "" + iEspecialidad.ToString()) + 
				            Serrores.proReplace(ref tempRefParam13, tempRefParam16, "" + lMedico.ToString());

				//                    proReplace(proReplace(stParametro, "CLAVE", "Terminal"), "VALOR", oParametrizacion.Terminal)

				string tempRefParam18 = stParametro;
				string tempRefParam19 = "CLAVE";
				string tempRefParam17 = Serrores.proReplace(ref tempRefParam18, tempRefParam19, "BandaTarjeta");
				string tempRefParam20 = "VALOR";
				string tempRefParam22 = stParametro;
				string tempRefParam23 = "CLAVE";
				string tempRefParam21 = Serrores.proReplace(ref tempRefParam22, tempRefParam23, "NumeroTarjeta");
				string tempRefParam24 = "VALOR";
				stMensaje = stMensaje + 
				            Serrores.proReplace(ref tempRefParam17, tempRefParam20, stBandaTarjeta) + 
				            Serrores.proReplace(ref tempRefParam21, tempRefParam24, stNumeroTarjeta) + 
				            "</Parametros>";

				// Cargamos las prestaciones

				stMensaje = stMensaje + 
				            "<Prestaciones>";

				foreach (string stPrestaciones_item in stPrestaciones)
				{
					string tempRefParam25 = stPrestacion;
					string tempRefParam26 = "PRESTACION";
					stMensaje = stMensaje + 
					            Serrores.proReplace(ref tempRefParam25, tempRefParam26, stPrestaciones_item);
				}

				stMensaje = stMensaje + 
				            "</Prestaciones>";

				// Cerramos el mensaje
				stMensaje = stMensaje + "</SolicitarAutorizacion></soap:Body></soap:Envelope>";

				// Guardamos la informaci�n en el Log
				proGuardaLog("AU", "S", ref stMensaje, iSociedad, iEspecialidad, lMedico, stBandaTarjeta, stNumeroTarjeta);

				// Hacemos el env�o
				result = proHacerEnvio(stMensaje, oParametrizacion.AccionAutorizacion, "SolicitarAutorizacionResult");

				// Guardamos la respuesta en el log
				proGuardaLog("AU", "E", ref oRespuesta.MensajeCompleto, -1, -1, -1, "", "", oRespuesta.Respuesta, oRespuesta.CodigoError, ref oRespuesta.DescripcionRespuesta, oRespuesta.Gestor, oRespuesta.Timestamp, oRespuesta.FechaAutorizacion, oRespuesta.Referencia, oRespuesta.Nfactura, oRespuesta.Terminal);

				return result;
			}
			catch (System.Exception excep)
			{

				//MsgBox "Error bSolicitarAutorizacion: " & Err.Description
				oRespuesta.CodigoError = "ERR";
				oRespuesta.DescripcionRespuesta = "Error bSolicitarAutorizacion: " + excep.Message;
				return false;
			}
		}

		internal static bool bSolicitarAnulacion(string stTimeStamp)
		{
			bool result = false;
			string stMensaje = String.Empty;
			string stTerminal = String.Empty;
			string stSql = String.Empty;
			DataSet RrDatos = null;

			try
			{
				// Tomamos un nuevo hash
				stHash = GET_HASH();

				// Tomamos el terminal que se us� en la llamada
				stSql = "SELECT terminal FROM DLOGBLBX WHERE " + 
				        "oTimeStamp = '" + stTimeStamp + "' AND " + 
				        "Tipo = 'AU' AND " + 
				        "Direccion = 'E' AND " + 
				        "terminal IS NOT NULL";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					stTerminal = oParametrizacion.Terminal;
				}
				else
				{					
					stTerminal = ("" + Convert.ToString(RrDatos.Tables[0].Rows[0]["terminal"])).Trim();
				}
				
				RrDatos.Close();

				// Componemos el mensaje de solicitud
				stMensaje = stCabecera + 
				            "<SolicitarAnulacion xmlns=\"http://BlackBox\">" + 
				            "<Centro>" + oParametrizacion.Centro + "</Centro>" + 
				            "<Terminal>" + stTerminal + "</Terminal>" + 
				            "<Timestamp>" + stTimeStamp + "</Timestamp>" + 
				            "</SolicitarAnulacion>";

				// Cerramos el mensaje
				stMensaje = stMensaje + "</soap:Body></soap:Envelope>";

				string tempRefParam = "";
				proGuardaLog("AN", "S", ref stMensaje, -1, -1, -1, "", "", "", "", ref tempRefParam, "", stTimeStamp);

				// Hacemos el env�o
				result = proHacerEnvio(stMensaje, oParametrizacion.AccionAnulacion, "SolicitarAnulacionResult");

				// Guardamos la respuesta en el log
				proGuardaLog("AN", "E", ref oRespuesta.MensajeCompleto, -1, -1, -1, "", "", oRespuesta.Respuesta, oRespuesta.CodigoError, ref oRespuesta.DescripcionRespuesta, oRespuesta.Gestor, oRespuesta.Timestamp, oRespuesta.FechaAutorizacion, oRespuesta.Referencia, oRespuesta.Nfactura, oRespuesta.Terminal);

				return result;
			}
			catch (System.Exception excep)
			{

				//MsgBox "Error bSolicitarAnulacion: " & Err.Description
				oRespuesta.CodigoError = "ERR";
				oRespuesta.DescripcionRespuesta = "Error bSolicitarAnulacion: " + excep.Message;
				return false;
			}
		}

		private static bool proHacerEnvio(string stMensaje, string stAccion, string stTagResultados)
		{
			bool result = false;
			try
			{				
				MSXML2.ServerXMLHTTP oSOAPprov = null;
                XmlDocument xmlRespuesta = null;
                XmlNodeList nodoRespuesta = null;

				// Vaciamos la variable de respuestas
				proVaciaRespuesta();
				// Enviamos
				
				oSOAPprov = new MSXML2.ServerXMLHTTP();
				
				oSOAPprov.open("POST", oParametrizacion.URL, false, Type.Missing, Type.Missing);				
				oSOAPprov.setRequestHeader("Man", "POST " + oParametrizacion.URL + " HTTP/1.1");				
				oSOAPprov.setRequestHeader("Host", oParametrizacion.HOST.Trim());				
				oSOAPprov.setRequestHeader("Content-Type", "text/xml; charset=utf-8");				
				oSOAPprov.setRequestHeader("Content-length", stMensaje.Length.ToString());				
				oSOAPprov.setRequestHeader("SOAPAction", stAccion);                				
				oSOAPprov.send(stMensaje);

				
				if (oSOAPprov.status == 200)
				{					
                    xmlRespuesta = new XmlDocument();               
                                       
                    xmlRespuesta.LoadXml(oSOAPprov.responseText);
					
					oRespuesta.MensajeCompleto = oSOAPprov.responseText;
					
					nodoRespuesta = xmlRespuesta.GetElementsByTagName(stTagResultados);
					
					if (Convert.ToDouble(nodoRespuesta.Count) == 1)
					{						
						proGuardaRespuesta(nodoRespuesta.Item(0).ChildNodes);
						result = true;
					}
					else
					{
						//MsgBox "No se encontr� la respuesta esperada"
						oRespuesta.CodigoError = "ERR";
						oRespuesta.DescripcionRespuesta = "No se encontr� la respuesta esperada";
						result = false;
					}
				}
				else
				{
					//MsgBox "Error " & oSOAPprov.Status & ": " & oSOAPprov.statusText & " (" & oSOAPprov.responseText & ")"

					oRespuesta.CodigoError = "ERR";
					oRespuesta.DescripcionRespuesta = " Error " + oSOAPprov.status.ToString() + ": " + oSOAPprov.statusText + " (" + oSOAPprov.responseText + ")";
					oRespuesta.DescripcionRespuesta = oRespuesta.DescripcionRespuesta.Substring(0, Math.Min(200, oRespuesta.DescripcionRespuesta.Length));
					oRespuesta.DescripcionRespuesta = Serrores.Replace(oRespuesta.DescripcionRespuesta, "\"", "�");
					oRespuesta.DescripcionRespuesta = Serrores.Replace(oRespuesta.DescripcionRespuesta, "'", "�");

					result = false;

				}
				oSOAPprov = null;
				xmlRespuesta = null;
				nodoRespuesta = null;

				return result;
			}
			catch (System.Exception excep)
			{
				//MsgBox "Error proHacerEnvio: " & Err.Description
				oRespuesta.CodigoError = "ERR";
				oRespuesta.DescripcionRespuesta = "Error proHacerEnvio: " + excep.Message;
				return false;
			}
		}

		private static void proGuardaRespuesta(XmlNodeList oNodo)
		{			
			for (int i = 0; i <= Convert.ToInt32(Convert.ToDouble(oNodo.Count) - 1); i++)
			{				
				switch(Convert.ToString(oNodo.Item(i).Name).ToUpper())
				{
					case "RESPUESTA" : 						
						oRespuesta.Respuesta = Convert.ToString(oNodo.Item(i).Value); 
						break;
					case "CODIGOERROR" : 						
						oRespuesta.CodigoError = Convert.ToString(oNodo.Item(i).Value); 
						break;
					case "DESCRIPCIONRESPUESTA" :						
						oRespuesta.DescripcionRespuesta = Convert.ToString(oNodo.Item(i).Value); 
						break;
					case "GESTOR" : 						
						oRespuesta.Gestor = Convert.ToString(oNodo.Item(i).InnerText); 
						break;
					case "TERMINAL" : 						
						oRespuesta.Terminal = Convert.ToString(oNodo.Item(i).Value); 
						break;
					case "TIMESTAMP" : 						
						oRespuesta.Timestamp = Convert.ToString(oNodo.Item(i).Value); 
						break;
					case "FECHAAUTORIZACION" : 						
						oRespuesta.FechaAutorizacion = Convert.ToString(oNodo.Item(i).Value); 
						break;
					case "REFERENCIA" : 						
						oRespuesta.Referencia = Convert.ToString(oNodo.Item(i).Value); 
						break;
					case "NFACTURA" : 						
						oRespuesta.Nfactura = Convert.ToString(oNodo.Item(i).Value); 
						break;
				}

			}

		}

		private static void proVaciaRespuesta()
		{
			oRespuesta.Respuesta = "";
			oRespuesta.CodigoError = "";
			oRespuesta.DescripcionRespuesta = "";
			oRespuesta.Gestor = "";
			oRespuesta.Terminal = "";
			oRespuesta.Timestamp = "";
			oRespuesta.FechaAutorizacion = "";
			oRespuesta.Referencia = "";
			oRespuesta.Nfactura = "";
		}

		internal static bool proObtenerParametros()
		{
			bool result = false;
			try
			{
				string stSql = String.Empty;
				DataSet RrDatos = null;

				stSql = "SELECT " + 
				        "rTrim(isNull(valfanu1, 'N')) Activo, rTrim(isNull(valfanu2, '')) + rTrim(isNull(valfanu3, '')) URL, valfanu1i1 HOST, " + 
				        "isNull(nnumeri1, -1) Centro, rTrim(isNull(valfanu2i1, '')) + rTrim(isNull(valfanu2i2, '')) AccionAutorizacion, " + 
				        "rTrim(isNull(valfanu3i1, '')) + rTrim(isNull(valfanu3i2, '')) AccionAnulacion, " + 
				        "isNull(valfanu1i2, 'N') AutorizacionManual " + 
				        "FROM SCONSGLO WHERE gconsglo = 'BLACKBOX'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					//MsgBox "Error en proObtenerParametros: Constante de parametrizaci�n no definida", vbCritical, "Comunicaci�n WS BlackBox"
					result = false;
				}
				else
				{
					// Obtenemos y almacenamos los datos					
					bBlackBoxActivo = (Convert.ToString(RrDatos.Tables[0].Rows[0]["Activo"]) + "").Trim().ToUpper() == "S";					
					oParametrizacion.URL = "" + Convert.ToString(RrDatos.Tables[0].Rows[0]["URL"]);					
					oParametrizacion.HOST = "" + Convert.ToString(RrDatos.Tables[0].Rows[0]["HOST"]);					
					oParametrizacion.Centro = Convert.ToString(RrDatos.Tables[0].Rows[0]["Centro"]);					
					oParametrizacion.AccionAutorizacion = "" + Convert.ToString(RrDatos.Tables[0].Rows[0]["AccionAutorizacion"]);					
					oParametrizacion.AccionAnulacion = "" + Convert.ToString(RrDatos.Tables[0].Rows[0]["AccionAnulacion"]);
					oParametrizacion.Terminal = proNombreTerminal();					
					bAutorizacionManual = (Convert.ToString(RrDatos.Tables[0].Rows[0]["AutorizacionManual"]) + "").Trim() == "S";

					result = true;

				}				
				RrDatos.Close();
				return result;
			}
			catch
			{
				//MsgBox "Error en proObtenerParametros: " & Err.Description, vbCritical, "Comunicaci�n WS BlackBox"
				return false;
			}
		}

		private static string proNombreTerminal()
		{
			string result = String.Empty;
			try
			{

				Conexion.Obtener_conexion instancia = null;
				instancia = new Conexion.Obtener_conexion();
				result = instancia.NonMaquina();
				instancia = null;
				return result;
			}
			catch
			{

				//MsgBox "Error proNombreTerminal: " & Err.Description
				return "";
			}
		}

		private static string GET_HASH(int length = 32)
		{
			double varNum = 0;
			double varAlpha = 0;
			int i = 0;
			StringBuilder stringNum = new StringBuilder();
			StringBuilder stringAlpha = new StringBuilder();
			StringBuilder stringConcat = new StringBuilder();

			VBMath.Randomize();

			for (i = 0; i <= (length / ((int) 2)); i++)
			{
				varNum = (float) Math.Floor(10 * VBMath.Rnd());
				stringNum.Append(varNum.ToString());
			}

			VBMath.Randomize();

			i = 0;
			do 
			{
				varAlpha = (float) Math.Floor(100 * VBMath.Rnd());
				if (varAlpha > 65 && varAlpha < 90)
				{
					stringAlpha.Append(Strings.Chr(Convert.ToInt32(varAlpha)).ToString());
					i++;
				}
			}
			while(i < (length / ((int) 2)));

			for (i = 1; i <= (stringNum.ToString() + stringAlpha.ToString()).Length; i++)
			{
				stringConcat.Append(stringNum.ToString().Substring(i - 1, Math.Min(1, stringNum.ToString().Length - (i - 1))) + stringAlpha.ToString().Substring(i - 1, Math.Min(1, stringAlpha.ToString().Length - (i - 1))));
			}

			return stringConcat.ToString();

		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta, string stRespuesta, string stCodigoError, ref string stDescripcionRespuesta, string stGestor, string stTimeStamp, string fAutorizacion, string stReferencia, string stFactura, string stTerminal)
		{

			try
			{

				string stSql = String.Empty;

				stSql = "INSERT INTO DLOGBLBX " + 
				        "(gmensaje, gidenpac, Direccion, " + 
				        "Documento, " + 
				        "Tipo, oCentro, " + 
				        "gsocieda, gservici, " + 
				        "gpersona, otarjeta, " + 
				        "ntarjeta, " + 
				        "grespues, " + 
				        "gcoderro, " + 
				        "orespues, " + 
				        "ogestor, " + 
				        "fautoriz, " + 
				        "oreferen, " + 
				        "nfactura, " + 
				        "terminal, " + 
				        "oTimestamp)" + 
				        " VALUES ";

				string tempRefParam2 = "'";
				string tempRefParam = Serrores.proReplace(ref stMensaje, tempRefParam2, "`");
				string tempRefParam3 = "`";
				string tempRefParam5 = "'";
				string tempRefParam4 = Serrores.proReplace(ref stDescripcionRespuesta, tempRefParam5, "`");
				string tempRefParam6 = "`";
				stSql = stSql + 
				        "('" + stHash + "', '" + stGidenpacPaciente + "', '" + stDireccion + "', '" + 
				        Serrores.proReplace(ref tempRefParam, tempRefParam3, "''") + "', '" + 
				        stTipo + "', " + oParametrizacion.Centro + ", " + 
				        ((gsocieda == -1) ? "NULL" : "" + gsocieda.ToString()) + ", " + ((gservici == -1) ? "NULL" : "" + gservici.ToString()) + ", " + 
				        ((gpersona == -1) ? "NULL" : "" + gpersona.ToString()) + ", " + 
				        ((stBandaTarjeta.Trim() == "") ? "NULL" : "'" + stBandaTarjeta + "'") + ", " + 
				        ((stNumeroTarjeta.Trim() == "") ? "NULL" : "'" + stNumeroTarjeta + "'") + ", " + 
				        ((stRespuesta.Trim() == "") ? "NULL" : "'" + stRespuesta + "'") + ", " + 
				        ((stCodigoError.Trim() == "") ? "NULL" : "'" + stCodigoError + "'") + ", " + 
				        ((stDescripcionRespuesta.Trim() == "") ? "NULL" : "'" + Serrores.proReplace(ref tempRefParam4, tempRefParam6, "''") + "'") + ", " + 
				        ((stGestor.Trim() == "") ? "NULL" : "'" + stGestor + "'") + ", " + 
				        ((fAutorizacion.Trim() == "") ? "NULL" : "'" + fAutorizacion + "'") + ", " + 
				        ((stReferencia.Trim() == "") ? "NULL" : "'" + stReferencia + "'") + ", " + 
				        ((stFactura.Trim() == "") ? "NULL" : "'" + stFactura + "'") + ", " + 
				        ((stTerminal.Trim() == "") ? "NULL" : "'" + stTerminal + "'") + ", " + 
				        ((stTimeStamp.Trim() == "") ? "NULL" : "'" + stTimeStamp + "'") + ")";

				SqlCommand tempCommand = new SqlCommand(stSql, cnConexion);
				tempCommand.ExecuteNonQuery();
			}
			catch
			{
				//MsgBox "Error en proGuardaLog (" & Err.Number & "): " & Err.Description
			}

		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta, string stRespuesta, string stCodigoError, ref string stDescripcionRespuesta, string stGestor, string stTimeStamp, string fAutorizacion, string stReferencia, string stFactura)
		{
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, stNumeroTarjeta, stRespuesta, stCodigoError, ref stDescripcionRespuesta, stGestor, stTimeStamp, fAutorizacion, stReferencia, stFactura, "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta, string stRespuesta, string stCodigoError, ref string stDescripcionRespuesta, string stGestor, string stTimeStamp, string fAutorizacion, string stReferencia)
		{
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, stNumeroTarjeta, stRespuesta, stCodigoError, ref stDescripcionRespuesta, stGestor, stTimeStamp, fAutorizacion, stReferencia, "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta, string stRespuesta, string stCodigoError, ref string stDescripcionRespuesta, string stGestor, string stTimeStamp, string fAutorizacion)
		{
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, stNumeroTarjeta, stRespuesta, stCodigoError, ref stDescripcionRespuesta, stGestor, stTimeStamp, fAutorizacion, "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta, string stRespuesta, string stCodigoError, ref string stDescripcionRespuesta, string stGestor, string stTimeStamp)
		{
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, stNumeroTarjeta, stRespuesta, stCodigoError, ref stDescripcionRespuesta, stGestor, stTimeStamp, "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta, string stRespuesta, string stCodigoError, ref string stDescripcionRespuesta, string stGestor)
		{
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, stNumeroTarjeta, stRespuesta, stCodigoError, ref stDescripcionRespuesta, stGestor, "", "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta, string stRespuesta, string stCodigoError, ref string stDescripcionRespuesta)
		{
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, stNumeroTarjeta, stRespuesta, stCodigoError, ref stDescripcionRespuesta, "", "", "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta, string stRespuesta, string stCodigoError)
		{
			string tempRefParam7 = "";
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, stNumeroTarjeta, stRespuesta, stCodigoError, ref tempRefParam7, "", "", "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta, string stRespuesta)
		{
			string tempRefParam8 = "";
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, stNumeroTarjeta, stRespuesta, "", ref tempRefParam8, "", "", "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta, string stNumeroTarjeta)
		{
			string tempRefParam9 = "";
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, stNumeroTarjeta, "", "", ref tempRefParam9, "", "", "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona, string stBandaTarjeta)
		{
			string tempRefParam10 = "";
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, stBandaTarjeta, "", "", "", ref tempRefParam10, "", "", "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici, int gpersona)
		{
			string tempRefParam11 = "";
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, gpersona, "", "", "", "", ref tempRefParam11, "", "", "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda, int gservici)
		{
			string tempRefParam12 = "";
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, gservici, -1, "", "", "", "", ref tempRefParam12, "", "", "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje, int gsocieda)
		{
			string tempRefParam13 = "";
			proGuardaLog(stTipo, stDireccion, ref stMensaje, gsocieda, -1, -1, "", "", "", "", ref tempRefParam13, "", "", "", "", "", "");
		}

		private static void proGuardaLog(string stTipo, string stDireccion, ref string stMensaje)
		{
			string tempRefParam14 = "";
			proGuardaLog(stTipo, stDireccion, ref stMensaje, -1, -1, -1, "", "", "", "", ref tempRefParam14, "", "", "", "", "", "");
		}
	}
}