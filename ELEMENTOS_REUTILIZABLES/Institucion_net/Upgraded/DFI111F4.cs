using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace Institucion
{
	public partial class DFI111F4
		: RadForm
	{

		DataSet rsCentros = null;
		public DFI111F4()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void DFI111F4_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				txtContenga.Focus();
			}
		}

		private void DFI111F4_Load(Object eventSender, EventArgs eventArgs)
		{
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			sprCentros.MaxRows = 0;
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
		}

		private void sprCentros_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			int Col = 0;
			int Row = 0;
			if (Row >= 0)
			{
				cbAceptar.Enabled = true;
			}
		}

		private void txtContenga_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = SustituirComilla();
			}
			txtEmpiecepor.Text = "";
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void txtEmpiecepor_TextChanged(Object eventSender, EventArgs eventArgs)
		{

			string stSql = String.Empty;

			if (txtEmpiecepor.Text != "")
			{
				txtContenga.Text = "";

				stSql = "select ghospita, dnomhosp from dhospitava " + 
				        "Where dnomhosp like '" + txtEmpiecepor.Text.Trim() + "%'" + 
				        " Order By dnomhosp ";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, MInstitucion.GConexion);
				rsCentros = new DataSet();
				tempAdapter.Fill(rsCentros);
				if (rsCentros.Tables[0].Rows.Count != 0 && rsCentros.Tables[0].Rows.Count != 0)
				{
					rsCentros.MoveLast(null);
					rsCentros.MoveFirst();
					sprCentros.MaxRows = rsCentros.Tables[0].Rows.Count;
					carga_grid();
				}
				else
				{
					sprCentros.MaxRows = 0;
					txtEmpiecepor.SelectionStart = 0;
					txtEmpiecepor.SelectionLength = Strings.Len(txtEmpiecepor.Text);
					txtEmpiecepor.Focus();
					return;
				}
				rsCentros.Close();
			}
			else
			{
				sprCentros.MaxRows = 0;
			}


		}

		private void txtEmpiecepor_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{
				KeyAscii = SustituirComilla();
			}
			txtContenga.Text = "";
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void cbBuscarCadena_Click(Object eventSender, EventArgs eventArgs)
		{



			txtEmpiecepor.Text = "";

			StringBuilder busca = new StringBuilder();
			string cadena = txtContenga.Text.Trim();

			int iCorte = (cadena.IndexOf(',') + 1);
			if (iCorte == 0)
			{
				busca = new StringBuilder("dnomhosp like '%" + cadena + "%'");
			}
			else
			{
				while (iCorte != 0)
				{
					busca = new StringBuilder("dnomhosp like '%" + cadena.Substring(0, Math.Min(iCorte - 1, cadena.Length)).Trim() + "%'");
					cadena = cadena.Substring(iCorte, Math.Min(cadena.Length, cadena.Length - iCorte)).Trim();
					iCorte = (cadena.IndexOf(',') + 1);
					if (iCorte != 0)
					{
						if (rbY.IsChecked)
						{
							busca.Append(" and ");
						}
						else
						{
							busca.Append(" or ");
						}
					}
				}
				if (rbY.IsChecked)
				{
					busca.Append(" and dnomhosp like '%" + cadena + "%'");
				}
				else
				{
					busca.Append(" or dnomhosp like '%" + cadena + "%'");
				}
			}


			string stSql = "select ghospita, dnomhosp from dhospitava " + 
			               "Where " + "(" + busca.ToString() + ")" + 
			               " Order By dnomhosp ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, MInstitucion.GConexion);
			rsCentros = new DataSet();
			tempAdapter.Fill(rsCentros);
			if (rsCentros.Tables[0].Rows.Count != 0)
			{
				rsCentros.MoveLast(null);
				rsCentros.MoveFirst();
				sprCentros.MaxRows = rsCentros.Tables[0].Rows.Count;
				carga_grid();
			}
			else
			{
				sprCentros.MaxRows = 0;
				txtContenga.SelectionStart = 0;
				txtContenga.SelectionLength = Strings.Len(txtContenga.Text);
				txtContenga.Focus();
				return;
			}
			rsCentros.Close();

		}

		private void carga_grid()
		{
			sprCentros.Row = 0;
			rsCentros.MoveFirst();
			foreach (DataRow iteration_row in rsCentros.Tables[0].Rows)
			{
				sprCentros.Row++;
				sprCentros.Col = 1;
				sprCentros.Text = Convert.ToString(iteration_row["ghospita"]).Trim();
				sprCentros.Col = 2;
				sprCentros.Text = Convert.ToString(iteration_row["dnomhosp"]).Trim();
			}
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (sprCentros.ActiveRowIndex <= 0)
			{
				cbAceptar.Enabled = false;
				return;
			}
			sprCentros.Row = sprCentros.ActiveRowIndex;
			sprCentros.Col = 1;
			string stCodigoCentro = sprCentros.Text.Trim();
			sprCentros.Col = 2;
			string stDescripcionCentro = sprCentros.Text.Trim();
			MInstitucion.oLLAMADOR.RecogerCentro(stCodigoCentro.Trim(), stDescripcionCentro.Trim());
			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		public int SustituirComilla()
		{
			return 180;
		}
		private void DFI111F4_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}