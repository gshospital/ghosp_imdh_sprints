using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Institucion
{
	public class MCInstitucion
	{


		public void llamada(object llamador, SqlConnection Conexion)
		{
			MInstitucion.GConexion = Conexion;
			MInstitucion.oLLAMADOR = llamador;
			DFI111F3.DefInstance.ShowDialog();
		}

		public void BuscarCentro(object llamador, SqlConnection Conexion)
		{
			MInstitucion.GConexion = Conexion;
			MInstitucion.oLLAMADOR = llamador;
			DFI111F4.DefInstance.ShowDialog();
		}

		public MCInstitucion()
		{
			MInstitucion.clasemensaje = new Mensajes.ClassMensajes();
		}

		~MCInstitucion()
		{
			MInstitucion.clasemensaje = null;
		}
	}
}