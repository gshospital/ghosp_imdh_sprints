using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Institucion
{
	partial class DFI111F4
	{

		#region "Upgrade Support "
		private static DFI111F4 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI111F4 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI111F4();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprCentros", "txtContenga", "rbY", "rbO", "FrmCriterio", "cbBuscarCadena", "LbBusqueda", "Frmbuscar", "cbCancelar", "cbAceptar", "txtEmpiecepor", "Label1", "Frame1", "commandButtonHelper1", "sprCentros_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
        public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprCentros;
		public Telerik.WinControls.UI.RadTextBoxControl txtContenga;
		public Telerik.WinControls.UI.RadRadioButton rbY;
		public Telerik.WinControls.UI.RadRadioButton rbO;
		public Telerik.WinControls.UI.RadGroupBox FrmCriterio;
		public Telerik.WinControls.UI.RadButton cbBuscarCadena;
		public Telerik.WinControls.UI.RadTextBox LbBusqueda;
		public Telerik.WinControls.UI.RadGroupBox Frmbuscar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadTextBoxControl txtEmpiecepor;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
        //private FarPoint.Win.Spread.SheetView sprCentros_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFI111F4));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.sprCentros = new UpgradeHelpers.Spread.FpSpread();
			this.Frmbuscar = new Telerik.WinControls.UI.RadGroupBox();
			this.txtContenga = new Telerik.WinControls.UI.RadTextBoxControl();
			this.FrmCriterio = new Telerik.WinControls.UI.RadGroupBox();
			this.rbY = new Telerik.WinControls.UI.RadRadioButton();
			this.rbO = new Telerik.WinControls.UI.RadRadioButton();
			this.cbBuscarCadena = new Telerik.WinControls.UI.RadButton();
			this.LbBusqueda = new Telerik.WinControls.UI.RadTextBox();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.txtEmpiecepor = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Frmbuscar.SuspendLayout();
			this.FrmCriterio.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
			// 
			// sprCentros
			// 
			this.sprCentros.Location = new System.Drawing.Point(8, 128);
			this.sprCentros.Name = "sprCentros";
			this.sprCentros.Size = new System.Drawing.Size(502, 177);
			this.sprCentros.TabIndex = 0;
            //this.sprCentros.CellClick += new UpgradeHelpers.FpSpread.CellClickEventHandler(this.sprCentros_CellClick);
            this.sprCentros.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprCentros_CellClick);


			// 
			// Frmbuscar
			// 
			this.Frmbuscar.Controls.Add(this.txtContenga);
			this.Frmbuscar.Controls.Add(this.FrmCriterio);
			this.Frmbuscar.Controls.Add(this.cbBuscarCadena);
			this.Frmbuscar.Controls.Add(this.LbBusqueda);
			this.Frmbuscar.Enabled = true;
			this.Frmbuscar.Location = new System.Drawing.Point(4, 0);
			this.Frmbuscar.Name = "Frmbuscar";
			this.Frmbuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frmbuscar.Size = new System.Drawing.Size(293, 117);
			this.Frmbuscar.TabIndex = 6;
			this.Frmbuscar.Text = "B�squeda por contenido";
			this.Frmbuscar.Visible = true;
			// 
			// txtContenga
			// 
			this.txtContenga.AcceptsReturn = true;
			this.txtContenga.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtContenga.Location = new System.Drawing.Point(12, 36);
			this.txtContenga.MaxLength = 0;
			this.txtContenga.Name = "txtContenga";
			this.txtContenga.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtContenga.Size = new System.Drawing.Size(273, 21);
			this.txtContenga.TabIndex = 11;
			this.txtContenga.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContenga_KeyPress);
			// 
			// FrmCriterio
			// 
			this.FrmCriterio.Controls.Add(this.rbY);
			this.FrmCriterio.Controls.Add(this.rbO);
			this.FrmCriterio.Enabled = true;
			this.FrmCriterio.Location = new System.Drawing.Point(12, 68);
			this.FrmCriterio.Name = "FrmCriterio";
			this.FrmCriterio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmCriterio.Size = new System.Drawing.Size(158, 42);
			this.FrmCriterio.TabIndex = 8;
			this.FrmCriterio.Text = "Criterio de selecci�n";
			this.FrmCriterio.Visible = true;
			// 
			// rbY
			// 
			this.rbY.CausesValidation = true;
			this.rbY.IsChecked = false;
			this.rbY.Cursor = System.Windows.Forms.Cursors.Default;
			this.rbY.Enabled = true;
			this.rbY.ForeColor = System.Drawing.SystemColors.ControlText;
			this.rbY.Location = new System.Drawing.Point(20, 19);
			this.rbY.Name = "rbY";
			this.rbY.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.rbY.Size = new System.Drawing.Size(46, 14);
			this.rbY.TabIndex = 10;
			this.rbY.TabStop = true;
			this.rbY.Text = "A y B";
			this.rbY.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbY.Visible = true;
			// 
			// rbO
			// 
			this.rbO.CausesValidation = true;
			this.rbO.IsChecked = false;
			this.rbO.Cursor = System.Windows.Forms.Cursors.Default;
			this.rbO.Enabled = true;
			this.rbO.Location = new System.Drawing.Point(92, 19);
			this.rbO.Name = "rbO";
			this.rbO.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.rbO.Size = new System.Drawing.Size(47, 14);
			this.rbO.TabIndex = 9;
			this.rbO.TabStop = true;
			this.rbO.Text = "A � B";
			this.rbO.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.rbO.Visible = true;
			// 
			// cbBuscarCadena
			// 
			this.cbBuscarCadena.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbBuscarCadena.Image = (System.Drawing.Image) resources.GetObject("cbBuscarCadena.Image");
			this.cbBuscarCadena.Location = new System.Drawing.Point(248, 76);
			this.cbBuscarCadena.Name = "cbBuscarCadena";
			this.cbBuscarCadena.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbBuscarCadena.Size = new System.Drawing.Size(37, 33);
			this.cbBuscarCadena.TabIndex = 7;
			this.cbBuscarCadena.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbBuscarCadena.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.cbBuscarCadena.Click += new System.EventHandler(this.cbBuscarCadena_Click);
			// 
			// LbBusqueda
			// 
			this.LbBusqueda.Cursor = System.Windows.Forms.Cursors.Default;
			this.LbBusqueda.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LbBusqueda.Location = new System.Drawing.Point(12, 17);
			this.LbBusqueda.Name = "LbBusqueda";
			this.LbBusqueda.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbBusqueda.Size = new System.Drawing.Size(58, 13);
			this.LbBusqueda.TabIndex = 12;
			this.LbBusqueda.Text = "Contenga :";
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(428, 312);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 29);
			this.cbCancelar.TabIndex = 5;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Location = new System.Drawing.Point(340, 312);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 4;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtEmpiecepor);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(304, 40);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(193, 77);
			this.Frame1.TabIndex = 1;
			this.Frame1.Text = "B�squeda por caracteres iniciales";
			this.Frame1.Visible = true;
			// 
			// txtEmpiecepor
			// 
			this.txtEmpiecepor.AcceptsReturn = true;
			this.txtEmpiecepor.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtEmpiecepor.Location = new System.Drawing.Point(12, 44);
			this.txtEmpiecepor.MaxLength = 0;
			this.txtEmpiecepor.Name = "txtEmpiecepor";
			this.txtEmpiecepor.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtEmpiecepor.Size = new System.Drawing.Size(169, 21);
			this.txtEmpiecepor.TabIndex = 2;
			this.txtEmpiecepor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmpiecepor_KeyPress);
			this.txtEmpiecepor.TextChanged += new System.EventHandler(this.txtEmpiecepor_TextChanged);
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(12, 24);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(70, 16);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "Comienzo por:";
			// 
			// DFI111F4
			// 
			this.AcceptButton = this.cbBuscarCadena;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(514, 348);
			this.Controls.Add(this.sprCentros);
			this.Controls.Add(this.Frmbuscar);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.Frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DFI111F4";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "B�squeda de Centro ";
            //commandButtonHelper1.SetStyle(this.cbBuscarCadena, 1);
			this.ToolTipMain.SetToolTip(this.cbBuscarCadena, "Llevar a cabo la b�squeda en base de datos");
			this.Activated += new System.EventHandler(this.DFI111F4_Activated);
			this.Closed += new System.EventHandler(this.DFI111F4_Closed);
			this.Load += new System.EventHandler(this.DFI111F4_Load);
			this.Frmbuscar.ResumeLayout(false);
			this.FrmCriterio.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}