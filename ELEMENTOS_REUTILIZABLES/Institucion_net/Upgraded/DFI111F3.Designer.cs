using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Institucion
{
	partial class DFI111F3
	{

		#region "Upgrade Support "
		private static DFI111F3 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI111F3 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI111F3();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbbmedico_centro", "cbbCentro", "Label1", "Label12", "Frame1", "cbCancelar", "cbAceptar", "lblCIAS"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.MSForms.MSCombobox cbbmedico_centro;
        public UpgradeHelpers.MSForms.MSCombobox cbbCentro;
		public Telerik.WinControls.UI.RadLabel Label1;
        public Telerik.WinControls.UI.RadLabel Label12;
        public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbCancelar;
        public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadTextBox lblCIAS;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbbmedico_centro = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbCentro = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.lblCIAS = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbmedico_centro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCentro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCIAS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.cbbmedico_centro);
            this.Frame1.Controls.Add(this.cbbCentro);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(0, 2);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(473, 105);
            this.Frame1.TabIndex = 2;
            // 
            // cbbmedico_centro
            // 
            this.cbbmedico_centro.ColumnWidths = "";
            this.cbbmedico_centro.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbmedico_centro.Location = new System.Drawing.Point(10, 72);
            this.cbbmedico_centro.Name = "cbbmedico_centro";
            this.cbbmedico_centro.Size = new System.Drawing.Size(453, 20);
            this.cbbmedico_centro.TabIndex = 7;
            this.cbbmedico_centro.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbmedico_centro_SelectedIndexChanged);
            this.cbbmedico_centro.Enter += new System.EventHandler(this.cbbmedico_centro_Enter);
            // 
            // cbbCentro
            // 
            this.cbbCentro.ColumnWidths = "";
            this.cbbCentro.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbCentro.Location = new System.Drawing.Point(10, 29);
            this.cbbCentro.Name = "cbbCentro";
            this.cbbCentro.Size = new System.Drawing.Size(453, 20);
            this.cbbCentro.TabIndex = 6;
            this.cbbCentro.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbCentro_SelectedIndexChanged);
            this.cbbCentro.Enter += new System.EventHandler(this.cbbCentro_Enter);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(10, 10);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(102, 18);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "Tipo de Instituci�n:";
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label12.Location = new System.Drawing.Point(10, 53);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(61, 18);
            this.Label12.TabIndex = 3;
            this.Label12.Text = "Instituci�n:";
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(392, 112);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 1;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Enabled = false;
            this.cbAceptar.Location = new System.Drawing.Point(304, 112);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 0;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // lblCIAS
            // 
            this.lblCIAS.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCIAS.Enabled = false;
            this.lblCIAS.Location = new System.Drawing.Point(8, 114);
            this.lblCIAS.Name = "lblCIAS";
            this.lblCIAS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCIAS.Size = new System.Drawing.Size(283, 20);
            this.lblCIAS.TabIndex = 5;
            this.lblCIAS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DFI111F3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 146);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.lblCIAS);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(84, 139);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI111F3";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "B�squeda de Instituci�n - DFI111F4";
            this.Closed += new System.EventHandler(this.DFI111F3_Closed);
            this.Load += new System.EventHandler(this.DFI111F3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbmedico_centro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCentro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCIAS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}