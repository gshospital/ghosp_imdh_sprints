using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;


namespace Institucion
{
	public partial class DFI111F3
		: Telerik.WinControls.UI.RadForm
	{

		public DFI111F3()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			object tempRefParam = 1;
			object tempRefParam2 = cbbmedico_centro.get_ListIndex();
			object tempRefParam3 = 0;
			object tempRefParam4 = cbbmedico_centro.get_ListIndex();
			MInstitucion.oLLAMADOR.DatosInstitucion(Convert.ToString(cbbmedico_centro.get_Column( tempRefParam,  tempRefParam2)).Trim(), lblCIAS.Text, Convert.ToString(cbbmedico_centro.get_Column( tempRefParam3,  tempRefParam4)).Trim());
			this.Close();
		}

		private void cbbCentro_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
            cbbCentro_ClickEvent(eventSender, eventArgs);
		}

		private void cbbCentro_ClickEvent(Object eventSender, EventArgs eventArgs)
		{
			if (Convert.ToDouble(cbbCentro.get_ListIndex()) > -1)
			{
				object tempRefParam = 1;
                object tempRefParam2 = cbbCentro.SelectedValue;
                MInstitucion.CargarComboAlfanumerico("select ghospita, dnomhosp from Dhospita where Dhospita.gtipinst = '" + cbbCentro.SelectedValue + "' order by dnomhosp", "ghospita", "dnomhosp", cbbmedico_centro);
				if (cbbmedico_centro.ListCount != 0)
				{
					cbbmedico_centro.set_ListIndex(0);
				}
				else
				{
					lblCIAS.Text = "";
				}
			}
			else
			{
				cbbmedico_centro.Clear();
				lblCIAS.Text = "";
			}
			COMPROBAR_DATOS();
		}

		private void cbbCentro_Enter(Object eventSender, EventArgs eventArgs)
		{
			cbbCentro.SelectionStart = 0;
			cbbCentro.SelectionLength = Strings.Len(cbbCentro.Text);
		}

		private void cbbmedico_centro_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			if (Convert.ToDouble(cbbmedico_centro.get_ListIndex()) > -1)
			{
				object tempRefParam = 1;
				object tempRefParam2 = cbbmedico_centro.get_ListIndex();
				this.lblCIAS.Text = ObtenerCodigoHospital(Convert.ToString(cbbmedico_centro.get_Column( tempRefParam,  tempRefParam2)));
			}
			else
			{
				this.lblCIAS.Text = "";
			}
			COMPROBAR_DATOS();
		}

		private void cbbmedico_centro_ClickEvent(Object eventSender, EventArgs eventArgs)
		{
			if (Convert.ToDouble(cbbmedico_centro.get_ListIndex()) > -1)
			{
				object tempRefParam = 1;
				object tempRefParam2 = cbbmedico_centro.get_ListIndex();
				this.lblCIAS.Text = ObtenerCodigoHospital(Convert.ToString(cbbmedico_centro.get_Column( tempRefParam,  tempRefParam2)));
			}
			else
			{
				this.lblCIAS.Text = "";
			}
			COMPROBAR_DATOS();

		}

		private void cbbmedico_centro_Enter(Object eventSender, EventArgs eventArgs)
		{
			cbbmedico_centro.SelectionStart = 0;
			cbbmedico_centro.SelectionLength = Strings.Len(cbbmedico_centro.Text);
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void DFI111F3_Load(Object eventSender, EventArgs eventArgs)
		{
			string SQLcias = String.Empty;
            MInstitucion.CargarComboAlfanumerico("select gtipinst, dtipinst from Dtipinst order by  dtipinst", "gtipinst", "dtipinst", cbbCentro);
			COMPROBAR_DATOS();
		}

		private void COMPROBAR_DATOS()
		{
			if (Convert.ToDouble(cbbCentro.get_ListIndex()) != -1)
			{
				cbAceptar.Enabled = Convert.ToDouble(this.cbbmedico_centro.get_ListIndex()) != -1;
			}
			else
			{
				cbAceptar.Enabled = false;
			}
		}
		public string ObtenerCodigoHospital(string Codigo)
		{
			string result = String.Empty;
			result = "";
			string sql = "select chospofi from dhospita where ghospita = " + Codigo;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MInstitucion.GConexion);
			DataSet Rr = new DataSet();
			tempAdapter.Fill(Rr);
			if (Rr.Tables[0].Rows.Count != 0)
			{
				if (Convert.IsDBNull(Rr.Tables[0].Rows[0]["chospofi"]))
				{
					result = "";
				}
				else
				{
					result = Convert.ToString(Rr.Tables[0].Rows[0]["chospofi"]);
				}
			}
			Rr.Close();
			return result;
		}
		private void DFI111F3_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}