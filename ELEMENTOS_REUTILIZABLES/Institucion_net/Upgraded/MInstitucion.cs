using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Institucion
{
	internal static class MInstitucion
	{

		public static SqlConnection GConexion = null;
		public static Mensajes.ClassMensajes clasemensaje = null;
		public static dynamic oLLAMADOR = null;
		internal static void CargarComboAlfanumerico(string sql, string CampoCod, string campoDes, UpgradeHelpers.MSForms.MSCombobox comboacargar)
		{
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, GConexion);
			DataSet RrCombo = new DataSet();
			tempAdapter.Fill(RrCombo);
			comboacargar.Clear();
			//comboacargar.ColumnCount = 2;
			comboacargar.ColumnWidths = "15;0";
			foreach (DataRow iteration_row in RrCombo.Tables[0].Rows)
			{
                comboacargar.Items.Add(new RadListDataItem((Convert.IsDBNull(iteration_row[campoDes])) ? "" : Convert.ToString(iteration_row[campoDes]), Strings.StrConv(Convert.ToString(iteration_row[CampoCod]).Trim(), VbStrConv.ProperCase, 0)));
			}
			RrCombo.Close();
		}
	}
}