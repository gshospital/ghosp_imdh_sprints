using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace MaterialesDLL
{
	public partial class Materiales
		: Telerik.WinControls.UI.RadForm
	{

		string vstNombreMat = String.Empty;
		//Dim vlCodMat As Long
		string vlCodMat = String.Empty;
		string stUnidadMedFarm = String.Empty; // se pueden cargar f�rmacos
		bool GestionLotes = false;

		//oscar 21/10/03
		string vlote = String.Empty;
		string vnserie = String.Empty;
		string vcodlote = String.Empty;
		string vFechaCaducidad = String.Empty;
		//--------------
		bool bVerPVP = false;
		public Materiales()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//oscar 28/02/2003
			string[, ] ColeccionMaterialesSel = null;
			int i = 0;
			int TipoArFarmaco = 0;
			int iTipoArt = 0;
			DataSet RR = null;
			string sql = String.Empty;
			int iUniPres = 0;
			string lote = String.Empty;
			string nserie = String.Empty;
			string codlote = String.Empty;
			string fechaCaducidad = String.Empty;
			if (sprArticulo.MaxRows == 0)
			{
				return;
			}
			//------------

			if (General_Mat.gConsumosPAC)
			{				
				if (General_Mat.VoFormulario.sprConsumos.ReadOnly) //
                    {

					//Oscar: Nueva Opcion para poder seleccionar varios materiales
					//cuando se tratra de consumos nuevos

					sql = "select gtipoart from dtipoart where ifarmaco = 'S'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, General_Mat.VRcMaterial);
					RR = new DataSet();
					tempAdapter.Fill(RR);
					if (RR.Tables[0].Rows.Count != 0)
					{						
						TipoArFarmaco = Convert.ToInt32(RR.Tables[0].Rows[0]["gtipoart"]);
					}					
					RR.Close();

					i = 0;
					if (sprArticulo.SelectedRows.Count > 0)
					{
						//col 1: Desc, col 2: Codigo, col 3: Unitoma
						//ReDim Preserve ColeccionMaterialesSel(sprArticulo.SelModeSelCount - 1, 6)
						ColeccionMaterialesSel = ArraysHelper.RedimPreserve<string[, ]>(ColeccionMaterialesSel, new int[]{sprArticulo.SelectedRows.Count, 8});

						int tempForVar = sprArticulo.MaxRows;
                            
						for (int x = 1; x <= tempForVar; x++)
						{
							sprArticulo.Row = x;//xma
							if (sprArticulo.SelModeSelected)
							{
								//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprArticulo.SelModeIndex was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
								//sprArticulo.setSelModeIndex(x);
                                sprArticulo.Col = 1;
								vstNombreMat = sprArticulo.Text;
								sprArticulo.Col = 2;
								vlCodMat = sprArticulo.Text;
								sprArticulo.Col = 3;
								stUnidadMedFarm = sprArticulo.Text;
								sprArticulo.Col = 4;
								lote = sprArticulo.Text;
								sprArticulo.Col = 5;
								nserie = sprArticulo.Text;
								sprArticulo.Col = 6;
								codlote = sprArticulo.Text;
								//oscar 17/11/2003
								sprArticulo.Col = 7;
								fechaCaducidad = sprArticulo.Text;
								//---------------

								ColeccionMaterialesSel[i, 0] = vlCodMat;
								ColeccionMaterialesSel[i, 1] = vstNombreMat;
								ColeccionMaterialesSel[i, 2] = stUnidadMedFarm;
								ColeccionMaterialesSel[i, 4] = lote;
								ColeccionMaterialesSel[i, 5] = nserie;
								ColeccionMaterialesSel[i, 6] = codlote;
								//oscar 17/11/2003
								ColeccionMaterialesSel[i, 7] = fechaCaducidad;
								//----------
                                								
								iTipoArt = Convert.ToInt32(General_Mat.VoFormulario.proObtenerTipoDeArticulo(vlCodMat));
								iUniPres = 0;
								if (iTipoArt != TipoArFarmaco)
								{ //si no es un farmaco
									sql = "select isnull(gunitoma,0) from darticulva where gcodiart ='" + vlCodMat + "'";
								}
								else
								{
									sql = "select isnull(gunitoma,0) from dcodfarmva where gfarmaco ='" + vlCodMat + "'";
								}
								SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, General_Mat.VRcMaterial);
								RR = new DataSet();
								tempAdapter_2.Fill(RR);
								if (RR.Tables[0].Rows.Count != 0)
								{									
									iUniPres = Convert.ToInt32(RR.Tables[0].Rows[0][0]);
								}
								
								RR.Close();
								if (iTipoArt == TipoArFarmaco)
								{ //si   es un farmaco
									sql = "select isnull(gunitoma,0) from dcodfarmva where gfarmaco ='" + vlCodMat + "'";
									SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, General_Mat.VRcMaterial);
									RR = new DataSet();
									tempAdapter_3.Fill(RR);
									if (RR.Tables[0].Rows.Count != 0)
									{
										
										iUniPres = Convert.ToInt32(RR.Tables[0].Rows[0][0]);
									}
									
									RR.Close();
								}
								if (iUniPres != 0)
								{
									sql = "select dunitoma from dunitomava where gunitoma=" + iUniPres.ToString();
									SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, General_Mat.VRcMaterial);
									RR = new DataSet();
									tempAdapter_4.Fill(RR);
									if (RR.Tables[0].Rows.Count != 0)
									{										
										ColeccionMaterialesSel[i, 3] = Convert.ToString(RR.Tables[0].Rows[0][0]).ToUpper();
									}
									else
									{
										ColeccionMaterialesSel[i, 3] = "";
									}
									
									RR.Close();
								}
								else
								{
									ColeccionMaterialesSel[i, 3] = "";
								}

								i++;
								//---------------

							}
						}
                        						
						General_Mat.VoFormulario.Recoger_ArticulosMultiples(ColeccionMaterialesSel);
						this.Close();

					}
				}

			}
			else
			{
				//Funcionalidad para cuando no es la pantalla de consumos quien llama a materiales.dll
				//o estamos en modo modificacion
				if (stUnidadMedFarm != "")
				{
					//oscar 21/10/03
					//VoFormulario.prorecojo_material vstNombreMat, vlCodMat, stUnidadMedFarm					
					General_Mat.VoFormulario.prorecojo_material(vstNombreMat, vlCodMat, stUnidadMedFarm, vcodlote, vlote, vnserie, vFechaCaducidad);
					//---------------
				}
				else
				{
					//oscar 21/10/03
					//VoFormulario.prorecojo_material vstNombreMat, vlCodMat					
					General_Mat.VoFormulario.prorecojo_material(vstNombreMat, vlCodMat, null, vcodlote, vlote, vnserie, vFechaCaducidad);
					//---------------
				}

				this.Close();

			}

		}

		private void cbBuscar_Click(Object eventSender, EventArgs eventArgs)
		{
			//si hay algo en la caja de texto hago una select para
			//sacar los materiales
			string tstBuscar = String.Empty;
			DataSet tRrBuscar = null;
			int tiRow = 0;
			string NoPresentaLotes = "";
			if (!General_Mat.GestionporLotes && GestionLotes)
			{
				NoPresentaLotes = " and lote is null and nserie is  null ";
			}
			string NuevosCampos = "";
			string CadenaBusquedaNuevosCampos = ""; //oscar 21/11/2003
			if (GestionLotes)
			{
				//oscar 17/11/2003
				//NuevosCampos = ",lote,nserie,gcodlote
				NuevosCampos = ",lote,nserie,gcodlote,fcaduc";
				//oscar 21/11/2003
				//Busqueda por lote/serie
				if (tbLote.Text != "")
				{
					CadenaBusquedaNuevosCampos = CadenaBusquedaNuevosCampos + " AND lote like '" + tbLote.Text + "%' ";
				}
				if (tbSerie.Text != "")
				{
					CadenaBusquedaNuevosCampos = CadenaBusquedaNuevosCampos + " AND nserie like '" + tbSerie.Text + "%' ";
				}
				//------
			}


			this.Cursor = Cursors.WaitCursor;


			//If tbNombre.Text <> "" Then
			if (tbNombre.Text != "" || tbLote.Text != "" || tbSerie.Text != "")
			{

				if (General_Mat.bIgnorarTipoArticulo)
				{
					if (General_Mat.ElAlmacen != "")
					{
						tstBuscar = "select substring(b03_titbreve,1,60) DARTICUL,b03_eaux gcodiart,b03_c005 as dunidmed" + NuevosCampos + 
						            " from dconsumo,ifmsal_artalm where " + " b03_titbreve like '%" + tbNombre.Text + "%' and dconsumo.fborrado is null ";
						tstBuscar = tstBuscar + " and ifmsal_artalm.articulo=b03_eaux and ifmsal_artalm.almacen='" + General_Mat.ElAlmacen.Trim() + "' and ifmsal_artalm.fborrado is null ";
					}
					else
					{
						tstBuscar = "select substring(b03_titbreve,1, 60) DARTICUL,b03_eaux gcodiart,b03_c005 as dunidmed" + NuevosCampos + 
						            " from dconsumo where " + " b03_titbreve like '%" + tbNombre.Text + "%' and fborrado is null ";
					}
					if (!General_Mat.bSacarFarmacos)
					{
						tstBuscar = tstBuscar + " and ifarmaco<>'S'";
					}
					//oscar 21/11/2003
					tstBuscar = tstBuscar + CadenaBusquedaNuevosCampos;
					//------
					tstBuscar = tstBuscar + NoPresentaLotes + " order by b03_titbreve";
				}
				else
				{
					if (General_Mat.ElAlmacen != "")
					{
						tstBuscar = "select * from darticulva,ifmsal_artalm where " + " gtipoart=" + General_Mat.ViTipoMat.ToString() + " and darticul like '%" + tbNombre.Text + "%' and " + " ifmsal_artalm.articulo= gcodiart and ifmsal_artalm.almacen='" + General_Mat.ElAlmacen.Trim() + "' and ifmsal_artalm.fborrado is null " + NoPresentaLotes + CadenaBusquedaNuevosCampos + " order by darticul";
					}
					else
					{
						tstBuscar = "select * from darticulva where " + " gtipoart=" + General_Mat.ViTipoMat.ToString() + " and darticul like '%" + tbNombre.Text + "%' " + NoPresentaLotes + CadenaBusquedaNuevosCampos + " order by darticul";
					}
				}

				tiRow = 0;

				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstBuscar, General_Mat.VRcMaterial);
				tRrBuscar = new DataSet();
				tempAdapter.Fill(tRrBuscar);
				sprArticulo.MaxRows = tRrBuscar.Tables[0].Rows.Count;
				foreach (DataRow iteration_row in tRrBuscar.Tables[0].Rows)
				{
					tiRow++;
					sprArticulo.Row = tiRow;
					sprArticulo.Col = 1;					
					sprArticulo.setTypeEditLen(65); //oscar 04/11/2003 --> Ojito con las descripciones en los grid
					sprArticulo.Text = Convert.ToString(iteration_row["darticul"]).Trim();
					sprArticulo.Col = 2;
					sprArticulo.Text = Convert.ToString(iteration_row["gcodiart"]);
					sprArticulo.Col = 3;					
					sprArticulo.Text = (!Convert.IsDBNull(iteration_row["dunidmed"])) ? Convert.ToString(iteration_row["dunidmed"]) : "";
					if (GestionLotes)
					{
						sprArticulo.Col = 4;
						
						sprArticulo.Text = (!Convert.IsDBNull(iteration_row["lote"])) ? Convert.ToString(iteration_row["lote"]) : "";
						sprArticulo.Col = 5;						
						sprArticulo.Text = (!Convert.IsDBNull(iteration_row["nserie"])) ? Convert.ToString(iteration_row["nserie"]) : "";
						sprArticulo.Col = 6;						
						sprArticulo.Text = (!Convert.IsDBNull(iteration_row["gcodlote"])) ? Convert.ToString(iteration_row["gcodlote"]) : "";
						//oscar 17/11/2003
						sprArticulo.Col = 7;						
						sprArticulo.Text = (!Convert.IsDBNull(iteration_row["fcaduc"])) ? Convert.ToDateTime(iteration_row["fcaduc"]).ToString("dd/MM/yyyy") : "";
						//----------------
					}
					sprArticulo.Col = 8;
					sprArticulo.Text = fCalculaPVP(Convert.ToString(iteration_row["gcodiart"]));
				}
				
				tRrBuscar.Close();
			}
			if (sprArticulo.MaxRows == 0)
			{
				tbNombre.Focus();
			}

			this.Cursor = Cursors.Default;

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		//oscar 12/03/03
		private void cmdBuscarConsumosHab_Click(Object eventSender, EventArgs eventArgs)
		{
			string tstBuscar = String.Empty;
			DataSet tRrBuscar = null;
			int tiRow = 0;
			string CadenaBusquedaNuevosCampos = String.Empty; //oscar 21/11/2003
			string NoPresentaLotes = "";
			if (!General_Mat.GestionporLotes && GestionLotes)
			{
				NoPresentaLotes = " and lote is null and nserie is  null ";
			}
			string NuevosCampos = "";
			if (GestionLotes)
			{
				//oscar 17/11/2003
				//NuevosCampos = ",lote,nserie,gcodlote"
				NuevosCampos = ",lote,nserie,gcodlote,fcaduc";
				//oscar 21/11/2003
				//Busqueda por lote/serie
				if (tbLote.Text != "")
				{
					CadenaBusquedaNuevosCampos = CadenaBusquedaNuevosCampos + " AND lote like '" + tbLote.Text + "%' ";
				}
				if (tbSerie.Text != "")
				{
					CadenaBusquedaNuevosCampos = CadenaBusquedaNuevosCampos + " AND nserie like '" + tbSerie.Text + "%' ";
				}
				//-----------
			}

			this.Cursor = Cursors.WaitCursor;

			//If cbbSerPeti.ListIndex > 0  Then
			if (Convert.ToDouble(cbbSerPeti.get_ListIndex()) > 0 || tbLote.Text != "" || tbSerie.Text != "")
			{

				if (General_Mat.bIgnorarTipoArticulo)
				{
					//If ElAlmacen <> "" Then
					//    tstBuscar = "select substring(b03_titbreve,1,60) DARTICUL,b03_eaux gcodiart,b03_c005 as dunidmed" & _
					//" from dconsumo,ifmsal_artalm where " _
					//& " b03_titbreve like '%" & tbNombre.Text & "%' and dconsumo.fborrado is null "
					//    tstBuscar = tstBuscar & " and ifmsal_artalm.articulo=b03_eaux and ifmsal_artalm.almacen='" & Trim(ElAlmacen) & "' and ifmsal_artalm.fborrado is null "
					//Else
					//    tstBuscar = "select substring(b03_titbreve,1, 60) DARTICUL,b03_eaux gcodiart,b03_c005 as dunidmed" & _
					//" from dconsumo where " _
					//& " b03_titbreve like '%" & tbNombre.Text & "%' and fborrado is null "
					//End If
					tstBuscar = "Select substring(DCONSUMO.b03_titbreve,1, 60) DARTICUL, DCONSUMO.b03_eaux gcodiart, DCONSUMO.b03_c005 as dunidmed " + NuevosCampos + 
					            "FROM FTMOVB97 , DCONSUMO " + 
					            "WHERE TMOV='ARTSERV' AND " + 
					            "FTMOVB97.b97_c001 = DCONSUMO.b03_eaux  AND ";
					if (Convert.ToDouble(cbbSerDisp.get_ListIndex()) > 0)
					{
						//UPGRADE_WARNING: (1068) cbbSerDisp.Column() of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
						object tempRefParam = 1;
						object tempRefParam2 = cbbSerDisp.get_ListIndex();
						tstBuscar = tstBuscar + 
						            "FTMOVB97.B97_C004 = '" + Convert.ToString(cbbSerDisp.get_Column(tempRefParam, tempRefParam2)).Trim() + "' AND ";
					}
					
					object tempRefParam3 = 1;
					object tempRefParam4 = cbbSerPeti.get_ListIndex();
					tstBuscar = tstBuscar + 
					            "FTMOVB97.B97_C002 = '" + Convert.ToString(cbbSerPeti.get_Column(tempRefParam3, tempRefParam4)).Trim() + "' AND " + 
					            "DCONSUMO.FBORRADO is null";

					if (!General_Mat.bSacarFarmacos)
					{
						tstBuscar = tstBuscar + " and DCONSUMO.ifarmaco<>'S'";
					}

					//oscar 21/11/2003
					tstBuscar = tstBuscar + CadenaBusquedaNuevosCampos;
					//------

					tstBuscar = tstBuscar + NoPresentaLotes + " order by DCONSUMO.b03_titbreve";
				}
				else
				{
					//If ElAlmacen <> "" Then
					//    tstBuscar = "select * from darticulva,ifmsal_artalm where " _
					//& " gtipoart=" & ViTipoMat _
					//& " and darticul like '%" & tbNombre.Text & "%' and " _
					//& " ifmsal_artalm.articulo= gcodiart and ifmsal_artalm.almacen='" & Trim(ElAlmacen) & "' and ifmsal_artalm.fborrado is null " _
					//& " order by darticul"
					//Else
					//    tstBuscar = "select * from darticulva where " _
					//& " gtipoart=" & ViTipoMat _
					//& " and darticul like '%" & tbNombre.Text & "%' " _
					//& " order by darticul"
					//End If
					tstBuscar = "select darticulva.* " + 
					            "FROM FTMOVB97 , DARTICULVA " + 
					            "WHERE TMOV='ARTSERV' AND " + 
					            "FTMOVB97.b97_c001 = darticulva.gcodiart AND " + 
					            "DARTICULVA.gtipoart=" + General_Mat.ViTipoMat.ToString() + " AND ";
					if (Convert.ToDouble(cbbSerDisp.get_ListIndex()) > 0)
					{						
						object tempRefParam5 = 1;
						object tempRefParam6 = cbbSerDisp.get_ListIndex();
						tstBuscar = tstBuscar + 
						            "FTMOVB97.B97_C004 = '" + Convert.ToString(cbbSerDisp.get_Column(tempRefParam5, tempRefParam6)).Trim() + "' AND ";
					}
					
					object tempRefParam7 = 1;
					object tempRefParam8 = cbbSerPeti.get_ListIndex();
					tstBuscar = tstBuscar + 
					            "FTMOVB97.B97_C002 = '" + Convert.ToString(cbbSerPeti.get_Column(tempRefParam7, tempRefParam8)).Trim() + "' " + 
					            NoPresentaLotes + 
					            CadenaBusquedaNuevosCampos + 
					            "ORDER BY DARTICULVA.darticul";
				}

				tiRow = 0;

				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstBuscar, General_Mat.VRcMaterial);
				tRrBuscar = new DataSet();
				tempAdapter.Fill(tRrBuscar);
				sprArticulo.MaxRows = tRrBuscar.Tables[0].Rows.Count;
				if (tRrBuscar.Tables[0].Rows.Count != 0)
				{
					foreach (DataRow iteration_row in tRrBuscar.Tables[0].Rows)
					{
						tiRow++;
						sprArticulo.Row = tiRow;
						sprArticulo.Col = 1;
						sprArticulo.setTypeEditLen(65); //oscar 04/11/2003 --> Ojito con las descripciones en los grid
						sprArticulo.Text = Convert.ToString(iteration_row["darticul"]).Trim();
						sprArticulo.Col = 2;
						sprArticulo.Text = Convert.ToString(iteration_row["gcodiart"]);
						sprArticulo.Col = 3;
						
						sprArticulo.Text = (!Convert.IsDBNull(iteration_row["dunidmed"])) ? Convert.ToString(iteration_row["dunidmed"]) : "";
						if (GestionLotes)
						{
							sprArticulo.Col = 4;							
							sprArticulo.Text = (!Convert.IsDBNull(iteration_row["lote"])) ? Convert.ToString(iteration_row["lote"]) : "";
							sprArticulo.Col = 5;							
							sprArticulo.Text = (!Convert.IsDBNull(iteration_row["nserie"])) ? Convert.ToString(iteration_row["nserie"]) : "";
							sprArticulo.Col = 6;							
							sprArticulo.Text = (!Convert.IsDBNull(iteration_row["gcodlote"])) ? Convert.ToString(iteration_row["gcodlote"]) : "";
							//oscar 17/11/2003
							sprArticulo.Col = 7;					
							sprArticulo.Text = (!Convert.IsDBNull(iteration_row["fcaduc"])) ? Convert.ToDateTime(iteration_row["fcaduc"]).ToString("dd/MM/yyyy") : "";
							//--------------
						}
						sprArticulo.Col = 8;
						sprArticulo.Text = fCalculaPVP(Convert.ToString(iteration_row["gcodiart"]));
					}
				}
				else
				{
					//si no devuelve ningun registro
					sprArticulo.MaxRows = 0; //se limpia el spread
					//se saca un mensaje dependiendo de lo que se haya pedido
					MessageBox.Show("No se han podido encontrar Consumos Habituales para el servicio peticionario y el servicio dispensador indicados.", "B�squeda de Consumos Habituales", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}				
				tRrBuscar.Close();

			}
			else
			{
				//se saca un mensaje de obligatoriedad de campo
				MessageBox.Show("El Servicio Peticionario es obligatorio.", "B�squeda de Consumos Habituales", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}
			//If sprArticulo.MaxRows = 0 Then
			//    tbNombre.SetFocus
			//End If

			this.Cursor = Cursors.Default;

		}
		//----------------

		private void Materiales_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				tbNombre.Focus();
			}
		}
		
		private void Materiales_Load(Object eventSender, EventArgs eventArgs)
		{
			string tstCab = String.Empty;
			DataSet tRrCab = null;

            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

			sprArticulo.MaxRows = 0;
			sprArticulo.Row = -1;
			sprArticulo.Col = 2;
			sprArticulo.SetColHidden(sprArticulo.Col, true);
            sprArticulo.Col = 3;
            sprArticulo.SetColHidden(sprArticulo.Col, true);
            sprArticulo.Col = 6;
            sprArticulo.SetColHidden(sprArticulo.Col, true);
            cbAceptar.Enabled = false;
			//busqueda de la caption

			if (General_Mat.bIgnorarTipoArticulo)
			{ // si son todos los tipos de art�culos
				this.Text = "B�squeda de art�culos-(TODOS)";
			}
			else
			{
				tstCab = "select dtiparti from dtipoart " + " where gtipoart=" + General_Mat.ViTipoMat.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, General_Mat.VRcMaterial);
				tRrCab = new DataSet();
				tempAdapter.Fill(tRrCab);			
				
				if (!Convert.IsDBNull(tRrCab.Tables[0].Rows[0]["dtiparti"]))
				{					
					this.Text = "Busqueda de materiales-(" + Convert.ToString(tRrCab.Tables[0].Rows[0]["dtiparti"]).Trim() + ")";
				}				
				tRrCab.Close();
			}
			GestionLotes = false;
			tstCab = "select * from sconsglo where gconsglo='geslotes'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, General_Mat.VRcMaterial);
			tRrCab = new DataSet();
			tempAdapter_2.Fill(tRrCab);
			if (tRrCab.Tables[0].Rows.Count != 0)
			{
				
				if (Convert.ToString(tRrCab.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
				{
					GestionLotes = true;
				}
			}			
			tRrCab.Close();

			GestionLotes = GestionLotes && (!General_Mat.EsProgramacion);

			//OSCAR C Abril 2014
			//En funcion de nueva constante global, se muestra el precio de la protesis/material selecionado
			bVerPVP = (Serrores.ObternerValor_CTEGLOBAL(General_Mat.VRcMaterial, "IQUIRPVP", "VALFANU1") == "S");

			if (!GestionLotes)
			{
				sprArticulo.Col = 4;
				sprArticulo.SetColHidden(sprArticulo.Col, true);
				sprArticulo.Col = 5;
				sprArticulo.SetColHidden(sprArticulo.Col, true);
				sprArticulo.Col = 6;
				sprArticulo.SetColHidden(sprArticulo.Col, true);
				//oscar 17/11/2003
				sprArticulo.Col = 7;
				sprArticulo.SetColHidden(sprArticulo.Col, true);
				//--------
			}

			//oscar 17/11/2003
			fraLote.Visible = GestionLotes;
			//-----

			sprArticulo.Col = 8;
			sprArticulo.SetColHidden(sprArticulo.Col, !bVerPVP);

			//oscar 28/02/2003
			string sql = String.Empty;
			DataSet RR = null;
			bool bAlmacen = false;
			if (General_Mat.gConsumosPAC)
			{

                //Solo cuando estamos en modo nuevo, se permite la multiseleccion.
                //En el caso de modificar articulo, funciona solo como seleccion simple.                
               
                if (General_Mat.VoFormulario.sprConsumos.ReadOnly)
                {
                    //camaya todo_X_4
					//sprArticulo.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.MultiRange;
					//sprArticulo.OperationMode =  FarPoint.Win.Spread.OperationMode.ExtendedSelect;
					
                 }

				//oscar 12/03/03
				bAlmacen = false;
				sql = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO = 'CONTALMA'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, General_Mat.VRcMaterial);
				RR = new DataSet();
				tempAdapter_3.Fill(RR);
				if (RR.Tables[0].Rows.Count != 0)
				{		
					
					if (!Convert.IsDBNull(RR.Tables[0].Rows[0][0]))
					{						
						if (Convert.ToString(RR.Tables[0].Rows[0][0]).Trim().ToUpper() == "S")
						{
							bAlmacen = true;
						}
					}
				}
				
				RR.Close();
				if (bAlmacen)
				{
					//IDC: de momento no dispone de la opcion de Busqueda por consumos habituales
					//frmConsumosHabituales.Visible = False
					//chkConsumosHabituales.Visible = False
					//chkConsumosHabituales.Top = 90
					//frmConsumosHabituales.Top = 360
					//frCab.Top = 90
					//sprArticulo.Top = 1215
					//cbAceptar.Top = 3765
					//cbCancelar.Top = 3765
					//Me.Height = 4620
				}
				else
				{
					//CURIA: Dispone la opcion de Busqueda por consumos habituales
					frmConsumosHabituales.Visible = true;
					chkConsumosHabituales.Visible = true;
					chkConsumosHabituales.Top = 6;
					frmConsumosHabituales.Top = 24;
					frCab.Top = 105;
					sprArticulo.Top = 165;
					cbAceptar.Top = 473;
					cbCancelar.Top = 473;
					this.Height = 547;
					CargarComboServicios();
					frmConsumosHabituales.Enabled = false;
				}

			}
			//------------

		}

		private void sprArticulo_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
            int Col = 1;
            int Row = 1;

            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = sprArticulo.Col;
                Row = sprArticulo.Row;

            }

            //oscar 27/02/2003
            if (General_Mat.gConsumosPAC)
			{			
				if (General_Mat.VoFormulario.sprConsumos.ReadOnly)
				{

					if (sprArticulo.SelectedRows.Count > 0)
					{
						cbAceptar.Enabled = true;
						cbAceptar.Focus();
					}
					else
					{
						cbAceptar.Enabled = false;
					}
				}

			}
			else
			{
				//Funcionalidad cuando no es Consumos quien llama a Materiales o
				//estamos en modo modificacion

				if (Col != 0)
				{
					sprArticulo.Row = Row;
					sprArticulo.Col = 1;
					vstNombreMat = sprArticulo.Text;
					sprArticulo.Col = 2;
					//vlCodMat = Val(.Text)
					vlCodMat = sprArticulo.Text.Trim();
					sprArticulo.Col = 3;
					stUnidadMedFarm = sprArticulo.Text.Trim();
					sprArticulo.Col = 4;
					vlote = sprArticulo.Text;
					sprArticulo.Col = 5;
					vnserie = sprArticulo.Text;
					sprArticulo.Col = 6;
					vcodlote = sprArticulo.Text;
					//oscar 17/11/2003
					sprArticulo.Col = 7;
					vFechaCaducidad = sprArticulo.Text;
					//----------
					cbAceptar.Enabled = true;
				}

			}
		}
        
		private void tbLote_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNombre_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNombre.SelectionStart = 0;
			tbNombre.SelectionLength = Strings.Len(tbNombre.Text);

		}

		private void tbNombre_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);			
			KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]);

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		//oscar 12/03/03
		private void chkConsumosHabituales_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (chkConsumosHabituales.CheckState == CheckState.Checked)
			{
				tbNombre.Text = "";
				frCab.Enabled = false;
				frmConsumosHabituales.Enabled = true;
			}
			else
			{
				cbbSerPeti.set_ListIndex(0);
				cbbSerDisp.set_ListIndex(0);
				frCab.Enabled = true;
				frmConsumosHabituales.Enabled = false;
			}
		}
		//------

		//oscar 12/03/03
		private void CargarComboServicios()
		{
			int i = 0;
			string sql = " SELECT * FROM DSERVICIVA ORDER BY dnomserv";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, General_Mat.VRcMaterial);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				i = 0;
				object tempRefParam = "";
				object tempRefParam2 = Type.Missing;
				cbbSerPeti.AddItem(tempRefParam, tempRefParam2);
				cbbSerPeti.set_Column(1, i, Convert.ToString(-1));
				object tempRefParam3 = "";
				object tempRefParam4 = Type.Missing;
				cbbSerDisp.AddItem(tempRefParam3, tempRefParam4);
				cbbSerDisp.set_Column(1, i, Convert.ToString(-1));
				i = 1;
				foreach (DataRow iteration_row in RR.Tables[0].Rows)
				{
					object tempRefParam5 = Convert.ToString(iteration_row["DNOMSERV"]).Trim();
					object tempRefParam6 = Type.Missing;
					cbbSerPeti.AddItem(tempRefParam5, tempRefParam6);					
					cbbSerPeti.set_Column(1, i, Convert.ToString(iteration_row["GSERVICI"]));

					object tempRefParam7 = Convert.ToString(iteration_row["DNOMSERV"]).Trim();
					object tempRefParam8 = Type.Missing;
					cbbSerDisp.AddItem(tempRefParam7, tempRefParam8);					
					cbbSerDisp.set_Column(1, i, Convert.ToString(iteration_row["GSERVICI"]));

					i++;
				}
			}
		}

		private void tbSerie_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private string fCalculaPVP(string ParaCodigoArt)
		{
			string result = String.Empty;
			result = "";
			if (!bVerPVP)
			{
				return result;
			}
			string sql = " SELECT pventau from ifmsal_articulo Where  articulo ='" + ParaCodigoArt + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, General_Mat.VRcMaterial);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{				
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["pventau"]))
				{					
					result = StringsHelper.Format(RR.Tables[0].Rows[0]["pventau"], "##0.00");
				}
			}			
			RR.Close();
			return result;
		}

		private void Materiales_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
            this.Dispose();
		}
	}
}