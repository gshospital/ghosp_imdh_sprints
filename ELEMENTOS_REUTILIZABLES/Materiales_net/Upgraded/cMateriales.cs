using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace MaterialesDLL
{
	public class cMateriales
	{

		public void proLlama_Materiales(object oForm, int iTipoArt, SqlConnection rcConecta, object IgnorarTipoArticulo_optional, object SacarFarmacos_optional, object Almacen_optional, object ConsumosPac, object ParaGestionporLotes, bool Programacion)
		{
			object IgnorarTipoArticulo = (IgnorarTipoArticulo_optional == Type.Missing) ? null : IgnorarTipoArticulo_optional as object;
			object SacarFarmacos = (SacarFarmacos_optional == Type.Missing) ? null : SacarFarmacos_optional as object;
			string Almacen = (Almacen_optional == Type.Missing) ? String.Empty : Almacen_optional as string;

			//RECIBE LOS DATOS EN proLlama_MAterriales.
			//Se le manda el objeto llamador,
			//el c�digo del tipo de material,
			//la conexi�n,
			//Devuelve en el proRecojo_Material vstNombreMat, vlCodMat
			//con la descripci�n del material y el c�digo

			Serrores.oClass = new Mensajes.ClassMensajes();
			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(rcConecta);
			Serrores.vAyuda = Serrores.ObtenerFicheroAyuda();

			General_Mat.VRcMaterial = rcConecta;
			General_Mat.ViTipoMat = iTipoArt;
			General_Mat.bIgnorarTipoArticulo = IgnorarTipoArticulo_optional != null;
			if (SacarFarmacos_optional != Type.Missing)
			{				
				General_Mat.bSacarFarmacos = Convert.ToBoolean(SacarFarmacos);
			}
			if (Almacen_optional == Type.Missing)
			{
				General_Mat.ElAlmacen = "";
			}
			else
			{
				General_Mat.ElAlmacen = Almacen;
			}

			//oscar 13/03/03
			General_Mat.gConsumosPAC = false;
			if (ConsumosPac != Type.Missing)
			{
				if (ConsumosPac.ToString() == "CP")
				{
					General_Mat.gConsumosPAC = true;
				}
			}
			//-------------
			General_Mat.GestionporLotes = false;
			if (ParaGestionporLotes != Type.Missing)
			{
				General_Mat.GestionporLotes = Convert.ToBoolean(ParaGestionporLotes);
			}

			// David 05/10/2004
			// Esta variable indicar� si la llamada viene de programaci�n. De ser as�, no se mostrar�
			// Lote, N�mero de Serie ni Fecha de Caducidad.
			General_Mat.EsProgramacion = Programacion;
			// ----

			General_Mat.VoFormulario = oForm;			
			Materiales.DefInstance.ShowDialog();

		}

		public void proLlama_Materiales(object oForm, int iTipoArt, SqlConnection rcConecta, object IgnorarTipoArticulo_optional, object SacarFarmacos_optional, object Almacen_optional, string ConsumosPac, bool ParaGestionporLotes)
		{
			proLlama_Materiales(oForm, iTipoArt, rcConecta, IgnorarTipoArticulo_optional, SacarFarmacos_optional, Almacen_optional, ConsumosPac, ParaGestionporLotes, false);
		}

		public void proLlama_Materiales(object oForm, int iTipoArt, SqlConnection rcConecta, object IgnorarTipoArticulo_optional, object SacarFarmacos_optional, object Almacen_optional, string ConsumosPac)
		{
			proLlama_Materiales(oForm, iTipoArt, rcConecta, IgnorarTipoArticulo_optional, SacarFarmacos_optional, Almacen_optional, ConsumosPac, false, false);
		}

		public void proLlama_Materiales(object oForm, int iTipoArt, SqlConnection rcConecta, object IgnorarTipoArticulo_optional, object SacarFarmacos_optional, object Almacen_optional)
		{
			proLlama_Materiales(oForm, iTipoArt, rcConecta, IgnorarTipoArticulo_optional, SacarFarmacos_optional, Almacen_optional, String.Empty, false, false);
		}

		public void proLlama_Materiales(object oForm, int iTipoArt, SqlConnection rcConecta, object IgnorarTipoArticulo_optional, object SacarFarmacos_optional)
		{
			proLlama_Materiales(oForm, iTipoArt, rcConecta, IgnorarTipoArticulo_optional, SacarFarmacos_optional, Type.Missing, String.Empty, false, false);
		}

		public void proLlama_Materiales(object oForm, int iTipoArt, SqlConnection rcConecta, object IgnorarTipoArticulo_optional)
		{
			proLlama_Materiales(oForm, iTipoArt, rcConecta, IgnorarTipoArticulo_optional, Type.Missing, Type.Missing, String.Empty, false, false);
		}

		public void proLlama_Materiales(object oForm, int iTipoArt, SqlConnection rcConecta)
		{
			proLlama_Materiales(oForm, iTipoArt, rcConecta, Type.Missing, Type.Missing, Type.Missing, String.Empty, false, false);
		}
	}
}