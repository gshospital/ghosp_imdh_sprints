using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;


namespace MaterialesDLL
{
	internal static class General_Mat
	{

		public static SqlConnection VRcMaterial = null;
		public static int ViTipoMat = 0;
		public static dynamic VoFormulario = null;
		public static bool bIgnorarTipoArticulo = false;
		public static bool bSacarFarmacos = false;
		public static string ElAlmacen = String.Empty;
		//oscar 13/03/03
		public static bool gConsumosPAC = false;
		//---
		public static bool GestionporLotes = false;
		//---
		public static bool EsProgramacion = false;
	}
}