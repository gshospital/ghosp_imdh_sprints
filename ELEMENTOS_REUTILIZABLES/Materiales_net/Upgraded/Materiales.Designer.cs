using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace MaterialesDLL
{
	partial class Materiales
	{

		#region "Upgrade Support "
		private static Materiales m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Materiales DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Materiales();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprArticulo", "tbLote", "tbSerie", "Label3", "Label4", "fraLote", "cbBuscar", "tbNombre", "lbNombre", "frCab", "chkConsumosHabituales", "cbCancelar", "cbAceptar", "cmdBuscarConsumosHab", "cbbSerDisp", "cbbSerPeti", "Label1", "Label2", "frmConsumosHabituales", "sprArticulo_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprArticulo;
		public Telerik.WinControls.UI.RadTextBox tbLote;
		public Telerik.WinControls.UI.RadTextBox tbSerie;
		public System.Windows.Forms.Label Label3;
		public System.Windows.Forms.Label Label4;
		public Telerik.WinControls.UI.RadGroupBox fraLote;
		public Telerik.WinControls.UI.RadButton cbBuscar;
		public Telerik.WinControls.UI.RadTextBox tbNombre;
		public System.Windows.Forms.Label lbNombre;
		public Telerik.WinControls.UI.RadGroupBox frCab;
		public Telerik.WinControls.UI.RadCheckBox chkConsumosHabituales;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cmdBuscarConsumosHab;
		//public AxMSForms.AxComboBox cbbSerDisp;
		public UpgradeHelpers.MSForms.MSCombobox cbbSerDisp;
        //public AxMSForms.AxComboBox cbbSerPeti;
        public UpgradeHelpers.MSForms.MSCombobox cbbSerPeti;
        public System.Windows.Forms.Label Label1;
		public System.Windows.Forms.Label Label2;
		public Telerik.WinControls.UI.RadGroupBox frmConsumosHabituales;

        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8;

        //	private FarPoint.Win.Spread.SheetView sprArticulo_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Materiales));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.sprArticulo = new UpgradeHelpers.Spread.FpSpread();
			this.fraLote = new Telerik.WinControls.UI.RadGroupBox();
			this.tbLote = new Telerik.WinControls.UI.RadTextBox();
			this.tbSerie = new Telerik.WinControls.UI.RadTextBox();
			this.Label3 = new System.Windows.Forms.Label();
			this.Label4 = new System.Windows.Forms.Label();
			this.frCab = new Telerik.WinControls.UI.RadGroupBox();
			this.cbBuscar = new Telerik.WinControls.UI.RadButton();
			this.tbNombre = new Telerik.WinControls.UI.RadTextBox();
			this.lbNombre = new System.Windows.Forms.Label();
			this.chkConsumosHabituales = new Telerik.WinControls.UI.RadCheckBox();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.frmConsumosHabituales = new Telerik.WinControls.UI.RadGroupBox();
			this.cmdBuscarConsumosHab = new Telerik.WinControls.UI.RadButton();
			this.cbbSerDisp = new UpgradeHelpers.MSForms.MSCombobox();
			this.cbbSerPeti = new UpgradeHelpers.MSForms.MSCombobox();
			this.Label1 = new System.Windows.Forms.Label();
			this.Label2 = new System.Windows.Forms.Label();
            gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.fraLote.SuspendLayout();
			this.frCab.SuspendLayout();
			this.frmConsumosHabituales.SuspendLayout();
			this.SuspendLayout();
			// 
			// sprArticulo
			// 
			this.sprArticulo.Location = new System.Drawing.Point(4, 100);
			this.sprArticulo.Name = "sprArticulo";
			this.sprArticulo.Size = new System.Drawing.Size(753, 297);
			this.sprArticulo.TabIndex = 1;
			this.sprArticulo.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprArticulo_CellClick);
            this.sprArticulo.AutoSize = false;
            this.sprArticulo.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprArticulo.MultiSelect = true;
            this.sprArticulo.ReadOnly = true;
            gridViewTextBoxColumn1.HeaderText = "Nombre";
            gridViewTextBoxColumn1.Name = "Col_Nombre";
            gridViewTextBoxColumn1.Width =330;
            gridViewTextBoxColumn2.HeaderText = "B";
            gridViewTextBoxColumn2.Name = "Col_B";
            gridViewTextBoxColumn2.Width = 120;
            gridViewTextBoxColumn3.HeaderText = "codigo";
            gridViewTextBoxColumn3.Name = "Col_cod";
            gridViewTextBoxColumn3.Width = 120;
            gridViewTextBoxColumn4.HeaderText = "Lote";
            gridViewTextBoxColumn4.Name = "Col_lote";
            gridViewTextBoxColumn4.Width = 120;
            gridViewTextBoxColumn5.HeaderText = "N� Serie";
            gridViewTextBoxColumn5.Name = "nserie";
            gridViewTextBoxColumn5.Width = 120;
            gridViewTextBoxColumn6.HeaderText = "C�digo";
            gridViewTextBoxColumn6.Name = "Col_codigo";
            gridViewTextBoxColumn6.Width = 120;
            gridViewTextBoxColumn7.HeaderText = "Fecha Caducidad";
            gridViewTextBoxColumn7.Name = "Col_FechaCaducidad";
            gridViewTextBoxColumn7.Width = 120;
            gridViewTextBoxColumn8.HeaderText = "PVP";
            gridViewTextBoxColumn8.Name = "Col_PVP";
            gridViewTextBoxColumn8.Width = 150;
            this.sprArticulo.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            // 
            // fraLote
            //            
            this.fraLote.Controls.Add(this.tbLote);
			this.fraLote.Controls.Add(this.tbSerie);
			this.fraLote.Controls.Add(this.Label3);
			this.fraLote.Controls.Add(this.Label4);
			this.fraLote.Enabled = true;
			this.fraLote.ForeColor = System.Drawing.SystemColors.ControlText;
			this.fraLote.Location = new System.Drawing.Point(498, 16);
			this.fraLote.Name = "fraLote";
			this.fraLote.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.fraLote.Size = new System.Drawing.Size(259, 57);
			this.fraLote.TabIndex = 12;
			this.fraLote.Visible = true;
			// 
			// tbLote
			// 
			this.tbLote.AcceptsReturn = true;			
			this.tbLote.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbLote.ForeColor = System.Drawing.SystemColors.WindowText;
			this.tbLote.Location = new System.Drawing.Point(52, 8);
			this.tbLote.MaxLength = 20;
			this.tbLote.Name = "tbLote";
			this.tbLote.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbLote.Size = new System.Drawing.Size(201, 21);
			this.tbLote.TabIndex = 14;
			this.tbLote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbLote_KeyPress);
			// 
			// tbSerie
			// 
			this.tbSerie.AcceptsReturn = true;			
			this.tbSerie.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbSerie.ForeColor = System.Drawing.SystemColors.WindowText;
			this.tbSerie.Location = new System.Drawing.Point(52, 30);
			this.tbSerie.MaxLength = 20;
			this.tbSerie.Name = "tbSerie";
			this.tbSerie.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbSerie.Size = new System.Drawing.Size(201, 21);
			this.tbSerie.TabIndex = 13;
			this.tbSerie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSerie_KeyPress);
			// 
			// Label3
			// 			
			this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;			
			this.Label3.Location = new System.Drawing.Point(6, 10);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(35, 13);
			this.Label3.TabIndex = 16;
			this.Label3.Text = "Lote:";
			// 
			// Label4
			// 			
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label4.Location = new System.Drawing.Point(6, 32);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(45, 13);
			this.Label4.TabIndex = 15;
			this.Label4.Text = "N� Serie:";
			// 
			// frCab
			// 			
			this.frCab.Controls.Add(this.cbBuscar);
			this.frCab.Controls.Add(this.tbNombre);
			this.frCab.Controls.Add(this.lbNombre);
			this.frCab.Enabled = true;
			this.frCab.ForeColor = System.Drawing.SystemColors.ControlText;
			this.frCab.Location = new System.Drawing.Point(2, 16);
			this.frCab.Name = "frCab";
			this.frCab.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frCab.Size = new System.Drawing.Size(493, 57);
			this.frCab.TabIndex = 0;
			this.frCab.Text = "Material ";
			this.frCab.Visible = true;
			// 
			// cbBuscar
			// 			
			this.cbBuscar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cbBuscar.Location = new System.Drawing.Point(414, 12);
			this.cbBuscar.Name = "cbBuscar";
			this.cbBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbBuscar.Size = new System.Drawing.Size(71, 38);
			this.cbBuscar.TabIndex = 17;
			this.cbBuscar.Text = "&Buscar";			
			this.cbBuscar.Click += new System.EventHandler(this.cbBuscar_Click);
			// 
			// tbNombre
			// 
			this.tbNombre.AcceptsReturn = true;			
			this.tbNombre.Cursor = System.Windows.Forms.Cursors.IBeam;			
			this.tbNombre.Location = new System.Drawing.Point(121, 21);
			this.tbNombre.MaxLength = 0;
			this.tbNombre.Name = "tbNombre";
			this.tbNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbNombre.Size = new System.Drawing.Size(280, 24);
			this.tbNombre.TabIndex = 4;
			this.tbNombre.Enter += new System.EventHandler(this.tbNombre_Enter);
			this.tbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombre_KeyPress);
			// 
			// lbNombre
			// 			
			this.lbNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lbNombre.Cursor = System.Windows.Forms.Cursors.Default;			
			this.lbNombre.Location = new System.Drawing.Point(14, 24);
			this.lbNombre.Name = "lbNombre";
			this.lbNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbNombre.Size = new System.Drawing.Size(75, 17);
			this.lbNombre.TabIndex = 5;
			this.lbNombre.Text = "Que contenga:";
			// 
			// chkConsumosHabituales
			// 			
			this.chkConsumosHabituales.CausesValidation = true;			
			this.chkConsumosHabituales.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.chkConsumosHabituales.Cursor = System.Windows.Forms.Cursors.Default;
			this.chkConsumosHabituales.Enabled = true;
			this.chkConsumosHabituales.ForeColor = System.Drawing.SystemColors.ControlText;
			this.chkConsumosHabituales.Location = new System.Drawing.Point(357, 3);
			this.chkConsumosHabituales.Name = "chkConsumosHabituales";
			this.chkConsumosHabituales.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.chkConsumosHabituales.Size = new System.Drawing.Size(133, 16);
			this.chkConsumosHabituales.TabIndex = 9;
			this.chkConsumosHabituales.TabStop = true;
			this.chkConsumosHabituales.Text = "Consumos Habituales";
			this.chkConsumosHabituales.Visible = false;
			this.chkConsumosHabituales.CheckStateChanged += new System.EventHandler(this.chkConsumosHabituales_CheckStateChanged);
			// 
			// cbCancelar
			// 			
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cbCancelar.Location = new System.Drawing.Point(676, 405);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 29);
			this.cbCancelar.TabIndex = 3;
			this.cbCancelar.Text = "&Cancelar";			
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 			
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;			
			this.cbAceptar.Location = new System.Drawing.Point(588, 405);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 2;
			this.cbAceptar.Text = "&Aceptar";			
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// frmConsumosHabituales
			// 			
			this.frmConsumosHabituales.Controls.Add(this.cmdBuscarConsumosHab);
			this.frmConsumosHabituales.Controls.Add(this.cbbSerDisp);
			this.frmConsumosHabituales.Controls.Add(this.cbbSerPeti);
			this.frmConsumosHabituales.Controls.Add(this.Label1);
			this.frmConsumosHabituales.Controls.Add(this.Label2);
			this.frmConsumosHabituales.Enabled = true;
			this.frmConsumosHabituales.ForeColor = System.Drawing.SystemColors.ControlText;
			this.frmConsumosHabituales.Location = new System.Drawing.Point(4, 16);
			this.frmConsumosHabituales.Name = "frmConsumosHabituales";
			this.frmConsumosHabituales.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmConsumosHabituales.Size = new System.Drawing.Size(493, 78);
			this.frmConsumosHabituales.TabIndex = 6;
			this.frmConsumosHabituales.Text = "Consumos Habituales";
			this.frmConsumosHabituales.Visible = false;           
            // 
            // cmdBuscarConsumosHab
            // 			
            this.cmdBuscarConsumosHab.Cursor = System.Windows.Forms.Cursors.Default;
			this.cmdBuscarConsumosHab.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdBuscarConsumosHab.Location = new System.Drawing.Point(416, 14);
			this.cmdBuscarConsumosHab.Name = "cmdBuscarConsumosHab";
			this.cmdBuscarConsumosHab.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdBuscarConsumosHab.Size = new System.Drawing.Size(71, 38);
			this.cmdBuscarConsumosHab.TabIndex = 18;
			this.cmdBuscarConsumosHab.Text = "&Buscar";			
			this.cmdBuscarConsumosHab.Click += new System.EventHandler(this.cmdBuscarConsumosHab_Click);
			// 
			// cbbSerDisp
			// 
			this.cbbSerDisp.Location = new System.Drawing.Point(123, 45);
			this.cbbSerDisp.Name = "cbbSerDisp";
			this.cbbSerDisp.Size = new System.Drawing.Size(271, 22);
			this.cbbSerDisp.TabIndex = 11;
			// 
			// cbbSerPeti
			// 
			this.cbbSerPeti.Location = new System.Drawing.Point(123, 18);
			this.cbbSerPeti.Name = "cbbSerPeti";
			this.cbbSerPeti.Size = new System.Drawing.Size(271, 22);
			this.cbbSerPeti.TabIndex = 10;
			// 
			// Label1
			// 			
			this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(14, 51);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(108, 17);
			this.Label1.TabIndex = 8;
			this.Label1.Text = "Servicio Dispensador:";
			// 
			// Label2
			// 			
			this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(14, 23);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(111, 17);
			this.Label2.TabIndex = 7;
			this.Label2.Text = "Servicio Peticionario:";
			// 
			// Materiales
			// 
			this.AcceptButton = this.cmdBuscarConsumosHab;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;			
			this.ClientSize = new System.Drawing.Size(763, 437);
			this.Controls.Add(this.sprArticulo);
			this.Controls.Add(this.fraLote);
			this.Controls.Add(this.frCab);
			this.Controls.Add(this.chkConsumosHabituales);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.frmConsumosHabituales);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Materiales";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Busqueda de materiales -(Materiales)";
			this.Activated += new System.EventHandler(this.Materiales_Activated);
			this.Closed += new System.EventHandler(this.Materiales_Closed);
			this.Load += new System.EventHandler(this.Materiales_Load);
			this.fraLote.ResumeLayout(false);
			this.frCab.ResumeLayout(false);
			this.frmConsumosHabituales.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}