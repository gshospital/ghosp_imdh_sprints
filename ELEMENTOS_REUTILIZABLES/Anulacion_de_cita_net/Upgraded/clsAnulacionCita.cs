using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace AnulacionCita
{
	public class clsAnulacionCita
	{
		public void Anulacion_Cita(int A�oCita, int NumeroCita, ref string tipoCita, ref SqlConnection Conexion, string usuario, object Motivo_optional, ref string BorrarPeticion)
		{
			string Motivo = (Motivo_optional == Type.Missing) ? String.Empty : Motivo_optional as string;
			DataSet rs = null;

			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			Serrores.AjustarRelojCliente(Conexion);
            			
			if (String.IsNullOrEmpty(BorrarPeticion))
			{
				BorrarPeticion = "S";
			}


			// Se obtienen los datos de la cita programada o de la cita pendiente de reprogramar. SI
			// el tipo de cita es "S" no se conoce el estado de la cita
			string stSql = "SELECT * ";
			if (tipoCita == "PROG" || tipoCita == "S")
			{
				stSql = stSql + "FROM ccitprog ";
			}

			else
			{
				stSql = stSql + "FROM ccitpend ";
			}
			stSql = stSql + "WHERE ganoregi = " + A�oCita.ToString() + " AND " + 
			        "gnumregi = " + NumeroCita.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
			DataSet rsc = new DataSet();
			tempAdapter.Fill(rsc);
			if (tipoCita == "S")
			{
				if (rsc.Tables[0].Rows.Count == 0)
				{
					stSql = "SELECT * " + 
					        "FROM ccitpend " + 
					        "WHERE ganoregi = " + A�oCita.ToString() + " AND " + 
					        "gnumregi = " + NumeroCita.ToString();
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion);
					rsc = new DataSet();
					tempAdapter_2.Fill(rsc);
					tipoCita = "PEND";
				}
				else
				{
					tipoCita = "PROG";
				}
			}

			// Si la cita esta programada
			if (tipoCita == "PROG")
			{

				// Se restauran los huecos utilizados por la cita
				
				Restaurar_Huecos(A�oCita, NumeroCita, Convert.ToString(rsc.Tables[0].Rows[0]["gagendas"]), Convert.ToInt32(rsc.Tables[0].Rows[0]["gbloques"]), ref Conexion, Type.Missing);

				if (BorrarPeticion == "S")
				{ //cita multiple una sola peticion para varias citas de la misma agenda

					//       stsql = "select * from ccitprog where gidenpac = '" & rsc!gidenpac & "' and " & _
					//'        "gagendas = '" & rsc!gagendas & "' and itipcita = 'M' and " & _
					//'        "ffeccita >= " & FormatFechaHM(Format(rsc!ffeccita, "dd/mm/yyyy") & " 00:00") & " AND " & _
					//'        "ffeccita <= " & FormatFechaHM(Format(rsc!ffeccita, "dd/mm/yyyy") & " 23:59")
					//
					//
					//        Set RscSql = Conexion.OpenResultset(stsql, rdOpenKeyset, rdConcurValues)
					//        If RscSql.RowCount > 1 Then
					//        'si hay mas de un registro no borro la peticion xq hay una sola peticion para varias citas multiples
					//        Else
					// Se anulan las posibles peticiones de historia generadas por la cita
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					Anular_Peticion_Historia(Convert.ToString(rsc.Tables[0].Rows[0]["gidenpac"]), Convert.ToString(rsc.Tables[0].Rows[0]["gagendas"]), Convert.ToDateTime(rsc.Tables[0].Rows[0]["ffeccita"]), Convert.ToDateTime(rsc.Tables[0].Rows[0]["fmecaniz"]), ref Conexion);

					//        End If
					//        RscSql.Close

					//'        ' Se anulan las posibles peticiones de historia generadas por la cita
					//'        Anular_Peticion_Historia rsc!gidenpac, rsc!gagendas, rsc!ffeccita, rsc!fmecaniz, Conexion
				}
				//Elimino la consulta del episodio de las pruebas de LEQ.
				SqlCommand tempCommand = new SqlCommand("UPDATE APREOLEQ SET irealiza='P', ganocita =Null,gnumcita=Null where irealiza='C' and ganocita =" + A�oCita.ToString() + " and gnumcita=" + NumeroCita.ToString(), Conexion);
				tempCommand.ExecuteNonQuery();

			}

			//se queda la prestacion pendiente en eprestac
			//''stsql = "SELECT * FROM eprestac WHERE iestsoli = 'C' AND itiprela = 'C' AND ganorela = " & A�oCita & " AND gnumrela = " & NumeroCita

			stSql = " update EPRESTAC SET " + 
			        " iestsoli = 'P', iestapcc=(case when iestapcc='C' then 'E' else iestapcc end), " + 
			        " itiprela = Null, " + 
			        " ganorela= Null, " + 
			        " gnumrela = Null ," + 
			        " fprevista = Null, " + 
			        " Gusuario = '" + usuario + "' " + 
			        " WHERE iestsoli = 'C' AND " + 
			        " itiprela = 'C' AND ganorela = " + A�oCita.ToString() + " AND gnumrela = " + NumeroCita.ToString();
			SqlCommand tempCommand_2 = new SqlCommand(stSql, Conexion);
			tempCommand_2.ExecuteNonQuery();

			//Miramos tambi�n en EPRESALT por si est� ya dado de alta el paciente
			stSql = " update EPRESALT SET " + 
			        " iestsoli = 'P', iestapcc=(case when iestapcc='C' then 'E' else iestapcc end), " + 
			        " itiprela = Null, " + 
			        " ganorela= Null, " + 
			        " gnumrela = Null ," + 
			        " fprevista = Null, " + 
			        " Gusuario = '" + usuario + "' " + 
			        " WHERE iestsoli = 'C' AND " + 
			        " itiprela = 'C' AND ganorela = " + A�oCita.ToString() + " AND gnumrela = " + NumeroCita.ToString();
			SqlCommand tempCommand_3 = new SqlCommand(stSql, Conexion);
			tempCommand_3.ExecuteNonQuery();
			//Anulamos lo que existe pendiente de env�o....

			stSql = "UPDATE CACTENVI SET IESTADOS = 'A', festados=getdate(), observac = 'Anulaci�n de cita.'  WHERE GANOREGI = " + A�oCita.ToString() + " AND GNUMREGI = " + NumeroCita.ToString() + " AND  IESTADOS = 'P' ";
			SqlCommand tempCommand_4 = new SqlCommand(stSql, Conexion);
			tempCommand_4.ExecuteNonQuery();

			// Se inserta una fila en citas anuladas con los datos de la cita programada
			// O.Frias - 29/05/2013
			// Se incorporan los nuevos campos de informacion clinica - MultiCita
			//O.Frias - 12/11/2015
			//Nuevo campo de fecha peticion origen
			
			//UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			object tempRefParam = rsc.Tables[0].Rows[0]["ffeccita"];
			stSql = "INSERT INTO ccitanul " + 
			        "(ganoregi, gnumregi, gidenpac, ncitapac, gagendas, " + 
			        "gbloques, gprestac, ffeccita, nordcita, gsersoli, " + 
			        "gmedsoli, omotaten, itipopac, itipcita, npriorid, " + 
			        "gservici, gpersona, gsalacon, fsolicit, gsocieda, " + 
			        "nafiliac, fmecaniz, gusuario, gmotanul, fanulaci, " + 
			        "gtipentr, gtipcita, fpropmed, fprhueco, numeroDU, isomalig, " + 
			        "ghospori, dmediori, " + 
			        "gtiporig, idpicorigenMUL, idnumercitaMUL, codcenoranuMUL, visibilidadMUL, ultespMUL, gtipmues, " + 
			        "ffecphlc, ffecphls, ffecphla, ffecmhlp, ffecmhlc, ilibrele, ginspecc, ivaloesp, gpriourg, dlaterMC, " + 
			        "omotexMC, dproyeMC, dtpetiMC, gdiagnos, ddiagnos, fpetiori )" + 
			        "VALUES(" + 
			        Convert.ToString(rsc.Tables[0].Rows[0]["ganoregi"]) + "," + Convert.ToString(rsc.Tables[0].Rows[0]["gnumregi"]) + ",'" + 
			        Convert.ToString(rsc.Tables[0].Rows[0]["gidenpac"]) + "'," + Convert.ToString(rsc.Tables[0].Rows[0]["ncitapac"]) + ",'" + 
			        Convert.ToString(rsc.Tables[0].Rows[0]["gagendas"]) + "'," + Convert.ToString(rsc.Tables[0].Rows[0]["gbloques"]) + ",'" + 
			        Convert.ToString(rsc.Tables[0].Rows[0]["gprestac"]) + "'," + Serrores.FormatFechaHM(tempRefParam) + "," + 
			        Convert.ToString(rsc.Tables[0].Rows[0]["nordcita"]) + ",";
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gsersoli"]))
			{
				stSql = stSql + "(Null)" + ",";
			}
			else
			{
				
				stSql = stSql + Convert.ToString(rsc.Tables[0].Rows[0]["gsersoli"]) + ",";
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gmedsoli"]))
			{
				stSql = stSql + "(Null)" + ",";
			}
			else
			{				
				stSql = stSql + Convert.ToString(rsc.Tables[0].Rows[0]["gmedsoli"]) + ",";
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["omotaten"]))
			{
				stSql = stSql + "(Null)" + ",'";
			}
			else
			{				
				stSql = stSql + "'" + Convert.ToString(rsc.Tables[0].Rows[0]["omotaten"]) + "','";
			}
			
			stSql = stSql + 
			        Convert.ToString(rsc.Tables[0].Rows[0]["itipopac"]) + "','" + Convert.ToString(rsc.Tables[0].Rows[0]["itipcita"]) + "'," + 
			        Convert.ToString(rsc.Tables[0].Rows[0]["npriorid"]) + ",";
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gservici"]))
			{
				stSql = stSql + "(Null)" + ",";
			}
			else
			{
				
				stSql = stSql + Convert.ToString(rsc.Tables[0].Rows[0]["gservici"]) + ",";
			}
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gpersona"]))
			{
				stSql = stSql + "(Null)" + ",";
			}
			else
			{				
				stSql = stSql + Convert.ToString(rsc.Tables[0].Rows[0]["gpersona"]) + ",";
			}
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gsalacon"]))
			{
				stSql = stSql + "(Null)" + ",";
			}
			else
			{				
				stSql = stSql + "'" + Convert.ToString(rsc.Tables[0].Rows[0]["gsalacon"]) + "',";
			}
						
			object tempRefParam2 = rsc.Tables[0].Rows[0]["fsolicit"];
			stSql = stSql + 
			        Serrores.FormatFecha(tempRefParam2) + ",";
						
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gsocieda"]))
			{
				stSql = stSql + "(Null),";
			}
			else
			{				
				stSql = stSql + Convert.ToString(rsc.Tables[0].Rows[0]["gsocieda"]) + ",";
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["nafiliac"]))
			{
				stSql = stSql + "(Null),";
			}
			else
			{				
				stSql = stSql + "'" + Convert.ToString(rsc.Tables[0].Rows[0]["nafiliac"]) + "',";
			}			
			
			object tempRefParam3 = rsc.Tables[0].Rows[0]["fmecaniz"];
			stSql = stSql + 
			        Serrores.FormatFechaHM(tempRefParam3) + ",'" + 
			        usuario + "',";
			if (Motivo_optional != Type.Missing)
			{
				stSql = stSql + 
				        "'" + Motivo + "',";
			}
			else
			{
				stSql = stSql + 
				        "(Null),";
			}
			object tempRefParam4 = DateTime.Now;
			stSql = stSql + 
			        "" + Serrores.FormatFechaHMS(tempRefParam4);

			//OSCAR C 16/06/2005
			
			stSql = stSql + " , " + Convert.ToString(rsc.Tables[0].Rows[0]["GTIPENTR"]) + " , " + Convert.ToString(rsc.Tables[0].Rows[0]["GTIPCITA"]);


			//diego 25/01/2007 - a�adimos las fechas de prescripci�n y de primer hueco encontrado
			//diego 08/02/2007 - controlo los nulls
			stSql = stSql + " , ";
            		
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["fpropmed"]))
			{
				stSql = stSql + "(Null),";
			}
			else
			{			
				
				object tempRefParam5 = rsc.Tables[0].Rows[0]["fpropmed"];
				stSql = stSql + Serrores.FormatFecha(tempRefParam5) + ", ";
			}
            		
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["fprhueco"]))
			{
				stSql = stSql + "(Null),";
			}
			else
			{			
				
				object tempRefParam6 = rsc.Tables[0].Rows[0]["fprhueco"];
				stSql = stSql + Serrores.FormatFechaHMS(tempRefParam6) + ", ";
			}

			//OSCAR C DU		
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["numeroDU"]))
			{
				stSql = stSql + "(Null), ";
			}
			else
			{				
				stSql = stSql + "'" + Convert.ToString(rsc.Tables[0].Rows[0]["numeroDU"]) + "', ";
			}
			//----

			//Sospecha de Malignidad
						
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["isomalig"]))
			{
				stSql = stSql + "(Null) ";
			}
			else
			{				
				stSql = stSql + "'" + Convert.ToString(rsc.Tables[0].Rows[0]["isomalig"]) + "'";
			}
			//--------------


			//Estos dos campos no tengo Ni Idea de porque no se metieron en su dia en la tabla de citas anuladas.
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ghospori"]))
			{
				stSql = stSql + ", (Null)";
			}
			else
			{				
				stSql = stSql + ", " + Convert.ToString(rsc.Tables[0].Rows[0]["ghospori"]);
			}
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["dmediori"]))
			{
				stSql = stSql + ", (Null)";
			}
			else
			{
				
				stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["dmediori"]) + "' ";
			}
			//-------------------------------------------

			//OSCAR C Octubre 2009			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gtiporig"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{				
				stSql = stSql + ", " + Convert.ToString(rsc.Tables[0].Rows[0]["gtiporig"]);
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["idpicorigenMUL"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{				
				stSql = stSql + ",'" + Convert.ToString(rsc.Tables[0].Rows[0]["idpicorigenMUL"]).Trim() + "'";
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["idnumercitaMUL"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{				
				stSql = stSql + ",'" + Convert.ToString(rsc.Tables[0].Rows[0]["idnumercitaMUL"]).Trim() + "'";
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["codcenoranuMUL"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{
				
				stSql = stSql + ",'" + Convert.ToString(rsc.Tables[0].Rows[0]["codcenoranuMUL"]).Trim() + "'";
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["visibilidadMUL"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{				
				stSql = stSql + ",'" + Convert.ToString(rsc.Tables[0].Rows[0]["visibilidadMUL"]).Trim() + "'";
			}
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ultespMUL"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{				
				stSql = stSql + ",'" + Convert.ToString(rsc.Tables[0].Rows[0]["ultespMUL"]).Trim() + "'";
			}
			//---------------
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gtipmues"]))
			{
				stSql = stSql + ",(Null)";
			}
			else
			{
				
				stSql = stSql + "," + Convert.ToString(rsc.Tables[0].Rows[0]["gtipmues"]) + " ";
			}
			//OSCAR C Noviembre 2010
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ffecphlc"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{
				
				object tempRefParam7 = rsc.Tables[0].Rows[0]["ffecphlc"];
				stSql = stSql + ", " + Serrores.FormatFechaHMS(tempRefParam7);
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ffecphls"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{				
				object tempRefParam8 = rsc.Tables[0].Rows[0]["ffecphls"];
				stSql = stSql + ", " + Serrores.FormatFechaHMS(tempRefParam8);
			}
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ffecphla"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{			
				
				object tempRefParam9 = rsc.Tables[0].Rows[0]["ffecphla"];
				stSql = stSql + ", " + Serrores.FormatFechaHMS(tempRefParam9);
			}
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ffecmhlp"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{					
				object tempRefParam10 = rsc.Tables[0].Rows[0]["ffecmhlp"];
				stSql = stSql + ", " + Serrores.FormatFechaHMS(tempRefParam10);
			}
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ffecmhlc"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{				
				object tempRefParam11 = rsc.Tables[0].Rows[0]["ffecmhlc"];
				stSql = stSql + ", " + Serrores.FormatFechaHMS(tempRefParam11);
			}
			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ilibrele"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{				
				stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["ilibrele"]).Trim() + "'";
			}


			//' O.Frias - 30/05/2011
			//' Incorporo la actualizaci�n del nuevo campo de inspecci�n.
						
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ginspecc"]))
			{
				stSql = stSql + ", (NULL) ";
			}
			else
			{				
				stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["ginspecc"]).Trim() + "'";
			}

			//Oscar C Febrero 2012. Indicador Valorado especialista			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ivaloesp"]))
			{
				stSql = stSql + ", (Null) ";
			}
			else
			{				
				stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["ivaloesp"]) + "'";
			}
			//--------------

			//Oscar C Junio 2012. Indicador Prioridad de la urgencia		
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gpriourg"]))
			{
				stSql = stSql + ", (Null) ";
			}
			else
			{				
				stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["gpriourg"]) + "'";
			}
			//--------------

			//Nuevos valores de informacion clinica MultiCita			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["dlaterMC"]))
			{
				stSql = stSql + ", (Null) ";
			}
			else
			{				
				stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["dlaterMC"]) + "'";
			}
            
			
			string stomotexMC = Convert.ToString(rsc.Tables[0].Rows[0]["omotexMC"]) + "";

			if (stomotexMC == "")
			{
				stSql = stSql + ", (Null) ";
			}
			else
			{
				stSql = stSql + ", '" + stomotexMC + "'";
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["dproyeMC"]))
			{
				stSql = stSql + ", (Null) ";
			}
			else
			{
				
				stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["dproyeMC"]) + "'";
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["dtpetiMC"]))
			{
				stSql = stSql + ", (Null) ";
			}
			else
			{				
				stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["dtpetiMC"]) + "'";
			}		
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["gdiagnos"]))
			{
				stSql = stSql + ", (Null) ";
			}
			else
			{
				
				if (Convert.ToString(rsc.Tables[0].Rows[0]["gdiagnos"]).Trim() == "")
				{
					stSql = stSql + ", (Null) ";
				}
				else
				{					
					stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["gdiagnos"]) + "'";
				}
			}			
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["ddiagnos"]))
			{
				stSql = stSql + ", (Null) ";
			}
			else
			{				
				if (Convert.ToString(rsc.Tables[0].Rows[0]["ddiagnos"]).Trim() == "")
				{
					stSql = stSql + ", (Null) ";
				}
				else
				{					
					stSql = stSql + ", '" + Convert.ToString(rsc.Tables[0].Rows[0]["ddiagnos"]) + "'";
				}
			}

			//O.Frias - 12/11/2015
			//Nuevo campo de fecha peticion origen	
			
			if (Convert.IsDBNull(rsc.Tables[0].Rows[0]["fpetiori"]))
			{
				stSql = stSql + ", (Null) ";
			}
			else
			{				
				object tempRefParam12 = rsc.Tables[0].Rows[0]["fpetiori"];
				stSql = stSql + ", " + Serrores.FormatFecha(tempRefParam12);
			}


			//---------------
			///diego 25/01/2007
			stSql = stSql + ")";
			//------

			SqlCommand tempCommand_5 = new SqlCommand(stSql, Conexion);
			tempCommand_5.ExecuteNonQuery();

			// Si la cita es de laboratorio y se est� anulando una cita se borran
			// los an�lisis solicitados para la petici�n
			if (Motivo_optional != Type.Missing)
			{
				stSql = "SELECT COUNT(*) AS Existe " + 
				        "FROM sysobjects " + 
				        "WHERE type = 'U' AND " + 
				        "name = 'danalsol'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
				rs = new DataSet();
				tempAdapter_3.Fill(rs);				
				if (Convert.ToDouble(rs.Tables[0].Rows[0]["Existe"]) > 0)
				{
					//UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
					
					object tempRefParam13 = rsc.Tables[0].Rows[0]["fmecaniz"];
					stSql = "DELETE FROM danalsol " + 
					        "WHERE itiposer = 'C' AND " + 
					        "ganoregi = " + Convert.ToString(rsc.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
					        "gnumregi = " + Convert.ToString(rsc.Tables[0].Rows[0]["gnumregi"]) + " AND " + 
					        "gprestac = '" + Convert.ToString(rsc.Tables[0].Rows[0]["gprestac"]) + "' AND " + 
					        "fpeticio = " + Serrores.FormatFechaHMS(tempRefParam13) + "";
					SqlCommand tempCommand_6 = new SqlCommand(stSql, Conexion);
					tempCommand_6.ExecuteNonQuery();
				}
			}

			// Se borra la cita programada o la cita pendiente			
			rsc.Edit();			
			//rsc.Delete(tempAdapter_3);

		}

		public void Anulacion_Cita(int A�oCita, int NumeroCita, ref string tipoCita, ref SqlConnection Conexion, string usuario, object Motivo_optional)
		{
			string tempRefParam3 = String.Empty;
			Anulacion_Cita(A�oCita, NumeroCita, ref tipoCita, ref Conexion, usuario, Motivo_optional, ref tempRefParam3);
		}

		public void Anulacion_Cita(int A�oCita, int NumeroCita, ref string tipoCita, ref SqlConnection Conexion, string usuario)
		{
			string tempRefParam4 = String.Empty;
			Anulacion_Cita(A�oCita, NumeroCita, ref tipoCita, ref Conexion, usuario, Type.Missing, ref tempRefParam4);
		}

		public void Restaurar_Huecos(int A�oCita, int NumeroCita, string Agenda, int Bloque, ref SqlConnection Conexion, object NumeroSesion)
		{
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			// Se obtienen los huecos utilizados por la cita y se restauran en
			string stSql = "SELECT * " + 
			               "FROM chueccit " + 
			               "WHERE ganoregi = " + A�oCita.ToString() + " AND " + 
			               "gnumregi = " + NumeroCita.ToString();
			if (NumeroSesion != Type.Missing)
			{
				stSql = stSql + " AND " + 
				        "nsesione = " + NumeroSesion.ToString();
			}
			else
			{
				stSql = stSql + " AND " + 
				        "nsesione = 0";
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
			DataSet rsh = new DataSet();
			tempAdapter.Fill(rsh);            		

			foreach (DataRow iteration_row in rsh.Tables[0].Rows)
			{

				// Se suma el hueco utilizado al punto de citacion y al dietario				
				object tempRefParam = iteration_row["fhueccit"];
				stSql = "UPDATE choraspu " + 
				        "SET npacnorm = npacnorm + " + Convert.ToString(iteration_row["npacnorm"]) + ", " + 
				        "npacpref = npacpref + " + Convert.ToString(iteration_row["npacpref"]) + ", " + 
				        "nminnorm = nminnorm + " + Convert.ToString(iteration_row["nminnorm"]) + ", " + 
				        "nminpref = nminpref + " + Convert.ToString(iteration_row["nminpref"]) + ", " + 
				        "nordcita = nordcita - 1 " + 
				        "WHERE gagendas = '" + Agenda + "' AND " + 
				        "gbloques = " + Bloque.ToString() + " AND " + 
				        "fdietari = " + Serrores.FormatFechaHM(tempRefParam) + "";
				SqlCommand tempCommand = new SqlCommand(stSql, Conexion);
				tempCommand.ExecuteNonQuery();

				
				object tempRefParam2 = iteration_row["fhueccit"];
				stSql = "UPDATE cdietari " + 
				        "SET npacnorm = npacnorm + " + Convert.ToString(iteration_row["npacnorm"]) + ", " + 
				        "npacpref = npacpref + " + Convert.ToString(iteration_row["npacpref"]) + ", " + 
				        "nminnorm = nminnorm + " + Convert.ToString(iteration_row["nminnorm"]) + ", " + 
				        "nminpref = nminpref + " + Convert.ToString(iteration_row["nminpref"]) + " " + 
				        "WHERE gagendas = '" + Agenda + "' AND " + 
				        "gbloques = " + Bloque.ToString() + " AND " + 
				        "fdietari = " + Serrores.FormatFecha(tempRefParam2) + "";
				SqlCommand tempCommand_2 = new SqlCommand(stSql, Conexion);
				tempCommand_2.ExecuteNonQuery();

				// Se borran los huecos utilizados				
				rsh.Edit();				
				rsh.Delete(tempAdapter);

			}

		}

		public void Anular_Peticion_Historia(string Paciente, string Agenda, System.DateTime FechaCita, System.DateTime FechaMecanizacion, ref SqlConnection Conexion)
		{
			DataSet rsc = null, rsh = null;
			DataSet rsDiasAnt = null;
			int DiasAntelacion = 0;
			System.DateTime dtFechaEntrega = DateTime.FromOADate(0);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			// Se obtienen los datos de la agenda donde esta programada la cita
			string stSql = "SELECT * FROM cagendas WHERE gagendas = '" + Agenda + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
			DataSet rsa = new DataSet();
			tempAdapter.Fill(rsa);

			// Si la agenda tiene peticion de historia automatica
			
			if (Convert.ToString(rsa.Tables[0].Rows[0]["ipetihis"]) == "S")
			{

				// Se obtiene el numero de dias de antelacion con que se pide la historia
				stSql = "SELECT nnumeri1 " + 
				        "FROM sconsglo " + 
				        "WHERE gconsglo = 'NDIASANT'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion);
				rsDiasAnt = new DataSet();
				tempAdapter_2.Fill(rsDiasAnt);
				
				DiasAntelacion = Convert.ToInt32(rsDiasAnt.Tables[0].Rows[0]["nnumeri1"]);

				//      dtFechaEntrega = Format(FechaCita, "dd/mm/yyyy")
				dtFechaEntrega = DateTime.Parse(FechaCita.ToString("dd/MM/yyyy HH:mm"));

				dtFechaEntrega = dtFechaEntrega.AddDays(-DiasAntelacion);

				if (DateTime.Parse(dtFechaEntrega.ToString("dd/MM/yyyy")) < DateTime.Parse(FechaMecanizacion.ToString("dd/MM/yyyy")))
				{
					//dtFechaEntrega = Format(FechaMecanizacion, "dd/mm/yyyy")
					dtFechaEntrega = DateTime.Parse(FechaMecanizacion.ToString("dd/MM/yyyy HH:mm"));
				}

				// Se comprueba si el paciente tiene historia en el archivo al que
				// que se pide desde la agenda				
				stSql = "SELECT * " + 
				        "FROM hdossier " + 
				        "WHERE gidenpac = '" + Paciente + "' AND " + 
				        "icontenido = 'H' AND " + 
				        "gserpropiet = " + Convert.ToString(rsa.Tables[0].Rows[0]["gserarch"]);
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, Conexion);
				rsh = new DataSet();
				tempAdapter_3.Fill(rsh);

				// Si no tiene historia se borra de peticiones sin historia
				if (rsh.Tables[0].Rows.Count == 0)
				{

					//stsql = "SELECT * " & _
					//"FROM hpetsinh " & _
					//"WHERE gidenpac = '" & Paciente & "' AND " & _
					//"gserpropiet = " & rsa!gserarch & " AND " & _
					//"fpreventreg >= " & FormatFechaHM(dtFechaEntrega & " 00:00") & "  AND " & _
					//"fpreventreg <= " & FormatFechaHM(dtFechaEntrega & " 23:59") & "  AND " & _
					//"gsersoli = " & rsa!gservici & " AND " & _
					//"icontenido = 'H' and gmedsoli = '" & rsa!gpersona & "' "				
					object tempRefParam = dtFechaEntrega;
					stSql = "SELECT * " + 
					        "FROM hpetsinh " + 
					        "WHERE gidenpac = '" + Paciente + "' AND " + 
					        "gserpropiet = " + Convert.ToString(rsa.Tables[0].Rows[0]["gserarch"]) + " AND " + 
					        "fpreventreg = " + Serrores.FormatFechaHM(tempRefParam) + "  AND " + 
					        "gsersoli = " + Convert.ToString(rsa.Tables[0].Rows[0]["gservici"]) + " AND " + 
					        "icontenido = 'H' and gmedsoli = '" + Convert.ToString(rsa.Tables[0].Rows[0]["gpersona"]) + "' ";
					dtFechaEntrega = Convert.ToDateTime(tempRefParam);

					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, Conexion);
					rsc = new DataSet();
					tempAdapter_4.Fill(rsc);
					if (rsc.Tables[0].Rows.Count > 0)
					{						
						rsc.MoveFirst();						
						rsc.Edit();            
						
						rsc.Delete(tempAdapter_4);
					}

					// Si tiene historia se borra de movimientos de consultas
				}
				else
				{

					//         stsql = "SELECT * " & _
					//"FROM hmovcons " & _
					//"WHERE ghistoria = " & rsh!ghistoria & " AND " & _
					//"icontenido = 'H' AND " & _
					//"gserpropiet = " & rsa!gserarch & " AND " & _
					//"gsersoli = " & rsa!gservici & " AND " & _
					//"fpreventreg >= " & FormatFechaHM(dtFechaEntrega & " 00:00") & " AND " & _
					//"fpreventreg <= " & FormatFechaHM(dtFechaEntrega & " 23:59") & " AND " & _
					//"IESTMOVIH <>'E' and gmedsoli = '" & rsa!gpersona & "' "
					//and " & _
					//'"gsalacon = '" & SalaConsulta & "'"				
					
					object tempRefParam2 = dtFechaEntrega;
					stSql = "SELECT * " + 
					        "FROM hmovcons " + 
					        "WHERE ghistoria = " + Convert.ToString(rsh.Tables[0].Rows[0]["ghistoria"]) + " AND " + 
					        "icontenido = 'H' AND " + 
					        "gserpropiet = " + Convert.ToString(rsa.Tables[0].Rows[0]["gserarch"]) + " AND " + 
					        "gsersoli = " + Convert.ToString(rsa.Tables[0].Rows[0]["gservici"]) + " AND " + 
					        "fpreventreg = " + Serrores.FormatFechaHM(tempRefParam2) + " AND " + 
					        "IESTMOVIH <>'E' and gmedsoli = '" + Convert.ToString(rsa.Tables[0].Rows[0]["gpersona"]) + "' AND " + 
					        "gsalacon = '" + Convert.ToString(rsa.Tables[0].Rows[0]["gsalacon"]) + "'";
					dtFechaEntrega = Convert.ToDateTime(tempRefParam2);
					//comprobar el spread de citas y comprobar q no existe otra peticion de historia igual
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSql, Conexion);
					rsc = new DataSet();
					tempAdapter_5.Fill(rsc);
					if (rsc.Tables[0].Rows.Count > 0)
					{						
						rsc.MoveFirst();						
						rsc.Edit();                       
                        rsc.Delete(tempAdapter_5);

                    }

                }

			}

		}
	}
}