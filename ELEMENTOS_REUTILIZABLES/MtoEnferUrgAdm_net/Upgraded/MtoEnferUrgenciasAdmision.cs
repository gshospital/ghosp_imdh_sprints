using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace MtoEnferUrgAdm
{
	public class MvtoEnferUrgAdm
	{

		//(maplaza)(07/04/2008)Ahora es 20, porque se a�aden las tablas "EPROCURA" (junto con "EPROCALT") y "EPLANPAC" (junto con "EPLANALT")
		//(ocontreras)(Enero 2009) ahora es 21, porque se a�ade la tabla "DVALORDM" (Ordenes medicas formato valoracion)
		//(ocontreras)(Septiembre 2009) Ahora es 26 porque se Se a�aden
		//                                        ERELACIO (junto con ERELAALT),  EMANIFES (junto con EMANIALT)
		//                                        EOBJETIV (junto con EOBJEALT),  EINTVCIO (junto con EINTVALT) y
		//                                        EVALUOBJ (junto con EVALUALT).
		const int NUMEROTABLAS = 27; //21 '20 '19 '18 '17 'antes era 14 diego 07-07-2006
		//-------
		string[] ArrayTablasOrigen = ArraysHelper.InitializeArray<string>(NUMEROTABLAS); // Guarda el conjunto de tablas a tratar origen
		string[] ArrayTablasDestino = ArraysHelper.InitializeArray<string>(NUMEROTABLAS); // Guarda el conjunto de tablas a tratar destino

		const int NUMEROTABLASHD = 38;
		string[] ArrayTablasOrigenHD = ArraysHelper.InitializeArray<string>(NUMEROTABLASHD); // Guarda el conjunto de tablas a tratar origen
		string[] ArrayTablasDestinoHD = ArraysHelper.InitializeArray<string>(NUMEROTABLASHD); // Guarda el conjunto de tablas a tratar destino


		// replica tablas de urgencias para que se vean en admision
		// inserta todos los apuntes de urgencias pero con el a�o,numero y servicio de admision
		// Operacion -> 0 : replica de urgencias a admision
		// Operacion -> 1 : borra todos los movimientos realizados

		// HayError : Se mandar� desde todos los sitios un FALSE
		// Cuando la llamada sea desde ANULACION DEL ALTA habra que tratar el error HayError
		// porque en caso de que haya un error distinto al de clave duplicada no se va a detectar
		// en ANULACION DEL ALTA porque aqui hay un controlador de errores(solo para el caso este)


		public void ReplicarTablasEnferUrgAdm(string A�oUrgencias, string NumeroUrgencias, string A�oAdmision, string NumeroAdmision, int Operacion, SqlConnection Conexion, bool HayError)
		{

			bool bErrorClaveDuplicada = false;

			string sqlCondicionAdmision = String.Empty;
			string sqlCondicionUrgencias = String.Empty;

			// las tablas para no tener que modificar c�digo cuando se cambie una
			// tabla
			StringBuilder sql = new StringBuilder();
			string sql2 = String.Empty;

			DataSet RrOrigen = null;
			DataSet RrDestino = null;



			object vText = null; //variable de campos text

			DataSet RrSql = null;

			string stFllegada = String.Empty;
			string stFechaPetMod = String.Empty;
			string stFechaPeticion = String.Empty;
			int i = 0;

			string ordenBorrado = String.Empty;

			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);

			proRellenarArrayTablas();

			// variable necesaria para ir independiente de la coleccion RDOERRORS
			if (Operacion == 0)
			{
				// replica de las movimientos de urgencias con el episodio de admision -> INGRESO


				sql = new StringBuilder("Select fllegada from AEPISADM where ganoadme =" + A�oAdmision + " and gnumadme=" + NumeroAdmision);

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql.ToString(), Conexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					stFllegada = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"]).ToString("yyyy-MM-dd");
				}
				RrSql.Close();


				sqlCondicionUrgencias = " where ganoregi =" + A�oUrgencias + " and gnumregi=" + NumeroUrgencias + 
				                        " and itiposer='U'";

				for (int ibucleTablas = 1; ibucleTablas <= NUMEROTABLAS; ibucleTablas++)
				{

					sql2 = "select * from " + ArrayTablasOrigen[ibucleTablas - 1] + " where 1=2";

					// cursor donde hacemos los AddNew
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql2, Conexion);
					RrOrigen = new DataSet();
					tempAdapter_2.Fill(RrOrigen);

					// por cada una de las tablas obtenemos las filas a mover del pasivo al activo
					// cuidando el posible error de clave duplicada
					// CUIDADO con Las tablas DANALSOL  ya que NO tienen tabla de alta correspondiente por lo que los datos
					// se deben obtener de la propia tabla.
					//If ibucleTablas <> NUMEROTABLAS Then
					//    sql = "select * from " & ArrayTablasDestino(ibucleTablas)
					//Else
					//    ' SI ES DANALSOL
					//    sql = "select * from " & ArrayTablasOrigen(ibucleTablas)
					//End If

					sql = new StringBuilder("select * from " + ArrayTablasDestino[ibucleTablas - 1]);

					//DIEGO 13-09-2006 - usamos >= porque la fecha de cierre se incluye en el periodo de la pauta
					switch(ArrayTablasDestino[ibucleTablas - 1])
					{
						case "EPAUCALT" : 
							sql.Append(sqlCondicionUrgencias + " and (fecfinal IS NULL OR fecfinal >= CONVERT(datetime,'" + stFllegada + "',121)) "); 
							break;
						case "DVALORDM" : 
							sql.Append(sqlCondicionUrgencias + " and fapuntes = (select max(fapuntes) from dvalordm " + sqlCondicionUrgencias + ")"); 
							break;
						default:
							sql.Append(sqlCondicionUrgencias); 
							break;
					}

					//Debug.Print sql
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql.ToString(), Conexion);
					RrDestino = new DataSet();
					tempAdapter_3.Fill(RrDestino);

					foreach (DataRow iteration_row in RrDestino.Tables[0].Rows)
					{ // por cada fila encontrada en las tablas de enfermeria
						//            rdoErrors.Clear
						RrOrigen.AddNew();
						RrOrigen.Tables[0].Rows[0]["itiposer"] = "H";
						RrOrigen.Tables[0].Rows[0]["ganoregi"] = A�oAdmision;
						RrOrigen.Tables[0].Rows[0]["gnumregi"] = NumeroAdmision;
						// recorremos los campos de las tablas implicadas a partir del campo
						// 4 porque los tres primeros se van a replicar con el episodio de admision
						for (int ibucleCampos = 3; ibucleCampos <= RrOrigen.Tables[0].Columns.Count - 1; ibucleCampos++)
						{
							if (iteration_row[ibucleCampos].GetType() == typeof(string))
							{
								vText = iteration_row[ibucleCampos];
								if (Convert.IsDBNull(vText))
								{
									RrOrigen.Tables[0].Rows[0][ibucleCampos] = DBNull.Value;
								}
								else
								{
									RrOrigen.Tables[0].Rows[0][ibucleCampos] = vText;
								}
								vText = null;
							}
							else
							{
								if (!Convert.IsDBNull(Convert.ToString(iteration_row[ibucleCampos]).Trim()))
								{
									RrOrigen.Tables[0].Rows[0][ibucleCampos] = iteration_row[ibucleCampos];
								}
								else
								{
									RrOrigen.Tables[0].Rows[0][ibucleCampos] = DBNull.Value;
								}
							}
							//Debug.Print " Campo : " & RrOrigen.rdoColumns(ibucleCampos).Name & " Valor Origen : " & RrDestino(ibucleCampos) & " Valor Destino : " & RrOrigen(ibucleCampos)
						}
						string tempQuery = RrOrigen.Tables[0].TableName;
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                        tempAdapter_2.Update(RrOrigen, RrOrigen.Tables[0].TableName);
					}
				}
				RrOrigen.Close();
				RrDestino.Close();



				//una vez que se han replicado las tablas de urgencias en Admision hay que cambiar
				//fpeticio de Prestaciones e Interconsultas
				//buscamos la fecha de llegada de admision
				sql = new StringBuilder("Select fllegada from AEPISADM where ganoadme =" + A�oAdmision + " and gnumadme=" + NumeroAdmision);

				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql.ToString(), Conexion);
				RrSql = new DataSet();
				tempAdapter_5.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					stFllegada = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:NN:SS");
				}
				RrSql.Close();


				//OSCAR C 10/03/2006
				object tempRefParam = stFllegada;
				sql = new StringBuilder(" UPDATE EDIETVIA SET ffinmovi=" + Serrores.FormatFechaHMS(tempRefParam) + 
				      " where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + 
				      "       and ffinmovi is null");
				stFllegada = Convert.ToString(tempRefParam);
				SqlCommand tempCommand = new SqlCommand(sql.ToString(), Conexion);
				tempCommand.ExecuteNonQuery();


				object tempRefParam2 = stFllegada;
				object tempRefParam3 = stFllegada;
				sql = new StringBuilder(" INSERT INTO EDIETVIA (itiposer, ganoregi, gnumregi, fapuntes, " + 
				      " ipaciaco, gtipodie, icsinsal, iconturm,idiabeti,gviaalim,oespdiet,ffinmovi,gusuario,fpasfact,iricafib,isupleme) " + 
				      " SELECT itiposer, ganoregi, gnumregi, " + Serrores.FormatFechaHMS(tempRefParam2) + ", " + 
				      "        ipaciaco, gtipodie, icsinsal, iconturm, idiabeti, gviaalim, oespdiet, NULL, gusuario, fpasfact,iricafib,isupleme " + 
				      " FROM EDIETVIA " + 
				      " where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + 
				      "  and ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam3));
				stFllegada = Convert.ToString(tempRefParam3);
				stFllegada = Convert.ToString(tempRefParam2);
				SqlCommand tempCommand_2 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_2.ExecuteNonQuery();
				//------


				sql = new StringBuilder("select * from EPRESTAC ");
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
				sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
				object tempRefParam4 = stFllegada;
				sql.Append(" and fpeticio < " + Serrores.FormatFechaHMS(tempRefParam4) + " ");
				stFllegada = Convert.ToString(tempRefParam4);


				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql.ToString(), Conexion);
				RrSql = new DataSet();
				tempAdapter_6.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
					RrSql.MoveFirst();
					i = 0;
					foreach (DataRow iteration_row_2 in RrSql.Tables[0].Rows)
					{
						stFechaPeticion = Convert.ToDateTime(iteration_row_2["fpeticio"]).ToString("dd/MM/yyyy HH:NN:SS");
						stFechaPetMod = DateTimeHelper.ToString(DateTime.Parse(stFllegada).AddSeconds(i));

						object tempRefParam5 = stFechaPetMod;
						sql = new StringBuilder("UPDATE EPRESTAC set fpeticio = " + Serrores.FormatFechaHMS(tempRefParam5) + " ");
						stFechaPetMod = Convert.ToString(tempRefParam5);
						System.DateTime TempDate = DateTime.FromOADate(0);
						if (Conversion.Val(Convert.ToDateTime(iteration_row_2["fsolicit"]).ToString("yyyyMMddHHNNSS")) < Conversion.Val((DateTime.TryParse(stFllegada, out TempDate)) ? TempDate.ToString("yyyyMMddHHNNSS") : stFllegada))
						{
							object tempRefParam6 = stFechaPetMod;
							sql.Append(", fsolicit = " + Serrores.FormatFechaHMS(tempRefParam6) + " ");
							stFechaPetMod = Convert.ToString(tempRefParam6);
						}
						sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
						sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
						object tempRefParam7 = stFechaPeticion;
						sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam7) + " ");
						stFechaPeticion = Convert.ToString(tempRefParam7);
						sql.Append(" and gprestac = '" + Convert.ToString(iteration_row_2["gprestac"]).Trim() + "' ");

						SqlCommand tempCommand_3 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_3.ExecuteNonQuery();

						object tempRefParam8 = stFechaPetMod;
						sql = new StringBuilder("UPDATE DANALSOL set fpeticio = " + Serrores.FormatFechaHMS(tempRefParam8) + " ");
						stFechaPetMod = Convert.ToString(tempRefParam8);
						sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
						object tempRefParam9 = stFechaPeticion;
						sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam9) + " ");
						stFechaPeticion = Convert.ToString(tempRefParam9);
						sql.Append(" and gprestac = '" + Convert.ToString(iteration_row_2["gprestac"]).Trim() + "' ");

						SqlCommand tempCommand_4 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_4.ExecuteNonQuery();

						//Modificamos los datos de la integraci�n, cambiando lo que apuntaba a urgencias para que apunte a planta.
						object tempRefParam10 = stFechaPetMod;
						sql = new StringBuilder("UPDATE DANALLAB set ganoregi = " + A�oAdmision + ", gnumregi = " + NumeroAdmision + ", itiposer='H', fpeticio = " + Serrores.FormatFechaHMS(tempRefParam10) + " ");
						stFechaPetMod = Convert.ToString(tempRefParam10);
						sql.Append(" where itiposer = 'U' and ganoregi =" + A�oUrgencias + " and gnumregi=" + NumeroUrgencias + " ");
						sql.Append(" and gprestac = '" + Convert.ToString(iteration_row_2["gprestac"]).Trim() + "' ");
						object tempRefParam11 = stFechaPeticion;
						sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam11) + " and icitpeti='P' ");
						stFechaPeticion = Convert.ToString(tempRefParam11);

						SqlCommand tempCommand_5 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_5.ExecuteNonQuery();

						object tempRefParam12 = stFechaPetMod;
						sql = new StringBuilder("UPDATE DOPENLAB set ganoregi = " + A�oAdmision + ", gnumregi = " + NumeroAdmision + ", itiposer='H', fpeticio = " + Serrores.FormatFechaHMS(tempRefParam12) + " ");
						stFechaPetMod = Convert.ToString(tempRefParam12);
						System.DateTime TempDate2 = DateTime.FromOADate(0);
						if (Conversion.Val(Convert.ToDateTime(iteration_row_2["fsolicit"]).ToString("yyyyMMddHHNNSS")) < Conversion.Val((DateTime.TryParse(stFllegada, out TempDate2)) ? TempDate2.ToString("yyyyMMddHHNNSS") : stFllegada))
						{
							object tempRefParam13 = stFechaPetMod;
							sql.Append(", fsolicit = " + Serrores.FormatFechaHMS(tempRefParam13) + " ");
							stFechaPetMod = Convert.ToString(tempRefParam13);
						}
						sql.Append(" where itiposer = 'U' and ganoregi =" + A�oUrgencias + " and gnumregi=" + NumeroUrgencias + " ");
						sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') and gprestac = '" + Convert.ToString(iteration_row_2["gprestac"]).Trim() + "' ");
						object tempRefParam14 = stFechaPeticion;
						sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam14) + "  and icitpeti='P' ");
						stFechaPeticion = Convert.ToString(tempRefParam14);

						SqlCommand tempCommand_6 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_6.ExecuteNonQuery();

						i++;

					}
				}
				RrSql.Close();

				sql = new StringBuilder("select * from EINTERCO ");
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
				sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
				object tempRefParam15 = stFllegada;
				sql.Append(" and fpeticio < " + Serrores.FormatFechaHMS(tempRefParam15) + " ");
				stFllegada = Convert.ToString(tempRefParam15);

				SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql.ToString(), Conexion);
				RrSql = new DataSet();
				tempAdapter_7.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
					RrSql.MoveFirst();
					i = 1;
					foreach (DataRow iteration_row_3 in RrSql.Tables[0].Rows)
					{
						stFechaPeticion = Convert.ToDateTime(iteration_row_3["fpeticio"]).ToString("dd/MM/yyyy HH:NN:SS");
						stFechaPetMod = DateTimeHelper.ToString(DateTime.Parse(stFllegada).AddSeconds(i));

						object tempRefParam16 = stFechaPetMod;
						sql = new StringBuilder("UPDATE EINTERCO set fpeticio = " + Serrores.FormatFechaHMS(tempRefParam16) + " ");
						stFechaPetMod = Convert.ToString(tempRefParam16);
						sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
						sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
						object tempRefParam17 = stFechaPeticion;
						sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam17) + " ");
						stFechaPeticion = Convert.ToString(tempRefParam17);

						SqlCommand tempCommand_7 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_7.ExecuteNonQuery();
						i++;
					}
				}
				RrSql.Close();

				// una vez que hemos actualizado la fecha previstsa hay que comprobar la fecha de comunicacion de la prestacion/interconsulta
				//fprevista
				sql = new StringBuilder("UPDATE EPRESTAC set fprevista = fpeticio ");
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
				sql.Append(" and iestsoli = 'C' ");
				sql.Append(" and fprevista < fpeticio and fprevista is not null");

				SqlCommand tempCommand_8 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_8.ExecuteNonQuery();

				sql = new StringBuilder("UPDATE EINTERCO set fprevista = fpeticio ");
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
				sql.Append(" and iestsoli = 'C' ");
				sql.Append(" and fprevista < fpeticio and fprevista is not null");

				SqlCommand tempCommand_9 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_9.ExecuteNonQuery();

				//Oscar C Enero 2009
				//Al copiar las ordenes medicas en formato valoracion del episodio de urgencias, se deben copiar con la
				//fecha de ingreso del episodio destino.
				object tempRefParam18 = stFllegada;
				sql = new StringBuilder("UPDATE DVALORDM set fapuntes =  " + Serrores.FormatFechaHMS(tempRefParam18));
				stFllegada = Convert.ToString(tempRefParam18);
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");

				SqlCommand tempCommand_10 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_10.ExecuteNonQuery();
				//------------------


			}
			else if (Operacion == 1)
			{  // ANULACION DEL INGRESO EN ADMISION
				// BORRAMOS LOS APUNTES DE ADMISION
				sqlCondicionAdmision = "where ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + 
				                       " and itiposer='H'";


				for (int ibucleTablas = 1; ibucleTablas <= NUMEROTABLAS; ibucleTablas++)
				{

					if (ArrayTablasOrigen[ibucleTablas - 1] == "ETOMASFA")
					{
						ordenBorrado = "EFARMACO";
					}
					else if (ArrayTablasOrigen[ibucleTablas - 1] == "EFARMACO")
					{ 
						ordenBorrado = "ETOMASFA";
					}
					else
					{
						ordenBorrado = ArrayTablasOrigen[ibucleTablas - 1];
					}

					sql = new StringBuilder(" delete " + ordenBorrado + " " + sqlCondicionAdmision);
					SqlCommand tempCommand_11 = new SqlCommand(sql.ToString(), Conexion);
					tempCommand_11.ExecuteNonQuery();
				}

				sql = new StringBuilder(" delete dopenlab " + sqlCondicionAdmision);
				SqlCommand tempCommand_12 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_12.ExecuteNonQuery();

				sql = new StringBuilder(" delete danallab " + sqlCondicionAdmision);
				SqlCommand tempCommand_13 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_13.ExecuteNonQuery();


				//Si existen prestaciones de laboratorio de urgencias
				sql = new StringBuilder("select * from epresalt inner join dcodpres on epresalt.gprestac = dcodpres.gprestac inner join dservici on dservici.gservici = dcodpres.gservici ");
				sql.Append("where itiposer='u' and ganoregi=" + A�oUrgencias + "  and gnumregi= " + NumeroUrgencias + " and iestsoli in ('P','C') and iservlab='S'");
				SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(sql.ToString(), Conexion);
				RrSql = new DataSet();
				tempAdapter_8.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					//Volvemos a volcar a la integraci�n, esas prestaciones de urgencias.

					//Primero las determinaciones de las prestaciones que sean protocolo
					sql = new StringBuilder("insert into danallab (itiposer,ganoregi,gnumregi,fpeticio,gprestac,gidenpac,gpruelab,error,icitpeti,iestfact,ncantida,");
					sql.Append("oresulta,nresulta,maxresul,minresul,uniresul) select epresalt.itiposer,epresalt.ganoregi,epresalt.gnumregi,");
					sql.Append("epresalt.fpeticio,epresalt.gprestac,gidenpac,gpruelab,null,'P',null,epresalt.ncantida,null,null,null,null,null ");
					sql.Append("from epresalt inner join dcodpres on epresalt.gprestac = dcodpres.gprestac inner join dservici on dservici.gservici = dcodpres.gservici ");
					sql.Append("inner join uepisurg on uepisurg.ganourge = epresalt.ganoregi and uepisurg.gnumurge = epresalt.gnumregi ");
					sql.Append("inner join danalsol on epresalt.itiposer = danalsol.itiposer and epresalt.ganoregi = danalsol.ganoregi and ");
					sql.Append("epresalt.gnumregi = danalsol.gnumregi and epresalt.gprestac = danalsol.gprestac and ");
					sql.Append("epresalt.fpeticio = danalsol.fpeticio ");
					sql.Append("where epresalt.itiposer='U' and epresalt.ganoregi=" + A�oUrgencias + "  and epresalt.gnumregi= " + NumeroUrgencias + " and iestsoli in ('P','C') and iservlab='S'");
					SqlCommand tempCommand_14 = new SqlCommand(sql.ToString(), Conexion);
					tempCommand_14.ExecuteNonQuery();
					//Despu�s los perfiles
					sql = new StringBuilder("insert into dopenlab select itiposer,ganoregi,gnumregi,fpeticio,epresalt.gprestac, gidenpac,null,ipriorid,iestsoli,omotisol,frealiza, ");
					sql.Append("fanulaci,gsersoli,gpersoli,ncantida,gperreal,'N','N' ");
					sql.Append(",null,null  ,gsocieda,null,'N','P',fsolicit,null,gtipmues,null, ginspecc,null ");
					sql.Append("from epresalt inner join dcodpres on epresalt.gprestac = dcodpres.gprestac inner join dservici on dservici.gservici = dcodpres.gservici ");
					sql.Append("inner join uepisurg on uepisurg.ganourge = epresalt.ganoregi and uepisurg.gnumurge = epresalt.gnumregi ");
					sql.Append(" where itiposer='U' and ganoregi=" + A�oUrgencias + "  and gnumregi= " + NumeroUrgencias + " and iestsoli in ('P','C') and iservlab='S'");
					SqlCommand tempCommand_15 = new SqlCommand(sql.ToString(), Conexion);
					tempCommand_15.ExecuteNonQuery();
				}
				RrSql.Close();



			}
			else if (Operacion == 2)
			{  // Anulacion del alta de admision

				// Abrir una cursor y por cada fila hacer un ADDNEW
				// ATRAPAR EL ERROR Y COMPROBAR QUE NO ES CLAVE DUPLICADA -> error -> 23000
				// SI LO ES HACER UN RESUME NEXT

				try
				{

					sqlCondicionUrgencias = " where ganoregi =" + A�oUrgencias + " and gnumregi=" + NumeroUrgencias + 
					                        " and itiposer='U'";


					//Oscar C Enero 2009
					//Al anular el alta de admision, no debemos hacer nada con la tabla DVALORDM (que debe estar en la ultima
					//posicion del array) por lo que en el bucle la exluimos
					//Septiembre 2009: si preguntamos por la tabla a tratar ya no hace falta que DVALORDM este en la utlima posicion del array
					for (int ibucleTablas = 1; ibucleTablas <= NUMEROTABLAS; ibucleTablas++)
					{ //- 1

						if (ArrayTablasOrigen[ibucleTablas - 1] != "DVALORDM" && ArrayTablasOrigen[ibucleTablas - 1] != "EFARCOMU")
						{

							sql2 = "select * from " + ArrayTablasOrigen[ibucleTablas - 1] + " where 1=2";

							// cursor donde hacemos los AddNew
							SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(sql2, Conexion);
							RrOrigen = new DataSet();
							tempAdapter_9.Fill(RrOrigen);
							// por cada una de las tablas obtenemos las filas a mover del pasivo al activo
							// cuidando el posible error de clave duplicada
							// CUIDADO con Las tablas DANALSOL Y DVALORDM ya que NO tienen tabla de alta correspondiente por lo que los datos
							// se deben obtener de la propia tabla.
							//If ibucleTablas <> NUMEROTABLAS Then
							//    sql = "select * from " & ArrayTablasDestino(ibucleTablas)
							//Else   ' SI ES DANALSOL
							//    sql = "select * from " & ArrayTablasOrigen(ibucleTablas)
							//End If

							sql = new StringBuilder("select * from " + ArrayTablasDestino[ibucleTablas - 1]);

							sql.Append(sqlCondicionUrgencias);

							SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(sql.ToString(), Conexion);
							RrDestino = new DataSet();
							tempAdapter_10.Fill(RrDestino);
							foreach (DataRow iteration_row_4 in RrDestino.Tables[0].Rows)
							{ // por cada fila encontrada en las tablas de enfermeria
								//            rdoErrors.Clear
								RrOrigen.AddNew();
								RrOrigen.Tables[0].Rows[0]["itiposer"] = "H";
								RrOrigen.Tables[0].Rows[0]["ganoregi"] = A�oAdmision;
								RrOrigen.Tables[0].Rows[0]["gnumregi"] = NumeroAdmision;
								// recorremos los campos de las tablas implicadas a partir del campo
								// 4 porque los tres primeros se van a replicar con el episodio de admision
								for (int ibucleCampos = 3; ibucleCampos <= RrOrigen.Tables[0].Columns.Count - 1; ibucleCampos++)
								{
									if (iteration_row_4[ibucleCampos].GetType() == typeof(string))
									{
										vText = iteration_row_4[ibucleCampos];
										if (Convert.IsDBNull(vText))
										{
											RrOrigen.Tables[0].Rows[0][ibucleCampos] = DBNull.Value;
										}
										else
										{
											RrOrigen.Tables[0].Rows[0][ibucleCampos] = vText;
										}
										vText = null;
									}
									else
									{
										if (!Convert.IsDBNull(iteration_row_4[ibucleCampos]))
										{
											RrOrigen.Tables[0].Rows[0][ibucleCampos] = iteration_row_4[ibucleCampos];
										}
										else
										{
											RrOrigen.Tables[0].Rows[0][ibucleCampos] = DBNull.Value;
										}
									}
									//                    Debug.Print " Campo : " & RrOrigen.rdoColumns(ibucleCampos).Name & " Valor Origen : " & RrDestino(ibucleCampos) & " Valor Destino : " & RrOrigen(ibucleCampos)
								}
								string tempQuery_2 = RrOrigen.Tables[0].TableName;
								SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_9);
                                tempAdapter_9.Update(RrOrigen, RrOrigen.Tables[0].TableName);
							}

						}
					}
					RrOrigen.Close();
					RrDestino.Close();

					sql = new StringBuilder("Select fllegada from AEPISADM where ganoadme =" + A�oAdmision + " and gnumadme=" + NumeroAdmision);


					SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(sql.ToString(), Conexion);
					RrSql = new DataSet();
					tempAdapter_12.Fill(RrSql);
					if (RrSql.Tables[0].Rows.Count != 0)
					{
						stFllegada = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:NN:SS");
					}
					RrSql.Close();
					//Eliminar las interconsultas y las prestaciones con fecha de peticion < que fllegada y
					//con estado "P" o "C"
					sql = new StringBuilder("select * from EPRESTAC ");
					sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
					sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
					object tempRefParam19 = stFllegada;
					sql.Append(" and fpeticio < " + Serrores.FormatFechaHMS(tempRefParam19) + " ");
					stFllegada = Convert.ToString(tempRefParam19);

					SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(sql.ToString(), Conexion);
					RrSql = new DataSet();
					tempAdapter_13.Fill(RrSql);

					if (RrSql.Tables[0].Rows.Count != 0)
					{
						RrSql.MoveFirst();
						foreach (DataRow iteration_row_5 in RrSql.Tables[0].Rows)
						{
							stFechaPeticion = Convert.ToDateTime(iteration_row_5["fpeticio"]).ToString("dd/MM/yyyy HH:NN:SS");

							sql = new StringBuilder("delete EPRESTAC where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
							sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
							object tempRefParam20 = stFechaPeticion;
							sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam20) + " ");
							stFechaPeticion = Convert.ToString(tempRefParam20);

							SqlCommand tempCommand_16 = new SqlCommand(sql.ToString(), Conexion);
							tempCommand_16.ExecuteNonQuery();

							sql = new StringBuilder("delete DANALSOL where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
							object tempRefParam21 = stFechaPeticion;
							sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam21) + " ");
							stFechaPeticion = Convert.ToString(tempRefParam21);

							SqlCommand tempCommand_17 = new SqlCommand(sql.ToString(), Conexion);
							tempCommand_17.ExecuteNonQuery();

						}
					}
					RrSql.Close();

					//a continuacion elimino las interconsultas
					sql = new StringBuilder("delete EINTERCO where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
					sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
					object tempRefParam22 = stFllegada;
					sql.Append(" and fpeticio < " + Serrores.FormatFechaHMS(tempRefParam22) + " ");
					stFllegada = Convert.ToString(tempRefParam22);

					SqlCommand tempCommand_18 = new SqlCommand(sql.ToString(), Conexion);
					tempCommand_18.ExecuteNonQuery();

					return;
				}
				catch (SqlException ex)
				{
					// recorrer RdoErros(i) para ver si encuentra el error 2627 -> clave duplicada
					bErrorClaveDuplicada = false;
                    var errors = new RDO_rdoErrors(ex);

                    foreach (SqlError er in errors.rdoErrors)
					{
						//For ibucleErrores = 0 To rdoErrors.Count - 1
						if (er.Number == 2627)
						{
							bErrorClaveDuplicada = true;
							//                MsgBox "Clave duplicada en " & ArrayTablasOrigen(ibucleTablas)
							if (RrOrigen.DesignMode)
							{ // rdEditAdd si hay un proceso de addnew si update
								RrOrigen.Tables[0].RejectChanges(); //  cancelo la operacion de actualizacion que
							}
                            errors.Clear();
							//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement");
							//                Exit For
						}
					}
					if (!bErrorClaveDuplicada)
					{ // si hay error en algun caso y el error no es por clave
						// duplicada entonces ponemos HAYERROR  = TRUE para que
						// ANULACION DE ALTA  lo trate
						//        MsgBox "Clave duplicada en " & ArrayTablasOrigen(ibucleTablas)
						HayError = true;
					}
				}
                catch (Exception exception)
                {
                    System.Console.Write(exception.Message);
                }
            }
		}



		private void proRellenarArrayTablas()
		{

			ArrayTablasOrigen[0] = "EINTERCO";
			ArrayTablasOrigen[1] = "ECURVEND";
			ArrayTablasOrigen[2] = "EMEDIDAS";
			ArrayTablasOrigen[3] = "EOBSENFE";
			ArrayTablasOrigen[4] = "EFARMACO";
			//ArrayTablasOrigen(5) = "ETOMASFA" 'EFARMACO
			ArrayTablasOrigen[5] = "EPRESTAC";
			ArrayTablasOrigen[6] = "ECUIDADO";
			ArrayTablasOrigen[7] = "ECONSVIT";
			ArrayTablasOrigen[8] = "EPROBENF";
			//ArrayTablasOrigen(10) = "EFARMACO" 'ETOMASFA
			ArrayTablasOrigen[9] = "ETOMASFA";
			ArrayTablasOrigen[10] = "ECONFCUI";
			ArrayTablasOrigen[11] = "EDIETVIA";
			ArrayTablasOrigen[12] = "EULCERAS";
			ArrayTablasOrigen[13] = "EBALANCE"; //nueva
			ArrayTablasOrigen[14] = "EGLUCEMI"; //nueva
			ArrayTablasOrigen[15] = "EPAUCORR"; //nueva
			ArrayTablasOrigen[16] = "EORDMEDI"; //ordenes medicas
			ArrayTablasOrigen[17] = "EPROCURA"; //Curas y Vendajes( maplaza)(11/02/2008)
			ArrayTablasOrigen[18] = "EPLANPAC"; //Planes de Cuidados del Paciente (maplaza)(07/04/2008)
			ArrayTablasOrigen[19] = "DANALSOL"; //ATENTOS PORQUE LA TABLA DANALSOL NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			ArrayTablasOrigen[20] = "DVALORDM"; //Ordenes Medicas Formato Valoracion (Oscar C Enero 2009) ATENTOS PORQUE LA TABLA DVALORDM NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA SI NO DE LA TABLA NORMAL
			//Oscar C Septiembre 2009
			ArrayTablasOrigen[21] = "ERELACIO";
			ArrayTablasOrigen[22] = "EMANIFES";
			ArrayTablasOrigen[23] = "EOBJETIV";
			ArrayTablasOrigen[24] = "EINTVCIO";
			ArrayTablasOrigen[25] = "EVALUOBJ";

			ArrayTablasOrigen[26] = "EFARCOMU";

			//---------------


			ArrayTablasDestino[0] = "EINTEALT";
			ArrayTablasDestino[1] = "ECURVALT";
			ArrayTablasDestino[2] = "EMEDIALT";
			ArrayTablasDestino[3] = "EOBSEALT";
			//ArrayTablasDestino(5) = "ETOMAALT" 'EFARMALT
			ArrayTablasDestino[4] = "EFARMALT";
			ArrayTablasDestino[5] = "EPRESALT";
			ArrayTablasDestino[6] = "ECUIDALT";
			ArrayTablasDestino[7] = "ECONSALT";
			ArrayTablasDestino[8] = "EPROBALT";
			//ArrayTablasDestino(10) = "EFARMALT" 'ETOMAALT
			ArrayTablasDestino[9] = "ETOMAALT";
			ArrayTablasDestino[10] = "ECONFALT";
			ArrayTablasDestino[11] = "EDIETALT";
			ArrayTablasDestino[12] = "EULCEALT";
			ArrayTablasDestino[13] = "EBALAALT"; //nueva
			ArrayTablasDestino[14] = "EGLUCALT"; //nueva
			ArrayTablasDestino[15] = "EPAUCALT"; //nueva
			ArrayTablasDestino[16] = "EORDMALT"; //ordenes medicas
			ArrayTablasDestino[17] = "EPROCALT"; //Curas y Vendajes (maplaza)(11/02/2008)
			ArrayTablasDestino[18] = "EPLANALT"; //Planes de Cuidados del Paciente (maplaza)(07/04/2008)
			ArrayTablasDestino[19] = "DANALSOL"; //ATENTOS PORQUE LA TABLA DANALSOL Y DVALORDM NO TIENEN TABLA DE ALTA
			ArrayTablasDestino[20] = "DVALORDM"; //ATENTOS PORQUE LA TABLA DANALSOL Y DVALORDM NO TIENEN TABLA DE ALTA
			//Oscar C Septiembre 2009
			ArrayTablasDestino[21] = "ERELAALT";
			ArrayTablasDestino[22] = "EMANIALT";
			ArrayTablasDestino[23] = "EOBJEALT";
			ArrayTablasDestino[24] = "EINTVALT";
			ArrayTablasDestino[25] = "EVALUALT";
			//---------------
			ArrayTablasDestino[26] = "EFARCOMU";


		}

		// replica tablas de HD para que se vean en admision
		// inserta todos los apuntes de HD pero con el a�o,numero y servicio de admision
		// Operacion -> 0 : replica de HD a admision
		// Operacion -> 1 : borra todos los movimientos realizados

		// HayError : Se mandar� desde todos los sitios un FALSE
		// Cuando la llamada sea desde ANULACION DEL ALTA habra que tratar el error HayError
		// porque en caso de que haya un error distinto al de clave duplicada no se va a detectar
		// en ANULACION DEL ALTA porque aqui hay un controlador de errores(solo para el caso este)


		public void ReplicarTablasEnferHDAdm(string A�oAdmisionHD, string NumeroAdmisionHD, string A�oAdmision, string NumeroAdmision, int Operacion, SqlConnection Conexion)
		{



			string sqlCondicionAdmisionHD = String.Empty;

			// las tablas para no tener que modificar c�digo cuando se cambie una
			// tabla
			StringBuilder sql = new StringBuilder();
			string sql2 = String.Empty;

			DataSet RrOrigen = null;
			DataSet RrDestino = null;



			object vText = null; //variable de campos text

			DataSet RrSql = null;

			string stFllegada = String.Empty;
			string stFllegadaYMD = String.Empty;
			string stFechaPetMod = String.Empty;
			string stFechaPeticion = String.Empty;
			int i = 0;
			string stgprestac = String.Empty;

			string ordenBorrado = String.Empty;

			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);

			proRellenarArrayTablasHD();

			if (Operacion == 0)
			{

				// replica de las movimientos de Hospital de DIA con el episodio de admision -> INGRESO

				sql = new StringBuilder("Select fllegada, ganrelur, gnurelur from AEPISADM where ganoadme =" + A�oAdmision + " and gnumadme=" + NumeroAdmision);

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql.ToString(), Conexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					//Si el episodio de hospitalizaci�n viene de urgencias (circuito en el mismo d�a hospital de d�a - urgencias - hospitalizaci�n) no se realiza la replica de las tablas de hospital de d�a
					if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ganrelur"]) && !Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gnurelur"]))
					{
						RrSql.Close();
						return;
					}
					stFllegadaYMD = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"]).ToString("yyyy-MM-dd");
					stFllegada = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fllegada"]).ToString("dd/MM/yyyy HH:NN:SS");
				}
				RrSql.Close();


				for (int ibucleTablas = 3; ibucleTablas <= NUMEROTABLASHD; ibucleTablas++)
				{ //1 'Diciembre 2012. Por orden de curia segun correo de 12 de noviembre de 2012, ya no se replica la valoracion de enfermeria

					//Debug.Print "ORIGEN:" & ArrayTablasDestinoHD(ibucleTablas)
					//Debug.Print "DESTINO:" & ArrayTablasOrigenHD(ibucleTablas)

					switch(ArrayTablasDestinoHD[ibucleTablas - 1])
					{
						case "EINCIDEN" : 
							sqlCondicionAdmisionHD = " where ganoregi =" + A�oAdmisionHD + " and gnumregi=" + NumeroAdmisionHD; 
							break;
						default:
							sqlCondicionAdmisionHD = " where itiposer='H' and ganoregi =" + A�oAdmisionHD + " and gnumregi=" + NumeroAdmisionHD; 
							break;
					}

					sql2 = "select * from " + ArrayTablasOrigenHD[ibucleTablas - 1] + " where 1=2";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql2, Conexion);
					RrOrigen = new DataSet();
					tempAdapter_2.Fill(RrOrigen);

					sql = new StringBuilder("select * from " + ArrayTablasDestinoHD[ibucleTablas - 1]);

					switch(ArrayTablasDestinoHD[ibucleTablas - 1])
					{
						case "EPAUCALT" : 
							sql.Append(sqlCondicionAdmisionHD + " and (fecfinal IS NULL OR fecfinal >= CONVERT(datetime,'" + stFllegadaYMD + "',121)) "); 
							break;
						case "DVALORDM" : 
							sql.Append(sqlCondicionAdmisionHD + " and fapuntes = (select max(fapuntes) from DVALORDM " + sqlCondicionAdmisionHD + ")"); 
							break;
						case "EFARMALT" : 
							sql.Append(sqlCondicionAdmisionHD + " AND iesttrat='I' AND ffinmovi is null"); 
							break;
						case "EPRESALT" : 
							//Puede ocurrir el siguiente problema 
							//Se ha generado una programaci�n quir�rgica para el d�a del ingreso 
							//Se ha ingresado en hospital de d�a quir�rgico con lo que se ha registrado una prestaci�n en ese episodio asociada a la programaci�n 
							//Se ha dado de alta de hospital de d�a quir�rgico y posteriormente ingresado en hospitalizaci�n en el mismo d�a 
							//Si se ingresa en hospitalizaci�n como programado mira si hay una programaci�n quir�rgica para ese d�a y en caso afirmativo registra una prestaci�n con esa programaci�n 
							//Como tambi�n hay un hospital de d�a dado de alta en el d�a convierte las prestaciones de ese episodio al de hospitalzaci�n (actualiza itiposer, ganoregi ,gnumregi) 
							//Entonces aqu� al hacer el update da error por clave itiposer, ganoregi, gnumregi , gprestac, fpeticio 
							sql.Append(sqlCondicionAdmisionHD + " AND (iestsoli='P' or iestsoli='C') "); 
							sql.Append(" and not exists (select '' from eprestac E where E.itiposer = 'H' and E.ganoregi = " + A�oAdmision); 
							object tempRefParam = stFllegada; 
							sql.Append(" and E.gnumregi = " + NumeroAdmision + " and E.gprestac = EPRESALT.gprestac and E.fpeticio = " + Serrores.FormatFechaHMS(tempRefParam) + " ) "); 
							stFllegada = Convert.ToString(tempRefParam); 
							break;
						case "ECUIDALT" : 
							sql.Append(sqlCondicionAdmisionHD + " AND (ffintrat IS NULL OR ffintrat >= CONVERT(datetime,'" + stFllegadaYMD + "',121)) "); 
							break;
						case "EINTEALT" : 
							sql.Append(sqlCondicionAdmisionHD + " AND (iestsoli='P' or iestsoli='C') "); 
							break;
						case "EFARCOMU" : 
							sql.Append(" WHERE " + 
							           " EFARCOMU.itiposer='H' and EFARCOMU.ganoregi =" + A�oAdmisionHD + " and EFARCOMU.gnumregi=" + NumeroAdmisionHD + 
							           " AND EXISTS (SELECT '' FROM EFARMALT where EFARMALT.itiposer = EFARCOMU.itiposer AND " + 
							           " EFARMALT.ganoregi = EFARCOMU.ganoregi AND " + 
							           " EFARMALT.gnumregi = EFARCOMU.gnumregi AND " + 
							           " EFARMALT.gfarmaco = EFARCOMU.gfarmaco AND " + 
							           " EFARMALT.finitrat = EFARCOMU.finitrat AND " + 
							           " EFARMALT.iesttrat='I' AND EFARMALT.ffinmovi is null)"); 
							break;
						case "EMATUTIL" : 
							//Tambi�n se realizan las validaciones como en EPRESALT 
							object tempRefParam2 = stFllegada; 
							sql.Append(" WHERE " + 
							           " EMATUTIL.itiposer='H' and EMATUTIL.ganoregi =" + A�oAdmisionHD + " and EMATUTIL.gnumregi=" + NumeroAdmisionHD + 
							           " AND EXISTS (SELECT '' FROM EPRESALT where EPRESALT.itiposer = EMATUTIL.itiposer AND " + 
							           " EPRESALT.ganoregi = EMATUTIL.ganoregi AND " + 
							           " EPRESALT.gnumregi = EMATUTIL.gnumregi AND " + 
							           " EPRESALT.gprestac = EMATUTIL.gprestac AND " + 
							           " (EPRESALT.iestsoli='P' or EPRESALT.iestsoli='C')) " + 
							           " and not exists (select '' from ematutil E where E.itiposer = 'H' and E.ganoregi = " + A�oAdmision + 
							           " and E.gnumregi = " + NumeroAdmision + " and E.gprestac = EMATUTIL.gprestac and E.fpeticio = " + Serrores.FormatFechaHMS(tempRefParam2) + 
							           " and E.gtipoart = EMATUTIL.gtipoart and E.gprotesi = EMATUTIL.gprotesi) "); 
							stFllegada = Convert.ToString(tempRefParam2); 
							break;
						default:
							sql.Append(sqlCondicionAdmisionHD); 
							break;
					}

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql.ToString(), Conexion);
					RrDestino = new DataSet();
					tempAdapter_3.Fill(RrDestino);

					foreach (DataRow iteration_row in RrDestino.Tables[0].Rows)
					{ // por cada fila encontrada en las tablas de enfermeria
						RrOrigen.AddNew();
						//RrOrigen("itiposer") = "H"
						//RrOrigen("ganoregi") = A�oAdmision
						//RrOrigen("gnumregi") = NumeroAdmision
						// recorremos los campos de las tablas implicadas a partir del campo
						// 4 porque los tres primeros se van a replicar con el episodio de admision
						for (int ibucleCampos = 0; ibucleCampos <= RrOrigen.Tables[0].Columns.Count - 1; ibucleCampos++)
						{
							if (iteration_row[ibucleCampos].ToString() == "itiposer")
							{
								RrOrigen.Tables[0].Rows[0]["itiposer"] = "H";
							}
                            else if (iteration_row[ibucleCampos].ToString() == "ganoregi")
							{ 
								RrOrigen.Tables[0].Rows[0]["ganoregi"] = A�oAdmision;
							}
                            else if (iteration_row[ibucleCampos].ToString() == "gnumregi")
							{ 
								RrOrigen.Tables[0].Rows[0]["gnumregi"] = NumeroAdmision;
							}
							else
							{
								if (iteration_row[ibucleCampos].GetType() == typeof(string))
								{
									vText = iteration_row[ibucleCampos];
									if (Convert.IsDBNull(vText))
									{
										RrOrigen.Tables[0].Rows[0][ibucleCampos] = DBNull.Value;
									}
									else
									{
										RrOrigen.Tables[0].Rows[0][ibucleCampos] = vText;
									}
									vText = null;
								}
								else
								{
									if (!Convert.IsDBNull(Convert.ToString(iteration_row[ibucleCampos]).Trim()))
									{
										RrOrigen.Tables[0].Rows[0][ibucleCampos] = iteration_row[ibucleCampos];
									}
									else
									{
										RrOrigen.Tables[0].Rows[0][ibucleCampos] = DBNull.Value;
									}
								}
							}
						}
						string tempQuery = RrOrigen.Tables[0].TableName;
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                        tempAdapter_2.Update(RrOrigen, RrOrigen.Tables[0].TableName);
					}
				}
				RrOrigen.Close();
				RrDestino.Close();


				object tempRefParam3 = stFllegada;
				sql = new StringBuilder(" UPDATE EDIETVIA SET ffinmovi=" + Serrores.FormatFechaHMS(tempRefParam3) + 
				      " where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + 
				      "       and ffinmovi is null");
				stFllegada = Convert.ToString(tempRefParam3);
				SqlCommand tempCommand = new SqlCommand(sql.ToString(), Conexion);
				tempCommand.ExecuteNonQuery();


				object tempRefParam4 = stFllegada;
				object tempRefParam5 = stFllegada;
				sql = new StringBuilder(" INSERT INTO EDIETVIA (itiposer, ganoregi, gnumregi, fapuntes, " + 
				      " ipaciaco, gtipodie, icsinsal, iconturm,idiabeti,gviaalim,oespdiet,ffinmovi,gusuario,fpasfact,iricafib,isupleme) " + 
				      " SELECT itiposer, ganoregi, gnumregi, " + Serrores.FormatFechaHMS(tempRefParam4) + ", " + 
				      "        ipaciaco, gtipodie, icsinsal, iconturm, idiabeti, gviaalim, oespdiet, NULL, gusuario, fpasfact, iricafib,isupleme " + 
				      " FROM EDIETVIA " + 
				      " where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + 
				      "  and ffinmovi = " + Serrores.FormatFechaHMS(tempRefParam5));
				stFllegada = Convert.ToString(tempRefParam5);
				stFllegada = Convert.ToString(tempRefParam4);
				SqlCommand tempCommand_2 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_2.ExecuteNonQuery();

				sql = new StringBuilder("select * from EPRESTAC ");
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
				sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
				object tempRefParam6 = stFllegada;
				sql.Append(" and fpeticio < " + Serrores.FormatFechaHMS(tempRefParam6) + " ");
				stFllegada = Convert.ToString(tempRefParam6);
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql.ToString(), Conexion);
				RrSql = new DataSet();
				tempAdapter_5.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
					RrSql.MoveFirst();
					i = 0;
					foreach (DataRow iteration_row_2 in RrSql.Tables[0].Rows)
					{
						stFechaPeticion = Convert.ToDateTime(iteration_row_2["fpeticio"]).ToString("dd/MM/yyyy HH:NN:SS");
						stFechaPetMod = DateTimeHelper.ToString(DateTime.Parse(stFllegada).AddSeconds(i));
						stgprestac = Convert.ToString(iteration_row_2["gprestac"]);

						object tempRefParam7 = stFechaPetMod;
						sql = new StringBuilder("UPDATE EPRESTAC set fpeticio = " + Serrores.FormatFechaHMS(tempRefParam7) + " ");
						stFechaPetMod = Convert.ToString(tempRefParam7);
						System.DateTime TempDate = DateTime.FromOADate(0);
						if (Conversion.Val(Convert.ToDateTime(iteration_row_2["fsolicit"]).ToString("yyyyMMddHHNNSS")) < Conversion.Val((DateTime.TryParse(stFllegada, out TempDate)) ? TempDate.ToString("yyyyMMddHHNNSS") : stFllegada))
						{
							object tempRefParam8 = stFechaPetMod;
							sql.Append(", fsolicit = " + Serrores.FormatFechaHMS(tempRefParam8) + " ");
							stFechaPetMod = Convert.ToString(tempRefParam8);
						}
						sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
						sql.Append(" and gprestac='" + stgprestac + "'");
						object tempRefParam9 = stFechaPeticion;
						sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam9) + " ");
						stFechaPeticion = Convert.ToString(tempRefParam9);

						SqlCommand tempCommand_3 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_3.ExecuteNonQuery();

						object tempRefParam10 = stFechaPetMod;
						sql = new StringBuilder("UPDATE EMATUTIL set fpeticio = " + Serrores.FormatFechaHMS(tempRefParam10));
						stFechaPetMod = Convert.ToString(tempRefParam10);
						sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
						sql.Append(" and gprestac ='" + stgprestac + "'");
						object tempRefParam11 = stFechaPeticion;
						sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam11) + " ");
						stFechaPeticion = Convert.ToString(tempRefParam11);

						SqlCommand tempCommand_4 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_4.ExecuteNonQuery();

						object tempRefParam12 = stFechaPetMod;
						sql = new StringBuilder("UPDATE EPRESVAL set fpeticio = " + Serrores.FormatFechaHMS(tempRefParam12));
						stFechaPetMod = Convert.ToString(tempRefParam12);
						System.DateTime TempDate2 = DateTime.FromOADate(0);
						if (Conversion.Val(Convert.ToDateTime(iteration_row_2["fsolicit"]).ToString("yyyyMMddHHNNSS")) < Conversion.Val((DateTime.TryParse(stFllegada, out TempDate2)) ? TempDate2.ToString("yyyyMMddHHNNSS") : stFllegada))
						{
							object tempRefParam13 = stFechaPetMod;
							sql.Append(", fsolicit = " + Serrores.FormatFechaHMS(tempRefParam13) + " ");
							stFechaPetMod = Convert.ToString(tempRefParam13);
						}
						sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
						sql.Append(" and gprestac ='" + stgprestac + "'");
						object tempRefParam14 = stFechaPeticion;
						sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam14) + " ");
						stFechaPeticion = Convert.ToString(tempRefParam14);


						SqlCommand tempCommand_5 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_5.ExecuteNonQuery();

						object tempRefParam15 = stFechaPetMod;
						object tempRefParam16 = stFechaPeticion;
						sql = new StringBuilder("INSERT INTO DOPENLAB (itiposer,ganoregi,gnumregi,fpeticio,gprestac,gidenpac,nmuestra,ipriorid,iestsoli," + 
						      "omotisol,frealiza,fanulaci,gsersoli,gpersoli,ncantida,gperreal,imodimdh,ileiopen,fcomunic,error,gsocieda," + 
						      "resultado,iresultado,icitpeti,fsolicit,ienviado,gtipmues,iestfact,ginspecc)" + " Select " + 
						      " 'H'," + A�oAdmision + "," + NumeroAdmision + "," + Serrores.FormatFechaHMS(tempRefParam15) + "," + 
						      "gprestac,gidenpac,nmuestra,ipriorid,iestsoli,omotisol,frealiza,fanulaci,gsersoli,gpersoli,ncantida,gperreal," + 
						      "imodimdh,ileiopen,fcomunic,error,gsocieda,resultado,iresultado,icitpeti,fsolicit,ienviado," + 
						      "gtipmues,iestfact,ginspecc from " + 
						      " DOPENLAB where itiposer = 'H'" + 
						      "   and ganoregi =" + A�oAdmisionHD + 
						      "   and gnumregi= " + NumeroAdmisionHD + 
						      "   and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam16) + 
						      "   and gprestac='" + stgprestac + "'");
						stFechaPeticion = Convert.ToString(tempRefParam16);
						stFechaPetMod = Convert.ToString(tempRefParam15);
						SqlCommand tempCommand_6 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_6.ExecuteNonQuery();


						object tempRefParam17 = stFechaPetMod;
						object tempRefParam18 = stFechaPeticion;
						sql = new StringBuilder("INSERT INTO DANALSOL (itiposer,ganoregi,gnumregi,fpeticio,gprestac,finivali,gpruelab,finivalp,ncantida," + 
						      "oresulta,nresulta,maxresul,minresul,uniresul) Select " + 
						      " 'H'," + A�oAdmision + "," + NumeroAdmision + "," + Serrores.FormatFechaHMS(tempRefParam17) + "," + 
						      " gprestac,finivali,gpruelab,finivalp,ncantida,oresulta,nresulta,maxresul,minresul,uniresul from " + 
						      " DANALSOL where itiposer = 'H'" + 
						      "   and ganoregi =" + A�oAdmisionHD + 
						      "   and gnumregi= " + NumeroAdmisionHD + 
						      "   and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam18) + 
						      "   and gprestac='" + stgprestac + "'");
						stFechaPeticion = Convert.ToString(tempRefParam18);
						stFechaPetMod = Convert.ToString(tempRefParam17);
						SqlCommand tempCommand_7 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_7.ExecuteNonQuery();

						object tempRefParam19 = stFechaPetMod;
						object tempRefParam20 = stFechaPeticion;
						sql = new StringBuilder("INSERT INTO DANALLAB (itiposer,ganoregi,gnumregi,fpeticio,gprestac,gidenpac,gpruelab,error,icitpeti," + 
						      "ncantida,iestfact,oresulta,nresulta,maxresul,minresul,uniresul)" + " Select " + 
						      " 'H'," + A�oAdmision + "," + NumeroAdmision + "," + Serrores.FormatFechaHMS(tempRefParam19) + "," + 
						      "gprestac,gidenpac,gpruelab,error,icitpeti,ncantida,iestfact,oresulta,nresulta,maxresul," + 
						      "minresul,uniresul from " + 
						      " DANALLAB where itiposer = 'H'" + 
						      "   and ganoregi =" + A�oAdmisionHD + 
						      "   and gnumregi= " + NumeroAdmisionHD + 
						      "   and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam20) + 
						      "   and gprestac='" + stgprestac + "'");
						stFechaPeticion = Convert.ToString(tempRefParam20);
						stFechaPetMod = Convert.ToString(tempRefParam19);
						SqlCommand tempCommand_8 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_8.ExecuteNonQuery();


						i++;

					}
				}
				RrSql.Close();

				sql = new StringBuilder("select * from EINTERCO ");
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
				sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
				object tempRefParam21 = stFllegada;
				sql.Append(" and fpeticio < " + Serrores.FormatFechaHMS(tempRefParam21) + " ");
				stFllegada = Convert.ToString(tempRefParam21);

				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql.ToString(), Conexion);
				RrSql = new DataSet();
				tempAdapter_6.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
					RrSql.MoveFirst();
					i = 1;
					foreach (DataRow iteration_row_3 in RrSql.Tables[0].Rows)
					{
						stFechaPeticion = Convert.ToDateTime(iteration_row_3["fpeticio"]).ToString("dd/MM/yyyy HH:NN:SS");
						stFechaPetMod = DateTimeHelper.ToString(DateTime.Parse(stFllegada).AddSeconds(i));

						object tempRefParam22 = stFechaPetMod;
						sql = new StringBuilder("UPDATE EINTERCO set fpeticio = " + Serrores.FormatFechaHMS(tempRefParam22) + " ");
						stFechaPetMod = Convert.ToString(tempRefParam22);
						sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
						sql.Append(" and (iestsoli = 'P' or iestsoli = 'C') ");
						object tempRefParam23 = stFechaPeticion;
						sql.Append(" and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam23) + " ");
						stFechaPeticion = Convert.ToString(tempRefParam23);

						SqlCommand tempCommand_9 = new SqlCommand(sql.ToString(), Conexion);
						tempCommand_9.ExecuteNonQuery();
						i++;
					}
				}
				RrSql.Close();

				// una vez que hemos actualizado la fecha previstsa hay que comprobar la fecha de comunicacion de la prestacion/interconsulta
				//fprevista
				sql = new StringBuilder("UPDATE EPRESTAC set fprevista = fpeticio ");
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
				sql.Append(" and iestsoli = 'C' ");
				sql.Append(" and fprevista < fpeticio and fprevista is not null");

				SqlCommand tempCommand_10 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_10.ExecuteNonQuery();

				sql = new StringBuilder("UPDATE EINTERCO set fprevista = fpeticio ");
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");
				sql.Append(" and iestsoli = 'C' ");
				sql.Append(" and fprevista < fpeticio and fprevista is not null");

				SqlCommand tempCommand_11 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_11.ExecuteNonQuery();

				//Al copiar las ordenes medicas en formato valoracion del episodio de HD, se deben copiar con la
				//fecha de ingreso del episodio destino.
				object tempRefParam24 = stFllegada;
				sql = new StringBuilder("UPDATE DVALORDM set fapuntes =  " + Serrores.FormatFechaHMS(tempRefParam24));
				stFllegada = Convert.ToString(tempRefParam24);
				sql.Append(" where itiposer = 'H' and ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " ");

				SqlCommand tempCommand_12 = new SqlCommand(sql.ToString(), Conexion);
				tempCommand_12.ExecuteNonQuery();

				//Oscar C Septiembre 2012
				//Si se ha replicado valoracion de enfermeria, se rellena la fecha de ingreso en planta
				//Diciembre 2012. Por orden de curia segun correo de 12 de noviembre de 2012, ya no se replica la valoracion de enfermeria
				//Sql = "SELECT count(*) FROM DVALOPAC"
				//Sql = Sql & " where itiposer = 'H' and ganoregi =" & A�oAdmision & " and gnumregi=" & NumeroAdmision & " "
				//
				//Set RrSQL = Conexion.OpenResultset(Sql, rdOpenKeyset, rdConcurReadOnly)
				//If RrSQL(0) > 0 Then
				//
				//     Sql = "UPDATE DVALOPAC set fapuntes =  " & FormatFechaHMS(stFllegada)
				//     Sql = Sql & " where itiposer = 'H' and ganoregi =" & A�oAdmision & " and gnumregi=" & NumeroAdmision & " "
				//
				//     Conexion.Execute Sql, rdExecDirect
				//
				//     Sql = "UPDATE DVALORAE set fapuntes =  " & FormatFechaHMS(stFllegada)
				//     Sql = Sql & " where itiposer = 'H' and ganoregi =" & A�oAdmision & " and gnumregi=" & NumeroAdmision & " "
				//
				//     Conexion.Execute Sql, rdExecDirect
				//
				//     Sql = "UPDATE AEPISADM set fingplan = fllegada "
				//     Sql = Sql & " Where ganoadme = " & A�oAdmision & " And gnumadme = " & NumeroAdmision
				//
				//     Conexion.Execute Sql, rdExecDirect
				//
				//End If
				//RrSQL.Close
				//------------------------


			}
			else if (Operacion == 1)
			{ 
				// ANULACION DEL INGRESO EN ADMISION
				// BORRAMOS LOS APUNTES DE ADMISION

				for (int ibucleTablas = 1; ibucleTablas <= NUMEROTABLASHD; ibucleTablas++)
				{

					switch(ArrayTablasOrigenHD[ibucleTablas - 1])
					{
						case "EINCIDEN" : 
							sqlCondicionAdmisionHD = " where ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision; 
							break;
						default:
							sqlCondicionAdmisionHD = " where ganoregi =" + A�oAdmision + " and gnumregi=" + NumeroAdmision + " and itiposer='H'"; 
							break;
					}

					ordenBorrado = ArrayTablasOrigenHD[ibucleTablas - 1];

					sql = new StringBuilder(" delete " + ordenBorrado + " " + sqlCondicionAdmisionHD);
					SqlCommand tempCommand_13 = new SqlCommand(sql.ToString(), Conexion);
					tempCommand_13.ExecuteNonQuery();
				}

			}
			else if (Operacion == 2)
			{ 
				//''     SE VER SI HCE FLT IMPLEMENTR EST OPCION


			}


		}

		private void proRellenarArrayTablasHD()
		{
			ArrayTablasOrigenHD[0] = "DVALOPAC";
			ArrayTablasOrigenHD[1] = "DVALORAE";
			ArrayTablasOrigenHD[2] = "EBALANCE";
			ArrayTablasOrigenHD[3] = "ECUIDADO";
			ArrayTablasOrigenHD[4] = "ECONFCUI";
			ArrayTablasOrigenHD[5] = "ECONSVIT";
			ArrayTablasOrigenHD[6] = "ECONULCE";
			ArrayTablasOrigenHD[7] = "ECURVEND";
			ArrayTablasOrigenHD[8] = "EDIETVIA";
			ArrayTablasOrigenHD[9] = "EFARMACO";
			ArrayTablasOrigenHD[10] = "EFARCOMU"; //ATENTOS PORQUE NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			ArrayTablasOrigenHD[11] = "EGLUCEMI";
			ArrayTablasOrigenHD[12] = "EINTERCO";
			ArrayTablasOrigenHD[13] = "EINTVCIO";
			ArrayTablasOrigenHD[14] = "EMANIFES";
			ArrayTablasOrigenHD[15] = "EMEDIDAS";
			ArrayTablasOrigenHD[16] = "EMENUELE";
			ArrayTablasOrigenHD[17] = "ENOSOCOM";
			ArrayTablasOrigenHD[18] = "ECONMECA";
			ArrayTablasOrigenHD[19] = "EOBJETIV";
			ArrayTablasOrigenHD[20] = "EOBSENFE";
			ArrayTablasOrigenHD[21] = "EORDMEDI";
			ArrayTablasOrigenHD[22] = "EPAUCORR";
			ArrayTablasOrigenHD[23] = "EPLANPAC";
			ArrayTablasOrigenHD[24] = "EPRESTAC";
			ArrayTablasOrigenHD[25] = "EMATUTIL"; //ATENTOS PORQUE NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			ArrayTablasOrigenHD[26] = "EPRESVAL"; //ATENTOS PORQUE NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			ArrayTablasOrigenHD[27] = "EPROBENF";
			ArrayTablasOrigenHD[28] = "EPROCURA";
			ArrayTablasOrigenHD[29] = "ERELACIO";
			ArrayTablasOrigenHD[30] = "EULCERAS";
			ArrayTablasOrigenHD[31] = "EVALUOBJ";
			ArrayTablasOrigenHD[32] = "DVALORDM"; //ATENTOS PORQUE NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			ArrayTablasOrigenHD[33] = "EINCIDEN"; //ATENTOS PORQUE NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			ArrayTablasOrigenHD[34] = "ERIESNUT"; //ATENTOS PORQUE NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			ArrayTablasOrigenHD[35] = "ERIESOCI"; //ATENTOS PORQUE NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			ArrayTablasOrigenHD[36] = "EINRTTPA";
			ArrayTablasOrigenHD[37] = "DPERSJUS"; //ATENTOS PORQUE NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			//ArrayTablasOrigenHD(39) = "DANALSOL" 'ATENTOS PORQUE NO TIENE TABLA DE ALTA CON LO CUAL AL INSERTAR EN AEPISADM NO DEBE SER DE LA TABLA DE ALTA  SI NO DE LA TABLA NORMAL
			//ArrayTablasOrigenHD(40) = "DOPENLAB"
			//ArrayTablasOrigenHD(41) = "DANALLAB"

			ArrayTablasDestinoHD[0] = "DVALPALT";
			ArrayTablasDestinoHD[1] = "DVALEALT";
			ArrayTablasDestinoHD[2] = "EBALAALT";
			ArrayTablasDestinoHD[3] = "ECUIDALT";
			ArrayTablasDestinoHD[4] = "ECONFALT";
			ArrayTablasDestinoHD[5] = "ECONSALT";
			ArrayTablasDestinoHD[6] = "ECONULAL";
			ArrayTablasDestinoHD[7] = "ECURVALT";
			ArrayTablasDestinoHD[8] = "EDIETALT";
			ArrayTablasDestinoHD[9] = "EFARMALT";
			ArrayTablasDestinoHD[10] = "EFARCOMU";
			ArrayTablasDestinoHD[11] = "EGLUCALT";
			ArrayTablasDestinoHD[12] = "EINTEALT";
			ArrayTablasDestinoHD[13] = "EINTVALT";
			ArrayTablasDestinoHD[14] = "EMANIALT";
			ArrayTablasDestinoHD[15] = "EMEDIALT";
			ArrayTablasDestinoHD[16] = "EMENUELE";
			ArrayTablasDestinoHD[17] = "ENOSOALT";
			ArrayTablasDestinoHD[18] = "ECONMALT";
			ArrayTablasDestinoHD[19] = "EOBJEALT";
			ArrayTablasDestinoHD[20] = "EOBSEALT";
			ArrayTablasDestinoHD[21] = "EORDMALT";
			ArrayTablasDestinoHD[22] = "EPAUCALT";
			ArrayTablasDestinoHD[23] = "EPLANALT";
			ArrayTablasDestinoHD[24] = "EPRESALT";
			ArrayTablasDestinoHD[25] = "EMATUTIL";
			ArrayTablasDestinoHD[26] = "EPRESVAL";
			ArrayTablasDestinoHD[27] = "EPROBALT";
			ArrayTablasDestinoHD[28] = "EPROCALT";
			ArrayTablasDestinoHD[29] = "ERELAALT";
			ArrayTablasDestinoHD[30] = "EULCEALT";
			ArrayTablasDestinoHD[31] = "EVALUALT";
			ArrayTablasDestinoHD[32] = "DVALORDM";
			ArrayTablasDestinoHD[33] = "EINCIDEN";
			ArrayTablasDestinoHD[34] = "ERIESNUT";
			ArrayTablasDestinoHD[35] = "ERIESOCI";
			ArrayTablasDestinoHD[36] = "EINRTALT";
			ArrayTablasDestinoHD[37] = "DPERSJUS";
			//ArrayTablasDestinoHD(39) = "DANALSOL"
			//ArrayTablasDestinoHD(40) = "DOPENLAB"
			//ArrayTablasDestinoHD(41) = "DANALLAB"


		}
	}
}