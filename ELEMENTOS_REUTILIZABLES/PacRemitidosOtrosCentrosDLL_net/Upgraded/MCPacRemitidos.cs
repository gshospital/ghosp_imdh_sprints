using System;
using System.Data.SqlClient;
using UpgradeHelpers.DB.DAO;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PacRemitidosDLL
{
	public class MCPacRemitidos
	{

		string vstPathTemp = String.Empty;
		bool vbBase = false;
		DAODatabaseHelper bs = null;
		string VstGrupoHo = String.Empty;
		string VstNombreHo = String.Empty;
		string VstDireccHo = String.Empty;

		public void proLlamada(object formulario, SqlConnection Conexion, string stArea, object crCrystal, string VSTPATHINFORMES, string VstUsuario, string VSTPATHBD, string NOMBREBASEDATOSTEMP, string NOMBRE_ACCESS, System.Windows.Forms.PrintDialog CommDiag_impre)
		{

			Serrores.oClass = new Mensajes.ClassMensajes();
			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(Conexion);
			Serrores.vAyuda = Serrores.ObtenerFicheroAyuda();
			//'''
			PacRemitidosBas.rc = Conexion;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref PacRemitidosBas.rc);
			Serrores.AjustarRelojCliente(PacRemitidosBas.rc);
			PacRemitidosBas.pathstring = VSTPATHINFORMES; //report
			PacRemitidosBas.gUsuario = VstUsuario;
			PacRemitidosBas.LISTADO_PARTICULAR = crCrystal;
			PacRemitidosBas.ST_PATHSTRING = VSTPATHBD;
			PacRemitidosBas.NOMBREBASEDATOSTEMPORAL = NOMBREBASEDATOSTEMP;
			PacRemitidosBas.NOMBRE_DSN_ACCESS = NOMBRE_ACCESS;
			PacRemitidosBas.vstArea = stArea;
			PacRemitidosBas.Cuadro_Diag_impre = CommDiag_impre;
			UIC8125F1.DefInstance.ShowDialog();
		}
	}
}