using UpgradeHelpers.MSForms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PacRemitidosDLL
{
	partial class UIC8125F1
	{

		#region "Upgrade Support "
		private static UIC8125F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static UIC8125F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new UIC8125F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "CbDestino", "cbCodificar", "Frame2", "cbPantalla", "cbsalir", "cbImprimir", "sdcInicio", "sdcFinal", "Label1", "Label4", "Frame1", "commandButtonHelper1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public MSCombobox CbDestino;
		public Telerik.WinControls.UI.RadButton cbCodificar;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public Telerik.WinControls.UI.RadButton cbPantalla;
		public Telerik.WinControls.UI.RadButton cbsalir;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public Telerik.WinControls.UI.RadDateTimePicker sdcInicio;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFinal;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UIC8125F1));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.CbDestino = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbCodificar = new Telerik.WinControls.UI.RadButton();
            this.cbPantalla = new Telerik.WinControls.UI.RadButton();
            this.cbsalir = new Telerik.WinControls.UI.RadButton();
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcInicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.sdcFinal = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbDestino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCodificar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbsalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame2
            // 
            this.Frame2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame2.Controls.Add(this.CbDestino);
            this.Frame2.Controls.Add(this.cbCodificar);
            this.Frame2.HeaderText = " Centro ";
            this.Frame2.Location = new System.Drawing.Point(0, 56);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(447, 55);
            this.Frame2.TabIndex = 8;
            this.Frame2.TabStop = false;
            this.Frame2.Text = " Centro ";
            // 
            // CbDestino
            // 
            this.CbDestino.ColumnWidths = "";
            this.CbDestino.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbDestino.Location = new System.Drawing.Point(48, 22);
            this.CbDestino.Name = "CbDestino";
            this.CbDestino.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbDestino.Size = new System.Drawing.Size(395, 20);
            this.CbDestino.TabIndex = 10;
            // 
            // cbCodificar
            // 
            this.cbCodificar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCodificar.Image = ((System.Drawing.Image)(resources.GetObject("cbCodificar.Image")));
            this.cbCodificar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbCodificar.Location = new System.Drawing.Point(8, 22);
            this.cbCodificar.Name = "cbCodificar";
            this.cbCodificar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCodificar.Size = new System.Drawing.Size(34, 23);
            this.cbCodificar.TabIndex = 9;
            this.cbCodificar.Click += new System.EventHandler(this.cbCodificar_Click);
            // 
            // cbPantalla
            // 
            this.cbPantalla.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbPantalla.Enabled = false;
            this.cbPantalla.Location = new System.Drawing.Point(274, 116);
            this.cbPantalla.Name = "cbPantalla";
            this.cbPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbPantalla.Size = new System.Drawing.Size(83, 30);
            this.cbPantalla.TabIndex = 7;
            this.cbPantalla.Text = "Pantalla";
            this.cbPantalla.Click += new System.EventHandler(this.cbPantalla_Click);
            // 
            // cbsalir
            // 
            this.cbsalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbsalir.Location = new System.Drawing.Point(362, 116);
            this.cbsalir.Name = "cbsalir";
            this.cbsalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbsalir.Size = new System.Drawing.Size(84, 30);
            this.cbsalir.TabIndex = 6;
            this.cbsalir.Text = "Salir";
            this.cbsalir.Click += new System.EventHandler(this.cbsalir_Click);
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.Enabled = false;
            this.cbImprimir.Location = new System.Drawing.Point(186, 116);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(83, 30);
            this.cbImprimir.TabIndex = 5;
            this.cbImprimir.Text = "Imprimir";
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.sdcInicio);
            this.Frame1.Controls.Add(this.sdcFinal);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.HeaderText = "Intervalo de Fechas";
            this.Frame1.Location = new System.Drawing.Point(0, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(446, 53);
            this.Frame1.TabIndex = 0;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Intervalo de Fechas";
            // 
            // sdcInicio
            // 
            this.sdcInicio.CustomFormat = "dd/MM/yyyy";
            this.sdcInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sdcInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcInicio.Location = new System.Drawing.Point(67, 23);
            this.sdcInicio.Name = "sdcInicio";
            this.sdcInicio.Size = new System.Drawing.Size(137, 18);
            this.sdcInicio.TabIndex = 1;
            this.sdcInicio.TabStop = false;
            this.sdcInicio.Text = "04/05/2016";
            this.sdcInicio.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            this.sdcInicio.Click += new System.EventHandler(this.sdcInicio_Click);
            this.sdcInicio.Leave += new System.EventHandler(this.sdcInicio_Leave);
            this.sdcInicio.Closing += new Telerik.WinControls.UI.RadPopupClosingEventHandler(sdcInicio_CloseUp);
            // 
            // sdcFinal
            // 
            this.sdcFinal.CustomFormat = "dd/MM/yyyy";
            this.sdcFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sdcFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFinal.Location = new System.Drawing.Point(265, 22);
            this.sdcFinal.Name = "sdcFinal";
            this.sdcFinal.Size = new System.Drawing.Size(137, 18);
            this.sdcFinal.TabIndex = 2;
            this.sdcFinal.TabStop = false;
            this.sdcFinal.Text = "04/05/2016";
            this.sdcFinal.Value = new System.DateTime(2016, 5, 4, 0, 0, 0, 0);
            this.sdcFinal.Click += new System.EventHandler(this.sdcFinal_Click);
            this.sdcFinal.Leave += new System.EventHandler(this.sdcFinal_Leave);
            this.sdcFinal.Closing += new Telerik.WinControls.UI.RadPopupClosingEventHandler(sdcFinal_CloseUp);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(229, 24);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(34, 18);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "Hasta";
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(19, 24);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(37, 18);
            this.Label4.TabIndex = 3;
            this.Label4.Text = "Desde";
            // 
            // UIC8125F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 150);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.cbPantalla);
            this.Controls.Add(this.cbsalir);
            this.Controls.Add(this.cbImprimir);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Location = new System.Drawing.Point(4, 23);
            this.Name = "UIC8125F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Pacientes remitidos de otros centros UIC8125F1";
            this.Closed += new System.EventHandler(this.UIC8125F1_Closed);
            this.Load += new System.EventHandler(this.UIC8125F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CbDestino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCodificar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPantalla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbsalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}