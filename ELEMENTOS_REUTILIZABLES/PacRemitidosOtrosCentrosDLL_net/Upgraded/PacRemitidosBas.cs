using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PacRemitidosDLL
{
	internal static class PacRemitidosBas
	{

		public static PacRemitidosBas.CabCrystal VstrCrystal = PacRemitidosBas.CabCrystal.CreateInstance();
		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public bool VfCabecera;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					return result;
			}
		}
		//'''''''''
		public static PrintDialog Cuadro_Diag_impre = null;
		public static string ST_PATHSTRING = String.Empty;
		public static string NOMBREBASEDATOSTEMPORAL = String.Empty;
		public static string NOMBRE_DSN_ACCESS = String.Empty;
		public static string gUsuario = String.Empty;
		public static string vstArea = String.Empty;
		public static SqlConnection rc = null;
		public static string pathstring = String.Empty;
		public static dynamic LISTADO_PARTICULAR = null;
		//''''''''
		public static string lFor_fecha = String.Empty; // FORMATO CONFIGURACION REGIONAL FECHA SOLO
		public static string lSep_Fecha = String.Empty;
		public static string lsep_hora = String.Empty;
		public static string LFOR_HORA = String.Empty; //FORMATO DE CONFIGURACION DE HORA
		public static string lEsp_For_Siglo = String.Empty; // MOSTRAR SIGLO O NO
		public static int LNumero_Ceros = 0; // numero de ceros de las horas
		public static int iFormato_Fecha = 0; // entero que identifica al formato  de fecha -> vaspread.typedateformat
		public static string lSep_Decimal = String.Empty;
		// CONSTANTES UTILIZADAS PARA OBTENER VALORES DE LA CONFIGURACION
		// REGIONAL DEL ORDENADOR
		public const int LOCALE_SDATE = 0x1D; //
		public const int LOCALE_STIME = 0x1E;
		public const int LOCALE_SSHORTDATE = 0x1F;
		public const int LOCALE_SYSTEM_DEFAULT = 0x800;
		public const int LOCALE_STIMEFORMAT = 0x1003; //formato de hora
		public const int LOCALE_ICENTURY = 0x24; //cuatro cifras de a�o
		public const int LOCALE_ITLZERO = 0x25; // numero de ceros en la hora
		public const int LOCALE_IDATE = 0x21; // entero que devuelve el formato de fecha
		public const int LOCALE_ITIME = 0x23; // ENTERO QUE IDENTIFICA A LA HORA
		public const int LOCALE_SDECIMAL = 0xE; // devuelve el caracter decimal
		//'''''''''
		//[DllImport("advapi32.dll", EntryPoint = "GetUserNameA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetUserName([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpbuffer, ref int nSize);
		//[DllImport("kernel32.dll", EntryPoint = "GetLocaleInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLCData, int cchData);
		internal static void proCabCrystal(SqlConnection rc)
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************


			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, rc);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			VstrCrystal.VstGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, rc);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			VstrCrystal.VstNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, rc);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			VstrCrystal.VstDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tRrCrystal.Close();
			VstrCrystal.VfCabecera = true;
		}
		// meter� el formato, separador de fechas,maxdate.Se llamar� desde el LOAD DE TODAS LAS PANTALLAS
		internal static void Establecer_Propiedades_DateCombo(RadDateTimePicker SsDateCombo)
		{
			ObtenerLocale();
			//SsDateCombo.setDateSeparator(lSep_Fecha);
			SsDateCombo.MaxDate = DateTime.Parse(StringsHelper.Format(DateTime.Now, lFor_fecha));
			SsDateCombo.CustomFormat = lFor_fecha;
			if (StringsHelper.ToDoubleSafe(lEsp_For_Siglo) == 1)
			{
				SsDateCombo.NullText = ("__" + lSep_Fecha + "__" + lSep_Fecha + "____");
				//SsDateCombo.setShowCentury(true);
			}
			else
			{
				//SsDateCombo.setShowCentury(false);
				SsDateCombo.NullText = ("__" + lSep_Fecha + "__" + lSep_Fecha + "__");
			}
		}
		internal static void ObtenerLocale()
		{
			FixedLengthString buffer = new FixedLengthString(100);

			//    #If Win32 Then
			string tempRefParam = buffer.Value;
			int dl = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDATE, ref tempRefParam, 99);
			buffer.Value = tempRefParam;
			lSep_Fecha = LPSTRToVBString(buffer.Value);
			string tempRefParam2 = buffer.Value;
			dl = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIME, ref tempRefParam2, 99);
			buffer.Value = tempRefParam2;
			lsep_hora = LPSTRToVBString(buffer.Value);
			string tempRefParam3 = buffer.Value;
			dl = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SSHORTDATE, ref tempRefParam3, 99);
			buffer.Value = tempRefParam3;
			lFor_fecha = "dd/MM/yyyy";
			//lFor_fecha = LPSTRToVBString(buffer)
			string tempRefParam4 = buffer.Value;
			dl = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIMEFORMAT, ref tempRefParam4, 99);
			buffer.Value = tempRefParam4;
			LFOR_HORA = LPSTRToVBString(buffer.Value);
			string tempRefParam5 = buffer.Value;
			dl = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ICENTURY, ref tempRefParam5, 99);
			buffer.Value = tempRefParam5;
			lEsp_For_Siglo = "1";
			//lEsp_For_Siglo = LPSTRToVBString(buffer)
			string tempRefParam6 = buffer.Value;
			dl = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ITLZERO, ref tempRefParam6, 99);
			buffer.Value = tempRefParam6;
			LNumero_Ceros = Convert.ToInt32(Double.Parse(LPSTRToVBString(buffer.Value)));
			string tempRefParam7 = buffer.Value;
			dl = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_IDATE, ref tempRefParam7, 99);
			buffer.Value = tempRefParam7;
			iFormato_Fecha = Convert.ToInt32(Double.Parse(LPSTRToVBString(buffer.Value)));
			//        dl = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ITIME, buffer, 99)
			//        iFormato_Hora = LPSTRToVBString(buffer)
			string tempRefParam8 = buffer.Value;
			dl = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDECIMAL, ref tempRefParam8, 99);
			buffer.Value = tempRefParam8;
			lSep_Decimal = LPSTRToVBString(buffer.Value);
			//    #Else
			//        Print " Not implemented under Win16"
			//    #End If
		}
		// Extracts a VB string from a buffer containing a null terminated
		// string
		private static string LPSTRToVBString(string s)
		{
			int nullpos = (s.IndexOf(Strings.Chr(0).ToString()) + 1);
			if (nullpos > 0)
			{
				return s.Substring(0, Math.Min(nullpos - 1, s.Length));
			}
			else
			{
				return "";
			}
		}
		//Sub proDialogo_impre(Pcrystal As Crystal.CrystalReport, Optional Num_Copi As Integer)
		//
		//End Sub
		internal static void proDialogo_impre(dynamic Pcrystal, int Num_Copi = 0)
		{
			if (false || Num_Copi == 0)
			{
				Cuadro_Diag_impre.PrinterSettings.Copies = 1;
			}
			else
			{
				Cuadro_Diag_impre.PrinterSettings.Copies = short.Parse(Num_Copi.ToString());
			}
			Pcrystal.PrinterStopPage = 0;
			//Cuadro_Diag_impre.CancelError = true;
			//Cuadro_Diag_impre.PrinterSettings.DefaultPrinter = true;
			Cuadro_Diag_impre.ShowDialog();
			Pcrystal.CopiesToPrinter = Cuadro_Diag_impre.PrinterSettings.Copies;
		}
	}
}