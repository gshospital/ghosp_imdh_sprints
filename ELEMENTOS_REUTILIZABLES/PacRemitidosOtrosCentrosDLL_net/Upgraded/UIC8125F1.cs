using ElementosCompartidos;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeHelpers.DB.DAO;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace PacRemitidosDLL
{
	public partial class UIC8125F1
		: RadForm
	{

		string VstAreaDescrip = String.Empty;
		string stDescripHosp = String.Empty; //en la cabecera del report
		bool Correcto = false;
		string vstPathTemp = String.Empty;
		bool vbBase = false;
		DAODatabaseHelper bs = null;
		DAORecordSetHelper rs = null;
		TableDefHelper Tabla = null;
		bool imprimir = false;
		public UIC8125F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		public void Activar_Imprimir()
		{
			if (sdcInicio.NullableValue != null || sdcFinal.NullableValue != null)
			{
				cbImprimir.Enabled = true;
				cbPantalla.Enabled = true;
			}
			else
			{
				cbImprimir.Enabled = false;
				cbPantalla.Enabled = false;
			}
		}

		private void cbCodificar_Click(Object eventSender, EventArgs eventArgs)
		{
			//llamar a la dll busqueda de filiacion
			Institucion.MCInstitucion Institucion = new Institucion.MCInstitucion();
			object tempRefParam = this;
			Institucion.llamada(tempRefParam, PacRemitidosBas.rc);
			Institucion = null;
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{


			try
			{

				COMPROBAR_FECHAS();
				validar_campos();
				if (Correcto)
				{
					imprimir = true;
					if (PacRemitidosBas.vstArea == "C")
					{
						VstAreaDescrip = "Consultas externas";
						Datos_Consulta();
					}
					else if (PacRemitidosBas.vstArea == "A")
					{ 
						VstAreaDescrip = "Admisi�n";
						Datos_Admision();
					}
				}
				else
				{
					return;
				}
			}
			catch(Exception ex)
			{
			}

			if (Information.Err().Number == 32755)
			{
				//se ha pulsado cancelar en el cuadro de dialogo imprimir
			}
		}

		private void cbPantalla_Click(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_FECHAS();
			validar_campos();
			if (Correcto)
			{
				imprimir = false;
				if (PacRemitidosBas.vstArea == "C")
				{
					VstAreaDescrip = "Consultas externas";
					Datos_Consulta();
				}
				else if (PacRemitidosBas.vstArea == "A")
				{ 
					VstAreaDescrip = "Admisi�n";
					Datos_Admision();
				}
			}
			else
			{
				return;
			}
		}

		private void cbsalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void UIC8125F1_Load(Object eventSender, EventArgs eventArgs)
		{
            Activar_Imprimir();
			imprimir = false;
			PacRemitidosBas.Establecer_Propiedades_DateCombo(this.sdcFinal);
			PacRemitidosBas.Establecer_Propiedades_DateCombo(this.sdcInicio);
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
            CbDestino.Text = "";

            CbDestino.AddItem("<Todos>", null);
			string sql = "select distinct dhospita.* from dhospita order by dnomhosp ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, PacRemitidosBas.rc);
			DataSet Rsql = new DataSet();
			tempAdapter.Fill(Rsql);
			if (Rsql.Tables[0].Rows.Count != 0)
			{
				Rsql.MoveFirst();
				foreach (DataRow iteration_row in Rsql.Tables[0].Rows)
				{
                    CbDestino.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dnomhosp"]).Trim(), Convert.ToInt32(iteration_row["ghospita"])));
				}
			}
			Rsql.Close();
			sdcFinal.Value = DateTime.Parse(StringsHelper.Format(DateTime.Now, PacRemitidosBas.lFor_fecha));
			sdcInicio.Value = DateTime.Parse(StringsHelper.Format(DateTime.Now, PacRemitidosBas.lFor_fecha));

			string LitPersona = Serrores.ObtenerLiteralPersona(PacRemitidosBas.rc);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower() + "s";

			UIC8125F1.DefInstance.Text = LitPersona + " remitidos de otros centros UIC8125F1";

		}
		private void sdcFinal_Click(Object eventSender, EventArgs eventArgs)
		{
			Activar_Imprimir();
		}

		private void sdcFinal_CloseUp(object eventSender, RadPopupClosingEventArgs eventArgs)
		{
			Activar_Imprimir();
		}

		private void sdcFinal_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_Imprimir();
		}

		private void sdcInicio_Click(Object eventSender, EventArgs eventArgs)
		{
			Activar_Imprimir();
		}
		private void sdcInicio_CloseUp(object eventSender, RadPopupClosingEventArgs eventArgs)
        {
			Activar_Imprimir();
		}
		private void sdcInicio_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_Imprimir();
		}
		private void COMPROBAR_FECHAS()
		{
			Serrores.oClassErrores = new Mensajes.ClassMensajes();
			if (sdcInicio.Value.Date > sdcFinal.Value.Date)
			{
				short tempRefParam = 1020;
				string[] tempRefParam2 = new string[]{Label4.Text, "menor", Label1.Text};
				Serrores.oClassErrores.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, PacRemitidosBas.rc, tempRefParam2);
				Correcto = false;
			}
			else
			{
				Correcto = true;
			}
		}
		private bool validar_campos()
		{
			bool result = false;
			Serrores.oClassErrores = new Mensajes.ClassMensajes();
			if (sdcInicio.NullableValue == null)
			{
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[]{"Fecha inicio"};
				Serrores.oClassErrores.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, PacRemitidosBas.rc, tempRefParam2);
				sdcInicio.Focus();
				return false;
			}
			if (sdcFinal.NullableValue == null)
			{
				short tempRefParam3 = 1040;
				string[] tempRefParam4 = new string[]{"Fecha final"};
				Serrores.oClassErrores.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, PacRemitidosBas.rc, tempRefParam4);
				result = false;
				sdcFinal.Focus();
				return result;
			}
			return true;
		}
		public void DatosInstitucion(string Codigo, string CodigoOFicial, string Descripcion)
		{
			if (Codigo != "")
			{
				for (int I = 0; I <= CbDestino.Items.Count - 1; I++)
				{
					if (CbDestino.Items[I].Value.ToString().ToLower() != "<todos>" && Convert.ToInt32(CbDestino.Items[I].Value) == Convert.ToInt32(Codigo))
					{
						CbDestino.SelectedIndex = I;
						break;
					}
				}
			}
		}
		public void Datos_Consulta()
		{
			string sql = "select dpacient.dape1pac,dpacient.dape2pac,dpacient.dnombpac, ";
			sql = sql + " dhospita.dnomhosp,cconsult.dmediori,cconsult.ffeccita,dsocieda.dsocieda,cconsult.gdiagnos,cconsult.ddiagnos,ddiagnos.dnombdia ";
			sql = sql + " From cconsult";
			sql = sql + " inner join dpacient on cconsult.gidenpac = dpacient.gidenpac";
			sql = sql + " inner join dhospita on cconsult.ghospori = dhospita.ghospita";
			sql = sql + " inner join dsocieda on cconsult.gsocieda = dsocieda.gsocieda";
			sql = sql + " left  join ddiagnos on cconsult.gdiagnos = ddiagnos.gcodidia";
			sql = sql + " Where";
			object tempRefParam = sdcInicio.Text + " 00:00:00";
			sql = sql + " cconsult.ffeccita >= " + Serrores.FormatFechaHMS(tempRefParam) + " and ";
			object tempRefParam2 = sdcFinal.Text + " 23:59:59";
			sql = sql + " cconsult.ffeccita <= " + Serrores.FormatFechaHMS(tempRefParam2) + " and ";

			if (CbDestino.SelectedIndex > 0)
			{
				sql = sql + "cconsult.ghospori = " + CbDestino.GetItemData(CbDestino.SelectedIndex).ToString() + " and ";
			}
			else
			{
				sql = sql + "cconsult.ghospori is not null and ";
			}

			sql = sql + "cconsult.ghospori = dhospita.ghospita ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, PacRemitidosBas.rc);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			Tabla_Pacientes_Remitidos(RrSql);
			RrSql.Close();
		}

		public void Datos_Admision()
		{
			// la fecha de inicio = '20/10/00 00:00:00'
			// la fecha de fin = '20/10/00 23:59:59'
			// tiene datos
			string sql = "select dpacient.dape1pac,dpacient.dape2pac,dpacient.dnombpac,";
			sql = sql + " dhospita.dnomhosp,aepisadm.dmediori,aepisadm.fllegada,dsocieda.dsocieda,ddiagnos.dnombdia, AEPISADM.odiaging,AEPISADM.gdiaging";
			sql = sql + " From AEPISADM ";
			sql = sql + " inner join  Amovifin on  Amovifin.ganoregi = aepisadm.ganoadme and";
			sql = sql + "             Amovifin.gnumregi = aepisadm.gnumadme and";
			sql = sql + "             Amovifin.fmovimie = AEPISADM.fLlegada";
			sql = sql + " inner join dpacient on  aepisadm.gidenpac = dpacient.gidenpac";
			sql = sql + " inner join dhospita on  aepisadm.ghospori = dhospita.ghospita";
			sql = sql + " inner join dsocieda on  Amovifin.gsocieda = dsocieda.gsocieda";
			sql = sql + " left  join ddiagnos on  AEPISADM.gdiaging = ddiagnos.gcodidia";
			sql = sql + " Where";
			object tempRefParam = sdcInicio.Text + " 00:00:00";
			sql = sql + " aepisadm.fllegada >= " + Serrores.FormatFechaHMS(tempRefParam) + " and";
			object tempRefParam2 = sdcFinal.Text + " 23:59:59";
			sql = sql + " aepisadm.fllegada <= " + Serrores.FormatFechaHMS(tempRefParam2) + " and";

			if (CbDestino.SelectedIndex > 0)
			{
				sql = sql + " aepisadm.ghospori = " + CbDestino.GetItemData(CbDestino.SelectedIndex).ToString() + " ";
			}
			else
			{
				sql = sql + " aepisadm.ghospori is not null ";
			}
			sql = sql + " order by fllegada";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, PacRemitidosBas.rc);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			Tabla_Pacientes_Remitidos(RrSql);
			RrSql.Close();
		}
		public void Tabla_Pacientes_Remitidos(DataSet RrSql)
		{
			string sqlInsertar = String.Empty;
			System.DateTime fLlegada = DateTime.FromOADate(0);

			vstPathTemp = PacRemitidosBas.ST_PATHSTRING + "\\" + PacRemitidosBas.NOMBREBASEDATOSTEMPORAL;
			if (FileSystem.Dir(vstPathTemp, FileAttribute.Normal) != "")
			{
				vbBase = true;
			} //SI ESTA CREADA
			// EL PROBLEMA ES QUE PUEDE ESTAR ABIERTA PERO NO TENER ESTA TABLA
			if (!vbBase)
			{
				//bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].CreateDatabase(vstPathTemp, DAO.LanguageConstants.dbLangSpanish, null);
				vbBase = true;
			}
			else
			{
				//bs = DBEngineHelper.Instance(UpgradeHelpers.DB.AdoFactoryManager.GetFactory())[0].OpenDatabase("<Connection-String>");
				foreach (TableDefHelper Tabla2 in bs.TableDefs)
				{
					Tabla = Tabla2;
					if (Tabla.Name == "PACIENTESREMITIDOS")
					{
						DbCommand TempCommand = bs.Connection.CreateCommand();
						TempCommand.CommandText = "DROP TABLE PACIENTESREMITIDOS";
						TempCommand.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
						TempCommand.ExecuteNonQuery();
						break;
					}
					Tabla = null;
				}

			}


			// ****************** O.Frias -- 25/10/2006 ******************
			//Cambio la longitud del campo DIAGNOSTICO de 100 a 120 caracteres.
			//bs.Execute "CREATE TABLE PACIENTESREMITIDOS(FECHA text ,NOMPACIEN TEXT(105)," & _
			//'   " DNOMHOSP TEXT(60),DMEDIORI TEXT(40),ENTIDAD_PAGADORA TEXT(40),DIAGNOSTICO TEXT(100))"
			DbCommand TempCommand_2 = bs.Connection.CreateCommand();
			TempCommand_2.CommandText = "CREATE TABLE PACIENTESREMITIDOS(FECHA text ,NOMPACIEN TEXT(105)," + 
			                            " DNOMHOSP TEXT(60),DMEDIORI TEXT(40),ENTIDAD_PAGADORA TEXT(40),DIAGNOSTICO TEXT(120))";
			TempCommand_2.Transaction = UpgradeHelpers.DB.TransactionManager.GetTransaction(bs.Connection);
			TempCommand_2.ExecuteNonQuery();
			// ****************** O.Frias -- 25/10/2006 ******************

			if (RrSql.Tables[0].Rows.Count != 0)
			{
				sqlInsertar = "SELECT * FROM PACIENTESREMITIDOS WHERE 1=2";
				rs = bs.OpenRecordset(sqlInsertar);
				RrSql.MoveFirst();
				foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
				{
					rs.AddNew();
					rs["NOMPACIEN"] = ((Convert.IsDBNull(iteration_row["DAPE1PAC"])) ? "" : Convert.ToString(iteration_row["DAPE1PAC"])).Trim() + " " + 
					                  ((Convert.IsDBNull(iteration_row["DAPE2PAC"])) ? "" : Convert.ToString(iteration_row["DAPE2PAC"])).Trim() + " " + 
					                  ((Convert.IsDBNull(iteration_row["DNOMBPAC"])) ? "" : Convert.ToString(iteration_row["DNOMBPAC"])).Trim(); //apellido2 paciente
					rs["DNOMHOSP"] = ((Convert.IsDBNull(iteration_row["DNOMHOSP"])) ? "" : Convert.ToString(iteration_row["DNOMHOSP"])).Trim();
					rs["DMEDIORI"] = ((Convert.IsDBNull(iteration_row["DMEDIORI"])) ? "" : Convert.ToString(iteration_row["DMEDIORI"])).Trim();
					if (PacRemitidosBas.vstArea == "A")
					{
						fLlegada = (Convert.IsDBNull(iteration_row["FLLEGADA"])) ? DateTime.Parse("") : Convert.ToDateTime(iteration_row["FLLEGADA"]);
					}
					else if (PacRemitidosBas.vstArea == "C")
					{ 
						fLlegada = (Convert.IsDBNull(iteration_row["FFECCITA"])) ? DateTime.Parse("") : Convert.ToDateTime(iteration_row["FFECCITA"]);
					}
					rs["FECHA"] = DateTimeHelper.ToString(fLlegada).Substring(0, Math.Min(16, DateTimeHelper.ToString(fLlegada).Length));

					if (!Convert.IsDBNull(iteration_row["dsocieda"]))
					{
						rs["ENTIDAD_PAGADORA"] = iteration_row["dsocieda"];
					}
					if (PacRemitidosBas.vstArea == "C")
					{
						//gdiagnos ddiagnos
						if (!Convert.IsDBNull(iteration_row["gdiagnos"]))
						{
							rs["diagnostico"] = iteration_row["dnombdia"];
						}
						else
						{
							rs["diagnostico"] = iteration_row["ddiagnos"];
						}
					}
					else if (PacRemitidosBas.vstArea == "A")
					{ 
						if (!Convert.IsDBNull(iteration_row["gdiaging"]))
						{
							rs["diagnostico"] = iteration_row["dnombdia"];
						}
						else
						{
							rs["diagnostico"] = iteration_row["odiaging"];
						}

					}
					rs.Update();
				}
				rs.Close();
			}
			UpgradeHelpers.DB.TransactionManager.DeEnlist(bs.Connection);
			bs.Close();

			if (CbDestino.Text == "<Todos>" || CbDestino.Text == "")
			{
				stDescripHosp = "TODOS";
			}
			else
			{
				stDescripHosp = CbDestino.Text.ToUpper();
			}
			if (!PacRemitidosBas.VstrCrystal.VfCabecera)
			{
				PacRemitidosBas.proCabCrystal(PacRemitidosBas.rc);
			}
			//LISTADO_PARTICULAR.Connect = "DSN=" & NOMBRE_DSN_ACCESS & ""
			PacRemitidosBas.LISTADO_PARTICULAR.ReportFileName = PacRemitidosBas.pathstring + "\\UIC8125r1.rpt";
			//Busqueda de la cabecera de la hoja
			//'**************************************************************************
			PacRemitidosBas.LISTADO_PARTICULAR.Formulas["Form1"] = "\"" + PacRemitidosBas.VstrCrystal.VstGrupoHo + "\"";
			PacRemitidosBas.LISTADO_PARTICULAR.Formulas["Form2"] = "\"" + PacRemitidosBas.VstrCrystal.VstNombreHo + "\"";
			PacRemitidosBas.LISTADO_PARTICULAR.Formulas["Form3"] = "\"" + PacRemitidosBas.VstrCrystal.VstDireccHo + "\"";
			PacRemitidosBas.LISTADO_PARTICULAR.Formulas["Centro"] = "\"" + stDescripHosp + "\"";
			PacRemitidosBas.LISTADO_PARTICULAR.Formulas["FechaDesde"] = "\"" + sdcInicio.Text + "\"";
			PacRemitidosBas.LISTADO_PARTICULAR.Formulas["FechaHasta"] = "\"" + sdcFinal.Text + "\"";

			string LitPersona = Serrores.ObtenerLiteralPersona(PacRemitidosBas.rc);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();
			PacRemitidosBas.LISTADO_PARTICULAR.Formulas["LitPers"] = "\"" + LitPersona + "\"";

			LitPersona = LitPersona.ToUpper() + "S" + " REMITIDOS DE OTROS CENTROS";
			PacRemitidosBas.LISTADO_PARTICULAR.Formulas["Titular"] = "\"" + LitPersona + "\"";


			PacRemitidosBas.LISTADO_PARTICULAR.datafiles[0] = vstPathTemp;
			PacRemitidosBas.LISTADO_PARTICULAR.SQLQuery = "SELECT * From PACIENTESREMITIDOS " + "\n" + "\r" + " Order By PACIENTESREMITIDOS.DNOMHOSP, PACIENTESREMITIDOS.FECHA";
			if (imprimir)
			{

				PacRemitidosBas.proDialogo_impre(PacRemitidosBas.LISTADO_PARTICULAR);

				PacRemitidosBas.LISTADO_PARTICULAR.WindowState = (int) FormWindowState.Maximized;
				PacRemitidosBas.LISTADO_PARTICULAR.Destination = 1;
				PacRemitidosBas.LISTADO_PARTICULAR.WindowShowPrintSetupBtn = true;
				PacRemitidosBas.LISTADO_PARTICULAR.Action = 1;
				PacRemitidosBas.LISTADO_PARTICULAR.PrinterStopPage = PacRemitidosBas.LISTADO_PARTICULAR.PageCount;
				PacRemitidosBas.LISTADO_PARTICULAR.Reset();

			}
			else
			{
				PacRemitidosBas.LISTADO_PARTICULAR.Destination = 0;
				PacRemitidosBas.LISTADO_PARTICULAR.WindowState = (int) FormWindowState.Maximized;
				PacRemitidosBas.LISTADO_PARTICULAR.Action = 1;
				PacRemitidosBas.LISTADO_PARTICULAR.PageCount();
				PacRemitidosBas.LISTADO_PARTICULAR.Reset();
			}
		}
		private void UIC8125F1_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}