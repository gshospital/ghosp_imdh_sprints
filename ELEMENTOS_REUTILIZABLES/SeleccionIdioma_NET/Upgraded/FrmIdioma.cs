using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;

using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SeleccionIdioma
{
	public partial class FrmIdioma
        : Telerik.WinControls.UI.RadForm
	{

		string mIdioma = String.Empty;
		public FrmIdioma()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void CbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
            object tempRefParam = 1;
            object tempRefParam2 = cbbIdioma.get_ListIndex();
			ModuloIdioma.CampIdioma = Convert.ToString(cbbIdioma.get_Column(tempRefParam, tempRefParam2));
            this.Close();
		}

		public void CbSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			//oscar C 03/09/04
			//CampIdioma = cbbIdioma.Column(1, 0)
			ModuloIdioma.CampIdioma = mIdioma;
			//------
			this.Close();
		}

		private void FrmIdioma_Load(Object eventSender, EventArgs eventArgs)
		{
			int I = 0;
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);

            //DGMORENOG TODO_2_1
			//UPGRADE_ISSUE: (2070) Constant App was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
			//UPGRADE_ISSUE: (2064) App property App.HelpFile was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Ayuda_dlls.hlp");
			//UPGRADE_ISSUE: (2064) Form property FrmIdioma.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//this.setHelpContextID(22800);

			string sql = "select * from SIDIOMAS order by dnomidio";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, ModuloIdioma.cnConexion);
			DataSet RrSQL = new DataSet();
			tempAdapter.Fill(RrSQL);

			foreach (DataRow iteration_row in RrSQL.Tables[0].Rows)
			{
				object tempRefParam = Convert.ToString(iteration_row["dnomidio"]).Trim();
				object tempRefParam2 = Type.Missing;
				cbbIdioma.AddItem(tempRefParam, tempRefParam2);
				cbbIdioma.set_Column(1, I, Convert.ToString(iteration_row["gidioma"]).Trim());
				I++;
				this.CbAceptar.Enabled = true;
			}

			//cbbIdioma.ListIndex = 0

			//oscar C 03/09/04
			mIdioma = "IDIOMA0";
			sql = "select valfanu1 from sconsglo where gconsglo ='IDIOMA'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, ModuloIdioma.cnConexion);
			RrSQL = new DataSet();
			tempAdapter_2.Fill(RrSQL);
			if (RrSQL.Tables[0].Rows.Count != 0)
			{				
				mIdioma = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["valfanu1"])) ? mIdioma : Convert.ToString(RrSQL.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
			}
			
			for (I = 0; I <= cbbIdioma.ListCount - 1; I++)
			{
                object tempRefParam3 = 1;
                object tempRefParam4 = I;
				if (Convert.ToString(cbbIdioma.get_Column(tempRefParam3, tempRefParam4)).Trim().ToUpper() == mIdioma.Trim().ToUpper())
				{
					I = Convert.ToInt32(tempRefParam4);
					cbbIdioma.set_ListIndex(I);
					break;
				}
				else
				{
					I = Convert.ToInt32(tempRefParam4);
				}
			}			
		}

		private void FrmIdioma_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}