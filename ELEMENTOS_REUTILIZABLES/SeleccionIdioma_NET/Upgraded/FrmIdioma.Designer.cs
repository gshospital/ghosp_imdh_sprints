using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SeleccionIdioma
{
	partial class FrmIdioma
	{

		#region "Upgrade Support "
		private static FrmIdioma m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static FrmIdioma DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new FrmIdioma();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbbIdioma", "Frame1", "CbSalir", "CbAceptar"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		//public AxMSForms.AxComboBox cbbIdioma;
        public UpgradeHelpers.MSForms.MSCombobox cbbIdioma;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
        public Telerik.WinControls.UI.RadButton CbSalir;
        public Telerik.WinControls.UI.RadButton CbAceptar;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbbIdioma = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.CbSalir = new Telerik.WinControls.UI.RadButton();
            this.CbAceptar = new Telerik.WinControls.UI.RadButton();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbIdioma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            //this.Frame1.BackColor = System.Drawing.SystemColors.Control;
            this.Frame1.Controls.Add(this.cbbIdioma);
            //this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame1.Location = new System.Drawing.Point(8, 16);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(265, 49);
            this.Frame1.TabIndex = 2;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Seleccionar idioma";
            // 
            // cbbIdioma
            // 
            this.cbbIdioma.Location = new System.Drawing.Point(16, 16);
            this.cbbIdioma.Name = "cbbIdioma";
            this.cbbIdioma.Size = new System.Drawing.Size(233, 20);
            this.cbbIdioma.TabIndex = 3;
            // 
            // CbSalir
            // 
            this.CbSalir.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbSalir.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbSalir.Location = new System.Drawing.Point(192, 80);
            this.CbSalir.Name = "CbSalir";
            this.CbSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbSalir.Size = new System.Drawing.Size(81, 33);
            this.CbSalir.TabIndex = 1;
            this.CbSalir.Text = "Cancelar";
            this.CbSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbSalir.Click += new System.EventHandler(this.CbSalir_Click);
            // 
            // CbAceptar
            // 
            this.CbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.CbAceptar.Enabled = false;
            this.CbAceptar.Location = new System.Drawing.Point(88, 80);
            this.CbAceptar.Name = "CbAceptar";
            this.CbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbAceptar.Size = new System.Drawing.Size(81, 33);
            this.CbAceptar.TabIndex = 0;
            this.CbAceptar.Text = "Aceptar";
            this.CbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CbAceptar.Click += new System.EventHandler(this.CbAceptar_Click);
            // 
            // FrmIdioma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(293, 130);
            this.ControlBox = false;
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.CbSalir);
            this.Controls.Add(this.CbAceptar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmIdioma";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Seleccionar idioma";
            this.Closed += new System.EventHandler(this.FrmIdioma_Closed);
            this.Load += new System.EventHandler(this.FrmIdioma_Load);
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbIdioma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbAceptar)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}