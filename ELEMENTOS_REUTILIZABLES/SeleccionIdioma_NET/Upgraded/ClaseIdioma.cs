using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace SeleccionIdioma
{
	public class ClaseIdioma
	{
		public void Recibir_Datos(SqlConnection vConexion)
		{
			//Se asigna la variable global cnConexion y la condicion de busqueda
			ModuloIdioma.cnConexion = vConexion;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref ModuloIdioma.cnConexion);			
			FrmIdioma.DefInstance.ShowDialog();
		}

		public string ActualizarIdioma()
		{
			return ModuloIdioma.CampIdioma;
		}
	}
}