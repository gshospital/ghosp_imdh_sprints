using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace RecuperarHistorico
{
	public class clsRecuperaHist
	{

		public void proReferencias(SqlConnection Conexion, object Formulario, string stNombre_Aplicacion, string stFichero_Ayuda)
		{
			DataSet RrServidor = null;
			try
			{
				RrServidor = null;

				// Asignamos los campos
				BasRecuperarHistorico.rc = Conexion;
				BasRecuperarHistorico.Formulario_Origen = Formulario;
				BasRecuperarHistorico.Nombre_Aplicacion = stNombre_Aplicacion;
				BasRecuperarHistorico.Fichero_Ayuda = stFichero_Ayuda;

				BasRecuperarHistorico.Servidor_Historico = "";

				// Tomamos el servidor

				SqlDataAdapter tempAdapter = new SqlDataAdapter("select valfanu2 from SCONSGLO where gconsglo = 'FHISTO'", BasRecuperarHistorico.rc);
				RrServidor = new DataSet();
				tempAdapter.Fill(RrServidor);

				if (RrServidor.Tables[0].Rows.Count != 0)
				{					
					if (!Convert.IsDBNull(RrServidor.Tables[0].Rows[0]["valfanu2"]))
					{						
						BasRecuperarHistorico.Servidor_Historico = Convert.ToString(RrServidor.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper();
					}
				}
				
				RrServidor.Close();
			}
			catch (System.Exception excep)
			{

				// Si hubo alg�n error mostramos el mensaje relacionado
				short tempRefParam = 1130;
                string[] tempRefParam2 = new string[] { "referenciar", "RecuperaHistorico. " + excep.Message };
				BasRecuperarHistorico.Clase_Mensaje.RespuestaMensaje(BasRecuperarHistorico.Nombre_Aplicacion,BasRecuperarHistorico.Fichero_Ayuda, tempRefParam, BasRecuperarHistorico.rc, tempRefParam2);
                				
				RrServidor.Close();

				return;
			}

		}
		public int RecuperarPaciente(string stPaciente)
		{
			try
			{
				string SqlRecuperar = String.Empty;
				SqlCommand RrRecuperar = null;

				// Comprobamos que exista servidor


				if (ExisteServidor())
				{

					// Recuperamos las consultas

					SqlRecuperar = "IMDHHI_RecuperarHistoricoConsultas";
                    //camaya todo_x_4
					//UPGRADE_ISSUE: (2064) RDO.rdoConnection method rc.CreateQuery was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//RrRecuperar = UpgradeStubs.System_Data_SqlClient_SqlConnection.CreateQuery("Recuperar", SqlRecuperar);
                    //esto es nuevo:RrRecuperar.CommandType = CommandType.StoredProcedure;
                                   

                    RrRecuperar.Parameters[0].Direction = ParameterDirection.Input;
					RrRecuperar.Parameters[0].Value = "gidenpac = '" + stPaciente + "'";

					//RrRecuperar.ExecuteNonQuery(null);
					//UPGRADE_ISSUE: (2064) RDO.rdoQuery method RrRecuperar.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//UpgradeStubs.System_Data_SqlClient_SqlCommand.Close();

					// Si todo fue bien, devolvemos 0

					return 0;
				}
				else
				{
					return -1;
				}
			}
			catch (System.Exception excep)
			{

				short tempRefParam = 1130;
                string[] tempRefParam2 = new string[] { "recuperar", "la base de datos hist�rica del paciente seleccionado. " + excep.Message};
				BasRecuperarHistorico.Clase_Mensaje.RespuestaMensaje(BasRecuperarHistorico.Nombre_Aplicacion, BasRecuperarHistorico.Fichero_Ayuda, tempRefParam, BasRecuperarHistorico.rc, tempRefParam2);
				//camaya todo_x_4
                //UPGRADE_ISSUE: (2064) RDO.rdoQuery method RrRecuperar.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//UpgradeStubs.System_Data_SqlClient_SqlCommand.Close();

				return -1;
			}
		}

		public bool ExisteServidor()
		{

			// Si no hay servidor, mostramos un mensaje de error
			if (BasRecuperarHistorico.Servidor_Historico == "")
			{
				short tempRefParam = 1100;
                string[] tempRefParam2 = new string[] { "Especificaci�n del servidor hist�rico"};
				BasRecuperarHistorico.Clase_Mensaje.RespuestaMensaje(BasRecuperarHistorico.Nombre_Aplicacion, BasRecuperarHistorico.Fichero_Ayuda, tempRefParam, BasRecuperarHistorico.rc, tempRefParam2);
			}
			// Devolvemos la existencia de servidor

			return BasRecuperarHistorico.Servidor_Historico != "";

		}

		public clsRecuperaHist()
		{
			BasRecuperarHistorico.Clase_Mensaje = new Mensajes.ClassMensajes();
		}

		~clsRecuperaHist()
		{
			BasRecuperarHistorico.Clase_Mensaje = null;
		}
	}
}