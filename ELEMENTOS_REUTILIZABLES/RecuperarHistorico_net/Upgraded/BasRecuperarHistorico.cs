using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;


namespace RecuperarHistorico
{
	internal static class BasRecuperarHistorico
	{
		public static Mensajes.ClassMensajes Clase_Mensaje = null;
		public static object Formulario_Origen = null;
		public static string Nombre_Aplicacion = String.Empty;
		public static string Fichero_Ayuda = String.Empty;
		public static SqlConnection rc = null;
		public static string Servidor_Historico = String.Empty;
	}
}