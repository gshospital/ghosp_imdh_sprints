using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using System.Transactions;
using ElementosCompartidos;

namespace Prestar
{
    public class ClassHTC421F2
    {

        private dynamic RefFormulario = null;
        private ClassHTC421F2 RefClase = null;
        private SqlConnection RefRc = null;
        private DataSet Rs = null;
        public int IError = 0;
        public string StStComando = String.Empty;
        public Mensajes.ClassMensajes ClassMensaje = null;

        private void ModificarEstado(string Historia, string IContenido, int SerPropiet)
        {
            string Msj = String.Empty;
            string stcomando = String.Empty;

            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {

                try
                {
                   
                    //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                    if (RefRc.State == ConnectionState.Open)



                    stcomando = "Select IESTHISTORIA from HDOSSIER where GHISTORIA=";
                    stcomando = stcomando + Historia;
                    stcomando = stcomando + " and ICONTENIDO=";
                    stcomando = stcomando + "'" + IContenido.Trim() + "'";
                    stcomando = stcomando + " and GSERPROPIET=" + SerPropiet.ToString();
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(stcomando, RefRc);
                    Rs = new DataSet();
                    tempAdapter.Fill(Rs);
                    
                    Rs.Edit();

                    Rs.Tables[0].Rows[0]["IESTHISTORIA"] = "P";

                    string tempQuery = Rs.Tables[0].TableName;
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                    tempAdapter_2.Update(Rs, Rs.Tables[0].TableName);
                    scope.Complete();

                }
                catch (Exception e)
                {
                    //Dim er As rdoError
                  
                    if (Information.Err().Number != 0)
                    {
                      
                        Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
                        MessageBox.Show(Msj, "Error");
                    }
                  
                    Rs.Close();
                  
                    scope.Complete();
                    IError = 1;
                    RefFormulario.RecogerError(IError);

                    return;
                }
            }
        }



        private void PrestarHMOVCONS(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string SerSoli, string Unidad, object FPrevDevol_optional, string Observaciones)
        {
            string FPrevDevol = (FPrevDevol_optional == Type.Missing) ? String.Empty : FPrevDevol_optional as string;
            string Msj = String.Empty;
            StringBuilder stcomando = new StringBuilder();
            //diego 29/05/2006 -> A�ADO EL PAR�METRO Optional Observaciones As String

            int INumRegistros = 0;


            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {
                try
                    {
                if (IContenido.Trim() == "A")
                { //Si el contenido es ambas
                    IContenido = "H"; //habra que prestar 2 registros, H y P
                    INumRegistros = 2;
                }
                else
                {
                    INumRegistros = 1;
                }
             
                      //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                        if (RefRc.State == ConnectionState.Open)


                            for (int I = 1; I <= INumRegistros; I++)
                            {
                                //diego 29/05/2006 -> a�ado DOBSMOVHIS al SELECT
                                stcomando = new StringBuilder("Select FPRESTAMOH,FPREVDEVOL,IESTMOVIH, DOBSMOVHIS from HMOVCONS");
                                stcomando.Append(" where GHISTORIA=" + Historia);
                                stcomando.Append(" and ICONTENIDO=" + "'" + IContenido.Trim() + "'");
                                stcomando.Append(" and GSERSOLI=" + Conversion.Val(SerSoli).ToString());
                                stcomando.Append(" and GSALACON=" + "'" + Unidad.Trim() + "'");
                                object tempRefParam = Fpeticion.Trim();
                                stcomando.Append(" and FPETICIO=" + "" + Serrores.FormatFechaHMS(tempRefParam) + "");
                                stcomando.Append(" and GSERPROPIET=" + SerPropiet.ToString());
                                SqlDataAdapter tempAdapter = new SqlDataAdapter(stcomando.ToString(), RefRc);
                                Rs = new DataSet();
                                tempAdapter.Fill(Rs);
                             
                                Rs.Edit();

                                Rs.Tables[0].Rows[0]["FPRESTAMOH"] = DateTime.Parse(FPrestamo.Trim() + " " + DateTimeHelper.ToString(DateTimeHelper.Time)).ToString("dd/MM/yyyy HH:mm");
                              
                                if (FPrevDevol_optional != Type.Missing && !Convert.IsDBNull(FPrevDevol))
                                {

                                    Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DateTime.Parse(FPrevDevol.Trim());
                                }
                                else
                                {

                                   
                                    Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DBNull.Value;
                                }

                                Rs.Tables[0].Rows[0]["IESTMOVIH"] = "E";

                                Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = Observaciones; //diego 29/05/2006

                                string tempQuery = Rs.Tables[0].TableName;
                                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                                tempAdapter_2.Update(Rs, Rs.Tables[0].TableName);
                               
                                Rs.Close();
                                ModificarEstado(Historia, IContenido, SerPropiet);
                                if (INumRegistros != 1)
                                {
                                    IContenido = "P";
                                }
                            }
                         
                                 scope.Complete();


                        IError = 0;
                       
                        RefFormulario.RecogerError(IError);
                    }

                    catch (Exception e)
                    {
                        //Dim er As rdoError
                       
                        if (Information.Err().Number != 0)
                        {
                          
                            Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
                            MessageBox.Show(Msj, "Error");
                        }
                       
                        Rs.Close();
                        scope.Complete();

                        IError = 1;
                      
                        RefFormulario.RecogerError(IError);

                        return;

                    }
                }
            }
        

		private void PrestarHMOVCONS(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string SerSoli, string Unidad, object FPrevDevol_optional)
		{
			PrestarHMOVCONS(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, SerSoli, Unidad, FPrevDevol_optional, String.Empty);
		}

		private void PrestarHMOVCONS(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string SerSoli, string Unidad)
		{
			PrestarHMOVCONS(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, SerSoli, Unidad, Type.Missing, String.Empty);
		}
        private void PrestarHMOVEXTE(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string Unidad, object FPrevDevol_optional, string Observaciones)
        {
            string FPrevDevol = (FPrevDevol_optional == Type.Missing) ? String.Empty : FPrevDevol_optional as string;
            string Msj = String.Empty;
            StringBuilder stcomando = new StringBuilder();
            int INumRegistros = 0;

            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {
                try
                {
                    if (IContenido.Trim() == "A")
                    { //Si el contenido es ambas
                        IContenido = "H"; //habra que prestar 2 registros, H y P
                        INumRegistros = 2;
                    }
                    else
                    {
                        INumRegistros = 1;
                    }
                    
                    //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                    if (RefRc.State == ConnectionState.Open)



                        for (int I = 1; I <= INumRegistros; I++)
                        {
                            //diego 29/05/2006 -> a�ado DOBSMOVHIS al SELECT
                            stcomando = new StringBuilder("Select FPRESTAMOH,FPREVDEVOL,IESTMOVIH, DOBSMOVHIS from HMOVEXTE");
                            stcomando.Append(" where GHISTORIA=" + Historia);
                            stcomando.Append(" and ICONTENIDO=" + "'" + IContenido.Trim() + "'");
                            stcomando.Append(" and GENTIEXT=" + "'" + Unidad.Trim() + "'");
                            object tempRefParam = Fpeticion.Trim();
                            stcomando.Append(" and FPETICIO=" + "" + Serrores.FormatFechaHMS(tempRefParam) + "");
                            stcomando.Append(" and GSERPROPIET=" + SerPropiet.ToString());
                            SqlDataAdapter tempAdapter = new SqlDataAdapter(stcomando.ToString(), RefRc);
                            Rs = new DataSet();
                            tempAdapter.Fill(Rs);
                          
                            Rs.Edit();
                          
                            Rs.Tables[0].Rows[0]["FPRESTAMOH"] = DateTime.Parse(FPrestamo.Trim() + " " + DateTimeHelper.ToString(DateTimeHelper.Time)).ToString("dd/MM/yyyy HH:mm");
                         
                            if (FPrevDevol_optional != Type.Missing && !Convert.IsDBNull(FPrevDevol))
                            {
                               
                                Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DateTime.Parse(FPrevDevol.Trim());
                            }
                            else
                            {
                                
                                Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DBNull.Value;
                            }
                          
                            Rs.Tables[0].Rows[0]["IESTMOVIH"] = "E";
                          
                            Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = Observaciones; //diego 29/05/2006
                                                                             
                            string tempQuery = Rs.Tables[0].TableName;
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                            SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                            tempAdapter_2.Update(Rs, Rs.Tables[0].TableName);
                          
                            Rs.Close();
                            ModificarEstado(Historia, IContenido, SerPropiet);
                            if (INumRegistros != 1)
                            {
                                IContenido = "P";
                            }
                        }
                  
                    scope.Complete();

                    IError = 0;
                    
                    RefFormulario.RecogerError(IError);
                }
                catch (Exception e)
                {
                    //Dim er As rdoError
                  
                    if (Information.Err().Number != 0)
                    {
                      
                        Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
                        MessageBox.Show(Msj, "Error");
                    }
                   
                    Rs.Close();
                 
                    scope.Complete();
                    IError = 1;
                  
                    RefFormulario.RecogerError(IError);

                    return;
                }
            }
        }

		private void PrestarHMOVEXTE(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string Unidad, object FPrevDevol_optional)
		{
			PrestarHMOVEXTE(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, Unidad, FPrevDevol_optional, String.Empty);
		}

		private void PrestarHMOVEXTE(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string Unidad)
		{
			PrestarHMOVEXTE(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, Unidad, Type.Missing, String.Empty);
		}

        private void PrestarHMOVSERV(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string SerSoli, object FPrevDevol_optional, string Observaciones)
        {
            string FPrevDevol = (FPrevDevol_optional == Type.Missing) ? String.Empty : FPrevDevol_optional as string;
            string Msj = String.Empty;
            StringBuilder stcomando = new StringBuilder();
            int INumRegistros = 0;


            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {


                try
                {
                    if (IContenido.Trim() == "A")
                    { //Si el contenido es ambas
                        IContenido = "H"; //habra que prestar 2 registros, H y P
                        INumRegistros = 2;
                    }
                    else
                    {
                        INumRegistros = 1;
                    }
                  

                    //Para que el transaction scope funcione, se debe abrir la conexi�n despues de iniciar la transacci�n
                    if (RefRc.State == ConnectionState.Open)


                        for (int I = 1; I <= INumRegistros; I++)
                        {
                            //diego 29/05/2006 -> a�ado DOBSMOVHIS al SELECT
                            stcomando = new StringBuilder("Select FPRESTAMOH,FPREVDEVOL,IESTMOVIH, DOBSMOVHIS from HMOVSERV");
                            stcomando.Append(" where GHISTORIA=" + Historia);
                            stcomando.Append(" and ICONTENIDO=" + "'" + IContenido.Trim() + "'");
                            stcomando.Append(" and GSERSOLI=" + Conversion.Val(SerSoli).ToString());
                            object tempRefParam = Fpeticion.Trim();
                            stcomando.Append(" and FPETICIO=" + "" + Serrores.FormatFechaHMS(tempRefParam) + "");
                            stcomando.Append(" and GSERPROPIET=" + SerPropiet.ToString());
                            SqlDataAdapter tempAdapter = new SqlDataAdapter(stcomando.ToString(), RefRc);
                            Rs = new DataSet();
                            tempAdapter.Fill(Rs);
                           
                            Rs.Edit();
                          
                            Rs.Tables[0].Rows[0]["FPRESTAMOH"] = DateTime.Parse(FPrestamo.Trim() + " " + DateTimeHelper.ToString(DateTimeHelper.Time)).ToString("dd/MM/yyyy HH:mm");
                            
                            if (FPrevDevol_optional != Type.Missing && !Convert.IsDBNull(FPrevDevol))
                            {
                             
                                Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DateTime.Parse(FPrevDevol.Trim());
                            }
                            else
                            {
                             
                                Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DBNull.Value;
                            }
                         
                            Rs.Tables[0].Rows[0]["IESTMOVIH"] = "E";
                          
                            Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = Observaciones; //diego 29/05/2006
                                                                             
                            string tempQuery = Rs.Tables[0].TableName;
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                            SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                            tempAdapter_2.Update(Rs, Rs.Tables[0].TableName);
                            Rs.Close();
                            ModificarEstado(Historia, IContenido, SerPropiet);
                            if (INumRegistros != 1)
                            {
                                IContenido = "P";
                            }
                        }
                    
                    scope.Complete();
                    IError = 0;
                  
                    RefFormulario.RecogerError(IError);
                }
                catch (Exception e)
                {
                    //Dim er As rdoError
                  
                    if (Information.Err().Number != 0)
                    {
                     
                        Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
                        MessageBox.Show(Msj, "Error");
                    }
                   
                    Rs.Close();
                    scope.Complete();

                    IError = 1;
                   
                    RefFormulario.RecogerError(IError);

                    return;
                }
            }
        }

		private void PrestarHMOVSERV(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string SerSoli, object FPrevDevol_optional)
		{
			PrestarHMOVSERV(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, SerSoli, FPrevDevol_optional, String.Empty);
		}

		private void PrestarHMOVSERV(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string SerSoli)
		{
			PrestarHMOVSERV(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, SerSoli, Type.Missing, String.Empty);
		}
        private void PrestarHMOVUENF(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string Unidad, object FPrevDevol_optional, string Observaciones)
        {
            string FPrevDevol = (FPrevDevol_optional == Type.Missing) ? String.Empty : FPrevDevol_optional as string;
            string Msj = String.Empty;
            StringBuilder stcomando = new StringBuilder();
            int INumRegistros = 0;

            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {
                try

                {
                    if (IContenido.Trim() == "A")
                    { //Si el contenido es ambas
                        IContenido = "H"; //habra que prestar 2 registros, H y P
                        INumRegistros = 2;
                    }
                    else
                    {
                        INumRegistros = 1;
                    }
                   
                    if (RefRc.State == ConnectionState.Open)


                        for (int I = 1; I <= INumRegistros; I++)
                        {
                            //diego 29/05/2006 -> a�ado DOBSMOVHIS al SELECT
                            stcomando = new StringBuilder("Select FPRESTAMOH,FPREVDEVOL,IESTMOVIH, DOBSMOVHIS from HMOVUENF");
                            stcomando.Append(" where GHISTORIA=" + Historia);
                            stcomando.Append(" and ICONTENIDO=" + "'" + IContenido.Trim() + "'");
                            stcomando.Append(" and GUNIDENF=" + "'" + Unidad.Trim() + "'");
                            object tempRefParam = Fpeticion.Trim();
                            stcomando.Append(" and FPETICIO=" + "" + Serrores.FormatFechaHMS(tempRefParam) + "");
                            stcomando.Append(" and GSERPROPIET=" + SerPropiet.ToString());
                            SqlDataAdapter tempAdapter = new SqlDataAdapter(stcomando.ToString(), RefRc);
                            Rs = new DataSet();
                            tempAdapter.Fill(Rs);
                          
                            Rs.Edit();
                           
                            Rs.Tables[0].Rows[0]["FPRESTAMOH"] = DateTime.Parse(FPrestamo.Trim() + " " + DateTimeHelper.ToString(DateTimeHelper.Time)).ToString("dd/MM/yyyy HH:mm");
                          
                            if (FPrevDevol_optional != Type.Missing && !Convert.IsDBNull(FPrevDevol))
                            {

                                Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DateTime.Parse(FPrevDevol.Trim());
                            }
                            else
                            {
                               
                                Rs.Tables[0].Rows[0]["FPREVDEVOL"] = DBNull.Value;
                            }
                        
                            Rs.Tables[0].Rows[0]["IESTMOVIH"] = "E";
                           
                            Rs.Tables[0].Rows[0]["DOBSMOVHIS"] = Observaciones; //diego 29/05/2006

                            string tempQuery = Rs.Tables[0].TableName;
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
                            SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                            tempAdapter_2.Update(Rs, Rs.Tables[0].TableName);
                          
                            Rs.Close();
                            ModificarEstado(Historia, IContenido, SerPropiet);
                            if (INumRegistros != 1)
                            {
                                IContenido = "P";
                            }
                        }
                   
                    scope.Complete();
                    IError = 0;
                  
                    RefFormulario.RecogerError(IError);
                }
                catch (Exception e)
                {
                    //Dim er As rdoError
                   
                    if (Information.Err().Number != 0)
                    {
                        
                        Msj = "Error # " + Conversion.Str(Information.Err().Number) + " fue generado por " + e.Source + "\r" + e.Message;
                        MessageBox.Show(Msj, "Error");
                    }
                   
                    Rs.Close();
                    
                    scope.Complete();

                    IError = 1;
                 
                    RefFormulario.RecogerError(IError);

                    return;
                }
            }
        }

		private void PrestarHMOVUENF(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string Unidad, object FPrevDevol_optional)
		{
			PrestarHMOVUENF(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, Unidad, FPrevDevol_optional, String.Empty);
		}

		private void PrestarHMOVUENF(string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, string Unidad)
		{
			PrestarHMOVUENF(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, Unidad, Type.Missing, String.Empty);
		}

		public void Recibir(string TipoU, string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, object FPrevDevol_optional, object SerSoli_optional, object Unidad_optional, string Observaciones)
		{
			string SerSoli = (SerSoli_optional == Type.Missing) ? String.Empty : SerSoli_optional as string;
			string FPrevDevol = (FPrevDevol_optional == Type.Missing) ? String.Empty : FPrevDevol_optional as string;
			string Unidad = (Unidad_optional == Type.Missing) ? String.Empty : Unidad_optional as string;
			//diego 29/06/2006 -> a�ado el par�metro Optional Observaciones As String
			switch(TipoU.Trim())
			{
				case "S" : 
					if (SerSoli_optional != Type.Missing)
					{
						if (FPrevDevol_optional != Type.Missing)
						{
							PrestarHMOVSERV(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, SerSoli, FPrevDevol, Observaciones);
						}
						else
						{
							PrestarHMOVSERV(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, SerSoli, String.Empty, Observaciones);
						}
					}
					else
					{
						//Call MsgBox("El Servicio solicitante no puede estar vacio para el tipo 'S'")
						short tempRefParam = 3200;
						string [] tempRefParam2 = new string[]  {"el tipo es Servicio Solicitante", "servicio solicitante"};
						ClassMensaje.RespuestaMensaje(Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam,  RefRc,  tempRefParam2);
					} 
					break;
				case "U" : 
					if (Unidad_optional != Type.Missing)
					{
						if (FPrevDevol_optional != Type.Missing)
						{
							PrestarHMOVUENF(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, Unidad, FPrevDevol, Observaciones);
						}
						else
						{
							PrestarHMOVUENF(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, Unidad, String.Empty, Observaciones);
						}
					}
					else
					{
						//Call MsgBox("La unidad de destino no puede estar vacio para el tipo 'U'")
						short tempRefParam3 = 3200;
						string[] tempRefParam4 = new string[] { "el tipo es Unidad de Enfermer�a", "unidad de destino"};
						ClassMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam3,  RefRc,  tempRefParam4);
					} 
					break;
				case "C" : 
					if (Unidad_optional != Type.Missing && SerSoli_optional != Type.Missing)
					{
						if (FPrevDevol_optional != Type.Missing)
						{
							PrestarHMOVCONS(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, SerSoli, Unidad, FPrevDevol, Observaciones);
						}
						else
						{
							PrestarHMOVCONS(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, SerSoli, Unidad, String.Empty, Observaciones);
						}
					}
					else
					{
						//Call MsgBox("La unidad de destino y el servicio solicitante no pueden estar vacios para el tipo 'C'")
						short tempRefParam5 = 3200;
						string [] tempRefParam6 = new string[] { "el tipo es Consulta", "unidad de destino y servicio solicitante"};
						ClassMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam5, RefRc,  tempRefParam6);
					} 
					break;
				case "E" :
                    if (Unidad_optional != Type.Missing)
                    {
                        if (FPrevDevol_optional != Type.Missing)
                        {
                            PrestarHMOVEXTE(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, Unidad, FPrevDevol, Observaciones);
                        }
                        else
                        {
                            PrestarHMOVEXTE(Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, Unidad, String.Empty, Observaciones);
                        }
                    }
                    else
                    {
                        //Call MsgBox("La unidad de destino no puede estar vacio para el tipo 'E'")
                        short tempRefParam7 = 3200;
                        string[] tempRefParam8 = new string[] { "el tipo es Entidad Externa", "unidad de destino"};
						ClassMensaje.RespuestaMensaje( Serrores.vNomAplicacion,  Serrores.vAyuda,  tempRefParam7,  RefRc,  tempRefParam8);
					} 
					break;
			}
		}

		public void Recibir(string TipoU, string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, object FPrevDevol_optional, object SerSoli_optional, object Unidad_optional)
		{
			Recibir(TipoU, Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, FPrevDevol_optional, SerSoli_optional, Unidad_optional, String.Empty);
		}

		public void Recibir(string TipoU, string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, object FPrevDevol_optional, object SerSoli_optional)
		{
			Recibir(TipoU, Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, FPrevDevol_optional, SerSoli_optional, Type.Missing, String.Empty);
		}

		public void Recibir(string TipoU, string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo, object FPrevDevol_optional)
		{
			Recibir(TipoU, Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, FPrevDevol_optional, Type.Missing, Type.Missing, String.Empty);
		}

		public void Recibir(string TipoU, string Historia, ref string IContenido, string Fpeticion, int SerPropiet, string FPrestamo)
		{
			Recibir(TipoU, Historia, ref IContenido, Fpeticion, SerPropiet, FPrestamo, Type.Missing, Type.Missing, Type.Missing, String.Empty);
		}


		public void ReferenciasClass(ClassHTC421F2 RefClass, object RefForm, SqlConnection rc)
		{
			RefFormulario = RefForm;
			RefClase = RefClass;
			RefRc = rc;
			ClassMensaje = new Mensajes.ClassMensajes();
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref RefRc);
			Serrores.AjustarRelojCliente(RefRc);
		}
	}
}