using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using UpgradeStubs;
using Telerik.WinControls;

namespace Numeracion
{
	public class SIGUIENTE
	{
		public int SIG_NUMERO(string codigo, SqlConnection RcDLL)
		{
			int result = 0;			
			string S_codigo = String.Empty;
			string consulta = String.Empty;
			string tipo = String.Empty;
			string fecha_actual = String.Empty;
			string fecha_tabla = String.Empty;
			string a�o_actual = String.Empty;
			string a�o_tabla = String.Empty;
			SqlConnection Rcconstantes = null;
			DataSet Rrconstantes = null;
			bool pertenece = false;
			int NumeroIntentos = 0; // Guarda el numero de intentos de acceso a numeracion

			// COMPRUEBA SI EL PARAMETRO ES CORRECTO
			pertenece = false;
			S_codigo = codigo.Trim().ToUpper();
			Rcconstantes = RcDLL;
            //GENERA LA CONSULTA

            
            consulta = "select * from S" + S_codigo.Substring(1) + " where " + "gconsglo = '" + S_codigo + "'";
            NumeroIntentos = 1;

            while (NumeroIntentos < 4)
            {
                try
                {
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(consulta, Rcconstantes);
                    Rrconstantes = new DataSet();
                    tempAdapter.Fill(Rrconstantes);
                    if (Rrconstantes.Tables[0].Rows.Count == 1)
                    {
                        //INICIALIZA LAS VARIABLES                        
                        tipo = Convert.ToString(Rrconstantes.Tables[0].Rows[0]["valfanu2"]).Substring(0, Math.Min(1, Convert.ToString(Rrconstantes.Tables[0].Rows[0]["valfanu2"]).Length));                        
                        fecha_tabla = Convert.ToDateTime(Rrconstantes.Tables[0].Rows[0]["valfanu1"]).ToString("dd/MM/yyyy");                        
                        a�o_tabla = DateTime.Parse(Convert.ToDateTime(Rrconstantes.Tables[0].Rows[0]["valfanu1"]).ToString("dd/MM/yyyy")).Year.ToString();
                        fecha_actual = DateTime.Today.ToString("dd/MM/yyyy");
                        a�o_actual = DateTime.Parse(fecha_actual).Year.ToString();

                        //ACTUALIZA EN FUNCION DEL TIPO DE CONSTANTE
                        if (tipo.ToUpper() == "A")
                        {
                            if (a�o_actual != a�o_tabla && String.CompareOrdinal(a�o_tabla, a�o_actual) < 0)
                            {
                                result = 1;
                                Rrconstantes.Tables[0].Rows[0]["nnumeri1"] = 2;
                                Rrconstantes.Tables[0].Rows[0]["valfanu1"] = fecha_actual;
                                string tempQuery = Rrconstantes.Tables[0].TableName;
                                SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                                tempAdapter.Update(Rrconstantes, tempQuery);
                                break;
                            }
                            else
                            {
                                result = Convert.ToInt32(Rrconstantes.Tables[0].Rows[0]["nnumeri1"]);
                                Rrconstantes.Tables[0].Rows[0]["nnumeri1"] = Convert.ToDouble(Rrconstantes.Tables[0].Rows[0]["nnumeri1"]) + 1;
                                string tempQuery_2 = Rrconstantes.Tables[0].TableName;
                                SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
                                tempAdapter.Update(Rrconstantes, tempQuery_2);
                                break;
                            }
                        }
                        else
                        {
                            if (tipo.ToUpper() == "T")
                            {
                                result = Convert.ToInt32(Rrconstantes.Tables[0].Rows[0]["nnumeri1"]);
                                Rrconstantes.Tables[0].Rows[0]["nnumeri1"] = Convert.ToDouble(Rrconstantes.Tables[0].Rows[0]["nnumeri1"]) + 1;
                                string tempQuery_3 = Rrconstantes.Tables[0].TableName;
                                SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter);
                                tempAdapter.Update(Rrconstantes, tempQuery_3);
                                break;
                            }
                            else
                            {
                                if (fecha_actual != fecha_tabla)
                                {
                                    result = 1;
                                    Rrconstantes.Tables[0].Rows[0]["nnumeri1"] = 2;
                                    Rrconstantes.Tables[0].Rows[0]["valfanu1"] = fecha_actual;
                                    string tempQuery_4 = Rrconstantes.Tables[0].TableName;
                                    SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter);
                                    tempAdapter.Update(Rrconstantes, tempQuery_4);
                                    break;
                                }
                                else
                                {
                                    result = Convert.ToInt32(Rrconstantes.Tables[0].Rows[0]["nnumeri1"]);
                                    Rrconstantes.Tables[0].Rows[0]["nnumeri1"] = Convert.ToDouble(Rrconstantes.Tables[0].Rows[0]["nnumeri1"]) + 1;
                                    string tempQuery_5 = Rrconstantes.Tables[0].TableName;
                                    SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter);
                                    tempAdapter.Update(Rrconstantes, tempQuery_5);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        SIG_NUMERO(codigo, RcDLL);
                    }
                    NumeroIntentos++;
                }
                catch(Exception ex)
                {
                    if (NumeroIntentos > 3)
                    {
                        RadMessageBox.Show("Error en la funci�n siguiente n�mero", Application.ProductName);
                    }
                    else
                    {

                    }
                }
            };

            //CIERRA LA CONEXION
            if (NumeroIntentos < 4)
            {
                Rrconstantes.Close();
                return result;
            }           
			return result;
		}

		private static SIGUIENTE _DefaultInstance = null;
		public static SIGUIENTE DefaultInstance
		{
			get
			{
				if (_DefaultInstance == null)
				{
					_DefaultInstance = new SIGUIENTE();
				}
				return _DefaultInstance;
			}
		}

	}
}