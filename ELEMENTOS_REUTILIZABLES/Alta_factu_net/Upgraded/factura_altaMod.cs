using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace Alta_factu
{
	internal static class Module1
	{

		public static SqlConnection rcalta = null;
		public static string stGidenpac = String.Empty;
		public static DataSet RrDmovfact = null;
		public static int iGanoregi = 0;
		public static int IGnumregi = 0;
		public static string fFechafactu = String.Empty;
		public static bool bError = false;
		public static string ITiposer = String.Empty;
		public static int StSer_paciente = 0;
		public static string StSociedad = String.Empty;
		public static string StAfilUrgencias = String.Empty;
		public static string StInspUrgencias = String.Empty;
		public static string stSep_Mil = String.Empty;
		public static string stSep_Decimal = String.Empty;
		public static int viUle = 0; // c�digo de servicio correspondiente a ULE para fact. mensual
		public static string sTipoFacturacion = String.Empty; // indicador de si la sociedad se factura mensual(M), mixta (X), al alta (A)
		public static bool bEsAlta = false; // es la facturaci�n mensual y el paciente ya se ha ido
		public static int iFarmacia = 0;
		public static int iCocina = 0;
		public static string sReingreso = String.Empty;
		public static string VstItipingr = String.Empty; //tipo de ingreso de aepisadm
		// indicador de si es una consulta o se est� facturando realmente
		// true = consulta
		public static bool bTemp = false;
		public static string vstFaltplan = String.Empty;
		public static System.DateTime dFperfact = DateTime.FromOADate(0); // fecha a grabar en fperfact

		// intervalo de fechas de facturaci�n recibido
		public static string fPini = String.Empty;
		public static string fPfin = String.Empty;

		// indicador de si es la facturaci�n mensual o no
		public static bool bMensual = false;

		public static DataSet rrEprestac = null;

		public static int iPrivado = 0; // c�digo correspondiente a pagador privado
		public const int LOCALE_SYSTEM_DEFAULT = 0x800;
		public const int LOCALE_SDECIMAL = 0xE; //  separador de decimales
		public const int LOCALE_STHOUSAND = 0xF; //  separador de miles

		public static string FechaLLegadaAdmision = String.Empty; // se utiliza para saber cuales son los apuntes de admision
		// realizados en urgencias
		public static bool bLLamadaIFMS = false;
		//[DllImport("kernel32.dll", EntryPoint = "GetLocaleInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLCData, int cchData);


		internal static void ObtenerLocale()
		{
			//'    Dim buffer As String * 100
			//'    Dim dl&
			//'
			//'    'SysInfo.Cls
			//'
			//'    #If Win32 Then
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDECIMAL, buffer, 99)
			//'        stSep_Decimal = LPSTRToVBString(buffer)
			//'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STHOUSAND, buffer, 99)
			//'        stSep_Mil = LPSTRToVBString(buffer)
			//'    #Else
			//'        Print " Not implemented under Win16"
			//'    #End If
			//' CURIAE - EQUIPO DE MIGRACION: NUEVA FORMA DE OBETENER LA CONFIGURACION REGIONAL


		}

		internal static string LPSTRToVBString(string s)
		{
			int nullpos = (s.IndexOf(Strings.Chr(0).ToString()) + 1);
			if (nullpos > 0)
			{
				return s.Substring(0, Math.Min(nullpos - 1, s.Length));
			}
			else
			{
				return "";
			}
		}

		internal static void ComprobarSocMensual(int iSociedad, string fecha = "", object sInspFact = null)
		{
			// compruebo el tipo de facturaci�n de la sociedad
			string tStLeer = String.Empty;

			sTipoFacturacion = "A";
			if (Convert.ToString(sInspFact) != "")
			{
				// la sociedad es Seguridad Social
				tStLeer = "Select imensual from dinspecc where ginspecc = '" + Convert.ToString(sInspFact) + "'";
			}
			else
			{
				tStLeer = "Select imensual from dsocieda where gsocieda = " + iSociedad.ToString();
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tStLeer, rcalta);
			DataSet tRrLeer = new DataSet();
			tempAdapter.Fill(tRrLeer);
			if (tRrLeer.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(tRrLeer.Tables[0].Rows[0]["imensual"]))
				{
					// valores de imensual: M = mensual, X = mixta (mensual y al alta), A = al alta
					sTipoFacturacion = Convert.ToString(tRrLeer.Tables[0].Rows[0]["imensual"]).Trim();
				}
			}

		}

		internal static bool FacturaFarmacosLaSociedad(int iSociedad, string stInspFact)
		{
			// compruebo el tipo de facturaci�n de los f�rmacos de la sociedad
			string tStLeer = String.Empty;

			bool btest = true;
			if (stInspFact != "")
			{
				// la sociedad es Seguridad Social
				tStLeer = "Select ifafarma from dinspecc where ginspecc = '" + stInspFact + "'";
			}
			else
			{
				tStLeer = "Select ifafarma from dsocieda where gsocieda = " + iSociedad.ToString();
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tStLeer, rcalta);
			DataSet tRrLeer = new DataSet();
			tempAdapter.Fill(tRrLeer);
			if (tRrLeer.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(tRrLeer.Tables[0].Rows[0]["ifafarma"]))
				{
					//Si es "S" devolver� true y si es "N", false
					btest = (Convert.ToString(tRrLeer.Tables[0].Rows[0]["ifafarma"]).Trim() == "S");
				}
			}
			return btest;

		}


		internal static int fnBuscaSerResp(string ParaFecha, int Paraano, int Paranum)
		{
			//ParaFecha tiene formato "dd/mm/yyyy hh:nn:ss"
			int stServicio = 0;
			object tempRefParam = ParaFecha;
			object tempRefParam2 = ParaFecha;
			string sstemp = "select dservici.gservici from amovimie,dservici where ganoregi=" + Paraano.ToString() + " and gnumregi=" + Paranum.ToString() + " and fmovimie<=" + Serrores.FormatFechaHMS(tempRefParam) + " and " + " (ffinmovi>=" + Serrores.FormatFechaHMS(tempRefParam2) + " or ffinmovi is null) and amovimie.gservior=dservici.gservici";
			ParaFecha = Convert.ToString(tempRefParam2);
			ParaFecha = Convert.ToString(tempRefParam);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sstemp, rcalta);
			DataSet RrTemp = new DataSet();
			tempAdapter.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				stServicio = Convert.ToInt32(RrTemp.Tables[0].Rows[0]["gservici"]);
			}
			RrTemp.Close();
			return stServicio;
		}

		internal static object proRellenarComunesPrestac(int iSocie)
		{

			// funci�n que rellena todos los datos de dmovfact comunes a los
			// diferentes casos de prestaciones (protocolos, normales ...)
			RrDmovfact.Tables[0].Rows[0]["gidenpac"] = stGidenpac;
			RrDmovfact.Tables[0].Rows[0]["ITPACFAC"] = "I";
			RrDmovfact.Tables[0].Rows[0]["gsersoli"] = rrEprestac.Tables[0].Rows[0]["gsersoli"];
			RrDmovfact.Tables[0].Rows[0]["gserreal"] = rrEprestac.Tables[0].Rows[0]["gservici"];
			RrDmovfact.Tables[0].Rows[0]["gsocieda"] = iSocie;
			RrDmovfact.Tables[0].Rows[0]["gcodeven"] = "PR";
			RrDmovfact.Tables[0].Rows[0]["ffinicio"] = Convert.ToDateTime(rrEprestac.Tables[0].Rows[0]["frealiza"]);
			RrDmovfact.Tables[0].Rows[0]["ffechfin"] = Convert.ToDateTime(rrEprestac.Tables[0].Rows[0]["frealiza"]);
			RrDmovfact.Tables[0].Rows[0]["fsistema"] = DateTime.Parse(fFechafactu);
			// antes se enviaba el n� de placas 31/3/2000
			if (Convert.IsDBNull(rrEprestac.Tables[0].Rows[0]["ncantida"]))
			{
				RrDmovfact.Tables[0].Rows[0]["ncantida"] = 1;
			}
			else
			{
				RrDmovfact.Tables[0].Rows[0]["ncantida"] = rrEprestac.Tables[0].Rows[0]["ncantida"];
			}
			if (!Convert.IsDBNull(rrEprestac.Tables[0].Rows[0]["iautosoc"]))
			{
				RrDmovfact.Tables[0].Rows[0]["iautosoc"] = rrEprestac.Tables[0].Rows[0]["iautosoc"];
			}
			if (!Convert.IsDBNull(rrEprestac.Tables[0].Rows[0]["iexiauto"]))
			{
				RrDmovfact.Tables[0].Rows[0]["iexiauto"] = rrEprestac.Tables[0].Rows[0]["iexiauto"];
			}
			//**el servicio responsable el que hizo la peticion
			if (ITiposer == "U")
			{
				RrDmovfact.Tables[0].Rows[0]["gserresp"] = StSer_paciente;
			}
			else
			{
				RrDmovfact.Tables[0].Rows[0]["gserresp"] = fnBuscaSerResp(Convert.ToString(rrEprestac.Tables[0].Rows[0]["frealiza"]), iGanoregi, IGnumregi);
			}
			RrDmovfact.Tables[0].Rows[0]["fperfact"] = dFperfact;

			return null;
		}
		internal static void proModiEINTERCO()
		{
			string stEinterco = String.Empty;
			DataSet rrEinterco = null;

			string stEinterco2 = String.Empty;
			DataSet rrEinterco2 = null;


			int iSocie = 0;

			int iAnoU = 0;
			int lNumU = 0; //episodio de urgencias
			int iSocU = 0;
			string sAfiU = String.Empty;
			string sInsU = String.Empty;
			string sInspecFact = String.Empty;

			object vMoticon = null; // para guardar el campo TEXT

			// ya viene en formato dd/mm/yyyy hh:nn:ss
			string stFechaFinFormateada = fPfin;

			// facturo todo lo pendiente de facturar
			// hasta la fecha de fin de facturaci�n 20/7/1999
			if (bEsAlta)
			{
				// ya se han trasvasado las tablas de enfermer�a
				if (ITiposer == "U")
				{
					object tempRefParam = stFechaFinFormateada;
					stEinterco = "select * from eintealt Where eintealt.ganoregi=" + iGanoregi.ToString() + " and eintealt.gnumregi=" + IGnumregi.ToString() + " and " + " eintealt.iestsoli='R' and eintealt.fpasfact is null and " + " eintealt.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam) + " and " + " eintealt.itiposer='" + ITiposer + "'";
					stFechaFinFormateada = Convert.ToString(tempRefParam);
				}
				else
				{
					object tempRefParam2 = stFechaFinFormateada;
					stEinterco = "select * from eintealt,amovifin Where eintealt.ganoregi=" + iGanoregi.ToString() + " and eintealt.gnumregi=" + IGnumregi.ToString() + " and " + " eintealt.iestsoli='R' and eintealt.fpasfact is null and " + " eintealt.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam2) + " and " + " eintealt.itiposer='" + ITiposer + "'" + " and eintealt.ganoregi=amovifin.ganoregi and eintealt.gnumregi=amovifin.gnumregi" + " and (amovifin.fmovimie<=eintealt.frealiza and (amovifin.ffinmovi>=eintealt.frealiza or amovifin.ffinmovi is null))";
					stFechaFinFormateada = Convert.ToString(tempRefParam2);
				}
			}
			else
			{
				if (ITiposer == "U")
				{
					object tempRefParam3 = stFechaFinFormateada;
					stEinterco = "select * from EINTERCO Where einterco.ganoregi=" + iGanoregi.ToString() + " and einterco.gnumregi=" + IGnumregi.ToString() + " and " + " einterco.iestsoli='R' and einterco.fpasfact is null and " + " einterco.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam3) + " and " + " einterco.itiposer='" + ITiposer + "'";
					stFechaFinFormateada = Convert.ToString(tempRefParam3);
				}
				else
				{
					// si es admision leer de de eprestac los apuntes del episodio con n� de episodio relacionado
					// de urgencias <> null (fpeticio < fechallegadaadmision)
					if (VstItipingr == "U")
					{
						stEinterco = "select ganourge,gnumurge,gsocieda, nafiliac, ginspecc from UEPISURG" + 
						             " where UEPISURG.ganrelad =" + iGanoregi.ToString() + " and UEPISURG.gnurelad =" + IGnumregi.ToString();
						SqlDataAdapter tempAdapter = new SqlDataAdapter(stEinterco, rcalta);
						rrEinterco = new DataSet();
						tempAdapter.Fill(rrEinterco);
						if (rrEinterco.Tables[0].Rows.Count == 0)
						{
							//como es posible que se pierda el episodio!!!!!
							//MsgBox "No tiene episodios de urgencias relacionado", vbCritical + vbOKOnly
							if (bLLamadaIFMS)
							{
								Serrores.GrabaLog("Error ALTAFACTU.ProModiEPRESTAC. Episodio n�: " + iGanoregi.ToString() + "/" + IGnumregi.ToString() + " - " + "0" + ": No tiene episodios de urgencias relacionado", Path.GetDirectoryName(Application.ExecutablePath) + "\\ErroresIFMSFA_FACTINDIV.log");
							}
							else
							{
								RadMessageBox.Show("No tiene episodios de urgencias relacionado", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
							}
							bError = true;
							return;
						}
						else
						{
							iAnoU = Convert.ToInt32(rrEinterco.Tables[0].Rows[0]["ganourge"]);
							lNumU = Convert.ToInt32(rrEinterco.Tables[0].Rows[0]["gnumurge"]);
							iSocU = Convert.ToInt32(rrEinterco.Tables[0].Rows[0]["gsocieda"]);
							if (Convert.IsDBNull(rrEinterco.Tables[0].Rows[0]["nafiliac"]))
							{
								sAfiU = "";
							}
							else
							{
								sAfiU = Convert.ToString(rrEinterco.Tables[0].Rows[0]["nafiliac"]);
							}
							if (Convert.IsDBNull(rrEinterco.Tables[0].Rows[0]["ginspecc"]))
							{
								sInsU = "";
							}
							else
							{
								sInsU = Convert.ToString(rrEinterco.Tables[0].Rows[0]["ginspecc"]);
							}
						}
						rrEinterco.Close();
						object tempRefParam4 = stFechaFinFormateada;
						object tempRefParam5 = FechaLLegadaAdmision;
						stEinterco = "select EINTERCO.* from EINTERCO" + 
						             " where EINTERCO.ganoregi =" + iGanoregi.ToString() + " and EINTERCO.gnumregi =" + IGnumregi.ToString() + " and " + 
						             "  EINTERCO.itiposer='" + ITiposer + "' and frealiza is not null and fpasfact is null " + 
						             " and einterco.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam4) + " " + 
						             " and fpeticio <" + Serrores.FormatFechaHMS(tempRefParam5);
						FechaLLegadaAdmision = Convert.ToString(tempRefParam5);
						stFechaFinFormateada = Convert.ToString(tempRefParam4);
						//                    " and fpeticio <" & FormatFechaHMS(FechaLLegadaAdmision & ":59")
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stEinterco, rcalta);
						rrEinterco = new DataSet();
						tempAdapter_2.Fill(rrEinterco);
						if (rrEinterco.Tables[0].Rows.Count != 0)
						{
							rrEinterco.MoveFirst();
							foreach (DataRow iteration_row in rrEinterco.Tables[0].Rows)
							{
								// si cumple las condiciones en base al servicio responsable y tipo de facturaci�n
								// de la sociedad, se graba en DMOVFACT
								iSocie = iSocU;
								object tempRefParam6 = iteration_row["gsersoli"];
								if (ComprobarFactur(iSocie, bMensual, Convert.ToDateTime(iteration_row["frealiza"]).ToString("dd/MM/yyyy"), null, null, ref tempRefParam6, sInsU, false))
								{

									RrDmovfact.AddNew();
									// grabarlos en DMOVFACT con el episodio de urgencias relacionado
									RrDmovfact.Tables[0].Rows[0]["ganoregi"] = iAnoU;
									RrDmovfact.Tables[0].Rows[0]["gnumregi"] = lNumU;
									RrDmovfact.Tables[0].Rows[0]["itiposer"] = "U";
									RrDmovfact.Tables[0].Rows[0]["itiporig"] = RrDmovfact.Tables[0].Rows[0]["itiposer"];
									RrDmovfact.Tables[0].Rows[0]["ganoorig"] = RrDmovfact.Tables[0].Rows[0]["ganoregi"];
									RrDmovfact.Tables[0].Rows[0]["gnumorig"] = RrDmovfact.Tables[0].Rows[0]["gnumregi"];
									//            RrDmovfact("gconcfac") = rrEinterco("gprestac")
									RrDmovfact.Tables[0].Rows[0]["gcodeven"] = "IN";
									RrDmovfact.Tables[0].Rows[0]["gidenpac"] = stGidenpac;
									RrDmovfact.Tables[0].Rows[0]["ITPACFAC"] = "I";
									RrDmovfact.Tables[0].Rows[0]["gsersoli"] = iteration_row["gsersoli"];
									RrDmovfact.Tables[0].Rows[0]["gserreal"] = iteration_row["gservici"];
									RrDmovfact.Tables[0].Rows[0]["gpersona"] = (Convert.IsDBNull(iteration_row["gperinte"])) ? DBNull.Value : iteration_row["gperinte"];
									RrDmovfact.Tables[0].Rows[0]["gsocieda"] = iSocU;
									if (sAfiU != "")
									{
										RrDmovfact.Tables[0].Rows[0]["nafiliac"] = sAfiU;
									}
									if (sInsU != "")
									{
										RrDmovfact.Tables[0].Rows[0]["ginspecc"] = sInsU;
									}
									RrDmovfact.Tables[0].Rows[0]["ffinicio"] = Convert.ToDateTime(iteration_row["frealiza"]);
									RrDmovfact.Tables[0].Rows[0]["ffechfin"] = Convert.ToDateTime(iteration_row["frealiza"]);
									RrDmovfact.Tables[0].Rows[0]["fsistema"] = DateTime.Parse(fFechafactu);
									RrDmovfact.Tables[0].Rows[0]["ncantida"] = 1;
									//**el servicio responsable el que hizo la peticion
									RrDmovfact.Tables[0].Rows[0]["gserresp"] = iteration_row["gservici"];
									RrDmovfact.Tables[0].Rows[0]["fperfact"] = dFperfact;
									
									SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                                    tempAdapter_2.Update(RrDmovfact, RrDmovfact.Tables[0].TableName);
									if (!bTemp)
									{
										rrEinterco.Edit();
										iteration_row["fpasfact"] = dFperfact;
										
										SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_2);
                                        tempAdapter_2.Update(rrEinterco, rrEinterco.Tables[0].TableName);
									}
									// accedo a EINTEALT con el episodio de urgencias  y la fecha de solicitud (fpeticio)
									// de la interconsulta
									object tempRefParam7 = iteration_row["fpeticio"];
									stEinterco2 = "select EINTEALT.* from EINTEALT where gnumregi=" + lNumU.ToString() + 
									              " and ganoregi = " + iAnoU.ToString() + " and EINTEALT.itiposer='U'" + 
									              " and fpeticio=" + Serrores.FormatFechaHMS(tempRefParam7) + "";
									//             " and gprestac ='" & rrEinterco("gprestac") & "'"
									SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stEinterco2, rcalta);
									rrEinterco2 = new DataSet();
									tempAdapter_5.Fill(rrEinterco2);
									if (rrEinterco2.Tables[0].Rows.Count == 1)
									{ // actualizamos EINTEALT CON LOS VALORES DE EINTERCO
										rrEinterco2.Edit();
										vMoticon = iteration_row["omoticon"];
										if (Convert.IsDBNull(vMoticon))
										{
											rrEinterco2.Tables[0].Rows[0]["omoticon"] = DBNull.Value;
										}
										else
										{
											rrEinterco2.Tables[0].Rows[0]["omoticon"] = vMoticon;
										}
										rrEinterco2.Tables[0].Rows[0]["iestsoli"] = (Convert.IsDBNull(iteration_row["iestsoli"])) ? DBNull.Value : iteration_row["iestsoli"];
										rrEinterco2.Tables[0].Rows[0]["fprevista"] = (Convert.IsDBNull(iteration_row["fprevista"])) ? DBNull.Value : iteration_row["fprevista"];
										rrEinterco2.Tables[0].Rows[0]["frealiza"] = (Convert.IsDBNull(iteration_row["frealiza"])) ? DBNull.Value : iteration_row["frealiza"];
										rrEinterco2.Tables[0].Rows[0]["fanulaci"] = (Convert.IsDBNull(iteration_row["fanulaci"])) ? DBNull.Value : iteration_row["fanulaci"];
										rrEinterco2.Tables[0].Rows[0]["iexidocu"] = (Convert.IsDBNull(iteration_row["iexidocu"])) ? DBNull.Value : iteration_row["iexidocu"];
										rrEinterco2.Tables[0].Rows[0]["gusuario"] = (Convert.IsDBNull(iteration_row["gusuario"])) ? DBNull.Value : iteration_row["gusuario"];
										rrEinterco2.Tables[0].Rows[0]["gperinte"] = (Convert.IsDBNull(iteration_row["gperinte"])) ? DBNull.Value : iteration_row["gperinte"];
										rrEinterco2.Tables[0].Rows[0]["fpasfact"] = (Convert.IsDBNull(iteration_row["fpasfact"])) ? DBNull.Value : iteration_row["fpasfact"];
										
										SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_5);
                                        tempAdapter_5.Update(rrEinterco2, rrEinterco2.Tables[0].TableName);
									}
								
									rrEinterco2.Close();
								}
							}
						}
					
						rrEinterco.Close();
					}
					if (!bMensual)
					{ //s�lo borro si se va de alta
						// BORRAMOS DE EINTERCO
						object tempRefParam8 = FechaLLegadaAdmision;
						SqlCommand tempCommand = new SqlCommand("delete from EINTERCO " + 
						                         " where ganoregi =" + iGanoregi.ToString() + " and gnumregi =" + IGnumregi.ToString() + " and " + 
						                         " EINTERCO.itiposer='" + ITiposer + "' and frealiza is not null " + 
						                         " and fpeticio <" + Serrores.FormatFechaHMS(tempRefParam8), rcalta);
						tempCommand.ExecuteNonQuery();
						FechaLLegadaAdmision = Convert.ToString(tempRefParam8);
					}

					object tempRefParam9 = FechaLLegadaAdmision;
					object tempRefParam10 = stFechaFinFormateada;
					stEinterco = "select * from EINTERCO,amovifin Where einterco.ganoregi=" + iGanoregi.ToString() + " and einterco.gnumregi=" + IGnumregi.ToString() + " and " + " einterco.iestsoli='R' and einterco.fpasfact is null and " + " fpeticio >= " + Serrores.FormatFechaHMS(tempRefParam9) + " and " + " einterco.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam10) + " and " + " einterco.itiposer='" + ITiposer + "'" + " and einterco.ganoregi=amovifin.ganoregi and einterco.gnumregi=amovifin.gnumregi" + " and (amovifin.fmovimie<=einterco.frealiza and (amovifin.ffinmovi>=einterco.frealiza or amovifin.ffinmovi is null))";
					stFechaFinFormateada = Convert.ToString(tempRefParam10);
					FechaLLegadaAdmision = Convert.ToString(tempRefParam9);

				}
			}
			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stEinterco, rcalta);
			rrEinterco = new DataSet();
			tempAdapter_7.Fill(rrEinterco);
			if (rrEinterco.Tables[0].Rows.Count != 0)
			{
				rrEinterco.MoveFirst();
				foreach (DataRow iteration_row_2 in rrEinterco.Tables[0].Rows)
				{
					// si cumple las condiciones en base al servicio responsable y tipo de facturaci�n
					// de la sociedad, se graba en DMOVFACT
					if (ITiposer == "U")
					{
						iSocie = Convert.ToInt32(Conversion.Val(StSociedad));
						sInspecFact = StInspUrgencias;
					}
					else
					{
						iSocie = Convert.ToInt32(iteration_row_2["gsocieda"]);
						if (!Convert.IsDBNull(iteration_row_2["ginspecc"]))
						{
							sInspecFact = Convert.ToString(iteration_row_2["ginspecc"]);
						}
						else
						{
							sInspecFact = "";
						}

					}

					object tempRefParam11 = iteration_row_2["gsersoli"];
					if (ComprobarFactur(iSocie, bMensual, Convert.ToDateTime(iteration_row_2["frealiza"]).ToString("dd/MM/yyyy"), null, null, ref tempRefParam11, sInspecFact, false))
					{

						//grabar en dmovfact
						RrDmovfact.AddNew();
						RrDmovfact.Tables[0].Rows[0]["gidenpac"] = stGidenpac;
						RrDmovfact.Tables[0].Rows[0]["ITPACFAC"] = "I";
						RrDmovfact.Tables[0].Rows[0]["itiposer"] = ITiposer;
						RrDmovfact.Tables[0].Rows[0]["ganoregi"] = iGanoregi;
						RrDmovfact.Tables[0].Rows[0]["gnumregi"] = IGnumregi;
						RrDmovfact.Tables[0].Rows[0]["itiporig"] = RrDmovfact.Tables[0].Rows[0]["itiposer"];
						RrDmovfact.Tables[0].Rows[0]["ganoorig"] = RrDmovfact.Tables[0].Rows[0]["ganoregi"];
						RrDmovfact.Tables[0].Rows[0]["gnumorig"] = RrDmovfact.Tables[0].Rows[0]["gnumregi"];
						RrDmovfact.Tables[0].Rows[0]["gcodeven"] = "IN";
						RrDmovfact.Tables[0].Rows[0]["gpersona"] = (Convert.IsDBNull(iteration_row_2["gperinte"])) ? DBNull.Value : iteration_row_2["gperinte"];
						RrDmovfact.Tables[0].Rows[0]["gsersoli"] = iteration_row_2["gsersoli"];
						if (ITiposer == "U")
						{
							RrDmovfact.Tables[0].Rows[0]["gsocieda"] = StSociedad;
							if (StAfilUrgencias != "")
							{
								RrDmovfact.Tables[0].Rows[0]["nafiliac"] = StAfilUrgencias;
							}
							if (StInspUrgencias != "")
							{
								RrDmovfact.Tables[0].Rows[0]["ginspecc"] = StInspUrgencias;
							}
						}
						else
						{
							RrDmovfact.Tables[0].Rows[0]["gsocieda"] = iteration_row_2["gsocieda"];
							if (!Convert.IsDBNull(iteration_row_2["nafiliac"]))
							{
								RrDmovfact.Tables[0].Rows[0]["nafiliac"] = iteration_row_2["nafiliac"];
							}
							if (!Convert.IsDBNull(iteration_row_2["ginspecc"]))
							{
								RrDmovfact.Tables[0].Rows[0]["ginspecc"] = iteration_row_2["ginspecc"];
							}
						}
						RrDmovfact.Tables[0].Rows[0]["gserreal"] = iteration_row_2["gservici"];
						RrDmovfact.Tables[0].Rows[0]["ffinicio"] = Convert.ToDateTime(iteration_row_2["frealiza"]);
						RrDmovfact.Tables[0].Rows[0]["ffechfin"] = Convert.ToDateTime(iteration_row_2["frealiza"]);
						RrDmovfact.Tables[0].Rows[0]["fsistema"] = DateTime.Parse(fFechafactu);
						RrDmovfact.Tables[0].Rows[0]["ncantida"] = 1;
						if (ITiposer == "U")
						{
							RrDmovfact.Tables[0].Rows[0]["gserresp"] = StSer_paciente;
						}
						else
						{
							RrDmovfact.Tables[0].Rows[0]["gserresp"] = fnBuscaSerResp(Convert.ToString(iteration_row_2["fpeticio"]), iGanoregi, IGnumregi);
						}
						RrDmovfact.Tables[0].Rows[0]["fperfact"] = dFperfact;
						
						SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_7);
                        tempAdapter_7.Update(RrDmovfact, RrDmovfact.Tables[0].TableName);
						//grabar en einterco
						if (!bTemp)
						{
							rrEinterco.Edit();
							iteration_row_2["fpasfact"] = dFperfact;
							
							SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter_7);
                            tempAdapter_7.Update(rrEinterco, rrEinterco.Tables[0].TableName);
						}
					}
				}
			}
			rrEinterco.Close();

		}
		internal static void proBuscarSociedadQui(object iAnoregi, object lNumregi, SqlConnection rcalta, ref object isociqui, ref object sinsqui)
		{

			// leo la sociedad que paga la intervenci�n para comprobar si
			// se factura o no
			string tstQuiro = "Select gsocieda, ginspecc from qintquir where " + 
			                  " ganoregi = " + Convert.ToString(iAnoregi) + 
			                  " and gnumregi = " + Convert.ToString(lNumregi);

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuiro, rcalta);
			DataSet tRrLeer = new DataSet();
			tempAdapter.Fill(tRrLeer);
			isociqui = tRrLeer.Tables[0].Rows[0]["gsocieda"];
			if (!Convert.IsDBNull(tRrLeer.Tables[0].Rows[0]["ginspecc"]))
			{
				sinsqui = tRrLeer.Tables[0].Rows[0]["ginspecc"];
			}
			tRrLeer.Close();

		}
		internal static bool ComprobarFactur(object iSociedad, bool bMensual, object dFecha_optional, object iAnoregi, object iNumregi, ref object iServicio_optional, object sInspecFact, bool bEsFarmaco)
		{
			string dFecha = (dFecha_optional == Type.Missing) ? String.Empty : dFecha_optional as string;
			object iServicio = (iServicio_optional == Type.Missing) ? null : iServicio_optional as object;
			try
			{

				bool bGraboDMOVFACT = false;
				//compruebo la sociedad con el c�digo y la fceha de borrado
				if (dFecha_optional == Type.Missing)
				{
					ComprobarSocMensual(Convert.ToInt32(iSociedad), String.Empty, sInspecFact);
				}
				else
				{
					ComprobarSocMensual(Convert.ToInt32(iSociedad), dFecha, sInspecFact);
				}
				if (ITiposer == "U")
				{
					// Modificaci�n 17/2/99: si se trata de una urgencia siempre hay que facturar
					// ya no existe la facturaci�n mensual
					bGraboDMOVFACT = true;
				}
				else
				{
					if (FacturaFarmacosLaSociedad(Convert.ToInt32(iSociedad), Convert.ToString(sInspecFact)) || !bEsFarmaco)
					{
						// si se est� facturando un ingreso en admisi�n
						if (bMensual)
						{
							// si es la facturaci�n mensual
							if (sTipoFacturacion == "M")
							{
								// la sociedad que financia se factura mensualmente o de forma mixta
								bGraboDMOVFACT = true;
							}
							else
							{
								if (sTipoFacturacion == "X")
								{
									if (vstFaltplan != "")
									{
										System.DateTime TempDate2 = DateTime.FromOADate(0);
										System.DateTime TempDate = DateTime.FromOADate(0);
										if (DateTime.Parse((DateTime.TryParse(fPfin, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : fPfin).Month != DateTime.Parse((DateTime.TryParse(vstFaltplan, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy") : vstFaltplan).Month)
										{
											// si es un paciente de facturaci�n mixta que est� de alta
											// s�lo grabo si se est� ejecutando la mensual del mes que no es el de alta
											bGraboDMOVFACT = true;
										}
									}
									else
									{
										bGraboDMOVFACT = true;
									}
								}
								else
								{
									if (iServicio_optional != Type.Missing)
									{
										// llega el servicio directamente
										if (Convert.ToDouble(iServicio) == viUle)
										{
											// si el servicio responsable es la ULE
											bGraboDMOVFACT = true;
										}
									}
									else
									{
										if ((fnBuscaSerResp(DateTimeHelper.ToString(DateTime.Parse(dFecha)), Convert.ToInt32(Conversion.Val(Convert.ToString(iAnoregi))), Convert.ToInt32(Conversion.Val(Convert.ToString(iNumregi))))) == viUle)
										{
											// si el servicio responsable es la ULE
											bGraboDMOVFACT = true;
										}
									}
								}
							} // no es la facturaci�n tipo al alta
						}
						else
						{
							// es la facturaci�n al alta del paciente
							if (sTipoFacturacion == "A" || sTipoFacturacion == "X")
							{
								// la sociedad que financia no se factura mensualmente
								//*a�adido 15/03/1999*******
								if (iServicio_optional == Type.Missing)
								{
									iServicio = fnBuscaSerResp(DateTimeHelper.ToString(DateTime.Parse(dFecha)), Convert.ToInt32(Conversion.Val(Convert.ToString(iAnoregi))), Convert.ToInt32(Conversion.Val(Convert.ToString(iNumregi))));
								}
								//********
								System.DateTime TempDate4 = DateTime.FromOADate(0);
								System.DateTime TempDate3 = DateTime.FromOADate(0);
								if ((Convert.ToDouble(iServicio) == viUle || sTipoFacturacion == "X") && DateTime.Parse((DateTime.TryParse(dFecha, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:mm:ss") : dFecha).Month != DateTime.Parse((DateTime.TryParse(fPfin, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:mm:ss") : fPfin).Month)
								{
									// si el apunte es de la ULE o mixto pero es de otro mes
									// no se lo cobro porque ir� en la mensual
								}
								else
								{
									bGraboDMOVFACT = true;
								}
							}
						}
					}
				} // fin de se est� facturando un ingreso

				return bGraboDMOVFACT;
			}
			finally
			{
				iServicio_optional = iServicio;
			}
			return false;
		}

		internal static bool ComprobarFactur(object iSociedad, bool bMensual, object dFecha_optional, object iAnoregi, object iNumregi, ref object iServicio_optional, object sInspecFact)
		{
			return ComprobarFactur(iSociedad, bMensual, dFecha_optional, iAnoregi, iNumregi, ref iServicio_optional, sInspecFact, false);
		}

		internal static bool ComprobarFactur(object iSociedad, bool bMensual, object dFecha_optional, object iAnoregi, object iNumregi, ref object iServicio_optional)
		{
			return ComprobarFactur(iSociedad, bMensual, dFecha_optional, iAnoregi, iNumregi, ref iServicio_optional, null, false);
		}

		internal static bool ComprobarFactur(object iSociedad, bool bMensual, object dFecha_optional, object iAnoregi, object iNumregi)
		{
			object tempRefParam = Type.Missing;
			return ComprobarFactur(iSociedad, bMensual, dFecha_optional, iAnoregi, iNumregi, ref tempRefParam, null, false);
		}

		internal static bool ComprobarFactur(object iSociedad, bool bMensual, object dFecha_optional, object iAnoregi)
		{
			object tempRefParam2 = Type.Missing;
			return ComprobarFactur(iSociedad, bMensual, dFecha_optional, iAnoregi, null, ref tempRefParam2, null, false);
		}

		internal static bool ComprobarFactur(object iSociedad, bool bMensual, object dFecha_optional)
		{
			object tempRefParam3 = Type.Missing;
			return ComprobarFactur(iSociedad, bMensual, dFecha_optional, null, null, ref tempRefParam3, null, false);
		}

		internal static bool ComprobarFactur(object iSociedad, bool bMensual)
		{
			object tempRefParam4 = Type.Missing;
			return ComprobarFactur(iSociedad, bMensual, Type.Missing, null, null, ref tempRefParam4, null, false);
		}

		internal static bool ComprobarFactur(object iSociedad)
		{
			object tempRefParam5 = Type.Missing;
			return ComprobarFactur(iSociedad, false, Type.Missing, null, null, ref tempRefParam5, null, false);
		}


		internal static void proModiEDIETVIA()
		{
			string stEdietvia = String.Empty;
			string stAmovimie = String.Empty;
			DataSet rrAmovimie = null;
			string fmovini = String.Empty;
			string fmovfin = String.Empty;


			// ya viene en formato dd/mm/yyyy hh:nn:ss
			string stFechaFinFormateada = fPfin;

			if (bEsAlta)
			{
				// ya se han trasvasado las tablas de enfermer�a
				object tempRefParam = stFechaFinFormateada;
				object tempRefParam2 = stFechaFinFormateada;
				stEdietvia = "select * from edietalt, dtipodie where edietalt.ganoregi=" + iGanoregi.ToString() + " and edietalt.gnumregi=" + IGnumregi.ToString() + " and edietalt.ipaciaco='A' and edietalt.gtipodie = dtipodie.gtipodie and " + "(edietalt.fapuntes<=" + Serrores.FormatFechaHMS(tempRefParam) + " and " + "(edietalt.ffinmovi<=" + Serrores.FormatFechaHMS(tempRefParam2) + " or " + "edietalt.ffinmovi is null)) " + " and edietalt.itiposer='" + ITiposer + "'";
				stFechaFinFormateada = Convert.ToString(tempRefParam2);
				stFechaFinFormateada = Convert.ToString(tempRefParam);

			}
			else
			{

				object tempRefParam3 = stFechaFinFormateada;
				object tempRefParam4 = stFechaFinFormateada;
				stEdietvia = "select * from EDIETVIA, dtipodie where edietvia.ganoregi=" + iGanoregi.ToString() + " and edietvia.gnumregi=" + IGnumregi.ToString() + " and edietvia.ipaciaco='A' and edietvia.gtipodie = dtipodie.gtipodie and " + "(edietvia.fapuntes<=" + Serrores.FormatFechaHMS(tempRefParam3) + " and " + "(edietvia.ffinmovi<=" + Serrores.FormatFechaHMS(tempRefParam4) + " or " + "edietvia.ffinmovi is null)) " + " and edietvia.itiposer='" + ITiposer + "'";
				stFechaFinFormateada = Convert.ToString(tempRefParam4);
				stFechaFinFormateada = Convert.ToString(tempRefParam3);
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stEdietvia, rcalta);
			DataSet rrEdietvia = new DataSet();
			tempAdapter.Fill(rrEdietvia);
			if (rrEdietvia.Tables[0].Rows.Count != 0)
			{
				rrEdietvia.MoveFirst();
				foreach (DataRow iteration_row in rrEdietvia.Tables[0].Rows)
				{
					if (Convert.ToString(iteration_row["inocomer"]) == "S")
					{
						// si no es dieta absoluta

						if (Convert.ToDateTime(iteration_row["fapuntes"]) >= DateTime.Parse(fPini) && Convert.ToDateTime(iteration_row["fapuntes"]) <= DateTime.Parse(fPfin))
						{
							//si fapuntes esta en el intervalo
							//si ffinmovi es distinto de nulo
							if (!Convert.IsDBNull(iteration_row["ffinmovi"]))
							{
								//si la fecha de paso a facturacion es nula FPASFACT
								if (Convert.IsDBNull(iteration_row["fpasfact"]))
								{
									//si la fecha de fin del movimiento es mayor que la fin recibida
									if (Convert.ToDateTime(iteration_row["ffinmovi"]) > DateTime.Parse(fPini))
									{
										fmovini = Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:mm:ss");
										fmovfin = fPfin;
										//si la fecha de fin de movimiento no es mayor que la de fin recibida
									}
									else
									{
										fmovini = Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:mm:ss");
										fmovfin = Convert.ToDateTime(iteration_row["ffinmovi"]).ToString("dd/MM/yyyy HH:mm:ss");
									}
									//si la fecha de paso a facturacion no es nula
								}
								else
								{
									//si la fecha FFINMOVI esta en el intervalo
									if (Convert.ToDateTime(iteration_row["ffinmovi"]) >= DateTime.Parse(fPini) && Convert.ToDateTime(iteration_row["ffinmovi"]) <= DateTime.Parse(fPfin))
									{
										fmovini = Convert.ToDateTime(iteration_row["ffinmovi"]).ToString("dd/MM/yyyy HH:mm:ss");
										fmovfin = fPfin;
										//si la fecha FFINMOVI no esta en el intervalo
									}
									else
									{
										fmovini = fPini;
										fmovfin = fPfin;
									}
								}
								//si ffinmovi es nulo
							}
							else
							{
								//si la fecha de paso a facturacion es nula FPASFACT
								if (Convert.IsDBNull(iteration_row["fpasfact"]))
								{
									fmovini = Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:mm:ss");
									fmovfin = fPfin;
									//si la fecha de paso a facturacion no es nula FPASFACT
								}
								else
								{
									fmovini = Convert.ToDateTime(iteration_row["fpasfact"]).ToString("dd/MM/yyyy HH:mm:ss");
									fmovfin = fPfin;
								}
							}
							//si no esta fapuntes en el intevalo
						}
						else
						{
							//si ffinmovi es nulo
							if (Convert.IsDBNull(iteration_row["ffinmovi"]))
							{
								fmovini = fPini;
								fmovfin = fPfin;
								//si ffinmovi no es nulo
							}
							else
							{
								fmovini = fPini;
								fmovfin = Convert.ToDateTime(iteration_row["ffinmovi"]).ToString("dd/MM/yyyy HH:mm:ss");
							}
						}


						// leo los servicios por los que ha pasado en el periodo de fechas
						object tempRefParam5 = fmovini;
						object tempRefParam6 = fmovfin;
						object tempRefParam7 = fmovini;
						object tempRefParam8 = fmovfin;
						object tempRefParam9 = fmovini;
						object tempRefParam10 = fmovfin;
						stAmovimie = " select * from amovimie where ganoregi=" + iGanoregi.ToString() + " and gnumregi=" + IGnumregi.ToString() + " and itiposer = 'H' and (" + "(fmovimie>=" + Serrores.FormatFechaHMS(tempRefParam5) + " and " + "fmovimie<=" + Serrores.FormatFechaHMS(tempRefParam6) + ") or (" + "ffinmovi >=" + Serrores.FormatFechaHMS(tempRefParam7) + " and " + "ffinmovi <=" + Serrores.FormatFechaHMS(tempRefParam8) + ") or (" + "fmovimie<=" + Serrores.FormatFechaHMS(tempRefParam9) + " and " + "(ffinmovi >=" + Serrores.FormatFechaHMS(tempRefParam10) + " or " + "ffinmovi is null))) ";
						fmovfin = Convert.ToString(tempRefParam10);
						fmovini = Convert.ToString(tempRefParam9);
						fmovfin = Convert.ToString(tempRefParam8);
						fmovini = Convert.ToString(tempRefParam7);
						fmovfin = Convert.ToString(tempRefParam6);
						fmovini = Convert.ToString(tempRefParam5);
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stAmovimie, rcalta);
						rrAmovimie = new DataSet();
						tempAdapter_2.Fill(rrAmovimie);
						if (rrAmovimie.Tables[0].Rows.Count > 0)
						{
							rrAmovimie.MoveFirst();
							foreach (DataRow iteration_row_2 in rrAmovimie.Tables[0].Rows)
							{
								//grabar registro en dmovfact

								RrDmovfact.AddNew();
								RrDmovfact.Tables[0].Rows[0]["gidenpac"] = stGidenpac;
								RrDmovfact.Tables[0].Rows[0]["ITPACFAC"] = "I";
								RrDmovfact.Tables[0].Rows[0]["ganoregi"] = iGanoregi;
								RrDmovfact.Tables[0].Rows[0]["gnumregi"] = IGnumregi;
								RrDmovfact.Tables[0].Rows[0]["itiposer"] = ITiposer;
								RrDmovfact.Tables[0].Rows[0]["itiporig"] = RrDmovfact.Tables[0].Rows[0]["itiposer"];
								RrDmovfact.Tables[0].Rows[0]["ganoorig"] = RrDmovfact.Tables[0].Rows[0]["ganoregi"];
								RrDmovfact.Tables[0].Rows[0]["gnumorig"] = RrDmovfact.Tables[0].Rows[0]["gnumregi"];
								RrDmovfact.Tables[0].Rows[0]["gcodeven"] = "DI";
								// siempre se cobra como privado
								RrDmovfact.Tables[0].Rows[0]["gsocieda"] = iPrivado;
								RrDmovfact.Tables[0].Rows[0]["fsistema"] = DateTime.Parse(fFechafactu);
								if (DateTime.Parse(Convert.ToDateTime(iteration_row_2["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss")) > DateTime.Parse(fmovini))
								{
									RrDmovfact.Tables[0].Rows[0]["ffinicio"] = DateTime.Parse(Convert.ToDateTime(iteration_row_2["fmovimie"]).ToString("dd/MM/yyyy HH:mm:ss"));
								}
								else
								{
									RrDmovfact.Tables[0].Rows[0]["ffinicio"] = DateTime.Parse(fmovini);
								}
								if (!Convert.IsDBNull(iteration_row_2["ffinmovi"]))
								{
									if (DateTime.Parse(Convert.ToDateTime(iteration_row_2["ffinmovi"]).ToString("dd/MM/yyyy HH:mm:ss")) < DateTime.Parse(fmovfin))
									{
										RrDmovfact.Tables[0].Rows[0]["ffechfin"] = DateTime.Parse(Convert.ToDateTime(iteration_row_2["ffinmovi"]).ToString("dd/MM/yyyy HH:mm:ss"));
									}
									else
									{
										RrDmovfact.Tables[0].Rows[0]["ffechfin"] = DateTime.Parse(fmovfin);
									}
								}
								else
								{
									RrDmovfact.Tables[0].Rows[0]["ffechfin"] = DateTime.Parse(fmovfin);
								}
								RrDmovfact.Tables[0].Rows[0]["ncantida"] = (int) DateAndTime.DateDiff("d", Convert.ToDateTime(RrDmovfact.Tables[0].Rows[0]["ffinicio"]), Convert.ToDateTime(RrDmovfact.Tables[0].Rows[0]["ffechfin"]), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
								RrDmovfact.Tables[0].Rows[0]["gtipodie"] = iteration_row["gtipodie"];
								RrDmovfact.Tables[0].Rows[0]["gsersoli"] = iteration_row_2["gservior"];
								RrDmovfact.Tables[0].Rows[0]["gserreal"] = iCocina;
								RrDmovfact.Tables[0].Rows[0]["fperfact"] = dFperfact;
								
								SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                                tempAdapter_2.Update(RrDmovfact, RrDmovfact.Tables[0].TableName);
								//rellenar la fecha de paso a facturacion
								if (!bTemp)
								{
									rrEdietvia.Edit();
									iteration_row["fpasfact"] = dFperfact;
									
									SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_2);
                                    tempAdapter_2.Update(rrEdietvia, rrEdietvia.Tables[0].TableName);
								}
							}
						}
						rrAmovimie.Close();
					} // fin de que no es dieta absoluta
				}
			}
			rrEdietvia.Close();


		}
		internal static void proBuscarSociedadHos(object iAnoregi, object lNumregi, SqlConnection rcalta, string sFrealiza, ref object iSociedad, ref object sInspeccion, ref object sAfiliac)
		{


			// si no es prestaci�n de quir�fanos
			// busco sociedad en amovifin
			object tempRefParam = sFrealiza;
			object tempRefParam2 = sFrealiza;
			string sMovifi = "select gsocieda, ginspecc, nafiliac " + 
			                 " from amovifin " + 
			                 " where ganoregi = " + Convert.ToString(iAnoregi) + 
			                 " and gnumregi = " + Convert.ToString(lNumregi) + 
			                 " and itiposer = 'H'" + 
			                 " and (amovifin.fmovimie<= " + Serrores.FormatFechaHMS(tempRefParam) + 
			                 " and (amovifin.ffinmovi>=" + Serrores.FormatFechaHMS(tempRefParam2) + " " + 
			                 " or amovifin.ffinmovi is null))";
			sFrealiza = Convert.ToString(tempRefParam2);
			sFrealiza = Convert.ToString(tempRefParam);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sMovifi, rcalta);
			DataSet tRrMovifi = new DataSet();
			tempAdapter.Fill(tRrMovifi);

			iSociedad = tRrMovifi.Tables[0].Rows[0]["gsocieda"];
			if (!Convert.IsDBNull(tRrMovifi.Tables[0].Rows[0]["ginspecc"]))
			{
				sInspeccion = tRrMovifi.Tables[0].Rows[0]["ginspecc"];
			}
			if (!Convert.IsDBNull(tRrMovifi.Tables[0].Rows[0]["nafiliac"]))
			{
				sAfiliac = tRrMovifi.Tables[0].Rows[0]["nafiliac"];
			}
			tRrMovifi.Close();

		}

		internal static void proBuscarSociedadUrg(object iAnoregi, object lNumregi, SqlConnection rcalta, string sFrealiza, ref object iSociedad, ref object sInspeccion, ref object sAfiliac)
		{
			// si no es prestaci�n de quir�fanos
			// busco sociedad en amovifin
			string sMovifi = "select ganourge,gnumurge,gsocieda, nafiliac, ginspecc from UEPISURG" + 
			                 " where UEPISURG.ganrelad =" + Convert.ToString(iAnoregi) + " and UEPISURG.gnurelad =" + Convert.ToString(lNumregi);
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sMovifi, rcalta);
			DataSet tRrMovifi = new DataSet();
			tempAdapter.Fill(tRrMovifi);

			iSociedad = tRrMovifi.Tables[0].Rows[0]["gsocieda"];
			if (!Convert.IsDBNull(tRrMovifi.Tables[0].Rows[0]["ginspecc"]))
			{
				sInspeccion = tRrMovifi.Tables[0].Rows[0]["ginspecc"];
			}
			if (!Convert.IsDBNull(tRrMovifi.Tables[0].Rows[0]["nafiliac"]))
			{
				sAfiliac = tRrMovifi.Tables[0].Rows[0]["nafiliac"];
			}
			tRrMovifi.Close();

		}
		internal static void proBuscarSociedadCon(object iAnoregi, object lNumregi, SqlConnection rcalta, string sFrealiza, ref object iSociedad, ref object sInspeccion, ref object sAfiliac)
		{
			// si no es prestaci�n de quir�fanos
			// busco sociedad en amovifin
			string sMovifi = "select ganoregi,gnumregi,gsocieda, nafiliac, ginspecc from cconsult" + 
			                 " where ganoregi =" + Convert.ToString(iAnoregi) + " and gnumregi =" + Convert.ToString(lNumregi); //_
			//& " and ffeccita=" & FormatFechaHMS(sFrealiza)

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sMovifi, rcalta);
			DataSet tRrMovifi = new DataSet();
			tempAdapter.Fill(tRrMovifi);

			iSociedad = tRrMovifi.Tables[0].Rows[0]["gsocieda"];
			if (!Convert.IsDBNull(tRrMovifi.Tables[0].Rows[0]["ginspecc"]))
			{
				sInspeccion = tRrMovifi.Tables[0].Rows[0]["ginspecc"];
			}
			if (!Convert.IsDBNull(tRrMovifi.Tables[0].Rows[0]["nafiliac"]))
			{
				sAfiliac = tRrMovifi.Tables[0].Rows[0]["nafiliac"];
			}
			tRrMovifi.Close();

		}


		internal static void proActualizarProceso(string sItiposer, string iAnoregi, string lNumregi, string sItiprela, string iAnorela, string lNumrela, object rcalt)
		{

			string sProceso = "select * from depiproc where " + 
			                  " ganoregi = " + iAnorela + 
			                  " and gnumregi = " + lNumrela + 
			                  " and itiposer = '" + sItiprela + "'";			
			
			//DataSet tRrProceso = (DataSet) rcalta.OpenResultset(sProceso, UpgradeStubs.RDO_ResultsetTypeConstants.getrdOpenKeyset(), 4);
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sProceso, rcalta);
            DataSet tRrProceso = new DataSet();
            tempAdapter.Fill(tRrProceso);
            if (tRrProceso.Tables[0].Rows.Count != 0)
			{
				// si est� incluido en un proceso
				// actualizo el episodio principal al que pertenece
				tRrProceso.Edit();
				tRrProceso.Tables[0].Rows[0]["itipprin"] = sItiposer;
				tRrProceso.Tables[0].Rows[0]["ganoprin"] = iAnoregi;
				tRrProceso.Tables[0].Rows[0]["gnumprin"] = lNumregi;
				
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
				tempAdapter.Update(tRrProceso, tRrProceso.Tables[0].TableName);
			}
			tRrProceso.Close();
		}

		internal static string procambiadia_mes(string ffecha)
		{
			string stdia = ffecha.Substring(0, Math.Min(2, ffecha.Length));
			string stmes = ffecha.Substring(3, Math.Min(2, ffecha.Length - 3));
			return stmes + "/" + stdia + "/" + ffecha.Substring(6);
		}

		internal static void proModiEPRESTAC(Factura_alta Clase, string TablaFacturacion)
		{
			string stEprestac = String.Empty;
			string sql = String.Empty;
			DataSet rrEprestac2 = null;
			string stEprestac2 = String.Empty;
			//object instancia = null;
			int iSocie = 0;
			int iAnoU = 0;
			int lNumU = 0; //episodio de urgencias
			int iSocU = 0;
			string sAfiU = String.Empty;
			string sInsU = String.Empty;
			object vOmotisol = null; //campo text de eprestac
			string sProto = String.Empty;
			DataSet tRrProto = null;
			string sInshos = String.Empty;
			string sAfihos = String.Empty;
			// ya viene en formato dd/mm/yyyy hh:nn:ss
			string stFechaFinFormateada = fPfin;

			if (bEsAlta)
			{
				// ya se han trasvasado las tablas de enfermer�a
				if (ITiposer == "U")
				{
					object tempRefParam = stFechaFinFormateada;
					stEprestac = "select * from epresalt,DCODPRES where epresalt.ganoregi=" + iGanoregi.ToString() + " and epresalt.gnumregi=" + IGnumregi.ToString() + " and " + " epresalt.iestsoli='R' and " + " epresalt.fpasfact is null and " + " epresalt.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam) + " and " + " epresalt.gprestac=dcodpres.gprestac and " + " dcodpres.finivali <= epresalt.frealiza " + " and (dcodpres.ffinvali >= epresalt.frealiza  or dcodpres.ffinvali is null ) " + " and epresalt.itiposer='" + ITiposer + "'";
					stFechaFinFormateada = Convert.ToString(tempRefParam);
				}
				else
				{
					object tempRefParam2 = stFechaFinFormateada;
					stEprestac = "select * from epresalt,DCODPRES where epresalt.ganoregi=" + iGanoregi.ToString() + " and epresalt.gnumregi=" + IGnumregi.ToString() + " and " + " epresalt.iestsoli='R' and " + " epresalt.fpasfact is null and " + " epresalt.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam2) + " and " + " epresalt.gprestac=dcodpres.gprestac and " + " dcodpres.finivali <= epresalt.frealiza " + " and (dcodpres.ffinvali >= epresalt.frealiza  or dcodpres.ffinvali is null ) " + " and epresalt.itiposer='" + ITiposer + "'";
					stFechaFinFormateada = Convert.ToString(tempRefParam2);
				}
			}
			else
			{
				if (ITiposer == "U")
				{
					object tempRefParam3 = stFechaFinFormateada;
					stEprestac = "select * from EPRESTAC,DCODPRES where eprestac.ganoregi=" + iGanoregi.ToString() + " and eprestac.gnumregi=" + IGnumregi.ToString() + " and " + " eprestac.iestsoli='R' and " + " eprestac.fpasfact is null and " + " eprestac.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam3) + " and " + " eprestac.gprestac=dcodpres.gprestac and " + " dcodpres.finivali <= eprestac.frealiza " + " and (dcodpres.ffinvali >= eprestac.frealiza  or dcodpres.ffinvali is null ) " + " and eprestac.itiposer='" + ITiposer + "'";
					stFechaFinFormateada = Convert.ToString(tempRefParam3);
				}
				else
				{
					// si es admision leer de eprestac los apuntes del episodio con n� de episodio relacionado
					// de urgencias <> null (fpeticio < fechallegadaadmision)
					if (VstItipingr == "U")
					{
						stEprestac = "select ganourge,gnumurge,gsocieda, nafiliac, ginspecc from UEPISURG" + 
						             " where UEPISURG.ganrelad =" + iGanoregi.ToString() + " and UEPISURG.gnurelad =" + IGnumregi.ToString();

						SqlDataAdapter tempAdapter = new SqlDataAdapter(stEprestac, rcalta);
						rrEprestac = new DataSet();
						tempAdapter.Fill(rrEprestac);
						if (rrEprestac.Tables[0].Rows.Count == 0)
						{
							//como es posible que se pierda el episodio!!!!!
							//MsgBox "No tiene episodios de urgencias relacionado", vbCritical + vbOKOnly
							if (bLLamadaIFMS)
							{
								Serrores.GrabaLog("Error ALTAFACTU.ProModiEPRESTAC. Episodio n�: " + iGanoregi.ToString() + "/" + IGnumregi.ToString() + " - " + "0" + ": No tiene episodios de urgencias relacionado", Path.GetDirectoryName(Application.ExecutablePath) + "\\ErroresIFMSFA_FACTINDIV.log");
							}
							else
							{
								RadMessageBox.Show("No tiene episodios de urgencias relacionado", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
							}
							bError = true;
							return;
						}
						else
						{
							iAnoU = Convert.ToInt32(rrEprestac.Tables[0].Rows[0]["ganourge"]);
							lNumU = Convert.ToInt32(rrEprestac.Tables[0].Rows[0]["gnumurge"]);
							iSocU = Convert.ToInt32(rrEprestac.Tables[0].Rows[0]["gsocieda"]);
							if (Convert.IsDBNull(rrEprestac.Tables[0].Rows[0]["nafiliac"]))
							{
								sAfiU = "";
							}
							else
							{
								sAfiU = Convert.ToString(rrEprestac.Tables[0].Rows[0]["nafiliac"]);
							}
							if (Convert.IsDBNull(rrEprestac.Tables[0].Rows[0]["ginspecc"]))
							{
								sInsU = "";
							}
							else
							{
								sInsU = Convert.ToString(rrEprestac.Tables[0].Rows[0]["ginspecc"]);
							}
						}
						rrEprestac.Close();

						object tempRefParam4 = stFechaFinFormateada;
						object tempRefParam5 = FechaLLegadaAdmision;
						stEprestac = "select * from EPRESTAC, DCODPRES" + 
						             " where eprestac.ganoregi =" + iGanoregi.ToString() + " and eprestac.gnumregi =" + IGnumregi.ToString() + " and " + " eprestac.fpasfact is null and " + 
						             "  EPRESTAC.itiposer='" + ITiposer + "' and frealiza is not null and " + 
						             " eprestac.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam4) + " and " + 
						             " eprestac.gprestac=dcodpres.gprestac " + 
						             " and dcodpres.finivali <= eprestac.frealiza " + 
						             " and (dcodpres.ffinvali >= eprestac.frealiza  or dcodpres.ffinvali is null ) " + 
						             " and fpeticio <" + Serrores.FormatFechaHMS(tempRefParam5);
						FechaLLegadaAdmision = Convert.ToString(tempRefParam5);
						stFechaFinFormateada = Convert.ToString(tempRefParam4);
						//                    " and fpeticio <" & FormatFechaHMS(FechaLLegadaAdmision & ":59")
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stEprestac, rcalta);
						rrEprestac = new DataSet();
						tempAdapter_2.Fill(rrEprestac);
						if (rrEprestac.Tables[0].Rows.Count != 0)
						{
							rrEprestac.MoveFirst();
							foreach (DataRow iteration_row in rrEprestac.Tables[0].Rows)
							{
								iSocie = iSocU; //Val(StSociedad)
								// Modificaci�n del 2/2/99:
								// si precisa autorizaci�n de la sociedad para la prueba, y no la ha traido
								// se le cobra como privado independientemente de la sociedad
								if (!Convert.IsDBNull(iteration_row["iautosoc"]))
								{
									if (Convert.IsDBNull(iteration_row["iexiauto"]))
									{
										iSocie = iPrivado;
										sInsU = "";
									}
								}

								// si cumple las condiciones en base al servicio responsable y tipo de facturaci�n
								// de la sociedad, se graba en DMOVFACT
								object tempRefParam6 = iteration_row["gsersoli"];
								if (ComprobarFactur(iSocie, bMensual, Convert.ToDateTime(iteration_row["frealiza"]).ToString("dd/MM/yyyy"), null, null, ref tempRefParam6, sInsU, false))
								{
									if (!Convert.IsDBNull(iteration_row["ganorela"]) && !Convert.IsDBNull(iteration_row["gnumrela"]))
									{
										// 21/3/2000: actualizo el n� de episodio principal si est� incluido
										// en un proceso
										if (!bTemp)
										{
											//esto lo comentamos porque lo hacemos una vez que se le da el alta al paciente
											//proActualizarProceso rrEprestac("itiposer"), rrEprestac("ganoregi"), rrEprestac("gnumregi"), rrEprestac("itiprela"), rrEprestac("ganorela"), rrEprestac("gnumrela"), rcalta
										}
										//si tiene episodio relacionado llamar a la dll facturacion quirofano o consultas
										if (Convert.ToString(iteration_row["itiprela"]) == "Q")
										{
                                            // seg�n nos hayan llamdo en modo consulta o en modo real
                                            // var�a el �ltimo par�metro de la llamada
                                            FacturacionQuirofanos.clsFacturacion instancia = new FacturacionQuirofanos.clsFacturacion();
											if (bTemp)
											{
                                                string tempRefParam7 = "";
                                                int tempRefParam = 0;
                                                instancia.GenerarMovimientosFacturacion(Convert.ToInt32(iteration_row["ganorela"]), Convert.ToInt32(iteration_row["gnumrela"]), rcalta, Clase, ref tempRefParam, ref tempRefParam7, dFperfact.ToString("dd/MM/yyyy HH:mm:ss"), bTemp, TablaFacturacion);
											}
											else
											{
                                                string tempRefParam9 = "";
                                                int tempRefParam10 = 0;
                                                instancia.GenerarMovimientosFacturacion(Convert.ToInt32(iteration_row["ganorela"]), Convert.ToInt32(iteration_row["gnumrela"]), rcalta, Clase, ref tempRefParam10, ref tempRefParam9, dFperfact.ToString("dd/MM/yyyy HH:mm:ss"), null, TablaFacturacion);
											}
											instancia = null;

										}
										if (Convert.ToString(iteration_row["itiprela"]) == "C")
										{
                                            // seg�n nos hayan llamdo en modo consulta o en modo real
                                            // var�a el �ltimo par�metro de la llamada
                                            FacturacionConsultas.clsFacturacion instancia = new FacturacionConsultas.clsFacturacion();
											if (bTemp)
											{
												instancia.GenerarMovimientosFacturacion(Convert.ToInt32(iteration_row["ganorela"]), Convert.ToInt32(iteration_row["gnumrela"]), ref rcalta, Clase, dFperfact.ToString("dd/MM/yyyy HH:mm:ss"), bTemp, TablaFacturacion);
											}
											else
											{
												instancia.GenerarMovimientosFacturacion(Convert.ToInt32(iteration_row["ganorela"]), Convert.ToInt32(iteration_row["gnumrela"]), ref rcalta, Clase, dFperfact.ToString("dd/MM/yyyy HH:mm:ss"), null, TablaFacturacion);
											}
											instancia = null;
										}
									}
									else
									{

										// 29/9/99
										// si la prestaci�n corresponde a un protocolo
										// se facturan cada una de las prestaciones a las
										// que corresponde, no la prestaci�n padre
										object tempRefParam7 = iteration_row["fpeticio"];
										sProto = "select * from danalsol  inner join DCODPRES on DANALSOL.gprestac = DCODPRES.gprestac " + 
										         " where itiposer = 'U' " + 
										         " and ganoregi = " + Convert.ToString(iteration_row["ganoregi"]) + 
										         " and gnumregi = " + Convert.ToString(iteration_row["gnumregi"]) + 
										         " and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam7) + "" + 
										         " and DCODPRES.gprestac = '" + Convert.ToString(iteration_row["gprestac"]) + "' " + 
										         " and DCODPRES.iprotcer <> 'C' ";

										SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sProto, rcalta);
										tRrProto = new DataSet();
										tempAdapter_3.Fill(tRrProto);
										if (tRrProto.Tables[0].Rows.Count != 0)
										{
											foreach (DataRow iteration_row_2 in tRrProto.Tables[0].Rows)
											{
												RrDmovfact.AddNew();
												RrDmovfact.Tables[0].Rows[0]["ganoregi"] = iAnoU;
												RrDmovfact.Tables[0].Rows[0]["gnumregi"] = lNumU;
												RrDmovfact.Tables[0].Rows[0]["itiposer"] = "U";
												RrDmovfact.Tables[0].Rows[0]["itiporig"] = RrDmovfact.Tables[0].Rows[0]["itiposer"];
												RrDmovfact.Tables[0].Rows[0]["ganoorig"] = RrDmovfact.Tables[0].Rows[0]["ganoregi"];
												RrDmovfact.Tables[0].Rows[0]["gnumorig"] = RrDmovfact.Tables[0].Rows[0]["gnumregi"];
												if (sAfiU != "")
												{
													RrDmovfact.Tables[0].Rows[0]["nafiliac"] = sAfiU;
												}
												if (iSocie != iPrivado)
												{
													if (sInsU != "")
													{
														RrDmovfact.Tables[0].Rows[0]["ginspecc"] = sInsU;
													}
												}
												//**el servicio responsable el que hizo la peticion
												RrDmovfact.Tables[0].Rows[0]["gserresp"] = StSer_paciente;
												proRellenarComunesPrestac(iSocie);
												RrDmovfact.Tables[0].Rows[0]["ncantida"] = iteration_row_2["ncantida"];
												RrDmovfact.Tables[0].Rows[0]["gconcfac"] = iteration_row_2["gpruelab"];
												
												SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
                                                tempAdapter_3.Update(RrDmovfact, RrDmovfact.Tables[0].TableName);
											}
											tRrProto.Close();

										}
										else
										{
											RrDmovfact.AddNew();
											// grabarlos en DMOVFACT con el episodio de urgencias relacionado
											RrDmovfact.Tables[0].Rows[0]["ganoregi"] = iAnoU;
											RrDmovfact.Tables[0].Rows[0]["gnumregi"] = lNumU;
											RrDmovfact.Tables[0].Rows[0]["itiposer"] = "U";
											RrDmovfact.Tables[0].Rows[0]["itiporig"] = RrDmovfact.Tables[0].Rows[0]["itiposer"];
											RrDmovfact.Tables[0].Rows[0]["ganoorig"] = RrDmovfact.Tables[0].Rows[0]["ganoregi"];
											RrDmovfact.Tables[0].Rows[0]["gnumorig"] = RrDmovfact.Tables[0].Rows[0]["gnumregi"];
											RrDmovfact.Tables[0].Rows[0]["gconcfac"] = iteration_row["gprestac"];
											if (sAfiU != "")
											{
												RrDmovfact.Tables[0].Rows[0]["nafiliac"] = sAfiU;
											}
											// pregunto la sociedad porque cuando no hay autorizaci�n
											// trata al paciente como privado
											if (iSocie != iPrivado)
											{
												if (sInsU != "")
												{
													RrDmovfact.Tables[0].Rows[0]["ginspecc"] = sInsU;
												}
											}
											//**el servicio responsable el que hizo la peticion
											RrDmovfact.Tables[0].Rows[0]["gserresp"] = StSer_paciente;
											proRellenarComunesPrestac(iSocie);
											
											SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);
                                            tempAdapter_3.Update(RrDmovfact, RrDmovfact.Tables[0].TableName);
										} // fin de que no es un protocolo

									}
									// tanto si es prestaci�n relacionada
									// como si no, hay que marcarla para que no facture 2 veces
									if (!bTemp)
									{
										rrEprestac.Edit();
										iteration_row["fpasfact"] = dFperfact;
										
										SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_2);
                                        tempAdapter_2.Update(rrEprestac, rrEprestac.Tables[0].TableName);
									}

									// accedo a EPRESALT con el episodio de urgencias  y la fecha de solicitud (fpeticio)
									// de la prestaci�n
									object tempRefParam8 = iteration_row["fpeticio"];
									stEprestac2 = "select EPRESALT.* from EPRESALT where gnumregi=" + lNumU.ToString() + 
									              " and ganoregi = " + iAnoU.ToString() + " and EPRESALT.itiposer='U'" + 
									              " and fpeticio=" + Serrores.FormatFechaHMS(tempRefParam8) + "" + 
									              " and gprestac ='" + Convert.ToString(iteration_row["gprestac"]) + "'";
									SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(stEprestac2, rcalta);
									rrEprestac2 = new DataSet();
									tempAdapter_7.Fill(rrEprestac2);
									if (rrEprestac2.Tables[0].Rows.Count == 1)
									{ // actualizamos EPRESALT CON LOS VALORES DE EPRESTAC
										vOmotisol = iteration_row["Omotisol"];
										rrEprestac2.Edit();
										rrEprestac2.Tables[0].Rows[0]["nmuestra"] = (Convert.IsDBNull(iteration_row["nmuestra"])) ? DBNull.Value : iteration_row["nmuestra"];
										rrEprestac2.Tables[0].Rows[0]["ipriorid"] = (Convert.IsDBNull(iteration_row["ipriorid"])) ? DBNull.Value : iteration_row["ipriorid"];
										if (Convert.IsDBNull(vOmotisol))
										{
											rrEprestac2.Tables[0].Rows[0]["omotisol"] = DBNull.Value;
										}
										else
										{
											rrEprestac2.Tables[0].Rows[0]["omotisol"] = vOmotisol;
										}
										rrEprestac2.Tables[0].Rows[0]["iestsoli"] = (Convert.IsDBNull(iteration_row["iestsoli"])) ? DBNull.Value : iteration_row["iestsoli"];
										rrEprestac2.Tables[0].Rows[0]["nplacasr"] = (Convert.IsDBNull(iteration_row["nplacasr"])) ? DBNull.Value : iteration_row["nplacasr"];
										rrEprestac2.Tables[0].Rows[0]["fprevista"] = (Convert.IsDBNull(iteration_row["fprevista"])) ? DBNull.Value : iteration_row["fprevista"];
										rrEprestac2.Tables[0].Rows[0]["frealiza"] = (Convert.IsDBNull(iteration_row["frealiza"])) ? DBNull.Value : iteration_row["frealiza"];
										rrEprestac2.Tables[0].Rows[0]["fanulaci"] = (Convert.IsDBNull(iteration_row["fanulaci"])) ? DBNull.Value : iteration_row["fanulaci"];
										rrEprestac2.Tables[0].Rows[0]["gusuario"] = iteration_row["gusuario"];
										rrEprestac2.Tables[0].Rows[0]["fpasfact"] = (Convert.IsDBNull(iteration_row["fpasfact"])) ? DBNull.Value : iteration_row["fpasfact"];
										rrEprestac2.Tables[0].Rows[0]["itiprela"] = (Convert.IsDBNull(iteration_row["itiprela"])) ? DBNull.Value : iteration_row["itiprela"];
										rrEprestac2.Tables[0].Rows[0]["ganorela"] = (Convert.IsDBNull(iteration_row["ganorela"])) ? DBNull.Value : iteration_row["ganorela"];
										rrEprestac2.Tables[0].Rows[0]["gnumrela"] = (Convert.IsDBNull(iteration_row["gnumrela"])) ? DBNull.Value : iteration_row["gnumrela"];
										rrEprestac2.Tables[0].Rows[0]["iautosoc"] = (Convert.IsDBNull(iteration_row["iautosoc"])) ? DBNull.Value : iteration_row["iautosoc"];
										rrEprestac2.Tables[0].Rows[0]["iexiauto"] = (Convert.IsDBNull(iteration_row["iexiauto"])) ? DBNull.Value : iteration_row["iexiauto"];
										
										SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapter_2);
                                        tempAdapter_2.Update(rrEprestac2, rrEprestac2.Tables[0].TableName);
									}
									rrEprestac2.Close();
								}

							}
						}
						rrEprestac.Close();
					}
					if (!bMensual && VstItipingr == "U")
					{ // si estamos facturando el alta y hay epis. relac  borramos apuntes duplicados
						// BORRAMOS DE EPRESTAC
						object tempRefParam9 = FechaLLegadaAdmision;
						object tempRefParam10 = FechaLLegadaAdmision;
						SqlCommand tempCommand = new SqlCommand("delete from EPRESTAC " + 
						                         " where ganoregi =" + iGanoregi.ToString() + " and gnumregi =" + IGnumregi.ToString() + " and " + 
						                         " itiposer='" + ITiposer + "' and frealiza is not null" + 
						                         " and fpeticio <" + Serrores.FormatFechaHMS(tempRefParam9) + 
						                         " and fpasfact <" + Serrores.FormatFechaHMS(tempRefParam10), rcalta);
						tempCommand.ExecuteNonQuery();
						FechaLLegadaAdmision = Convert.ToString(tempRefParam10);
						FechaLLegadaAdmision = Convert.ToString(tempRefParam9);
						//                        " and fpeticio <" & FormatFechaHMS(FechaLLegadaAdmision & ":59")

						//                      " and (itiprela <>'Q' or itiprela is null)"
					}

					// trato todas las prestaciones porque las de urgencias
					// ya tienen fpasfact
					object tempRefParam11 = stFechaFinFormateada;
					stEprestac = "select * from EPRESTAC,DCODPRES where eprestac.ganoregi=" + iGanoregi.ToString() + " and eprestac.gnumregi=" + IGnumregi.ToString() + " and " + " eprestac.iestsoli='R' and " + " eprestac.fpasfact is null and " + " eprestac.frealiza<=" + Serrores.FormatFechaHMS(tempRefParam11) + " and " + " eprestac.gprestac=dcodpres.gprestac and " + " dcodpres.finivali <= eprestac.frealiza " + " and (dcodpres.ffinvali >= eprestac.frealiza  or dcodpres.ffinvali is null ) " + " and eprestac.itiposer='" + ITiposer + "'";
					stFechaFinFormateada = Convert.ToString(tempRefParam11);
				}
			}

			//empieza tratamiento de cada una de las prestaciones del episodio a facturar

			SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(stEprestac, rcalta);
			rrEprestac = new DataSet();
			tempAdapter_9.Fill(rrEprestac);
			if (rrEprestac.Tables[0].Rows.Count != 0)
			{
				rrEprestac.MoveFirst();
				foreach (DataRow iteration_row_3 in rrEprestac.Tables[0].Rows)
				{

					// Modificaci�n del 2/2/99:
					// si precisa autorizaci�n de la sociedad para la prueba, y no la ha traido
					// se le cobra como privado independientemente de la sociedad
					sInshos = "";
					sAfihos = "";

					if (!Convert.IsDBNull(iteration_row_3["iautosoc"]) && Convert.IsDBNull(iteration_row_3["iexiauto"]))
					{
						iSocie = iPrivado;
					}
					else
					{
						if (ITiposer == "U")
						{
							iSocie = Convert.ToInt32(Conversion.Val(StSociedad));
							sInshos = StInspUrgencias;
						}
						else
						{
							if (!Convert.IsDBNull(iteration_row_3["itiprela"]))
							{
								if (Convert.ToString(iteration_row_3["itiprela"]) == "Q")
								{
									// si es prestaci�n de quir�fanos
									// busco la sociedad en qintquir
									iSocie = 0;
									object tempRefParam12 = iSocie;
									object tempRefParam13 = sInshos;
									proBuscarSociedadQui(iteration_row_3["ganorela"], iteration_row_3["gnumrela"], rcalta, ref tempRefParam12, ref tempRefParam13);
									sInshos = Convert.ToString(tempRefParam13);
									iSocie = Convert.ToInt32(tempRefParam12);
								}
								else if (Convert.ToString(iteration_row_3["itiprela"]) == "U")
								{ 
									object tempRefParam14 = iSocie;
									object tempRefParam15 = sInshos;
									object tempRefParam16 = sAfihos;
									proBuscarSociedadUrg(iteration_row_3["ganorela"], iteration_row_3["gnumrela"], rcalta, Convert.ToString(iteration_row_3["frealiza"]), ref tempRefParam14, ref tempRefParam15, ref tempRefParam16);
									sAfihos = Convert.ToString(tempRefParam16);
									sInshos = Convert.ToString(tempRefParam15);
									iSocie = Convert.ToInt32(tempRefParam14);
								}
								else if (Convert.ToString(iteration_row_3["itiprela"]) == "C")
								{ 
									object tempRefParam17 = iSocie;
									object tempRefParam18 = sInshos;
									object tempRefParam19 = sAfihos;
									proBuscarSociedadCon(iteration_row_3["ganorela"], iteration_row_3["gnumrela"], rcalta, Convert.ToString(iteration_row_3["frealiza"]), ref tempRefParam17, ref tempRefParam18, ref tempRefParam19);
									sAfihos = Convert.ToString(tempRefParam19);
									sInshos = Convert.ToString(tempRefParam18);
									iSocie = Convert.ToInt32(tempRefParam17);
								}
								else
								{
									object tempRefParam20 = iSocie;
									object tempRefParam21 = sInshos;
									object tempRefParam22 = sAfihos;
									proBuscarSociedadHos(iteration_row_3["ganoregi"], iteration_row_3["gnumregi"], rcalta, Convert.ToString(iteration_row_3["frealiza"]), ref tempRefParam20, ref tempRefParam21, ref tempRefParam22);
									sAfihos = Convert.ToString(tempRefParam22);
									sInshos = Convert.ToString(tempRefParam21);
									iSocie = Convert.ToInt32(tempRefParam20);
								}
							}
							else
							{
								object tempRefParam23 = iSocie;
								object tempRefParam24 = sInshos;
								object tempRefParam25 = sAfihos;
								proBuscarSociedadHos(iteration_row_3["ganoregi"], iteration_row_3["gnumregi"], rcalta, Convert.ToString(iteration_row_3["frealiza"]), ref tempRefParam23, ref tempRefParam24, ref tempRefParam25);
								sAfihos = Convert.ToString(tempRefParam25);
								sInshos = Convert.ToString(tempRefParam24);
								iSocie = Convert.ToInt32(tempRefParam23);
							}
						}
					}

					// si cumple las condiciones en base al servicio responsable y tipo de facturaci�n
					// de la sociedad, se graba en DMOVFACT
					object tempRefParam26 = iteration_row_3["gsersoli"];
					if (ComprobarFactur(iSocie, bMensual, Convert.ToDateTime(iteration_row_3["frealiza"]).ToString("dd/MM/yyyy"), null, null, ref tempRefParam26, sInshos, false))
					{
						//si tiene episodio relacionado llamar a la dll facturacion quirofano o consultas
						if (!Convert.IsDBNull(iteration_row_3["ganorela"]) && !Convert.IsDBNull(iteration_row_3["gnumrela"]))
						{
							// 21/3/2000: actualizo el n� de episodio relacionado si est� incluido
							// en un proceso y no estamos en modo consulta
							if (!bTemp)
							{
								//lo comentamos porque lo hace cuando se le da de alta al paciente
								//proActualizarProceso rrEprestac("itiposer"), rrEprestac("ganoregi"), rrEprestac("gnumregi"), rrEprestac("itiprela"), rrEprestac("ganorela"), rrEprestac("gnumrela"), rcalta
							}
							if (Convert.ToString(iteration_row_3["itiprela"]) == "Q")
							{
                                FacturacionQuirofanos.clsFacturacion instancia = new FacturacionQuirofanos.clsFacturacion();
								if (bTemp)
								{
                                    string refuno = "";
                                    int numero = 0;
                                    instancia.GenerarMovimientosFacturacion(Convert.ToInt32(iteration_row_3["ganorela"]), Convert.ToInt32(iteration_row_3["gnumrela"]), rcalta, Clase, ref numero, ref refuno, dFperfact.ToString("dd/MM/yyyy HH:mm:ss"), bTemp, TablaFacturacion);
								}
								else
								{
                                    string refuno = "";
                                    int numero = 0;
									instancia.GenerarMovimientosFacturacion(Convert.ToInt32(iteration_row_3["ganorela"]), Convert.ToInt32(iteration_row_3["gnumrela"]), rcalta, Clase, ref numero, ref refuno, dFperfact.ToString("dd/MM/yyyy HH:mm:ss"), null, TablaFacturacion);
								}
								instancia = null;
							}
							if (Convert.ToString(iteration_row_3["itiprela"]) == "C")
							{
                                //If PrestacionFacturada(rrEprestac("itiprela"), rrEprestac("ganorela"), rrEprestac("gnumrela"), ITiposer) = False Then
                                FacturacionConsultas.clsFacturacion instancia = new FacturacionConsultas.clsFacturacion();
								if (bTemp)
								{
									instancia.GenerarMovimientosFacturacion(Convert.ToInt32(iteration_row_3["ganorela"]), Convert.ToInt32(iteration_row_3["gnumrela"]),ref rcalta, Clase, dFperfact.ToString("dd/MM/yyyy HH:mm:ss"), bTemp, TablaFacturacion);
								}
								else
								{
									instancia.GenerarMovimientosFacturacion(Convert.ToInt32(iteration_row_3["ganorela"]), Convert.ToInt32(iteration_row_3["gnumrela"]),ref rcalta, Clase, dFperfact.ToString("dd/MM/yyyy HH:mm:ss"), null, TablaFacturacion);
								}
								instancia = null;
								//End If
							}
						}
						else
						{
							//no tiene episodio relacionado en EPRESTAC/EPRESALT
							// 29/9/99
							// si la prestaci�n corresponde a un protocolo
							// se facturan cada una de las prestaciones a las
							// que corresponde, no la prestaci�n padre
							object tempRefParam27 = iteration_row_3["fpeticio"];
							sProto = "select * from danalsol inner join DCODPRES on DANALSOL.gprestac = DCODPRES.gprestac " + 
							         "where itiposer = '" + Convert.ToString(iteration_row_3["itiposer"]) + "' and ganoregi = " + Convert.ToString(iteration_row_3["ganoregi"]) + 
							         " and gnumregi = " + Convert.ToString(iteration_row_3["gnumregi"]) + 
							         " and fpeticio = " + Serrores.FormatFechaHMS(tempRefParam27) + "" + 
							         " and DCODPRES.gprestac = '" + Convert.ToString(iteration_row_3["gprestac"]) + "' and DCODPRES.iprotcer <> 'C' ";

							SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(sProto, rcalta);
							tRrProto = new DataSet();
							tempAdapter_10.Fill(tRrProto);
							if (tRrProto.Tables[0].Rows.Count != 0)
							{
								foreach (DataRow iteration_row_4 in tRrProto.Tables[0].Rows)
								{
									RrDmovfact.AddNew();
									RrDmovfact.Tables[0].Rows[0]["ganoregi"] = iGanoregi;
									RrDmovfact.Tables[0].Rows[0]["itiposer"] = ITiposer;
									RrDmovfact.Tables[0].Rows[0]["gnumregi"] = IGnumregi;
									RrDmovfact.Tables[0].Rows[0]["itiporig"] = RrDmovfact.Tables[0].Rows[0]["itiposer"];
									RrDmovfact.Tables[0].Rows[0]["ganoorig"] = RrDmovfact.Tables[0].Rows[0]["ganoregi"];
									RrDmovfact.Tables[0].Rows[0]["gnumorig"] = RrDmovfact.Tables[0].Rows[0]["gnumregi"];
									if (ITiposer != "U")
									{
										RrDmovfact.Tables[0].Rows[0]["ireingre"] = sReingreso;
										if (sInshos != "" && iSocie != iPrivado)
										{
											RrDmovfact.Tables[0].Rows[0]["ginspecc"] = sInshos;
										}
										if (sAfihos != "")
										{
											RrDmovfact.Tables[0].Rows[0]["nafiliac"] = sAfihos;
										}
									}
									else
									{
										if (StAfilUrgencias != "")
										{
											RrDmovfact.Tables[0].Rows[0]["nafiliac"] = StAfilUrgencias;
										}
										if (StInspUrgencias != "")
										{
											RrDmovfact.Tables[0].Rows[0]["ginspecc"] = StInspUrgencias;
										}
									}
									proRellenarComunesPrestac(iSocie);
									RrDmovfact.Tables[0].Rows[0]["ncantida"] = iteration_row_4["ncantida"];
									RrDmovfact.Tables[0].Rows[0]["gconcfac"] = iteration_row_4["gpruelab"];
									
									SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapter_10);
                                    tempAdapter_10.Update(RrDmovfact, RrDmovfact.Tables[0].TableName);
								}
								tRrProto.Close();
							}
							else
							{
								//es una prueba que no tiene episodio relacionado y no es un protocolo
								RrDmovfact.AddNew();
								RrDmovfact.Tables[0].Rows[0]["itiposer"] = ITiposer;
								RrDmovfact.Tables[0].Rows[0]["ganoregi"] = iGanoregi;
								RrDmovfact.Tables[0].Rows[0]["gnumregi"] = IGnumregi;
								RrDmovfact.Tables[0].Rows[0]["itiporig"] = RrDmovfact.Tables[0].Rows[0]["itiposer"];
								RrDmovfact.Tables[0].Rows[0]["ganoorig"] = RrDmovfact.Tables[0].Rows[0]["ganoregi"];
								RrDmovfact.Tables[0].Rows[0]["gnumorig"] = RrDmovfact.Tables[0].Rows[0]["gnumregi"];
								if (ITiposer != "U")
								{
									RrDmovfact.Tables[0].Rows[0]["ireingre"] = sReingreso;
									if (sInshos != "" && iSocie != iPrivado)
									{
										RrDmovfact.Tables[0].Rows[0]["ginspecc"] = sInshos;
									}
									if (sAfihos != "")
									{
										RrDmovfact.Tables[0].Rows[0]["nafiliac"] = sAfihos;
									}

								}
								else
								{
									if (StAfilUrgencias != "")
									{
										RrDmovfact.Tables[0].Rows[0]["nafiliac"] = StAfilUrgencias;
									}
									if (StInspUrgencias != "")
									{
										RrDmovfact.Tables[0].Rows[0]["ginspecc"] = StInspUrgencias;
									}
								}
								proRellenarComunesPrestac(iSocie);
								RrDmovfact.Tables[0].Rows[0]["gconcfac"] = iteration_row_3["gprestac"];
								
								SqlCommandBuilder tempCommandBuilder_6 = new SqlCommandBuilder(tempAdapter_10);
                                tempAdapter_10.Update(RrDmovfact, RrDmovfact.Tables[0].TableName);
							} // fin de que no es protocolo

						} //fin de que no tiene prestaci�n relacionada
						if (!bTemp)
						{

							//0scar
							object tempRefParam28 = dFperfact;
							object tempRefParam29 = iteration_row_3["fpeticio"];
							sql = "UPDATE " + ((bEsAlta) ? "EPRESALT" : "EPRESTAC") + 
							      " SET FPASFACT=" + Serrores.FormatFechaHMS(tempRefParam28) + 
							      " WHERE " + 
							      " ITIPOSER = '" + Convert.ToString(iteration_row_3["itiposer"]) + "' AND " + 
							      " GANOREGI = " + Convert.ToString(iteration_row_3["ganoregi"]) + " AND " + 
							      " GNUMREGI = " + Convert.ToString(iteration_row_3["gnumregi"]) + " AND " + 
							      " FPETICIO=" + Serrores.FormatFechaHMS(tempRefParam29) + " AND " + 
							      " GPRESTAC='" + Convert.ToString(iteration_row_3["gprestac"]) + "'";
							dFperfact = Convert.ToDateTime(tempRefParam28);
							SqlCommand tempCommand_2 = new SqlCommand(sql, rcalta);
							tempCommand_2.ExecuteNonQuery();
							//Debug.Print rcalta.RowsAffected
							//----------

						}
					} // fin de si hay que facturar

				}
			}
			rrEprestac.Close();
		}



		internal static void proModiEFARMACO()
		{
			string stEfarmaco = String.Empty;

			string stActuEfarmaco = String.Empty;
			DataSet rrActuEfarmaco = null;



			float siFrascos = 0;
			int iSocie = 0;
			string sInspecPaciente = String.Empty;

			// ya viene en formato dd/mm/yyyy hh:nn:ss
			string stFechaFinFormateada = fPfin;

			//01/12/2004 Se hace una modificacion para HGC de la siguiente forma
			//Si la constante global "CONTALMA" en el valfanu3 tiene valor "N" y el episodio
			//es de hospitalizacion no se graban los farmacos en DMOVFACT
			bool NoActualizarDMOVFACT = false;
			string sql = "select valfanu3 from sconsglo where gconsglo='CONTALMA'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rcalta);
			DataSet RrBuscar = new DataSet();
			tempAdapter.Fill(RrBuscar);

			if (RrBuscar.Tables[0].Rows.Count != 0)
			{
				if (Convert.IsDBNull(RrBuscar.Tables[0].Rows[0]["valfanu3"]))
				{
				}
				else
				{
					if (Convert.ToString(RrBuscar.Tables[0].Rows[0]["valfanu3"]).Trim().ToUpper() == "N")
					{
						NoActualizarDMOVFACT = true;
					}
				}
			}
			RrBuscar.Close();
			//Fin modificacion

			bool bFTOMAFA = Serrores.ObternerValor_CTEGLOBAL(rcalta, "FTOMAFA", "VALFANU1") == "S";

			// se factura todo lo pendiente
			// hasta la fecha de fin de facturaci�n  20/7/1999
			if (bEsAlta)
			{
				// ya se han trasvasado las tablas de enfermer�a
				if (ITiposer == "U")
				{
					object tempRefParam = stFechaFinFormateada;
					stEfarmaco = "select distinct etomaalt.gfarmaco, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival from etomaalt, DCODFARMVT where etomaalt.ganoregi=" + iGanoregi.ToString() + " and etomaalt.gnumregi=" + IGnumregi.ToString() + " and  " + " etomaalt.fpasfact is null and (etomaalt.iproppac is null or etomaalt.iproppac <> 'S') and" + " cdosimpu is not null and gfarmadm is null and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam) + " and " + " etomaalt.itiposer='" + ITiposer + "' and etomaalt.gfarmaco = dcodfarmvt.gfarmaco group by etomaalt.gfarmaco, nequival, cporenva ";
					stFechaFinFormateada = Convert.ToString(tempRefParam);

					stEfarmaco = stEfarmaco + "Union all ";
					//para el farmaco equivalente
					object tempRefParam2 = stFechaFinFormateada;
					stEfarmaco = stEfarmaco + "select distinct etomaalt.gfarmadm, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival from etomaalt, DCODFARMVT where etomaalt.ganoregi=" + iGanoregi.ToString() + " and etomaalt.gnumregi=" + IGnumregi.ToString() + " and  " + " etomaalt.fpasfact is null and (etomaalt.iproppac is null or etomaalt.iproppac <> 'S') and " + " cdosimpu is not null and gfarmadm is not null and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam2) + " and " + " etomaalt.itiposer='" + ITiposer + "' and etomaalt.gfarmadm = dcodfarmvt.gfarmaco group by etomaalt.gfarmadm, nequival, cporenva ";
					stFechaFinFormateada = Convert.ToString(tempRefParam2);

					stEfarmaco = stEfarmaco + " ORDER BY 1, nequival, cporenva ";

				}
				else
				{
					//modificacion para HGC
					stEfarmaco = "select distinct etomaalt.gfarmaco, ";
					if (bFTOMAFA)
					{
						stEfarmaco = stEfarmaco + " amovimie.fmovimie as fechamin, amovimie.ffinmovi as fechamax ";
					}
					else
					{
						stEfarmaco = stEfarmaco + " min(ftomasfa) as fechamin, max(ftomasfa) as fechamax ";
					}
					object tempRefParam3 = stFechaFinFormateada;
					stEfarmaco = stEfarmaco + ", sum(cdosimpu) as cantidad, cporenva, nequival, gsocieda, nafiliac, ginspecc,gservior " + " from etomaalt, DCODFARMVT, AMOVIFIN,AMOVIMIE where etomaalt.ganoregi=" + iGanoregi.ToString() + " and etomaalt.gnumregi=" + IGnumregi.ToString() + " and  " + " etomaalt.fpasfact is null and (etomaalt.iproppac is null or etomaalt.iproppac <> 'S') and " + " etomaalt.itiposer='" + ITiposer + "' and " + " cdosimpu is not null and gfarmadm is null and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam3) + " and " + " amovifin.itiposer='" + ITiposer + "' and amovifin.ganoregi=" + iGanoregi.ToString() + " and amovifin.gnumregi=" + IGnumregi.ToString() + " and  " + " amovimie.itiposer=amovifin.itiposer and amovimie.ganoregi=amovifin.ganoregi and amovimie.gnumregi=amovifin.gnumregi and  " + " (amovifin.fmovimie<=etomaalt.ftomasfa and (amovifin.ffinmovi>=etomaalt.ftomasfa or amovifin.ffinmovi is null)) and " + " (amovimie.fmovimie<=etomaalt.ftomasfa and (amovimie.ffinmovi>=etomaalt.ftomasfa or amovimie.ffinmovi is null)) and " + " etomaalt.gfarmaco = dcodfarmvt.gfarmaco group by etomaalt.gfarmaco, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior ";
					stFechaFinFormateada = Convert.ToString(tempRefParam3);
					if (bFTOMAFA)
					{
						stEfarmaco = stEfarmaco + " , AMOVIMIE.fmovimie , AMOVIMIE.ffinmovi";
					}
					stEfarmaco = stEfarmaco + "Union all ";
					//para el farmaco equivalente
					stEfarmaco = stEfarmaco + "select distinct etomaalt.gfarmadm, ";
					if (bFTOMAFA)
					{
						stEfarmaco = stEfarmaco + " amovimie.fmovimie as fechamin, amovimie.ffinmovi as fechamax ";
					}
					else
					{
						stEfarmaco = stEfarmaco + " min(ftomasfa) as fechamin, max(ftomasfa) as fechamax ";
					}
					object tempRefParam4 = stFechaFinFormateada;
					stEfarmaco = stEfarmaco + ", sum(cdosimpu) as cantidad, cporenva, nequival, gsocieda, nafiliac, ginspecc, gservior " + " from etomaalt, DCODFARMVT, AMOVIFIN, AMOVIMIE where etomaalt.ganoregi=" + iGanoregi.ToString() + " and etomaalt.gnumregi=" + IGnumregi.ToString() + " and  " + " etomaalt.fpasfact is null and (etomaalt.iproppac is null or etomaalt.iproppac <> 'S') and " + " etomaalt.itiposer='" + ITiposer + "' and " + " cdosimpu is not null and gfarmadm is not null and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam4) + " and " + " amovifin.itiposer='" + ITiposer + "' and amovifin.ganoregi=" + iGanoregi.ToString() + " and amovifin.gnumregi=" + IGnumregi.ToString() + " and  " + " amovimie.itiposer=amovifin.itiposer and amovimie.ganoregi=amovifin.ganoregi and amovimie.gnumregi=amovifin.gnumregi and  " + " (amovifin.fmovimie<=etomaalt.ftomasfa and (amovifin.ffinmovi>=etomaalt.ftomasfa or amovifin.ffinmovi is null)) and " + " (amovimie.fmovimie<=etomaalt.ftomasfa and (amovimie.ffinmovi>=etomaalt.ftomasfa or amovimie.ffinmovi is null)) and " + " etomaalt.gfarmaco = dcodfarmvt.gfarmaco group by etomaalt.gfarmadm, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior ";
					stFechaFinFormateada = Convert.ToString(tempRefParam4);
					if (bFTOMAFA)
					{
						stEfarmaco = stEfarmaco + " , AMOVIMIE.fmovimie , AMOVIMIE.ffinmovi";
					}
					stEfarmaco = stEfarmaco + " ORDER BY 1, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior ";
				}

			}
			else
			{
				if (ITiposer == "U")
				{
					object tempRefParam5 = stFechaFinFormateada;
					stEfarmaco = "select distinct etomasfa.gfarmaco, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival from ETOMASFA, DCODFARMVT where etomasfa.ganoregi=" + iGanoregi.ToString() + " and etomasfa.gnumregi=" + IGnumregi.ToString() + " and  " + " etomasfa.fpasfact is null and (etomasfa.iproppac is null or etomasfa.iproppac <> 'S') and " + " cdosimpu is not null and gfarmadm is null and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam5) + " and " + " etomasfa.itiposer='" + ITiposer + "' and etomasfa.gfarmaco = dcodfarmvt.gfarmaco group by etomasfa.gfarmaco, nequival, cporenva ";
					stFechaFinFormateada = Convert.ToString(tempRefParam5);

					stEfarmaco = stEfarmaco + "Union all ";
					//para el farmaco equivalente
					object tempRefParam6 = stFechaFinFormateada;
					stEfarmaco = stEfarmaco + "select distinct etomasfa.gfarmadm, min(ftomasfa) as fechamin, max(ftomasfa) as fechamax,  sum(cdosimpu) as cantidad, cporenva, nequival from ETOMASFA, DCODFARMVT where etomasfa.ganoregi=" + iGanoregi.ToString() + " and etomasfa.gnumregi=" + IGnumregi.ToString() + " and  " + " etomasfa.fpasfact is null and (etomasfa.iproppac is null or etomasfa.iproppac <> 'S') and " + " cdosimpu is not null and gfarmadm is not null and " + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam6) + " and " + " etomasfa.itiposer='" + ITiposer + "' and etomasfa.gfarmadm = dcodfarmvt.gfarmaco group by etomasfa.gfarmadm, nequival, cporenva ";
					stFechaFinFormateada = Convert.ToString(tempRefParam6);

					stEfarmaco = stEfarmaco + " ORDER BY 1 , nequival, cporenva ";

				}
				else
				{
					//modificacion para HGC
					stEfarmaco = "select distinct etomasfa.gfarmaco, ";
					if (bFTOMAFA)
					{
						stEfarmaco = stEfarmaco + " amovimie.fmovimie as fechamin, amovimie.ffinmovi as fechamax ";
					}
					else
					{
						stEfarmaco = stEfarmaco + " min(ftomasfa) as fechamin, max(ftomasfa) as fechamax ";
					}
					object tempRefParam7 = stFechaFinFormateada;
					stEfarmaco = stEfarmaco + ", sum(cdosimpu) as cantidad, cporenva, nequival, gsocieda, nafiliac, ginspecc, gservior " + " from ETOMASFA, DCODFARMVT, AMOVIFIN, AMOVIMIE where etomasfa.ganoregi=" + iGanoregi.ToString() + " and etomasfa.gnumregi=" + IGnumregi.ToString() + " and  " + " etomasfa.fpasfact is null and (etomasfa.iproppac is null or etomasfa.iproppac <> 'S') and " + " etomasfa.itiposer='" + ITiposer + "' and " + " cdosimpu is not null and gfarmadm is null and ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam7) + " and " + " amovifin.itiposer='" + ITiposer + "' and amovifin.ganoregi=" + iGanoregi.ToString() + " and amovifin.gnumregi=" + IGnumregi.ToString() + " and  " + " amovimie.itiposer=amovifin.itiposer and amovimie.ganoregi=amovifin.ganoregi and amovimie.gnumregi=amovifin.gnumregi and  " + " (amovifin.fmovimie<=etomasfa.ftomasfa and (amovifin.ffinmovi>=etomasfa.ftomasfa or amovifin.ffinmovi is null)) and " + " (amovimie.fmovimie<=etomasfa.ftomasfa and (amovimie.ffinmovi>=etomasfa.ftomasfa or amovimie.ffinmovi is null)) and " + " etomasfa.gfarmaco = dcodfarmvt.gfarmaco group by etomasfa.gfarmaco, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior ";
					stFechaFinFormateada = Convert.ToString(tempRefParam7);
					if (bFTOMAFA)
					{
						stEfarmaco = stEfarmaco + " , AMOVIMIE.fmovimie , AMOVIMIE.ffinmovi";
					}
					stEfarmaco = stEfarmaco + " Union all ";

					stEfarmaco = stEfarmaco + "select distinct etomasfa.gfarmadm, ";
					if (bFTOMAFA)
					{
						stEfarmaco = stEfarmaco + " amovimie.fmovimie as fechamin, amovimie.ffinmovi as fechamax ";
					}
					else
					{
						stEfarmaco = stEfarmaco + " min(ftomasfa) as fechamin, max(ftomasfa) as fechamax ";
					}
					object tempRefParam8 = stFechaFinFormateada;
					stEfarmaco = stEfarmaco + ",  sum(cdosimpu) as cantidad, cporenva, nequival, gsocieda, nafiliac, ginspecc, gservior " + " from ETOMASFA, DCODFARMVT, AMOVIFIN, AMOVIMIE where etomasfa.ganoregi=" + iGanoregi.ToString() + " and etomasfa.gnumregi=" + IGnumregi.ToString() + " and  " + " etomasfa.fpasfact is null and (etomasfa.iproppac is null or etomasfa.iproppac <> 'S') and " + " etomasfa.itiposer='" + ITiposer + "' and " + " cdosimpu is not null and gfarmadm is not null and ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam8) + " and " + " amovifin.itiposer='" + ITiposer + "' and amovifin.ganoregi=" + iGanoregi.ToString() + " and amovifin.gnumregi=" + IGnumregi.ToString() + " and  " + " amovimie.itiposer=amovifin.itiposer and amovimie.ganoregi=amovifin.ganoregi and amovimie.gnumregi=amovifin.gnumregi and  " + " (amovifin.fmovimie<=etomasfa.ftomasfa and (amovifin.ffinmovi>=etomasfa.ftomasfa or amovifin.ffinmovi is null)) and " + " (amovimie.fmovimie<=etomasfa.ftomasfa and (amovimie.ffinmovi>=etomasfa.ftomasfa or amovimie.ffinmovi is null)) and " + " etomasfa.gfarmadm = dcodfarmvt.gfarmaco group by etomasfa.gfarmadm, nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior ";
					stFechaFinFormateada = Convert.ToString(tempRefParam8);
					if (bFTOMAFA)
					{
						stEfarmaco = stEfarmaco + " , AMOVIMIE.fmovimie , AMOVIMIE.ffinmovi";
					}
					stEfarmaco = stEfarmaco + " ORDER BY 1 , nequival, cporenva, gsocieda, nafiliac, ginspecc, gservior ";

				}
			}
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stEfarmaco, rcalta);
			DataSet rrEfarmaco = new DataSet();
			tempAdapter_2.Fill(rrEfarmaco);
			if (rrEfarmaco.Tables[0].Rows.Count != 0)
			{
				rrEfarmaco.MoveFirst();
				foreach (DataRow iteration_row in rrEfarmaco.Tables[0].Rows)
				{
					//grabo una fila en DMOVFACT por cada f�rmaco pendiente de facturar
					//primero paso la cantidad consumida a unidades de medida
					if (!Convert.IsDBNull(iteration_row["nequival"]) && !Convert.IsDBNull(iteration_row["cporenva"]))
					{
						siFrascos = ((float) (Conversion.Val(Convert.ToString(iteration_row["cantidad"])) / Convert.ToDouble(iteration_row["nequival"]))); //* rrEfarmaco("nequival")) / rrEfarmaco("cporenva")
					}
					else
					{
						// si no est�n rellenos los campos en farmacia no le cobro porque no
						// puedo calcular la cantidad consumida
						siFrascos = 0;
					}
					if (siFrascos != 0)
					{

						if (ITiposer == "U")
						{
							iSocie = Convert.ToInt32(Conversion.Val(StSociedad));
							sInspecPaciente = StInspUrgencias;
						}
						else
						{
							iSocie = Convert.ToInt32(iteration_row["gsocieda"]);
							if (!Convert.IsDBNull(iteration_row["ginspecc"]))
							{
								sInspecPaciente = Convert.ToString(iteration_row["ginspecc"]);
							}
							else
							{
								sInspecPaciente = "";
							}

						}
						// si cumple las condiciones en base al servicio responsable y tipo de facturaci�n
						// de la sociedad, se graba en DMOVFACT
						object tempRefParam9 = null;
						if (ComprobarFactur(iSocie, bMensual, Convert.ToDateTime(iteration_row["fechamin"]).ToString("dd/MM/yyyy HH:mm:ss"), iGanoregi, IGnumregi, ref tempRefParam9, sInspecPaciente, true))
						{
							if (ITiposer == "H" && NoActualizarDMOVFACT)
							{
								//si es de hospitalizacion no tiene que grabar en DMOVFACT en el resto de los casos
								//si gue como hasta ahora
							}
							else
							{
								//grabar registro en dmovfact
								RrDmovfact.AddNew();
								RrDmovfact.Tables[0].Rows[0]["gidenpac"] = stGidenpac;
								RrDmovfact.Tables[0].Rows[0]["ITPACFAC"] = "I";
								RrDmovfact.Tables[0].Rows[0]["ganoregi"] = iGanoregi;
								RrDmovfact.Tables[0].Rows[0]["gnumregi"] = IGnumregi;
								RrDmovfact.Tables[0].Rows[0]["itiposer"] = ITiposer;
								RrDmovfact.Tables[0].Rows[0]["itiporig"] = RrDmovfact.Tables[0].Rows[0]["itiposer"];
								RrDmovfact.Tables[0].Rows[0]["ganoorig"] = RrDmovfact.Tables[0].Rows[0]["ganoregi"];
								RrDmovfact.Tables[0].Rows[0]["gnumorig"] = RrDmovfact.Tables[0].Rows[0]["gnumregi"];
								RrDmovfact.Tables[0].Rows[0]["ncantida"] = siFrascos;
								RrDmovfact.Tables[0].Rows[0]["gconcfac"] = iteration_row["gfarmaco"];
								if (ITiposer == "U")
								{
									RrDmovfact.Tables[0].Rows[0]["gsocieda"] = StSociedad;
									if (StAfilUrgencias != "")
									{
										RrDmovfact.Tables[0].Rows[0]["nafiliac"] = StAfilUrgencias;
									}
									if (StInspUrgencias != "")
									{
										RrDmovfact.Tables[0].Rows[0]["ginspecc"] = StInspUrgencias;
									}
								}
								else
								{
									RrDmovfact.Tables[0].Rows[0]["gsocieda"] = iteration_row["gsocieda"];
									if (!Convert.IsDBNull(iteration_row["nafiliac"]))
									{
										RrDmovfact.Tables[0].Rows[0]["nafiliac"] = iteration_row["nafiliac"];
									}
									if (!Convert.IsDBNull(iteration_row["ginspecc"]))
									{
										RrDmovfact.Tables[0].Rows[0]["ginspecc"] = iteration_row["ginspecc"];
									}
								}
								RrDmovfact.Tables[0].Rows[0]["gcodeven"] = "FA";
								RrDmovfact.Tables[0].Rows[0]["ffinicio"] = iteration_row["fechamin"];
								if (ITiposer == "H")
								{
									sql = "Select max(fultfact) as fechafac from aperfact where ganoadme = " + iGanoregi.ToString() + " and gnumadme  = " + IGnumregi.ToString();
									SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, rcalta);
									RrBuscar = new DataSet();
									tempAdapter_3.Fill(RrBuscar);
									if (RrBuscar.Tables[0].Rows.Count != 0)
									{
										if (!Convert.IsDBNull(RrBuscar.Tables[0].Rows[0]["fechafac"]))
										{
											if (Conversion.Val(Convert.ToDateTime(RrBuscar.Tables[0].Rows[0]["fechafac"]).ToString("yyyyMMddHHmmss")) > Conversion.Val(Convert.ToDateTime(iteration_row["fechamin"]).ToString("yyyyMMddHHmmss")))
											{
												RrDmovfact.Tables[0].Rows[0]["ffinicio"] = RrBuscar.Tables[0].Rows[0]["fechafac"];
											}
										}
									}
									RrBuscar.Close();
								}
								if (!Convert.IsDBNull(iteration_row["fechamax"]))
								{
									RrDmovfact.Tables[0].Rows[0]["ffechfin"] = iteration_row["fechamax"];
									if (ITiposer == "H")
									{
										if (Conversion.Val(Convert.ToDateTime(iteration_row["fechamax"]).ToString("yyyyMMddHHmmss")) > Conversion.Val(dFperfact.ToString("yyyyMMddHHmmss")))
										{
											RrDmovfact.Tables[0].Rows[0]["ffechfin"] = dFperfact;
										}
									}
								}
								else
								{
									RrDmovfact.Tables[0].Rows[0]["ffechfin"] = dFperfact;
								}
								RrDmovfact.Tables[0].Rows[0]["fsistema"] = DateTime.Parse(fFechafactu);
								if (ITiposer == "U")
								{
									RrDmovfact.Tables[0].Rows[0]["gserresp"] = StSer_paciente;
									RrDmovfact.Tables[0].Rows[0]["gsersoli"] = StSer_paciente;
								}
								else
								{
									RrDmovfact.Tables[0].Rows[0]["gserresp"] = iteration_row["gservior"];
									RrDmovfact.Tables[0].Rows[0]["gsersoli"] = iteration_row["gservior"];
								}
								RrDmovfact.Tables[0].Rows[0]["gserreal"] = iFarmacia;
								RrDmovfact.Tables[0].Rows[0]["fperfact"] = dFperfact;
								
								SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                                tempAdapter_2.Update(RrDmovfact, RrDmovfact.Tables[0].TableName);
							}
							// pongo marca de pasado a facturaci�n de todos los acumulados si no
							// es modo consulta
							if (!bTemp)
							{
								if (bEsAlta)
								{
									stActuEfarmaco = "select fpasfact from ETOMAALT where ";
								}
								else
								{
									stActuEfarmaco = "select fpasfact from ETOMASFA where ";
								}
								object tempRefParam10 = iteration_row["fechamin"];
								stActuEfarmaco = stActuEfarmaco + " ganoregi = " + iGanoregi.ToString() + " and gnumregi=" + IGnumregi.ToString() + " and  " + " fpasfact is null and " + " cdosimpu is not null and " + " gfarmaco = '" + Convert.ToString(iteration_row["gfarmaco"]) + "' and " + " ftomasfa >=" + Serrores.FormatFechaHMS(tempRefParam10) + " and ";

								if (!Convert.IsDBNull(iteration_row["fechamax"]))
								{
									object tempRefParam11 = iteration_row["fechamax"];
									stActuEfarmaco = stActuEfarmaco + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam11) + " and ";
								}
								else
								{
									object tempRefParam12 = dFperfact;
									stActuEfarmaco = stActuEfarmaco + " ftomasfa <=" + Serrores.FormatFechaHMS(tempRefParam12) + " and ";
									dFperfact = Convert.ToDateTime(tempRefParam12);
								}
								stActuEfarmaco = stActuEfarmaco + " itiposer='" + ITiposer + "' ";

								SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stActuEfarmaco, rcalta);
								rrActuEfarmaco = new DataSet();
								tempAdapter_5.Fill(rrActuEfarmaco);

								//Si no existe registro puede ser que sea el equivalente
								if (rrActuEfarmaco.Tables[0].Rows.Count == 0)
								{
									rrActuEfarmaco.Close();
									string tempRefParam13 = "gfarmaco";
									stActuEfarmaco = Serrores.proReplace(ref stActuEfarmaco, tempRefParam13, "gfarmadm");
									SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stActuEfarmaco, rcalta);
									rrActuEfarmaco = new DataSet();
									tempAdapter_6.Fill(rrActuEfarmaco);
								}
								rrActuEfarmaco.MoveFirst();
								foreach (DataRow iteration_row_2 in rrActuEfarmaco.Tables[0].Rows)
								{
									rrActuEfarmaco.Edit();
									iteration_row_2["fpasfact"] = dFperfact;
									
									SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_2);
                                    tempAdapter_2.Update(rrActuEfarmaco, rrActuEfarmaco.Tables[0].TableName);
								}
								rrActuEfarmaco.Close();
							}


						} //fin de que la sociedad es facturable
					} //fin de hay frascos

				}
			}
			rrEfarmaco.Close();
		}

		internal static void prorecibirerror(int FbError, string StErrodescri)
		{
			if (FbError == 0)
			{
				bError = false;
			}
			else
			{
				bError = true;
				if (bLLamadaIFMS)
				{
					Serrores.GrabaLog("Error ALTAFACTU.Proerror(.Bas). Episodio n�: " + iGanoregi.ToString() + "/" + IGnumregi.ToString() + " - " + FbError.ToString() + ":" + StErrodescri, Path.GetDirectoryName(Application.ExecutablePath) + "\\ErroresIFMSFA_FACTINDIV.log");
				}
				else
				{
					RadMessageBox.Show("Error en Proerror(.Bas): " + FbError.ToString() + ":" + StErrodescri, Application.ProductName);
					RadMessageBox.Show("Episodio n�: " + iGanoregi.ToString() + "/" + IGnumregi.ToString(), Application.ProductName);
				}
			}
		}
	}
}