using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace Alta_factu
{
	public class Factura_alta
	{


		public string stTablaFacturacion = String.Empty;
		public bool fCrearTablaFacturacion = false;


		public void proRecibePara(dynamic Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad, string stFecha, int StServi_pak, object bMens_optional, object bEstadeAlta_optional, object esConsulta_optional, object sAfilUrg_optional, object sInspUrg_optional, object TablaFacturacion_optional, bool PbLLamadaIFMS)
		{
			object bMens = (bMens_optional == Type.Missing) ? null : bMens_optional as object;
			object bEstadeAlta = (bEstadeAlta_optional == Type.Missing) ? null : bEstadeAlta_optional as object;
			object esConsulta = (esConsulta_optional == Type.Missing) ? null : esConsulta_optional as object;
			string sAfilUrg = (sAfilUrg_optional == Type.Missing) ? String.Empty : sAfilUrg_optional as string;
			string sInspUrg = (sInspUrg_optional == Type.Missing) ? String.Empty : sInspUrg_optional as string;
			string TablaFacturacion = (TablaFacturacion_optional == Type.Missing) ? String.Empty : TablaFacturacion_optional as string;
			string stDmovfact = String.Empty;
			string tStSS = String.Empty;
			DataSet tRrSS = null;
			Module1.bLLamadaIFMS = PbLLamadaIFMS;
			Module1.ObtenerLocale();
			//cuidado como esta este formato tiene que ser DD/MM/YYYY HH:NN:SS

			Module1.rcalta = Conexion;
			Serrores.AjustarRelojCliente(Conexion);
			Module1.fPini = fFini;

			// 21/9/2000: se necesitan los segundos para la anulaci�n del alta
			//  ya que esta fecha se graba en fpasfact y fperfact

			System.DateTime TempDate = DateTime.FromOADate(0);
			Module1.fPfin = (DateTime.TryParse(fFfin, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : fFfin;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Module1.rcalta);
			if (bMens_optional != Type.Missing)
			{
				Module1.bMensual = true;
				// si es la fact. mensual busco el servicio de ULE
				tStSS = "select nnumeri1 from sconsglo where gconsglo = 'SERVIULE'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(tStSS, Conexion);
				tRrSS = new DataSet();
				tempAdapter.Fill(tRrSS);
				Module1.viUle = Convert.ToInt32(tRrSS.Tables[0].Rows[0]["nnumeri1"]);
				tRrSS.Close();
			}
			else
			{
				Module1.bMensual = false;
			}
			if (bEstadeAlta_optional != Type.Missing)
			{
				// es la facturaci�n mensual y se trasvasaron las tablas de enfermer�a
				Module1.bEsAlta = true;
			}
			else
			{
				//no se han trasvasado las tablas de enfermer�a del paciente
				Module1.bEsAlta = false;
			}
			if (esConsulta_optional != Type.Missing)
			{
				// es una consulta de lo que ser�a la facturaci�n mensual
				Module1.bTemp = true;
			}
			else
			{
				// se est� haciendo realmente la facturaci�n mensual
				Module1.bTemp = false;
			}
			Module1.StAfilUrgencias = "";
			Module1.StInspUrgencias = "";
			if (stTipoSer == "U")
			{
				// si es una Urgencia recibo la sociedad que financia de par�metro
				// as� como la p�liza e inspecci�n
				Module1.StSociedad = StParaSociedad;
				if (sAfilUrg_optional != Type.Missing)
				{
					Module1.StAfilUrgencias = sAfilUrg;
				}
				if (sInspUrg_optional != Type.Missing)
				{
					Module1.StInspUrgencias = sInspUrg;
				}
			}
			else
			{
				// si no es urgencias
				Module1.FechaLLegadaAdmision = ObtenerFechaLLegada(IGano, IGnum);
			}


			Module1.fFechafactu = stFecha;
			Module1.bError = false;
			Module1.stGidenpac = StCodpac;
			Module1.iGanoregi = IGano;
			Module1.IGnumregi = IGnum;
			Module1.StSer_paciente = StServi_pak;
			Module1.ITiposer = stTipoSer;

			if (TablaFacturacion_optional == Type.Missing)
			{
				//si no se pasa el parametro se utiliza la tabla DMOVFACT
				stTablaFacturacion = "DMOVFACT";
			}
			else
			{
				stTablaFacturacion = TablaFacturacion;
			}

			string sFechaBisiesto = String.Empty;
			if (Module1.bMensual)
			{
				//pongo el �ltimo d�a de mes porque si no me graba la fecha de alta
				System.DateTime TempDate5 = DateTime.FromOADate(0);
				System.DateTime TempDate4 = DateTime.FromOADate(0);
				System.DateTime TempDate3 = DateTime.FromOADate(0);
				System.DateTime TempDate2 = DateTime.FromOADate(0);
				if (DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy") : Module1.fPfin).Month == 11 || DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : Module1.fPfin).Month == 4 || DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy") : Module1.fPfin).Month == 6 || DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy") : Module1.fPfin).Month == 9)
				{
					System.DateTime TempDate7 = DateTime.FromOADate(0);
					System.DateTime TempDate6 = DateTime.FromOADate(0);
					Module1.dFperfact = DateTime.Parse("30/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Year.ToString() + " 23:59:59");
				}
				else
				{
					System.DateTime TempDate8 = DateTime.FromOADate(0);
					if (DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate8)) ? TempDate8.ToString("dd/MM/yyyy") : Module1.fPfin).Month == 2)
					{
						System.DateTime TempDate10 = DateTime.FromOADate(0);
						System.DateTime TempDate9 = DateTime.FromOADate(0);
						sFechaBisiesto = "29/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate9)) ? TempDate9.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate10)) ? TempDate10.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Year.ToString() + " 23:59:59";
						if (Information.IsDate(sFechaBisiesto))
						{
							System.DateTime TempDate12 = DateTime.FromOADate(0);
							System.DateTime TempDate11 = DateTime.FromOADate(0);
							Module1.dFperfact = DateTime.Parse("29/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate11)) ? TempDate11.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate12)) ? TempDate12.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Year.ToString() + " 23:59:59");
						}
						else
						{
							System.DateTime TempDate14 = DateTime.FromOADate(0);
							System.DateTime TempDate13 = DateTime.FromOADate(0);
							Module1.dFperfact = DateTime.Parse("28/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate13)) ? TempDate13.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate14)) ? TempDate14.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Year.ToString() + " 23:59:59");
						}
					}
					else
					{
						System.DateTime TempDate16 = DateTime.FromOADate(0);
						System.DateTime TempDate15 = DateTime.FromOADate(0);
						Module1.dFperfact = DateTime.Parse("31/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate15)) ? TempDate15.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Month.ToString() + "/" + DateTime.Parse((DateTime.TryParse(Module1.fPfin, out TempDate16)) ? TempDate16.ToString("dd/MM/yyyy HH:NN:SS") : Module1.fPfin).Year.ToString() + " 23:59:59");
					}
				}
			}
			else
			{
				Module1.dFperfact = DateTime.Parse(Module1.fPfin);
			}

			try
			{

				// BUSCO EL SERVICIO DE FARMACIA Y COCINA PARA GRABAR LUEGO EN DMOVFACT
				tStSS = "select nnumeri1, nnumeri2 from sconsglo where gconsglo = 'SERVREAL'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tStSS, Conexion);
				tRrSS = new DataSet();
				tempAdapter_2.Fill(tRrSS);
				Module1.iFarmacia = Convert.ToInt32(tRrSS.Tables[0].Rows[0]["nnumeri1"]);
				Module1.iCocina = Convert.ToInt32(tRrSS.Tables[0].Rows[0]["nnumeri2"]);
				tRrSS.Close();

				// Busco la sociedad que corresponde a Privado porque hay cosas que se cargan
				// directamente a privado independientemente de la sociedad pagadora
				tStSS = "select nnumeri1 from sconsglo where gconsglo = 'PRIVADO'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tStSS, Module1.rcalta);
				tRrSS = new DataSet();
				tempAdapter_3.Fill(tRrSS);
				Module1.iPrivado = Convert.ToInt32(tRrSS.Tables[0].Rows[0]["nnumeri1"]);
				tRrSS.Close();

				if (Module1.bTemp)
				{
					//Modificaci�n 5/7: se graba en tabla f�sica para poder hacer consultas
					stDmovfact = "select * from dmovfacc where 1=2";
				}
				else
				{
					stDmovfact = "select * from " + stTablaFacturacion + " where 1=2";
				}

				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stDmovfact, Module1.rcalta);
				Module1.RrDmovfact = new DataSet();
				tempAdapter_4.Fill(Module1.RrDmovfact);

				if (!Module1.bError && stTipoSer != "U" && bMens_optional == Type.Missing)
				{
					//si no es de Urgencias y no es la mensual se cobra la comida del acompa�ante
					Module1.proModiEDIETVIA();
				}
				if (!Module1.bError)
				{
					//actualiza tabla de EPRESTAC
					Module1.proModiEPRESTAC(this, stTablaFacturacion);
				}
				if (!Module1.bError)
				{
					//calcula consumos por ETOMASFA
					Module1.proModiEFARMACO();
				}
				if (!Module1.bError)
				{
					//actualiza tabla de EINTERCO'
					Module1.proModiEINTERCO();
				}
				//RcAlta.CommitTrans
				if (Module1.bError)
				{

					if (Module1.bLLamadaIFMS)
					{
						Serrores.GrabaLog("Error ALTA_FACTU.prorecibePara. Episodio n�: " + Module1.iGanoregi.ToString() + "/" + Module1.IGnumregi.ToString() + " - " + Information.Err().Number.ToString() + ":" + Information.Err().Description, Path.GetDirectoryName(Application.ExecutablePath) + "\\ErroresIFMSFA_FACTINDIV.log");
					}
					else
					{
						Serrores.GrabaLog("Error ALTA_FACTU.prorecibePara. Episodio n�: " + Module1.iGanoregi.ToString() + "/" + Module1.IGnumregi.ToString() + " - " + Information.Err().Number.ToString() + ":" + Information.Err().Description, Path.GetDirectoryName(Application.ExecutablePath) + "\\ALTA_FACTU.log");

					}
				}                
                Formulario.proRecibeError(Module1.bError);
			}
			catch (System.Exception excep)
			{
				//RcAlta.RollbackTrans
				Module1.bError = true;
				if (Module1.bLLamadaIFMS)
				{
					Serrores.GrabaLog("Error ALTA_FACTU.prorecibePara. Episodio n�: " + Module1.iGanoregi.ToString() + "/" + Module1.IGnumregi.ToString() + " - " + Information.Err().Number.ToString() + ":" + excep.Message, Path.GetDirectoryName(Application.ExecutablePath) + "\\ErroresIFMSFA_FACTINDIV.log");
				}
				else
				{
					Serrores.GrabaLog("Error ALTA_FACTU.prorecibePara. Episodio n�: " + Module1.iGanoregi.ToString() + "/" + Module1.IGnumregi.ToString() + " - " + Information.Err().Number.ToString() + ":" + excep.Message, Path.GetDirectoryName(Application.ExecutablePath) + "\\ALTA_FACTU.log");

				}                
				Formulario.proRecibeError(Module1.bError);
			}
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad, string stFecha, int StServi_pak, object bMens_optional, object bEstadeAlta_optional, object esConsulta_optional, object sAfilUrg_optional, object sInspUrg_optional, object TablaFacturacion_optional)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, StParaSociedad, stFecha, StServi_pak, bMens_optional, bEstadeAlta_optional, esConsulta_optional, sAfilUrg_optional, sInspUrg_optional, TablaFacturacion_optional, false);
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad, string stFecha, int StServi_pak, object bMens_optional, object bEstadeAlta_optional, object esConsulta_optional, object sAfilUrg_optional, object sInspUrg_optional)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, StParaSociedad, stFecha, StServi_pak, bMens_optional, bEstadeAlta_optional, esConsulta_optional, sAfilUrg_optional, sInspUrg_optional, Type.Missing, false);
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad, string stFecha, int StServi_pak, object bMens_optional, object bEstadeAlta_optional, object esConsulta_optional, object sAfilUrg_optional)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, StParaSociedad, stFecha, StServi_pak, bMens_optional, bEstadeAlta_optional, esConsulta_optional, sAfilUrg_optional, Type.Missing, Type.Missing, false);
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad, string stFecha, int StServi_pak, object bMens_optional, object bEstadeAlta_optional, object esConsulta_optional)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, StParaSociedad, stFecha, StServi_pak, bMens_optional, bEstadeAlta_optional, esConsulta_optional, Type.Missing, Type.Missing, Type.Missing, false);
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad, string stFecha, int StServi_pak, object bMens_optional, object bEstadeAlta_optional)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, StParaSociedad, stFecha, StServi_pak, bMens_optional, bEstadeAlta_optional, Type.Missing, Type.Missing, Type.Missing, Type.Missing, false);
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad, string stFecha, int StServi_pak, object bMens_optional)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, StParaSociedad, stFecha, StServi_pak, bMens_optional, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, false);
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad, string stFecha, int StServi_pak)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, StParaSociedad, stFecha, StServi_pak, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, false);
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad, string stFecha)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, StParaSociedad, stFecha, 0, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, false);
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer, string StParaSociedad)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, StParaSociedad, String.Empty, 0, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, false);
		}

		public void proRecibePara(object Formulario, SqlConnection Conexion, string fFini, string fFfin, int IGano, int IGnum, string StCodpac, string stTipoSer)
		{
			proRecibePara(Formulario, Conexion, fFini, fFfin, IGano, IGnum, StCodpac, stTipoSer, String.Empty, String.Empty, 0, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, false);
		}

		public void prorecibirerror(int FbError, string StErrodescri)
		{
			if (FbError == 0)
			{
				Module1.bError = false;
			}
			else
			{
				Module1.bError = true;


				if (Module1.bLLamadaIFMS)
				{
					Serrores.GrabaLog("Error ALTA_FACTU.prorecibirerror. Episodio n�: " + Module1.iGanoregi.ToString() + "/" + Module1.IGnumregi.ToString() + " - " + FbError.ToString() + ":" + StErrodescri, Path.GetDirectoryName(Application.ExecutablePath) + "\\ErroresIFMSFA_FACTINDIV.log");
				}
				else
				{
					Serrores.GrabaLog("Error ALTA_FACTU.prorecibirerror. Episodio n�: " + Module1.iGanoregi.ToString() + "/" + Module1.IGnumregi.ToString() + " - " + FbError.ToString() + ":" + StErrodescri, Path.GetDirectoryName(Application.ExecutablePath) + "\\ALTA_FACTU.log");

				}

			}
		}


		private string ObtenerFechaLLegada(int a�o, int Numero)
		{
			string result = String.Empty;
			string sqlFecha = "select fllegada, ireingre, faltplan,itipingr from aepisadm where ganoadme =" + a�o.ToString() + " and gnumadme =" + Numero.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlFecha, Module1.rcalta);
			DataSet RrFecha = new DataSet();
			tempAdapter.Fill(RrFecha);
			result = Convert.ToString(RrFecha.Tables[0].Rows[0][0]);
			if (Convert.IsDBNull(RrFecha.Tables[0].Rows[0][1]))
			{
				Module1.sReingreso = "";
			}
			else
			{
				Module1.sReingreso = Convert.ToString(RrFecha.Tables[0].Rows[0][1]);
			}
			if (!Convert.IsDBNull(RrFecha.Tables[0].Rows[0]["faltplan"]))
			{
				Module1.vstFaltplan = Convert.ToString(RrFecha.Tables[0].Rows[0]["faltplan"]);
			}
			else
			{
				Module1.vstFaltplan = "";
			}

			Module1.VstItipingr = Convert.ToString(RrFecha.Tables[0].Rows[0]["itipingr"]);

			RrFecha.Close();
			return result;
		}
	}
}