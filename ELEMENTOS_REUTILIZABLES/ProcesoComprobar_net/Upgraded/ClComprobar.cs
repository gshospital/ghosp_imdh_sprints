using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ProcesoComprobar
{
	public class ClComprobar
	{

		//***********************************************************************
		//* Para las variables opcionales, si se manda una no se manda la otra. *
		//* En la pantalla que llame a esta DLL debe haber una funci�n que      *
		//* recoja lo que �sta devuelve:                                        *
		//*        Public Sub DevolverAsociarProceso(stError, stDescripcionError)             *
		//*          If Trim(stError) <> "" Then                                *
		//*            MsgBox stDescripcionError                                *
		//*          End If                                                     *
		//*        End Sub                                                      *
		//***********************************************************************
		public void Iniciar(SqlConnection Conexion, dynamic oVentana, string itiposer, int ganoregi, int gnumregi, object stFecha_optional, object stAccion_optional)
		{
			object stFecha = (stFecha_optional == Type.Missing) ? null : stFecha_optional as object;
			string stAccion = (stAccion_optional == Type.Missing) ? String.Empty : stAccion_optional as string;
			string stSql = String.Empty;
			DataSet RrEpi = null;
			DataSet RrPro = null;

			try
			{

				//Comprobamos si existe el episodio
				stSql = "select * from depiproc where itiposer  = '" + itiposer + "' and  ganoregi = " + ganoregi.ToString() + " and gnumregi = " + gnumregi.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, Conexion);
				RrEpi = new DataSet();
				tempAdapter.Fill(RrEpi);
				if (RrEpi.Tables[0].Rows.Count > 0)
				{
					//Si existe el episodio, comprobamos si el proceso est� abierto
					stSql = "select * from dproceso where ganoproc  = " + Convert.ToString(RrEpi.Tables[0].Rows[0]["ganoproc"]) + " and  gnumproc = " + Convert.ToString(RrEpi.Tables[0].Rows[0]["gnumproc"]);
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, Conexion);
					RrPro = new DataSet();
					tempAdapter_2.Fill(RrPro);
					if (RrPro.Tables[0].Rows.Count > 0)
					{
						if (Convert.IsDBNull(RrPro.Tables[0].Rows[0]["ffinproc"]))
						{
							if (stFecha_optional == Type.Missing)
							{
								//Si est� abierto y no se manda la fecha..., se comprueba la acci�n
								if (stAccion_optional != Type.Missing)
								{
									if (stAccion.Trim().ToUpper() == "B")
									{
										//Si queremos borrar...., se borra y no devolvemos nada
										RrEpi.Delete(tempAdapter_2);
										oVentana.DevolverAsociarProceso("", "");
									}
									if (stAccion.Trim().ToUpper() == "V")
									{
										//Si queremos validar...., no devolvemos nada
										oVentana.DevolverAsociarProceso("", "");
									}
								}
							}
							else
							{
								//Si est� abierto y se manda la fecha..., se compara con la de inicio
								if (Convert.ToDateTime(RrPro.Tables[0].Rows[0]["finiproc"]) <= Convert.ToDateTime(stFecha))
								{
									//Si es mayor...,no se devuelve nada
									oVentana.DevolverAsociarProceso("", "");
								}
								else
								{
									//Si no es mayor...,se devuelve error
									oVentana.DevolverAsociarProceso("ERROR", "Fecha de cambio de episodio menor que la de inicio del proceso.");
								}
							}
						}
						else
						{
							if (stFecha_optional == Type.Missing)
							{
								//Si no est� abierto y no se manda la fecha..., se devuelve el error
								oVentana.DevolverAsociarProceso("ERROR", "Episodio incluido en proceso cerrado. Reabrirlo previamente.");
							}
							else
							{
								//Si no est� abierto y se manda la fecha..., se compara con la de inicio y fin
								if (Convert.ToDateTime(RrPro.Tables[0].Rows[0]["finiproc"]) <= Convert.ToDateTime(stFecha) && Convert.ToDateTime(stFecha) <= Convert.ToDateTime(RrPro.Tables[0].Rows[0]["ffinproc"]))
								{
									//Si est� entre la de fin e inicio...,no se devuelve nada
									oVentana.DevolverAsociarProceso("", "");
								}
								else
								{
									//Si no est� entre la de fin e inicio...,se devuelve error
									oVentana.DevolverAsociarProceso("ERROR", "Fecha de cambio no incluida entre el inicio y el final del proceso.");
								}
							}
						}
					}
					RrPro.Close();
				}
				else
				{
					//Si no existe el episodio..., no devolvemos nada
					oVentana.DevolverAsociarProceso("", "");
				}
				RrEpi.Close();
			}
			catch
			{
				oVentana.DevolverAsociarProceso("ERROR", "Imposible realizar la comprobaci�n");
			}

		}

		public void Iniciar(SqlConnection Conexion, object oVentana, string itiposer, int ganoregi, int gnumregi, object stFecha_optional)
		{
			Iniciar(Conexion, oVentana, itiposer, ganoregi, gnumregi, stFecha_optional, Type.Missing);
		}

		public void Iniciar(SqlConnection Conexion, object oVentana, string itiposer, int ganoregi, int gnumregi)
		{
			Iniciar(Conexion, oVentana, itiposer, ganoregi, gnumregi, Type.Missing, Type.Missing);
		}
	}
}