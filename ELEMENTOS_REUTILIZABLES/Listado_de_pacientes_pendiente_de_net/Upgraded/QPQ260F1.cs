using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using CrystalWrapper;

namespace rptPacInterProg
{
	public partial class QPQ260F1
		: RadForm
	{


		const int Consulta7 = 1; // Ejecuta la consulta de 7 joins

		const string co_TABLA = "QPQ260D1";
		const int co_FILAS = 100;

		SqlConnection vRcCon = null;
		dynamic voCry = null;
		string vstAppPath = String.Empty;
		string vstDatabase = String.Empty;
		public string stEnfermeria = String.Empty;

		private bool bConsentimientoInformado = false;
		public QPQ260F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		public SqlConnection Conexion
		{
			set
			{
				vRcCon = value;
			}
		}

		public Crystal Crystal
		{
			set
			{
				voCry = value;
			}
		}


		public string AppPath
		{
			get
			{
				return vstAppPath;
			}
			set
			{
				vstAppPath = value;
			}
		}


		public string Database
		{
			get
			{
				return vstDatabase;
			}
			set
			{
				vstDatabase = value;
			}
		}


		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (DatosValidos())
			{
				if (ImprimeInforme())
				{
					this.Close();
				}

			}
		}



		private bool Prog2Database()
		{
			bool result = false;
			object dbLangGeneral = null;
			object[] Workspaces = null;
			// ralopezn TODO_X_5 24/05/2016
            //Database dbAux = null;

			DataSet RrPro = null;
			DataSet RrPrt = null;

			string stSql = String.Empty;
			string StCama = String.Empty;
			string stUnidad = String.Empty; // Unidad de enfermeria


			bool bInsertarRegistro = false;

			try
			{
				// Generamos una tabla temporal
				// �Como va a  ser la tabla?

				stSql = "CREATE TABLE " + co_TABLA + " (" + "ganoregi INTEGER, " + "gnumregi long, " + "campo0    DATE, " + "Campo1    TEXT (80), " + "Campo2    TEXT (80), " + "Campo3    integer, " + "Campo4    TEXT (80), " + "Campo5    TEXT (10), " + "Campo6    TEXT (80), " + "Campo7    TEXT (80), " + "Campo8    TEXT (80), " + "Campo9    TEXT (80), " + "Campo10   TEXT (80),  " + "Campo11   TEXT (20),  " + "Campo12   TEXT (1),  " + "Campo13   TEXT (40),  " + "Campo14   TEXT , " + "Campo15   TEXT (20),  " + "Campo16   TEXT (2)  " + "); ";

				//Si no existe la base de datos la creamos
				if (FileSystem.Dir(vstDatabase, FileAttribute.Normal) == "")
				{
                    // ralopezn TODO_X_5 24/05/2016	
                    //dbAux = (Database) Workspaces[0].CreateDatabase(Database, dbLangGeneral);

                }
                else
				{
                    //Si existe la BD la abrimos y la vaciamos
                    // ralopezn TODO_X_5 24/05/2016
                    //dbAux = (Database) Workspaces[0].OpenDatabase(vstDatabase);

                    // ralopezn TODO_X_5 24/05/2016
                    //foreach (TableDef Tabla in dbAux.TableDefs)
                    //{
                    //	if (Convert.ToString(Tabla.Name).ToUpper() == co_TABLA)
                    //	{
                    //		dbAux.Execute("DROP TABLE " + co_TABLA);
                    //		Information.Err().Clear();
                    //		break;
                    //	}
                    //}
                }

                // ralopezn TODO_X_5 24/05/2016
                //dbAux.Execute(stSql);

                //Abrimos la consulta de origen
                //   ESTA CONSULTA TIENE 7 JIONS

                stSql = " SELECT PRO.gidenpac,PRO.iingambu , PRO.ganoregi, PRO.gnumregi, PRO.finterve, PRO.nordquir, PRO.odiagnos, " + 
				        " PRO.odiagnos , PRO.ointerve, PRO.nafiliac, PRO.iturnomt, PRO.iconsinf, " + 
				        " SER.dnomserv, QUI.dquirofa, PER.dnompers, PER.dap1pers, PER.dap2pers, PAC.dnombpac, " + 
				        " PAC.DAPE1PAC , PAC.DAPE2PAC, PRE.dprestac, ANE.danestes, HIS.ghistoria, SOC.dsocieda ";
				stSql = stSql + " FROM " + " QPROGQUI PRO " + "left join DSERVICI SER on PRO.gservici = SER.gservici " + "left join DPERSONA PER on PRO.gmedicoc = PER.gpersona " + "left join QQUIROFA QUI on PRO.gquirofa = QUI.gquirofa " + "left join DPACIENT PAC on PRO.gidenpac = PAC.gidenpac " + "left join DCODPRES PRE on PRO.gprestac = PRE.gprestac " + "left join DANESTES ANE on PRO.ganestes = ANE.ganestes " + "left join HDOSSIER HIS on PRO.gidenpac = HIS.gidenpac " + "left join DSOCIEDA SOC on PRO.gsocieda = SOC.gsocieda ";
				stSql = stSql + " WHERE 1=1 and ";



				object tempRefParam = sdcDesde.Value.Date.ToString();
				object tempRefParam2 = sdcHasta.Value.Date.ToShortDateString() + " 23:59:59";
				stSql = stSql + "PRO.FINTERVE >= " + Serrores.FormatFecha(tempRefParam) + " AND PRO.FINTERVE <= " + Serrores.FormatFechaHMS(tempRefParam2) + " AND ";

				//(maplaza)(27/12/2006)Los controles de tipo "Selecion" son sustituidos por controles que realicen una funci�n equivalente.
				//stSQLCond = stSQLCond + slcServicios.sql
				if (lsbSeleccionEspec.Items.Count > 0)
				{
					stSql = stSql + "PRO.GSERVICI IN(";
					for (int iContLista = 0; iContLista <= lsbSeleccionEspec.Items.Count - 1; iContLista++)
					{
						if (iContLista < lsbSeleccionEspec.Items.Count - 1)
						{
							int tempRefParam3 = 1;
							int tempRefParam4 = iContLista;
							stSql = stSql + Convert.ToString(lsbSeleccionEspec.Items[iContLista].Value) + ",";
							iContLista = Convert.ToInt32(tempRefParam4);
						}
						else
						{
							int tempRefParam5 = 1;
							int tempRefParam6 = iContLista;
							stSql = stSql + Convert.ToString(lsbSeleccionEspec.Items[iContLista].Value) + ") ";
							iContLista = Convert.ToInt32(tempRefParam6);
						}
					}
				}
				//-----

				stSql = stSql + "AND PRO.isitprog = 'P' ";

				stSql = stSql + " ORDER BY PRO.ganoregi, PRO.gnumregi";
				//Abrimos el resultado de la consulta
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, vRcCon);
				RrPro = new DataSet();
				tempAdapter.Fill(RrPro);


				foreach (DataRow iteration_row in RrPro.Tables[0].Rows)
				{

					// procedimiento para decidir si sacar al paciente o no dependiendo del tipo de ingreso -> ambulante o no

					bDevuelveCama(Convert.ToString(iteration_row["gidenpac"]), ref StCama, ref stUnidad);


					if (StCama != "")
					{ // si tiene cama

						if (Convert.ToDouble(cbbEnfermeria.get_ListIndex()) != -1)
						{ // si ha filtrado por unidad de enfermeria

							int tempRefParam7 = 1;
							int tempRefParam8 = 0;
							bInsertarRegistro = (stUnidad.Trim() == Convert.ToString(cbbEnfermeria.get_Column(tempRefParam7, tempRefParam8)).Trim());
						}
						else
						{
							bInsertarRegistro = true;

						}

					}
					else
					{
						// si no tiene cama

						if (Convert.ToDouble(cbbEnfermeria.get_ListIndex()) != -1)
						{ // si ha filtrado por unidad de enfermeria

							bInsertarRegistro = (Convert.ToString(iteration_row["iingambu"]) == "I");
						}
						else
						{
							bInsertarRegistro = true;
						}

					}

					if (bInsertarRegistro)
					{

						// Primero construimos la consulta de inserci�n
						stSql = "Insert into QPQ260D1 VALUES (";

						stSql = stSql + Convert.ToString(iteration_row["ganoregi"]) + ", "; //ganoregi
						stSql = stSql + Convert.ToString(iteration_row["gnumregi"]) + ", "; //gnumregi

						stSql = stSql + StringsHelper.Format(iteration_row["finterve"], "'DD/MM/YYYY', "); //Campo 0
						stSql = stSql + "'" + Minuscula(iteration_row["dnomserv"]) + "', "; //Campo 1
						stSql = stSql + "'" + Minuscula(iteration_row["dquirofa"]) + "', "; //Campo 2
						stSql = stSql + ((!Convert.IsDBNull(iteration_row["nordquir"])) ? Convert.ToString(iteration_row["nordquir"]) : "0") + ", "; //Campo 3
						stSql = stSql + "'" + Minuscula(iteration_row["dnombpac"]) + " " + Minuscula(iteration_row["DAPE1PAC"]) + " " + Minuscula(iteration_row["DAPE2PAC"]) + "', "; //Campo 4

						//Para la consulta de 7 joins
						stSql = stSql + "'" + Convert.ToString(iteration_row["ghistoria"]) + "', "; //Campo 5

						stSql = stSql + "'Dr.- Dra. " + Minuscula(iteration_row["dap1pers"]) + "" + " " + Minuscula(iteration_row["dap2pers"]) + "" + ", " + Minuscula(iteration_row["dnompers"]) + "', "; //campo 6
						stSql = stSql + "'" + Minuscula(iteration_row["odiagnos"]) + "', "; //campo 7

						//Para la consulta de 7 joins
						stSql = stSql + "'" + Minuscula(iteration_row["dprestac"]) + "', "; // campo 8
						stSql = stSql + "'" + Minuscula(iteration_row["danestes"]) + "', "; // Campo 9

						stSql = stSql + "'',";

						if (StCama != "")
						{

							stSql = stSql + "'Cama : " + StCama + "',";

						}
						else
						{
							stSql = stSql + "null,";

						}

						stSql = stSql + "'" + Convert.ToString(iteration_row["iturnomt"]) + "', ";
						stSql = stSql + "'" + Minuscula(iteration_row["dsocieda"]) + "', ";
						stSql = stSql + "'" + Minuscula((Convert.ToString(iteration_row["ointerve"]) + "").Trim()) + "' , ";
						stSql = stSql + "'" + Convert.ToString(iteration_row["nafiliac"]) + "', ";

						if (bConsentimientoInformado)
						{
							if (Convert.IsDBNull(iteration_row["iconsinf"]))
							{
								stSql = stSql + "'NO' ) ";
							}
							else
							{
								stSql = stSql + "'" + ((Convert.ToString(iteration_row["iconsinf"]).Trim().ToUpper() == "S") ? "SI" : "NO") + "' ) ";
							}
						}
						else
						{
							stSql = stSql + "'' ) ";
						}

                        //ejecutamos la  inserci�n
                        // ralopezn TODO_X_5 24/05/2016
                        //dbAux.Execute(stSql);


                        RrPrt = AbreResultSet("SELECT ART.* " + 
						        "FROM DARTICUL ART left join  QPROTESI PRT on " + 
						        "PRT.gprotesi = ART.gcodiart AND " + 
						        "PRT.gtipoart = ART.gtipoart " + 
						        "WHERE " + 
						        "PRT.ganoregi = " + Conversion.Str(iteration_row["ganoregi"]) + " And " + "PRT.gnumregi = " + Conversion.Str(iteration_row["gnumregi"]) + " And PRT.iproreal = 'P' ");


						foreach (DataRow iteration_row_2 in RrPrt.Tables[0].Rows)
						{
							stSql = "INSERT INTO " + co_TABLA + "( " + "ganoregi, " + "gnumregi, " + "Campo0, " + "Campo1, " + "Campo2, " + "Campo3, " + "Campo10) " + " VALUES ( " + Conversion.Str(iteration_row["ganoregi"]) + ", " + Conversion.Str(iteration_row["gnumregi"]) + ", " + StringsHelper.Format(iteration_row["finterve"], "'DD/MM/YYYY', ") + "'" + Minuscula(iteration_row["dnomserv"]) + "', " + "'" + Minuscula(iteration_row["dquirofa"]) + "', ";
							if (Convert.IsDBNull(iteration_row["nordquir"]))
							{
								stSql = stSql + "null, ";
							}
							else
							{
								stSql = stSql + Conversion.Str(iteration_row["nordquir"]) + ", ";
							}
							stSql = stSql + "'" + Minuscula(iteration_row_2["darticul"]) + "')";

                            // ralopezn TODO_X_5 24/05/2016	
                            //dbAux.Execute(stSql);

                        }


                    }


				}




                // ralopezn TODO_X_5 24/05/2016
                //dbAux.Close();
                RrPro.Close();

				return true;
			}
			catch (System.Exception excep)
			{

				RadMessageBox.Show(excep.Message + ": No tratado en la gesti�n de errores : " + excep.Message, Application.ProductName);


				return result;
			}
		}
		private bool ImprimeInforme()
		{
			bool result = false;
			Cursor iMouse = null;

			try
			{
				//Creamos la tabla temporal
				iMouse = this.Cursor;
				this.Cursor = Cursors.AppStarting;
				if (Prog2Database())
				{
					// Asignamos el nombre de la base de datos
					LimpiaFormulas();
					//voCry.Formulas[0] = "GrupoHosp ='" + GetGlobalVar("GRUPOHO", "VALFANU1") + "'";
                    voCry.Formulas["GrupoHosp"] = "'" + GetGlobalVar("GRUPOHO", "VALFANU1") + "'";
                    //voCry.Formulas[1] = "NombreHosp ='" + GetGlobalVar("NombreHo", "VALFANU1") + "'";
                    voCry.Formulas["NombreHosp"] = "'" + GetGlobalVar("NombreHo", "VALFANU1") + "'";
                    //voCry.Formulas[3] = "DireccHosp ='" + GetGlobalVar("DireccHO", "VALFANU1") + "'";
                    voCry.Formulas["DireccHosp"] = "'" + GetGlobalVar("DireccHO", "VALFANU1") + "'";
                    //voCry.Formulas[4] = "TitInforme='HOJA OPERATORIA'";
                    voCry.Formulas["TitInforme"] = "'HOJA OPERATORIA'";
                    //voCry.Formulas[5] = "FechaDesde='" + sdcDesde.Value.Date + "'";
                    voCry.Formulas["FechaDesde"] = "'" + sdcDesde.Value.Date + "'";
                    //voCry.Formulas[6] = "FechaHasta='" + sdcHasta.Value.Date + "'";
                    voCry.Formulas["FechaHasta"] = "'" + sdcHasta.Value.Date + "'";
                    //UPGRADE-WARNING: It was not possible to determine if this addition expression should use an addition operator (+) or a concatenation operator (&)
                    //UPGRADE-WARNING: It was not possible to determine if this addition expression should use an addition operator (+) or a concatenation operator (&)
                    //voCry.Formulas[7] = Convert.ToDouble(Double.Parse("Enfermeria='") + Convert.ToDouble(cbbEnfermeria.get_Value())) + Double.Parse("'");
                    voCry.Formulas["Enfermeria"] = Convert.ToString("'" + Convert.ToDouble(cbbEnfermeria.get_Value()) + "'");
                    //voCry.Formulas[8] = "HayConsentimiento='" + ((bConsentimientoInformado) ? "S" : "N") + "'";
                    voCry.Formulas["HayConsentimiento"] = "'" + ((bConsentimientoInformado) ? "S" : "N") + "'";
                    voCry.ReportFileName = AppPath + "\\..\\ElementosComunes\\rpt\\qpq260r1.rpt";
					voCry.DataFiles[0] = Database;
					voCry.SubreportToChange = "protesis";
					voCry.DataFiles[0] = Database;
					voCry.SubreportToChange = "";

					voCry.WindowTitle = Text;
                    // ralopezn TODO_X_5 24/05/2016
                    //voCry.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;
                    voCry.Action = 1;
					voCry.PrinterStopPage = voCry.PageCount;
					//voCry.PageShow(1);
					voCry.Reset();

				}
				else
				{
					Information.Err().Number = -1;
				}

				this.Cursor = (Cursor) iMouse;

				return result;
			}
			catch(Exception ex)
			{


				int lError = 0;
				int iResume = 0;
				if (Information.Err().Number != 0)
				{

					lError = Information.Err().Number;
					this.Cursor = (Cursor) iMouse;
					iResume = Serrores.GestionErrorCrystal(lError, vRcCon);

					switch(iResume)
					{
						case 0 :  //Salida de la funcion 
							result = false; 
							break;
						case 1 :  //resume next 
							this.Cursor = Cursors.WaitCursor; 
							//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement"); 
							break;
						case 2 : 
							this.Cursor = Cursors.WaitCursor; 
							//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Statement"); 
							break;
					}

				}
				this.Cursor = (Cursor) iMouse;

				return result;
			}
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private bool proConsentimientoActivado()
		{

			bool result = false;
			string stSql = String.Empty;
			DataSet RrDatos = null;

			try
			{

				stSql = "SELECT 'S' FROM SCONSGLO WHERE gconsglo = 'QCONSINF' AND valfanu1 = 'S'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, vRcCon);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				result = RrDatos.Tables[0].Rows.Count != 0;

				RrDatos.Close();

				return result;
			}
			catch
			{

				return false;
			}
		}

		private void QPQ260F1_Load(Object eventSender, EventArgs eventArgs)
		{
			LlenaComboEnfermeria(cbbEnfermeria, stEnfermeria);

			bConsentimientoInformado = proConsentimientoActivado();

			sdcDesde.Value = DateTime.Now;
			sdcHasta.Value = DateTime.Now;
			//(maplaza)(26/12/2006)Los controles de tipo "Selecion" son sustituidos por controles que realicen una funci�n equivalente.
			//Set slcServicios.Conexion = vRcCon
			proRellenarEspecialidades();
			//-----
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			if (stEnfermeria != "")
			{
				cbbEnfermeria.Enabled = false;
			}

		}

		public void Muestra(object parent)
		{
			Visible = true;
		}

		private bool DatosValidos()
		{

			bool result = false;
			if (esVacio(sdcDesde.Value.ToShortDateString()))
			{ //fecha desde en blanco
				int tempRefParam = 1040;
				string tempRefParam2 = frmFecha.Text.ToLower() + " " + lbFecDesde.Text.ToLower();
				MuestraMensaje(ref tempRefParam, ref tempRefParam2);

			}
			else if (esVacio(sdcHasta.Value.ToShortDateString()))
			{  // Fecha hasta en blanco
				int tempRefParam3 = 1040;
				string tempRefParam4 = frmFecha.Text.ToLower() + " " + lbFecHasta.Text.ToLower();
				MuestraMensaje(ref tempRefParam3, ref tempRefParam4);

			}
			else if (sdcDesde.Value.Date > sdcHasta.Value.Date)
			{ 
				int tempRefParam5 = 1020;
				string tempRefParam6 = frmFecha.Text.ToLower() + " " + lbFecDesde.Text.ToLower();
				string tempRefParam7 = "menor o igual";
				string tempRefParam8 = frmFecha.Text.ToLower() + " " + lbFecHasta.Text.ToLower();
				MuestraMensaje(ref tempRefParam5, ref tempRefParam6, ref tempRefParam7, ref tempRefParam8);
			}
			else if ((lsbSeleccionEspec.Items.Count == 0))
			{ 
				//-----
				int tempRefParam9 = 1500;
				string tempRefParam10 = "Selecionar";
				string tempRefParam11 = "un servicio.";
				MuestraMensaje(ref tempRefParam9, ref tempRefParam10, ref tempRefParam11);

			}
			else
			{
				result = true;

			}

			return result;
		}

		private bool esVacio(string oAlgo)
		{
			return Convert.IsDBNull(oAlgo) || oAlgo == "";
		}
		public int MuestraMensaje(ref int icodigo, ref string st1, ref string st2, ref string st3, ref string st4, ref string st5, ref string st6)
		{

			int result = 0;
			Mensajes.ClassMensajes oClass = new Mensajes.ClassMensajes();
			string tempRefParam = "Listado de pacientes por intervenir";
			string tempRefParam2 = "";
			short tempRefParam3 = (short) icodigo;
			string[] tempRefParam4 = new string[]{st1, st2, st3, st4, st5, st6};
			result = Convert.ToInt32(oClass.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, vRcCon, tempRefParam4));
			icodigo = tempRefParam3;

			return result;
		}

		public int MuestraMensaje(ref int icodigo, ref string st1, ref string st2, ref string st3, ref string st4, ref string st5)
		{
			string tempRefParam = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref st2, ref st3, ref st4, ref st5, ref tempRefParam);
		}

		public int MuestraMensaje(ref int icodigo, ref string st1, ref string st2, ref string st3, ref string st4)
		{
			string tempRefParam2 = String.Empty;
			string tempRefParam3 = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref st2, ref st3, ref st4, ref tempRefParam2, ref tempRefParam3);
		}

		public int MuestraMensaje(ref int icodigo, ref string st1, ref string st2, ref string st3)
		{
			string tempRefParam4 = String.Empty;
			string tempRefParam5 = String.Empty;
			string tempRefParam6 = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref st2, ref st3, ref tempRefParam4, ref tempRefParam5, ref tempRefParam6);
		}

		public int MuestraMensaje(ref int icodigo, ref string st1, ref string st2)
		{
			string tempRefParam7 = String.Empty;
			string tempRefParam8 = String.Empty;
			string tempRefParam9 = String.Empty;
			string tempRefParam10 = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref st2, ref tempRefParam7, ref tempRefParam8, ref tempRefParam9, ref tempRefParam10);
		}

		public int MuestraMensaje(ref int icodigo, ref string st1)
		{
			string tempRefParam11 = String.Empty;
			string tempRefParam12 = String.Empty;
			string tempRefParam13 = String.Empty;
			string tempRefParam14 = String.Empty;
			string tempRefParam15 = String.Empty;
			return MuestraMensaje(ref icodigo, ref st1, ref tempRefParam11, ref tempRefParam12, ref tempRefParam13, ref tempRefParam14, ref tempRefParam15);
		}

		public int MuestraMensaje(ref int icodigo)
		{
			string tempRefParam16 = String.Empty;
			string tempRefParam17 = String.Empty;
			string tempRefParam18 = String.Empty;
			string tempRefParam19 = String.Empty;
			string tempRefParam20 = String.Empty;
			string tempRefParam21 = String.Empty;
			return MuestraMensaje(ref icodigo, ref tempRefParam16, ref tempRefParam17, ref tempRefParam18, ref tempRefParam19, ref tempRefParam20, ref tempRefParam21);
		}
		private void LimpiaFormulas()
		{
            if (voCry != null)
            {
                foreach(var key in voCry.Formulas.Keys)
                {
                    voCry.Formulas[key] = "";
                }
               
                voCry.SQLQuery = "";
                voCry.SortFields[0] = "";
                voCry.SortFields[1] = "";
                voCry.SortFields[2] = "";
            }
		}

		private string GetGlobalVar(string stVar, string stField)
		{
			// Devuelve el valor del la constante almacenada en SCONSGLO
			// con el nombre stVar y el valor de stField

			string result = String.Empty;
			DataSet RrGlobal = AbreResultSet("Select * from sConsGlo where gConsGlo = '" + stVar + "'");
			result = Convert.ToString(RrGlobal.Tables[0].Rows[0][stField]).Trim();
			RrGlobal.Close();
			return result;
		}

		private DataSet AbreResultSet(string stSql, int Tipo, object iBloqueo, ref int opciones)
		{
			DataSet result = null;
			try
			{
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, vRcCon);
				result = new DataSet();
				tempAdapter.Fill(result);
			}
			catch (System.Exception excep)
			{
				RadMessageBox.Show(excep.Message, Application.ProductName);
				switch(Serrores.GestionErrorBaseDatos(excep.InnerException as SqlException, vRcCon))
				{
					case 1 :  //Ejecuta la siguiente linea de c�digo 
						result = null; 
						//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement"); 
						break;
					case 2 :  //VUelve a ejecutar la linea del error 
						//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Statement"); 
						break;
				}
			}
			return result;
		}

		private DataSet AbreResultSet(string stSql, int Tipo, object iBloqueo)
		{
			int tempRefParam22 = 0;
			return AbreResultSet(stSql, Tipo, iBloqueo, ref tempRefParam22);
		}

		private DataSet AbreResultSet(string stSql, int Tipo)
		{
			int tempRefParam23 = 0;
			return AbreResultSet(stSql, Tipo, 1, ref tempRefParam23);
		}

		private DataSet AbreResultSet(string stSql)
		{
			int tempRefParam24 = 0;
			return AbreResultSet(stSql, 0, 1, ref tempRefParam24);
		}
		//private string Padl(string stCadena, int iEspacios, string stRelleno = " ")
		//{
				//return new string(stRelleno[0], iEspacios - stCadena.Trim().Length) + stCadena.Trim();
		//}
		private string Minuscula(object stString)
		{
			//Convierte a min�scula todas las letras de ststring menos la primera que la pone en may�sculas
			if (!Convert.IsDBNull(stString))
			{
				return Convert.ToString(stString).Trim().Substring(0, Math.Min(1, Convert.ToString(stString).Trim().Length)).ToUpper() + Convert.ToString(stString).Trim().Substring(1).ToLower();
			}
			else
			{
				return "";
			}
		}

		private void LlenaComboEnfermeria(UpgradeHelpers.MSForms.MSCombobox cbbEnf, string stEnf = "")
		{
			string stSql = String.Empty;
			DataSet RrEnf = null;

			int iIndex = -1;
			try
			{
				stSql = " SELECT * FROM DUNIENFEVA";
				RrEnf = AbreResultSet(stSql);

				foreach (DataRow iteration_row in RrEnf.Tables[0].Rows)
				{
					cbbEnf.AddItem(Convert.ToString(iteration_row["dunidenf"]));
                    cbbEnf.set_Column(1, cbbEnf.Items.Count - 1, Convert.ToString(iteration_row["gunidenf"]).Trim());

					if (stEnf.Trim() != "" && stEnf.Trim() == Convert.ToString(iteration_row["gunidenf"]).Trim())
					{
						iIndex = cbbEnf.Items.Count - 1;
					}


				}
				RrEnf.Close();
				cbbEnf.SelectedIndex = iIndex;
			}
			catch
			{
			}



		}




		public double GetNHistClinica(string stIdenPac)
		{
			double result = 0;
			string stSql = String.Empty;
			DataSet RrHis = null;
			try
			{

				stSql = "select ghistoria from hdosSier where gidenpac = '" + stIdenPac + "'";
				RrHis = AbreResultSet(stSql);
				result = Convert.ToDouble(RrHis.Tables[0].Rows[0]["ghistoria"]);
				RrHis.Close();
			}
			catch
			{
				result = 0d;
			}

			return result;
		}

		public string GetDescGeneral(string stTabla, string stCodCampo, string stDescCampo, string stCodigo)
		{


			string result = String.Empty;
			DataSet RrGen = null;
			string stSql = String.Empty;
			try
			{
				stSql = "SELECT " + stDescCampo + " " + "FROM " + stTabla + " " + "WHERE " + stCodCampo + " = " + stCodigo;

				RrGen = AbreResultSet(stSql);
				result = Convert.ToString(RrGen.Tables[0].Rows[0][stDescCampo]).Trim();
				RrGen.Close();
			}
			catch
			{
				result = "";
			}

			return result;
		}




		private bool bDevuelveCama(string IdPaciente, ref string StCama, ref string UnidadEnfemeria)
		{
			bool result = false;
			string sqlCama = "select gplantas,ghabitac,gcamasbo,gcounien from DCAMASBO where gidenpac ='" + IdPaciente + "'" + 
			                 " and iestcama ='O'";

			//If cbbEnfermeria.ListIndex > -1 Then
			//       sqlCama = sqlCama & " and gcounien='" & cbbEnfermeria.Column(1) & "'"
			//End If

			DataSet RrCama = AbreResultSet(sqlCama, 1);
			result = RrCama.Tables[0].Rows.Count == 1;
			if (result)
			{
				//UPGRADE-WARNING: It was not possible to determine if this addition expression should use an addition operator (+) or a concatenation operator (&)
				StCama = Convert.ToString(Double.Parse(StringsHelper.Format(RrCama.Tables[0].Rows[0]["gplantas"], "00") + StringsHelper.Format(RrCama.Tables[0].Rows[0]["ghabitac"], "00")) + Convert.ToDouble(RrCama.Tables[0].Rows[0]["gcamasbo"]));
				UnidadEnfemeria = Convert.ToString(RrCama.Tables[0].Rows[0]["gcounien"]).Trim();
			}
			else
			{
				StCama = "";
				UnidadEnfemeria = "";
			}
			RrCama.Close();
			return result;
		}

		//(maplaza)(26/12/2006)Los controles de tipo "Selecion" son sustituidos por controles que realicen una funci�n equivalente.
		private void proRellenarEspecialidades()
		{


			lsbEspecialidades.Items.Clear();

			//lsbEspecialidades.ColumnCount = 2;
			//lsbEspecialidades.ColumnWidths = "30;0";

			string sqlEspecialidades = "select ltrim(rtrim(dnomServ)) dnombre, gservici from DSERVICI";
			sqlEspecialidades = sqlEspecialidades + " Where iquirofa = 'S'";
			sqlEspecialidades = sqlEspecialidades + " Order By dnombre";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlEspecialidades, vRcCon);
			DataSet RrEspecialidades = new DataSet();
			tempAdapter.Fill(RrEspecialidades);

			int ilistas = 0;

			foreach (DataRow iteration_row in RrEspecialidades.Tables[0].Rows)
			{
				object tempRefParam = Convert.ToString(iteration_row["dnombre"]).Trim();
				object tempRefParam2 = Type.Missing;
                lsbEspecialidades.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dnombre"]).Trim(), Convert.ToString(iteration_row["gservici"]).Trim()));

				ilistas++;

			}

			RrEspecialidades.Close();
			CbMayorMayor.Enabled = true;
		}

		//(maplaza)(22/12/2006)Los controles de tipo "Selecion" son sustituidos por controles que realicen una funci�n equivalente.
		//Los procedimientos que aparecen a continuaci�n se encargan de realizar esa funci�n.
		private void cbMayor_Click(Object eventSender, EventArgs eventArgs)
		{
			proMandarIzquierdaDerechaSeleccionados(lsbEspecialidades, lsbSeleccionEspec);
			lsbEspecialidades_Change(null, null);
			lsbSeleccionEspec_Change(null, null);
            lsbEspecialidades.SelectedIndex = -1;
        }

		private void CbMayorMayor_Click(Object eventSender, EventArgs eventArgs)
		{
			proMandarIzquierdaDerechaTodos(lsbEspecialidades, lsbSeleccionEspec);
			lsbEspecialidades_Change(null, null);
			lsbSeleccionEspec_Change(null, null);
		}

		private void cbMenor_Click(Object eventSender, EventArgs eventArgs)
		{
			proMandarDerechaIzquierdaSeleccionados(lsbEspecialidades, lsbSeleccionEspec);
			lsbEspecialidades_Change(null, null);
			lsbSeleccionEspec_Change(null, null);
            lsbSeleccionEspec.SelectedIndex = -1;

        }

		private void cbMenorMenor_Click(Object eventSender, EventArgs eventArgs)
		{
			proMandarDerechaIzquierdaTodos(lsbEspecialidades, lsbSeleccionEspec);
			lsbEspecialidades_Change(null, null);
			lsbSeleccionEspec_Change(null, null);
		}

		private void proMandarDerechaIzquierdaSeleccionados(Telerik.WinControls.UI.RadListControl lsbListaOrigen, Telerik.WinControls.UI.RadListControl lsbListaDestino)
		{
			if (!HayAlgunoSeleccionado(lsbListaDestino))
			{
				return;
			}

			for (int ilista = 0; ilista <= lsbListaDestino.SelectedItems.Count - 1; ilista++)
			{
                lsbListaOrigen.Items.Add(new RadListDataItem(
                    lsbListaDestino.SelectedItems[ilista].Text,
                    lsbListaDestino.SelectedItems[ilista].Value));
			}

			for (int ilista = lsbListaDestino.SelectedItems.Count - 1; ilista >= 0; ilista--)
			{
                lsbListaDestino.Items.Remove(lsbListaDestino.SelectedItems[ilista]);
            }
            lsbListaDestino.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            lsbListaOrigen.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
        }

		private void proMandarIzquierdaDerechaSeleccionados(Telerik.WinControls.UI.RadListControl lsbListaOrigen, Telerik.WinControls.UI.RadListControl lsbListaDestino)
		{
			if (!HayAlgunoSeleccionado(lsbListaOrigen))
			{
				return;
			}

			for (int ilista = 0; ilista <= lsbListaOrigen.SelectedItems.Count - 1; ilista++)
			{
                lsbListaDestino.Items.Add(new RadListDataItem(
                    lsbListaOrigen.SelectedItems[ilista].Text, 
                    lsbListaOrigen.SelectedItems[ilista].Value));
			}

			for (int ilista = lsbListaOrigen.SelectedItems.Count - 1; ilista >= 0; ilista--)
			{
				lsbListaOrigen.Items.Remove(lsbListaOrigen.SelectedItems[ilista]);
			}
            lsbListaDestino.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            lsbListaOrigen.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
        }

		private void proMandarIzquierdaDerechaTodos(Telerik.WinControls.UI.RadListControl lsbListaOrigen, Telerik.WinControls.UI.RadListControl lsbListaDestino)
		{
			for (int ilista = 0; ilista <= lsbListaOrigen.Items.Count - 1; ilista++)
			{
                lsbListaDestino.Items.Add(new RadListDataItem(lsbListaOrigen.Items[ilista].Text, lsbListaOrigen.Items[ilista].Value));
			}

			lsbListaOrigen.Items.Clear();
		}

		private void proMandarDerechaIzquierdaTodos(Telerik.WinControls.UI.RadListControl lsbListaOrigen, Telerik.WinControls.UI.RadListControl lsbListaDestino)
		{
            for (int ilista = 0; ilista <= lsbListaDestino.Items.Count - 1; ilista++)
			{
				lsbListaOrigen.Items.Add(new RadListDataItem(lsbListaDestino.Items[ilista].Text, lsbListaDestino.Items[ilista].Value));
			}

			lsbListaDestino.Items.Clear();
		}

		private void lsbEspecialidades_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			cbMayor_Click(cbMayor, new EventArgs());
		}

		private void lsbSeleccionEspec_DoubleClick(Object eventSender, EventArgs eventArgs)
		{
			cbMenor_Click(cbMenor, new EventArgs());
		}

		private void lsbEspecialidades_Change(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
		{
			if (lsbEspecialidades.Items.Count > 0)
			{
				CbMayorMayor.Enabled = true;
				cbMayor.Enabled = HayAlgunoSeleccionado(lsbEspecialidades);
			}
			else
			{
				CbMayorMayor.Enabled = false;
				cbMayor.Enabled = false;
			}

		}

		private void lsbSeleccionEspec_Change(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
		{

			if (lsbSeleccionEspec.Items.Count > 0)
			{
				cbMenorMenor.Enabled = true;
				cbMenor.Enabled = HayAlgunoSeleccionado(lsbSeleccionEspec);
			}
			else
			{
				cbMenorMenor.Enabled = false;
				cbMenor.Enabled = false;
			}

		}

		private bool HayAlgunoSeleccionado(Telerik.WinControls.UI.RadListControl lista)
		{
            return lista.SelectedItems.Count != 0;
		}

		//FIN (maplaza)(22/12/2006)
		//-----
		private void QPQ260F1_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}