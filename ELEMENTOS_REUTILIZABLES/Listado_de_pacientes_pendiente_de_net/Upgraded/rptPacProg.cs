using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace rptPacInterProg
{
	public class rptPacProgInt
	{


		QPQ260F1 oForm = null;


		public void Muestra(dynamic parent, string AppPath, string Database, CrystalWrapper.Crystal Crystal, ref SqlConnection Conexion, string stEnfermeria = "")
		{
			oForm = new QPQ260F1();
			oForm.stEnfermeria = stEnfermeria;
			oForm.AppPath = AppPath;
			oForm.Database = Database;
			oForm.Conexion = Conexion;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			oForm.Crystal = Crystal;

			//.Muestra parent
			oForm.ShowDialog();
			parent.Focus();
        }
	}
}