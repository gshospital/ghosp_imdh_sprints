using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace rptPacInterProg
{
	partial class QPQ260F1
	{

		#region "Upgrade Support "
		private static QPQ260F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static QPQ260F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new QPQ260F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbAceptar", "cbCancelar", "sdcDesde", "sdcHasta", "lbFecHasta", "lbFecDesde", "frmFecha", "cbMenorMenor", "cbMenor", "cbMayor", "CbMayorMayor", "lsbSeleccionEspec", "lsbEspecialidades", "frmEspecialidad", "cbbEnfermeria", "Label4"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadDateTimePicker sdcDesde;
		public Telerik.WinControls.UI.RadDateTimePicker sdcHasta;
		public Telerik.WinControls.UI.RadLabel lbFecHasta;
		public Telerik.WinControls.UI.RadLabel lbFecDesde;
		public Telerik.WinControls.UI.RadGroupBox frmFecha;
		public Telerik.WinControls.UI.RadButton cbMenorMenor;
		public Telerik.WinControls.UI.RadButton cbMenor;
		public Telerik.WinControls.UI.RadButton cbMayor;
		public Telerik.WinControls.UI.RadButton CbMayorMayor;
		public Telerik.WinControls.UI.RadListControl lsbSeleccionEspec;
		public Telerik.WinControls.UI.RadListControl lsbEspecialidades;
		public Telerik.WinControls.UI.RadGroupBox frmEspecialidad;
		public UpgradeHelpers.MSForms.MSCombobox cbbEnfermeria;
		public Telerik.WinControls.UI.RadLabel Label4;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.frmFecha = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcDesde = new Telerik.WinControls.UI.RadDateTimePicker();
            this.sdcHasta = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lbFecHasta = new Telerik.WinControls.UI.RadLabel();
            this.lbFecDesde = new Telerik.WinControls.UI.RadLabel();
            this.frmEspecialidad = new Telerik.WinControls.UI.RadGroupBox();
            this.cbMenorMenor = new Telerik.WinControls.UI.RadButton();
            this.cbMenor = new Telerik.WinControls.UI.RadButton();
            this.cbMayor = new Telerik.WinControls.UI.RadButton();
            this.CbMayorMayor = new Telerik.WinControls.UI.RadButton();
            this.lsbSeleccionEspec = new Telerik.WinControls.UI.RadListControl();
            this.lsbEspecialidades = new Telerik.WinControls.UI.RadListControl();
            this.cbbEnfermeria = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmFecha)).BeginInit();
            this.frmFecha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFecHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFecDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmEspecialidad)).BeginInit();
            this.frmEspecialidad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenorMenor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbMayorMayor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbSeleccionEspec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbEspecialidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbEnfermeria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(420, 271);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 29);
            this.cbAceptar.TabIndex = 9;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(508, 272);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 29);
            this.cbCancelar.TabIndex = 10;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // frmFecha
            // 
            this.frmFecha.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmFecha.Controls.Add(this.sdcDesde);
            this.frmFecha.Controls.Add(this.sdcHasta);
            this.frmFecha.Controls.Add(this.lbFecHasta);
            this.frmFecha.Controls.Add(this.lbFecDesde);
            this.frmFecha.HeaderText = "Fecha de intervenci�n ";
            this.frmFecha.Location = new System.Drawing.Point(13, 215);
            this.frmFecha.Name = "frmFecha";
            this.frmFecha.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmFecha.Size = new System.Drawing.Size(173, 88);
            this.frmFecha.TabIndex = 12;
            this.frmFecha.Text = "Fecha de intervenci�n ";
            // 
            // sdcDesde
            // 
            this.sdcDesde.CustomFormat = "dd/MM/yyyy";
            this.sdcDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sdcDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcDesde.Location = new System.Drawing.Point(53, 28);
            this.sdcDesde.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.sdcDesde.Name = "sdcDesde";
            this.sdcDesde.Size = new System.Drawing.Size(109, 18);
            this.sdcDesde.TabIndex = 6;
            this.sdcDesde.TabStop = false;
            this.sdcDesde.Text = "25/05/2016";
            this.sdcDesde.Value = new System.DateTime(2016, 5, 25, 9, 49, 47, 935);
            // 
            // sdcHasta
            // 
            this.sdcHasta.CustomFormat = "dd/MM/yyyy";
            this.sdcHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sdcHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcHasta.Location = new System.Drawing.Point(52, 55);
            this.sdcHasta.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.sdcHasta.Name = "sdcHasta";
            this.sdcHasta.Size = new System.Drawing.Size(109, 18);
            this.sdcHasta.TabIndex = 7;
            this.sdcHasta.TabStop = false;
            this.sdcHasta.Text = "25/05/2016";
            this.sdcHasta.Value = new System.DateTime(2016, 5, 25, 9, 49, 47, 956);
            // 
            // lbFecHasta
            // 
            this.lbFecHasta.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFecHasta.Location = new System.Drawing.Point(14, 56);
            this.lbFecHasta.Name = "lbFecHasta";
            this.lbFecHasta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFecHasta.Size = new System.Drawing.Size(37, 18);
            this.lbFecHasta.TabIndex = 14;
            this.lbFecHasta.Text = "Hasta:";
            // 
            // lbFecDesde
            // 
            this.lbFecDesde.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbFecDesde.Location = new System.Drawing.Point(14, 29);
            this.lbFecDesde.Name = "lbFecDesde";
            this.lbFecDesde.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFecDesde.Size = new System.Drawing.Size(40, 18);
            this.lbFecDesde.TabIndex = 13;
            this.lbFecDesde.Text = "Desde:";
            // 
            // frmEspecialidad
            // 
            this.frmEspecialidad.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmEspecialidad.Controls.Add(this.cbMenorMenor);
            this.frmEspecialidad.Controls.Add(this.cbMenor);
            this.frmEspecialidad.Controls.Add(this.cbMayor);
            this.frmEspecialidad.Controls.Add(this.CbMayorMayor);
            this.frmEspecialidad.Controls.Add(this.lsbSeleccionEspec);
            this.frmEspecialidad.Controls.Add(this.lsbEspecialidades);
            this.frmEspecialidad.HeaderText = "Especialidades";
            this.frmEspecialidad.Location = new System.Drawing.Point(13, 14);
            this.frmEspecialidad.Name = "frmEspecialidad";
            this.frmEspecialidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmEspecialidad.Size = new System.Drawing.Size(576, 185);
            this.frmEspecialidad.TabIndex = 11;
            this.frmEspecialidad.Text = "Especialidades";
            // 
            // cbMenorMenor
            // 
            this.cbMenorMenor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMenorMenor.Enabled = false;
            this.cbMenorMenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMenorMenor.Location = new System.Drawing.Point(276, 152);
            this.cbMenorMenor.Name = "cbMenorMenor";
            this.cbMenorMenor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMenorMenor.Size = new System.Drawing.Size(26, 19);
            this.cbMenorMenor.TabIndex = 4;
            this.cbMenorMenor.Text = "<<";
            this.cbMenorMenor.Click += new System.EventHandler(this.cbMenorMenor_Click);
            // 
            // cbMenor
            // 
            this.cbMenor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMenor.Enabled = false;
            this.cbMenor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMenor.Location = new System.Drawing.Point(276, 109);
            this.cbMenor.Name = "cbMenor";
            this.cbMenor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMenor.Size = new System.Drawing.Size(26, 19);
            this.cbMenor.TabIndex = 3;
            this.cbMenor.Text = "<";
            this.cbMenor.Click += new System.EventHandler(this.cbMenor_Click);
            // 
            // cbMayor
            // 
            this.cbMayor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbMayor.Enabled = false;
            this.cbMayor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMayor.Location = new System.Drawing.Point(276, 66);
            this.cbMayor.Name = "cbMayor";
            this.cbMayor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMayor.Size = new System.Drawing.Size(26, 19);
            this.cbMayor.TabIndex = 2;
            this.cbMayor.Text = ">";
            this.cbMayor.Click += new System.EventHandler(this.cbMayor_Click);
            // 
            // CbMayorMayor
            // 
            this.CbMayorMayor.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbMayorMayor.Enabled = false;
            this.CbMayorMayor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbMayorMayor.Location = new System.Drawing.Point(276, 23);
            this.CbMayorMayor.Name = "CbMayorMayor";
            this.CbMayorMayor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbMayorMayor.Size = new System.Drawing.Size(26, 19);
            this.CbMayorMayor.TabIndex = 1;
            this.CbMayorMayor.Text = ">>";
            this.CbMayorMayor.Click += new System.EventHandler(this.CbMayorMayor_Click);
            // 
            // lsbSeleccionEspec
            // 
            this.lsbSeleccionEspec.Location = new System.Drawing.Point(312, 16);
            this.lsbSeleccionEspec.Name = "lsbSeleccionEspec";
            this.lsbSeleccionEspec.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lsbSeleccionEspec.Size = new System.Drawing.Size(257, 161);
            this.lsbSeleccionEspec.TabIndex = 5;
            this.lsbSeleccionEspec.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.lsbSeleccionEspec_Change);
            this.lsbSeleccionEspec.DoubleClick += new System.EventHandler(this.lsbSeleccionEspec_DoubleClick);
            // 
            // lsbEspecialidades
            // 
            this.lsbEspecialidades.Location = new System.Drawing.Point(8, 16);
            this.lsbEspecialidades.Name = "lsbEspecialidades";
            this.lsbEspecialidades.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lsbEspecialidades.Size = new System.Drawing.Size(257, 161);
            this.lsbEspecialidades.TabIndex = 0;
            this.lsbEspecialidades.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.lsbEspecialidades_Change);
            this.lsbEspecialidades.DoubleClick += new System.EventHandler(this.lsbEspecialidades_DoubleClick);
            // 
            // cbbEnfermeria
            // 
            this.cbbEnfermeria.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbbEnfermeria.ColumnWidths = "";
            this.cbbEnfermeria.Location = new System.Drawing.Point(200, 234);
            this.cbbEnfermeria.Name = "cbbEnfermeria";
            this.cbbEnfermeria.Size = new System.Drawing.Size(197, 20);
            this.cbbEnfermeria.TabIndex = 8;
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(200, 210);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(117, 18);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "Unidad de enfermer�a:";
            // 
            // QPQ260F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 316);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.frmFecha);
            this.Controls.Add(this.frmEspecialidad);
            this.Controls.Add(this.cbbEnfermeria);
            this.Controls.Add(this.Label4);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(120, 155);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QPQ260F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Selecci�n de parte programaci�n quir�rgica - QPQ260F1";
            this.Closed += new System.EventHandler(this.QPQ260F1_Closed);
            this.Load += new System.EventHandler(this.QPQ260F1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmFecha)).EndInit();
            this.frmFecha.ResumeLayout(false);
            this.frmFecha.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFecHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbFecDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmEspecialidad)).EndInit();
            this.frmEspecialidad.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbMenorMenor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMenor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMayor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbMayorMayor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbSeleccionEspec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsbEspecialidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbEnfermeria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        #endregion
    }
}