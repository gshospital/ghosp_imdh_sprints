using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Autorizaciones
{
	partial class Resultado
	{

		#region "Upgrade Support "
		private static Resultado m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Resultado DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Resultado();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "Line1", "Label2", "lbResultado", "ShapeContainer3", "Frame1" };
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel lbResultado;
        public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
        public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer3;
        public System.Windows.Forms.Panel Frame1;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.ShapeContainer3 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.lbResultado = new Telerik.WinControls.UI.RadLabel();
            this.Frame1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbResultado)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ShapeContainer3
            // 
            this.ShapeContainer3.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer3.Name = "ShapeContainer3";
            this.ShapeContainer3.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.Line1});
            this.ShapeContainer3.Size = new System.Drawing.Size(426, 62);
            this.ShapeContainer3.TabIndex = 2;
            this.ShapeContainer3.TabStop = false;
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.SystemColors.WindowText;
            this.Line1.Enabled = false;
            this.Line1.Name = "Line1";
            this.Line1.X1 = 2;
            this.Line1.X2 = 426;
            this.Line1.Y1 = 24;
            this.Line1.Y2 = 24;
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(8, 7);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(386, 15);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "RELACION DE CONCEPTOS - NUMEROS DE VOLANTES GENERADOS:";
            // 
            // lbResultado
            // 
            this.lbResultado.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbResultado.Location = new System.Drawing.Point(8, 34);
            this.lbResultado.Name = "lbResultado";
            this.lbResultado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbResultado.Size = new System.Drawing.Size(45, 17);
            this.lbResultado.TabIndex = 0;
            this.lbResultado.Text = "Label1";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.ShapeContainer3);
            this.Frame1.Location = new System.Drawing.Point(-4, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(432, 31);
            this.Frame1.TabIndex = 0;
            // 
            // Resultado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 59);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.lbResultado);
            this.Controls.Add(this.Frame1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Resultado";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resultado de la generación de Volantes automaticos";
            this.Activated += new System.EventHandler(this.Resultado_Activated);
            this.Closed += new System.EventHandler(this.Resultado_Closed);
            this.Load += new System.EventHandler(this.Resultado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbResultado)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion

      
    }
}