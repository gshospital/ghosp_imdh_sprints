using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Autorizaciones
{
	public partial class Actividad_Programada
		: RadForm
	{


		string Sql = String.Empty;
		DataSet rr = null;

		SqlConnection mcnConexion = null;
		int mSociedad = 0;
		string mInspeccion = String.Empty;
		int mServicio = 0;
		int mPersona = 0;
		string mTipoConc = String.Empty;
		string mConcepto = String.Empty;
		string mCantidad = String.Empty;
		string Musuario = String.Empty;
		string mIdenpac = String.Empty;

		string mfPrevAct = String.Empty;
		string mfRealiza = String.Empty;

		//variables que identifican el epsodio de actividad programada
		string miTiposer = String.Empty; //H, Q, C, U
		string mTipoActividad = String.Empty; //P(rogramacion)
		int mAnoprog = 0;
		int gNumprog = 0;

		string mSeparadorDecimal = String.Empty;

		string mNumeroDu = String.Empty;
		string mgsersoli = String.Empty;
		string mgpersoli = String.Empty;
		string mgdiagnos = String.Empty;
		string mTipoIngresoIntervencion = String.Empty;

		//Solicitudes
		const int COL_ANOSOL = 1;
		const int COL_NUMSOL = 2;
		const int COL_REGIMEN = 3;
		const int COL_CANTIDAD = 4;
		const int COL_FECHAPREVREAL = 5;
		const int COL_FECHASOL = 6;
		const int COL_FECHAINI = 7;
		const int COL_FECHARES = 8;
		const int COL_estado = 9;
		const int COL_VIA = 10;
		const int COL_OBSERVACIONES = 11;

		//Asociaciones
		const int COL_FECHAASOC = 1;
		const int COL_TIPOACTO = 2;
		const int COL_AREA = 3;
		const int COL_EPISODIO = 4;
		public Actividad_Programada()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void ProCargaCabecera()
		{

			string Sql = "SELECT " + " ISNULL(LTRIM(RTRIM(DPACIENT.DAPE1PAC)),'')+'  '+" + " ISNULL(LTRIM(RTRIM(DPACIENT.DAPE2PAC)),'')+', '+" + " ISNULL(LTRIM(RTRIM(DPACIENT.DNOMBPAC)),'') AS NOMBREPACIENTE, " + " HDOSSIER.GHISTORIA" + " FROM " + " DPACIENT " + " LEFT JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gIdenpac " + " WHERE " + " DPACIENT.gidenpac= '" + mIdenpac + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				lbPaciente.Text = Convert.ToString(rr.Tables[0].Rows[0]["NOMBREPACIENTE"]).Trim();
				if (Convert.ToString(rr.Tables[0].Rows[0]["GHISTORIA"]) != "")
				{
					lbHistoria.Text = (Convert.IsDBNull(rr.Tables[0].Rows[0]["ghistoria"])) ? "" : Convert.ToString(rr.Tables[0].Rows[0]["ghistoria"]);
				}
			}
			rr.Close();

			Sql = "SELECT DSOCIEDA FROM DSOCIEDA WHERE GSOCIEDA= " + mSociedad.ToString();
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, mcnConexion);
			rr = new DataSet();
			tempAdapter_2.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				LbFinanciadora.Text = Convert.ToString(rr.Tables[0].Rows[0]["DSOCIEDA"]);
			}
			rr.Close();

			if (mInspeccion.Trim() != "")
			{
				Sql = "SELECT DINSPECC FROM DINSPECC WHERE GINSPECC= '" + mInspeccion + "'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, mcnConexion);
				rr = new DataSet();
				tempAdapter_3.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					LbFinanciadora.Text = LbFinanciadora.Text + " - " + Convert.ToString(rr.Tables[0].Rows[0]["DINSPECC"]);
				}
				rr.Close();

			}

			Sql = "SELECT DNOMSERV FROM DSERVICI WHERE GSERVICI= " + mServicio.ToString();
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(Sql, mcnConexion);
			rr = new DataSet();
			tempAdapter_4.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				lbServicio.Text = Convert.ToString(rr.Tables[0].Rows[0]["DNOMSERV"]);
			}
			rr.Close();


			Sql = "SELECT ISNULL(LTRIM(RTRIM(DAP1PERS)),'')+' '+" + " ISNULL(LTRIM(RTRIM(DAP2PERS)),'')+','+" + " ISNULL(LTRIM(RTRIM(DNOMPERS)),'') FROM DPERSONA WHERE gPersona= " + mPersona.ToString();
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(Sql, mcnConexion);
			rr = new DataSet();
			tempAdapter_5.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				lbMedico.Text = Convert.ToString(rr.Tables[0].Rows[0][0]);
			}
			rr.Close();


			switch(mTipoConc)
			{
				case "PR" : 
					Sql = "SELECT DPRESTAC FROM DCODPRES WHERE GPRESTAC= '" + mConcepto + "'"; 
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(Sql, mcnConexion); 
					rr = new DataSet(); 
					tempAdapter_6.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lbConcepto.Text = Convert.ToString(rr.Tables[0].Rows[0]["DPRESTAC"]);
					} 
					rr.Close(); 
					break;
				case "ES" : 
					Sql = "SELECT DNOMSERV FROM DSERVICI WHERE GSERVICI= " + mConcepto; 
					SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(Sql, mcnConexion); 
					rr = new DataSet(); 
					tempAdapter_7.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lbConcepto.Text = "ESTANCIAS PARA " + Convert.ToString(rr.Tables[0].Rows[0]["DNOMSERV"]).Trim().ToUpper();
					} 
					rr.Close(); 
					break;
			}

			tbCantidad.Text = mCantidad;

		}

		public void Establecevariables(SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaCantidad, string ParamIdenpac, string ParafPrevact, string ParafRealiza, string ParaTipoActividad, int ParaAno, int ParaNum, string ParaUsuario, string NumeroDu = "", string gsersoli = "", string gpersoli = "", string gdiagnos = "", string TipoIngresoIntervencion = "", string ParaInspeccion = "")
		{

			mcnConexion = ParaConexion;

			mSociedad = ParaSociedad;
			mInspeccion = ParaInspeccion;
			mServicio = ParaServicio;
			mPersona = ParaPersona;
			mTipoConc = ParaTipoConc;
			mConcepto = ParaConcepto;
			mCantidad = ParaCantidad;

			Musuario = ParaUsuario;
			mIdenpac = ParamIdenpac;

			mfPrevAct = ParafPrevact;
			mfRealiza = ParafRealiza;

			//variables que identifican el episodio
			miTiposer = ParaItiposer;
			mTipoActividad = ParaTipoActividad;
			mAnoprog = ParaAno;
			gNumprog = ParaNum;

			mSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(mcnConexion);


			mNumeroDu = NumeroDu;
			mgsersoli = gsersoli;
			mgpersoli = gpersoli;
			mgdiagnos = gdiagnos;

			mTipoIngresoIntervencion = TipoIngresoIntervencion;
		}

		private void cbNuevaSolicitud_Click(Object eventSender, EventArgs eventArgs)
		{

			if (tbCantidad.Text == "")
			{
				string tempRefParam = Serrores.ObtenerNombreAplicacion(mcnConexion);
				string tempRefParam2 = "";
				short tempRefParam3 = 1040;
				string[] tempRefParam4 = new string[]{Label9.Text};
				Serrores.oClass.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, mcnConexion, tempRefParam4);
				if (tbCantidad.Enabled)
				{
					tbCantidad.Focus();
				}
			}

			//Dim NumeroDu As String
			//NumeroDu = DameNumeroDu(mcnConexion, mTipoActividad, miTiposer, mAnoprog, gNumprog)

			bAutorizaciones.gError = bAutorizaciones.bRegistraSolicitud(ref mcnConexion, mSociedad, miTiposer, mServicio, mPersona, mTipoConc, mConcepto, tbCantidad.Text, mIdenpac, ref mfPrevAct, ref mfRealiza, mTipoActividad, Musuario, ref bAutorizaciones.gANOAUT, ref bAutorizaciones.gNUMAUT, mNumeroDu, mgsersoli, mgpersoli, mgdiagnos, mTipoIngresoIntervencion, mInspeccion);

			if (!bAutorizaciones.gError)
			{
				bAutorizaciones.gError = bAutorizaciones.bAsociaSolicitud(mcnConexion, bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT, mTipoActividad, miTiposer, mAnoprog, gNumprog, ref mfRealiza, mTipoConc, mConcepto, tbCantidad.Text, Musuario);
			}

			this.Close();
		}

		private void cbAsociar_Click(Object eventSender, EventArgs eventArgs)
		{

			sprSolicitudes.Row = sprSolicitudes.ActiveRowIndex;
			sprSolicitudes.Col = COL_ANOSOL;
			int lanoau = Convert.ToInt32(Double.Parse(sprSolicitudes.Text));
			sprSolicitudes.Col = COL_NUMSOL;
			int lnumau = Convert.ToInt32(Double.Parse(sprSolicitudes.Text));
			sprSolicitudes.Col = COL_CANTIDAD;
			string lCantida = sprSolicitudes.Text;
			sprSolicitudes.Col = COL_FECHAPREVREAL;
			string fRealiza = sprSolicitudes.Text;

			bAutorizaciones.gError = bAutorizaciones.bAsociaSolicitud(mcnConexion, lanoau, lnumau, mTipoActividad, miTiposer, mAnoprog, gNumprog, ref mfRealiza, mTipoConc, mConcepto, lCantida, Musuario);

			this.Close();

		}



		private void Actividad_Programada_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				ProCargaCabecera();

				EstablecePropiedadesGrid();
				RellenaGridSolicitudes();

				cbAsociar.Enabled = false;
				if (mTipoConc == "PR")
				{
					tbCantidad.Enabled = false;
				}

				//sprSolicitudes.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect;
				//sprAsociaciones.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly;
				////UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.SelModeIndex was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				sprSolicitudes.setSelModeIndex(0);

			}
		}

		private void Actividad_Programada_Load(Object eventSender, EventArgs eventArgs)
		{

			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);

			Line5.BorderColor = ColorTranslator.FromOle(bAutorizaciones.COLOR_PENDIENTE);
			Line4.BorderColor = ColorTranslator.FromOle(bAutorizaciones.COLOR_SOLICITADA);
			Line6.BorderColor = ColorTranslator.FromOle(bAutorizaciones.COLOR_AUTORIZADA);

			bAutorizaciones.gError = false;
			bAutorizaciones.gANOAUT = 0;
			bAutorizaciones.gNUMAUT = 0;

            //sdsalazar 5_5
			//UPGRADE_ISSUE: (2064) Form property Actividad_Programada.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//this.setHelpContextID(142000);

		}

		private void sprSolicitudes_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			//int Col = eventArgs.Column;
			//int Row = eventArgs.Row;

            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;
            //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.SelModeIndex was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            sprSolicitudes.setSelModeIndex(Row);
			if (Row > 0)
			{
				cbAsociar.Enabled = true;
				RellenaGridAsociaciones();
			}
			else
			{
				cbAsociar.Enabled = false;
			}
		}

		private void tbCantidad_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			double dbNumericTemp = 0;
			if (!Double.TryParse(Strings.Chr(KeyAscii).ToString(), NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
			{
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void RellenaGridSolicitudes()
		{
			Color color = new Color();
			int iMaxFilas = 0;
			string stFiliacion = bAutorizaciones.fObtenerNafiliac(mcnConexion, mIdenpac, mSociedad, mInspeccion);

			Sql = "SELECT * FROM IFMSFA_AUTORIZA WHERE" + 
			      " gIdenpac ='" + mIdenpac + "' AND " + 
			      " gsocieda =" + mSociedad.ToString() + " AND " + 
			      " gregimen ='" + miTiposer + "' AND" + 
			      " gservici =" + mServicio.ToString() + " AND " + 
			      " (gpersona is null or gPersona =" + mPersona.ToString() + ") AND " + 
			      " rconc ='" + mTipoConc + "' AND " + 
			      " econc ='" + mConcepto + "' AND " + 
			      " gestsoli IN ('P','S','A')  AND fborrado is null ";

			//14/11/2008 A�ADIMOS LA CONDICION DE BUSQUEDA DE AUTORIZACION EL NAFILIAC
			if (stFiliacion.Trim() != "")
			{
				Sql = Sql + " AND (IFMSFA_AUTORIZA.nafiliac is null or IFMSFA_AUTORIZA.nafiliac='" + stFiliacion + "')";
			}

			if (mInspeccion.Trim() != "")
			{
				Sql = Sql + " AND IFMSFA_AUTORIZA.ginspecc='" + mInspeccion + "'";
			}

			Sql = Sql + " AND NOT EXISTS(SELECT ''  FROM  IFMSFA_ACTIVTAU " + 
			      "            WHERE  " + 
			      "                   IFMSFA_AUTORIZA.ganoauto = IFMSFA_ACTIVTAU.ganoauto and" + 
			      "                   IFMSFA_AUTORIZA.gnumauto = IFMSFA_ACTIVTAU.gnumauto )";


			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion);
			rr = new DataSet();
			tempAdapter.Fill(rr);

			if (rr.Tables[0].Rows.Count != 0)
			{
				rr.MoveLast(null);
				rr.MoveFirst();
				iMaxFilas = rr.Tables[0].Rows.Count;
				sprSolicitudes.MaxRows = iMaxFilas;
				for (int iFilas = 1; iFilas <= iMaxFilas; iFilas++)
				{
					sprSolicitudes.Row = iFilas;

					switch(Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]))
					{
						case "P" :  
							color = ColorTranslator.FromOle(bAutorizaciones.COLOR_PENDIENTE); 
							break;
						case "S" :  
							color = ColorTranslator.FromOle(bAutorizaciones.COLOR_SOLICITADA); 
							break;
						case "A" :  
							color = ColorTranslator.FromOle(bAutorizaciones.COLOR_AUTORIZADA); 
							break;
					}

					sprSolicitudes.Col = COL_ANOSOL;
					sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["ganoauto"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_NUMSOL;
					sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["gnumauto"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_REGIMEN;
					sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["gregimen"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_CANTIDAD;
					string tempRefParam = Convert.ToString(rr.Tables[0].Rows[0]["ncantida"]);
					sprSolicitudes.Text = "" + Serrores.ConvertirDecimales(ref tempRefParam, ref mSeparadorDecimal, 0);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_FECHAPREVREAL;
					sprSolicitudes.Text = "" + Convert.ToDateTime(rr.Tables[0].Rows[0]["fprevact"]).ToString("dd/MM/yyyy");
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_FECHASOL;
					sprSolicitudes.Text = "" + Convert.ToDateTime(rr.Tables[0].Rows[0]["fsoliaut"]).ToString("dd/MM/yyyy");
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_FECHAINI;
					sprSolicitudes.Text = "" + Convert.ToDateTime(rr.Tables[0].Rows[0]["ftramite"]).ToString("dd/MM/yyyy");
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_FECHARES;
					sprSolicitudes.Text = "" + Convert.ToDateTime(rr.Tables[0].Rows[0]["fresoluc"]).ToString("dd/MM/yyyy");
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_estado;
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_VIA;
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["gviasoli"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_OBSERVACIONES;
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]);
					sprSolicitudes.ForeColor = color;

					rr.MoveNext();
				}
			}
			rr.Close();

		}

		private void RellenaGridAsociaciones()
		{
			int iMaxFilas = 0;
			sprSolicitudes.Row = sprSolicitudes.ActiveRowIndex;

			sprSolicitudes.Col = 1;
			int lanoauto = Convert.ToInt32(Double.Parse(sprSolicitudes.Text));
			sprSolicitudes.Col = 2;
			int lnumauto = Convert.ToInt32(Double.Parse(sprSolicitudes.Text));

			sprAsociaciones.MaxRows = 0;

			Sql = "SELECT * FROM IFMSFA_ACTIVTAU WHERE " + 
			      " ganoauto =" + lanoauto.ToString() + " AND " + 
			      " gnumauto =" + lnumauto.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion);
			rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				rr.MoveLast(null);
				rr.MoveFirst();
				iMaxFilas = rr.Tables[0].Rows.Count;
				sprAsociaciones.MaxRows = iMaxFilas;
				for (int iFilas = 1; iFilas <= iMaxFilas; iFilas++)
				{
					sprAsociaciones.Row = iFilas;

					sprAsociaciones.Col = COL_FECHAASOC;
					sprAsociaciones.Text = "" + Convert.ToDateTime(rr.Tables[0].Rows[0]["fasociac"]).ToString("dd/MM/yyyy");
					sprAsociaciones.Col = COL_TIPOACTO;
					sprAsociaciones.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["itipacti"]);
					sprAsociaciones.Col = COL_AREA;
					sprAsociaciones.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["itiposer"]);
					sprAsociaciones.Col = COL_EPISODIO;
					sprAsociaciones.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["ganoregi"]) + "/" + Convert.ToString(rr.Tables[0].Rows[0]["gnumregi"]);

					rr.MoveNext();
				}
			}
			rr.Close();

		}

		private void EstablecePropiedadesGrid()
		{
			sprSolicitudes.MaxCols = 11;
			sprSolicitudes.MaxRows = 0;
			//cabecereas
			sprSolicitudes.SetColWidth(COL_ANOSOL, 4);
			sprSolicitudes.SetText(COL_ANOSOL, 0, "A�o");
			sprSolicitudes.SetColWidth(COL_NUMSOL, 16);
			sprSolicitudes.SetText(COL_NUMSOL, 0, "Numero");
			sprSolicitudes.SetColWidth(COL_REGIMEN, 6);
			sprSolicitudes.SetText(COL_REGIMEN, 0, "R�gimen");
			sprSolicitudes.SetColWidth(COL_CANTIDAD, 7);
			sprSolicitudes.SetText(COL_CANTIDAD, 0, "Cantidad Solicitada");
			sprSolicitudes.SetColWidth(COL_FECHAPREVREAL, 8);
			sprSolicitudes.SetText(COL_FECHAPREVREAL, 0, "Fecha Prev/Real");
			sprSolicitudes.SetColWidth(COL_FECHASOL, 8);
			sprSolicitudes.SetText(COL_FECHASOL, 0, "Fecha Solicitud");
			sprSolicitudes.SetColWidth(COL_FECHAINI, 8);
			sprSolicitudes.SetText(COL_FECHAINI, 0, "Fecha Inicio tr�mite");
			sprSolicitudes.SetColWidth(COL_FECHARES, 8);
			sprSolicitudes.SetText(COL_FECHARES, 0, "Fecha Resoluci�n");
			sprSolicitudes.SetColWidth(COL_estado, 5);
			sprSolicitudes.SetText(COL_estado, 0, "Estado");
			sprSolicitudes.SetColWidth(COL_VIA, 8);
			sprSolicitudes.SetText(COL_VIA, 0, "Via tr�mite");
			sprSolicitudes.SetColWidth(COL_OBSERVACIONES, 100);
			sprSolicitudes.SetText(COL_OBSERVACIONES, 0, "Observaciones");
			//sprSolicitudes.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect;

			sprAsociaciones.MaxCols = 4;
			sprAsociaciones.MaxRows = 0;
			sprAsociaciones.SetColWidth(COL_FECHAASOC, 8);
			sprAsociaciones.SetText(COL_FECHAASOC, 0, "Fecha Asociaci�n");
			sprAsociaciones.SetColWidth(COL_TIPOACTO, 5);
			sprAsociaciones.SetText(COL_TIPOACTO, 0, "Tipo Acto");
			sprAsociaciones.SetColWidth(COL_AREA, 5);
			sprAsociaciones.SetText(COL_AREA, 0, "�rea");
			sprAsociaciones.SetColWidth(COL_EPISODIO, 10);
			sprAsociaciones.SetText(COL_EPISODIO, 0, "Episodio");
			sprAsociaciones.Col = 4;
			sprAsociaciones.SetColHidden(sprAsociaciones.Col, false);
			//sprAsociaciones.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly;

		}
		private void Actividad_Programada_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}