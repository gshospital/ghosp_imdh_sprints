using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Autorizaciones
{
	partial class Actividad_Realizada
	{

		#region "Upgrade Support "
		private static Actividad_Realizada m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Actividad_Realizada DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Actividad_Realizada();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprSolicitudes", "Label10", "frmCancelando", "Timer1", "Label11", "lbPaciente", "Label28", "lbHistoria", "Frame2", "Picture1", "CmdReintentar", "Label9", "Timer", "Label7", "Label4", "Label5", "Label6", "FrmConexion", "Label8", "lbFecha", "Label3", "lbServicio", "LbFinanciadora", "Label14", "lbMedico", "Label2", "frasol", "pcBuscar", "cbCancelar", "cbAceptar", "labelDirectorio", "lbDirectorio", "Label1", "sprSolicitudes_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprSolicitudes;
		public Telerik.WinControls.UI.RadLabel Label10;
		public Telerik.WinControls.UI.RadGroupBox frmCancelando;
		public System.Windows.Forms.Timer Timer1;
		public Telerik.WinControls.UI.RadLabel Label11;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		public System.Windows.Forms.PictureBox Picture1;
		public Telerik.WinControls.UI.RadButton CmdReintentar;
		public Telerik.WinControls.UI.RadLabel Label9;
		public Telerik.WinControls.UI.RadLabel Timer;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadGroupBox FrmConexion;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadTextBox lbFecha;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadTextBox lbServicio;
		public Telerik.WinControls.UI.RadTextBox LbFinanciadora;
		public Telerik.WinControls.UI.RadLabel Label14;
		public Telerik.WinControls.UI.RadTextBox lbMedico;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox frasol;
		public System.Windows.Forms.PictureBox pcBuscar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadLabel labelDirectorio;
		public Telerik.WinControls.UI.RadTextBox lbDirectorio;
		public Telerik.WinControls.UI.RadLabel Label1;
		//private FarPoint.Win.Spread.SheetView sprSolicitudes_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Actividad_Realizada));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.sprSolicitudes = new UpgradeHelpers.Spread.FpSpread();
			this.frmCancelando = new Telerik.WinControls.UI.RadGroupBox();
			this.Label10 = new Telerik.WinControls.UI.RadLabel();
			this.Timer1 = new System.Windows.Forms.Timer(components);
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.Label11 = new Telerik.WinControls.UI.RadLabel();
			this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.frasol = new Telerik.WinControls.UI.RadGroupBox();
			this.FrmConexion = new Telerik.WinControls.UI.RadGroupBox();
			this.Picture1 = new System.Windows.Forms.PictureBox();
			this.CmdReintentar = new Telerik.WinControls.UI.RadButton();
			this.Label9 = new Telerik.WinControls.UI.RadLabel();
			this.Timer = new Telerik.WinControls.UI.RadLabel();
			this.Label7 = new Telerik.WinControls.UI.RadLabel();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.Label8 = new Telerik.WinControls.UI.RadLabel();
			this.lbFecha = new Telerik.WinControls.UI.RadTextBox();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.lbServicio = new Telerik.WinControls.UI.RadTextBox();
			this.LbFinanciadora = new Telerik.WinControls.UI.RadTextBox();
			this.Label14 = new Telerik.WinControls.UI.RadLabel();
			this.lbMedico = new Telerik.WinControls.UI.RadTextBox();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.pcBuscar = new System.Windows.Forms.PictureBox();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.labelDirectorio = new Telerik.WinControls.UI.RadLabel();
			this.lbDirectorio = new Telerik.WinControls.UI.RadTextBox();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.frmCancelando.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.frasol.SuspendLayout();
			this.FrmConexion.SuspendLayout();
			this.SuspendLayout();
			// 
			// sprSolicitudes
			// 
			this.sprSolicitudes.Location = new System.Drawing.Point(0, 176);
			this.sprSolicitudes.Name = "sprSolicitudes";
			this.sprSolicitudes.Size = new System.Drawing.Size(767, 259);
			this.sprSolicitudes.TabIndex = 0;
            //this.sprSolicitudes.ButtonClicked += new FarPoint.Win.Spread.EditorNotifyEventHandler(this.sprSolicitudes_ButtonClicked);
            this.sprSolicitudes.CommandCellClick += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.sprSolicitudes_ButtonClicked);
            this.sprSolicitudes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprSolicitudes_CellClick);
			this.sprSolicitudes.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprSolicitudes_Change);
			this.sprSolicitudes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sprSolicitudes_KeyPress);
            //this.sprSolicitudes.LeaveCell += new  UpgradeHelpers.Spread.FpSpread.LeaveCellEventHandler(this.sprSolicitudes_LeaveCell);
            this.sprSolicitudes.CurrentCellChanged += new Telerik.WinControls.UI.CurrentCellChangedEventHandler(this.sprSolicitudes_LeaveCell);
            this.sprSolicitudes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.sprSolicitudes_MouseDown);
            // 
            // frmCancelando
            // 
            //this.frmCancelando.BackColor = System.Drawing.SystemColors.Control;
			this.frmCancelando.Controls.Add(this.Label10);
			this.frmCancelando.Enabled = true;
			//this.frmCancelando.ForeColor = System.Drawing.SystemColors.ControlText;
			this.frmCancelando.Location = new System.Drawing.Point(760, 160);
			this.frmCancelando.Name = "frmCancelando";
			this.frmCancelando.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmCancelando.Size = new System.Drawing.Size(333, 117);
			this.frmCancelando.TabIndex = 28;
			this.frmCancelando.Visible = false;
			// 
			// Label10
			// 
			//this.Label10.BackColor = System.Drawing.SystemColors.Control;
			//this.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			//this.Label10.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label10.Location = new System.Drawing.Point(28, 44);
			this.Label10.Name = "Label10";
			this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label10.Size = new System.Drawing.Size(289, 53);
			this.Label10.TabIndex = 29;
			this.Label10.Text = "Cancelando transacción...";
			// 
			// Timer1
			// 
			this.Timer1.Enabled = false;
			this.Timer1.Interval = 1000;
			this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
			// 
			// Frame2
			// 
			//this.Frame2.BackColor = System.Drawing.SystemColors.Control;
			this.Frame2.Controls.Add(this.Label11);
			this.Frame2.Controls.Add(this.lbPaciente);
			this.Frame2.Controls.Add(this.Label28);
			this.Frame2.Controls.Add(this.lbHistoria);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(0, 0);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(769, 42);
			this.Frame2.TabIndex = 14;
			this.Frame2.Text = "Datos del Paciente";
			this.Frame2.Visible = true;
			// 
			// Label11
			// 
			//this.Label11.BackColor = System.Drawing.SystemColors.Control;
			//this.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label11.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label11.Location = new System.Drawing.Point(488, 16);
			this.Label11.Name = "Label11";
			this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label11.Size = new System.Drawing.Size(75, 17);
			this.Label11.TabIndex = 18;
			this.Label11.Text = "Historia clínica:";
			// 
			// lbPaciente
			// 
			//this.lbPaciente.BackColor = System.Drawing.SystemColors.Control;
			//this.lbPaciente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbPaciente.Location = new System.Drawing.Point(56, 16);
			this.lbPaciente.Name = "lbPaciente";
			this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbPaciente.Size = new System.Drawing.Size(423, 17);
			this.lbPaciente.TabIndex = 17;
            this.lbPaciente.Enabled = false;
			// 
			// Label28
			// 
			//this.Label28.BackColor = System.Drawing.SystemColors.Control;
			//this.Label28.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label28.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label28.Location = new System.Drawing.Point(8, 16);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(59, 17);
			this.Label28.TabIndex = 16;
			this.Label28.Text = "Paciente:";
			// 
			// lbHistoria
			// 
			//this.lbHistoria.BackColor = System.Drawing.SystemColors.Control;
			//this.lbHistoria.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbHistoria.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbHistoria.Location = new System.Drawing.Point(568, 16);
			this.lbHistoria.Name = "lbHistoria";
			this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbHistoria.Size = new System.Drawing.Size(97, 17);
			this.lbHistoria.TabIndex = 15;
            this.lbHistoria.Enabled = false;
            // 
            // frasol
            // 
            //this.frasol.BackColor = System.Drawing.SystemColors.Control;
			this.frasol.Controls.Add(this.FrmConexion);
			this.frasol.Controls.Add(this.Label8);
			this.frasol.Controls.Add(this.lbFecha);
			this.frasol.Controls.Add(this.Label3);
			this.frasol.Controls.Add(this.lbServicio);
			this.frasol.Controls.Add(this.LbFinanciadora);
			this.frasol.Controls.Add(this.Label14);
			this.frasol.Controls.Add(this.lbMedico);
			this.frasol.Controls.Add(this.Label2);
			this.frasol.Enabled = true;
			//this.frasol.ForeColor = System.Drawing.SystemColors.ControlText;
			this.frasol.Location = new System.Drawing.Point(0, 44);
			this.frasol.Name = "frasol";
			this.frasol.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frasol.Size = new System.Drawing.Size(769, 115);
			this.frasol.TabIndex = 5;
			this.frasol.Text = "Datos de la Solicitud";
			this.frasol.Visible = true;
			// 
			// FrmConexion
			// 
			//this.FrmConexion.BackColor = System.Drawing.SystemColors.Control;
			this.FrmConexion.Controls.Add(this.Picture1);
			this.FrmConexion.Controls.Add(this.CmdReintentar);
			this.FrmConexion.Controls.Add(this.Label9);
			this.FrmConexion.Controls.Add(this.Timer);
			this.FrmConexion.Controls.Add(this.Label7);
			this.FrmConexion.Controls.Add(this.Label4);
			this.FrmConexion.Controls.Add(this.Label5);
			this.FrmConexion.Controls.Add(this.Label6);
			this.FrmConexion.Enabled = true;
			this.FrmConexion.ForeColor = System.Drawing.SystemColors.ControlText;
			this.FrmConexion.Location = new System.Drawing.Point(464, 8);
			this.FrmConexion.Name = "FrmConexion";
			this.FrmConexion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmConexion.Size = new System.Drawing.Size(297, 97);
			this.FrmConexion.TabIndex = 19;
			this.FrmConexion.Text = "Conexión Autorizador";
			this.FrmConexion.Visible = false;
			// 
			// Picture1
			// 
			this.Picture1.BackColor = System.Drawing.SystemColors.Control;
			this.Picture1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Picture1.CausesValidation = true;
			this.Picture1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Picture1.Dock = System.Windows.Forms.DockStyle.None;
			this.Picture1.Enabled = true;
			this.Picture1.Image = (System.Drawing.Image) resources.GetObject("Picture1.Image");
			this.Picture1.Location = new System.Drawing.Point(128, 40);
			this.Picture1.Name = "Picture1";
			this.Picture1.Size = new System.Drawing.Size(41, 41);
			this.Picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this.Picture1.TabIndex = 27;
			this.Picture1.TabStop = true;
			this.Picture1.Visible = true;
			// 
			// CmdReintentar
			// 
			//this.CmdReintentar.BackColor = System.Drawing.SystemColors.Control;
			this.CmdReintentar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.CmdReintentar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.CmdReintentar.Location = new System.Drawing.Point(8, 24);
			this.CmdReintentar.Name = "CmdReintentar";
			this.CmdReintentar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CmdReintentar.Size = new System.Drawing.Size(113, 65);
			this.CmdReintentar.TabIndex = 20;
			this.CmdReintentar.Text = "Reintentar Conexión con el centro autorizador";
			this.CmdReintentar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.CmdReintentar.UseVisualStyleBackColor = false;
			this.CmdReintentar.Visible = false;
			this.CmdReintentar.Click += new System.EventHandler(this.CmdReintentar_Click);
            this.CmdReintentar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label9
			// 
			//this.Label9.BackColor = System.Drawing.SystemColors.Control;
			//this.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label9.ForeColor = System.Drawing.SystemColors.Highlight;
			this.Label9.Location = new System.Drawing.Point(176, 8);
			this.Label9.Name = "Label9";
			this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label9.Size = new System.Drawing.Size(49, 17);
			this.Label9.TabIndex = 26;
			this.Label9.Text = "Time Out";
			// 
			// Timer
			// 
			//this.Timer.BackColor = System.Drawing.SystemColors.Control;
			//this.Timer.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Timer.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Timer.ForeColor = System.Drawing.SystemColors.Highlight;
			this.Timer.Location = new System.Drawing.Point(248, 8);
			this.Timer.Name = "Timer";
			this.Timer.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Timer.Size = new System.Drawing.Size(25, 17);
			this.Timer.TabIndex = 25;
			this.Timer.Text = "0";
			// 
			// Label7
			// 
			//this.Label7.BackColor = System.Drawing.Color.White;
			//this.Label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label7.ForeColor = System.Drawing.Color.Black;
			this.Label7.Location = new System.Drawing.Point(176, 24);
			this.Label7.Name = "Label7";
			this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label7.Size = new System.Drawing.Size(113, 17);
			this.Label7.TabIndex = 24;
			this.Label7.Text = "    Manual";
			// 
			// Label4
			// 
			this.Label4.BackColor = System.Drawing.Color.FromArgb(0, 192, 0);
			//this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label4.ForeColor = System.Drawing.Color.Black;
			this.Label4.Location = new System.Drawing.Point(176, 40);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(113, 17);
			this.Label4.TabIndex = 23;
			this.Label4.Text = "    Autorizada";
			// 
			// Label5
			// 
			this.Label5.BackColor = System.Drawing.Color.FromArgb(255, 128, 0);
			//this.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label5.ForeColor = System.Drawing.Color.Black;
			this.Label5.Location = new System.Drawing.Point(176, 56);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(113, 17);
			this.Label5.TabIndex = 22;
			this.Label5.Text = "    Pendiente Autorizar";
			// 
			// Label6
			// 
			this.Label6.BackColor = System.Drawing.Color.Red;
			//this.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label6.ForeColor = System.Drawing.Color.Black;
			this.Label6.Location = new System.Drawing.Point(176, 72);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(113, 17);
			this.Label6.TabIndex = 21;
			this.Label6.Text = "    Denegada";
			// 
			// Label8
			// 
			//this.Label8.BackColor = System.Drawing.SystemColors.Control;
			//this.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label8.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label8.Location = new System.Drawing.Point(8, 88);
			this.Label8.Name = "Label8";
			this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label8.Size = new System.Drawing.Size(95, 13);
			this.Label8.TabIndex = 13;
			this.Label8.Text = "Fecha Prev / Real:";
			// 
			// lbFecha
			// 
			//this.lbFecha.BackColor = System.Drawing.SystemColors.Control;
			//this.lbFecha.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbFecha.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbFecha.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbFecha.Location = new System.Drawing.Point(120, 88);
			this.lbFecha.Name = "lbFecha";
			this.lbFecha.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbFecha.Size = new System.Drawing.Size(105, 19);
			this.lbFecha.TabIndex = 12;
            this.lbFecha.Enabled = false;
			// 
			// Label3
			// 
			//this.Label3.BackColor = System.Drawing.SystemColors.Control;
			//this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label3.Location = new System.Drawing.Point(8, 40);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(93, 13);
			this.Label3.TabIndex = 11;
			this.Label3.Text = "Servicio Solicitado:";
			// 
			// lbServicio
			// 
			//this.lbServicio.BackColor = System.Drawing.SystemColors.Control;
			//this.lbServicio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbServicio.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbServicio.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbServicio.Location = new System.Drawing.Point(120, 40);
			this.lbServicio.Name = "lbServicio";
			this.lbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbServicio.Size = new System.Drawing.Size(339, 19);
			this.lbServicio.TabIndex = 10;
            this.lbServicio.Enabled = false;
            // 
            // LbFinanciadora
            // 
            //this.LbFinanciadora.BackColor = System.Drawing.SystemColors.Control;
            //this.LbFinanciadora.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LbFinanciadora.Cursor = System.Windows.Forms.Cursors.Default;
			//this.LbFinanciadora.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LbFinanciadora.Location = new System.Drawing.Point(120, 18);
			this.LbFinanciadora.Name = "LbFinanciadora";
			this.LbFinanciadora.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbFinanciadora.Size = new System.Drawing.Size(339, 19);
			this.LbFinanciadora.TabIndex = 9;
            this.LbFinanciadora.Enabled = false; 
            // 
            // Label14
            // 
            //this.Label14.BackColor = System.Drawing.SystemColors.Control;
            //this.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label14.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label14.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label14.Location = new System.Drawing.Point(8, 20);
			this.Label14.Name = "Label14";
			this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label14.Size = new System.Drawing.Size(91, 17);
			this.Label14.TabIndex = 8;
			this.Label14.Text = "Ent. Financiadora:";
			// 
			// lbMedico
			// 
			//this.lbMedico.BackColor = System.Drawing.SystemColors.Control;
			//this.lbMedico.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbMedico.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbMedico.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbMedico.Location = new System.Drawing.Point(120, 64);
			this.lbMedico.Name = "lbMedico";
			this.lbMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbMedico.Size = new System.Drawing.Size(339, 19);
			this.lbMedico.TabIndex = 7;
            this.lbMedico.Enabled = false;
			// 
			// Label2
			// 
			//this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(8, 64);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(107, 13);
			this.Label2.TabIndex = 6;
			this.Label2.Text = "Profesional Solicitado:";
			// 
			// pcBuscar
			// 
			this.pcBuscar.BackColor = System.Drawing.SystemColors.Control;
			this.pcBuscar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pcBuscar.CausesValidation = true;
			this.pcBuscar.Cursor = System.Windows.Forms.Cursors.Default;
			this.pcBuscar.Dock = System.Windows.Forms.DockStyle.None;
			this.pcBuscar.Enabled = true;
			this.pcBuscar.Image = (System.Drawing.Image) resources.GetObject("pcBuscar.Image");
			this.pcBuscar.Location = new System.Drawing.Point(400, 448);
			this.pcBuscar.Name = "pcBuscar";
			this.pcBuscar.Size = new System.Drawing.Size(23, 19);
			this.pcBuscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
			this.pcBuscar.TabIndex = 4;
			this.pcBuscar.TabStop = true;
			this.pcBuscar.Visible = false;
			// 
			// cbCancelar
			// 
			//this.cbCancelar.BackColor = System.Drawing.SystemColors.Control;
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCancelar.Location = new System.Drawing.Point(644, 440);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(119, 29);
			this.cbCancelar.TabIndex = 2;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbCancelar.UseVisualStyleBackColor = false;
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            this.cbCancelar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cbAceptar
			// 
			//this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAceptar.Location = new System.Drawing.Point(520, 440);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(119, 29);
			this.cbAceptar.TabIndex = 1;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbAceptar.UseVisualStyleBackColor = false;
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            this.cbAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelDirectorio
            // 
            //this.labelDirectorio.BackColor = System.Drawing.SystemColors.Control;
            //this.labelDirectorio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.labelDirectorio.Cursor = System.Windows.Forms.Cursors.Default;
			//this.labelDirectorio.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelDirectorio.Location = new System.Drawing.Point(4, 456);
			this.labelDirectorio.Name = "labelDirectorio";
			this.labelDirectorio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.labelDirectorio.Size = new System.Drawing.Size(95, 13);
			this.labelDirectorio.TabIndex = 31;
			this.labelDirectorio.Text = "Directorio utilizado:";
			// 
			// lbDirectorio
			// 
			//this.lbDirectorio.BackColor = System.Drawing.SystemColors.Control;
			//this.lbDirectorio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbDirectorio.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbDirectorio.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbDirectorio.Location = new System.Drawing.Point(100, 456);
			this.lbDirectorio.Name = "lbDirectorio";
			this.lbDirectorio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbDirectorio.Size = new System.Drawing.Size(49, 17);
			this.lbDirectorio.TabIndex = 30;
            this.lbDirectorio.Enabled = false;
            // 
            // Label1
            // 
            //this.Label1.BackColor = System.Drawing.SystemColors.Control;
            //this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			//this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(4, 440);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(381, 17);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "Marque con * aquellos volantes que no tengan numeración previa";
			// 
			// Actividad_Realizada
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(771, 474);
			this.ControlBox = false;
			this.Controls.Add(this.sprSolicitudes);
			this.Controls.Add(this.frmCancelando);
			this.Controls.Add(this.Frame2);
			this.Controls.Add(this.frasol);
			this.Controls.Add(this.pcBuscar);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.labelDirectorio);
			this.Controls.Add(this.lbDirectorio);
			this.Controls.Add(this.Label1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Actividad_Realizada";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Documentación de Solicitudes de Autorización de Actividad Realizada - AUT002F1";
			this.Activated += new System.EventHandler(this.Actividad_Realizada_Activated);
			this.Closed += new System.EventHandler(this.Actividad_Realizada_Closed);
			this.frmCancelando.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.frasol.ResumeLayout(false);
			this.FrmConexion.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}