using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Microsoft.CSharp;
using Telerik.WinControls;
using Telerik.WinControls.UI;
namespace Autorizaciones
{
	public partial class Actividad_Realizada
		: RadForm
	{


		string mgidenpac = String.Empty;
		int mSociedad = 0;
		string mInspeccion = String.Empty;
		string miTiposer = String.Empty;
		string mNumDu = String.Empty;
		int mServicio = 0;
		string mTipoActividad = String.Empty;
		string RegimenAutorizador = String.Empty;
		int mPersona = 0;
		string mParaFechaPrev_Real = String.Empty;


		string mServicioSolicitante = String.Empty;
		string mPersonaSolicitante = String.Empty;
		string mDiagnostico = String.Empty;
		public string Musuario = String.Empty;

		dynamic mFormulario = null;
		SqlConnection mcnConexion = null;
		string[, ] mPrestaciones = null;

		//Identificador del episodio de la actividad programada si hubiera.
		//Ej. ganoprog/gnumprog de la consulta que viene de una cita programada.
		//    ganoregi/gnumregi del ingreso con una reserva de ingreso.
		int mAnoprog = 0;
		int mNumProg = 0;

		//Identificador del episodio de la actividad Realizada si hubiera (en el caso de nueva, llegara 0,0
		//porque tosavia no se ha generado la actividad pero en el caso de modificaciones, llegara el identificador
		//del episodio que se esta realizando).
		//Ej. ganoadme/gnumadme del episodio de admision.
		//    ganourge/gnumurge del episodio de urgencias
		//    ganoregi/gnumregi del episodio de consultas
		int mAnoregi = 0;
		int mNumregi = 0;


		//Area de control TEAM
		string mAreaT = String.Empty;
		string[, ] Peticiones = ArraysHelper.InitializeArray<string[, ]>(new int[]{201, 15}, new int[]{0, 0});

		bool bGT = false; //Gestion Team
		bool bGA = false; //Gestion Autorizaciones
		bool bFC = false; //Facturacion Copago

		bool bGI = false; // Gesti�n ITC

		bool bGTr = false; // Gesti�n Traditum

		bool bGB = false; // Gesti�n BlackBox

		string mTipoIngreso = String.Empty;

		string mSeparadorDecimal = String.Empty;

        int vlposy = 0;
        int Button = 0;
        // Objeto para las autorizaciones BlackBox de IDCSalud

        public BlackBoxWS.clsBlackBox oBlackBox = null;

		public Actividad_Realizada()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		public void Establecevariables(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, string ParaAreaT, string ParaTipoActividad, string ParaNumeroDu, string Paragsersoli, string Paragpersoli, string Paragdiagnos, string ParaUsuario, string paraTipoIngreso, string ParaInspeccion)
		{

			mcnConexion = ParaConexion;
			mFormulario = ParaFormu;

			mTipoActividad = ParaTipoActividad;
			mgidenpac = ParaGidenpac;
			mSociedad = ParaSociedad;
			mInspeccion = ParaInspeccion;
			miTiposer = ParaItiposer;
			mServicio = ParaServicio;
			mPersona = ParaPersona;
			mParaFechaPrev_Real = ParaFechaPrev_Real;
			mNumDu = ParaNumeroDu;

			mPrestaciones = ParaPrestaciones;

			mAnoprog = ParaAnoProg;
			mNumProg = ParaNumProg;

			mAnoregi = ParaAnoRegi;
			mNumregi = ParaNumRegi;
			mAreaT = ParaAreaT;

			mServicioSolicitante = Paragsersoli;
			mPersonaSolicitante = Paragpersoli;
			mDiagnostico = Paragdiagnos;
			Musuario = ParaUsuario.ToUpper();
			mTipoIngreso = paraTipoIngreso;

			mSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(mcnConexion);
		}


		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			string[, ] ArrayinfSolicitudes = null;
			string idSolicitud = String.Empty;
			string iTipoConc = String.Empty;
			string stConcepto = String.Empty;
			string stCantidad = String.Empty;
			string stNAutoriz = String.Empty;
			string stNVolante = String.Empty;
			StringBuilder stNOpelec = new StringBuilder();
			string iNecesita = String.Empty;
			string iautoe = String.Empty;
			string estado = String.Empty;
			string StOperacion = String.Empty;
			string Fallo = String.Empty;
			string FechaTransaccion = String.Empty;
			string FechaSesion = String.Empty;
			string FechaSesionAux = String.Empty;
			string FechaTransaccionAux = String.Empty;
			string stCopago = String.Empty;

			string stSql = String.Empty;
			DataSet RrSql = null;
			bool BEncontrada = false;
			string idOpOriginal = String.Empty;
			bool BAnularEpi = false;

			this.Cursor = Cursors.WaitCursor;

			ArrayinfSolicitudes = ArraysHelper.InitializeArray<string[, ]>(new int[]{sprSolicitudes.MaxRows + 1, 15}, new int[]{0, 0}); //13

			if (!ValidaDatos())
			{
				this.Cursor = Cursors.Default;
				return;
			}


			bAutorizaciones.gContinuarRecogiendoInf = true;

			int tempForVar = sprSolicitudes.MaxRows;
			for (int i = 1; i <= tempForVar; i++)
			{
				Application.DoEvents();

				sprSolicitudes.Row = i;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
				idSolicitud = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_TIPOCONC;
				iTipoConc = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_CONCEPTO;
				stConcepto = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
				stCantidad = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_IDNECESITA;
				iNecesita = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
				stNAutoriz = sprSolicitudes.Text;


				sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
				stNVolante = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
				stNOpelec = new StringBuilder(Convert.ToString(sprSolicitudes.Value));

				sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
				StOperacion = sprSolicitudes.Text;
				stNOpelec.Append("*" + sprSolicitudes.Text);

				sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
				iautoe = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
				estado = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
				Fallo = sprSolicitudes.Text;

				FechaSesionAux = Peticiones[i, 9];
				//fecha hora operacion original
				FechaTransaccionAux = Peticiones[i, 10];

				//Oscar C Agosto 2011. Copago
				sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
				stCopago = sprSolicitudes.Text;

				double dbNumericTemp = 0;
				if (Double.TryParse(stCopago, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
				{
					if (StringsHelper.ToDoubleSafe(stCopago) == 0)
					{
						stCopago = "";
					}
				}
				else
				{
					stCopago = "";
				}
				//---------------

				ArrayinfSolicitudes[i, 0] = idSolicitud;
				ArrayinfSolicitudes[i, 1] = iTipoConc;
				ArrayinfSolicitudes[i, 2] = stConcepto;
				ArrayinfSolicitudes[i, 3] = stCantidad;
				ArrayinfSolicitudes[i, 4] = iNecesita;
				ArrayinfSolicitudes[i, 5] = stNAutoriz;
				ArrayinfSolicitudes[i, 6] = stNVolante;
				ArrayinfSolicitudes[i, 7] = stNOpelec.ToString();
				ArrayinfSolicitudes[i, 8] = iautoe;
				ArrayinfSolicitudes[i, 9] = estado;
				ArrayinfSolicitudes[i, 10] = StOperacion;
				ArrayinfSolicitudes[i, 11] = Fallo;
				ArrayinfSolicitudes[i, 12] = FechaSesionAux;
				ArrayinfSolicitudes[i, 13] = FechaTransaccionAux;

				//Oscar C Agosto 2011. Copago
				ArrayinfSolicitudes[i, 14] = stCopago;
				//--------------------------

			}

			this.Cursor = Cursors.Default;

            mFormulario.RecogeDatosSolicitudes(ArrayinfSolicitudes);
			this.Close();

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{

			bool lError = false;
			string idOpOriginal = String.Empty;
			string StSolicitud = String.Empty;
			string StSiAutorizada = String.Empty;

			this.Cursor = Cursors.WaitCursor;



			//OSCAR C DICIEMBRE 2008
			//Si mAnoregi y mNumregi tienen valores significaria que hemos accedidoa a la pantalla de actividad realizada
			//desde la modificacion de una actividad realizada.
			//Sin embargo hemos descubierto un "agujero" en la gestion de autorizaciones de modificaciones de actividad realizada
			//ya que antes de entrar en esta pantalla cuando se esta modificando una actividad, se desasocia la actividad realizada
			//de la autorizacion correspondiente por lo que si se pulsa cancelar de la pantalla de documentacion de solicitudes de
			//autorizacion de actividad realizada y despues se pulsa cancelar en la pantalla de origen de la modificacion de actividad
			//realizada, se queda dicha actividad sin asociar a ninguna autorizacion
			//''If mAnoregi > 0 And mNumregi > 0 Then
			//''    proAsociaSolicitudXGrid_Cancelar
			//''Else

			Actividad_Realizada.DefInstance.Enabled = false;
			cbAceptar.Enabled = false;
			cbCancelar.Enabled = false;
			bool bHuboCancelaciones = false;

			// Si hubo alguna de ITC que est� autorizada, tenemos que cancelarla

			int tempForVar = sprSolicitudes.MaxRows;
			for (int i = 1; i <= tempForVar; i++)
			{

				sprSolicitudes.Row = i;

				// Vemos si es de ITC y est� autorizada

				sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
				StSolicitud = sprSolicitudes.Text;
				sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
				StSiAutorizada = sprSolicitudes.Text;


				if (StSolicitud == "I" && ColorTranslator.ToOle(sprSolicitudes.BackColor) == 0xC000 && StSiAutorizada == "S")
				{

					// Mostramos el frame de cancelaci�n en el centro

					frmCancelando.Left = 212;
					frmCancelando.Top = 160;
					frmCancelando.Visible = true;
					frmCancelando.BringToFront();

					// Nos posicionamos en el n�mero de autorizaci�n

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE; //AR_COL_NAUTORIZ

					// La cancelamos

					string tempRefParam = "" + mPrestaciones[i, 2];
					bAutArgentina.fCancelaAutorizacionITC(mcnConexion, mSociedad, mgidenpac, ref tempRefParam, Convert.ToInt32(Double.Parse(mPrestaciones[i, 3])), miTiposer, mPrestaciones[i, 1] == "ES", sprSolicitudes.Text.Trim().Substring(sprSolicitudes.Text.Trim().Length - 6, Math.Min(6, 6)));

					bHuboCancelaciones = true;

					//''    End If

				}
				else if (StSolicitud == "T" && ColorTranslator.ToOle(sprSolicitudes.BackColor) == 0xC000 && StSiAutorizada == "S")
				{ 

					// Mostramos el frame de cancelaci�n en el centro

					frmCancelando.Left = 212;
					frmCancelando.Top = 160;
					frmCancelando.Visible = true;
					frmCancelando.BringToFront();

					// Nos posicionamos en el n�mero de autorizaci�n

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE; //AR_COL_NAUTORIZ

					// La cancelamos

					string tempRefParam2 = "" + mPrestaciones[i, 2];
					bAutArgentina.fCancelaAutorizacionTraditum(mcnConexion, mSociedad, mgidenpac, ref tempRefParam2, Convert.ToInt32(Double.Parse(mPrestaciones[i, 3])), miTiposer, mServicio, mPrestaciones[i, 1] == "ES", sprSolicitudes.Text.Trim());

					bHuboCancelaciones = true;

					//''       End If

				}
				else if (StSolicitud == "B" && ColorTranslator.ToOle(sprSolicitudes.BackColor) == 0xC000 && StSiAutorizada == "S")
				{ 
					frmCancelando.Left = 212;
					frmCancelando.Top = 160;
					frmCancelando.Visible = true;
					frmCancelando.BringToFront();

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE; //AR_COL_NAUTORIZ
					idOpOriginal = this.sprSolicitudes.Text;
					// La cancelamos
					string tempRefParam3 = idOpOriginal.Trim();
					bAutorizaciones.fCancelaAutorizacionBlackBox(ref mcnConexion, ref tempRefParam3, ref mgidenpac);

					bHuboCancelaciones = true;
				}
				else
				{
					if (mAnoregi > 0 && mNumregi > 0)
					{
						proAsociaSolicitudXGrid_Cancelar(sprSolicitudes.Row);
					}
				}

			}

			// Ocultamos el frame de cancelaci�n

			frmCancelando.Visible = false;

			// Si hubo cancelaciones, mostramos un mensaje

			if (bHuboCancelaciones)
			{

				MessageBox.Show("Transacci�n cancelada", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

			}

			cbAceptar.Enabled = true;
			cbCancelar.Enabled = true;
			Actividad_Realizada.DefInstance.Enabled = true;

			//''End If

			//Cuidado con la variable del objeto llamador porque no se vacia y de una ejecucion a otra podria
			//tener guarreria.
			string[, ] ArrayinfSolicitudes = ArraysHelper.InitializeArray<string[, ]>(new int[]{2, 15}, new int[]{0, 0}); //13

            if (ArrayinfSolicitudes != null)
			{
				Array.Clear(ArrayinfSolicitudes, 0, ArrayinfSolicitudes.Length);
			}
			ArrayinfSolicitudes[1, 0] = "NOTHING";

            mFormulario.RecogeDatosSolicitudes(ArrayinfSolicitudes);

			bAutorizaciones.gContinuarRecogiendoInf = false;
			this.Cursor = Cursors.Default;
			this.Close();

		}

		private void proAsociaSolicitudXGrid_Cancelar(int iRow)
		{

			int AnoAuto = 0;
			int NumAuto = 0;
			string stTipoConc = String.Empty;
			string stConcepto = String.Empty;
			string stCantidad = String.Empty;

			//'    For i = 1 To sprSolicitudes.MaxRows
			sprSolicitudes.Row = iRow;
			sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;

			if (sprSolicitudes.Text.IndexOf('/') >= 0)
			{

				AnoAuto = Convert.ToInt32(Double.Parse(sprSolicitudes.Text.Substring(0, Math.Min(4, sprSolicitudes.Text.Length))));
				NumAuto = Convert.ToInt32(Double.Parse(sprSolicitudes.Text.Substring(5)));

				sprSolicitudes.Col = bAutorizaciones.AR_COL_TIPOCONC;
				stTipoConc = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_CONCEPTO;
				stConcepto = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
				stCantidad = sprSolicitudes.Text;

				bAutorizaciones.bAsociaSolicitud(mcnConexion, AnoAuto, NumAuto, mTipoActividad, miTiposer, mAnoregi, mNumregi, ref mParaFechaPrev_Real, stTipoConc, stConcepto, stCantidad, Musuario);
			}
			//'    Next

		}


		private void ProCargaCabecera()
		{
			string Sql = String.Empty;
			DataSet rr = null;
			try
			{
				Sql = "SELECT valfanu1 , valfanu2,valfanu3 FROM SCONSGLO WHERE GCONSGLO='MIDDLEWA'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion);
				rr = new DataSet();
				tempAdapter.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					Picture1.Visible = Convert.ToString(rr.Tables[0].Rows[0][0]).Trim().ToUpper() == "HTTPS://";
					rr.Close();
				}


				Sql = "SELECT " + " ISNULL(LTRIM(RTRIM(DPACIENT.DAPE1PAC)),'')+'  '+" + " ISNULL(LTRIM(RTRIM(DPACIENT.DAPE2PAC)),'')+', '+" + " ISNULL(LTRIM(RTRIM(DPACIENT.DNOMBPAC)),'') AS NOMBREPACIENTE, " + " HDOSSIER.GHISTORIA" + " FROM " + " DPACIENT " + " LEFT JOIN HDOSSIER ON DPACIENT.GIDENPAC = HDOSSIER.GIDENPAC " + " WHERE " + " DPACIENT.GIDENPAC= '" + mgidenpac + "'";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, mcnConexion);
				rr = new DataSet();
				tempAdapter_2.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					lbPaciente.Text = Convert.ToString(rr.Tables[0].Rows[0]["NOMBREPACIENTE"]).Trim();
					if (Convert.ToString(rr.Tables[0].Rows[0]["GHISTORIA"]) != "")
					{
						lbHistoria.Text = (Convert.IsDBNull(rr.Tables[0].Rows[0]["ghistoria"])) ? "" : Convert.ToString(rr.Tables[0].Rows[0]["ghistoria"]);
					}
				}
				rr.Close();

				Sql = "SELECT DSOCIEDA FROM DSOCIEDA WHERE GSOCIEDA= " + mSociedad.ToString();
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, mcnConexion);
				rr = new DataSet();
				tempAdapter_3.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					LbFinanciadora.Text = Convert.ToString(rr.Tables[0].Rows[0]["DSOCIEDA"]);
				}
				rr.Close();

				if (mInspeccion.Trim() != "")
				{
					Sql = "SELECT DINSPECC FROM DINSPECC WHERE GINSPECC='" + mInspeccion + "'";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(Sql, mcnConexion);
					rr = new DataSet();
					tempAdapter_4.Fill(rr);
					if (rr.Tables[0].Rows.Count != 0)
					{
						LbFinanciadora.Text = LbFinanciadora.Text + " - " + Convert.ToString(rr.Tables[0].Rows[0]["DINSPECC"]);
					}
					rr.Close();
				}

				Sql = "SELECT DNOMSERV FROM DSERVICI WHERE GSERVICI= " + mServicio.ToString();
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(Sql, mcnConexion);
				rr = new DataSet();
				tempAdapter_5.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					lbServicio.Text = Convert.ToString(rr.Tables[0].Rows[0]["DNOMSERV"]);
				}
				rr.Close();


				Sql = "SELECT ISNULL(LTRIM(RTRIM(DAP1PERS)),'')+' '+" + " ISNULL(LTRIM(RTRIM(DAP2PERS)),'')+','+" + " ISNULL(LTRIM(RTRIM(DNOMPERS)),'') FROM DPERSONA WHERE GPERSONA= " + mPersona.ToString();
				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(Sql, mcnConexion);
				rr = new DataSet();
				tempAdapter_6.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					lbMedico.Text = Convert.ToString(rr.Tables[0].Rows[0][0]);
				}
				rr.Close();

				lbFecha.Text = mParaFechaPrev_Real;
			}
			catch(Exception Excep)
			{
				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), "indra", "Actividad_Realizada: Form_Load: ProCargaCabecera (" + Sql + ")", Excep);
			}
		}

		private void EstablecePropiedadesGrid()
		{
			sprSolicitudes.MaxCols = 18; //17 '16
			sprSolicitudes.MaxRows = 0;
			//cabeceras
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_IDSOLICITUD, 10);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_IDSOLICITUD, 0, "N�Solicitud");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_CBBUSCAR, 4);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_CBBUSCAR, 0, "...");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_DCONCEPTO, 18);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_DCONCEPTO, 0, "Concepto");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_CANTIDAD, 5);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_CANTIDAD, 0, "Cant Sol");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_NAUTORIZ, 12);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_NAUTORIZ, 0, "N�mero de Autorizaci�n");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_NVOLANTE, 16);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_NVOLANTE, 0, "N�mero de Volante");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_NOPERELE, 10);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_NOPERELE, 0, "N�mero de Op. Elec.");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_IDNECESITA, 4);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_IDNECESITA, 0, "Nec.");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_IDPERMITE, 4);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_IDPERMITE, 0, "Cont.");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_TIPOCONC, 4);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_TIPOCONC, 0, "TCONC");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_CONCEPTO, 4);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_CONCEPTO, 0, "CONC");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_TIM, 5);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_TIM, 0, "TEAM");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_iautoe, 4);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_iautoe, 0, "Aut");
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_estado, 4);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_estado, 0, "Est.");

			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 0);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_OBSERV, 0, "Observaciones.");

			//Oscar C Agosto 2011. Copago.
			sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_COPAGO, 8);
			sprSolicitudes.SetText(bAutorizaciones.AR_COL_COPAGO, 0, "Copago");
			//--------------------

			//David. Columna de ya autorizada
			sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
			sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
			//--------------------

            //sdsalazar
			//sprSolicitudes.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.Normal;

			if (bGT && !bGA)
			{
				sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_DCONCEPTO, 70);
				sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_NOPERELE, 20);
			}

			if (!bGT && bGA)
			{
				if (bFC)
				{
					sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_DCONCEPTO, 20);
				}
				else
				{
					sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_DCONCEPTO, 35);
				}
			}

		}

		private void RellenaGridSolicitudes()
		{


			int lservicioprestacion = 0;
			StringBuilder Sql = new StringBuilder();
			DataSet rr = null;
			DataSet rr1 = null;

			//Para la Realizacion electronica

			string dnautori = String.Empty;
			string dnvolant = String.Empty;
			string numoper = String.Empty;
			string dnopelec = String.Empty;
			string tarjeta = String.Empty;
			string codProf = String.Empty;
			string nifProf = String.Empty;
			string numcole = String.Empty;
			string CodDiag = String.Empty;
			string codEntidad = String.Empty;
			string codUsuario = String.Empty;
			string claveOperativa = String.Empty;
			string FechaTransaccion = String.Empty;
			string FechaSesion = String.Empty;
			string Version = String.Empty;
			string Provincia = String.Empty;
			string FechaTransaccionAux = String.Empty;
			string FechaSesionAux = String.Empty;
			string NumDu = String.Empty;
			string ParaTipoActividad = String.Empty;
			XmlDocument RespXML = null;
			//Set RespXML = New DOMDocument
			// Para la realizacion  electronica
			string dnautoriAUX = String.Empty;
			string dnvolantAUX = String.Empty;
			string dnopelecAUX = String.Empty;
			string dnopelecAUXDEPITEAM = String.Empty;
			// Indica si nos conectamos con sanitas, es decir se ha asociado la autorizacion, con el servicio
			bool Pedir = false;
			string Respuesta = String.Empty;
			string Fallo = String.Empty;
			//Dim ServicioSolicitante As String
			//
			string CodInfra = String.Empty;
			int cantidad = 0;
			string lanoauto = String.Empty;
			string lnumauto = String.Empty;
			System.DateTime Prevact = DateTime.FromOADate(0);
			string RegistroDatos = String.Empty;
			string Obsolici = String.Empty;
			//Variable que indica si debemos solicitar el siguiente numero para IFMSFA_AUTORIZA
			//para mandarlo al autorizador, como operacion en origen.
			Numeracion.SIGUIENTE lobjSiguiente = null;
			System.DateTime FechaTimeOut = DateTime.FromOADate(0);



			string stFiliacion = String.Empty;


			string Oper = "FAAU9";
			string nOper = "000000";
			string tmov = "AUTORIZA";

			int X = 0;

			bool CogePKElectronica = false;

			try
			{

				sprSolicitudes.MaxRows = 0;

				//For i = 0 To UBound(mPrestaciones)
				int CantidadAutorizador = 0;
				string CodAcc = String.Empty;
				for (int i = 1; i <= mPrestaciones.GetUpperBound(0); i++)
				{

					//casos particulares en los que NO es necesario mostrar registro
					//1. Otras Prestaciones de Quirofanos que no sean de servicios quirurgicos
					if (bGA && miTiposer == "Q" && mPrestaciones[i, 1] == "PR" && !bAutorizaciones.PrestacionQuirurgica(mcnConexion, mPrestaciones[i, 2]) && bAutorizaciones.bAutorizacionElectronica(mcnConexion, mSociedad, miTiposer, lservicioprestacion, mPersona, mPrestaciones[i, 1], mPrestaciones[i, 2], mInspeccion))
					{

					}
					else
					{

						stFiliacion = bAutorizaciones.fObtenerNafiliac(mcnConexion, mgidenpac, mSociedad, mInspeccion);

						//fin casos

						Application.DoEvents();
						Application.DoEvents();
						Application.DoEvents();
						Application.DoEvents();
						Application.DoEvents();
						this.Refresh();
						Obsolici = "";
						sprSolicitudes.MaxRows++;
						Timer.Text = "0";
						FechaTimeOut = DateTime.Now;

						//RESETEAMOS VALORES

						lanoauto = "";
						lnumauto = "";
						dnautoriAUX = "";
						dnvolantAUX = "";
						dnopelecAUX = "";
						lanoauto = "";
						lnumauto = "";
						dnautori = "";
						dnopelec = "";
						dnvolant = "";
						RegistroDatos = "";
						FechaTransaccionAux = "";
						FechaSesionAux = "";
						cantidad = 1;
						dnautoriAUX = "";
						dnvolantAUX = "";
						dnopelecAUX = "";
						dnopelecAUXDEPITEAM = "";
						dnautori = "";
						dnopelec = "";
						dnvolant = "";
						numoper = "";
						FechaTransaccionAux = "";
						FechaSesionAux = "";
						RegistroDatos = "";
						Fallo = "";
						NumDu = "";
						Respuesta = "";


						sprSolicitudes.Row = sprSolicitudes.MaxRows; //i '+ 1
						sprSolicitudes.SetRowHeight(sprSolicitudes.Row, 15);

						//Id Solicitud (A�o / Numero)

						sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTELECT;
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

						sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
						sprSolicitudes.Text = "";
						sprSolicitudes.Lock = true;
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

						//Boton Para Seleccionar Solicitud
						sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
						sprSolicitudes.CellType = 7;
						sprSolicitudes.TypeButtonText = "";
                        
                        //sdsalazar
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeButtonPicture was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        //sprSolicitudes.setTypeButtonPicture(pcBuscar.Image);
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

						//Concepto
						sprSolicitudes.Col = bAutorizaciones.AR_COL_DCONCEPTO;
						switch(mPrestaciones[i, 1])
						{
							case "ES" : 
								Sql = new StringBuilder("SELECT gservici, DNOMSERV FROM DSERVICI WHERE GSERVICI= " + mPrestaciones[i, 2]); 
								SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql.ToString(), mcnConexion); 
								rr = new DataSet(); 
								tempAdapter.Fill(rr); 
								if (rr.Tables[0].Rows.Count != 0)
								{
									sprSolicitudes.Text = "ESTANCIAS PARA " + Convert.ToString(rr.Tables[0].Rows[0]["dnomserv"]).Trim().ToUpper();
									lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
								}
								else
								{
									sprSolicitudes.Text = "ESTANCIAS";
								} 
								rr.Close(); 
								break;
							case "PR" : 
								Sql = new StringBuilder("SELECT gservici, DPRESTAC FROM DCODPRES WHERE GPRESTAC= '" + mPrestaciones[i, 2] + "'"); 
								SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql.ToString(), mcnConexion); 
								rr = new DataSet(); 
								tempAdapter_2.Fill(rr); 
								if (rr.Tables[0].Rows.Count != 0)
								{
									sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["dprestac"]).Trim().ToUpper();
									lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
								} 
								rr.Close(); 
								break;
						}
						sprSolicitudes.Lock = true;

						//Cantidad
						sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
						sprSolicitudes.CellType =  3;
						sprSolicitudes.Text = mPrestaciones[i, 3];
						switch(mPrestaciones[i, 1])
						{
							case "ES" :  
								sprSolicitudes.Lock = false; 
								break;
							case "PR" :  
								sprSolicitudes.Lock = true; 
								break;
						}
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
						cantidad = Convert.ToInt32(Double.Parse(mPrestaciones[i, 3]));

						//Numero de autorizacion
						sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
						sprSolicitudes.Text = "";
						sprSolicitudes.Lock = false;
						//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						sprSolicitudes.setTypeEditLen(30); //20
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

						//Numero de volante
						sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
						// SI nos han pasado un volante es el de la prestacion principal.
						if (i == 1 && mNumDu != "")
						{
							sprSolicitudes.Text = mNumDu;
							NumDu = mNumDu;
						}
						else
						{
							sprSolicitudes.Text = "";
							NumDu = "";
						}
						sprSolicitudes.Lock = false;
						//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						sprSolicitudes.setTypeEditLen(30); //20
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

						//Indicador de TEAM
						sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
						sprSolicitudes.Lock = false;
						sprSolicitudes.CellType = 10;
						//sdsalazar
                        //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeCheckText was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						//sprSolicitudes.setTypeCheckText("");
						sprSolicitudes.TypeCheckCenter = true;
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGT);

						//Numero de operacion
						sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
						sprSolicitudes.Text = "";
						sprSolicitudes.Lock = false;
						//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						sprSolicitudes.setTypeEditLen(30); //20
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGT && !bGI && !bGB);

						//Puede ser electronica
						sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
						sprSolicitudes.Text = "N";
						sprSolicitudes.Lock = true;
						//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						sprSolicitudes.setTypeEditLen(20);
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

						//Estado
						sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
						sprSolicitudes.Text = "P";
						sprSolicitudes.Lock = true;
						//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						sprSolicitudes.setTypeEditLen(20);
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

						//Necesita..
						sprSolicitudes.Col = bAutorizaciones.AR_COL_IDNECESITA;
						if (bGA)
						{
							if (bAutorizaciones.bNecesitaAutorizacion(mcnConexion, mSociedad, miTiposer, lservicioprestacion, mPersona, mPrestaciones[i, 1], mPrestaciones[i, 2], mInspeccion))
							{
								sprSolicitudes.Text = "A";
							}
							else if (bAutorizaciones.bNecesitaVolante(mcnConexion, mSociedad, miTiposer, lservicioprestacion, mPersona, mPrestaciones[i, 1], mPrestaciones[i, 2], mInspeccion))
							{ 
								sprSolicitudes.Text = "V";
							}
							else
							{
								sprSolicitudes.Text = "N";
							}
						}
						sprSolicitudes.Lock = true;
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

						//Permite realizar prueba
						sprSolicitudes.Col = bAutorizaciones.AR_COL_IDPERMITE;
						if (bGA)
						{
							if (bAutorizaciones.bPermitirPrueba(mcnConexion, mSociedad, miTiposer, lservicioprestacion, mPersona, mPrestaciones[i, 1], mPrestaciones[i, 2], mInspeccion))
							{
								sprSolicitudes.Text = "S";
							}
							else
							{
								sprSolicitudes.Text = "N";
							}
						}
						sprSolicitudes.Lock = true;
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

						//concepto
						sprSolicitudes.Col = bAutorizaciones.AR_COL_TIPOCONC;
						sprSolicitudes.Text = mPrestaciones[i, 1];
						sprSolicitudes.Lock = true;
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

						//tipo de concepto
						sprSolicitudes.Col = bAutorizaciones.AR_COL_CONCEPTO;
						sprSolicitudes.Text = mPrestaciones[i, 2];
						sprSolicitudes.Lock = true;
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

						//Oscar C Agosto 2011. Copago
						sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
						sprSolicitudes.Text = "";
						sprSolicitudes.Lock = false;
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bFC);
						//--------------------------

						if (bGA)
						{

							if (mAnoprog != 0 && mNumProg != 0 && sprSolicitudes.Row == 1)
							{

								//Si la prestacion principal (primera Fila de la matriz)
								//es una actividad programada, se buscara la solicitud de
								//autorizacion asociada al episodio programado

								Sql = new StringBuilder(" SELECT * FROM IFMSFA_AUTORIZA  " + 
								      " INNER JOIN IFMSFA_ACTIVTAU  on IFMSFA_ACTIVTAU.ganoauto = IFMSFA_AUTORIZA.ganoauto AND " + 
								      " IFMSFA_ACTIVTAU.gnumauto = IFMSFA_AUTORIZA.gnumauto " + 
								      " WHERE IFMSFA_AUTORIZA.fborrado is null and " + 
								      " IFMSFA_ACTIVTAU.itipacti='P' AND " + 
								      " IFMSFA_ACTIVTAU.itiposer='" + miTiposer + "' AND " + 
								      " IFMSFA_ACTIVTAU.ganoregi=" + mAnoprog.ToString() + " AND " + 
								      " IFMSFA_ACTIVTAU.gnumregi=" + mNumProg.ToString() + " AND " + 
								      " IFMSFA_ACTIVTAU.rconc ='" + mPrestaciones[i, 1] + "' AND " + 
								      " IFMSFA_ACTIVTAU.econc ='" + mPrestaciones[i, 2] + "' AND " + 
								      " (IFMSFA_AUTORIZA.gpersona IS NULL or " + 
								      "  IFMSFA_AUTORIZA.gpersona =" + mPersona.ToString() + ") AND " + 
								      " IFMSFA_AUTORIZA.gestsoli IN ('P','S','A') and " + 
								      " IFMSFA_AUTORIZA.gsocieda =" + mSociedad.ToString() + " AND " + 
								      " IFMSFA_AUTORIZA.gidenpac ='" + mgidenpac + "'");

								//14/11/2008 A�ADIMOS LA CONDICION DE BUSQUEDA DE AUTORIZACION EL NAFILIAC
								if (stFiliacion.Trim() != "")
								{
									Sql.Append(" AND (IFMSFA_AUTORIZA.nafiliac is null or IFMSFA_AUTORIZA.nafiliac='" + stFiliacion + "')");
								}

								if (mInspeccion.Trim() != "")
								{
									Sql.Append(" AND IFMSFA_AUTORIZA.ginspecc='" + mInspeccion + "'");
								}


								SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql.ToString(), mcnConexion);
								rr = new DataSet();
								tempAdapter_3.Fill(rr);

								if (rr.Tables[0].Rows.Count == 0)
								{

									CogePKElectronica = true;

									//Si no existen datos de solicitudes, asociada al episodio programado, buscamos si existe
									//cualquier otra solicitud para las condiciones recibidas.
									//Si solo existe una solicitud para los datos recibidos sin utilizar, se muestra dicha
									//solicitud por defecto

									Sql = new StringBuilder("SELECT * FROM IFMSFA_AUTORIZA  WHERE" + 
									      " gidenpac ='" + mgidenpac + "' AND " + 
									      " gsocieda =" + mSociedad.ToString() + " AND " + 
									      " gregimen ='" + miTiposer + "' AND" + 
									      " gservici =" + lservicioprestacion.ToString() + " AND " + 
									      " (gpersona IS NULL or gpersona =" + mPersona.ToString() + ") AND " + 
									      " rconc ='" + mPrestaciones[i, 1] + "' AND " + 
									      " econc ='" + mPrestaciones[i, 2] + "' AND " + 
									      " gestsoli IN ('P','S','A') AND   fborrado is null  ");

									//14/11/2008 A�ADIMOS LA CONDICION DE BUSQUEDA DE AUTORIZACION EL NAFILIAC
									if (stFiliacion.Trim() != "")
									{
										Sql.Append(" AND (IFMSFA_AUTORIZA.nafiliac is null or IFMSFA_AUTORIZA.nafiliac='" + stFiliacion + "')");
									}

									if (mInspeccion.Trim() != "")
									{
										Sql.Append(" AND IFMSFA_AUTORIZA.ginspecc='" + mInspeccion + "'");
									}

									Sql.Append(" and NOT EXISTS (SELECT ''  FROM  IFMSFA_ACTIVTAU  ");

									//Sql = "      WHERE  ITIPACTI = 'R' and "
									Sql.Append(" WHERE  " + 
									           "                   IFMSFA_AUTORIZA.ganoauto = IFMSFA_ACTIVTAU.ganoauto and" + 
									           "                   IFMSFA_AUTORIZA.gnumauto = IFMSFA_ACTIVTAU.gnumauto )");
									//" STR(GANOAUTO)+STR(GNUMAUTO) NOT IN (SELECT STR(GANOAUTO)+STR(GNUMAUTO) FROM " & _
									//" IFMSFA_ACTIVTAU WHERE " & _
									//" ITIPACTI = 'R')"

								}

								rr.Close();

							}
							else
							{

								//Otras Prestaciones o Activad no programada
								//Si solo existe una solicitud para los datos recibidos sin utilizar, se muestra dicha
								//solicitud por defecto

								Sql = new StringBuilder("SELECT * FROM IFMSFA_AUTORIZA  WHERE " + 
								      " gidenpac ='" + mgidenpac + "' AND " + 
								      " gsocieda =" + mSociedad.ToString() + " AND " + 
								      " gregimen ='" + miTiposer + "' AND" + 
								      " gservici =" + lservicioprestacion.ToString() + "  ");

								Sql.Append(" AND (gpersona IS NULL or gpersona =" + mPersona.ToString() + ") AND " + 
								           " rconc ='" + mPrestaciones[i, 1] + "' AND " + 
								           " econc ='" + mPrestaciones[i, 2] + "' AND " + 
								           " gestsoli IN ('P','S','A') AND fborrado is  null ");

								//14/11/2008 A�ADIMOS LA CONDICION DE BUSQUEDA DE AUTORIZACION EL NAFILIAC
								if (stFiliacion.Trim() != "")
								{
									Sql.Append(" AND (IFMSFA_AUTORIZA.nafiliac is null or IFMSFA_AUTORIZA.nafiliac='" + stFiliacion + "')");
								}

								if (mInspeccion.Trim() != "")
								{
									Sql.Append(" AND IFMSFA_AUTORIZA.ginspecc='" + mInspeccion + "'");
								}

								Sql.Append("  AND NOT EXISTS (SELECT ''  FROM  IFMSFA_ACTIVTAU  ");
								Sql.Append(" WHERE  " + 
								           "                   IFMSFA_AUTORIZA.ganoauto = IFMSFA_ACTIVTAU.ganoauto and" + 
								           "                   IFMSFA_AUTORIZA.gnumauto = IFMSFA_ACTIVTAU.gnumauto )");

							}

							SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(Sql.ToString(), mcnConexion);
							rr = new DataSet();
							tempAdapter_4.Fill(rr);

							dnautoriAUX = "";
							dnvolantAUX = "";
							dnopelecAUX = "";

							if (rr.Tables[0].Rows.Count != 0)
							{

								CogePKElectronica = false;

								if (rr.Tables[0].Rows.Count == 1)
								{

									CogePKElectronica = false;

									sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
									sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["ganoauto"]) + "/" + Convert.ToString(rr.Tables[0].Rows[0]["gnumauto"]);
									sprSolicitudes.Lock = true;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Boton Para Seleccionar Solicitud
									sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
									sprSolicitudes.CellType = 1;
									sprSolicitudes.Lock = true;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//cantidad
									//30/07/2004
									//La cantidad se mostrara la que se hubiera pedido,
									//sin importar si hubiera otras autorizaciones aprobadas u de episodios de
									//actividad programada
									sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
									sprSolicitudes.CellType = 3;
									if (Convert.IsDBNull(rr.Tables[0].Rows[0]["ncantida"]))
									{
										sprSolicitudes.Text = "1";
									}
									else
									{
										sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["ncantida"]).Trim();
									}
									sprSolicitudes.Lock = false;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
									cantidad = Convert.ToInt32(Double.Parse("0" + Convert.ToString(rr.Tables[0].Rows[0]["ncantida"])));

									//Numero de autorizacion
									sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]).Trim();
									sprSolicitudes.Lock = false;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(30); //20
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Numero de volante
									sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnvolant"]).Trim();
									sprSolicitudes.Lock = false;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(30); //20
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Numero de operacion
									sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnopelec"]).Trim();
									sprSolicitudes.Lock = false;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(30); //20
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGT && !bGI && !bGTr && !bGB);

									//Puede ser electronica
									sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
									sprSolicitudes.Text = "N";
									sprSolicitudes.Lock = true;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(20);
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Estado
									sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]).Trim();
									sprSolicitudes.Lock = true;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(20);
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);


									sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
									sprSolicitudes.Value = "0";
									sprSolicitudes.Lock = true;

									sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(1);
									sprSolicitudes.Text = "N";
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

									sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(200);
									sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 200);
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).Trim();
									sprSolicitudes.Lock = true;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Oscar C Agosto 2011. Copago
									sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
									sprSolicitudes.Text = "";
									sprSolicitudes.Lock = false;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bFC);
									if (!Convert.IsDBNull(rr.Tables[0].Rows[0]["cpagpaci"]))
									{
										string tempRefParam = Convert.ToString(rr.Tables[0].Rows[0]["cpagpaci"]);
										sprSolicitudes.Text = Serrores.ConvertirDecimales(ref tempRefParam, ref mSeparadorDecimal, 2);
									}

									sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
									sprSolicitudes.Text = "" + ((Convert.IsDBNull(rr.Tables[0].Rows[0]["iautoe"])) ? "N" : Convert.ToString(rr.Tables[0].Rows[0]["iautoe"]).Trim());

									//--------------------------


									dnautoriAUX = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]).Trim();
									dnvolantAUX = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnvolant"]).Trim();
									dnopelecAUX = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnopelec"]).Trim();
									lanoauto = Convert.ToString(rr.Tables[0].Rows[0]["GANOAUTO"]);
									lnumauto = Convert.ToString(rr.Tables[0].Rows[0]["GNUMAUTO"]);
									Prevact = Convert.ToDateTime(rr.Tables[0].Rows[0]["fPrevact"]);

								}
								else
								{

									CogePKElectronica = false;
									dnvolantAUX = "";

									foreach (DataRow iteration_row in rr.Tables[0].Rows)
									{
										if (Convert.ToString(iteration_row["iautoe"]) == "S" && !Convert.IsDBNull(iteration_row["dnautori"]))
										{
											dnvolantAUX = "" + Convert.ToString(iteration_row["dnvolant"]).Trim();
											CogePKElectronica = false;
											break;
										}
									}

									if (rr.Tables[0].Rows.Count == 0)
									{
										rr.MoveFirst();
									}

									sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
									sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["ganoauto"]) + "/" + Convert.ToString(rr.Tables[0].Rows[0]["gnumauto"]);
									sprSolicitudes.Lock = true;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Boton Para Seleccionar Solicitud
									sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
									sprSolicitudes.CellType = 1;
									sprSolicitudes.Lock = true;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//cantidad
									//30/07/2004
									//La cantidad se mostrara la que se hubiera pedido,
									//sin importar si hubiera otras autorizaciones aprobadas u de episodios de
									//actividad programada
									sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
									sprSolicitudes.CellType = 3;
									if (Convert.IsDBNull(rr.Tables[0].Rows[0]["ncantida"]))
									{
										sprSolicitudes.Text = "1";
									}
									else
									{
										sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["ncantida"]).Trim();
									}
									sprSolicitudes.Lock = false;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
									cantidad = Convert.ToInt32(Double.Parse("0" + Convert.ToString(rr.Tables[0].Rows[0]["ncantida"])));
									//Numero de autorizacion
									sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]).Trim();
									sprSolicitudes.Lock = false;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(30); //20
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Numero de volante
									sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnvolant"]).Trim();
									sprSolicitudes.Lock = false;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(30); //20
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Numero de operacion
									sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnopelec"]).Trim();
									sprSolicitudes.Lock = false;
									sprSolicitudes.setTypeEditLen(30); //20
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGT);

									//Puede ser electronica

									sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
									sprSolicitudes.Text = "N";
									sprSolicitudes.Lock = true;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(20);
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Estado

									sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]).Trim();
									sprSolicitudes.Lock = true;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(20);
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);


									sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
									sprSolicitudes.Value = "0";
									sprSolicitudes.Lock = true;

									sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(1);
									sprSolicitudes.Text = "N";
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

									sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(200);
									sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 200);
									sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).Trim();
									sprSolicitudes.Lock = true;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//Oscar C Agosto 2011. Copago
									sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
									sprSolicitudes.Text = "";
									sprSolicitudes.Lock = false;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bFC);
									if (!Convert.IsDBNull(rr.Tables[0].Rows[0]["cpagpaci"]))
									{
										string tempRefParam2 = Convert.ToString(rr.Tables[0].Rows[0]["cpagpaci"]);
										sprSolicitudes.Text = Serrores.ConvertirDecimales(ref tempRefParam2, ref mSeparadorDecimal, 2);
									}

									sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
									sprSolicitudes.Text = "" + ((Convert.IsDBNull(rr.Tables[0].Rows[0]["iautoe"])) ? "N" : Convert.ToString(rr.Tables[0].Rows[0]["iautoe"]).Trim());
									//--------------------------

									dnautoriAUX = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]).Trim();
									dnvolantAUX = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnvolant"]).Trim();
									dnopelecAUX = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnopelec"]).Trim();
									lanoauto = Convert.ToString(rr.Tables[0].Rows[0]["GANOAUTO"]);
									lnumauto = Convert.ToString(rr.Tables[0].Rows[0]["GNUMAUTO"]);
									Prevact = Convert.ToDateTime(rr.Tables[0].Rows[0]["fPrevact"]);

								}

							}
							else
							{

								//Si el itiposer (normalmente de la Prestacion es H u U), se debera buscar el DU del Ingreso
								if (mAreaT == "H" || mAreaT == "U")
								{

									Sql = new StringBuilder("  SELECT * FROM IFMSFA_AUTORIZA " + 
									      "  INNER JOIN IFMSFA_ACTIVTAU on IFMSFA_ACTIVTAU.ganoauto = IFMSFA_AUTORIZA.ganoauto AND " + 
									      "             IFMSFA_ACTIVTAU.gnumauto = IFMSFA_AUTORIZA.gnumauto " + 
									      " Where " + 
									      " IFMSFA_ACTIVTAU.itipacti = 'R' AND " + 
									      " IFMSFA_ACTIVTAU.itiposer = '" + mAreaT + "' AND " + 
									      " IFMSFA_ACTIVTAU.ganoregi = " + mAnoregi.ToString() + " AND " + 
									      " IFMSFA_ACTIVTAU.gnumregi = " + mNumregi.ToString() + " AND " + 
									      " IFMSFA_ACTIVTAU.rconc = 'ES' AND " + 
									      " IFMSFA_AUTORIZA.GIDENPAC = '" + mgidenpac + "' AND " + 
									      " IFMSFA_AUTORIZA.GSOCIEDA = " + mSociedad.ToString() + " AND " + 
									      " IFMSFA_AUTORIZA.gestsoli  = 'A' AND " + 
									      " IFMSFA_AUTORIZA.iautoe = 'S' AND " + 
									      " IFMSFA_AUTORIZA.DNAUTORI IS NOT NULL AND " + 
									      " IFMSFA_AUTORIZA.fborrado is null ");
									//14/11/2008 A�ADIMOS LA CONDICION DE BUSQUEDA DE AUTORIZACION EL NAFILIAC
									if (stFiliacion.Trim() != "")
									{
										Sql.Append(" AND (IFMSFA_AUTORIZA.nafiliac is null or IFMSFA_AUTORIZA.nafiliac='" + stFiliacion + "')");
									}

									if (mInspeccion.Trim() != "")
									{
										Sql.Append(" AND IFMSFA_AUTORIZA.ginspecc='" + mInspeccion + "'");
									}

									SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(Sql.ToString(), mcnConexion);
									rr = new DataSet();
									tempAdapter_5.Fill(rr);

									if (rr.Tables[0].Rows.Count != 0)
									{

										CogePKElectronica = false;

										//.Col = AR_COL_IDSOLICITUD
										//.Text = rr("ganoauto") & "/" & rr("gnumauto")
										//.Lock = True
										//.ColHidden = Not bGA

										//Boton Para Seleccionar Solicitud
										sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
										sprSolicitudes.CellType = 1;
										sprSolicitudes.Lock = true;
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

										//Numero de autorizacion
										sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
										sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]).Trim();
										sprSolicitudes.Lock = false;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(30); //20
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

										//Numero de volante
										sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
										sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnvolant"]).Trim();
										sprSolicitudes.Lock = false;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(30); //20
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

										//Numero de operacion
										sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
										sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnopelec"]).Trim();
										sprSolicitudes.Lock = false;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(30); //20
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGT);

										//Puede ser electronica
										sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
										//''.Text = "N"
										sprSolicitudes.Text = "" + ((Convert.IsDBNull(rr.Tables[0].Rows[0]["iautoe"])) ? "N" : Convert.ToString(rr.Tables[0].Rows[0]["iautoe"]).Trim());
										sprSolicitudes.Lock = true;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(20);
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

										sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
										sprSolicitudes.Value = "0";
										sprSolicitudes.Lock = true;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(1);
										sprSolicitudes.Text = "N";
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

										sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(200);
										sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 200);
										sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).Trim();
										sprSolicitudes.Lock = true;
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

										//dnautoriAUX = "" & Trim(rr("dnautori"))
										//dnvolantAUX = "" & Trim(rr("dnvolant"))
										//dnopelecAUX = "" & Trim(rr("dnopelec"))
										//lanoauto = rr("GANOAUTO")
										//lnumauto = rr("GNUMAUTO")
										//Prevact = rr("fPrevact")

									}
									else
									{

										//REcorset vacio, es decir no hay autorizacion, la pedimos.
										//primero insertarmos de la BdD
										CogePKElectronica = true;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
										sprSolicitudes.CellType = 1;
										sprSolicitudes.Lock = true;
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									}

								}
								else
								{

									//REcorset vacio, es decir no hay autorizacion, la pedimos.
									//primero insertarmos de la BdD

									CogePKElectronica = true;

									sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
									sprSolicitudes.CellType = 1;
									sprSolicitudes.Lock = true;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
								}

							}

							rr.Close();

						} //bGA

						if (bGT)
						{

							//No se si esto funcionara bien
							//Numero de operacion. Ahora va independientemente de las Autorizaciones

							Sql = new StringBuilder("SELECT iteam, dnopelec, dnautori FROM DEPITEAM  " + 
							      " WHERE ganoregi=" + mAnoregi.ToString() + " AND gnumregi =" + mNumregi.ToString() + 
							      " AND itiposer='" + miTiposer + "'" + 
							      " AND rconc='" + mPrestaciones[i, 1] + "'" + 
							      " AND econc='" + mPrestaciones[i, 2] + "'");

							SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(Sql.ToString(), mcnConexion);
							rr1 = new DataSet();
							tempAdapter_6.Fill(rr1);

							if (rr1.Tables[0].Rows.Count != 0)
							{

								if (!Convert.IsDBNull(rr1.Tables[0].Rows[0]["dnopelec"]))
								{
									sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
									sprSolicitudes.Text = "" + Convert.ToString(rr1.Tables[0].Rows[0]["dnopelec"]).Trim();
								}

								sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
								if (Convert.ToString(rr1.Tables[0].Rows[0]["iteam"]) == "S")
								{
									sprSolicitudes.Value = 1;
								}
								else
								{
									sprSolicitudes.Value = 0;
								}

								if (!Convert.IsDBNull(rr1.Tables[0].Rows[0]["dnautori"]))
								{
									sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
									sprSolicitudes.Text = "" + Convert.ToString(rr1.Tables[0].Rows[0]["dnautori"]).Trim();
								}

								dnopelecAUXDEPITEAM = "" + ("" + Convert.ToString(rr1.Tables[0].Rows[0]["dnopelec"])).Trim();

							}

							rr1.Close();

						}
						else
						{

							dnopelecAUXDEPITEAM = "";

						}

						sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
						//                .ColHidden = Not bTEAM_NumeroOperacionElectronica

						if (!sprSolicitudes.GetColHidden(sprSolicitudes.Col))
						{
							sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_NOPERELE, 20);
						}

						sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
						//                .ColHidden = Not bTEAM_NumeroAutorizacion

						if (!sprSolicitudes.GetColHidden(sprSolicitudes.Col))
						{
							sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_NAUTORIZ, 20);
						}


						//PRINCIPIO REALIZACION ELECTR�NICA

						if (dnautoriAUX == "")
						{
							ParaTipoActividad = "020000";
						}
						else
						{
							ParaTipoActividad = "020000";
						}

						// Realizaci�n electr�nica ITC
						// PHARES

						if (bGA && bGI && bAutArgentina.bUtilizaITC(mcnConexion, mSociedad, miTiposer, mPersona, mPrestaciones[i, 1].TrimStart(), mPrestaciones[i, 2].TrimStart()))
						{

							// Mostramos el frame del TimeOut, la leyenda y el bot�n de reintentos (con enable a false)

							FrmConexion.Visible = true;
							CmdReintentar.Visible = true;
							CmdReintentar.Enabled = false;

							// Intentamos autorizar

							proAutorizaITC(i, cantidad);

						}

						// Realizaci�n electr�nica Traditum

						if (bGA && bGTr && bAutArgentina.bUtilizaTraditum(mcnConexion, mSociedad, miTiposer, mPersona, mPrestaciones[i, 1].TrimStart(), mPrestaciones[i, 2].TrimStart()))
						{

							// Mostramos el frame del TimeOut, la leyenda y el bot�n de reintentos (con enable a false)

							FrmConexion.Visible = true;
							CmdReintentar.Visible = true;
							CmdReintentar.Enabled = false;

							// Intentamos autorizar

							proAutorizaTraditum(i, cantidad);

						}

						// Realizaci�n electr�nica BlackBox


						if (bGA && bGB && bAutorizaciones.bUtilizaBlackBox(mcnConexion, mSociedad, miTiposer, mPersona, mPrestaciones[i, 1].TrimStart(), mPrestaciones[i, 2].TrimStart()))
						{

							// Mostramos el frame del TimeOut, la leyenda y el bot�n de reintentos (con enable a false)

							FrmConexion.Visible = true;
							CmdReintentar.Visible = true;
							CmdReintentar.Enabled = false;

							// Intentamos autorizar

							proAutorizaBlackBox(i, cantidad);
							sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
							sprSolicitudes.Text = "B";

						}

						// Realizaci�n electr�nica Cerezuela

						string tempRefParam3 = Conversion.Str(mSociedad);
						if (bGA && bAutorizaciones.AceptaOnline(ref Provincia, mgidenpac, ref tarjeta, ref Version, codProf, ref nifProf, ref numcole, NumDu, CodDiag, ref codEntidad, ref codUsuario, ref claveOperativa, ref mcnConexion, mSociedad, miTiposer, lservicioprestacion, mPersona, "" + mPrestaciones[i, 1], "" + mPrestaciones[i, 2], ref tempRefParam3, ref CodInfra, mInspeccion))
						{

							sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
							sprSolicitudes.Lock = true;

							if (CogePKElectronica)
							{
								//UPGRADE_ISSUE: (2064) SSCalendarWidgets_A.WIConstants property WIConstants.Year was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
								//lanoauto = ((int) UpgradeStubs.SSCalendarWidgets_A_WIConstants.getYear(DateTime.Today)).ToString();
                                lanoauto = DateTime.Today.Year.ToString();
                                lobjSiguiente = new Numeracion.SIGUIENTE();
								string tempRefParam4 = "NROAUTOR";
								lnumauto = lobjSiguiente.SIG_NUMERO(tempRefParam4, mcnConexion).ToString();
								lobjSiguiente = null;
							}


							if (CogePKElectronica)
							{

								sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
								sprSolicitudes.Text = lanoauto + "*" + lnumauto;
								sprSolicitudes.Lock = true;
								sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
								// Call InsertarAutoriza(Str(lanoauto), Str(lnumauto), mgidenpac, Str(mSociedad), miTiposer, Str(lservicioprestacion), Str(mPersona), "" & mPrestaciones(i, 1), "" & mPrestaciones(i, 2), 1, mParaFechaPrev_Real, mParaFechaPrev_Real, FechaTransaccionAUX, FechaSesionAUX, "USUARIO", oper, noper, tmov, dnautori, dnvolant, dnopelec, "S")
								// Call bAsociaSolicitud(mcnConexion, Val(lanoauto), CLng(lnumauto), "" & mPrestaciones(i, 1), "" & mPrestaciones(i, 2), "", "", mParaFechaPrev_Real, miTiposer, Str(lservicioprestacion), Str(Cantidad), "Usuario")

							}
							else
							{

								if (lanoauto == "" || lnumauto == "")
								{

									sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
									sprSolicitudes.Text = lanoauto + "" + lnumauto;
									sprSolicitudes.Lock = true;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

								}
								else
								{

									sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
									sprSolicitudes.Text = lanoauto + "/" + lnumauto;
									sprSolicitudes.Lock = true;
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

								}

							}

							sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTELECT;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);


							sprSolicitudes.Col = bAutorizaciones.AR_COL_DCONCEPTO;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
							sprSolicitudes.Lock = true;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Col = bAutorizaciones.AR_COL_IDPERMITE;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Col = bAutorizaciones.AR_COL_IDNECESITA;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);


							sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Text = "S";
							sprSolicitudes.Lock = true;
							//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							sprSolicitudes.setTypeEditLen(20);
							sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGT);

							sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Text = "P";
							sprSolicitudes.Lock = true;
							//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							sprSolicitudes.setTypeEditLen(20);
							sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGT);

							//ambar

							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);

							sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
							sprSolicitudes.Lock = true;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
							sprSolicitudes.Lock = true;
							if (sprSolicitudes.Text.Trim() == "")
							{
								sprSolicitudes.Text = NumDu;
							}
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
							sprSolicitudes.Lock = true;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);


							sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
							sprSolicitudes.Value = "0";
							sprSolicitudes.Lock = true;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);

							//Oscar C Agosto 2011. Copago
							sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
							sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
							//------------

							Pedir = false;

							//Casos particulares en los que es necesario pedir
							sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;

							if (bGA && sprSolicitudes.Text.Trim() == "")
							{
								Pedir = true;
							}

							// Fin casos

							FrmConexion.Visible = true;

							//inserta en IFMSAL_AUTORIZA
							//lanoauto = ""
							//lnumauto = ""

							//HACEMOS LAS SOLICITUD, ONLINE

							if (CogePKElectronica || Pedir)
							{

								//�APA
								//Segun correo de Tere del 01 de julio de 2008 18:42,
								//En el Cambio de Solicitud de Sesiones, al ir a autorizar,
								//en el mensaje poner la cantidad = cantidad en pantalla menos el n�mero de sesiones a las
								//que "NO ASISTI�" el paciente.


								CantidadAutorizador = cantidad;

								if (miTiposer == "S" && mTipoActividad == "R")
								{

									//En el caso de las Sesiones
									Sql = new StringBuilder("Select count(*) FROM CSESIONE WHERE " + 
									      " ganoregi = " + mAnoregi.ToString() + " AND " + 
									      " gnumregi = " + mNumregi.ToString() + " AND iestsesi='A'  ");
									SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(Sql.ToString(), mcnConexion);
									rr = new DataSet();
									tempAdapter_7.Fill(rr);
									CantidadAutorizador = Convert.ToInt32(cantidad - Convert.ToDouble(rr.Tables[0].Rows[0][0]));
									rr.Close();

								}

								//ServicioSolicitante = DameServicioSolicitante(mcnConexion, miTiposer, mTipoActividad, mAnoregi, mNumregi)

								RegimenAutorizador = bAutorizaciones.TransformaRegimen(mcnConexion, miTiposer, mTipoActividad, mAnoregi, mNumregi, mServicio, mTipoIngreso);
								string tempRefParam5 = SOAP.ON_LINE;
								Respuesta = SOAP.SolAutorizacion("P", mcnConexion, ref tempRefParam5, mSociedad.ToString(), SOAP.FormatearFechaSoap(DateTimeHelper.ToString(DateTime.Now)), "0100", codEntidad, codUsuario, "34", Provincia, claveOperativa, CodInfra, tarjeta.Trim(), Version, "", "", lanoauto + "" + lnumauto, Musuario.ToUpper(), "0", "0", "", mPrestaciones[i, 1].TrimStart() + mPrestaciones[i, 2].TrimStart(), mServicioSolicitante.Trim(), mServicioSolicitante.Trim(), "", nifProf, numcole, mNumDu, mDiagnostico, "1", RegimenAutorizador, Conversion.Str(lservicioprestacion).Trim(), Conversion.Str(lservicioprestacion).Trim(), CantidadAutorizador.ToString(), SOAP.FormatearFechaSoap(mParaFechaPrev_Real), SOAP.FormatearFechaSoap(String.Format("{0:d}", DateTime.Now)), "");

							}
							else
							{

								PonerEnVerde();


							}

							//Borramos la realizacion, CAMBIO DE REQUISITOS.
							//Respuesta = SolReaHonMedicos(ParaTipoActividad, mcnConexion, ON_LINE, Mid$(tarjeta, 1, 6), FormatearFechaSoap(Now()), "0100", codEntidad, codUsuario, "34", Provincia, claveOperativa, CodInfra, Trim(tarjeta), Version, "", "", lanoauto & "" & lnumauto, "usuario", "0", "0", "", mPrestaciones(i, 2), Trim(Str(lservicioprestacion)), Trim(Str(lservicioprestacion)), "", nifProf, numcole, NumDu, "", "1", "", Cantidad, FormatearFechaSoap(Now()), "", "", "")
							//INSERTAMOS EN UN ARRAY POR SI ES NECESARIO VOLVER A PEDIR.

							X++;
							//   If X <> i Then MsgBox "X no es igual a Y", vbInformation, "Error"
							//ReDim Preserve Peticiones(X, 9)
							Peticiones[X, 0] = lanoauto + "";
							Peticiones[X, 1] = lnumauto + "";
							Peticiones[X, 2] = ParaTipoActividad;
							Peticiones[X, 3] = SOAP.ON_LINE;
							Peticiones[X, 4] = Respuesta;
							Peticiones[X, 5] = "http://indracapiosanitas/SolReaHonMedicos";
							Peticiones[X, 6] = claveOperativa;
							Peticiones[X, 7] = "A";
							Peticiones[X, 8] = SOAP.FormatearFechaSoap(DateTimeHelper.ToString(DateTime.Now));


							RespXML = new XmlDocument();

							try
							{
								RespXML.LoadXml(Respuesta);
							}
							catch
							{
							}

							if (Respuesta != "")
							{

								Fallo = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/fallo").InnerText;
								CodAcc = "";


								try
								{
									CodAcc = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/CodAcc").InnerText;
								}
								catch
								{
								}


								if (CodAcc == "XXX" || CodAcc == "")
								{

									if (CogePKElectronica)
									{
										string tempRefParam6 = Conversion.Str(mPersona);
										InsertarAutoriza(Conversion.Str(lanoauto), Conversion.Str(lnumauto), mgidenpac, Conversion.Str(mSociedad), miTiposer, Conversion.Str(lservicioprestacion), ref tempRefParam6, "" + mPrestaciones[i, 1], "" + mPrestaciones[i, 2], "" + mPrestaciones[i, 3], ref mParaFechaPrev_Real, ref mParaFechaPrev_Real, ref FechaTransaccion, ref FechaSesion, Musuario.ToUpper(), Oper, nOper, tmov, dnautori, dnvolant, numoper, "S", mInspeccion);
									}

								}

								if (CodAcc == "102" || CodAcc == "000")
								{

									dnautori = "" + RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/dnautori").InnerText;
									dnopelec = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/dnopelec").InnerText;
									dnopelec = "";
									dnvolant = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/dnvolant").InnerText;
									numoper = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/noper").InnerText;
									FechaTransaccionAux = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/fechatransaccion").InnerText;
									FechaTransaccion = SOAP.SOAPFormateaFecha(FechaTransaccionAux);
									FechaSesionAux = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/fechasesion").InnerText;
									FechaSesion = SOAP.SOAPFormateaFecha(FechaSesionAux);
									RegistroDatos = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/RegistroDatos").InnerText;
									//fecha sesion


									if (CodAcc == "000")
									{

										if (CogePKElectronica)
										{
											string tempRefParam7 = Conversion.Str(mPersona);
											InsertarAutoriza(Conversion.Str(lanoauto), Conversion.Str(lnumauto), mgidenpac, Conversion.Str(mSociedad), miTiposer, Conversion.Str(lservicioprestacion), ref tempRefParam7, "" + mPrestaciones[i, 1], "" + mPrestaciones[i, 2], "" + mPrestaciones[i, 3], ref mParaFechaPrev_Real, ref mParaFechaPrev_Real, ref FechaTransaccion, ref FechaSesion, Musuario.ToUpper(), Oper, nOper, tmov, dnautori, dnvolant, numoper, "S", mInspeccion);
										}

										//AUTORIZADA

										Peticiones[X, 9] = FechaSesionAux;
										//fecha hora operacion original
										Peticiones[X, 10] = FechaTransaccionAux;
										//Identificacion de op original
										Peticiones[X, 11] = numoper;
										//num aut
										Peticiones[X, 12] = dnautori;
										// fecha realizacion
										Peticiones[X, 13] = SOAP.FormatearFechaSoap(mParaFechaPrev_Real);
										//CodSerCentro
										Peticiones[X, 14] = mPrestaciones[i, 1].TrimStart() + mPrestaciones[i, 2].TrimStart();

										sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTELECT;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);
										sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);
										sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);


										sprSolicitudes.Col = bAutorizaciones.AR_COL_DCONCEPTO;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);
										sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);
										sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);
										sprSolicitudes.Col = bAutorizaciones.AR_COL_IDPERMITE;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);
										sprSolicitudes.Col = bAutorizaciones.AR_COL_IDNECESITA;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
										sprSolicitudes.Text = "" + dnautori;
										sprSolicitudes.Lock = false;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(30); //20
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);

										//Numero de volante

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
										sprSolicitudes.Text = "" + dnvolant;
										sprSolicitudes.Lock = false;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(30); //20
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);


										//Numero de operacion

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
										sprSolicitudes.Text = "" + dnopelec;
										sprSolicitudes.Lock = false;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(30); //20
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGT);
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);

										// Verde

										sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
										sprSolicitudes.Text = "S";
										sprSolicitudes.Lock = true;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(20);
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);

										// Verde

										sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
										sprSolicitudes.Text = "A";
										sprSolicitudes.Lock = true;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(20);
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);

										// Verde

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
										sprSolicitudes.Lock = true;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
										sprSolicitudes.Lock = true;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
										sprSolicitudes.Lock = true;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
										sprSolicitudes.Value = "0";
										sprSolicitudes.Lock = true;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);

										sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(1);
										sprSolicitudes.Text = "N";
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

										sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(200);
										sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 200);
										sprSolicitudes.Text = numoper + "*" + sprSolicitudes.Text + "*" + Fallo + " " + RegistroDatos;
										sprSolicitudes.Lock = true;

										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);

										//Oscar C Agosto 2011. Copago

										sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
										sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);
										//------------

									}

									if (CodAcc == "102")
									{

										if (CogePKElectronica)
										{
											string tempRefParam8 = Conversion.Str(mPersona);
											InsertarAutoriza(Conversion.Str(lanoauto), Conversion.Str(lnumauto), mgidenpac, Conversion.Str(mSociedad), miTiposer, Conversion.Str(lservicioprestacion), ref tempRefParam8, "" + mPrestaciones[i, 1], "" + mPrestaciones[i, 2], "" + mPrestaciones[i, 3], ref mParaFechaPrev_Real, ref mParaFechaPrev_Real, ref FechaTransaccion, ref FechaSesion, Musuario.ToUpper(), Oper, nOper, tmov, dnautori, dnvolant, numoper, "S", mInspeccion);
										}

										//DENEGADA
										sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTELECT;
										sprSolicitudes.BackColor = Color.Red;
										sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
										sprSolicitudes.BackColor = Color.Red;
										sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
										sprSolicitudes.BackColor = Color.Red;



										sprSolicitudes.Col = bAutorizaciones.AR_COL_DCONCEPTO;
										sprSolicitudes.BackColor = Color.Red;
										sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
										sprSolicitudes.BackColor = Color.Red;
										sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
										sprSolicitudes.BackColor = Color.Red;
										sprSolicitudes.Col = bAutorizaciones.AR_COL_IDPERMITE;
										sprSolicitudes.BackColor = Color.Red;
										sprSolicitudes.Col = bAutorizaciones.AR_COL_IDNECESITA;
										sprSolicitudes.BackColor = Color.Red;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
										sprSolicitudes.Text = "" + dnautori;
										sprSolicitudes.Lock = false;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(30); //20
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.Red;

										// Numero de volante

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
										sprSolicitudes.Text = "" + dnvolant;
										sprSolicitudes.Lock = false;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(30); //20
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.Red;

										// Numero de operacion

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
										sprSolicitudes.Text = "" + dnopelec;
										sprSolicitudes.Lock = false;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(30); //20
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGT);
										sprSolicitudes.BackColor = Color.Red;

										// Rojo

										sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
										sprSolicitudes.Text = "S";
										sprSolicitudes.Lock = true;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(20);
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.Red;

										// Rojo

										sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
										sprSolicitudes.Text = "D";
										sprSolicitudes.Lock = true;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(20);
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.Red;

										// Rojo

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
										sprSolicitudes.Lock = true;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
										sprSolicitudes.Lock = true;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
										sprSolicitudes.Lock = true;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
										sprSolicitudes.Value = "0";
										sprSolicitudes.Lock = true;
										sprSolicitudes.BackColor = Color.Red;

										sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(1);
										sprSolicitudes.Text = "N";
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

										sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
										//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
										sprSolicitudes.setTypeEditLen(200);
										sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 200);
										sprSolicitudes.Text = sprSolicitudes.Text + " " + numoper + "*" + Fallo + " " + RegistroDatos;
										sprSolicitudes.Lock = true;

										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);
										sprSolicitudes.BackColor = Color.Red;

										//Oscar C Agosto 2011. Copago
										sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
										sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bFC);
										sprSolicitudes.BackColor = Color.Red;
										//------------

										deniegaEpisodio(Convert.ToInt32(Conversion.Val(lanoauto)), Convert.ToInt32(Double.Parse(lnumauto)), mcnConexion, FechaTransaccion, FechaSesion, Fallo);

									}


									FechaTransaccionAux = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/fechatransaccion").InnerText;
									FechaTransaccion = SOAP.SOAPFormateaFecha(FechaTransaccionAux);
									FechaSesionAux = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/fechasesion").InnerText;
									FechaSesion = SOAP.SOAPFormateaFecha(FechaSesionAux);
									RegistroDatos = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/RegistroDatos").InnerText;

									//Codigo de accion
									// sprSolicitudes.Col = AR_COL_NAUTORIZ
									// sprSolicitudes.BackColor = "H0000FF00"

								}
								else
								{

									sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(1);
									sprSolicitudes.Text = "N";
									sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

									sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
									//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
									sprSolicitudes.setTypeEditLen(200);
									sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 200);
									sprSolicitudes.Text = sprSolicitudes.Text + " " + numoper + "*" + Fallo + " " + RegistroDatos;
									sprSolicitudes.Lock = true;

									sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

									//sprSolicitudes.BackColor = &H80FF&

								}

							}
							else
							{

								sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
								//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
								sprSolicitudes.setTypeEditLen(1);
								sprSolicitudes.Text = "N";
								sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

								sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
								//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
								sprSolicitudes.setTypeEditLen(200);
								sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 200);
								sprSolicitudes.Text = sprSolicitudes.Text + " " + numoper + "*" + Fallo + " " + RegistroDatos;
								sprSolicitudes.Lock = true;

								sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bGA);

								//sprSolicitudes.BackColor = &H80FF&

							}

						}

						//FIN REGISTRO ELECTRONICA

					}

					Timer.Text = Conversion.Str((int) DateAndTime.DateDiff("s", FechaTimeOut, DateTime.Now, Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1));

				}

				//Si exsite control de autorizaciones pero ninguna de las prestaciones
				//requiere Autorizacion/Volante, ocultamos las columnas correspondientes para no liar
				//a los usuarios y dejar solo las columnas correspondientes al TIM.

				if (bAutorizaciones.bGestionAutorizaciones(mcnConexion) && !bAutorizaciones.bNecesitaDocumentacion(mcnConexion, mSociedad, miTiposer, mServicio, mPersona, mPrestaciones, mInspeccion))
				{


					sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
					sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
					sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
					sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;

					if (bAutorizaciones.bTEAM_NumeroAutorizacion && bGT)
					{
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);
						sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_NAUTORIZ, 20);
					}
					else
					{
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
					}

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
					sprSolicitudes.Col = bAutorizaciones.AR_COL_IDNECESITA;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
					sprSolicitudes.Col = bAutorizaciones.AR_COL_IDPERMITE;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
					sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

					//Oscar C Agosto 2011. Copago
					sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
					//---------

					sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_DCONCEPTO, 50);
					//            .ColWidth(AR_COL_NOPERELE) = 20

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;

					//If bTEAM_NumeroOperacionElectronica And .ColHidden = True Then
					//    .ColHidden = False
					if (!sprSolicitudes.GetColHidden(sprSolicitudes.Col))
					{
						sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_NOPERELE, 20);
					}
					//End If

					Label1.Visible = false;


				}
			}
			catch(Exception Excep)
			{

				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), "indra", "Actividad_Realizada: Form_Load: RellenaGridSolicitudes (" + Sql.ToString() + ")", Excep);
			}

		}
		//UPGRADE_NOTE: (7001) The following declaration (sacadatosARRAY) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void sacadatosARRAY(int ganoauto, int gnumauto, ref string FechaSesionAux, ref string FechaTransaccionAux, ref string numoper, ref string dnautori)
		//{
				//string Sql = " select  fresoluc as FechaSesionAUX, ftramite as FechaTransaccionAUX , obsolici as NumOper ,dnautori as  dnautori from ifmsfa_autoriza " + 
				//             " where gnumauto=" + gnumauto.ToString() + " and ganoauto=" + ganoauto.ToString();
				//SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion);
				//DataSet rr = new DataSet();
				//tempAdapter.Fill(rr);
				////UPGRADE_ISSUE: (2064) RDO.rdoResultset property rr.BOF was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//if (rr.Tables[0].Rows.Count != 0 || rr.getBOF())
				//{
					////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//FechaSesionAux = "" + SOAP.FormatearFechaSoap(Convert.ToString(rr.Tables[0].Rows[0]["FechaSesionAUX"]));
					////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//FechaTransaccionAux = "" + SOAP.FormatearFechaSoap(Convert.ToString(rr.Tables[0].Rows[0]["FechaTransaccionAUX"]));
					////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					////UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					//if (!Convert.IsDBNull(rr.Tables[0].Rows[0]["numoper"]))
					//{
						////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//numoper = "" + Convert.ToString(rr.Tables[0].Rows[0]["numoper"]).Substring(0, Math.Min(Convert.ToString(rr.Tables[0].Rows[0]["NumOper"]).IndexOf('*'), Convert.ToString(rr.Tables[0].Rows[0]["numoper"]).Length));
						////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						//dnautori = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]).Trim();
					//}
				//}
		//}

		private void PonerEnVerde()
		{


			for (int i = 0; i <= sprSolicitudes.MaxCols + 1; i++)
			{
				sprSolicitudes.Col = i;
				sprSolicitudes.BackColor = Color.FromArgb(0, 192, 0);
			}

			sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
			sprSolicitudes.Text = "A";

		}

		private void PonerLockFila()
		{


			for (int i = 0; i <= sprSolicitudes.MaxCols + 1; i++)
			{
				sprSolicitudes.Col = i;
				sprSolicitudes.Lock = true;
			}

		}

		private void PonerEnRojo()
		{


			for (int i = 0; i <= sprSolicitudes.MaxCols + 1; i++)
			{
				sprSolicitudes.Col = i;
				sprSolicitudes.BackColor = Color.Red;
			}

			sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
			sprSolicitudes.Text = "D";

		}

		private void PonerEnNaranja()
		{


			for (int i = 0; i <= sprSolicitudes.MaxCols + 1; i++)
			{
				sprSolicitudes.Col = i;
				sprSolicitudes.BackColor = Color.FromArgb(255, 128, 0);
			}

		}

		public void deniegaEpisodio(int ganoauto, int gnumauto, SqlConnection ParacnConexion, string FECHATRAMITE, string FechaSesion, string Fallo)
		{
			object tempRefParam = FECHATRAMITE;
			object tempRefParam2 = FechaSesion;
			string Sql = " UPDATE IFMSFA_AUTORIZA SET " + 
			             "    gestsoli='D',obsolici='" + Fallo + "-D', FTRAMITE=" + Serrores.FormatFechaHMS(tempRefParam) + " , FRESOLUC=" + Serrores.FormatFechaHMS(tempRefParam2) + " where ganoauto=" + ganoauto.ToString() + " and gnumauto=" + gnumauto.ToString() + " ";
			FechaSesion = Convert.ToString(tempRefParam2);
			FECHATRAMITE = Convert.ToString(tempRefParam);
			SqlCommand tempCommand = new SqlCommand(Sql, ParacnConexion);
			tempCommand.ExecuteNonQuery();
		}
		private void InsertarAutoriza(string lanoauto, string lnumauto, string ParaGidenpac, string ParaSociedad, string ParaItiposer, string lservicioprestacion, ref string lpersona, string ParaTipoConc, string ParaConcepto, string ParaCantidad, ref string ParafPrevact, ref string ParafRealiza, ref string FechaTransaccion, ref string FechaSesion, string ParaUsuario, string Oper, string nOper, string tmov, string dnautori, string dnvolant, string numoper, string iautoe, string ParaInspeccion)
		{

			string stFiliacion = bAutorizaciones.fObtenerNafiliac(mcnConexion, ParaGidenpac, Convert.ToInt32(Double.Parse(ParaSociedad)), ParaInspeccion);

			//�apa NO SE PORQUE:
			//Al registrar una solicitud de autorizacion, se tendra en cuenta lo siguiente para rellenar el campo de
			//la persona: Se grabar� en gpersona el contenido del campo gpersona recuperado de la tabla NECESIDADES DE DOCUMENTACION (IFMSFA_ACTIVTAU),
			//es decir pasamos de la persona recibida y grabamos la de dicha tabla, segun pagina 4.1 del cuaderno de carga.
			string Sql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
			             " (case when gsocieda is not null then 1 else 0 end)+ " + 
			             " (case when gregimen is not null then 1 else 0 end)+ " + 
			             " (case when gservici is not null then 1 else 0 end)+ " + 
			             " (case when gpersona is not null then 1 else 0 end)+ " + 
			             " (case when ginspecc is not null then 1 else 0 end), " + 
			             " gpersona " + 
			             " FROM IFMSFA_DOCUTAU " + 
			             " WHERE " + 
			             " rconc = '" + ParaTipoConc + "' AND " + 
			             " (econc is null or econc = '" + ParaConcepto + "') AND " + 
			             " (gsocieda is null or gsocieda = " + ParaSociedad + ") AND " + 
			             " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
			             " (gservici is null or gservici = " + lservicioprestacion + ") AND " + 
			             " (gpersona is null or gpersona = " + lpersona + ") AND " + 
			             " (ginspecc is null or ginspecc = '" + ParaInspeccion + "') AND " + 
			             " fborrado is null " + 
			             " ORDER BY iprioridad desc";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				rr.MoveFirst();
				if (!Convert.IsDBNull(rr.Tables[0].Rows[0][1]))
				{
					lpersona = Convert.ToString(rr.Tables[0].Rows[0][1]);
				}
				else
				{
					lpersona = "0";
				}
			}
			rr.Close();


			Sql = "INSERT INTO IFMSFA_AUTORIZA " + 
			      "   (ganoauto, gnumauto, gIdenpac," + 
			      "    gSocieda, gregimen, gservici, gPersona, " + 
			      "    rconc, econc, ncantida," + 
			      "    fprevact, fsoliaut,ftramite,fresoluc, gestsoli, " + 
			      "    gUsuario, oper, noper , tmov,dnautori,dnvolant,obsolici,iautoe,nafiliac, ginspecc)" + 
			      " VALUES (" + 
			      lanoauto + ", " + lnumauto + ", '" + ParaGidenpac + "', " + 
			      ParaSociedad + ", '" + ParaItiposer + "', " + lservicioprestacion + ", ";
			if (StringsHelper.ToDoubleSafe(lpersona) != 0)
			{
				Sql = Sql + lpersona + ", ";
			}
			else
			{
				Sql = Sql + " NULL, ";
			}
			object tempRefParam = ParafPrevact;
			object tempRefParam2 = ParafRealiza;
			object tempRefParam3 = FechaTransaccion;
			object tempRefParam4 = FechaSesion;
			Sql = Sql + 
			      "'" + ParaTipoConc + "', '" + ParaConcepto + "', '" + ParaCantidad + "', " + 
			      Serrores.FormatFechaHMS(tempRefParam) + ", " + Serrores.FormatFechaHMS(tempRefParam2) + "," + Serrores.FormatFechaHMS(tempRefParam3) + "," + Serrores.FormatFechaHMS(tempRefParam4) + ", 'A',";
			FechaSesion = Convert.ToString(tempRefParam4);
			FechaTransaccion = Convert.ToString(tempRefParam3);
			ParafRealiza = Convert.ToString(tempRefParam2);
			ParafPrevact = Convert.ToString(tempRefParam);

			if (ParaUsuario != "")
			{
				Sql = Sql + "'" + ParaUsuario.ToUpper() + "',";
			}
			else
			{
				Sql = Sql + "NULL, ";
			}


			Sql = Sql + "'" + Oper + "','" + nOper + "','" + tmov + "',";

			if (dnautori != "")
			{
				Sql = Sql + "'" + dnautori + "',";
			}
			else
			{
				Sql = Sql + "NULL, ";
			}

			if (dnvolant != "")
			{
				Sql = Sql + "'" + dnvolant + "',";
			}
			else
			{
				Sql = Sql + "NULL, ";
			}

			if (numoper != "")
			{
				Sql = Sql + "'" + numoper + "*',";
			}
			else
			{
				Sql = Sql + "NULL,";
			}

			if (iautoe != "")
			{
				Sql = Sql + "'" + iautoe + "',";
			}
			else
			{
				Sql = Sql + "'N',";
			}

			if (stFiliacion.Trim() != "")
			{ //14/11/2008 se a�ade la tarjeta  a la autorizacion
				Sql = Sql + "'" + stFiliacion + "',";
			}
			else
			{
				Sql = Sql + "NULL,";
			}

			if (ParaInspeccion.Trim() != "")
			{
				Sql = Sql + "'" + ParaInspeccion + "'";
			}
			else
			{
				Sql = Sql + "NULL";
			}

			Sql = Sql + ")";

			//" & dnautori & "','" & dnvolant & "','" & dnopelec & "')"

			SqlCommand tempCommand = new SqlCommand(Sql, mcnConexion);
			tempCommand.ExecuteNonQuery();
		}
		private bool ValidaDatos()
		{
			bool result = false;
			string stCabeceraCantidad = String.Empty;
			string stCabeceraPP = String.Empty;
			string stCabeceraAutorizacion = String.Empty;
			string stCabeceraVolante = String.Empty;
			bool bNecesitaAUT = false;
			bool bNecesitaVOL = false;
			bool bPermitePRUE = false;
			string stNumAUT = String.Empty;
			string stNumVOL = String.Empty;
			bool Automatica = false;

			if (bGA)
			{

				sprSolicitudes.Row = 0;
				sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
				stCabeceraCantidad = sprSolicitudes.Text;
				sprSolicitudes.Col = bAutorizaciones.AR_COL_IDPERMITE;
				stCabeceraPP = sprSolicitudes.Text;
				sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
				stCabeceraAutorizacion = sprSolicitudes.Text;
				sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
				stCabeceraVolante = sprSolicitudes.Text;

				int tempForVar = sprSolicitudes.MaxRows;
				for (int i = 1; i <= tempForVar; i++)
				{

					bPermitePRUE = false;
					bNecesitaAUT = false;
					bNecesitaVOL = false;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
					sprSolicitudes.Row = i;
					if (sprSolicitudes.Text == "0")
					{
						string tempRefParam = Serrores.ObtenerNombreAplicacion(mcnConexion);
						string tempRefParam2 = "";
						short tempRefParam3 = 1040;
						string[] tempRefParam4 = new string[]{stCabeceraCantidad};
						Serrores.oClass.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, mcnConexion, tempRefParam4);
						//sdsalazar
                        //sprSolicitudes.Action = 1;
						sprSolicitudes.Action = 0;
						return result;
					}

					sprSolicitudes.Col = bAutorizaciones.AR_COL_IDPERMITE;
					if (sprSolicitudes.Text.Trim().ToUpper() == "S")
					{
						bPermitePRUE = true;
					}
					sprSolicitudes.Col = bAutorizaciones.AR_COL_IDNECESITA;
					if (sprSolicitudes.Text.Trim().ToUpper() == "A")
					{
						bNecesitaAUT = true;
					}
					if (sprSolicitudes.Text.Trim().ToUpper() == "V")
					{
						bNecesitaVOL = true;
					}

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
					stNumAUT = sprSolicitudes.Text;
					sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
					stNumVOL = sprSolicitudes.Text;
					sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;

					if (!bPermitePRUE)
					{

						if (bNecesitaAUT && stNumAUT == "")
						{
							string tempRefParam5 = Serrores.ObtenerNombreAplicacion(mcnConexion);
							string tempRefParam6 = "";
							short tempRefParam7 = 1040;
							string[] tempRefParam8 = new string[]{stCabeceraAutorizacion};
							Serrores.oClass.RespuestaMensaje(tempRefParam5, tempRefParam6, tempRefParam7, mcnConexion, tempRefParam8);
							return result;
						}

						if (bNecesitaVOL && stNumVOL == "")
						{
							string tempRefParam9 = Serrores.ObtenerNombreAplicacion(mcnConexion);
							string tempRefParam10 = "";
							short tempRefParam11 = 1040;
							string[] tempRefParam12 = new string[]{stCabeceraVolante};
							Serrores.oClass.RespuestaMensaje(tempRefParam9, tempRefParam10, tempRefParam11, mcnConexion, tempRefParam12);
							return result;
						}

					}

				}

			}

			if (bGT)
			{
				int tempForVar2 = sprSolicitudes.MaxRows;
				for (int i = 1; i <= tempForVar2; i++)
				{
					sprSolicitudes.Row = i;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
					Automatica = sprSolicitudes.Text == "S";


					if (Automatica)
					{
						sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
						sprSolicitudes.Value = "0";
					}
					else
					{
						sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE; //AR_COL_NVOLANTE
						if (sprSolicitudes.Text != "")
						{
							sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
							sprSolicitudes.Value = "1";
						}
					}
				}
			}



			return true;
		}



		private void CmdReintentar_Click(Object eventSender, EventArgs eventArgs)
		{

			int cantidad = 0;

			CmdReintentar.Enabled = false;
			Actividad_Realizada.DefInstance.Enabled = false;
			Actividad_Realizada.DefInstance.Cursor = Cursors.WaitCursor;

			int tempForVar = sprSolicitudes.MaxRows;
			for (int i = 1; i <= tempForVar; i++)
			{

				sprSolicitudes.Row = i;
				sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;

				if (sprSolicitudes.Text.Trim() == "" && (ColorTranslator.ToOle(sprSolicitudes.BackColor) == 0xFF || ColorTranslator.ToOle(sprSolicitudes.BackColor) == 0x80FF))
				{

					// Tomamos los valores necesarios

					sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;

					if (sprSolicitudes.Text.Trim() == "")
					{
						cantidad = 1;
					}
					else
					{
						cantidad = Convert.ToInt32(Double.Parse(sprSolicitudes.Text));
					}

					// Si alguna solicitud est� en rojo o naranja, volvemos a intentar enviarla

					sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;

					if (sprSolicitudes.Text == "I")
					{
						proAutorizaITC(i, cantidad);
					}
					else if (sprSolicitudes.Text == "T")
					{ 
						proAutorizaTraditum(i, cantidad);
					}
					else if (sprSolicitudes.Text == "B" && bGA && bGB)
					{ 
						proAutorizaBlackBox(i, cantidad);
					}

				}

			}

			Actividad_Realizada.DefInstance.Cursor = Cursors.Default;
			Actividad_Realizada.DefInstance.Enabled = true;

		}

		private void proAutorizaITC(int elemento, int cantidad)
		{

			string stResultadoITC = String.Empty;
			string stNumAutoITC = String.Empty;
			string stNumOperaITC = String.Empty;
			string stFirmaAutITC = String.Empty;
			string stCopagoITC = String.Empty;
			string stMotivoRechazoITC = String.Empty;
			int CantidadITC = 0;

			string Sql = String.Empty;
			DataSet rr = null;

			// Iniciamos el timer

			Timer.Text = "0";

			// Si la prestaci�n ya est� autorizada, no hacemos nada m�s que ponerla en verde

			if (bYaEstaRealizada())
			{

				PonerLockFila();
				PonerEnVerde();

			}
			else
			{

				Timer1.Enabled = true;

				// Ocultamos la casilla de b�squeda

				sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

				// Establecemos que es autorizaci�n electr�nica por ITC

				sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
				sprSolicitudes.Text = "I";

				// Bloqueamos la fila para que no se pueda escribir

				PonerLockFila();

				// Ponemos la fila de color naranja

				PonerEnNaranja();

				// Vemos la cantidad para la que vamos a pedir autorizaci�n, en base a si eran sesiones

				CantidadITC = cantidad;

				if (miTiposer == "S" && mTipoActividad == "R")
				{

					//En el caso de las Sesiones
					Sql = "Select count(*) FROM CSESIONE WHERE " + 
					      " ganoregi = " + mAnoregi.ToString() + " AND " + 
					      " gnumregi = " + mNumregi.ToString() + " AND iestsesi='A'  ";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion);
					rr = new DataSet();
					tempAdapter.Fill(rr);
					CantidadITC = Convert.ToInt32(cantidad - Convert.ToDouble(rr.Tables[0].Rows[0][0]));
					rr.Close();

				}

				// Bloqueamos los controles para que no se puedan pulsar los botones

				cbAceptar.Enabled = false;
				cbCancelar.Enabled = false;

				// Mandamos la autorizaci�n

				string tempRefParam = mPrestaciones[elemento, 2].TrimStart();
				bAutArgentina.fAutorizaITC(mcnConexion, mSociedad, mgidenpac.Trim(), ref tempRefParam, CantidadITC, miTiposer, mPrestaciones[elemento, 1].Trim().ToUpper() == "ES", ref stResultadoITC, ref stNumAutoITC, ref stNumOperaITC, ref stFirmaAutITC, ref stCopagoITC, ref stMotivoRechazoITC);

				// Reactivamos los controles para que se puedan pulsar los botones

				cbAceptar.Enabled = true;
				cbCancelar.Enabled = true;

				// Marcamos como reci�n intentada la autorizaci�n

				sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
				sprSolicitudes.Text = "S";
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

				// Analizamos la autorizaci�n

				if (stResultadoITC == "A")
				{

					// Autorizada

					// Completamos los datos en la fila


					sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
					sprSolicitudes.Text = stNumAutoITC;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
					sprSolicitudes.Text = stNumOperaITC;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
					sprSolicitudes.Text = stCopagoITC;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
					sprSolicitudes.Text = ""; //stFirmaAutITC
					//'                .ColHidden = True


					PonerEnVerde();

				}
				else if (stResultadoITC == "D")
				{ 

					// Denegada

					// Completamos los datos en la fila

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
					sprSolicitudes.Text = stNumOperaITC;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
					sprSolicitudes.Text = stCopagoITC;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
					//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					sprSolicitudes.setTypeEditLen(700); // 255
					sprSolicitudes.Text = stMotivoRechazoITC;
					sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 80);
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);

					PonerEnRojo();

					// Activamos el bot�n de reintentos

					CmdReintentar.Enabled = true;

				}
				else
				{

					// Si hubo time-out

					// Borramos los ficheros de la carpeta

					bAutArgentina.proCompruebaCarpetasITC();

					// Completamos los datos en la fila

					sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
					sprSolicitudes.Text = "";
					sprSolicitudes.Lock = !bAutArgentina.bManualTimeOutITC; // Si se permite rellenar el n�mero de autorizaci�n manualmente, desbloqueamos esa casilla

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
					//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					sprSolicitudes.setTypeEditLen(700); // 255
					sprSolicitudes.Text = stMotivoRechazoITC;
					sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 80);
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);

					// Activamos el bot�n de reintentos

					CmdReintentar.Enabled = true;

				}

				// Finalizamos el timer

				Timer1.Enabled = false;

			}

		}

		private void proAutorizaBlackBox(int elemento, int cantidad)
		{

			int CantidadBlackBox = 0;
			bool bResultadoBlackBox = false;

			string stSql = String.Empty;
			DataSet rr = null;
			string[] stPrestacion = new string[]{String.Empty};

			// Iniciamos el timer

			Timer.Text = "0";
			stPrestacion[0] = mPrestaciones[elemento, 2].TrimStart();

			// Si la prestaci�n ya est� autorizada, no hacemos nada m�s que ponerla en verde

			if (bYaEstaRealizada())
			{

				PonerLockFila();
				PonerEnVerde();

			}
			else
			{

				Timer1.Enabled = true;

				// Ocultamos la casilla de b�squeda

				sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

				// Establecemos que es autorizaci�n electr�nica por BlackBox

				sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
				sprSolicitudes.Text = "B";

				// Bloqueamos la fila para que no se pueda escribir

				PonerLockFila();

				// Ponemos la fila de color naranja

				PonerEnNaranja();

				// Vemos la cantidad para la que vamos a pedir autorizaci�n, en base a si eran sesiones

				CantidadBlackBox = cantidad;

				if (miTiposer == "S" && mTipoActividad == "R")
				{

					//En el caso de las Sesiones
					stSql = "Select count(*) FROM CSESIONE WHERE " + 
					        " ganoregi = " + mAnoregi.ToString() + " AND " + 
					        " gnumregi = " + mNumregi.ToString() + " AND iestsesi='A'  ";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, mcnConexion);
					rr = new DataSet();
					tempAdapter.Fill(rr);
					CantidadBlackBox = Convert.ToInt32(cantidad - Convert.ToDouble(rr.Tables[0].Rows[0][0]));
					rr.Close();

				}

				// Bloqueamos los controles para que no se puedan pulsar los botones

				cbAceptar.Enabled = false;
				cbCancelar.Enabled = false;

				// Mandamos la autorizaci�n

				short tempRefParam = (short) mSociedad;
				short tempRefParam2 = (short) mServicio;
				string tempRefParam3 = mgidenpac.Trim();
				string[] tempRefParam4 = stPrestacion;
				bResultadoBlackBox = oBlackBox.proSolicitarAutorizacion(tempRefParam, tempRefParam2, mPersona, tempRefParam3, tempRefParam4);
				stPrestacion = ArraysHelper.CastArray<string[]>(tempRefParam4);

				// Reactivamos los controles para que se puedan pulsar los botones

				cbAceptar.Enabled = true;
				cbCancelar.Enabled = true;

				// Marcamos como reci�n intentada la autorizaci�n

				sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
				sprSolicitudes.Text = "S";
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

				// Analizamos la autorizaci�n

				if (bResultadoBlackBox)
				{

					if (oBlackBox.Respuesta.ToUpper() == "AUTORIZADA")
					{

						// Autorizada

						// Completamos los datos en la fila


						sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
						sprSolicitudes.Text = oBlackBox.Referencia;
						sprSolicitudes.Lock = true;

						sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
						sprSolicitudes.Text = oBlackBox.Timestamp;
						sprSolicitudes.Lock = true;

						sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
						sprSolicitudes.Text = "";

						sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
						sprSolicitudes.Text = "";


						PonerEnVerde();

					}
					else
					{

						// Denegada o error

						// Completamos los datos en la fila

						sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
						sprSolicitudes.Text = "";
						sprSolicitudes.Lock = !(oBlackBox.AutorizacionManual);

						sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
						sprSolicitudes.Text = "";

						sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
						sprSolicitudes.Text = "";

						sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
						//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						sprSolicitudes.setTypeEditLen(700); // 255
						sprSolicitudes.Text = oBlackBox.Respuesta + " (" + oBlackBox.CodigoError + "): " + oBlackBox.DescripcionRespuesta;
						sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 80);
						sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);

						PonerEnRojo();

						// Activamos el bot�n de reintentos

						CmdReintentar.Enabled = true;

					}

				}
				else
				{

					// Timeout o error

					// Completamos los datos en la fila

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
					sprSolicitudes.Text = "";
					sprSolicitudes.Lock = !(oBlackBox.AutorizacionManual);

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
					//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					sprSolicitudes.setTypeEditLen(700); // 255
					sprSolicitudes.Text = oBlackBox.Respuesta + " (" + oBlackBox.CodigoError + "): " + oBlackBox.DescripcionRespuesta;
					sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 80);
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);

					PonerEnRojo();

					// Activamos el bot�n de reintentos

					CmdReintentar.Enabled = true;

				}

				// Finalizamos el timer

				Timer1.Enabled = false;

			}

		}

		private void proAutorizaTraditum(int elemento, int cantidad)
		{

			string stResultadoTraditum = String.Empty;
			string stNumAutoTraditum = String.Empty;
			string stNumOperaTraditum = String.Empty;
			string stFirmaAutTraditum = String.Empty;
			string stCopagoTraditum = String.Empty;
			string stMotivoRechazoTraditum = String.Empty;
			int CantidadTraditum = 0;

			string Sql = String.Empty;
			DataSet rr = null;

			// Iniciamos el timer

			Timer.Text = "0";

			// Si la prestaci�n ya est� autorizada, no hacemos nada m�s que ponerla en verde

			if (bYaEstaRealizada())
			{

				PonerLockFila();
				PonerEnVerde();

			}
			else
			{

				Timer1.Enabled = true;

				// Ocultamos la casilla de b�squeda

				sprSolicitudes.Col = bAutorizaciones.AR_COL_CBBUSCAR;
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

				// Establecemos que es autorizaci�n electr�nica por Traditum

				sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
				sprSolicitudes.Text = "T";

				// Bloqueamos la fila para que no se pueda escribir

				PonerLockFila();

				// Ponemos la fila de color naranja

				PonerEnNaranja();

				// Vemos la cantidad para la que vamos a pedir autorizaci�n, en base a si eran sesiones

				CantidadTraditum = cantidad;

				if (miTiposer == "S" && mTipoActividad == "R")
				{

					//En el caso de las Sesiones
					Sql = "Select count(*) FROM CSESIONE WHERE " + 
					      " ganoregi = " + mAnoregi.ToString() + " AND " + 
					      " gnumregi = " + mNumregi.ToString() + " AND iestsesi='A'  ";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion);
					rr = new DataSet();
					tempAdapter.Fill(rr);
					CantidadTraditum = Convert.ToInt32(cantidad - Convert.ToDouble(rr.Tables[0].Rows[0][0]));
					rr.Close();

				}

				// Bloqueamos los controles para que no se puedan pulsar los botones

				cbAceptar.Enabled = false;
				cbCancelar.Enabled = false;

				// Mandamos la autorizaci�n

				string tempRefParam = mPrestaciones[elemento, 2].TrimStart();
				bAutArgentina.fAutorizaTraditum(mcnConexion, mSociedad, mgidenpac.Trim(), ref tempRefParam, CantidadTraditum, miTiposer, mServicio, mPrestaciones[elemento, 1].Trim().ToUpper() == "ES", ref stResultadoTraditum, ref stNumAutoTraditum, ref stNumOperaTraditum, ref stFirmaAutTraditum, ref stCopagoTraditum, ref stMotivoRechazoTraditum);

				// Reactivamos los controles para que se puedan pulsar los botones

				cbAceptar.Enabled = true;
				cbCancelar.Enabled = true;

				// Marcamos como reci�n intentada la autorizaci�n

				sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;
				sprSolicitudes.Text = "S";
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

				// Analizamos la autorizaci�n

				if (stResultadoTraditum == "A")
				{

					// Autorizada

					// Completamos los datos en la fila


					sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
					sprSolicitudes.Text = stNumAutoTraditum;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
					sprSolicitudes.Text = stNumOperaTraditum;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
					sprSolicitudes.Text = stCopagoTraditum;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
					sprSolicitudes.Text = "";
					//'                .ColHidden = True


					PonerEnVerde();

				}
				else if (stResultadoTraditum == "D")
				{ 

					// Denegada

					// Completamos los datos en la fila

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
					sprSolicitudes.Text = stNumOperaTraditum;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
					sprSolicitudes.Text = stCopagoTraditum;

					sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
					//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					sprSolicitudes.setTypeEditLen(700); // 255
					sprSolicitudes.Text = stMotivoRechazoTraditum;
					sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 80);
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);

					PonerEnRojo();

					// Activamos el bot�n de reintentos

					CmdReintentar.Enabled = true;

				}
				else
				{

					// Si hubo time-out

					// Borramos los ficheros de la carpeta

					bAutArgentina.proCompruebaCarpetasTraditum();

					// Completamos los datos en la fila

					sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
					sprSolicitudes.Text = "";
					sprSolicitudes.Lock = !bAutArgentina.bManualTimeOutTraditum; // Si se permite rellenar el n�mero de autorizaci�n manualmente, desbloqueamos esa casilla

					sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
					sprSolicitudes.Text = "";

					sprSolicitudes.Col = bAutorizaciones.AR_COL_OBSERV;
					//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					sprSolicitudes.setTypeEditLen(700); // 255
					sprSolicitudes.Text = stMotivoRechazoTraditum;
					sprSolicitudes.SetColWidth(bAutorizaciones.AR_COL_OBSERV, 80);
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);

					// Activamos el bot�n de reintentos

					CmdReintentar.Enabled = true;

				}

				// Finalizamos el timer

				Timer1.Enabled = false;

			}

		}




		private bool bEstaBlackBoxActivo(ref SqlConnection mcnConexion)
		{

			try
			{

				oBlackBox = new BlackBoxWS.clsBlackBox();

				oBlackBox.proIniciaBlackBox(mcnConexion);


				return oBlackBox.Activo;
			}
			catch
			{

				return false;
			}
		}

		private void Actividad_Realizada_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				try
				{

					// Control de tipo de gestiones

					bGT = (bAutorizaciones.bGestionTEAM_SOC(mcnConexion, mSociedad) && mPrestaciones[1, 1] == "PR" && bAutorizaciones.bAreaControl_TEAM(mcnConexion, ref mAreaT, ref miTiposer)) || (bAutorizaciones.bGestionTEAM_SOC(mcnConexion, mSociedad) && mPrestaciones[1, 1] == "ES" && miTiposer == "U" && bAutorizaciones.bAreaControl_TEAM(mcnConexion, ref mAreaT, ref miTiposer));

					bGA = bAutorizaciones.bGestionAutorizaciones(mcnConexion);

					bGI = bAutArgentina.bGestionITC(mcnConexion);

					bGTr = bAutArgentina.bGestionTraditum(mcnConexion);

					bGB = bEstaBlackBoxActivo(ref mcnConexion);

					labelDirectorio.Visible = bGI || bGTr;
					lbDirectorio.Visible = bGI || bGTr;

					Label1.Visible = bGA;
					if (!bGA)
					{
						this.Text = "Documentaci�n de Operaci�n Electr�nica - AUT002F1";
					}

					//Oscar C Agosto 2011. Copago
					bFC = (Serrores.ObternerValor_CTEGLOBAL(mcnConexion, "FPAGPACI", "VALFANU1") == "S");
					//------------------

					ProCargaCabecera();
					EstablecePropiedadesGrid();

					Timer1.Enabled = true;
					Timer.Text = "0";
					this.Cursor = Cursors.WaitCursor;

					RellenaGridSolicitudes();

					this.Cursor = Cursors.Default;
					Timer1.Enabled = false;



					//UPGRADE_ISSUE: (2064) Form property Actividad_Realizada.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//this.setHelpContextID(142001);

					return;
				}
				catch(Exception Excep)
				{
					Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), "indra", "Actividad_Realizada: Form_Load", Excep);

					this.Cursor = Cursors.Default;
					Timer1.Enabled = false;
					bAutorizaciones.gContinuarRecogiendoInf = false;
				}
			}
		}


		private void sprSolicitudes_ButtonClicked(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			//int Col = eventArgs.Column;
			//int Row = eventArgs.Row;

            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;

            int ButtonDown = 0;
			string lTipoConc = String.Empty;
			string lConcepto = String.Empty;
			string Sql = String.Empty;
			DataSet rr = null;
			int lservicioprestacion = 0;
			if (Col == bAutorizaciones.AR_COL_CBBUSCAR)
			{
				sprSolicitudes.Col = bAutorizaciones.AR_COL_TIPOCONC;
				sprSolicitudes.Row = Row;
				lTipoConc = sprSolicitudes.Text;

				sprSolicitudes.Col = bAutorizaciones.AR_COL_CONCEPTO;
				sprSolicitudes.Row = Row;
				lConcepto = sprSolicitudes.Text;


				switch(lTipoConc)
				{
					case "ES" : 
						Sql = "SELECT gservici, DNOMSERV FROM DSERVICI WHERE GSERVICI= " + lConcepto; 
						SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion); 
						rr = new DataSet(); 
						tempAdapter.Fill(rr); 
						if (rr.Tables[0].Rows.Count != 0)
						{
							lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
						} 
						rr.Close(); 
						break;
					case "PR" : 
						Sql = "SELECT gservici, DPRESTAC FROM DCODPRES WHERE GPRESTAC= '" + lConcepto + "'"; 
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, mcnConexion); 
						rr = new DataSet(); 
						tempAdapter_2.Fill(rr); 
						if (rr.Tables[0].Rows.Count != 0)
						{
							lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
						} 
						rr.Close(); 
						break;
				}

				Seleccionar_Solicitud.DefInstance.Establecevariables(mcnConexion, mgidenpac, mSociedad, miTiposer, lservicioprestacion, mPersona, lTipoConc, lConcepto, Row, mInspeccion);
				Seleccionar_Solicitud.DefInstance.ShowDialog();

			}
			else if (Col == bAutorizaciones.AR_COL_TIM)
			{ 

				if (bGT)
				{
					sprSolicitudes.Col = Col;
					sprSolicitudes.Row = Row;
					if (StringsHelper.ToDoubleSafe(Convert.ToString(sprSolicitudes.Value)) == 0)
					{
						sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
						if (sprSolicitudes.Text != "S")
						{
							sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
							sprSolicitudes.Text = "";
						}
					}
				}

			}

		}


		private void sprSolicitudes_Change(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
		//	int Col = eventArgs.Column + 1;
		//	int Row = eventArgs.Row + 1;

            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;

            if (bGI && Col == bAutorizaciones.AR_COL_NAUTORIZ)
			{

				sprSolicitudes.Row = Row;
				sprSolicitudes.Col = Col;

				// Si es una fila que est� en naranja y dejamos la celda sin texto, activamos el bot�n de reintentos

				if (ColorTranslator.ToOle(sprSolicitudes.BackColor) == 0x80FF && sprSolicitudes.Text.Trim() == "")
				{
					CmdReintentar.Enabled = true;
				}

				// Si es una fila que est� en naranja y ponemos texto, comprobamos si tenemos alguna otra fila pendiente de autorizar

				if (ColorTranslator.ToOle(sprSolicitudes.BackColor) == 0x80FF && sprSolicitudes.Text.Trim() != "")
				{

					// Por defecto, desactivamos el bot�n

					CmdReintentar.Enabled = false;

					int tempForVar = sprSolicitudes.MaxRows;
					for (int i = 1; i <= tempForVar; i++)
					{

						if (i != Col)
						{

							sprSolicitudes.Row = i;

							// Si la fila es roja o naranja y no tiene n�mero de autorizaci�n, activamos el bot�n

							if ((ColorTranslator.ToOle(sprSolicitudes.BackColor) == 0x80FF || ColorTranslator.ToOle(sprSolicitudes.BackColor) == 0xFF) && sprSolicitudes.Text.Trim() == "")
							{
								CmdReintentar.Enabled = true;
							}

						}

					}

				}

			}

		}

		private void sprSolicitudes_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			if (Button == 1)
			{
				//int Col = eventArgs.Column;
				//int Row = eventArgs.Row;
                int Col = eventArgs.ColumnIndex + 1;
                int Row = eventArgs.RowIndex + 1;

                // Si la fila fue autorizada por Traditum o por ITC, al seleccionarla comprobamos la carpeta que se utiliz�

                sprSolicitudes.Row = Row;
				sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;

				string stTipoAutorizacion = sprSolicitudes.Text.Trim().ToUpper();

				if (stTipoAutorizacion == "I" || stTipoAutorizacion == "T")
				{

					// Si lo hemos autorizado ahora, s� tenemos el directorio. Si ya ven�a autorizada, no

					sprSolicitudes.Col = bAutorizaciones.AR_COL_AUTORIZADA_AHORA;

					if (sprSolicitudes.Text.Trim() == "S")
					{

						lbDirectorio.Text = "term" + ((stTipoAutorizacion == "I") ? bAutArgentina.proTerminalITC() : bAutArgentina.proTerminalTraditum()).ToString();

					}

				}

			}
		}

		private void sprSolicitudes_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if ((((sprSolicitudes.ActiveColumnIndex == bAutorizaciones.AR_COL_NAUTORIZ) ? -1 : 0) | bAutorizaciones.AR_COL_NVOLANTE) != 0)
			{
				if (KeyAscii == 39)
				{
					KeyAscii = 0;
				}
			}
			//Oscar C Agosto 2011. Copago
			sprSolicitudes.Row = sprSolicitudes.ActiveRowIndex;
			sprSolicitudes.Col = sprSolicitudes.ActiveColumnIndex;
			int pos = 0;
			if (sprSolicitudes.Col == bAutorizaciones.AR_COL_COPAGO)
			{
				if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8 && Strings.Chr(KeyAscii).ToString() != mSeparadorDecimal)
				{
					KeyAscii = 0;
					//retroceso, tabulador, etc...
				}
				if ((((KeyAscii == Strings.Asc(mSeparadorDecimal[0])) ? -1 : 0) & (sprSolicitudes.Text.IndexOf(mSeparadorDecimal) + 1)) != 0)
				{
					KeyAscii = 0;
				}
				pos = (sprSolicitudes.Text.IndexOf(mSeparadorDecimal) + 1);
				if (pos > 0)
				{
					if (sprSolicitudes.Text.Substring(pos).Length >= 2)
					{
						KeyAscii = 0;
					}
				}
			}
			//-----------------
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void sprSolicitudes_LeaveCell(object eventSender, Telerik.WinControls.UI.CurrentCellChangedEventArgs eventArgs)
		{
            //int Col = eventArgs.ColumnIndex + 1;
            //int Row = eventArgs.RowIndex + 1;
            int Col = eventArgs.CurrentCell.ColumnInfo.Index + 1;
            int Row = eventArgs.CurrentCell.RowInfo.Index + 1;
            int NewCol = eventArgs.NewCell.ColumnInfo.Index + 1;
            int NewRow = eventArgs.NewCell.RowInfo.Index + 1;
            //int NewCol = eventArgs.NewColumn + 1;
            //int NewRow = eventArgs.NewRow + 1;
            //bool Cancel = eventArgs.Cancel;

            if (bGT && Col == bAutorizaciones.AR_COL_NOPERELE)
			{
				sprSolicitudes.Row = Row;
				sprSolicitudes.Col = Col;
				if (sprSolicitudes.Text != "")
				{
					sprSolicitudes.Col = bAutorizaciones.AR_COL_TIM;
					sprSolicitudes.Value = "1";
				}
			}

		}


		private void Timer1_Tick(Object eventSender, EventArgs eventArgs)
		{
			Application.DoEvents();
			double dbNumericTemp = 0;
			if (Double.TryParse(Timer.Text, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
			{
				Timer.Text = Conversion.Str(Math.Floor(Double.Parse(Timer.Text)) + 1);
			}
			Application.DoEvents();
		}

		private bool bYaEstaRealizada()
		{


			sprSolicitudes.Col = bAutorizaciones.AR_COL_estado;
			string stEstado = sprSolicitudes.Text;

			sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
			string stAuto = sprSolicitudes.Text;

			return (stEstado.Trim().ToUpper() == "A") && (stAuto.Trim() != "");

		}
		private void Actividad_Realizada_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
        private void sprSolicitudes_MouseDown(Object eventSender, MouseEventArgs eventArgs)
        {
            
            if(eventArgs.Button == MouseButtons.Left)
            {
                Button = 1;
            }
        }
    }
}