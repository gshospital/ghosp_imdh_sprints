using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Autorizaciones
{
	public partial class Seleccionar_Solicitud
		: RadForm 
	{


		string Sql = String.Empty;
		DataSet rr = null;

		SqlConnection mcnConexion = null;
		string mgidenpac = String.Empty;
		int mSociedad = 0;
		string mInspeccion = String.Empty;
		string miTiposer = String.Empty;
		int mServicio = 0;
		int mPersona = 0;
		string miTipoConc = String.Empty;
		string mConcepto = String.Empty;
		int miFilaAcambiar = 0;

		string mSeparadorDecimal = String.Empty;

		bool bFC = false; //Facturacion Copago

		//Solicitudes
		const int COL_IDSOLICITUD = 1;
		const int COL_REGIMEN = 2;
		const int COL_CANTIDAD = 3;
		const int COL_FECHAPREVREAL = 4;
		const int COL_FECHASOL = 5;
		const int COL_FECHAINI = 6;
		const int COL_FECHARES = 7;
		const int COL_estado = 8;
		const int COL_DCONCEPTO = 9;
		const int COL_NAUTORIZ = 10;
		const int COL_NVOLANTE = 11;
		const int COL_NOPERELE = 12;
		const int COL_IDNECESITA = 13;
		const int COL_IDPERMITE = 14;
		const int COL_OBSERVACIONES = 15;
		const int COL_TIPOCONC = 16;
		const int COL_CONCEPTO = 17;
		const int COL_iautoe = 18; //REgistro electronico
		const int COL_COPAGO = 19; //Oscar C Agosto 2011. Copago
		public Seleccionar_Solicitud()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{

			if (sprSolicitudes.ActiveRowIndex <= 0)
			{
				return;
			}
			sprSolicitudes.Row = sprSolicitudes.ActiveRowIndex;


			Actividad_Realizada.DefInstance.sprSolicitudes.Row = miFilaAcambiar;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
			sprSolicitudes.Col = COL_iautoe;
			if (Actividad_Realizada.DefInstance.sprSolicitudes.Text != sprSolicitudes.Text)
			{
				MessageBox.Show("Atenci�n", "No puede Realizar una autorizacion electronica, con una solicitud manual y viceversa", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}


			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_IDSOLICITUD;
			sprSolicitudes.Col = COL_IDSOLICITUD;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_CANTIDAD;
			sprSolicitudes.Col = COL_CANTIDAD;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_NAUTORIZ;
			sprSolicitudes.Col = COL_NAUTORIZ;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_NVOLANTE;
			sprSolicitudes.Col = COL_NVOLANTE;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_NOPERELE;
			sprSolicitudes.Col = COL_NOPERELE;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_IDNECESITA;
			sprSolicitudes.Col = COL_IDNECESITA;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_IDPERMITE;
			sprSolicitudes.Col = COL_IDPERMITE;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_TIPOCONC;
			sprSolicitudes.Col = COL_TIPOCONC;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_CONCEPTO;
			sprSolicitudes.Col = COL_CONCEPTO;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_iautoe;
			sprSolicitudes.Col = COL_iautoe;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;

			//Oscar C Agosto 2011. Copago
			Actividad_Realizada.DefInstance.sprSolicitudes.Col = bAutorizaciones.AR_COL_COPAGO;
			sprSolicitudes.Col = COL_COPAGO;
			Actividad_Realizada.DefInstance.sprSolicitudes.Text = sprSolicitudes.Text;
			//----------------


			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void RellenaGridSolicitudes()
		{
			Color color = new Color();
			int iMaxFilas = 0;
			string stDescConcepto = String.Empty;

			string stFiliacion = bAutorizaciones.fObtenerNafiliac(mcnConexion, mgidenpac, mSociedad, mInspeccion);

			switch(miTipoConc)
			{
				case "ES" : 
					Sql = "SELECT DNOMSERV FROM DSERVICI WHERE GSERVICI= " + mConcepto; 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mcnConexion); 
					rr = new DataSet(); 
					tempAdapter.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						stDescConcepto = "ESTANCIAS PARA " + Convert.ToString(rr.Tables[0].Rows[0]["dnomserv"]).Trim().ToUpper();
					}
					else
					{
						stDescConcepto = "ESTANCIAS";
					} 
					rr.Close(); 
					break;
				case "PR" : 
					Sql = "SELECT DPRESTAC FROM DCODPRES WHERE GPRESTAC= '" + mConcepto + "'"; 
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, mcnConexion); 
					rr = new DataSet(); 
					tempAdapter_2.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						stDescConcepto = Convert.ToString(rr.Tables[0].Rows[0]["dprestac"]).Trim().ToUpper();
					} 
					rr.Close(); 
					break;
			}

			Sql = "SELECT * FROM IFMSFA_AUTORIZA WHERE" + 
			      " gIdenpac ='" + mgidenpac + "' AND " + 
			      " gsocieda =" + mSociedad.ToString() + " AND " + 
			      " gregimen ='" + miTiposer + "' AND" + 
			      " gservici =" + mServicio.ToString() + " AND " + 
			      " (gpersona is null or gPersona =" + mPersona.ToString() + ") AND " + 
			      " rconc ='" + miTipoConc + "' AND " + 
			      " econc ='" + mConcepto + "' AND " + 
			      " gestsoli IN ('P','S','A')  AND fborrado is null ";
			//14/11/2008 A�ADIMOS LA CONDICION DE BUSQUEDA DE AUTORIZACION EL NAFILIAC
			if (stFiliacion.Trim() != "")
			{
				Sql = Sql + " AND (IFMSFA_AUTORIZA.nafiliac is null or IFMSFA_AUTORIZA.nafiliac='" + stFiliacion + "')";
			}

			if (mInspeccion.Trim() != "")
			{
				Sql = Sql + " AND ginspecc='" + mInspeccion + "'";
			}

			Sql = Sql + " AND NOT EXISTS(SELECT ''  FROM  IFMSFA_ACTIVTAU " + 
			      "            WHERE  ITIPACTI = 'R' and " + 
			      "                   IFMSFA_AUTORIZA.ganoauto = IFMSFA_ACTIVTAU.ganoauto and" + 
			      "                   IFMSFA_AUTORIZA.gnumauto = IFMSFA_ACTIVTAU.gnumauto )";


			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, mcnConexion);
			rr = new DataSet();
			tempAdapter_3.Fill(rr);

			if (rr.Tables[0].Rows.Count != 0)
			{
				rr.MoveLast(null);
				rr.MoveFirst();
				iMaxFilas = rr.Tables[0].Rows.Count;
				sprSolicitudes.MaxRows = iMaxFilas;
				for (int iFilas = 1; iFilas <= iMaxFilas; iFilas++)
				{
					sprSolicitudes.Row = iFilas;

					switch(Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]))
					{
						case "P" :  
							color = ColorTranslator.FromOle(bAutorizaciones.COLOR_PENDIENTE); 
							break;
						case "S" :  
							color = ColorTranslator.FromOle(bAutorizaciones.COLOR_SOLICITADA); 
							break;
						case "A" :  
							color = ColorTranslator.FromOle(bAutorizaciones.COLOR_AUTORIZADA); 
							break;
					}

					sprSolicitudes.Col = COL_IDSOLICITUD;
					sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["ganoauto"]) + "/" + Convert.ToString(rr.Tables[0].Rows[0]["gnumauto"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_REGIMEN;
					sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["gregimen"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_CANTIDAD;
					string tempRefParam = Convert.ToString(rr.Tables[0].Rows[0]["ncantida"]);
					sprSolicitudes.Text = "" + Serrores.ConvertirDecimales(ref tempRefParam, ref mSeparadorDecimal, 0);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_FECHAPREVREAL;
					sprSolicitudes.Text = "" + Convert.ToDateTime(rr.Tables[0].Rows[0]["fprevact"]).ToString("dd/MM/yyyy");
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_FECHASOL;
					sprSolicitudes.Text = "" + Convert.ToDateTime(rr.Tables[0].Rows[0]["fsoliaut"]).ToString("dd/MM/yyyy");
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_FECHAINI;
					sprSolicitudes.Text = "" + Convert.ToDateTime(rr.Tables[0].Rows[0]["ftramite"]).ToString("dd/MM/yyyy");
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_FECHARES;
					sprSolicitudes.Text = "" + Convert.ToDateTime(rr.Tables[0].Rows[0]["fresoluc"]).ToString("dd/MM/yyyy");
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_estado;
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_OBSERVACIONES;
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_DCONCEPTO;
					sprSolicitudes.Text = stDescConcepto;
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_NAUTORIZ;
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_NVOLANTE;
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnvolant"]);
					sprSolicitudes.ForeColor = color;

					sprSolicitudes.Col = COL_NOPERELE;
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnopelec"]);
					sprSolicitudes.ForeColor = color;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);

					sprSolicitudes.Col = COL_iautoe;
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["iautoe"]);
					sprSolicitudes.ForeColor = color;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);

					//Necesita..
					sprSolicitudes.Col = COL_IDNECESITA;
					if (bAutorizaciones.bNecesitaAutorizacion(mcnConexion, mSociedad, miTiposer, mServicio, mPersona, miTipoConc, mConcepto, mInspeccion))
					{
						sprSolicitudes.Text = "A";
					}
					else if (bAutorizaciones.bNecesitaVolante(mcnConexion, mSociedad, miTiposer, mServicio, mPersona, miTipoConc, mConcepto, mInspeccion))
					{ 
						sprSolicitudes.Text = "V";
					}
					else
					{
						sprSolicitudes.Text = "N";
					}
					sprSolicitudes.ForeColor = color;

					//Permite realizar prueba
					sprSolicitudes.Col = COL_IDPERMITE;
					if (bAutorizaciones.bPermitirPrueba(mcnConexion, mSociedad, miTiposer, mServicio, mPersona, miTipoConc, mConcepto, mInspeccion))
					{
						sprSolicitudes.Text = "S";
					}
					else
					{
						sprSolicitudes.Text = "N";
					}
					sprSolicitudes.ForeColor = color;

					//concepto
					sprSolicitudes.Col = COL_TIPOCONC;
					sprSolicitudes.Text = miTipoConc;
					sprSolicitudes.ForeColor = color;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

					//tipo de concepto
					sprSolicitudes.Col = COL_CONCEPTO;
					sprSolicitudes.Text = mConcepto;
					sprSolicitudes.ForeColor = color;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

					//Oscar C Agosto 2011. Copago
					sprSolicitudes.Col = COL_COPAGO;
					if (!Convert.IsDBNull(rr.Tables[0].Rows[0]["cpagpaci"]))
					{
						string tempRefParam2 = Convert.ToString(rr.Tables[0].Rows[0]["cpagpaci"]);
						sprSolicitudes.Text = Serrores.ConvertirDecimales(ref tempRefParam2, ref mSeparadorDecimal, 2);
					}
					else
					{
						sprSolicitudes.Text = "";
					}
					sprSolicitudes.ForeColor = color;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bFC);
					//-------------

					rr.MoveNext();
				}
			}
			else
			{
				cbAceptar.Enabled = false;
			}
			rr.Close();

			//concepto
			sprSolicitudes.Col = COL_TIPOCONC;
			sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

			//tipo de concepto
			sprSolicitudes.Col = COL_CONCEPTO;
			sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

			//Oscar C Agosto 2011. Copago
			sprSolicitudes.Col = COL_COPAGO;
			sprSolicitudes.SetColHidden(sprSolicitudes.Col, !bFC);
			//-----------
		}

		private void Seleccionar_Solicitud_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				EstablecePropiedadesGrid();
				RellenaGridSolicitudes();

				//sprSolicitudes.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect;
				//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.SelModeIndex was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				sprSolicitudes.setSelModeIndex(0);

			}
		}

		private void EstablecePropiedadesGrid()
		{
			sprSolicitudes.MaxCols = 19; //20
			sprSolicitudes.MaxRows = 0;
			//cabecereas

			sprSolicitudes.SetColWidth(COL_REGIMEN, 6);
			sprSolicitudes.SetText(COL_REGIMEN, 0, "R�gimen");
			sprSolicitudes.SetColWidth(COL_CANTIDAD, 7);
			sprSolicitudes.SetText(COL_CANTIDAD, 0, "Cantidad Solicitada");
			sprSolicitudes.SetColWidth(COL_FECHAPREVREAL, 8);
			sprSolicitudes.SetText(COL_FECHAPREVREAL, 0, "Fecha Prev/Real");
			sprSolicitudes.SetColWidth(COL_FECHASOL, 8);
			sprSolicitudes.SetText(COL_FECHASOL, 0, "Fecha Solicitud");
			sprSolicitudes.SetColWidth(COL_FECHAINI, 8);
			sprSolicitudes.SetText(COL_FECHAINI, 0, "Fecha Inicio tr�mite");
			sprSolicitudes.SetColWidth(COL_FECHARES, 8);
			sprSolicitudes.SetText(COL_FECHARES, 0, "Fecha Resoluci�n");
			sprSolicitudes.SetColWidth(COL_estado, 5);
			sprSolicitudes.SetText(COL_estado, 0, "Estado");
			sprSolicitudes.SetColWidth(COL_OBSERVACIONES, 100);
			sprSolicitudes.SetText(COL_OBSERVACIONES, 0, "Observaciones");
			sprSolicitudes.SetColWidth(COL_IDSOLICITUD, 21);
			sprSolicitudes.SetText(COL_IDSOLICITUD, 0, "N� Soliciud");
			sprSolicitudes.SetColWidth(COL_DCONCEPTO, 30);
			sprSolicitudes.SetText(COL_DCONCEPTO, 0, "Concepto");
			sprSolicitudes.SetColWidth(COL_CANTIDAD, 7);
			sprSolicitudes.SetText(COL_CANTIDAD, 0, "Cantidad Solicitada");
			sprSolicitudes.SetColWidth(COL_NAUTORIZ, 20);
			sprSolicitudes.SetText(COL_NAUTORIZ, 0, "N�mero de Autorizaci�n");
			sprSolicitudes.SetColWidth(COL_NVOLANTE, 20);
			sprSolicitudes.SetText(COL_NVOLANTE, 0, "N�mero de Volante");
			//    .ColWidth(COL_NOPERELE) = 20
			//    .SetText COL_NOPERELE, 0, "N�mero de Op. Elec."
			sprSolicitudes.SetColWidth(COL_IDNECESITA, 10);
			sprSolicitudes.SetText(COL_IDNECESITA, 0, "Necesita ...");
			sprSolicitudes.SetColWidth(COL_IDPERMITE, 10);
			sprSolicitudes.SetText(COL_IDPERMITE, 0, "Se Permite...");
			sprSolicitudes.SetColWidth(COL_TIPOCONC, 8);
			sprSolicitudes.SetText(COL_TIPOCONC, 0, "TCONC");
			sprSolicitudes.SetColWidth(COL_CONCEPTO, 8);
			sprSolicitudes.SetText(COL_CONCEPTO, 0, "CONC");
			sprSolicitudes.SetColWidth(COL_iautoe, 8);
			sprSolicitudes.SetText(COL_iautoe, 0, "Aut");

			//concepto
			sprSolicitudes.Col = COL_TIPOCONC;
			sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

			//tipo de concepto
			sprSolicitudes.Col = COL_CONCEPTO;
			sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

			//Operacion Electronica
			sprSolicitudes.Col = COL_NOPERELE;
			sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

			//Oscar C Agosto 2011. Copago
			sprSolicitudes.SetColWidth(COL_COPAGO, 10);
			sprSolicitudes.SetText(COL_COPAGO, 0, "Copago");
			if (!bFC)
			{
				sprSolicitudes.Col = COL_COPAGO;
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, false);
			}
			else
			{
				sprSolicitudes.Col = COL_COPAGO;
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
			}
			//----------
            //SDSALAZAR
			//sprSolicitudes.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect;


		}

		public void Establecevariables(SqlConnection ParaConexion, string ParaGidenpac, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaiTipoConc, string ParaConcepto, int ParaiFilaACambiar, string ParaInspeccion)
		{
			mcnConexion = ParaConexion;

			mgidenpac = ParaGidenpac;
			mSociedad = ParaSociedad;
			mInspeccion = ParaInspeccion;
			miTiposer = ParaItiposer;
			mServicio = ParaServicio;
			mPersona = ParaPersona;
			miTipoConc = ParaiTipoConc;
			mConcepto = ParaConcepto;
			miFilaAcambiar = ParaiFilaACambiar;

			mSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(mcnConexion);

			//Oscar C Agosto 2011. Copago
			bFC = (Serrores.ObternerValor_CTEGLOBAL(mcnConexion, "FPAGPACI", "VALFANU1") == "S");
			//------------------

		}

		private void Seleccionar_Solicitud_Load(Object eventSender, EventArgs eventArgs)
		{

			//UPGRADE_ISSUE: (2064) Form property Seleccionar_Solicitud.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//this.setHelpContextID(142002);

		}

		private void sprSolicitudes_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			//int Col = eventArgs.Column;
			//int Row = eventArgs.Row;

            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;

            sprSolicitudes.setSelModeIndex(Row);
			cbAceptar.Enabled = Row > 0;
		}
		private void Seleccionar_Solicitud_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}