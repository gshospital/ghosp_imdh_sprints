using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Autorizaciones
{
	partial class Actividad_Programada
	{

		#region "Upgrade Support "
		private static Actividad_Programada m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Actividad_Programada DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Actividad_Programada();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "tbCantidad", "Label2", "lbMedico", "Label14", "LbFinanciadora", "lbServicio", "Label3", "lbConcepto", "Label8", "Label9", "frasol", "lbHistoria", "Label28", "lbPaciente", "Label11", "Frame2", "cbNuevaSolicitud", "cbAsociar", "sprSolicitudes", "sprAsociaciones", "Frame1", "Line6", "Label6", "Line5", "Label5", "Line4", "Label4", "Frame4", "ShapeContainer1", "sprAsociaciones_Sheet1", "sprSolicitudes_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadTextBoxControl tbCantidad;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadTextBox lbMedico;
		public Telerik.WinControls.UI.RadLabel Label14;
		public Telerik.WinControls.UI.RadTextBox LbFinanciadora;
		public Telerik.WinControls.UI.RadTextBox lbServicio;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadTextBox lbConcepto;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadLabel Label9;
		public System.Windows.Forms.GroupBox frasol;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadLabel Label11;
		public System.Windows.Forms.GroupBox Frame2;
		public Telerik.WinControls.UI.RadButton cbNuevaSolicitud;
		public Telerik.WinControls.UI.RadButton cbAsociar;
		public UpgradeHelpers.Spread.FpSpread sprSolicitudes;
		public UpgradeHelpers.Spread.FpSpread sprAsociaciones;
		public System.Windows.Forms.GroupBox Frame1;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line6;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line5;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line4;
		public Telerik.WinControls.UI.RadLabel Label4;
		public System.Windows.Forms.Panel Frame4;
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		//private FarPoint.Win.Spread.SheetView sprAsociaciones_Sheet1 = null;
		//private FarPoint.Win.Spread.SheetView sprSolicitudes_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Actividad_Programada));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.frasol = new System.Windows.Forms.GroupBox();
			this.tbCantidad = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.lbMedico = new Telerik.WinControls.UI.RadTextBox();
			this.Label14 = new Telerik.WinControls.UI.RadLabel();
			this.LbFinanciadora = new Telerik.WinControls.UI.RadTextBox();
			this.lbServicio = new Telerik.WinControls.UI.RadTextBox();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.lbConcepto = new Telerik.WinControls.UI.RadTextBox();
			this.Label8 = new Telerik.WinControls.UI.RadLabel();
			this.Label9 = new Telerik.WinControls.UI.RadLabel();
			this.Frame2 = new System.Windows.Forms.GroupBox();
			this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label11 = new Telerik.WinControls.UI.RadLabel();
			this.cbNuevaSolicitud = new Telerik.WinControls.UI.RadButton();
			this.cbAsociar = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new System.Windows.Forms.GroupBox();
			this.sprSolicitudes = new UpgradeHelpers.Spread.FpSpread();
			this.sprAsociaciones = new UpgradeHelpers.Spread.FpSpread();
			this.Frame4 = new System.Windows.Forms.Panel();
			this.Line6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.Line5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.Line4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.frasol.SuspendLayout();
			this.Frame2.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.Frame4.SuspendLayout();
			this.SuspendLayout();
			// 
			// ShapeContainer1
			// 
			this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
			this.ShapeContainer1.Size = new System.Drawing.Size(273, 57);
            //this.ShapeContainer1.Size = new System.Drawing.Size(737, 355);
            this.ShapeContainer1.Shapes.Add(Line6);
			this.ShapeContainer1.Shapes.Add(Line5);
			this.ShapeContainer1.Shapes.Add(Line4);
			// 
			// frasol
			// 
			//this.frasol.BackColor = System.Drawing.SystemColors.Control;
			this.frasol.Controls.Add(this.tbCantidad);
			this.frasol.Controls.Add(this.Label2);
			this.frasol.Controls.Add(this.lbMedico);
			this.frasol.Controls.Add(this.Label14);
			this.frasol.Controls.Add(this.LbFinanciadora);
			this.frasol.Controls.Add(this.lbServicio);
			this.frasol.Controls.Add(this.Label3);
			this.frasol.Controls.Add(this.lbConcepto);
			this.frasol.Controls.Add(this.Label8);
			this.frasol.Controls.Add(this.Label9);
			this.frasol.Enabled = true;
			//this.frasol.ForeColor = System.Drawing.SystemColors.ControlText;
			this.frasol.Location = new System.Drawing.Point(4, 48);
			this.frasol.Name = "frasol";
			this.frasol.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frasol.Size = new System.Drawing.Size(781, 111);
			this.frasol.TabIndex = 13;
			this.frasol.Text = "Datos de la Solicitud";
			this.frasol.Visible = true;
			// 
			// tbCantidad
			// 
			this.tbCantidad.AcceptsReturn = true;
			//this.tbCantidad.BackColor = System.Drawing.SystemColors.Window;
			//this.tbCantidad.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.tbCantidad.Cursor = System.Windows.Forms.Cursors.IBeam;
			//this.tbCantidad.ForeColor = System.Drawing.SystemColors.WindowText;
			this.tbCantidad.Location = new System.Drawing.Point(716, 86);
			this.tbCantidad.MaxLength = 5;
			this.tbCantidad.Name = "tbCantidad";
			this.tbCantidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbCantidad.Size = new System.Drawing.Size(48, 19);
			this.tbCantidad.TabIndex = 14;
			this.tbCantidad.Text = "1";
			this.tbCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tbCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCantidad_KeyPress);
			// 
			// Label2
			// 
			//this.Label2.BackColor = System.Drawing.SystemColors.Control;
			//this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(8, 66);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(109, 13);
			this.Label2.TabIndex = 24;
			this.Label2.Text = "Profesional Solicitado:";
			// 
			// lbMedico
			// 
			//this.lbMedico.BackColor = System.Drawing.SystemColors.Control;
			//this.lbMedico.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbMedico.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbMedico.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbMedico.Location = new System.Drawing.Point(126, 64);
			this.lbMedico.Name = "lbMedico";
			this.lbMedico.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbMedico.Size = new System.Drawing.Size(401, 19);
			this.lbMedico.TabIndex = 23;
            this.lbMedico.Enabled = false;
			// 
			// Label14
			// 
			//this.Label14.BackColor = System.Drawing.SystemColors.Control;
			//this.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label14.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label14.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label14.Location = new System.Drawing.Point(8, 20);
			this.Label14.Name = "Label14";
			this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label14.Size = new System.Drawing.Size(91, 17);
			this.Label14.TabIndex = 22;
			this.Label14.Text = "Ent. Financiadora:";
			// 
			// LbFinanciadora
			// 
			//this.LbFinanciadora.BackColor = System.Drawing.SystemColors.Control;
			//this.LbFinanciadora.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.LbFinanciadora.Cursor = System.Windows.Forms.Cursors.Default;
			//this.LbFinanciadora.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LbFinanciadora.Location = new System.Drawing.Point(126, 18);
			this.LbFinanciadora.Name = "LbFinanciadora";
			this.LbFinanciadora.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbFinanciadora.Size = new System.Drawing.Size(637, 19);
			this.LbFinanciadora.TabIndex = 21;
            this.LbFinanciadora.Enabled = false;
			// 
			// lbServicio
			// 
			//this.lbServicio.BackColor = System.Drawing.SystemColors.Control;
			//this.lbServicio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbServicio.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbServicio.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbServicio.Location = new System.Drawing.Point(126, 40);
			this.lbServicio.Name = "lbServicio";
			this.lbServicio.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbServicio.Size = new System.Drawing.Size(401, 19);
			this.lbServicio.TabIndex = 19;
            this.lbServicio.Enabled = false;
			// 
			// Label3
			// 
			//this.Label3.BackColor = System.Drawing.SystemColors.Control;
			//this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label3.Location = new System.Drawing.Point(8, 42);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(93, 13);
			this.Label3.TabIndex = 18;
			this.Label3.Text = "Servicio Solicitado:";
			// 
			// lbConcepto
			// 
			//this.lbConcepto.BackColor = System.Drawing.SystemColors.Control;
			//this.lbConcepto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbConcepto.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbConcepto.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbConcepto.Location = new System.Drawing.Point(126, 86);
			this.lbConcepto.Name = "lbConcepto";
			this.lbConcepto.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbConcepto.Size = new System.Drawing.Size(401, 19);
			this.lbConcepto.TabIndex = 17;
            this.lbConcepto.Enabled = false;
			// 
			// Label8
			// 
			//this.Label8.BackColor = System.Drawing.SystemColors.Control;
			//this.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label8.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label8.Location = new System.Drawing.Point(8, 88);
			this.Label8.Name = "Label8";
			this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label8.Size = new System.Drawing.Size(103, 13);
			this.Label8.TabIndex = 16;
			this.Label8.Text = "Concepto Solicitado:";
			// 
			// Label9
			// 
			//this.Label9.BackColor = System.Drawing.SystemColors.Control;
			//this.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label9.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label9.Location = new System.Drawing.Point(594, 88);
			this.Label9.Name = "Label9";
			this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label9.Size = new System.Drawing.Size(117, 13);
			this.Label9.TabIndex = 15;
			this.Label9.Text = "Cantidad Solicitada:";
			// 
			// Frame2
			// 
			//this.Frame2.BackColor = System.Drawing.SystemColors.Control;
			this.Frame2.Controls.Add(this.lbHistoria);
			this.Frame2.Controls.Add(this.Label28);
			this.Frame2.Controls.Add(this.lbPaciente);
			this.Frame2.Controls.Add(this.Label11);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(4, 4);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(781, 42);
			this.Frame2.TabIndex = 9;
			this.Frame2.Text = "Datos del Paciente";
			this.Frame2.Visible = true;
			// 
			// lbHistoria
			// 
			//this.lbHistoria.BackColor = System.Drawing.SystemColors.Control;
			//this.lbHistoria.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbHistoria.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbHistoria.Location = new System.Drawing.Point(666, 16);
			this.lbHistoria.Name = "lbHistoria";
			this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbHistoria.Size = new System.Drawing.Size(97, 17);
			this.lbHistoria.TabIndex = 20;
			// 
			// Label28
			// 
			//this.Label28.BackColor = System.Drawing.SystemColors.Control;
			//this.Label28.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label28.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label28.Location = new System.Drawing.Point(8, 16);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(59, 17);
			this.Label28.TabIndex = 12;
			this.Label28.Text = "Paciente:";
			// 
			// lbPaciente
			// 
			//this.lbPaciente.BackColor = System.Drawing.SystemColors.Control;
			//this.lbPaciente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbPaciente.Location = new System.Drawing.Point(104, 16);
			this.lbPaciente.Name = "lbPaciente";
			this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbPaciente.Size = new System.Drawing.Size(423, 17);
			this.lbPaciente.TabIndex = 11;
            this.lbPaciente.Enabled = false;
			// 
			// Label11
			// 
			//this.Label11.BackColor = System.Drawing.SystemColors.Control;
			//this.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label11.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label11.Location = new System.Drawing.Point(586, 16);
			this.Label11.Name = "Label11";
			this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label11.Size = new System.Drawing.Size(75, 17);
			this.Label11.TabIndex = 10;
			this.Label11.Text = "Historia cl�nica:";
			// 
			// cbNuevaSolicitud
			// 
			//this.cbNuevaSolicitud.BackColor = System.Drawing.SystemColors.Control;
			this.cbNuevaSolicitud.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbNuevaSolicitud.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbNuevaSolicitud.Location = new System.Drawing.Point(586, 484);
			this.cbNuevaSolicitud.Name = "cbNuevaSolicitud";
			this.cbNuevaSolicitud.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbNuevaSolicitud.Size = new System.Drawing.Size(95, 29);
			this.cbNuevaSolicitud.TabIndex = 7;
			this.cbNuevaSolicitud.Text = "&Nueva Solicitud";
			this.cbNuevaSolicitud.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbNuevaSolicitud.UseVisualStyleBackColor = false;
			this.cbNuevaSolicitud.Click += new System.EventHandler(this.cbNuevaSolicitud_Click);
            this.cbNuevaSolicitud.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cbAsociar
			// 
			//this.cbAsociar.BackColor = System.Drawing.SystemColors.Control;
			this.cbAsociar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbAsociar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAsociar.Location = new System.Drawing.Point(690, 484);
			this.cbAsociar.Name = "cbAsociar";
			this.cbAsociar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAsociar.Size = new System.Drawing.Size(95, 29);
			this.cbAsociar.TabIndex = 6;
			this.cbAsociar.Text = "&Asociar Solicitud";
			this.cbAsociar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbAsociar.UseVisualStyleBackColor = false;
			this.cbAsociar.Click += new System.EventHandler(this.cbAsociar_Click);
            this.cbAsociar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Frame1
			// 
			//this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			this.Frame1.Controls.Add(this.sprSolicitudes);
			this.Frame1.Controls.Add(this.sprAsociaciones);
			this.Frame1.Enabled = true;
			//this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(4, 160);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(781, 283);
			this.Frame1.TabIndex = 1;
			this.Frame1.Text = "Solicitudes de autorizaciones";
			this.Frame1.Visible = true;
			// 
			// sprSolicitudes
			// 
			this.sprSolicitudes.Location = new System.Drawing.Point(8, 20);
			this.sprSolicitudes.Name = "sprSolicitudes";
			this.sprSolicitudes.Size = new System.Drawing.Size(515, 257);
			this.sprSolicitudes.TabIndex = 5;
			this.sprSolicitudes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprSolicitudes_CellClick);
            this.sprSolicitudes.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.FullRowSelect;
            // 
            // sprAsociaciones
            // 
            this.sprAsociaciones.Location = new System.Drawing.Point(532, 20);
			this.sprAsociaciones.Name = "sprAsociaciones";
			this.sprAsociaciones.Size = new System.Drawing.Size(243, 257);
			this.sprAsociaciones.TabIndex = 8;
			// 
			// Frame4
			// 
			//this.Frame4.BackColor = System.Drawing.SystemColors.Control;
			//this.Frame4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Frame4.Controls.Add(this.Label6);
			this.Frame4.Controls.Add(this.Label5);
			this.Frame4.Controls.Add(this.Label4);
			this.Frame4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Frame4.Enabled = true;
			//this.Frame4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame4.Location = new System.Drawing.Point(10, 448);
            //this.Frame4.Location = new System.Drawing.Point(0, 6); //prueba
            this.Frame4.Name = "Frame4";
			this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(273, 57);
            //this.Frame4.Size = new System.Drawing.Size(737, 355); //prueba
            this.Frame4.TabIndex = 0;
			this.Frame4.Visible = true;
			// 
			// Line6
			// 
			this.Line6.BorderColor = System.Drawing.Color.FromArgb(0, 192, 0);
			this.Line6.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line6.BorderWidth = 4;
			this.Line6.Enabled = false;
			this.Line6.Name = "Line6";
			this.Line6.Visible = true;
            this.Line6.X1 = (int)6;
            this.Line6.X2 = (int)54;
            this.Line6.Y1 = (int)44;
            this.Line6.Y2 = (int)44;
            // 
            // Label6
            // 
            //this.Label6.BackColor = System.Drawing.SystemColors.Control;
            //this.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label6.Location = new System.Drawing.Point(68, 37);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(127, 19);
			this.Label6.TabIndex = 4;
			this.Label6.Text = "Solicitudes Autorizadas";
			// 
			// Line5
			// 
			this.Line5.BorderColor = System.Drawing.Color.Black;
			this.Line5.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line5.BorderWidth = 4;
			this.Line5.Enabled = false;
			this.Line5.Name = "Line5";
			this.Line5.Visible = true;
			this.Line5.X1 = (int) 6;
			this.Line5.X2 = (int) 54;
			this.Line5.Y1 = (int) 10;
			this.Line5.Y2 = (int) 10;
			// 
			// Label5
			// 
			//this.Label5.BackColor = System.Drawing.SystemColors.Control;
			//this.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label5.Location = new System.Drawing.Point(68, 2);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(193, 19);
			this.Label5.TabIndex = 3;
			this.Label5.Text = "Solicitudes pendientes de tramitar";
			// 
			// Line4
			// 
			this.Line4.BorderColor = System.Drawing.Color.Blue;
			this.Line4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line4.BorderWidth = 4;
			this.Line4.Enabled = false;
			this.Line4.Name = "Line4";
			this.Line4.Visible = true;
			this.Line4.X1 = (int) 6;
			this.Line4.X2 = (int) 54;
			this.Line4.Y1 = (int) 27;
			this.Line4.Y2 = (int) 27;
			// 
			// Label4
			// 
			//this.Label4.BackColor = System.Drawing.SystemColors.Control;
			//this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label4.Location = new System.Drawing.Point(68, 20);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(127, 19);
			this.Label4.TabIndex = 2;
			this.Label4.Text = "Solicitudes en curso";
			// 
			// Actividad_Programada
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(788, 516);
			this.ControlBox = false;
			this.Controls.Add(this.frasol);
			this.Controls.Add(this.Frame2);
			this.Controls.Add(this.cbNuevaSolicitud);
			this.Controls.Add(this.cbAsociar);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.Frame4);
			this.Frame4.Controls.Add(ShapeContainer1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Actividad_Programada";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Solicitud / Asociaci�n de Autorizaciones  de Actividad Programada - AUT001F1";
			this.Activated += new System.EventHandler(this.Actividad_Programada_Activated);
			this.Closed += new System.EventHandler(this.Actividad_Programada_Closed);
			this.Load += new System.EventHandler(this.Actividad_Programada_Load);
			this.frasol.ResumeLayout(false);
			this.Frame2.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.Frame4.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}