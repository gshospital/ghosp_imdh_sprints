using System;
using System.Drawing;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Autorizaciones
{
	public partial class Resultado
		: RadForm
	{

		public string mresultado = String.Empty;
		public string midioma = String.Empty;
		public Resultado()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}



		private void Resultado_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				lbResultado.Text = mresultado + Environment.NewLine;
				lbResultado.BackColor = Color.White;

				if (((float) (Label2.Width * 15)) > ((float) (lbResultado.Width * 15)))
				{
					this.Width = (int) (Label2.Width + 20);
					Line1.X1 = (int) Label2.Left;
					Line1.X2 = (int) Label2.Width;
				}
				else
				{
					this.Width = (int) (lbResultado.Width + 20);
					Line1.X1 = (int) lbResultado.Left;
					Line1.X2 = (int) lbResultado.Width;
				}

				this.Height = (int) (lbResultado.Height + 67);

			}
		}

		private void Resultado_Load(Object eventSender, EventArgs eventArgs)
		{

			//Falta traducir dependiendo del idioma
			this.Text = "Resultado de la generación de Volantes automaticos";
			Label2.Text = "RELACION DE CONCEPTOS - NUMEROS DE VOLANTES GENERADOS:";

		}

		private void Resultado_Closed(Object eventSender, EventArgs eventArgs)
		{
			mresultado = "";
			midioma = "";
		}
	}
}