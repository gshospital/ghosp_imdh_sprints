using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace Autorizaciones
{
	internal static class bAutorizaciones
	{

		public static bool gContinuarRecogiendoInf = false;
		public static bool gError = false;
		public static bool gErrorTEAM = false;

		// Identificador del n�mero de autorizaci�n generado de actividad programada

		public static int gANOAUT = 0;
		public static int gNUMAUT = 0;

		// Parametrizaci�n del TEAM

		public static bool bTEAM_NumeroAutorizacion = false;
		public static bool bTEAM_NumeroOperacionElectronica = false;

		//Columnas de la pantalla de Actividad_Realizada para poder ser invocadas desde la
		//pantalla de Seleccion de Solicitudes
		public const int AR_COL_IDSOLICITUD = 1;
		public const int AR_COL_CBBUSCAR = 2;
		public const int AR_COL_DCONCEPTO = 3;
		public const int AR_COL_CANTIDAD = 4;
		public const int AR_COL_NAUTORIZ = 5;
		public const int AR_COL_NVOLANTE = 6;
		public const int AR_COL_TIM = 7;
		public const int AR_COL_NOPERELE = 8;
		public const int AR_COL_IDNECESITA = 9;
		public const int AR_COL_IDPERMITE = 10;
		public const int AR_COL_TIPOCONC = 11;
		public const int AR_COL_CONCEPTO = 12;
		public const int AR_COL_AUTELECT = 13;
		public const int AR_COL_iautoe = 14;
		public const int AR_COL_estado = 15;
		public const int AR_COL_COPAGO = 16;
		public const int AR_COL_OBSERV = 17;
		public const int AR_COL_AUTORIZADA_AHORA = 18;

		//colores identificativos del estado de las solicitudes
		public const int COLOR_PENDIENTE = 0x0;
		public const int COLOR_SOLICITADA = 0xFF0000;
		public const int COLOR_AUTORIZADA = 0xC000;

		static string stSeparadordecimalBD = String.Empty;

		//�apa 09/03/05
		//Aunque la sociedad facture por TEAM, ahora mediante la constante global
		//se indica en que areas NO se utiliza el TEAM y si el area recibida esta
		//en la constante global, se considera que no hay control de TEAM.

		//VALFANU2 (itiposer de de prestaciones)
		//VALFANU3 (Captura de actividad): Q (Modulo de quirofanos) / C (CEX) / U (Urgencias)

		//FUNCION bAreaControl_TEAM
		//Devuelve FALSE si exite control de TEAM en el area recibida
		//Devuelve TRUE si NO existe control de TEAM en el area recibida
		internal static bool bAreaControl_TEAM(SqlConnection ParacnConexion, ref string ParaAreaT, ref string ParaAreaT2)
		{
			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;
			try
			{
				result = true;
				Sql = "SELECT VALFANU2, VALFANU3 FROM SCONSGLO WHERE GCONSGLO='CTRLTEAM'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion);
				rr = new DataSet();
				tempAdapter.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					//VALFANU2 indica el tipo de prestaciones (itiposer) que no se facturan por TEAM
					if (ParaAreaT != "" && !Convert.IsDBNull(rr.Tables[0].Rows[0]["valfanu2"]))
					{
						if (Convert.ToString(rr.Tables[0].Rows[0]["valfanu2"]).Trim() != "")
						{
							if (Convert.ToString(rr.Tables[0].Rows[0]["valfanu2"]).Trim().IndexOf(ParaAreaT) >= 0)
							{
								return false;
							}
						}
					}
					//VALFANU3 Indica en que modulos Asistenciales no se factura por TEAM
					//Q (Captura de actividad quirurgica)
					//C (Registro de consultas...) / Registro de Prestaciones
					if (ParaAreaT2 != "" && !Convert.IsDBNull(rr.Tables[0].Rows[0]["valfanu3"]))
					{
						if (Convert.ToString(rr.Tables[0].Rows[0]["valfanu3"]).Trim() != "")
						{
							if (Convert.ToString(rr.Tables[0].Rows[0]["valfanu3"]).Trim().IndexOf(ParaAreaT2) >= 0)
							{
								return false;
							}
						}
					}
				}
			 
				rr.Close();
				return result;
			}
			catch(Exception Exp)
			{
				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), "indra", "Actividad_Realizada: Form_Load: bareacontrol_team (" + Sql + ")", Exp);
				return true;
			}
		}

		//FUNCION bGestionTEAM_SOC
		//Devuelve FALSE si no existe control de TEAM para la Sociedad
		//Devuelve TRUE si existe control de TEAM para la Sociedad
		internal static bool bGestionTEAM_SOC(SqlConnection ParacnConexion, int ParaSociedad)
		{
			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;
			try
			{

				// Valores por defecto de las variables TEAM

				bTEAM_NumeroAutorizacion = false;
				bTEAM_NumeroOperacionElectronica = true;


				Sql = "SELECT VALFANU1, isNull(valfanu1i1, 'N') NumAuto, isNull(valfanu1i2, 'S') OpElec FROM SCONSGLO WHERE GCONSGLO='CTRLTEAM'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion);
				rr = new DataSet();
				tempAdapter.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{

					bTEAM_NumeroAutorizacion = Convert.ToString(rr.Tables[0].Rows[0]["NumAuto"]).Trim().ToUpper() == "S";
					bTEAM_NumeroOperacionElectronica = Convert.ToString(rr.Tables[0].Rows[0]["OpElec"]).Trim().ToUpper() == "S";

					if (Convert.ToString(rr.Tables[0].Rows[0][0]).Trim().ToUpper() == "S")
					{

						Sql = "SELECT isnull(iteam,'N') FROM DSOCIEDA WHERE GSOCIEDA=" + ParaSociedad.ToString();
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion);
						rr = new DataSet();
						tempAdapter_2.Fill(rr);
						if (rr.Tables[0].Rows.Count != 0)
						{
							if (Convert.ToString(rr.Tables[0].Rows[0][0]).Trim().ToUpper() == "S")
							{
								result = true;
							}
						}

					}

				}
			 
				rr.Close();
				return result;
			}
			catch(Exception Exep)
			{
				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), "indra", "Actividad_Realizada: Form_Load: bGestionTEAM_SOC (" + Sql + ")", Exep);
				return false;
			}
		}


		//FUNCION bGestionAutorizaciones
		//Devuelve FALSE si no existe control de autorizaciones
		//Devuelve TRUE si existe control de autorizaciones
		internal static bool bGestionAutorizaciones(SqlConnection ParacnConexion)
		{
			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;
			try
			{

				Sql = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='CTRLAUTO'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion);
				rr = new DataSet();
				tempAdapter.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					if (Convert.ToString(rr.Tables[0].Rows[0][0]).Trim().ToUpper() == "S")
					{
						result = true;
					}
				}
			 
				rr.Close();
				return result;
			}
			catch(Exception Exep)
			{
				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), "indra", "Actividad_Realizada: Form_Load: bGestionAutorizaciones (" + Sql + ")", Exep);
				return false;
			}
		}

		//FUNCION bNecesitaAutorizacion
		//Devuelve FALSE si no se necesita autorizacion
		//Devuelve TRUE si se necesita autorizacion
		internal static bool bNecesitaAutorizacion(SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaInspeccion)
		{
			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;
			int lservicioprestacion = 0;



			switch(ParaTipoConc)
			{
				case "ES" : 
					Sql = "SELECT gservici FROM DSERVICI WHERE GSERVICI= " + ParaConcepto; 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
				case "PR" : 
					Sql = "SELECT gservici FROM DCODPRES WHERE GPRESTAC= '" + ParaConcepto + "'"; 
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter_2.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
			}

			Sql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
			      " (case when gsocieda is not null then 1 else 0 end)+ " + 
			      " (case when gregimen is not null then 1 else 0 end)+ " + 
			      " (case when gservici is not null then 1 else 0 end)+ " + 
			      " (case when gpersona is not null then 1 else 0 end)+ " + 
			      " (case when ginspecc is not null then 1 else 0 end), " + 
			      " ISNULL(inecauto,'N') " + 
			      " FROM IFMSFA_DOCUTAU " + 
			      " WHERE " + 
			      " rconc = '" + ParaTipoConc + "' AND " + 
			      " (econc is null or econc = '" + ParaConcepto + "') AND " + 
			      " (gsocieda is null or gsocieda = " + ParaSociedad.ToString() + ") AND " + 
			      " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
			      " (gservici is null or gservici = " + lservicioprestacion.ToString() + ") AND " + 
			      " (gpersona is null or gpersona = " + ParaPersona.ToString() + ") AND " + 
			      " (ginspecc is null or ginspecc ='" + ParaInspeccion + "') AND " + 
			      " fborrado is null " + 
			      " ORDER BY iprioridad desc";

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, ParacnConexion);
			rr = new DataSet();
			tempAdapter_3.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				rr.MoveFirst();
				if (Convert.ToString(rr.Tables[0].Rows[0][1]).Trim().ToUpper() == "S")
				{
					result = true;
				}
			}

		 
			rr.Close();
			return result;
		}

		//FUNCION bNecesitaVolante
		//Devuelve FALSE si no se necesita volante
		//Devuelve TRUE si se necesita volante
		internal static bool bNecesitaVolante(SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaInspeccion)
		{
			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;
			int lservicioprestacion = 0;


			switch(ParaTipoConc)
			{
				case "ES" : 
					Sql = "SELECT gservici FROM DSERVICI WHERE GSERVICI= " + ParaConcepto; 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
				case "PR" : 
					Sql = "SELECT gservici FROM DCODPRES WHERE GPRESTAC= '" + ParaConcepto + "'"; 
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter_2.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
			}

			Sql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
			      " (case when gsocieda is not null then 1 else 0 end)+ " + 
			      " (case when gregimen is not null then 1 else 0 end)+ " + 
			      " (case when gservici is not null then 1 else 0 end)+ " + 
			      " (case when gpersona is not null then 1 else 0 end)+ " + 
			      " (case when ginspecc is not null then 1 else 0 end), " + 
			      " ISNULL(inecvola,'N') " + 
			      " FROM IFMSFA_DOCUTAU " + 
			      " WHERE " + 
			      " rconc = '" + ParaTipoConc + "' AND " + 
			      " (econc is null or econc = '" + ParaConcepto + "') AND " + 
			      " (gsocieda is null or gsocieda = " + ParaSociedad.ToString() + ") AND " + 
			      " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
			      " (gservici is null or gservici = " + lservicioprestacion.ToString() + ") AND " + 
			      " (gpersona is null or gpersona = " + ParaPersona.ToString() + ") AND " + 
			      " (ginspecc is null or ginspecc = '" + ParaInspeccion + "') AND " + 
			      " fborrado is null " + 
			      " ORDER BY iprioridad desc";

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, ParacnConexion);
			rr = new DataSet();
			tempAdapter_3.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				rr.MoveFirst();
				if (Convert.ToString(rr.Tables[0].Rows[0][1]).Trim().ToUpper() == "S")
				{
					result = true;
				}
			}

		 
			rr.Close();
			return result;
		}

		//FUNCION bPermitirPrueba
		//Devuelve FALSE si no se permite registrar la actividad
		//Devuelve TRUE si se permite registrar la actividad
		internal static bool bPermitirPrueba(SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaInspeccion)
		{
			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;
			int lservicioprestacion = 0;


			switch(ParaTipoConc)
			{
				case "ES" : 
					Sql = "SELECT gservici FROM DSERVICI WHERE GSERVICI= " + ParaConcepto; 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
				case "PR" : 
					Sql = "SELECT gservici FROM DCODPRES WHERE GPRESTAC= '" + ParaConcepto + "'"; 
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter_2.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
			}

			Sql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
			      " (case when gsocieda is not null then 1 else 0 end)+ " + 
			      " (case when gregimen is not null then 1 else 0 end)+ " + 
			      " (case when gservici is not null then 1 else 0 end)+ " + 
			      " (case when gpersona is not null then 1 else 0 end)+ " + 
			      " (case when ginspecc is not null then 1 else 0 end), " + 
			      " ISNULL(iperealiz,'N') " + 
			      " FROM IFMSFA_DOCUTAU " + 
			      " WHERE " + 
			      " rconc = '" + ParaTipoConc + "' AND " + 
			      " (econc is null or econc = '" + ParaConcepto + "') AND " + 
			      " (gsocieda is null or gsocieda = " + ParaSociedad.ToString() + ") AND " + 
			      " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
			      " (gservici is null or gservici = " + ParaServicio.ToString() + ") AND " + 
			      " (gpersona is null or gpersona = " + ParaPersona.ToString() + ") AND " + 
			      " (ginspecc is null or ginspecc ='" + ParaInspeccion + "') AND " + 
			      " fborrado is null " + 
			      " ORDER BY iprioridad desc";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, ParacnConexion);
			rr = new DataSet();
			tempAdapter_3.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				rr.MoveFirst();
				if (Convert.ToString(rr.Tables[0].Rows[0][1]).Trim().ToUpper() == "S")
				{
					result = true;
				}
			}

		 
			rr.Close();
			return result;
		}

		//FUNCION AceptaOnline Cerezuela 8/3/2007

		//Devuelve FALSE si no se necesita autorizacion
		//Devuelve TRUE si se necesita autorizacion y la sociedad en ParamSocieda

		internal static bool AceptaOnline(ref string Provincia, string gidenpac, ref string tarjeta, ref string Version, string codProf, ref string nifProf, ref string numcole, string NumDu, string CodDiag, ref string codEntidad, ref string codUsuario, ref string claveOperativa, ref SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, ref string ParamSociedad, ref string CodInfra, string ParaInspeccion)
		{
			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;
			int lservicioprestacion = 0;
			Mensajes.ClassMensajes clasemensaje = null;
			int imensaje = 0;

			Version = "1";


			switch(ParaTipoConc)
			{
				case "ES" : 
					Sql = "SELECT gservici FROM DSERVICI with (nolock) WHERE GSERVICI= " + ParaConcepto; 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
				case "PR" : 
					Sql = "SELECT gservici FROM DCODPRES with (nolock) WHERE GPRESTAC= '" + ParaConcepto + "'"; 
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter_2.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
			}

			Sql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
			      " (case when gsocieda is not null then 1 else 0 end)+ " + 
			      " (case when gregimen is not null then 1 else 0 end)+ " + 
			      " (case when gservici is not null then 1 else 0 end)+ " + 
			      " (case when gpersona is not null then 1 else 0 end)+ " + 
			      " (case when ginspecc is not null then 1 else 0 end), " + 
			      " ISNULL(iautoe,'N') " + 
			      " FROM IFMSFA_DOCUTAU " + 
			      " WHERE " + 
			      " rconc = '" + ParaTipoConc + "' AND " + 
			      " (econc is null or econc = '" + ParaConcepto + "') AND " + 
			      " (gsocieda is null or gsocieda = " + ParaSociedad.ToString() + ") AND " + 
			      " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
			      " (gservici is null or gservici = " + lservicioprestacion.ToString() + ") AND " + 
			      " (gpersona is null or gpersona = " + ParaPersona.ToString() + ") AND " + 
			      " (ginspecc is null or ginspecc ='" + ParaInspeccion + "') AND " + 
			      " fborrado is null " + 
			      " ORDER BY iprioridad desc";

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, ParacnConexion);
			rr = new DataSet();
			tempAdapter_3.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				rr.MoveFirst();
				if (Convert.ToString(rr.Tables[0].Rows[0][1]).Trim().ToUpper() == "S")
				{
					result = true;
				}
				if (result)
				{
					Sql = "Select Dsocieda from dsocieda with (nolock) where gsocieda=" + ParaSociedad.ToString();
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(Sql, ParacnConexion);
					rr = new DataSet();
					tempAdapter_4.Fill(rr);
					ParamSociedad = Convert.ToString(rr.Tables[0].Rows[0][0]).Trim();


					Sql = "Select dtiptarj.gtarjeta + dentipac.nafiliac,dentipac.nversion,dpacient.gprovinc,iversio,idcontrol,dtiptarj.CodEnt,dtiptarj.CodUsu,dtiptarj.ClavOp,dtiptarj.codinf from  dentipac  inner join dpacient  on dpacient.gidenpac=dentipac.gidenpac inner join dtiptarj on dentipac.gsocieda=dtiptarj.gsocieda where dentipac.gidenpac='" + gidenpac + "'  and dentipac.gsocieda=" + ParaSociedad.ToString();
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(Sql, ParacnConexion);
					rr = new DataSet();
					tempAdapter_5.Fill(rr);
					if (rr.Tables[0].Rows.Count > 0)
					{
						tarjeta = Convert.ToString(rr.Tables[0].Rows[0][0]) + "";
						Version = Convert.ToString(rr.Tables[0].Rows[0][1]) + "";
						Provincia = Convert.ToString(rr.Tables[0].Rows[0][2]) + "";
						if (Convert.IsDBNull(rr.Tables[0].Rows[0][5]) || Convert.IsDBNull(rr.Tables[0].Rows[0][6]) || Convert.IsDBNull(rr.Tables[0].Rows[0][7]))
						{
							clasemensaje = new Mensajes.ClassMensajes();
							string tempRefParam = "Filiaci�n";
							string tempRefParam2 = "";
							short tempRefParam3 = 4014;
							string[] tempRefParam4 = new string[]{""};
							imensaje = Convert.ToInt32(clasemensaje.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, ParacnConexion, tempRefParam4));
							return true;
						}
						codEntidad = Convert.ToString(rr.Tables[0].Rows[0][5]).Trim();
						codUsuario = Convert.ToString(rr.Tables[0].Rows[0][6]).Trim();
						claveOperativa = Convert.ToString(rr.Tables[0].Rows[0][7]).Trim();
						CodInfra = Convert.ToString(rr.Tables[0].Rows[0][8]).Trim();
						if ((tarjeta == "") && !Convert.IsDBNull(rr.Tables[0].Rows[0]["idcontrol"]))
						{
							MessageBox.Show("Debe de registrar la tarjeta.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return true;
						}
					}

					if (ParaPersona > 0)
					{
						Sql = "Select dnif,ncolegia,dnompers,dap1pers from dpersona with (nolock) where gpersona=" + ParaPersona.ToString();
						SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(Sql, ParacnConexion);
						rr = new DataSet();
						tempAdapter_6.Fill(rr);
						if (rr.Tables[0].Rows.Count > 0)
						{
							nifProf = Convert.ToString(rr.Tables[0].Rows[0][0]) + "";
							numcole = Convert.ToString(rr.Tables[0].Rows[0][1]) + "";
							if (nifProf == "" || numcole == "")
							{

								//        AceptaOnline = False
							}
						}
						else
						{

							//            AceptaOnline = False
						}
					}
				}
				// numDU , CodDiag ,
			}


		 
			rr.Close();
			return result;
		}
		//FUNCION bRegistraSolicitud
		//Devuelve FALSE si no se ha producido ningun error al registrar la solicitud
		//Devuelve TRUE si se ha producido algun error en el proceso de registrar
		internal static bool bRegistraSolicitud(ref SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaCantidad, string ParaGidenpac, ref string ParafPrevact, ref string ParafRealiza, string ParaTipoActividad, string ParaUsuario, ref int sANOAUT, ref int sNUMAUT, string ParaNumeroDu = "", string Paragsersoli = "", string Paragpersoli = "", string Paragdiagnos = "", string paraTipoIngresoIntervencion = "", string ParaInspeccion = "")
		{
			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;

			Numeracion.SIGUIENTE lobjSiguiente = null;
			int lanoauto = 0;
			int lnumauto = 0;
			int lpersona = 0;
			int lservicioprestacion = 0;

			XmlDocument RespXML = null;
			//Set RespXML = New DOMDocument
			string Respuesta = String.Empty;
			string Fallo = String.Empty;
			// Variable en la que ponemos si esta preparado para la autorizacion OnLine.


			try
			{




				switch(ParaTipoConc)
				{
					case "ES" : 
						Sql = "SELECT gservici FROM DSERVICI WHERE GSERVICI= " + ParaConcepto; 
						SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion); 
						rr = new DataSet(); 
						tempAdapter.Fill(rr); 
						if (rr.Tables[0].Rows.Count != 0)
						{
							lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
						} 
					  
						rr.Close(); 
						break;
					case "PR" : 
						Sql = "SELECT gservici FROM DCODPRES WHERE GPRESTAC= '" + ParaConcepto + "'"; 
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion); 
						rr = new DataSet(); 
						tempAdapter_2.Fill(rr); 
						if (rr.Tables[0].Rows.Count != 0)
						{
							lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
						} 
					  
						rr.Close(); 
						break;
				}


				if (ParaTipoActividad == "P")
				{
					//UPGRADE_ISSUE: (2064) SSCalendarWidgets_A.WIConstants property WIConstants.Year was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					//lanoauto = (int) UpgradeStubs.SSCalendarWidgets_A_WIConstants.getYear(DateTime.Today);
                    lanoauto = (int)DateTime.Today.Year;
                    lobjSiguiente = new Numeracion.SIGUIENTE();
					string tempRefParam = "NROAUTOR";
					lnumauto = lobjSiguiente.SIG_NUMERO(tempRefParam, ParacnConexion);
					lobjSiguiente = null;
				}
				else
				{
					if (sANOAUT == 0 || sNUMAUT == 0)
					{
                        //UPGRADE_ISSUE: (2064) SSCalendarWidgets_A.WIConstants property WIConstants.Year was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        //lanoauto = (int)UpgradeStubs.SSCalendarWidgets_A_WIConstants.getYear(DateTime.Today);
                        lanoauto = (int) DateTime.Today.Year;
						lobjSiguiente = new Numeracion.SIGUIENTE();
						string tempRefParam2 = "NROAUTOR";
						lnumauto = lobjSiguiente.SIG_NUMERO(tempRefParam2, ParacnConexion);
						lobjSiguiente = null;
					}
					else
					{
						lanoauto = sANOAUT;
						lnumauto = sNUMAUT;
					}
				}


				//�apa NO SE PORQUE:
				//Al registrar una solicitud de autorizacion, se tendra en cuenta lo siguiente para rellenar el campo de
				//la persona: Se grabar� en gpersona el contenido del campo gpersona recuperado de la tabla NECESIDADES DE DOCUMENTACION (IFMSFA_ACTIVTAU),
				//es decir pasamos de la persona recibida y grabamos la de dicha tabla, segun pagina 4.1 del cuaderno de carga.
				Sql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
				      " (case when gsocieda is not null then 1 else 0 end)+ " + 
				      " (case when gregimen is not null then 1 else 0 end)+ " + 
				      " (case when gservici is not null then 1 else 0 end)+ " + 
				      " (case when gpersona is not null then 1 else 0 end)+ " + 
				      " (case when ginspecc is not null then 1 else 0 end), " + 
				      " gpersona " + 
				      " FROM IFMSFA_DOCUTAU " + 
				      " WHERE " + 
				      " rconc = '" + ParaTipoConc + "' AND " + 
				      " (econc is null or econc = '" + ParaConcepto + "') AND " + 
				      " (gsocieda is null or gsocieda = " + ParaSociedad.ToString() + ") AND " + 
				      " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
				      " (gservici is null or gservici = " + lservicioprestacion.ToString() + ") AND " + 
				      " (gpersona is null or gpersona = " + ParaPersona.ToString() + ") AND " + 
				      " (ginspecc is null or ginspecc ='" + ParaInspeccion + "') AND " + 
				      " fborrado is null " + 
				      " ORDER BY iprioridad desc";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, ParacnConexion);
				rr = new DataSet();
				tempAdapter_3.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					rr.MoveFirst();
					if (!Convert.IsDBNull(rr.Tables[0].Rows[0][1]))
					{
						lpersona = Convert.ToInt32(rr.Tables[0].Rows[0][1]);
					}
					else
					{
						lpersona = 0;
					}
				}
			 
				rr.Close();
				//----------- 12 Marzo 2007 REGISTRO DE ACTIVIDAD ELECTRONICA CEREZUELA ----------------------------------
				string Oper = String.Empty;
				string nOper = String.Empty;
				string tmov = String.Empty;
				string dnautori = String.Empty;
				string dnvolant = String.Empty;
				string dnopelec = String.Empty;
				string tarjeta = String.Empty;
				string codProf = String.Empty;
				string nifProf = String.Empty;
				string numcole = String.Empty;
				string CodDiag = String.Empty;
				string codEntidad = String.Empty;
				string codUsuario = String.Empty;
				string claveOperativa = String.Empty;
				string IinfPaci = String.Empty;
				string Version = String.Empty;
				string Provincia = String.Empty;
				string FechaTransaccion = String.Empty;
				string FechaTransaccionAux = String.Empty;
				string FechaSesion = String.Empty;
				string FechaSesionAux = String.Empty;
				string ParamSociedad = String.Empty;
				string iautoe = String.Empty;
				string CodInfra = String.Empty;
				Mensajes.ClassMensajes clasemensaje = null;
				int imensaje = 0;
				string RegimenAutorizador = String.Empty;


				Oper = "FAAU9";
				nOper = "000000";
				tmov = "AUTORIZA";
				iautoe = "N";

				if (ParaNumeroDu != "")
				{
					dnvolant = ParaNumeroDu;
				}

				//**************************PROGRAMACIONES*********************************************************/
				if (bGestionAutorizaciones(ParacnConexion) && AceptaOnline(ref Provincia, ParaGidenpac, ref tarjeta, ref Version, codProf, ref nifProf, ref numcole, ParaNumeroDu, Paragdiagnos, ref codEntidad, ref codUsuario, ref claveOperativa, ref ParacnConexion, ParaSociedad, ParaItiposer, lservicioprestacion, ParaPersona, ParaTipoConc, ParaConcepto, ref ParamSociedad, ref CodInfra, ParaInspeccion))
				{
					iautoe = "S";
					if (ParaTipoActividad == "P")
					{
						IinfPaci = "N";

						RegimenAutorizador = TransformaRegimen(ParacnConexion, ParaItiposer, ParaTipoActividad, lanoauto, lnumauto, lservicioprestacion, paraTipoIngresoIntervencion);
						string tempRefParam3 = SOAP.OFF_LINE;
						Respuesta = SOAP.SolAutorizacion(ParaTipoActividad, ParacnConexion, ref tempRefParam3, ParamSociedad, SOAP.FormatearFechaSoap(DateTime.Now.ToString()), "0100", codEntidad, codUsuario, "34", Provincia, claveOperativa, CodInfra, tarjeta.Trim(), Version, "", "", lanoauto.ToString() + "" + lnumauto.ToString(), ParaUsuario.ToUpper(), "0", "0", "", ParaTipoConc.Trim() + ParaConcepto.Trim(), Paragsersoli.Trim(), Paragsersoli.Trim(), ParaPersona.ToString(), nifProf, numcole, ParaNumeroDu, "", "1", RegimenAutorizador, Conversion.Str(lservicioprestacion).Trim(), Conversion.Str(lservicioprestacion).Trim(), ParaCantidad, SOAP.FormatearFechaSoap(ParafPrevact), SOAP.FormatearFechaSoap(ParafRealiza), "");
						RespXML = new XmlDocument();
						try
						{
							RespXML.LoadXml(Respuesta);
						}
						catch
						{
						}
						if (Respuesta != "")
						{
							Fallo = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/fallo").InnerText;
							if (Fallo == "")
							{
								dnautori = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/dnautori").InnerText;
								dnopelec = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/dnopelec").InnerText;
								dnvolant = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/dnvolant").InnerText;
								FechaTransaccionAux = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/fechatransaccion").InnerText;
								FechaTransaccion = SOAP.SOAPFormateaFecha(FechaTransaccionAux);
								FechaSesionAux = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAutorizacionResponse/SolAutorizacionResult/fechasesion").InnerText;
								FechaSesion = SOAP.SOAPFormateaFecha(FechaSesionAux);
								//Codigo de accion
								clasemensaje = new Mensajes.ClassMensajes();
								string tempRefParam4 = "Autorizaciones";
								string tempRefParam5 = "";
								short tempRefParam6 = 4012;
								string[] tempRefParam7 = new string[]{dnautori + "/"};
								imensaje = Convert.ToInt32(clasemensaje.RespuestaMensaje(tempRefParam4, tempRefParam5, tempRefParam6, ParacnConexion, tempRefParam7));
							}
							else
							{
								if (Fallo == SOAP.OFF_LINE)
								{
									//Set clasemensaje = CreateObject("Mensajes.ClassMensajes")
									//imensaje = clasemensaje.RespuestaMensaje("Filiaci�n", "", 4011, ParacnConexion, Fallo)
								}
								else
								{
									//Set clasemensaje = CreateObject("Mensajes.ClassMensajes")
									//imensaje = clasemensaje.RespuestaMensaje("Filiaci�n", "", 4011, ParacnConexion, Fallo)
								}
							}
						}
						else
						{
							//Set clasemensaje = CreateObject("Mensajes.ClassMensajes")
							//imensaje = clasemensaje.RespuestaMensaje("Filiaci�n", "", 4011, ParacnConexion, Fallo)
						}
					}
				}
				//----------- FIN REGISTRO DE ACTIVIDAD ELECTRONICA CEREZUELA----------------------------------

				// Vemos si es autorizable por ITC

				if (bAutArgentina.bGestionITC(ParacnConexion) && bAutArgentina.bUtilizaITC(ParacnConexion, ParaSociedad, ParaItiposer, lpersona, ParaTipoConc, ParaConcepto))
				{

					iautoe = "I";

				}
				else if (bAutArgentina.bGestionTraditum(ParacnConexion) && bAutArgentina.bUtilizaTraditum(ParacnConexion, ParaSociedad, ParaItiposer, lpersona, ParaTipoConc, ParaConcepto))
				{ 

					iautoe = "T";

				}
				else if (bGestionBlackBox(ParacnConexion) && bUtilizaBlackBox(ParacnConexion, ParaSociedad, ParaItiposer, lpersona, ParaTipoConc, ParaConcepto))
				{ 

					iautoe = "B";

				}

				string stFiliacion = String.Empty;
				stFiliacion = fObtenerNafiliac(ParacnConexion, ParaGidenpac, ParaSociedad, ParaInspeccion);


				Sql = "INSERT INTO IFMSFA_AUTORIZA " + 
				      "   (ganoauto, gnumauto, gIdenpac," + 
				      "    gSocieda, gregimen, gservici, gPersona, " + 
				      "    rconc, econc, ncantida," + 
				      "    fprevact, fsoliaut,ftramite,fresoluc, gestsoli, " + 
				      "    gUsuario, oper, noper , tmov,dnautori,dnvolant,dnopelec,iautoe,IINFPACI, NAFILIAC, ginspecc)" + 
				      " VALUES (" + 
				      lanoauto.ToString() + ", " + lnumauto.ToString() + ", '" + ParaGidenpac + "', " + 
				      ParaSociedad.ToString() + ", '" + ParaItiposer + "', " + lservicioprestacion.ToString() + ", ";
				if (lpersona != 0)
				{
					Sql = Sql + lpersona.ToString() + ", ";
				}
				else
				{
					Sql = Sql + " NULL, ";
				}
				object tempRefParam8 = ParafPrevact;
				object tempRefParam9 = ParafRealiza;
				Sql = Sql + 
				      "'" + ParaTipoConc + "', '" + ParaConcepto + "', '" + ParaCantidad + "', " + 
				      Serrores.FormatFechaHMS(tempRefParam8) + ", " + Serrores.FormatFechaHMS(tempRefParam9) + ",";
				ParafRealiza = Convert.ToString(tempRefParam9);
				ParafPrevact = Convert.ToString(tempRefParam8);
				if (FechaTransaccion != "")
				{
					object tempRefParam10 = FechaTransaccion;
					Sql = Sql + "" + Serrores.FormatFechaHMS(tempRefParam10) + ",";
					FechaTransaccion = Convert.ToString(tempRefParam10);
				}
				else
				{
					Sql = Sql + "NULL, ";
				}
				if (FechaSesion != "")
				{
					object tempRefParam11 = FechaSesion;
					Sql = Sql + "" + Serrores.FormatFechaHMS(tempRefParam11) + ",";
					FechaSesion = Convert.ToString(tempRefParam11);
				}
				else
				{
					Sql = Sql + "NULL, ";
				}

				Sql = Sql + "'P','" + ParaUsuario.ToUpper() + "','" + Oper + "','" + nOper + "','" + tmov + "',";

				if (dnautori != "")
				{
					Sql = Sql + "'" + dnautori + "',";
				}
				else
				{
					Sql = Sql + "NULL, ";
				}

				if (dnvolant != "")
				{
					Sql = Sql + "'" + dnvolant + "',";
				}
				else
				{
					Sql = Sql + "NULL, ";
				}

				if (dnopelec != "")
				{
					Sql = Sql + "'" + dnopelec + "',";
				}
				else
				{
					Sql = Sql + "NULL,";
				}

				if (iautoe != "")
				{
					Sql = Sql + "'" + iautoe + "',";
				}
				else
				{
					Sql = Sql + "'N',";
				}

				if (IinfPaci != "")
				{
					Sql = Sql + "'" + IinfPaci + "',";
				}
				else
				{
					Sql = Sql + "NULL,";
				}


				if (stFiliacion.Trim() != "")
				{ //14/11/2008 se a�ade la tarjeta  a la autorizacion
					Sql = Sql + "'" + stFiliacion + "',";
				}
				else
				{
					Sql = Sql + "NULL,";
				}

				if (ParaInspeccion.Trim() != "")
				{
					Sql = Sql + "'" + ParaInspeccion + "'";
				}
				else
				{
					Sql = Sql + "NULL";
				}

				Sql = Sql + ")";

				//" & dnautori & "','" & dnvolant & "','" & dnopelec & "')"

				SqlCommand tempCommand = new SqlCommand(Sql, ParacnConexion);
				tempCommand.ExecuteNonQuery();

				sANOAUT = lanoauto;
				sNUMAUT = lnumauto;

				return result;
			}
			catch(Exception Exep)
			{

				result = true;
				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), ParaUsuario, "bAutorizaciones: bRegistraSolicitud (" + Sql + ")", Exep);

				return result;
			}
		}

		//FUNCION bAsociaSolicitud
		//Devuelve FALSE si no se ha producido ningun error al asociar la solicitud
		//Devuelve TRUE si se ha producido algun error en el proceso de asociacion
		internal static bool bAsociaSolicitud(SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaTipoActividad, string ParaItiposer, int ParaAnoRegi, int ParaNumRegi, ref string ParafRealiza, string ParaTipoConc, string ParaConcepto, string ParaCantidad, string ParaUsuario)
		{
			bool result = false;
			string Sql = String.Empty;
			try
			{


				object tempRefParam = ParafRealiza;
				object tempRefParam2 = DateTime.Today;
				Sql = "INSERT INTO IFMSFA_ACTIVTAU (" + 
				      "    ganoauto, gnumauto, " + 
				      "    itipacti, itiposer, ganoregi, gnumregi, " + 
				      "    frealiza, rconc, econc, " + 
				      "    ncantidad , fasociac, " + 
				      "    gUsuario, oper, noper, tmov )" + 
				      " VALUES (" + 
				      ParaAnoAuto.ToString() + ", " + ParaNumAuto.ToString() + ", " + 
				      "'" + ParaTipoActividad + "', '" + ParaItiposer + "', " + ParaAnoRegi.ToString() + "," + ParaNumRegi.ToString() + "," + 
				      Serrores.FormatFechaHMS(tempRefParam) + ", '" + ParaTipoConc + "', '" + ParaConcepto + "', " + 
				      ParaCantidad + "," + Serrores.FormatFechaHMS(tempRefParam2) + "," + 
				      "'" + ParaUsuario.ToUpper() + "','FAAU9','000000','ACTIVTAU')";
				ParafRealiza = Convert.ToString(tempRefParam);

				SqlCommand tempCommand = new SqlCommand(Sql, ParacnConexion);
				tempCommand.ExecuteNonQuery();

				return result;
			}
			catch(Exception Exep)
			{

				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), ParaUsuario, "bAutorizaciones: AsociaSolicitud (" + Sql + ")", Exep);
				return true;
			}
		}

		//FUNCION fApruebaSolicitud
		//Devuelve FALSE si no se ha producido ningun error al aprobar la solicitud
		//Devuelve TRUE si se ha producido algun error en el proceso de aprobar

		internal static bool bApruebaSolicitud(ref SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string StOperacion, ref string iautoe, string Fallo, string FechaTransaccionAux, string FechaSesionAux, ref string stCopago)
		{

			bool result = false;
			string Sql = String.Empty;
			try
			{

				stSeparadordecimalBD = Serrores.ObtenerSeparadorDecimalBD(ParacnConexion);

				Sql = " UPDATE IFMSFA_AUTORIZA SET " + 
				      "    gestsoli='A', " + 
				      "    gusuario='" + ParaUsuario.ToUpper() + "'," + 
				      "    ncantau=" + ParaCantidad + "," + 
				      "    ncantaut=" + ParaCantidad + "," + 
				      "    ncantida=" + ParaCantidad + "," + 
				      "    iinfpaci='S',";

				if (ParaNAutoriz == "")
				{
					Sql = Sql + " dnautori=NULL,";
				}
				else
				{
					Sql = Sql + " dnautori='" + ParaNAutoriz + "',";
				}

				if (ParaNVolante == "")
				{
					Sql = Sql + " dnvolant=NULL,";
				}
				else
				{
					Sql = Sql + " dnvolant='" + ParaNVolante + "',";
				}
				if (Fallo.Trim() == "")
				{
					Sql = Sql + " obsolici='" + Fallo + "',";
				}
				if (FechaSesionAux == "")
				{
					System.DateTime tempRefParam = DateTime.Today;
					Sql = Sql + " fresoluc=" + Serrores.FormatFecha(tempRefParam) + ",";
				}
				else
				{
					object tempRefParam2 = SOAP.SOAPFormateaFecha(FechaSesionAux);
					Sql = Sql + " fresoluc=" + Serrores.FormatFechaHMS(tempRefParam2) + ",";
				}
				if (FechaTransaccionAux == "")
				{
					System.DateTime tempRefParam3 = DateTime.Today;
					Sql = Sql + " ftramite=" + Serrores.FormatFecha(tempRefParam3) + ",";
				}
				else
				{
					object tempRefParam4 = SOAP.SOAPFormateaFecha(FechaTransaccionAux);
					Sql = Sql + " ftramite=" + Serrores.FormatFechaHMS(tempRefParam4) + ",";
				}
				if (StOperacion == "")
				{
					Sql = Sql + " dnopelec=NULL,";
				}
				else
				{
					Sql = Sql + " dnopelec='" + StOperacion + "',";
				}

				//Oscar C Agosto 2011. Copago
				if (stCopago.Trim() == "")
				{
					Sql = Sql + " cpagpaci=NULL";
				}
				else
				{
					Sql = Sql + " cpagpaci=" + Serrores.ConvertirDecimales(ref stCopago, ref stSeparadordecimalBD, 2);
				}
				//---------

				Sql = Sql + 
				      " WHERE " + 
				      "    ganoauto=" + ParaAnoAuto.ToString() + " AND " + 
				      "    gnumauto=" + ParaNumAuto.ToString();

				SqlCommand tempCommand = new SqlCommand(Sql, ParacnConexion);
				tempCommand.ExecuteNonQuery();


				//Si se aprueba una Autorizacion Quirurgica, se debe generar, si no existe ya,
				//una autorizacion para la posible estancia en el servicio de hospitalizacion correspondiente a la intervencion.
				string sql2 = String.Empty;
				DataSet rr = null;
				DataSet rr2 = null;
				int lServicio = 0;
				//UPGRADE_ISSUE: (2068) WIConstants object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
				//UpgradeStubs.SSCalendarWidgets_A_WIConstantsEnum lanoauto = (UpgradeStubs.SSCalendarWidgets_A_WIConstantsEnum) 0;
                int lanoauto   = DateTime.Now.Year;
                int lnumauto = 0;
				Numeracion.SIGUIENTE lobjSiguiente = null;
				bool bNAIngreso = false;

				Sql = "SELECT count(*) FROM SCONSGLO WHERE GCONSGLO = 'HISREUTI' and valfanu1='S'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion);
				rr = new DataSet();
				tempAdapter.Fill(rr);
				//control del b�squeda de historia activa.
				bNAIngreso = (Convert.ToDouble(rr.Tables[0].Rows[0][0]) == 1);
			 
				rr.Close();


				Sql = "SELECT * FROM IFMSFA_AUTORIZA " + 
				      "  WHERE ganoauto = " + ParaAnoAuto.ToString() + " AND " + 
				      "    gnumauto=" + ParaNumAuto.ToString();
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion);
				rr = new DataSet();
				tempAdapter_2.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					if (Convert.IsDBNull(rr.Tables[0].Rows[0]["iautoe"]))
					{
						iautoe = "N";
					}
					else
					{
						iautoe = Convert.ToString(rr.Tables[0].Rows[0]["iautoe"]);
					}
					if (Convert.ToString(rr.Tables[0].Rows[0]["gregimen"]) == "Q" && Convert.ToString(rr.Tables[0].Rows[0]["rconc"]) == "PR" && ((iautoe == "S" && !bNAIngreso) || bNAIngreso))
					{

						sql2 = "select gservici FROM dservici WHERE " + 
						       " ggrupser = (select ggrupser from dservici " + 
						       "  where gservici = " + Convert.ToString(rr.Tables[0].Rows[0]["gservici"]) + ") and ihospita='S' and fborrado is null";
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql2, ParacnConexion);
						rr2 = new DataSet();
						tempAdapter_3.Fill(rr2);
						if (rr2.Tables[0].Rows.Count == 0)
						{
							return result;
						}
						else
						{
							rr2.MoveFirst();
							lServicio = Convert.ToInt32(rr2.Tables[0].Rows[0]["gservici"]);
						}
						rr2.Close();

						sql2 = "SELECT * FROM IFMSFA_AUTORIZA WHERE " + 
						       " gidenpac= '" + Convert.ToString(rr.Tables[0].Rows[0]["gidenpac"]) + "' AND  " + 
						       " gsocieda = " + Convert.ToString(rr.Tables[0].Rows[0]["gsocieda"]) + " AND " + 
						       " Gregimen ='H' AND " + 
						       " rconc ='ES' AND " + 
						       " econc = " + lServicio.ToString() + " AND " + 
						       " dnautori = '" + Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]) + "'";

						if (!Convert.IsDBNull(rr.Tables[0].Rows[0]["ginspecc"]))
						{
							sql2 = sql2 + " AND ginspecc ='" + Convert.ToString(rr.Tables[0].Rows[0]["ginspecc"]) + "'";
						}

						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql2, ParacnConexion);
						rr2 = new DataSet();
						tempAdapter_4.Fill(rr2);
						if (rr2.Tables[0].Rows.Count == 0)
						{

							//generamos una nueva autorizacion autorizada para la estancia

							//UPGRADE_ISSUE: (2064) SSCalendarWidgets_A.WIConstants property WIConstants.Year was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
							//lanoauto = UpgradeStubs.SSCalendarWidgets_A_WIConstants.getYear(DateTime.Today);
                            lanoauto = DateTime.Today.Year;
                            lobjSiguiente = new Numeracion.SIGUIENTE();
							string tempRefParam5 = "NROAUTOR";
							lnumauto = lobjSiguiente.SIG_NUMERO(tempRefParam5, ParacnConexion);
							lobjSiguiente = null;


							Sql = "INSERT INTO IFMSFA_AUTORIZA " + 
							      "   (ganoauto, gnumauto, gIdenpac," + 
							      "    gSocieda, gregimen, gservici, gPersona, " + 
							      "    rconc, econc, ncantida,ncantau,ncantaut," + 
							      "    fprevact, fsoliaut,ftramite,fresoluc, gestsoli, " + 
							      "    gUsuario, oper, noper , tmov,dnautori,dnvolant,dnopelec,iautoe,IINFPACI,nafiliac,ginspecc)" + 
							      " SELECT " + 
							      ((int) lanoauto).ToString() + "," + lnumauto.ToString() + ", gIdenpac," + 
							      "    gSocieda,'H', " + lServicio.ToString() + ", gPersona, " + 
							      "    'ES', " + lServicio.ToString() + ", 1,1,1," + 
							      "    fprevact, fsoliaut,ftramite,fresoluc, gestsoli, " + 
							      "    gUsuario, oper, noper , tmov,dnautori,dnvolant,dnopelec,isnull(iautoe,'N'),IINFPACI,nafiliac,ginspecc " + 
							      "    FROM IFMSFA_AUTORIZA WHERE " + 
							      "              ganoauto = " + ParaAnoAuto.ToString() + " AND gnumauto=" + ParaNumAuto.ToString();

							SqlCommand tempCommand_2 = new SqlCommand(Sql, ParacnConexion);
							tempCommand_2.ExecuteNonQuery();

							//------------------------------------------------------------
						}
						rr2.Close();
					}


				}

				rr.Close();

				return result;
			}
			catch(Exception Exep)
			{

				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), ParaUsuario, "bAutorizaciones: fApruebaSolicitud (" + Sql + ")", Exep);
				return true;
			}
		}

		internal static bool bApruebaSolicitud(ref SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string StOperacion, ref string iautoe, string Fallo, string FechaTransaccionAux, string FechaSesionAux)
		{
			string tempRefParam = String.Empty;
			return bApruebaSolicitud(ref ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, StOperacion, ref iautoe, Fallo, FechaTransaccionAux, FechaSesionAux, ref tempRefParam);
		}

		internal static bool bApruebaSolicitud(ref SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string StOperacion, ref string iautoe, string Fallo, string FechaTransaccionAux)
		{
			string tempRefParam2 = String.Empty;
			return bApruebaSolicitud(ref ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, StOperacion, ref iautoe, Fallo, FechaTransaccionAux, String.Empty, ref tempRefParam2);
		}

		internal static bool bApruebaSolicitud(ref SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string StOperacion, ref string iautoe, string Fallo)
		{
			string tempRefParam3 = String.Empty;
			return bApruebaSolicitud(ref ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, StOperacion, ref iautoe, Fallo, String.Empty, String.Empty, ref tempRefParam3);
		}

		internal static bool bApruebaSolicitud(ref SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string StOperacion, ref string iautoe)
		{
			string tempRefParam4 = String.Empty;
			return bApruebaSolicitud(ref ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, StOperacion, ref iautoe, String.Empty, String.Empty, String.Empty, ref tempRefParam4);
		}

		internal static bool bApruebaSolicitud(ref SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string StOperacion)
		{
			string tempRefParam5 = String.Empty;
			string tempRefParam6 = String.Empty;
			return bApruebaSolicitud(ref ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, StOperacion, ref tempRefParam5, String.Empty, String.Empty, String.Empty, ref tempRefParam6);
		}

		internal static bool bApruebaSolicitud(ref SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante)
		{
			string tempRefParam7 = String.Empty;
			string tempRefParam8 = String.Empty;
			return bApruebaSolicitud(ref ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, String.Empty, ref tempRefParam7, String.Empty, String.Empty, String.Empty, ref tempRefParam8);
		}


		internal static string BuscaDescripcion(SqlConnection ParaConexion, string ParaiTipoConc, string ParaConcepto)
		{
			string result = String.Empty;
			string Sql = String.Empty;
			DataSet rr = null;
			switch(ParaiTipoConc)
			{
				case "PR" : 
					Sql = "SELECT DPRESTAC FROM DCODPRES WHERE GPRESTAC= '" + ParaConcepto + "'"; 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParaConexion); 
					rr = new DataSet(); 
					tempAdapter.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						result = Convert.ToString(rr.Tables[0].Rows[0]["DPRESTAC"]);
					} 
				  
					rr.Close(); 
					break;
				case "ES" : 
					Sql = "SELECT DNOMSERV FROM DSERVICI WHERE GSERVICI= " + ParaConcepto; 
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParaConexion); 
					rr = new DataSet(); 
					tempAdapter_2.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						result = "ESTANCIAS PARA " + Convert.ToString(rr.Tables[0].Rows[0]["DNOMSERV"]).Trim().ToUpper();
					}
					else
					{
						result = "ESTANCIAS";
					} 
				  
					rr.Close(); 
					break;
			}
			return result;
		}


		internal static string bEstadoSolicitud(SqlConnection ParaConexion, int paraANOAUT, int paraNUMAUT)
		{
			string result = String.Empty;
			string Sql = " SELECT gestsoli FROM IFMSFA_AUTORIZA " + 
			             " WHERE ganoauto= " + paraANOAUT.ToString() + " AND " + 
			             "   gnumauto=" + paraNUMAUT.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParaConexion);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(rr.Tables[0].Rows[0][0]).Trim().ToUpper();
			}
		 
			rr.Close();
			return result;
		}


		//FUNCION bActualizaSolicitudAprobada
		//Devuelve FALSE si no se ha producido ningun error al actualiza la solicitud
		//Devuelve TRUE si se ha producido algun error en el proceso de actualizar
		internal static bool bActualizaSolicitudAprobada(SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string ParaNOper, string FechaTransaccionAux, string FechaSesionAux, ref string stCopago)
		{

			bool result = false;
			string Sql = String.Empty;
			try
			{

				stSeparadordecimalBD = Serrores.ObtenerSeparadorDecimalBD(ParacnConexion);

				Sql = " UPDATE IFMSFA_AUTORIZA SET " + 
				      "    gusuario='" + ParaUsuario.ToUpper() + "'," + 
				      "    ncantau=" + ParaCantidad + "," + 
				      "    ncantaut=" + ParaCantidad + "," + 
				      "    ncantida=" + ParaCantidad + ",";

				if (ParaNAutoriz == "")
				{
					Sql = Sql + " dnautori=NULL,";
				}
				else
				{
					Sql = Sql + " dnautori='" + ParaNAutoriz + "',";
				}

				if (ParaNOper == "")
				{
					Sql = Sql + " dnopelec=NULL,";
				}
				else
				{
					Sql = Sql + " dnopelec='" + ParaNOper + "',";
				}
				if (FechaSesionAux == "")
				{
					Sql = Sql + " fresoluc=NULL,";
				}
				else
				{
					object tempRefParam = SOAP.SOAPFormateaFecha(FechaSesionAux);
					Sql = Sql + " fresoluc=" + Serrores.FormatFechaHMS(tempRefParam) + ",";
				}
				if (FechaTransaccionAux == "")
				{
					Sql = Sql + " ftramite=NULL,";
				}
				else
				{
					object tempRefParam2 = SOAP.SOAPFormateaFecha(FechaTransaccionAux);
					Sql = Sql + " ftramite=" + Serrores.FormatFechaHMS(tempRefParam2) + ",";
				}
				if (ParaNVolante == "")
				{
					Sql = Sql + " dnvolant=NULL,";
				}
				else
				{
					Sql = Sql + " dnvolant='" + ParaNVolante + "',";
				}

				//Oscar C Agosto 2011. Copago
				if (stCopago.Trim() == "")
				{
					Sql = Sql + " cpagpaci=NULL";
				}
				else
				{
					Sql = Sql + " cpagpaci=" + Serrores.ConvertirDecimales(ref stCopago, ref stSeparadordecimalBD, 2) + "";
				}
				//---------

				Sql = Sql + 
				      " WHERE " + 
				      "    ganoauto=" + ParaAnoAuto.ToString() + " AND " + 
				      "    gnumauto=" + ParaNumAuto.ToString() + " AND " + 
				      "  ( (ncantida<>" + ParaCantidad + ") or" + 
				      "    (ISNULL(dnautori,'') <> '" + ParaNAutoriz + "') or" + 
				      "    (ISNULL(dnvolant,'') <> '" + ParaNVolante + "') or" + 
				      "    (ISNULL(cpagpaci,0) <> " + ((stCopago == "") ? "0" : Serrores.ConvertirDecimales(ref stCopago, ref stSeparadordecimalBD, 2)) + ") )";

				SqlCommand tempCommand = new SqlCommand(Sql, ParacnConexion);
				tempCommand.ExecuteNonQuery();

				return result;
			}
			catch(Exception Exep)
			{

				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), ParaUsuario, "bAutorizaciones: bActualizaSolicitudAprobada (" + Sql + ")", Exep);
				return true;
			}
		}

		internal static bool bActualizaSolicitudAprobada(SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string ParaNOper, string FechaTransaccionAux, string FechaSesionAux)
		{
			string tempRefParam9 = String.Empty;
			return bActualizaSolicitudAprobada(ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, ParaNOper, FechaTransaccionAux, FechaSesionAux, ref tempRefParam9);
		}

		internal static bool bActualizaSolicitudAprobada(SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string ParaNOper, string FechaTransaccionAux)
		{
			string tempRefParam10 = String.Empty;
			return bActualizaSolicitudAprobada(ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, ParaNOper, FechaTransaccionAux, String.Empty, ref tempRefParam10);
		}

		internal static bool bActualizaSolicitudAprobada(SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante, string ParaNOper)
		{
			string tempRefParam11 = String.Empty;
			return bActualizaSolicitudAprobada(ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, ParaNOper, String.Empty, String.Empty, ref tempRefParam11);
		}

		internal static bool bActualizaSolicitudAprobada(SqlConnection ParacnConexion, int ParaAnoAuto, int ParaNumAuto, string ParaUsuario, string ParaCantidad, string ParaNAutoriz, string ParaNVolante)
		{
			string tempRefParam12 = String.Empty;
			return bActualizaSolicitudAprobada(ParacnConexion, ParaAnoAuto, ParaNumAuto, ParaUsuario, ParaCantidad, ParaNAutoriz, ParaNVolante, String.Empty, String.Empty, String.Empty, ref tempRefParam12);
		}

		//FUNCION bNecesitaDocumentacion
		//Devuelve FALSE si ninguna actividad a realizar recibida necesita documentacion y /o volante
		//Devuelve TRUE si alguna actividad a realizar necesita documentacion y / o volante
		internal static bool bNecesitaDocumentacion(SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string[, ] ParaPrestaciones, string ParaInspeccion)
		{
			bool result = false;

			for (int i = 1; i <= ParaPrestaciones.GetUpperBound(0); i++)
			{
				if (bNecesitaAutorizacion(ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaPrestaciones[i, 1], ParaPrestaciones[i, 2], ParaInspeccion) || bNecesitaVolante(ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaPrestaciones[i, 1], ParaPrestaciones[i, 2], ParaInspeccion))
				{

					return true;

				}
			}

			return result;
		}

		internal static bool bTieneAutorizacionOnline(SqlConnection ParacnConexion, string ParaTipoActividad, string ParaItiposer, int ParaAno, int ParaNum, string ParaUsuario)
		{

			bool result = false;
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;

				// Valores por defecto


				// Si no hay gesti�n de autorizaciones, salimos

				if (!bGestionAutorizaciones(ParacnConexion))
				{
					return result;
				}

				// Si hay, buscamos el n�mero de autorizaci�n y si fue de Traditum o ITC

				stSql = "SELECT isNull(A.iautoe, 'N') iautoe, isNull(dnopelec, ' ') dnopelec FROM IFMSFA_AUTORIZA A " + 
				        "INNER JOIN IFMSFA_ACTIVTAU B ON " + 
				        "A.ganoauto = B.ganoauto AND " + 
				        "A.gnumauto = B.gnumauto AND " + 
				        "B.itipacti = '" + ParaTipoActividad + "' AND " + 
				        "B.itiposer = '" + ParaItiposer + "' AND " + 
				        "B.ganoregi = " + ParaAno.ToString() + " AND " + 
				        "B.gnumregi = " + ParaNum.ToString();

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{
                    result = (Convert.ToString(RrDatos.Tables[0].Rows[0]["dnopelec"]).Trim() != "") && (Convert.ToString(RrDatos.Tables[0].Rows[0]["iautoe"]).Trim().ToUpper() == "T" || Convert.ToString(RrDatos.Tables[0].Rows[0]["iautoe"]).Trim().ToUpper() == "I");

				}
				 
				RrDatos.Close();

				return result;
			}
			catch(Exception Exep)
			{

				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), ParaUsuario, "clsAutorizaciones: fTieneAutorizacionOnline", Exep);

				return false;
			}
		}

        //FUNCION bAnulaSolicitud
        //Devuelve FALSE si no se ha producido ningun error al anular la solicitud
        //Devuelve TRUE si se ha producido algun error en el proceso de anulacion
        internal static bool bAnulaEpisodio(ref SqlConnection ParacnConexion, string ParaTipoActividad, string ParaItiposer, int ParaAno, int ParaNum, string ParaUsuario, string stAccion = "")
        {

            bool result = false;
            int lAnoSol = 0;
            int lNumSol = 0;
            string Sql = String.Empty;
            DataSet rr = null;

            try
            {



                if (!bGestionAutorizaciones(ParacnConexion))
                {
                    return result;
                }

                //----------- 21 Marzo 2007 REGISTRO DE ACTIVIDAD ELECTRONICA CEREZUELA ----------------------------------
                string Oper = String.Empty;
                string nOper = String.Empty;
                string tmov = String.Empty;
                string tarjeta = String.Empty;
                string codProf = String.Empty;
                string nifProf = String.Empty;
                string numcole = String.Empty;
                string CodDiag = String.Empty;
                string codEntidad = String.Empty;
                string codUsuario = String.Empty;
                string claveOperativa = String.Empty;
                string Version = String.Empty;
                string Provincia = String.Empty;
                string ParamSociedad = String.Empty;
                string iautoe = String.Empty;
                Oper = "FAAU9";
                nOper = "000000";
                tmov = "AUTORIZA";
                iautoe = "N";
                string ParaGidenpac = String.Empty;
                string NumDu = String.Empty;
                int ParaSociedad = 0;
                string ParaInspeccion = String.Empty;
                int lservicioprestacion = 0;
                int ParaPersona = 0;
                int ParaCantidad = 0;
                string ParaTipoConc = String.Empty;
                string ParaConcepto = String.Empty;
                string fechaSesionOpOriginal = String.Empty;
                string fechaOpOriginal = String.Empty;
                string idOpOriginal = String.Empty;
                string numAutorizacion = String.Empty;
                string fecharealizacion = String.Empty;
                string Respuesta = String.Empty;
                XmlDocument RespXML = null;
                //Set RespXML = New DOMDocument
                string Fallo = String.Empty;
                string CodInfra = String.Empty;
                string OperacionAnula = String.Empty;
                string CodAcc = String.Empty;
                string numoper = String.Empty;
                string Obsolici = String.Empty;

                string EstadoPeticion = String.Empty;

                Mensajes.ClassMensajes clasemensaje = null;
                int imensaje = 0;


                numoper = "";
                Sql = "SELECT ganoauto, gnumauto FROM IFMSFA_ACTIVTAU " +
                      " WHERE " +
                      "    itipacti='" + ParaTipoActividad + "' AND " +
                      "    itiposer='" + ParaItiposer + "' AND " +
                      "    ganoregi=" + ParaAno.ToString() + " AND " +
                      "    gnumregi= " + ParaNum.ToString();
                SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion);
                rr = new DataSet();
                tempAdapter.Fill(rr);
                if (rr.Tables[0].Rows.Count != 0)
                {
                    lAnoSol = Convert.ToInt32(rr.Tables[0].Rows[0]["ganoauto"]);
                    lNumSol = Convert.ToInt32(rr.Tables[0].Rows[0]["gnumauto"]);
                }
                else
                {

                }

                rr.Close();

                Sql = "SELECT * FROM IFMSFA_Autoriza " +
                      " WHERE " +
                      "    ganoauto='" + lAnoSol.ToString() + "' AND " +
                      "    gnumauto='" + lNumSol.ToString() + "'";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion);
                rr = new DataSet();
                tempAdapter_2.Fill(rr);
                //iautoe = ""
                if (rr.Tables[0].Rows.Count != 0)
                {
                    if (Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]) == "D")
                    {
                        goto sigue;
                    }
                    ParaGidenpac = Convert.ToString(rr.Tables[0].Rows[0]["Gidenpac"]);
                    NumDu = Convert.ToString(rr.Tables[0].Rows[0]["dnvolant"]) + "";
                    ParaSociedad = Convert.ToInt32(rr.Tables[0].Rows[0]["gsocieda"]);
                    ParaInspeccion = Convert.ToString(rr.Tables[0].Rows[0]["ginspecc"]) + "";

                    EstadoPeticion = Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]) + "";

                    ParaCantidad = Convert.ToInt32(Conversion.Val("0" + Convert.ToString(rr.Tables[0].Rows[0]["ncantau"])));

                    lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
                    ParaPersona = Convert.ToInt32(Double.Parse("0" + Convert.ToString(rr.Tables[0].Rows[0]["gpersona"])));
                    ParaTipoConc = Convert.ToString(rr.Tables[0].Rows[0]["rconc"]) + "";
                    ParaConcepto = Convert.ToString(rr.Tables[0].Rows[0]["econc"]) + "";
                    fechaSesionOpOriginal = Convert.ToString(rr.Tables[0].Rows[0]["fresoluc"]) + "";
                    fechaOpOriginal = Convert.ToString(rr.Tables[0].Rows[0]["ftramite"]) + "";
                    idOpOriginal = Convert.ToString(rr.Tables[0].Rows[0]["dnopelec"]) + "";
                    numAutorizacion = Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]) + "";
                    fecharealizacion = Convert.ToString(rr.Tables[0].Rows[0]["fprevact"]) + "";
                    if (Convert.IsDBNull(rr.Tables[0].Rows[0]["iautoe"]))
                    {
                        iautoe = "N";
                    }
                    else
                    {
                        iautoe = Convert.ToString(rr.Tables[0].Rows[0]["iautoe"]);
                    }
                    if (Convert.IsDBNull(rr.Tables[0].Rows[0]["obsolici"]))
                    {
                        Obsolici = "";
                    }
                    else
                    {
                        Obsolici = Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).Trim();
                    }
                    //If Not IsNull(rr("obsolici")) Then
                    if (Obsolici.IndexOf('*') >= 0)
                    {
                        OperacionAnula = "" + Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).Substring(0, Math.Min(Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).IndexOf('*'), Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).Length));
                    }
                    else
                    {
                        OperacionAnula = "";
                    }

                }

                rr.Close();
                //**************************CANCELACION ELETRONICA*********************************************************/
                if (iautoe == "S")
                {
                    if (bGestionAutorizaciones(ParacnConexion) && AceptaOnline(ref Provincia, ParaGidenpac, ref tarjeta, ref Version, codProf, ref nifProf, ref numcole, NumDu, CodDiag, ref codEntidad, ref codUsuario, ref claveOperativa, ref ParacnConexion, Convert.ToInt32(Conversion.Val(ParaSociedad.ToString())), ParaItiposer, lservicioprestacion, ParaPersona, ParaTipoConc, ParaConcepto, ref ParamSociedad, ref CodInfra, ParaInspeccion))
                    {
                        if (OperacionAnula != "")
                        {
                            iautoe = "S";
                            RespXML = new XmlDocument();
                            string tempRefParam = SOAP.OFF_LINE;
                            Respuesta = SOAP.SolAnulacion(ParaTipoActividad, ParacnConexion, ref tempRefParam, ParamSociedad, tarjeta, "020000", SOAP.FormatearFechaSoap(DateTime.Now.ToString()), codEntidad, codUsuario, lAnoSol.ToString() + "" + lNumSol.ToString(), ParaUsuario.ToUpper(), SOAP.FormatearFechaSoap(fechaSesionOpOriginal), SOAP.FormatearFechaSoap(fechaOpOriginal), OperacionAnula, numAutorizacion, "0", Conversion.Str(lservicioprestacion), Conversion.Str(lservicioprestacion), SOAP.FormatearFechaSoap(fecharealizacion), claveOperativa);
                            try
                            {
                                RespXML.LoadXml(Respuesta);
                            }
                            catch
                            {
                            }

                            UpgradeHelpers.Helpers.ErrorHandlingHelper.ResumeNext(
                                () => { CodAcc = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAnulacionResponse/SolAnulacionResult/CodAcc").InnerText; },
                                () => { Fallo = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAnulacionResponse/SolAnulacionResult/fallo").InnerText; },
                                () => { numoper = RespXML.SelectSingleNode("/soap:Envelope/soap:Body/SolAnulacionResponse/SolAnulacionResult/Numoper").InnerText; });
                            if (Respuesta == "")
                            {
                                //Set clasemensaje = CreateObject("Mensajes.ClassMensajes")
                                //imensaje = clasemensaje.RespuestaMensaje("Filiaci�n", "", 4011, ParacnConexion, Fallo)
                            }
                            else
                            {
                                if (CodAcc == "102")
                                {
                                    clasemensaje = new Mensajes.ClassMensajes();
                                    string tempRefParam2 = "Autorizaciones";
                                    string tempRefParam3 = "";
                                    short tempRefParam4 = 4013;
                                    string[] tempRefParam5 = new string[] { numoper };
                                    imensaje = Convert.ToInt32(clasemensaje.RespuestaMensaje(tempRefParam2, tempRefParam3, tempRefParam4, ParacnConexion, tempRefParam5));
                                }
                                else
                                {
                                    if (CodAcc == "000")
                                    {
                                        //Set clasemensaje = CreateObject("Mensajes.ClassMensajes")
                                        //imensaje = clasemensaje.RespuestaMensaje("Autorizaciones", "", 4015, ParacnConexion, "/" & numoper)
                                    }
                                    else
                                    {
                                        if (CodAcc == "" || CodAcc == "908" || CodAcc == "400")
                                        {
                                            //Set clasemensaje = CreateObject("Mensajes.ClassMensajes")
                                            //imensaje = clasemensaje.RespuestaMensaje("Filiaci�n", "", 4011, ParacnConexion, Fallo)
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Sql = "delete from ENVIOS_SOLAUTORIZACION where ganoauto=" + lAnoSol.ToString() + " and gnumauto=" + lNumSol.ToString() + " and idcentro='" + claveOperativa + "' and itipomov='A' ";
                            SqlCommand tempCommand = new SqlCommand(Sql, ParacnConexion);
                            tempCommand.ExecuteNonQuery();
                        }
                    }
                }
                //----------- FIN CANCELACION ELECTRONICA CEREZUELA----------------------------------

                // Comprobamos si hay autorizaci�n con ITC
                if (iautoe == "I" && EstadoPeticion == "A")
                {

                    //MsgBox idOpOriginal

                    // Activamos ITC

                    if (bAutArgentina.bGestionITC(ParacnConexion))
                    {

                        // Anulamos la autorizaci�n

                        if (idOpOriginal.Trim().Length <= 6)
                        {
                            idOpOriginal = idOpOriginal.Trim();
                        }
                        else
                        {
                            idOpOriginal = idOpOriginal.Trim().Substring(idOpOriginal.Trim().Length - 6, Math.Min(6, 6));
                        }

                        bAutArgentina.fCancelaAutorizacionITC(ParacnConexion, ParaSociedad, ParaGidenpac, ref ParaConcepto, ParaCantidad, ParaItiposer, ParaTipoConc == "ES", idOpOriginal);

                    }

                }

                // Comprobamos si hay autorizaci�n con Traditum
                if (iautoe == "T" && EstadoPeticion == "A")
                {

                    // Activamos Traditum

                    if (bAutArgentina.bGestionTraditum(ParacnConexion))
                    {

                        // Anulamos la autorizaci�n

                        bAutArgentina.fCancelaAutorizacionTraditum(ParacnConexion, ParaSociedad, ParaGidenpac, ref ParaConcepto, ParaCantidad, ParaItiposer, lservicioprestacion, ParaTipoConc == "ES", idOpOriginal);

                    }

                }

                // Comprobamos si hay autorizaci�n con BlackBox
                if (iautoe == "B" && EstadoPeticion == "A" && stAccion != "M")
                {

                    // Anulamos la autorizaci�n

                    string tempRefParam6 = idOpOriginal.Trim();
                    fCancelaAutorizacionBlackBox(ref ParacnConexion, ref tempRefParam6, ref ParaGidenpac);

                }

            sigue:

                Sql = "DELETE IFMSFA_ACTIVTAU " +
                      " WHERE " +
                      "    itipacti='" + ParaTipoActividad + "' AND " +
                      "    itiposer='" + ParaItiposer + "' AND " +
                      "    ganoregi=" + ParaAno.ToString() + " AND " +
                      "    gnumregi= " + ParaNum.ToString();
                SqlCommand tempCommand_2 = new SqlCommand(Sql, ParacnConexion);
                tempCommand_2.ExecuteNonQuery();

                Sql = "SELECT COUNT (*) FROM IFMSFA_ACTIVTAU WHERE " +
                      " ganoauto=" + lAnoSol.ToString() + " AND " +
                      " gnumauto=" + lNumSol.ToString();
                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, ParacnConexion);
                rr = new DataSet();
                tempAdapter_3.Fill(rr);
                 if (Convert.ToDouble(rr.Tables[0].Rows[0][0]) == 0)
                {
                    if (iautoe == "I" || iautoe == "T" || (iautoe == "B" && stAccion != "M"))
                    {
                        Sql = " UPDATE IFMSFA_AUTORIZA SET " +
                              "    gestsoli='C', " +
                              "    fborrado=getdate()," +
                              "    gusuario='" + ParaUsuario.ToUpper() + "'," +
                              "    obsolici=obsolici + '" + numoper + "-C'" +
                              " WHERE " +
                              "    ganoauto=" + lAnoSol.ToString() + " AND " +
                              "    gnumauto=" + lNumSol.ToString();
                    }
                    else
                    {
                        Sql = " UPDATE IFMSFA_AUTORIZA SET " +
                              "    gestsoli='C', " +
                              "    fborrado=getdate()," +
                              "    gusuario='" + ParaUsuario.ToUpper() + "'," +
                              "    obsolici=obsolici + '" + numoper + "-C'" +
                              " WHERE " +
                              "    ganoauto=" + lAnoSol.ToString() + " AND " +
                              "    gnumauto=" + lNumSol.ToString() + " AND " +
                              "    gestsoli IN ('P','S')";
                    }

                    SqlCommand tempCommand_3 = new SqlCommand(Sql, ParacnConexion);
                    tempCommand_3.ExecuteNonQuery();
                }

                rr.Close();
                return result;
            }
            catch(Exception Exep)
            { 
            //errores:
			Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), ParaUsuario, "clsAutorizaciones: bAnulaSolicitud (" + Sql + ")", Exep);
			return true;
		    }
        }
        //FUNCION bRegistraTEAM
        //Devuelve FALSE si no se ha producido ningun error al registrar la Op. elec.
        //Devuelve TRUE si se ha producido algun error en el proceso de registrar la Op. elec.

        internal static bool bRegistraTEAM(SqlConnection ParaConexion, string ParaItiposer, int ParaAno, int ParaNum, string ParaTipoConc, string ParaConcepto, ref string ParaNOpelec, string ParaUsuario, string ParaAutoriza = "", bool ParaTEAM = true)
		{

			bool result = false;
			string stSql = String.Empty;
			DataSet rr = null;
			int PosAs = 0;
			string[] AEventos = null;
			string Eventos = String.Empty;
			string SeparadorEventos = String.Empty;
			bool bprestacionquirurgica = false;

			try
			{


				PosAs = (ParaNOpelec.IndexOf('*') + 1);

				if (ParaNOpelec.StartsWith("0") && ParaAutoriza.Trim() == "")
				{

					// Si no existe registro de TEAM, borramos su relaci�n

					stSql = "DELETE DEPITEAM WHERE " + 
					        "itiposer = '" + ParaItiposer + "' AND " + 
					        "ganoregi = " + ParaAno.ToString() + " AND " + 
					        "gnumregi = " + ParaNum.ToString() + " AND " + 
					        "rconc = '" + ParaTipoConc + "' AND " + 
					        "econc = '" + ParaConcepto + "'";

					SqlCommand tempCommand = new SqlCommand(stSql, ParaConexion);
					tempCommand.ExecuteNonQuery();

					// OSCAR C Agosto 2007
					// Adem�s se borrar�n todos los eventos de la prestaci�n Quir�rgica

					if (Serrores.ObternerValor_CTEGLOBAL(ParaConexion, "QTEAM", "VALFANU1") == "S" && ParaItiposer == "Q" && ParaTipoConc == "PR")
					{

						stSql = "DELETE DEPITEAM WHERE " + 
						        "itiposer = '" + ParaItiposer + "' AND " + 
						        "ganoregi = " + ParaAno.ToString() + " AND " + 
						        "gnumregi = " + ParaNum.ToString() + " AND " + 
						        "econc = '" + ParaConcepto + "'";

						SqlCommand tempCommand_2 = new SqlCommand(stSql, ParaConexion);
						tempCommand_2.ExecuteNonQuery();

					}

					//-----

				}
				else
				{

					stSql = "SELECT * FROM DEPITEAM WHERE " + 
					        "itiposer = '" + ParaItiposer + "' AND " + 
					        "ganoregi = " + ParaAno.ToString() + " AND " + 
					        "gnumregi = " + ParaNum.ToString() + " AND " + 
					        "rconc = '" + ParaTipoConc + "' AND " + 
					        "econc = '" + ParaConcepto + "'";

					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParaConexion);
					rr = new DataSet();
					tempAdapter.Fill(rr);

					string auxVar = (Convert.ToString(rr.Tables[0].Rows[0]["iquirofa"]) == "S").ToString().ToUpper();
					if (rr.Tables[0].Rows.Count != 0)
					{

						rr.Edit();

						if (ParaNOpelec.StartsWith("0"))
						{
							rr.Tables[0].Rows[0]["dnopelec"] = DBNull.Value;
						}
						else
						{
							rr.Tables[0].Rows[0]["dnopelec"] = ParaNOpelec.Substring(PosAs);
						}

						if (ParaAutoriza.Trim() == "")
						{
							rr.Tables[0].Rows[0]["dnautori"] = DBNull.Value;
						}
						else
						{
							rr.Tables[0].Rows[0]["dnautori"] = ParaAutoriza.Trim();
						}

						rr.Tables[0].Rows[0]["iteam"] = (ParaTEAM) ? "S" : "N";

						string tempQuery = rr.Tables[0].TableName;
						//SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                        tempAdapter.Update(rr, rr.Tables[0].TableName);

						// OSCAR C Agosto 2007
						// Adem�s se actualizar�n todos los eventos de la prestaci�n Quir�rgica

						if (Serrores.ObternerValor_CTEGLOBAL(ParaConexion, "QTEAM", "VALFANU1") == "S" && ParaItiposer == "Q" && ParaTipoConc == "PR")
						{

							stSql = "UPDATE DEPITEAM SET " + 
							        "dnopelec = " + ((ParaNOpelec.StartsWith("0")) ? "NULL" : "'" + ParaNOpelec.Substring(PosAs) + "'") + ", " + 
							        "dnautori = " + ((ParaAutoriza.Trim() == "") ? "NULL" : "'" + ParaAutoriza + "'") + ", " + 
							        "iteam = '" + ((ParaTEAM) ? "S" : "N") + "' WHERE " + 
							        "itiposer = '" + ParaItiposer + "' AND " + 
							        "ganoregi = " + ParaAno.ToString() + " AND " + 
							        "gnumregi = " + ParaNum.ToString() + " AND " + 
							        "econc = '" + ParaConcepto + "'";

							SqlCommand tempCommand_3 = new SqlCommand(stSql, ParaConexion);
							tempCommand_3.ExecuteNonQuery();

						}

						//-----

					}
					else
					{

						rr.AddNew();
						rr.Tables[0].Rows[0]["itiposer"] = ParaItiposer;
						rr.Tables[0].Rows[0]["ganoregi"] = ParaAno;
						rr.Tables[0].Rows[0]["gnumregi"] = ParaNum;
						rr.Tables[0].Rows[0]["rconc"] = ParaTipoConc;
						rr.Tables[0].Rows[0]["econc"] = ParaConcepto;
						rr.Tables[0].Rows[0]["iteam"] = (ParaTEAM) ? "S" : "N"; // "S"

						if (!ParaNOpelec.StartsWith("0"))
						{
								rr.Tables[0].Rows[0]["dnopelec"] = ParaNOpelec.Substring(PosAs);
						}

						if (ParaAutoriza.Trim() != "")
						{
							rr.Tables[0].Rows[0]["dnautori"] = ParaAutoriza.Trim();
						}

						string tempQuery_2 = rr.Tables[0].TableName;
						//SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery_2, "");
						SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
                        tempAdapter.Update(rr, rr.Tables[0].TableName);

						//OSCAR C Agosto 2007
						//Ademas se insertaran los eventos defindos en constante global de las prestacions Quirurgicas

						bprestacionquirurgica = false;

						stSql = "SELECT iquirofa FROM DSERVICI " + 
						        "INNER JOIN DCODPRES ON DCODPRES.gservici = DSERVICI.gservici " + 
						        "WHERE " + 
						        "gprestac = '" + ParaConcepto + "'";

						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, ParaConexion);
						rr = new DataSet();
						tempAdapter_4.Fill(rr);

						if (rr.Tables[0].Rows.Count != 0)
						{
							rr.MoveFirst();
							bool tempBool = false;
							if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
							{
								bprestacionquirurgica = true;
							}
						}

						if (Serrores.ObternerValor_CTEGLOBAL(ParaConexion, "QTEAM", "VALFANU1") == "S" && ParaItiposer == "Q" && ParaTipoConc == "PR" && bprestacionquirurgica)
						{

							Eventos = Serrores.ObternerValor_CTEGLOBAL(ParaConexion, "QTEAM", "VALFANU2");
							SeparadorEventos = Serrores.ObternerValor_CTEGLOBAL(ParaConexion, "QTEAM", "VALFANU3");

							if (Eventos != "" && SeparadorEventos != "")
							{

								Serrores.proDevolverARRAY(ref Eventos, SeparadorEventos, ref AEventos);

								for (int i = 0; i <= AEventos.GetUpperBound(0) - 1; i++)
								{

									if (AEventos[i].Length > 2)
									{
										AEventos[i] = AEventos[i].Substring(0, Math.Min(2, AEventos[i].Length));
									}

									stSql = "INSERT INTO DEPITEAM " + 
									        "(itiposer, ganoregi, gnumregi, " + 
									        "rconc, econc, iteam, " + 
									        "dnopelec, " + 
									        "dnautori)" + 
									        " VALUES " + 
									        "('" + ParaItiposer + "', " + ParaAno.ToString() + ", " + ParaNum.ToString() + ", " + 
									        "'" + AEventos[i] + "', " + "'" + ParaConcepto + "', '" + ((ParaTEAM) ? "S" : "N") + "', " + 
									        ((ParaNOpelec.StartsWith("0")) ? "NULL" : "'" + ParaNOpelec.Substring(PosAs) + "'") + ", " + 
									        ((ParaAutoriza.Trim() == "") ? "NULL" : "'" + ParaAutoriza.Trim() + "'") + ")";

									SqlCommand tempCommand_4 = new SqlCommand(stSql, ParaConexion);
									tempCommand_4.ExecuteNonQuery();

								}

							}

						}

						//-----

					}

				 
					rr.Close();

				}

				return result;
			}
			catch(Exception Exep)
			{

				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), ParaUsuario, "clsAutorizaciones: bRegistraTEAM", Exep);
				return true;
			}
		}



		internal static string TransformaRegimen(SqlConnection ParacnConexion, string TipoArea, string tipoActividad, int AnoRegi, int NumeroReg, int Servicio, string TipoIngreso = "")
		{
			string result = String.Empty;
			string Sql = String.Empty;
			DataSet rr = null;
			result = "999";



			switch(TipoArea)
			{
				case "C" : case "S" : 
					 
					result = "001"; 
					 
					break;
				case "Q" : 
					 
					result = "002"; 
					if (TipoIngreso.Trim() != "")
					{
						//SOLO VIENE DESDE QUIROFANOS PROGRAMACION E INTERVENCION QUIRURGICA
						//CUANDO SE ESPECIFICA TIPO DE INTERVENCION CON INGRESO
						//PERO POR SI ACASO DEJAMOS EL SELECT CASE
						switch(TipoIngreso)
						{
							case "I" :  
								result = "004";  //Ingreso debido a Proceso Quirurgico 
								break;
							case "A" :  
								result = "002";  //Cirujia Ambulante 
								break;
						}
					} 
					 
					break;
				case "H" : 
					 
					switch(tipoActividad)
					{
						case "P" : 
							 
							//--Actividad Programada de Area H (Reservas de Ingreso) 
							result = "005"; 

							 
							break;
						case "R" : 
							 
							//Actividad Realizada 
							result = "005";  //Por defecto ?? 
							 
							if (TipoIngreso.Trim() != "")
							{
								//SOLO VIENE DESDE INGRESO.DLL / INGRESOHD.DLL
								//(Ingresos en Hospitalizacion y Hospital de Dia)
								switch(TipoIngreso)
								{
									case "Q" :  
										result = "004";  //Ingreso debido a Proceso Quirurgico 
										break;
									case "M" :  
										result = "005";  //Ingreso debido a Proceso Medico 
										break;
									case "DM" :  
										result = "003";  //Ingreso en Hospital De D�a Medico 
										break;
									case "DQ" :  
										result = "003";  //Ingreso en Hospital De D�a Quirurgico 
										break;
									case "DD" :  
										result = "003";  //Ingreso en Centro de Dia 
										break;
								}
							}
							else
							{
								//ENTRARA AL REALIZAR UNA PRESTACION DE UN EPISODIO H
								if (AnoRegi > 0 && NumeroReg > 0)
								{
									Sql = "SELECT gtiproce, gtiplaza FROM AEPISADM " + 
									      " WHERE ganoadme=" + AnoRegi.ToString() + " AND " + 
									      " gnumadme=" + NumeroReg.ToString();
									SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion);
									rr = new DataSet();
									tempAdapter.Fill(rr);
									if (Convert.ToDouble(rr.Tables[0].Rows[0]["ganoadme"]) < 1900)
									{
										//Episosidos de Hospital de Dia
										result = "003"; //Ingreso en Hospital De D�a

									}
									else
									{
										//Episodios de Hospitalizacion
										if (!Convert.IsDBNull(rr.Tables[0].Rows[0]["gtiproce"]))
										{
											switch(Convert.ToString(rr.Tables[0].Rows[0]["gtiproce"]).Trim().ToUpper())
											{
												case "Q" :  
													result = "004";  //Ingreso debido a Proceso Quirurgico 
													break;
												case "M" :  
													result = "005";  //Ingreso debido a Proceso Medico 
													break;
											}
										}
									}
								}
							} 
							 
							break;
					} 
					 
					break;
				case "U" : 
					result = "006"; 
					 
					break;
			}



			return result;
		}



		//NO SE BORRAN LOS REGISTROS DE TEAM QUE SOBREN DEBIDO A
		//- MODIFICACION DE UNA ACTIVIDAD - BORRANDO OTRAS PRESTACIONES
		//- ANULACION LA ACTIVIDAD
		//A lo mejor se quiere que esto si se haga en el futuro y como ya esta programdado,
		//de momento se queda comentado.
		//Public Function bBorraTEAM(ParaConexion As rdoConnection, _
		//'                           ParaItiposer As String, ParaAno As Integer, ParaNum As Long, _
		//'                           ParaInfSolicitudes As Variant, ParaUsuario As String)
		//Dim sql As String
		//Dim aux As String
		//Dim rr As rdoResultset
		//Dim i As Integer
		//
		//On Error GoTo errores
		//
		//bBorraTEAM = False
		//
		//aux = ""
		//For i = 1 To UBound(ParaInfSolicitudes)
		//     aux = aux & "'" & ParaInfSolicitudes(i, 2) & "',"
		//Next
		//aux = Mid(aux, 1, Len(aux) - 1)
		//
		//sql = "DELETE DEPITEAM WHERE " & _
		//'     "  itiposer = '" & ParaItiposer & "' AND " & _
		//'     "  ganoregi = " & ParaAno & " AND " & _
		//'     "  gnumregi = " & ParaNum & " AND " & _
		//'     "  rconc = '" & ParaInfSolicitudes(1, 1) & "' AND " & _
		//'     "  econc NOT IN (" & aux & ")"
		//
		//ParaConexion.Execute sql
		//
		//Exit Function
		//
		//errores:
		//AnalizaError "Autorizaciones", App.Path, ParaUsuario, "clsAutorizaciones: bBorraTEAM", ParaConexion
		//bBorraTEAM = True
		//End Function

		internal static bool PrestacionQuirurgica(SqlConnection Conexion, string Prestacion)
		{
			bool result = false;
			string Sql = "SELECT DSERVICI.gservici FROM DSERVICI " + 
			             " INNER JOIN DCODPRES on DCODPRES.gservici = DSERVICI.gservici " + 
			             " WHERE " + 
			             " DCODPRES.ffinvali is null AND " + 
			             " DCODPRES.gprestac ='" + Prestacion + "' AND " + 
			             " DSERVICI.iquirofa='S'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, Conexion);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				result = true;
			}
		 
			rr.Close();
			return result;
		}

		//FUNCION bAutorizacionElectronica
		//Devuelve FALSE si no se necesita autorizacion electronica
		//Devuelve TRUE si se necesita autorizacion electronica
		internal static bool bAutorizacionElectronica(SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaInspeccion)
		{
			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;
			int lservicioprestacion = 0;



			switch(ParaTipoConc)
			{
				case "ES" : 
					Sql = "SELECT gservici FROM DSERVICI WHERE GSERVICI= " + ParaConcepto; 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
				case "PR" : 
					Sql = "SELECT gservici FROM DCODPRES WHERE GPRESTAC= '" + ParaConcepto + "'"; 
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion); 
					rr = new DataSet(); 
					tempAdapter_2.Fill(rr); 
					if (rr.Tables[0].Rows.Count != 0)
					{
						lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
					} 
				  
					rr.Close(); 
					break;
			}

			Sql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
			      " (case when gsocieda is not null then 1 else 0 end)+ " + 
			      " (case when gregimen is not null then 1 else 0 end)+ " + 
			      " (case when gservici is not null then 1 else 0 end)+ " + 
			      " (case when gpersona is not null then 1 else 0 end)+ " + 
			      " (case when ginspecc is not null then 1 else 0 end), " + 
			      " ISNULL(iautoe,'N') " + 
			      " FROM IFMSFA_DOCUTAU " + 
			      " WHERE " + 
			      " rconc = '" + ParaTipoConc + "' AND " + 
			      " (econc is null or econc = '" + ParaConcepto + "') AND " + 
			      " (gsocieda is null or gsocieda = " + ParaSociedad.ToString() + ") AND " + 
			      " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
			      " (gservici is null or gservici = " + lservicioprestacion.ToString() + ") AND " + 
			      " (gpersona is null or gpersona = " + ParaPersona.ToString() + ") AND " + 
			      " (ginspecc is null or ginspecc ='" + ParaInspeccion + "') AND " + 
			      " fborrado is null " + 
			      " ORDER BY iprioridad desc";

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, ParacnConexion);
			rr = new DataSet();
			tempAdapter_3.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				rr.MoveFirst();
				if (Convert.ToString(rr.Tables[0].Rows[0][1]).Trim().ToUpper() == "S")
				{
					result = true;
				}
			}

		 
			rr.Close();
			return result;
		}

		internal static string fObtenerNafiliac(SqlConnection PCn, string ParaGidenpac, int ParaSociedad, string ParaInspeccion)
		{
			string result = String.Empty;
			result = "";
			string Sql = "select nafiliac from dentipac " + 
			             " where gidenpac='" + ParaGidenpac + "' and " + 
			             " gsocieda =" + ParaSociedad.ToString();
			if (ParaInspeccion.Trim() != "")
			{
				Sql = Sql + " AND ginspecc='" + ParaInspeccion + "'";
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, PCn);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(rr.Tables[0].Rows[0]["nafiliac"]) + "";
			}
		 
			rr.Close();
			return result;
		}

		internal static bool bUtilizaBlackBox(SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaPersona, string ParaTipoConc, string ParaConcepto)
		{

			bool result = false;
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;
				int lservicioprestacion = 0;
				object clasemensaje = null;
				int imensaje = 0;


				// Tomamos el servicio


				switch(ParaTipoConc)
				{
					case "ES" : 
						 
						stSql = "SELECT gservici FROM DSERVICI with (nolock) WHERE GSERVICI= " + ParaConcepto; 
						 
						break;
					case "PR" : 
						 
						stSql = "SELECT gservici FROM DCODPRES with (nolock) WHERE GPRESTAC= '" + ParaConcepto + "'"; 
						 
						break;
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					lservicioprestacion = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["gservici"]);
				}

				 
				RrDatos.Close();

				// Vemos si usa ITC

				stSql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
				        " (case when gsocieda is not null then 1 else 0 end)+ " + 
				        " (case when gregimen is not null then 1 else 0 end)+ " + 
				        " (case when gservici is not null then 1 else 0 end)+ " + 
				        " (case when gpersona is not null then 1 else 0 end), " + 
				        " ISNULL(iautoe,'N') iautoe " + 
				        " FROM IFMSFA_DOCUTAU " + 
				        " WHERE " + 
				        " rconc = '" + ParaTipoConc + "' AND " + 
				        " (econc is null or econc = '" + ParaConcepto + "') AND " + 
				        " (gsocieda is null or gsocieda = " + ParaSociedad.ToString() + ") AND " + 
				        " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
				        " (gservici is null or gservici = " + lservicioprestacion.ToString() + ") AND " + 
				        " (gpersona is null or gpersona = " + ParaPersona.ToString() + ") AND " + 
				        " fborrado is null " + 
				        " ORDER BY iprioridad desc";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter_2.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{

					RrDatos.MoveFirst();

					result = (Convert.ToString(RrDatos.Tables[0].Rows[0]["iautoe"]).Trim().ToUpper() == "B");

				}

				 
				RrDatos.Close();

				return result;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("bUtilizaBlackBox: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		internal static void fCancelaAutorizacionBlackBox(ref SqlConnection cnConexion, ref string stTimeStamp, ref string ParaGidenpac)
		{

			try
			{

				BlackBoxWS.clsBlackBox oBlackBox = null;

				oBlackBox = new BlackBoxWS.clsBlackBox();

				if (!oBlackBox.proIniciaBlackBox(cnConexion))
				{
					return;
				}

				if (!oBlackBox.proSolicitarAnulacion(ParaGidenpac, stTimeStamp))
				{



				}

				oBlackBox = null;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("fCancelaAutorizacionBlackBox: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		internal static bool bGestionBlackBox(SqlConnection cnConexion)
		{

			bool result = false;
			string stSql = String.Empty;
			DataSet RrDatos = null;

			try
			{

				stSql = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'BLACKBOX' AND rTrim(Upper(valfanu1)) = 'S'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				result = RrDatos.Tables[0].Rows.Count != 0;

				 
				RrDatos.Close();

				return result;
			}
			catch
			{

				return false;
			}
		}

		internal static bool bAnulaEpisodioSinFactElectronica(ref SqlConnection ParacnConexion, string ParaTipoActividad, string ParaItiposer, int ParaAno, int ParaNum, string ParaUsuario)
		{

			bool result = false;
			int lAnoSol = 0;
			int lNumSol = 0;
			string Sql = String.Empty;
			DataSet rr = null;

			//UPGRADE_TODO: (1065) Error handling statement (On Error Goto) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx
			//UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (errores)");
            try
            {

            

			if (!bGestionAutorizaciones(ParacnConexion))
			{
				return result;
			}

			//----------- 21 Marzo 2007 REGISTRO DE ACTIVIDAD ELECTRONICA CEREZUELA ----------------------------------
			string Oper = String.Empty;
			string nOper = String.Empty;
			string tmov = String.Empty;
			string iautoe = String.Empty;
			Oper = "FAAU9";
			nOper = "000000";
			tmov = "AUTORIZA";
			iautoe = "N";
			string ParaGidenpac = String.Empty;
			string NumDu = String.Empty;
			int ParaSociedad = 0;
			string ParaInspeccion = String.Empty;
			int lservicioprestacion = 0;
			int ParaPersona = 0;
			int ParaCantidad = 0;
			string ParaTipoConc = String.Empty;
			string ParaConcepto = String.Empty;
			string fechaSesionOpOriginal = String.Empty;
			string fechaOpOriginal = String.Empty;
			string idOpOriginal = String.Empty;
			string numAutorizacion = String.Empty;
			string fecharealizacion = String.Empty;
			string OperacionAnula = String.Empty;
			string numoper = String.Empty;
			string Obsolici = String.Empty;

			string EstadoPeticion = String.Empty;



			numoper = "";
			Sql = "SELECT ganoauto, gnumauto FROM IFMSFA_ACTIVTAU " + 
			      " WHERE " + 
			      "    itipacti='" + ParaTipoActividad + "' AND " + 
			      "    itiposer='" + ParaItiposer + "' AND " + 
			      "    ganoregi=" + ParaAno.ToString() + " AND " + 
			      "    gnumregi= " + ParaNum.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion);
			rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				lAnoSol = Convert.ToInt32(rr.Tables[0].Rows[0]["ganoauto"]);
				lNumSol = Convert.ToInt32(rr.Tables[0].Rows[0]["gnumauto"]);
			}
			else
			{

			}
		 
			rr.Close();

			Sql = "SELECT * FROM IFMSFA_Autoriza " + 
			      " WHERE " + 
			      "    ganoauto='" + lAnoSol.ToString() + "' AND " + 
			      "    gnumauto='" + lNumSol.ToString() + "'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParacnConexion);
			rr = new DataSet();
			tempAdapter_2.Fill(rr);
			//iautoe = ""
			if (rr.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]) == "D")
				{
					goto sigue;
				}
				ParaGidenpac = Convert.ToString(rr.Tables[0].Rows[0]["Gidenpac"]);
				NumDu = Convert.ToString(rr.Tables[0].Rows[0]["dnvolant"]) + "";
				ParaSociedad = Convert.ToInt32(rr.Tables[0].Rows[0]["gsocieda"]);
				ParaInspeccion = Convert.ToString(rr.Tables[0].Rows[0]["ginspecc"]) + "";
				EstadoPeticion = Convert.ToString(rr.Tables[0].Rows[0]["gestsoli"]) + "";
				ParaCantidad = Convert.ToInt32(Conversion.Val("0" + Convert.ToString(rr.Tables[0].Rows[0]["ncantau"])));
				lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
				ParaPersona = Convert.ToInt32(Double.Parse("0" + Convert.ToString(rr.Tables[0].Rows[0]["gpersona"])));
				ParaTipoConc = Convert.ToString(rr.Tables[0].Rows[0]["rconc"]) + "";
				ParaConcepto = Convert.ToString(rr.Tables[0].Rows[0]["econc"]) + "";
				fechaSesionOpOriginal = Convert.ToString(rr.Tables[0].Rows[0]["fresoluc"]) + "";
				fechaOpOriginal = Convert.ToString(rr.Tables[0].Rows[0]["ftramite"]) + "";
				idOpOriginal = Convert.ToString(rr.Tables[0].Rows[0]["dnopelec"]) + "";
				numAutorizacion = Convert.ToString(rr.Tables[0].Rows[0]["dnautori"]) + "";
				fecharealizacion = Convert.ToString(rr.Tables[0].Rows[0]["fprevact"]) + "";
				if (Convert.IsDBNull(rr.Tables[0].Rows[0]["iautoe"]))
				{
					iautoe = "N";
				}
				else
				{
					iautoe = Convert.ToString(rr.Tables[0].Rows[0]["iautoe"]);
				}
				if (Convert.IsDBNull(rr.Tables[0].Rows[0]["obsolici"]))
				{
					Obsolici = "";
				}
				else
				{
					Obsolici = Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).Trim();
				}
				//If Not IsNull(rr("obsolici")) Then
				if (Obsolici.IndexOf('*') >= 0)
				{
					OperacionAnula = "" + Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).Substring(0, Math.Min(Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).IndexOf('*'), Convert.ToString(rr.Tables[0].Rows[0]["obsolici"]).Length));
				}
				else
				{
					OperacionAnula = "";
				}

			}
		 
			rr.Close();

			// Comprobamos si hay autorizaci�n con ITC
			if (iautoe == "I" && EstadoPeticion == "A")
			{

				//MsgBox idOpOriginal

				// Activamos ITC

				if (bAutArgentina.bGestionITC(ParacnConexion))
				{

					// Anulamos la autorizaci�n

					if (idOpOriginal.Trim().Length <= 6)
					{
						idOpOriginal = idOpOriginal.Trim();
					}
					else
					{
						idOpOriginal = idOpOriginal.Trim().Substring(idOpOriginal.Trim().Length - 6, Math.Min(6, 6));
					}

					bAutArgentina.fCancelaAutorizacionITC(ParacnConexion, ParaSociedad, ParaGidenpac, ref ParaConcepto, ParaCantidad, ParaItiposer, ParaTipoConc == "ES", idOpOriginal);

				}

			}

			// Comprobamos si hay autorizaci�n con Traditum
			if (iautoe == "T" && EstadoPeticion == "A")
			{

				// Activamos Traditum

				if (bAutArgentina.bGestionTraditum(ParacnConexion))
				{

					// Anulamos la autorizaci�n

					bAutArgentina.fCancelaAutorizacionTraditum(ParacnConexion, ParaSociedad, ParaGidenpac, ref ParaConcepto, ParaCantidad, ParaItiposer, lservicioprestacion, ParaTipoConc == "ES", idOpOriginal);

				}

			}

			// Comprobamos si hay autorizaci�n con BlackBox
			if (iautoe == "B" && EstadoPeticion == "A")
			{

				// Anulamos la autorizaci�n

				string tempRefParam = idOpOriginal.Trim();
				fCancelaAutorizacionBlackBox(ref ParacnConexion, ref tempRefParam, ref ParaGidenpac);

			}

sigue:

			Sql = "DELETE IFMSFA_ACTIVTAU " + 
			      " WHERE " + 
			      "    itipacti='" + ParaTipoActividad + "' AND " + 
			      "    itiposer='" + ParaItiposer + "' AND " + 
			      "    ganoregi=" + ParaAno.ToString() + " AND " + 
			      "    gnumregi= " + ParaNum.ToString();
			SqlCommand tempCommand = new SqlCommand(Sql, ParacnConexion);
			tempCommand.ExecuteNonQuery();

			Sql = "SELECT COUNT (*) FROM IFMSFA_ACTIVTAU WHERE " + 
			      " ganoauto=" + lAnoSol.ToString() + " AND " + 
			      " gnumauto=" + lNumSol.ToString();
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, ParacnConexion);
			rr = new DataSet();
			tempAdapter_3.Fill(rr);
			
            if (Convert.ToDouble(rr.Tables[0].Rows[0][0]) == 0)
			{
				if (iautoe == "I" || iautoe == "T" || iautoe == "B")
				{
					Sql = " UPDATE IFMSFA_AUTORIZA SET " + 
					      "    gestsoli='C', " + 
					      "    fborrado=getdate()," + 
					      "    gusuario='" + ParaUsuario.ToUpper() + "'," + 
					      "    obsolici=obsolici + '" + numoper + "-C'" + 
					      " WHERE " + 
					      "    ganoauto=" + lAnoSol.ToString() + " AND " + 
					      "    gnumauto=" + lNumSol.ToString();
				}
				else
				{
					Sql = " UPDATE IFMSFA_AUTORIZA SET " + 
					      "    gestsoli='C', " + 
					      "    fborrado=getdate()," + 
					      "    gusuario='" + ParaUsuario.ToUpper() + "'," + 
					      "    obsolici=obsolici + '" + numoper + "-C'" + 
					      " WHERE " + 
					      "    ganoauto=" + lAnoSol.ToString() + " AND " + 
					      "    gnumauto=" + lNumSol.ToString() + " AND " + 
					      "    gestsoli IN ('P','S')";
				}

				SqlCommand tempCommand_2 = new SqlCommand(Sql, ParacnConexion);
				tempCommand_2.ExecuteNonQuery();
			}
		 
			rr.Close();
			return result;
       }
        catch(Exception Exep)
        { 
        //errores:
		Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), ParaUsuario, "clsAutorizaciones: bAnulaSolicitud (" + Sql + ")", Exep);
		return true;

		}
    }

        internal static bool bTieneAutorizacionOnlineBlackBox(SqlConnection ParacnConexion, string ParaTipoActividad, string ParaItiposer, int ParaAno, int ParaNum, string ParaUsuario)
		{

			bool result = false;
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;

				// Valores por defecto


				// Si no hay gesti�n de autorizaciones, salimos

				if (!bGestionAutorizaciones(ParacnConexion))
				{
					return result;
				}

				// Si hay, buscamos el n�mero de autorizaci�n y si fue de Black Box

				stSql = "SELECT isNull(A.iautoe, 'N') iautoe, isNull(dnopelec, ' ') dnopelec FROM IFMSFA_AUTORIZA A " + 
				        "INNER JOIN IFMSFA_ACTIVTAU B ON " + 
				        "A.ganoauto = B.ganoauto AND " + 
				        "A.gnumauto = B.gnumauto AND " + 
				        "B.itipacti = '" + ParaTipoActividad + "' AND " + 
				        "B.itiposer = '" + ParaItiposer + "' AND " + 
				        "B.ganoregi = " + ParaAno.ToString() + " AND " + 
				        "B.gnumregi = " + ParaNum.ToString();

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					result = Convert.ToString(RrDatos.Tables[0].Rows[0]["dnopelec"]).Trim() != "" && Convert.ToString(RrDatos.Tables[0].Rows[0]["iautoe"]).Trim().ToUpper() == "B";

				}

				 
				RrDatos.Close();

				return result;
			}
			catch(Exception Exep)
			{

				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), ParaUsuario, "clsAutorizaciones: bTieneAutorizacionOnlineBlackBox", Exep);

				return false;
			}
		}
	}
}