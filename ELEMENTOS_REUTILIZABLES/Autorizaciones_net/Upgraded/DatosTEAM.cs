using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Autorizaciones
{
	public partial class DatosTEAM
		: RadForm
	{
		string mgidenpac = String.Empty;
		int mSociedad = 0;
		string miTiposer = String.Empty;

		SqlConnection mcnConexion = null;
		string[, ] mPrestaciones = null;
		string Musuario = String.Empty;

		//Identificador del episodio de la actividad Realizada
		//    ganourge/gnumurge del episodio de urgencias
		int mAnoregi = 0;
		int mNumregi = 0;

		const int T_COL_AREA = 1;
		const int T_COL_IDEPISODIO = 2;
		const int T_COL_DCONCEPTO = 3;
		const int T_COL_TIM = 4;
		const int T_COL_NOPERELE = 5;
		const int T_COL_TIPOCONC = 6;
		const int T_COL_CONCEPTO = 7;
		const int T_COL_iautoe = 8;
		const int T_COL_estado = 9;


		public DatosTEAM()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public void Establecevariables(SqlConnection ParaConexion, string ParaItiposer, int ParaAnoRegi, int ParaNumRegi, string Paragidenpac, int ParaSociedad, string[, ] ParaPrestaciones, string ParaUsuario)
		{
			mcnConexion = ParaConexion;
			mgidenpac = Paragidenpac;
			mSociedad = ParaSociedad;
			miTiposer = ParaItiposer;
			mPrestaciones = ParaPrestaciones;
			mAnoregi = ParaAnoRegi;
			mNumregi = ParaNumRegi;
			Musuario = ParaUsuario;
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{

			string iTipoConc = String.Empty;
			string stConcepto = String.Empty;

			string stNAutoriz = String.Empty;
			string stNOpelec = String.Empty;
			string iNecesita = String.Empty;
			bool bHayTeam = false;


			bAutorizaciones.gErrorTEAM = false;

			mcnConexion.BeginTransScope();

            int tempForVar = sprSolicitudes.MaxRows;
			for (int i = 1; i <= tempForVar; i++)
			{

				sprSolicitudes.Row = i;

				sprSolicitudes.Col = T_COL_TIPOCONC;
				iTipoConc = sprSolicitudes.Text;

				sprSolicitudes.Col = T_COL_CONCEPTO;
				stConcepto = sprSolicitudes.Text;

				sprSolicitudes.Col = T_COL_TIM;
				stNOpelec = Convert.ToString(sprSolicitudes.Value);

				bHayTeam = stNOpelec.Trim().ToUpper() == "1";

				sprSolicitudes.Col = T_COL_NOPERELE;
				stNOpelec = stNOpelec + "*" + sprSolicitudes.Text;



				bAutorizaciones.gErrorTEAM = bAutorizaciones.bRegistraTEAM(mcnConexion, miTiposer, mAnoregi, mNumregi, iTipoConc, stConcepto, ref stNOpelec, Musuario, "", bHayTeam);
				if (bAutorizaciones.gErrorTEAM)
				{
			        mcnConexion.RollbackTransScope();

                    break;
				}

			}

			mcnConexion.CommitTransScope();


            this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			bAutorizaciones.gErrorTEAM = false;
			this.Close();
		}

		private void DatosTEAM_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				//ProCargaCabecera
				//EstablecePropiedadesGrid
				//RellenaGridSolicitudes
			}
		}

		private void ProCargaCabecera()
		{

			string sql = "SELECT " + " ISNULL(LTRIM(RTRIM(DPACIENT.DAPE1PAC)),'')+'  '+" + " ISNULL(LTRIM(RTRIM(DPACIENT.DAPE2PAC)),'')+', '+" + " ISNULL(LTRIM(RTRIM(DPACIENT.DNOMBPAC)),'') AS NOMBREPACIENTE, " + " HDOSSIER.GHISTORIA" + " FROM " + " DPACIENT " + " LEFT JOIN HDOSSIER ON DPACIENT.GIDENPAC = HDOSSIER.GIDENPAC " + " WHERE " + " DPACIENT.GIDENPAC= '" + mgidenpac + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mcnConexion);
			DataSet rr = new DataSet();
			tempAdapter.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				lbPaciente.Text = Convert.ToString(rr.Tables[0].Rows[0]["NOMBREPACIENTE"]).Trim();
				if (Convert.ToString(rr.Tables[0].Rows[0]["GHISTORIA"]) != "")
				{
					lbHistoria.Text = (Convert.IsDBNull(rr.Tables[0].Rows[0]["ghistoria"])) ? "" : Convert.ToString(rr.Tables[0].Rows[0]["ghistoria"]);
				}
			}
			rr.Close();

			sql = "SELECT DSOCIEDA FROM DSOCIEDA WHERE GSOCIEDA= " + mSociedad.ToString();
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, mcnConexion);
			rr = new DataSet();
			tempAdapter_2.Fill(rr);
			if (rr.Tables[0].Rows.Count != 0)
			{
				LbFinanciadora.Text = Convert.ToString(rr.Tables[0].Rows[0]["DSOCIEDA"]);
			}
			rr.Close() ;

		}

		private void EstablecePropiedadesGrid()
		{
			sprSolicitudes.MaxCols = 7;
			sprSolicitudes.MaxRows = 0;
			//cabeceras
			sprSolicitudes.SetColWidth(T_COL_AREA, 5);
			sprSolicitudes.SetText(T_COL_AREA, 0, "Area");
			sprSolicitudes.SetColWidth(T_COL_IDEPISODIO, 10);
			sprSolicitudes.SetText(T_COL_IDEPISODIO, 0, "Id Episodio");
			sprSolicitudes.SetColWidth(T_COL_DCONCEPTO, 45);
			sprSolicitudes.SetText(T_COL_DCONCEPTO, 0, "Concepto");
			sprSolicitudes.SetColWidth(T_COL_TIM, 5);
			sprSolicitudes.SetText(T_COL_TIM, 0, "TEAM");
			sprSolicitudes.SetColWidth(T_COL_NOPERELE, 20);
			sprSolicitudes.SetText(T_COL_NOPERELE, 0, "N�mero de Op. Elec.");
			sprSolicitudes.SetColWidth(T_COL_TIPOCONC, 8);
			sprSolicitudes.SetText(T_COL_TIPOCONC, 0, "TCONC");
			sprSolicitudes.SetColWidth(T_COL_CONCEPTO, 8);
			sprSolicitudes.SetText(T_COL_CONCEPTO, 0, "CONC");
            //sdsalazar
			//sprSolicitudes.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.Normal;
		}

		private void RellenaGridSolicitudes()
		{
			int lservicioprestacion = 0;
			string sql = String.Empty;
			DataSet rr = null;

			sprSolicitudes.MaxRows = mPrestaciones.GetUpperBound(0);

			for (int i = 1; i <= mPrestaciones.GetUpperBound(0); i++)
			{

				sprSolicitudes.Row = i;
				sprSolicitudes.SetRowHeight(sprSolicitudes.Row, 15);

				//Area
				sprSolicitudes.Col = T_COL_AREA;
				sprSolicitudes.Text = miTiposer;
				sprSolicitudes.Lock = true;

				//Id Episodio (A�o / Numero)
				sprSolicitudes.Col = T_COL_IDEPISODIO;
				sprSolicitudes.Text = mAnoregi.ToString() + "/" + mNumregi.ToString();
				sprSolicitudes.Lock = true;

				//Concepto
				sprSolicitudes.Col = T_COL_DCONCEPTO;
				switch(mPrestaciones[i, 1])
				{
					case "ES" : 
						sql = "SELECT gservici, DNOMSERV FROM DSERVICI WHERE GSERVICI= " + mPrestaciones[i, 2]; 
						SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mcnConexion); 
						rr = new DataSet(); 
						tempAdapter.Fill(rr); 
						if (rr.Tables[0].Rows.Count != 0)
						{
							sprSolicitudes.Text = "ESTANCIAS PARA " + Convert.ToString(rr.Tables[0].Rows[0]["dnomserv"]).Trim().ToUpper();
							lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
						}
						else
						{
							sprSolicitudes.Text = "ESTANCIAS";
						} 
						rr.Close(); 
						break;
					case "PR" : 
						sql = "SELECT gservici, DPRESTAC FROM DCODPRES WHERE GPRESTAC= '" + mPrestaciones[i, 2] + "'"; 
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, mcnConexion); 
						rr = new DataSet(); 
						tempAdapter_2.Fill(rr); 
						if (rr.Tables[0].Rows.Count != 0)
						{
							sprSolicitudes.Text = Convert.ToString(rr.Tables[0].Rows[0]["dprestac"]).Trim().ToUpper();
							lservicioprestacion = Convert.ToInt32(rr.Tables[0].Rows[0]["gservici"]);
						} 
						rr.Close(); 
						break;
				}
				sprSolicitudes.Lock = true;

				//concepto
				sprSolicitudes.Col = T_COL_TIPOCONC;
				sprSolicitudes.Text = mPrestaciones[i, 1];
				sprSolicitudes.Lock = true;
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

				//tipo de concepto
				sprSolicitudes.Col = T_COL_CONCEPTO;
				sprSolicitudes.Text = mPrestaciones[i, 2];
				sprSolicitudes.Lock = true;
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);

				//Indicador de TEAM
				sprSolicitudes.Col = T_COL_TIM;
				sprSolicitudes.Lock = false;
				sprSolicitudes.CellType = 10;
				//sdsalazar
                //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeCheckText was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//sprSolicitudes.setTypeCheckText("");
				sprSolicitudes.TypeCheckCenter = true;


				//Numero de operacion
				sprSolicitudes.Col = T_COL_NOPERELE;
				sprSolicitudes.Text = "";
				sprSolicitudes.Lock = false;
				//UPGRADE_ISSUE: (2064) FPSpread.vaSpread property sprSolicitudes.TypeEditLen was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				sprSolicitudes.setTypeEditLen(30); //20

				sql = "SELECT iteam, dnopelec FROM DEPITEAM" + 
				      " WHERE ganoregi=" + mAnoregi.ToString() + " AND gnumregi =" + mNumregi.ToString() + 
				      " AND itiposer='" + miTiposer + "'" + 
				      " AND rconc='" + mPrestaciones[i, 1] + "'" + 
				      " AND econc='" + mPrestaciones[i, 2] + "'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, mcnConexion);
				rr = new DataSet();
				tempAdapter_3.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					sprSolicitudes.Col = T_COL_NOPERELE;
					sprSolicitudes.setTypeEditLen(30); //20
					sprSolicitudes.Text = "" + Convert.ToString(rr.Tables[0].Rows[0]["dnopelec"]).Trim();
					sprSolicitudes.Col = T_COL_TIM;
					if (Convert.ToString(rr.Tables[0].Rows[0]["iteam"]) == "S")
					{
						sprSolicitudes.Value = 1;
					}
					else
					{
						sprSolicitudes.Value = 0;
					}
				}
				rr.Close();


			}

		}


		private void DatosTEAM_Load(Object eventSender, EventArgs eventArgs)
		{
			try
			{

				this.Text = "Documentaci�n de Operaci�n Electr�nica - AUT002F2";

				ProCargaCabecera();
				EstablecePropiedadesGrid();
				RellenaGridSolicitudes();

				//UPGRADE_ISSUE: (2064) Form property DatosTEAM.HelpContextID was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//this.setHelpContextID(142001);
			}
			catch(Exception Excep)
			{
				Serrores.AnalizaError("Autorizaciones", Path.GetDirectoryName(Application.ExecutablePath), Musuario, "DatosTEAM: Form_Load", Excep);
				bAutorizaciones.gErrorTEAM = false;
			}

		}

		private void sprSolicitudes_ButtonClicked(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			//int Col = eventArgs.Column;
			//int Row = eventArgs.Row;

            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;

            int ButtonDown = 0;
			if (Col == T_COL_TIM)
			{
				sprSolicitudes.Col = Col;
				sprSolicitudes.Row = Row;
				if (StringsHelper.ToDoubleSafe(Convert.ToString(sprSolicitudes.Value)) == 0)
				{
					sprSolicitudes.Col = T_COL_NOPERELE;
					sprSolicitudes.Text = "";
				}
			}
		}
        private void sprSolicitudes_LeaveCell(object eventSender, Telerik.WinControls.UI.CurrentCellChangedEventArgs  eventArgs)
        {
            //int Col = eventArgs.Column + 1;
            //int Row = eventArgs.Row + 1;
            //int NewCol = eventArgs.NewColumn + 1;
            //int NewRow = eventArgs.NewRow + 1;
            //bool Cancel = eventArgs.Cancel;
            int Col = eventArgs.CurrentCell.ColumnInfo.Index + 1;
            int Row = eventArgs.CurrentCell.RowInfo.Index + 1;
            int NewCol = eventArgs.NewCell.ColumnInfo.Index + 1;
            int NewRow = eventArgs.NewCell.RowInfo.Index + 1;

            
            if (Col == T_COL_NOPERELE)
            {
                sprSolicitudes.Row = Row;
                sprSolicitudes.Col = Col;
                if (sprSolicitudes.Text != "")
                {
                    sprSolicitudes.Col = T_COL_TIM;
                    sprSolicitudes.Value = "1";
                }
            }
        }
        private void DatosTEAM_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}