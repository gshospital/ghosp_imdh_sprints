using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Microsoft.CSharp;

namespace Autorizaciones
{
	public class clsAutorizaciones
	{


		//FUNCION fActividad_Programada
		//ESTA FUNCION SE UTILIZA CUANDO LA ACTIVIDAD ES PROGRAMADA (RESERVAS DE INGRESOS, CITAS...)
		//Devuelve FALSE si no se ha producido ningun error al registrar y/o asociar la solicitud
		//Devuelve TRUE si se ha producido algun error en el proceso de registrar y/o asociar solicitud
		public bool fActividad_Programada(ref SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaCantidad, string ParaGidenpac, ref string ParafPrevact, ref string ParafRealiza, string ParaTipoActividad, int ParaAno, int ParaNum, string ParaUsuario, string ParaNumeroDu, ref string Paragsersoli, string Paragpersoli, string Paragdiagnos, string ParaInspeccion)
		{


			//Parametros Opcionales:
			//ParaNumeroDu --> Numero de Documento Unico
			//Paragsersoli --> Servicio Solicitante
			//Paragpersoli --> Persona Solicitante
			//Paragdiagnos --> Diagnostico CIE9 en el caso de que se codifique:
			//                En el caso de Actividad programada solo esta disponible en reservas de ingreso
			//                y programaciones quirurgicas.

			bool result = false;
			string Sql = String.Empty;
			DataSet rr = null;
			bool bMostrar = false;
			bool bError = false;



			//�APA 07/07/08
			//Segun conclusion de la reunion del 04/07/08 en el Hospital Tres Culturas
			//No se tramiran solicitudes de autorizacion de programaciones quirurgicas cuando
			//el regimen del cirugano de la programacion sea "abierto" y no se haya especificado
			//Numero de Documento Unico.
			if (ParaItiposer == "Q" && ParaTipoActividad == "P" && ParaNumeroDu.Trim() == "")
			{
				Sql = "SELECT iregciru FROM QPROGQUI where ganoregi=" + ParaAno.ToString() + " AND gnumregi = " + ParaNum.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParaConexion);
				rr = new DataSet();
				tempAdapter.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(rr.Tables[0].Rows[0][0]))
					{
						if (Convert.ToString(rr.Tables[0].Rows[0][0]) == "A")
						{
							return result;
						}
					}
				}
				 
				rr.Close();
			}
			//------

			if (Paragsersoli == "")
			{
				Paragsersoli = Conversion.Str(ParaServicio);
			}

			bAutorizaciones.gANOAUT = 0;
			bAutorizaciones.gNUMAUT = 0;

			Serrores.AjustarRelojCliente(ParaConexion);
			if (!bAutorizaciones.bGestionAutorizaciones(ParaConexion))
			{
				return result;
			}

			string stFiliacion = String.Empty;
			string TipoIngresoIntervencion = String.Empty;
			if (bAutorizaciones.bNecesitaAutorizacion(ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaInspeccion) || bAutorizaciones.bNecesitaVolante(ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaInspeccion))
			{


				stFiliacion = bAutorizaciones.fObtenerNafiliac(ParaConexion, ParaGidenpac, ParaSociedad, ParaInspeccion);

				Sql = "SELECT count(*) FROM IFMSFA_AUTORIZA WHERE" + 
				      " gidenpac ='" + ParaGidenpac + "' AND " + 
				      " gsocieda =" + ParaSociedad.ToString() + " AND " + 
				      " gregimen ='" + ParaItiposer + "' AND" + 
				      " gservici =" + ParaServicio.ToString() + " AND " + 
				      " (gpersona is null or gpersona =" + ParaPersona.ToString() + ") AND " + 
				      " rconc ='" + ParaTipoConc + "' AND " + 
				      " econc ='" + ParaConcepto + "' AND " + 
				      " gestsoli IN ('P','S','A')  AND fborrado is null ";

				//14/11/2008 A�ADIMOS LA CONDICION DE BUSQUEDA DE AUTORIZACION EL NAFILIAC
				if (stFiliacion.Trim() != "")
				{
					Sql = Sql + " AND (IFMSFA_AUTORIZA.nafiliac is null or IFMSFA_AUTORIZA.nafiliac='" + stFiliacion + "')";
				}

				if (ParaInspeccion.Trim() != "")
				{
					Sql = Sql + " AND IFMSFA_AUTORIZA.ginspecc='" + ParaInspeccion + "'";
				}

				Sql = Sql + " AND NOT EXISTS(SELECT ''  FROM  IFMSFA_ACTIVTAU " + 
				      "            WHERE  " + 
				      "                   IFMSFA_AUTORIZA.ganoauto = IFMSFA_ACTIVTAU.ganoauto and" + 
				      "                   IFMSFA_AUTORIZA.gnumauto = IFMSFA_ACTIVTAU.gnumauto )";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, ParaConexion);
				rr = new DataSet();
				tempAdapter_2.Fill(rr);

                if (Convert.ToDouble(rr.Tables[0].Rows[0][0]) > 0)
				{
					bMostrar = true;
				}
				else
				{
					if (ParaTipoConc == "ES")
					{
						bMostrar = true;
					}
				}
				 
				rr.Close();

				//FEBRERO 2008
				//SEGUN EL INDICADOR IINGAMBU DE LA PROGRAMACION, SE ESTABLECERA EL AREA ASISTENCIAL
				//EN EL MENSAJE DE SOLICITUD DE AUTORIZACION
				TipoIngresoIntervencion = "";
				if (ParaItiposer == "Q")
				{
					Sql = "SELECT iingambu FROM QPROGQUI where ganoregi=" + ParaAno.ToString() + " AND gnumregi = " + ParaNum.ToString();
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, ParaConexion);
					rr = new DataSet();
					tempAdapter_3.Fill(rr);
					if (rr.Tables[0].Rows.Count != 0)
					{
						TipoIngresoIntervencion = Convert.ToString(rr.Tables[0].Rows[0][0]) + "";
					}
					 
					rr.Close();
				}
				//--------

				if (bMostrar)
				{
					Actividad_Programada.DefInstance.Establecevariables(ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaCantidad, ParaGidenpac, ParafPrevact, ParafRealiza, ParaTipoActividad, ParaAno, ParaNum, ParaUsuario.ToUpper(), ParaNumeroDu, Paragsersoli, Paragpersoli, Paragdiagnos, TipoIngresoIntervencion, ParaInspeccion);
					Actividad_Programada.DefInstance.ShowDialog();
					bError = bAutorizaciones.gError;

				}
				else
				{


					bError = bAutorizaciones.bRegistraSolicitud(ref ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaCantidad, ParaGidenpac, ref ParafPrevact, ref ParafRealiza, ParaTipoActividad, ParaUsuario.ToUpper(), ref bAutorizaciones.gANOAUT, ref bAutorizaciones.gNUMAUT, ParaNumeroDu, Paragsersoli, Paragpersoli, Paragdiagnos, TipoIngresoIntervencion, ParaInspeccion);
					if (!bError)
					{
						bError = bAutorizaciones.bAsociaSolicitud(ParaConexion, bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT, ParaTipoActividad, ParaItiposer, ParaAno, ParaNum, ref ParafPrevact, ParaTipoConc, ParaConcepto, ParaCantidad, ParaUsuario.ToUpper());
					}

				}

			}
			return bError;

		}

		public bool fActividad_Programada(ref SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaCantidad, string ParaGidenpac, ref string ParafPrevact, ref string ParafRealiza, string ParaTipoActividad, int ParaAno, int ParaNum, string ParaUsuario, string ParaNumeroDu, ref string Paragsersoli, string Paragpersoli, string Paragdiagnos)
		{
			return fActividad_Programada(ref ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaCantidad, ParaGidenpac, ref ParafPrevact, ref ParafRealiza, ParaTipoActividad, ParaAno, ParaNum, ParaUsuario, ParaNumeroDu, ref Paragsersoli, Paragpersoli, Paragdiagnos, String.Empty);
		}

		public bool fActividad_Programada(ref SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaCantidad, string ParaGidenpac, ref string ParafPrevact, ref string ParafRealiza, string ParaTipoActividad, int ParaAno, int ParaNum, string ParaUsuario, string ParaNumeroDu, ref string Paragsersoli, string Paragpersoli)
		{
			return fActividad_Programada(ref ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaCantidad, ParaGidenpac, ref ParafPrevact, ref ParafRealiza, ParaTipoActividad, ParaAno, ParaNum, ParaUsuario, ParaNumeroDu, ref Paragsersoli, Paragpersoli, String.Empty, String.Empty);
		}

		public bool fActividad_Programada(ref SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaCantidad, string ParaGidenpac, ref string ParafPrevact, ref string ParafRealiza, string ParaTipoActividad, int ParaAno, int ParaNum, string ParaUsuario, string ParaNumeroDu, ref string Paragsersoli)
		{
			return fActividad_Programada(ref ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaCantidad, ParaGidenpac, ref ParafPrevact, ref ParafRealiza, ParaTipoActividad, ParaAno, ParaNum, ParaUsuario, ParaNumeroDu, ref Paragsersoli, String.Empty, String.Empty, String.Empty);
		}

		public bool fActividad_Programada(ref SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaCantidad, string ParaGidenpac, ref string ParafPrevact, ref string ParafRealiza, string ParaTipoActividad, int ParaAno, int ParaNum, string ParaUsuario, string ParaNumeroDu)
		{
			string tempRefParam = String.Empty;
			return fActividad_Programada(ref ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaCantidad, ParaGidenpac, ref ParafPrevact, ref ParafRealiza, ParaTipoActividad, ParaAno, ParaNum, ParaUsuario, ParaNumeroDu, ref tempRefParam, String.Empty, String.Empty, String.Empty);
		}

		public bool fActividad_Programada(ref SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaCantidad, string ParaGidenpac, ref string ParafPrevact, ref string ParafRealiza, string ParaTipoActividad, int ParaAno, int ParaNum, string ParaUsuario)
		{
			string tempRefParam2 = String.Empty;
			return fActividad_Programada(ref ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaCantidad, ParaGidenpac, ref ParafPrevact, ref ParafRealiza, ParaTipoActividad, ParaAno, ParaNum, ParaUsuario, String.Empty, ref tempRefParam2, String.Empty, String.Empty, String.Empty);
		}

		public clsAutorizaciones()
		{
			Serrores.oClass = new Mensajes.ClassMensajes();
            //UPGRADE_ISSUE: (2070) Constant App was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
            //UPGRADE_ISSUE: (2064) App property App.HelpFile was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //sdsalazar
            //UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\" + "AYUDA_DLLS.HLP");
            
		}

		~clsAutorizaciones()
		{
			Serrores.oClass = null;
		}


		//FUNCION fRecogeDatosSolicitudes:
		//Devuelve FALSE si se produce algun error y no queremos que continue el proceso de captura de actividad realizada
		//Devuelve TRUE si puede continuar el proceso de realizacion de actividad
		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, ref string ParaAreaT, string ParaNumeroDu, ref string Paragsersoli, string Paragpersoli, string Paragdiagnos, string ParaUsuario, string paraTipoIngresoIntervencion, bool paraNuevaSolicitudSesiones, string ParaInspeccion)
		{

			Serrores.Datos_Conexion(ParaConexion);

			if (Paragsersoli == "")
			{
				Paragsersoli = ParaServicio.ToString();
			}

			string tempRefParam = "";
			string[, ] ArrayinfSolicitudes = ArraysHelper.InitializeArray<string[, ]>(new int[]{2, 15}, new int[]{0, 0});
			if ((bAutorizaciones.bGestionTEAM_SOC(ParaConexion, ParaSociedad) && ParaPrestaciones[1, 1] == "PR" && bAutorizaciones.bAreaControl_TEAM(ParaConexion, ref ParaAreaT, ref ParaItiposer)) || (bAutorizaciones.bGestionAutorizaciones(ParaConexion) && bAutorizaciones.bNecesitaDocumentacion(ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaPrestaciones, ParaInspeccion) && !bAutArgentina.bEsSolicitudSesionesTraditum(ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaPrestaciones, paraNuevaSolicitudSesiones)) || (bAutorizaciones.bGestionTEAM_SOC(ParaConexion, ParaSociedad) && ParaPrestaciones[1, 1] == "ES" && ParaItiposer == "U" && bAutorizaciones.bAreaControl_TEAM(ParaConexion, ref tempRefParam, ref ParaItiposer)))
			{

				Actividad_Realizada.DefInstance.Establecevariables(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ParaAreaT, "R", ParaNumeroDu, Paragsersoli, Paragpersoli, Paragdiagnos, ParaUsuario.ToUpper(), paraTipoIngresoIntervencion, ParaInspeccion);

				Actividad_Realizada.DefInstance.ShowDialog();

				return bAutorizaciones.gContinuarRecogiendoInf;
			}
			else
			{
				//Cuidado con la variable del objeto llamador porque no se vacia y de una ejecucion a otra podria
				//tener guarreria.
				ArrayinfSolicitudes = ArraysHelper.InitializeArray<string[, ]>(new int[]{2, 15}, new int[]{0, 0});
				//UPGRADE_NOTE: (1061) Erase was upgraded to System.Array.Clear. More Information: http://www.vbtonet.com/ewis/ewi1061.aspx
				if (ArrayinfSolicitudes != null)
				{
					Array.Clear(ArrayinfSolicitudes, 0, ArrayinfSolicitudes.Length);
				}
				ArrayinfSolicitudes[1, 0] = "NOTHING";

                ParaFormu.RecogeDatosSolicitudes(ArrayinfSolicitudes);

				//Si no hay gestion de autorizaciones, forzamos a que devuelva true
				//para que continue el proceso de registro de actividad
				return true;
			}

		}

		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, ref string ParaAreaT, string ParaNumeroDu, ref string Paragsersoli, string Paragpersoli, string Paragdiagnos, string ParaUsuario, string paraTipoIngresoIntervencion, bool paraNuevaSolicitudSesiones)
		{
			return fRecogeDatosSolicitudes(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ref ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ref ParaAreaT, ParaNumeroDu, ref Paragsersoli, Paragpersoli, Paragdiagnos, ParaUsuario, paraTipoIngresoIntervencion, paraNuevaSolicitudSesiones, String.Empty);
		}

		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, ref string ParaAreaT, string ParaNumeroDu, ref string Paragsersoli, string Paragpersoli, string Paragdiagnos, string ParaUsuario, string paraTipoIngresoIntervencion)
		{
			return fRecogeDatosSolicitudes(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ref ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ref ParaAreaT, ParaNumeroDu, ref Paragsersoli, Paragpersoli, Paragdiagnos, ParaUsuario, paraTipoIngresoIntervencion, false, String.Empty);
		}

		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, ref string ParaAreaT, string ParaNumeroDu, ref string Paragsersoli, string Paragpersoli, string Paragdiagnos, string ParaUsuario)
		{
			return fRecogeDatosSolicitudes(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ref ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ref ParaAreaT, ParaNumeroDu, ref Paragsersoli, Paragpersoli, Paragdiagnos, ParaUsuario, String.Empty, false, String.Empty);
		}

		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, ref string ParaAreaT, string ParaNumeroDu, ref string Paragsersoli, string Paragpersoli, string Paragdiagnos)
		{
			return fRecogeDatosSolicitudes(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ref ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ref ParaAreaT, ParaNumeroDu, ref Paragsersoli, Paragpersoli, Paragdiagnos, String.Empty, String.Empty, false, String.Empty);
		}

		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, ref string ParaAreaT, string ParaNumeroDu, ref string Paragsersoli, string Paragpersoli)
		{
			return fRecogeDatosSolicitudes(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ref ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ref ParaAreaT, ParaNumeroDu, ref Paragsersoli, Paragpersoli, String.Empty, String.Empty, String.Empty, false, String.Empty);
		}

		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, ref string ParaAreaT, string ParaNumeroDu, ref string Paragsersoli)
		{
			return fRecogeDatosSolicitudes(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ref ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ref ParaAreaT, ParaNumeroDu, ref Paragsersoli, String.Empty, String.Empty, String.Empty, String.Empty, false, String.Empty);
		}

		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, ref string ParaAreaT, string ParaNumeroDu)
		{
			string tempRefParam3 = String.Empty;
			return fRecogeDatosSolicitudes(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ref ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ref ParaAreaT, ParaNumeroDu, ref tempRefParam3, String.Empty, String.Empty, String.Empty, String.Empty, false, String.Empty);
		}

		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi, ref string ParaAreaT)
		{
			string tempRefParam4 = String.Empty;
			return fRecogeDatosSolicitudes(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ref ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ref ParaAreaT, String.Empty, ref tempRefParam4, String.Empty, String.Empty, String.Empty, String.Empty, false, String.Empty);
		}

		public bool fRecogeDatosSolicitudes(SqlConnection ParaConexion, dynamic ParaFormu, string ParaGidenpac, int ParaSociedad, ref string ParaItiposer, int ParaServicio, int ParaPersona, string ParaFechaPrev_Real, string[, ] ParaPrestaciones, int ParaAnoProg, int ParaNumProg, int ParaAnoRegi, int ParaNumRegi)
		{
			string tempRefParam5 = String.Empty;
			string tempRefParam6 = String.Empty;
			return fRecogeDatosSolicitudes(ParaConexion, ParaFormu, ParaGidenpac, ParaSociedad, ref ParaItiposer, ParaServicio, ParaPersona, ParaFechaPrev_Real, ParaPrestaciones, ParaAnoProg, ParaNumProg, ParaAnoRegi, ParaNumRegi, ref tempRefParam5, String.Empty, ref tempRefParam6, String.Empty, String.Empty, String.Empty, String.Empty, false, String.Empty);
		}
		public void deniegaEpisodio(int ganoauto, int gnumauto, SqlConnection ParacnConexion, string Fallo)
		{
			string Sql = " UPDATE IFMSFA_AUTORIZA SET " + 
			             "    gestsoli='D',";
			if (Fallo == "")
			{
				Sql = Sql + "obsolici = Null";
			}
			else
			{
				Sql = Sql + "obsolici ='" + Fallo + "-D' ";
			}

			Sql = Sql + "  where ganoauto=" + ganoauto.ToString() + " and gnumauto=" + gnumauto.ToString() + " ";
			SqlCommand tempCommand = new SqlCommand(Sql, ParacnConexion);
			tempCommand.ExecuteNonQuery();

		}

		//FUNCION fActividad_Realizada
		//ESTA FUNCION SE UTILIZA CUANDO LA ACTIVIDAD ES REALIZADA (INGRESOS, CONSULTAS, INTERVENCIONES...)
		//Devuelve FALSE si no se ha producido ningun error al registrar, asociar, aprobar la solicitud
		//Devuelve TRUE si se ha producido algun error en el proceso de registrar, asociar, Aprobar solicitud
		public bool fActividad_Realizada(dynamic ParaFormu, ref SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaGidenpac, ref string ParafPrevact, ref string ParafRealiza, string ParaTipoActividad, int ParaAno, int ParaNum, string ParaUsuario, string[, ] ParaInfSolicitudes, string ParaInspeccion = "")
		{
			bool result = false;
			int i = 0;
			int iPos = 0;
			bool bError = false;
			string stDescripcion = String.Empty;
			string stMensaje = String.Empty;

			string idSolicitud = String.Empty;
			string iTipoConc = String.Empty;
			string stConcepto = String.Empty;
			string stCantidad = String.Empty;
			string stNAutoriz = String.Empty;
			string stNVolante = String.Empty;
			string stNOpelec = String.Empty;
			string iNecesita = String.Empty;
			string StOperacion = String.Empty;
			string EstadoElec = String.Empty;
			string iautoe = String.Empty;
			string Fallo = String.Empty;
			string stCopago = String.Empty;

			string FechaSesionAux = String.Empty;
			string FechaTransaccionAux = String.Empty;


			//CHAPU nominada a CHAPU del 2008!!!
			//Cuidado con la variable del objeto llamador porque no se vacia y de una ejecucion a otra podria
			//tener guarreria no deseada, por lo que se establece a "NOTHING" cuando:
			//Desde la pantalla de Actividad_Realizada (recogida de datos de autorizaciones) se pulsa cancelar
			//o se sale de la funcion sin entrar en la pantalla porque no se requieren autorizaciones.
			if (ParaInfSolicitudes[1, 0] == "NOTHING")
			{
				return result;
			}
			//--------------------

			//UPGRADE_WARNING: (2080) IsEmpty was upgraded to a comparison and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
			if ((!bAutorizaciones.bGestionTEAM_SOC(ParaConexion, ParaSociedad) && !bAutorizaciones.bGestionAutorizaciones(ParaConexion)) || ParaInfSolicitudes == null)
			{
				return result;
			}

			Serrores.AjustarRelojCliente(ParaConexion);
			if (bAutorizaciones.bGestionAutorizaciones(ParaConexion))
			{
				for (i = 1; i <= ParaInfSolicitudes.GetUpperBound(0); i++)
				{

					bAutorizaciones.gANOAUT = 0;
					bAutorizaciones.gNUMAUT = 0;

					idSolicitud = ParaInfSolicitudes[i, 0];
					iTipoConc = ParaInfSolicitudes[i, 1];
					stConcepto = ParaInfSolicitudes[i, 2];
					stCantidad = ParaInfSolicitudes[i, 3];
					iNecesita = ParaInfSolicitudes[i, 4];
					stNAutoriz = ParaInfSolicitudes[i, 5];
					stNVolante = ParaInfSolicitudes[i, 6];
					stNOpelec = ParaInfSolicitudes[i, 7];
					EstadoElec = ParaInfSolicitudes[i, 9];
					iautoe = ParaInfSolicitudes[i, 8];
					StOperacion = ParaInfSolicitudes[i, 10];
					Fallo = ParaInfSolicitudes[i, 11];
					FechaSesionAux = ParaInfSolicitudes[i, 12];
					FechaTransaccionAux = ParaInfSolicitudes[i, 13];

					//Oscar C Agosto 2011. Agosto
					stCopago = ParaInfSolicitudes[i, 14];
					//----------------------------

					if (idSolicitud.IndexOf('*') >= 0)
					{
						iPos = (idSolicitud.IndexOf('*') + 1);
						bAutorizaciones.gANOAUT = Convert.ToInt32(Double.Parse(idSolicitud.Substring(0, Math.Min(iPos - 1, idSolicitud.Length))));
						bAutorizaciones.gNUMAUT = Convert.ToInt32(Double.Parse(idSolicitud.Substring(iPos)));
					}
					else
					{
						if (idSolicitud != "")
						{
							iPos = (idSolicitud.IndexOf('/') + 1);
							bAutorizaciones.gANOAUT = Convert.ToInt32(Double.Parse(idSolicitud.Substring(0, Math.Min(iPos - 1, idSolicitud.Length))));
							bAutorizaciones.gNUMAUT = Convert.ToInt32(Double.Parse(idSolicitud.Substring(iPos)));
						}
					}
					if (EstadoElec == "D")
					{
						deniegaEpisodio(bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT, ParaConexion, Fallo);
					}
					//1.
					if ((iNecesita == "A" || iNecesita == "V") && (idSolicitud == ""))
					{
						//Or (iautoe = "S" And idSolicitud = "")  Then
						bError = bAutorizaciones.bRegistraSolicitud(ref ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, iTipoConc, stConcepto, stCantidad, ParaGidenpac, ref ParafPrevact, ref ParafRealiza, ParaTipoActividad, ParaUsuario.ToUpper(), ref bAutorizaciones.gANOAUT, ref bAutorizaciones.gNUMAUT, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, ParaInspeccion);
						if (bError)
						{
							return bError;
						}

					}

					//2.
					if (bAutorizaciones.gANOAUT != 0 && bAutorizaciones.gNUMAUT != 0)
					{

						if (bAutorizaciones.bEstadoSolicitud(ParaConexion, bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT) == "A")
						{

							bError = bAutorizaciones.bActualizaSolicitudAprobada(ParaConexion, bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT, ParaUsuario.ToUpper(), stCantidad, stNAutoriz, stNVolante, StOperacion, FechaTransaccionAux, FechaSesionAux, ref stCopago);
							if (bError)
							{
								return bError;
							}

						}

						if (bAutorizaciones.bEstadoSolicitud(ParaConexion, bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT) == "P" || bAutorizaciones.bEstadoSolicitud(ParaConexion, bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT) == "S")
						{

							if (iNecesita == "A" && stNAutoriz != "")
							{
								bError = bAutorizaciones.bApruebaSolicitud(ref ParaConexion, bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT, ParaUsuario.ToUpper(), stCantidad, stNAutoriz, stNVolante, StOperacion, ref iautoe, Fallo, FechaTransaccionAux, FechaSesionAux, ref stCopago);
							}

							if (iNecesita == "V" && stNVolante != "")
							{
								//Or (iautoe = "S") Then

								if (stNVolante == "*")
								{
									stNVolante = bAutorizaciones.gANOAUT.ToString().Trim() + "/" + Conversion.Str(bAutorizaciones.gNUMAUT).Trim();
									stDescripcion = bAutorizaciones.BuscaDescripcion(ParaConexion, iTipoConc, stConcepto);
									stMensaje = stMensaje + Environment.NewLine + 
									            stDescripcion + new string(' ', 100 - stDescripcion.Length) + stNVolante;
								}
								//If EstadoElec <> "P" Then
								bError = bAutorizaciones.bApruebaSolicitud(ref ParaConexion, bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT, ParaUsuario.ToUpper(), stCantidad, stNAutoriz, stNVolante, StOperacion, ref iautoe, Fallo, FechaTransaccionAux, FechaSesionAux, ref stCopago);
								//End If
							}

							if (bError)
							{
								return bError;
							}

						}


					}

					//3.
					if (iNecesita == "A" || iNecesita == "V")
					{
						//Or (iautoe = "S") Then

						bError = bAutorizaciones.bAsociaSolicitud(ParaConexion, bAutorizaciones.gANOAUT, bAutorizaciones.gNUMAUT, ParaTipoActividad, ParaItiposer, ParaAno, ParaNum, ref ParafRealiza, iTipoConc, stConcepto, stCantidad, ParaUsuario.ToUpper());

						if (bError)
						{
							return bError;
						}

					}

				}

			}



			if (bAutorizaciones.bGestionTEAM_SOC(ParaConexion, ParaSociedad))
			{

				for (i = 1; i <= ParaInfSolicitudes.GetUpperBound(0); i++)
				{

					iTipoConc = ParaInfSolicitudes[i, 1];
					stConcepto = ParaInfSolicitudes[i, 2];
					stNOpelec = ParaInfSolicitudes[i, 7];
					stNAutoriz = ParaInfSolicitudes[i, 5];
					if ((iTipoConc == "PR") || (iTipoConc == "ES" && ParaItiposer == "U"))
					{
						bError = bAutorizaciones.bRegistraTEAM(ParaConexion, ParaItiposer, ParaAno, ParaNum, iTipoConc, stConcepto, ref stNOpelec, ParaUsuario.ToUpper(), stNAutoriz, stNOpelec.StartsWith("1"));
						if (bError)
						{
							return bError;
						}
					}

				}

			}

			//A lo mejor se quiere que esto si se haga en el futuro y como ya esta programado,
			//de momento se queda comentado.
			//If bGestionTEAM(ParaConexion) Then
			//        bError = bBorraTEAM(ParaConexion, ParaItiposer, ParaAno, ParaNum, ParaInfSolicitudes, ParaUsuario)
			//
			//        If bError Then
			//            fActividad_Realizada = bError
			//            Exit Function
			//        End If
			//End If




			//*************************************************************************************************
			//Habria que buscar las autorizaciones electronicas aprobadas asociadas a la actividad realizada que se recibe.
			string stSql = String.Empty;
			DataSet RrSql = null;
			bool BEncontrada = false;

			if (ParaAno != 0)
			{ //solo para modificaciones, la primera vez no tenemos consulta generada.
				stSql = "select gidenpac, itiposer,A.ganoauto, A.gnumauto, B.iautoe, B.dnopelec, A.rconc,A.econc from ifmsfa_activtau A ";
				stSql = stSql + "inner join ifmsfa_autoriza B on A.ganoauto = B.ganoauto and A.gnumauto = B.gnumauto ";
				stSql = stSql + "Where ";
				stSql = stSql + "A.itipacti ='R' and A.itiposer='" + ParaItiposer + "' and A.ganoregi=" + ParaAno.ToString() + " and A.gnumregi=" + ParaNum.ToString() + " and ";
				stSql = stSql + "B.iautoe IN('I','T','B') and B.gestsoli='A' and ISNULL(B.dnopelec,'')<>''";
				//Siendo AA el area recibida , XX el id de episodio, YY el id de episodio

				//Por cada linea, buscamos la prestacion (ECONC+RCONC) en el grid y si no esta,
				//se debe anular la autorizacion y realizar las llamadas a las cancelaciones electronicas.


				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParaConexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);

				foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
				{
					for (i = 1; i <= ParaInfSolicitudes.GetUpperBound(0); i++)
					{

						BEncontrada = false;
						Application.DoEvents();

						iTipoConc = ParaInfSolicitudes[i, 1];
						stConcepto = ParaInfSolicitudes[i, 2];
						stNOpelec = ParaInfSolicitudes[i, 7].Substring(ParaInfSolicitudes[i, 7].IndexOf('*') + 1);


						if (stConcepto.Trim().ToUpper() == Convert.ToString(iteration_row["ECONC"]).Trim().ToUpper() && iTipoConc.Trim().ToUpper() == Convert.ToString(iteration_row["RCONC"]).Trim().ToUpper())
						{
							BEncontrada = true;
							break;
						}

					}
					if (!BEncontrada)
					{
						switch(Convert.ToString(iteration_row["iautoe"]))
						{
							case "B" : 
								string tempRefParam = stNOpelec.Trim(); 
								string tempRefParam2 = Convert.ToString(iteration_row["gidenpac"]); 
								bAutorizaciones.fCancelaAutorizacionBlackBox(ref ParaConexion, ref tempRefParam, ref tempRefParam2); 
								//''                Case "I" 
								//''                   fCancelaAutorizacionITC ParaConexion, mSociedad, mgidenpac, "" & mPrestaciones(i, 2), CLng(mPrestaciones(i, 3)), miTiposer, _
								//'''                                                                     mPrestaciones(i, 1) = "ES", Mid(Trim(sprSolicitudes.Text), Len(Trim(sprSolicitudes.Text)) - 5, 6) 
								//''                Case "T" 
								//''                      fCancelaAutorizacionTraditum ParaConexion, mSociedad, mgidenpac, "" & mPrestaciones(i, 2), CLng(mPrestaciones(i, 3)), miTiposer, mServicio, _
								//'''                                                                           mPrestaciones(i, 1) = "ES", Trim(sprSolicitudes.Text) 
								break;
						}

						if (Convert.ToString(iteration_row["itiposer"]) == "C")
						{
							if (!bAutorizaciones.bAnulaEpisodioSinFactElectronica(ref ParaConexion, "R", ParaItiposer, ParaAno, ParaNum, ParaUsuario.ToUpper()))
							{
							}
						}

					}
					i = 1;

				}
			}


			//*******************************************************************************************


			ParaFormu.RecogeResultadoVolantesAutomaticos(stMensaje);

			return result;
		}

		//FUNCION fNecesitaAutorizacion
		//Devuelve FALSE si no se necesita autorizacion
		//Devuelve TRUE si se necesita autorizacion
		public bool fNecesitaAutorizacion(SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string ParaTipoConc, string ParaConcepto, string ParaInspeccion = "")
		{
			return bAutorizaciones.bNecesitaAutorizacion(ParacnConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaTipoConc, ParaConcepto, ParaInspeccion);
		}


		//FUNCION fGestionAutorizaciones
		//Devuelve FALSE si no existe control de autorizaciones
		//Devuelve TRUE si existe control de autorizaciones
		public bool fGestionAutorizaciones(SqlConnection ParacnConexion)
		{
			return bAutorizaciones.bGestionAutorizaciones(ParacnConexion);
		}

		//FUNCION fMuestraResultadoVolantesAutomaticos
		//Muestra por pantalla el resultado recibido
		public object fMuestraResultadoVolantesAutomaticos(string ParaMensajeVolantesAutomaticos, string Paraidioma = "")
		{
			Resultado tempLoadForm = Resultado.DefInstance;
			Resultado.DefInstance.mresultado = ParaMensajeVolantesAutomaticos;
			Resultado.DefInstance.midioma = Paraidioma;
			Resultado.DefInstance.ShowDialog();
			return null;
		}

		public bool fAnulaEpisodio(ref SqlConnection ParacnConexion, string ParaTipoActividad, string ParaItiposer, int ParaAno, int ParaNum, string ParaUsuario, string ParaAccion = "")
		{
			return bAutorizaciones.bAnulaEpisodio(ref ParacnConexion, ParaTipoActividad, ParaItiposer, ParaAno, ParaNum, ParaUsuario.ToUpper(), ParaAccion);
		}

		public bool fTieneAutorizacionOnline(SqlConnection ParacnConexion, string ParaTipoActividad, string ParaItiposer, int ParaAno, int ParaNum, string ParaUsuario)
		{
			return bAutorizaciones.bTieneAutorizacionOnline(ParacnConexion, ParaTipoActividad, ParaItiposer, ParaAno, ParaNum, ParaUsuario.ToUpper());
		}

		public bool fTieneAutorizacionOnlineBlackBox(SqlConnection ParacnConexion, string ParaTipoActividad, string ParaItiposer, int ParaAno, int ParaNum, string ParaUsuario)
		{
			return bAutorizaciones.bTieneAutorizacionOnlineBlackBox(ParacnConexion, ParaTipoActividad, ParaItiposer, ParaAno, ParaNum, ParaUsuario.ToUpper());
		}

		//CORRECCION ERROR:
		//EN ALGUNOS SITIOS TODAVIA QUEDABA LA LLAMDA A FANULAESOLICITUD
		//QUE FUE SUSTITUIDA POR FANULAEPISODIO. CON ESTO DECIMOS QUE SE PUEDEN
		//UTILIZAR LAS DOS FUNCIONES PARA ANULAR UNA SOLICITUD
		public bool fAnulaSolicitud(ref SqlConnection ParacnConexion, string ParaTipoActividad, string ParaItiposer, int ParaAno, int ParaNum, string ParaUsuario, string stAccion = "")
		{
			return bAutorizaciones.bAnulaEpisodio(ref ParacnConexion, ParaTipoActividad, ParaItiposer, ParaAno, ParaNum, ParaUsuario.ToUpper(), stAccion);
		}
		//-----------------

		//DE MOMENTO ESTA ENTRADA A LA CAPTURA DE DATOS DEL TEAM SE UTILIZA SOLO
		//EN LA CAPTURA DE LOS DATOS DE LA ATENCION EN URGENCIAS
		public bool fRegistroTEAM(SqlConnection ParaConexion, ref string ParaItiposer, int ParaAnoRegi, int ParaNumRegi, string ParaGidenpac, int ParaSociedad, string[, ] ParaPrestaciones, string ParaUsuario)
		{

			bool result = false;
			string tempRefParam = "";
			if (bAutorizaciones.bGestionTEAM_SOC(ParaConexion, ParaSociedad) && bAutorizaciones.bAreaControl_TEAM(ParaConexion, ref tempRefParam, ref ParaItiposer))
			{

				DatosTEAM.DefInstance.Establecevariables(ParaConexion, ParaItiposer, ParaAnoRegi, ParaNumRegi, ParaGidenpac, ParaSociedad, ParaPrestaciones, ParaUsuario.ToUpper());

				DatosTEAM.DefInstance.ShowDialog();

				result = bAutorizaciones.gErrorTEAM;

			}

			return result;
		}

		//FUNCION bNecesitaDocumentacion
		//Devuelve FALSE si ninguna actividad a realizar recibida necesita documentacion y /o volante
		//Devuelve TRUE si alguna actividad a realizar necesita documentacion y / o volante
		public bool fNecesitaDocumentacion(SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string[, ] ParaPrestaciones, string ParaInspeccion = "")
		{
			return bAutorizaciones.bNecesitaDocumentacion(ParaConexion, ParaSociedad, ParaItiposer, ParaServicio, ParaPersona, ParaPrestaciones, ParaInspeccion);
		}

		//FUNCION bGestionTEAM_SOC
		//Devuelve FALSE si no existe control de TEAM para la Sociedad
		//Devuelve TRUE si existe control de TEAM para la Sociedad
		public bool fGestionTEAM_SOC(SqlConnection ParacnConexion, int ParaSociedad)
		{
			return bAutorizaciones.bGestionTEAM_SOC(ParacnConexion, ParaSociedad);
		}

		public void fCancelaSesionITC(SqlConnection ParacnConexion, int ParaSociedad, string ParaGidenpac, ref string ParaPrestacion, int ParaPersona, string ParaNumOperacion)
		{

			// Si no est� activo ITC, no hacemos nada

			if (!bAutArgentina.bGestionITC(ParacnConexion))
			{
				return;
			}

			// Si no hay que validarla contra ITC, no hacemos nada

			if (!bAutArgentina.bUtilizaITC(ParacnConexion, ParaSociedad, "S", ParaPersona, "PR", ParaPrestacion))
			{
				return;
			}

			// Mandamos los seis �ltimos caracteres del n�mero de operaci�n, que son los que se necesitan para cancelar la operaci�n

			bAutArgentina.bCancelaSesionITC(ParacnConexion, ParaSociedad, ParaGidenpac, ref ParaPrestacion, ParaNumOperacion.Trim().Substring(ParaNumOperacion.Trim().Length - 6, Math.Min(6, 6)));

		}

		public void fCancelaSesionTraditum(SqlConnection ParacnConexion, int ParaSociedad, string ParaGidenpac, ref string ParaPrestacion, int ParaPersona, string ParaNumOperacion)
		{

			// Si no est� activo Traditum, no hacemos nada

			if (!bAutArgentina.bGestionTraditum(ParacnConexion))
			{
				return;
			}

			// Si no hay que validarla contra Traditum, no hacemos nada

			if (!bAutArgentina.bUtilizaTraditum(ParacnConexion, ParaSociedad, "S", ParaPersona, "PR", ParaPrestacion))
			{
				return;
			}

			// Si seg�n el servicio no es necesario validarla, no hacemos nada

			int ParaServicio = 0;

			string stSql = "SELECT A.gservici FROM DCODPRES A, DSERTRAD B WHERE " + 
			               "A.gprestac = '" + ParaPrestacion + "' AND " + 
			               "B.gservici = A.gservici AND " + 
			               "B.gsocieda = " + ParaSociedad.ToString() + " AND " + 
			               "B.ivalises = 'S'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParacnConexion);
			DataSet RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);

			if (RrDatos.Tables[0].Rows.Count == 0)
			{
				 
				RrDatos.Close();
				return;
			}
			else
			{
				ParaServicio = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["gservici"]);
			}

			 
			RrDatos.Close();

			// Cancelamos la autorizaci�n

			bAutArgentina.fCancelaAutorizacionTraditum(ParacnConexion, ParaSociedad, ParaGidenpac, ref ParaPrestacion, 1, "S", ParaServicio, false, ParaNumOperacion);

		}

		public string fQueUtiliza(SqlConnection ParacnConexion, int ParaSociedad, string ParaPrestacion, int ParaPersona)
		{

			try
			{

				// En funci�n de qu� utilice mandamos un valor u otro


				if (bAutArgentina.bUtilizaTraditum(ParacnConexion, ParaSociedad, "S", ParaPersona, "PR", ParaPrestacion))
				{
					return "T";
				}
				else if (bAutArgentina.bUtilizaITC(ParacnConexion, ParaSociedad, "S", ParaPersona, "PR", ParaPrestacion))
				{ 
					return "I";
				}
				else
				{
					return "";
				}
			}
			catch
			{

				return "";
			}
		}

		public string fConfirmaSesionTraditum(SqlConnection ParacnConexion, int ParaSociedad, string ParaGidenpac, ref string ParaPrestacion, int ParaPersona, ref string ParaMotivoRechazo, ref string ParaNumOpera, ref string ParaCopago)
		{

			string result = String.Empty;
			try
			{

				string stResultado = String.Empty;
				string stNumAuto = String.Empty;
				string stNumOpera = String.Empty;
				string stFirmaAuto = String.Empty;
				string stCopago = String.Empty;
				string stMotivoRechazo = String.Empty;

				// Si no est� activo Traditum, no hacemos nada

				if (!bAutArgentina.bGestionTraditum(ParacnConexion))
				{
					return "A";
				}

				// Si no hay que validarla contra Traditum, no hacemos nada

				if (!bAutArgentina.bUtilizaTraditum(ParacnConexion, ParaSociedad, "S", ParaPersona, "PR", ParaPrestacion))
				{
					return "A";
				}

				// Si seg�n el servicio no es necesario validarla, no hacemos nada

				string stSql = String.Empty;
				DataSet RrDatos = null;
				int ParaServicio = 0;

				stSql = "SELECT A.gservici FROM DCODPRES A, DSERTRAD B WHERE " + 
				        "A.gprestac = '" + ParaPrestacion + "' AND " + 
				        "B.gservici = A.gservici AND " + 
				        "B.gsocieda = " + ParaSociedad.ToString() + " AND " + 
				        "B.ivalises = 'S'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					 
					RrDatos.Close();
					return "A";
				}
				else
				{
					ParaServicio = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["gservici"]);
				}

				 
				RrDatos.Close();

				// Mandamos autorizaci�n

				bAutArgentina.fAutorizaTraditum(ParacnConexion, ParaSociedad, ParaGidenpac, ref ParaPrestacion, 1, "S", ParaServicio, false, ref stResultado, ref stNumAuto, ref stNumOpera, ref stFirmaAuto, ref stCopago, ref stMotivoRechazo);

				// Si se autoriz�, devolveremos True

				result = stResultado;
				ParaMotivoRechazo = stMotivoRechazo;
				ParaNumOpera = stNumOpera;
				ParaCopago = stCopago;

				return result;
			}
			catch (System.Exception excep)
			{

				result = "";
				ParaMotivoRechazo = excep.Message;

				return result;
			}
		}

		public string fConfirmaSesionITC(SqlConnection ParacnConexion, int ParaSociedad, string ParaGidenpac, ref string ParaPrestacion, int ParaPersona, int ParaCantidad, string ParaTipoServicio, ref string ParaMotivoRechazo, string stEpisodioSesiones, ref string ParaNumOpera)
		{

			string result = String.Empty;
			try
			{

				string stResultado = String.Empty;
				string stNumAuto = String.Empty;
				string stNumOpera = String.Empty;
				string stFirmaAuto = String.Empty;
				string stCopago = String.Empty;
				string stMotivoRechazo = String.Empty;
				string stSql = String.Empty;
				DataSet RrDatos = null;

				// Si no est� activo ITC, no hacemos nada

				if (!bAutArgentina.bGestionITC(ParacnConexion))
				{
					return "A";
				}

				// Si no hay que validarla contra ITC, no hacemos nada

				if (!bAutArgentina.bUtilizaITC(ParacnConexion, ParaSociedad, ParaTipoServicio, ParaPersona, "PR", ParaPrestacion))
				{
					return "A";
				}

				// Tomamos el n�mero de autorizaci�n del episodio de sesiones

				stSql = "SELECT A.dnautori FROM IFMSFA_AUTORIZA A, IFMSFA_ACTIVTAU B WHERE " + 
				        "B.itiposer = 'S' AND " + 
				        "B.ganoregi = " + stEpisodioSesiones.Substring(0, Math.Min(4, stEpisodioSesiones.Length)) + " AND " + 
				        "B.gnumregi = " + stEpisodioSesiones.Substring(4) + " AND " + 
				        "A.ganoauto = B.ganoauto AND " + 
				        "A.gnumauto = B.gnumauto";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					stNumAuto = ("" + Convert.ToString(RrDatos.Tables[0].Rows[0]["dnautori"])).Trim();
				}

				 
				RrDatos.Close();

				if (stNumAuto.Trim() == "")
				{

					result = "A";

				}
				else
				{

					// Mandamos autorizaci�n

					bAutArgentina.fAutorizaITC(ParacnConexion, ParaSociedad, ParaGidenpac, ref ParaPrestacion, ParaCantidad, ParaTipoServicio, false, ref stResultado, ref stNumAuto, ref stNumOpera, ref stFirmaAuto, ref stCopago, ref stMotivoRechazo, true);

					// Si se autoriz�, devolveremos True

					result = stResultado;
					ParaMotivoRechazo = stMotivoRechazo;
					ParaNumOpera = stNumOpera;

				}

				return result;
			}
			catch (System.Exception excep)
			{

				result = "";
				ParaMotivoRechazo = excep.Message;

				return result;
			}
		}
	}
}