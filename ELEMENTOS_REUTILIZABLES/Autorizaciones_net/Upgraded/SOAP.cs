using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Xml;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using MSXML2;
namespace Autorizaciones
{
	internal static class SOAP
	{

		public const string ON_LINE = "ONLINE";
		public const string OFF_LINE = "OFFLINE";

		private const string ESPACIODENOMBRES = "http://indracapiosanitas/";

		//cabeceras de las respuestas con error.
		private static readonly string CABECERASOAPRESP = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Environment.NewLine + 
		                                                  "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + Environment.NewLine + 
		                                                  "<soap:Body><SolAutorizacionResponse xmlns=\"http://indracapiosanitas/\"><SolAutorizacionResult><fallo>";

		private static readonly string CABECERASOAPRESPREA = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Environment.NewLine + 
		                                                     "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + Environment.NewLine + 
		                                                     "<soap:Body><SolReaHonMedicosResponse xmlns=\"http://indracapiosanitas/\"><SolReaHonMedicosResult><fallo>";

		private static readonly string CABECERASOAPRESPANU = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Environment.NewLine + 
		                                                     "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + Environment.NewLine + 
		                                                     "<soap:Body><SolAnulacionResponse xmlns=\"http://indracapiosanitas/\"><SolAnulacionResult><fallo>";

		//Pies de las respuestas con error.
		private const string PIESOAPRESPREA = "</fallo></SolReaHonMedicosResult></SolReaHonMedicosResponse></soap:Body></soap:Envelope>";

		private const string PIESOAPRESP = "</fallo></SolAutorizacionResult></SolAutorizacionResponse></soap:Body></soap:Envelope>";

		private const string PIESOAPANU = "</fallo></SolAnulacionResult></SolAnulacionResponse></soap:Body></soap:Envelope>";


		private static readonly string CABECERASOAP = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Environment.NewLine + 
		                                              "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + 
		                                              "<soap:Body>";
		private static readonly string PIESOAP = "</soap:Body>" + 
		                                         "</soap:Envelope>";
		private static string PaginaWebService = String.Empty;


		private static string CrearSolAutorizacion(string destinatario, string fechaHora, string modEntDatos, string codEntidadPr, string codUsuario, string codPais, string codProvincia, string claveop, string CodInfra, string panTarjeta, ref string verTarjeta, string datosPista1, string datosPista2, string refOpera, string codOrigen, string indCenPropios, string indTipoCodificacion, string codActo, string codActoCentro, string codEsp, string codSubEsp, string codProf, string nifProf, string numColegiado, string NumDu, ref string codDiagn, string indMismoCentro, string codRegimen, string codEsp2, string codSubEsp2, string numSesiones, string fechacitacion, string fecharealizacion, string datosOp)
		{

			string result = String.Empty;
			if (codDiagn != "")
			{
				if ((codDiagn.IndexOf('.') + 1) == 0)
				{
					codDiagn = codDiagn + ".";
				}
			}

			if (verTarjeta.Trim() == "")
			{
				verTarjeta = "1";
			}


			result = CABECERASOAP + 
			         "<SolAutorizacion xmlns=\"" + ESPACIODENOMBRES + "\">";
			result = result + "" + 
			         "<destinatario>" + destinatario + "</destinatario>" + Environment.NewLine + 
			         "<fechaHora>" + fechaHora + "</fechaHora>" + Environment.NewLine + 
			         "<modEntDatos>" + modEntDatos + "</modEntDatos>" + Environment.NewLine + 
			         "<codEntidadPr>" + codEntidadPr + "</codEntidadPr>" + Environment.NewLine + 
			         "<codUsuario>" + codUsuario + "</codUsuario>" + Environment.NewLine + 
			         "<codPais>" + codPais + "</codPais>" + Environment.NewLine + 
			         "<codProvincia>" + codProvincia + "</codProvincia>" + Environment.NewLine + 
			         "<claveOp>" + claveop + "</claveOp>" + Environment.NewLine + 
			         "<codInfra>" + CodInfra + "</codInfra>" + Environment.NewLine + 
			         "<panTarjeta>" + panTarjeta + "</panTarjeta>" + Environment.NewLine + 
			         "<verTarjeta>" + verTarjeta + "</verTarjeta>" + Environment.NewLine + 
			         "<datosPista1>" + datosPista1 + "</datosPista1>" + Environment.NewLine + 
			         "<datosPista2>" + datosPista2 + "</datosPista2>" + Environment.NewLine + 
			         "<refOpera>" + refOpera + "</refOpera>" + Environment.NewLine + 
			         "<codOrigen>" + codOrigen + "</codOrigen>" + Environment.NewLine + 
			         "<indCenPropios>" + indCenPropios + "</indCenPropios>" + Environment.NewLine + 
			         "<indTipoCodificacion>" + indTipoCodificacion + "</indTipoCodificacion>" + Environment.NewLine + 
			         "<codActo>" + codActo + "</codActo>" + Environment.NewLine + 
			         "<codActoCentro>" + codActoCentro + "</codActoCentro>";
			result = result + 
			         "<codEsp>" + codEsp + "</codEsp>" + Environment.NewLine + 
			         "<codSubEsp>" + codSubEsp + "</codSubEsp>" + Environment.NewLine + 
			         "<codProf>" + codProf + "</codProf>" + 
			         "<nifProf>" + nifProf + "</nifProf>" + Environment.NewLine + 
			         "<numColegiado>" + numColegiado + "</numColegiado>" + 
			         "<numDU>" + NumDu + "</numDU>";
			result = result + 
			         "<codDiagn>" + codDiagn + "</codDiagn>" + 
			         "<indMismoCentro>" + indMismoCentro + "</indMismoCentro>" + 
			         "<codRegimen>" + codRegimen + "</codRegimen>" + Environment.NewLine + 
			         "<codEsp2>" + codEsp2 + "</codEsp2>" + 
			         "<codSubEsp2>" + codSubEsp2 + "</codSubEsp2>" + Environment.NewLine + 
			         "<numSesiones>" + numSesiones + "</numSesiones>" + 
			         "<fechacitacion>" + fechacitacion + "</fechacitacion>" + Environment.NewLine + 
			         "<fechaRealizacion>" + fecharealizacion + "</fechaRealizacion>" + 
			         "<datosOp>" + datosOp + "</datosOp>" + Environment.NewLine + 
			         "</SolAutorizacion>";
			return result + PIESOAP;
		}
		private static string CrearSolReaHonMedicos(string destinatario, string fechaHora, string modEntDatos, string codEntidadPr, string codUsuario, string codPais, string codProvincia, string claveop, string CodInfra, string panTarjeta, string verTarjeta, string datosPista1, string datosPista2, string refOpera, string codOrigen, string indCenPropios, string indTipoCodificacion, string codActo, string codActoCentro, string codEsp, string codSubEsp, string codProf, string nifProf, string numColegiado, string NumDu, string codDiagn, string indMismoCentro, string codalbaran, string numSesiones, string fecharealizacion, string datosOp, string Codformatoregistro, string regdatos)
		{
			// nifProf = "11111111111111"
			// numColegiado = 0
			// codActoCentro = "CEF0011"
			// codEsp = "10220"
			// codSubEsp = "10220"

			string result = String.Empty;
			if (codDiagn != "")
			{
				if ((codDiagn.IndexOf('.') + 1) == 0)
				{
					codDiagn = codDiagn + ".";
				}
			}

			if (verTarjeta.Trim() == "")
			{
				verTarjeta = "1";
			}

			result = CABECERASOAP + 
			         "<SolReaHonMedicos xmlns=\"" + ESPACIODENOMBRES + "\">";
			result = result + "" + 
			         "<destinatario>" + destinatario + "</destinatario>" + Environment.NewLine + 
			         "<fechaHora>" + fechaHora + "</fechaHora>" + Environment.NewLine + 
			         "<modEntDatos>" + modEntDatos + "</modEntDatos>" + Environment.NewLine + 
			         "<codEntidadPr>" + codEntidadPr + "</codEntidadPr>" + Environment.NewLine + 
			         "<codUsuario>" + codUsuario + "</codUsuario>" + Environment.NewLine + 
			         "<codPais>" + codPais + "</codPais>" + Environment.NewLine + 
			         "<codProvincia>" + codProvincia + "</codProvincia>" + Environment.NewLine + 
			         "<claveOp>" + claveop + "</claveOp>" + Environment.NewLine + 
			         "<codInfra>" + CodInfra + "</codInfra>" + Environment.NewLine + 
			         "<panTarjeta>" + panTarjeta + "</panTarjeta>" + Environment.NewLine + 
			         "<verTarjeta>" + verTarjeta + "</verTarjeta>" + Environment.NewLine + 
			         "<datosPista1>" + datosPista1 + "</datosPista1>" + Environment.NewLine + 
			         "<datosPista2>" + datosPista2 + "</datosPista2>" + Environment.NewLine + 
			         "<refOpera>" + refOpera + "</refOpera>" + Environment.NewLine + 
			         "<codOrigen>" + codOrigen + "</codOrigen>" + Environment.NewLine + 
			         "<indCenPropios>" + indCenPropios + "</indCenPropios>" + Environment.NewLine + 
			         "<indTipoCodificacion>" + indTipoCodificacion + "</indTipoCodificacion>" + Environment.NewLine + 
			         "<codActo>" + codActo + "</codActo>" + Environment.NewLine + 
			         "<codActoCentro>" + codActoCentro + "</codActoCentro>";
			result = result + 
			         "<codEsp>" + codEsp + "</codEsp>" + Environment.NewLine + 
			         "<codSubEsp>" + codSubEsp + "</codSubEsp>" + Environment.NewLine + 
			         "<codProf>" + codProf + "</codProf>" + 
			         "<nifProf>" + nifProf + "</nifProf>" + Environment.NewLine + 
			         "<numColegiado>" + numColegiado + "</numColegiado>" + 
			         "<numDU>" + NumDu + "</numDU>";
			result = result + 
			         "<codDiagn>" + codDiagn + "</codDiagn>" + 
			         "<indMismoCentro>" + indMismoCentro + "</indMismoCentro>" + 
			         "<codalbaran>" + codalbaran + "</codalbaran>" + Environment.NewLine + 
			         "<numSesiones>" + numSesiones + "</numSesiones>" + 
			         "<fechaRealizacion>" + fecharealizacion + "</fechaRealizacion>" + 
			         "<datosOp>" + datosOp + "</datosOp>" + Environment.NewLine + 
			         "<Codformatoregistro>" + Codformatoregistro + "</Codformatoregistro>" + Environment.NewLine + 
			         "<regdatos>" + regdatos + "</regdatos>" + Environment.NewLine + 
			         "</SolReaHonMedicos>";
			return result + PIESOAP;
		}

		private static string CrearSolAnulacion(string destinatario, string panTarjeta, string codProceso, string FechaTransaccion, string codEntdad, string codUsuario, string refOperacion, string codOrigen, string fechaSesionOpOriginal, string fechaOpOriginal, string idOpOriginal, string numAutorizacion, string indTipoCodificacion, string CodServicio, string CodServicioCentro, string fecharealizacion, string claveop)
		{
			string result = String.Empty;
			result = CABECERASOAP + 
			         "<SolAnulacion xmlns=\"" + ESPACIODENOMBRES + "\">";
			result = result + "" + 
			         "<destinatario>" + destinatario + "</destinatario>" + 
			         "<panTarjeta>" + panTarjeta + "</panTarjeta>" + 
			         "<codProceso>" + codProceso + "</codProceso>" + 
			         "<fechatransaccion>" + FechaTransaccion + "</fechatransaccion>" + 
			         "<codEntidad>" + codEntdad + "</codEntidad>" + 
			         "<codUsuario>" + codUsuario + "</codUsuario>" + 
			         "<refOperacion>" + refOperacion + "</refOperacion>" + 
			         "<codOrigen>" + codOrigen + "</codOrigen>" + 
			         "<fechaSesionOpOriginal>" + fechaSesionOpOriginal + "</fechaSesionOpOriginal>" + 
			         "<fechaOpOriginal>" + fechaOpOriginal + "</fechaOpOriginal>" + 
			         "<idOpOriginal>" + idOpOriginal + "</idOpOriginal>" + 
			         "<numAutorizacion>" + numAutorizacion + "</numAutorizacion>" + 
			         "<indTipoCodificacion>" + indTipoCodificacion + "</indTipoCodificacion>" + 
			         "<CodServicio>" + CodServicio + "</CodServicio>" + 
			         "<CodServicioCentro>" + CodServicioCentro + "</CodServicioCentro>" + 
			         "<fecharealizacion>" + fecharealizacion + "</fecharealizacion>" + 
			         "<claveOperativa>" + claveop + "</claveOperativa>" + 
			         "</SolAnulacion>";
			return result + PIESOAP;
		}



		internal static string SolAnulacion(string ParaTipoActividad, SqlConnection ParacnConexion, ref string TIPOENVIO_ONOFF, string destinatario, string panTarjeta, string codProceso, string FechaTransaccion, string codEntdad, string codUsuario, string refOperacion, string codOrigen, string fechaSesionOpOriginal, string fechaOpOriginal, string idOpOriginal, string numAutorizacion, string indTipoCodificacion, string CodServicio, string CodServicioCentro, string fecharealizacion, string claveop)
		{
			string result = String.Empty;
			XmlDocument parser = new XmlDocument();
			string Respuesta = String.Empty;
			try
			{
				Respuesta = CrearSolAnulacion(destinatario, panTarjeta, codProceso, FechaTransaccion, codEntdad, codUsuario, refOperacion, codOrigen, fechaSesionOpOriginal, fechaOpOriginal, idOpOriginal, numAutorizacion, indTipoCodificacion, CodServicio, CodServicioCentro, fecharealizacion, claveop);
				try
				{
					parser.LoadXml(Respuesta);
				}
				catch
				{
				}
				Respuesta = EnviarComando(ParaTipoActividad, ParacnConexion, ref TIPOENVIO_ONOFF, parser.InnerXml, "http://indracapiosanitas/SolAnulacion", claveop, "B", refOperacion, FechaTransaccion);
				return Respuesta;
			}
			catch
			{
				if (result == "")
				{
					Respuesta = CABECERASOAPRESPANU + "<Error>" + OFF_LINE + "</Error>" + PIESOAPANU;
				}
				else
				{
					Respuesta = CABECERASOAPRESPANU + "<Error>" + Information.Err().Number.ToString() + "</Error>" + PIESOAPANU;
				}
				return Respuesta;
			}
		}


		internal static string SolAutorizacion(string ParaTipoActividad, SqlConnection ParacnConexion, ref string TIPOENVIO_ONOFF, string destinatario, string fechaHora, string modEntDatos, string codEntidadPr, string codUsuario, string codPais, string codProvincia, string claveop, string CodInfra, string panTarjeta, string verTarjeta, string datosPista1, string datosPista2, string refOpera, string codOrigen, string indCenPropios, string indTipoCodificacion, string codActo, string codActoCentro, string codEsp, string codSubEsp, string codProf, string nifProf, string numColegiado, string NumDu, string codDiagn, string indMismoCentro, string codRegimen, string codEsp2, string codSubEsp2, string numSesiones, string fechacitacion, string fecharealizacion, string datosOp)
		{
			XmlDocument parser = new XmlDocument();
			string Respuesta = String.Empty;
			try
			{
				string tempRefParam = verTarjeta.Trim();
				string tempRefParam2 = codDiagn.Trim();
				Respuesta = CrearSolAutorizacion(destinatario.Trim(), fechaHora.Trim(), modEntDatos.Trim(), codEntidadPr.Trim(), codUsuario.Trim(), codPais.Trim(), codProvincia.Trim(), claveop.Trim(), CodInfra.Trim(), panTarjeta.Trim(), ref tempRefParam, datosPista1.Trim(), datosPista2.Trim(), refOpera.Trim(), codOrigen.Trim(), indCenPropios.Trim(), indTipoCodificacion.Trim(), codActo.Trim(), codActoCentro.Trim(), codEsp.Trim(), codSubEsp.Trim(), codProf.Trim(), nifProf.Trim(), numColegiado.Trim(), NumDu.Trim(), ref tempRefParam2, indMismoCentro.Trim(), codRegimen.Trim(), codEsp2.Trim(), codSubEsp2.Trim(), numSesiones.Trim(), fechacitacion.Trim(), fecharealizacion.Trim(), datosOp.Trim());
				try
				{
					parser.LoadXml(Respuesta);
				}
				catch
				{
				}
				Respuesta = EnviarComando(ParaTipoActividad, ParacnConexion, ref TIPOENVIO_ONOFF, parser.InnerXml, "http://indracapiosanitas/SolAutorizacion", claveop, "A", refOpera, fechaHora);
				return Respuesta;
			}
			catch (System.Exception excep)
			{
				Respuesta = CABECERASOAPRESP + "" + excep.Message + "" + PIESOAPRESP;

				return Respuesta;
			}
		}

		internal static string SolReaHonMedicos(string ParaTipoActividad, SqlConnection ParacnConexion, ref string TIPOENVIO_ONOFF, string destinatario, string fechaHora, string modEntDatos, string codEntidadPr, string codUsuario, string codPais, string codProvincia, string claveop, string CodInfra, string panTarjeta, string verTarjeta, string datosPista1, string datosPista2, string refOpera, string codOrigen, string indCenPropios, string indTipoCodificacion, string codActo, string codActoCentro, string codEsp, string codSubEsp, string codProf, string nifProf, string numColegiado, string NumDu, string codDiagn, string indMismoCentro, string codalbaran, string numSesiones, string fecharealizacion, string datosOp, string Codformatoregistro, string regdatos)
		{
			XmlDocument parser = new XmlDocument();
			string Respuesta = String.Empty;
			try
			{
				Respuesta = CrearSolReaHonMedicos(destinatario.Trim(), fechaHora.Trim(), modEntDatos.Trim(), codEntidadPr.Trim(), codUsuario.Trim(), codPais.Trim(), codProvincia.Trim(), claveop.Trim(), CodInfra.Trim(), panTarjeta.Trim(), verTarjeta.Trim(), datosPista1.Trim(), datosPista2.Trim(), refOpera.Trim(), codOrigen.Trim(), indCenPropios.Trim(), indTipoCodificacion.Trim(), codActo.Trim(), codActoCentro.Trim(), codEsp.Trim(), codSubEsp.Trim(), codProf.Trim(), nifProf.Trim(), numColegiado.Trim(), NumDu.Trim(), codDiagn.Trim(), indMismoCentro.Trim(), codalbaran.Trim(), numSesiones.Trim(), fecharealizacion.Trim(), datosOp.Trim(), Codformatoregistro.Trim(), regdatos.Trim());
				try
				{
					parser.LoadXml(Respuesta);
				}
				catch
				{
				}
				Respuesta = EnviarComando(ParaTipoActividad, ParacnConexion, ref TIPOENVIO_ONOFF, parser.InnerXml, "http://indracapiosanitas/SolReaHonMedicos", claveop, "A", refOpera, fechaHora);
				return Respuesta;
			}
			catch (System.Exception excep)
			{
				Respuesta = CABECERASOAPRESPREA + "" + excep.Message + "" + PIESOAPRESPREA;
				return Respuesta;
			}
		}

		internal static string EnviarComando(string accion, SqlConnection ParacnConexion, ref string Tipo, string sXml, string sSoapAction, string idCentro = "", string itipoMov = "", string RefOrigen = "", string Fcreacion = "", bool Noinsertar = false)
		{

			string result = String.Empty;
			string Sql = String.Empty;
			string LoginAuT = String.Empty;
			string PassAut = String.Empty;
			string CodAcc = String.Empty;

			bool nOeSxML = false;

			string ganoauto = String.Empty;
			string gnumauto = String.Empty;
			System.DateTime FechaComienzoTransaccion = DateTime.FromOADate(0);
			bool PasarCertificado = true;
			XmlDocument RespXML = new XmlDocument();
			int TimerEspera = 60;
			bool Asincrono = true;

			bool HTTPS = false;
			if (RefOrigen != "")
			{
				ganoauto = Conversion.Str(RefOrigen).Substring(0, Math.Min(5, Conversion.Str(RefOrigen).Length));
				gnumauto = RefOrigen.Substring(4, RefOrigen.Length - 4);
			}
			else
			{
				//UPGRADE_ISSUE: (2064) SSCalendarWidgets_A.WIConstants property WIConstants.Year was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				ganoauto = DateTime.Today.Year.ToString();
				gnumauto = "000";
			}

			string Mensajexml = sXml;
			if (!Noinsertar)
			{
				if (accion == "P" || accion == "020000")
				{

					Sql = "delete from ENVIOS_SOLAUTORIZACION where ";
					Sql = Sql + "Ganoauto=" + ganoauto + " and gnumauto=" + gnumauto + " and ";
					Sql = Sql + "idCentro= '" + idCentro + "' and ";
					Sql = Sql + "itipomov='" + itipoMov + "'";
					SqlCommand tempCommand = new SqlCommand(Sql, ParacnConexion);
					tempCommand.ExecuteNonQuery();

					Sql = "INSERT INTO ENVIOS_SOLAUTORIZACION(idCentro,itipoMov ,Ganoauto,Gnumauto,Fcreacion,Mensajexml,estado) VALUES ";
					Sql = Sql + "('" + idCentro + "',";
					Sql = Sql + "'" + itipoMov + "',";
					Sql = Sql + "" + ganoauto + ",";
					Sql = Sql + "" + gnumauto + ",";
					Sql = Sql + "getdate(),";
					Sql = Sql + "'" + Mensajexml + "',";
					if (Tipo == ON_LINE)
					{
						Sql = Sql + "2)";
					}
					else
					{
						Sql = Sql + "null)";
					}


					SqlCommand tempCommand_2 = new SqlCommand(Sql, ParacnConexion);
					tempCommand_2.ExecuteNonQuery();
					result = CABECERASOAPRESP + "" + OFF_LINE + "" + PIESOAPRESP;

				}

				if (itipoMov == "B")
				{
					result = CABECERASOAPRESPANU + "" + OFF_LINE + "" + PIESOAPANU;
				}
			}


			DataSet rr = null;
			int estadoaux = 0;

            MSXML2.ServerXMLHTTP oHttReq = null;

            //MSXML2.XMLHTTP60 oHttReq = null;
            if (Tipo == ON_LINE)
			{


				Sql = "SELECT * FROM SCONSGLO WHERE GCONSGLO='MIDDLEWA'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, ParacnConexion);
				rr = new DataSet();
				tempAdapter.Fill(rr);
				if (rr.Tables[0].Rows.Count != 0)
				{
					PaginaWebService = (Convert.ToString(rr.Tables[0].Rows[0]["valfanu1"]) + "").Trim() + (Convert.ToString(rr.Tables[0].Rows[0]["valfanu2"]) + "").Trim();
					LoginAuT = (Convert.ToString(rr.Tables[0].Rows[0]["valfanu3"]) + "").Trim();

					//parametrizamos que la contraseņa pueda estar encriptada o no
					//segun encriptacion IFMS (hay programa en donde estan las versiones EncriptarPWD.exe)
					if ((Convert.ToString(rr.Tables[0].Rows[0]["valfanu3i1"]) + "").Trim() == "S")
					{
						string tempRefParam = Convert.ToString(rr.Tables[0].Rows[0]["valfanu3i2"]).Trim() + " ";
						PassAut = Serrores.Decodificar_frase(ref tempRefParam).Trim();
					}
					else
					{
						PassAut = (Convert.ToString(rr.Tables[0].Rows[0]["valfanu3i2"]) + "").Trim();
					}
					//-------------------------------------------------------------

					if ((Convert.ToString(rr.Tables[0].Rows[0]["valfanu1"]) + "").Trim().ToUpper() == "HTTPS://")
					{
						HTTPS = true;
					}
					//If Trim(LoginAuT) <> "" Then
					//PosAut = InStr(1, LoginAuT, "&")
					//If PosAut <> 0 Then
					//PassAut = Right(LoginAuT, Len(LoginAuT) - PosAut)
					//LoginAuT = Left(LoginAuT, PosAut - 1)
					//End If
					if (!Convert.IsDBNull(rr.Tables[0].Rows[0]["nnumeri1"]))
					{
						if (Convert.ToDouble(rr.Tables[0].Rows[0]["nnumeri1"]) == 0)
						{
							PasarCertificado = false;
						}
					}
					if (!Convert.IsDBNull(rr.Tables[0].Rows[0]["nnumeri2"]))
					{
						TimerEspera = Convert.ToInt32(rr.Tables[0].Rows[0]["nnumeri2"]);
					}
					if (!Convert.IsDBNull(rr.Tables[0].Rows[0]["nnumeri3"]))
					{
						if (Convert.ToDouble(rr.Tables[0].Rows[0]["nnumeri3"]) == 0)
						{
							Asincrono = false;
						}
					}
					//passaut=instr(
				}
			 
				rr.Close();


				// Set oHttReq = New XMLHTTPRequest

				if (HTTPS)
				{
					//Con seguridad
					oHttReq = (MSXML2.ServerXMLHTTP)new MSXML2.ServerXMLHTTP();
                    //oHttReq = (MSXML2.XMLHTTP60)new MSXML2.XMLHTTP60();
                   

                    oHttReq.open("POST", PaginaWebService, Asincrono, LoginAuT, PassAut);
					oHttReq.setRequestHeader("ssl-https", "on");
					if (PasarCertificado)
					{
                        //oHttReq.getSetOption(2, 13056);
                        oHttReq.setOption(SERVERXMLHTTP_OPTION.SXH_OPTION_IGNORE_SERVER_SSL_CERT_ERROR_FLAGS, 13056);

                    }
                    oHttReq.setRequestHeader("Content-Type", "application/soap+xml");
					oHttReq.setRequestHeader("Content-length", sXml.Length.ToString());
					oHttReq.setRequestHeader("SOAPAction", sSoapAction);
				}
				else
				{
					oHttReq = (MSXML2.ServerXMLHTTP)new MSXML2.ServerXMLHTTP();
                    //oHttReq = (MSXML2.XMLHTTP60)new MSXML2.XMLHTTP60();
                    oHttReq.open("POST", PaginaWebService, Asincrono, LoginAuT, PassAut);
					oHttReq.setRequestHeader("Content-Type", "application/soap+xml");
					oHttReq.setRequestHeader("Content-length", sXml.Length.ToString());
					oHttReq.setRequestHeader("SOAPAction", sSoapAction);
					if (PasarCertificado)
					{
                        //oHttReq.getSetOption(2, 13056);
                        oHttReq.setOption(SERVERXMLHTTP_OPTION.SXH_OPTION_IGNORE_SERVER_SSL_CERT_ERROR_FLAGS, 13056);
                        
					}

				}

				// oHttReq.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
				//ohttreq.setRequestHeader "Content-Type", "application/soap+xml"
				FechaComienzoTransaccion = DateTime.Now;
				oHttReq.send(sXml);
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();
				if (Asincrono)
				{
					while (oHttReq.readyState != 4 && ((int) DateAndTime.DateDiff("s", FechaComienzoTransaccion, DateTime.Now, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) < TimerEspera)
					{
						Application.DoEvents();
					}
				}
				nOeSxML = true;

				nOeSxML = !(oHttReq.responseText.ToUpper().IndexOf(("?XML").ToUpper()) >= 0);
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();
				Application.DoEvents();

				if (nOeSxML)
				{
					//TIme Out

					if (accion == "P" || accion == "020000")
					{
						result = CABECERASOAPRESP + "Fallo De Red" + PIESOAPRESP;
					}
					if (accion == "010000" || accion == "010001")
					{
						result = CABECERASOAPRESPREA + "Fallo De Red" + PIESOAPRESPREA;
					}
					if (itipoMov == "B")
					{
						result = CABECERASOAPRESPANU + "" + OFF_LINE + "" + PIESOAPANU;
						Tipo = OFF_LINE;
					}
				}
				else
				{
					oHttReq.getAllResponseHeaders();
					result = oHttReq.responseText;


					CodAcc = "";
					try
					{
						RespXML.LoadXml(result);
					}
					catch
					{
					}
					CodAcc = RespXML.GetElementsByTagName("CodAcc").Item(0).InnerText;



					//tengo que parsear la respuesta.

					if ((accion == "P" || accion == "020000") && Tipo == ON_LINE)
					{
						if (CodAcc.Trim() == "100" || CodAcc.Trim() == "000")
						{
							estadoaux = 3;
						}
						else
						{
							estadoaux = 4;
						}
						string tempRefParam2 = RespXML.GetElementsByTagName("fallo").Item(0).InnerText;
						string tempRefParam3 = "'";
						Sql = "update ENVIOS_SOLAUTORIZACION set estado=" + estadoaux.ToString() + ",destado='" + Serrores.proReplace(ref tempRefParam2, tempRefParam3, "") + "' where ";
						Sql = Sql + "Ganoauto=" + ganoauto + " and gnumauto=" + gnumauto + " and ";
						Sql = Sql + "idCentro= '" + idCentro + "' and ";
						Sql = Sql + "itipomov='" + itipoMov + "'";
						SqlCommand tempCommand_3 = new SqlCommand(Sql, ParacnConexion);
						tempCommand_3.ExecuteNonQuery();

						Sql = "delete from ENVIOS_SOLAUTORIZACION where ";
						Sql = Sql + "Ganoauto=" + ganoauto + " and gnumauto=" + gnumauto + " and ";
						Sql = Sql + "idCentro= '" + idCentro + "' and ";
						Sql = Sql + "itipomov='" + itipoMov + "'";
						SqlCommand tempCommand_4 = new SqlCommand(Sql, ParacnConexion);
						tempCommand_4.ExecuteNonQuery();
					}
				}
			}
			return result;
		}
		internal static string FormatearFechaSoap(string Fecha)
		{
			string result = String.Empty;
			if (Fecha != "")
			{
				result = DateTime.Parse(Fecha).ToString("yyyyMMddHHMMss");
			}
			return result;
		}
		internal static string SOAPFormateaFecha(string Fecha)
		{
			string Hora = "00";
			string Minuto = "00";
			string Segundo = "00";
			if (Fecha.Length > 8)
			{
				Hora = ("0" + Fecha.Substring(8, Math.Min(2, Fecha.Length - 8))).Substring(Math.Max(("0" + Fecha.Substring(8, Math.Min(2, Fecha.Length - 8))).Length - 2, 0));
				Minuto = ("0" + Fecha.Substring(10, Math.Min(2, Fecha.Length - 10))).Substring(Math.Max(("0" + Fecha.Substring(10, Math.Min(2, Fecha.Length - 10))).Length - 2, 0));
				Segundo = ("0" + Fecha.Substring(12, Math.Min(2, Fecha.Length - 12))).Substring(Math.Max(("0" + Fecha.Substring(12, Math.Min(2, Fecha.Length - 12))).Length - 2, 0));
			}
			string Dia = Fecha.Substring(6, Math.Min(2, Fecha.Length - 6));
			string Mes = Fecha.Substring(4, Math.Min(2, Fecha.Length - 4));
			string anno = Fecha.Substring(0, Math.Min(4, Fecha.Length));


			System.DateTime TempDate = DateTime.FromOADate(0);
			return (DateTime.TryParse(Dia + "/" + Mes + "/" + anno + " " + Hora + ":" + Minuto + ":" + Segundo, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : Dia + "/" + Mes + "/" + anno + " " + Hora + ":" + Minuto + ":" + Segundo;
		}
	}
}