using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace Autorizaciones
{
	internal static class bAutArgentina
	{


		// Nombre del equipo

		//extern public static void Sleep(int dwMilliseconds);

		// Constantes de cabeceras Traditum

		private const string ELEGIBILIDAD_SOL = "ZQI^Z01^ZQI_Z01";
		private const string ELEGIBILIDAD_RES = "ZPI^Z01^ZPI_Z01";

		private const string AUTORIZACION_SOL = "ZQA^Z02^ZQA_Z02";
		private const string AUTORIZACION_RES = "ZPA^Z02^ZPA_Z02";

		private const string CANCELACION_SOL = "ZQA^Z04^ZQA_Z02";
		private const string CANCELACION_RES = "ZPA^Z04^ZPA_Z02";

		// �rea de control Traditum

		private static string stPathTraditum = String.Empty;
		private static int iTiempoEsperaTraditum = 0;
		private static int iNumeroTerminalTraditum = 0;
		private static string stIdSitioEmisor = String.Empty;
		private static string stPrestacionUrgenciaTraditum = String.Empty;
		public static bool bManualTimeOutTraditum = false;

		private static string stPathTraditumUpload = String.Empty;
		private static string stPathTraditumDownload = String.Empty;
		private static string stPathTraditumTX = String.Empty;

		// �rea de control ITC

		private static string stPathITC = String.Empty;
		private static int iTiempoEsperaITC = 0;
		private static int iNumeroTerminalITC = 0;
		private static string stCUITCentroITC = String.Empty;
		private static bool bModoTXITC = false;
		public static bool bManualTimeOutITC = false;
		private static string stPrestacionUrgenciaITC = String.Empty;

		private static string stPathITCUpload = String.Empty;
		private static string stPathITCDownload = String.Empty;
		private static string stPathITCTX = String.Empty;

		// Ficheros Traditum

		private const int HDR = 0;
		private const int DT1 = 1;
		private const int DT2 = 2;
		private const int DT3 = 3;
		private const int DT2a = 4;
		private const int DT2b = 5;

		// Ficheros ITC

		private const int SVL_0 = 0;
		private const int SVL_1 = 1;
		private const int SVL_2 = 2;
		private const int SVL_3 = 3;
		private const int SVL_4 = 4;

		// Estructura de los campos

		private struct Campo
		{
			public string valor; // Texto que se escribir�
			public short Tamano; // Tama�o del campo
			public string Tipo; // C: Caracter, N:Num�rico. En funci�n de esto se rellenar� el campo con espacios o ceros
			public string Direccion; // R: Respuesta, S: Salida, T: Todas
			public static Campo CreateInstance()
			{
					Campo result = new Campo();
					result.valor = String.Empty;
					result.Tipo = String.Empty;
					result.Direccion = String.Empty;
					return result;
			}
		}

		private struct Fichero
		{
			public string Nombre;
			public bAutArgentina.Campo[] Campos;
			public static Fichero CreateInstance()
			{
					Fichero result = new Fichero();
					result.Nombre = String.Empty;
					return result;
			}
		}

		private static bAutArgentina.Fichero[] Ficheros = null;

		//Public Const AR_COL_COPAGO = 17
		//Public Const AR_COL_FIRMA_AUTORIZACION = 18

		internal static int proTerminalITC()
		{

			return iNumeroTerminalITC;

		}

		internal static int proTerminalTraditum()
		{

			return iNumeroTerminalTraditum;

		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: bGestionTraditum                                                               -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n determinar� si Traditum se encuentra activo en el centro. Adem�s,      -
		//-  recuperar� los valores por defecto de ciertos campos como el path, el tiempo de espera  -
		//-  y la ID del sitio emisor.                                                               -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		internal static bool bGestionTraditum(SqlConnection cnConexion)
		{

			bool result = false;
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;

				// Si ya est� activo, no volvemos a pedirlo

				if (stPathTraditum.Trim() != "")
				{
					return true;
				}

				// Comprobamos si est� activo

				stSql = "SELECT isNull(valfanu1, 'N') Activo, isNull(valfanu2, '') + isNull(valfanu3, '') \"PATH\", " + 
				        "isNull(nnumeri1, 10) TiempoEspera, isNull(nnumeri2, 0) NumeroTerminal, isNull(valfanu1i1, '') SitioEmisor, " + 
				        "isNull(valfanu2i1, 'N') Manual, ISNULL(valfanu3i1, ' ') Urgencia " + 
				        "FROM SCONSGLO WHERE gconsglo = 'TRADITUM'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					result = false;
					stPathTraditum = "";
					iTiempoEsperaTraditum = 10;
					iNumeroTerminalTraditum = 0;
					stIdSitioEmisor = "";
					stPrestacionUrgenciaTraditum = "";
					bManualTimeOutTraditum = false;
				}
				else
				{
					result = Convert.ToString(RrDatos.Tables[0].Rows[0]["Activo"]).Trim().ToUpper() == "S";

					if (result)
					{

						stPathTraditum = Convert.ToString(RrDatos.Tables[0].Rows[0]["PATH"]).Trim();
						iTiempoEsperaTraditum = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["TiempoEspera"]);
						iNumeroTerminalTraditum = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["NumeroTerminal"]);
						stIdSitioEmisor = Convert.ToString(RrDatos.Tables[0].Rows[0]["SitioEmisor"]).Trim();
						bManualTimeOutTraditum = Convert.ToString(RrDatos.Tables[0].Rows[0]["Manual"]).Trim() == "S";
						stPrestacionUrgenciaTraditum = Convert.ToString(RrDatos.Tables[0].Rows[0]["Urgencia"]).Trim();

						if (iTiempoEsperaTraditum <= 0)
						{
							iTiempoEsperaTraditum = 10;
						}

						if (iNumeroTerminalTraditum < 0)
						{
							iNumeroTerminalTraditum = 0;
						}

						if (stPathTraditum != "")
						{

							// Si tiene texto y no termina en el caracter "\", se lo a�ado

							if (!stPathTraditum.EndsWith("\\"))
							{
								stPathTraditum = stPathTraditum + "\\";
							}

							// Actualizamos el valor del n�mero de terminal

							stSql = "UPDATE SCONSGLO SET nnumeri2 = " + (((iNumeroTerminalTraditum + 1) % 1000).ToString()) + " WHERE gconsglo = 'TRADITUM'";

							SqlCommand tempCommand = new SqlCommand(stSql, cnConexion);
							tempCommand.ExecuteNonQuery();

						}
						else
						{

							// Como no tenemos path correcto, no activamos Traditum

							MessageBox.Show("Path Traditum no especificado", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
							result = false;

						}

					}

				}

				 
				RrDatos.Close();

				return result;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("bGestionTraditum: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				stPathTraditum = "";
				iTiempoEsperaTraditum = 10;
				iNumeroTerminalTraditum = 0;
				stIdSitioEmisor = "";
				bManualTimeOutTraditum = false;

				return result;
			}
		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: bGestionITC                                                                    -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n determinar� si ITC se encuentra activo en el centro. Adem�s,           -
		//-  recuperar� los valores por defecto de ciertos campos como el path, el tiempo de espera  -
		//-  y el ID del path.                                                                       -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		internal static bool bGestionITC(SqlConnection cnConexion)
		{

			bool result = false;
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;

				// Si ya est� activo, no volvemos a pedirlo

				//    If fValidaITC = True Then
				//        bGestionITC = True
				//        Exit Function
				//    End If

				if (stPathITC.Trim() != "")
				{
					return true;
				}

				// Comprobamos si est� activo

				stSql = "SELECT isNull(valfanu1, 'N') valfanu1, isNull(valfanu2, '') + isNull(valfanu3, '') \"PATH\", " + 
				        "isNull(nnumeri1, 10) nnumeri1, isNull(nnumeri2, 0) nnumeri2, isNull(valfanu1i1, ' ') valfanu1i1, " + 
				        "isNull(valfanu2i1, 'N') valfanu2i1, isNull(valfanu3i1, '') valfanu3i1, isNull(valfanu1i2, 'N') valfanu1i2 " + 
				        "FROM SCONSGLO WHERE gconsglo = 'AUTORITC'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					result = false;
					stPathITC = "";
					iTiempoEsperaITC = 10;
					iNumeroTerminalITC = 0;
					stCUITCentroITC = "";
					stPrestacionUrgenciaITC = "";
					bModoTXITC = false;
					bManualTimeOutITC = false;
				}
				else
				{

					result = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S";

					if (result)
					{

						stPathITC = Convert.ToString(RrDatos.Tables[0].Rows[0]["PATH"]).Trim();
						iTiempoEsperaITC = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["nnumeri1"]);
						iNumeroTerminalITC = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["nnumeri2"]);
						stCUITCentroITC = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1i1"]).Trim();
						bModoTXITC = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2i1"]).Trim().ToUpper() == "S";
						stPrestacionUrgenciaITC = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3i1"]).Trim();

						bManualTimeOutITC = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1i2"]).Trim() == "S";

						if (iTiempoEsperaITC <= 0)
						{
							iTiempoEsperaITC = 10;
						}

						if (iNumeroTerminalITC < 0)
						{
							iNumeroTerminalITC = 0;
						}

						if (stPathITC != "")
						{

							// Si tiene texto y no termina en el caracter "\", se lo a�ado

							if (!stPathITC.EndsWith("\\"))
							{
								stPathITC = stPathITC + "\\";
							}

							// Actualizamos el valor del n�mero de terminal

							stSql = "UPDATE SCONSGLO SET nnumeri2 = " + (((iNumeroTerminalITC + 1) % 1000).ToString()) + " WHERE gconsglo = 'AUTORITC'";

							SqlCommand tempCommand = new SqlCommand(stSql, cnConexion);
							tempCommand.ExecuteNonQuery();

						}
						else
						{

							// Como no tenemos path correcto, no activamos ITC

							MessageBox.Show("Path ITC no especificado", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
							result = false;

						}

					}

				}

				 
				RrDatos.Close();

				return result;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("bGestionITC: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				stPathITC = "";
				stPrestacionUrgenciaITC = "";
				iTiempoEsperaITC = 10;
				iNumeroTerminalITC = 0;
				bModoTXITC = false;
				bManualTimeOutITC = false;

				return result;
			}
		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proDefineEstructurasTraditum                                                   -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n determinar� las estructuras de los distintos ficheros que necesita     -
		//-  la interfaz Traditum.                                                                   -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		private static void proDefineEstructurasTraditum()
		{

			try
			{


				// Establecemos la cantidad de ficheros que vamos a necesitar

				Ficheros = new bAutArgentina.Fichero[6]; //3)

				//----------------------------
				//------- Cabecera HDR -------
				//----------------------------

				Ficheros[HDR].Nombre = "Traditum.hdr";

				Ficheros[HDR].Campos = new bAutArgentina.Campo[26];

				// Tipo de Mensaje � �ZQI^Z01^ZQI_Z01�, �ZQA^Z02^ZQA_Z02�,....

				Ficheros[HDR].Campos[0].Tamano = 15;
				Ficheros[HDR].Campos[0].Direccion = "T";

				// Tipo de Carga (Manual -> "M" o Autom�tica -> "A")

				Ficheros[HDR].Campos[1].Tamano = 1;
				Ficheros[HDR].Campos[1].Direccion = "S";

				// Id de Sitio Emisor � Prestador. (Ej: �TRIA00000012�)

				Ficheros[HDR].Campos[2].Tamano = 20;
				Ficheros[HDR].Campos[2].Direccion = "S";

				// ID de Aplicaci�n Receptora � (Ej: �MEDIFE�)

				Ficheros[HDR].Campos[3].Tamano = 20;
				Ficheros[HDR].Campos[3].Direccion = "S";

				// ID de Sitio Receptor � (Ej: �MEDIFE�)

				Ficheros[HDR].Campos[4].Tamano = 20;
				Ficheros[HDR].Campos[4].Direccion = "S";

				// ID Universal de Sitio Receptor � (Ej: �222222�)

				Ficheros[HDR].Campos[5].Tamano = 10;
				Ficheros[HDR].Campos[5].Direccion = "S";

				// Tipo de ID Universal de Sitio Receptor � (Ej: �IIN�)

				Ficheros[HDR].Campos[6].Tamano = 10;
				Ficheros[HDR].Campos[6].Direccion = "S";

				// ID de Mensaje (Solicitud � identificador �nico del Mensaje)

				Ficheros[HDR].Campos[7].Tamano = 20;
				Ficheros[HDR].Campos[7].Direccion = "S";

				// ID de Mensaje (Recepci�n � identificador �nico del Mensaje)

				Ficheros[HDR].Campos[8].Tamano = 20;
				Ficheros[HDR].Campos[8].Direccion = "R";

				// Id de Procesamiento (Producci�n � �P� o Desarrollo � �D�)

				Ficheros[HDR].Campos[9].Tamano = 1;
				Ficheros[HDR].Campos[9].Direccion = "S";

				// Rol del Prestador (PS -> Solicitante, PE -> Efector, PR -> Prescriptor)

				Ficheros[HDR].Campos[10].Tamano = 2;
				Ficheros[HDR].Campos[10].Direccion = "S";

				// Tipo de Identificaci�n del Prestador (Solicitante) -> "CU" (CUIT)

				Ficheros[HDR].Campos[11].Tamano = 5;
				Ficheros[HDR].Campos[11].Direccion = "S";

				// N�mero de Identificaci�n del Prestador (Solicitante)

				Ficheros[HDR].Campos[12].Tamano = 20;
				Ficheros[HDR].Campos[12].Direccion = "S";

				// C�digo de Especialidad del Prestador (Solicitante)

				Ficheros[HDR].Campos[13].Tamano = 5;
				Ficheros[HDR].Campos[13].Direccion = "S";

				// Apellido del Prestador o Raz�n Social (Solicitante)

				Ficheros[HDR].Campos[14].Tamano = 50;
				Ficheros[HDR].Campos[14].Direccion = "R";

				// Nombre del Prestador (Solicitante)

				Ficheros[HDR].Campos[15].Tamano = 50;
				Ficheros[HDR].Campos[15].Direccion = "R";

				// AcceptAcknowledgementType -> (Valor Fijo "NE")

				Ficheros[HDR].Campos[16].Tamano = 2;
				Ficheros[HDR].Campos[16].Direccion = "S";

				// ApplicationAcknowledgementType -> (Valor Fijo "NE")

				Ficheros[HDR].Campos[17].Tamano = 2;
				Ficheros[HDR].Campos[17].Direccion = "S";

				// C�digo de Aceptaci�n o Error de Aplicaci�n o Transmisi�n

				Ficheros[HDR].Campos[18].Tamano = 10;
				Ficheros[HDR].Campos[18].Direccion = "R";

				// Descripci�n de Aceptaci�n o Error de Aplicaci�n o Transmisi�n

				Ficheros[HDR].Campos[19].Tamano = 150;
				Ficheros[HDR].Campos[19].Direccion = "R";

				// C�digo de Transacci�n o Ticket

				Ficheros[HDR].Campos[20].Tamano = 10;
				Ficheros[HDR].Campos[20].Direccion = "R";

				// C�digo de Aceptaci�n o Rechazo del Mensaje

				Ficheros[HDR].Campos[21].Tamano = 4;
				Ficheros[HDR].Campos[21].Direccion = "R";

				// Descripci�n de Aceptaci�n o Rechazo del Mensaje

				Ficheros[HDR].Campos[22].Tamano = 150;
				Ficheros[HDR].Campos[22].Direccion = "R";

				// Comentario

				Ficheros[HDR].Campos[23].Tamano = 350;
				Ficheros[HDR].Campos[23].Direccion = "R";

				// C�digo de Pa�s (ARG --> Argentina)

				Ficheros[HDR].Campos[24].Tamano = 3;
				Ficheros[HDR].Campos[24].Direccion = "S";

				// FIN de archivo (CRLF --> CHR(13) + CHR(10) --> 0D + 0A)

				Ficheros[HDR].Campos[25].Tamano = 2;
				Ficheros[HDR].Campos[25].Direccion = "T";

				for (int i = 0; i <= Ficheros[HDR].Campos.GetUpperBound(0); i++)
				{
					Ficheros[HDR].Campos[i].Tipo = "C";
				}

				//----------------------------
				//----------- DT1 ------------
				//----------------------------

				Ficheros[DT1].Nombre = "Traditum.dt1";

				Ficheros[DT1].Campos = new bAutArgentina.Campo[13];

				// Tipo de Identificaci�n del Beneficiario � �HC� (Credencial)

				Ficheros[DT1].Campos[0].Tamano = 5;
				Ficheros[DT1].Campos[0].Direccion = "S";

				// N�mero de Identificaci�n del Beneficiario

				Ficheros[DT1].Campos[1].Tamano = 25;
				Ficheros[DT1].Campos[1].Direccion = "S";

				// Apellido del Beneficiario

				Ficheros[DT1].Campos[2].Tamano = 50;
				Ficheros[DT1].Campos[2].Direccion = "R";

				// Nombre del Beneficiario

				Ficheros[DT1].Campos[3].Tamano = 50;
				Ficheros[DT1].Campos[3].Direccion = "R";

				// C�digo del Plan

				Ficheros[DT1].Campos[4].Tamano = 10;
				Ficheros[DT1].Campos[4].Direccion = "R";

				// Descripci�n del Plan

				Ficheros[DT1].Campos[5].Tamano = 30;
				Ficheros[DT1].Campos[5].Direccion = "R";

				// C�digo de Categor�a (Condici�n Frente al IVA)

				Ficheros[DT1].Campos[6].Tamano = 10;
				Ficheros[DT1].Campos[6].Direccion = "R";

				// Descripci�n de Categor�a (Condici�n Frente al IVA)

				Ficheros[DT1].Campos[7].Tamano = 30;
				Ficheros[DT1].Campos[7].Direccion = "R";

				// Comentario

				Ficheros[DT1].Campos[8].Tamano = 347;
				Ficheros[DT1].Campos[8].Direccion = "R";

				// Clase de Paciente (E = Internaci�n, O = Ambulatorio, D = Odontolog�a)

				Ficheros[DT1].Campos[9].Tamano = 1;
				Ficheros[DT1].Campos[9].Direccion = "S";

				// Indicador de Visita (Valor Fijo = �V�)

				Ficheros[DT1].Campos[10].Tamano = 1;
				Ficheros[DT1].Campos[10].Direccion = "S";

				// Tipo Admisi�n (Tipo Admisi�n � G = Guardia, U = Urgencia, P = Programada)

				Ficheros[DT1].Campos[11].Tamano = 1;
				Ficheros[DT1].Campos[11].Direccion = "S";

				// FIN de archivo (CRLF --> CHR(13) + CHR(10) --> 0D + 0A)

				Ficheros[DT1].Campos[12].Tamano = 2;
				Ficheros[DT1].Campos[12].Direccion = "T";

				for (int i = 0; i <= Ficheros[DT1].Campos.GetUpperBound(0); i++)
				{
					Ficheros[DT1].Campos[i].Tipo = "C";
				}

				//----------------------------
				//----------- DT2 ------------
				//----------------------------

				Ficheros[DT2].Nombre = "Traditum.dt2";

				Ficheros[DT2].Campos = new bAutArgentina.Campo[30];

				// Rol del Prestador (EF -> Efector) Si no se carga Efector dejar este campo en blanco

				Ficheros[DT2].Campos[0].Tamano = 2;
				Ficheros[DT2].Campos[0].Direccion = "S";

				// Tipo de Identificaci�n del Prestador (Efector) -> "CU" (CUIT) / MN (Mat. Nac) / MP (Mat. Prov.)

				Ficheros[DT2].Campos[1].Tamano = 5;
				Ficheros[DT2].Campos[1].Direccion = "S";

				// N�mero de Identificaci�n del Prestador (Efector)

				Ficheros[DT2].Campos[2].Tamano = 15;
				Ficheros[DT2].Campos[2].Direccion = "S";

				// C�digo de Especialidad (Efector)

				Ficheros[DT2].Campos[3].Tamano = 10;
				Ficheros[DT2].Campos[3].Direccion = "S";

				// C�digo de Provincia (Efector)

				Ficheros[DT2].Campos[4].Tamano = 4;
				Ficheros[DT2].Campos[4].Direccion = "S";

				// Descripci�n de Especialidad (Efector)

				Ficheros[DT2].Campos[5].Tamano = 25;
				Ficheros[DT2].Campos[5].Direccion = "R";

				// Apellido del Prestador o Raz�n Social (Efector)

				Ficheros[DT2].Campos[6].Tamano = 50;
				Ficheros[DT2].Campos[6].Direccion = "R";

				// Nombre del Prestador (Efector)

				Ficheros[DT2].Campos[7].Tamano = 50;
				Ficheros[DT2].Campos[7].Direccion = "R";

				// Rol del Prestador (PR -> Prescriptor) Si no se carga Prescriptor dejar este campo en blanco

				Ficheros[DT2].Campos[8].Tamano = 2;
				Ficheros[DT2].Campos[8].Direccion = "S";

				// Tipo de Identificaci�n del Prestador (Prescriptor) -> "CU" (CUIT) / MN (Mat. Nac) / MP (Mat. Prov.)

				Ficheros[DT2].Campos[9].Tamano = 5;
				Ficheros[DT2].Campos[9].Direccion = "S";

				// N�mero de Identificaci�n del Prestador (Prescriptor)

				Ficheros[DT2].Campos[10].Tamano = 15;
				Ficheros[DT2].Campos[10].Direccion = "S";

				// C�digo de Especialidad (Prescriptor)

				Ficheros[DT2].Campos[11].Tamano = 10;
				Ficheros[DT2].Campos[11].Direccion = "S";

				// C�digo de Provincia (Prescriptor)

				Ficheros[DT2].Campos[12].Tamano = 4;
				Ficheros[DT2].Campos[12].Direccion = "S";

				// Descripci�n de Especialidad (Prescriptor)

				Ficheros[DT2].Campos[13].Tamano = 25;
				Ficheros[DT2].Campos[13].Direccion = "R";

				// Apellido del Prestador o Raz�n Social (Prescriptor)

				Ficheros[DT2].Campos[14].Tamano = 50;
				Ficheros[DT2].Campos[14].Direccion = "R";

				// Nombre del Prestador (Prescriptor)

				Ficheros[DT2].Campos[15].Tamano = 50;
				Ficheros[DT2].Campos[15].Direccion = "R";

				// Moneda Copago (Cabecera - Total)

				Ficheros[DT2].Campos[16].Tamano = 4;
				Ficheros[DT2].Campos[16].Direccion = "R";

				// Valor Copago (Cabecera)

				Ficheros[DT2].Campos[17].Tamano = 8;
				Ficheros[DT2].Campos[17].Direccion = "R";

				// N�mero de PreAutorizaci�n Global

				Ficheros[DT2].Campos[18].Tamano = 10;
				Ficheros[DT2].Campos[18].Direccion = "S";

				// C�digo de Nomenclador

				Ficheros[DT2].Campos[19].Tamano = 5;
				Ficheros[DT2].Campos[19].Direccion = "S";

				// C�digo de Pr�ctica

				Ficheros[DT2].Campos[20].Tamano = 10;
				Ficheros[DT2].Campos[20].Direccion = "S";

				// C�digo de Aceptaci�n o Rechazo

				Ficheros[DT2].Campos[21].Tamano = 4;
				Ficheros[DT2].Campos[21].Direccion = "R";

				// Descripci�n de Aceptaci�n o Rechazo

				Ficheros[DT2].Campos[22].Tamano = 25;
				Ficheros[DT2].Campos[22].Direccion = "R";

				// N�mero de PreAutorizaci�n

				Ficheros[DT2].Campos[23].Tamano = 10;
				Ficheros[DT2].Campos[23].Direccion = "S";

				// Fecha de PreAutorizaci�n (AAAAMMDD)

				Ficheros[DT2].Campos[24].Tamano = 8;
				Ficheros[DT2].Campos[24].Direccion = "S";

				// Nombre de Pr�ctica

				Ficheros[DT2].Campos[25].Tamano = 30;
				Ficheros[DT2].Campos[25].Direccion = "R";

				// Cantidad Solicitada

				Ficheros[DT2].Campos[26].Tamano = 2;
				Ficheros[DT2].Campos[26].Direccion = "S";

				// Cantidad Aprobada

				Ficheros[DT2].Campos[27].Tamano = 2;
				Ficheros[DT2].Campos[27].Direccion = "R";

				// Moneda Copago

				Ficheros[DT2].Campos[28].Tamano = 3;
				Ficheros[DT2].Campos[28].Direccion = "R";

				// Valor Copago

				Ficheros[DT2].Campos[29].Tamano = 8;
				Ficheros[DT2].Campos[29].Direccion = "R";

				for (int i = 0; i <= Ficheros[DT2].Campos.GetUpperBound(0); i++)
				{
					Ficheros[DT2].Campos[i].Tipo = "C";
				}

				//----------------------------
				//----------- DT3 ------------
				//----------------------------

				Ficheros[DT3].Nombre = "Traditum.dt3";

				Ficheros[DT3].Campos = new bAutArgentina.Campo[3];

				// N�mero de Transacci�n a Anular

				Ficheros[DT3].Campos[0].Tamano = 10;
				Ficheros[DT3].Campos[0].Direccion = "S";

				// Estado de Anulaci�n

				Ficheros[DT3].Campos[1].Tamano = 40;
				Ficheros[DT3].Campos[1].Direccion = "R";

				// FIN de archivo (CRLF --> CHR(13) + CHR(10) --> 0D + 0A)

				Ficheros[DT3].Campos[2].Tamano = 2;
				Ficheros[DT3].Campos[2].Direccion = "T";

				for (int i = 0; i <= Ficheros[DT3].Campos.GetUpperBound(0); i++)
				{
					Ficheros[DT3].Campos[i].Tipo = "C";
				}

				//----------------------------
				//----------- DT2a -----------
				//----------------------------

				Ficheros[DT2a].Nombre = "Traditum.dt2a";

				Ficheros[DT2a].Campos = new bAutArgentina.Campo[1];

				//----------------------------
				//----------- DT2b -----------
				//----------------------------

				Ficheros[DT2b].Nombre = "Traditum.dt2b";

				Ficheros[DT2b].Campos = new bAutArgentina.Campo[1];
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proDefineEstructurasTraditum: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proDefineEstructurasITC                                                        -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n determinar� las estructuras de los distintos ficheros que necesita     -
		//-  la interfaz ITC.                                                                        -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		private static void proDefineEstructurasITC()
		{

			try
			{

				int i = 0;

				// Establecemos la cantidad de ficheros que vamos a necesitar

				Ficheros = new bAutArgentina.Fichero[5];

				//----------------------------
				//---------- SVL.0 -----------
				//----------------------------

				Ficheros[SVL_0].Nombre = "_svl.0";

				Ficheros[SVL_0].Campos = new bAutArgentina.Campo[3];

				// Versi�n del formato�de transacciones valor fijo:�V107

				Ficheros[SVL_0].Campos[0].Tamano = 4;
				Ficheros[SVL_0].Campos[0].Direccion = "T";
				Ficheros[SVL_0].Campos[0].Tipo = "C";

				// C�digo de la Empresa. Permite identificar el financiador. Ej.�11�para OSDE METRO.�Ver c�digos

				Ficheros[SVL_0].Campos[1].Tamano = 2;
				Ficheros[SVL_0].Campos[1].Direccion = "T";
				Ficheros[SVL_0].Campos[1].Tipo = "N";

				// C�digo de Actividad:�01�para Polivalente

				Ficheros[SVL_0].Campos[2].Tamano = 2;
				Ficheros[SVL_0].Campos[2].Direccion = "T";
				Ficheros[SVL_0].Campos[2].Tipo = "N";

				//----------------------------
				//---------- SVL.1 -----------
				//----------------------------

				Ficheros[SVL_1].Nombre = "_svl.1";

				Ficheros[SVL_1].Campos = new bAutArgentina.Campo[29];


				// CUIT del Prestador (quien va a facturar la/s prestaciones)

				Ficheros[SVL_1].Campos[0].Tamano = 11;
				Ficheros[SVL_1].Campos[0].Direccion = "T";
				Ficheros[SVL_1].Campos[0].Tipo = "N";

				// Nro. ISO del Emisor de Credencial. Se recomienda utilizar el campo�BANDA�en su lugar. (Ver datos de la credencial de cada financiador)

				Ficheros[SVL_1].Campos[1].Tamano = 6;
				Ficheros[SVL_1].Campos[1].Direccion = "T";
				Ficheros[SVL_1].Campos[1].Tipo = "N";

				// Tarjeta del afiliado. Se recomienda utilizar el campo�BANDA�en su lugar. (Ver datos de la credencial de cada financiador)

				Ficheros[SVL_1].Campos[2].Tamano = 11;
				Ficheros[SVL_1].Campos[2].Direccion = "T";
				Ficheros[SVL_1].Campos[2].Tipo = "N";

				// D�gito Verificador. Se recomienda utilizar el campo�BANDA�en su lugar. (Ver datos de la credencial de cada financiador)

				Ficheros[SVL_1].Campos[3].Tamano = 1;
				Ficheros[SVL_1].Campos[3].Direccion = "T";
				Ficheros[SVL_1].Campos[3].Tipo = "N";

				// Versi�n de la credencial. Se recomienda utilizar el campo�BANDA�en su lugar. (Ver datos de la credencial de cada financiador)

				Ficheros[SVL_1].Campos[4].Tamano = 2;
				Ficheros[SVL_1].Campos[4].Direccion = "T";
				Ficheros[SVL_1].Campos[4].Tipo = "N";

				// C�digo de Transacci�n:�01�(Consultas),�02�(Registraci�n),�03�(Rescates),�04�(Anulaci�n),�05(Reportes/Consultas)

				Ficheros[SVL_1].Campos[5].Tamano = 2;
				Ficheros[SVL_1].Campos[5].Direccion = "T";
				Ficheros[SVL_1].Campos[5].Tipo = "N";

				// Atributo o tipo del C�digo de Transacci�n: (A/B/C/D/E/I/L/Q/P/S/X)

				Ficheros[SVL_1].Campos[6].Tamano = 1;
				Ficheros[SVL_1].Campos[6].Direccion = "T";
				Ficheros[SVL_1].Campos[6].Tipo = "C";

				// Forma de Ingreso de la credencial:�M�(manual),�A�(autom�tico, lectura de banda magn�tica de la credencial)

				Ficheros[SVL_1].Campos[7].Tamano = 1;
				Ficheros[SVL_1].Campos[7].Direccion = "T";
				Ficheros[SVL_1].Campos[7].Tipo = "C";

				// Firma del Generador Nro. de Referencia. Corresponde al�campo AUWHOO de la respuesta de una transacci�n anterior, o un n�mero de referencia que traer� el paciente en una autorizaci�n escrita. Este campo indica qui�n o que sistema gener� una autorizaci�n (AUNREF) espec�fica.

				Ficheros[SVL_1].Campos[8].Tamano = 2;
				Ficheros[SVL_1].Campos[8].Direccion = "T";
				Ficheros[SVL_1].Campos[8].Tipo = "C";

				// Nro. de Autorizaci�n de Referencia

				Ficheros[SVL_1].Campos[9].Tamano = 6;
				Ficheros[SVL_1].Campos[9].Direccion = "T";
				Ficheros[SVL_1].Campos[9].Tipo = "N";

				// Tipo de Matr�cula del Prestador Prescriptor. La primer letra identifica el tipo de m�dico, correspondiendo�O�para Odont�logos y�M�para m�dicos, y la segunda corresponde al origen del matr�cula, utilizando�la nomenclatura de las patentes antiguas. Siendo por ejemplo�C�para matr�culas nacionales, y�B�provincia de Buenos Aires. (Ej.�MC: m�dico con matr�cula nacional,�MX: m�dico con matr�cula provincia de C�rdoba)

				Ficheros[SVL_1].Campos[10].Tamano = 2;
				Ficheros[SVL_1].Campos[10].Direccion = "T";
				Ficheros[SVL_1].Campos[10].Tipo = "C";

				// Nro. de Matr�cula del Prestador Prescriptor o del anestesista en caso de que la transacci�n sea para registro de una cirug�a.

				Ficheros[SVL_1].Campos[11].Tamano = 6;
				Ficheros[SVL_1].Campos[11].Direccion = "T";
				Ficheros[SVL_1].Campos[11].Tipo = "N";

				// Fecha de Pr�ctica (AAAAMMDD)

				Ficheros[SVL_1].Campos[12].Tamano = 8;
				Ficheros[SVL_1].Campos[12].Direccion = "T";
				Ficheros[SVL_1].Campos[12].Tipo = "N";

				// Corresponden a diagn�sticos�ICD-9/CIE-9,�CIE-10�o�DSM-IV�para psicopatolog�a.�Si utiliza una norma diferente los c�digos ser�n convertidos para ello debe proveer una tabla con los c�digos y descripci�n.

				Ficheros[SVL_1].Campos[13].Tamano = 6;
				Ficheros[SVL_1].Campos[13].Direccion = "T";
				Ficheros[SVL_1].Campos[13].Tipo = "C";

				// Idem anterior.

				Ficheros[SVL_1].Campos[14].Tamano = 6;
				Ficheros[SVL_1].Campos[14].Direccion = "T";
				Ficheros[SVL_1].Campos[14].Tipo = "C";

				// C�digo finalizaci�n de Terapia (solo se utiliza cuando se est� realizando un tratamiento por sesiones, enviar�0�en cualquier otro caso):

				Ficheros[SVL_1].Campos[15].Tamano = 1;
				Ficheros[SVL_1].Campos[15].Direccion = "T";
				Ficheros[SVL_1].Campos[15].Tipo = "N";

				// CUIT del Efector de la prestaci�n

				Ficheros[SVL_1].Campos[16].Tamano = 11;
				Ficheros[SVL_1].Campos[16].Direccion = "T";
				Ficheros[SVL_1].Campos[16].Tipo = "N";

				// Nro. de transacci�n interno con que el prestador registra la transacci�n si este fuese diferente al otorgado (AUSYST).

				Ficheros[SVL_1].Campos[17].Tamano = 12;
				Ficheros[SVL_1].Campos[17].Direccion = "T";
				Ficheros[SVL_1].Campos[17].Tipo = "N";

				// CUIT Prescriptor - derivador / receptor de derivaci�n

				Ficheros[SVL_1].Campos[18].Tamano = 11;
				Ficheros[SVL_1].Campos[18].Direccion = "T";
				Ficheros[SVL_1].Campos[18].Tipo = "N";

				// Hora de pr�ctica (hhmmss)

				Ficheros[SVL_1].Campos[19].Tamano = 6;
				Ficheros[SVL_1].Campos[19].Direccion = "T";
				Ficheros[SVL_1].Campos[19].Tipo = "N";

				// Motivo de alta de Internaci�n:

				Ficheros[SVL_1].Campos[20].Tamano = 2;
				Ficheros[SVL_1].Campos[20].Direccion = "T";
				Ficheros[SVL_1].Campos[20].Tipo = "C";

				// Contenido de los tracks de la banda magn�tica (track 1 & 2) tal cual son le�dos. O el ingreso manual de la Tarjeta de Afiliado. A diferencia de los campos anteriores TARJETA, AUDIGA y AUVERA donde debe la aplicaci�n buscar datos espec�ficos, el campo BANDA se encarga de parsear los tracks o el ingreso manual y obtener todos los datos requeridos, en la respuesta obtendr� todos estos datos sin necesidad de saber como manipular la credencial. (Ver cada financiador).

				Ficheros[SVL_1].Campos[21].Tamano = 100;
				Ficheros[SVL_1].Campos[21].Direccion = "T";
				Ficheros[SVL_1].Campos[21].Tipo = "C";

				// Tipo de archivo enviado: C=Protocolo Quir�rgico, A=Protocolo Anestesia, I=Informe M�dico, L=Resultado de Laboratorio, F=Pre-factura

				Ficheros[SVL_1].Campos[22].Tamano = 1;
				Ficheros[SVL_1].Campos[22].Direccion = "T";
				Ficheros[SVL_1].Campos[22].Tipo = "C";

				// Nombre del archivo enviado con la extensi�n (solo se permite el env�o de formatos determinados) (Utilizar el mismo case en este campo y en el archivo enviado.

				Ficheros[SVL_1].Campos[23].Tamano = 30;
				Ficheros[SVL_1].Campos[23].Direccion = "T";
				Ficheros[SVL_1].Campos[23].Tipo = "C";

				// (completar con espacios en blanco)

				Ficheros[SVL_1].Campos[24].Tamano = 26;
				Ficheros[SVL_1].Campos[24].Direccion = "T";
				Ficheros[SVL_1].Campos[24].Tipo = "C";

				// Consulta: Fecha de inicio de la consulta (AAAAMMDD)

				Ficheros[SVL_1].Campos[25].Tamano = 8;
				Ficheros[SVL_1].Campos[25].Direccion = "T";
				Ficheros[SVL_1].Campos[25].Tipo = "C";

				// Consulta: Fecha de finalizaci�n de la consulta (AAAAMMDD)

				Ficheros[SVL_1].Campos[26].Tamano = 8;
				Ficheros[SVL_1].Campos[26].Direccion = "T";
				Ficheros[SVL_1].Campos[26].Tipo = "C";

				// Consulta: Tipo de consulta solicita: R=Reimpresi�n de ticket, C=Consulta de Lote, X=Cierre de Lote

				Ficheros[SVL_1].Campos[27].Tamano = 1;
				Ficheros[SVL_1].Campos[27].Direccion = "T";
				Ficheros[SVL_1].Campos[27].Tipo = "C";

				// ASCII(13), ASCII(10)

				Ficheros[SVL_1].Campos[28].Tamano = 2;
				Ficheros[SVL_1].Campos[28].Direccion = "T";
				Ficheros[SVL_1].Campos[28].Tipo = "C";

				//----------------------------
				//---------- SVL.2 -----------
				//----------------------------

				Ficheros[SVL_2].Nombre = "_svl.2";

				Ficheros[SVL_2].Campos = new bAutArgentina.Campo[11];

				// C�digo de Prestaci�n. Pueden enviarse c�digos no nomenclados propios de hasta 10 caracteres de longitud (requiere que el�prestador env�e a ITC la tabla de equivalencias entre sus c�digos y los del financiador para realizar la transcodificaci�n)

				Ficheros[SVL_2].Campos[0].Tamano = 10;
				Ficheros[SVL_2].Campos[0].Direccion = "T";
				Ficheros[SVL_2].Campos[0].Tipo = "C";

				// Tipo de Prestaci�n:�1�(Ambulatoria),�2�(Cl�nica),�3�(Quir�rgica),�4�(Domiciliaria)

				Ficheros[SVL_2].Campos[1].Tamano = 1;
				Ficheros[SVL_2].Campos[1].Direccion = "T";
				Ficheros[SVL_2].Campos[1].Tipo = "N";

				// Arancel Prestaci�n:�1�(Honorarios Especialista),�2�(Honorarios Ayudante),�3�(Honorarios Anestesista),4�(Gastos),�0�(Todos los honorarios que correspondan a la prestaci�n)

				Ficheros[SVL_2].Campos[2].Tamano = 1;
				Ficheros[SVL_2].Campos[2].Direccion = "T";
				Ficheros[SVL_2].Campos[2].Tipo = "N";

				// Frecuencia o Cantidad Prestaci�n

				Ficheros[SVL_2].Campos[3].Tamano = 2;
				Ficheros[SVL_2].Campos[3].Direccion = "T";
				Ficheros[SVL_2].Campos[3].Tipo = "N";

				// Pieza o Sector Dental. Pieza es un valor entre�11�y�85. Sector puede ser:�IN�(inferior),�II�(inferior izquierdo),�IA�(inferior anterior),�ID�(inferior derecho),�SU�(superior),�SI�(superior izquierdo),�SA(superior anterior),�SD�(superior derecho)

				Ficheros[SVL_2].Campos[4].Tamano = 2;
				Ficheros[SVL_2].Campos[4].Direccion = "T";
				Ficheros[SVL_2].Campos[4].Tipo = "C";

				// Cara Dental. Los valores que podr�a tener son:�M�(mesial),�O�(oclusal),�D�(distal),�V�(vestibular) y�L(lingual)

				Ficheros[SVL_2].Campos[5].Tamano = 5;
				Ficheros[SVL_2].Campos[5].Direccion = "T";
				Ficheros[SVL_2].Campos[5].Tipo = "C";

				// C�digo de Respuesta:

				Ficheros[SVL_2].Campos[6].Tamano = 2;
				Ficheros[SVL_2].Campos[6].Direccion = "T";
				Ficheros[SVL_2].Campos[6].Tipo = "C";

				// Nombre de la prestaci�n

				Ficheros[SVL_2].Campos[7].Tamano = 30;
				Ficheros[SVL_2].Campos[7].Direccion = "T";
				Ficheros[SVL_2].Campos[7].Tipo = "C";

				// Monto unitario que debe abonar el afiliado en concepto de coseguro o copago por esta prestaci�n

				Ficheros[SVL_2].Campos[8].Tamano = 9;
				Ficheros[SVL_2].Campos[8].Direccion = "T";
				Ficheros[SVL_2].Campos[8].Tipo = "N";

				// Monto unitario que abonar� el financiador por esta prestaci�n

				Ficheros[SVL_2].Campos[9].Tamano = 9;
				Ficheros[SVL_2].Campos[9].Direccion = "T";
				Ficheros[SVL_2].Campos[9].Tipo = "N";

				// (completar con espacios en blanco)

				Ficheros[SVL_2].Campos[10].Tamano = 30;
				Ficheros[SVL_2].Campos[10].Direccion = "T";
				Ficheros[SVL_2].Campos[10].Tipo = "C";

				// ASCII(13), ASCII(10)

				//    Ficheros(SVL_2).Campos(11).Tamano = 2
				//    Ficheros(SVL_2).Campos(11).Direccion = "T"
				//    Ficheros(SVL_2).Campos(11).Tipo = "C"

				//----------------------------
				//---------- SVL.3 -----------
				//----------------------------

				Ficheros[SVL_3].Nombre = "_svl.3";

				Ficheros[SVL_3].Campos = new bAutArgentina.Campo[16];

				// Nro. de Autorizaci�n Complementario o Solicitud del Financiador

				Ficheros[SVL_3].Campos[0].Tamano = 6;
				Ficheros[SVL_3].Campos[0].Direccion = "T";
				Ficheros[SVL_3].Campos[0].Tipo = "N";

				// Firma del Generador del Nro. de Autorizaci�n:�98�el Financiador o�92�ITC (en este caso la respuesta es por time-out del Financiador). Identifica quien emite la autorizaci�n.

				Ficheros[SVL_3].Campos[1].Tamano = 2;
				Ficheros[SVL_3].Campos[1].Direccion = "T";
				Ficheros[SVL_3].Campos[1].Tipo = "C";

				// C�digo de Respuesta Transaccional:�00�Transacci�n ha sido aprobada o�01�Transacci�n ha sido rechazada

				Ficheros[SVL_3].Campos[2].Tamano = 2;
				Ficheros[SVL_3].Campos[2].Direccion = "T";
				Ficheros[SVL_3].Campos[2].Tipo = "C";

				// Leyenda a Desplegar (se recomienda mostrar en renglones de 20 caracteres)

				Ficheros[SVL_3].Campos[3].Tamano = 120;
				Ficheros[SVL_3].Campos[3].Direccion = "T";
				Ficheros[SVL_3].Campos[3].Tipo = "C";

				// Monto total que abonar� el financiador

				Ficheros[SVL_3].Campos[4].Tamano = 11;
				Ficheros[SVL_3].Campos[4].Direccion = "T";
				Ficheros[SVL_3].Campos[4].Tipo = "N";

				// Nro. Transacci�n

				Ficheros[SVL_3].Campos[5].Tamano = 9;
				Ficheros[SVL_3].Campos[5].Direccion = "T";
				Ficheros[SVL_3].Campos[5].Tipo = "N";

				// Plan Asociado (si la respuesta es por time-out puede no devolver el plan)[3]

				Ficheros[SVL_3].Campos[6].Tamano = 5;
				Ficheros[SVL_3].Campos[6].Direccion = "T";
				Ficheros[SVL_3].Campos[6].Tipo = "C";

				// Condici�n de IVA del Socio:�D�IVA gravado,�O�IVA NO Gravado (si la respuesta es por time-out puede no devolver la condici�n frente al IVA)[3]

				Ficheros[SVL_3].Campos[7].Tamano = 1;
				Ficheros[SVL_3].Campos[7].Direccion = "T";
				Ficheros[SVL_3].Campos[7].Tipo = "C";

				// N�mero de Credencial del Afiliado

				Ficheros[SVL_3].Campos[8].Tamano = 20;
				Ficheros[SVL_3].Campos[8].Direccion = "T";
				Ficheros[SVL_3].Campos[8].Tipo = "C";

				// Apellido y Nombre del beneficiario

				Ficheros[SVL_3].Campos[9].Tamano = 30;
				Ficheros[SVL_3].Campos[9].Direccion = "T";
				Ficheros[SVL_3].Campos[9].Tipo = "C";

				// Sexo del beneficiario (M/F)

				Ficheros[SVL_3].Campos[10].Tamano = 1;
				Ficheros[SVL_3].Campos[10].Direccion = "T";
				Ficheros[SVL_3].Campos[10].Tipo = "C";

				// Fecha de Nacimiento del Afiliado (AAAAMMDD)

				Ficheros[SVL_3].Campos[11].Tamano = 8;
				Ficheros[SVL_3].Campos[11].Direccion = "T";
				Ficheros[SVL_3].Campos[11].Tipo = "C";

				// Indica si la transacci�n requiere el env�o posterior de documentaci�n. Los valores son los mismos al campo TIPODOCUMENTO.

				Ficheros[SVL_3].Campos[12].Tamano = 1;
				Ficheros[SVL_3].Campos[12].Direccion = "T";
				Ficheros[SVL_3].Campos[12].Tipo = "C";

				// Monto total a abonar por el afiliado en concepto de coseguro o copago.

				Ficheros[SVL_3].Campos[13].Tamano = 9;
				Ficheros[SVL_3].Campos[13].Direccion = "T";
				Ficheros[SVL_3].Campos[13].Tipo = "N";

				// (espacios en blanco)

				Ficheros[SVL_3].Campos[14].Tamano = 73;
				Ficheros[SVL_3].Campos[14].Direccion = "T";
				Ficheros[SVL_3].Campos[14].Tipo = "C";

				// ASCII(13), ASCII(10)

				Ficheros[SVL_3].Campos[15].Tamano = 2;
				Ficheros[SVL_3].Campos[15].Direccion = "T";
				Ficheros[SVL_3].Campos[15].Tipo = "C";

				//----------------------------
				//---------- SVL.4 -----------
				//----------------------------

				Ficheros[SVL_4].Nombre = "_svl.4";

				Ficheros[SVL_4].Campos = new bAutArgentina.Campo[2];

				// C�digo Mensaje de Respuesta de Rechazo (ver listado)

				Ficheros[SVL_4].Campos[0].Tamano = 3;
				Ficheros[SVL_4].Campos[0].Direccion = "T";
				Ficheros[SVL_4].Campos[0].Tipo = "C";

				// ASCII(13) + ASCII(10)

				Ficheros[SVL_4].Campos[1].Tamano = 2;
				Ficheros[SVL_4].Campos[1].Direccion = "T";
				Ficheros[SVL_4].Campos[1].Tipo = "C";
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proDefineEstructurasITC: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proGeneraEstructura                                                            -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n construir� el texto del fichero a generar, teniendo en cuenta si es    -
		//-  necesario rellenar con espacios o ceros a la izquierda para completar el tama�o total   -
		//-  del campo.                                                                              -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		private static string proGeneraEstructura(int iTipo, string[] stCadena, int iAPartirDe = 0)
		{

			try
			{

				StringBuilder stSalida = new StringBuilder();
				StringBuilder stTemp = new StringBuilder();

				for (int i = iAPartirDe; i <= Ficheros[iTipo].Campos.GetUpperBound(0); i++)
				{

					if (Ficheros[iTipo].Campos[i].Tipo != "R")
					{

						// Si el campo no es de respuesta, lo rellenamos con el valor que nos llegue

						stTemp = new StringBuilder(stCadena[i].Substring(0, Math.Min(Ficheros[iTipo].Campos[i].Tamano, stCadena[i].Length)));

						if (stTemp.ToString().Length < Ficheros[iTipo].Campos[i].Tamano)
						{
							if (Ficheros[iTipo].Campos[i].Tipo == "C")
							{
								stTemp.Append(new String(' ', Ficheros[iTipo].Campos[i].Tamano - stTemp.ToString().Length));
							}
							else
							{
								stTemp = new StringBuilder(new string('0', Ficheros[iTipo].Campos[i].Tamano - stTemp.ToString().Length) + stTemp.ToString());
							}
						}

					}
					else
					{

						// Si el campo es de respuesta, lo rellenamos con ceros o espacios seg�n el campo sea de un tipo u otro

						if (Ficheros[iTipo].Campos[i].Tipo == "C")
						{
							stTemp = new StringBuilder(new String(' ', Ficheros[iTipo].Campos[i].Tamano));
						}
						else
						{
							stTemp = new StringBuilder(new string('0', Ficheros[iTipo].Campos[i].Tamano));
						}

					}

					stSalida.Append(stTemp.ToString());

				}


				return stSalida.ToString();
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proGeneraEstructura: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

				return "";
			}
		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proObtenerRespuesta                                                            -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n recuperar� los datos del fichero de respuesta especificado, cargando   -
		//-  estos datos sobre el array del fichero.                                                 -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		//UPGRADE_NOTE: (7001) The following declaration (proObtenerRespuesta) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void proObtenerRespuesta(int iTipo, ref string stPath)
		//{
				//
				//try
				//{
					//
					//int fArchivo = 0;
					//string stContenido = String.Empty;
					//
					// Recuperamos el nombre del fichero
					//
					//stPath = stPath + "\\" + Ficheros[iTipo].Nombre.Trim();
					//
					// Leemos los datos del fichero
					//
					//fArchivo = FileSystem.FreeFile();
					//
					//FileSystem.FileOpen(fArchivo, stPath, OpenMode.Input, OpenAccess.Default, OpenShare.Default, -1);
					//
					//stContenido = FileSystem.InputString(fArchivo, (int) FileSystem.LOF(fArchivo));
					//
					//FileSystem.FileClose(fArchivo);
					//
					// Guardamos los datos del fichero en la estructura
					//
					//for (int i = 0; i <= Ficheros[iTipo].Campos.GetUpperBound(0); i++)
					//{
						//
						//if (Ficheros[iTipo].Campos[i].Direccion == "R")
						//{
							//Ficheros[iTipo].Campos[i].valor = stContenido.Substring(0, Math.Min(Ficheros[iTipo].Campos[i].Tamano, stContenido.Length));
						//}
						//
						//stContenido = stContenido.Substring(Ficheros[iTipo].Campos[i].Tamano);
						//
					//}
				//}
				//catch (System.Exception excep)
				//{
					//
					//MessageBox.Show("proObtenerRespuesta: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				//}
				//
		//}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proGrabaFichero                                                                -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n grabar� un fichero de texto con el contenido y en el path indicados    -
		//-  como argumentos de la funci�n.                                                          -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		private static bool proGrabaFichero(int iTipo, string stPath, string stContenido, bool bA�adir = false)
		{

			try
			{

				int fArchivo = 0;

				fArchivo = FileSystem.FreeFile();

				if (bA�adir)
				{
					FileSystem.FileOpen(fArchivo, stPath + Ficheros[iTipo].Nombre.Trim(), OpenMode.Append, OpenAccess.Default, OpenShare.Default, -1);
				}
				else
				{
					FileSystem.FileOpen(fArchivo, stPath + Ficheros[iTipo].Nombre.Trim(), OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);
				}

				FileSystem.PrintLine(fArchivo, stContenido);

				FileSystem.FileClose(fArchivo);


				return true;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proGrabaFichero: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proCompruebaCarpetasTraditum                                                   -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n comprobar� que las carpetas a utilizar para el intercambio con la      -
		//-  interfaz de Traditum existen. Si no est�n, las intentar� crear.                         -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		internal static bool proCompruebaCarpetasTraditum()
		{

			try
			{

				string stPathTemp = String.Empty;

				// Creamos el objeto para borrar ficheros

				// Comprobamos si existe la carpeta original de traditum

				if (!Directory.Exists(stPathTraditum))
				{
					MessageBox.Show("�Path Traditum no existe!", Application.ProductName);
					return false;
				}

				stPathTemp = stPathTraditum + "term" + StringsHelper.Format(iNumeroTerminalTraditum, "000");

				// Comprobamos si existe la carpeta con el nombre de la terminal que hemos recuperado

				if (!Directory.Exists(stPathTemp))
				{

					// Si no existe, la creamos

					Directory.CreateDirectory(stPathTemp);

				}

				// Comprobamos ahora si existe la carpeta de Upload

				if (!Directory.Exists(stPathTemp + "\\upload"))
				{

					// Si no existe, la creamos

					Directory.CreateDirectory(stPathTemp + "\\upload");

				}
				else
				{

					// Vaciamos la carpeta

					File.Delete(stPathTemp + "\\upload\\*.*");

				}

				stPathTraditumUpload = stPathTemp + "\\upload\\";

				// Comprobamos ahora si existe la carpeta de Download

				if (!Directory.Exists(stPathTemp + "\\download"))
				{

					// Si no existe, la creamos

					Directory.CreateDirectory(stPathTemp + "\\download");

				}
				else
				{

					// Vaciamos la carpeta

					File.Delete(stPathTemp + "\\download\\*.*");

				}

				stPathTraditumDownload = stPathTemp + "\\download\\";

				// Si est� activo el modo TX, compruebo si existe la carpeta

				stPathTraditumTX = stPathTraditum + "tx\\" + "term" + StringsHelper.Format(iNumeroTerminalTraditum, "000");

				// Si existe el fichero, lo borramos

				if (FileSystem.Dir(stPathTraditumTX, FileAttribute.Archive) != "")
				{
					File.Delete(stPathTraditumTX);
				}

				// Todo est� OK


				return true;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proCompruebaCarpetasTraditum: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proCompruebaCarpetasITC                                                        -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n comprobar� que las carpetas a utilizar para el intercambio con la      -
		//-  interfaz de ITC existen. Si no est�n, las intentar� crear.                              -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		internal static bool proCompruebaCarpetasITC()
		{

			try
			{

				string stPathTemp = String.Empty;
				

				// Creamos el objeto para borrar ficheros

		

				// Comprobamos si existe la carpeta original de ITC

				if (!Directory.Exists(stPathITC))
				{
					MessageBox.Show("�Path ITC no existe!", Application.ProductName);
					return false;
				}

				stPathTemp = stPathITC + "term" + StringsHelper.Format(iNumeroTerminalITC, "000");

				// Comprobamos si existe la carpeta con el nombre de la terminal que hemos recuperado

				if (!Directory.Exists(stPathTemp))
				{

					// Si no existe, la creamos

					Directory.CreateDirectory(stPathTemp);

				}

				// Comprobamos ahora si existe la carpeta de Upload

				if (!Directory.Exists(stPathTemp + "\\upload"))
				{

					// Si no existe, la creamos

					Directory.CreateDirectory(stPathTemp + "\\upload");

				}
				else
				{

					// Vaciamos la carpeta

					File.Delete(stPathTemp + "\\upload\\*.*");

				}

				stPathITCUpload = stPathTemp + "\\upload\\";

				// Comprobamos ahora si existe la carpeta de Download

				if (!Directory.Exists(stPathTemp + "\\download"))
				{

					// Si no existe, la creamos

					Directory.CreateDirectory(stPathTemp + "\\download");

				}
				else
				{

					// Vaciamos la carpeta

					File.Delete(stPathTemp + "\\download\\*.*");

				}

				stPathITCDownload = stPathTemp + "\\download\\";

				// Si est� activo el modo TX, compruebo si existe la carpeta

				if (bModoTXITC)
				{

					stPathITCTX = stPathITC + "tx\\" + "term" + StringsHelper.Format(iNumeroTerminalITC, "000");

					// Si existe el fichero, lo borramos

					if (FileSystem.Dir(stPathITCTX, FileAttribute.Archive) != "")
					{
						File.Delete(stPathITCTX);
					}

				}
				else
				{

					stPathITCTX = "";

				}

				// Todo est� OK


				return true;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proCompruebaCarpetasITC: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: fAutorizaTraditum                                                              -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n generar� los ficheros necesarios para solicitar la autorizaci�n de     -
		//-  pr�cticas m�dicas a trav�s de la interfaz Traditum.                                     -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		internal static void fAutorizaTraditum(SqlConnection cnConexion, int lSociedad, string stGidenpac, ref string stPrestacion, int iCantidad, string stTipoServicio, int lServicio, bool bEsEstancia, ref string stResultado, ref string stNumAuto, ref string stNumOpera, ref string stFirmaAuto, ref string stCopago, ref string stMotivoRechazo)
		{

			try
			{

				string stTieneError = String.Empty;
				string mSeparadorDecimal = String.Empty;

				// Comprobamos si las carpetas est�n creadas. Si no, intentamos crearlas

				if (!proCompruebaCarpetasTraditum())
				{
					return;
				}

				// Definimos la estructura de los ficheros. Si ya estaban definidas, aprovechamos para vaciar los datos

				proDefineEstructurasTraditum();

				// Si es estancia, mandamos la prestaci�n de urgencia

				if (bEsEstancia)
				{
					stPrestacion = stPrestacionUrgenciaTraditum;
				}

				// Tomamos el separador decimal

				mSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(cnConexion);

				// Enviamos los archivos

				if (!proGeneraEnvioTraditum(cnConexion, lSociedad, stGidenpac, stPrestacion, iCantidad, stTipoServicio, lServicio))
				{
					return;
				}

				// Esperamos la respuesta

				if (proEsperaRespuestaTraditum(cnConexion, lSociedad))
				{

					// Hemos recibido respuesta. La procesamos

					stTieneError = "";

					// Vemos si hubo alg�n error en la transmisi�n o en el mensaje

					if (Ficheros[HDR].Campos[18].valor == "AE")
					{
						stTieneError = Ficheros[HDR].Campos[19].valor.Trim();
					}

					if (!Ficheros[HDR].Campos[21].valor.StartsWith("B"))
					{

						if (stTieneError == "")
						{
							stTieneError = "(" + Ficheros[HDR].Campos[21].valor + "): " + Ficheros[HDR].Campos[22].valor.Trim();
						}
						else
						{
							stTieneError = stTieneError + ". (" + Ficheros[HDR].Campos[21].valor + "): " + Ficheros[HDR].Campos[22].valor.Trim();
						}

						if (Ficheros[HDR].Campos[23].valor.Trim() != "")
						{

							stTieneError = stTieneError + ". " + Ficheros[HDR].Campos[23].valor.Trim();

						}

					}

					// Si tiene errores, miramos en el resto de ficheros a ver si hay m�s descripciones de los errores

					if (stTieneError.Trim() != "")
					{
						stTieneError = proRecogerErrores() + ". " + stTieneError;
					}

					// Si se encontr� alg�n error en la transmisi�n, mostramos el mensaje y marcamos la fila como si tuviera error

					if (stTieneError != "")
					{

						stResultado = "D";
						stNumAuto = "";
						stNumOpera = "";
						stFirmaAuto = "";
						stCopago = "";
						stMotivoRechazo = stTieneError;

						proGrabaLog(cnConexion, stGidenpac, stTipoServicio, lSociedad, stPrestacion, iCantidad, "T", "D", stMotivoRechazo, stFirmaAuto, stNumAuto, stNumOpera, "AU");

						return;

					}

					// Vemos el resultado de la transacci�n

					if (Ficheros[DT2].Campos[21].valor.StartsWith("B"))
					{

						// Autorizada

						stResultado = "A";
						stNumAuto = Ficheros[HDR].Campos[20].valor;
						stNumOpera = Ficheros[HDR].Campos[20].valor;
						stFirmaAuto = "";

						if (Ficheros[DT2].Campos[17].valor.Trim() != "")
						{
							stCopago = Ficheros[DT2].Campos[17].valor.Trim();
						}
						else
						{
							stCopago = Ficheros[DT2].Campos[29].valor.Trim();
						}

						if (stCopago.Trim() != "")
						{

							if (Double.Parse(Serrores.Replace(Serrores.Replace(stCopago, ".", ""), ",", "")) == 0)
							{

								// Si el valor es cero, lo mando vac�o

								stCopago = "";

							}
							else
							{

								// Ponemos el copago en el formato que se requiere

								stCopago = Serrores.ConvertirDecimales(ref stCopago, ref mSeparadorDecimal);

							}

						}

						stMotivoRechazo = "";

						proGrabaLog(cnConexion, stGidenpac, stTipoServicio, lSociedad, stPrestacion, iCantidad, "T", "A", stMotivoRechazo, stFirmaAuto, stNumAuto, stNumOpera, "AU");

					}
					else
					{

						// Denegada

						stResultado = "D";
						stNumAuto = "";
						stNumOpera = Ficheros[HDR].Campos[20].valor;
						stFirmaAuto = "";
						stCopago = "";
						stMotivoRechazo = "(" + Ficheros[DT2].Campos[21].valor.Substring(1) + "): " + Ficheros[DT2].Campos[22].valor;

						proGrabaLog(cnConexion, stGidenpac, stTipoServicio, lSociedad, stPrestacion, iCantidad, "T", "D", stMotivoRechazo, stFirmaAuto, stNumAuto, stNumOpera, "AU");

					}

				}
				else
				{

					// No se recibi� respuesta pasado el timeout

					stResultado = "";
					stNumAuto = "";
					stNumOpera = "";
					stFirmaAuto = "";
					stCopago = "";
					stMotivoRechazo = "Time-out";

					proCompruebaCarpetasTraditum();

					proGrabaLog(cnConexion, stGidenpac, stTipoServicio, lSociedad, stPrestacion, iCantidad, "T", "T", stMotivoRechazo, stFirmaAuto, stNumAuto, stNumOpera, "AU");

				}
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("fAutorizaTraditum: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		private static string proRecogerErrores()
		{

			int iPosicion = 0;
			int fArchivo = 0;
			string stContenido = String.Empty;

			// Valores iniciales

			int iIncremento = 107;
			StringBuilder stErrores = new StringBuilder();

			// Si est� el archivo DT2, lo recorremos

			if (FileSystem.Dir(stPathTraditumDownload + Ficheros[DT2].Nombre, FileAttribute.Archive) != "")
			{

				// Leemos el fichero

				fArchivo = FileSystem.FreeFile();

				FileSystem.FileOpen(fArchivo, stPathTraditumDownload + Ficheros[DT2].Nombre, OpenMode.Input, OpenAccess.Default, OpenShare.Default, -1);

				stContenido = FileSystem.InputString(fArchivo, (int) FileSystem.LOF(fArchivo));

				FileSystem.FileClose(fArchivo);

				// Posici�n de la descripci�n de aceptaci�n o rechazo

				iPosicion = 360;

				// Ahora vamos viendo si hay o no errores (hasta 4 repeticiones)

				for (int i = 1; i <= 4; i++)
				{

					// Si hay c�digo de error y hay texto, lo ponemos

					if (stContenido.Substring(iPosicion - 1, Math.Min(1, stContenido.Length - (iPosicion - 1))).Trim().ToUpper() == "M" && stContenido.Substring(iPosicion + 3, Math.Min(25, stContenido.Length - (iPosicion + 3))).Trim() != "")
					{
						stErrores.Append(
						                 ((stErrores.ToString().Trim() == "") ? "" : ", ") + 
						                 stContenido.Substring(iPosicion, Math.Min(3, stContenido.Length - iPosicion)).Trim().ToUpper() + " - " + 
						                 stContenido.Substring(iPosicion + 3, Math.Min(25, stContenido.Length - (iPosicion + 3))).Trim());
					}

					iPosicion += iIncremento;

				}

			}

			// Si est� el archivo DT2a, lo recorremos

			if (FileSystem.Dir(stPathTraditumDownload + Ficheros[DT2a].Nombre, FileAttribute.Archive) != "")
			{

				// Leemos el fichero

				fArchivo = FileSystem.FreeFile();

				FileSystem.FileOpen(fArchivo, stPathTraditumDownload + Ficheros[DT2a].Nombre, OpenMode.Input, OpenAccess.Default, OpenShare.Default, -1);

				stContenido = FileSystem.InputString(fArchivo, (int) FileSystem.LOF(fArchivo));

				FileSystem.FileClose(fArchivo);

				// Posici�n de la descripci�n de aceptaci�n o rechazo

				iPosicion = 16;

				// Ahora vamos viendo si hay o no errores (hasta 4 repeticiones)

				for (int i = 1; i <= 5; i++)
				{

					// Si hay c�digo de error y hay texto, lo ponemos

					if (stContenido.Substring(iPosicion - 1, Math.Min(1, stContenido.Length - (iPosicion - 1))).Trim().ToUpper() == "M" && stContenido.Substring(iPosicion + 3, Math.Min(25, stContenido.Length - (iPosicion + 3))).Trim() != "")
					{
						stErrores.Append(
						                 ((stErrores.ToString().Trim() == "") ? "" : ", ") + 
						                 stContenido.Substring(iPosicion, Math.Min(3, stContenido.Length - iPosicion)).Trim().ToUpper() + " - " + 
						                 stContenido.Substring(iPosicion + 3, Math.Min(25, stContenido.Length - (iPosicion + 3))).Trim());
					}

					iPosicion += iIncremento;

				}

			}

			// Si est� el archivo DT2b, lo recorremos

			if (FileSystem.Dir(stPathTraditumDownload + Ficheros[DT2b].Nombre, FileAttribute.Archive) != "")
			{

				// Leemos el fichero

				fArchivo = FileSystem.FreeFile();

				FileSystem.FileOpen(fArchivo, stPathTraditumDownload + Ficheros[DT2b].Nombre, OpenMode.Input, OpenAccess.Default, OpenShare.Default, -1);

				stContenido = FileSystem.InputString(fArchivo, (int) FileSystem.LOF(fArchivo));

				FileSystem.FileClose(fArchivo);

				// Posici�n de la descripci�n de aceptaci�n o rechazo

				iPosicion = 16;

				// Ahora vamos viendo si hay o no errores (hasta 4 repeticiones)

				for (int i = 1; i <= 5; i++)
				{

					// Si hay c�digo de error y hay texto, lo ponemos

					if (stContenido.Substring(iPosicion - 1, Math.Min(1, stContenido.Length - (iPosicion - 1))).Trim().ToUpper() == "M" && stContenido.Substring(iPosicion + 3, Math.Min(25, stContenido.Length - (iPosicion + 3))).Trim() != "")
					{
						stErrores.Append(
						                 ((stErrores.ToString().Trim() == "") ? "" : ", ") + 
						                 stContenido.Substring(iPosicion, Math.Min(3, stContenido.Length - iPosicion)).Trim().ToUpper() + " - " + 
						                 stContenido.Substring(iPosicion + 3, Math.Min(25, stContenido.Length - (iPosicion + 3))).Trim());
					}

					iPosicion += iIncremento;

				}

			}

			return stErrores.ToString().Trim();

		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: fAutorizaITC                                                                   -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n generar� los ficheros necesarios para solicitar la autorizaci�n de     -
		//-  pr�cticas m�dicas a trav�s de la interfaz ITC.                                          -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		internal static void fAutorizaITC(SqlConnection cnConexion, int lSociedad, string stGidenpac, ref string stPrestacion, int iCantidad, string stTipoServicio, bool bEsEstancia, ref string stResultado, ref string stNumAuto, ref string stNumOpera, ref string stFirmaAuto, ref string stCopago, ref string stMotivoRechazo, bool bConfirmaSesion = false)
		{

			try
			{

				// Comprobamos si las carpetas est�n creadas. Si no, intentamos crearlas

				if (!proCompruebaCarpetasITC())
				{
					return;
				}

				// Definimos la estructura de los ficheros. Si ya estaban definidas, aprovechamos para vaciar los datos

				proDefineEstructurasITC();

				// Enviamos los archivos

				if (bEsEstancia)
				{
					stPrestacion = stPrestacionUrgenciaITC;
				}

				// Si es de sesiones y viene con n�mero de autorizaci�n, estamos confirmando asistencia a una sesi�n

				if (bConfirmaSesion)
				{
					if (!proGeneraEnvioITC(cnConexion, lSociedad, stGidenpac, stPrestacion, iCantidad, stTipoServicio, stNumAuto, true))
					{
						return;
					}
				}
				else
				{
					if (!proGeneraEnvioITC(cnConexion, lSociedad, stGidenpac, stPrestacion, iCantidad, stTipoServicio))
					{
						return;
					}
				}

				// Esperamos la respuesta

				if (proEsperaRespuestaITC(cnConexion, lSociedad))
				{

					// Hemos recibido respuesta. La procesamos

					// Vemos el resultado de la transacci�n

					if (Ficheros[SVL_3].Campos[2].valor == "00")
					{

						// Autorizada

						stResultado = "A";
						stNumAuto = Ficheros[SVL_3].Campos[0].valor;
						stNumOpera = Ficheros[SVL_3].Campos[5].valor;
						stFirmaAuto = Ficheros[SVL_3].Campos[1].valor;
						stCopago = Ficheros[SVL_3].Campos[13].valor;
						stMotivoRechazo = "";

						proGrabaLog(cnConexion, stGidenpac, stTipoServicio, lSociedad, stPrestacion, iCantidad, "I", "A", stMotivoRechazo, stFirmaAuto, stNumAuto, stNumOpera, "AU");

					}
					else
					{

						// Denegada

						stResultado = "D";
						stNumAuto = "";
						stNumOpera = Ficheros[SVL_3].Campos[5].valor;
						stFirmaAuto = Ficheros[SVL_3].Campos[1].valor;
						stCopago = "";
						stMotivoRechazo = "(" + Ficheros[SVL_4].Campos[0].valor + "): " + proInterpretaRechazo(Ficheros[SVL_4].Campos[0].valor);

						if (Ficheros[SVL_3].Campos[2].valor == "01" && Ficheros[SVL_3].Campos[3].valor.Trim() != "")
						{
							stMotivoRechazo = stMotivoRechazo + ". " + Ficheros[SVL_3].Campos[3].valor.Trim();
						}

						proGrabaLog(cnConexion, stGidenpac, stTipoServicio, lSociedad, stPrestacion, iCantidad, "I", "D", stMotivoRechazo, stFirmaAuto, stNumAuto, stNumOpera, "AU");

					}

				}
				else
				{

					// No se recibi� respuesta pasado el timeout

					stResultado = "";
					stNumAuto = "";
					stNumOpera = "";
					stFirmaAuto = "";
					stCopago = "";
					stMotivoRechazo = "Time-out";

					proCompruebaCarpetasITC();

					proGrabaLog(cnConexion, stGidenpac, stTipoServicio, lSociedad, stPrestacion, iCantidad, "I", "T", stMotivoRechazo, stFirmaAuto, stNumAuto, stNumOpera, "AU");

				}
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("fAutorizaITC: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proGeneraEnvioTraditum                                                         -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n generar� los ficheros necesarios para solicitar la autorizaci�n de     -
		//-  pr�cticas m�dicas a trav�s de la interfaz Traditum.                                     -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		private static bool proGeneraEnvioTraditum(SqlConnection cnConexion, int lSociedad, string stGidenpac, string stPrestacion, int iCantidad, string stTipoServicio, int lServicio, string stNumAutoPrevio = "")
		{

			bool result = false;
			try
			{

				string[] stCadena = null;
				string[] stcodigos = null;
				string[] stNomencladores = null;
				string stSql = String.Empty;
				string stCargaDT2 = String.Empty;
				string stCargaDT2a = String.Empty;
				string stCargaDT2b = String.Empty;
				DataSet RrDatos = null;
				int i = 0;

				// Inicializamos la semilla

				VBMath.Randomize();

				// Creamos la estructura de la cabecera y la completamos

				stCadena = UpgradeHelpers.Helpers.ArraysHelper.InitializeArray<string>(Ficheros[HDR].Campos.GetUpperBound(0) + 1);

				if (stNumAutoPrevio != "")
				{
					stCadena[0] = CANCELACION_SOL; // Tipo de mensaje CANCELACION
				}
				else
				{
					stCadena[0] = AUTORIZACION_SOL; // Tipo de mensaje AUTORIZACION
				}

				stCadena[1] = "M"; // Tipo de carga
				stCadena[2] = stIdSitioEmisor; // ID Sitio Emisor

				// Traemos los datos de la aplicaci�n receptora

				stSql = "SELECT D.* FROM DTRADITU D, DSOCTRAD S WHERE D.gtraditum = S.gtraditum AND S.gsocieda = " + lSociedad.ToString();

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					MessageBox.Show("Sociedad no encontrada en tabla de datos Traditum", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
					result = false;
					 
					RrDatos.Close();
					return result;
				}
				else
				{
					stCadena[3] = Convert.ToString(RrDatos.Tables[0].Rows[0]["idaplica"]); // ID Aplicaci�n Receptora
					stCadena[4] = Convert.ToString(RrDatos.Tables[0].Rows[0]["idrecept"]); // ID Sitio Receptor
					stCadena[5] = Convert.ToString(RrDatos.Tables[0].Rows[0]["iduniver"]); // ID Universal de Sitio Receptor
					stCadena[6] = Convert.ToString(RrDatos.Tables[0].Rows[0]["tipiduni"]); // Tipo de ID Universal de Sitio Receptor
					stCadena[11] = Convert.ToString(RrDatos.Tables[0].Rows[0]["dtipiden"]); // Tipo de Identificaci�n del Prestador (Solicitante)
					stCadena[12] = Convert.ToString(RrDatos.Tables[0].Rows[0]["dnumiden"]); // N�mero de Identificaci�n del Prestador (Solicitante)
				}

				 
				RrDatos.Close();

				stCadena[7] = DateTime.Now.ToString("yyMMddHHNNSS") + StringsHelper.Format(Convert.ToInt32(99999999 * VBMath.Rnd()), "00000000"); // ID de Mensaje de Solicitud
				stCadena[9] = "P"; // Id de Procesamiento (Producci�n -> P, Desarrollo -> D)
				stCadena[10] = "PS"; // Rol del Prestador
				stCadena[16] = "NE"; // AcceptAcknowledgementType
				stCadena[17] = "NE"; // ApplicationAcknowledgementType
				stCadena[24] = "ARG"; // C�digo de Pa�s
				//    stCadena(25) = Chr(13) & Chr(10)                            ' Fin de archivo   ----- Ya lo incluye al grabarlo en el fichero

				// Comprobamos si hay que mandar el servicio
				if (stTipoServicio == "C" || stTipoServicio == "Q" || stTipoServicio == "S")
				{

					stSql = "SELECT A.gespeofi FROM DESPEOFI A, DCODPRES B WHERE " + 
					        "B.gprestac = '" + stPrestacion + "' AND " + 
					        "A.gservici = B.gservici AND " + 
					        "A.gsocieda = " + lSociedad.ToString();

				}
				else
				{

					stSql = "SELECT gespeofi FROM DESPEOFI WHERE " + 
					        " gservici = " + lServicio.ToString() + 
					        " AND gsocieda = " + lSociedad.ToString();

				}

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter_2.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					stCadena[13] = Convert.ToString(RrDatos.Tables[0].Rows[0]["gespeofi"]).Trim();
				}
				else
				{
					stCadena[13] = "";
				}

				 
				RrDatos.Close();

				// Grabamos el fichero

				if (!proGrabaFichero(HDR, stPathTraditumUpload, proGeneraEstructura(HDR, stCadena)))
				{
					return false;
				}

				// Montamos la estructura del DT1

				stCadena = ArraysHelper.InitializeArray<string>(Ficheros[DT1].Campos.GetUpperBound(0) + 1);

				stCadena[0] = "HC";

				// Traemos el c�digo de la tarjeta, eliminando todos los caracteres especiales

				stSql = "SELECT " + 
				        "isNull(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(nafiliac, '/', ''), '\\', ''), '-', ''), ' ', ''), '.', ''), '') Tarjeta " + 
				        " FROM DENTIPAC WHERE gidenpac = '" + stGidenpac + "' AND gsocieda = " + lSociedad.ToString();

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter_3.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					stCadena[1] = "";
				}
				else
				{
					stCadena[1] = Convert.ToString(RrDatos.Tables[0].Rows[0]["Tarjeta"]).Trim();
				}

				 
				RrDatos.Close();

				stCadena[9] = "O";
				stCadena[10] = "V";
				stCadena[11] = "P";
				if (stPrestacionUrgenciaTraditum == stPrestacion)
				{

					stCadena[11] = "G";

					// En funci�n del servicio y el Cliente asistencial, quiz� haya que mandar una letra distinta

					stSql = "SELECT iletraur FROM DSERTRAD WHERE " + 
					        "gservici = " + lServicio.ToString() + " AND " + 
					        "gsocieda = " + lSociedad.ToString() + " AND " + 
					        "iletraur IS NOT NULL";

					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, cnConexion);
					RrDatos = new DataSet();
					tempAdapter_4.Fill(RrDatos);

					if (RrDatos.Tables[0].Rows.Count != 0)
					{
						stCadena[11] = Convert.ToString(RrDatos.Tables[0].Rows[0]["iletraur"]);
					}

					 
					RrDatos.Close();

				}

				//    stCadena(12) = Chr(13) & Chr(10)       ----- Ya lo incluye al grabarlo en el fichero

				// Grabamos el fichero

				if (!proGrabaFichero(DT1, stPathTraditumUpload, proGeneraEstructura(DT1, stCadena)))
				{
					return false;
				}

				// Si hay n�mero de autorizaci�n, estamos mandando una cancelaci�n

				if (stNumAutoPrevio == "")
				{

					// Montamos la estructura del DT2

					stCadena = ArraysHelper.InitializeArray<string>(Ficheros[DT2].Campos.GetUpperBound(0) + 1);

					// Buscamos el c�digo oficial, primero para la prestaci�n y la sociedad, si no, para la prestaci�n y sociedad 0, si no,
					// dejamos el c�digo de la prestaci�n

					stSql = "SELECT gcodofic, dcodnome FROM DPRESOFI WHERE " + 
					        "gprestac = '" + stPrestacion + "' AND " + 
					        "gsocieda = " + lSociedad.ToString() + " order by norden";

					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stSql, cnConexion);
					RrDatos = new DataSet();
					tempAdapter_5.Fill(RrDatos);

					if (RrDatos.Tables[0].Rows.Count != 0)
					{

						// Hay prestaci�n oficial. La guardamos

						stcodigos = ArraysHelper.InitializeArray<string>(RrDatos.Tables[0].Rows.Count);
						stNomencladores = ArraysHelper.InitializeArray<string>(RrDatos.Tables[0].Rows.Count);

						i = 0;

						foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
						{

							stcodigos[i] = Convert.ToString(iteration_row["gcodofic"]).Trim();
							stNomencladores[i] = Convert.ToString(iteration_row["dcodnome"]).Trim() + "";

							i++;


						}

					}
					else
					{

						stSql = "SELECT gcodofic, dcodnome FROM DPRESOFI WHERE " + 
						        "gprestac = '" + stPrestacion + "' AND " + 
						        "gsocieda = 0";

						SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSql, cnConexion);
						RrDatos = new DataSet();
						tempAdapter_6.Fill(RrDatos);

						if (RrDatos.Tables[0].Rows.Count != 0)
						{

							// Hay prestaci�n oficial gen�rica. La guardamos

							stcodigos = ArraysHelper.InitializeArray<string>(RrDatos.Tables[0].Rows.Count);
							stNomencladores = ArraysHelper.InitializeArray<string>(RrDatos.Tables[0].Rows.Count);

							i = 0;

							foreach (DataRow iteration_row_2 in RrDatos.Tables[0].Rows)
							{

								stcodigos[i] = Convert.ToString(iteration_row_2["gcodofic"]).Trim();
								stNomencladores[i] = Convert.ToString(iteration_row_2["dcodnome"]).Trim() + "";

								i++;


							}

						}
						else
						{

							// No hay prestaci�n oficial. Guardamos nuestro c�digo de prestaci�n

							stcodigos = new string[]{String.Empty};
							stNomencladores = new string[]{String.Empty};

							stcodigos[0] = stPrestacion.Trim();
							stNomencladores[0] = "";

						}

					}

					 
					RrDatos.Close();

					// Si hay m�s de 14 pr�cticas, no podemos procesarlo con Traditum. Que lo separen

					if (stcodigos.GetUpperBound(0) >= 14)
					{

						MessageBox.Show("proGeneraEnvioTraditum: No se pueden enviar m�s de 14 pr�cticas simult�neas a trav�s de Traditum. Divida la solicitud.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
						return false;

					}

					// Vaciamos los campos de cargas

					stCargaDT2 = "";
					stCargaDT2a = "";
					stCargaDT2b = "";

					// Recorremos el listado de c�digos

					for (i = 0; i <= stcodigos.GetUpperBound(0); i++)
					{

						// Cargamos la pr�ctica, nomenclador y cantidad

						stCadena[19] = stNomencladores[i]; // C�digo de Nomenclador
						stCadena[20] = stcodigos[i]; // C�digo de Pr�ctica
						stCadena[26] = iCantidad.ToString(); // Cantidad Solicitada

						if (i < 4)
						{

							if (i == 0)
							{
								// Si es la primera, cargamos la estructura completa

								stCargaDT2 = proGeneraEstructura(DT2, stCadena);
							}
							else
							{
								// Las posteriores, s�lo cargamos la parte de la pr�ctica

								stCargaDT2 = stCargaDT2 + proGeneraEstructura(DT2, stCadena, 19);
							}

						}
						else if (i < 9)
						{ 

							stCargaDT2a = stCargaDT2a + proGeneraEstructura(DT2, stCadena, 19);

						}
						else
						{

							stCargaDT2b = stCargaDT2b + proGeneraEstructura(DT2, stCadena, 19);

						}

					}

					// Si hemos salido del for antes de haber mandado alguno de los ficheros, lo mandamos

					if (stCargaDT2.Trim() != "")
					{
						// Tenemos que grabar el dt2

						if (!proGrabaFichero(DT2, stPathTraditumUpload, stCargaDT2))
						{
							return false;
						}

					}

					if (stCargaDT2a.Trim() != "")
					{
						// Tenemos que grabar el dt2a

						if (!proGrabaFichero(DT2a, stPathTraditumUpload, stCargaDT2a))
						{
							return false;
						}

					}

					if (stCargaDT2b.Trim() != "")
					{

						// Tenemos que grabar el dt2b

						if (!proGrabaFichero(DT2b, stPathTraditumUpload, stCargaDT2b))
						{
							return false;
						}

					}

					//        stCadena(19) = stNomencladores(0)                                   ' C�digo de Nomenclador
					//        stCadena(20) = stCodigos(0)                                         ' C�digo de Pr�ctica
					//        stCadena(26) = iCantidad                                            ' Cantidad Solicitada
					//
					//        stCarga = proGeneraEstructura(DT2, stCadena)
					//
					//        For i = 1 To UBound(stCodigos)
					//
					//            stCadena(19) = stNomencladores(i)                                   ' C�digo de Nomenclador
					//            stCadena(20) = stCodigos(i)                                         ' C�digo de Pr�ctica
					//            stCadena(26) = iCantidad                                            ' Cantidad Solicitada
					//
					//            stCarga = stCarga & proGeneraEstructura(DT2, stCadena, 19)
					//
					//        Next
					//
					//        ' Grabamos el fichero
					//
					//        If Not proGrabaFichero(DT2, stPathTraditumUpload, stCarga) Then
					//            proGeneraEnvioTraditum = False
					//            Exit Function
					//        End If

				}
				else
				{

					// Montamos la estructura del DT3

					stCadena = ArraysHelper.InitializeArray<string>(Ficheros[DT3].Campos.GetUpperBound(0) + 1);

					stCadena[0] = stNumAutoPrevio; // N�mero de autorizaci�n previo
					//        stCadena(2) = Chr(13) & Chr(10)          ' Fin de fichero    ----- Ya lo incluye al grabarlo en el fichero

					// Grabamos el fichero

					if (!proGrabaFichero(DT3, stPathTraditumUpload, proGeneraEstructura(DT3, stCadena)))
					{
						return false;
					}

				}

				// Generamos el fichero en la carpeta TX

				if (!proGrabaFicheroTX(stPathTraditumTX))
				{
					return false;
				}


				return true;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proGeneraEnvioTraditum: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proGeneraEnvioITC                                                              -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n generar� los ficheros necesarios para solicitar la autorizaci�n de     -
		//-  pr�cticas m�dicas a trav�s de la interfaz ITC.                                          -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		//Private Function proGeneraEnvioITC(cnConexion As rdoConnection, lSociedad As Long, stGidenpac As String, _
		//'                                   mPrestaciones As Variant, stTipoServicio As String) As Boolean

		private static bool proGeneraEnvioITC(SqlConnection cnConexion, int lSociedad, string stGidenpac, string stPrestacion, int iCantidad, string stTipoServicio, string stNumAutoPrevio = "", bool bConfirmarSesion = false)
		{

			bool result = false;
			try
			{

				string[] stCadena = null;
				string[] stcodigos = null;
				string stSql = String.Empty;
				string stTarjeta = String.Empty;
				//    Dim stPrestacionOficial     As String
				DataSet RrDatos = null;
				int i = 0;

				//-----------------------
				// _svl.0
				//-----------------------

				stCadena = ArraysHelper.InitializeArray<string>(Ficheros[SVL_0].Campos.GetUpperBound(0) + 1);

				stCadena[0] = "V107"; // Versi�n del formato�de transacciones

				// Traemos los datos de la socidad receptora

				stSql = "SELECT * FROM DSOCIITC WHERE gsocieda = " + lSociedad.ToString();

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					MessageBox.Show("Sociedad no encontrada en tabla de datos ITC", Application.ProductName);
					result = false;
					 
					RrDatos.Close();
					return result;
				}
				else
				{
					stCadena[1] = Convert.ToString(RrDatos.Tables[0].Rows[0]["codigoem"]); // C�digo de la empresa
				}

				 
				RrDatos.Close();

				stCadena[2] = "01"; // C�digo de Actividad

				// Grabamos el fichero

				if (!proGrabaFichero(SVL_0, stPathITCUpload, proGeneraEstructura(SVL_0, stCadena)))
				{
					return false;
				}

				//-----------------------
				// _svl.1
				//-----------------------

				stCadena = ArraysHelper.InitializeArray<string>(Ficheros[SVL_1].Campos.GetUpperBound(0) + 1);

				// Traemos el c�digo de la tarjeta, eliminando todos los caracteres especiales

				stSql = "SELECT " + 
				        "isNull(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(nafiliac, '/', ''), '\\', ''), '-', ''), ' ', ''), '.', ''), '') Tarjeta " + 
				        " FROM DENTIPAC WHERE gidenpac = '" + stGidenpac + "' AND gsocieda = " + lSociedad.ToString();

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter_2.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					stTarjeta = "";
				}
				else
				{
					stTarjeta = Convert.ToString(RrDatos.Tables[0].Rows[0]["Tarjeta"]).Trim();
				}

				 
				RrDatos.Close();

				stCadena[0] = stCUITCentroITC; // CUIT del centro
				stCadena[2] = stTarjeta; // Tarjeta del paciente
				stCadena[5] = "2"; // Registro

				// Si es de sesiones y estamos pidiendo autorizaci�n para el tratamiento, mandamos una C.
				// Si es de sesiones y estamos autorizando una sesi�n concreta, mandamos una A.
				// Para el resto de casos, mandamos una A.

				if (stTipoServicio == "S" && !bConfirmarSesion)
				{
					stCadena[6] = "C"; // Registro de Solicitud de Sesiones
				}
				else
				{
					stCadena[6] = "A"; // Solicitud de autorizaci�n
				}

				// ----------------------------------------

				stCadena[7] = "M"; // Env�o en campo tarjeta

				// Si hay n�mero de autorizaci�n, puede ser una cancelaci�n o una autorizaci�n de una sesi�n concreta

				if (stNumAutoPrevio != "")
				{

					if (!bConfirmarSesion)
					{
						stCadena[5] = "4";
						stCadena[6] = "A";
					}

					stCadena[8] = "98"; // Firma autorizaci�n
					stCadena[9] = stNumAutoPrevio; // Autorizaci�n previa

				}

				stCadena[28] = "\r" + "\n"; // Cierre de fichero

				// Grabamos el fichero

				if (!proGrabaFichero(SVL_1, stPathITCUpload, proGeneraEstructura(SVL_1, stCadena)))
				{
					return false;
				}

				//-----------------------
				// _svl.2
				//-----------------------

				stCadena = ArraysHelper.InitializeArray<string>(Ficheros[SVL_2].Campos.GetUpperBound(0) + 1);

				// Buscamos el c�digo oficial, primero para la prestaci�n y la sociedad, si no, para la prestaci�n y sociedad 0, si no,
				// dejamos el c�digo de la prestaci�n

				stSql = "SELECT gcodofic FROM DPRESOFI WHERE " + 
				        "gprestac = '" + stPrestacion + "' AND " + 
				        "gsocieda = " + lSociedad.ToString() + " " + 
				        "order by norden";

				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter_3.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{

					// Hay prestaci�n oficial. La guardamos

					stcodigos = ArraysHelper.InitializeArray<string>(RrDatos.Tables[0].Rows.Count);

					i = 0;

					foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
					{

						stcodigos[i] = Convert.ToString(iteration_row["gcodofic"]).Trim();

						i++;


					}

					//        stPrestacionOficial = Trim(RrDatos("gcodofic"))

				}
				else
				{

					stSql = "SELECT gcodofic FROM DPRESOFI WHERE " + 
					        "gprestac = '" + stPrestacion + "' AND " + 
					        "gsocieda = 0 order by norden";

					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stSql, cnConexion);
					RrDatos = new DataSet();
					tempAdapter_4.Fill(RrDatos);

					if (RrDatos.Tables[0].Rows.Count != 0)
					{

						// Hay prestaci�n oficial gen�rica. La guardamos

						stcodigos = ArraysHelper.InitializeArray<string>(RrDatos.Tables[0].Rows.Count);

						i = 0;

						foreach (DataRow iteration_row_2 in RrDatos.Tables[0].Rows)
						{

							stcodigos[i] = Convert.ToString(iteration_row_2["gcodofic"]).Trim();
							i++;


						}

						//            stPrestacionOficial = Trim(RrDatos("gcodofic"))

					}
					else
					{

						// No hay prestaci�n oficial. Guardamos nuestro c�digo de prestaci�n

						stcodigos = new string[]{String.Empty};

						stcodigos[0] = stPrestacion.Trim();
						//stPrestacionOficial = Trim(stPrestacion)

					}

				}

				 
				RrDatos.Close();

				for (i = 0; i <= stcodigos.GetUpperBound(0); i++)
				{

					stCadena[0] = stcodigos[i]; //stPrestacionOficial                                 ' C�digo de la prestaci�n

					// Tipo de prestaci�n (1-Ambulatoria, 2-Cl�nica, 3-Quir�rgica)

					if (stTipoServicio == "H" || stTipoServicio == "U")
					{
						stCadena[1] = "2";
					}
					else if (stTipoServicio == "Q")
					{ 
						stCadena[1] = "3";
					}
					else
					{
						stCadena[1] = "1";
					}

					stCadena[2] = "0"; // Arancel prestaci�n
					stCadena[3] = iCantidad.ToString(); // Cantidad
					//        stCadena(11) = Chr(13) & Chr(10)                            ' Cierre de fichero

					// Grabamos el fichero

					if (!proGrabaFichero(SVL_2, stPathITCUpload, proGeneraEstructura(SVL_2, stCadena), i != 0))
					{
						return false;
					}

				}

				// Una vez creados los tres ficheros, creamos el documento en la carpeta TX si est� activado este modo

				if (bModoTXITC)
				{

					// Creamos el fichero TX

					if (!proGrabaFicheroTX(stPathITCTX))
					{
						return false;
					}

				}


				return true;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proGeneraEnvioITC: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		private static bool proGrabaFicheroTX(string stPathFicheroTX)
		{

			try
			{

				int fArchivo = 0;

				fArchivo = FileSystem.FreeFile();

				FileSystem.FileOpen(fArchivo, stPathFicheroTX, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);

				FileSystem.FileClose(fArchivo);


				return true;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proGrabaFicheroTX: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proEsperaRespuestaITC                                                          -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n esperar� la respuesta del servicio ITC, leyendo cada segundo de la     -
		//-  carpeta correspondiente hasta encontrar los ficheros de respuesta. Si pasado un cierto  -
		//-  tiempo (determinado por la constante) no se recibieron los ficheros, se tendr� como un  -
		//-  timeout y se cancelar� la solicitud.                                                    -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		private static bool proEsperaRespuestaITC(SqlConnection cnConexion, int lSociedad)
		{

			try
			{

				// Esperamos a recibir los dos ficheros de respuesta

				float Inicio = 0;

				Inicio = (float) DateTime.Now.TimeOfDay.TotalSeconds;


				while(DateTime.Now.TimeOfDay.TotalSeconds < Inicio + iTiempoEsperaITC)
				{

					// Comprobamos si hay respuesta en el directorio

					if (FileSystem.Dir(stPathITCDownload + Ficheros[SVL_2].Nombre, FileAttribute.Archive) != "" && FileSystem.Dir(stPathITCDownload + Ficheros[SVL_3].Nombre, FileAttribute.Archive) != "")
					{

						// Ya tenemos los dos ficheros. Los procesamos

						// Procesamos el SVL_2 tantas veces como elementos haya

						proProcesaRespuesta(stPathITCDownload, SVL_2);
						proProcesaRespuesta(stPathITCDownload, SVL_3);

						// Esperamos por si envian tambi�n el fichero SVL4

						UpgradeSupportHelper.PInvoke.SafeNative.kernel32.Sleep(500);

						// Si existe, leemos tambi�n el _svl.4 (porque hubo alg�n rechazo)

						if (FileSystem.Dir(stPathITCDownload + Ficheros[SVL_4].Nombre, FileAttribute.Archive) != "")
						{

							proProcesaRespuesta(stPathITCDownload, SVL_4);

						}


						return true;

					}

					Application.DoEvents();

				};


				return false;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proEsperaRespuestaITC: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: proProcesaRespuesta                                                            -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n leer� el contenido de un fichero concreto y lo volcar� en el array     -
		//-  correspondiente.                                                                        -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		private static void proProcesaRespuesta(string stPathRespuesta, int iFichero)
		{

			int i = 0;
			try
			{

				int fArchivo = 0;
				string stContenido = String.Empty;

				// Leemos los datos del fichero

				fArchivo = FileSystem.FreeFile();

				FileSystem.FileOpen(fArchivo, stPathRespuesta + Ficheros[iFichero].Nombre, OpenMode.Input, OpenAccess.Default, OpenShare.Default, -1);

				stContenido = FileSystem.InputString(fArchivo, (int) FileSystem.LOF(fArchivo));

				FileSystem.FileClose(fArchivo);

				// Guardamos los datos del fichero en la estructura

				for (; i <= Ficheros[iFichero].Campos.GetUpperBound(0); i++)
				{

					if (stContenido.Trim() != "")
					{

						Ficheros[iFichero].Campos[i].valor = stContenido.Substring(0, Math.Min(Ficheros[iFichero].Campos[i].Tamano, stContenido.Length));

						stContenido = stContenido.Substring(Ficheros[iFichero].Campos[i].Tamano);

					}
					else
					{

						Ficheros[iFichero].Campos[i].valor = "";

					}

				}
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proProcesaRespuesta: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

				for (; i <= Ficheros[iFichero].Campos.GetUpperBound(0); i++)
				{

					Ficheros[iFichero].Campos[i].valor = "";

				}
			}

		}

		//--------------------------------------------------------------------------------------------
		//-                                                                                          -
		//-  Funci�n: NombreMaquina                                                                  -
		//-                                                                                          -
		//-  Descripci�n:                                                                            -
		//-                                                                                          -
		//-      Esta funci�n obtendr� el nombre de la m�quina desde la que se est� ejecutando el    -
		//-  proceso.                                                                                -
		//-                                                                                          -
		//--------------------------------------------------------------------------------------------
		//UPGRADE_NOTE: (7001) The following declaration (NombreMaquina) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string NombreMaquina()
		//{
				//
				//try
				//{
					//
					//Devuelve el nombre del equipo actual
					//
					//string sComputerName = String.Empty;
					//int ComputerNameLength = 0;
					//
					//sComputerName = new string((char) 0, Serrores.MAX_COMPUTERNAME_LENGTH + 1);
					//ComputerNameLength = Serrores.MAX_COMPUTERNAME_LENGTH;
					//AutorizacionesSupport.PInvoke.SafeNative.kernel32.GetComputerName(ref sComputerName, ref ComputerNameLength);
					//
					//
					//return sComputerName.Substring(0, Math.Min(ComputerNameLength, sComputerName.Length));
				//}
				//catch (System.Exception excep)
				//{
					//
					//MessageBox.Show("NombreMaquina: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
					//return "";
				//}
		//}

		private static string proInterpretaRechazo(string stRechazo)
		{

			if (stRechazo == "000")
			{
				return "Solicitud Autorizada/Aprobada";
			}
			else if (stRechazo == "001")
			{ 
				return "Asociado (beneficiario) Inexistente";
			}
			else if (stRechazo == "002")
			{ 
				return "Asociado (beneficiario) dado de baja o con alta a futuro";
			}
			else if (stRechazo == "003")
			{ 
				return "Asociado (beneficiario) moroso";
			}
			else if (stRechazo == "004")
			{ 
				return "Prestaci�n Inexistente";
			}
			else if (stRechazo == "005")
			{ 
				return "Carencia o tiempo de espera para la prestaci�n";
			}
			else if (stRechazo == "006")
			{ 
				return "Preexistencia";
			}
			else if (stRechazo == "007")
			{ 
				return "Excepci�n";
			}
			else if (stRechazo == "008")
			{ 
				return "Supera Tope";
			}
			else if (stRechazo == "010")
			{ 
				return "Prestador Inexistente";
			}
			else if (stRechazo == "011")
			{ 
				return "Prestador No Habilitado para el POS XXX";
			}
			else if (stRechazo == "012")
			{ 
				return "Prestaci�n no contratada";
			}
			else if (stRechazo == "013")
			{ 
				return "C�digo de autorizaci�n err�neo";
			}
			else if (stRechazo == "014")
			{ 
				return "Solicitud anulada / rechazada/ o sin saldo";
			}
			else if (stRechazo == "015")
			{ 
				return "No existe la solicitud para el socio";
			}
			else if (stRechazo == "017")
			{ 
				return "Prestador no autorizado a prescribir";
			}
			else if (stRechazo == "018")
			{ 
				return "Prestador no autorizado a emitir �rdenes de internaci�n";
			}
			else if (stRechazo == "019")
			{ 
				return "La transacci�n es err�nea";
			}
			else if (stRechazo == "021")
			{ 
				return "No requiere prescripci�n";
			}
			else if (stRechazo == "023")
			{ 
				return "La prestaci�n no requiere autorizaci�n previa";
			}
			else if (stRechazo == "024")
			{ 
				return "Solicitud existente";
			}
			else if (stRechazo == "025")
			{ 
				return "Fecha inv�lida";
			}
			else if (stRechazo == "026")
			{ 
				return "Credencial vencida";
			}
			else if (stRechazo == "027")
			{ 
				return "C�digo de Fin de Terapia Inv�lido";
			}
			else if (stRechazo == "028")
			{ 
				return "Fecha de Pr�ctica < Fecha de la Orden";
			}
			else if (stRechazo == "029")
			{ 
				return "Debe ser ingreso manual";
			}
			else if (stRechazo == "030")
			{ 
				return "Debe informar n�mero de orden";
			}
			else if (stRechazo == "031")
			{ 
				return "Asociado (beneficiario) no habilitado";
			}
			else if (stRechazo == "032")
			{ 
				return "Asociado (beneficiario) no habilitado";
			}
			else if (stRechazo == "036")
			{ 
				return "Pendiente de resoluci�n";
			}
			else if (stRechazo == "037")
			{ 
				return "Solicitud rechazada";
			}
			else if (stRechazo == "038")
			{ 
				return "La solicitud est� anulada";
			}
			else if (stRechazo == "039")
			{ 
				return "Se ha cargado mal el n�mero de solicitud";
			}
			else if (stRechazo == "040")
			{ 
				return "Anulaci�n no registrada";
			}
			else if (stRechazo == "041")
			{ 
				return "La solicitud no existe";
			}
			else
			{
				return ""; //"Error desconocido (" & stRechazo & ")"
			}

		}

		internal static bool bUtilizaITC(SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaPersona, string ParaTipoConc, string ParaConcepto)
		{

			bool result = false;
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;
				int lservicioprestacion = 0;
				object clasemensaje = null;
				int imensaje = 0;


				// Tomamos el servicio


				switch(ParaTipoConc)
				{
					case "ES" : 
						 
						stSql = "SELECT gservici FROM DSERVICI with (nolock) WHERE GSERVICI= " + ParaConcepto; 
						 
						break;
					case "PR" : 
						 
						stSql = "SELECT gservici FROM DCODPRES with (nolock) WHERE GPRESTAC= '" + ParaConcepto + "'"; 
						 
						break;
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{
					lservicioprestacion = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["gservici"]);
				}

				 
				RrDatos.Close();

				// Vemos si usa ITC

				stSql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
				        " (case when gsocieda is not null then 1 else 0 end)+ " + 
				        " (case when gregimen is not null then 1 else 0 end)+ " + 
				        " (case when gservici is not null then 1 else 0 end)+ " + 
				        " (case when gpersona is not null then 1 else 0 end), " + 
				        " ISNULL(iautoe,'N') iautoe " + 
				        " FROM IFMSFA_DOCUTAU " + 
				        " WHERE " + 
				        " rconc = '" + ParaTipoConc + "' AND " + 
				        " (econc is null or econc = '" + ParaConcepto + "') AND " + 
				        " (gsocieda is null or gsocieda = " + ParaSociedad.ToString() + ") AND " + 
				        " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
				        " (gservici is null or gservici = " + lservicioprestacion.ToString() + ") AND " + 
				        " (gpersona is null or gpersona = " + ParaPersona.ToString() + ") AND " + 
				        " fborrado is null " + 
				        " ORDER BY iprioridad desc";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter_2.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{

					RrDatos.MoveFirst();

					result = (Convert.ToString(RrDatos.Tables[0].Rows[0]["iautoe"]).Trim().ToUpper() == "I");

				}

				 
				RrDatos.Close();

				return result;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("bUtilizaITC: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		internal static bool bEsSolicitudSesionesTraditum(SqlConnection ParaConexion, int ParaSociedad, string ParaItiposer, int ParaServicio, int ParaPersona, string[, ] ParaPrestaciones, bool ParaEsSolicitudSesionesTraditum)
		{

			bool result = false;
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;


				if (ParaEsSolicitudSesionesTraditum && bUtilizaTraditum(ParaConexion, ParaSociedad, ParaItiposer, ParaPersona, ParaPrestaciones[1, 1], ParaPrestaciones[1, 2]))
				{

					// Comprobamos si su servicio dice que tiene que ser validado por sesi�n y no por tratamiento

					stSql = "SELECT 'S' FROM DSERTRAD WHERE " + 
					        "gservici = " + ParaServicio.ToString() + " AND " + 
					        "gsocieda = " + ParaSociedad.ToString() + " AND " + 
					        "ivalises = 'S'";

					SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParaConexion);
					RrDatos = new DataSet();
					tempAdapter.Fill(RrDatos);

					result = RrDatos.Tables[0].Rows.Count != 0;

					 
					RrDatos.Close();

				}

				return result;
			}
			catch
			{

				return false;
			}
		}

		internal static bool bUtilizaTraditum(SqlConnection ParacnConexion, int ParaSociedad, string ParaItiposer, int ParaPersona, string ParaTipoConc, string ParaConcepto)
		{

			bool result = false;
			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;
				int lservicioprestacion = 0;
				object clasemensaje = null;
				int imensaje = 0;


				// Tomamos el servicio


				switch(ParaTipoConc)
				{
					case "ES" : 
						 
						stSql = "SELECT gservici FROM DSERVICI with (nolock) WHERE GSERVICI= " + ParaConcepto; 
						 
						break;
					case "PR" : 
						 
						stSql = "SELECT gservici FROM DCODPRES with (nolock) WHERE GPRESTAC= '" + ParaConcepto + "'"; 
						 
						break;
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{
    				lservicioprestacion = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["gservici"]);
				}

				 
				RrDatos.Close();

				// Vemos si usa ITC

				stSql = "SELECT iprioridad = (case when econc is not null then 1 else 0 end)+ " + 
				        " (case when gsocieda is not null then 1 else 0 end)+ " + 
				        " (case when gregimen is not null then 1 else 0 end)+ " + 
				        " (case when gservici is not null then 1 else 0 end)+ " + 
				        " (case when gpersona is not null then 1 else 0 end), " + 
				        " ISNULL(iautoe,'N') iautoe " + 
				        " FROM IFMSFA_DOCUTAU " + 
				        " WHERE " + 
				        " rconc = '" + ParaTipoConc + "' AND " + 
				        " (econc is null or econc = '" + ParaConcepto + "') AND " + 
				        " (gsocieda is null or gsocieda = " + ParaSociedad.ToString() + ") AND " + 
				        " (gregimen is null or gregimen ='" + ParaItiposer + "') AND " + 
				        " (gservici is null or gservici = " + lservicioprestacion.ToString() + ") AND " + 
				        " (gpersona is null or gpersona = " + ParaPersona.ToString() + ") AND " + 
				        " fborrado is null " + 
				        " ORDER BY iprioridad desc";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stSql, ParacnConexion);
				RrDatos = new DataSet();
				tempAdapter_2.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{

					RrDatos.MoveFirst();

					result = (Convert.ToString(RrDatos.Tables[0].Rows[0]["iautoe"]).Trim().ToUpper() == "T");

				}

				 
				RrDatos.Close();

				return result;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("bUtilizaTraditum: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		private static void proGrabaLog(SqlConnection cnConexion, string ParaGidenpac, string ParaItiposer, int ParaSociedad, string ParaGprestac, int ParaCantidad, string ParaDestino, string ParaResultado, string ParaMotivo, string ParaFirmaAuto, string ParaNumAuto, string ParaNumOpera, string ParaTipoLlamada)
		{

			try
			{

				string stSql = String.Empty;
				DataSet RrDatos = null;

				stSql = "SELECT * FROM DLOGAUTO WHERE 1 = 2";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, cnConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				RrDatos.AddNew();

				RrDatos.Tables[0].Rows[0]["gidenpac"] = ParaGidenpac;

				if (ParaItiposer.Trim() != "")
				{
					RrDatos.Tables[0].Rows[0]["itiposer"] = ParaItiposer.Trim();
				}

				RrDatos.Tables[0].Rows[0]["gsocieda"] = ParaSociedad;
				RrDatos.Tables[0].Rows[0]["gprestac"] = ParaGprestac;
				RrDatos.Tables[0].Rows[0]["ncantida"] = ParaCantidad;
				RrDatos.Tables[0].Rows[0]["idestino"] = ParaDestino;
				RrDatos.Tables[0].Rows[0]["iresulta"] = ParaResultado;

				if (ParaMotivo.Trim() != "")
				{
					RrDatos.Tables[0].Rows[0]["dmotivo"] = ParaMotivo.Trim();
				}
				if (ParaNumAuto.Trim() != "")
				{
					RrDatos.Tables[0].Rows[0]["dnumauto"] = ParaNumAuto.Trim();
				}
				if (ParaNumOpera.Trim() != "")
				{
					RrDatos.Tables[0].Rows[0]["dnumoper"] = ParaNumOpera.Trim();
				}

				RrDatos.Tables[0].Rows[0]["dtipllam"] = ParaTipoLlamada;

				string tempQuery = RrDatos.Tables[0].TableName;
				//SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
				SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                tempAdapter.Update(RrDatos, RrDatos.Tables[0].TableName);

				 
				RrDatos.Close();
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proGrabaLog: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		internal static void bCancelaSesionITC(SqlConnection ParacnConexion, int ParaSociedad, string ParaGidenpac, ref string ParaPrestacion, string ParaNumOperacion)
		{

			try
			{

				fCancelaAutorizacionITC(ParacnConexion, ParaSociedad, ParaGidenpac, ref ParaPrestacion, 1, "S", false, ParaNumOperacion);
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("bCancelaSesionITC: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		internal static void fCancelaAutorizacionITC(SqlConnection cnConexion, int lSociedad, string stGidenpac, ref string stPrestacion, int iCantidad, string stTipoServicio, bool bEsEstancia, string stNumAuto)
		{

			try
			{

				string stNumOpera = String.Empty;
				string stMotivoRechazo = String.Empty;

				// Comprobamos si las carpetas est�n creadas. Si no, intentamos crearlas

				if (!proCompruebaCarpetasITC())
				{
					return;
				}

				// Definimos la estructura de los ficheros. Si ya estaban definidas, aprovechamos para vaciar los datos

				proDefineEstructurasITC();

				// Enviamos los archivos

				if (bEsEstancia)
				{
					stPrestacion = stPrestacionUrgenciaITC;
				}

				if (!proGeneraEnvioITC(cnConexion, lSociedad, stGidenpac, stPrestacion, iCantidad, stTipoServicio, stNumAuto))
				{
					return;
				}

				if (proEsperaRespuestaITC(cnConexion, lSociedad))
				{

					// Hemos recibido respuesta. La procesamos

					stNumOpera = Ficheros[SVL_3].Campos[5].valor;
					stMotivoRechazo = "(" + Ficheros[SVL_4].Campos[0].valor + "): " + proInterpretaRechazo(Ficheros[SVL_4].Campos[0].valor);

				}

				// Grabamos el log de la cancelaci�n

				proGrabaLog(cnConexion, stGidenpac, stTipoServicio, lSociedad, stPrestacion, iCantidad, "I", "C", stMotivoRechazo, "", stNumAuto, stNumOpera, "CA");
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("fCancelaAutorizacionITC: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		internal static void fCancelaAutorizacionTraditum(SqlConnection cnConexion, int lSociedad, string stGidenpac, ref string stPrestacion, int iCantidad, string stTipoServicio, int lServicio, bool bEsEstancia, string stNumAuto)
		{

			try
			{

				string stNumOpera = String.Empty;
				string stMotivoRechazo = String.Empty;

				// Comprobamos si las carpetas est�n creadas. Si no, intentamos crearlas

				if (!proCompruebaCarpetasTraditum())
				{
					return;
				}

				// Definimos la estructura de los ficheros. Si ya estaban definidas, aprovechamos para vaciar los datos

				proDefineEstructurasTraditum();

				// Enviamos los archivos

				if (bEsEstancia)
				{
					stPrestacion = stPrestacionUrgenciaTraditum;
				}

				if (!proGeneraEnvioTraditum(cnConexion, lSociedad, stGidenpac, stPrestacion, iCantidad, stTipoServicio, lServicio, stNumAuto))
				{
					return;
				}

				// Esperamos la respuesta

				if (proEsperaRespuestaTraditum(cnConexion, lSociedad))
				{

					// Hemos recibido respuesta. La procesamos

					stNumOpera = Ficheros[HDR].Campos[20].valor;

					// Vemos si hubo alg�n error en la transmisi�n o en el mensaje

					if (!Ficheros[HDR].Campos[21].valor.StartsWith("B"))
					{

						stMotivoRechazo = "(" + Ficheros[HDR].Campos[21].valor + "): " + Ficheros[HDR].Campos[22].valor.Trim();

						if (Ficheros[HDR].Campos[23].valor.Trim() != "")
						{

							stMotivoRechazo = stMotivoRechazo + ". " + Ficheros[HDR].Campos[23].valor.Trim();

						}

					}

				}
				else
				{

					stNumOpera = "";
					stMotivoRechazo = "Time-Out";

				}

				// Grabamos el log de la cancelaci�n

				proGrabaLog(cnConexion, stGidenpac, stTipoServicio, lSociedad, stPrestacion, iCantidad, "T", "C", stMotivoRechazo, "", stNumAuto, stNumOpera, "CA");
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("fCancelaAutorizacionTraditum: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		private static bool proEsperaRespuestaTraditum(SqlConnection cnConexion, int lSociedad)
		{

			try
			{

				// Esperamos a recibir los dos ficheros de respuesta

				float Inicio = 0;

				Inicio = (float) DateTime.Now.TimeOfDay.TotalSeconds;


				while(DateTime.Now.TimeOfDay.TotalSeconds < Inicio + iTiempoEsperaTraditum)
				{

					// Comprobamos si hay respuesta en el directorio

					if (FileSystem.Dir(stPathTraditumDownload + Ficheros[HDR].Nombre, FileAttribute.Archive) != "" && FileSystem.Dir(stPathTraditumDownload + Ficheros[DT1].Nombre, FileAttribute.Archive) != "" && (FileSystem.Dir(stPathTraditumDownload + Ficheros[DT2].Nombre, FileAttribute.Archive) != "" || FileSystem.Dir(stPathTraditumDownload + Ficheros[DT3].Nombre, FileAttribute.Archive) != ""))
					{

						// Ya tenemos los tres ficheros. Los procesamos

						proProcesaRespuesta(stPathTraditumDownload, HDR);
						proProcesaRespuesta(stPathTraditumDownload, DT1);

						if (FileSystem.Dir(stPathTraditumDownload + Ficheros[DT2].Nombre, FileAttribute.Archive) != "")
						{
							proProcesaRespuesta(stPathTraditumDownload, DT2);
						}

						if (FileSystem.Dir(stPathTraditumDownload + Ficheros[DT3].Nombre, FileAttribute.Archive) != "")
						{
							proProcesaRespuesta(stPathTraditumDownload, DT3);
						}


						return true;

					}

					Application.DoEvents();

				};


				return false;
			}
			catch (System.Exception excep)
			{

				MessageBox.Show("proEsperaRespuestaTraditum: " + excep.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}
	}
}