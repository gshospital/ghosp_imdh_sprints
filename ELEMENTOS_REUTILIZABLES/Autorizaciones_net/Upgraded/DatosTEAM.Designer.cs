using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Autorizaciones
{
	partial class DatosTEAM
	{

		#region "Upgrade Support "
		private static DatosTEAM m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DatosTEAM DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DatosTEAM();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprSolicitudes", "cbAceptar", "cbCancelar", "Label14", "LbFinanciadora", "lbHistoria", "Label28", "lbPaciente", "Label11", "Frame2", "sprSolicitudes_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprSolicitudes;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadLabel Label14;
		public Telerik.WinControls.UI.RadTextBox LbFinanciadora;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadLabel Label11;
		public Telerik.WinControls.UI.RadGroupBox Frame2;
		//private FarPoint.Win.Spread.SheetView sprSolicitudes_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DatosTEAM));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.sprSolicitudes = new UpgradeHelpers.Spread.FpSpread();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
			this.Label14 = new Telerik.WinControls.UI.RadLabel();
			this.LbFinanciadora = new Telerik.WinControls.UI.RadTextBox();
			this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.Label28 = new Telerik.WinControls.UI.RadLabel();
			this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label11 = new Telerik.WinControls.UI.RadLabel();
			this.Frame2.SuspendLayout();
			this.SuspendLayout();
			// 
			// sprSolicitudes
			// 
			this.sprSolicitudes.Location = new System.Drawing.Point(2, 68);
			this.sprSolicitudes.Name = "sprSolicitudes";
			this.sprSolicitudes.Size = new System.Drawing.Size(691, 219);
			this.sprSolicitudes.TabIndex = 0;
			//this.sprSolicitudes.ButtonClicked += new FarPoint.Win.Spread.EditorNotifyEventHandler(this.sprSolicitudes_ButtonClicked);
            this.sprSolicitudes.CommandCellClick  += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.sprSolicitudes_ButtonClicked);
            //sdsalazar no se sabe por cual cambiar
            //this.sprSolicitudes.LeaveCell += new UpgradeHelpers.Spread.FpSpread.LeaveCellEventHandler(this.sprSolicitudes_LeaveCell);
            this.sprSolicitudes.CurrentCellChanged  += new Telerik.WinControls.UI.CurrentCellChangedEventHandler(this.sprSolicitudes_LeaveCell);
            // 
            // cbAceptar
            // 
            //this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAceptar.Location = new System.Drawing.Point(494, 296);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(95, 29);
			this.cbAceptar.TabIndex = 7;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbAceptar.UseVisualStyleBackColor = false;
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            this.cbAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cbCancelar
			// 
			//this.cbCancelar.BackColor = System.Drawing.SystemColors.Control;
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCancelar.Location = new System.Drawing.Point(598, 296);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(95, 29);
			this.cbCancelar.TabIndex = 6;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbCancelar.UseVisualStyleBackColor = false;
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            this.cbCancelar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame2
            // 
            //this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            this.Frame2.Controls.Add(this.Label14);
			this.Frame2.Controls.Add(this.LbFinanciadora);
			this.Frame2.Controls.Add(this.lbHistoria);
			this.Frame2.Controls.Add(this.Label28);
			this.Frame2.Controls.Add(this.lbPaciente);
			this.Frame2.Controls.Add(this.Label11);
			this.Frame2.Enabled = true;
			//this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame2.Location = new System.Drawing.Point(0, 0);
			this.Frame2.Name = "Frame2";
			this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame2.Size = new System.Drawing.Size(769, 66);
			this.Frame2.TabIndex = 1;
			this.Frame2.Text = "Datos del Paciente";
			this.Frame2.Visible = true;
			// 
			// Label14
			// 
			//this.Label14.BackColor = System.Drawing.SystemColors.Control;
			//this.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label14.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label14.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label14.Location = new System.Drawing.Point(8, 40);
			this.Label14.Name = "Label14";
			this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label14.Size = new System.Drawing.Size(91, 17);
			this.Label14.TabIndex = 9;
			this.Label14.Text = "Ent. Financiadora:";
			// 
			// LbFinanciadora
			// 
			//this.LbFinanciadora.BackColor = System.Drawing.SystemColors.Control;
			//this.LbFinanciadora.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.LbFinanciadora.Cursor = System.Windows.Forms.Cursors.Default;
			//this.LbFinanciadora.ForeColor = System.Drawing.SystemColors.ControlText;
			this.LbFinanciadora.Location = new System.Drawing.Point(108, 38);
			this.LbFinanciadora.Name = "LbFinanciadora";
			this.LbFinanciadora.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LbFinanciadora.Size = new System.Drawing.Size(401, 19);
			this.LbFinanciadora.TabIndex = 8;
            this.LbFinanciadora.Enabled = false;
			// 
			// lbHistoria
			// 
			//this.lbHistoria.BackColor = System.Drawing.SystemColors.Control;
			//this.lbHistoria.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbHistoria.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbHistoria.Location = new System.Drawing.Point(596, 16);
			this.lbHistoria.Name = "lbHistoria";
			this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbHistoria.Size = new System.Drawing.Size(97, 17);
			this.lbHistoria.TabIndex = 5;
            this.lbHistoria.Enabled = false;
            // 
            // Label28
            // 
            //this.Label28.BackColor = System.Drawing.SystemColors.Control;
            //this.Label28.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label28.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label28.Location = new System.Drawing.Point(8, 16);
			this.Label28.Name = "Label28";
			this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label28.Size = new System.Drawing.Size(59, 17);
			this.Label28.TabIndex = 4;
			this.Label28.Text = "Paciente:";
			// 
			// lbPaciente
			// 
			//this.lbPaciente.BackColor = System.Drawing.SystemColors.Control;
			//this.lbPaciente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbPaciente.Location = new System.Drawing.Point(108, 16);
			this.lbPaciente.Name = "lbPaciente";
			this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbPaciente.Size = new System.Drawing.Size(401, 19);
			this.lbPaciente.TabIndex = 3;
            this.lbPaciente.Enabled = false;
            // 
            // Label11
            // 
            //this.Label11.BackColor = System.Drawing.SystemColors.Control;
            //this.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label11.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label11.Location = new System.Drawing.Point(516, 16);
			this.Label11.Name = "Label11";
			this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label11.Size = new System.Drawing.Size(75, 17);
			this.Label11.TabIndex = 2;
			this.Label11.Text = "Historia cl�nica:";
			// 
			// DatosTEAM
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(696, 329);
			this.Controls.Add(this.sprSolicitudes);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.Frame2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DatosTEAM";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.Activated += new System.EventHandler(this.DatosTEAM_Activated);
			this.Closed += new System.EventHandler(this.DatosTEAM_Closed);
			this.Load += new System.EventHandler(this.DatosTEAM_Load);
			this.Frame2.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}