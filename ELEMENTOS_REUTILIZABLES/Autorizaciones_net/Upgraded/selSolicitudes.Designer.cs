using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace Autorizaciones
{
	partial class Seleccionar_Solicitud
	{

		#region "Upgrade Support "
		private static Seleccionar_Solicitud m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Seleccionar_Solicitud DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Seleccionar_Solicitud();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprSolicitudes", "Label4", "Line4", "Label5", "Line5", "Label6", "Line6", "Frame4", "cbAceptar", "cbCancelar", "ShapeContainer2", "sprSolicitudes_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprSolicitudes;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line4;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line5;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line6;
		public System.Windows.Forms.Panel Frame4;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
		//private FarPoint.Win.Spread.SheetView sprSolicitudes_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Seleccionar_Solicitud));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.sprSolicitudes = new UpgradeHelpers.Spread.FpSpread();
			this.Frame4 = new System.Windows.Forms.Panel();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.Line4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.Line5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.Line6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.Frame4.SuspendLayout();
			this.SuspendLayout();
			// 
			// ShapeContainer2
			// 
			this.ShapeContainer2.Location = new System.Drawing.Point(3, 16);
			this.ShapeContainer2.Size = new System.Drawing.Size(273, 57);
			this.ShapeContainer2.Shapes.Add(Line4);
			this.ShapeContainer2.Shapes.Add(Line5);
			this.ShapeContainer2.Shapes.Add(Line6);
			// 
			// sprSolicitudes
			// 
			this.sprSolicitudes.Location = new System.Drawing.Point(6, 2);
			this.sprSolicitudes.Name = "sprSolicitudes";
			this.sprSolicitudes.Size = new System.Drawing.Size(705, 257);
			this.sprSolicitudes.TabIndex = 0;
			this.sprSolicitudes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprSolicitudes_CellClick);
			// 
			// Frame4
			// 
			//this.Frame4.BackColor = System.Drawing.SystemColors.Control;
			this.Frame4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Frame4.Controls.Add(this.Label4);
			this.Frame4.Controls.Add(this.Label5);
			this.Frame4.Controls.Add(this.Label6);
			this.Frame4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Frame4.Enabled = true;
			//this.Frame4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame4.Location = new System.Drawing.Point(8, 262);
			this.Frame4.Name = "Frame4";
			this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame4.Size = new System.Drawing.Size(273, 57);
			this.Frame4.TabIndex = 3;
			this.Frame4.Visible = true;
			// 
			// Label4
			// 
			//this.Label4.BackColor = System.Drawing.SystemColors.Control;
			//this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label4.Location = new System.Drawing.Point(68, 20);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(127, 19);
			this.Label4.TabIndex = 6;
			this.Label4.Text = "Solicitudes en curso";
			// 
			// Line4
			// 
			this.Line4.BorderColor = System.Drawing.Color.Blue;
			this.Line4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line4.BorderWidth = 4;
			this.Line4.Enabled = false;
			this.Line4.Name = "Line4";
			this.Line4.Visible = true;
			this.Line4.X1 = (int) 6;
			this.Line4.X2 = (int) 54;
			this.Line4.Y1 = (int) 27;
			this.Line4.Y2 = (int) 27;
			// 
			// Label5
			// 
			//this.Label5.BackColor = System.Drawing.SystemColors.Control;
			//this.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label5.Location = new System.Drawing.Point(68, 2);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(193, 19);
			this.Label5.TabIndex = 5;
			this.Label5.Text = "Solicitudes pendientes de tramitar";
			// 
			// Line5
			// 
			this.Line5.BorderColor = System.Drawing.Color.Black;
			this.Line5.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line5.BorderWidth = 4;
			this.Line5.Enabled = false;
			this.Line5.Name = "Line5";
			this.Line5.Visible = true;
			this.Line5.X1 = (int) 6;
			this.Line5.X2 = (int) 54;
			this.Line5.Y1 = (int) 10;
			this.Line5.Y2 = (int) 10;
			// 
			// Label6
			// 
			//this.Label6.BackColor = System.Drawing.SystemColors.Control;
			//this.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label6.Location = new System.Drawing.Point(68, 37);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(127, 19);
			this.Label6.TabIndex = 4;
			this.Label6.Text = "Solicitudes Autorizadas";
			// 
			// Line6
			// 
			this.Line6.BorderColor = System.Drawing.Color.FromArgb(0, 192, 0);
			this.Line6.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line6.BorderWidth = 4;
			this.Line6.Enabled = false;
			this.Line6.Name = "Line6";
			this.Line6.Visible = true;
			this.Line6.X1 = (int) 6;
			this.Line6.X2 = (int) 54;
			this.Line6.Y1 = (int) 44;
			this.Line6.Y2 = (int) 44;
			// 
			// cbAceptar
			// 
			//this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbAceptar.Location = new System.Drawing.Point(514, 286);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(95, 29);
			this.cbAceptar.TabIndex = 2;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbAceptar.UseVisualStyleBackColor = false;
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            this.cbAceptar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cbCancelar
            // 
            //this.cbCancelar.BackColor = System.Drawing.SystemColors.Control;
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCancelar.Location = new System.Drawing.Point(616, 286);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(95, 29);
			this.cbCancelar.TabIndex = 1;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbCancelar.UseVisualStyleBackColor = false;
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            this.cbCancelar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Seleccionar_Solicitud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(716, 320);
			this.ControlBox = false;
			this.Controls.Add(this.sprSolicitudes);
			this.Controls.Add(this.Frame4);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCancelar);
			this.Frame4.Controls.Add(ShapeContainer2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Seleccionar_Solicitud";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Selecci�n de Solicitudes - AUT003F1";
			this.Activated += new System.EventHandler(this.Seleccionar_Solicitud_Activated);
			this.Closed += new System.EventHandler(this.Seleccionar_Solicitud_Closed);
			this.Load += new System.EventHandler(this.Seleccionar_Solicitud_Load);
			this.Frame4.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}