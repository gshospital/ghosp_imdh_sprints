using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Microsoft.CSharp;
using Telerik.WinControls;

namespace ConfirmacionTomas
{
	public class clsConfirmacionTomas
	{


		bool fValidar = false;


		//(maplaza)(18/09/2006)Se a�ade un par�metro a pasar a la dll, que es la fecha de fin
		//de movimiento del apunte (GstFechaFinMovimiento), para saber si el apunte es
		//cerrado (GstFechaFinMovimiento = "") o no (GstFechaFinMovimiento<>"")

		//*  O.Frias ( 06/11/2006 ) Realizo el control de llamada desde glucemias, SOLO PARA LA PETICI�N DE USUARIO
		public void Llamada(dynamic nForm, SqlConnection nConnet, string stUsuario, string PathAplicacion, string Tipo, string Episodio = "", string stCodigoFarmaco = "", string stFechaApunte = "", string stConfirmacionMinima = "", string stConfirmacionMaxima = "", string stFechaFinMovimiento = "", bool blnGlucemias = false, bool bDesdeBloqueo = false)
		{

			string StSql = String.Empty;
			DataSet RrValidar = null;

			// Almacenamos los valores recibidos por par�metro

			mbConfirmacionTomas.FormularioOrigen = nForm;
			mbConfirmacionTomas.RcEnfermeria = nConnet;
			mbConfirmacionTomas.gUsuario = stUsuario;
			mbConfirmacionTomas.stPathAplicacion = PathAplicacion;
			mbConfirmacionTomas.stTipo = Tipo.Trim().ToUpper();
			mbConfirmacionTomas.DesdeBloqueo = bDesdeBloqueo;

			if (Tipo != "VALIDACION")
			{

				mbConfirmacionTomas.GstTiposervicio = Episodio.Substring(0, Math.Min(1, Episodio.Length));
				mbConfirmacionTomas.GstA�oFarmaco = Episodio.Substring(1, Math.Min(4, Episodio.Length - 1));
				mbConfirmacionTomas.GstNumeroFarmaco = Episodio.Substring(5);

				mbConfirmacionTomas.GstFechaApunte = stFechaApunte.Trim();
				mbConfirmacionTomas.GstCodigoFarmaco = stCodigoFarmaco.Trim();

				//(maplaza)(18/09/2006)Se obtiene la fecha de fin de movimiento, para saber si el apunte est�
				//cerrado o no
				mbConfirmacionTomas.vstFechaFinMov = stFechaFinMovimiento;
				//-----

				mbConfirmacionTomas.GstConfirmaMaxima = stConfirmacionMaxima;
				mbConfirmacionTomas.GstConfirmaMinima = stConfirmacionMinima;

				//mbConfirmacionTomas.vstFechaHoraSis = DateTime.Now.ToString("dd/MM/yyyy HH:NN:SS");
                mbConfirmacionTomas.vstFechaHoraSis = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

				// Obtenemos el formato de fecha de la base de datos

				Serrores.Obtener_TipoBD_FormatoFechaBD(ref mbConfirmacionTomas.RcEnfermeria);
				mbConfirmacionTomas.vstSeparadorDecimal = Serrores.ObtenerSeparadorDecimal(mbConfirmacionTomas.RcEnfermeria);
				mbConfirmacionTomas.stDecimalBD = Serrores.ObtenerSeparadorDecimalBD(mbConfirmacionTomas.RcEnfermeria);
				mbConfirmacionTomas.vstValorNEQUIVAL = mbConfirmacionTomas.ObtenerValorNEQUIVAL();

				Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(mbConfirmacionTomas.RcEnfermeria);

				Serrores.vAyuda = "";

			}

			// Vemos si es necesario validar. Si hay bloqueo de sesi�n, siempre ser� necesario

			if (!mbConfirmacionTomas.DesdeBloqueo)
			{
				StSql = "select isNull(valfanu1,'S') valfanu1 from SCONSGLO where gconsglo = 'ICONFUSU'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, mbConfirmacionTomas.RcEnfermeria);
				RrValidar = new DataSet();
				tempAdapter.Fill(RrValidar);

				if (RrValidar.Tables[0].Rows.Count == 0)
				{
					fValidar = true;
				}
				else
				{
					fValidar = Convert.ToString(RrValidar.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S";
				}

				RrValidar.Close();

			}
			else
			{
				fValidar = true;
			}

			// En funci�n del tipo hacemos una acci�n u otra

			if (fValidar)
			{
				AccesoTomas.DefInstance.ShowDialog();

				//*********************** O.Frias (06/11/2006) ***********************
				if (!blnGlucemias)                
				{
					mbConfirmacionTomas.FormularioOrigen.ObtenerAcceso(mbConfirmacionTomas.UsuarioConfirmacion);
				}
				else
				{
					mbConfirmacionTomas.FormularioOrigen.Tag = (mbConfirmacionTomas.fUsuarioValidado) ? "" : "Salir";
				}
				//*********************** O.Frias (06/11/2006) ***********************

				if (!mbConfirmacionTomas.fUsuarioValidado)
				{
					return;
				}
				if (blnGlucemias)
				{
					mbConfirmacionTomas.FormularioOrigen.ObtenerAcceso(mbConfirmacionTomas.UsuarioConfirmacion.Trim());
					return;
				}
			}
			else
			{
				if (blnGlucemias)
				{
					return;
				}
				mbConfirmacionTomas.UsuarioConfirmacion = mbConfirmacionTomas.gUsuario;

				mbConfirmacionTomas.FormularioOrigen.ObtenerAcceso(mbConfirmacionTomas.UsuarioConfirmacion);

				mbConfirmacionTomas.fUsuarioValidado = true;

			}


			switch(mbConfirmacionTomas.stTipo)
			{
				case "FARMACO" : 					 
					// Nos traemos los datos del f�rmaco 					 
					mbConfirmacionTomas.CargarDatosFarmaco();

                    //DGMORENOG_NOTODO_X_1: Solo se hace cuando el fortmulario origen tiene esa variable ProtegerCeldas
                    //Para las pruebas desde IMH00F2 y IMH00F3 no lo tienen por lo tanto se comenta
					//mbConfirmacionTomas.gfProtegerCeldas = Convert.ToBoolean(nForm.protegerCeldas);
					                     
					ConfirmarFarmacos.DefInstance.ShowDialog(); 					 
					break;
				case "PERFUSION" :
					// Nos traemos los datos de la perfusi�n
					mbConfirmacionTomas.CargarDatosPerfusion();
					ConfirmarPerfusiones.DefInstance.ShowDialog();
					break;
				case "VALIDACION" : 
					 
					return; 

				default:
					 
					short tempRefParam = 1100; 
					string[] tempRefParam2 = new string[]{"Tipo de acceso (" + mbConfirmacionTomas.stTipo + ")"};//new object{"Tipo de acceso (" + mbConfirmacionTomas.stTipo + ")"}; 
					mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbConfirmacionTomas.RcEnfermeria, tempRefParam2); 
					 
					break;
			}

		}

		public clsConfirmacionTomas()
		{
			try
			{

				mbConfirmacionTomas.clase_mensaje = new Mensajes.ClassMensajes();
			}
			catch
			{

				RadMessageBox.Show("Smensajes.dll no encontrada", Application.ProductName);
			}

		}

		~clsConfirmacionTomas()
		{

			mbConfirmacionTomas.clase_mensaje = null;

		}

		public string LlamadaValidacion(SqlConnection nConnet)
		{

			mbConfirmacionTomas.RcEnfermeria = nConnet;

			//    Load ACCESO_TOMAS
			//    ACCESO_TOMAS.OrigenEnfermeria = True
			//    ACCESO_TOMAS.Caption = "Confirmaci�n de acceso"
			//    ACCESO_TOMAS.Show 1
			//
			//    LlamadaValidacion = UsuarioConfirmacion

			return String.Empty;
		}
	}
}