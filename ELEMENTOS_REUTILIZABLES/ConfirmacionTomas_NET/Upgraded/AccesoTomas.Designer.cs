using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ConfirmacionTomas
{
	partial class AccesoTomas
	{

		#region "Upgrade Support "
		private static AccesoTomas m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static AccesoTomas DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new AccesoTomas();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "tbUsuario", "tbContraseña", "cbAceptar", "cbCancelar", "lbDescripcion", "lbUsuario", "lbContraseña"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
        public Telerik.WinControls.UI.RadTextBoxControl tbUsuario;
        public Telerik.WinControls.UI.RadTextBoxControl tbContraseña;
        public Telerik.WinControls.UI.RadButton cbAceptar;
        public Telerik.WinControls.UI.RadButton cbCancelar;
        public Telerik.WinControls.UI.RadLabel lbUsuario;
        public Telerik.WinControls.UI.RadLabel lbContraseña;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.tbUsuario = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbContraseña = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.lbUsuario = new Telerik.WinControls.UI.RadLabel();
            this.lbContraseña = new Telerik.WinControls.UI.RadLabel();
            this.lbDescripcion = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tbUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbContraseña)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tbUsuario
            // 
            this.tbUsuario.AcceptsReturn = true;
            this.tbUsuario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbUsuario.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tbUsuario.Location = new System.Drawing.Point(146, 60);
            this.tbUsuario.MaxLength = 20;
            this.tbUsuario.Name = "tbUsuario";
            this.tbUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbUsuario.Size = new System.Drawing.Size(143, 20);
            this.tbUsuario.TabIndex = 0;
            this.tbUsuario.Enter += new System.EventHandler(this.tbUsuario_Enter);
            this.tbUsuario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbUsuario_KeyPress);
            // 
            // tbContraseña
            // 
            this.tbContraseña.AcceptsReturn = true;
            this.tbContraseña.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbContraseña.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tbContraseña.Location = new System.Drawing.Point(146, 88);
            this.tbContraseña.MaxLength = 8;
            this.tbContraseña.Name = "tbContraseña";
            this.tbContraseña.PasswordChar = '*';
            this.tbContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbContraseña.Size = new System.Drawing.Size(143, 20);
            this.tbContraseña.TabIndex = 1;
            this.tbContraseña.Enter += new System.EventHandler(this.tbContraseña_Enter);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(146, 138);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 2;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(234, 138);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 3;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // lbUsuario
            // 
            this.lbUsuario.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbUsuario.Location = new System.Drawing.Point(60, 64);
            this.lbUsuario.Name = "lbUsuario";
            this.lbUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUsuario.Size = new System.Drawing.Size(50, 18);
            this.lbUsuario.TabIndex = 5;
            this.lbUsuario.Text = "Usuario :";
            // 
            // lbContraseña
            // 
            this.lbContraseña.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbContraseña.Location = new System.Drawing.Point(60, 92);
            this.lbContraseña.Name = "lbContraseña";
            this.lbContraseña.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbContraseña.Size = new System.Drawing.Size(68, 18);
            this.lbContraseña.TabIndex = 4;
            this.lbContraseña.Text = "Contraseña :";
            // 
            // lbDescripcion
            // 
            this.lbDescripcion.AutoSize = false;
            this.lbDescripcion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDescripcion.Location = new System.Drawing.Point(60, 12);
            this.lbDescripcion.Name = "lbDescripcion";
            this.lbDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDescripcion.Size = new System.Drawing.Size(193, 35);
            this.lbDescripcion.TabIndex = 6;
            this.lbDescripcion.Text = "Escriba un código de Usuario y una Contraseña válidos para este Sistema ";
            // 
            // AccesoTomas
            // 
            this.AcceptButton = this.cbAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 180);
            this.ControlBox = false;
            this.Controls.Add(this.lbDescripcion);
            this.Controls.Add(this.tbUsuario);
            this.Controls.Add(this.tbContraseña);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.lbUsuario);
            this.Controls.Add(this.lbContraseña);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AccesoTomas";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Acceso Confirmación";
            this.Closed += new System.EventHandler(this.AccesoTomas_Closed);
            this.Load += new System.EventHandler(this.AccesoTomas_Load);
            this.Leave += new System.EventHandler(this.AccesoTomas_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.tbUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbContraseña)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        public Telerik.WinControls.UI.RadLabel lbDescripcion;
	}
}