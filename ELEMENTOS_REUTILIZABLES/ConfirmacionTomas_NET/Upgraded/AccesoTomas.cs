using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using System.DirectoryServices.AccountManagement;

namespace ConfirmacionTomas
{
    public partial class AccesoTomas : Telerik.WinControls.UI.RadForm
	{
		public bool fLoginSucceeded = false;
		public bool fOK = false;
		public bool fTarjeta = false;
		int iNumeroIntentos = 0;
		string stPasswEncriptada = String.Empty;
		public DataSet RsPrincipal = null;
		public bool bLargo = false;
		public AccesoTomas()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public void Usuario_largo()
		{
			string tstLargo = "select * from sconsglo where gconsglo='IUSULARG'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstLargo, mbConfirmacionTomas.RcEnfermeria);
			DataSet tRrLargo = new DataSet();
			tempAdapter.Fill(tRrLargo);

			if (tRrLargo.Tables[0].Rows.Count == 0)
			{
				bLargo = false;
			}
			else
			{				
				bLargo = Convert.ToString(tRrLargo.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S";
			}
		}
		
		private void AccesoTomas_Load(Object eventSender, EventArgs eventArgs)
		{
			mbConfirmacionTomas.UsuarioConfirmacion = "";
			tbUsuario.Text = "";
			tbContraseña.Text = "";

			Usuario_largo();

			if (!bLargo)
			{
				tbUsuario.MaxLength = 8;
			}
			else
			{
				tbUsuario.MaxLength = 20;
			}

			//(maplaza)(16/11/2006)Dependiendo del valor de "bLargo", se obtendrá el nombre de usuario largo o no.
			tbUsuario.Text = ObtenerNombreUsuario();
			//-----

			// Comprobamos el tipo de autenticación a realizar

			mbDirectorioActivo.proCompruebaAutenticacion(mbConfirmacionTomas.RcEnfermeria);

			// Ponemos el tamaño del password

			tbContraseña.MaxLength = int.Parse(mbDirectorioActivo.viTamañoPassword);

            if (mbConfirmacionTomas.DesdeBloqueo)
            {
                UpgradeSupportHelper.PInvoke.SafeNative.user32.SetWindowPos(this.Handle.ToInt32(), mbConfirmacionTomas.HWND_TOPMOST, 0, 0, 0, 0, mbConfirmacionTomas.wFlags);
            }   
		}

		public void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//Comprobar que los campos no están vacíos            
			if (tbUsuario.Text == "")
			{
				//Campo "USUARIO" requerido
				short tempRefParam = 1040;                
                string[] tempRefParam2 = new string[] {lbUsuario.Text};
                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbConfirmacionTomas.RcEnfermeria, tempRefParam2);                
				tbUsuario.Focus();
				return;
			}
			else if (tbContraseña.Text.Trim() == "")
			{ 
				//Campo "CONTRASEÑA" requerido
				short tempRefParam3 = 1040;                
                string[] tempRefParam4 = new string[] {lbContraseña.Text};
				mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbConfirmacionTomas.RcEnfermeria, tempRefParam4);
				tbContraseña.Focus();
				return;
			}

			mbConfirmacionTomas.UsuarioConfirmacion = tbUsuario.Text.Trim();

			if (mbDirectorioActivo.vstTipoAutenticacion == "M")
			{
				mbDirectorioActivo.proAutenticacionUsuario(mbConfirmacionTomas.UsuarioConfirmacion, mbConfirmacionTomas.RcEnfermeria, bLargo);
			}
			else
			{
				mbDirectorioActivo.vstAutenticacionUsuario = mbDirectorioActivo.vstTipoAutenticacion;
			}

			if (mbDirectorioActivo.vstTipoAutenticacion == "I" || mbDirectorioActivo.vstAutenticacionUsuario == "I")
			{
				string tempRefParam5 = tbContraseña.Text;
				stPasswEncriptada = mbControl.EncriptarPassword(ref tempRefParam5);
			}
			else
			{
				stPasswEncriptada = tbContraseña.Text;
			}

			//    If Trim(tbContraseña) <> "" Then
			//
			//        'Encriptamos la contraseña
			//
			//        stPasswEncriptada = EncriptarPassword(tbContraseña)
			//
			//    End If

			if (mbControl.iContador == 0)
			{
				iNumeroIntentos = mbControl.ObtenerNIntentos();

				ComprobarUsuario(mbConfirmacionTomas.UsuarioConfirmacion, stPasswEncriptada);

				if (RsPrincipal.Tables[0].Rows.Count < 1)
				{
					//Usuario no autorizado.

					short tempRefParam6 = 1240;					
                    string[] tempRefParam7 = new string[]{""};
					mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam6, mbConfirmacionTomas.RcEnfermeria, tempRefParam7);
					tbContraseña.Text = "";
					mbConfirmacionTomas.UsuarioConfirmacion = "";
					mbConfirmacionTomas.fUsuarioValidado = false;
					mbControl.iContador++;
					tbUsuario.Focus();

					return;
				}
				else
				{

					mbControl.iContador = 0; // inicializamos el contador cada vez que entre
                    					
					mbConfirmacionTomas.UsuarioConfirmacion = Convert.ToString(RsPrincipal.Tables[0].Rows[0]["gusuario"]);

					mbConfirmacionTomas.fUsuarioValidado = true;

					this.Close();
				}
                				
				RsPrincipal.Close();                
			}
			else
			{
				//de número de intentos > que 0

				ComprobarUsuario(mbConfirmacionTomas.UsuarioConfirmacion, stPasswEncriptada);

				if (RsPrincipal.Tables[0].Rows.Count < 1)
				{
					if (mbControl.iContador < iNumeroIntentos - 1)
					{
						//Usuario no autorizado.

						short tempRefParam8 = 1240;						
                        string[] tempRefParam9 = new string[] { "" }; 
						mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam8, mbConfirmacionTomas.RcEnfermeria, tempRefParam9);
						tbContraseña.Text = "";
						mbConfirmacionTomas.UsuarioConfirmacion = "";
						mbControl.iContador++;
						tbUsuario.Focus();

						return;
					}
					else
					{
						// La Aplicación "V2" se va a cerrar.

						short tempRefParam10 = 1240;						
                        string[] tempRefParam11 = new string[] { "" }; 
						mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam10, mbConfirmacionTomas.RcEnfermeria, tempRefParam11);
						mbConfirmacionTomas.UsuarioConfirmacion = "";
						mbConfirmacionTomas.fUsuarioValidado = false;

						this.Close();
					}
				}
				else
				{
					mbControl.iContador = 0; // inicializamos el contador cada vez que entre
					
					mbConfirmacionTomas.UsuarioConfirmacion = Convert.ToString(RsPrincipal.Tables[0].Rows[0]["gusuario"]);

					mbConfirmacionTomas.fUsuarioValidado = true;

					this.Close();
				}
                				
				RsPrincipal.Close();
			}
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			mbConfirmacionTomas.fUsuarioValidado = false;

			this.Close();
		}

		public object ComprobarUsuario(string stUsuario, string stContraseña)
		{
			//
			//    Dim stConsulta As String
			//
			//    If bLargo = True Then
			//        stConsulta = "select gusuario,vpassword,gusularg from susuario "
			//        stConsulta = stConsulta & "Where gusularg ='" & stUsuario & "'"
			//    Else
			//        stConsulta = "select gusuario,vpassword from susuario "
			//        stConsulta = stConsulta & "Where gusuario ='" & stUsuario & "'"
			//    End If
			//
			//    stConsulta = stConsulta & " and vpassword='" & stContraseña & "' and fborrado is null"
			//    stConsulta = stConsulta & " AND (FVALIDEZ IS NULL OR FVALIDEZ >=" & FormatFechaHMS(Now) & ")"
			//
			//    Set RsPrincipal = RcEnfermeria.OpenResultset(stConsulta, rdOpenStatic, rdConcurReadOnly)
			//

			string stConsulta = String.Empty;

			if (mbDirectorioActivo.vstTipoAutenticacion == "I" || mbDirectorioActivo.vstAutenticacionUsuario == "I")
			{
				// Si la validación es por I-MDH, seguimos igual

				if (bLargo)
				{
					stConsulta = "select gusuario,vpassword,gusularg from susuario ";
					stConsulta = stConsulta + "Where gusularg ='" + stUsuario + "'";
				}
				else
				{
					stConsulta = "select gusuario,vpassword from susuario ";
					stConsulta = stConsulta + "Where gusuario ='" + stUsuario + "'";
				}

				stConsulta = stConsulta + " and vpassword='" + stContraseña + "' and fborrado is null";
				object tempRefParam = DateTime.Now;
				stConsulta = stConsulta + " AND (FVALIDEZ IS NULL OR FVALIDEZ >=" + Serrores.FormatFechaHMS(tempRefParam) + ")";
			}
			else
			{
				// La validación es por Dominio. Intentamos validar

				//If AuthenticateUser(vstDominioAutenticacion, stUsuario, stContraseña) Then
				if (AuthCheck(stUsuario, stContraseña))
				{
					// Validación correcta. Avanzamos, comprobando siempre con el usuario largo

					object tempRefParam2 = DateTime.Now;
					stConsulta = "select gusuario,vpassword,gusularg from susuario where " + 
					             "gusularg = '" + stUsuario + "' and " + 
					             "fborrado is null and " + 
					             "(FVALIDEZ IS NULL OR FVALIDEZ >=" + Serrores.FormatFechaHMS(tempRefParam2) + ")";
				}
				else
				{
					// No se devolverá ningún resultado

					stConsulta = "select * from SUSUARIO where 1 = 2";
				}

			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stConsulta, mbConfirmacionTomas.RcEnfermeria);
			RsPrincipal = new DataSet();
			tempAdapter.Fill(RsPrincipal);

			return null;
		}

        /// <summary>
        /// Metodo que reemplazo al AuthenticateUser como optimización del acceso al Directorio Activo
        /// </summary>
        /// <param name="sUser"></param>
        /// <param name="sPassword"></param>
        /// <returns></returns>
		private bool AuthCheck(string sUser, string sPassword)
		{            
            bool result = false;

            try
            {
                using (PrincipalContext context = new PrincipalContext(ContextType.Domain))
                {
                    result = context.ValidateCredentials(sUser, sPassword);
                }

                return result;
            }
            catch
            {

                return false;
            }
		}

		//(maplaza)(16/11/2006)Dependiendo del valor de la variable "bLargo" se obtendrá el nombre largo de usuario o no.
		private string ObtenerNombreUsuario()
		{
			string result = String.Empty;
			DataSet RsTemp = null;
			string stConsulta = String.Empty;

            //DGMORENOG - se debe asignar el tipo de base de datos de lo contrario se va por debug al obtener el tipo de formato
            Serrores.Obtener_TipoBD_FormatoFechaBD(ref mbConfirmacionTomas.RcEnfermeria);

			if (bLargo)
			{
				object tempRefParam = DateTime.Now;
				stConsulta = " SELECT gusularg FROM susuario " + 
				             " WHERE  gusuario ='" + mbConfirmacionTomas.gUsuario + "' " + 
				             "   AND  fborrado IS NULL " + 
				             "   AND (fvalidez IS NULL OR fvalidez >=" + Serrores.FormatFechaHMS(tempRefParam) + ")";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stConsulta, mbConfirmacionTomas.RcEnfermeria);
				RsTemp = new DataSet();
				tempAdapter.Fill(RsTemp);
				if (RsTemp.Tables[0].Rows.Count != 0)
				{					
					result = Convert.ToString(RsTemp.Tables[0].Rows[0]["gusularg"]).Trim();
				}
				else
				{
					result = "";
				}				
				RsTemp.Close();
				RsTemp = null;
			}
			else
			{
				result = mbConfirmacionTomas.gUsuario.Trim();
			}

			return result;
		}

		private void AccesoTomas_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (mbConfirmacionTomas.DesdeBloqueo)
			{
				this.Activate();
				UpgradeSupportHelper.PInvoke.SafeNative.user32.SetWindowPos(this.Handle.ToInt32(), mbConfirmacionTomas.HWND_TOPMOST, 0, 0, 0, 0, mbConfirmacionTomas.wFlags);
			}
		}

		private void AccesoTomas_Closed(Object eventSender, EventArgs eventArgs)
		{
			if (mbConfirmacionTomas.DesdeBloqueo)
			{
                UpgradeSupportHelper.PInvoke.SafeNative.user32.SetWindowPos(this.Handle.ToInt32(), mbConfirmacionTomas.HWND_NOTOPMOST, 0, 0, 0, 0, mbConfirmacionTomas.wFlags);                
			}
		}

		private void tbContraseña_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbContraseña.SelectionStart = 0;
			tbContraseña.SelectionLength = Strings.Len(tbContraseña.Text);
		}

		private void tbUsuario_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbUsuario.SelectionStart = 0;
			tbUsuario.SelectionLength = Strings.Len(tbUsuario.Text);
		}

		private void tbUsuario_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			if (KeyAscii == 39)
			{				
				KeyAscii = Serrores.SustituirComilla();
			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}          
	}
}