using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace ConfirmacionTomas
{
    public partial class ConfirmarFarmacos
        : Telerik.WinControls.UI.RadForm
    {

        string stUsuarioRegistro = String.Empty;
        int lTomasSeleccionado = 0; //controla el registro seleccionado
        bool fHayFilaSeleccionada = false; //true si hay alguna fila seleccionada
        int iApunteToma = 0;
        bool iModificarDosis = false;
        bool iFarmacoEquivalente = false;
        //(maplaza)(16/11/2006)Ser� true cuando (motaus <> null)
        bool vfAusencia = false;
        //-----

        //(maplaza)(05/07/2006)Constante para el n�mero de columna del spread que tiene la fecha/hora real de la toma
        private const int COL_FECHA_HORA_REAL = 3;

        const int NUEVATOMA = 6;
        const int CONFIRMATOMA = 7;
        string iMedidaFraccionable = String.Empty;
        string iEnvaseFraccionable = String.Empty;
        string stUnidadEquivalencia = String.Empty;
        float SngCantidadPorEnvase = 0;

        public ConfirmarFarmacos()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
        }

        //(maplaza)(17/11/2006)Esta funci�n dice si el episodio de Hospitalizaci�n pasado a la dll tiene motivo de ausencia (True) o no.
        private bool ConMotivoDeAusencia()
        {

            bool result = false;
            string SqlSt = String.Empty;
            DataSet RrSqlSt = null;

            if (mbConfirmacionTomas.GstTiposervicio == "H")
            {
                SqlSt = "Select gmotause from aepisadm where ganoadme = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumadme = " + mbConfirmacionTomas.GstNumeroFarmaco;
                SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlSt, mbConfirmacionTomas.RcEnfermeria);
                RrSqlSt = new DataSet();
                tempAdapter.Fill(RrSqlSt);
                if (RrSqlSt.Tables[0].Rows.Count != 0)
                {                                        
                    if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["gmotause"]))
                    {
                        result = true;
                    }
                }                
            }

            return result;
        }

        private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
        {
            string FechaMenor = String.Empty; //para la validacion de fecha anterior y fecha posterior
            string FechaMayor = String.Empty;
            string stFechaAux = String.Empty;
            string stFechaAuxPrevista = String.Empty;

            try
            {
                //'' Comprobar si el paciente ya ha sido dado de alta en cualquier modulo  por otro usuario
                string SqlSt = String.Empty;
                DataSet RrSqlSt = null;
                switch (mbConfirmacionTomas.GstTiposervicio)
                {
                    case "U":
                        SqlSt = "Select faltaurg from uepisurg where ganourge = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumurge = " + mbConfirmacionTomas.GstNumeroFarmaco;
                        SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlSt, mbConfirmacionTomas.RcEnfermeria);
                        RrSqlSt = new DataSet();
                        tempAdapter.Fill(RrSqlSt);
                        if (RrSqlSt.Tables[0].Rows.Count != 0)
                        {                            
                            if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["faltaurg"]))
                            {
                                RadMessageBox.Show("Paciente de alta en urgencias", Application.ProductName);                                                                
                                return;
                            }
                        }                        
                        break;
                    case "H":
                        SqlSt = "Select faltplan from aepisadm where ganoadme = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumadme = " + mbConfirmacionTomas.GstNumeroFarmaco;
                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SqlSt, mbConfirmacionTomas.RcEnfermeria);
                        RrSqlSt = new DataSet();
                        tempAdapter_2.Fill(RrSqlSt);
                        if (RrSqlSt.Tables[0].Rows.Count != 0)
                        {                            
                            if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["faltplan"]))
                            {
                                RadMessageBox.Show("Paciente de alta en Hospitalizaci�n", Application.ProductName);                                
                                return;
                            }
                        }                        
                        break;
                }
                //'''''''''''''''''''''''''''''''''

                //Validaciones del aceptar
                switch (iApunteToma)
                {
                    case NUEVATOMA:
                        if (tbDosis.Text.Trim() == "")
                        {
                            //tiene que estar rellena la dosis
                            short tempRefParam = 1040;
                            string[] tempRefParam2 = new string[] { lbDDosis.Text };
                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbConfirmacionTomas.RcEnfermeria, tempRefParam2);
                            tbDosis.Focus();
                            return;
                        }
                        else
                        {
                            //hay que comprobar que la dosis sea mayor que 0
                            if (Double.Parse(tbDosis.Text) <= 0)
                            {
                                short tempRefParam3 = 1020;
                                string[] tempRefParam4 = new string[] { lbDDosis.Text, "mayor", "0" };
                                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbConfirmacionTomas.RcEnfermeria, tempRefParam4);
                                tbDosis.Focus();
                                return;
                            }
                        }
                        //es obligatorio que lleve una fecha prevista 
                        string tempRefParam5 = "Fecha Prevista ";
                        if (mbConfirmacionTomas.ValidacionFecha(SdcFechaPrevista, tbHoraPrevista, ref tempRefParam5))
                        {
                            SdcFechaPrevista.Focus();
                            return;
                        }
                        //es obligatorio que lleve una fecha real 
                        string tempRefParam6 = "Fecha real ";
                        if (mbConfirmacionTomas.ValidacionFecha(sdcFecha, tbHora, ref tempRefParam6))
                        {
                            SdcFechaPrevista.Focus();
                            return;
                        }

                        string auxVar = String.Empty;
                        if (iFarmacoEquivalente)
                        {
                            if (!lbFarmacoAdm.Visible)
                            {
                                bool tempBool = false;
                                auxVar = (Convert.ToDouble(CbbFarmacoAdm.get_ListIndex()) == -1).ToString().Trim();
                                if ((Boolean.TryParse(auxVar, out tempBool)) ? tempBool : Convert.ToBoolean(Double.Parse(auxVar)))
                                {
                                    //tiene que estar rellena la dosis
                                    short tempRefParam7 = 1040;
                                    string[] tempRefParam8 = new string[] { lbfarmacoEqui.Text };
                                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam7, mbConfirmacionTomas.RcEnfermeria, tempRefParam8);
                                    CbbFarmacoAdm.Focus();
                                    return;
                                }
                            }
                        }

                        break;
                    default:
                        //si es una toma que ya existe lleva otras validaciones 
                        if (stUsuarioRegistro.Trim() != "")
                        {
                            //si el registro ya existe en etomasfa solo le permito modificar las observaciones
                            //cuando el usuario que va confirmar es el mismo que las grabo
                            if (stUsuarioRegistro.Trim().ToUpper() == mbConfirmacionTomas.UsuarioConfirmacion.Trim().ToUpper())
                            {
                                if (sdcFecha.Text == "" && tbObservaciones.Text.Trim() == "")
                                {
                                    short tempRefParam9 = 1040;
                                    string[] tempRefParam10 = new string[] { "Observaciones" };
                                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam9, mbConfirmacionTomas.RcEnfermeria, tempRefParam10);
                                    tbObservaciones.Focus();
                                    return;
                                }
                            }
                        }
                        else
                        {
                            //no existe la toma seleccionada en ETOMASFA
                            //no se puede modificar la fecha prevista
                            //se pueden teclear las observaciones solo y no tiene que rellenar la fecha real
                            //si la fecha real no esta rellena la dosis tiene que ser 0
                            //debe preguntar por esto
                            if (tbDosis.Text.Trim() == "")
                            {
                                //tiene que estar rellena la dosis
                                short tempRefParam11 = 1040;
                                string[] tempRefParam12 = new string[] { lbDDosis.Text };
                                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam11, mbConfirmacionTomas.RcEnfermeria, tempRefParam12);
                                tbDosis.Focus();
                                return;
                            }
                            else
                            {                                
                                if (mbConfirmacionTomas.farmacoSeleccion.iPerfusion != "S" && mbConfirmacionTomas.farmacoSeleccion.iMotDosisCero == "P" && Double.Parse(tbDosis.Text) == 0)
                                {
                                    short tempRefParam13 = 1020;
                                    string[] tempRefParam14 = new string[] { lbDDosis.Text, "mayor", "0" };
                                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, mbConfirmacionTomas.RcEnfermeria, tempRefParam14);
                                    return;
                                }
                                //hay que comprobar que la dosis debe ser mayor o igual que 0
                                if (iModificarDosis)
                                {
                                    if (Double.Parse(tbDosis.Text) < 0)
                                    {
                                        short tempRefParam15 = 1020;
                                        string[] tempRefParam16 = new string[] { lbDDosis.Text, "mayor", "0" };
                                        mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, mbConfirmacionTomas.RcEnfermeria, tempRefParam16);
                                        tbDosis.Focus();
                                        return;
                                    }
                                }
                            }
                            //es obligatorio que lleve una fecha prevista
                            string tempRefParam17 = " Fecha prevista";
                            if (mbConfirmacionTomas.ValidacionFecha(SdcFechaPrevista, tbHoraPrevista, ref tempRefParam17))
                            {
                                SdcFechaPrevista.Focus();
                                return;
                            }

                            if (tbHora.Text.Trim() == ":")
                            { //hora vacia
                                if (sdcFecha.Text == "")
                                { //fecha vacia
                                    if (tbObservaciones.Text.Trim() == "")
                                    { //observaciones vacia
                                        short tempRefParam18 = 1040;
                                        string[] tempRefParam19 = new string[] { Frame4.Text };
                                        mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam18, mbConfirmacionTomas.RcEnfermeria, tempRefParam19);
                                        sdcFecha.Focus();
                                        return;
                                    }
                                }
                                else
                                {
                                    //si la fecha no esta vacia debe teclear la hora
                                    short tempRefParam20 = 1040;
                                    string[] tempRefParam21 = new string[] { "Hora Real" };
                                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam20, mbConfirmacionTomas.RcEnfermeria, tempRefParam21);
                                    tbHora.Focus();
                                    return;
                                }
                            }
                            else
                            {
                                string tempRefParam22 = " Fecha real";
                                if (mbConfirmacionTomas.ValidacionFecha(sdcFecha, tbHora, ref tempRefParam22))
                                {
                                    sdcFecha.Focus();
                                    return;
                                }
                            }
                            if (iModificarDosis)
                            {
                                if (tbObservaciones.Text.Trim() != "" && sdcFecha.Text == "" && tbHora.Text.Trim() == ":" && Conversion.Val(tbDosis.Text) > 0)
                                {
                                    short tempRefParam23 = 1020;
                                    string[] tempRefParam24 = new string[] { lbDDosis.Text, "igual", "0" };
                                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam23, mbConfirmacionTomas.RcEnfermeria, tempRefParam24);
                                    tbDosis.Focus();
                                    return;
                                    //cuidado aqui
                                }
                            }
                            else
                            {

                            }
                        }
                        break;
                }


                //validacion de fecha prevista
                if (SdcFechaPrevista.Text != "" && tbHoraPrevista.Text.Trim() != ":")
                {
                    //cargamos la fecha real de la toma en una variable local
                    //cuando es una nueva toma hay que validar la introducion de la fecha prevista
                    stFechaAuxPrevista = SdcFechaPrevista.Text + " " + tbHoraPrevista.Text;
                    //no se puede grabar un registro con fecha/hora mayor que la del dia
                    if (iApunteToma == NUEVATOMA)
                    {
                        if (DateTime.Parse(stFechaAuxPrevista) > DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis))
                        {
                            short tempRefParam25 = 1020;
                            string[] tempRefParam26 = new string[] { Frame3.Text, "menor o igual", DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis).ToString("dd/MM/yyyy HH:mm") };
                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam25, mbConfirmacionTomas.RcEnfermeria, tempRefParam26);
                            tbHoraPrevista.Focus();
                            return;
                        }
                        //la fecha prevista tiene que ser mayor o igual que la fecha de inicio de tratamiento
                        System.DateTime TempDate2 = DateTime.FromOADate(0);
                        System.DateTime TempDate = DateTime.FromOADate(0);
                        if (DateTime.Parse((DateTime.TryParse(stFechaAuxPrevista, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:NN") : stFechaAuxPrevista) < DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaIni, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.farmacoSeleccion.FechaIni))
                        {
                            short tempRefParam27 = 1020;
                            string[] tempRefParam28 = new string[] { Frame3.Text, "mayor o igual", DateTime.Parse(mbConfirmacionTomas.farmacoSeleccion.FechaIni).ToString("dd/MM/yyyy HH:mm") };
                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam27, mbConfirmacionTomas.RcEnfermeria, tempRefParam28);
                            tbHoraPrevista.Focus();
                            return;
                        }
                        //(maplaza)(31/10/2006)Si el f�rmaco est� cerrado (gris), la fecha/hora prevista debe ser menor o igual
                        //que la fecha de fin movimiento del apunte cerrado (ffinmovi de efarmaco).
                        if (mbConfirmacionTomas.vstFechaFinMov != "")
                        { //si el f�rmaco est� cerrado
                            System.DateTime TempDate4 = DateTime.FromOADate(0);
                            System.DateTime TempDate3 = DateTime.FromOADate(0);
                            if (DateTime.Parse((DateTime.TryParse(stFechaAuxPrevista, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:NN") : stFechaAuxPrevista) > DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaFMovi, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.farmacoSeleccion.FechaFMovi))
                            {
                                short tempRefParam29 = 1020;
                                string[] tempRefParam30 = new string[] { Frame3.Text, "menor o igual", DateTime.Parse(mbConfirmacionTomas.farmacoSeleccion.FechaFMovi).ToString("dd/MM/yyyy HH:mm") };
                                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam29, mbConfirmacionTomas.RcEnfermeria, tempRefParam30);
                                if (tbHoraPrevista.Enabled)
                                {
                                    tbHoraPrevista.Focus();
                                }
                                return;
                            }
                        }
                        //-----
                        //la fecha prevista tiene que ser mayor o igual que la fecha minima
                        System.DateTime TempDate6 = DateTime.FromOADate(0);
                        System.DateTime TempDate5 = DateTime.FromOADate(0);
                        if (DateTime.Parse((DateTime.TryParse(stFechaAuxPrevista, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy HH:NN") : stFechaAuxPrevista) < DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.GstConfirmaMinima, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.GstConfirmaMinima))
                        {
                            short tempRefParam31 = 1020;
                            string[] tempRefParam32 = new string[] { Frame3.Text, "mayor o igual", DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima).ToString("dd/MM/yyyy HH:mm") };
                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam31, mbConfirmacionTomas.RcEnfermeria, tempRefParam32);
                            tbHoraPrevista.Focus();
                            return;
                        }
                    }
                }
                //validacion de fecha real
                if (sdcFecha.Text != "" && tbHora.Text.Trim() != ":")
                {
                    //cargamos la fecha real de la toma en una variable local
                    stFechaAux = sdcFecha.Text + " " + tbHora.Text;
                    //no se puede grabar un registro con fecha/hora mayor que la del dia
                    System.DateTime TempDate7 = DateTime.FromOADate(0);
                    if (DateTime.Parse(stFechaAux) > DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy HH:mm") : mbConfirmacionTomas.vstFechaHoraSis))
                    {
                        short tempRefParam33 = 1020;
                        string[] tempRefParam34 = new string[] { Frame4.Text, "menor o igual", DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis).ToString("dd/MM/yyyy HH:mm") };
                        mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam33, mbConfirmacionTomas.RcEnfermeria, tempRefParam34);
                        tbHora.Focus();
                        return;
                    }

                    //(maplaza)(31/10/2006)Si el f�rmaco est� cerrado (gris), la fecha/hora real debe ser menor o igual
                    //que la fecha de fin movimiento del apunte cerrado (ffinmovi de efarmaco).
                    if (mbConfirmacionTomas.vstFechaFinMov != "")
                    { //si el f�rmaco est� cerrado
                        System.DateTime TempDate9 = DateTime.FromOADate(0);
                        System.DateTime TempDate8 = DateTime.FromOADate(0);
                        if (DateTime.Parse((DateTime.TryParse(stFechaAux, out TempDate8)) ? TempDate8.ToString("dd/MM/yyyy HH:NN") : stFechaAux) > DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaFMovi, out TempDate9)) ? TempDate9.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.farmacoSeleccion.FechaFMovi))
                        {
                            short tempRefParam35 = 1020;
                            string[] tempRefParam36 = new string[] { Frame4.Text, "menor o igual", DateTime.Parse(mbConfirmacionTomas.farmacoSeleccion.FechaFMovi).ToString("dd/MM/yyyy HH:mm") };
                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam35, mbConfirmacionTomas.RcEnfermeria, tempRefParam36);
                            if (tbHora.Enabled)
                            {
                                tbHora.Focus();
                            }
                            return;
                        }
                    }
                    //-----

                    //(maplaza)(12/06/2006)Se necesita poder confirmar una toma antes de la hora prescrita (prevista), por lo tanto
                    //se deja comentada esta validaci�n.
                    //la fecha real tiene que ser >= que la fecha prevista
                    //If CDate(stFechaAux) < CDate(stFechaAuxPrevista) Then
                    //    clase_mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 1020, RcEnfermeria, Frame4.Caption, "mayor o igual", Frame3.Caption
                    //    tbHora.SetFocus
                    //    'sdcFecha.SetFocus
                    //    Exit Sub
                    //End If
                    //-----


                    //la fecha real tiene que ser >= que la fecha minima
                    System.DateTime TempDate10 = DateTime.FromOADate(0);
                    if (DateTime.Parse(stFechaAux) < DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.GstConfirmaMinima, out TempDate10)) ? TempDate10.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.GstConfirmaMinima))
                    {
                        short tempRefParam37 = 1020;
                        string[] tempRefParam38 = new string[] { Frame4.Text, "mayor o igual", DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima).ToString("dd/MM/yyyy HH:mm") };
                        mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam37, mbConfirmacionTomas.RcEnfermeria, tempRefParam38);
                        tbHora.Focus();
                        //sdcFecha.SetFocus
                        return;
                    }
                    if (iApunteToma == NUEVATOMA && mbConfirmacionTomas.farmacoSeleccion.SiPrecisa == "S")
                    {
                        if (DateTime.Parse(stFechaAux) != DateTime.Parse(stFechaAuxPrevista))
                        {
                            short tempRefParam39 = 1020;
                            string[] tempRefParam40 = new string[] { Frame4.Text, "igual", Frame3.Text };
                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam39, mbConfirmacionTomas.RcEnfermeria, tempRefParam40);
                            tbHora.Focus();
                            //sdcFecha.SetFocus
                            return;
                        }

                        //si se trata de una nueva toma y ya hay tomas hay que comprobar que
                    }
                    if (fHayFilaSeleccionada)
                    { //hay un farmaco seleccionado
                        //la fecha de confirmacion tiene que estar entre la fecha prevista propia (FechaMenor)
                        //y la prevista posterior FechaMayor
                        SprFarmacos.Col = 1;
                        //comprobar todos los sprfarmacos.text en caso de que sean blancos
                        if (SprFarmacos.ActiveRowIndex > 1)
                        {
                            // el registro seleccionado no es el primero
                            SprFarmacos.Row = SprFarmacos.ActiveRowIndex;
                            if (SprFarmacos.Text.Trim() != "")
                            {
                                FechaMenor = SprFarmacos.Text.Trim();
                            }
                            SprFarmacos.Row = SprFarmacos.ActiveRowIndex - 1;
                            if (SprFarmacos.Text.Trim() != "")
                            {
                                FechaMayor = SprFarmacos.Text.Trim();
                            }
                            SprFarmacos.Row = SprFarmacos.ActiveRowIndex;
                        }
                        else
                        {
                            //en el caso de que pincha la primera fila del spread
                            if (SprFarmacos.ActiveRowIndex == 1)
                            {
                                //no tendriamos que dejar confirmar una fecha por encima de gstconfirmamaxima
                                if (DateTime.Parse(mbConfirmacionTomas.GstConfirmaMaxima) > DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis))
                                {
                                    FechaMayor = mbConfirmacionTomas.vstFechaHoraSis;
                                }
                                else
                                {
                                    FechaMayor = mbConfirmacionTomas.GstConfirmaMaxima;
                                }
                                SprFarmacos.Row = SprFarmacos.ActiveRowIndex;
                                if (SprFarmacos.Text.Trim() != "")
                                {
                                    FechaMenor = SprFarmacos.Text.Trim();
                                }
                            }
                            else
                            {
                                //no hay ningun registro seleccionado

                            }
                        }

                        //(maplaza)(Detectado el (05/07/2006)):No se debe poder confirmar una toma antes de la fecha/hora real de la toma anterior.
                        if (SprFarmacos.ActiveRowIndex < SprFarmacos.MaxRows)
                        { //si no es la primera toma
                            SprFarmacos.Row = SprFarmacos.ActiveRowIndex + 1; //nos situamos en la toma anterior
                            SprFarmacos.Col = COL_FECHA_HORA_REAL;
                            if (SprFarmacos.Text.Trim() != "")
                            { //si la toma anterior tiene fecha real
                                if (DateTime.Parse(stFechaAux) <= DateTime.Parse(SprFarmacos.Text.Trim()))
                                { //si la fecha/hora real de la toma actual es menor o igual que la fecha/hora real de la toma anterior
                                    short tempRefParam41 = 1020;
                                    string[] tempRefParam42 = new string[] { Frame4.Text, "mayor", DateTime.Parse(SprFarmacos.Text.Trim()).ToString("dd/MM/yyyy HH:mm") };
                                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam41, mbConfirmacionTomas.RcEnfermeria, tempRefParam42);
                                    if (tbHora.Enabled)
                                    {
                                        tbHora.Focus();
                                    }
                                    return;
                                }
                            }
                            //nos volvemos a situar en la misma fila y columna que est�bamos anteriormente
                            SprFarmacos.Row = SprFarmacos.ActiveRowIndex;
                            SprFarmacos.Col = 1;
                        }
                        //-----

                        //(maplaza)(Detectado el (05/07/2006)):No se debe poder confirmar una toma despu�s de la fecha/hora real de la toma posterior.
                        if (SprFarmacos.ActiveRowIndex > 1)
                        { //si no es la �ltima toma
                            SprFarmacos.Row = SprFarmacos.ActiveRowIndex - 1; //nos situamos en la toma posterior
                            SprFarmacos.Col = COL_FECHA_HORA_REAL;
                            if (SprFarmacos.Text.Trim() != "")
                            { //si la toma posterior tiene fecha real
                                if (DateTime.Parse(stFechaAux) >= DateTime.Parse(SprFarmacos.Text.Trim()))
                                { //si la fecha/hora real de la toma actual es mayor o igual que la fecha/hora real de la toma posterior
                                    short tempRefParam43 = 1020;
                                    string[] tempRefParam44 = new string[] { Frame4.Text, "menor", DateTime.Parse(SprFarmacos.Text.Trim()).ToString("dd/MM/yyyy HH:mm") };
                                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam43, mbConfirmacionTomas.RcEnfermeria, tempRefParam44);
                                    if (tbHora.Enabled)
                                    {
                                        tbHora.Focus();
                                    }
                                    return;
                                }
                            }
                            //nos volvemos a situar en la misma fila y columna que est�bamos anteriormente
                            SprFarmacos.Row = SprFarmacos.ActiveRowIndex;
                            SprFarmacos.Col = 1;
                        }
                        //-----

                        //una vez que tenemos los limites cargados hacemos la validacion
                        if (DateTime.Parse(stFechaAux) < DateTime.Parse(FechaMenor))
                        {
                            //(maplaza)(12/06/2006)La validaci�n que aparece a continuaci�n ser� aplicable siempre y cuando se cumpla
                            //que la fecha/hora real es mayor o igual que la fecha/hora prevista.
                            if (DateTime.Parse(stFechaAux) >= DateTime.Parse(stFechaAuxPrevista))
                            {
                                short tempRefParam45 = 1020;
                                string[] tempRefParam46 = new string[] { Frame4.Text, "mayor o igual", DateTime.Parse(FechaMenor).ToString("dd/MM/yyyy HH:mm") };
                                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam45, mbConfirmacionTomas.RcEnfermeria, tempRefParam46);
                                if (tbHora.Enabled)
                                {
                                    tbHora.Focus();
                                }
                                return;
                            }
                            else
                            {
                                //(CDate(stFechaAux) < CDate(stFechaAuxPrevista))
                                //(maplaza)(12/06/2006)Si se cumple que la fecha/hora real es menor que la fecha/hora prevista, deben
                                //cumplirse las siguientes condiciones:
                                //1�)Que la fecha/hora real sea mayor que la fecha prevista anterior.
                                //2�)Que sea mayor que la fecha inicio de tratamiento. (Esto ya est� tratado antes en este procedimiento)
                                //3�)Que sea mayor que "ayer"
                                SprFarmacos.Col = 1;
                                if (SprFarmacos.ActiveRowIndex < SprFarmacos.MaxRows)
                                {
                                    //el registro seleccionado no es el �ltimo (en el spread)
                                    SprFarmacos.Row = SprFarmacos.ActiveRowIndex + 1;
                                    FechaMenor = SprFarmacos.Text.Trim();
                                    if (DateTime.Parse(stFechaAux) <= DateTime.Parse(FechaMenor))
                                    {
                                        short tempRefParam47 = 1020;
                                        string[] tempRefParam48 = new string[] { Frame4.Text, "mayor", DateTime.Parse(FechaMenor).ToString("dd/MM/yyyy HH:mm") };
                                        mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam47, mbConfirmacionTomas.RcEnfermeria, tempRefParam48);
                                        if (sdcFecha.Enabled)
                                        {
                                            sdcFecha.Focus();
                                        }
                                        return;
                                    }

                                    //la fecha tiene que ser mayor que "ayer" (con respecto a la fecha prevista actual); es decir, tiene
                                    //que corresponder entonces al mismo d�a que el de la fecha/hora prevista
                                    //(maplaza)(26/03/2007)Como puede ocurrir que la fecha prevista puede ser superior al d�a actual (porque
                                    //se pueden adelantar tomas futuras posteriores al d�a actual), hay que a�adir una condici�n para que se
                                    //entre en la validaci�n.
                                    if (!(((int)DateAndTime.DateDiff("d", DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis), DateTime.Parse(stFechaAuxPrevista), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) >= 1))
                                    {                                                                                                                        
                                        if (!(DateTime.Parse(stFechaAux).Day == DateTime.Parse(stFechaAuxPrevista).Day &&
                                            DateTime.Parse(stFechaAux).Month == DateTime.Parse(stFechaAuxPrevista).Month &&
                                            DateTime.Parse(stFechaAux).Year == DateTime.Parse(stFechaAuxPrevista).Year))
                                        {
                                            short tempRefParam49 = 1020;
                                            string[] tempRefParam50 = new string[] { Frame4.Text, "mayor", StringsHelper.Format(DateTime.Parse(stFechaAuxPrevista), "dd/mm/yyyy 00:00") };
                                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam49, mbConfirmacionTomas.RcEnfermeria, tempRefParam50);
                                            if (sdcFecha.Enabled)
                                            {
                                                sdcFecha.Focus();
                                            }
                                            return;
                                        }
                                    }
                                }
                                else
                                {
                                    //en el caso de que pincha la �ltima fila del spread
                                    //la fecha tiene que ser mayor que "ayer" (con respecto a la fecha prevista actual); es decir, tiene
                                    //que corresponder entonces al mismo d�a que el de la fecha/hora prevista
                                    //(maplaza)(26/03/2007)Como puede ocurrir que la fecha prevista puede ser superior al d�a actual (porque
                                    //se pueden adelantar tomas futuras posteriores al d�a actual), hay que a�adir una condici�n para que se
                                    //entre en la validaci�n.
                                    if (!(((int)DateAndTime.DateDiff("d", DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis), DateTime.Parse(stFechaAuxPrevista), FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) >= 1))
                                    {                                                                                
                                        if (!(DateTime.Parse(stFechaAux).Day == DateTime.Parse(stFechaAuxPrevista).Day &&
                                            DateTime.Parse(stFechaAux).Month == DateTime.Parse(stFechaAuxPrevista).Month &&
                                            DateTime.Parse(stFechaAux).Year == DateTime.Parse(stFechaAuxPrevista).Year))
                                        {
                                            short tempRefParam51 = 1020;
                                            string[] tempRefParam52 = new string[] { Frame4.Text, "mayor", StringsHelper.Format(DateTime.Parse(stFechaAuxPrevista), "dd/mm/yyyy 00:00") };
                                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam51, mbConfirmacionTomas.RcEnfermeria, tempRefParam52);
                                            if (sdcFecha.Enabled)
                                            {
                                                sdcFecha.Focus();
                                            }
                                            return;
                                        }
                                    }
                                }
                                //-----
                            }
                        }
                        else
                        {
                            if (DateTime.Parse(stFechaAux) >= DateTime.Parse(FechaMayor))
                            {
                                short tempRefParam53 = 1020;
                                string[] tempRefParam54 = new string[] { Frame4.Text, "menor", DateTime.Parse(FechaMayor).ToString("dd/MM/yyyy HH:mm") };
                                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam53, mbConfirmacionTomas.RcEnfermeria, tempRefParam54);
                                if (tbHora.Enabled)
                                {
                                    tbHora.Focus();
                                }
                                return;
                            }
                        }
                    }
                }
                if (iFarmacoEquivalente)
                { //caso de gesti�n de farmaco equivalente
                    if (CbbFarmacoAdm.Visible)
                    { //caso de que exista farmaco equivalente
                        if (Convert.ToDouble(CbbFarmacoAdm.get_ListIndex()) < 0)
                        { //si no se selecciona farmaco
                            short tempRefParam55 = 1040;
                            string[] tempRefParam56 = new string[] { lbfarmacoEqui.Text };
                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam55, mbConfirmacionTomas.RcEnfermeria, tempRefParam56);
                            return;
                        }
                    }
                }

                //(maplaza)(26/03/2007)Se produc�a un error cuando no se introduc�a el campo de la fecha, y s� se introduc�a el campo
                //de "Observaciones".
                if (iApunteToma != NUEVATOMA)
                {
                    if (sdcFecha.Text == "")
                    { //fecha vacia
                        short tempRefParam57 = 1040;
                        string[] tempRefParam58 = new string[] { Frame4.Text };
                        mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam57, mbConfirmacionTomas.RcEnfermeria, tempRefParam58);
                        sdcFecha.Focus();
                        return;
                    }
                }
                //-----

                //(maplaza)(12/06/2006)En el caso de de que la fecha real sea menor que la fecha prevista, no se permitir� dejar
                //vac�o el campo de "Observaciones". Para hacer esto, sirve un registro que ya exist�a anteriormente en la tabla
                //"SMENSAJE" (gmensaje='3200'). (05/07/2006)Ser�a conveniente que esta validaci�n se colocase al final de todas las validaciones
                if (DateTime.Parse(stFechaAux) < DateTime.Parse(stFechaAuxPrevista))
                {
                    if (tbObservaciones.Text.Trim() == "")
                    { //observaciones vac�a
                        short tempRefParam59 = 3200;
                        string[] tempRefParam60 = new string[] { Frame4.Text + " es menor que " + Frame3.Text, lbObservaciones.Text };
                        mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam59, mbConfirmacionTomas.RcEnfermeria, tempRefParam60);
                        if (tbObservaciones.Enabled)
                        {
                            tbObservaciones.Focus();
                        }
                        return;
                    }
                }
                //-----

                GrabarRegistro();
                SprFarmacos.MaxRows = 0;
                mbConfirmacionTomas.vstFechaHoraSis = DateTimeHelper.ToString(DateTime.Now);
                Llenar_grid();
                IniciarPantalla();
                ProtegerPantalla("vacio");
            }
            catch (SqlException ex)
            {
                Serrores.AnalizaError("ConfirmacionTomas.dll", mbConfirmacionTomas.stPathAplicacion, mbConfirmacionTomas.gUsuario, "ConfirmarFarmacos:cbAceptar_Click", ex);
            }

        }

        private void cbAnular_Click(Object eventSender, EventArgs eventArgs)
        {
            string Sql = String.Empty;
            DataSet RrSQL = null;

            //para anular una toma tiene que estar seleccionada
            //no tiene que estar facturada
            if (fHayFilaSeleccionada)
            {
                //comprobamos que la toma se pueda anular
                Sql = Sql + "Select * from ETOMASFA where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ";
                Sql = Sql + " and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " ";
                Sql = Sql + " and  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ";
                //UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                object tempRefParam = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                Sql = Sql + " and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam) + " ";
                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam);
                Sql = Sql + " and gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco.Trim() + "' ";
                object tempRefParam2 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                Sql = Sql + " and fprevist =" + Serrores.FormatFechaHMS(tempRefParam2) + " ";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mbConfirmacionTomas.RcEnfermeria);
                RrSQL = new DataSet();
                tempAdapter.Fill(RrSQL);

                if (RrSQL.Tables[0].Rows.Count != 0)
                {
                    //comprobamos que la toma no este facturada                    
                    if (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["fpasfact"]))
                    {
                        //hay que comprobar si la toma que vamos a anular tiene fecha de facturacion?
                        if (RadMessageBox.Show("�Esta seguro de que quiere anular la toma seleccionada?", Serrores.vNomAplicacion, MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                        {
                            //si responde que no
                            return;
                        }
                        else
                        {
                            //responde que si
                            //se pasa el registro de la toma a la tabla de tomas anuladas
                            //con la fecha de anulacion FANULACI = Fecha del sistema
                            SqlTransaction transaction = mbConfirmacionTomas.RcEnfermeria.BeginTrans();
                            try
                            {
                                Sql = "insert into ETOMANUL (itiposer,ganoregi,gnumregi,fapuntes,gfarmaco,fprevist,ftomasfa,";
                                Sql = Sql + " gusuario, fpasfact, cdosisto, oobserva, gfarmadm, iPropPac,fanulaci,cdosimpu )";
                                Sql = Sql + "Select ETOMASFA.itiposer,ETOMASFA.ganoregi,ETOMASFA.gnumregi,ETOMASFA.fapuntes,";
                                Sql = Sql + " ETOMASFA.gfarmaco , ETOMASFA.fprevist, ETOMASFA.ftomasfa, ";
                                Sql = Sql + "'" + mbConfirmacionTomas.UsuarioConfirmacion.Trim().ToUpper() + "', ETOMASFA.fpasfact, ETOMASFA.cdosisto,";
                                object tempRefParam3 = DateTime.Now;
                                Sql = Sql + " ETOMASFA.oobserva, ETOMASFA.gfarmadm, ETOMASFA.iPropPac," + Serrores.FormatFechaHMS(tempRefParam3) + ",ETOMASFA.cdosimpu ";
                                Sql = Sql + " from ETOMASFA where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ";
                                Sql = Sql + " and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " ";
                                Sql = Sql + " and  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ";
                                //UPGRADE_WARNING: (1068) tempRefParam4 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam4 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                Sql = Sql + " and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam4) + " ";
                                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam4);
                                Sql = Sql + " and gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco.Trim() + "' ";
                                object tempRefParam5 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                                Sql = Sql + " and fprevist =" + Serrores.FormatFechaHMS(tempRefParam5) + " ";

                                SqlCommand tempCommand = new SqlCommand(Sql, mbConfirmacionTomas.RcEnfermeria, transaction);
                                tempCommand.ExecuteNonQuery();

                                Sql = "delete from ETOMASFA where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ";
                                Sql = Sql + " and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " ";
                                Sql = Sql + " and  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ";
                                //UPGRADE_WARNING: (1068) tempRefParam6 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam6 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                Sql = Sql + " and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam6) + " ";
                                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam6);
                                Sql = Sql + " and gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco.Trim() + "' ";
                                object tempRefParam7 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                                Sql = Sql + " and fprevist =" + Serrores.FormatFechaHMS(tempRefParam7) + " ";

                                SqlCommand tempCommand_2 = new SqlCommand(Sql, mbConfirmacionTomas.RcEnfermeria, transaction);
                                tempCommand_2.ExecuteNonQuery();

                                // Borramos tambi�n el registro de datos modificados

                                Sql = "delete from ETOMAMOD where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ";
                                Sql = Sql + " and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " ";
                                Sql = Sql + " and  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ";
                                //UPGRADE_WARNING: (1068) tempRefParam8 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam8 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                Sql = Sql + " and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam8) + " ";
                                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam8);
                                Sql = Sql + " and gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco.Trim() + "' ";
                                object tempRefParam9 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                                Sql = Sql + " and fprevist =" + Serrores.FormatFechaHMS(tempRefParam9) + " ";

                                SqlCommand tempCommand_3 = new SqlCommand(Sql, mbConfirmacionTomas.RcEnfermeria, transaction);
                                tempCommand_3.ExecuteNonQuery();

                                //OSCAR C 03/11/2005
                                //Al anular una toma de dosis unica, se debe restaurar la situacion anterior. O sea,
                                //borrando el apunte finalizado y activando de nuevo el apunte cerrado del farmaco.
                                if (mbConfirmacionTomas.farmacoSeleccion.idounica.Trim().ToUpper() == "S")
                                {
                                    //UPGRADE_WARNING: (1068) tempRefParam10 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                    object tempRefParam10 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                    Sql = "UPDATE EFARMACO SET " +
                                          "  ffinmovi = (NULL) " +
                                          " WHERE " +
                                          "  itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and " +
                                          "  ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and " +
                                          "  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and " +
                                          "  fapuntes = " + Serrores.FormatFechaHMS(tempRefParam10) + " and " +
                                          "  gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "'";
                                    mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam10);

                                    SqlCommand tempCommand_4 = new SqlCommand(Sql, mbConfirmacionTomas.RcEnfermeria, transaction);
                                    tempCommand_4.ExecuteNonQuery();

                                    object tempRefParam11 = DateTime.Parse(sdcFecha.Value.Date + " " + tbHora.Text).AddMinutes(1);
                                    Sql = "DELETE EFARMACO " +
                                          " WHERE " +
                                          "  itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and " +
                                          "  ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and " +
                                          "  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and " +
                                          "  gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' and " +
                                          "  iesttrat='F' and " +
                                          "  fapuntes=" + Serrores.FormatFechaHMS(tempRefParam11);

                                    SqlCommand tempCommand_5 = new SqlCommand(Sql, mbConfirmacionTomas.RcEnfermeria, transaction);
                                    tempCommand_5.ExecuteNonQuery();

                                    // Tambi�n lo modificado

                                    object tempRefParam12 = DateTime.Parse(sdcFecha.Value.Date + " " + tbHora.Text).AddMinutes(1);
                                    Sql = "DELETE EFARMMOD " +
                                          " WHERE " +
                                          "  itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and " +
                                          "  ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and " +
                                          "  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and " +
                                          "  gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' and " +
                                          "  iesttrat='F' and " +
                                          "  fapuntes=" + Serrores.FormatFechaHM(tempRefParam12);

                                    SqlCommand tempCommand_6 = new SqlCommand(Sql, mbConfirmacionTomas.RcEnfermeria, transaction);
                                    tempCommand_6.ExecuteNonQuery();
                                }
                                //--------

                                mbConfirmacionTomas.RcEnfermeria.CommitTrans(transaction);
                                //una vez que hemos anulado la toma volvemos a cargar la pantalla
                                SprFarmacos.MaxRows = 0;
                                mbConfirmacionTomas.vstFechaHoraSis = DateTimeHelper.ToString(DateTime.Now);
                                Llenar_grid();
                                IniciarPantalla();
                                ProtegerPantalla("vacio");
                            }
                            catch (SqlException ex)
                            {
                                mbConfirmacionTomas.RcEnfermeria.RollbackTrans(transaction);
                                var texto = Serrores.CreaError("", ex.Source, ex.Number, ex.Message, "");
                                Serrores.GrabaLog(texto, "ConfirmarFarmacos.log");
                            } finally
                            {
                                transaction.Dispose();
                            }
                        } //fin de toma anulada
                    }
                    else
                    {
                        //no se puede anular la toma ya esta facturada
                        short tempRefParam13 = 1420;
                        string[] tempRefParam14 = new string[] { "La toma seleccionada no se puede anular", "facturada" };
                        mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam13, mbConfirmacionTomas.RcEnfermeria, tempRefParam14);
                    }
                }
                else
                {
                    //la toma seleccionada no esta en ETOMASFA
                }                
            }
            else
            {
                //debe seleccionar un registro
                short tempRefParam15 = 1270;
                string[] tempRefParam16 = new string[] { "una fila" };
                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam15, mbConfirmacionTomas.RcEnfermeria, tempRefParam16);
            }

        }

        private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
        {
            IniciarPantalla();
            ProtegerPantalla("vacio");
        }

        private void cbNuevo_Click(Object eventSender, EventArgs eventArgs)
        {

            iApunteToma = NUEVATOMA;

            tbDosis.Text = mbConfirmacionTomas.farmacoSeleccion.DosisAdm;

            System.DateTime TempDate = DateTime.FromOADate(0);
            SdcFechaPrevista.Value = DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : mbConfirmacionTomas.vstFechaHoraSis);
            System.DateTime TempDate3 = DateTime.FromOADate(0);
            System.DateTime TempDate2 = DateTime.FromOADate(0);
            tbHoraPrevista.Text = ((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate2)) ? TempDate2.ToString("HH") : mbConfirmacionTomas.vstFechaHoraSis) + ":" + ((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate3)) ? TempDate3.ToString("mm") : mbConfirmacionTomas.vstFechaHoraSis);

            System.DateTime TempDate4 = DateTime.FromOADate(0);
            sdcFecha.Value = DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy") : mbConfirmacionTomas.vstFechaHoraSis);
            System.DateTime TempDate6 = DateTime.FromOADate(0);
            System.DateTime TempDate5 = DateTime.FromOADate(0);
            tbHora.Text = ((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate5)) ? TempDate5.ToString("HH") : mbConfirmacionTomas.vstFechaHoraSis) + ":" + ((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate6)) ? TempDate6.ToString("mm") : mbConfirmacionTomas.vstFechaHoraSis);

            if (iFarmacoEquivalente)
            {
                CargarFarmacoEquivalente();
            }
            else
            {
                lbFarmacoAdm.Visible = true;
                lbFarmacoAdm.Text = lbNombreFarmaco.Text.Trim();
            }

            ProtegerPantalla("nuevo");

            tbDosis.Focus();
        }

        private void cbSalir_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }

        private void ConfirmarFarmacos_Activated(Object eventSender, EventArgs eventArgs)
        {
            if (ActivateHelper.myActiveForm != eventSender)
            {
                ActivateHelper.myActiveForm = (Form)eventSender;
                try
                {
                    //Aplicar el control de acceso a la pantalla
                    if (!mbAcceso.AplicarControlAcceso(mbConfirmacionTomas.UsuarioConfirmacion.ToUpper(), mbConfirmacionTomas.GstTiposervicio, this, "EGI215F4", ref mbConfirmacionTomas.astrcontroles, ref mbConfirmacionTomas.astreventos, mbConfirmacionTomas.RcEnfermeria))
                    {
                        return;
                    }

                    // Cargar los valores del farmaco

                    lbNombreFarmaco.Text = mbConfirmacionTomas.GstNombreFarmaco;
                    SprFarmacos.MaxRows = 0;
                    mbConfirmacionTomas.vstFechaHoraSis = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    //(maplaza)(17/11/2006)Se debe conocer si en el per�odo del episodio est� ausente el paciente, para saber
                    //as� si se pueden confirmar tomas o no.
                    vfAusencia = ConMotivoDeAusencia();
                    //-----
                    FechasPantalla();
                    IniciarPantalla();

                    Llenar_grid();
                    ProtegerPantalla("vacio");
                    //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprFarmacos.SelModeIndex was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                    //SprFarmacos.setSelModeIndex(0);

                    cbSalir.Focus();

                    return;
                }
                catch (SqlException ex)
                {
                    Serrores.AnalizaError("ConfirmacionTomas.dll", mbConfirmacionTomas.stPathAplicacion, mbConfirmacionTomas.gUsuario, "ConfirmarFarmacos:Form_activate", ex);
                    this.Close();
                }
            }
        }
        
        private void ConfirmarFarmacos_Load(Object eventSender, EventArgs eventArgs)
        {
            //If Not AplicarControlAcceso(gUsuario, GstTiposervicio, Me, "EGI215F4", astrcontroles(), astreventos(), RcEnfermeria) Then Exit Sub
            iModificarDosis = mbAcceso.ObtenerCodigoSConsglo("IMODDOSI", "VALFANU1", mbConfirmacionTomas.RcEnfermeria) == "S";
            iFarmacoEquivalente = mbAcceso.ObtenerCodigoSConsglo("UNIDOSIS", "VALFANU1", mbConfirmacionTomas.RcEnfermeria).ToUpper() == "S";
            tbDosis.Enabled = iModificarDosis;
            //UPGRADE_ISSUE: (2070) Constant App was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
            //UPGRADE_ISSUE: (2064) App property App.HelpFile was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Ayuda_DLLS.hlp");            
        }

        //UPGRADE_NOTE: (7001) The following declaration (llenar_grid2) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
        private void llenar_grid2()
        {
            StringBuilder Sql = new StringBuilder();
            DataSet RrTomas = null;
            //Dim colorapunte As Long
            //Dim RrDescripcion As rdoResultset
            string sDiasPrevistos = String.Empty; //lleva asteriscos en los dias que tocan tomas
            int iDiaSemana = 0;
            string stFechaInicioBucle = String.Empty;
            string stFechafinBucle = String.Empty;
            string stFechaBucle = String.Empty;
            int aij = 0;
            int iHoraIni = 0;
            int iHoraFin = 0;
            string stFechaToma = String.Empty;
            string stFechaToma2 = String.Empty;
            string stFechaPrevista = String.Empty;
                
            try
            {        
                //se llenan con las horas prevista de las ultimas 48 horas
                //si a esa hora hay un registro en etomasfa se rellena las casillas del spread con
                //los valores del registro, sino hay registro se rellena a blancos
                //hay que hacer una rutina que calcule las horas previstas
                //GstConfirmaMaxima lleva la hora del sistema
                //GstConfirmaMinima lleva 48 horas antes de la fecha del sistema o la de inicio del tratamiento
        
                if (DateTime.Parse(mbConfirmacionTomas.GstConfirmaMaxima) < DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis))
                {
                    stFechaInicioBucle = mbConfirmacionTomas.GstConfirmaMaxima;
                }
                else
                {
                    stFechaInicioBucle = mbConfirmacionTomas.vstFechaHoraSis;
                }
                //hay que buscar 48 horas antes
        
                stFechafinBucle = DateTimeHelper.ToString(DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis).AddHours(-48));
        
                //If CDate(GstConfirmaMinima) > CDate(stFechaFinBucle & " " & Format(vstFechaHoraSis, "HH:NN")) Then
                if (DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima) > DateTime.Parse(stFechafinBucle))
                {
                    stFechafinBucle = mbConfirmacionTomas.GstConfirmaMinima;
                }
                else
                {
                    //stFechaFinBucle = DateAdd("d", CDate(vstFechaHoraSis), -2) & " " & Format(vstFechaHoraSis, "HH:NN");
                    stFechafinBucle = DateTimeHelper.ToString(DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis).AddHours(-48));
                }

                System.DateTime TempDate = DateTime.FromOADate(0);

                if (Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate)) ? TempDate.ToString("NN") : stFechafinBucle) > 0)
                {
                    System.DateTime TempDate2 = DateTime.FromOADate(0);
                    if (Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate2)) ? TempDate2.ToString("HH") : stFechafinBucle) == 23)
                    {
                        //comprobar lo que devuelve
                        stFechafinBucle = DateTimeHelper.ToString(DateTime.FromOADate(1).AddDays(DateTime.Parse(stFechafinBucle).ToOADate())) + " 00:00";
                    }
                    else
                    {
                        //stFechaFinBucle = Format(stFechaFinBucle, "DD/MM/YYYY") & " " & Format(Val(Format(stFechaFinBucle, "hh")) + 1, "00") & ":00"
                        System.DateTime TempDate4 = DateTime.FromOADate(0);
                        System.DateTime TempDate3 = DateTime.FromOADate(0);
                        stFechafinBucle = ((DateTime.TryParse(stFechafinBucle, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : stFechafinBucle) + " " + StringsHelper.Format(Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate4)) ? TempDate4.ToString("HH") : stFechafinBucle), "00") + ":00";
                    }
                }
                //tengo que cargar la fecha inicial hasta la fecha final
                //el bucle empieza en la hora actual
                for (int aik = 0; aik <= 2; aik++)
                {
                    //le descuento dias porque son dias completos desde 0 a 23 horas
                    //stFechaBucle = DateTimeHelper.ToString(DateTime.FromOADate(-aik).AddDays(DateTime.Parse(stFechaInicioBucle).ToOADate())); //esta la fecha del bucle "DD/MM/YYYY"
                    //asi obtengo el dia de la fecha del bucle
                    System.DateTime TempDate6 = DateTime.FromOADate(0);
                    System.DateTime TempDate5 = DateTime.FromOADate(0);
                    if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy") : stFechaBucle) >= DateTime.Parse((DateTime.TryParse(stFechafinBucle, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy") : stFechafinBucle) && DateTime.Parse(stFechaBucle) <= DateTime.Parse(stFechaInicioBucle))
                    {
                        if (mbConfirmacionTomas.farmacoSeleccion.Periodic > 0)
                        {
                            //tenemos que mirar si la fecha del bucle coincide con una fecha de toma
                            //BuscarFechaToma
                            System.DateTime TempDate8 = DateTime.FromOADate(0);
                            System.DateTime TempDate7 = DateTime.FromOADate(0);
                            stFechaBucle = ((DateTime.TryParse(stFechaBucle, out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy") : stFechaBucle) + " " + ((DateTime.TryParse(stFechaInicioBucle, out TempDate8)) ? TempDate8.ToString("HH:NN") : stFechaInicioBucle);
                            sDiasPrevistos = DiasPrevistos(stFechaBucle);
                        }
                        else
                        {
                            //miramos si en esta fecha hay tomas
                            sDiasPrevistos = mbConfirmacionTomas.farmacoSeleccion.VsDias;
                            //hay que controlar que la fecha este en los limites entre la maxima y la minimima posible
                        }
                        if (sDiasPrevistos.Trim() != "")
                        {
                            iDiaSemana = DateAndTime.Weekday(DateTime.Parse(stFechaBucle), FirstDayOfWeek.Monday); //'devuelve el dia de la semana que es hoy
                            //si el dia de la semana en el que estoy hay tomas previstas
                            if (sDiasPrevistos.Substring(iDiaSemana - 1, Math.Min(1, sDiasPrevistos.Length - (iDiaSemana - 1))) != " ")
                            {
                            //hay tomas ese dia bucle de horas
                                System.DateTime TempDate10 = DateTime.FromOADate(0);
                                System.DateTime TempDate9 = DateTime.FromOADate(0);
                                if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate9)) ? TempDate9.ToString("dd/MM/yyyy") : stFechaBucle) == DateTime.Parse((DateTime.TryParse(stFechaInicioBucle, out TempDate10)) ? TempDate10.ToString("dd/MM/yyyy") : stFechaInicioBucle))
                                {
                                    System.DateTime TempDate11 = DateTime.FromOADate(0);
                                    iHoraIni = Convert.ToInt32(Double.Parse((DateTime.TryParse(stFechaInicioBucle, out TempDate11)) ? TempDate11.ToString("HH") : stFechaInicioBucle));
                                    //puede que el inicio y fin del bucle este en el mismo dia
                                    System.DateTime TempDate13 = DateTime.FromOADate(0);
                                    System.DateTime TempDate12 = DateTime.FromOADate(0);
                                    if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate12)) ? TempDate12.ToString("dd/MM/yyyy") : stFechaBucle) == DateTime.Parse((DateTime.TryParse(stFechafinBucle, out TempDate13)) ? TempDate13.ToString("dd/MM/yyyy") : stFechafinBucle))
                                    {
                                        System.DateTime TempDate14 = DateTime.FromOADate(0);
                                        iHoraFin = Convert.ToInt32(Double.Parse((DateTime.TryParse(stFechafinBucle, out TempDate14)) ? TempDate14.ToString("HH") : stFechafinBucle));
                                    }
                                    else
                                    {
                                        iHoraFin = 0;
                                    }
                                }
                                else
                                {
                                    //la fecha del bucle es inferior a la fecha de de inicio del bucle
                                    iHoraIni = 23;
                                    //si la fecha del bucle es igual que la fecha de fin del bucle se carga la hora del fin del bucle
                                    System.DateTime TempDate16 = DateTime.FromOADate(0);
                                    System.DateTime TempDate15 = DateTime.FromOADate(0);
                                    if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate15)) ? TempDate15.ToString("dd/MM/yyyy") : stFechaBucle) == DateTime.Parse((DateTime.TryParse(stFechafinBucle, out TempDate16)) ? TempDate16.ToString("dd/MM/yyyy") : stFechafinBucle))
                                    {
                                        //si la hora es mayor que Hora:00
                                        System.DateTime TempDate17 = DateTime.FromOADate(0);
                                        if (Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate17)) ? TempDate17.ToString("mm") : stFechafinBucle) > 0)
                                        {
                                            System.DateTime TempDate18 = DateTime.FromOADate(0);
                                            iHoraFin = Convert.ToInt32(Double.Parse((DateTime.TryParse(stFechafinBucle, out TempDate18)) ? TempDate18.ToString("HH") : stFechafinBucle) + 1);
                                        }
                                        else
                                        {
                                            System.DateTime TempDate19 = DateTime.FromOADate(0);
                                            iHoraFin = Convert.ToInt32(Double.Parse((DateTime.TryParse(stFechafinBucle, out TempDate19)) ? TempDate19.ToString("HH") : stFechafinBucle));
                                        }
                                    }
                                    else
                                    {
                                        //en el resto de los casos considero el dia completo
                                        iHoraFin = 0;
                                    }
                                }
                                //si hay toma en fecha bucle
                                for (aij = iHoraIni; aij >= iHoraFin; aij--)
                                { //el bucle tiene que ir hacia atras
                                    if (mbConfirmacionTomas.farmacoSeleccion.Dosis[aij].Trim() != "")
                                    {
                                        System.DateTime TempDate20 = DateTime.FromOADate(0);
                                        stFechaToma = ((DateTime.TryParse(stFechaBucle, out TempDate20)) ? TempDate20.ToString("dd/MM/yyyy") : stFechaBucle) + " " + StringsHelper.Format(aij, "00") + ":00:00";
                                        System.DateTime TempDate21 = DateTime.FromOADate(0);
                                        stFechaPrevista = ((DateTime.TryParse(stFechaBucle, out TempDate21)) ? TempDate21.ToString("dd/MM/yyyy") : stFechaBucle) + " " + StringsHelper.Format(aij, "00") + ":00";
                                        //buscamos las tomas que hay en la hora
                                        if (mbConfirmacionTomas.farmacoSeleccion.SiPrecisa == "N")
                                        {
                                            //si el farmaco no es si precisa
                                            Sql = new StringBuilder("select * from ETOMASFA where ");
                                            Sql.Append("itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and ");
                                            Sql.Append("ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and ");
                                            Sql.Append("gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and ");
                                            //UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                            object tempRefParam = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                            Sql.Append("fapuntes = " + Serrores.FormatFechaHMS(tempRefParam) + " and ");
                                            mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam);
                                            Sql.Append("gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' and ");
                                            //UPGRADE_WARNING: (1068) tempRefParam2 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                            object tempRefParam2 = stFechaToma;
                                            Sql.Append("fprevist = " + Serrores.FormatFechaHMS(tempRefParam2) + " ");
                                            stFechaToma = Convert.ToString(tempRefParam2);
        
                                            SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                                            RrTomas = new DataSet();
                                            tempAdapter.Fill(RrTomas);
                                            if (RrTomas.Tables[0].Rows.Count != 0)
                                            {
                                                //si hay un registro se muestra                                                
                                                string tempRefParam3 = Convert.ToString(RrTomas.Tables[0].Rows[0]["cdosisto"]);
                                                Cargar_grid(stFechaPrevista, ref tempRefParam3, (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["ftomasfa"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["ftomasfa"]), (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["oobserva"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["oobserva"]), Convert.ToString(RrTomas.Tables[0].Rows[0]["gusuario"]));
                                            }
                                            else
                                            {
                                                //no hay registro se muestra la toma prevista vacia
                                                Cargar_grid(stFechaPrevista, ref mbConfirmacionTomas.farmacoSeleccion.Dosis[aij], "", "", Type.Missing);
                                            }                                            
                                        }
                                    }
                                    else
                                    {
                                        //no hay toma prevista pero el dia 18/11/2002 si que pueden existir tomas que no esten previstas
                                        System.DateTime TempDate22 = DateTime.FromOADate(0);
                                        stFechaToma = ((DateTime.TryParse(stFechaBucle, out TempDate22)) ? TempDate22.ToString("dd/MM/yyyy") : stFechaBucle) + " " + StringsHelper.Format(aij, "00") + ":00:00";
                                        //stFechaToma2 = Format(stFechaBucle, "dd/mm/yyyy") & " " & Format(aij, "00") & ":59:59"
                                        System.DateTime TempDate24 = DateTime.FromOADate(0);
                                        System.DateTime TempDate23 = DateTime.FromOADate(0);
                                        if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate23)) ? TempDate23.ToString("dd/MM/yyyy") : stFechaBucle) == DateTime.Parse((DateTime.TryParse(stFechafinBucle, out TempDate24)) ? TempDate24.ToString("dd/MM/yyyy") : stFechafinBucle))
                                        {
                                            //la fecha del bucle es la misma que la fecha fin del bucle
                                            System.DateTime TempDate25 = DateTime.FromOADate(0);
                                            stFechaToma2 = ((DateTime.TryParse(stFechafinBucle, out TempDate25)) ? TempDate25.ToString("dd/MM/yyyy HH") : stFechafinBucle) + ":59:59";
                                        }
                                        else
                                        {
                                            System.DateTime TempDate26 = DateTime.FromOADate(0);
                                            stFechaToma2 = ((DateTime.TryParse(stFechaBucle, out TempDate26)) ? TempDate26.ToString("dd/MM/yyyy") : stFechaBucle) + " 23:59:59";
                                        }
                                        //en este caso no hay tomas previstas
                                        //stFechaPrevista = Format(stFechaBucle, "dd/mm/yyyy") & " " & Format(aij, "00") & ":00"
        
                                        Sql = new StringBuilder("select * from ETOMASFA where ");
                                        Sql.Append("itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and ");
                                        Sql.Append("ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and ");
                                        Sql.Append("gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and ");
                                        //UPGRADE_WARNING: (1068) tempRefParam4 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                        object tempRefParam4 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                        Sql.Append("fapuntes = " + Serrores.FormatFechaHMS(tempRefParam4) + " and ");
                                        mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam4);
                                        Sql.Append("gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' and ");
                                        //UPGRADE_WARNING: (1068) tempRefParam5 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                        object tempRefParam5 = stFechaToma;
                                        Sql.Append("(fprevist >= " + Serrores.FormatFechaHMS(tempRefParam5) + " and ");
                                        stFechaToma = Convert.ToString(tempRefParam5);
                                        //UPGRADE_WARNING: (1068) tempRefParam6 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                        object tempRefParam6 = stFechaToma2;
                                        Sql.Append("fprevist <= " + Serrores.FormatFechaHMS(tempRefParam6) + " ) ");
                                        stFechaToma2 = Convert.ToString(tempRefParam6);
                                        Sql.Append("order by ftomasfa desc ");
                                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                                        RrTomas = new DataSet();
                                        tempAdapter_2.Fill(RrTomas);
                                        if (RrTomas.Tables[0].Rows.Count != 0)
                                        {
                                            //si hay un registro
                                            //puede devolver mas de un registro
                                            //UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
                                            RrTomas.MoveFirst();
                                            foreach (DataRow iteration_row in RrTomas.Tables[0].Rows)
                                            {
                                                //Cargar_grid stFechaPrevista, FormatearCifra(RrTomas("cdosisto")), IIf(IsNull(RrTomas("ftomasfa")), "", RrTomas("ftomasfa")), IIf(IsNull(RrTomas("oobserva")), "", RrTomas("oobserva")), RrTomas("gusuario")
                                                stFechaPrevista = Convert.ToDateTime(iteration_row["fprevist"]).ToString("dd/MM/yyyy HH:NN");
                                                //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                                                string tempRefParam7 = Convert.ToString(iteration_row["cdosisto"]);
                                                Cargar_grid(stFechaPrevista, ref tempRefParam7, (Convert.IsDBNull(iteration_row["ftomasfa"])) ? "" : Convert.ToString(iteration_row["ftomasfa"]), (Convert.IsDBNull(iteration_row["oobserva"])) ? "" : Convert.ToString(iteration_row["oobserva"]), Convert.ToString(iteration_row["gusuario"]));
                                            }        
                                        }
                                        else
                                        {
                                            //no hay toma que no este prevista
                                            //Cargar_grid stFechaPrevista, FarmacoSeleccion.Dosis(aij), "", ""
                                        }                                        
                                        break; //a�adido
                                    }
                                }      
                            }
                            else
                            {
                                //no hay tomas ese dia
                                //buscamos en ETOMASFA por si acaso han introducido alguna toma el dia que no esta previsto
                                System.DateTime TempDate27 = DateTime.FromOADate(0);
                                stFechaToma = ((DateTime.TryParse(stFechaBucle, out TempDate27)) ? TempDate27.ToString("dd/MM/yyyy") : stFechaBucle) + " " + StringsHelper.Format(aij, "00") + ":00:00";
                                System.DateTime TempDate29 = DateTime.FromOADate(0);
                                System.DateTime TempDate28 = DateTime.FromOADate(0);
                                if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate28)) ? TempDate28.ToString("dd/MM/yyyy") : stFechaBucle) == DateTime.Parse((DateTime.TryParse(stFechafinBucle, out TempDate29)) ? TempDate29.ToString("dd/MM/yyyy") : stFechafinBucle))
                                {
                                    //la fecha del bucle es la misma que la fecha fin del bucle
                                    System.DateTime TempDate30 = DateTime.FromOADate(0);
                                    stFechaToma2 = ((DateTime.TryParse(stFechafinBucle, out TempDate30)) ? TempDate30.ToString("dd/MM/yyyy HH") : stFechafinBucle) + ":59:59";
                                }
                                else
                                {
                                    System.DateTime TempDate31 = DateTime.FromOADate(0);
                                    stFechaToma2 = ((DateTime.TryParse(stFechaBucle, out TempDate31)) ? TempDate31.ToString("dd/MM/yyyy") : stFechaBucle) + " 23:59:59";
                                }
        
                                Sql = new StringBuilder("select * from ETOMASFA where ");
                                Sql.Append("itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and ");
                                Sql.Append("ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and ");
                                Sql.Append("gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and ");
                                //UPGRADE_WARNING: (1068) tempRefParam8 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam8 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                Sql.Append("fapuntes = " + Serrores.FormatFechaHMS(tempRefParam8) + " and ");
                                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam8);
                                Sql.Append("gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' and ");
                                //UPGRADE_WARNING: (1068) tempRefParam9 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam9 = stFechaToma;
                                Sql.Append("(fprevist >= " + Serrores.FormatFechaHMS(tempRefParam9) + " and ");
                                stFechaToma = Convert.ToString(tempRefParam9);
                                //UPGRADE_WARNING: (1068) tempRefParam10 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam10 = stFechaToma2;
                                Sql.Append("fprevist <= " + Serrores.FormatFechaHMS(tempRefParam10) + " ) ");
                                stFechaToma2 = Convert.ToString(tempRefParam10);
                                Sql.Append("order by ftomasfa desc ");
                                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                                RrTomas = new DataSet();
                                tempAdapter_3.Fill(RrTomas);
                                if (RrTomas.Tables[0].Rows.Count != 0)
                                {
                                    //puede devolver mas de un registro                                    
                                    foreach (DataRow iteration_row_2 in RrTomas.Tables[0].Rows)
                                    {
                                        stFechaPrevista = Convert.ToDateTime(iteration_row_2["fprevist"]).ToString("dd/MM/yyyy HH:NN");                                        
                                        string tempRefParam11 = Convert.ToString(iteration_row_2["cdosisto"]);
                                        Cargar_grid(stFechaPrevista, ref tempRefParam11, (Convert.IsDBNull(iteration_row_2["ftomasfa"])) ? "" : Convert.ToString(iteration_row_2["ftomasfa"]), (Convert.IsDBNull(iteration_row_2["oobserva"])) ? "" : Convert.ToString(iteration_row_2["oobserva"]), Convert.ToString(iteration_row_2["gusuario"]));
                                    }
                                }                                
                            }        
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        //la hora se sale de intervalo entre la maxima y la minima
                    }
                }
            }
            catch (SqlException ex)
            {        
                Serrores.AnalizaError("Sueros.dll", Path.GetDirectoryName(Application.ExecutablePath), mbConfirmacionTomas.gUsuario, "Farmacos_sueros:Llenar_grid2", ex);
            }        
        }

        private void SprFarmacos_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            //int Col = eventArgs.Column;
            //int Row = eventArgs.Row;
            int Col = eventArgs.ColumnIndex;
            int Row = eventArgs.RowIndex;
            //cuando se selecciona un registro del spread se cargan los valores en los campos de texto
            //***********
            SprFarmacos.Row = Row;
            //se se selecciona el registro de cabecera o no se selecciona ninguno
            if (SprFarmacos.Row <= 0)
            {
                return;
            }

            if (lTomasSeleccionado == Row)
            {
                //si el seleccionado coincide con el seleccionado anterior
                if (fHayFilaSeleccionada)
                {
                    //dgmorenog
                    //SprFarmacos.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly; //Se desmarca la fila (Por la Banda)
                    fHayFilaSeleccionada = false;
                }
                else
                {
                    //dgmorenog
                    //SprFarmacos.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; //Se marca la fila
                    fHayFilaSeleccionada = true;
                }
            }
            else
            {
                //dgmorenog
                //SprFarmacos.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; //Se marca la fila
                fHayFilaSeleccionada = true;
            }

            if (fHayFilaSeleccionada)
            {
                //se ha seleccionado un registro
                iApunteToma = CONFIRMATOMA;
                lTomasSeleccionado = Row;
                SprFarmacos.Row = SprFarmacos.SelBlockRow;
                SprFarmacos.Col = 1;
                if (SprFarmacos.Text != "")
                { //si se ha seleccionado uno con datos
                    //cargar los datos del registro seleccionado en los campos de texto de la pantalla
                    SdcFechaPrevista.Value = DateTime.Parse(DateTime.Parse(SprFarmacos.Text).ToString("dd/MM/yyyy"));
                    tbHoraPrevista.Text = DateTime.Parse(SprFarmacos.Text).ToString("HH") + ":" + DateTime.Parse(SprFarmacos.Text).ToString("mm");

                    SprFarmacos.Col = 2;
                    //lbDosis = Trim(SprFarmacos.Text)
                    tbDosis.Text = SprFarmacos.Text.Trim();
                    SprFarmacos.Col = 3;
                    if (SprFarmacos.Text.Trim() == "")
                    {
                        //sdcFecha.Date = Format(vstFechaHoraSis, "dd/mm/yyyy")
                        //tbHora = Format(vstFechaHoraSis, "hh") & ":" & Format(vstFechaHoraSis, "nn")
                        sdcFecha.Value = DateTime.Parse("");
                        tbHora.Text = "  :  ";
                    }
                    else
                    {
                        sdcFecha.Value = DateTime.Parse(DateTime.Parse(SprFarmacos.Text).ToString("dd/MM/yyyy"));
                        tbHora.Text = DateTime.Parse(SprFarmacos.Text).ToString("HH") + ":" + DateTime.Parse(SprFarmacos.Text).ToString("mm");
                    }

                    SprFarmacos.Col = 4;
                    tbObservaciones.Text = SprFarmacos.Text.Trim();

                    SprFarmacos.Col = 5;
                    stUsuarioRegistro = SprFarmacos.Text.Trim();
                    if (iFarmacoEquivalente)
                    {
                        CargarFarmacoEquivalente();
                    }
                    else
                    {
                        lbFarmacoAdm.Visible = true;
                        lbFarmacoAdm.Text = lbNombreFarmaco.Text.Trim();
                    }
                    ProtegerPantalla("datos");
                }
            }
            else
            {
                //si no hay fila seleccionada se deben de limpiar los campos
                IniciarPantalla();
                ProtegerPantalla("vacio");

            }
        }

        private void DatosFarmaco()
        {
            int aij = 0;

            string Sql = "Select dunitoma from DUNITOMAVA " + " where gunitoma = " + mbConfirmacionTomas.farmacoSeleccion.CodTomas;

            SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mbConfirmacionTomas.RcEnfermeria);
            DataSet RrFarmaco = new DataSet();
            tempAdapter.Fill(RrFarmaco);
            if (RrFarmaco.Tables[0].Rows.Count != 0)
            {                
                lbUnidad.Text = (Convert.IsDBNull(RrFarmaco.Tables[0].Rows[0]["dunitoma"])) ? "" : Convert.ToString(RrFarmaco.Tables[0].Rows[0]["dunitoma"]).Trim();
            }
            else
            {
                lbUnidad.Text = "";
            }            
        }

        private void Cargar_grid(string stFechaPrevista, ref string stDosis, string stFechaConfirma, string stObservaciones, object stUsuario)
        {
            //se llenan el spread
            int iFila = 0;
            if (SprFarmacos.MaxRows == 0)
            {
                SprFarmacos.MaxRows = 1;
                iFila = SprFarmacos.MaxRows + 1;
            }
            else
            {
                SprFarmacos.MaxRows++;
                iFila = SprFarmacos.MaxRows + 1;
            }
            SprFarmacos.Row = iFila;
            SprFarmacos.Col = 1; //fecha/Hora prevista
            System.DateTime TempDate = DateTime.FromOADate(0);
            SprFarmacos.Text = (DateTime.TryParse(stFechaPrevista, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : stFechaPrevista;

            SprFarmacos.Col = 2; //dosis
            SprFarmacos.Text = Serrores.ConvertirDecimales(ref stDosis, ref mbConfirmacionTomas.vstSeparadorDecimal, 2);

            SprFarmacos.Col = 3; //Fecha/hora confirmacion
            System.DateTime TempDate2 = DateTime.FromOADate(0);
            SprFarmacos.Text = (DateTime.TryParse(stFechaConfirma, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm") : stFechaConfirma;

            SprFarmacos.Col = 4; //observaciones
            SprFarmacos.Text = stObservaciones;

            if (stUsuario != Type.Missing)
            {
                SprFarmacos.Col = 5; //viene el usuario que ha grabado el registro
                SprFarmacos.Text = stUsuario.ToString();
            }
            else
            {
                //no hay usuario
                SprFarmacos.Col = 5;
                SprFarmacos.Text = "";
            }
        }

        private void SprFarmacos_KeyDown(Object eventSender, KeyEventArgs eventArgs)
        {
            int KeyCode = (int)eventArgs.KeyCode;
            int Shift = (eventArgs.Shift) ? 1 : 0;
            if (KeyCode == ((int)Keys.Up))
            {
                if (SprFarmacos.ActiveRowIndex > 1)
                {
                    SprFarmacos_CellClick(SprFarmacos, null);
                }
                else
                {
                    SprFarmacos.Row = 1;
                    //dgmorenog
                    //SprFarmacos.OperationMode = FarPoint.Win.Spread.OperationMode.RowMode;
                    KeyCode = 0;
                }
            }

            if (KeyCode == ((int)Keys.Down))
            {
                if (SprFarmacos.ActiveRowIndex < SprFarmacos.GetLastNonEmptyRow(null))
                {
                    SprFarmacos_CellClick(SprFarmacos, null);
                }
                else
                {
                    SprFarmacos.Row = SprFarmacos.GetLastNonEmptyRow(null);
                    //dgmorenog
                    //SprFarmacos.OperationMode = FarPoint.Win.Spread.OperationMode.RowMode;
                    KeyCode = 0;
                }
            }

            if (KeyCode == ((int)Keys.PageUp))
            {
                if (SprFarmacos.ActiveRowIndex - 7 < 1)
                {
                    KeyCode = 0;
                }
                else
                {
                    SprFarmacos_CellClick(SprFarmacos, null);
                }
            }
            if (KeyCode == ((int)Keys.PageDown))
            {
                if (SprFarmacos.ActiveRowIndex + 7 > SprFarmacos.GetLastNonEmptyRow(null))
                {
                    SprFarmacos.Row = 1;
                    //dgmorenog
                    //SprFarmacos.OperationMode = FarPoint.Win.Spread.OperationMode.RowMode;
                    KeyCode = 0;
                }
                else
                {
                    SprFarmacos_CellClick(SprFarmacos, null);
                }
            }
        }

        private void IniciarPantalla()
        {
            //inicializar los campos de la pantalla
            lbFarmacoAdm.Text = "";
            lbFarmacoAdm.Visible = true;
            CbbFarmacoAdm.Clear();
            CbbFarmacoAdm.Visible = false;

            DatosFarmaco();
            //lbDosis.Caption = ""
            tbDosis.Text = "";

            SdcFechaPrevista.Value = DateTime.Now;
            sdcFecha.Value = DateTime.Parse(DateTime.Now.ToShortDateString());

            tbHoraPrevista.Text = "  :  ";
            tbHora.Text = "  :  ";
            tbObservaciones.Text = "";

            //seg�n el valor de gfProtegerCeldas se activan o no las celdas
            SdcFechaPrevista.Enabled = false;
            tbHoraPrevista.Enabled = false;
            //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprFarmacos.SelModeIndex was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //SprFarmacos.setSelModeIndex(0);
            stUsuarioRegistro = "";
            fHayFilaSeleccionada = false;
        }

        private void GrabarRegistro()
        {
            string sqlDatos = String.Empty;
            DataSet RrTomas = null;
            float iCantidadToma = 0;
            string iCantidadAux = String.Empty;

            SqlTransaction transaction = mbConfirmacionTomas.RcEnfermeria.BeginTrans();
            try
            {
                //cargo los datos del f�rmaco seleccionado para calcular la dosis que voy a imputar
                if (iFarmacoEquivalente)
                {
                    //hemos seleccionado el equivalente
                    if (Convert.ToDouble(CbbFarmacoAdm.get_ListIndex()) == 1)
                    {
                        //UPGRADE_WARNING: (1068) CbbFarmacoAdm.Column() of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                        object tempRefParam = 1;
                        object tempRefParam2 = CbbFarmacoAdm.get_ListIndex();
                        DatosDosisFarmaco(Convert.ToString(CbbFarmacoAdm.get_Column(tempRefParam, tempRefParam2)));
                    }
                    else
                    {
                        DatosDosisFarmaco(mbConfirmacionTomas.farmacoSeleccion.CodFarmaco);
                    }
                }
                else
                {
                    DatosDosisFarmaco(mbConfirmacionTomas.farmacoSeleccion.CodFarmaco);
                }

                //se comprueba que exista ya el registro
                //UPGRADE_WARNING: (1068) tempRefParam3 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                object tempRefParam3 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                object tempRefParam4 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                sqlDatos = "select * from ETOMASFA " + "where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' " + "and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " " + "and fapuntes =" + Serrores.FormatFechaHMS(tempRefParam3) + " " + "and gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' " + "and fprevist =" + Serrores.FormatFechaHMS(tempRefParam4) + " ";
                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam3);
                SqlCommand command = new SqlCommand(sqlDatos, mbConfirmacionTomas.RcEnfermeria, transaction);
                SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
                RrTomas = new DataSet();
                tempAdapter.Fill(RrTomas);
                DataSet rrAux = null;
                if (RrTomas.Tables[0].Rows.Count != 0)
                { //si encuentra el registro
                    //se puede modificar siempre que el usuario que lo ha grabado sea el mismo                    
                    if (tbObservaciones.Text.Trim() == "")
                    {                        
                        RrTomas.Tables[0].Rows[0]["oobserva"] = DBNull.Value;
                    }
                    else
                    {                        
                        RrTomas.Tables[0].Rows[0]["oobserva"] = tbObservaciones.Text.Trim();
                    }                    
                    string tempQuery = RrTomas.Tables[0].TableName;
                    tempAdapter.Update(RrTomas, tempQuery);
                    //que lo va a modificar
                }
                else
                {
                    //si no encuentra el registro hay que grabar uno nuevo                    
                    RrTomas.AddNew();                    
                    RrTomas.Tables[0].Rows[0]["itiposer"] = mbConfirmacionTomas.GstTiposervicio;                    
                    RrTomas.Tables[0].Rows[0]["ganoregi"] = mbConfirmacionTomas.GstA�oFarmaco;                    
                    RrTomas.Tables[0].Rows[0]["gnumregi"] = mbConfirmacionTomas.GstNumeroFarmaco;                    
                    RrTomas.Tables[0].Rows[0]["fapuntes"] = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;                    
                    RrTomas.Tables[0].Rows[0]["gfarmaco"] = mbConfirmacionTomas.farmacoSeleccion.CodFarmaco;
                    if (iFarmacoEquivalente)
                    {
                        //estamos en farmaco equivalente
                        if (Convert.ToDouble(CbbFarmacoAdm.get_ListIndex()) == 1)
                        {                            
                            //UPGRADE_WARNING: (1068) CbbFarmacoAdm.Column() of type Variant is being forced to Variant. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                            object tempRefParam5 = 1;
                            object tempRefParam6 = CbbFarmacoAdm.get_ListIndex();
                            RrTomas.Tables[0].Rows[0]["gfarmadm"] = CbbFarmacoAdm.get_Column(tempRefParam5, tempRefParam6);
                            //en el listindex = 1 siempre cargo el farmaco equivalente
                        }
                        else
                        {                            
                            //UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
                            RrTomas.Tables[0].Rows[0]["gfarmadm"] = DBNull.Value;
                        }
                    }
                    else
                    {                        
                        RrTomas.Tables[0].Rows[0]["gfarmadm"] = DBNull.Value;
                    }                    
                    RrTomas.Tables[0].Rows[0]["fprevist"] = DateTime.Parse(SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00");
                    if (sdcFecha.Text == "")
                    {                        
                        RrTomas.Tables[0].Rows[0]["ftomasfa"] = DBNull.Value;
                    }
                    else
                    {                        
                        RrTomas.Tables[0].Rows[0]["ftomasfa"] = DateTime.Parse(sdcFecha.Value.Date + " " + tbHora.Text + ":00");
                    }                    
                    RrTomas.Tables[0].Rows[0]["gUsuario"] = mbConfirmacionTomas.UsuarioConfirmacion.ToUpper();                    
                    RrTomas.Tables[0].Rows[0]["fpasfact"] = DBNull.Value;                    
                    string tempRefParam7 = tbDosis.Text;
                    RrTomas.Tables[0].Rows[0]["cdosisto"] = Serrores.ConvertirDecimales(ref tempRefParam7, ref mbConfirmacionTomas.stDecimalBD);
                    if (tbObservaciones.Text.Trim() == "")
                    {                        
                        RrTomas.Tables[0].Rows[0]["oobserva"] = DBNull.Value;
                    }
                    else
                    {                        
                        RrTomas.Tables[0].Rows[0]["oobserva"] = tbObservaciones.Text.Trim();
                    }
                    if (mbConfirmacionTomas.farmacoSeleccion.iPropPac.Trim().ToUpper() == "S")
                    {                        
                        RrTomas.Tables[0].Rows[0]["iPropPac"] = "S";
                    }
                    else
                    {                        
                        RrTomas.Tables[0].Rows[0]["iPropPac"] = "N";
                    }
                    //hay que calcular la dosis a imputar
                    if (iEnvaseFraccionable.Trim().ToUpper() == "N" && iMedidaFraccionable.Trim().ToUpper() == "S")
                    {
                        //comprobar                        
                        string tempRefParam8 = DosisAImputar(mbConfirmacionTomas.farmacoSeleccion.CodFarmaco, tbDosis.Text, stUnidadEquivalencia);
                        RrTomas.Tables[0].Rows[0]["cdosimpu"] = Serrores.ConvertirDecimales(ref tempRefParam8, ref mbConfirmacionTomas.stDecimalBD);
                    }
                    else
                    {
                        if (iMedidaFraccionable == "S")
                        {
                            //si la dosis es fraccionable seguimos como hasta ahora                            
                            string tempRefParam9 = tbDosis.Text;
                            RrTomas.Tables[0].Rows[0]["cdosimpu"] = Serrores.ConvertirDecimales(ref tempRefParam9, ref mbConfirmacionTomas.stDecimalBD);
                        }
                        else
                        {
                            //la dosis no es fraccionable hay que calcular la dosis a imputar
                            //la cantidad de toma la tengo que redondear a la unidad de medida
                            iCantidadToma = ((float)(Double.Parse(tbDosis.Text) / Double.Parse(stUnidadEquivalencia)));
                            if (iCantidadToma - ((float)Math.Floor(iCantidadToma)) == 0)
                            {
                                iCantidadToma = (float)(iCantidadToma * Double.Parse(stUnidadEquivalencia)); //para convertirlo a unidad de medida
                            }
                            else
                            {
                                iCantidadToma = (float)((((float)Math.Floor(iCantidadToma)) + 1) * Double.Parse(stUnidadEquivalencia)); //para convertirlo a unidad de medida
                            }
                            iCantidadAux = iCantidadToma.ToString();                            
                            RrTomas.Tables[0].Rows[0]["cdosimpu"] = Serrores.ConvertirDecimales(ref iCantidadAux, ref mbConfirmacionTomas.stDecimalBD);
                        }
                    }                    
                    string tempQuery_2 = RrTomas.Tables[0].TableName;                    
                    tempAdapter.Update(RrTomas, tempQuery_2);

                    //OSCAR C 24/10/2005
                    //Si el farmaco que para el que se confirma la toma es dosis unica, se finaliza el farmaco:
                    // - Se actualiza la fecha de movimiento
                    // - Se graba un nuevo registro
                    if (mbConfirmacionTomas.farmacoSeleccion.idounica.Trim().ToUpper() == "S")
                    {

                        //UPGRADE_WARNING: (1068) tempRefParam10 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                        object tempRefParam10 = mbConfirmacionTomas.farmacoSeleccion.FechaIni;
                        sqlDatos = "SELECT * FROM EFARMACO " +
                                   " WHERE " +
                                   "  itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and " +
                                   "  ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and " +
                                   "  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and " +
                                   "  finitrat = " + Serrores.FormatFechaHMS(tempRefParam10) + " and " +
                                   "  gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' and iesttrat='F'";
                        mbConfirmacionTomas.farmacoSeleccion.FechaIni = Convert.ToString(tempRefParam10);
                        SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sqlDatos, mbConfirmacionTomas.RcEnfermeria);
                        rrAux = new DataSet();
                        tempAdapter_4.Fill(rrAux);
                        if (rrAux.Tables[0].Rows.Count == 0)
                        {
                            //UPGRADE_WARNING: (1068) tempRefParam12 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                            object tempRefParam11 = DateTime.Parse(sdcFecha.Value.Date + " " + tbHora.Text).AddMinutes(1);
                            object tempRefParam12 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                            sqlDatos = "UPDATE EFARMACO SET " +
                                       "  ffinmovi = " + Serrores.FormatFechaHM(tempRefParam11) + ",  " +
                                       "  fprevist = Null , iestprev = Null " +
                                       " WHERE " +
                                       "  itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and " +
                                       "  ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and " +
                                       "  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and " +
                                       "  fapuntes = " + Serrores.FormatFechaHMS(tempRefParam12) + " and " +
                                       "  gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "'";
                            mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam12);

                            SqlCommand tempCommand = new SqlCommand(sqlDatos, mbConfirmacionTomas.RcEnfermeria, transaction);
                            tempCommand.ExecuteNonQuery();

                            //(26/10/2006)a la hora de insertar, se a�ade el campo "icoagula"
                            sqlDatos = " INSERT INTO EFARMACO " +
                                       " (itiposer, ganoregi, gnumregi, " +
                                       " fapuntes, " +
                                       " finitrat, gfarmaco, " +
                                       " gviadmin, gunitoma, iesttrat, gUsuario, cdiastra, gpersona, " +
                                       " ffinmovi, gmsuspro, fsuspens, " +
                                       " ffintrat, " +
                                       " oobserva, " +
                                       " cdosisfa00, cdosisfa01, cdosisfa02, cdosisfa03, cdosisfa04, cdosisfa05," +
                                       " cdosisfa06, cdosisfa07, cdosisfa08, cdosisfa09, cdosisfa10, cdosisfa11," +
                                       " cdosisfa12, cdosisfa13, cdosisfa14, cdosisfa15, cdosisfa16, cdosisfa17," +
                                       " cdosisfa18, cdosisfa19, cdosisfa20, cdosisfa21, cdosisfa22, cdosisfa23," +
                                       " vdiasfre, Periodic, iprecisa, fcubiped, cdosisac, sitadmtt," +
                                       " gfretoma, fprevist, iestprev, iLargTra, Fcubirec, ncodperf," +
                                       " nordfarm, veloperf, oobsperf, iPropPac, gsofarma, idounica, icoagula, indicinr, indittpa, gcodfarm) ";
                            //UPGRADE_WARNING: (1068) tempRefParam15 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                            object tempRefParam13 = DateTime.Parse(sdcFecha.Value.Date + " " + tbHora.Text).AddMinutes(1);
                            object tempRefParam14 = DateTime.Parse(sdcFecha.Value.Date + " " + tbHora.Text).AddMinutes(1);
                            object tempRefParam15 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                            sqlDatos = sqlDatos +
                                       " SELECT " +
                                       " '" + mbConfirmacionTomas.GstTiposervicio + "'," + mbConfirmacionTomas.GstA�oFarmaco + ", " + mbConfirmacionTomas.GstNumeroFarmaco + ", " +
                                       Serrores.FormatFechaHM(tempRefParam13) + ", " +
                                       " finitrat, gfarmaco, " +
                                       " gviadmin, gunitoma, 'F', gUsuario, cdiastra, gpersona, " +
                                       " (NULL), (SELECT NNUMERI1 FROM SCONSGLO WHERE GCONSGLO='GFINFARM'), (NULL), " +
                                       Serrores.FormatFechaHM(tempRefParam14) + ", " +
                                       " oobserva, " +
                                       " cdosisfa00, cdosisfa01, cdosisfa02, cdosisfa03, cdosisfa04, cdosisfa05," +
                                       " cdosisfa06, cdosisfa07, cdosisfa08, cdosisfa09, cdosisfa10, cdosisfa11," +
                                       " cdosisfa12, cdosisfa13, cdosisfa14, cdosisfa15, cdosisfa16, cdosisfa17," +
                                       " cdosisfa18, cdosisfa19, cdosisfa20, cdosisfa21, cdosisfa22, cdosisfa23," +
                                       " vdiasfre, Periodic, iprecisa, fcubiped, cdosisac, sitadmtt," +
                                       " gfretoma, fprevist, iestprev, iLargTra, Fcubirec, ncodperf," +
                                       " nordfarm, veloperf, oobsperf, iPropPac, gsofarma, idounica, icoagula, indicinr, indittpa, gcodfarm " +
                                       " FROM EFARMACO " +
                                       " WHERE " +
                                       "  itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and " +
                                       "  ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and " +
                                       "  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and " +
                                       "  fapuntes = " + Serrores.FormatFechaHMS(tempRefParam15) + " and " +
                                       "  gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "'";
                            mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam15);

                            SqlCommand tempCommand_2 = new SqlCommand(sqlDatos, mbConfirmacionTomas.RcEnfermeria, transaction);
                            tempCommand_2.ExecuteNonQuery();

                        }                        
                    }
                    //--------

                }
                
                mbConfirmacionTomas.RcEnfermeria.CommitTrans(transaction);
            }
            catch
            {
                mbConfirmacionTomas.RcEnfermeria.RollbackTrans(transaction);
                RadMessageBox.Show("error al grabar ", Application.ProductName);
            }
            finally
            {
                transaction.Dispose();
            }
        }

        private void FechasPantalla()
        {
            //procedimiento para cargar las fecha de la pantalla
            SdcFechaPrevista.Value = DateTime.Now;
            sdcFecha.Value = DateTime.Now;

            SdcFechaPrevista.MinDate = DateTime.Parse("01/01/1753");
            SdcFechaPrevista.MaxDate = DateTime.Parse("31/12/9998");

            sdcFecha.MinDate = DateTime.Parse("01/01/1753");
            sdcFecha.MaxDate = DateTime.Parse("31/12/9998");

            //sdcFecha.MinDate = Format(GstConfirmaMaxima, "dd/mm/yyyy")
            //SdcFechaPrevista.MinDate = Format(GstConfirmaMaxima, "dd/mm/yyyy")
            //la fecha maxima no puede ser mayor que la fecha en la que estamos
            System.DateTime TempDate = DateTime.FromOADate(0);

            sdcFecha.MaxDate = DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : mbConfirmacionTomas.vstFechaHoraSis);
            //SdcFechaPrevista.MaxDate = Format(vstFechaHoraSis, "dd/mm/yyyy")

        }

        private void tbDosis_Enter(Object eventSender, EventArgs eventArgs)
        {
            tbDosis.SelectionStart = 0;
            tbDosis.SelectionLength = Strings.Len(tbDosis.Text);

        }

        private void tbDosis_KeyDown(Object eventSender, KeyEventArgs eventArgs)
        {
            int KeyCode = (int)eventArgs.KeyCode;
            int Shift = (eventArgs.Shift) ? 1 : 0;
            if (KeyCode == 8 && tbDosis.Text.IndexOf(',') >= 0 && (tbDosis.Text.IndexOf(',') + 1) == tbDosis.SelectionStart)
            {
                tbDosis.Text = tbDosis.Text.Substring(0, Math.Min(tbDosis.SelectionStart, tbDosis.Text.Length));
                tbDosis.SelectionStart = Strings.Len(tbDosis.Text);
            }
        }

        private void tbDosis_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {
            int KeyAscii = Strings.Asc(eventArgs.KeyChar);
            //cuidado hay que controlar la longitud que nos va a dejar marcar
            int viPosSep = 0, viPosCursor = 0;
            if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8 && KeyAscii != 9 && KeyAscii != Strings.Asc(mbConfirmacionTomas.vstSeparadorDecimal[0]))
            { //si no son numeros,ni tabulador, ni retroceso,ni punto, ni coma no le dejo
                KeyAscii = 0; // retrocede1
            }
            else
            {
                viPosSep = (tbDosis.Text.IndexOf(mbConfirmacionTomas.vstSeparadorDecimal) + 1);
                viPosCursor = tbDosis.SelectionStart;
                //Si tiene separador decimal,dos decimales, las teclas no son ni retroceso,ni separador y la posici�n es posterior al separador entonces no hace nada
                if (viPosSep != 0 && tbDosis.Text.Substring(viPosSep).Length == 2 && KeyAscii != 8 && KeyAscii != 9 && viPosSep <= viPosCursor)
                {
                    KeyAscii = 0;
                }

                //Si tiene separador decimal y la tecla es el separador entonces no hace nada
                if (viPosSep != 0 && KeyAscii == Strings.Asc(mbConfirmacionTomas.vstSeparadorDecimal[0]))
                {
                    KeyAscii = 0;
                }

                //Si tiene separador decimal,tres enteros, las teclas no son ni retroceso,ni separador y la posici�n es anterior al separador entonces no hace nada
                if (viPosSep != 0)
                {
                    if (tbDosis.Text.Substring(0, Math.Min(viPosSep - 1, tbDosis.Text.Length)).Length == 5 && KeyAscii != 8 && KeyAscii != 9 && viPosSep > viPosCursor)
                    {
                        KeyAscii = 0;
                    }
                }

                //Si no tiene separador decimal,tres enteros y las teclas no son ni retroceso,ni separador
                if (viPosSep == 0 && Strings.Len(tbDosis.Text) == 5 && KeyAscii != 8 && KeyAscii != 9 && KeyAscii != Strings.Asc(mbConfirmacionTomas.vstSeparadorDecimal[0]) && tbDosis.SelectionLength == 0)
                {
                    KeyAscii = 0;
                }
            }

            if (KeyAscii == 0)
            {
                eventArgs.Handled = true;
            }
            eventArgs.KeyChar = Convert.ToChar(KeyAscii);
        }

        private void tbDosis_Leave(Object eventSender, EventArgs eventArgs)
        {
            if (tbDosis.Text.Trim() != "")
            {
                string tempRefParam = tbDosis.Text;
                tbDosis.Text = Serrores.ConvertirDecimales(ref tempRefParam, ref mbConfirmacionTomas.vstSeparadorDecimal, 2);
            }
            else
            {
                tbDosis.Text = "";
            }
        }

        private void tbHora_Enter(Object eventSender, EventArgs eventArgs)
        {
            tbHora.SelectionStart = 0;
            tbHora.SelectionLength = Strings.Len(tbHora.Text);
        }

        private void tbHora_Leave(Object eventSender, EventArgs eventArgs)
        {
            int iHora = 0;
            int iMinutos = 0;
            int iRespuesta = 0;

            if (tbHora.Text.Trim() != "" && tbHora.Text.Trim() != ":")
            {
                //tbHora.Text = Format$(tbHora.Text, "hh:mm")
                iHora = Convert.ToInt32(Conversion.Val(tbHora.Text.Substring(0, Math.Min(tbHora.Text.IndexOf(':'), tbHora.Text.Length))));
                iMinutos = Convert.ToInt32(Conversion.Val(tbHora.Text.Substring(tbHora.Text.IndexOf(':') + 1, Math.Min(2, tbHora.Text.Length - (tbHora.Text.IndexOf(':') + 1)))));

                if (iHora < 0 || iHora > 23)
                {
                    short tempRefParam = 1150;
                    string[] tempRefParam2 = new string[] { "Hora real" };
                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbConfirmacionTomas.RcEnfermeria, tempRefParam2);
                    //MsgBox "la hora tecleada no es correcta", vbOKOnly & vbCritical, Me.Caption
                    tbHora.Focus();
                    return;
                }
                if (iMinutos < 0 || iMinutos > 59)
                {
                    short tempRefParam3 = 1150;
                    string[] tempRefParam4 = new string[] { "Hora real" };
                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbConfirmacionTomas.RcEnfermeria, tempRefParam4);
                    //MsgBox "los minutos tecleados no son correctos", vbOKOnly & vbCritical, Me.Caption
                    tbHora.Focus();
                    return;
                }
            }

        }

        private void tbHoraPrevista_Enter(Object eventSender, EventArgs eventArgs)
        {
            tbHoraPrevista.SelectionStart = 0;
            tbHoraPrevista.SelectionLength = Strings.Len(tbHoraPrevista.Text);
        }

        private void tbHoraPrevista_Leave(Object eventSender, EventArgs eventArgs)
        {
            int iHora = 0;
            int iMinutos = 0;
            int iRespuesta = 0;

            if (tbHoraPrevista.Text.Trim() != "" && tbHoraPrevista.Text.Trim() != ":")
            {
                //tbhoraPrevista.Text = Format$(tbhoraPrevista.Text, "hh:mm")
                iHora = Convert.ToInt32(Conversion.Val(tbHora.Text.Substring(0, Math.Min(tbHora.Text.IndexOf(':'), tbHora.Text.Length))));
                iMinutos = Convert.ToInt32(Conversion.Val(tbHoraPrevista.Text.Substring(tbHoraPrevista.Text.IndexOf(':') + 1, Math.Min(2, tbHoraPrevista.Text.Length - (tbHoraPrevista.Text.IndexOf(':') + 1)))));

                if (iHora < 0 || iHora > 23)
                {
                    short tempRefParam = 1150;
                    string[] tempRefParam2 = new string[] { "Hora prevista" };
                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbConfirmacionTomas.RcEnfermeria, tempRefParam2);
                    //MsgBox "la hora tecleada no es correcta", vbOKOnly & vbCritical, Me.Caption
                    tbHoraPrevista.Focus();
                    return;
                }
                if (iMinutos < 0 || iMinutos > 59)
                {
                    short tempRefParam3 = 1150;
                    string[] tempRefParam4 = new string[] { "Hora prevista" };
                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbConfirmacionTomas.RcEnfermeria, tempRefParam4);
                    //MsgBox "los minutos tecleados no son correctos", vbOKOnly & vbCritical, Me.Caption
                    tbHoraPrevista.Focus();
                    return;
                }
            }
            else
            {
                return;
            }
            //si ha pasado las validaciones de hora tendra que buscar si en ese intervalo
            //hay carrito con equivalente
            if (iFarmacoEquivalente)
            {
                CargarFarmacoEquivalente();
            }
            else
            {
                CbbFarmacoAdm.Visible = false;
                CbbFarmacoAdm.Clear();
                lbFarmacoAdm.Visible = true;
                lbFarmacoAdm.Text = lbNombreFarmaco.Text.Trim();
            }
        }

        private void tbObservaciones_Enter(Object eventSender, EventArgs eventArgs)
        {
            tbObservaciones.SelectionStart = 0;
            tbObservaciones.SelectionLength = Strings.Len(tbObservaciones.Text);
        }

        //(maplaza)(25/10/2006)Esta funci�n dice si, cuando hay un registro (toma) cargado en el listado, este se corresponde
        //con una toma futura o no.
        private bool TomaFutura()
        {
            bool result = false;
            string stFechaReferencia = String.Empty;

            if (SprFarmacos.MaxRows == 1)
            {
                if (DateTime.Parse(mbConfirmacionTomas.GstConfirmaMaxima) < DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis))
                {
                    stFechaReferencia = mbConfirmacionTomas.GstConfirmaMaxima;
                }
                else
                {
                    stFechaReferencia = mbConfirmacionTomas.vstFechaHoraSis;
                }

                SprFarmacos.Row = 1;
                SprFarmacos.Col = 1; //fecha/Hora prevista
                if (SprFarmacos.Text.Trim() != "")
                {
                    System.DateTime TempDate = DateTime.FromOADate(0);
                    if (DateTime.Parse(DateTime.Parse(SprFarmacos.Text).ToString("dd/MM/yyyy HH:mm")) > DateTime.Parse((DateTime.TryParse(stFechaReferencia, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : stFechaReferencia))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        private void ProtegerPantalla(string stForma)
        {
            //se presenta la pantalla de confirmacion de tomas, hasta ahora solo se podian confirmar
            //tomas de los farmacos que esten iniciados pero hay que dejar confirmar tomas
            //de aquellos farmacos que tengan fecha fin de movimiento
            //hay que tener cuidado con los farmacos si precisa
            //if iEstadoAccion =

            //(maplaza)(17/11/2006)En el caso de que vfAusencia=True, no se deben poder confirmar tomas, as� que se ha
            //a�adido la condici�n "Or (vfAusencia = True)"
            if ((mbConfirmacionTomas.gfProtegerCeldas) || (vfAusencia))
            { //no se pueden confirmar tomas
                tbDosis.Enabled = false;
                sdcFecha.Enabled = false;
                tbHora.Enabled = false;
                tbObservaciones.Enabled = false;
                cbAceptar.Enabled = false;
                cbNuevo.Enabled = false;
                cbAnular.Enabled = false;
            }
            else
            {
                //se pueden confirmar tomas
                if (stUsuarioRegistro.Trim() != "")
                {
                    //el registro ya esta grabado => solo lo puede modificar el usuario que ha
                    //grabado el registro y solo pued modificar las observaciones
                    tbDosis.Enabled = false;
                    sdcFecha.Enabled = false;
                    tbHora.Enabled = false;
                    cbNuevo.Enabled = false;
                    cbAnular.Enabled = true;
                    if (stUsuarioRegistro.Trim().ToUpper() != mbConfirmacionTomas.UsuarioConfirmacion.Trim().ToUpper())
                    {
                        //no se puede modificar nada
                        tbObservaciones.Enabled = false;
                        cbAceptar.Enabled = false;
                    }
                    else
                    {
                        tbObservaciones.Enabled = true;
                        cbAceptar.Enabled = true;
                    }
                }
                else
                {
                    //no se ha grabado el registro
                    switch (stForma)
                    {
                        case "vacio":  //cuando se limpia la pantalla 
                            tbDosis.Enabled = false;
                            sdcFecha.Enabled = false;
                            tbHora.Enabled = false;
                            tbObservaciones.Enabled = false;
                            cbAceptar.Enabled = false;
                            cbAnular.Enabled = false;
                            if (mbConfirmacionTomas.farmacoSeleccion.SiPrecisa == "S")
                            {
                                //Bot�n "Nuevo" debe estar activo cuando f�rmaco es "Si Precisa"
                                cbNuevo.Enabled = true;
                            }
                            else
                            {
                                if (mbConfirmacionTomas.iEstadoAccion == mbConfirmacionTomas.NOCAMBIOS)
                                {
                                    cbNuevo.Enabled = false;
                                }
                                else
                                {
                                    //si es un farmaco normal
                                    //si hay registros en el spread tanto confirmados como no
                                    //se tendra que mostrar el bot�n Nuevo inhabilitado
                                    if (SprFarmacos.MaxRows > 0)
                                    {
                                        //(maplaza)(25/10/2006)
                                        //cbNuevo.Enabled = False
                                        //(maplaza)(31/10/2006)En el caso de dosis �nica, no debe activarse el bot�n "Nuevo" en
                                        //ninguno de los casos.
                                        if (mbConfirmacionTomas.farmacoSeleccion.idounica == "S")
                                        {
                                            cbNuevo.Enabled = false;
                                            //-----
                                        }
                                        else if ((SprFarmacos.MaxRows == 1 && TomaFutura()))
                                        {
                                            cbNuevo.Enabled = true;
                                        }
                                        else
                                        {
                                            cbNuevo.Enabled = false;
                                        }
                                        //-----
                                    }
                                    else
                                    {
                                        //si no hay registros habra que tener en cuenta si es la primera
                                        //toma ya se ha realizado alguna toma

                                        //(maplaza)(31/10/2006)En el caso de dosis �nica, no debe activarse el bot�n "Nuevo" en
                                        //ninguno de los casos.
                                        if (mbConfirmacionTomas.farmacoSeleccion.idounica == "S")
                                        {
                                            cbNuevo.Enabled = false;
                                            //-----
                                        }
                                        else if (TomaNueva() && SprFarmacos.MaxRows == 0)
                                        {
                                            cbNuevo.Enabled = true;
                                        }
                                        else
                                        {
                                            cbNuevo.Enabled = false;
                                        }
                                    }
                                }
                            }
                            break;
                        case "nuevo":  //cuando se pulsa el bot�n nuevo 
                            tbDosis.Enabled = true;
                            SdcFechaPrevista.Enabled = true;
                            tbHoraPrevista.Enabled = true;
                            sdcFecha.Enabled = true;
                            tbHora.Enabled = true;
                            tbObservaciones.Enabled = true;
                            cbAceptar.Enabled = true;
                            cbAnular.Enabled = false;
                            break;
                        default: //cuando se selecciona un registro del spread para ver los datos 
                            if (iModificarDosis)
                            {
                                tbDosis.Enabled = true;
                            }
                            sdcFecha.Enabled = true;
                            tbHora.Enabled = true;
                            tbObservaciones.Enabled = true;
                            cbAceptar.Enabled = true;
                            cbAnular.Enabled = false;
                            if (fHayFilaSeleccionada)
                            {
                                cbNuevo.Enabled = false;
                                cbAnular.Enabled = !(tbObservaciones.Text.Trim() == "" && tbHora.Text.Trim() == ":");
                            }
                            else
                            {
                                cbAnular.Enabled = false;
                                if (mbConfirmacionTomas.farmacoSeleccion.SiPrecisa == "S")
                                {
                                    //Bot�n "Nuevo" debe estar activo cuando f�rmaco es "Si Precisa"
                                    cbNuevo.Enabled = true;
                                }
                                else
                                {
                                    //(maplaza)(31/10/2006)En el caso de dosis �nica, no debe activarse el bot�n "Nuevo" en
                                    //ninguno de los casos.
                                    if (mbConfirmacionTomas.farmacoSeleccion.idounica == "S")
                                    {
                                        cbNuevo.Enabled = false;
                                    }                                    
                                }
                            }
                            break;
                    }
                }
            }
            cbSalir.Focus();
        }

        public string DiasPrevistos(string stFechaBucle)
        {
            //funcion que tiene que ver si el dia en el que estamos coincide con una toma
            string stFechaCalculada = String.Empty;
            int iDiaSemana = 0;

            StringBuilder sDiasPrevistos = new StringBuilder();

            if (mbConfirmacionTomas.farmacoSeleccion.Periodic > 0)
            { //existe periodicidad
                stFechaCalculada = mbConfirmacionTomas.farmacoSeleccion.FechaIni;
                System.DateTime TempDate2 = DateTime.FromOADate(0);
                System.DateTime TempDate = DateTime.FromOADate(0);
                while (DateTime.Parse((DateTime.TryParse(stFechaCalculada, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : stFechaCalculada) <= DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy") : stFechaBucle))
                {
                    System.DateTime TempDate4 = DateTime.FromOADate(0);
                    System.DateTime TempDate3 = DateTime.FromOADate(0);
                    if (DateTime.Parse((DateTime.TryParse(stFechaCalculada, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : stFechaCalculada) < DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy") : stFechaBucle))
                    {
                        //mientras que la fecha sea menor que la fecha de busqueda
                        //hay que sumar un periodo a la fechacalculada
                        stFechaCalculada = DateTimeHelper.ToString(DateTime.FromOADate(mbConfirmacionTomas.farmacoSeleccion.Periodic).AddDays(DateTime.Parse(stFechaCalculada).ToOADate()));
                        System.DateTime TempDate5 = DateTime.FromOADate(0);
                        stFechaCalculada = stFechaCalculada + " " + ((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaIni, out TempDate5)) ? TempDate5.ToString("HH:NN:SS") : mbConfirmacionTomas.farmacoSeleccion.FechaIni);
                    }
                    else
                    {
                        System.DateTime TempDate7 = DateTime.FromOADate(0);
                        System.DateTime TempDate6 = DateTime.FromOADate(0);
                        if (DateTime.Parse((DateTime.TryParse(stFechaCalculada, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy") : stFechaCalculada) == DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy") : stFechaBucle))
                        {
                            //ya hemos encontrado la fecha
                            iDiaSemana = DateAndTime.Weekday(DateTime.Parse(stFechaCalculada), FirstDayOfWeek.Monday);
                            for (int aik = 1; aik <= 7; aik++)
                            { //'poner la marca
                                if (iDiaSemana == aik)
                                {
                                    sDiasPrevistos.Append("*");
                                }
                                else
                                {
                                    sDiasPrevistos.Append(" ");
                                }
                            }
                        }

                        stFechaCalculada = DateTimeHelper.ToString(DateTime.FromOADate(mbConfirmacionTomas.farmacoSeleccion.Periodic).AddDays(DateTime.Parse(stFechaCalculada).ToOADate()));
                        System.DateTime TempDate8 = DateTime.FromOADate(0);
                        stFechaCalculada = stFechaCalculada + " " + ((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaIni, out TempDate8)) ? TempDate8.ToString("HH:NN:SS") : mbConfirmacionTomas.farmacoSeleccion.FechaIni);
                    }
                }
            }
            return sDiasPrevistos.ToString();
        }

        private void Llenar_grid()
        {
            StringBuilder Sql = new StringBuilder();
            DataSet RrTomas = null;
            //Dim colorapunte As Long
            //Dim RrDescripcion As rdoResultset
            string sDiasPrevistos = String.Empty; //lleva asteriscos en los dias que tocan tomas
            int iDiaSemana = 0;
            string stFechaInicioBucle = String.Empty;
            string stFechafinBucle = String.Empty;
            string stFechaBucle = String.Empty;
            int iHoraIni = 0;
            int iHoraFin = 0;
            string stFechaToma = String.Empty;
            string stFechaPrevista = String.Empty;
            //(maplaza)(20/03/2007)
            bool bTomaFuturaEnDiaActual = false;
            bool bTomaFuturaDiasSiguientes = false;
            //-----
            //(maplaza)(19/04/2007)
            string strInsulina = String.Empty;
            bool bGrupoInsulina = false;
            //-----

            try
            {

                //se llenan con las horas prevista de las ultimas 48 horas
                //si a esa hora hay un registro en etomasfa se rellena las casillas del spread con
                //los valores del registro, sino hay registro se rellena a blancos
                //hay que hacer una rutina que calcule las horas previstas
                //GstConfirmaMaxima lleva la hora del sistema
                //GstConfirmaMinima lleva 48 horas antes de la fecha del sistema o la de inicio del tratamiento

                if (DateTime.Parse(mbConfirmacionTomas.GstConfirmaMaxima) < DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis))
                {
                    stFechaInicioBucle = mbConfirmacionTomas.GstConfirmaMaxima;
                }
                else
                {
                    stFechaInicioBucle = mbConfirmacionTomas.vstFechaHoraSis;
                }
                //hay que buscar 48 horas antes

                stFechafinBucle = DateTimeHelper.ToString(DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis).AddHours(-48));

                if (DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima) > DateTime.Parse(stFechafinBucle))
                {
                    stFechafinBucle = mbConfirmacionTomas.GstConfirmaMinima;
                }
                else
                {
                    stFechafinBucle = DateTimeHelper.ToString(DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis).AddHours(-48));
                }

                System.DateTime TempDate = DateTime.FromOADate(0);
                if (Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate)) ? TempDate.ToString("NN") : stFechafinBucle) > 0)
                {
                    System.DateTime TempDate2 = DateTime.FromOADate(0);
                    if (Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate2)) ? TempDate2.ToString("HH") : stFechafinBucle) == 23)
                    {
                        //comprobar lo que devuelve
                        stFechafinBucle = DateTimeHelper.ToString(DateTime.FromOADate(1).AddDays(DateTime.Parse(stFechafinBucle).ToOADate())) + " 00:00";
                    }
                    else
                    {
                        //stFechaFinBucle = Format(stFechaFinBucle, "DD/MM/YYYY") & " " & Format(Val(Format(stFechaFinBucle, "hh")) + 1, "00") & ":00"
                        System.DateTime TempDate4 = DateTime.FromOADate(0);
                        System.DateTime TempDate3 = DateTime.FromOADate(0);
                        stFechafinBucle = ((DateTime.TryParse(stFechafinBucle, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : stFechafinBucle) + " " + StringsHelper.Format(Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate4)) ? TempDate4.ToString("HH") : stFechafinBucle), "00") + ":00";
                    }
                }

                //(maplaza)(19/04/2007)Se obtiene la informaci�n de si el F�rmaco pertenece al Grupo Terap�utico "Insulinas"
                strInsulina = mbAcceso.ObtenerCodigoSConsglo("INSULINA", "valfanu1", mbConfirmacionTomas.RcEnfermeria);
                if (strInsulina != "")
                {
                    if (strInsulina == mbConfirmacionTomas.farmacoSeleccion.ggrutera.Substring(0, Math.Min(strInsulina.Length, mbConfirmacionTomas.farmacoSeleccion.ggrutera.Length)))
                    {
                        bGrupoInsulina = true;
                    }
                }
                //-----

                //tengo que cargar la fecha inicial hasta la fecha final
                //el bucle empieza en la hora actual y va hacia abajo
                for (int aik = 0; aik <= 2; aik++)
                {
                    //le descuento dias porque son dias completos desde 0 a 23 horas
                    stFechaBucle = DateTimeHelper.ToString(DateTime.FromOADate(-aik).AddDays(DateTime.Parse(stFechaInicioBucle).ToOADate())); //esta la fecha del bucle "DD/MM/YYYY"

                    //asi obtengo el dia de la fecha del bucle
                    System.DateTime TempDate6 = DateTime.FromOADate(0);
                    System.DateTime TempDate5 = DateTime.FromOADate(0);
                    if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy") : stFechaBucle) >= DateTime.Parse((DateTime.TryParse(stFechafinBucle, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy") : stFechafinBucle) && DateTime.Parse(stFechaBucle) <= DateTime.Parse(stFechaInicioBucle))
                    {
                        if (mbConfirmacionTomas.farmacoSeleccion.SiPrecisa == "S")
                        {
                            //tiene que cargar todas las tomas que se encuentre en la fecha del bucle
                            System.DateTime TempDate7 = DateTime.FromOADate(0);
                            stFechaToma = (DateTime.TryParse(stFechaBucle, out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy") : stFechaBucle;
                            //buscamos las tomas que hay en la hora
                            Sql = new StringBuilder("select * from ETOMASFA where ");
                            Sql.Append("itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and ");
                            Sql.Append("ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and ");
                            Sql.Append("gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and ");
                            //UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                            object tempRefParam = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                            Sql.Append("fapuntes = " + Serrores.FormatFechaHMS(tempRefParam) + " and ");
                            mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam);
                            Sql.Append("gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco.Trim() + "' and ");
                            object tempRefParam2 = stFechaToma + " 00:00:00";
                            Sql.Append("( fprevist >= " + Serrores.FormatFechaHMS(tempRefParam2) + " ");
                            object tempRefParam3 = stFechaToma + " 23:59:59";
                            Sql.Append("and fprevist <= " + Serrores.FormatFechaHMS(tempRefParam3) + ") ");
                            Sql.Append("order by fprevist DESC ");

                            SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                            RrTomas = new DataSet();
                            tempAdapter.Fill(RrTomas);
                            //puede haber mas de una?
                            if (RrTomas.Tables[0].Rows.Count != 0)
                            {
                                //UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
                                RrTomas.MoveFirst();
                                foreach (DataRow iteration_row in RrTomas.Tables[0].Rows)
                                {
                                    //si hay un registro se muestra
                                    stFechaPrevista = Convert.ToDateTime(iteration_row["fprevist"]).ToString("dd/MM/yyyy HH:NN");                                    
                                    string tempRefParam4 = Convert.ToString(iteration_row["cdosisto"]);
                                    Cargar_grid(stFechaPrevista, ref tempRefParam4, (Convert.IsDBNull(iteration_row["ftomasfa"])) ? "" : Convert.ToString(iteration_row["ftomasfa"]), (Convert.IsDBNull(iteration_row["oobserva"])) ? "" : Convert.ToString(iteration_row["oobserva"]), Convert.ToString(iteration_row["gusuario"]));
                                }
                            }
                            else
                            {
                            }
                        }
                        else
                        {
                            //si es un farmaco diferente a si precisa tiene que cargar las tomas previstas
                            // y aquellas que se den antes
                            if (mbConfirmacionTomas.farmacoSeleccion.Periodic > 0)
                            {
                                //tenemos que mirar si la fecha del bucle coincide con una fecha de toma
                                //BuscarFechaToma
                                System.DateTime TempDate9 = DateTime.FromOADate(0);
                                System.DateTime TempDate8 = DateTime.FromOADate(0);
                                stFechaBucle = ((DateTime.TryParse(stFechaBucle, out TempDate8)) ? TempDate8.ToString("dd/MM/yyyy") : stFechaBucle) + " " + ((DateTime.TryParse(stFechaInicioBucle, out TempDate9)) ? TempDate9.ToString("HH:NN") : stFechaInicioBucle);
                                sDiasPrevistos = DiasPrevistos(stFechaBucle);
                            }
                            else
                            {
                                //miramos si en esta fecha hay tomas
                                sDiasPrevistos = mbConfirmacionTomas.farmacoSeleccion.VsDias;
                                //hay que controlar que la fecha este en los limites entre la maxima y la minimima posible
                            }
                            if (sDiasPrevistos.Trim() != "")
                            { //hay periodicidad
                                iDiaSemana = DateAndTime.Weekday(DateTime.Parse(stFechaBucle), FirstDayOfWeek.Monday); //'devuelve el dia de la semana que es hoy
                                //si el dia de la semana en el que estoy hay tomas previstas
                                if (sDiasPrevistos.Substring(iDiaSemana - 1, Math.Min(1, sDiasPrevistos.Length - (iDiaSemana - 1))) != " ")
                                {
                                    //hay tomas ese dia bucle de horas
                                    System.DateTime TempDate11 = DateTime.FromOADate(0);
                                    System.DateTime TempDate10 = DateTime.FromOADate(0);
                                    if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate10)) ? TempDate10.ToString("dd/MM/yyyy") : stFechaBucle) == DateTime.Parse((DateTime.TryParse(stFechaInicioBucle, out TempDate11)) ? TempDate11.ToString("dd/MM/yyyy") : stFechaInicioBucle))
                                    {
                                        System.DateTime TempDate12 = DateTime.FromOADate(0);
                                        iHoraIni = Convert.ToInt32(Double.Parse((DateTime.TryParse(stFechaInicioBucle, out TempDate12)) ? TempDate12.ToString("HH") : stFechaInicioBucle));
                                        //puede que el inicio y fin del bucle este en el mismo dia
                                        System.DateTime TempDate14 = DateTime.FromOADate(0);
                                        System.DateTime TempDate13 = DateTime.FromOADate(0);
                                        if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate13)) ? TempDate13.ToString("dd/MM/yyyy") : stFechaBucle) == DateTime.Parse((DateTime.TryParse(stFechafinBucle, out TempDate14)) ? TempDate14.ToString("dd/MM/yyyy") : stFechafinBucle))
                                        {
                                            System.DateTime TempDate15 = DateTime.FromOADate(0);
                                            iHoraFin = Convert.ToInt32(Double.Parse((DateTime.TryParse(stFechafinBucle, out TempDate15)) ? TempDate15.ToString("HH") : stFechafinBucle));
                                        }
                                        else
                                        {
                                            iHoraFin = 0;
                                        }
                                    }
                                    else
                                    {
                                        //la fecha del bucle es inferior a la fecha de de inicio del bucle
                                        iHoraIni = 23;
                                        //si la fecha del bucle es igual que la fecha de fin del bucle se carga la hora del fin del bucle
                                        System.DateTime TempDate17 = DateTime.FromOADate(0);
                                        System.DateTime TempDate16 = DateTime.FromOADate(0);
                                        if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate16)) ? TempDate16.ToString("dd/MM/yyyy") : stFechaBucle) == DateTime.Parse((DateTime.TryParse(stFechafinBucle, out TempDate17)) ? TempDate17.ToString("dd/MM/yyyy") : stFechafinBucle))
                                        {
                                            //si la hora es mayor que Hora:00
                                            System.DateTime TempDate18 = DateTime.FromOADate(0);
                                            if (Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate18)) ? TempDate18.ToString("mm") : stFechafinBucle) > 0)
                                            {
                                                System.DateTime TempDate19 = DateTime.FromOADate(0);
                                                iHoraFin = Convert.ToInt32(Double.Parse((DateTime.TryParse(stFechafinBucle, out TempDate19)) ? TempDate19.ToString("HH") : stFechafinBucle) + 1);
                                            }
                                            else
                                            {
                                                System.DateTime TempDate20 = DateTime.FromOADate(0);
                                                iHoraFin = Convert.ToInt32(Double.Parse((DateTime.TryParse(stFechafinBucle, out TempDate20)) ? TempDate20.ToString("HH") : stFechafinBucle));
                                            }
                                        }
                                        else
                                        {
                                            //en el resto de los casos considero el dia completo
                                            iHoraFin = 0;
                                        }
                                    }
                                    //End If

                                    //(maplaza)(21/08/2006)Importante: Debido a la modificaci�n que se hizo para que se pudiesen adelantar las
                                    //tomas ser�a necesario tambi�n cargar la siguiente toma prevista no confirmada posterior a la hora m�xima.
                                    //Esta hora m�xima viene representada en la variable "stFechaInicioBucle".
                                    //Ejemplo: la hora m�xima son las 9:00. Si la siguiente toma prevista tiene como hora prevista las 10:00 (superior
                                    //a la hora m�xima), esta toma podr�a adelantarse, por ejemplo a las 8:50 (menor que las 9:00, con lo cual se
                                    //cumplir�a la condici�n de que no sobrepase la hora m�xima), siempre y cuando se cumplan adem�s el resto de las
                                    //condiciones adicionales. ESTO APLICA PARA LOS APUNTES QUE NO EST�N CERRADOS (vstFechaFinMov = "").
                                    if (mbConfirmacionTomas.vstFechaFinMov == "")
                                    { //si el apunte no est� cerrado

                                        if ((aik == 0) && (iHoraIni != 23))
                                        { //si nos encontramos en el d�a mayor del intervalo, y la hora es menor que 23:00
                                            for (int aij = (iHoraIni + 1); aij <= 23; aij++)
                                            {
                                                System.DateTime TempDate21 = DateTime.FromOADate(0);
                                                stFechaToma = ((DateTime.TryParse(stFechaBucle, out TempDate21)) ? TempDate21.ToString("dd/MM/yyyy") : stFechaBucle) + " " + StringsHelper.Format(aij, "00"); // & ":00:00"
                                                System.DateTime TempDate22 = DateTime.FromOADate(0);
                                                stFechaPrevista = ((DateTime.TryParse(stFechaBucle, out TempDate22)) ? TempDate22.ToString("dd/MM/yyyy") : stFechaBucle) + " " + StringsHelper.Format(aij, "00") + ":00";

                                                //buscamos las tomas que hay en la hora
                                                Sql = new StringBuilder("select * from ETOMASFA where ");
                                                Sql.Append("itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and ");
                                                Sql.Append("ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and ");
                                                Sql.Append("gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and ");
                                                //UPGRADE_WARNING: (1068) tempRefParam5 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                                object tempRefParam5 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                                Sql.Append("fapuntes = " + Serrores.FormatFechaHMS(tempRefParam5) + " and ");
                                                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam5);
                                                Sql.Append("gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' and ");
                                                object tempRefParam6 = stFechaToma + ":00:00";
                                                Sql.Append("( fprevist >= " + Serrores.FormatFechaHMS(tempRefParam6) + " ");
                                                object tempRefParam7 = stFechaToma + ":59:59";
                                                Sql.Append("and fprevist <= " + Serrores.FormatFechaHMS(tempRefParam7) + ") ");
                                                Sql.Append("order by fprevist ");

                                                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                                                RrTomas = new DataSet();
                                                tempAdapter_2.Fill(RrTomas);
                                                //puede haber mas de una?
                                                if (RrTomas.Tables[0].Rows.Count != 0)
                                                {                                                    
                                                    while (RrTomas.Tables[0].Rows.Count != 0)
                                                    {
                                                        //si hay un registro se muestra
                                                        if (DateTime.Parse(stFechaPrevista) < DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima))
                                                        {
                                                            //si la fecha prevista de la pantalla es mayor que la fecha minima de confirmacion
                                                            //carga la fecha del                                                            
                                                            stFechaPrevista = Convert.ToDateTime(RrTomas.Tables[0].Rows[0]["fprevist"]).ToString("dd/MM/yyyy HH:NN");
                                                        }
                                                        else
                                                        {

                                                        }
                                                        //(maplaza)(19/04/2007)Si la fecha prevista de suspensi�n/finalizaci�n del f�rmaco es menor que la fecha
                                                        //prevista de la toma, no se cargar� la primera toma futura del d�a actual.
                                                        //Cargar_grid stFechaPrevista, CStr(RrTomas("cdosisto")), IIf(IsNull(RrTomas("ftomasfa")), "", RrTomas("ftomasfa")), IIf(IsNull(RrTomas("oobserva")), "", RrTomas("oobserva")), RrTomas("gusuario")
                                                        System.DateTime TempDate23 = DateTime.FromOADate(0);
                                                        if (mbConfirmacionTomas.farmacoSeleccion.FechaPrevista == "")
                                                        {
                                                            string tempRefParam8 = Convert.ToString(RrTomas.Tables[0].Rows[0]["cdosisto"]);
                                                            Cargar_grid(stFechaPrevista, ref tempRefParam8, (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["ftomasfa"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["ftomasfa"]), (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["oobserva"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["oobserva"]), Convert.ToString(RrTomas.Tables[0].Rows[0]["gusuario"]));
                                                        }
                                                        else if (DateTime.Parse(stFechaPrevista) <= DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaPrevista, out TempDate23)) ? TempDate23.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.farmacoSeleccion.FechaPrevista))
                                                        {
                                                            string tempRefParam9 = Convert.ToString(RrTomas.Tables[0].Rows[0]["cdosisto"]);
                                                            Cargar_grid(stFechaPrevista, ref tempRefParam9, (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["ftomasfa"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["ftomasfa"]), (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["oobserva"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["oobserva"]), Convert.ToString(RrTomas.Tables[0].Rows[0]["gusuario"]));
                                                        }
                                                        //-----
                                                        //(maplaza)(20/03/2007)
                                                        bTomaFuturaEnDiaActual = true;
                                                        //-----
                                                        break;
                                                    };
                                                }
                                                else
                                                {
                                                    if (mbConfirmacionTomas.farmacoSeleccion.Dosis[aij].Trim() != "")
                                                    {
                                                        //no hay registro hay que validar si existia una toma prevista
                                                        //si existe una toma prevista se carga un registro vacio
                                                        if (DateTime.Parse(stFechaPrevista) < DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima))
                                                        {
                                                            //si la fecha prevista de la pantalla es mayor que la fecha minima de confirmacion
                                                            //carga la fecha del
                                                            System.DateTime TempDate24 = DateTime.FromOADate(0);
                                                            stFechaPrevista = (DateTime.TryParse(mbConfirmacionTomas.GstConfirmaMinima, out TempDate24)) ? TempDate24.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.GstConfirmaMinima;
                                                        }
                                                        else
                                                        {
                                                        }
                                                        //(maplaza)(19/04/2007)Si la fecha prevista de suspensi�n/finalizaci�n del f�rmaco es menor que la fecha
                                                        //prevista de la toma, no se cargar� la primera toma futura del d�a actual.
                                                        //Cargar_grid stFechaPrevista, FarmacoSeleccion.Dosis(aij), "", ""
                                                        System.DateTime TempDate25 = DateTime.FromOADate(0);
                                                        if (mbConfirmacionTomas.farmacoSeleccion.FechaPrevista == "")
                                                        {
                                                            Cargar_grid(stFechaPrevista, ref mbConfirmacionTomas.farmacoSeleccion.Dosis[aij], "", "", Type.Missing);
                                                        }
                                                        else if (DateTime.Parse(stFechaPrevista) <= DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaPrevista, out TempDate25)) ? TempDate25.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.farmacoSeleccion.FechaPrevista))
                                                        {
                                                            Cargar_grid(stFechaPrevista, ref mbConfirmacionTomas.farmacoSeleccion.Dosis[aij], "", "", Type.Missing);
                                                        }
                                                        //-----
                                                        //(maplaza)(20/03/2007)
                                                        bTomaFuturaEnDiaActual = true;
                                                        //-----
                                                        break;
                                                    }
                                                }                                                
                                            }
                                        }
                                        //(maplaza)(20/03/2007)Se debe poder adelantar tambi�n tomas de f�rmacos cuya pr�xima
                                        //toma esta al d�a (o d�as) siguiente al d�a actual y en los f�rmacos que tienen
                                        //periodicidad (es decir el campo EFARMACO.periodic tiene un valor diferente de null),
                                        //no sale la toma hasta que estamos en el d�a en el que toca.
                                        //(maplaza)(19/04/2007)Se a�ade la condici�n de que, si se trata de dosis �nica, tampoco
                                        //debe aparecer toma futura del d�a siguiente, pues la �nica dosis se realizar� en el d�a actual.
                                        //Y adem�s se a�ade la condici�n de que si el Grupo Terapeutico no es "Insulinas", tampoco debe
                                        //aparecer toma futura del d�a siguiente.
                                        if ((!bTomaFuturaEnDiaActual) && (!bTomaFuturaDiasSiguientes) && (mbConfirmacionTomas.farmacoSeleccion.idounica == "N") && (!bGrupoInsulina))
                                        {
                                            //se busca si hay una toma futura para el d�a o d�as siguientes
                                            CargarPrimeraTomaFuturaDiasSiguientes(stFechaBucle);
                                            bTomaFuturaDiasSiguientes = true;
                                        }
                                        //-----
                                    }
                                    //-----

                                    //si hay toma en fecha bucle
                                    for (int aij = iHoraIni; aij >= iHoraFin; aij--)
                                    { //el bucle tiene que ir hacia atras
                                        //If Trim(FarmacoSeleccion.Dosis(aij)) <> "" Then
                                        System.DateTime TempDate26 = DateTime.FromOADate(0);
                                        stFechaToma = ((DateTime.TryParse(stFechaBucle, out TempDate26)) ? TempDate26.ToString("dd/MM/yyyy") : stFechaBucle) + " " + StringsHelper.Format(aij, "00"); // & ":00:00"
                                        System.DateTime TempDate27 = DateTime.FromOADate(0);
                                        stFechaPrevista = ((DateTime.TryParse(stFechaBucle, out TempDate27)) ? TempDate27.ToString("dd/MM/yyyy") : stFechaBucle) + " " + StringsHelper.Format(aij, "00") + ":00";

                                        //buscamos las tomas que hay en la hora
                                        Sql = new StringBuilder("select * from ETOMASFA where ");
                                        Sql.Append("itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and ");
                                        Sql.Append("ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and ");
                                        Sql.Append("gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and ");
                                        //UPGRADE_WARNING: (1068) tempRefParam10 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                        object tempRefParam10 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                        Sql.Append("fapuntes = " + Serrores.FormatFechaHMS(tempRefParam10) + " and ");
                                        mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam10);
                                        Sql.Append("gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' and ");
                                        object tempRefParam11 = stFechaToma + ":00:00";
                                        Sql.Append("( fprevist >= " + Serrores.FormatFechaHMS(tempRefParam11) + " ");
                                        object tempRefParam12 = stFechaToma + ":59:59";
                                        Sql.Append("and fprevist <= " + Serrores.FormatFechaHMS(tempRefParam12) + ") ");
                                        Sql.Append("order by fprevist ");

                                        SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                                        RrTomas = new DataSet();
                                        tempAdapter_3.Fill(RrTomas);
                                        //puede haber mas de una?
                                        if (RrTomas.Tables[0].Rows.Count != 0)
                                        {                                            
                                            foreach (DataRow iteration_row_2 in RrTomas.Tables[0].Rows)
                                            {
                                                //si hay un registro se muestra
                                                if (DateTime.Parse(stFechaPrevista) < DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima))
                                                {
                                                    //si la fecha prevista de la pantalla es mayor que la fecha minima de confirmacion
                                                    //carga la fecha del
                                                    stFechaPrevista = Convert.ToDateTime(iteration_row_2["fprevist"]).ToString("dd/MM/yyyy HH:NN");
                                                }
                                                else
                                                {

                                                }

                                                string tempRefParam13 = Convert.ToString(iteration_row_2["cdosisto"]);
                                                Cargar_grid(stFechaPrevista, ref tempRefParam13, (Convert.IsDBNull(iteration_row_2["ftomasfa"])) ? "" : Convert.ToString(iteration_row_2["ftomasfa"]), (Convert.IsDBNull(iteration_row_2["oobserva"])) ? "" : Convert.ToString(iteration_row_2["oobserva"]), Convert.ToString(iteration_row_2["gusuario"]));
                                            }
                                        }
                                        else
                                        {
                                            if (mbConfirmacionTomas.farmacoSeleccion.Dosis[aij].Trim() != "")
                                            {
                                                //no hay registro hay que validar si existia una toma prevista
                                                //si existe una toma prevista se carga un registro vacio
                                                if (DateTime.Parse(stFechaPrevista) < DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima))
                                                {
                                                    //si la fecha prevista de la pantalla es mayor que la fecha minima de confirmacion
                                                    //carga la fecha del
                                                    System.DateTime TempDate28 = DateTime.FromOADate(0);
                                                    stFechaPrevista = (DateTime.TryParse(mbConfirmacionTomas.GstConfirmaMinima, out TempDate28)) ? TempDate28.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.GstConfirmaMinima;
                                                }
                                                else
                                                {
                                                }
                                                Cargar_grid(stFechaPrevista, ref mbConfirmacionTomas.farmacoSeleccion.Dosis[aij], "", "", Type.Missing);
                                            }
                                        }                                        
                                    }
                                    //(maplaza)(30/10/2006)
                                }
                                else
                                {
                                    //(Mid(sDiasPrevistos, iDiaSemana, 1) = " ")
                                    //habr� que comprobar si el d�a de la semana en el que estoy hay tomas no previstas, pues
                                    //se tendr�n que mostrar (hasta antes de la hora m�xima)
                                    //buscamos las tomas que hay en el d�a, hasta antes de la hora representada por la variable "GstConfirmaMaxima"
                                    Sql = new StringBuilder("select * from ETOMASFA where ");
                                    Sql.Append("itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and ");
                                    Sql.Append("ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and ");
                                    Sql.Append("gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and ");
                                    //UPGRADE_WARNING: (1068) tempRefParam14 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                    object tempRefParam14 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                                    Sql.Append("fapuntes = " + Serrores.FormatFechaHMS(tempRefParam14) + " and ");
                                    mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam14);
                                    Sql.Append("gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco.Trim() + "' and ");
                                    object tempRefParam15 = stFechaBucle + " 00:00:00";
                                    Sql.Append("( fprevist >= " + Serrores.FormatFechaHMS(tempRefParam15) + " ");
                                    object tempRefParam16 = stFechaBucle + " 23:59:59";
                                    Sql.Append("and fprevist <= " + Serrores.FormatFechaHMS(tempRefParam16) + " ");
                                    System.DateTime TempDate29 = DateTime.FromOADate(0);
                                    object tempRefParam17 = (DateTime.TryParse(mbConfirmacionTomas.GstConfirmaMaxima, out TempDate29)) ? TempDate29.ToString("dd/MM/yyyy HH:NN:SS") : mbConfirmacionTomas.GstConfirmaMaxima;
                                    Sql.Append("and fprevist <= " + Serrores.FormatFechaHMS(tempRefParam17) + ") ");
                                    Sql.Append("order by fprevist DESC ");

                                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(Sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                                    RrTomas = new DataSet();
                                    tempAdapter_4.Fill(RrTomas);

                                    if (RrTomas.Tables[0].Rows.Count != 0)
                                    {
                                        //UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
                                        RrTomas.MoveFirst();

                                        while (RrTomas.Tables[0].Rows.Count != 0)
                                        {
                                            //si hay un registro se muestra                                            
                                            stFechaPrevista = Convert.ToDateTime(RrTomas.Tables[0].Rows[0]["fprevist"]).ToString("dd/MM/yyyy HH:NN");                                            
                                            string tempRefParam18 = Convert.ToString(RrTomas.Tables[0].Rows[0]["cdosisto"]);
                                            Cargar_grid(stFechaPrevista, ref tempRefParam18, (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["ftomasfa"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["ftomasfa"]), (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["oobserva"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["oobserva"]), Convert.ToString(RrTomas.Tables[0].Rows[0]["gusuario"]));
                                            break;
                                        };
                                    }                                    
                                    //-----
                                    //(maplaza)(19/04/2007)Se debe poder adelantar tambi�n tomas de f�rmacos cuya inmediata
                                    //toma futura esta al d�a (o d�as) siguiente al d�a actual. Se a�ade la condici�n de que,
                                    //si se trata de dosis �nica, tampoco debe aparecer toma futura del d�a siguiente.
                                    //Y adem�s se a�ade la condici�n de que si el Grupo Terapeutico no es "Insulinas", tampoco debe
                                    //aparecer toma futura del d�a siguiente.
                                    if ((!bTomaFuturaEnDiaActual) && (!bTomaFuturaDiasSiguientes) && (mbConfirmacionTomas.farmacoSeleccion.idounica == "N") && (!bGrupoInsulina))
                                    {
                                        //se busca si hay una toma futura para el d�a o d�as siguientes
                                        CargarPrimeraTomaFuturaDiasSiguientes(stFechaBucle);
                                        bTomaFuturaDiasSiguientes = true;
                                    }
                                    //-----
                                } //hay toma
                            }
                            else
                            {
                                //(Trim(sDiasPrevistos) = "") hay periodicidad
                                //puede haber tomas en dias no previstos?

                            } //no hay periodicidad

                        } //fin farmaco no precisa
                        //la hora no corresponde
                        //la hora se sale de intervalo entre la maxima y la minima
                    } //estamos en la fecha del bucle
                }
            }
            catch (SqlException ex)
            {

                Serrores.AnalizaError("Sueros.dll", Path.GetDirectoryName(Application.ExecutablePath), mbConfirmacionTomas.gUsuario, "Farmacos_sueros:Llenar_grid", ex);
            }

        }

        //(maplaza)(20/03/2007)Se obtiene la primera toma futura del d�a siguiente (que puede ser dentro de un d�a, dos,
        //tres,...etc) al que es pasado como argumento.
        private void CargarPrimeraTomaFuturaDiasSiguientes(string stFechaBucle)
        {
            string Sql = String.Empty;
            string stFechaPrevista = String.Empty;
            DataSet RrTomas = null;
            int iHora = 0;

            try
            {
                Sql = "select * from ETOMASFA where ";
                Sql = Sql + "itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and ";
                Sql = Sql + "ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and ";
                Sql = Sql + "gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " and ";
                //UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                object tempRefParam = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                Sql = Sql + "fapuntes = " + Serrores.FormatFechaHMS(tempRefParam) + " and ";
                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam);
                Sql = Sql + "gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' and ";
                System.DateTime TempDate = DateTime.FromOADate(0);
                object tempRefParam2 = ((DateTime.TryParse(stFechaBucle, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : stFechaBucle) + " 23:59:00";
                Sql = Sql + "fprevist > " + Serrores.FormatFechaHMS(tempRefParam2) + " ";
                Sql = Sql + "order by fprevist ";

                SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mbConfirmacionTomas.RcEnfermeria);
                RrTomas = new DataSet();
                tempAdapter.Fill(RrTomas);

                int iContador = 0;
                int iDiaSemana = 0;
                bool bFin = false;
                int iDiferencia = 0;
                if (RrTomas.Tables[0].Rows.Count != 0)
                {                    
                    stFechaPrevista = Convert.ToDateTime(RrTomas.Tables[0].Rows[0]["fprevist"]).ToString("dd/MM/yyyy HH:NN");
                    //(maplaza)(19/04/2007)Si la fecha prevista de suspensi�n/finalizaci�n del f�rmaco es menor que la fecha
                    //prevista de la toma, no se cargar� la primera toma futura del d�a siguiente.
                    //Cargar_grid stFechaPrevista, CStr(RrTomas("cdosisto")), IIf(IsNull(RrTomas("ftomasfa")), "", RrTomas("ftomasfa")), IIf(IsNull(RrTomas("oobserva")), "", RrTomas("oobserva")), RrTomas("gusuario")
                    System.DateTime TempDate2 = DateTime.FromOADate(0);
                    if (mbConfirmacionTomas.farmacoSeleccion.FechaPrevista == "")
                    {
                        //puede tener valor nulo la fecha prevista de suspensi�n/finalizaci�n del f�rmaco                        
                        string tempRefParam3 = Convert.ToString(RrTomas.Tables[0].Rows[0]["cdosisto"]);
                        Cargar_grid(stFechaPrevista, ref tempRefParam3, (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["ftomasfa"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["ftomasfa"]), (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["oobserva"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["oobserva"]), Convert.ToString(RrTomas.Tables[0].Rows[0]["gusuario"]));
                    }
                    else if (DateTime.Parse(stFechaPrevista) <= DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaPrevista, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.farmacoSeleccion.FechaPrevista))
                    {                        
                        string tempRefParam4 = Convert.ToString(RrTomas.Tables[0].Rows[0]["cdosisto"]);
                        Cargar_grid(stFechaPrevista, ref tempRefParam4, (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["ftomasfa"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["ftomasfa"]), (Convert.IsDBNull(RrTomas.Tables[0].Rows[0]["oobserva"])) ? "" : Convert.ToString(RrTomas.Tables[0].Rows[0]["oobserva"]), Convert.ToString(RrTomas.Tables[0].Rows[0]["gusuario"]));
                    }                 
                }
                else
                {
                    //se tendr�a que obtener la primera toma para el d�a en el que est� prevista la siguiente toma
                    for (iContador = 0; iContador <= 23; iContador++)
                    {
                        if (mbConfirmacionTomas.farmacoSeleccion.Dosis[iContador].Trim() != "")
                        {
                            iHora = iContador;
                            break;
                        }
                    }

                    if (mbConfirmacionTomas.farmacoSeleccion.Periodic > 0)
                    { //si existe periodicidad en las tomas del f�rmaco
                        //se suman los d�as indicados por la periodicidad para llegar al d�a de la siguiente toma prevista
                        stFechaPrevista = DateTimeHelper.ToString(DateTime.Parse(stFechaBucle).AddDays(mbConfirmacionTomas.farmacoSeleccion.Periodic));

                        System.DateTime TempDate3 = DateTime.FromOADate(0);
                        stFechaPrevista = ((DateTime.TryParse(stFechaPrevista, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : stFechaPrevista) + " " + StringsHelper.Format(iHora, "00") + ":00";

                        if (iContador != 24)
                        {
                            if (mbConfirmacionTomas.farmacoSeleccion.Dosis[iHora].Trim() != "")
                            {
                                //no hay registro hay que validar si existia una toma prevista
                                //si existe una toma prevista se carga un registro vacio
                                if (DateTime.Parse(stFechaPrevista) < DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima))
                                {
                                    //si la fecha prevista de la pantalla es mayor que la fecha minima de confirmacion
                                    System.DateTime TempDate4 = DateTime.FromOADate(0);
                                    stFechaPrevista = (DateTime.TryParse(mbConfirmacionTomas.GstConfirmaMinima, out TempDate4)) ? TempDate4.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.GstConfirmaMinima;
                                }
                                //(maplaza)(19/04/2007)Si la fecha prevista de suspensi�n/finalizaci�n del f�rmaco es menor
                                //que la fecha prevista de la toma, no se cargar� la primera toma futura del d�a siguiente.
                                //Cargar_grid stFechaPrevista, FarmacoSeleccion.Dosis(iHora), "", ""
                                System.DateTime TempDate5 = DateTime.FromOADate(0);
                                if (mbConfirmacionTomas.farmacoSeleccion.FechaPrevista == "")
                                {
                                    //puede tener valor nulo la fecha prevista de suspensi�n/finalizaci�n del f�rmaco
                                    Cargar_grid(stFechaPrevista, ref mbConfirmacionTomas.farmacoSeleccion.Dosis[iHora], "", "", Type.Missing);
                                }
                                else if (DateTime.Parse(stFechaPrevista) <= DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaPrevista, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.farmacoSeleccion.FechaPrevista))
                                {
                                    Cargar_grid(stFechaPrevista, ref mbConfirmacionTomas.farmacoSeleccion.Dosis[iHora], "", "", Type.Missing);
                                }
                                //-----
                            }
                        }
                    }
                    else
                    {
                        //(FarmacoSeleccion.Periodic=0) no existe periodicidad

                        if (mbConfirmacionTomas.farmacoSeleccion.VsDias.Trim() != "")
                        {
                            iDiaSemana = DateAndTime.Weekday(DateTime.Parse(stFechaBucle), FirstDayOfWeek.Monday); //devuelve el dia de la semana que es hoy
                            iContador = iDiaSemana;

                            while (!bFin)
                            {
                                if (iContador == 7)
                                {
                                    iContador = 1; //de domingo pasa a lunes
                                }
                                else
                                {
                                    iContador = (iContador + 1); //si no es domingo, se incrementa en uno el contador
                                }

                                if (mbConfirmacionTomas.farmacoSeleccion.VsDias.Substring(iContador - 1, Math.Min(1, mbConfirmacionTomas.farmacoSeleccion.VsDias.Length - (iContador - 1))) == "*")
                                {
                                    if (iContador >= iDiaSemana)
                                    {
                                        if (iContador == iDiaSemana)
                                        {
                                            iDiferencia = 7;
                                        }
                                        else
                                        {
                                            //(iContador > iDiaSemana)
                                            iDiferencia = iContador - iDiaSemana;
                                        }
                                        bFin = true;
                                    }
                                    else
                                    {
                                        //(iContador < iDiaSemana)
                                        iDiferencia = (7 - iDiaSemana + iContador);
                                        bFin = true;
                                    }
                                }
                            }

                            stFechaPrevista = DateTimeHelper.ToString(DateTime.Parse(stFechaBucle).AddDays(iDiferencia));
                            System.DateTime TempDate6 = DateTime.FromOADate(0);
                            stFechaPrevista = ((DateTime.TryParse(stFechaPrevista, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy") : stFechaPrevista) + " " + StringsHelper.Format(iHora, "00") + ":00";

                            if (iContador != 24)
                            {
                                if (mbConfirmacionTomas.farmacoSeleccion.Dosis[iHora].Trim() != "")
                                {
                                    //no hay registro hay que validar si existia una toma prevista
                                    //si existe una toma prevista se carga un registro vacio
                                    if (DateTime.Parse(stFechaPrevista) < DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima))
                                    {
                                        //si la fecha prevista de la pantalla es mayor que la fecha minima de confirmacion
                                        System.DateTime TempDate7 = DateTime.FromOADate(0);
                                        stFechaPrevista = (DateTime.TryParse(mbConfirmacionTomas.GstConfirmaMinima, out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.GstConfirmaMinima;
                                    }
                                    //(maplaza)(19/04/2007)Si la fecha prevista de suspensi�n/finalizaci�n del f�rmaco es menor
                                    //que la fecha prevista de la toma, no se cargar� la primera toma futura del d�a siguiente.
                                    //Cargar_grid stFechaPrevista, FarmacoSeleccion.Dosis(iHora), "", ""
                                    System.DateTime TempDate8 = DateTime.FromOADate(0);
                                    if (mbConfirmacionTomas.farmacoSeleccion.FechaPrevista == "")
                                    {
                                        //puede tener valor nulo la fecha prevista de suspensi�n/finalizaci�n del f�rmaco
                                        Cargar_grid(stFechaPrevista, ref mbConfirmacionTomas.farmacoSeleccion.Dosis[iHora], "", "", Type.Missing);
                                    }
                                    else if (DateTime.Parse(stFechaPrevista) <= DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.farmacoSeleccion.FechaPrevista, out TempDate8)) ? TempDate8.ToString("dd/MM/yyyy HH:NN") : mbConfirmacionTomas.farmacoSeleccion.FechaPrevista))
                                    {
                                        Cargar_grid(stFechaPrevista, ref mbConfirmacionTomas.farmacoSeleccion.Dosis[iHora], "", "", Type.Missing);
                                    }
                                    //-----
                                }
                            }

                        }
                    }
                }                
            }
            catch (SqlException ex)
            {
                Serrores.AnalizaError("ConfirmacionTomas.dll", mbConfirmacionTomas.stPathAplicacion, mbConfirmacionTomas.gUsuario, "ConfirmarFarmacos:CargarPrimeraTomaFuturaDiasSiguientes", ex);
            }

        }

        private void CargarFarmacoEquivalente()
        {
            //entra en este procedimiento cuando se trata de un farmaco de si precisa (bot�n nuevo)
            //"NUEVATOMA"
            //y cuando se selecciona una toma en el spread. "CONFIRMATOMA"
            //primero busca en la tabla de ifms si hay un farmaco equivalente
            //si los codigos de los farmacos son diferentes(gfarmaco y gfarmaco2)
            //se habilitara el combo para selecccionar uno de ellos
            //si solo existe uno se mostrar� la etiqueta.
            string Sql = String.Empty;
            DataSet RrEquivalente = null;

            //primero tiene que ver si se trata de un farmaco que ya se ha grabado en la tabla de
            //etomasfa, en este caso no se puede cambiar el farmaco equivalente ni nada
            if (iApunteToma == CONFIRMATOMA)
            {
                //se trata de confirmar una toma, tenemos que buscar si ya existe una toma guardada
                //o no hay toma ya confirmada.

                //    sql = "Select prs.gfarmaco as codpresc, prs.dnomfarm as nompresc,"
                //    sql = sql & " adm.gfarmaco as codadmin, adm.dnomfarm as nomadmin, etomasfa.* "
                //    sql = sql & "from ETOMASFA ,dcodfarmvt prs, dcodfarmvt adm "
                //    sql = sql & "where itiposer = '" & GstTiposervicio & "' "
                //    sql = sql & "and ganoregi = " & GstA�oFarmaco & " and gnumregi = " & GstNumeroFarmaco & " "
                //    sql = sql & "and fapuntes =" & FormatFechaHMS(FarmacoSeleccion.FechaApunte) & " "
                //    sql = sql & "and ETOMASFA.gfarmaco = '" & FarmacoSeleccion.CodFarmaco & "' "
                //    sql = sql & "and fprevist =" & FormatFechaHMS(SdcFechaPrevista & " " & tbHoraPrevista & ":00") & " "
                //    sql = sql & "and ETOMASFA.gfarmaco *= prs.gfarmaco "
                //    sql = sql & "and ETOMASFA.gfarmadm *= adm.gfarmaco "


                //UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                object tempRefParam = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                object tempRefParam2 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                Sql = "Select prs.gfarmaco as codpresc, prs.dnomfarm as nompresc," +
                      " adm.gfarmaco as codadmin, adm.dnomfarm as nomadmin, etomasfa.* " +
                      " from ETOMASFA " +
                      "  LEFT JOIN dcodfarmvt_ljoin prs ON ETOMASFA.gfarmaco = prs.gfarmaco " +
                      "  LEFT JOIN dcodfarmvt_ljoin adm ON ETOMASFA.gfarmadm = adm.gfarmaco " +
                      " where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' " +
                      "   and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " " +
                      "   and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam) + " " +
                      "   and ETOMASFA.gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' " +
                      "   and fprevist =" + Serrores.FormatFechaHMS(tempRefParam2) + " ";
                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam);

                SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mbConfirmacionTomas.RcEnfermeria);
                RrEquivalente = new DataSet();
                tempAdapter.Fill(RrEquivalente);
                if (RrEquivalente.Tables[0].Rows.Count != 0)
                {
                    //si existe una toma confirmada compruebo si se ha rellenado el farmaco equivalente
                    if (Convert.IsDBNull(RrEquivalente.Tables[0].Rows[0]["codadmin"]))
                    {
                        //se ha administrado el farmaco prescrito
                        lbFarmacoAdm.Text = Convert.ToString(RrEquivalente.Tables[0].Rows[0]["nompresc"]).Trim();
                        lbFarmacoAdm.Visible = true;
                    }
                    else
                    {
                        //se ha administrado el farmaco equivalente
                        if (Convert.ToString(RrEquivalente.Tables[0].Rows[0]["codpresc"]).Trim() != Convert.ToString(RrEquivalente.Tables[0].Rows[0]["codadmin"]).Trim())
                        {
                            //se ha administrado un farmaco equivalente                            
                            lbFarmacoAdm.Text = Convert.ToString(RrEquivalente.Tables[0].Rows[0]["nomadmin"]).Trim();
                            lbFarmacoAdm.Visible = true;
                        }
                        else
                        {
                            //se trata del mismo farmaco.                            
                            lbFarmacoAdm.Text = Convert.ToString(RrEquivalente.Tables[0].Rows[0]["nompresc"]).Trim();
                            lbFarmacoAdm.Visible = true;
                        }
                    }
                }
                else
                {
                    //no hay datos de toma luego hay que buscar si hay farmaco equivalente
                    Sql = "select prs.gfarmaco as codpresc, adm.gfarmaco as codadmin,";
                    Sql = Sql + "prs.dnomfarm as nompresc,adm.dnomfarm as nomadmin ";
                    Sql = Sql + "from ifmsfm_fadisadm , dcodfarmvt prs, dcodfarmvt adm ";
                    Sql = Sql + "where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ";
                    Sql = Sql + "and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " ";
                    Sql = Sql + "and gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ";
                    //UPGRADE_WARNING: (1068) tempRefParam3 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                    object tempRefParam3 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                    Sql = Sql + "and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam3) + " ";
                    mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam3);
                    Sql = Sql + "and ifmsfm_fadisadm.gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco.Trim() + "' ";
                    Sql = Sql + "and ifmsfm_fadisadm.gfarmaco = prs.gfarmaco ";
                    Sql = Sql + "and ifmsfm_fadisadm.gfarmaco2 = adm.gfarmaco ";
                    object tempRefParam4 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                    Sql = Sql + "and ifmsfm_fadisadm.finicio <= " + Serrores.FormatFechaHMS(tempRefParam4) + " ";
                    object tempRefParam5 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                    Sql = Sql + "and ifmsfm_fadisadm.ffin >= " + Serrores.FormatFechaHMS(tempRefParam5) + " ";

                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Sql, mbConfirmacionTomas.RcEnfermeria);
                    RrEquivalente = new DataSet();
                    tempAdapter_2.Fill(RrEquivalente);
                    if (RrEquivalente.Tables[0].Rows.Count != 0)
                    {
                        //si devuelve algun registro comprueba que el codigo1 = codigo 2                        
                        if (Convert.ToString(RrEquivalente.Tables[0].Rows[0]["codpresc"]).Trim() == Convert.ToString(RrEquivalente.Tables[0].Rows[0]["codadmin"]).Trim())
                        {
                            //se trata del mismo codigo de farmaco                            
                            lbFarmacoAdm.Text = Convert.ToString(RrEquivalente.Tables[0].Rows[0]["nompresc"]).Trim();
                            lbFarmacoAdm.Visible = true;
                            CbbFarmacoAdm.Visible = false;
                        }
                        else
                        {
                            //si es diferente hay que cargar el combo con los dos farmacos para seleccionar uno
                            CbbFarmacoAdm.Clear();
                            CbbFarmacoAdm.ColumnWidths = "200" + ";" + "0";
                            //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                            object tempRefParam6 = Convert.ToString(RrEquivalente.Tables[0].Rows[0]["nompresc"]).Trim();
                            object tempRefParam7 = Type.Missing;
                            CbbFarmacoAdm.AddItem(tempRefParam6, tempRefParam7);
                            //UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
                            //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                            CbbFarmacoAdm.set_Column(1, 0, RrEquivalente.Tables[0].Rows[0]["codpresc"].ToString());
                            //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                            object tempRefParam8 = Convert.ToString(RrEquivalente.Tables[0].Rows[0]["nomadmin"]).Trim();
                            object tempRefParam9 = Type.Missing;
                            CbbFarmacoAdm.AddItem(tempRefParam8, tempRefParam9);
                            //UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
                            //UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
                            CbbFarmacoAdm.set_Column(1, 1, RrEquivalente.Tables[0].Rows[0]["codadmin"].ToString());
                            //para que se ponga en el farmaco prescrito con posiblidad de cambiar al equivalente
                            CbbFarmacoAdm.set_ListIndex(0); //-1
                            CbbFarmacoAdm.Visible = true;
                            lbFarmacoAdm.Visible = true;
                        }
                    }
                    else
                    {
                        lbFarmacoAdm.Visible = true;
                        lbFarmacoAdm.Text = lbNombreFarmaco.Text.Trim();
                    }
                }                
            }
            else
            {
                //NUEVATOMA
                //hay que buscar si hay un farmaco equivalente
                //buscamos los farmacos equivalentes
                Sql = "select prs.gfarmaco as codpresc, adm.gfarmaco as codadmin,";
                Sql = Sql + "prs.dnomfarm as nompresc,adm.dnomfarm as nomadmin ";
                Sql = Sql + "from ifmsfm_fadisadm , dcodfarmvt prs, dcodfarmvt adm ";
                Sql = Sql + "where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ";
                Sql = Sql + "and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " ";
                Sql = Sql + "and gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ";
                //UPGRADE_WARNING: (1068) tempRefParam10 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                object tempRefParam10 = mbConfirmacionTomas.farmacoSeleccion.FechaApunte;
                Sql = Sql + "and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam10) + " ";
                mbConfirmacionTomas.farmacoSeleccion.FechaApunte = Convert.ToString(tempRefParam10);
                Sql = Sql + "and ifmsfm_fadisadm.gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco.Trim() + "' ";
                Sql = Sql + "and ifmsfm_fadisadm.gfarmaco = prs.gfarmaco ";
                Sql = Sql + "and ifmsfm_fadisadm.gfarmaco2 = adm.gfarmaco ";
                object tempRefParam11 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                Sql = Sql + "and ifmsfm_fadisadm.finicio <= " + Serrores.FormatFechaHMS(tempRefParam11) + " ";
                object tempRefParam12 = SdcFechaPrevista.Value.Date + " " + tbHoraPrevista.Text + ":00";
                Sql = Sql + "and ifmsfm_fadisadm.ffin >= " + Serrores.FormatFechaHMS(tempRefParam12) + " ";

                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(Sql, mbConfirmacionTomas.RcEnfermeria);
                RrEquivalente = new DataSet();
                tempAdapter_3.Fill(RrEquivalente);
                if (RrEquivalente.Tables[0].Rows.Count != 0)
                {
                    //si devuelve algun registro comprueba que el codigo1 = codigo 2                    
                    if (Convert.ToString(RrEquivalente.Tables[0].Rows[0]["codpresc"]).Trim() == Convert.ToString(RrEquivalente.Tables[0].Rows[0]["codadmin"]).Trim())
                    {
                        //se trata del mismo codigo de farmaco                        
                        lbFarmacoAdm.Text = Convert.ToString(RrEquivalente.Tables[0].Rows[0]["nompresc"]).Trim();
                        lbFarmacoAdm.Visible = true;
                        CbbFarmacoAdm.Visible = false;
                    }
                    else
                    {
                        //si es diferente hay que cargar el combo con los dos farmacos para seleccionar uno
                        CbbFarmacoAdm.Clear();
                        CbbFarmacoAdm.ColumnWidths = "200" + ";" + "0";                        
                        object tempRefParam13 = Convert.ToString(RrEquivalente.Tables[0].Rows[0]["nompresc"]).Trim();
                        object tempRefParam14 = Type.Missing;
                        CbbFarmacoAdm.AddItem(tempRefParam13, tempRefParam14);
                        //UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx                        
                        CbbFarmacoAdm.set_Column(1, 0, RrEquivalente.Tables[0].Rows[0]["codpresc"].ToString());                        
                        object tempRefParam15 = Convert.ToString(RrEquivalente.Tables[0].Rows[0]["nomadmin"]).Trim();
                        object tempRefParam16 = Type.Missing;
                        CbbFarmacoAdm.AddItem(tempRefParam15, tempRefParam16);
                        //UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx                        
                        CbbFarmacoAdm.set_Column(1, 1, RrEquivalente.Tables[0].Rows[0]["codadmin"].ToString());

                        //para que se ponga en el farmaco prescrito con posiblidad de cambiar al equivalente
                        CbbFarmacoAdm.set_ListIndex(0);
                        CbbFarmacoAdm.Visible = true;
                        lbFarmacoAdm.Visible = true;
                    }
                }
                else
                {
                    lbFarmacoAdm.Visible = true;
                    lbFarmacoAdm.Text = lbNombreFarmaco.Text.Trim();
                    CbbFarmacoAdm.Clear();
                    CbbFarmacoAdm.Visible = false;
                    CbbFarmacoAdm.set_ListIndex(-1);
                }                
            }
        }

        //(maplaza)(31/10/2006)
        //Esta funci�n sirve para saber si el Bot�n "Nuevo" debe estar activo o no.
        //El bot�n "Nuevo" debe estar activo siempre que no haya ninguna toma confirmada para el f�rmaco
        //(debe controlar itiposer, ganoregi, gnumregi, gfarmaco, finitrat).
        //Antes miraba solamente en �stas cuando el f�rmaco est� activo (ffinmovi null). Habr� que quitar lo de
        //ffinmovi a null y no es necesario cruzar por fapuntes.
        public bool TomaNueva()
        {
            //funcion que devuelve si para el farmaco que vamos a confirmar es la
            //primera toma o ya esta en tratamiento. el registro es unico o hay varios
            bool result = false;
            string Sql = "select * from EFARMACO ";
            Sql = Sql + "inner join ETOMASFA on ETOMASFA.itiposer = EFARMACO.itiposer ";
            Sql = Sql + "and ETOMASFA.ganoregi = EFARMACO.ganoregi ";
            Sql = Sql + "and ETOMASFA.gnumregi = EFARMACO.gnumregi ";
            Sql = Sql + "and ETOMASFA.gfarmaco = EFARMACO.gfarmaco ";
            //sql = sql & "and ETOMASFA.fapuntes = EFARMACO.fapuntes "  '(maplaza)(31/10/2006)
            Sql = Sql + "where EFARMACO.itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and EFARMACO.ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and EFARMACO.gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ";
            Sql = Sql + "and EFARMACO.gfarmaco = '" + mbConfirmacionTomas.farmacoSeleccion.CodFarmaco + "' ";
            //UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
            object tempRefParam = mbConfirmacionTomas.farmacoSeleccion.FechaIni;
            Sql = Sql + "and EFARMACO.finitrat = " + Serrores.FormatFechaHMS(tempRefParam) + " ";
            mbConfirmacionTomas.farmacoSeleccion.FechaIni = Convert.ToString(tempRefParam);
            //sql = sql & "and EFARMACO.ffinmovi is null "   '(maplaza)(31/10/2006)
            Sql = Sql + "order by EFARMACO.fapuntes ";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mbConfirmacionTomas.RcEnfermeria);
            DataSet RrSQL = new DataSet();
            tempAdapter.Fill(RrSQL);
            if (RrSQL.Tables[0].Rows.Count == 0)
            {
                //si no devuelve ningun registro
                result = true;
            }
            else
            {
                result = false;
            }            
            return result;
        }

        private void DatosDosisFarmaco(string stCodigoFarmaco)
        {
            //saca de dcodfarm el campo cporenva ,el indicador unidosis y el indicador de receta y la equivalencia
            string tstSql = "Select cporenva,iunidosis,nequival,iumedfra from dcodfarmvt " + " where gfarmaco='" + stCodigoFarmaco + "'";
            // "  and fborrado is null "
            SqlDataAdapter tempAdapter = new SqlDataAdapter(tstSql, mbConfirmacionTomas.RcEnfermeria);
            DataSet RrSQL = new DataSet();
            tempAdapter.Fill(RrSQL);
            if (RrSQL.Tables[0].Rows.Count != 0)
            {
                if (!Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["iumedfra"]))
                {                    
                    iMedidaFraccionable = Convert.ToString(RrSQL.Tables[0].Rows[0]["iumedfra"]).ToUpper();
                }
                
                if (!Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["iunidosis"]))
                {                    
                    iEnvaseFraccionable = Convert.ToString(RrSQL.Tables[0].Rows[0]["iunidosis"]).ToUpper();
                }                
                if (!Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["nequival"]))
                {                    
                    string tempRefParam = Convert.ToString(RrSQL.Tables[0].Rows[0]["nequival"]);
                    stUnidadEquivalencia = Serrores.ConvertirDecimales(ref tempRefParam, ref mbConfirmacionTomas.vstSeparadorDecimal, 2);
                }
                else
                {
                    string tempRefParam2 = "1";
                    stUnidadEquivalencia = Serrores.ConvertirDecimales(ref tempRefParam2, ref mbConfirmacionTomas.vstSeparadorDecimal, 2);
                }                
                if (!Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["cporenva"]))
                {
                    //SngCantidadPorEnvase = Rrsql("cporenva")
                    SngCantidadPorEnvase = 1;
                }
            }            
        }

        private void tbObservaciones_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {
            int KeyAscii = Strings.Asc(eventArgs.KeyChar);
            if (KeyAscii == 39)
            {
                //UPGRADE_ISSUE: (1058) Assignment not supported: KeyAscii to a non-positive constant More Information: http://www.vbtonet.com/ewis/ewi1058.aspx
                KeyAscii = Serrores.SustituirComilla();
            }
            if (KeyAscii == 0)
            {
                eventArgs.Handled = true;
            }
            eventArgs.KeyChar = Convert.ToChar(KeyAscii);
        }

        private string DosisAImputar(string stCodFarmaco, string stDosisAdm, string stNequival)
        {
            //funcion para buscar el indicador de quien trae el farmaco
            string result = String.Empty;
            result = "0";
            string Sql = "select sum(cdosisto) as TotalAdministrado, sum(cdosimpu) as TotalImputado " + "from ETOMASFA where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' " + "and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " " + "and gfarmaco = '" + stCodFarmaco + "' ";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, mbConfirmacionTomas.RcEnfermeria);
            DataSet RrSQL = new DataSet();
            tempAdapter.Fill(RrSQL);
            if (RrSQL.Tables[0].Rows.Count != 0)
            {                
                if (!Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["TotalAdministrado"]) && !Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["TotalImputado"]))
                {
                    //si la suma de lo que tenemos administrado es mayor que lo imputado
                    //imputamos una unidad de equivalencia completa                    
                    string tempRefParam = (Convert.ToString(RrSQL.Tables[0].Rows[0]["TotalAdministrado"]));
                    string tempRefParam2 = (Convert.ToString(RrSQL.Tables[0].Rows[0]["TotalImputado"]));
                    if (Serrores.ToDOUBLE(tempRefParam) + Double.Parse(stDosisAdm) > Serrores.ToDOUBLE(tempRefParam2))
                    {
                        result = stNequival;
                    }
                }
                else
                {
                    //tiene que tener algun valor                    
                    if (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["TotalAdministrado"]) && Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["TotalImputado"]))
                    {
                        //se trata de la primera toma
                        result = stNequival;
                    }
                }

            }            
            return result;
        }

        private void ConfirmarFarmacos_Closed(Object eventSender, EventArgs eventArgs)
        {
            MemoryHelper.ReleaseMemory();
        }
    }
}