using Microsoft.VisualBasic;
using System;
using System.Text;
using UpgradeHelpers.Helpers;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ConfirmacionTomas
{
	internal static class mbControl
	{


		//Public fFormCargado As Boolean
		//Public fConexion As Boolean
		//Public vstUsuario_NT As String

		//Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long

		//Sive para poner los dos puntos (:) como separador de hora en la
		//configuraci�n regional
		//Declare Function SetLocaleInfo Lib "kernel32" Alias "SetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String) As Long

		public static int iContador = 0;
		//Public viNuevoTop As Integer
		//Public vinuevoLeft As Integer

		//Public Const COiSuperior As Integer = 531 '360
		//Public Const COiLateral As Integer = 805 '1440

		//*****************************CRIS***********************************
		//Para saber si hay alg�n m�dulo ejecut�ndose
		//Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
		//Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long

		//Public Const SYNCHRONIZE = &H100000'

		//Public Const wait_timeout = &H102&

		//Public vPathAplicacion As String

		internal static int ObtenerNIntentos()
		{
			//Aqu� tenemos que hacer una llamada a SCONSGLO para determinar
			//el m�ximo n�mero de intentos al poner la contrase�a.
			//De momento...

			return 3;

		}
		internal static string DesencriptarPassword(string stContrase�a)
		{

			int[] arraydesp = new int[9];
			int[] entrada = new int[9];
			StringBuilder stDevolucion = new StringBuilder();

			arraydesp[1] = 10;
			arraydesp[2] = 13;
			arraydesp[3] = 31;
			arraydesp[4] = 14;
			arraydesp[5] = 1;
			arraydesp[6] = 22;
			arraydesp[7] = 34;
			arraydesp[8] = 8;

			//A PARTIR DE AQUI, LA DESENCRIPTACION
			for (int i = 1; i <= 8; i++)
			{
				entrada[i] = Strings.Asc(stContrase�a.Substring(i - 1, Math.Min(1, stContrase�a.Length - (i - 1)))[0]);
				if (entrada[i] == 32)
				{
					entrada[i] = 1;
				}
				else
				{
					if (entrada[i] >= 48 && entrada[i] <= 57)
					{
						entrada[i] -= 46;
					}
					else
					{
						if (entrada[i] >= 65 && entrada[i] <= 90)
						{
							entrada[i] -= 53;
						}
						else
						{
							entrada[i] = 38;
						}
					}
				}
				entrada[i] -= arraydesp[i];
				if (entrada[i] < 0)
				{
					entrada[i] += 38;
				}
				if (entrada[i] == 1)
				{
					entrada[i] = 32;
				}
				else
				{
					if (entrada[i] >= 2 && entrada[i] <= 11)
					{
						entrada[i] += 46;
					}
					else
					{
						if (entrada[i] >= 12 && entrada[i] <= 37)
						{
							entrada[i] += 53;
						}
						else
						{
							entrada[i] = 209;
						}
					}
				}
				stDevolucion.Append(Strings.Chr(entrada[i]).ToString());

			}

			return stDevolucion.ToString();

		}
		internal static string EncriptarPassword(ref string stContrase�a)
		{

			int[] arraydesp = new int[9];
			int[] entrada = new int[9];
			string[] salida = ArraysHelper.InitializeArray<string>(9);
			StringBuilder stDevolver = new StringBuilder();

			//el n�mero m�ximo de desplazamiento se debe fijar a 37
			//para que el cociente siempre sea 1 para poder
			//desencriptar partiendo del resto
			//(fijando el cociente siempre a 1).
			arraydesp[1] = 10;
			arraydesp[2] = 13;
			arraydesp[3] = 31;
			arraydesp[4] = 14;
			arraydesp[5] = 1;
			arraydesp[6] = 22;
			arraydesp[7] = 34;
			arraydesp[8] = 8;
			//Pasamos a may�sculas la contrase�a
			stContrase�a = stContrase�a.ToUpper();
			//metemos espacios a la derecha para almacenar como password siempre 8 caracteres
			if (stContrase�a.Length < 8)
			{
				stContrase�a = stContrase�a + new String(' ', 8 - stContrase�a.Length);
			}
			//vamos recortando la contrase�a palabra por palabra
			for (int i = 1; i <= 8; i++)
			{
				entrada[i] = Strings.Asc(stContrase�a.Substring(i - 1, Math.Min(1, stContrase�a.Length - (i - 1)))[0]);
				//'    'validar caracteres tecleados: solo se admite el espacio, n�meros, letras may�sculas, letras min�sculas, la � y la �
				//'    If Not (entrada(I) = 32 Or _
				//''        (entrada(I) >= 48 And entrada(I) <= 57) Or _
				//''        (entrada(I) >= 65 And entrada(I) <= 90) Or _
				//''        entrada(I) = 209) Then
				//'        Mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 1230, RcPrincipal, S01000F1.lbContrase�a, "letras y n�meros"
				//'        Exit Function
				//'    End If
				//se transforman los rangos de caracteres v�lidos en un rango de 1..38
				if (entrada[i] == 32)
				{
					entrada[i] = 1;
				}
				else
				{
					if (entrada[i] >= 48 && entrada[i] <= 57)
					{
						entrada[i] -= 46;
					}
					else
					{
						if (entrada[i] >= 65 && entrada[i] <= 90)
						{
							entrada[i] -= 53;
						}
						else
						{
							entrada[i] = 38;
						}
					}
				}
				//se le suma el desplazamiento y se obtiene el m�dulo 38
				entrada[i] += arraydesp[i];
				entrada[i] = entrada[i] % 38;
				//se transforma el ascii en su caracter
				if (entrada[i] == 1)
				{
					entrada[i] = 32;
				}
				else
				{
					if (entrada[i] >= 2 && entrada[i] <= 11)
					{
						entrada[i] += 46;
					}
					else
					{
						if (entrada[i] >= 12 && entrada[i] <= 37)
						{
							entrada[i] += 53;
						}
						else
						{
							entrada[i] = 209;
						}
					}
				}
				salida[i] = Strings.Chr(entrada[i]).ToString();
				stDevolver.Append(salida[i]);
			}

			return stDevolver.ToString();

		}
	}
}