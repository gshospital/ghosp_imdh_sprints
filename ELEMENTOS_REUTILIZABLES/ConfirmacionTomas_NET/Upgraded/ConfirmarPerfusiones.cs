using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace ConfirmacionTomas
{
    public partial class ConfirmarPerfusiones
        : Telerik.WinControls.UI.RadForm
    {
        string stUsuarioRegistro = String.Empty;
        int lTomasSeleccionado = 0; //controla el registro seleccionado
        bool fHayFilaSeleccionada = false; //true si hay alguna fila seleccionada
        int iApunteToma = 0;
        string stFechaPrevista = String.Empty;
        string stFechaApunte = String.Empty;

        const int NUEVATOMA = 6;
        const int CONFIRMATOMA = 7;
        string FechaMaximaToma = String.Empty; //se guardan los datos que llegan a la pantalla
        string FechaMinimaToma = String.Empty; //de fecha maxima y de fecha minima
        //(maplaza)(16/11/2006)Ser� true cuando (motaus <> null)
        bool vfAusencia = false;
        //-----

        string iMedidaFraccionable = String.Empty;
        string iEnvaseFraccionable = String.Empty;
        string stUnidadEquivalencia = String.Empty;
        float SngCantidadPorEnvase = 0;

        public ConfirmarPerfusiones()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
        }

        //(maplaza)(17/11/2006)Esta funci�n dice si el episodio de Hospitalizaci�n pasado a la dll tiene motivo de ausencia (True) o no.
        private bool ConMotivoDeAusencia()
        {
            bool result = false;
            string SqlSt = String.Empty;
            DataSet RrSqlSt = null;

            if (mbConfirmacionTomas.GstTiposervicio == "H")
            {
                SqlSt = "Select gmotause from aepisadm where ganoadme = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumadme = " + mbConfirmacionTomas.GstNumeroFarmaco;
                SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlSt, mbConfirmacionTomas.RcEnfermeria);
                RrSqlSt = new DataSet();
                tempAdapter.Fill(RrSqlSt);
                if (RrSqlSt.Tables[0].Rows.Count != 0)
                {                    
                    if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["gmotause"]))
                    {
                        result = true;
                    }
                }                
            }
            return result;
        }

        private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
        {
            string FechaMenor = String.Empty; //para la validacion de fecha anterior y fecha posterior
            string FechaMayor = String.Empty;
            string stFechaAux = String.Empty;
            string stFechaAuxPrevista = String.Empty;

            //'' Comprobar si el paciente ya ha sido dado de alta en cualquier modulo  por otro usuario
            string SqlSt = String.Empty;
            DataSet RrSqlSt = null;
            switch (mbConfirmacionTomas.GstTiposervicio)
            {
                case "U":
                    SqlSt = "Select faltaurg from uepisurg where ganourge = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumurge = " + mbConfirmacionTomas.GstNumeroFarmaco;
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(SqlSt, mbConfirmacionTomas.RcEnfermeria);
                    RrSqlSt = new DataSet();
                    tempAdapter.Fill(RrSqlSt);
                    if (RrSqlSt.Tables[0].Rows.Count != 0)
                    {                        
                        if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["faltaurg"]))
                        {
                            RadMessageBox.Show("Paciente de alta en urgencias", Application.ProductName);                            
                            return;
                        }
                    }                    
                    break;

                case "H":
                    SqlSt = "Select faltplan from aepisadm where ganoadme = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumadme = " + mbConfirmacionTomas.GstNumeroFarmaco;
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SqlSt, mbConfirmacionTomas.RcEnfermeria);
                    RrSqlSt = new DataSet();
                    tempAdapter_2.Fill(RrSqlSt);
                    if (RrSqlSt.Tables[0].Rows.Count != 0)
                    {                        
                        if (!Convert.IsDBNull(RrSqlSt.Tables[0].Rows[0]["faltplan"]))
                        {
                            RadMessageBox.Show("Paciente de alta en Hospitalizaci�n", Application.ProductName);                            
                            return;
                        }
                    }                    
                    break;
            }
            
            this.Enabled = false;
            //Validaciones del aceptar
            //Oscar C Septiembre 2010
            //Si la velocidad de perfusion confirmada es diferente a la velocidad de perfusion prescrita.....
            //se obliga al usuario a especifcar el motivo del cambio en las observaciones de la toma.
            string tempRefParam = mbConfirmacionTomas.FarmacosPantalla[1].veloperf.Trim();
            string tempRefParam2 = tbVelocidad.Text.Trim();
            if (Serrores.ConvertirDecimales(ref tempRefParam, ref mbConfirmacionTomas.vstSeparadorDecimal, 1) != Serrores.ConvertirDecimales(ref tempRefParam2, ref mbConfirmacionTomas.vstSeparadorDecimal, 1) && tbObservaciones.Text.Trim() == "")
            {
                this.Enabled = true;
                RadMessageBox.Show("Cuando la Velocidad de Perfusi�n prescrita es diferente a la Velocidad de Perfusi�n confirmada, debe especificar el motivo de dicha variaci�n en las observaciones de la toma.", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            //------------
            switch (iApunteToma)
            {
                case NUEVATOMA:
                    //es obligatorio que lleve una fecha prevista 
                    string tempRefParam3 = "Fecha real ";
                    if (mbConfirmacionTomas.ValidacionFecha(sdcFecha, tbHora, ref tempRefParam3))
                    {
                        this.Enabled = true;
                        sdcFecha.Focus();
                        return;
                    }

                    break;
                default:
                    //si es una toma que ya existe lleva otras validaciones 
                    if (stUsuarioRegistro.Trim() != "")
                    {
                        //si el registro ya existe en etomasfa solo le permito modificar las observaciones
                        //cuando el usuario que va confirmar es el mismo que las grabo
                        if (stUsuarioRegistro.Trim().ToUpper() == mbConfirmacionTomas.UsuarioConfirmacion.Trim().ToUpper())
                        {
                            if (sdcFecha.Text == "" && tbObservaciones.Text.Trim() == "")
                            {
                                short tempRefParam4 = 1040;
                                string[] tempRefParam5 = new string[] { "Observaciones" };
                                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam4, mbConfirmacionTomas.RcEnfermeria, tempRefParam5);
                                this.Enabled = true;
                                tbObservaciones.Focus();
                                return;
                            }
                        }
                    }
                    else
                    {
                        //no existe la toma seleccionada en ETOMASFA
                        //no se puede modificar la fecha real de la toma
                        //se pueden teclear las observaciones solo
                        if (tbObservaciones.Text.Trim() == "")
                        { //observaciones vacia
                            short tempRefParam6 = 1040;
                            string[] tempRefParam7 = new string[] { Frame4.Text };
                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam6, mbConfirmacionTomas.RcEnfermeria, tempRefParam7);
                            this.Enabled = true;
                            sdcFecha.Focus();
                            return;
                        }
                    }
                    break;
            }
            //la fecha no puede ser mayor que la fecha del sistema
            if (sdcFecha.Text != "" && tbHora.Text.Trim() != ":")
            {
                //cargamos la fecha real de la toma en una variable local
                stFechaAux = sdcFecha.Text + " " + tbHora.Text;
                if (FechaMaximaToma.Trim() == "")
                {
                    FechaMayor = mbConfirmacionTomas.vstFechaHoraSis;
                }
                else
                {
                    FechaMayor = FechaMaximaToma;
                }
                //    If CDate(GstConfirmaMaxima) > CDate(vstFechaHoraSis) Then
                //        FechaMayor = vstFechaHoraSis
                //    Else
                //        FechaMayor = GstConfirmaMaxima
                //    End If
                FechaMenor = mbConfirmacionTomas.GstConfirmaMinima;
                //no se puede grabar un registro con fecha/hora mayor que la del dia
                //una vez que tenemos los limites cargados hacemos la validacion
                if (DateTime.Parse(stFechaAux) < DateTime.Parse(FechaMenor))
                {
                    short tempRefParam8 = 1020;
                    string[] tempRefParam9 = new string[] { Frame4.Text, "mayor", DateTime.Parse(FechaMenor).ToString("dd/MM/yyyy HH:mm") };
                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam8, mbConfirmacionTomas.RcEnfermeria, tempRefParam9);
                    this.Enabled = true;
                    tbHora.Focus();
                    return;
                }
                else
                {
                    if (DateTime.Parse(stFechaAux) > DateTime.Parse(FechaMayor))
                    {
                        short tempRefParam10 = 1020;
                        string[] tempRefParam11 = new string[] { Frame4.Text, "menor", DateTime.Parse(FechaMayor).ToString("dd/MM/yyyy HH:mm") };
                        mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam10, mbConfirmacionTomas.RcEnfermeria, tempRefParam11);
                        this.Enabled = true;
                        tbHora.Focus();
                        return;
                    }
                }
            }

            if (tbVelocidad.Text.Trim() == "")
            {
                short tempRefParam12 = 1040;
                string[] tempRefParam13 = new string[] { Label5.Text };
                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam12, mbConfirmacionTomas.RcEnfermeria, tempRefParam13);
                this.Enabled = true;
                tbVelocidad.Focus();
                return;
            }

            double dbNumericTemp = 0;
            if (!Double.TryParse(tbVelocidad.Text, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
            {
                short tempRefParam14 = 1210;
                string[] tempRefParam15 = new string[] { Label5.Text };
                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam14, mbConfirmacionTomas.RcEnfermeria, tempRefParam15);
                this.Enabled = true;
                tbVelocidad.Focus();
                return;
            }

            GrabarRegistro();
            this.Enabled = true;
            SprFarmacos.MaxRows = 0;
            mbConfirmacionTomas.vstFechaHoraSis = DateTime.Now.ToString("dd/MM/yyyy HH:NN:SS");
            if (FechaMaximaToma.Trim() == "")
            {
                mbConfirmacionTomas.GstConfirmaMaxima = mbConfirmacionTomas.vstFechaHoraSis;
            }
            Llenar_grid();
            IniciarPantalla();
            ProtegerPantalla("vacio");

        }

        private void cbAnular_Click(Object eventSender, EventArgs eventArgs)
        {
            StringBuilder sql = new StringBuilder();
            DataSet RrSql = null;

            bool fAnular = false;
            //para anular una toma tiene que estar seleccionada
            //no tiene que estar facturada
            if (fHayFilaSeleccionada)
            {
                //comprobamos que la toma se pueda anular
                for (int iFarmaco = 1; iFarmaco <= mbConfirmacionTomas.FarmacosPantalla.GetUpperBound(0); iFarmaco++)
                {
                    //por cada uno de los farmacos pertenecientes a la perfusion
                    sql = new StringBuilder("select * from ETOMASFA where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ");
                    sql.Append("and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ");
                    sql.Append("and gfarmaco = '" + mbConfirmacionTomas.FarmacosPantalla[iFarmaco].CodFarmaco + "' ");
                    //UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                    object tempRefParam = mbConfirmacionTomas.GstFechaApunte;
                    sql.Append("and fapuntes =" + Serrores.FormatFechaHMS(tempRefParam) + " ");
                    mbConfirmacionTomas.GstFechaApunte = Convert.ToString(tempRefParam);
                    //UPGRADE_WARNING: (1068) tempRefParam2 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                    object tempRefParam2 = stFechaPrevista;
                    sql.Append("and fprevist =" + Serrores.FormatFechaHMS(tempRefParam2) + " ");
                    stFechaPrevista = Convert.ToString(tempRefParam2);

                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                    RrSql = new DataSet();
                    tempAdapter.Fill(RrSql);

                    if (RrSql.Tables[0].Rows.Count != 0)
                    {
                        //comprobamos que la toma no este facturada
                        //si alguna de las tomas ya esta facturada no podemos anularla                        
                        if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["fpasfact"]))
                        {
                            fAnular = true;
                        }
                        else
                        {
                            fAnular = false;                            
                            break;
                        }
                    }
                    else
                    {
                        //la toma no esta en ETOMASFA
                        fAnular = false;                        
                        break;
                    }                    
                }

                if (fAnular)
                {
                    //se pueden anular las tomas
                    if (RadMessageBox.Show("�Esta seguro de que quiere anular la toma seleccionada?", Serrores.vNomAplicacion, MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                    {
                        //si responde que no
                        return;
                    }
                    else
                    {
                        //responde que si
                        //se pasa el registro de la toma a la tabla de tomas anuladas
                        //con la fecha de anulacion FANULACI = Fecha del sistema
                        //UPGRADE_ISSUE: (2064) RDO.rdoConnection method RcEnfermeria.BeginTrans was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();
                        SqlTransaction transaction = mbConfirmacionTomas.RcEnfermeria.BeginTrans();
                        //por cada uno de los farmacos componentes del suero
                        try
                        {
                            for (int iFarmaco = 1; iFarmaco <= mbConfirmacionTomas.FarmacosPantalla.GetUpperBound(0); iFarmaco++)
                            {
                                sql = new StringBuilder("insert into ETOMANUL (itiposer,ganoregi,gnumregi,fapuntes,gfarmaco,fprevist,ftomasfa,");
                                sql.Append(" gusuario, fpasfact, cdosisto, oobserva, gfarmadm, iPropPac,fanulaci,cdosimpu,vperfcon )");
                                sql.Append("Select ETOMASFA.itiposer,ETOMASFA.ganoregi,ETOMASFA.gnumregi,ETOMASFA.fapuntes,");
                                sql.Append(" ETOMASFA.gfarmaco , ETOMASFA.fprevist, ETOMASFA.ftomasfa, ");
                                sql.Append("'" + mbConfirmacionTomas.UsuarioConfirmacion.Trim().ToUpper() + "', ETOMASFA.fpasfact, ETOMASFA.cdosisto,");
                                object tempRefParam3 = DateTime.Now;
                                sql.Append(" ETOMASFA.oobserva, ETOMASFA.gfarmadm, ETOMASFA.iPropPac," + Serrores.FormatFechaHMS(tempRefParam3) + ",ETOMASFA.cdosimpu, ETOMASFA.vperfcon ");
                                sql.Append(" from ETOMASFA where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ");
                                sql.Append(" and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " ");
                                sql.Append(" and gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ");
                                //UPGRADE_WARNING: (1068) tempRefParam4 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam4 = mbConfirmacionTomas.FarmacosPantalla[iFarmaco].FechaApunte;
                                sql.Append(" and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam4) + " ");
                                mbConfirmacionTomas.FarmacosPantalla[iFarmaco].FechaApunte = Convert.ToString(tempRefParam4);
                                sql.Append(" and gfarmaco = '" + mbConfirmacionTomas.FarmacosPantalla[iFarmaco].CodFarmaco.Trim() + "' ");
                                //UPGRADE_WARNING: (1068) tempRefParam5 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam5 = stFechaPrevista;
                                sql.Append(" and fprevist =" + Serrores.FormatFechaHMS(tempRefParam5) + " ");
                                stFechaPrevista = Convert.ToString(tempRefParam5);

                                SqlCommand tempCommand = new SqlCommand(sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                                tempCommand.ExecuteNonQuery();

                                sql = new StringBuilder("delete from ETOMASFA where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ");
                                sql.Append(" and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " ");
                                sql.Append(" and  gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ");
                                //UPGRADE_WARNING: (1068) tempRefParam6 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam6 = mbConfirmacionTomas.FarmacosPantalla[iFarmaco].FechaApunte;
                                sql.Append(" and fapuntes = " + Serrores.FormatFechaHMS(tempRefParam6) + " ");
                                mbConfirmacionTomas.FarmacosPantalla[iFarmaco].FechaApunte = Convert.ToString(tempRefParam6);
                                sql.Append(" and gfarmaco = '" + mbConfirmacionTomas.FarmacosPantalla[iFarmaco].CodFarmaco.Trim() + "' ");
                                //UPGRADE_WARNING: (1068) tempRefParam7 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam7 = stFechaPrevista;
                                sql.Append(" and fprevist =" + Serrores.FormatFechaHMS(tempRefParam7) + " ");
                                stFechaPrevista = Convert.ToString(tempRefParam7);

                                SqlCommand tempCommand_2 = new SqlCommand(sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                                tempCommand_2.ExecuteNonQuery();

                                mbConfirmacionTomas.RcEnfermeria.CommitTrans(transaction);                                
                            }
                        }
                        catch
                        {
                            mbConfirmacionTomas.RcEnfermeria.RollbackTrans(transaction);
                        }
                        finally
                        {
                            transaction.Dispose();
                        }
                        
                        
                        //una vez que hemos anulado las tomas correspondientes volvemos a cargar la pantalla
                        this.Enabled = true;
                        SprFarmacos.MaxRows = 0;
                        mbConfirmacionTomas.vstFechaHoraSis = DateTime.Now.ToString("dd/MM/yyyy HH:NN:SS");
                        if (FechaMaximaToma.Trim() == "")
                        {
                            mbConfirmacionTomas.GstConfirmaMaxima = mbConfirmacionTomas.vstFechaHoraSis;
                        }
                        Llenar_grid();
                        IniciarPantalla();
                        ProtegerPantalla("vacio");
                    } //fin de toma anulada
                }
                else
                {
                    //no se puede anular la toma ya esta facturada
                    short tempRefParam8 = 1420;
                    string[] tempRefParam9 = new string[] { "La toma seleccionada no se puede anular", "facturada" };
                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam8, mbConfirmacionTomas.RcEnfermeria, tempRefParam9);
                }
            }
            else
            {
                //debe seleccionar un registro
                short tempRefParam10 = 1270;
                string[] tempRefParam11 = new string[] { "una fila" };
                mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam10, mbConfirmacionTomas.RcEnfermeria, tempRefParam11);
            }

        }

        private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
        {
            IniciarPantalla();
            ProtegerPantalla("vacio");
        }

        private void cbNuevo_Click(Object eventSender, EventArgs eventArgs)
        {
            iApunteToma = NUEVATOMA;

            mbConfirmacionTomas.vstFechaHoraSis = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

            System.DateTime TempDate = DateTime.FromOADate(0);
            sdcFecha.Value = DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : mbConfirmacionTomas.vstFechaHoraSis);
            System.DateTime TempDate3 = DateTime.FromOADate(0);
            System.DateTime TempDate2 = DateTime.FromOADate(0);
            tbHora.Text = ((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate2)) ? TempDate2.ToString("HH") : mbConfirmacionTomas.vstFechaHoraSis) + ":" + ((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate3)) ? TempDate3.ToString("mm") : mbConfirmacionTomas.vstFechaHoraSis);

            ProtegerPantalla("nuevo");

            tbHora.Focus();
        }

        private void cbSalir_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }

        private void ConfirmarPerfusiones_Activated(Object eventSender, EventArgs eventArgs)
        {
            if (ActivateHelper.myActiveForm != eventSender)
            {
                ActivateHelper.myActiveForm = (Form)eventSender;
                try
                {
                    //Aplicar el control de acceso a la pantalla
                    if (!mbAcceso.AplicarControlAcceso(mbConfirmacionTomas.UsuarioConfirmacion.ToUpper(), mbConfirmacionTomas.GstTiposervicio, this, "EGI2151F4", ref mbConfirmacionTomas.astrcontroles, ref mbConfirmacionTomas.astreventos, mbConfirmacionTomas.RcEnfermeria))
                    {
                        return;
                    }

                    //cargamos los datos de los farmacos que componen la perfusi�n
                    //los tenemos guardados en FarmacosPantalla()
                    listFarmacos.Clear();

                    for (int aik = 1; aik <= mbConfirmacionTomas.FarmacosPantalla.GetUpperBound(0); aik++)
                    {
                        if (aik == 1)
                        {
                            lbDisolucion.Text = mbConfirmacionTomas.FarmacosPantalla[aik].NombreFarmaco.Trim() + " - ";
                            lbDisolucion.Text = lbDisolucion.Text + mbConfirmacionTomas.FarmacosPantalla[aik].Dosis.Trim() + " ";
                            lbDisolucion.Text = lbDisolucion.Text + mbConfirmacionTomas.FarmacosPantalla[aik].DesTomas.Trim();
                            //            LbVelocidad.Caption = Trim(FarmacosPantalla(aik).vperfcon) & " ml/hora"
                            tbVelocidad.Text = "";
                            //tbVelocidad.Text = Trim(FarmacosPantalla(aik).vperfcon)
                        }
                        else
                        {
                            listFarmacos.AddItem(mbConfirmacionTomas.FarmacosPantalla[aik].NombreFarmaco.Trim() + " - " + mbConfirmacionTomas.FarmacosPantalla[aik].Dosis.Trim() + " " + mbConfirmacionTomas.FarmacosPantalla[aik].DesTomas.Trim(),"");
                        }
                    }

                    //(maplaza)(17/11/2006)Se debe conocer si en el per�odo del episodio est� ausente el paciente, para saber
                    //as� si se pueden confirmar tomas o no.
                    vfAusencia = ConMotivoDeAusencia();
                    //-----

                    //iniciamos el spread de las tomas reales de la perfusi�n
                    SprFarmacos.MaxRows = 0;
                    mbConfirmacionTomas.vstFechaHoraSis = DateTime.Now.ToString("dd/MM/yyyy HH:NN:SS");
                    FechaMaximaToma = mbConfirmacionTomas.GstConfirmaMaxima.Trim();
                    FechaMinimaToma = mbConfirmacionTomas.GstConfirmaMinima.Trim();
                    FechasPantalla();
                    IniciarPantalla();
                    Llenar_grid();
                    ProtegerPantalla("vacio");                    
                    SprFarmacos.setSelModeIndex(0);
                    cbSalir.Focus();

                    return;
                }
                catch (SqlException ex)
                {

                    Serrores.AnalizaError("Perfusiones.dll", Path.GetDirectoryName(Application.ExecutablePath), mbConfirmacionTomas.gUsuario, "Activate_PantallaConfirmar", ex);
                    this.Close();
                }
            }
        }
        
        private void ConfirmarPerfusiones_Load(Object eventSender, EventArgs eventArgs)
        {

            //UPGRADE_ISSUE: (2070) Constant App was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2070.aspx
            //UPGRADE_ISSUE: (2064) App property App.HelpFile was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            //UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\Ayuda_DLLS.hlp");
        }

        private void SprFarmacos_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
        {
            int Col = eventArgs.ColumnIndex+1;
            int Row = eventArgs.RowIndex+1;
            //cuando se selecciona un registro del spread se cargan los valores en los campos de texto
            //***********
            SprFarmacos.Row = Row;
            //se se selecciona el registro de cabecera o no se selecciona ninguno
            if (SprFarmacos.Row <= 0)
            {
                return;
            }

            if (lTomasSeleccionado == Row)
            {
                //si el seleccionado coincide con el seleccionado anterior
                if (fHayFilaSeleccionada)
                {
                    //SprFarmacos.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.ReadOnly; //Se desmarca la fila (Por la Banda)
                    fHayFilaSeleccionada = false;
                }
                else
                {
                    //SprFarmacos.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; //Se marca la fila
                    fHayFilaSeleccionada = true;
                }
            }
            else
            {
                //SprFarmacos.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.SingleSelect; //Se marca la fila
                fHayFilaSeleccionada = true;
            }

            if (fHayFilaSeleccionada)
            {
                //se ha seleccionado un registro
                iApunteToma = CONFIRMATOMA;
                lTomasSeleccionado = Row;
                SprFarmacos.Row = SprFarmacos.SelBlockRow;
                SprFarmacos.Col = 1;
                if (SprFarmacos.Text != "")
                { //si se ha seleccionado uno con datos
                    //cargar los datos del registro seleccionado en los campos de texto de la pantalla
                    sdcFecha.Value = DateTime.Parse(DateTime.Parse(SprFarmacos.Text).ToString("dd/MM/yyyy"));
                    tbHora.Text = DateTime.Parse(SprFarmacos.Text).ToString("HH") + ":" + DateTime.Parse(SprFarmacos.Text).ToString("mm");

                    SprFarmacos.Col = 2; //observaciones
                    tbObservaciones.Text = SprFarmacos.Text.Trim();

                    SprFarmacos.Col = 3; //usuario
                    stUsuarioRegistro = SprFarmacos.Text.Trim();

                    SprFarmacos.Col = 4; //fecha prevista
                    stFechaPrevista = SprFarmacos.Text.Trim();

                    SprFarmacos.Col = 5; //fecha del apunte
                    stFechaApunte = SprFarmacos.Text.Trim();

                    SprFarmacos.Col = 6; //Velocidad confirmada
                    string tempRefParam = SprFarmacos.Text.Trim();
                    tbVelocidad.Text = Serrores.ConvertirDecimales(ref tempRefParam, ref mbConfirmacionTomas.vstSeparadorDecimal, 1);

                    ProtegerPantalla("datos");
                }
            }
            else
            {
                //si no hay fila seleccionada se deben de limpiar los campos
                IniciarPantalla();
                ProtegerPantalla("vacio");
            }
        }

        private void Cargar_grid(string stFechaApunte, string stFechaPrevista, string stFechaConfirma, string stObservaciones, object stUsuario, ref string stVelocidad)
        {
            int iFila = 0;
            //procedimiento que rellena el spread de las tomas

            if (SprFarmacos.MaxRows == 0)
            {
                SprFarmacos.MaxRows = 1;
                iFila = SprFarmacos.MaxRows + 1;
            }
            else
            {
                SprFarmacos.MaxRows++;
                iFila = SprFarmacos.MaxRows + 1;
            }
            SprFarmacos.Row = iFila;
            SprFarmacos.Col = 1; //fecha/Hora toma
            System.DateTime TempDate = DateTime.FromOADate(0);
            SprFarmacos.Text = (DateTime.TryParse(stFechaConfirma, out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm") : stFechaConfirma;

            SprFarmacos.Col = 2; //observaciones
            SprFarmacos.Text = stObservaciones;

            if (stUsuario != Type.Missing)
            {
                SprFarmacos.Col = 3; //viene el usuario que ha grabado el registro
                SprFarmacos.Text = stUsuario.ToString();
            }
            else
            {
                //no hay usuario
                SprFarmacos.Col = 3;
                SprFarmacos.Text = "";
            }

            SprFarmacos.Col = 4; //fecha/Hora prevista columna oculta
            System.DateTime TempDate2 = DateTime.FromOADate(0);
            SprFarmacos.Text = (DateTime.TryParse(stFechaPrevista, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:NN:SS") : stFechaPrevista;

            SprFarmacos.Col = 5; //fecha/Hora apunte columna oculta
            System.DateTime TempDate3 = DateTime.FromOADate(0);
            SprFarmacos.Text = (DateTime.TryParse(stFechaApunte, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:NN:SS") : stFechaApunte;

            SprFarmacos.Col = 6;
            SprFarmacos.Text = Serrores.ConvertirDecimales(ref stVelocidad, ref mbConfirmacionTomas.vstSeparadorDecimal, 1);

        }

        private void Cargar_grid(string stFechaApunte, string stFechaPrevista, string stFechaConfirma, string stObservaciones, object stUsuario)
        {
            string tempRefParam = String.Empty;
            Cargar_grid(stFechaApunte, stFechaPrevista, stFechaConfirma, stObservaciones, stUsuario, ref tempRefParam);
        }

        private void Cargar_grid(string stFechaApunte, string stFechaPrevista, string stFechaConfirma, string stObservaciones)
        {
            string tempRefParam2 = String.Empty;
            Cargar_grid(stFechaApunte, stFechaPrevista, stFechaConfirma, stObservaciones, String.Empty, ref tempRefParam2);
        }

        private void SprFarmacos_ColumnWidthChanged(object eventSender, ColumnWidthChangedEventArgs eventArgs)
        {
            //MsgBox Str(SprFarmacos.ColWidth(Col2))
        }

        private void SprFarmacos_KeyDown(Object eventSender, KeyEventArgs eventArgs)
        {
            int KeyCode = (int)eventArgs.KeyCode;
            int Shift = (eventArgs.Shift) ? 1 : 0;
            if (KeyCode == ((int)Keys.Up))
            {
                if (SprFarmacos.ActiveRowIndex > 1)
                {
                    SprFarmacos_CellClick(SprFarmacos, null);
                }
                else
                {
                    SprFarmacos.Row = 1;
                    //SprFarmacos.OperationMode = FarPoint.Win.Spread.OperationMode.RowMode;
                    KeyCode = 0;
                }
            }
            if (KeyCode == ((int)Keys.Down))
            {
                if (SprFarmacos.ActiveRowIndex < SprFarmacos.GetLastNonEmptyRow(""))
                {
                    SprFarmacos_CellClick(SprFarmacos, null);
                }
                else
                {
                    SprFarmacos.Row = SprFarmacos.GetLastNonEmptyRow("");
                    ////SprFarmacos.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.RowMode;
                    KeyCode = 0;
                }
            }

            if (KeyCode == ((int)Keys.PageUp))
            {
                if (SprFarmacos.ActiveRowIndex - 7 < 1)
                {
                    KeyCode = 0;
                }
                else
                {
                    SprFarmacos_CellClick(SprFarmacos, null);
                }
            }
            if (KeyCode == ((int)Keys.PageDown))
            {
                if (SprFarmacos.ActiveRowIndex + 7 > SprFarmacos.GetLastNonEmptyRow(""))
                {
                    SprFarmacos.Row = 1;
                    //SprFarmacos.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.RowMode;
                    KeyCode = 0;
                }
                else
                {
                    SprFarmacos_CellClick(SprFarmacos, null);
                }
            }

        }

        private void IniciarPantalla()
        {
            //inicializar los campos de la pantalla
            tbVelocidad.Text = "";
            //dgmorenog
            //sdcFecha.Value = DateTime.Parse("");
            sdcFecha.Value = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            tbHora.Text = "  :  ";
            tbObservaciones.Text = "";
            //seg�n el valor de gfProtegerCeldas se activan o no las celdas
            //UPGRADE_ISSUE: (2064) FPSpread.vaSpread property SprFarmacos.SelModeIndex was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            SprFarmacos.setSelModeIndex(0);
            stUsuarioRegistro = "";
            stFechaPrevista = "";
            fHayFilaSeleccionada = false;
        }

        private void GrabarRegistro()
        {
            StringBuilder sqlDatos = new StringBuilder();
            DataSet RrTomas = null;
            double iCantidadToma = 0;
            string iCantidadAux = String.Empty;

            SqlTransaction transaction = mbConfirmacionTomas.RcEnfermeria.BeginTrans();

            try
            {                                
                //hay que hacer un bucle por cada uno de los registros que tenmos cargados en la estructura
                //para grabar un registro o actualizar los que existan
                for (int iFarmaco = 1; iFarmaco <= mbConfirmacionTomas.FarmacosPantalla.GetUpperBound(0); iFarmaco++)
                {
                    sqlDatos = new StringBuilder("select * from ETOMASFA where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' ");
                    sqlDatos.Append("and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ");
                    sqlDatos.Append("and gfarmaco = '" + mbConfirmacionTomas.FarmacosPantalla[iFarmaco].CodFarmaco + "' ");
                    if (fHayFilaSeleccionada)
                    {
                        //UPGRADE_WARNING: (1068) tempRefParam of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                        object tempRefParam = stFechaApunte;
                        sqlDatos.Append("and fapuntes =" + Serrores.FormatFechaHMS(tempRefParam) + " ");
                        stFechaApunte = Convert.ToString(tempRefParam);
                        //UPGRADE_WARNING: (1068) tempRefParam2 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                        object tempRefParam2 = stFechaPrevista;
                        sqlDatos.Append("and fprevist =" + Serrores.FormatFechaHMS(tempRefParam2) + " ");
                        stFechaPrevista = Convert.ToString(tempRefParam2);
                    }
                    else
                    {
                        //se trata de una toma nueva
                        //UPGRADE_WARNING: (1068) tempRefParam3 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                        object tempRefParam3 = mbConfirmacionTomas.FarmacosPantalla[iFarmaco].FechaApunte;
                        sqlDatos.Append("and fapuntes =" + Serrores.FormatFechaHMS(tempRefParam3) + " ");
                        mbConfirmacionTomas.FarmacosPantalla[iFarmaco].FechaApunte = Convert.ToString(tempRefParam3);
                        object tempRefParam4 = sdcFecha.Value.Date + " " + tbHora.Text + ":00";
                        sqlDatos.Append("and fprevist =" + Serrores.FormatFechaHMS(tempRefParam4) + " ");
                    }

                    SqlCommand command = new SqlCommand(sqlDatos.ToString(), mbConfirmacionTomas.RcEnfermeria, transaction);
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(command);
                    RrTomas = new DataSet();
                    tempAdapter.Fill(RrTomas);
                    if (RrTomas.Tables[0].Rows.Count != 0)
                    { //si encuentra el registro
                        //se puede modificar siempre que el usuario que lo ha grabado sea el mismo
                        if (!fHayFilaSeleccionada)
                        {
                            //si no se ha seleccionado ningun registro
                            short tempRefParam5 = 1080;
                            string[] tempRefParam6 = new string[] { "Toma confirmada " };
                            mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, mbConfirmacionTomas.RcEnfermeria, tempRefParam6);                            
                            break;
                        }
                        else
                        {                            
                            if (tbObservaciones.Text.Trim() == "")
                            {                                
                                RrTomas.Tables[0].Rows[0]["oobserva"] = DBNull.Value;
                            }
                            else
                            {                                
                                RrTomas.Tables[0].Rows[0]["oobserva"] = tbObservaciones.Text.Trim();
                            }
                            
                            string tempRefParam7 = tbVelocidad.Text.Trim();
                            RrTomas.Tables[0].Rows[0]["vperfcon"] = Serrores.ConvertirDecimales(ref tempRefParam7, ref mbConfirmacionTomas.stDecimalBD);                            
                            string tempQuery = RrTomas.Tables[0].TableName;                            
                            SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                            tempAdapter.Update(RrTomas, tempQuery);
                            //que lo va a modificar
                        }
                    }
                    else
                    {
                        //si no encuentra el registro hay que grabar uno nuevo                        
                        RrTomas.AddNew();                        
                        RrTomas.Tables[0].Rows[0]["itiposer"] = mbConfirmacionTomas.GstTiposervicio;                        
                        RrTomas.Tables[0].Rows[0]["ganoregi"] = mbConfirmacionTomas.GstA�oFarmaco;                        
                        RrTomas.Tables[0].Rows[0]["gnumregi"] = mbConfirmacionTomas.GstNumeroFarmaco;                        
                        RrTomas.Tables[0].Rows[0]["fapuntes"] = mbConfirmacionTomas.FarmacosPantalla[iFarmaco].FechaApunte;                        
                        RrTomas.Tables[0].Rows[0]["gfarmaco"] = mbConfirmacionTomas.FarmacosPantalla[iFarmaco].CodFarmaco;
                        //en las perfusiones no hay posibilidad de seleccionar f�rmaco equivalente                        
                        RrTomas.Tables[0].Rows[0]["gfarmadm"] = DBNull.Value;                        
                        RrTomas.Tables[0].Rows[0]["fprevist"] = DateTime.Parse(sdcFecha.Value.Date + " " + tbHora.Text + ":00");                        
                        RrTomas.Tables[0].Rows[0]["ftomasfa"] = DateTime.Parse(sdcFecha.Value.Date + " " + tbHora.Text + ":00");
                        RrTomas.Tables[0].Rows[0]["gUsuario"] = mbConfirmacionTomas.UsuarioConfirmacion.ToUpper();
                        RrTomas.Tables[0].Rows[0]["fpasfact"] = DBNull.Value;                        
                        RrTomas.Tables[0].Rows[0]["cdosisto"] = Serrores.ConvertirDecimales(ref mbConfirmacionTomas.FarmacosPantalla[iFarmaco].Dosis, ref mbConfirmacionTomas.stDecimalBD);
                        if (tbObservaciones.Text.Trim() == "")
                        {                            
                            RrTomas.Tables[0].Rows[0]["oobserva"] = DBNull.Value;
                        }
                        else
                        {                            
                            RrTomas.Tables[0].Rows[0]["oobserva"] = tbObservaciones.Text.Trim();
                        }                        
                        string tempRefParam8 = tbVelocidad.Text.Trim();
                        RrTomas.Tables[0].Rows[0]["vperfcon"] = Serrores.ConvertirDecimales(ref tempRefParam8, ref mbConfirmacionTomas.stDecimalBD);
                        DatosDosisFarmaco(mbConfirmacionTomas.FarmacosPantalla[iFarmaco].CodFarmaco);
                        
                        RrTomas.Tables[0].Rows[0]["iPropPac"] = "N"; //indicador de propio paciente
                        //************
                        if (iEnvaseFraccionable == "N" && iMedidaFraccionable == "S")
                        {                            
                            string tempRefParam9 = DosisAImputar(mbConfirmacionTomas.FarmacosPantalla[iFarmaco].CodFarmaco, mbConfirmacionTomas.FarmacosPantalla[iFarmaco].Dosis, stUnidadEquivalencia);
                            RrTomas.Tables[0].Rows[0]["cdosimpu"] = Serrores.ConvertirDecimales(ref tempRefParam9, ref mbConfirmacionTomas.stDecimalBD);
                        }
                        else
                        {
                            if (iMedidaFraccionable == "S")
                            {
                                //si la dosis es fraccionable seguimos como hasta ahora                                
                                RrTomas.Tables[0].Rows[0]["cdosimpu"] = Serrores.ConvertirDecimales(ref mbConfirmacionTomas.FarmacosPantalla[iFarmaco].Dosis, ref mbConfirmacionTomas.stDecimalBD);
                            }
                            else
                            {
                                //la dosis no es fraccionable hay que calcular la dosis a imputar
                                //la cantidad de toma la tengo que redondear a la unidad de medida
                                iCantidadToma = (Double.Parse(mbConfirmacionTomas.FarmacosPantalla[iFarmaco].Dosis) / Double.Parse(stUnidadEquivalencia));
                                if (iCantidadToma - Math.Floor(iCantidadToma) == 0)
                                {
                                    iCantidadToma *= Double.Parse(stUnidadEquivalencia); //para convertirlo a unidad de medida
                                }
                                else
                                {
                                    iCantidadToma = (Math.Floor(iCantidadToma) + 1) * Double.Parse(stUnidadEquivalencia); //para convertirlo a unidad de medida
                                }
                                iCantidadAux = iCantidadToma.ToString();                                
                                RrTomas.Tables[0].Rows[0]["cdosimpu"] = Serrores.ConvertirDecimales(ref iCantidadAux, ref mbConfirmacionTomas.stDecimalBD);
                            }
                        }                        
                        string tempQuery_2 = RrTomas.Tables[0].TableName;                        
                        SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
                        tempAdapter.Update(RrTomas, tempQuery_2);
                    }                    
                }

                proAvisoCambioVelocidadPerfusion(); //Oscar C Septiembre 2010
                mbConfirmacionTomas.RcEnfermeria.CommitTrans(transaction);
            }
            catch (SqlException ex)
            {
                mbConfirmacionTomas.RcEnfermeria.RollbackTrans(transaction);
                Serrores.AnalizaError("Perfusiones.dll", Path.GetDirectoryName(Application.ExecutablePath), mbConfirmacionTomas.gUsuario, "GrabarTomas", ex);
            }
            finally
            {
                transaction.Dispose();
            }
        }

        //Oscar C Septiembre 2010
        //Si la velocidad de perfusion confirmada es diferente a la velocidad de perfusion prescrita.....
        //se avisa al medico responsable del paciente o al medico presciptor
        private void proAvisoCambioVelocidadPerfusion()
        {
            StringBuilder stFarmacos = new StringBuilder();
            string sql = String.Empty;
            DataSet rrDatos = null;
            int iA�oMensaje = 0;
            int iNumMensaje = 0;
            string stAsuntoMensaje = String.Empty;
            string stCuerpoMensaje = String.Empty;
            string stNombrePaciente = String.Empty;
            int iRespPaciente = 0;
            string stFechaSistema = String.Empty;


            int iMedicoPrescriptor = 0;
            string stAviso = Serrores.ObternerValor_CTEGLOBAL(mbConfirmacionTomas.RcEnfermeria, "AVICOVPE", "VALFANU1");

            string tempRefParam = mbConfirmacionTomas.FarmacosPantalla[1].veloperf.Trim();
            string tempRefParam2 = tbVelocidad.Text.Trim();
            if (Serrores.ConvertirDecimales(ref tempRefParam, ref mbConfirmacionTomas.vstSeparadorDecimal, 1) != Serrores.ConvertirDecimales(ref tempRefParam2, ref mbConfirmacionTomas.vstSeparadorDecimal, 1) && tbObservaciones.Text.Trim() != "" && stAviso.Trim() != "")
            {
                stFechaSistema = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                switch (mbConfirmacionTomas.GstTiposervicio)
                {
                    case "H":
                        sql = " SELECT DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, AEPISADM.gperulti gpersona " +
                              " FROM DPACIENT " +
                              " INNER JOIN AEPISADM ON AEPISADM.GIDENPAC = DPACIENT.GIDENPAC " +
                              " WHERE " +
                              "     AEPISADM.GANOADME = " + mbConfirmacionTomas.GstA�oFarmaco +
                              " AND AEPISADM.GNUMADME = " + mbConfirmacionTomas.GstNumeroFarmaco;
                        break;
                    case "U":
                        sql = " SELECT DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, UEPISURG.gpersona  " +
                              " FROM DPACIENT " +
                              " INNER JOIN UEPISURG ON UEPISURG.GIDENPAC = DPACIENT.GIDENPAC " +
                              " WHERE " +
                              "     UEPISURG.GANOURGE = " + mbConfirmacionTomas.GstA�oFarmaco +
                              " AND UEPISURG.GNUMURGE = " + mbConfirmacionTomas.GstNumeroFarmaco;
                        break;
                    case "C":
                        sql = " SELECT DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, CCONSULT.gpersona  " +
                              " FROM DPACIENT " +
                              " INNER JOIN CCONSULT ON CCONSULT.GIDENPAC = DPACIENT.GIDENPAC " +
                              " WHERE " +
                              "     CCONSULT.GANOREGI = " + mbConfirmacionTomas.GstA�oFarmaco +
                              " AND CCONSULT.GNUMREGI = " + mbConfirmacionTomas.GstNumeroFarmaco;
                        break;
                    case "Q":
                        sql = " SELECT DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.dnombpac, QINTQUIR.gmedicoc gpersona " +
                              " FROM DPACIENT " +
                              " INNER JOIN QINTQUIR ON QINTQUIR.GIDENPAC = DPACIENT.GIDENPAC " +
                              " WHERE " +
                              "     QINTQUIR.GANOREGI = " + mbConfirmacionTomas.GstA�oFarmaco +
                              " AND QINTQUIR.GNUMREGI = " + mbConfirmacionTomas.GstNumeroFarmaco;
                        //��Case "N"?? 
                        break;
                }
                SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mbConfirmacionTomas.RcEnfermeria);
                rrDatos = new DataSet();
                tempAdapter.Fill(rrDatos);                
                stNombrePaciente = (Convert.ToString(rrDatos.Tables[0].Rows[0]["dnombpac"]) + "").Trim() + " " + (Convert.ToString(rrDatos.Tables[0].Rows[0]["dape1pac"]) + "").Trim() + " " + (Convert.ToString(rrDatos.Tables[0].Rows[0]["dape2pac"]) + "").Trim();                
                iRespPaciente = Convert.ToInt32(rrDatos.Tables[0].Rows[0]["GPERSONA"]);
                
                for (int i = 1; i <= mbConfirmacionTomas.FarmacosPantalla.GetUpperBound(0); i++)
                {
                    stFarmacos.Append(mbConfirmacionTomas.FarmacosPantalla[i].NombreFarmaco + ", ");
                }
                stFarmacos = new StringBuilder(stFarmacos.ToString().Substring(0, Math.Min(stFarmacos.ToString().Length - 2, stFarmacos.ToString().Length)));

                stAsuntoMensaje = "CAMBIO EN LA VELOCIDAD DE PERFUSION CONFIRMADA PARA " + stNombrePaciente;
                string tempRefParam3 = mbConfirmacionTomas.FarmacosPantalla[1].veloperf.Trim();
                string tempRefParam4 = tbVelocidad.Text.Trim();
                stCuerpoMensaje = "SE HAN CONFIRMADO LOS FARMACOS " + stFarmacos.ToString().ToUpper() +
                                  " CON UNA VELOCIDAD DE PERFUSION DISTINTA DE LA VELOCIDAD DE PERFUSION PRESCRITA. " + Environment.NewLine +
                                  " VELOCIDAD PRESCRITA: " + Serrores.ConvertirDecimales(ref tempRefParam3, ref mbConfirmacionTomas.vstSeparadorDecimal, 1) + " ml/h" + Environment.NewLine +
                                  " VELOCIDAD CONFIRMADA: " + Serrores.ConvertirDecimales(ref tempRefParam4, ref mbConfirmacionTomas.vstSeparadorDecimal, 1) + " ml/h" + Environment.NewLine +
                                  " OBSERVACIONES DE LA TOMA: " + tbObservaciones.Text.Trim();

                sql = "SELECT MAX(NMENSAJE) + 1, YEAR(GETDATE()) FROM stexmens WHERE amensaje=YEAR(GETDATE())";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, mbConfirmacionTomas.RcEnfermeria);
                rrDatos = new DataSet();
                tempAdapter_2.Fill(rrDatos);                
                iA�oMensaje = Convert.ToInt32(rrDatos.Tables[0].Rows[0][1]);                
                iNumMensaje = (Convert.IsDBNull(rrDatos.Tables[0].Rows[0][0])) ? 1 : Convert.ToInt32(rrDatos.Tables[0].Rows[0][0]);
                
                //UPGRADE_WARNING: (1068) tempRefParam5 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                object tempRefParam5 = stFechaSistema;
                sql = "INSERT INTO STEXMENS (AMENSAJE, NMENSAJE, ASUNTO, TEXTOMEN, FECHMES, IRAYOSMEN, GUSUARIO, IMENAUTO)" +
                      " VALUES (" + iA�oMensaje.ToString() + ", " + iNumMensaje.ToString() + ", '" + stAsuntoMensaje + "', '" + stCuerpoMensaje + "', " + Serrores.FormatFechaHMS(tempRefParam5) + " , 'N' , '" + mbConfirmacionTomas.gUsuario + "', 'S')";
                stFechaSistema = Convert.ToString(tempRefParam5);
                SqlCommand tempCommand = new SqlCommand(sql, mbConfirmacionTomas.RcEnfermeria);
                tempCommand.ExecuteNonQuery();


                switch (stAviso)
                {
                    case "P":

                        //Aviso al Medico Prescriptor 
                        for (int i = 1; i <= mbConfirmacionTomas.FarmacosPantalla.GetUpperBound(0); i++)
                        {
                            if (iMedicoPrescriptor != Conversion.Val(mbConfirmacionTomas.FarmacosPantalla[i].CodMedico))
                            {
                                stFechaSistema = DateTime.Parse(stFechaSistema).AddSeconds(1).ToString("dd/MM/yyyy HH:mm:ss");
                                //UPGRADE_WARNING: (1068) tempRefParam6 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam6 = stFechaSistema;
                                sql = "INSERT INTO SMENSUSU (GUSUARIO, AMENSAJE, NMENSAJE, FECHLECT, FECHENVI) " +
                                      " SELECT distinct SUSUARIO.gusuario, " + iA�oMensaje.ToString() + ", " + iNumMensaje.ToString() + ", NULL, " + Serrores.FormatFechaHMS(tempRefParam6) +
                                      " FROM SUSUARIO " +
                                      " INNER JOIN DPERSONAVA on DPERSONAVa.gpersona = SUSUARIO.gpersona " +
                                      " WHERE SUSUARIO.GPERSONA = " + Conversion.Val(mbConfirmacionTomas.FarmacosPantalla[i].CodMedico).ToString();
                                stFechaSistema = Convert.ToString(tempRefParam6);
                                SqlCommand tempCommand_2 = new SqlCommand(sql, mbConfirmacionTomas.RcEnfermeria);
                                tempCommand_2.ExecuteNonQuery();
                                iMedicoPrescriptor = Convert.ToInt32(Conversion.Val(mbConfirmacionTomas.FarmacosPantalla[i].CodMedico));
                            }
                        }

                        break;
                    case "R":

                        //Aviso al Responsable del Paciente 
                        stFechaSistema = DateTime.Parse(stFechaSistema).AddSeconds(1).ToString("dd/MM/yyyy HH:mm:ss");
                        //UPGRADE_WARNING: (1068) tempRefParam7 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx 
                        object tempRefParam7 = stFechaSistema;
                        sql = "INSERT INTO SMENSUSU (GUSUARIO, AMENSAJE, NMENSAJE, FECHLECT, FECHENVI) " +
                              " SELECT distinct SUSUARIO.gusuario, " + iA�oMensaje.ToString() + ", " + iNumMensaje.ToString() + ", NULL, " + Serrores.FormatFechaHMS(tempRefParam7) +
                              " FROM SUSUARIO " +
                              " INNER JOIN DPERSONAVA on DPERSONAVa.gpersona = SUSUARIO.gpersona " +
                              " WHERE SUSUARIO.GPERSONA = " + iRespPaciente.ToString();
                        stFechaSistema = Convert.ToString(tempRefParam7);
                        SqlCommand tempCommand_3 = new SqlCommand(sql, mbConfirmacionTomas.RcEnfermeria);
                        tempCommand_3.ExecuteNonQuery();

                        break;
                    case "A":

                        //Aviso al Medico Prescriptor 
                        for (int i = 1; i <= mbConfirmacionTomas.FarmacosPantalla.GetUpperBound(0); i++)
                        {
                            if (iMedicoPrescriptor != Conversion.Val(mbConfirmacionTomas.FarmacosPantalla[i].CodMedico))
                            {
                                stFechaSistema = DateTime.Parse(stFechaSistema).AddSeconds(1).ToString("dd/MM/yyyy HH:mm:ss");
                                //UPGRADE_WARNING: (1068) tempRefParam8 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
                                object tempRefParam8 = stFechaSistema;
                                sql = "INSERT INTO SMENSUSU (GUSUARIO, AMENSAJE, NMENSAJE, FECHLECT, FECHENVI) " +
                                      " SELECT distinct SUSUARIO.gusuario, " + iA�oMensaje.ToString() + ", " + iNumMensaje.ToString() + ", NULL, " + Serrores.FormatFechaHMS(tempRefParam8) +
                                      " FROM SUSUARIO " +
                                      " INNER JOIN DPERSONAVA on DPERSONAVa.gpersona = SUSUARIO.gpersona " +
                                      " WHERE SUSUARIO.GPERSONA = " + Conversion.Val(mbConfirmacionTomas.FarmacosPantalla[i].CodMedico).ToString();
                                stFechaSistema = Convert.ToString(tempRefParam8);
                                SqlCommand tempCommand_4 = new SqlCommand(sql, mbConfirmacionTomas.RcEnfermeria);
                                tempCommand_4.ExecuteNonQuery();
                                iMedicoPrescriptor = Convert.ToInt32(Conversion.Val(mbConfirmacionTomas.FarmacosPantalla[i].CodMedico));
                            }
                        }

                        //Aviso al Responsable del Paciente 
                        stFechaSistema = DateTime.Parse(stFechaSistema).AddSeconds(1).ToString("dd/MM/yyyy HH:mm:ss");
                        //UPGRADE_WARNING: (1068) tempRefParam9 of type Variant is being forced to string. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx 
                        object tempRefParam9 = stFechaSistema;
                        sql = "INSERT INTO SMENSUSU (GUSUARIO, AMENSAJE, NMENSAJE, FECHLECT, FECHENVI) " +
                              " SELECT distinct SUSUARIO.gusuario, " + iA�oMensaje.ToString() + ", " + iNumMensaje.ToString() + ", NULL, " + Serrores.FormatFechaHMS(tempRefParam9) +
                              " FROM SUSUARIO " +
                              " INNER JOIN DPERSONAVA on DPERSONAVa.gpersona = SUSUARIO.gpersona " +
                              " WHERE SUSUARIO.GPERSONA = " + iRespPaciente.ToString();
                        stFechaSistema = Convert.ToString(tempRefParam9);
                        SqlCommand tempCommand_5 = new SqlCommand(sql, mbConfirmacionTomas.RcEnfermeria);
                        tempCommand_5.ExecuteNonQuery();

                        break;
                }
            }
        }
        
        private void FechasPantalla()
        {

            sdcFecha.MinDate = DateTime.Parse("01/01/1973");
            sdcFecha.MaxDate = DateTime.Parse("31/12/9998");
            //la fecha maxima no puede ser mayor que la fecha en la que estamos
            System.DateTime TempDate = DateTime.FromOADate(0);
            sdcFecha.MaxDate = DateTime.Parse((DateTime.TryParse(mbConfirmacionTomas.vstFechaHoraSis, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : mbConfirmacionTomas.vstFechaHoraSis);
        }

        private void tbHora_Enter(Object eventSender, EventArgs eventArgs)
        {
            tbHora.SelectionStart = 0;
            tbHora.SelectionLength = Strings.Len(tbHora.Text);
        }

        private void tbHora_Leave(Object eventSender, EventArgs eventArgs)
        {
            int iHora = 0;
            int iMinutos = 0;
            int iRespuesta = 0;

            if (tbHora.Text.Trim() != "" && tbHora.Text.Trim() != ":")
            {
                //tbHora.Text = Format$(tbHora.Text, "hh:mm")
                iHora = Convert.ToInt32(Conversion.Val(tbHora.Text.Substring(0, Math.Min(tbHora.Text.IndexOf(':'), tbHora.Text.Length))));
                iMinutos = Convert.ToInt32(Conversion.Val(tbHora.Text.Substring(tbHora.Text.IndexOf(':') + 1, Math.Min(2, tbHora.Text.Length - (tbHora.Text.IndexOf(':') + 1)))));

                if (iHora < 0 || iHora > 23)
                {
                    short tempRefParam = 1150;
                    string[] tempRefParam2 = new string[] { "Hora real" };
                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, mbConfirmacionTomas.RcEnfermeria, tempRefParam2);
                    //MsgBox "la hora tecleada no es correcta", vbOKOnly & vbCritical, Me.Caption
                    tbHora.Focus();
                    return;
                }
                if (iMinutos < 0 || iMinutos > 59)
                {
                    short tempRefParam3 = 1150;
                    string[] tempRefParam4 = new string[] { "Hora real" };
                    mbConfirmacionTomas.clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, mbConfirmacionTomas.RcEnfermeria, tempRefParam4);
                    //MsgBox "los minutos tecleados no son correctos", vbOKOnly & vbCritical, Me.Caption
                    tbHora.Focus();
                    return;
                }
            }

        }

        private void tbObservaciones_Enter(Object eventSender, EventArgs eventArgs)
        {
            tbObservaciones.SelectionStart = 0;
            tbObservaciones.SelectionLength = Strings.Len(tbObservaciones.Text);
        }

        private void ProtegerPantalla(string stForma)
        {
            //se presenta la pantalla de confirmacion de tomas

            //(maplaza)(17/11/2006)En el caso de que vfAusencia=True, no se deben poder confirmar tomas, as� que se ha
            //a�adido la condici�n "Or (vfAusencia = True)"
            if ((mbConfirmacionTomas.gfProtegerCeldas) || (vfAusencia))
            { //no se pueden confirmar tomas
                sdcFecha.Enabled = false;
                tbHora.Enabled = false;
                tbObservaciones.Enabled = false;
                cbAceptar.Enabled = false;
                cbNuevo.Enabled = false;
                cbAnular.Enabled = false;
                tbVelocidad.Enabled = false;
            }
            else
            {
                //se pueden confirmar tomas
                if (stUsuarioRegistro.Trim() != "")
                {
                    //el registro ya esta grabado => solo lo puede modificar el usuario que ha
                    //grabado el registro y solo puede modificar las observaciones
                    tbVelocidad.Enabled = false;
                    sdcFecha.Enabled = false;
                    tbHora.Enabled = false;
                    cbNuevo.Enabled = false;
                    cbAnular.Enabled = true;
                    if (stUsuarioRegistro.Trim().ToUpper() != mbConfirmacionTomas.UsuarioConfirmacion.Trim().ToUpper())
                    {
                        //no se puede modificar nada
                        tbObservaciones.Enabled = false;
                        cbAceptar.Enabled = false;
                    }
                    else
                    {
                        tbVelocidad.Enabled = true;
                        tbObservaciones.Enabled = true;
                        cbAceptar.Enabled = true;
                    }
                }
                else
                {
                    //no se ha grabado el registro
                    switch (stForma)
                    {
                        case "vacio":  //cuando se limpia la pantalla 
                            sdcFecha.Enabled = false;
                            tbHora.Enabled = false;
                            tbObservaciones.Enabled = false;
                            cbAceptar.Enabled = false;
                            //siempre se tiene que presentar el bot�n "Nuevo" activo 
                            cbNuevo.Enabled = true;
                            cbAnular.Enabled = false;
                            tbVelocidad.Enabled = false;

                            break;
                        case "nuevo":  //cuando se pulsa el bot�n nuevo 
                            sdcFecha.Enabled = true;
                            tbHora.Enabled = true;
                            tbObservaciones.Enabled = true;
                            cbNuevo.Enabled = false;
                            cbAceptar.Enabled = true;
                            cbAnular.Enabled = false;
                            tbVelocidad.Enabled = true;
                            string tempRefParam = mbConfirmacionTomas.FarmacosPantalla[1].veloperf.Trim();
                            tbVelocidad.Text = Serrores.ConvertirDecimales(ref tempRefParam, ref mbConfirmacionTomas.vstSeparadorDecimal, 1);
                            break;
                        default: //cuando se selecciona un registro del spread para ver los datos 
                            sdcFecha.Enabled = true;
                            tbHora.Enabled = true;
                            tbObservaciones.Enabled = true;
                            cbAceptar.Enabled = true;
                            tbVelocidad.Enabled = true;
                            if (fHayFilaSeleccionada)
                            {
                                cbNuevo.Enabled = false;
                                cbAnular.Enabled = true;
                            }
                            else
                            {
                                cbNuevo.Enabled = true;
                                cbAnular.Enabled = false;
                            }
                            break;
                    }
                }
            }
            cbSalir.Focus();
        }

        private void Llenar_grid()
        {
            StringBuilder sql = new StringBuilder();
            DataSet RrTomas = null;
            string stFechaToma = String.Empty;
            string stFechaInicioBucle = String.Empty;
            string stFechafinBucle = String.Empty;
            string stFechaBucle = String.Empty;

            try
            {
                //se llenan con las horas prevista de las ultimas 48 horas
                //si a esa hora hay un registro en etomasfa se rellena las casillas del spread con
                //los valores del registro, sino hay registro se rellena a blancos
                //hay que hacer una rutina que calcule las horas previstas
                //GstConfirmaMaxima lleva la hora del sistema
                //GstConfirmaMinima lleva 48 horas antes de la fecha del sistema o la de inicio del tratamiento

                if (mbConfirmacionTomas.GstConfirmaMaxima.Trim() == "")
                {
                    mbConfirmacionTomas.GstConfirmaMaxima = mbConfirmacionTomas.vstFechaHoraSis;
                }
                if (DateTime.Parse(mbConfirmacionTomas.GstConfirmaMaxima) < DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis))
                {
                    stFechaInicioBucle = mbConfirmacionTomas.GstConfirmaMaxima;
                }
                else
                {
                    stFechaInicioBucle = mbConfirmacionTomas.vstFechaHoraSis;
                }
                //hay que buscar 48 horas antes

                stFechafinBucle = DateTimeHelper.ToString(DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis).AddHours(-48));

                if (DateTime.Parse(mbConfirmacionTomas.GstConfirmaMinima) > DateTime.Parse(stFechafinBucle))
                {
                    stFechafinBucle = mbConfirmacionTomas.GstConfirmaMinima;
                }
                else
                {
                    stFechafinBucle = DateTimeHelper.ToString(DateTime.Parse(mbConfirmacionTomas.vstFechaHoraSis).AddHours(-48));
                }

                System.DateTime TempDate = DateTime.FromOADate(0);
                if (Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate)) ? TempDate.ToString("NN") : stFechafinBucle) > 0)
                {
                    System.DateTime TempDate2 = DateTime.FromOADate(0);
                    if (Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate2)) ? TempDate2.ToString("HH") : stFechafinBucle) == 23)
                    {
                        stFechafinBucle = DateTimeHelper.ToString(DateTime.FromOADate(1).AddDays(DateTime.Parse(stFechafinBucle).ToOADate())) + " 00:00";
                    }
                    else
                    {
                        System.DateTime TempDate4 = DateTime.FromOADate(0);
                        System.DateTime TempDate3 = DateTime.FromOADate(0);
                        stFechafinBucle = ((DateTime.TryParse(stFechafinBucle, out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : stFechafinBucle) + " " + StringsHelper.Format(Conversion.Val((DateTime.TryParse(stFechafinBucle, out TempDate4)) ? TempDate4.ToString("HH") : stFechafinBucle), "00") + ":00";
                    }
                }
                //tengo que cargar la fecha inicial hasta la fecha final
                //el bucle empieza en la hora actual y va hacia abajo
                for (int aik = 0; aik <= 2; aik++)
                {
                    //le descuento dias porque son dias completos desde 0 a 23 horas
                    stFechaBucle = DateTimeHelper.ToString(DateTime.FromOADate(-aik).AddDays(DateTime.Parse(stFechaInicioBucle).ToOADate())); //esta la fecha del bucle "DD/MM/YYYY"
                    //asi obtengo el dia de la fecha del bucle
                    System.DateTime TempDate6 = DateTime.FromOADate(0);
                    System.DateTime TempDate5 = DateTime.FromOADate(0);
                    if (DateTime.Parse((DateTime.TryParse(stFechaBucle, out TempDate5)) ? TempDate5.ToString("dd/MM/yyyy") : stFechaBucle) >= DateTime.Parse((DateTime.TryParse(stFechafinBucle, out TempDate6)) ? TempDate6.ToString("dd/MM/yyyy") : stFechafinBucle) && DateTime.Parse(stFechaBucle) <= DateTime.Parse(stFechaInicioBucle))
                    {
                        //tiene que cargar todas las tomas que se encuentre en la fecha del bucle
                        System.DateTime TempDate7 = DateTime.FromOADate(0);
                        stFechaToma = (DateTime.TryParse(stFechaBucle, out TempDate7)) ? TempDate7.ToString("dd/MM/yyyy") : stFechaBucle;

                        sql = new StringBuilder("select distinct etomasfa.fapuntes,ETOMASFA.fprevist,ETOMASFA.ftomasfa,ETOMASFA.oobserva,ETOMASFA.gusuario,ETOMASFA.vperfcon ");
                        sql.Append(" from ETOMASFA inner join EFARMACO on ETOMASFA.itiposer = EFARMACO.itiposer ");
                        sql.Append(" and ETOMASFA.ganoregi = EFARMACO.ganoregi and ETOMASFA.gnumregi= EFARMACO.gnumregi ");
                        sql.Append(" and ETOMASFA.fapuntes = efarmaco.fapuntes And ETOMASFA.gfarmaco = efarmaco.gfarmaco ");
                        sql.Append(" where ETOMASFA.itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' and ETOMASFA.ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " ");
                        sql.Append(" and ETOMASFA.gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " ");
                        sql.Append(" and EFARMACO.ncodperf = " + mbConfirmacionTomas.GstCodigoFarmaco + " ");
                        
                        object tempRefParam = mbConfirmacionTomas.GstFechaApunte;
                        object tempRefParam2 = mbConfirmacionTomas.GstConfirmaMaxima;
                        sql.Append(" and (efarmaco.fapuntes >=" + Serrores.FormatFechaHMS(tempRefParam) + " and efarmaco.fapuntes < " + Serrores.FormatFechaHMS(tempRefParam2) + ") ");
                        mbConfirmacionTomas.GstConfirmaMaxima = Convert.ToString(tempRefParam2);
                        mbConfirmacionTomas.GstFechaApunte = Convert.ToString(tempRefParam);
                        object tempRefParam3 = stFechaToma + " 00:00:00";
                        sql.Append(" and (ETOMASFA.fprevist >= " + Serrores.FormatFechaHMS(tempRefParam3) + " ");
                        object tempRefParam4 = stFechaToma + " 23:59:59";
                        sql.Append("and ETOMASFA.fprevist <= " + Serrores.FormatFechaHMS(tempRefParam4) + ") ");
                        sql.Append(" order by ETOMASFA.ftomasfa DESC ");
                        
                        SqlDataAdapter tempAdapter = new SqlDataAdapter(sql.ToString(), mbConfirmacionTomas.RcEnfermeria);
                        RrTomas = new DataSet();
                        tempAdapter.Fill(RrTomas);

                        if (RrTomas.Tables[0].Rows.Count != 0)
                        {                            
                            foreach (DataRow iteration_row in RrTomas.Tables[0].Rows)
                            {                                
                                string tempRefParam5 = (Convert.IsDBNull(iteration_row["vperfcon"])) ? "" : Convert.ToString(iteration_row["vperfcon"]);
                                Cargar_grid(Convert.ToString(iteration_row["fapuntes"]), Convert.ToString(iteration_row["fprevist"]), Convert.ToString(iteration_row["ftomasfa"]), (Convert.IsDBNull(iteration_row["oobserva"])) ? "" : Convert.ToString(iteration_row["oobserva"]), Convert.ToString(iteration_row["gusuario"]), ref tempRefParam5);
                            }
                        }
                        else
                        {
                            //no se recuperan registros
                        }
                    } //estamos en la fecha del bucle
                }
            }
            catch (SqlException ex)
            {
                Serrores.AnalizaError("perfusiones.dll", Path.GetDirectoryName(Application.ExecutablePath), mbConfirmacionTomas.gUsuario, "Llenar_gridTomas", ex);
            }
        }

        private void DatosDosisFarmaco(string stCodigoFarmaco)
        {
            //saca de dcodfarm el campo cporenva ,el indicador unidosis y el indicador de receta y la equivalencia
            string tstSql = "Select cporenva,iunidosis,nequival,iumedfra from dcodfarmvt " + " where gfarmaco='" + stCodigoFarmaco + "' and fborrado is null ";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(tstSql, mbConfirmacionTomas.RcEnfermeria);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);
            if (RrSql.Tables[0].Rows.Count != 0)
            {                
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iumedfra"]))
                {                    
                    iMedidaFraccionable = Convert.ToString(RrSql.Tables[0].Rows[0]["iumedfra"]).ToUpper();
                }                
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["iunidosis"]))
                {                    
                    iEnvaseFraccionable = Convert.ToString(RrSql.Tables[0].Rows[0]["iunidosis"]).ToUpper();
                }
                
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["nequival"]))
                {                    
                    string tempRefParam = Convert.ToString(RrSql.Tables[0].Rows[0]["nequival"]);
                    stUnidadEquivalencia = Serrores.ConvertirDecimales(ref tempRefParam, ref mbConfirmacionTomas.vstSeparadorDecimal, 2);
                }
                else
                {
                    string tempRefParam2 = "1";
                    stUnidadEquivalencia = Serrores.ConvertirDecimales(ref tempRefParam2, ref mbConfirmacionTomas.vstSeparadorDecimal, 2);
                }
                
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["cporenva"]))
                {                    
                    SngCantidadPorEnvase = Convert.ToSingle(RrSql.Tables[0].Rows[0]["cporenva"]);
                }
            }            
        }

        private string DosisAImputar(string stCodFarmaco, string stDosisAdm, string stNequival)
        {
            //funcion para buscar el indicador de quien trae el farmaco
            string result = String.Empty;
            result = "0";

            string sql = "select sum(cdosisto) as TotalAdministrado, sum(cdosimpu) as TotalImputado " + "from ETOMASFA where itiposer = '" + mbConfirmacionTomas.GstTiposervicio + "' " + "and ganoregi = " + mbConfirmacionTomas.GstA�oFarmaco + " and gnumregi = " + mbConfirmacionTomas.GstNumeroFarmaco + " " + "and gfarmaco = '" + stCodFarmaco + "' ";

            SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, mbConfirmacionTomas.RcEnfermeria);
            DataSet RrSql = new DataSet();
            tempAdapter.Fill(RrSql);
            if (RrSql.Tables[0].Rows.Count != 0)
            {                
                if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["TotalAdministrado"]) && !Convert.IsDBNull(RrSql.Tables[0].Rows[0]["TotalImputado"]))
                {
                    //si la suma de lo que tenemos administrado es mayor que lo imputado
                    //imputamos una unidad de equivalencia completa                    
                    if (Convert.ToDouble(RrSql.Tables[0].Rows[0]["TotalAdministrado"]) + Double.Parse(stDosisAdm) > Convert.ToDouble(RrSql.Tables[0].Rows[0]["TotalImputado"]))
                    {
                        result = stNequival;
                    }
                }
                else
                {
                    //tiene que tener algun valor                    
                    if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["TotalAdministrado"]) && Convert.IsDBNull(RrSql.Tables[0].Rows[0]["TotalImputado"]))
                    {
                        result = stNequival;
                    }
                }

            }
            
            return result;
        }

        private void tbVelocidad_Enter(Object eventSender, EventArgs eventArgs)
        {
            tbVelocidad.SelectionStart = 0;
            tbVelocidad.SelectionLength = Strings.Len(tbVelocidad.Text);
        }

        //********************************************************************************************************************
        //*
        //*  O.Frias (15/11/2007) Incorporo un decimal.
        //*
        //********************************************************************************************************************
        private void tbVelocidad_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {
            int KeyAscii = Strings.Asc(eventArgs.KeyChar);
            int viPosSep = 0, viPosCursor = 0;
            if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8 && KeyAscii != 9 && KeyAscii != Strings.Asc(mbConfirmacionTomas.vstSeparadorDecimal[0]))
            { //si no son numeros,ni tabulador, ni retroceso,ni punto, ni coma no le dejo
                KeyAscii = 0;
            }
            else
            {
                viPosSep = (tbVelocidad.Text.IndexOf(mbConfirmacionTomas.vstSeparadorDecimal) + 1);
                viPosCursor = tbVelocidad.SelectionStart;
                //Si tiene separador decimal,dos decimales, las teclas no son ni retroceso,ni separador y la posici�n es posterior al separador entonces no hace nada
                if (viPosSep != 0 && tbVelocidad.Text.Substring(viPosSep).Length == 1 && KeyAscii != 8 && KeyAscii != 9 && viPosSep <= viPosCursor)
                {
                    KeyAscii = 0;
                }

                //Si tiene separador decimal y la tecla es el separador entonces no hace nada
                if (viPosSep != 0 && KeyAscii == Strings.Asc(mbConfirmacionTomas.vstSeparadorDecimal[0]))
                {
                    KeyAscii = 0;
                }

                //Si tiene separador decimal,tres enteros, las teclas no son ni retroceso,ni separador y la posici�n es anterior al separador entonces no hace nada
                if (viPosSep != 0)
                {
                    if (KeyAscii != 8 && KeyAscii != 9 && viPosSep > viPosCursor)
                    {
                        KeyAscii = 0;
                    }
                }

                //Si no tiene separador decimal,tres enteros y las teclas no son ni retroceso,ni separador
                if (viPosSep == 0 && Strings.Len(tbVelocidad.Text) == 9 && KeyAscii != 8 && KeyAscii != 9 && KeyAscii != Strings.Asc(mbConfirmacionTomas.vstSeparadorDecimal[0]) && tbVelocidad.SelectionLength == 0)
                {
                    KeyAscii = 0;
                }
            }

            if (KeyAscii == 0)
            {
                eventArgs.Handled = true;
            }
            eventArgs.KeyChar = Convert.ToChar(KeyAscii);
        }
        private void ConfirmarPerfusiones_Closed(Object eventSender, EventArgs eventArgs)
        {
        }
    }
}