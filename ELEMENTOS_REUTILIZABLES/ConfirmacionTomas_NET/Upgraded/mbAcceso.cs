using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ConfirmacionTomas
{
	internal static class mbAcceso
	{


		//'Global Const KEY_ALL_ACCESS = &H3F
		//'Global Const REG_OPTION_NON_VOLATILE = 0
		//'Global Const HKEY_CURRENT_USER = &H80000001
		//'Global Const HKEY_LOCAL_MACHINE = &H80000002
		//'Global Const REG_SZ As Long = 1
		//'Global Const ERROR_SUCCESS = 0&

		// David 22/04/2004

		public static bool vfLiteralProblemas = false; // Indicar� cuando hemos de usar el literal de
		// los problemas de enfermer�a indicado por la
		// constante global LITPROBE
		public static string vstLitProbLargo = String.Empty; // Literal de Problemas de Enfermer�a largo
		public static string vstLitProbCorto = String.Empty; // Literal de Problemas de Enfermer�a corto

		public static bool vfLiteralCuidados = false;
		public static string vstLiteralCuidados = String.Empty;

		// Fin David 22/04/2004

		// Datos sobre Laboratorio

		public static string vstServLab = String.Empty;
		public static string vstServLabIP = String.Empty;
		public static string vstTipoLlamadaLab = String.Empty;
		public static string vstUsuarioSiglo = String.Empty;
		public static string vstClaveSiglo = String.Empty;

		public static string vstTipoSiglo = String.Empty;
		public static string vstEstructuraSiglo = String.Empty;

		public static string vstPrefijo = String.Empty;
		public static string vstArgumento = String.Empty;

		// Datos sobre el Visor de Laboratorio

		public static string vstServVisor = String.Empty;
		public static string vstUsuarioVisor = String.Empty;
		public static string vstPasswordVisor = String.Empty;
		public static string vstTipoVisor = String.Empty;

		// Datos sobre Radiodiagn�stico

		public static string vstServRad = String.Empty;
		public static string vstServRadIP = String.Empty;
		public static string vstTipoLlamadaRad = String.Empty;

		// Datos sobre Anatom�a Patol�gica

		public static string vstServAnaIP = String.Empty;
		public static string vstCadenaAnatomia = String.Empty;

		// Determinar qu� servicio existe

		public static bool vfLaboratorioActivado = false;
		public static bool vfRadiodiagnosticoActivado = false;
		public static bool vfAnatomiaActivado = false;
		public static bool vfVisorLaboratorioActivado = false;


		// Funci�n para el link

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("shell32.dll", EntryPoint = "ShellExecuteA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int ShellExecute(int hwnd, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpOperation, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpFile, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpParameters, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpDirectory, int nShowCmd);

		//'Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
		//'
		//'Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
		//'
		//'Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpdata As Any, lpcbData As Long) As Long       ' Note that if you declare the lpData parameter as String, you must pass it By Value.

		// WINSOCK
		// Variables y funciones necesarias para tomar la IP de nuestro equipo y almacenarla en el
		// fichero de sesi�n del servicio de laboratorio SIGLO.

		public const int SOCKET_ERROR = -1;

		public struct HOSTENT
		{
			public int hName;
			public int hAliases;
			public short hAddrType;
			public short hLength;
			public int hAddrList;
		}

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("ws2_32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int gethostbyname([MarshalAs(UnmanagedType.VBByRefStr)] ref string host_name);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("ws2_32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int gethostname([MarshalAs(UnmanagedType.VBByRefStr)] ref string host_name, int namelen);

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("kernel32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static void RtlMoveMemory(System.IntPtr hpvDest, int hpvSource, int cbCopy);

		//UPGRADE_ISSUE: (2068) rdoEnvironment object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		static UpgradeStubs.RDO_rdoEnvironment ReAcceso = null;
		static SqlConnection RcAcceso = null;
		static DataSet RsAcceso = null;

		public static string vstMetaUsuario = String.Empty;
		public static string vstMetaPassword = String.Empty;
		static bool fErrorControlAcceso = false;

		public static bool fMetaUsuario = false;
		public static bool fErrorAcceso = false;
		public static string gstUsuario = String.Empty; //no  v�lido
		public static string gstPassword = String.Empty; //no v�lido
		public static string stLetraSubsistema = String.Empty;

		//Estructura de Controles Ocultos
		public struct strControlOculto
		{
			public string stNombrecontrol;
			public string stIndiceControl;
			public static strControlOculto CreateInstance()
			{
					strControlOculto result = new strControlOculto();
					result.stNombrecontrol = String.Empty;
					result.stIndiceControl = String.Empty;
					return result;
			}
		}

		public static mbAcceso.strControlOculto[] aControlesOcultos = null;

		//Estructura de Eventos no accesibles
		public struct strEventoNulo
		{
			public string stNombrecontrol;
			public string stIndiceControl;
			public string stNombreEvento;
			public static strEventoNulo CreateInstance()
			{
					strEventoNulo result = new strEventoNulo();
					result.stNombrecontrol = String.Empty;
					result.stIndiceControl = String.Empty;
					result.stNombreEvento = String.Empty;
					return result;
			}
		}

		public static mbAcceso.strEventoNulo[] aEventosNulos = null;

		public static Mensajes.ClassMensajes MensajeAcceso = null; //en ejecuci�n se le asignar� CreateObject("Mensajes.ClassMensajes")

		//Identifica aquellos controles que no pertenecen a un array de controles
		//es decir, aquellos que no tengan �ndice
		public const string CoStIndiceArrayNulo = "#";

		//oscar
		public static string gstDriver = String.Empty;
		public static string gstBasedatosActual = String.Empty;
		public static string gstServerActual = String.Empty;
		public static string gstUsuarioBD = String.Empty;
		public static string gstPasswordBD = String.Empty;

		//Public vstSeparadorDecimal  As String
		//******************** O.Frias (14/12/2006) ********************
		//* Variable para el retorno del gidenpac desde control de duplicados
		public static string strGIdenPac = String.Empty;
		//******************** O.Frias (14/12/2006) ********************

		//Oscar C Febrero 2010
		public static string stVisorWEB_literal = String.Empty;
		public static string stVisorWEB_URL = String.Empty;
		public static bool bVisorWEB = false;
		//----------
		//' O.Frias - 19/03/2014
		//' Nueva variable para saber si es un visor WEb o Aplicacion
		public static bool blnVisorAplicacion = false;


		internal static void TroceoDSN(ref string manejador)
		{
			//Nota:
			//El manejador debera de ser del tipo:
			//SRVSAN2000#{SQL SERVER}#IDC_TOLEDO#GHOSP#INDRA

			gstServerActual = manejador.Substring(0, Math.Min(manejador.IndexOf('#'), manejador.Length));
			int ti = (manejador.IndexOf('#') + 1);
			gstDriver = manejador.Substring(ti, Math.Min(Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary) - ti - 1, manejador.Length - ti));
			ti = Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary);
			gstBasedatosActual = manejador.Substring(ti, Math.Min(Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary) - ti - 1, manejador.Length - ti));
			ti = Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary);
			gstUsuarioBD = manejador.Substring(ti, Math.Min(Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary) - ti - 1, manejador.Length - ti));
			ti = Strings.InStr(ti + 1, manejador, "#", CompareMethod.Binary);
			gstPasswordBD = manejador.Substring(ti).Trim();
		}

		//----------

		internal static string ValidaPrincipal()
		{
			//Nota (oscar):
			//Ahora los parametros pasados via command deberan ser del formato:
			//curia,Principal@*SRVSAN2000#{SQL SERVER}#IDC_TOLEDO#GHOSP#INDRA

			bool EstaActivo = false;
			string RecogeCommand = Interaction.Command();
			int StComa = (RecogeCommand.IndexOf(',') + 1);

			//oscar
			int staster = (RecogeCommand.IndexOf('*') + 1);
			string manejador = RecogeCommand.Substring(StComa, Math.Min(staster - StComa - 1, RecogeCommand.Length - StComa));
			//-----

			if (manejador != "Principal@")
			{
				EstaActivo = false;
			}
			else
			{
				EstaActivo = true;

				//oscar
				staster = (RecogeCommand.IndexOf('*') + 1);
				string tempRefParam = RecogeCommand.Substring(staster);
				TroceoDSN(ref tempRefParam);
				//-----

			}
			if (!EstaActivo)
			{
				return "No Activa";
			}
			else
			{
				return RecogeCommand.Substring(0, Math.Min(StComa - 1, RecogeCommand.Length));
			}
		}

		//Public Sub EstablecerConexionAcceso()
		internal static object EstablecerConexionAcceso(ref string PstServer, ref string PstDriver, ref string PstBasedatos, ref string PstUsuarioBD, ref string PstPasswordBD)
		{


			//UPGRADE_ISSUE: (2068) rdoEnvironment object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			//UPGRADE_ISSUE: (2064) RDO.rdoEngine property rdoEngine.rdoEnvironments was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//UPGRADE_ISSUE: (2064) RDO.rdoEnvironments property rdoEngine.rdoEnvironments.Item was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			ReAcceso = (UpgradeStubs.RDO_rdoEnvironment) UpgradeSupport.RDO_rdoEngine_definst.getrdoEnvironments().Item(0);

			//vstServer = fnQueryRegistro("SERVIDOR")
			//vstDriver = fnQueryRegistro("DRIVER")
			//vstBasedat = fnQueryRegistro("BASEDATOS")
			//vstUsuario = fnQueryRegistro("INICIO")
			//vstPassword = "Indra"
			//stLetraAcceso = "P"

			Conexion.Obtener_conexion Instancia = new Conexion.Obtener_conexion();

			//Instancia.Conexion RcAcceso, stLetraSubsistema, vstUsuario_NT, fErrorAcceso, gstUsuario, gstPassword
			string tempRefParam = String.Empty;
			string tempRefParam2 = String.Empty;
			string tempRefParam3 = String.Empty;
			string tempRefParam4 = String.Empty;
			Instancia.Conexion(ref RcAcceso, ref stLetraSubsistema, ref mbConfirmacionTomas.VstUsuario_NT, ref fErrorAcceso, ref gstUsuario, ref gstPassword, ref PstServer, ref PstDriver, ref PstBasedatos, ref PstUsuarioBD, ref PstPasswordBD, ref tempRefParam, ref tempRefParam2, ref tempRefParam3, ref tempRefParam4);

			if (!fErrorAcceso)
			{
				MessageBox.Show("ERROR DE CONEXION EN EL CONTROL DE ACCESO", Application.ProductName);
				fErrorControlAcceso = true;
				return null;
			}

			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(RcAcceso);

			Serrores.vAyuda = Serrores.ObtenerFicheroAyuda();

			return null;
		}
		//End Sub


		internal static bool PermitirEvento(ref SqlConnection rcConexion, mbAcceso.strEventoNulo[] astrEventosNulos, string stControl, ref string StIndice, string stEvento)
		{

			bool result = false;
			MensajeAcceso = new Mensajes.ClassMensajes();
			//Se preguntar� a la estructura si ese Evento est�,
			//y se devolver� Verdadero o Falso
			StIndice = StIndice.Trim();

			//Si el control no es de un array  de controles
			//esta funci�n recibe como indice="", que se debe convertir a
			//CoStIndiceArrayNulo para no cambiar los programas que llaman a esta funci�n
			if (StIndice == "")
			{
				StIndice = CoStIndiceArrayNulo;
			}
			//Debemos comprobar primero si el indice viene vacio para montar las preguntas
			if (StIndice == CoStIndiceArrayNulo)
			{ //""
				for (int iIndiceEvento = 0; iIndiceEvento <= astrEventosNulos.GetUpperBound(0); iIndiceEvento++)
				{

					if (astrEventosNulos[iIndiceEvento].stNombrecontrol == stControl && astrEventosNulos[iIndiceEvento].stNombreEvento == stEvento)
					{

						result = false;
						short tempRefParam = 1620;
						object tempRefParam2 = new object{};
						MensajeAcceso.RespuestaMensaje(ref Serrores.vNomAplicacion, ref Serrores.vAyuda, ref tempRefParam, ref rcConexion, ref tempRefParam2);
						return result;
					}
					else
					{
						result = true;
					}
				}

			}
			else
			{

				for (int iIndiceEvento = 0; iIndiceEvento <= astrEventosNulos.GetUpperBound(0); iIndiceEvento++)
				{

					if (astrEventosNulos[iIndiceEvento].stNombrecontrol == stControl && astrEventosNulos[iIndiceEvento].stNombreEvento == stEvento && astrEventosNulos[iIndiceEvento].stIndiceControl == StIndice)
					{

						result = false;
						short tempRefParam3 = 1620;
						object tempRefParam4 = new object{};
						MensajeAcceso.RespuestaMensaje(ref Serrores.vNomAplicacion, ref Serrores.vAyuda, ref tempRefParam3, ref rcConexion, ref tempRefParam4);
						return result;
					}
					else
					{
						result = true;
					}

				}

			}

			return result;
		}
		internal static object OcultarControles(Form Formulario, mbAcceso.strControlOculto[] aControlesOcultos)
		{
			bool Errormenu = false;
			//Recorremos la estructura de controles y los ponemos a NO Visible
			//los que nos d�.
			string NombreControl = String.Empty;
			string NumeroControl = String.Empty;

			try
			{
				if (aControlesOcultos.GetUpperBound(0) == 0)
				{
					//No tenemos controles que ocultar
					return null;

				}
				else
				{
					//Manejador de Errores para controlar la imposibilidar de que un men� Hijo
					//d� un error al ocultarlo, siendo el �ltimo Hijo Visible.08/01/1998
					Errormenu = true;

					for (int iIndiceControles = 0; iIndiceControles <= aControlesOcultos.GetUpperBound(0) - 1; iIndiceControles++)
					{
						NombreControl = aControlesOcultos[iIndiceControles].stNombrecontrol;
						NumeroControl = aControlesOcultos[iIndiceControles].stIndiceControl;
						//UPGRADE_WARNING: (2065) Form property Formulario.Controls has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2065.aspx
						foreach (Control MiControl in ContainerHelper.Controls(Formulario))
						{
							if (MiControl.Name == NombreControl)
							{
								if (NumeroControl != "")
								{
									if (MiControl is ToolStrip)
									{
										int tempForVar = ((ToolStrip) MiControl).Items.Count;
										for (int X = 1; X <= tempForVar; X++)
										{
											if (X == StringsHelper.ToDoubleSafe(NumeroControl))
											{
												((ToolStrip) MiControl).Items[X - 1].Visible = false;
											}
										}
									}
									else if (MiControl is TabControl)
									{ 
										for (int X = 0; X <= SSTabHelper.GetTabCount((TabControl) MiControl) - 1; X++)
										{
											if (X == StringsHelper.ToDoubleSafe(NumeroControl))
											{
												//MiControl.Tabs(x).Visible = False
												SSTabHelper.SetTabVisible((TabControl) MiControl, X, false);
											}
										}
									}
									else
									{
										if (ControlArrayHelper.GetControlIndex(MiControl) == StringsHelper.ToDoubleSafe(NumeroControl))
										{
											ControlHelper.SetVisible(MiControl, false);
										}
									}
								}
								else
								{
									ControlHelper.SetVisible(MiControl, false);
								}
							}
						}
					}
				}
			}
			catch (Exception excep)
			{
				if (!Errormenu)
				{
					throw excep;
				}

				if (Errormenu)
				{


					int lCodigoError = 0;

					//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
					lCodigoError = Information.Err().Number;


					switch(lCodigoError)
					{
						case 387 : 
							//UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: http://www.vbtonet.com/ewis/ewi1065.aspx 
							UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement"); 
							 
							break;
						default:
							//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx 
							MessageBox.Show(Information.Err().Number.ToString() + " " + excep.Message, Application.ProductName); 
							 
							break;
					}

				}
			}

			return null;
		}

		internal static object CargarEstrucControlesOcultos(string stUsuario, string stFormulario, ref mbAcceso.strControlOculto[] astrControlesOcultos)
		{
			//Llamada a SQL para cargar la estructura de controles ocultos, llamar
			//a la rutina OcultarControles para dejarlos invisibles
			//y dejarla disponible para la aplicaci�n.

			string stSQLAcceso = "SELECT DISTINCT gcontrol,nindice INTO #SCONNVUFV1gusugfrm ";
			stSQLAcceso = stSQLAcceso + "FROM #SACONAUFV1gusugfrm WHERE ";
			stSQLAcceso = stSQLAcceso + "iaccpublicas='N' AND ";
			stSQLAcceso = stSQLAcceso + "gformulario='" + stFormulario + "'";
			stSQLAcceso = stSQLAcceso + " AND (gformulario+gcontrol+nindice) NOT IN ";
			stSQLAcceso = stSQLAcceso + "(SELECT(gformulario+gcontrol+nindice) FROM ";
			stSQLAcceso = stSQLAcceso + "SACOSAUFV1 WHERE iaccpublicas = 'N'";
			stSQLAcceso = stSQLAcceso + " AND gusuario='" + stUsuario + "'";
			stSQLAcceso = stSQLAcceso + " AND gformulario='" + stFormulario + "')";

			SqlCommand tempCommand = new SqlCommand(stSQLAcceso, RcAcceso);
			tempCommand.ExecuteNonQuery();

			stSQLAcceso = "";
			stSQLAcceso = "SELECT gcontrol, nindice FROM #SCONNVUFV1gusugfrm ";
			stSQLAcceso = stSQLAcceso + "order by gcontrol,nindice";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQLAcceso, RcAcceso);
			RsAcceso = new DataSet();
			tempAdapter.Fill(RsAcceso);

			if (RsAcceso.Tables[0].Rows.Count != 0)
			{

				//UPGRADE_ISSUE: (1040) MoveLast function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
				RsAcceso.MoveLast(null);
				//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
				RsAcceso.MoveFirst();

				astrControlesOcultos = new mbAcceso.strControlOculto[RsAcceso.Tables[0].Rows.Count + 1];

				for (int iContador = 0; iContador <= RsAcceso.Tables[0].Rows.Count - 1; iContador++)
				{

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					astrControlesOcultos[iContador].stNombrecontrol = Convert.ToString(RsAcceso.Tables[0].Rows[0]["gcontrol"]).Trim();

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToString(RsAcceso.Tables[0].Rows[0]["nindice"]).Trim() == CoStIndiceArrayNulo)
					{ //" "
						//Seguimos dejando el criterio antiguo de que los controles
						//que no pertenezcan a un array de controles tengan �ndice =""
						//para que aquellos programas que llaman a esta funci�n sigan
						//recibiendo el mismo criterio
						astrControlesOcultos[iContador].stIndiceControl = "";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						astrControlesOcultos[iContador].stIndiceControl = Convert.ToString(RsAcceso.Tables[0].Rows[0]["nindice"]).Trim();
					}

					//UPGRADE_ISSUE: (1040) MoveNext function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
					RsAcceso.MoveNext();

				}
			}
			else
			{
				//ReDim astrControlesOcultos(RsAcceso.RowCount)
				astrControlesOcultos = new mbAcceso.strControlOculto[1];
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RsAcceso.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RsAcceso.Close();

			return null;
		}

		internal static object CargarEstrucEventosNulos(string stUsuario, string stFormulario, ref mbAcceso.strEventoNulo[] astrEventosNulos, SqlConnection rcControl = null)
		{
			//Llamada a SQL para cargar la estructura de eventos nulos y dejarla
			//disponible para la aplicaci�n.
			string stSQLAcceso = String.Empty;

			//Llamada a Conexion.DLL para recibir una conexi�n propia para el Control
			//de Acceso, para poder borrar las tablas temporales, y que �stas
			//desaparezcan.

			//oscar
			//Call EstablecerConexionAcceso
			//hay que controlar que las dll's que utilizan control de acceso tengan tpdos los datos
			//rellenos

			if (gstServerActual == "")
			{
				try
				{ //si llega aqui no funciona el control de acceso,revisar esta parte
					//UPGRADE_ISSUE: (2064) RDO.rdoConnection property rcControl.Name was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					if (rcControl.getName() != "")
					{
						Serrores.Datos_Conexion(rcControl);
						gstDriver = Serrores.VVstDriver;
						gstBasedatosActual = Serrores.VVstBasedatosActual;
						gstServerActual = Serrores.VVstServerActual;
						gstUsuarioBD = Serrores.VVstUsuarioBD;
						gstPasswordBD = Serrores.VVstPasswordBD;
					}
				}
				catch
				{
				}
				//si llega aqui no funciona el control de acceso,revisar esta parte
				gstDriver = Serrores.VVstDriver;
				gstBasedatosActual = Serrores.VVstBasedatosActual;
				gstServerActual = Serrores.VVstServerActual;
				gstUsuarioBD = Serrores.VVstUsuarioBD;
				gstPasswordBD = Serrores.VVstPasswordBD;
			}

			EstablecerConexionAcceso(ref gstServerActual, ref gstDriver, ref gstBasedatosActual, ref gstUsuarioBD, ref gstPasswordBD);
			//-----
			if (fErrorControlAcceso)
			{
				return null;
			}

			stSQLAcceso = "SELECT * INTO #SACONAUFV1gusugfrm ";
			stSQLAcceso = stSQLAcceso + "FROM SACCCONTV1 WHERE gformulario='" + stFormulario + "'";
			stSQLAcceso = stSQLAcceso + " AND  NOT exists (SELECT ";
			stSQLAcceso = stSQLAcceso + "' ' FROM SACOSAUFV1 WHERE ";
			stSQLAcceso = stSQLAcceso + "gformulario='" + stFormulario + "'";
			stSQLAcceso = stSQLAcceso + " AND gusuario='" + stUsuario + "' and (gformulario+gaccion)=(SACCCONTV1.gformulario+SACCCONTV1.gaccion) )";



			SqlCommand tempCommand = new SqlCommand(stSQLAcceso, RcAcceso);
			tempCommand.ExecuteNonQuery();

			stSQLAcceso = "";

			stSQLAcceso = "select gcontrol,nindice,gevento FROM ";
			stSQLAcceso = stSQLAcceso + "#SACONAUFV1gusugfrm ";
			stSQLAcceso = stSQLAcceso + "order by gcontrol,nindice";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stSQLAcceso, RcAcceso);
			RsAcceso = new DataSet();
			tempAdapter.Fill(RsAcceso);

			if (RsAcceso.Tables[0].Rows.Count != 0)
			{

				//UPGRADE_ISSUE: (1040) MoveLast function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
				RsAcceso.MoveLast(null);
				//UPGRADE_ISSUE: (1040) MoveFirst function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
				RsAcceso.MoveFirst();

				astrEventosNulos = new mbAcceso.strEventoNulo[RsAcceso.Tables[0].Rows.Count + 1];

				for (int iContador = 0; iContador <= RsAcceso.Tables[0].Rows.Count - 1; iContador++)
				{

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					astrEventosNulos[iContador].stNombrecontrol = Convert.ToString(RsAcceso.Tables[0].Rows[0]["gcontrol"]).Trim();

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToString(RsAcceso.Tables[0].Rows[0]["nindice"]).Trim() == CoStIndiceArrayNulo)
					{ //" "
						//Seguimos dejando el criterio antiguo de que los controles
						//que no pertenezcan a un array de controles tengan �ndice =""
						//para que aquellos programas que llaman a esta funci�n sigan
						//recibiendo el mismo criterio
						astrEventosNulos[iContador].stIndiceControl = "";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						astrEventosNulos[iContador].stIndiceControl = Convert.ToString(RsAcceso.Tables[0].Rows[0]["nindice"]).Trim();
					}

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					astrEventosNulos[iContador].stNombreEvento = Convert.ToString(RsAcceso.Tables[0].Rows[0]["gevento"]).Trim();
					//UPGRADE_ISSUE: (1040) MoveNext function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
					RsAcceso.MoveNext();
				}
			}
			else
			{
				//ReDim astrEventosNulos(RsAcceso.RowCount)
				astrEventosNulos = new mbAcceso.strEventoNulo[1];
			}

			return null;
		}

		internal static bool ControlOculto(mbAcceso.strControlOculto[] astrControlesOcultos, string stControl, ref string StIndice)
		{


			//Se preguntar� a la estructura si ese Control est� ya oculto
			//y se devolver� Verdadero o Falso
			bool result = false;

			//Si el control no es de un array  de controles
			//esta funci�n recibe como indice="", que se debe convertir a
			//CoStIndiceArrayNulo para no cambiar los programas que llaman a esta funci�n
			if (StIndice == "")
			{
				StIndice = CoStIndiceArrayNulo;
			}

			//Debemos comprobar primero si el indice viene vacio para montar las preguntas
			if (StIndice == CoStIndiceArrayNulo)
			{ //""

				for (int iIndiceControl = 0; iIndiceControl <= astrControlesOcultos.GetUpperBound(0); iIndiceControl++)
				{

					if (astrControlesOcultos[iIndiceControl].stNombrecontrol == stControl)
					{

						return true;
					}
					else
					{
						result = false;
					}
				}

			}
			else
			{

				for (int iIndiceControl = 0; iIndiceControl <= astrControlesOcultos.GetUpperBound(0); iIndiceControl++)
				{

					if (astrControlesOcultos[iIndiceControl].stNombrecontrol == stControl && astrControlesOcultos[iIndiceControl].stIndiceControl == StIndice)
					{


						return true;
					}
					else
					{
						result = false;
					}

				}

			}

			return result;
		}

		internal static object BorrarTablasTemp()
		{
			//Borraremos las tablas temporales

			//StSql = "DROP TABLE #SACONAUFV1gusugfrm"
			//RcAcceso.Execute StSql


			//StSql = "DROP TABLE #SCONNVUFV1gusugfrm"
			//RcAcceso.Execute StSql

			//Y ya cerramos nuestra sesi�n
			//UPGRADE_ISSUE: (2064) RDO.rdoConnection method RcAcceso.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			UpgradeStubs.System_Data_SqlClient_SqlConnection.Close();
			ReAcceso.Close();

			return null;
		}

		internal static bool ComprobarMetaUsuario(string stUsuario, string stPassword = "")
		{

			if (vstMetaPassword == "")
			{
				return stUsuario == vstMetaUsuario;
			}
			else
			{
				return stUsuario == vstMetaUsuario && stPassword == vstMetaPassword;
			}

		}
		internal static string ObtenerCodigoSConsglo(string Codigo, string CampoObtener, SqlConnection nConnet)
		{
			string result = String.Empty;
			string sql = "select " + CampoObtener + " from sconsglo where gconsglo='" + Codigo + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, nConnet);
			DataSet RrConstantes = new DataSet();
			tempAdapter.Fill(RrConstantes);
			if (RrConstantes.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				result = (Convert.IsDBNull(Convert.ToString(RrConstantes.Tables[0].Rows[0][0]).Trim())) ? "" : Convert.ToString(RrConstantes.Tables[0].Rows[0][0]).Trim();
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrConstantes.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrConstantes.Close();
			return result;
		}

		internal static bool AplicarControlAcceso(string stUsuario, string stLetraAcceso, Form Formulario, string stFrm, ref mbAcceso.strControlOculto[] astrControlesOcultos, ref mbAcceso.strEventoNulo[] astrEventosNulos, SqlConnection rcControl = null)
		{
			vstMetaUsuario = "metausua";
			stLetraSubsistema = stLetraAcceso;
			fErrorControlAcceso = false;

			//Preguntar si es el Metausuario para dejarle acceso total
			if (stUsuario != vstMetaUsuario)
			{

				//Primer cargaremos la estructura de los Eventos Nulos
				//preguntamos por el name de la conexion, si me devuelve el error 91 no me ha llegado
				try
				{
					//    If rcControl.Name <> "" Then
					//        CargarEstrucEventosNulos stUsuario, stFrm, astrEventosNulos(), rcControl
					//    End If

					if (false)
					{
						CargarEstrucEventosNulos(stUsuario, stFrm, ref astrEventosNulos);
					}
					else
					{
						CargarEstrucEventosNulos(stUsuario, stFrm, ref astrEventosNulos, rcControl);
					}
				}
				catch
				{
				}




				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 91)
				{
					CargarEstrucEventosNulos(stUsuario, stFrm, ref astrEventosNulos);
				}

				if (fErrorControlAcceso)
				{
					return !fErrorControlAcceso;
				}
				//A continuaci�n cargamos la estructura de los Controles que van a estar ocultos.
				CargarEstrucControlesOcultos(stUsuario, stFrm, ref astrControlesOcultos);
				if (fErrorControlAcceso)
				{
					return !fErrorControlAcceso;
				}
				//Ahora es el momento de borrar las dos tablas temporales
				BorrarTablasTemp();
				if (fErrorControlAcceso)
				{
					return !fErrorControlAcceso;
				}
				//Ahora hacemos desaparecer los controles necesarios
				OcultarControles(Formulario, astrControlesOcultos);

			}
			else
			{

				astrEventosNulos = new mbAcceso.strEventoNulo[1];
				astrControlesOcultos = new mbAcceso.strControlOculto[1];
				fErrorControlAcceso = false;

			}

			return !fErrorControlAcceso;

		}


		internal static bool EsDeLaboratorio(string Codigo, SqlConnection RcLabo, bool servicio = false)
		{
			bool result = false;
			string tstQuery = String.Empty;

			if (servicio)
			{
				tstQuery = "select iservlab from DSERVICI where gservici = '" + Codigo + "' ";
			}
			else
			{
				tstQuery = "select iservlab from DSERVICI ";
				tstQuery = tstQuery + "inner join DCODPRES on DSERVICI.gservici = DCODPRES.gservici and ";
				tstQuery = tstQuery + "DCODPRES.gprestac = '" + Codigo + "' ";
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, RcLabo);
			DataSet tRr2 = new DataSet();
			tempAdapter.Fill(tRr2);

			if (tRr2.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				result = (Convert.IsDBNull(tRr2.Tables[0].Rows[0]["iservlab"])) ? false : Convert.ToString(tRr2.Tables[0].Rows[0]["iservlab"]) == "S";
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRr2.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			tRr2.Close();
			return result;
		}

		internal static bool EsDeRayos(string Codigo, SqlConnection RcRayos)
		{

			// Comprobamos que sea de diagn�stico por imagen

			bool result = false;
			string tstQuery = "select iservrad from DSERVICI ";
			tstQuery = tstQuery + "inner join DCODPRES on DSERVICI.gservici = DCODPRES.gservici and ";
			tstQuery = tstQuery + "DCODPRES.gprestac = '" + Codigo + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, RcRayos);
			DataSet tRr2 = new DataSet();
			tempAdapter.Fill(tRr2);

			if (tRr2.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				result = (Convert.IsDBNull(tRr2.Tables[0].Rows[0]["iservrad"])) ? false : Convert.ToString(tRr2.Tables[0].Rows[0]["iservrad"]) == "S";
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRr2.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			tRr2.Close();
			return result;
		}

		internal static bool EsDeAnatomiaPatologica(string Codigo, SqlConnection RcAnatomia)
		{
			bool result = false;
			int SrvAnatomia = 0;

			// Tomamos el servicio de Anatom�a Patol�gica

			string tmpValue = ObtenerCodigoSConsglo("ISERANAT", "NNUMERI1", RcAnatomia);

			double dbNumericTemp = 0;
			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (Convert.IsDBNull(tmpValue) || !Double.TryParse(tmpValue, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
			{
				return result;
			}
			else
			{
				SrvAnatomia = Convert.ToInt32(Double.Parse(tmpValue));
			}

			// Comprobamos que sea de diagn�stico por imagen

			string tstQuery = "select * from DCODPRES where " + 
			                  "gprestac = '" + Codigo + "' and " + 
			                  "gservici = " + SrvAnatomia.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, RcAnatomia);
			DataSet tRr2 = new DataSet();
			tempAdapter.Fill(tRr2);

			result = (tRr2.Tables[0].Rows.Count != 0);

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRr2.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			tRr2.Close();
			return result;
		}

		internal static bool fImagenDigital(string stPrestacion, SqlConnection RcImagen)
		{
			bool result = false;
			string StSql = String.Empty;
			DataSet rdoTemp = null;



			string tmpValue = ObtenerCodigoSConsglo("ILINKRAD", "valfanu1", RcImagen);

			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (!(Convert.IsDBNull(tmpValue) || tmpValue == "N"))
			{
				StSql = "SELECT iimagdig FROM DCODPRES WHERE gprestac = '" + stPrestacion + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, RcImagen);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					result = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["iimagdig"])) ? false : ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["iimagdig"]) + "").Trim().ToUpper() == "S");
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rdoTemp.Close();
				rdoTemp = null;
			}

			return result;
		}


		//*************************************************************************************************************
		//* O.Frias (15/06/2007) - Se podr� realizar la llamada con un unico estudi o todos
		//*************************************************************************************************************
		internal static string DireccionOKDICOM(string stIdentificadorPaciente, string stIdEstudio = "", SqlConnection RcOpenlab = null)
		{
			bool ErrOKDICOM = false;
			string result = String.Empty;
			string StSql = String.Empty;
			DataSet rdoTemp = null;

			// Comprobamos que vstServRadIP no est� vac�o. De estarlo, mostraremos mensaje de error y saldremos

			if (vstServRadIP == "")
			{
				MessageBox.Show("Error: HOST o IP del servidor no definido", Application.ProductName);
				return "";
			}

			// David 27/06/2007
			// Si no ha llegado conexi�n, montamos la direcci�n como antes

			if (stIdEstudio.Trim() == "" || RcOpenlab == null)
			{
				return "http://" + 
				vstServRadIP + 
				"/okdcjpg/okdcjpg2.asp?idpaciente=" + 
				stIdentificadorPaciente.Trim() + 
				                             "&Usuario=okdicom";
			}

			// Buscaremos los episodios de un paciente
			//******************** O.Frias - 16/06/2007 ********************

			result = "http://" + 
			         vstServRadIP + 
			         "/okdcjpg/okdcjpg2.asp?";

			try
			{
				ErrOKDICOM = true;

				if (stIdEstudio.Trim() != "")
				{
					StSql = "SELECT * FROM SCONSGLO WHERE gconsglo = 'URLESTUD'";

					SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, RcOpenlab);
					rdoTemp = new DataSet();
					tempAdapter.Fill(rdoTemp);
					if (rdoTemp.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
						{
							result = result + "idestudio=" + stIdEstudio.Trim() + "&";
						}
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					rdoTemp.Close();
				}

				result = result + "idpaciente=" + 
				         stIdentificadorPaciente.Trim() + 
				         "&Usuario=okdicom";

				ErrOKDICOM = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!ErrOKDICOM)
				{
					throw excep;
				}

				if (ErrOKDICOM)
				{

					MessageBox.Show(excep.Message, "DireccionOKDICOM", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return "";
				}
			}
			return System.String.Empty;
		}
		internal static string DireccionOpenlab(string stQuery, SqlConnection RcOpenlab)
		{

			string result = String.Empty;
			DataSet RrOpenLab = null;
			try
			{

				// Comprobamos que el host no est� vac�o

				if (vstServLabIP == "")
				{
					MessageBox.Show("Error: HOST o IP del servidor no definido", Application.ProductName);
					return "";
				}

				// Creamos la direcci�n a falta del n�mero de muestra

				result = "http://" + 
				         vstServLabIP + 
				         "/resultadosbusqueda.php?NIDLAB=";

				// Tomamos el n�mero de muestra

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stQuery, RcOpenlab);
				RrOpenLab = new DataSet();
				tempAdapter.Fill(RrOpenLab);

				if (RrOpenLab.Tables[0].Rows.Count == 0)
				{
					// No hay n�mero de muestra. Error

					MessageBox.Show("Error: No existe n�mero de muestra", Application.ProductName);
					result = "";
				}
				else
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (Convert.IsDBNull(RrOpenLab.Tables[0].Rows[0]["nmuestra"]))
					{
						// No hay n�mero de muestra. Error

						MessageBox.Show("Error: No existe n�mero de muestra", Application.ProductName);
						result = "";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						result = result + Convert.ToString(RrOpenLab.Tables[0].Rows[0]["nmuestra"]).Trim();
					}
				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrOpenLab.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrOpenLab.Close();

				return result;
			}
			catch
			{

				Serrores.AnalizaError("HistoriaClinicaPaciente", Path.GetDirectoryName(Application.ExecutablePath), "", "DireccionOpenlab", RcOpenlab);
				return "";
			}
		}

		internal static string DireccionAgfa(string stGiden, string stAccesion, SqlConnection RcAgfa)
		{

			try
			{

				DataSet RrAgfa = null;
				string StSql = String.Empty;
				string stTemp = String.Empty;
				int lnumero = 0;
				string stAccesion2 = String.Empty;
				int iLongitudN = 0;

				if (stGiden.Trim() == "" || stAccesion.Trim() == "")
				{
					MessageBox.Show("Par�metros no v�lidos para la llamada WEB", Application.ProductName);
					return "";
				}

				StSql = "select valfanu1, valfanu2, valfanu3, valfanu1i1, valfanu2i1, " + 
				        "valfanu3i1, valfanu1i2, valfanu2i2, valfanu3i2,nnumeri1 " + 
				        "from SCONSGLO where gconsglo = 'IAGFAWEB'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, RcAgfa);
				RrAgfa = new DataSet();
				tempAdapter.Fill(RrAgfa);

				if (RrAgfa.Tables[0].Rows.Count != 0)
				{

					for (int I = 0; I <= 8; I++)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stTemp = stTemp + (Convert.ToString(RrAgfa.Tables[0].Rows[0][I]) + "").Trim();
					}
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (!Convert.IsDBNull(RrAgfa.Tables[0].Rows[0][9]))
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						iLongitudN = Convert.ToInt32(Convert.ToDouble(RrAgfa.Tables[0].Rows[0][9]) - 4 - Conversion.Val(stAccesion.Substring(4)).ToString().Trim().Length); //A la longitud definida por constante de AN le restamos 4 de la longitud del a�o y la longitud del n�mero que venga
					}
					else
					{
						iLongitudN = 10 - Conversion.Val(stAccesion.Substring(4)).ToString().Trim().Length; //Si no se informa por defecto 10
					}
					stAccesion2 = stAccesion.Trim().Substring(0, Math.Min(4, stAccesion.Trim().Length));
					for (int j = 1; j <= iLongitudN; j++)
					{
						stAccesion2 = stAccesion2 + "0";
					}
					stAccesion2 = stAccesion2 + Conversion.Val(stAccesion.Substring(4)).ToString().Trim();
					string tempRefParam2 = "##1##";
					string tempRefParam = Serrores.proReplace(ref stTemp, ref tempRefParam2, stGiden);
					string tempRefParam3 = "##2##";
					stTemp = Serrores.proReplace(ref tempRefParam, ref tempRefParam3, stAccesion2);

				}

				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrAgfa.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrAgfa.Close();


				return stTemp.Trim();
			}
			catch
			{

				Serrores.AnalizaError("HistoriaClinicaPaciente", Path.GetDirectoryName(Application.ExecutablePath), "", "DireccionAgfa", RcAgfa);
				return "";
			}
		}

		internal static string DireccionVitro(string stGiden, string gUsuario, SqlConnection RcVitro)
		{


			// Comprobamos que el host no est� vac�o

			string result = String.Empty;
			if (vstServAnaIP == "")
			{
				MessageBox.Show("Error: HOST o IP del servidor no definido", Application.ProductName);
				return "";
			}

			string StSql = "select ghistoria from HDOSSIER where gidenpac = '" + stGiden.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, RcVitro);
			DataSet RrHistoria = new DataSet();
			tempAdapter.Fill(RrHistoria);

			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (RrHistoria.Tables[0].Rows.Count == 0 || Convert.IsDBNull(RrHistoria.Tables[0].Rows[0]["ghistoria"]))
			{
				MessageBox.Show("Error: El paciente carece de n�mero de historia", Application.ProductName);
				result = "";
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrHistoria.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrHistoria.Close();
				return result;
			}

			if (vstCadenaAnatomia == "")
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vstCadenaAnatomia = "NHC=" + Convert.ToString(RrHistoria.Tables[0].Rows[0]["ghistoria"]) + 
				                    "&USER=" + gUsuario.Trim().ToUpper();
			}
			else
			{

				if (vstCadenaAnatomia.IndexOf('#') >= 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					vstCadenaAnatomia = vstCadenaAnatomia.Substring(0, Math.Min(vstCadenaAnatomia.IndexOf('#'), vstCadenaAnatomia.Length)) + 
					                    Convert.ToString(RrHistoria.Tables[0].Rows[0]["ghistoria"]) + 
					                    vstCadenaAnatomia.Substring(vstCadenaAnatomia.IndexOf('#') + 1);
				}

				if (vstCadenaAnatomia.IndexOf('$') >= 0)
				{
					vstCadenaAnatomia = vstCadenaAnatomia.Substring(0, Math.Min(vstCadenaAnatomia.IndexOf('$'), vstCadenaAnatomia.Length)) + 
					                    gUsuario.Trim().ToUpper() + 
					                    vstCadenaAnatomia.Substring(vstCadenaAnatomia.IndexOf('$') + 1);
				}

			}

			result = vstServAnaIP + 
			         vstCadenaAnatomia;
			//                     "NHC=" & RrHistoria("ghistoria") & _
			//'                     "&USER=" & UCase$(Trim$(gUsuario))

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrHistoria.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrHistoria.Close();

			return result;
		}


		internal static string DireccionDiagImag(string stIdentificadorPaciente, string stIdEstudio = "", SqlConnection RcImagen = null, string stUsuar = "")
		{
			bool ErrImgDig = false;
			string result = String.Empty;
			string StSql = String.Empty;
			DataSet rdoTemp = null;
			string stConexion = String.Empty;

			result = "";

			try
			{
				ErrImgDig = true;

				StSql = "SELECT valfanu1, valfanu2, valfanu3 FROM SCONSGLO WHERE gconsglo = 'ILINKIMG'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, RcImagen);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count == 0)
				{
					MessageBox.Show("Error: No existe la constante ILINKIMG.", Application.ProductName);
					return result;
				}
				else
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					stConexion = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]) + "").Trim() + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu2"]) + "").Trim() + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu3"]) + "").Trim();
					if (stConexion == "")
					{
						MessageBox.Show("Error: No existe el valor de conexion.", Application.ProductName);
						return result;
					}

				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				rdoTemp.Close();
				rdoTemp = null;


				if (stIdEstudio.Trim() == "" || RcImagen == null)
				{
					return stConexion + "idpaciente=" + stIdentificadorPaciente.Trim() + "&Usuario=" + stUsuar;
				}

				// Buscaremos los episodios de un paciente
				//******************** O.Frias - 16/06/2007 ********************

				result = stConexion;

				if (stIdEstudio.Trim() != "")
				{
					StSql = "SELECT * FROM SCONSGLO WHERE gconsglo = 'URLESTUD'";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, RcImagen);
					rdoTemp = new DataSet();
					tempAdapter_2.Fill(rdoTemp);
					if (rdoTemp.Tables[0].Rows.Count != 0)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]).Trim() == "S")
						{
							result = result + "idestudio=" + stIdEstudio.Trim() + "&";
						}
					}
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					rdoTemp.Close();
				}

				result = result + "idpaciente=" + stIdentificadorPaciente.Trim() + "&Usuario=" + stUsuar;

				ErrImgDig = false;
				return result;
			}
			catch (Exception excep)
			{
				if (!ErrImgDig)
				{
					throw excep;
				}

				if (ErrImgDig)
				{

					MessageBox.Show(excep.Message, "DireccionDiagImag", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return "";
				}
			}
			return System.String.Empty;
		}

		internal static string DireccionSiglo(string stHistoria, string gUsuario, SqlConnection RcSiglo, string stIdentificador)
		{
			string stSessionID = String.Empty;
			System.DateTime tmpDate = DateTime.FromOADate(0);
			string stArgumento = String.Empty;

			try
			{

				// Comprobamos que el host no est� vac�o

				if (vstServLabIP == "")
				{
					MessageBox.Show("Error: HOST o IP del servidor no definido", Application.ProductName);
					return "";
				}

				// Si el argumento tiene que ser el gidenpac, lo recuperamos

				if (vstArgumento == "G")
				{
					stArgumento = stIdentificador;
				}
				else
				{
					stArgumento = stHistoria;
				}

				// A�adimos el prefijo, si lo hubiera

				stArgumento = vstPrefijo + stArgumento;


				if (vstTipoLlamadaLab == "U")
				{

					// Realizamos la llamada

					return vstTipoSiglo + "://" + 
					vstServLabIP + "/" + 
					vstEstructuraSiglo + 
					"USR=" + vstUsuarioSiglo + 
					"&PASS=" + vstClaveSiglo + 
					"&ACCION=LISTA&PID=" + stArgumento.Trim() + 
					                                        "&INTRANET=1";

					//                         "&ACCION=LISTA&PID=" & Trim$(stHistoria) & _
					//'                         "&INTRANET=1"

					//        DireccionSiglo = "https://" & _
					//'                         vstServLabIP & "/cgi-bin/sigloweb.exe?" & _
					//'                         "USR=" & vstUsuarioSiglo & _
					//'                         "&PASS=" & vstClaveSiglo & _
					//'                         "&ACCION=LISTA&PID=" & Trim$(stHistoria) & _
					//'                         "&INTRANET=1"
				}
				else
				{

					// Creamos el SESSIONID

					tmpDate = DateTime.Now;

					stSessionID = tmpDate.ToString("yyyy") + tmpDate.ToString("MM") + tmpDate.ToString("dd") + 
					              tmpDate.ToString("HH") + tmpDate.ToString("mm") + tmpDate.ToString("ss") + 
					              "_" + gUsuario.ToUpper() + ".AUTH";

					if (!CrearFicheroDeSesion(stSessionID, gUsuario, RcSiglo))
					{
						return "";
					}
					else
					{

						// El identificador ser� el n�mero de historia del paciente

						return vstTipoSiglo + "://" + 
						vstServLabIP + "/" + 
						vstEstructuraSiglo + 
						"SESIONID=" + stSessionID.Substring(0, Math.Min(stSessionID.Length - 5, stSessionID.Length)) + 
						                                    "&ACCION=LISTA&PID=" + stArgumento.Trim() + 
						                                    "&INTRANET=1";

						//                             "&ACCION=LISTA&PID=" & Trim$(stHistoria) & _
						//'                             "&INTRANET=1"

						//            DireccionSiglo = "https://" & _
						//'                             vstServLabIP & "/cgi-bin/sigloweb.exe?SESIONID=" & _
						//'                             Left(stSessionID, Len(stSessionID) - 5) & "&ACCION=LISTA&PID=" & Trim$(stHistoria) & _
						//'                             "&INTRANET=1"
					}

				}
			}
			catch
			{

				Serrores.AnalizaError("HistoriaClinicaPaciente", Path.GetDirectoryName(Application.ExecutablePath), "", "DireccionSiglo", RcSiglo);
				return "";
			}
		}




		internal static string DireccionIzasa(string stHistoria, SqlConnection RcSiglo)
		{

			try
			{

				// Comprobamos que el host no est� vac�o

				if (vstServLabIP == "")
				{
					MessageBox.Show("Error: HOST o IP del servidor no definido", Application.ProductName);
					return "";
				}

				//http://198.168.121.81/directo.htm&n�historiadelpaciente&parametro&usuario
				// Realizamos la llamada


				return vstTipoSiglo + "://" + 
				vstServLabIP + "/" + 
				vstEstructuraSiglo + 
				stHistoria.Trim() + "&" + 
				                vstUsuarioSiglo;
			}
			catch
			{

				Serrores.AnalizaError("HistoriaClinicaPaciente", Path.GetDirectoryName(Application.ExecutablePath), "", "DireccionIzasa", RcSiglo);
				return "";
			}
		}

		internal static bool CrearFicheroDeSesion(string stSessionID, string gUsuario, SqlConnection RcFichSesion)
		{
			string stDirectorio = String.Empty;
			string stTemp = String.Empty;
			FixedLengthString stIP = new FixedLengthString(256);
			string stResultado = String.Empty;
			int lngPtrToHOSTENT = 0;
			mbAcceso.HOSTENT udtHostent = new mbAcceso.HOSTENT();
			int lngPtrToIP = 0;
			byte[] arrIpAddress = null;
			StringBuilder strIpAddress = new StringBuilder();
			string stPathSiglo = String.Empty;

			try
			{

				//Jes�s 10/12/2004 Almacenar en una constante global la ruta de Siglo
				// Tomamos el path completo del fichero a crear donde la ruta
				// viene especificada en una constante global
				DataSet RrPath = null;
				stPathSiglo = "";
				SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO ='PATSIGLO'", RcFichSesion);
				RrPath = new DataSet();
				tempAdapter.Fill(RrPath);
				if (RrPath.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (!Convert.IsDBNull(RrPath.Tables[0].Rows[0][0]))
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						stPathSiglo = Convert.ToString(RrPath.Tables[0].Rows[0][0]).Trim();
					}
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrPath.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrPath.Close();


				//stDirectorio = "c:\siglo\web\sesion\" & stSessionID
				stDirectorio = stPathSiglo + stSessionID;

				// Tomamos el host y vemos si hay error

				string tempRefParam = stIP.Value;
				if (ConfirmacionTomasSupport.PInvoke.SafeNative.ws2_32.gethostname(ref tempRefParam, 256) == SOCKET_ERROR)
				{
					stIP.Value = tempRefParam;
					MessageBox.Show(Information.Err().LastDllError.ToString(), Application.ProductName);
					return false;
				}
				else
				{
					stIP.Value = tempRefParam;
				}

				// Quitamos los espacios en blanco

				stResultado = stIP.Value.Substring(0, Math.Min(stIP.Value.IndexOf(Strings.Chr(0).ToString()), stIP.Value.Length)).ToUpper();

				// Tomamos el host por el nombre

				string tempRefParam2 = stResultado.Trim();
				lngPtrToHOSTENT = ConfirmacionTomasSupport.PInvoke.SafeNative.ws2_32.gethostbyname(ref tempRefParam2);

				// Comprobamos si hubo error

				if (lngPtrToHOSTENT == 0)
				{
					MessageBox.Show(Information.Err().LastDllError.ToString(), Application.ProductName);
					return false;
				}
				else
				{
					// Copiamos la informaci�n recibida a la estructura udtHostent

					//UPGRADE_WARNING: (2081) LenB has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
					ConfirmacionTomasSupport.PInvoke.SafeNative.kernel32.RtlMoveMemory(ref udtHostent, lngPtrToHOSTENT, Marshal.SizeOf(udtHostent));

					// Tomamos el primer �ndice de las direcciones IP almacenadads

					ConfirmacionTomasSupport.PInvoke.SafeNative.kernel32.RtlMoveMemory(ref lngPtrToIP, udtHostent.hAddrList, 4);

					// Preparamos el Array para tomar la IP

					arrIpAddress = new byte[udtHostent.hLength];

					// Copiamos la IP al array

					ConfirmacionTomasSupport.PInvoke.SafeNative.kernel32.RtlMoveMemory(ref arrIpAddress[0], lngPtrToIP, udtHostent.hLength);

					// Convertimos la IP al formato correcto

					for (int I = 1; I <= udtHostent.hLength; I++)
					{
						strIpAddress.Append(arrIpAddress[I - 1].ToString() + ".");
					}

					// Quitamos el �ltimo punto

					strIpAddress = new StringBuilder(strIpAddress.ToString().Substring(0, Math.Min(strIpAddress.ToString().Length - 1, strIpAddress.ToString().Length)));
				}

				// Creamos el directorio y escribimos la informaci�n

				FileSystem.FileOpen(1, stDirectorio, OpenMode.Output, OpenAccess.Default, OpenShare.LockWrite, -1);
				stTemp = "USR=INDRA"; //& Trim(UCase(gUsuario))  Cambiado por permisos para hgc
				FileSystem.PrintLine(1, stTemp);
				stTemp = "DESCUSR=" + gUsuario.Trim().ToUpper();
				FileSystem.PrintLine(1, stTemp);
				stTemp = "IP=" + strIpAddress.ToString().Trim();
				FileSystem.PrintLine(1, stTemp);
				stTemp = "fecha=" + stSessionID.Substring(0, Math.Min(8, stSessionID.Length)).Trim();
				FileSystem.PrintLine(1, stTemp);
				stTemp = "HORA=" + stSessionID.Substring(8, Math.Min(6, stSessionID.Length - 8)).Trim();
				FileSystem.PrintLine(1, stTemp);
				//stTemp = "CONDICION=DEMO1='605','516'"
				//Print #1, stTemp
				FileSystem.FileClose(1);

				return true;
			}
			catch
			{

				Serrores.AnalizaError("HistoriaClinicaPaciente", Path.GetDirectoryName(Application.ExecutablePath), "", "CrearFicheroDeSesion", RcFichSesion);
				return false;
			}
		}

		internal static void TomarLaboratorio(SqlConnection rcTomar)
		{

			string StSql = String.Empty;
			DataSet RrDatos = null;


			string tmpValue = ObtenerCodigoSConsglo("ILINKLAB", "valfanu1", rcTomar);

			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (!(Convert.IsDBNull(tmpValue) || tmpValue == "N"))
			{
				vfLaboratorioActivado = true;

				// Tomamos el servicio que es

				tmpValue = ObtenerCodigoSConsglo("ILINKLAB", "valfanu2", rcTomar);

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(tmpValue))
				{
					vstServLab = tmpValue;
				}

				// Tomamos la IP o el host de la aplicaci�n

				tmpValue = ObtenerCodigoSConsglo("ILINKLAB", "valfanu3", rcTomar);

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(tmpValue))
				{
					vstServLabIP = tmpValue.Trim();
				}

				// Tipo de llamada a SIGLO

				if (vstServLab == "SIGLO" || vstServLab == "IZASA")
				{
					vstTipoLlamadaLab = "N";

					tmpValue = ObtenerCodigoSConsglo("ISIGLO", "valfanu1", rcTomar);

					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					if (!Convert.IsDBNull(tmpValue))
					{
						if (tmpValue.Trim().ToUpper() == "U")
						{
							vstTipoLlamadaLab = "U";
							vstUsuarioSiglo = "";
							vstClaveSiglo = "";

							tmpValue = ObtenerCodigoSConsglo("ISIGLO", "valfanu2", rcTomar);

							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							if (!Convert.IsDBNull(tmpValue))
							{
								vstUsuarioSiglo = tmpValue.Trim();
							}

							tmpValue = ObtenerCodigoSConsglo("ISIGLO", "valfanu3", rcTomar);

							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							if (!Convert.IsDBNull(tmpValue))
							{
								vstClaveSiglo = tmpValue.Trim();
							}
						}
					}

					// Tenemos que tomar los datos de la estructura

					StSql = "SELECT isNull(valfanu1, 'http') valfanu1, " + 
					        "isNull(valfanu2, 'cgi-bin/sigloweb.exe?') valfanu2, " + 
					        "isNull(valfanu3, '') valfanu3 FROM SCONSGLO WHERE " + 
					        "gconsglo = 'ISIGLO2'";

					SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcTomar);
					RrDatos = new DataSet();
					tempAdapter.Fill(RrDatos);

					if (RrDatos.Tables[0].Rows.Count == 0)
					{
						vstTipoSiglo = "http";
						vstEstructuraSiglo = "cgi-bin/sigloweb.exe?";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						vstTipoSiglo = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu1"]).Trim();
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						vstEstructuraSiglo = Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu2"]).Trim() + Convert.ToString(RrDatos.Tables[0].Rows[0]["valfanu3"]).Trim();
					}

					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrDatos.Close();

					// Tomamos datos sobre el argumento de la llamada

					StSql = "SELECT isNull(valfanu1, '') Prefijo, " + 
					        "isNull(valfanu2, 'H') Argumento from SCONSGLO where " + 
					        "gconsglo = 'ISIGLO3'";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, rcTomar);
					RrDatos = new DataSet();
					tempAdapter_2.Fill(RrDatos);

					if (RrDatos.Tables[0].Rows.Count == 0)
					{
						vstPrefijo = "";
						vstArgumento = "H";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						vstPrefijo = Convert.ToString(RrDatos.Tables[0].Rows[0]["Prefijo"]).Trim();
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						vstArgumento = Convert.ToString(RrDatos.Tables[0].Rows[0]["Argumento"]).Trim().ToUpper();
					}

					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrDatos.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					RrDatos.Close();

				}
			}
			else
			{
				vfLaboratorioActivado = false;
			}
		}

		internal static void TomarRayos(SqlConnection rcTomar)
		{

			string tmpValue = ObtenerCodigoSConsglo("ILINKRAD", "valfanu1", rcTomar);

			//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
			if (!(Convert.IsDBNull(tmpValue) || tmpValue == "N"))
			{

				vfRadiodiagnosticoActivado = true;

				// Tomamos el servicio que es

				tmpValue = ObtenerCodigoSConsglo("ILINKRAD", "valfanu2", rcTomar);

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(tmpValue))
				{
					vstServRad = tmpValue;
				}

				// Tomamos la IP o el host de la aplicaci�n

				tmpValue = ObtenerCodigoSConsglo("ILINKRAD", "valfanu3", rcTomar);

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(tmpValue))
				{
					vstServRadIP = tmpValue.Trim();
				}

				// Tomamos el tipo de llamada que se realizar�, por defecto G

				vstTipoLlamadaRad = "G";

				tmpValue = ObtenerCodigoSConsglo("IOKDICOM", "valfanu1", rcTomar);

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(tmpValue))
				{
					if (tmpValue.Trim().ToUpper() == "H")
					{
						vstTipoLlamadaRad = "H";
					}
				}

			}
			else
			{
				vfRadiodiagnosticoActivado = false;
			}
		}

		internal static void TomarAnatomia(SqlConnection rcTomar)
		{

			string tmpValue = ObtenerCodigoSConsglo("ILINKANA", "valfanu1", rcTomar);

			//UPGRADE_WARNING: (2080) IsEmpty was upgraded to a comparison and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
			if (!(String.IsNullOrEmpty(tmpValue) || tmpValue == "N"))
			{

				vfAnatomiaActivado = true;

				// Tomamos la IP o el host de la aplicaci�n

				tmpValue = ObtenerCodigoSConsglo("ILINKANA", "valfanu2", rcTomar);

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(tmpValue))
				{
					vstServAnaIP = tmpValue.Trim();
				}
				else
				{
					vstServAnaIP = "";
				}

				// Tomamos la cadena a montar

				tmpValue = ObtenerCodigoSConsglo("ILINKANA", "valfanu3", rcTomar);

				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(tmpValue))
				{
					//            If InStr(1, Trim(tmpValue), "#") = 0 Or InStr(1, Trim(tmpValue), "$") = 0 Then
					//                vstCadenaAnatomia = ""
					//            Else
					vstCadenaAnatomia = tmpValue.Trim();
					//            End If
				}
				else
				{
					vstCadenaAnatomia = "";
				}

			}
			else
			{
				vfAnatomiaActivado = false;
			}
		}

		internal static void TomarVisor(SqlConnection rcTomar)
		{


			// Valor por defecto

			vfVisorLaboratorioActivado = false;

			// Vemos las constantes globales

			string StSql = "select isNull(valfanu1,'N') valfanu1, isNull(valfanu2,'') valfanu2, " + 
			               "isNull(valfanu3,'') valfanu3 from SCONSGLO where gconsglo = 'PATHVISO'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcTomar);
			DataSet RrVisor = new DataSet();
			tempAdapter.Fill(RrVisor);

			if (RrVisor.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vfVisorLaboratorioActivado = (Convert.ToString(RrVisor.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S") && (Convert.ToString(RrVisor.Tables[0].Rows[0]["valfanu2"]).Trim() + Convert.ToString(RrVisor.Tables[0].Rows[0]["valfanu3"]).Trim() != "");
			}

			if (!vfVisorLaboratorioActivado)
			{
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrVisor.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrVisor.Close();
				return;
			}

			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			vstServVisor = Convert.ToString(RrVisor.Tables[0].Rows[0]["valfanu2"]).Trim() + Convert.ToString(RrVisor.Tables[0].Rows[0]["valfanu3"]).Trim();

			// Ahora que tenemos el servidor, vamos a ver qu� validaci�n necesitamos

			StSql = "select isNull(valfanu1,'F') valfanu1, isNull(valfanu2,'GENERICO') valfanu2, " + 
			        "isNull(valfanu3,'GENERICO') valfanu3 from SCONSGLO where gconsglo = 'PWDVISOR'";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, rcTomar);
			RrVisor = new DataSet();
			tempAdapter_2.Fill(RrVisor);

			if (RrVisor.Tables[0].Rows.Count == 0)
			{
				vfVisorLaboratorioActivado = false;
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrVisor.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrVisor.Close();
				return;
			}

			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			if (Convert.ToString(RrVisor.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "V")
			{
				vstTipoVisor = "V";
			}
			else
			{
				vstTipoVisor = "F";
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vstUsuarioVisor = Convert.ToString(RrVisor.Tables[0].Rows[0]["valfanu2"]).Trim();
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				vstPasswordVisor = Convert.ToString(RrVisor.Tables[0].Rows[0]["valfanu3"]).Trim();
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrVisor.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrVisor.Close();

		}

		internal static bool proEjecutarVisor(string stMuestra, string stHistoria, object Formulario)
		{

			bool result = false;
			int hs = 0;

			result = true;
			// Comprobamos que nos den una muestra correcta
			if (stMuestra.Trim() == "")
			{
				//clase_mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 1100, RcVisor, "Identificador de muestra"
				return false;
			}
			// Comprobamos que venga el n�mero de historia
			if (stHistoria.Trim() == "")
			{
				//clase_mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 1100, RcVisor, "Identificador de muestra"
				return false;
			}

			// Comprobamos el tipo de validaci�n

			if (vstTipoVisor == "V")
			{
				// Si en un futuro es necesario utilizar una pantalla intermedia, ser� aqu�
				// donde deber� llamarse para llenar los valores de usuario y contrase�a, es
				// decir, vstUsuarioVisor y vstPasswordVisor
			}

			// Montamos la sentencia a ejecutar

			//stSentencia = vstServVisor & " " & _
			//"USER=" & vstUsuarioVisor & " " & _
			//"PASSWORD=" & vstPasswordVisor & " " & _
			//"SAMPLE=" & Trim(stMuestra) & " " & _
			//"DATE=" & stFechaRea

			// Comprobamos que el fichero exista donde se dice
			string stSentencia = vstServVisor + "?" + 
			                     "MUESTRA=" + stMuestra.Trim() + "&" + 
			                     "USUARIO=" + vstUsuarioVisor + "&" + 
			                     "CLAVE=" + vstPasswordVisor + "&" + 
			                     "HISTORIA=" + stHistoria;


			//    If Dir(vstServVisor, vbArchive) <> "" Then
			//        MyAppID = Shell(stSentencia, 1) ' Se ejecuta .
			//        AppActivate MyAppID ' Se activa
			//        proEjecutarVisor = True
			//    Else
			//        'clase_mensaje.RespuestaMensaje vNomAplicacion, vAyuda, 1100, RcVisor, "Visor de laboratorio (" & vstServVisor & ")"
			//        proEjecutarVisor = False
			//    End If

			if (stSentencia != "")
			{
				//UPGRADE_TODO: (1067) Member hwnd is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				string tempRefParam = "open";
				string tempRefParam2 = "";
				string tempRefParam3 = Path.GetDirectoryName(Application.ExecutablePath);
				hs = ConfirmacionTomasSupport.PInvoke.SafeNative.shell32.ShellExecute(Convert.ToInt32(Formulario.hwnd), ref tempRefParam, ref stSentencia, ref tempRefParam2, ref tempRefParam3, 1);

				// Si hay error lo mostramos

				if (hs < 33)
				{
					result = false;
				}
			}
			return result;
		}

		internal static string proTomarHistoria(SqlConnection rcHistoria, string stIdentificador)
		{


			// Buscamos el correspondiente al paciente

			string result = String.Empty;
			string StSql = "select ghistoria from HDOSSIER where gidenpac = '" + stIdentificador.Trim() + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcHistoria);
			DataSet RrHistoria = new DataSet();
			tempAdapter.Fill(RrHistoria);

			if (RrHistoria.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				result = Convert.ToString(RrHistoria.Tables[0].Rows[0]["ghistoria"]).Trim();
			}
			else
			{
				result = "";
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrHistoria.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrHistoria.Close();

			return result;
		}


		internal static string proTomarCIPAutonomico(SqlConnection rcHistoria, string stIdentificador)
		{


			// Buscamos el correspondiente al paciente
			string result = String.Empty;
			result = "";

			string StSql = "select ncipauto, ocipauto from DPACIENT  where gidenpac = '" + stIdentificador.Trim() + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcHistoria);
			DataSet rrpaciente = new DataSet();
			tempAdapter.Fill(rrpaciente);

			if (rrpaciente.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(rrpaciente.Tables[0].Rows[0]["ocipauto"]))
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					result = Convert.ToString(rrpaciente.Tables[0].Rows[0]["ocipauto"]).Trim();
				}
				else if (!Convert.IsDBNull(rrpaciente.Tables[0].Rows[0]["ncipauto"]))
				{ 
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					result = Convert.ToString(rrpaciente.Tables[0].Rows[0]["ncipauto"]).Trim();
				}
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rrpaciente.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rrpaciente.Close();

			return result;
		}


		internal static void ActulizaDatosWeb(SqlConnection rcConUno, SqlConnection rcConDos, string stIdPac, string stCodUsuar)
		{
			DataSet rdoEnti = null;
			int codSS = 0;
			int codPriva = 0;

			string StSql = "select nnumeri1 from sconsglo where gconsglo = 'segursoc'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, rcConUno);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				codSS = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["nnumeri1"])) ? 0 : Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["nnumeri1"]);
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rdoTemp.Close();
			rdoTemp = null;

			StSql = "select nnumeri1 from sconsglo where gconsglo = 'privado'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, rcConUno);
			rdoTemp = new DataSet();
			tempAdapter_2.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				codPriva = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["nnumeri1"])) ? 0 : Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["nnumeri1"]);
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			rdoTemp.Close();
			rdoTemp = null;


			StSql = "SELECT DPACIENT.dnombpac as dnombpac, DPACIENT.dape1pac as dape1pac, DPACIENT.dape2pac as dape2pac, " + 
			        "DPACIENT.fnacipac as fnacipac, DPACIENT.itipsexo as itipsexo, DPACIENT.ndninifp as ndninifp, " + 
			        "DPACIENT.ntarjeta as ntarjeta, DPACIENT.gsocieda as gsocieda, DPACIENT.nafiliac as nafiliac, " + 
			        "DPRIVADO.dnombpri as dnombpri, DPRIVADO.dape1pri as dape1pri, DPRIVADO.dape2pri as dape2pri, " + 
			        "DPRIVADO.nnifpriv as nnifpriv, DPACIENT.gtipovia as gtipovia, DPACIENT.DDOMICIL as DDOMICIL, " + 
			        "DPACIENT.nnumevia as nnumevia, DPACIENT.dotrovia as dotrovia, DPACIENT.gcodipos as gcodipos, " + 
			        "DPACIENT.gpoblaci as gpoblaci, DPACIENT.ntelefo1 as ntelefo1, DPACIENT.ntelefo3 as ntelefo3, " + 
			        "DPACIENT.ddiemail as ddiemail, DPACIENT.gprovinc as gprovinc, DPACIENT.gpaisres as gpaisres, " + 
			        "DPACIENT.ienvisms as ienvisms, DPACIENT.npasapor as npasapor " + 
			        "From DPACIENT " + 
			        "LEFT JOIN DPRIVADO ON DPACIENT.gidenpac = DPRIVADO.gidenpac " + 
			        "WHERE DPACIENT.gidenpac = '" + stIdPac + "' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, rcConUno);
			DataSet rdoPaci = new DataSet();
			tempAdapter_3.Fill(rdoPaci);
			if (rdoPaci.Tables[0].Rows.Count != 0)
			{

				StSql = "SELECT dnombpac, dape1pac, dape2pac, fnacipac, itipsexo, ndninifp, iregieco, " + 
				        "ntarjsan, gsocieda, nafiliac, dnombpri, dape1pri, dape2pri, nnifpriv, " + 
				        "gtipovia, dnombvia, nnumevia, dotravia, gcodipos, gpoblaci, ntelefo1, " + 
				        "nnumovil, ddiemail, fmecaniz, gprovinc, gpaisres, gcodcias, ienvisms, " + 
				        "npasapor, nversion " + 
				        "FROM USUARWEB WHERE codusuar = '" + stCodUsuar + "' ";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSql, rcConDos);
				rdoTemp = new DataSet();
				tempAdapter_4.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_ISSUE: (2064) RDO.rdoConnection method rcConDos.BeginTrans was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();
					//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rdoTemp.Edit was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					rdoTemp.Edit();

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					rdoTemp.Tables[0].Rows[0]["dnombpac"] = rdoPaci.Tables[0].Rows[0]["dnombpac"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					rdoTemp.Tables[0].Rows[0]["dape1pac"] = rdoPaci.Tables[0].Rows[0]["dape1pac"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["dape2pac"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["dape2pac"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["dape2pac"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["fnacipac"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["fnacipac"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["fnacipac"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["itipsexo"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["itipsexo"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["itipsexo"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["ndninifp"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["ndninifp"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["ndninifp"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["ntarjsan"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["ntarjeta"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["ntarjeta"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["gsocieda"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["gsocieda"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["gsocieda"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["nafiliac"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["nafiliac"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["nafiliac"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["gtipovia"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["gtipovia"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["gtipovia"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["dnombvia"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["DDOMICIL"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["DDOMICIL"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["nnumevia"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["nnumevia"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["nnumevia"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["dotravia"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["dotrovia"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["dotrovia"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["gcodipos"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["gcodipos"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["gcodipos"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["gpoblaci"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["gpoblaci"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["gpoblaci"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["ntelefo1"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["ntelefo1"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["ntelefo1"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["nnumovil"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["ntelefo3"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["ntelefo3"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["ddiemail"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["ddiemail"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["ddiemail"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["gprovinc"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["gprovinc"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["gprovinc"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["gpaisres"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["gpaisres"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["gpaisres"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["ienvisms"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["ienvisms"])) ? "N" : rdoPaci.Tables[0].Rows[0]["ienvisms"];
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
					rdoTemp.Tables[0].Rows[0]["npasapor"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["npasapor"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["npasapor"];

					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					if (Convert.ToDouble(rdoPaci.Tables[0].Rows[0]["gsocieda"]) == codSS)
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						rdoTemp.Tables[0].Rows[0]["iregieco"] = "S";
					}
					else
					{
						//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
						if (Convert.ToDouble(rdoPaci.Tables[0].Rows[0]["gsocieda"]) == codPriva)
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							rdoTemp.Tables[0].Rows[0]["iregieco"] = "P";
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							rdoTemp.Tables[0].Rows[0]["dnombpri"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["dnombpri"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["dnombpri"];
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							rdoTemp.Tables[0].Rows[0]["dape1pri"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["dape1pri"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["dape1pri"];
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							rdoTemp.Tables[0].Rows[0]["dape2pri"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["dape2pri"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["dape2pri"];
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
							rdoTemp.Tables[0].Rows[0]["nnifpriv"] = (Convert.IsDBNull(rdoPaci.Tables[0].Rows[0]["nnifpriv"])) ? DBNull.Value : rdoPaci.Tables[0].Rows[0]["nnifpriv"];
						}
						else
						{
							//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
							StSql = "SELECT * From DENTIPAC " + 
							        "WHERE gidenpac = '" + stIdPac + "' AND gsocieda = " + Convert.ToString(rdoPaci.Tables[0].Rows[0]["gsocieda"]);
							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSql, rcConUno);
							rdoEnti = new DataSet();
							tempAdapter_5.Fill(rdoEnti);
							if (rdoEnti.Tables[0].Rows.Count != 0)
							{
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								rdoTemp.Tables[0].Rows[0]["iregieco"] = "A";
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
								rdoTemp.Tables[0].Rows[0]["gcodcias"] = (Convert.IsDBNull(rdoEnti.Tables[0].Rows[0]["gcodcias"])) ? DBNull.Value : rdoEnti.Tables[0].Rows[0]["gcodcias"];
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
								rdoTemp.Tables[0].Rows[0]["nafiliac"] = (Convert.IsDBNull(rdoEnti.Tables[0].Rows[0]["nafiliac"])) ? DBNull.Value : rdoEnti.Tables[0].Rows[0]["nafiliac"];
								//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
								rdoTemp.Tables[0].Rows[0]["nversion"] = (Convert.IsDBNull(rdoEnti.Tables[0].Rows[0]["nversion"])) ? DBNull.Value : rdoEnti.Tables[0].Rows[0]["nversion"];
							}
						}
					}
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					rdoTemp.Tables[0].Rows[0]["fmecaniz"] = DateTime.Now;
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					string tempQuery = rdoTemp.Tables(0).TableName;
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_6);
					tempAdapter_6.Update(rdoTemp, rdoTemp.Tables(0).TableName);
					//UPGRADE_ISSUE: (2064) RDO.rdoConnection method rcConDos.CommitTrans was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
					UpgradeStubs.System_Data_SqlClient_SqlConnection.CommitTrans();
				}
			}

		}

		//OSCAR C Febrero 2010
		internal static bool fVisorWEBAsociado(string codigoPrestacion, SqlConnection RcEnfermeria)
		{
			bool result = false;
			blnVisorAplicacion = false;
			string sql = "select SVISORPR.sliteweb, SVISORPR.surlweb, ISNULL(SVISORPR.itivisor, 'W') itivisor from SVISORPR " + 
			             " INNER JOIN DCODPRES on DCODPRES.gtipovis = SVISORPR.gtipovis" + 
			             " where DCODPRES.gprestac = '" + codigoPrestacion.Trim() + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcEnfermeria);
			DataSet RrServicio = new DataSet();
			tempAdapter.Fill(RrServicio);
			if (RrServicio.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx
				if (!Convert.IsDBNull(RrServicio.Tables[0].Rows[0]["sliteweb"]) && !Convert.IsDBNull(RrServicio.Tables[0].Rows[0]["surlweb"]))
				{
					result = true;
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					stVisorWEB_literal = Convert.ToString(RrServicio.Tables[0].Rows[0]["sliteweb"]).Trim();
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					stVisorWEB_URL = Convert.ToString(RrServicio.Tables[0].Rows[0]["surlweb"]).Trim();
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					blnVisorAplicacion = (Convert.ToString(RrServicio.Tables[0].Rows[0]["itivisor"]).Trim() == "A");
				}
			}
			bVisorWEB = result;
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrServicio.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			RrServicio.Close();
			return result;
		}

		internal static string DireccionVisorWeb(SqlConnection rCon, string stTipoSer, string stAnoReg, string stNumReg, string stIdPaciente, string stHistoria, string stServicio, string stPrestacion, string stFecha, string stNumeroMuestra, string stUsuario)
		{

			string stCadenaVisorWeb = String.Empty;

			if (stVisorWEB_URL == "")
			{
				MessageBox.Show("Error: URL del Servicio no definido", Application.ProductName);
				return "";
			}

			//If Trim(stHistoria = "") Then
			//    MsgBox "Error: El paciente carece de n�mero de historia"
			//    DireccionVisorWeb = ""
			//    Exit Function
			//End If

			if (!blnVisorAplicacion)
			{
				stCadenaVisorWeb = stVisorWEB_URL + "?";
				stCadenaVisorWeb = stCadenaVisorWeb + "FLLAMADA=" + DateTime.Now.ToString("yyyyMMddHHmmss");
				stCadenaVisorWeb = stCadenaVisorWeb + "&T=" + stTipoSer.Trim();
				stCadenaVisorWeb = stCadenaVisorWeb + "&A=" + stAnoReg.Trim();
				stCadenaVisorWeb = stCadenaVisorWeb + "&N=" + stNumReg.Trim();
				stCadenaVisorWeb = stCadenaVisorWeb + "&GIDENPAC=" + stIdPaciente.Trim();
				stCadenaVisorWeb = stCadenaVisorWeb + "&NHISTORIA=" + stHistoria.Trim();
				stCadenaVisorWeb = stCadenaVisorWeb + "&GSERVICI=" + stServicio.Trim();
				stCadenaVisorWeb = stCadenaVisorWeb + "&PRESTACION=" + stPrestacion.Trim();
				System.DateTime TempDate = DateTime.FromOADate(0);
				stCadenaVisorWeb = stCadenaVisorWeb + "&FECHA=" + ((DateTime.TryParse(stFecha, out TempDate)) ? TempDate.ToString("yyyyMMddHHmmss") : stFecha);
				stCadenaVisorWeb = stCadenaVisorWeb + "&NMUESTRA=" + stNumeroMuestra.Trim();
				stCadenaVisorWeb = stCadenaVisorWeb + "&USUARIO=" + stUsuario.Trim().ToUpper();
				stCadenaVisorWeb = stCadenaVisorWeb + "&CENTRO=" + Serrores.ObternerValor_CTEGLOBAL(rCon, "FFACTPRI", "VALFANU1").Trim();
			}
			else
			{
				stCadenaVisorWeb = stVisorWEB_URL + " " + stIdPaciente.Trim();
			}
			return stCadenaVisorWeb;

		}
		//---------------------


		internal static bool BuscarSociedadEpisodio(string PacId, int PacGano, int PacGnu, string stTipoSer, SqlConnection rCon)
		{

			bool result = false;
			string stCodigosoc = String.Empty, lCodSS = String.Empty;
			bool EsSS = false;
			bool EstaFacturado = false;

			result = true;

			string tSqlSoci = "SELECT * FROM  SCONSGLO WHERE gconsglo = 'IMPFCTSM' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(tSqlSoci, rCon);
			DataSet tRrSoci = new DataSet();
			tempAdapter.Fill(tRrSoci);
			if (tRrSoci.Tables[0].Rows.Count != 0)
			{
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				if (Convert.ToString(tRrSoci.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					return true;
				}
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRrSoci.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			tRrSoci.Close();



			//busca la sociedad del paciente
			tSqlSoci = "SELECT NNUMERI1,valfanu1 FROM  SCONSGLO WHERE gconsglo = 'SEGURSOC'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tSqlSoci, rCon);
			tRrSoci = new DataSet();
			tempAdapter_2.Fill(tRrSoci);
			if (tRrSoci.Tables[0].Rows.Count != 0)
			{ //SS
				//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
				lCodSS = Convert.ToString(tRrSoci.Tables[0].Rows[0]["NNUMERI1"]).Trim();
			}

			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRrSoci.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			tRrSoci.Close();

			//''''''''''
			if (stTipoSer == "H")
			{
				tSqlSoci = "SELECT gsocieda FROM  AMOVIFIN " + " WHERE AMOVIFIN.GNUMREGI=" + PacGnu.ToString() + " AND AMOVIFIN.GANOREGI= " + PacGano.ToString() + " order by fmovimie";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tSqlSoci, rCon);
				tRrSoci = new DataSet();
				tempAdapter_3.Fill(tRrSoci);
				//UPGRADE_ISSUE: (1040) MoveLast function is not supported. More Information: http://www.vbtonet.com/ewis/ewi1040.aspx
				tRrSoci.MoveLast(null);
				if (tRrSoci.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					stCodigosoc = Convert.ToString(tRrSoci.Tables[0].Rows[0]["gsocieda"]);
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRrSoci.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				tRrSoci.Close();
			}
			else if (stTipoSer == "C")
			{ 
				tSqlSoci = "SELECT gsocieda FROM  CCONSULT " + " WHERE gidenpac = '" + PacId + "' and " + "  GNUMREGI=" + PacGnu.ToString() + " AND " + "  GANOREGI= " + PacGano.ToString() + " ";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tSqlSoci, rCon);
				tRrSoci = new DataSet();
				tempAdapter_4.Fill(tRrSoci);
				if (tRrSoci.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					stCodigosoc = Convert.ToString(tRrSoci.Tables[0].Rows[0]["gsocieda"]);
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRrSoci.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				tRrSoci.Close();
			}
			else if (stTipoSer == "U")
			{ 
				tSqlSoci = "SELECT gsocieda FROM  uepisurg " + " WHERE gidenpac = '" + PacId + "' and " + "  GNUMurge=" + PacGnu.ToString() + " AND " + "  GANOurge= " + PacGano.ToString() + " ";
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tSqlSoci, rCon);
				tRrSoci = new DataSet();
				tempAdapter_5.Fill(tRrSoci);
				if (tRrSoci.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					stCodigosoc = Convert.ToString(tRrSoci.Tables[0].Rows[0]["gsocieda"]);
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRrSoci.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				tRrSoci.Close();
			}
			else if (stTipoSer == "Q")
			{ 
				tSqlSoci = "SELECT gsocieda FROM  qintquir " + " WHERE gidenpac = '" + PacId + "' and " + "  GNUMREGI=" + PacGnu.ToString() + " AND " + "  GANOREGI= " + PacGano.ToString() + " ";
				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(tSqlSoci, rCon);
				tRrSoci = new DataSet();
				tempAdapter_6.Fill(tRrSoci);
				if (tRrSoci.Tables[0].Rows.Count != 0)
				{
					//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					stCodigosoc = Convert.ToString(tRrSoci.Tables[0].Rows[0]["gsocieda"]);
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRrSoci.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				tRrSoci.Close();

			}


			tSqlSoci = "SELECT ffactura FROM  DMOVFACT " + " WHERE gidenpac = '" + PacId + "' and " + "  GNUMREGI=" + PacGnu.ToString() + " AND " + "  GANOREGI= " + PacGano.ToString() + " and itiposer = '" + stTipoSer + "' and ffactura is not null";

			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(tSqlSoci, rCon);
			tRrSoci = new DataSet();
			tempAdapter_7.Fill(tRrSoci);
			if (tRrSoci.Tables[0].Rows.Count != 0)
			{
				EstaFacturado = true;
			}
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRrSoci.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			tRrSoci.Close();
			if (stCodigosoc == lCodSS)
			{
				EsSS = true;
			}

			if (!EsSS || (EstaFacturado && stTipoSer != "H"))
			{
				result = false;
			}


			return result;
		}
	}
}