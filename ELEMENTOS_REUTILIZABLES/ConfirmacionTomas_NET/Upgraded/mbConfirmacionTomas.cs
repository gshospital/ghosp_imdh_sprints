using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace ConfirmacionTomas
{
	internal static class mbConfirmacionTomas
	{


		private const int SWP_NOSIZE = 0x1;
		private const int SWP_NOMOVE = 0x2;
		private const int SWP_NOACTIVATE = 0x10;

		public static readonly int wFlags = SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE;

		//   Valores de hwndInsertAfter

		public const int HWND_TOPMOST = -1;
		public const int HWND_NOTOPMOST = -2;

		//UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
		//[DllImport("user32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int SetWindowPos(int hwnd, int hWndInsertAfter, int X, int y, int cx, int cy, int wFlags);

		// Estructuras

		public struct PerfusionSeleccion
		{
			public string NombreFarmaco;
			public string CodFarmaco;
			public string FechaApunte;
			public string FechaIni;
			public string FechaSus;
			public string FechaFin;
			public string FechaFMovi;
			public string CodMotivo;
			public string CodMedico;
			public string CodViaadm;
			public string Estado;
			public string Duracion;
			public string CodTomas;
			public string DesTomas;
			public string VsDias;
			public string Observa;
			public string SiPrecisa;
			public string Dosis;
			public string FechaPrevista;
			public string EstPrevisto;
			public short ncodperf;
			public short nordfarm;
			public string veloperf;
			public string oobsperf;
			public static PerfusionSeleccion CreateInstance()
			{
					PerfusionSeleccion result = new PerfusionSeleccion();
					result.NombreFarmaco = String.Empty;
					result.CodFarmaco = String.Empty;
					result.FechaApunte = String.Empty;
					result.FechaIni = String.Empty;
					result.FechaSus = String.Empty;
					result.FechaFin = String.Empty;
					result.FechaFMovi = String.Empty;
					result.CodMotivo = String.Empty;
					result.CodMedico = String.Empty;
					result.CodViaadm = String.Empty;
					result.Estado = String.Empty;
					result.Duracion = String.Empty;
					result.CodTomas = String.Empty;
					result.DesTomas = String.Empty;
					result.VsDias = String.Empty;
					result.Observa = String.Empty;
					result.SiPrecisa = String.Empty;
					result.Dosis = String.Empty;
					result.FechaPrevista = String.Empty;
					result.EstPrevisto = String.Empty;
					result.veloperf = String.Empty;
					result.oobsperf = String.Empty;
					return result;
			}
		}

		public struct FarmacoSeleccion
		{
			public string NombreFarmaco;
			public string CodFarmaco;
			public string FechaApunte;
			public string FechaIni;
			public string FechaSus;
			public string FechaFin;
			public string FechaFMovi;
			public string CodMotivo;
			public string CodMedico;
			public string CodViaadm;
			public string Estado;
			public string Duracion;
			public string CodTomas;
			public string VsDias;
			public short Periodic;
			public string Observa;
			public string SiPrecisa;
			public string DosisAdm;
			public string iLargTra;
			public string Fcubirec;
			public string[] Dosis;
			public string FechaPrevista;
			public string EstPrevisto;
			public string CodFrecuencia;
			public short A�oFarmaco;
			public int NumeroFarmaco;
			public string DiasInicio;
			public string HoraInicio;
			public string CodSuero;
			public string nordfarm;
			public string veloperf;
			public string oobsperf;
			public string iPropPac;
			public string CodFantasia;
			public string usuario;
			public short gsofarma;
			public string idounica;
			public string ggrutera; //(maplaza)(19/04/2007)Grupo Terape�tico
			public string iPerfusion;
			public string iMotDosisCero;

			public static FarmacoSeleccion CreateInstance()
			{
					FarmacoSeleccion result = new FarmacoSeleccion();
					result.NombreFarmaco = String.Empty;
					result.CodFarmaco = String.Empty;
					result.FechaApunte = String.Empty;
					result.FechaIni = String.Empty;
					result.FechaSus = String.Empty;
					result.FechaFin = String.Empty;
					result.FechaFMovi = String.Empty;
					result.CodMotivo = String.Empty;
					result.CodMedico = String.Empty;
					result.CodViaadm = String.Empty;
					result.Estado = String.Empty;
					result.Duracion = String.Empty;
					result.CodTomas = String.Empty;
					result.VsDias = String.Empty;
					result.Observa = String.Empty;
					result.SiPrecisa = String.Empty;
					result.DosisAdm = String.Empty;
					result.iLargTra = String.Empty;
					result.Fcubirec = String.Empty;
					result.Dosis = ArraysHelper.InitializeArray<string>(24);
					result.FechaPrevista = String.Empty;
					result.EstPrevisto = String.Empty;
					result.CodFrecuencia = String.Empty;
					result.DiasInicio = String.Empty;
					result.HoraInicio = String.Empty;
					result.CodSuero = String.Empty;
					result.nordfarm = String.Empty;
					result.veloperf = String.Empty;
					result.oobsperf = String.Empty;
					result.iPropPac = String.Empty;
					result.CodFantasia = String.Empty;
					result.usuario = String.Empty;
					result.idounica = String.Empty;
					result.ggrutera = String.Empty;
					result.iPerfusion = String.Empty;
					result.iMotDosisCero = String.Empty;
					return result;
			}
		}

		// Constantes

		public const int ALTA = 1; // Controla que el estado se alta
		public const int MODIFICADO = 2; // Controla que el estado sea modificacion
		public const int NOCAMBIOS = 3; // Controla que la fecha finmovimento es <> null

		// Variables que se cargan al entrar en la DLL

		public static SqlConnection RcEnfermeria = null;
		public static string gUsuario = String.Empty;
		public static string stPathAplicacion = String.Empty;
		public static Mensajes.ClassMensajes clase_mensaje = null;
		public static string stTipo = String.Empty;
		public static bool fUsuarioValidado = false;
		public static string UsuarioConfirmacion = String.Empty;
		public static dynamic FormularioOrigen = null;
		public static string vstSeparadorDecimal = String.Empty;
		public static string stDecimalBD = String.Empty;
		public static string VstUsuario_NT = String.Empty; //Usuario de NT
		public static mbAcceso.strEventoNulo[] astreventos = null; //Array para el control de acceso en los eventos
		public static mbAcceso.strControlOculto[] astrcontroles = null; //Array para el control de acceso de los controles

		public static string GstTiposervicio = String.Empty;
		public static string GstA�oFarmaco = String.Empty;
		public static string GstNumeroFarmaco = String.Empty;
		public static string GstCodigoFarmaco = String.Empty;
		public static string GstNombreFarmaco = String.Empty;
		public static string GstFechaApunte = String.Empty;
		public static string GstConfirmaMinima = String.Empty;
		public static string GstConfirmaMaxima = String.Empty;
		public static string vstFechaHoraSis = String.Empty;
		public static string vstFechaFinMov = String.Empty; //(maplaza)(18/09/2006)Para saber si el apunte est� cerrado o no

		public static string vstValorNEQUIVAL = String.Empty;
		public static bool gfProtegerCeldas = false;
		public static int iEstadoAccion = 0;
		public static bool DesdeBloqueo = false;

        public static mbConfirmacionTomas.FarmacoSeleccion farmacoSeleccion = mbConfirmacionTomas.FarmacoSeleccion.CreateInstance(); //estructura donde se guardan los datos del farmaco
		public static mbConfirmacionTomas.PerfusionSeleccion[] FarmacosPantalla = null; //array donde se guardan los farmaco que estan seleccionados en la pantalla


		// Funciones de uso com�n

        internal static bool ValidacionFecha(Telerik.WinControls.UI.RadDateTimePicker CampoFecha, Telerik.WinControls.UI.RadMaskedEditBox CampoHora, ref string etiqueta)
		{

			bool result = false;

			if (CampoFecha.Text == "")
			{

				short tempRefParam = 1040;
                string[] tempRefParam2 = new string[] { etiqueta };//new object{etiqueta};
				clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, RcEnfermeria, tempRefParam2);
				result = true;
				CampoFecha.Focus();

				return result;

			}
			else
			{

				if (CampoHora.Text.Trim() == ":")
				{

					short tempRefParam3 = 1040;
                    string[] tempRefParam4 = new string[] { "hora" };//new object{"hora"};
					clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, RcEnfermeria, tempRefParam4);
					result = true;
					CampoHora.Focus();

					return result;

				}
				else
				{

					if (!Information.IsDate(CampoHora.Text))
					{

						short tempRefParam5 = 1150;
                        string[] tempRefParam6 = new string[] { "hora" };//new object{"hora"};
						clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, RcEnfermeria, tempRefParam6);
						result = true;
						CampoHora.Focus();

						return result;

					}

				}

			}

			return result;
		}

		internal static void CargarDatosFarmaco()
		{

			string iDosisFarmaco = String.Empty;

			string stFechaFormateada = GstFechaApunte;

			string sqlDatos = "select DCODFARMVT.dnomfarm,DCODFARMVT.nequival,DCODFARMVT.ndnormal,DCODFARMVT.Fantasia,EFARMACO.*,";
			//(maplaza)(19/04/2007)Se obtiene este campo para saber el Grupo Terape�tico.
			sqlDatos = sqlDatos + "DCODFARMVT.ggrutera ";
			//-----
			sqlDatos = sqlDatos + "from EFARMACO,DCODFARMVT ";
			sqlDatos = sqlDatos + "where EFARMACO.itiposer ='" + GstTiposervicio + "' ";
			sqlDatos = sqlDatos + "and EFARMACO.ganoregi = " + GstA�oFarmaco + " "; //" & gstA�ofarmaco & " "
			sqlDatos = sqlDatos + "and EFARMACO.gnumregi = " + GstNumeroFarmaco + " "; //" & gstNumerofarmaco & " "
			
			object tempRefParam = stFechaFormateada;
			sqlDatos = sqlDatos + "and EFARMACO.fapuntes =" + Serrores.FormatFechaHMS(tempRefParam) + " ";
			stFechaFormateada = Convert.ToString(tempRefParam);
			sqlDatos = sqlDatos + "and EFARMACO.gfarmaco = '" + GstCodigoFarmaco + "' ";
			sqlDatos = sqlDatos + "and DCODFARMVT.gfarmaco = EFARMACO.gfarmaco ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlDatos, RcEnfermeria);
			DataSet RrDatos = new DataSet();
			tempAdapter.Fill(RrDatos);

			if (RrDatos.Tables[0].Rows.Count != 0)
			{
                
				// Fechas del apunte
				
				farmacoSeleccion.FechaApunte = Convert.ToString(RrDatos.Tables[0].Rows[0]["fapuntes"]);				
				farmacoSeleccion.FechaIni = Convert.ToString(RrDatos.Tables[0].Rows[0]["finitrat"]);				
				farmacoSeleccion.idounica = ((Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["idounica"])) ? "N" : Convert.ToString(RrDatos.Tables[0].Rows[0]["idounica"])).Trim().ToUpper();

				// Dependiendo de lo que est� relleno se cargar� con la fecha de suspensi�n o con la de finalizaci�n				
				if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["fsuspens"]))
				{					
					farmacoSeleccion.FechaSus = Convert.ToString(RrDatos.Tables[0].Rows[0]["fsuspens"]);
				}
				else
				{					
					if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ffintrat"]))
					{
						farmacoSeleccion.FechaFin = Convert.ToString(RrDatos.Tables[0].Rows[0]["ffintrat"]);
					}
					else
					{
						farmacoSeleccion.FechaFin = "";
						farmacoSeleccion.FechaSus = "";
					}
				}

				// Fecha fin de movimiento
                			
				if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ffinmovi"]))
				{					
					farmacoSeleccion.FechaFMovi = Convert.ToString(RrDatos.Tables[0].Rows[0]["ffinmovi"]);
				}
				else
				{
					farmacoSeleccion.FechaFMovi = "";
				}

				// Motivo de la suspension
                				
				if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["gmsuspro"]))
				{					
					farmacoSeleccion.CodMotivo = Conversion.Str(RrDatos.Tables[0].Rows[0]["gmsuspro"]);
				}
				else
				{
					farmacoSeleccion.CodMotivo = "";
				}

				// Nombre del farmaco
				GstNombreFarmaco = Convert.ToString(RrDatos.Tables[0].Rows[0]["dnomfarm"]).Trim();
				farmacoSeleccion.CodFarmaco = GstCodigoFarmaco;

				// Estado del farmaco				
				farmacoSeleccion.Estado = Convert.ToString(RrDatos.Tables[0].Rows[0]["iesttrat"]);

				// M�dico
				farmacoSeleccion.CodMedico = Convert.ToString(RrDatos.Tables[0].Rows[0]["gpersona"]);

				// V�a de administraci�n
				farmacoSeleccion.CodViaadm = Convert.ToString(RrDatos.Tables[0].Rows[0]["gviadmin"]);

				// Duraci�n en d�as
				farmacoSeleccion.Duracion = (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["cdiastra"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["cdiastra"]);

				// Unidad de dosis
				if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["gunitoma"]))
				{
					farmacoSeleccion.CodTomas = "";
				}
				else
				{					
					farmacoSeleccion.CodTomas = Convert.ToString(RrDatos.Tables[0].Rows[0]["gunitoma"]);
				}

				
				if (Convert.ToString(RrDatos.Tables[0].Rows[0]["iprecisa"]) == "S")
				{
					for (int aik = 0; aik <= 23; aik++)
					{						
						if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["cdosisfa" + StringsHelper.Format(aik, "00") + ""]))
						{							
							string tempRefParam2 = Convert.ToString(RrDatos.Tables[0].Rows[0]["cdosisfa" + StringsHelper.Format(aik, "00") + ""]);
							farmacoSeleccion.DosisAdm = Serrores.ConvertirDecimales(ref tempRefParam2, ref vstSeparadorDecimal, 2).Trim();
							break;
						}
					}
				}
				else
				{
					switch(vstValorNEQUIVAL)
					{
						case "nequival" : 							
							iDosisFarmaco = (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["nequival"])) ? "0" : Convert.ToString(RrDatos.Tables[0].Rows[0]["nequival"]); 
							break;
						case "ndnormal" : 							
							iDosisFarmaco = (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ndnormal"])) ? "0" : Convert.ToString(RrDatos.Tables[0].Rows[0]["ndnormal"]); 
							break;
						case "nnumeri1" : 
							iDosisFarmaco = mbAcceso.ObtenerCodigoSConsglo("NEQUIVAL", "nnumeri1", RcEnfermeria); 
							break;
						default:
							iDosisFarmaco = "0"; 
							break;
					}
					if (StringsHelper.ToDoubleSafe(iDosisFarmaco) == 0)
					{
						farmacoSeleccion.DosisAdm = "";
					}
					else
					{
						string tempRefParam3 = Conversion.Str(iDosisFarmaco).Trim();
						farmacoSeleccion.DosisAdm = Serrores.ConvertirDecimales(ref tempRefParam3, ref vstSeparadorDecimal, 2).Trim();
					}
				}

				// Dosis				
				if (Convert.ToString(RrDatos.Tables[0].Rows[0]["iprecisa"]) == "S")
				{
					farmacoSeleccion.SiPrecisa = "S";

					// Se cargan las horas/dosis en la estructura interna del f�rmaco

					for (int aik = 0; aik <= 23; aik++)
					{						
						if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["cdosisfa" + StringsHelper.Format(aik, "00") + ""]))
						{
							farmacoSeleccion.Dosis[aik] = "";
						}
						else
						{						
							string tempRefParam4 = Convert.ToString(RrDatos.Tables[0].Rows[0]["cdosisfa" + StringsHelper.Format(aik, "00") + ""]);
							farmacoSeleccion.Dosis[aik] = Serrores.ConvertirDecimales(ref tempRefParam4, ref vstSeparadorDecimal).Trim();
						}
					}

					// D�as de la semana
					if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["vdiasfre"]))
					{						
						farmacoSeleccion.VsDias = Convert.ToString(RrDatos.Tables[0].Rows[0]["vdiasfre"]);
					}
					else
					{
						farmacoSeleccion.VsDias = "";
					}
                    				
					farmacoSeleccion.CodFrecuencia = (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["gfretoma"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["gfretoma"]);

				}
				else
				{

					farmacoSeleccion.SiPrecisa = "N";

					// Se cargan las horas y no la frecuencia

					for (int aik = 0; aik <= 23; aik++)
					{
						if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["cdosisfa" + StringsHelper.Format(aik, "00") + ""]))
						{
							farmacoSeleccion.Dosis[aik] = "";
						}
						else
						{							
							string tempRefParam5 = Convert.ToString(RrDatos.Tables[0].Rows[0]["cdosisfa" + StringsHelper.Format(aik, "00") + ""]);
							farmacoSeleccion.Dosis[aik] = Serrores.ConvertirDecimales(ref tempRefParam5, ref vstSeparadorDecimal).Trim();
						}
					}

					// D�as de la semana
					if (!Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["vdiasfre"]))
					{
						farmacoSeleccion.VsDias = Convert.ToString(RrDatos.Tables[0].Rows[0]["vdiasfre"]);
					}
					else
					{
						farmacoSeleccion.VsDias = "";
					}

					farmacoSeleccion.CodFrecuencia = "";

				}

				// Frecuencia d�as
				farmacoSeleccion.Periodic = Convert.ToInt16(Conversion.Val((Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["periodic"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["periodic"])));

				// Observaciones
				farmacoSeleccion.Observa = (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["oobserva"])) ? "" : Convert.ToString(RrDatos.Tables[0].Rows[0]["oobserva"]).Trim();                				
				farmacoSeleccion.iLargTra = (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ilargtra"])) ? "N" : Convert.ToString(RrDatos.Tables[0].Rows[0]["ilargtra"]);
				farmacoSeleccion.iPropPac = (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["iproppac"])) ? "N" : Convert.ToString(RrDatos.Tables[0].Rows[0]["iproppac"]);

				if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["fprevist"]))
				{
					farmacoSeleccion.FechaPrevista = "";
				}
				else
				{
					farmacoSeleccion.FechaPrevista = Convert.ToString(RrDatos.Tables[0].Rows[0]["fprevist"]);
				}

				if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["iestprev"]))
				{
					farmacoSeleccion.EstPrevisto = "";
				}
				else
				{					
					farmacoSeleccion.EstPrevisto = Convert.ToString(RrDatos.Tables[0].Rows[0]["iestprev"]);
				}

				if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["fantasia"]))
				{
					farmacoSeleccion.CodFantasia = "";
				}
				else
				{					
					farmacoSeleccion.CodFantasia = Convert.ToString(RrDatos.Tables[0].Rows[0]["fantasia"]).Trim();
				}
				
				if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["gusuario"]))
				{
					farmacoSeleccion.usuario = "";
				}
				else
				{					
					farmacoSeleccion.usuario = Convert.ToString(RrDatos.Tables[0].Rows[0]["gusuario"]).Trim().ToUpper();
				}
				
				if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["gsofarma"]))
				{
					farmacoSeleccion.gsofarma = 0;
				}
				else
				{					
					farmacoSeleccion.gsofarma = (short) RrDatos.Tables[0].Rows[0]["gsofarma"];
				}
				
				if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["fcubirec"]))
				{
					farmacoSeleccion.Fcubirec = "";
				}
				else
				{
					farmacoSeleccion.Fcubirec = Convert.ToDateTime(RrDatos.Tables[0].Rows[0]["fcubirec"]).ToString("dd/MM/yyyy HH:NN:SS");
				}

				//(maplaza)(19/04/2007)GRUPO TERAPEUTICO				
				if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["ggrutera"]))
				{
					farmacoSeleccion.ggrutera = "";
				}
				else
				{
					farmacoSeleccion.ggrutera = Convert.ToString(RrDatos.Tables[0].Rows[0]["ggrutera"]).Trim();
				}
				//-----
                farmacoSeleccion.iMotDosisCero = Convert.ToString(RrDatos.Tables[0].Rows[0]["imotdoce"]) + "";
				
				sqlDatos = "SELECT iperfusi FROM DVIASADM where gviadmin=" + Convert.ToString(RrDatos.Tables[0].Rows[0]["gviadmin"]);
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlDatos, RcEnfermeria);
				RrDatos = new DataSet();
				tempAdapter_2.Fill(RrDatos);
				if (RrDatos.Tables[0].Rows.Count != 0)
				{					
					farmacoSeleccion.iPerfusion = Convert.ToString(RrDatos.Tables[0].Rows[0]["iperfusi"]) + "";
				}

			}			
			RrDatos.Close();

		}

		internal static string ObtenerValorNEQUIVAL()
		{
			//funcion que devuelve cual es el campo sobre el que hay que recuperar el valor que va
			//en el campo Dosis de la pantalla de detalle

			string result = String.Empty;
			result = "nequival";

			string Sql = "select * from sconsglo where gconsglo= 'NEQUIVAL' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(Sql, RcEnfermeria);
			DataSet RrConstantes = new DataSet();
			tempAdapter.Fill(RrConstantes);

			if (RrConstantes.Tables[0].Rows.Count != 0)
			{				
				if (!Convert.IsDBNull(RrConstantes.Tables[0].Rows[0]["valfanu2"]))
				{					
					if (Convert.ToString(RrConstantes.Tables[0].Rows[0]["valfanu2"]).Trim().ToUpper() == "S")
					{
						result = "ndnormal";
					}
					else
					{						
						if (!Convert.IsDBNull(RrConstantes.Tables[0].Rows[0]["valfanu1"]))
						{							
							if (Convert.ToString(RrConstantes.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
							{
								result = "nequival";
							}
							else
							{
								if (!Convert.IsDBNull(RrConstantes.Tables[0].Rows[0]["nnumeri1"]))
								{
									result = "nnumeri1";
								}
								else
								{
									result = "";
								}
							}
						}
					}
				}
				else
				{
					//si no esta relleno valfanu2
					
					if (!Convert.IsDBNull(RrConstantes.Tables[0].Rows[0]["valfanu1"]))
					{						
						if (Convert.ToString(RrConstantes.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
						{
							result = "nequival";
						}
						else
						{							
							if (!Convert.IsDBNull(RrConstantes.Tables[0].Rows[0]["nnumeri1"]))
							{
								result = "nnumeri1";
							}
							else
							{
								result = "";
							}
						}
					}
				}
			}
			else
			{
				result = "nequival";
			}
			
			RrConstantes.Close();

			return result;
		}

		internal static void CargarDatosPerfusion()
		{
			string sqlDatos = String.Empty;
			DataSet RrDatos = null;
			int iIndiceArray = 0;

			switch(iEstadoAccion)
			{
				case ALTA : 
					 
					FarmacosPantalla = new mbConfirmacionTomas.PerfusionSeleccion[1]; 
					 
					break;
				default:
					 
					//se tienen que mostrar los datos del suero seleccionado 
					//tenemos que recuperar los datos del suero seleccionado 
					 
					sqlDatos = "select DCODFARMVT.dnomfarm,DUNITOMAVA.dunitoma,EFARMACO.* "; 
					sqlDatos = sqlDatos + "from EFARMACO,DCODFARMVT,DUNITOMAVA "; 
					sqlDatos = sqlDatos + "where EFARMACO.itiposer ='" + GstTiposervicio + "' "; 
					sqlDatos = sqlDatos + "and EFARMACO.ganoregi = " + GstA�oFarmaco + " "; 
					sqlDatos = sqlDatos + "and EFARMACO.gnumregi = " + GstNumeroFarmaco + " "; 
					//comento la fecha del apunte para evitar que al suspender o finalizar automaticamente 
					//no se descabale el suero 
					sqlDatos = sqlDatos + "and EFARMACO.ncodperf = " + Convert.ToInt32(Double.Parse(GstCodigoFarmaco)).ToString() + " "; 
					sqlDatos = sqlDatos + "and DCODFARMVT.gfarmaco = EFARMACO.gfarmaco "; 
					sqlDatos = sqlDatos + "and EFARMACO.gunitoma = DUNITOMAVA.gunitoma "; 
					if (iEstadoAccion == NOCAMBIOS)
					{						
						object tempRefParam = GstFechaApunte;
						sqlDatos = sqlDatos + "and EFARMACO.fapuntes =" + Serrores.FormatFechaHMS(tempRefParam) + " ";
						GstFechaApunte = Convert.ToString(tempRefParam);
					}
					else
					{
						sqlDatos = sqlDatos + "and EFARMACO.ffinmovi is null ";
					} 
					sqlDatos = sqlDatos + "order by nordfarm "; 
					 
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlDatos, RcEnfermeria); 
					RrDatos = new DataSet(); 
					tempAdapter.Fill(RrDatos); 
					 
					if (RrDatos.Tables[0].Rows.Count != 0)
					{
						//se supone que vienen datos y los tenemos que cargar en pantalla						
						RrDatos.MoveFirst();
						FarmacosPantalla = new mbConfirmacionTomas.PerfusionSeleccion[1];

						iIndiceArray = 0;

						foreach (DataRow iteration_row in RrDatos.Tables[0].Rows)
						{
							iIndiceArray = FarmacosPantalla.GetUpperBound(0) + 1;
							FarmacosPantalla = ArraysHelper.RedimPreserve(FarmacosPantalla, new int[]{FarmacosPantalla.GetUpperBound(0) + 2});

							FarmacosPantalla[iIndiceArray].FechaApunte = Convert.ToDateTime(iteration_row["fapuntes"]).ToString("dd/MM/yyyy HH:NN:SS");

							FarmacosPantalla[iIndiceArray].FechaIni = Convert.ToDateTime(iteration_row["finitrat"]).ToString("dd/MM/yyyy HH:NN:SS");

							// Dependiendo de lo que este relleno se cargara con la fecha de suspension o con la de finalizacion
                            							
							if (!Convert.IsDBNull(iteration_row["fsuspens"]))
							{
								FarmacosPantalla[iIndiceArray].FechaSus = Convert.ToDateTime(iteration_row["fsuspens"]).ToString("dd/MM/yyyy HH:NN:SS");
							}
							else
							{
								FarmacosPantalla[iIndiceArray].FechaSus = "";
								
								if (!Convert.IsDBNull(iteration_row["ffintrat"]))
								{
									FarmacosPantalla[iIndiceArray].FechaFin = Convert.ToDateTime(iteration_row["ffintrat"]).ToString("dd/MM/yyyy HH:NN:SS");
								}
								else
								{
									FarmacosPantalla[iIndiceArray].FechaFin = "";
								}
							}
							//fecha fin de movimiento
							
							if (!Convert.IsDBNull(iteration_row["ffinmovi"]))
							{
								FarmacosPantalla[iIndiceArray].FechaFMovi = Convert.ToDateTime(iteration_row["ffinmovi"]).ToString("dd/MM/yyyy HH:NN:SS");
							}
							else
							{
								FarmacosPantalla[iIndiceArray].FechaFMovi = "";
							}
							//motivo de la suspension
							
							if (!Convert.IsDBNull(iteration_row["gmsuspro"]))
							{
								FarmacosPantalla[iIndiceArray].CodMotivo = Conversion.Str(iteration_row["gmsuspro"]);
							}
							else
							{
								FarmacosPantalla[iIndiceArray].CodMotivo = "";
							}
							//medico
							
							if (Convert.IsDBNull(iteration_row["gpersona"]))
							{
								FarmacosPantalla[iIndiceArray].CodMedico = "";
							}
							else
							{
								FarmacosPantalla[iIndiceArray].CodMedico = Convert.ToString(iteration_row["gpersona"]);
							}
							//via de administracion
							FarmacosPantalla[iIndiceArray].CodViaadm = Convert.ToString(iteration_row["gviadmin"]);
							//estado del farmaco
							FarmacosPantalla[iIndiceArray].Estado = Convert.ToString(iteration_row["iesttrat"]);
							//duracion en dias
							
							if (Convert.IsDBNull(iteration_row["cdiastra"]))
							{
								FarmacosPantalla[iIndiceArray].Duracion = "";
							}
							else
							{
								FarmacosPantalla[iIndiceArray].Duracion = Convert.ToString(iteration_row["cdiastra"]);
							}
							//si el farmaco es si precisa se cargara la frecuencia de la toma y no las horas/dosis
							FarmacosPantalla[iIndiceArray].VsDias = Convert.ToString(iteration_row["vdiasfre"]).Trim();
							//observaciones generales del suero
							
							if (Convert.IsDBNull(iteration_row["oobserva"]))
							{
								FarmacosPantalla[iIndiceArray].Observa = "";
							}
							else
							{
								//hay que quitar de las observaciones las particulares
								string tempRefParam2 = Convert.ToString(iteration_row["oobserva"]).Trim();
								string tempRefParam3 = "*";
								FarmacosPantalla[iIndiceArray].Observa = LimpiaCampo(ref tempRefParam2, ref tempRefParam3).Trim();
							}
							//fecha prevista
							
							if (Convert.IsDBNull(iteration_row["fprevist"]))
							{
								FarmacosPantalla[iIndiceArray].FechaPrevista = "";
							}
							else
							{
								FarmacosPantalla[iIndiceArray].FechaPrevista = Convert.ToDateTime(iteration_row["fprevist"]).ToString("dd/MM/yyyy HH:NN:SS");
							}
							//estado previsto
							
							if (Convert.IsDBNull(iteration_row["iestprev"]))
							{
								FarmacosPantalla[iIndiceArray].EstPrevisto = "";
							}
							else
							{
								FarmacosPantalla[iIndiceArray].EstPrevisto = Convert.ToString(iteration_row["iestprev"]);
							}
							//velocidad de perfusion
							
							if (Convert.IsDBNull(iteration_row["veloperf"]))
							{
								FarmacosPantalla[iIndiceArray].veloperf = "";
							}
							else
							{
								FarmacosPantalla[iIndiceArray].veloperf = Convert.ToString(iteration_row["veloperf"]).Trim();
							}
							//Datos de cada uno de los farmacos que forman parte del suero
							FarmacosPantalla[iIndiceArray].CodFarmaco = Convert.ToString(iteration_row["gfarmaco"]);
							FarmacosPantalla[iIndiceArray].ncodperf = (short) iteration_row["ncodperf"];
							FarmacosPantalla[iIndiceArray].nordfarm = (short) iteration_row["nordfarm"];

							if (Convert.ToInt32(iteration_row["nordfarm"]) == 0)
							{
								//Datos de la disolucion
								//nombre del farmaco

								FarmacosPantalla[iIndiceArray].NombreFarmaco = Convert.ToString(iteration_row["dnomfarm"]).Trim();
								for (int aik = 0; aik <= 23; aik++)
								{									
									if (!Convert.IsDBNull(iteration_row["cdosisfa" + StringsHelper.Format(aik, "00") + ""]))
									{
										string tempRefParam4 = Convert.ToString(iteration_row["cdosisfa" + StringsHelper.Format(aik, "00") + ""]);
										FarmacosPantalla[iIndiceArray].Dosis = Serrores.ConvertirDecimales(ref tempRefParam4, ref vstSeparadorDecimal, 2);
										break;
									}
								}
								FarmacosPantalla[iIndiceArray].DesTomas = Convert.ToString(iteration_row["dunitoma"]).Trim();
								FarmacosPantalla[iIndiceArray].CodTomas = Convert.ToString(iteration_row["gunitoma"]);
								FarmacosPantalla[iIndiceArray].SiPrecisa = "N";
								FarmacosPantalla[iIndiceArray].oobsperf = "";
							}
							else
							{
								//Datos de los f�rmacos
								FarmacosPantalla[iIndiceArray].NombreFarmaco = Convert.ToString(iteration_row["dnomfarm"]).Trim();
								for (int aik = 0; aik <= 23; aik++)
								{									
									if (!Convert.IsDBNull(iteration_row["cdosisfa" + StringsHelper.Format(aik, "00") + ""]))
									{
										string tempRefParam5 = Convert.ToString(iteration_row["cdosisfa" + StringsHelper.Format(aik, "00") + ""]);
										FarmacosPantalla[iIndiceArray].Dosis = Serrores.ConvertirDecimales(ref tempRefParam5, ref vstSeparadorDecimal, 2);
										break;
									}
								}
								FarmacosPantalla[iIndiceArray].DesTomas = Convert.ToString(iteration_row["dunitoma"]).Trim();
								//(maplaza)(18/08/2006)Si el campo RrDatos("iprecisa") es NULO, se produce un error
								//FarmacosPantalla(iIndiceArray).SiPrecisa = Trim(RrDatos("iprecisa"))								
								if (Convert.IsDBNull(iteration_row["iprecisa"]))
								{
									FarmacosPantalla[iIndiceArray].SiPrecisa = "N";
								}
								else
								{
									FarmacosPantalla[iIndiceArray].SiPrecisa = Convert.ToString(iteration_row["iprecisa"]).Trim();
								}
								//-----
								FarmacosPantalla[iIndiceArray].CodTomas = Convert.ToString(iteration_row["gunitoma"]);
							}
						}
					} 
					 
					break;
			}

		}

		internal static string LimpiaCampo(ref string stCadena, ref string stCaracter)
		{
			//Funcion que limpia de un determinado caracter un string
			string result = String.Empty;
			string stCadenaW1 = String.Empty;
			int iPosicion = 0;

			result = "";
			for (int aik = 0; aik <= stCadena.Length; aik++)
			{
				stCadenaW1 = stCadena;
				iPosicion = (stCadenaW1.IndexOf(stCaracter) + 1);
				if (iPosicion == 0)
				{
					return stCadenaW1;
				}
				stCadena = stCadenaW1.Substring(iPosicion);
				aik = iPosicion;
			}

			return result;
		}
	}
}