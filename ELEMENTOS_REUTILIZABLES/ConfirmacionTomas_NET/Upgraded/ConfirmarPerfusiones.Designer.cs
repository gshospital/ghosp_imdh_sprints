using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ConfirmacionTomas
{
    partial class ConfirmarPerfusiones
    {

        #region "Upgrade Support "
        private static ConfirmarPerfusiones m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static ConfirmarPerfusiones DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new ConfirmarPerfusiones();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "SprFarmacos", "Frame2", "cbAnular", "cbNuevo", "sdcFecha", "tbHora", "Frame4", "tbObservaciones", "cbCancelar", "cbAceptar", "lbObservaciones", "Frame1", "cbSalir", "tbVelocidad", "listFarmacos", "Label1", "Label5", "Label4", "lbduracion", "lbDisolucion", "frmFarmacos", "listBoxHelper1", "SprFarmacos_Sheet1" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread SprFarmacos;
        public Telerik.WinControls.UI.RadGroupBox Frame2;
        public Telerik.WinControls.UI.RadButton cbAnular;
        public Telerik.WinControls.UI.RadButton cbNuevo;
        public Telerik.WinControls.UI.RadDateTimePicker sdcFecha;
        public Telerik.WinControls.UI.RadMaskedEditBox tbHora;
        public Telerik.WinControls.UI.RadGroupBox Frame4;
        public Telerik.WinControls.UI.RadTextBox tbObservaciones;
        public Telerik.WinControls.UI.RadButton cbCancelar;
        public Telerik.WinControls.UI.RadButton cbAceptar;
        public Telerik.WinControls.UI.RadLabel lbObservaciones;
        public Telerik.WinControls.UI.RadGroupBox Frame1;
        public Telerik.WinControls.UI.RadButton cbSalir;
        public Telerik.WinControls.UI.RadTextBox tbVelocidad;
        public UpgradeHelpers.MSForms.MSListBox listFarmacos;
        public Telerik.WinControls.UI.RadLabel Label1;
        public Telerik.WinControls.UI.RadLabel Label5;
        public Telerik.WinControls.UI.RadLabel Label4;
        public Telerik.WinControls.UI.RadLabel lbduracion;
        public Telerik.WinControls.UI.RadLabel lbDisolucion;
        public Telerik.WinControls.UI.RadGroupBox frmFarmacos;
        private UpgradeHelpers.Gui.ListBoxHelper listBoxHelper1;
        //private FarPoint.Win.Spread.SheetView SprFarmacos_Sheet1 = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmarPerfusiones));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.SprFarmacos = new UpgradeHelpers.Spread.FpSpread();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbAnular = new Telerik.WinControls.UI.RadButton();
            this.cbNuevo = new Telerik.WinControls.UI.RadButton();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcFecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbHora = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbObservaciones = new Telerik.WinControls.UI.RadTextBox();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.lbObservaciones = new Telerik.WinControls.UI.RadLabel();
            this.cbSalir = new Telerik.WinControls.UI.RadButton();
            this.frmFarmacos = new Telerik.WinControls.UI.RadGroupBox();
            this.tbVelocidad = new Telerik.WinControls.UI.RadTextBox();
            this.listFarmacos = new UpgradeHelpers.MSForms.MSListBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.lbduracion = new Telerik.WinControls.UI.RadLabel();
            this.lbDisolucion = new Telerik.WinControls.UI.RadLabel();
            this.Frame2.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.Frame4.SuspendLayout();
            this.frmFarmacos.SuspendLayout();
            this.SuspendLayout();
            this.listBoxHelper1 = new UpgradeHelpers.Gui.ListBoxHelper(this.components);
            // 
            // Frame2
            // 
            //this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            this.Frame2.Controls.Add(this.SprFarmacos);
            this.Frame2.Enabled = true;
            //this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame2.Location = new System.Drawing.Point(6, 102);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(699, 141);
            this.Frame2.TabIndex = 9;
            this.Frame2.Text = "Tomas confirmadas";
            this.Frame2.Visible = true;
            // 
            // SprFarmacos
            // 
            this.SprFarmacos.Location = new System.Drawing.Point(12, 18);
            this.SprFarmacos.Name = "SprFarmacos";
            this.SprFarmacos.Size = new System.Drawing.Size(673, 115);
            this.SprFarmacos.TabIndex = 10;
            this.SprFarmacos.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(SprFarmacos_CellClick);
            this.SprFarmacos.ColumnWidthChanged += new System.Windows.Forms.ColumnWidthChangedEventHandler(SprFarmacos_ColumnWidthChanged);
            this.SprFarmacos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SprFarmacos_KeyDown);
            // 
            // Frame1
            // 
            //this.Frame1.BackColor = System.Drawing.SystemColors.Control;
            this.Frame1.Controls.Add(this.cbAnular);
            this.Frame1.Controls.Add(this.cbNuevo);
            this.Frame1.Controls.Add(this.Frame4);
            this.Frame1.Controls.Add(this.tbObservaciones);
            this.Frame1.Controls.Add(this.cbCancelar);
            this.Frame1.Controls.Add(this.cbAceptar);
            this.Frame1.Controls.Add(this.lbObservaciones);
            this.Frame1.Enabled = true;
            //this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame1.Location = new System.Drawing.Point(6, 244);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(699, 95);
            this.Frame1.TabIndex = 8;
            this.Frame1.Visible = true;
            // 
            // cbAnular
            // 
            //this.cbAnular.BackColor = System.Drawing.SystemColors.Control;
            this.cbAnular.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAnular.Enabled = false;
            //this.cbAnular.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbAnular.Location = new System.Drawing.Point(344, 58);
            this.cbAnular.Name = "cbAnular";
            this.cbAnular.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAnular.Size = new System.Drawing.Size(81, 28);
            this.cbAnular.TabIndex = 18;
            this.cbAnular.Text = "Anu&lar";
            this.cbAnular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAnular.Click += new System.EventHandler(this.cbAnular_Click);
            // 
            // cbNuevo
            // 
            //this.cbNuevo.BackColor = System.Drawing.SystemColors.Control;
            this.cbNuevo.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbNuevo.Enabled = false;
            //this.cbNuevo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbNuevo.Location = new System.Drawing.Point(432, 58);
            this.cbNuevo.Name = "cbNuevo";
            this.cbNuevo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbNuevo.Size = new System.Drawing.Size(81, 28);
            this.cbNuevo.TabIndex = 4;
            this.cbNuevo.Text = "&Nuevo";
            this.cbNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbNuevo.Click += new System.EventHandler(this.cbNuevo_Click);
            // 
            // Frame4
            // 
            //this.Frame4.BackColor = System.Drawing.SystemColors.Control;
            this.Frame4.Controls.Add(this.sdcFecha);
            this.Frame4.Controls.Add(this.tbHora);
            this.Frame4.Enabled = true;
            //this.Frame4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame4.Location = new System.Drawing.Point(6, 10);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(181, 43);
            this.Frame4.TabIndex = 12;
            this.Frame4.Text = "Fecha/Hora real de toma";
            this.Frame4.Visible = true;
            // 
            // sdcFecha
            // 
            //this.sdcFecha.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.sdcFecha.CustomFormat = "DD/MM/YYYY";
            this.sdcFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFecha.Location = new System.Drawing.Point(6, 16);
            this.sdcFecha.Name = "sdcFecha";
            this.sdcFecha.Size = new System.Drawing.Size(117, 21);
            this.sdcFecha.TabIndex = 1;
            // 
            // tbHora
            // 
            this.tbHora.AllowPromptAsInput = false;
            this.tbHora.Location = new System.Drawing.Point(130, 16);
            this.tbHora.Mask = "##:##";
            this.tbHora.Name = "tbHora";
            this.tbHora.PromptChar = ' ';
            this.tbHora.Size = new System.Drawing.Size(42, 21);
            this.tbHora.TabIndex = 2;
            this.tbHora.Enter += new System.EventHandler(this.tbHora_Enter);
            this.tbHora.Leave += new System.EventHandler(this.tbHora_Leave);
            // 
            // tbObservaciones
            // 
            this.tbObservaciones.AcceptsReturn = true;
            //this.tbObservaciones.BackColor = System.Drawing.SystemColors.Window;
            //this.tbObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam;
            //this.tbObservaciones.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbObservaciones.Location = new System.Drawing.Point(280, 22);
            this.tbObservaciones.MaxLength = 60;
            this.tbObservaciones.Name = "tbObservaciones";
            this.tbObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbObservaciones.Size = new System.Drawing.Size(409, 23);
            this.tbObservaciones.TabIndex = 3;
            this.tbObservaciones.Enter += new System.EventHandler(this.tbObservaciones_Enter);
            // 
            // cbCancelar
            // 
            //this.cbCancelar.BackColor = System.Drawing.SystemColors.Control;
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            //this.cbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbCancelar.Location = new System.Drawing.Point(610, 58);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 28);
            this.cbCancelar.TabIndex = 6;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this.cbCancelar.UseVisualStyleBackColor = false;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            //this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Enabled = false;
            //this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbAceptar.Location = new System.Drawing.Point(522, 58);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 28);
            this.cbAceptar.TabIndex = 5;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // lbObservaciones
            // 
            //this.lbObservaciones.BackColor = System.Drawing.SystemColors.Control;
            //this.lbObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbObservaciones.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbObservaciones.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbObservaciones.Location = new System.Drawing.Point(196, 26);
            this.lbObservaciones.Name = "lbObservaciones";
            this.lbObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbObservaciones.Size = new System.Drawing.Size(87, 18);
            this.lbObservaciones.TabIndex = 11;
            this.lbObservaciones.Text = "Observaciones:";
            // 
            // cbSalir
            // 
            //this.cbSalir.BackColor = System.Drawing.SystemColors.Control;
            this.cbSalir.Cursor = System.Windows.Forms.Cursors.Default;
            //this.cbSalir.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbSalir.Location = new System.Drawing.Point(622, 344);
            this.cbSalir.Name = "cbSalir";
            this.cbSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbSalir.Size = new System.Drawing.Size(83, 28);
            this.cbSalir.TabIndex = 7;
            this.cbSalir.Text = "&Cerrar";
            this.cbSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this.cbSalir.UseVisualStyleBackColor = false;
            this.cbSalir.Click += new System.EventHandler(this.cbSalir_Click);
            // 
            // frmFarmacos
            // 
            //this.frmFarmacos.BackColor = System.Drawing.SystemColors.Control;
            this.frmFarmacos.Controls.Add(this.tbVelocidad);
            this.frmFarmacos.Controls.Add(this.listFarmacos);
            this.frmFarmacos.Controls.Add(this.Label1);
            this.frmFarmacos.Controls.Add(this.Label5);
            this.frmFarmacos.Controls.Add(this.Label4);
            this.frmFarmacos.Controls.Add(this.lbduracion);
            this.frmFarmacos.Controls.Add(this.lbDisolucion);
            this.frmFarmacos.Enabled = true;
            //this.frmFarmacos.ForeColor = System.Drawing.SystemColors.ControlText;
            this.frmFarmacos.Location = new System.Drawing.Point(6, 4);
            this.frmFarmacos.Name = "frmFarmacos";
            this.frmFarmacos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmFarmacos.Size = new System.Drawing.Size(699, 95);
            this.frmFarmacos.TabIndex = 0;
            this.frmFarmacos.Text = "Perfusi�n";
            this.frmFarmacos.Visible = true;
            // 
            // tbVelocidad
            // 
            this.tbVelocidad.AcceptsReturn = true;
            //this.tbVelocidad.BackColor = System.Drawing.SystemColors.Window;
            //this.tbVelocidad.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbVelocidad.Cursor = System.Windows.Forms.Cursors.IBeam;
            //this.tbVelocidad.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbVelocidad.Location = new System.Drawing.Point(604, 56);
            this.tbVelocidad.MaxLength = 9;
            this.tbVelocidad.Name = "tbVelocidad";
            this.tbVelocidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbVelocidad.Size = new System.Drawing.Size(51, 19);
            this.tbVelocidad.TabIndex = 19;
            this.tbVelocidad.Enter += new System.EventHandler(this.tbVelocidad_Enter);
            this.tbVelocidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbVelocidad_KeyPress);
            // 
            // listFarmacos
            // 
            //this.listFarmacos.BackColor = System.Drawing.SystemColors.Window;
            //this.listFarmacos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.listFarmacos.CausesValidation = true;
            //this.listFarmacos.ColumnWidth = this.listFarmacos.ClientSize.Width;
            this.listFarmacos.Cursor = System.Windows.Forms.Cursors.Default;
            this.listFarmacos.Enabled = true;
            this.listFarmacos.ForeColor = System.Drawing.SystemColors.WindowText;
            //this.listFarmacos.IntegralHeight = true;
            this.listFarmacos.Location = new System.Drawing.Point(80, 44);
            this.listFarmacos.Name = "listFarmacos";
            this.listFarmacos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.listFarmacos.Size = new System.Drawing.Size(405, 46);
            //this.listFarmacos.Sorted = false;
            this.listFarmacos.TabIndex = 17;
            this.listFarmacos.TabStop = true;
            this.listFarmacos.Visible = true;
            // 
            // Label1
            // 
            //this.Label1.BackColor = System.Drawing.SystemColors.Control;
            //this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            //this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(656, 58);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(39, 18);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "ml/hora";
            // 
            // Label5
            // 
            //this.Label5.BackColor = System.Drawing.SystemColors.Control;
            //this.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            //this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label5.Location = new System.Drawing.Point(490, 58);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(117, 18);
            this.Label5.TabIndex = 16;
            this.Label5.Text = "Velocidad de perfusi�n:";
            // 
            // Label4
            // 
            //this.Label4.BackColor = System.Drawing.SystemColors.Control;
            //this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            //this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label4.Location = new System.Drawing.Point(14, 38);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(65, 22);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "F�rmacos:";
            // 
            // lbduracion
            // 
            //this.lbduracion.BackColor = System.Drawing.SystemColors.Control;
            //this.lbduracion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbduracion.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbduracion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbduracion.Location = new System.Drawing.Point(14, 18);
            this.lbduracion.Name = "lbduracion";
            this.lbduracion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbduracion.Size = new System.Drawing.Size(61, 20);
            this.lbduracion.TabIndex = 14;
            this.lbduracion.Text = "Disoluci�n:";
            // 
            // lbDisolucion
            // 
            //this.lbDisolucion.BackColor = System.Drawing.SystemColors.Window;
            //this.lbDisolucion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbDisolucion.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbDisolucion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbDisolucion.Location = new System.Drawing.Point(80, 16);
            this.lbDisolucion.Name = "lbDisolucion";
            this.lbDisolucion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDisolucion.Size = new System.Drawing.Size(405, 20);
            this.lbDisolucion.TabIndex = 13;
            // 
            // ConfirmarPerfusiones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(712, 380);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbSalir);
            this.Controls.Add(this.frmFarmacos);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfirmarPerfusiones";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Confirmaci�n de tomas - EGI215F4";
            //listBoxHelper1.SetSelectionMode(this.listFarmacos, System.Windows.Forms.SelectionMode.One);
            this.Activated += new System.EventHandler(this.ConfirmarPerfusiones_Activated);
            this.Closed += new System.EventHandler(this.ConfirmarPerfusiones_Closed);
            this.Load += new System.EventHandler(this.ConfirmarPerfusiones_Load);
            this.Frame2.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.Frame4.ResumeLayout(false);
            this.frmFarmacos.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        #endregion
    }
}