using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ConfirmacionTomas
{
    partial class ConfirmarFarmacos
    {

        #region "Upgrade Support "
        private static ConfirmarFarmacos m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static ConfirmarFarmacos DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new ConfirmarFarmacos();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "SprFarmacos", "Frame2", "cbAnular", "tbDosis", "cbNuevo", "sdcFecha", "tbHora", "Frame4", "SdcFechaPrevista", "tbHoraPrevista", "Frame3", "tbObservaciones", "cbCancelar", "cbAceptar", "CbbFarmacoAdm", "lbFarmacoAdm", "lbfarmacoEqui", "lbObservaciones", "lbUnidad", "lbDDosis", "Frame1", "cbSalir", "lbNombreFarmaco", "frmFarmacos", "SprFarmacos_Sheet1" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread SprFarmacos;
        public Telerik.WinControls.UI.RadGroupBox Frame2;
        public Telerik.WinControls.UI.RadButton cbAnular;
        public Telerik.WinControls.UI.RadTextBox tbDosis;
        public Telerik.WinControls.UI.RadButton cbNuevo;
        public Telerik.WinControls.UI.RadDateTimePicker sdcFecha;
        public Telerik.WinControls.UI.RadMaskedEditBox tbHora;
        public Telerik.WinControls.UI.RadGroupBox Frame4;
        public Telerik.WinControls.UI.RadDateTimePicker SdcFechaPrevista;
        public Telerik.WinControls.UI.RadMaskedEditBox tbHoraPrevista;
        public Telerik.WinControls.UI.RadGroupBox Frame3;
        public Telerik.WinControls.UI.RadTextBox tbObservaciones;
        public Telerik.WinControls.UI.RadButton cbCancelar;
        public Telerik.WinControls.UI.RadButton cbAceptar;
        public UpgradeHelpers.MSForms.MSCombobox CbbFarmacoAdm;
        public Telerik.WinControls.UI.RadLabel lbFarmacoAdm;
        public Telerik.WinControls.UI.RadLabel lbfarmacoEqui;
        public Telerik.WinControls.UI.RadLabel lbObservaciones;
        public Telerik.WinControls.UI.RadLabel lbUnidad;
        public Telerik.WinControls.UI.RadLabel lbDDosis;
        public Telerik.WinControls.UI.RadGroupBox Frame1;
        public Telerik.WinControls.UI.RadButton cbSalir;
        public Telerik.WinControls.UI.RadLabel lbNombreFarmaco;
        public Telerik.WinControls.UI.RadGroupBox frmFarmacos;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmarFarmacos));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame2 = new Telerik.WinControls.UI.RadGroupBox();
            this.SprFarmacos = new UpgradeHelpers.Spread.FpSpread();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbAnular = new Telerik.WinControls.UI.RadButton();
            this.tbDosis = new Telerik.WinControls.UI.RadTextBox();
            this.cbNuevo = new Telerik.WinControls.UI.RadButton();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcFecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbHora = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.SdcFechaPrevista = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbHoraPrevista = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbObservaciones = new Telerik.WinControls.UI.RadTextBox();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.CbbFarmacoAdm = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.lbFarmacoAdm = new Telerik.WinControls.UI.RadLabel();
            this.lbfarmacoEqui = new Telerik.WinControls.UI.RadLabel();
            this.lbObservaciones = new Telerik.WinControls.UI.RadLabel();
            this.lbUnidad = new Telerik.WinControls.UI.RadLabel();
            this.lbDDosis = new Telerik.WinControls.UI.RadLabel();
            this.cbSalir = new Telerik.WinControls.UI.RadButton();
            this.frmFarmacos = new Telerik.WinControls.UI.RadGroupBox();
            this.lbNombreFarmaco = new Telerik.WinControls.UI.RadLabel();
            this.Frame2.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.Frame4.SuspendLayout();
            this.Frame3.SuspendLayout();
            this.frmFarmacos.SuspendLayout();
            this.SuspendLayout();
            // 
            // Frame2
            // 
            //this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            this.Frame2.Controls.Add(this.SprFarmacos);
            this.Frame2.Enabled = true;
            //this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame2.Location = new System.Drawing.Point(6, 58);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(621, 141);
            this.Frame2.TabIndex = 15;
            this.Frame2.Text = "Tomas realizadas ";
            this.Frame2.Visible = true;
            // 
            // SprFarmacos
            // 
            this.SprFarmacos.Location = new System.Drawing.Point(8, 18);
            this.SprFarmacos.Name = "SprFarmacos";
            this.SprFarmacos.Size = new System.Drawing.Size(605, 115);
            this.SprFarmacos.TabIndex = 16;
            this.SprFarmacos.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(SprFarmacos_CellClick);
            this.SprFarmacos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SprFarmacos_KeyDown);
            // 
            // Frame1
            // 
            //this.Frame1.BackColor = System.Drawing.SystemColors.Control;
            this.Frame1.Controls.Add(this.cbAnular);
            this.Frame1.Controls.Add(this.tbDosis);
            this.Frame1.Controls.Add(this.cbNuevo);
            this.Frame1.Controls.Add(this.Frame4);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.Controls.Add(this.tbObservaciones);
            this.Frame1.Controls.Add(this.cbCancelar);
            this.Frame1.Controls.Add(this.cbAceptar);
            this.Frame1.Controls.Add(this.CbbFarmacoAdm);
            this.Frame1.Controls.Add(this.lbFarmacoAdm);
            this.Frame1.Controls.Add(this.lbfarmacoEqui);
            this.Frame1.Controls.Add(this.lbObservaciones);
            this.Frame1.Controls.Add(this.lbUnidad);
            this.Frame1.Controls.Add(this.lbDDosis);
            this.Frame1.Enabled = true;
            //this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame1.Location = new System.Drawing.Point(6, 204);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(531, 203);
            this.Frame1.TabIndex = 12;
            this.Frame1.Text = "Toma a confirmar ";
            this.Frame1.Visible = true;
            // 
            // cbAnular
            // 
            //this.cbAnular.BackColor = System.Drawing.SystemColors.Control;
            this.cbAnular.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAnular.Enabled = false;
            //this.cbAnular.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbAnular.Location = new System.Drawing.Point(170, 164);
            this.cbAnular.Name = "cbAnular";
            this.cbAnular.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAnular.Size = new System.Drawing.Size(81, 28);
            this.cbAnular.TabIndex = 22;
            this.cbAnular.Text = "Anu&lar";
            this.cbAnular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAnular.Click += new System.EventHandler(this.cbAnular_Click);
            // 
            // tbDosis
            // 
            this.tbDosis.AcceptsReturn = true;
            //this.tbDosis.BackColor = System.Drawing.SystemColors.Window;
            //this.tbDosis.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbDosis.Cursor = System.Windows.Forms.Cursors.IBeam;
            //this.tbDosis.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbDosis.Location = new System.Drawing.Point(150, 48);
            this.tbDosis.MaxLength = 0;
            this.tbDosis.Name = "tbDosis";
            this.tbDosis.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDosis.Size = new System.Drawing.Size(77, 19);
            this.tbDosis.TabIndex = 2;
            this.tbDosis.Enter += new System.EventHandler(this.tbDosis_Enter);
            this.tbDosis.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbDosis_KeyDown);
            this.tbDosis.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDosis_KeyPress);
            this.tbDosis.Leave += new System.EventHandler(this.tbDosis_Leave);
            // 
            // cbNuevo
            // 
            //this.cbNuevo.BackColor = System.Drawing.SystemColors.Control;
            this.cbNuevo.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbNuevo.Enabled = false;
            //this.cbNuevo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbNuevo.Location = new System.Drawing.Point(260, 164);
            this.cbNuevo.Name = "cbNuevo";
            this.cbNuevo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbNuevo.Size = new System.Drawing.Size(81, 28);
            this.cbNuevo.TabIndex = 8;
            this.cbNuevo.Text = "&Nuevo";
            this.cbNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbNuevo.Click += new System.EventHandler(this.cbNuevo_Click);
            // 
            // Frame4
            // 
            //this.Frame4.BackColor = System.Drawing.SystemColors.Control;
            this.Frame4.Controls.Add(this.sdcFecha);
            this.Frame4.Controls.Add(this.tbHora);
            this.Frame4.Enabled = true;
            //this.Frame4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame4.Location = new System.Drawing.Point(218, 74);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(187, 47);
            this.Frame4.TabIndex = 19;
            this.Frame4.Text = "Fecha/Hora real de toma";
            this.Frame4.Visible = true;
            // 
            // sdcFecha
            // 
            //this.sdcFecha.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.sdcFecha.CustomFormat = "DD/MM/YYYY";
            this.sdcFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFecha.Location = new System.Drawing.Point(8, 18);
            this.sdcFecha.Name = "sdcFecha";
            this.sdcFecha.Size = new System.Drawing.Size(117, 21);
            this.sdcFecha.TabIndex = 5;
            // 
            // tbHora
            // 
            this.tbHora.AllowPromptAsInput = false;
            this.tbHora.Location = new System.Drawing.Point(132, 18);
            this.tbHora.Mask = "##:##";
            this.tbHora.Name = "tbHora";
            this.tbHora.PromptChar = ' ';
            this.tbHora.Size = new System.Drawing.Size(42, 21);
            this.tbHora.TabIndex = 6;
            this.tbHora.Enter += new System.EventHandler(this.tbHora_Enter);
            this.tbHora.Leave += new System.EventHandler(this.tbHora_Leave);
            // 
            // Frame3
            // 
            //this.Frame3.BackColor = System.Drawing.SystemColors.Control;
            this.Frame3.Controls.Add(this.SdcFechaPrevista);
            this.Frame3.Controls.Add(this.tbHoraPrevista);
            this.Frame3.Enabled = true;
            //this.Frame3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame3.Location = new System.Drawing.Point(16, 74);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(187, 47);
            this.Frame3.TabIndex = 18;
            this.Frame3.Text = "Fecha/Hora prevista de toma";
            this.Frame3.Visible = true;
            // 
            // SdcFechaPrevista
            // 
            //this.SdcFechaPrevista.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            this.SdcFechaPrevista.CustomFormat = "DD/MM/YYYY";
            this.SdcFechaPrevista.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SdcFechaPrevista.Location = new System.Drawing.Point(10, 18);
            this.SdcFechaPrevista.Name = "SdcFechaPrevista";
            this.SdcFechaPrevista.Size = new System.Drawing.Size(117, 21);
            this.SdcFechaPrevista.TabIndex = 3;
            // 
            // tbHoraPrevista
            // 
            this.tbHoraPrevista.AllowPromptAsInput = false;
            this.tbHoraPrevista.Location = new System.Drawing.Point(134, 18);
            this.tbHoraPrevista.Mask = "##:##";
            this.tbHoraPrevista.Name = "tbHoraPrevista";
            this.tbHoraPrevista.PromptChar = ' ';
            this.tbHoraPrevista.Size = new System.Drawing.Size(42, 21);
            this.tbHoraPrevista.TabIndex = 4;
            this.tbHoraPrevista.Enter += new System.EventHandler(this.tbHoraPrevista_Enter);
            this.tbHoraPrevista.Leave += new System.EventHandler(this.tbHoraPrevista_Leave);
            // 
            // tbObservaciones
            // 
            this.tbObservaciones.AcceptsReturn = true;
            //this.tbObservaciones.BackColor = System.Drawing.SystemColors.Window;
            //this.tbObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam;
            //this.tbObservaciones.ForeColor = System.Drawing.SystemColors.WindowText;
            this.tbObservaciones.Location = new System.Drawing.Point(96, 132);
            this.tbObservaciones.MaxLength = 60;
            this.tbObservaciones.Name = "tbObservaciones";
            this.tbObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbObservaciones.Size = new System.Drawing.Size(421, 21);
            this.tbObservaciones.TabIndex = 7;
            this.tbObservaciones.Text = " ";
            this.tbObservaciones.Enter += new System.EventHandler(this.tbObservaciones_Enter);
            this.tbObservaciones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbObservaciones_KeyPress);
            // 
            // cbCancelar
            // 
            //this.cbCancelar.BackColor = System.Drawing.SystemColors.Control;
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            //this.cbCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbCancelar.Location = new System.Drawing.Point(438, 164);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 28);
            this.cbCancelar.TabIndex = 10;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this.cbCancelar.UseVisualStyleBackColor = false;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            //this.cbAceptar.BackColor = System.Drawing.SystemColors.Control;
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Enabled = false;
            //this.cbAceptar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbAceptar.Location = new System.Drawing.Point(350, 164);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 28);
            this.cbAceptar.TabIndex = 9;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // CbbFarmacoAdm
            // 
            this.CbbFarmacoAdm.Location = new System.Drawing.Point(150, 18);
            this.CbbFarmacoAdm.Name = "CbbFarmacoAdm";
            this.CbbFarmacoAdm.Size = new System.Drawing.Size(360, 21);
            this.CbbFarmacoAdm.TabIndex = 23;
            this.CbbFarmacoAdm.Enabled = true;
            // 
            // lbFarmacoAdm
            // 
            //this.lbFarmacoAdm.BackColor = System.Drawing.Color.White;
            //this.lbFarmacoAdm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbFarmacoAdm.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbFarmacoAdm.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbFarmacoAdm.Location = new System.Drawing.Point(128, 18);
            this.lbFarmacoAdm.Name = "lbFarmacoAdm";
            this.lbFarmacoAdm.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbFarmacoAdm.Size = new System.Drawing.Size(391, 20);
            this.lbFarmacoAdm.TabIndex = 21;
            // 
            // lbfarmacoEqui
            // 
            //this.lbfarmacoEqui.BackColor = System.Drawing.SystemColors.Control;
            //this.lbfarmacoEqui.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbfarmacoEqui.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbfarmacoEqui.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbfarmacoEqui.Location = new System.Drawing.Point(16, 22);
            this.lbfarmacoEqui.Name = "lbfarmacoEqui";
            this.lbfarmacoEqui.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbfarmacoEqui.Size = new System.Drawing.Size(115, 20);
            this.lbfarmacoEqui.TabIndex = 20;
            this.lbfarmacoEqui.Text = "F�rmaco administrado:";
            // 
            // lbObservaciones
            // 
            //this.lbObservaciones.BackColor = System.Drawing.SystemColors.Control;
            //this.lbObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbObservaciones.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbObservaciones.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbObservaciones.Location = new System.Drawing.Point(16, 134);
            this.lbObservaciones.Name = "lbObservaciones";
            this.lbObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbObservaciones.Size = new System.Drawing.Size(93, 18);
            this.lbObservaciones.TabIndex = 17;
            this.lbObservaciones.Text = "Observaciones:";
            // 
            // lbUnidad
            // 
            //this.lbUnidad.BackColor = System.Drawing.SystemColors.Control;
            //this.lbUnidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbUnidad.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbUnidad.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbUnidad.Location = new System.Drawing.Point(238, 52);
            this.lbUnidad.Name = "lbUnidad";
            this.lbUnidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbUnidad.Size = new System.Drawing.Size(267, 18);
            this.lbUnidad.TabIndex = 14;
            this.lbUnidad.Text = "Comprimidos";
            // 
            // lbDDosis
            // 
            //this.lbDDosis.BackColor = System.Drawing.SystemColors.Control;
            //this.lbDDosis.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbDDosis.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbDDosis.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbDDosis.Location = new System.Drawing.Point(16, 48);
            this.lbDDosis.Name = "lbDDosis";
            this.lbDDosis.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDDosis.Size = new System.Drawing.Size(105, 20);
            this.lbDDosis.TabIndex = 13;
            this.lbDDosis.Text = "Dosis suministrada:";
            // 
            // cbSalir
            // 
            //this.cbSalir.BackColor = System.Drawing.SystemColors.Control;
            this.cbSalir.Cursor = System.Windows.Forms.Cursors.Default;
            //this.cbSalir.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbSalir.Location = new System.Drawing.Point(544, 378);
            this.cbSalir.Name = "cbSalir";
            this.cbSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbSalir.Size = new System.Drawing.Size(83, 28);
            this.cbSalir.TabIndex = 11;
            this.cbSalir.Text = "&Cerrar";
            this.cbSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            //this.cbSalir.UseVisualStyleBackColor = false;
            this.cbSalir.Click += new System.EventHandler(this.cbSalir_Click);
            // 
            // frmFarmacos
            // 
            //this.frmFarmacos.BackColor = System.Drawing.SystemColors.Control;
            this.frmFarmacos.Controls.Add(this.lbNombreFarmaco);
            this.frmFarmacos.Enabled = true;
            //this.frmFarmacos.ForeColor = System.Drawing.SystemColors.ControlText;
            this.frmFarmacos.Location = new System.Drawing.Point(6, 6);
            this.frmFarmacos.Name = "frmFarmacos";
            this.frmFarmacos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmFarmacos.Size = new System.Drawing.Size(621, 48);
            this.frmFarmacos.TabIndex = 0;
            this.frmFarmacos.Text = "F�rmaco prescrito";
            this.frmFarmacos.Visible = true;
            // 
            // lbNombreFarmaco
            // 
            //this.lbNombreFarmaco.BackColor = System.Drawing.SystemColors.Control;
            //this.lbNombreFarmaco.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbNombreFarmaco.Cursor = System.Windows.Forms.Cursors.Default;
            //this.lbNombreFarmaco.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbNombreFarmaco.Location = new System.Drawing.Point(20, 16);
            this.lbNombreFarmaco.Name = "lbNombreFarmaco";
            this.lbNombreFarmaco.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNombreFarmaco.Size = new System.Drawing.Size(583, 22);
            this.lbNombreFarmaco.TabIndex = 1;
            // 
            // ConfirmarFarmacos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(633, 418);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbSalir);
            this.Controls.Add(this.frmFarmacos);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfirmarFarmacos";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Confirmaci�n de tomas - EGI215F4";
            this.Activated += new System.EventHandler(this.ConfirmarFarmacos_Activated);
            this.Closed += new System.EventHandler(this.ConfirmarFarmacos_Closed);
            this.Load += new System.EventHandler(this.ConfirmarFarmacos_Load);
            this.Frame2.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.Frame4.ResumeLayout(false);
            this.Frame3.ResumeLayout(false);
            this.frmFarmacos.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        #endregion
    }
}