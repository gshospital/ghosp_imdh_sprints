using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace MovimientosPac
{
	public class MCMovimientosPac
	{
		public void load(ref SqlConnection Conexion, string stIdPaciente, string NombPaciente, object CrystalReport, string stPathString, string stCrystal, string usuario, string Contrase�a, int ParaanoregiAlt = 0, int ParanumregiAlt = 0)
		{
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			MMovimientosPac.RcAdmision = Conexion;
			MMovimientosPac.stCodpac = stIdPaciente;
			MMovimientosPac.stNombPaciente = NombPaciente;
			MMovimientosPac.PathString = stPathString;
			MMovimientosPac.VstCrystal = stCrystal;
			MMovimientosPac.gUsuario = usuario;
			MMovimientosPac.gContrase�a = Contrase�a;

			MMovimientosPac.gAnoregi_alt = ParaanoregiAlt;
			MMovimientosPac.gNumregi_alt = ParanumregiAlt;

			MMovimientosPac.LISTADO_CRYSTAL = CrystalReport;
			Listados_paciente.DefInstance.proRecPaciente(MMovimientosPac.stCodpac, MMovimientosPac.stNombPaciente);
			MMovimientosPac.RellenarConstantes();
			Listados_paciente.DefInstance.ShowDialog();
		}
	}
}