using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace MovimientosPac
{
	internal static class MMovimientosPac
	{

		public static SqlConnection RcAdmision = null;
		public static string stCodpac = String.Empty; //Gidenpac
		public static string stNombPaciente = String.Empty;
		public static string PathString = String.Empty;
		public static string VstCrystal = String.Empty;
		public static string gUsuario = String.Empty;
		public static string gContrase�a = String.Empty;
		public static object LISTADO_CRYSTAL = null;
		public static dynamic CrystalReport1 = null;
		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public bool VfCabecera;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					return result;
			}
		}
		public static MMovimientosPac.CabCrystal VstrCrystal = MMovimientosPac.CabCrystal.CreateInstance();
		//UPGRADE_NOTE: (6013) CommonDialog variable has been upgraded to a different type More Information: http://www.vbtonet.com/ewis/ewi6013.aspx
		public static PrintDialog Cuadro_Diag_imprePrint = null;
		public static string NomAplicacion = String.Empty;
		public static string Ayuda = String.Empty;
		public static Mensajes.ClassMensajes clase_mensaje = null;

		//Identificadores del episodio de alta
		public static int gAnoregi_alt = 0;
		public static int gNumregi_alt = 0;
		//--------

		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//**************************************************************************
			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, RcAdmision);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			VstrCrystal.VstGrupoHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, RcAdmision);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			VstrCrystal.VstNombreHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, RcAdmision);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			VstrCrystal.VstDireccHo = Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			//UPGRADE_ISSUE: (2064) RDO.rdoResultset method tRrCrystal.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			tRrCrystal.Close();
			VstrCrystal.VfCabecera = true;
		}

		internal static void proDialogo_impre(object Pcrystal, int Num_Copi = 0)
		{
			//Sub proDialogo_impre(Pcrystal As Crystal.CrystalReport, Optional Num_Copi As Integer)
			if (false || Num_Copi == 0)
			{
				Cuadro_Diag_imprePrint.PrinterSettings.Copies = 1;
			}
			else
			{
				Cuadro_Diag_imprePrint.PrinterSettings.Copies = Int16.Parse(Num_Copi.ToString());
			}

            /*//DGMORENOG TODO_X_4 - USO CRISTAL REPORT - INICIO
            //UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            Pcrystal.PrinterStopPage = 0;
			//UPGRADE_WARNING: (6022) The CommonDialog CancelError property is not supported in .NET. More Information: http://www.vbtonet.com/ewis/ewi6022.aspx
			Cuadro_Diag_impre.CancelError = true;
			//UPGRADE_ISSUE: (2064) MSComDlg.CommonDialog property Cuadro_Diag_impre.PrinterDefault was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			Cuadro_Diag_impre.setPrinterDefault(true);
			Cuadro_Diag_imprePrint.ShowDialog();
			//UPGRADE_TODO: (1067) Member CopiesToPrinter is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			Pcrystal.CopiesToPrinter = Cuadro_Diag_imprePrint.PrinterSettings.Copies;
            //DGMORENOG TODO_X_4 - USO CRISTAL REPORT - FIN*/
		}

		internal static void vacia_formulas(object listado, int Numero)
		{
            /*//DGMORENOG TODO_X_4 - USO CRISTAL REPORT - INICIO
            //Public Sub vacia_formulas(listado As Crystal.CrystalReport, Numero As Integer)
            for (int tiForm = 0; tiForm <= Numero; tiForm++)
			{
				//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				listado.Formulas[tiForm] = "";
			}
			for (int tiForm = 0; tiForm <= 10; tiForm++)
			{
				//UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				listado.SortFields[tiForm] = "";
			}
			//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			listado.Reset();
            //DGMORENOG TODO_X_4 - USO CRISTAL REPORT - FIN */
        }

        internal static void RellenarConstantes()
		{
			NomAplicacion = Serrores.ObtenerNombreAplicacion(RcAdmision);
			Ayuda = Serrores.ObtenerFicheroAyuda();
			clase_mensaje = new Mensajes.ClassMensajes();
		}
	}
}
 