using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using ElementosCompartidos;
using Microsoft.CSharp;

namespace MovimientosPac
{
	public partial class Listados_paciente
		: RadForm
	{

		string stsql = String.Empty;
		DataSet RrTest = null;
		string stSeleccion = String.Empty;
		int viform = 0;
		//Dim CrystalReport1 As Crystal.CrystalReport
		string stIntervalo = String.Empty;
		string[] stUnidad_Origen = new string[]{String.Empty, String.Empty, String.Empty, String.Empty, String.Empty};
		string[] stUnidad_Destino = new string[]{String.Empty, String.Empty, String.Empty, String.Empty, String.Empty};
		public Listados_paciente()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cbImprimir_Click(Object eventSender, EventArgs eventArgs)
		{
			string LitPersona = String.Empty;
			string NombTablaTemp = String.Empty;
			string StBaseTemporal = String.Empty;
			bool bError = false;
            
            try
            {
				if (chbAusencias.CheckState == CheckState.Checked)
				{
					if (!ValidaCampos())
					{
						return;
					}
				}

				//listado de movimientos
				if (ChbMovimientos.CheckState == CheckState.Checked)
				{
					MMovimientosPac.CrystalReport1 = MMovimientosPac.LISTADO_CRYSTAL;
					this.Cursor = Cursors.WaitCursor;
					pro_query();

					//*********cabeceras
					if (!MMovimientosPac.VstrCrystal.VfCabecera)
					{
						MMovimientosPac.proCabCrystal();
					}
                    //*******************
                                        
                    //UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    MMovimientosPac.CrystalReport1.Formulas["FORM1"] = "\"" + MMovimientosPac.VstrCrystal.VstGrupoHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM2"] = "\"" + MMovimientosPac.VstrCrystal.VstNombreHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM3"] = "\"" + MMovimientosPac.VstrCrystal.VstDireccHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["SELECCION"] = "\"" + " " + "\"";

					LitPersona = Serrores.ObtenerLiteralPersona(MMovimientosPac.RcAdmision);
					LitPersona = LitPersona.ToUpper();
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["Litpers"] = "\"" + LitPersona + "\"";

					LitPersona = "CAMBIOS DE SERVICIO Y M�DICO DE " + LitPersona.ToUpper() + "S";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["LitpersTitulo"] = "\"" + LitPersona + "\"";

					//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.ReportFileName = "" + MMovimientosPac.PathString + "\\rpt\\agi415r1.rpt";
					//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowTitle = "Listado de Movimientos";
					//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowState = 2; //crptMaximized
					//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Connect = "DSN=" + MMovimientosPac.VstCrystal + ";UID=" + MMovimientosPac.gUsuario + ";PWD=" + MMovimientosPac.gContrase�a + "";
					//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Destination = 0; //crptToWindow
					//UPGRADE_TODO: (1067) Member WindowShowPrintSetupBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowShowPrintSetupBtn = true;
					//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					if (Convert.ToDouble(MMovimientosPac.CrystalReport1.Destination) == 1)
					{ //crptToPrinter Then
						MMovimientosPac.proDialogo_impre(MMovimientosPac.CrystalReport1);
					}
					//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Action = 1;
					//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.PrinterStopPage = MMovimientosPac.CrystalReport1.PageCount;
					this.Cursor = Cursors.Default;
					viform = 6;
					MMovimientosPac.vacia_formulas(MMovimientosPac.CrystalReport1, viform);
					stSeleccion = "";
				}

				if (ChbTraslados.CheckState == CheckState.Checked)
				{
					MMovimientosPac.CrystalReport1 = MMovimientosPac.LISTADO_CRYSTAL;
					this.Cursor = Cursors.WaitCursor;
					if (!MMovimientosPac.VstrCrystal.VfCabecera)
					{
						MMovimientosPac.proCabCrystal();
					}
					//*******************
					proListado_Query();
					//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Destination = 0; //crptToWindow
					//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.SQLQuery = stsql;
					//    PathString = App.Path
					//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.ReportFileName = "" + MMovimientosPac.PathString + "\\rpt\\Agi330f3.rpt";

					if (!MMovimientosPac.VstrCrystal.VfCabecera)
					{
						MMovimientosPac.proCabCrystal();
					}
					//*******************
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM1"] = "\"" + MMovimientosPac.VstrCrystal.VstGrupoHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM2"] = "\"" + MMovimientosPac.VstrCrystal.VstNombreHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM3"] = "\"" + MMovimientosPac.VstrCrystal.VstDireccHo + "\"";
					stIntervalo = " ";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["INTERVALO"] = "\"" + stIntervalo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADOR"] = "\"" + stUnidad_Origen[0] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADOR2"] = "\"" + stUnidad_Origen[1] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADOR3"] = "\"" + stUnidad_Origen[2] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADOR4"] = "\"" + stUnidad_Origen[3] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADOR5"] = "\"" + stUnidad_Origen[4] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADDE"] = "\"" + stUnidad_Destino[0] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADDE2"] = "\"" + stUnidad_Destino[1] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADDE3"] = "\"" + stUnidad_Destino[2] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADDE4"] = "\"" + stUnidad_Destino[3] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDADDE5"] = "\"" + stUnidad_Destino[4] + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIORI"] = "\"" + " " + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["UNIDES"] = "\"" + " " + "\"";

					LitPersona = Serrores.ObtenerLiteralPersona(MMovimientosPac.RcAdmision);
					LitPersona = LitPersona.ToUpper();
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["Litpers"] = "\"" + LitPersona + "\"";

					//UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.SortFields[0] = "+{AMOVIMIE.ffinmovi}";
					//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowTitle = "Listado de los Traslados Realizados";
					//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowState = 2; //crptMaximized
					//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Connect = "DSN=" + MMovimientosPac.VstCrystal + ";UID=" + MMovimientosPac.gUsuario + ";PWD=" + MMovimientosPac.gContrase�a + "";

					//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Destination = 0; //crptToWindow
					//UPGRADE_TODO: (1067) Member WindowShowPrintSetupBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowShowPrintSetupBtn = true;
					//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					if (Convert.ToDouble(MMovimientosPac.CrystalReport1.Destination) == 1)
					{ //crptToPrinter Then
						MMovimientosPac.proDialogo_impre(MMovimientosPac.CrystalReport1);
					}
					//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Action = 1;
					//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.PrinterStopPage = MMovimientosPac.CrystalReport1.PageCount;
					viform = 17;
					MMovimientosPac.vacia_formulas(MMovimientosPac.CrystalReport1, viform);
				}

				if (chbAusencias.CheckState == CheckState.Checked)
				{
					MMovimientosPac.CrystalReport1 = MMovimientosPac.LISTADO_CRYSTAL;
					this.Cursor = Cursors.WaitCursor;

					if (!MMovimientosPac.VstrCrystal.VfCabecera)
					{
						MMovimientosPac.proCabCrystal();
					}

					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM1"] = "\"" + MMovimientosPac.VstrCrystal.VstGrupoHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM2"] = "\"" + MMovimientosPac.VstrCrystal.VstNombreHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM3"] = "\"" + MMovimientosPac.VstrCrystal.VstDireccHo + "\"";

					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["DESDE"] = "'" + sdcDesde.Value.Date + "'";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["HASTA"] = "'" + sdcHasta.Value.Date + "'";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["Paciente"] = "'" + lbPaciente.Text + "'";


					//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.SQLQuery = "select * from aepisadm,aausenci,dmotause where aepisadm.gidenpac ='" + MMovimientosPac.stCodpac + 
					                                          "' and aepisadm.ganoadme = aausenci.ganoadme and " + 
					                                          "  aepisadm.gnumadme = aausenci.gnumadme and aausenci.gmotause = dmotause.gmotause and ";


					//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					object tempRefParam = sdcDesde.Value.ToString();
					MMovimientosPac.CrystalReport1.SQLQuery = Convert.ToString(MMovimientosPac.CrystalReport1.SQLQuery) + " fausenci>=" + Serrores.FormatFechaHMS(tempRefParam) + "";
					
					//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					object tempRefParam2 = sdcHasta.Value.Date.AddDays(1).ToString();
					MMovimientosPac.CrystalReport1.SQLQuery = Convert.ToString(MMovimientosPac.CrystalReport1.SQLQuery) + " and fausenci<" + Serrores.FormatFechaHMS(tempRefParam2) + "";

					if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
					{
						//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						MMovimientosPac.CrystalReport1.SQLQuery = Convert.ToString(MMovimientosPac.CrystalReport1.SQLQuery) + " and AEPISADM.ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + " AND AEPISADM.gnumadme = " + MMovimientosPac.gNumregi_alt.ToString();
					}
					else
					{
						//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						MMovimientosPac.CrystalReport1.SQLQuery = Convert.ToString(MMovimientosPac.CrystalReport1.SQLQuery) + " and AEPISADM.faltplan IS NULL ";
					}

					//UPGRADE_TODO: (1067) Member Connect is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Connect = "DSN=" + MMovimientosPac.VstCrystal + ";UID=" + MMovimientosPac.gUsuario + ";PWD=" + MMovimientosPac.gContrase�a;

					LitPersona = Serrores.ObtenerLiteralPersona(MMovimientosPac.RcAdmision);
					LitPersona = LitPersona.ToLower();

					//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowTitle = "Ausencias del " + LitPersona;
					//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Destination = 0; //crptToWindow
					//UPGRADE_TODO: (1067) Member WindowShowPrintSetupBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowShowPrintSetupBtn = true;
					//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					if (Convert.ToDouble(MMovimientosPac.CrystalReport1.Destination) == 1)
					{ //crptToPrinter Then
						MMovimientosPac.proDialogo_impre(MMovimientosPac.CrystalReport1);
					}

					//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.ReportFileName = MMovimientosPac.PathString + "\\rpt\\Agi330r4.rpt";

					//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowState = 2; // maximizado
					//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Action = 1;
					//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.PrinterStopPage = MMovimientosPac.CrystalReport1.PageCount;

					//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Reset();
				}

				//listado de cambios de entidad
				if (chbMovimientosEntidad.CheckState == CheckState.Checked)
				{
					MMovimientosPac.CrystalReport1 = MMovimientosPac.LISTADO_CRYSTAL;

					this.Cursor = Cursors.WaitCursor;

					NombTablaTemp = "W_MOVENT_" + MMovimientosPac.VstCrystal;
					StBaseTemporal = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + NombTablaTemp + ".mdb";
					bError = ImpresionCambiosEntidad(StBaseTemporal);

					if (bError)
					{
						this.Cursor = Cursors.Default;
						ChbMovimientos.CheckState = CheckState.Unchecked;
						ChbTraslados.CheckState = CheckState.Unchecked;
						chbAusencias.CheckState = CheckState.Unchecked;
						chbMovimientosEntidad.CheckState = CheckState.Unchecked;
						chbMovimientosProducto.CheckState = CheckState.Unchecked;
						proActivaimprimir();
						//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						MMovimientosPac.CrystalReport1.Reset();
						return;
					}

					stsql = "SELECT * FROM TEMPORAL " + "\n" + "\r" + 
					        " ORDER BY NOMBREPACIENTE, FCAMBIO";
					//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.SQLQuery = stsql;

					if (!MMovimientosPac.VstrCrystal.VfCabecera)
					{
						MMovimientosPac.proCabCrystal();
					}

					LitPersona = Serrores.ObtenerLiteralPersona(MMovimientosPac.RcAdmision);
					LitPersona = LitPersona.ToUpper();

					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM1"] = "\"" + MMovimientosPac.VstrCrystal.VstGrupoHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM2"] = "\"" + MMovimientosPac.VstrCrystal.VstNombreHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM3"] = "\"" + MMovimientosPac.VstrCrystal.VstDireccHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["Litpers"] = "\"" + LitPersona + "\"";

					//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.ReportFileName = "" + MMovimientosPac.PathString + "\\rpt\\agi419r1.rpt";
					//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowTitle = "Listado de Cambios de Entidad";

					//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowState = 2; //crptMaximized
					//UPGRADE_TODO: (1067) Member datafiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.datafiles[0] = StBaseTemporal;
					//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Destination = 0; //crptToWindow
					//UPGRADE_TODO: (1067) Member WindowShowPrintSetupBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowShowPrintSetupBtn = true;

					//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Action = 1;
					//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.PrinterStopPage = MMovimientosPac.CrystalReport1.PageCount;
					this.Cursor = Cursors.Default;

					MMovimientosPac.vacia_formulas(MMovimientosPac.CrystalReport1, 4);
					stSeleccion = "";

					File.Delete(StBaseTemporal);

				}

				//listado de cambios de productos
				if (chbMovimientosProducto.CheckState == CheckState.Checked)
				{
					MMovimientosPac.CrystalReport1 = MMovimientosPac.LISTADO_CRYSTAL;

					this.Cursor = Cursors.WaitCursor;

					NombTablaTemp = "W_MOVPRO_" + MMovimientosPac.VstCrystal;
					StBaseTemporal = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + NombTablaTemp + ".mdb";
					bError = ImpresionCambiosProducto(StBaseTemporal);


					if (bError)
					{
						this.Cursor = Cursors.Default;
						ChbMovimientos.CheckState = CheckState.Unchecked;
						ChbTraslados.CheckState = CheckState.Unchecked;
						chbAusencias.CheckState = CheckState.Unchecked;
						chbMovimientosEntidad.CheckState = CheckState.Unchecked;
						chbMovimientosProducto.CheckState = CheckState.Unchecked;
						proActivaimprimir();
						//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						MMovimientosPac.CrystalReport1.Reset();
						return;
					}


					stsql = "SELECT * FROM TEMPORAL " + "\n" + "\r" + 
					        " ORDER BY NOMBREPACIENTE, FCAMBIO";
					//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.SQLQuery = stsql;

					if (!MMovimientosPac.VstrCrystal.VfCabecera)
					{
						MMovimientosPac.proCabCrystal();
					}

					LitPersona = Serrores.ObtenerLiteralPersona(MMovimientosPac.RcAdmision);
					LitPersona = LitPersona.ToUpper();

					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM1"] = "\"" + MMovimientosPac.VstrCrystal.VstGrupoHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM2"] = "\"" + MMovimientosPac.VstrCrystal.VstNombreHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["FORM3"] = "\"" + MMovimientosPac.VstrCrystal.VstDireccHo + "\"";
					//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Formulas["Litpers"] = "\"" + LitPersona + "\"";

					//UPGRADE_TODO: (1067) Member ReportFileName is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.ReportFileName = "" + MMovimientosPac.PathString + "\\rpt\\agi417r1.rpt";
					//UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowTitle = "Listado de Cambios de Producto";

					//UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowState = 2; //crptMaximized
					//UPGRADE_TODO: (1067) Member datafiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.datafiles[0] = StBaseTemporal;
					//UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Destination = 0; //crptToWindow
					//UPGRADE_TODO: (1067) Member WindowShowPrintSetupBtn is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.WindowShowPrintSetupBtn = true;

					//UPGRADE_TODO: (1067) Member Action is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Action = 1;
					//UPGRADE_TODO: (1067) Member PrinterStopPage is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					//UPGRADE_TODO: (1067) Member PageCount is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.PrinterStopPage = MMovimientosPac.CrystalReport1.PageCount;
					this.Cursor = Cursors.Default;

					MMovimientosPac.vacia_formulas(MMovimientosPac.CrystalReport1, 4);
					stSeleccion = "";

					File.Delete(StBaseTemporal);
				}

				this.Cursor = Cursors.Default;
				ChbMovimientos.CheckState = CheckState.Unchecked;
				ChbTraslados.CheckState = CheckState.Unchecked;
				chbAusencias.CheckState = CheckState.Unchecked;
				chbMovimientosEntidad.CheckState = CheckState.Unchecked;
				chbMovimientosProducto.CheckState = CheckState.Unchecked;
				proActivaimprimir();
			}
			catch (Exception e)
			{
				this.Cursor = Cursors.Default;
				//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
				if (Information.Err().Number == 32755)
				{
					//CANCELADA IMPRESION
				}
				else
				{
					//UPGRADE_TODO: (1067) Member Reset is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					MMovimientosPac.CrystalReport1.Reset();
					//UPGRADE_WARNING: (2081) Err.Number has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2081.aspx
					MessageBox.Show("Error: " + Information.Err().Number.ToString() + " " + e.Message, Application.ProductName);
				}
			}
		}

		public void proListado_Query()
		{
			// SQL inicial a la que le concatenar� los trozos segun las unidades hopitalarias
			// que se hayan seleccionado en las listas
			stsql = " SELECT AMOVIMIE.fmovimie, AMOVIMIE.gplantor, AMOVIMIE.ghabitor, " + " AMOVIMIE.gcamaori, AMOVIMIE.gplantde, AMOVIMIE.ghabitde, " + " AMOVIMIE.gcamades, AMOVIMIE.ffinmovi, " + " DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " + " DMOTTRAS.dmotitra, " + " A.dunidenf, B.dunidenf " + " FROM AMOVIMIE, AEPISADM, DPACIENT, DMOTTRAS, DUNIENFE A, DUNIENFE B " + " WHERE " + " AMOVIMIE.itipotra IS NOT NULL AND " + " AMOVIMIE.ffinmovi IS NOT NULL AND " + " AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " + " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " + " AEPISADM.gidenpac = DPACIENT.gidenpac AND " + " AMOVIMIE.gmottras = DMOTTRAS.gmottras AND " + " AMOVIMIE.gunenfor = A.gunidenf AND " + " AMOVIMIE.gunenfde = B.gunidenf and AEPISADM.gidenpac='" + MMovimientosPac.stCodpac + "'";

			if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
			{
				stsql = stsql + " AND AEPISADM.ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + " AND AEPISADM.gnumadme = " + MMovimientosPac.gNumregi_alt.ToString();
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MMovimientosPac.RcAdmision);
			RrTest = new DataSet();
			tempAdapter.Fill(RrTest);

			if (RrTest.Tables[0].Rows.Count == 0)
			{

				//************************* O.Frias (SQL-2005) *************************
				//'''        stsql = " SELECT AMOVIMIE.fmovimie, AMOVIMIE.gplantor, AMOVIMIE.ghabitor, " _
				//''''                & " AMOVIMIE.gcamaori, AMOVIMIE.gplantde, AMOVIMIE.ghabitde, " _
				//''''                & " AMOVIMIE.gcamades, AMOVIMIE.ffinmovi, " _
				//''''                & " DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, " _
				//''''                & " DMOTTRAS.dmotitra, " _
				//''''                & " A.dunidenf, B.dunidenf " _
				//''''            & " FROM AMOVIMIE, AEPISADM, DPACIENT, DMOTTRAS, DUNIENFE A, DUNIENFE B " _
				//''''            & " WHERE " _
				//''''                & " AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " _
				//''''                & " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " _
				//''''                & " AEPISADM.gidenpac = DPACIENT.gidenpac AND " _
				//''''                & " AMOVIMIE.gmottras *= DMOTTRAS.gmottras AND " _
				//''''                & " AMOVIMIE.gunenfor = A.gunidenf AND " _
				//''''                & " AMOVIMIE.gunenfde *= B.gunidenf and " _
				//''''                & " AEPISADM.gidenpac='" & stCodpac & "'"
				//'''
				//'''
				//'''        If gAnoregi_alt > 0 And gNumregi_alt > 0 Then
				//'''            stsql = stsql & " AND AEPISADM.ganoadme=" & gAnoregi_alt & " AND AEPISADM.gnumadme = " & gNumregi_alt
				//'''        End If
				//'''
				//'''        stsql = stsql & " AND AMOVIMIE.fmovimie in (select max(fmovimie) from AMOVIMIE, AEPISADM where ganoregi = ganoadme and gnumregi = gnumadme and gidenpac = '" & stCodpac & "' "
				//'''
				//'''        If gAnoregi_alt > 0 And gNumregi_alt > 0 Then
				//'''            stsql = stsql & " AND ganoadme=" & gAnoregi_alt & " AND gnumadme = " & gNumregi_alt & ")"
				//'''        Else
				//'''            stsql = stsql & ")"
				//'''        End If


				stsql = "SELECT AMOVIMIE.fmovimie, AMOVIMIE.gplantor, AMOVIMIE.ghabitor, AMOVIMIE.gcamaori, " + 
				        "AMOVIMIE.gplantde, AMOVIMIE.ghabitde, AMOVIMIE.gcamades, AMOVIMIE.ffinmovi, " + 
				        "DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, DMOTTRAS.dmotitra, " + 
				        "A.dunidenf, B.dunidenf " + 
				        "FROM AMOVIMIE " + 
				        "INNER JOIN AEPISADM ON AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " + 
				        "AMOVIMIE.gnumregi = AEPISADM.gnumadme " + 
				        "INNER JOIN DPACIENT ON AEPISADM.gidenpac = DPACIENT.gidenpac " + 
				        "LEFT OUTER JOIN DMOTTRAS ON AMOVIMIE.gmottras = DMOTTRAS.gmottras " + 
				        "Inner Join DUNIENFE AS A ON AMOVIMIE.gunenfor = A.gunidenf " + 
				        "LEFT OUTER JOIN DUNIENFE AS B ON AMOVIMIE.gunenfde = B.gunidenf " + 
				        "WHERE (AEPISADM.gidenpac = '" + MMovimientosPac.stCodpac + "' ) AND " + 
				        "(AMOVIMIE.fmovimie IN ( SELECT MAX(AMOVIMIE.fmovimie) FROM AMOVIMIE " + 
				        "INNER JOIN AEPISADM ON AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " + 
				        "AMOVIMIE.gnumregi = AEPISADM.gnumadme " + 
				        "WHERE (AEPISADM.gidenpac = '" + MMovimientosPac.stCodpac + "' )) ";

				if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
				{
					stsql = stsql + " AND (ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + ") AND (gnumadme = " + MMovimientosPac.gNumregi_alt.ToString() + "))";
				}
				else
				{
					stsql = stsql + ")";
				}

				if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
				{
					stsql = stsql + " AND (AEPISADM.ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + " ) AND ( AEPISADM.gnumadme = " + MMovimientosPac.gNumregi_alt.ToString() + " )";
				}

				//************************* O.Frias (SQL-2005) *************************
			}			
			RrTest.Close();

			stUnidad_Origen[0] = " ";
			stUnidad_Destino[0] = " ";
		}

		private void chbAusencias_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{            
            this.sdcDesde.NullableValue = null;
            this.sdcDesde.SetToNullValue();

            this.sdcHasta.NullableValue = null;
            this.sdcHasta.SetToNullValue();

            frmFechas.Enabled = chbAusencias.CheckState == CheckState.Checked;
			sdcDesde.Enabled = chbAusencias.CheckState == CheckState.Checked;
			sdcHasta.Enabled = chbAusencias.CheckState == CheckState.Checked;
			proActivaimprimir();
		}

		private void ChbMovimientos_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivaimprimir();
		}

		private void chbMovimientosEntidad_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivaimprimir();
		}

		private void chbMovimientosProducto_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivaimprimir();
		}

		private void ChbTraslados_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			proActivaimprimir();
		}

		public void proActivaimprimir()
		{
			if (chbAusencias.CheckState == CheckState.Checked)
			{
				cbImprimir.Enabled = sdcDesde.Text != "" && sdcHasta.Text != "";
			}
			else
			{
				cbImprimir.Enabled = ChbMovimientos.CheckState == CheckState.Checked || ChbTraslados.CheckState == CheckState.Checked || chbMovimientosEntidad.CheckState == CheckState.Checked || chbMovimientosProducto.CheckState == CheckState.Checked;
			}
			//Else
			//    cbImprimir.Enabled = False
			//End If
		}


		//UPGRADE_WARNING: (2080) Form_Load event was upgraded to Form_Load event and has a new behavior. More Information: http://www.vbtonet.com/ewis/ewi2080.aspx
		private void Listados_paciente_Load(Object eventSender, EventArgs eventArgs)
		{

			cbImprimir.Enabled = false;
			frmFechas.Enabled = false;
			sdcDesde.Enabled = false;
			sdcHasta.Enabled = false;

            this.sdcDesde.NullableValue = null;
            this.sdcDesde.SetToNullValue();

            this.sdcHasta.NullableValue = null;
            this.sdcHasta.SetToNullValue();


            this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);

            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

			string LitPersona = Serrores.ObtenerLiteralPersona(MMovimientosPac.RcAdmision);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();

			Frame3.Text = LitPersona;

			Listados_paciente.DefInstance.Text = "Listados del " + LitPersona.ToLower() + " AGI180F1";

			string sql = "SELECT VALFANU1 FROM SCONSGLO WHERE GCONSGLO='GPRODUCT'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MMovimientosPac.RcAdmision);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			Frame2.Visible = false;
			if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
			{
				Frame2.Visible = true;
				Label5.Text = MMovimientosPac.gAnoregi_alt.ToString() + " / " + MMovimientosPac.gNumregi_alt.ToString();
			}
			chbMovimientosProducto.Visible = false;
			if (RR.Tables[0].Rows.Count != 0)
			{				
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0][0]))
				{					
					if (Convert.ToString(RR.Tables[0].Rows[0][0]).Trim().ToUpper() == "S")
					{
						chbMovimientosProducto.Visible = true;
					}
				}
			}            

            Telerik.WinControls.Primitives.BorderPrimitive border = null;
            border = (Telerik.WinControls.Primitives.BorderPrimitive)this.Frame2.PanelElement.Children[1];
            border.ForeColor = System.Drawing.Color.Transparent;
            border.ForeColor2 = System.Drawing.Color.Transparent;
            border.ForeColor3 = System.Drawing.Color.Transparent;
            border.ForeColor4 = System.Drawing.Color.Transparent;
        }

        public void pro_query()
		{
			stSeleccion = "Movimientos del paciente";
			stsql = " SELECT * " + " FROM AMOVIMIE, AEPISADM, DPACIENT, DSERVICI, DPERSONA, " + " DSERVICI AS A, DPERSONA AS B " + " WHERE AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " + " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " + " AEPISADM.gidenpac = DPACIENT.gidenpac AND " + " (AMOVIMIE.gservior <> AMOVIMIE.gservide or " + " AMOVIMIE.gmedicor <> AMOVIMIE.gmedicde) " + " AND " + " AMOVIMIE.gservior = DSERVICI.gservici AND " + " AMOVIMIE.gservide = A.gservici AND " + " AMOVIMIE.gmedicor = DPERSONA.gpersona AND " + " AMOVIMIE.gmedicde = B.gpersona AND " + " AEPISADM.GIDENPAC = '" + MMovimientosPac.stCodpac + "' and ";

			if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
			{
				stsql = stsql + " AEPISADM.ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + " AND AEPISADM.gnumadme = " + MMovimientosPac.gNumregi_alt.ToString();
			}
			else
			{
				stsql = stsql + " AEPISADM.faltplan IS NULL ";
			}

			// Si no hay registros es que s�lo hay un movimiento. Lo ponemos

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MMovimientosPac.RcAdmision);
			RrTest = new DataSet();
			tempAdapter.Fill(RrTest);

			if (RrTest.Tables[0].Rows.Count == 0)
			{

				//**************************** O.Frias (SQL-2005) ****************************
				//'''        stsql = " SELECT * " _
				//''''            & " FROM AMOVIMIE, AEPISADM, DPACIENT, DSERVICI, DPERSONA, " _
				//''''            & " DSERVICI AS A, DPERSONA AS B " _
				//''''            & " WHERE AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " _
				//''''            & " AMOVIMIE.gnumregi = AEPISADM.gnumadme AND " _
				//''''            & " AEPISADM.gidenpac = DPACIENT.gidenpac AND " _
				//''''            & " AMOVIMIE.gservior = DSERVICI.gservici AND " _
				//''''            & " AMOVIMIE.gservide *= A.gservici AND " _
				//''''            & " AMOVIMIE.gmedicor = DPERSONA.gpersona AND " _
				//''''            & " AMOVIMIE.gmedicde *= B.gpersona AND " _
				//''''            & " AEPISADM.GIDENPAC = '" & stCodpac & "' and "

				stsql = "SELECT * " + 
				        "FROM AMOVIMIE " + 
				        "INNER JOIN AEPISADM ON AMOVIMIE.ganoregi = AEPISADM.ganoadme AND " + 
				        "AMOVIMIE.gnumregi = AEPISADM.gnumadme " + 
				        "INNER JOIN DPACIENT ON AEPISADM.gidenpac = DPACIENT.gidenpac " + 
				        "INNER JOIN DSERVICI ON AMOVIMIE.gservior = DSERVICI.gservici " + 
				        "LEFT OUTER JOIN DSERVICI AS A ON AMOVIMIE.gservide = A.gservici " + 
				        "INNER JOIN DPERSONA ON AMOVIMIE.gmedicor = DPERSONA.gpersona " + 
				        "LEFT OUTER JOIN DPERSONA AS B ON AMOVIMIE.gmedicde = B.gpersona " + 
				        "WHERE (AEPISADM.gidenpac = '" + MMovimientosPac.stCodpac + "' ) AND ";

				//**************************** O.Frias (SQL-2005) ****************************

				if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
				{
					stsql = stsql + " AEPISADM.ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + " AND AEPISADM.gnumadme = " + MMovimientosPac.gNumregi_alt.ToString();
				}
				else
				{
					stsql = stsql + " AEPISADM.faltplan IS NULL ";
				}
			}
			
			RrTest.Close();
            
            //& " ORDER BY DAPE1PAC,DAPE2PAC,DNOMBPAC,ffinmovi ASC "
            //UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
            MMovimientosPac.CrystalReport1.SortFields[0] = "+{DPACIENT.DAPE1PAC}";
			//UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			MMovimientosPac.CrystalReport1.SortFields[1] = "+{DPACIENT.DAPE2PAC}";
			//UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			MMovimientosPac.CrystalReport1.SortFields[2] = "+{DPACIENT.DNOMBPAC}";
			//UPGRADE_TODO: (1067) Member SortFields is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			MMovimientosPac.CrystalReport1.SortFields[3] = "+{AMOVIMIE.FFINMOVI}";

			//UPGRADE_TODO: (1067) Member Formulas is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			MMovimientosPac.CrystalReport1.Formulas["SELECCION"] = "\"" + stSeleccion + "\"";
			//UPGRADE_TODO: (1067) Member SQLQuery is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
			MMovimientosPac.CrystalReport1.SQLQuery = stsql;
		}

		public void proRecPaciente(string stCodPaciente, string stNombPaciente)
		{
			//******
			try
			{
				//***********
				lbPaciente.Text = stNombPaciente;
				MMovimientosPac.stCodpac = stCodPaciente;
			}
			catch (SqlException ex)
			{
				//**********				
				Serrores.GestionErrorBaseDatos(ex, MMovimientosPac.RcAdmision);
			}
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

		//On Error GoTo ErrorImpresion
		//Dim sqlAusencias As String
		//If Destino = 1 Then ' impresora
		//    ObjImpresion.ShowPrinter
		//End If
		//sqlAusencias = "select * from aausenci where ganoregi=" & A�o & _
		//'               " and gnumregi=" & Numero
		//
		//
		//ObjCrystal.SQLQuery = sqlAusencias
		//ObjCrystal.Connect = "DSN=" & NombreDSN & ";UID=" & UsuarioCrystal & ";PWD=" & Contrase�aCrystal
		//If Destino = 1 Then ' impresora
		//    ObjCrystal.CopiesToPrinter = ObjImpresion.Copies
		//End If
		//ObjCrystal.Destination = Destino
		//ObjCrystal.WindowState = 2 ' maximizado
		//ObjCrystal.Action = 1
		//
		//ObjCrystal.Reset
		//
		//Exit Sub
		//

		//UPGRADE_WARNING: (2050) SSCalendarWidgets_A.SSDateCombo Event sdcDesde.CloseUp was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2050.aspx
		private void sdcDesde_CloseUp(object eventSender, RadPopupClosingEventArgs eventArgs)
		{
			proActivaimprimir();
		}

		private void sdcDesde_Leave(Object eventSender, EventArgs eventArgs)
		{
			proActivaimprimir();
		}
		
		private void sdcHasta_CloseUp(object eventSender, RadPopupClosingEventArgs eventArgs)
        {
			proActivaimprimir();
		}

		private void sdcHasta_Leave(Object eventSender, EventArgs eventArgs)
		{
			proActivaimprimir();
		}

		private bool ValidaCampos()
		{
			if (sdcDesde.Text == "")
			{
				short tempRefParam = 1040;
				string[] tempRefParam2 = new string[] {Label3.Text};
				MMovimientosPac.clase_mensaje.RespuestaMensaje( MMovimientosPac.NomAplicacion, MMovimientosPac.Ayuda, tempRefParam, MMovimientosPac.RcAdmision, tempRefParam2);
				sdcDesde.Focus();
				return false;
			}
			if (sdcHasta.Text == "")
			{
				short tempRefParam3 = 1040;
                string[] tempRefParam4 = new string[] { Label2.Text};
				MMovimientosPac.clase_mensaje.RespuestaMensaje(MMovimientosPac.NomAplicacion, MMovimientosPac.Ayuda, tempRefParam3, MMovimientosPac.RcAdmision, tempRefParam4);
				sdcDesde.Focus();
				return false;
			}
			if (DateTime.Parse(sdcDesde.Text) > DateTime.Parse(sdcHasta.Text))
			{
				short tempRefParam5 = 1020;
                string[] tempRefParam6 = new string[] { Label2.Text, "mayor o igual", Label3.Text};
				MMovimientosPac.clase_mensaje.RespuestaMensaje(MMovimientosPac.NomAplicacion, MMovimientosPac.Ayuda, tempRefParam5, MMovimientosPac.RcAdmision, tempRefParam6);
				sdcDesde.Focus();
				return false;
			}
			return true;
		}

		private bool ImpresionCambiosEntidad(string NombTablaTemp)
		{
			bool result = false;
			object dbLangSpanish = null;
			object DBEngine = null;

			DataSet RR = null;
			DataSet rr2 = null;
            //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES - INICIO
            /*//UPGRADE_ISSUE: (2068) Workspace object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            Workspace Wk1 = null;
			//UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			Database Dbt1 = null;
			//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES - FIN*/

			//Variable para el listado de movimientos de cambio de entidad
			string Lnombrepac = String.Empty;
			string Lgidenpac = String.Empty;
			string Lfllegada = String.Empty;
			string Lfaltplan = String.Empty;
			string Lfmovimie = String.Empty;
			string Lsocieda_o = String.Empty;
			string Linspecc_o = String.Empty;
			string Lnafiliac_o = String.Empty;
			string Lsocieda_d = String.Empty;
			string Linspecc_d = String.Empty;
			string Lnafiliac_d = String.Empty;
			//----------------------

			try
			{
                /*//DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES - INICIO
                //UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                Wk1 = (Workspace) DBEngine.Workspaces(0);
				if (FileSystem.Dir(NombTablaTemp, FileAttribute.Normal) == "")
				{
					//UPGRADE_TODO: (1067) Member CreateDatabase is not defined in type Workspace. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Dbt1 = (Database) Wk1.CreateDatabase(NombTablaTemp, dbLangSpanish);
				}
				else
				{
					//UPGRADE_TODO: (1067) Member OpenDatabase is not defined in type Workspace. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Dbt1 = (Database) Wk1.OpenDatabase(NombTablaTemp);
					//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					foreach (TableDef dtabla in Dbt1.TableDefs)
					{
						//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						if (Convert.ToString(dtabla.Name) == "TEMPORAL")
						{
							//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Dbt1.Execute("drop table TEMPORAL ");
							break;
						}
					}
				}
                
				stsql = "CREATE TABLE TEMPORAL " + 
				        " ( GIDENPAC CHAR (13), " + 
				        "   NOMBREPACIENTE CHAR(100), " + 
				        "   FINGRESO CHAR(20), " + 
				        "   FALTA CHAR(20), " + 
				        "   FCAMBIO CHAR(20), " + 
				        "   SOCIEDAD_OR CHAR (50)," + 
				        "   INSPECC_OR CHAR (50)," + 
				        "   NAFILIAC_OR CHAR(20), " + 
				        "   SOCIEDAD_DE CHAR (50)," + 
				        "   INSPECC_DE CHAR (50)," + 
				        "   NAFILIAC_DE CHAR(20))";
				//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Dbt1.Execute(stsql);
                //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES - FIN*/

                //******************** O.Frias (SQL-2005) ********************

                //'''stsql = " SELECT rtrim(isnull(dpacient.dape1pac,''))+' '+" & _
                //''''             "   rtrim(isnull(dpacient.dape2pac,''))+', '+" & _
                //''''             "   rtrim(isnull(dpacient.dnombpac,'')) as nombrepac," & _
                //''''             "   aepisadm.fllegada, aepisadm.faltplan, aepisadm.gidenpac," & _
                //''''             "   amovifin.fmovimie," & _
                //''''             "   dsocieda.dsocieda," & _
                //''''             "   DINSPECC.DINSPECC," & _
                //''''             "   amovifin.nafiliac," & _
                //''''             "   amovifin.ganoregi, amovifin.gnumregi, amovifin.ffinmovi " & _
                //''''        " From amovifin, aepisadm, dpacient, dsocieda, DINSPECC" & _
                //''''        " Where " & _
                //''''        " AMOVIFIN.ganoregi = AEPISADM.ganoadme AND " & _
                //''''        " AMOVIFIN.gnumregi = AEPISADM.gnumadme AND " & _
                //''''        " AEPISADM.gidenpac = DPACIENT.gidenpac AND " & _
                //''''        " AMOVIFIN.gsocieda = DSOCIEDA.gsocieda AND " & _
                //''''        " AMOVIFIN.ginspecc *= DINSPECC.ginspecc AND " & _
                //''''        " AEPISADM.GIDENPAC = '" & stCodpac & "' and " & _
                //''''        " AMOVIFIN.ffinmovi is not null and"


                stsql = "SELECT RTRIM(ISNULL(DPACIENT.dape1pac, '')) + ' ' + RTRIM(ISNULL(DPACIENT.dape2pac, '')) + ', ' + " + 
				        "RTRIM(ISNULL(DPACIENT.dnombpac, '')) AS nombrepac, AEPISADM.fllegada, AEPISADM.faltplan, " + 
				        "AEPISADM.gidenpac, AMOVIFIN.fmovimie, DSOCIEDA.dsocieda, DINSPECC.dinspecc, AMOVIFIN.nafiliac, " + 
				        "AMOVIFIN.ganoregi , AMOVIFIN.gnumregi, AMOVIFIN.ffinmovi " + 
				        "FROM AMOVIFIN " + 
				        "INNER JOIN AEPISADM ON AMOVIFIN.ganoregi = AEPISADM.ganoadme AND " + 
				        "AMOVIFIN.gnumregi = AEPISADM.gnumadme " + 
				        "INNER JOIN DPACIENT ON AEPISADM.gidenpac = DPACIENT.gidenpac " + 
				        "INNER JOIN DSOCIEDA ON AMOVIFIN.gsocieda = DSOCIEDA.gsocieda " + 
				        "LEFT OUTER JOIN DINSPECC ON AMOVIFIN.ginspecc = DINSPECC.ginspecc " + 
				        "WHERE (AEPISADM.gidenpac = '" + MMovimientosPac.stCodpac + "' ) AND " + 
				        "(AMOVIFIN.ffinmovi IS NOT NULL) AND ";

				//******************** O.Frias (SQL-2005) ********************
				if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
				{
					stsql = stsql + " (AEPISADM.ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + " ) AND (AEPISADM.gnumadme = " + MMovimientosPac.gNumregi_alt.ToString();
					stsql = stsql + " ) AND (AMOVIFIN.ffinmovi <> AEPISADM.faltplan )";
				}
				else
				{
					stsql = stsql + "( AEPISADM.faltplan IS NULL ) ";
				}

				stsql = stsql + " ORDER BY DPACIENT.DAPE1PAC, AMOVIFIN.fmovimie";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MMovimientosPac.RcAdmision);
				RR = new DataSet();
				tempAdapter.Fill(RR);

				if (RR.Tables[0].Rows.Count == 0)
				{
					//******************** O.Frias (SQL-2005) ********************
					//'''    stsql = " SELECT rtrim(isnull(dpacient.dape1pac,''))+' '+" & _
					//''''                 "   rtrim(isnull(dpacient.dape2pac,''))+', '+" & _
					//''''                 "   rtrim(isnull(dpacient.dnombpac,'')) as nombrepac," & _
					//''''                 "   aepisadm.fllegada, aepisadm.faltplan, aepisadm.gidenpac," & _
					//''''                 "   amovifin.fmovimie," & _
					//''''                 "   dsocieda.dsocieda," & _
					//''''                 "   DINSPECC.DINSPECC," & _
					//''''                 "   amovifin.nafiliac," & _
					//''''                 "   amovifin.ganoregi, amovifin.gnumregi, amovifin.ffinmovi " & _
					//''''            " From amovifin, aepisadm, dpacient, dsocieda, DINSPECC" & _
					//''''            " Where " & _
					//''''            " AMOVIFIN.ganoregi = AEPISADM.ganoadme AND " & _
					//''''            " AMOVIFIN.gnumregi = AEPISADM.gnumadme AND " & _
					//''''            " AEPISADM.gidenpac = DPACIENT.gidenpac AND " & _
					//''''            " AMOVIFIN.gsocieda = DSOCIEDA.gsocieda AND " & _
					//''''            " AMOVIFIN.ginspecc *= DINSPECC.ginspecc AND " & _
					//''''            " AEPISADM.GIDENPAC = '" & stCodpac & "' "

					stsql = "SELECT RTRIM(ISNULL(DPACIENT.dape1pac, '')) + ' ' + RTRIM(ISNULL(DPACIENT.dape2pac, '')) + ', ' + " + 
					        "RTRIM(ISNULL(DPACIENT.dnombpac, '')) AS nombrepac, AEPISADM.fllegada, AEPISADM.faltplan, " + 
					        "AEPISADM.gidenpac, AMOVIFIN.fmovimie, DSOCIEDA.dsocieda, DINSPECC.dinspecc, AMOVIFIN.nafiliac, " + 
					        "AMOVIFIN.ganoregi , AMOVIFIN.gnumregi, AMOVIFIN.ffinmovi " + 
					        "FROM AMOVIFIN " + 
					        "INNER JOIN AEPISADM ON AMOVIFIN.ganoregi = AEPISADM.ganoadme AND " + 
					        "AMOVIFIN.gnumregi = AEPISADM.gnumadme " + 
					        "INNER JOIN DPACIENT ON AEPISADM.gidenpac = DPACIENT.gidenpac " + 
					        "INNER JOIN DSOCIEDA ON AMOVIFIN.gsocieda = DSOCIEDA.gsocieda " + 
					        "LEFT OUTER JOIN DINSPECC ON AMOVIFIN.ginspecc = DINSPECC.ginspecc " + 
					        "WHERE (AEPISADM.gidenpac = '" + MMovimientosPac.stCodpac + "' ) ";

					//******************** O.Frias (SQL-2005) ********************
					if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
					{
						stsql = stsql + " and AEPISADM.ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + " AND AEPISADM.gnumadme = " + MMovimientosPac.gNumregi_alt.ToString();
					}
					else
					{
						stsql = stsql + " and AEPISADM.faltplan IS NULL ";
					}

					stsql = stsql + " ORDER BY DPACIENT.DAPE1PAC, AMOVIFIN.fmovimie";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql, MMovimientosPac.RcAdmision);
					RR = new DataSet();
					tempAdapter_2.Fill(RR);
				}

				if (RR.Tables[0].Rows.Count == 0)
				{
					//    ImpresionCambiosEntidad = True
					//    Exit Function
				}
				else
				{
					foreach (DataRow iteration_row in RR.Tables[0].Rows)
					{

						Lsocieda_d = "";
						Linspecc_d = "";
						Lnafiliac_d = "";
						Lnombrepac = "";
						Lgidenpac = "";
						Lfllegada = "";
						Lfaltplan = "";
						Lfmovimie = "";
						Lsocieda_o = "";
						Linspecc_o = "";
						Lnafiliac_o = "";

						Lnombrepac = Convert.ToString(iteration_row["nombrepac"]);
						Lgidenpac = Convert.ToString(iteration_row["Gidenpac"]);
						Lfllegada = Convert.ToDateTime(iteration_row["fllegada"]).ToString("dd/MM/yyyy HH:mm");
						Lfaltplan = Convert.ToDateTime(iteration_row["faltplan"]).ToString("dd/MM/yyyy HH:mm");
						Lfmovimie = Convert.ToDateTime(iteration_row["ffinmovi"]).ToString("dd/MM/yyyy HH:mm"); //fmovimie
						Lsocieda_o = Convert.ToString(iteration_row["DSOCIEDA"]).Trim() + "";
						Linspecc_o = Convert.ToString(iteration_row["DINSPECC"]).Trim() + "";
						Lnafiliac_o = Convert.ToString(iteration_row["nafiliac"]).Trim() + "";

						//Buscamos la sociedad destino


						//******************** O.Frias (SQL-2005) ********************
						//'''        stsql = " SELECT dsocieda.dsocieda," & _
						//''''                " DINSPECC.DINSPECC, nafiliac" & _
						//''''                " From amovifin, dsocieda, DINSPECC" & _
						//''''                " Where " & _
						//''''                " AMOVIFIN.gsocieda = DSOCIEDA.gsocieda AND " & _
						//''''                " AMOVIFIN.ginspecc *= DINSPECC.ginspecc AND " & _
						//''''                " AMOVIFIN.ganoregi = " & RR!ganoregi & " AND " & _
						//''''                " AMOVIFIN.gnumregi = " & RR!gnumregi & " AND " & _
						//''''                " AMOVIFIN.fmovimie = " & FormatFechaHMS(RR!ffinmovi)

						//UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
						object tempRefParam = iteration_row["ffinmovi"];
						stsql = "SELECT DSOCIEDA.dsocieda, DINSPECC.dinspecc, AMOVIFIN.nafiliac " + 
						        "FROM AMOVIFIN " + 
						        "INNER JOIN DSOCIEDA ON AMOVIFIN.gsocieda = DSOCIEDA.gsocieda " + 
						        "LEFT OUTER JOIN DINSPECC ON AMOVIFIN.ginspecc = DINSPECC.ginspecc " + 
						        "WHERE (AMOVIFIN.ganoregi = " + Convert.ToString(iteration_row["ganoregi"]) + " ) AND " + 
						        "(AMOVIFIN.gnumregi = " + Convert.ToString(iteration_row["gnumregi"]) + " ) AND " + 
						        "(AMOVIFIN.fmovimie = " + Serrores.FormatFechaHMS(tempRefParam) + " ) ";

						//******************** O.Frias (SQL-2005) ********************
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stsql, MMovimientosPac.RcAdmision);
						rr2 = new DataSet();
						tempAdapter_3.Fill(rr2);
						if (rr2.Tables[0].Rows.Count != 0)
						{
							Lsocieda_d = Convert.ToString(rr2.Tables[0].Rows[0]["DSOCIEDA"]).Trim() + "";
							Linspecc_d = Convert.ToString(rr2.Tables[0].Rows[0]["DINSPECC"]).Trim() + "";
							Lnafiliac_d = Convert.ToString(rr2.Tables[0].Rows[0]["nafiliac"]).Trim() + "";
						}
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rr2.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						rr2.Close();
						//----------

						stsql = "INSERT INTO TEMPORAL " + 
						        " ( GIDENPAC, NOMBREPACIENTE, " + 
						        "   FINGRESO, FALTA, FCAMBIO, " + 
						        "   SOCIEDAD_OR, INSPECC_OR, NAFILIAC_OR, " + 
						        "   SOCIEDAD_DE, INSPECC_DE, NAFILIAC_DE )" + 
						        " VALUES ( " + 
						        " '" + Lgidenpac + "', '" + Lnombrepac + "'," + 
						        " '" + Lfllegada + "', '" + Lfaltplan + "', '" + Lfmovimie + "'," + 
						        " '" + Lsocieda_o + "','" + Linspecc_o + "', '" + Lnafiliac_o + "'," + 
						        " '" + Lsocieda_d + "','" + Linspecc_d + "', '" + Lnafiliac_d + "' )";
                        //UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                        //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES
                        //Dbt1.Execute(stsql);

					}
				}
				
				RR.Close();

                //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES
                /*Dbt1.Close();				
				Wk1.Close();*/

				return result;
			}
			catch (System.Exception excep)
			{
				RadMessageBox.Show(excep.Message, Application.ProductName);
				return true;
			}
		}

		private bool ImpresionCambiosProducto(string NombTablaTemp)
		{
			bool result = false;
			object dbLangSpanish = null;
			object DBEngine = null;

			DataSet RR = null;
			DataSet rr2 = null;
            /*//DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES - INICIO
            //UPGRADE_ISSUE: (2068) Workspace object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            Workspace Wk1 = null;
			//UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
			Database Dbt1 = null;
			//UPGRADE_ISSUE: (2068) TableDef object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
            //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES - FIN*/

            //Variable para el listado de movimientos de cambio de entidad
            string Lnombrepac = String.Empty;
			string Lgidenpac = String.Empty;
			string Lfllegada = String.Empty;
			string Lfaltplan = String.Empty;
			string Lfmovimie = String.Empty;
			string Lproducto_o = String.Empty;
			string Lproducto_d = String.Empty;
			//----------------------

			try
			{
                /*//DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES - INICIO
                //UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                Wk1 = (Workspace) DBEngine.Workspaces(0);
				if (FileSystem.Dir(NombTablaTemp, FileAttribute.Normal) == "")
				{
					//UPGRADE_TODO: (1067) Member CreateDatabase is not defined in type Workspace. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Dbt1 = (Database) Wk1.CreateDatabase(NombTablaTemp, dbLangSpanish);
				}
				else
				{
					//UPGRADE_TODO: (1067) Member OpenDatabase is not defined in type Workspace. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					Dbt1 = (Database) Wk1.OpenDatabase(NombTablaTemp);
					//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					foreach (TableDef dtabla in Dbt1.TableDefs)
					{
						//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						if (Convert.ToString(dtabla.Name) == "TEMPORAL")
						{
							//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							Dbt1.Execute("drop table TEMPORAL ");
							break;
						}
					}
				}
                

                stsql = "CREATE TABLE TEMPORAL " + 
				        " ( GIDENPAC CHAR (13), " + 
				        "   NOMBREPACIENTE CHAR(100), " + 
				        "   FINGRESO CHAR(20), " + 
				        "   FALTA CHAR(20), " + 
				        "   FCAMBIO CHAR(20), " + 
				        "   PRODUCTO_OR CHAR (30)," + 
				        "   PRODUCTO_DE CHAR(30))";
				//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Dbt1.Execute(stsql);
                //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES - FIN*/

                stsql = " SELECT rtrim(isnull(dpacient.dape1pac,''))+' '+" + 
				        "   rtrim(isnull(dpacient.dape2pac,''))+', '+" + 
				        "   rtrim(isnull(dpacient.dnombpac,'')) as nombrepac," + 
				        "   aepisadm.fllegada, aepisadm.faltplan, aepisadm.gidenpac," + 
				        "   amovprod.fmovimie," + 
				        "   dproduct.dproducto," + 
				        "   amovprod.ganoregi, amovprod.gnumregi, amovprod.ffinmovi " + 
				        " From amovprod, aepisadm, dpacient, dproduct" + 
				        " Where " + 
				        " amovprod.ganoregi = AEPISADM.ganoadme AND " + 
				        " amovprod.gnumregi = AEPISADM.gnumadme AND " + 
				        " AEPISADM.gidenpac = DPACIENT.gidenpac AND " + 
				        " amovprod.gproducto = dproduct.gproducto AND " + 
				        " AEPISADM.GIDENPAC = '" + MMovimientosPac.stCodpac + "' and " + 
				        " amovprod.ffinmovi is not null and";

				if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
				{
					stsql = stsql + " AEPISADM.ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + " AND AEPISADM.gnumadme = " + MMovimientosPac.gNumregi_alt.ToString();
					stsql = stsql + " AND amovprod.ffinmovi <> AEPISADM.faltplan";
				}
				else
				{
					stsql = stsql + " AEPISADM.faltplan IS NULL ";
				}

				stsql = stsql + " ORDER BY DPACIENT.DAPE1PAC, amovprod.fmovimie";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, MMovimientosPac.RcAdmision);
				RR = new DataSet();
				tempAdapter.Fill(RR);

				if (RR.Tables[0].Rows.Count == 0)
				{
					stsql = " SELECT rtrim(isnull(dpacient.dape1pac,''))+' '+" + 
					        "   rtrim(isnull(dpacient.dape2pac,''))+', '+" + 
					        "   rtrim(isnull(dpacient.dnombpac,'')) as nombrepac," + 
					        "   aepisadm.fllegada, aepisadm.faltplan, aepisadm.gidenpac," + 
					        "   amovprod.fmovimie," + 
					        "   dproduct.dproducto," + 
					        "   amovprod.ganoregi, amovprod.gnumregi, amovprod.ffinmovi " + 
					        " From amovprod, aepisadm, dpacient, dproduct" + 
					        " Where " + 
					        " amovprod.ganoregi = AEPISADM.ganoadme AND " + 
					        " amovprod.gnumregi = AEPISADM.gnumadme AND " + 
					        " AEPISADM.gidenpac = DPACIENT.gidenpac AND " + 
					        " amovprod.gproducto = dproduct.gproducto AND " + 
					        " AEPISADM.GIDENPAC = '" + MMovimientosPac.stCodpac + "' and ";

					if (MMovimientosPac.gAnoregi_alt > 0 && MMovimientosPac.gNumregi_alt > 0)
					{
						stsql = stsql + " AEPISADM.ganoadme=" + MMovimientosPac.gAnoregi_alt.ToString() + " AND AEPISADM.gnumadme = " + MMovimientosPac.gNumregi_alt.ToString();
					}
					else
					{
						stsql = stsql + " AEPISADM.faltplan IS NULL ";
					}

					stsql = stsql + " ORDER BY DPACIENT.DAPE1PAC, amovprod.fmovimie";

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql, MMovimientosPac.RcAdmision);
					RR = new DataSet();
					tempAdapter_2.Fill(RR);
				}

				if (RR.Tables[0].Rows.Count == 0)
				{
					//ImpresionCambiosProducto = True
					//Exit Function
				}
				else
				{
					foreach (DataRow iteration_row in RR.Tables[0].Rows)
					{

						Lnombrepac = "";
						Lgidenpac = "";
						Lfllegada = "";
						Lfaltplan = "";
						Lfmovimie = "";
						Lproducto_o = "";
						Lproducto_d = "";

						Lnombrepac = Convert.ToString(iteration_row["nombrepac"]);
						Lgidenpac = Convert.ToString(iteration_row["Gidenpac"]);
						Lfllegada = Convert.ToDateTime(iteration_row["fllegada"]).ToString("dd/MM/yyyy HH:mm");
						Lfaltplan = Convert.ToDateTime(iteration_row["faltplan"]).ToString("dd/MM/yyyy HH:mm");
						Lfmovimie = Convert.ToDateTime(iteration_row["ffinmovi"]).ToString("dd/MM/yyyy HH:mm"); //fmovimie
						Lproducto_o = Convert.ToString(iteration_row["dproducto"]).Trim() + "";

						//Buscamos el producto destino
						//UPGRADE_ISSUE: (2068) _rdoColumn object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
						object tempRefParam = iteration_row["ffinmovi"];
						stsql = " SELECT dproduct.dproducto, amovprod.fmovimie, amovprod.ffinmovi" + 
						        " From amovprod, dproduct" + 
						        " Where " + 
						        " amovprod.gproducto = dproduct.gproducto AND " + 
						        " amovprod.ganoregi = " + Convert.ToString(iteration_row["ganoregi"]) + " AND " + 
						        " amovprod.gnumregi = " + Convert.ToString(iteration_row["gnumregi"]) + " AND " + 
						        " amovprod.fmovimie = " + Serrores.FormatFechaHMS(tempRefParam);
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stsql, MMovimientosPac.RcAdmision);
						rr2 = new DataSet();
						tempAdapter_3.Fill(rr2);
						if (rr2.Tables[0].Rows.Count != 0)
						{
							Lproducto_d = Convert.ToString(rr2.Tables[0].Rows[0]["dproducto"]).Trim() + "";
						}
						//UPGRADE_ISSUE: (2064) RDO.rdoResultset method rr2.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
						rr2.Close();
						//----------

						stsql = "INSERT INTO TEMPORAL " + 
						        " ( GIDENPAC, NOMBREPACIENTE, " + 
						        "   FINGRESO, FALTA, FCAMBIO, " + 
						        "   PRODUCTO_OR, PRODUCTO_DE )" + 
						        " VALUES ( " + 
						        " '" + Lgidenpac + "', '" + Lnombrepac + "'," + 
						        " '" + Lfllegada + "', '" + Lfaltplan + "', '" + Lfmovimie + "'," + 
						        " '" + Lproducto_o + "','" + Lproducto_d + "')";
                        //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES
                        //Dbt1.Execute(stsql);

					}
				}
				
				RR.Close();
                //DGMORENOG_TODO_X_4 - IMPLEMENTA DBACCES
                //Dbt1.Close();
				//UPGRADE_TODO: (1067) Member Close is not defined in type Workspace. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				//Wk1.Close();
				return result;
			}
			catch (System.Exception excep)
			{

				RadMessageBox.Show(excep.Message, Application.ProductName);
				return true;
			}
		}
		private void Listados_paciente_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}
 