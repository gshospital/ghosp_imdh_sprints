using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace MovimientosPac
{
	partial class Listados_paciente
	{

		#region "Upgrade Support "
		private static Listados_paciente m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static Listados_paciente DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new Listados_paciente();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "Label5", "Label4", "Frame2", "chbMovimientosProducto", "chbMovimientosEntidad", "sdcDesde", "sdcHasta", "Label3", "Label2", "frmFechas", "chbAusencias", "ChbMovimientos", "ChbTraslados", "Label1", "lbPaciente", "Frame3", "Frame1", "cbCancelar", "cbImprimir"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadTextBox Label5;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadPanel Frame2;
		public Telerik.WinControls.UI.RadCheckBox chbMovimientosProducto;
		public Telerik.WinControls.UI.RadCheckBox chbMovimientosEntidad;
		public Telerik.WinControls.UI.RadDateTimePicker sdcDesde;
		public Telerik.WinControls.UI.RadDateTimePicker sdcHasta;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadGroupBox frmFechas;
		public Telerik.WinControls.UI.RadCheckBox chbAusencias;
		public Telerik.WinControls.UI.RadCheckBox ChbMovimientos;
		public Telerik.WinControls.UI.RadCheckBox ChbTraslados;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbImprimir;
		public System.Windows.Forms.OpenFileDialog CommDiag_impreOpen;
		public System.Windows.Forms.SaveFileDialog CommDiag_impreSave;
		public System.Windows.Forms.FontDialog CommDiag_impreFont;
		public System.Windows.Forms.ColorDialog CommDiag_impreColor;
		public System.Windows.Forms.PrintDialog CommDiag_imprePrint;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame2 = new Telerik.WinControls.UI.RadPanel();
            this.Label5 = new Telerik.WinControls.UI.RadTextBox();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.chbMovimientosProducto = new Telerik.WinControls.UI.RadCheckBox();
            this.chbMovimientosEntidad = new Telerik.WinControls.UI.RadCheckBox();
            this.frmFechas = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcDesde = new Telerik.WinControls.UI.RadDateTimePicker();
            this.sdcHasta = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.chbAusencias = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbMovimientos = new Telerik.WinControls.UI.RadCheckBox();
            this.ChbTraslados = new Telerik.WinControls.UI.RadCheckBox();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbImprimir = new Telerik.WinControls.UI.RadButton();
            this.CommDiag_impreOpen = new System.Windows.Forms.OpenFileDialog();
            this.CommDiag_impreSave = new System.Windows.Forms.SaveFileDialog();
            this.CommDiag_impreFont = new System.Windows.Forms.FontDialog();
            this.CommDiag_impreColor = new System.Windows.Forms.ColorDialog();
            this.CommDiag_imprePrint = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbMovimientosProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbMovimientosEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmFechas)).BeginInit();
            this.frmFechas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAusencias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbMovimientos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbTraslados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.Frame2);
            this.Frame1.Controls.Add(this.chbMovimientosProducto);
            this.Frame1.Controls.Add(this.chbMovimientosEntidad);
            this.Frame1.Controls.Add(this.frmFechas);
            this.Frame1.Controls.Add(this.chbAusencias);
            this.Frame1.Controls.Add(this.ChbMovimientos);
            this.Frame1.Controls.Add(this.ChbTraslados);
            this.Frame1.Controls.Add(this.Frame3);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(2, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(359, 235);
            this.Frame1.TabIndex = 2;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.Label5);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Frame2.Location = new System.Drawing.Point(112, 56);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(236, 25);
            this.Frame2.TabIndex = 16;
            // 
            // Label5
            // 
            this.Label5.AutoSize = false;
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Location = new System.Drawing.Point(108, 5);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(114, 19);
            this.Label5.TabIndex = 18;
            this.Label5.Enabled = false;
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(33, 3);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(65, 18);
            this.Label4.TabIndex = 17;
            this.Label4.Text = "N� episodio";
            // 
            // chbMovimientosProducto
            // 
            this.chbMovimientosProducto.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbMovimientosProducto.Location = new System.Drawing.Point(16, 184);
            this.chbMovimientosProducto.Name = "chbMovimientosProducto";
            this.chbMovimientosProducto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbMovimientosProducto.Size = new System.Drawing.Size(128, 18);
            this.chbMovimientosProducto.TabIndex = 15;
            this.chbMovimientosProducto.Text = "Cambios de Producto";
            this.chbMovimientosProducto.CheckStateChanged += new System.EventHandler(this.chbMovimientosProducto_CheckStateChanged);
            // 
            // chbMovimientosEntidad
            // 
            this.chbMovimientosEntidad.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbMovimientosEntidad.Location = new System.Drawing.Point(16, 136);
            this.chbMovimientosEntidad.Name = "chbMovimientosEntidad";
            this.chbMovimientosEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbMovimientosEntidad.Size = new System.Drawing.Size(120, 18);
            this.chbMovimientosEntidad.TabIndex = 14;
            this.chbMovimientosEntidad.Text = "Cambios de Entidad";
            this.chbMovimientosEntidad.CheckStateChanged += new System.EventHandler(this.chbMovimientosEntidad_CheckStateChanged);
            // 
            // frmFechas
            // 
            this.frmFechas.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmFechas.Controls.Add(this.sdcDesde);
            this.frmFechas.Controls.Add(this.sdcHasta);
            this.frmFechas.Controls.Add(this.Label3);
            this.frmFechas.Controls.Add(this.Label2);
            this.frmFechas.Enabled = false;
            this.frmFechas.HeaderText = "";
            this.frmFechas.Location = new System.Drawing.Point(208, 82);
            this.frmFechas.Name = "frmFechas";
            this.frmFechas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmFechas.Size = new System.Drawing.Size(145, 105);
            this.frmFechas.TabIndex = 9;
            // 
            // sdcDesde
            // 
            this.sdcDesde.CustomFormat = "dd/MM/yyyy";
            this.sdcDesde.Enabled = false;
            this.sdcDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcDesde.Location = new System.Drawing.Point(16, 32);
            this.sdcDesde.Name = "sdcDesde";
            this.sdcDesde.Size = new System.Drawing.Size(113, 20);
            this.sdcDesde.TabIndex = 10;
            this.sdcDesde.TabStop = false;
            this.sdcDesde.Text = "03/05/16";
            this.sdcDesde.Value = new System.DateTime(2016, 5, 3, 16, 17, 43, 402);
            this.sdcDesde.Leave += new System.EventHandler(this.sdcDesde_Leave);
            this.sdcDesde.Closing += new Telerik.WinControls.UI.RadPopupClosingEventHandler(sdcDesde_CloseUp);
            // 
            // sdcHasta
            // 
            this.sdcHasta.CustomFormat = "dd/MM/yyyy";
            this.sdcHasta.Enabled = false;
            this.sdcHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcHasta.Location = new System.Drawing.Point(16, 72);
            this.sdcHasta.Name = "sdcHasta";
            this.sdcHasta.Size = new System.Drawing.Size(113, 20);
            this.sdcHasta.TabIndex = 11;
            this.sdcHasta.TabStop = false;
            this.sdcHasta.Text = "03/05/16";
            this.sdcHasta.Value = new System.DateTime(2016, 5, 3, 16, 17, 43, 421);
            this.sdcHasta.Leave += new System.EventHandler(this.sdcHasta_Leave);
            this.sdcHasta.Closing += new Telerik.WinControls.UI.RadPopupClosingEventHandler(sdcHasta_CloseUp);
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(16, 16);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(75, 18);
            this.Label3.TabIndex = 13;
            this.Label3.Text = "Fecha Desde :";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(16, 56);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(72, 18);
            this.Label2.TabIndex = 12;
            this.Label2.Text = "Fecha Hasta :";
            // 
            // chbAusencias
            // 
            this.chbAusencias.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbAusencias.Location = new System.Drawing.Point(16, 160);
            this.chbAusencias.Name = "chbAusencias";
            this.chbAusencias.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbAusencias.Size = new System.Drawing.Size(70, 18);
            this.chbAusencias.TabIndex = 8;
            this.chbAusencias.Text = "Ausencias";
            this.chbAusencias.CheckStateChanged += new System.EventHandler(this.chbAusencias_CheckStateChanged);
            // 
            // ChbMovimientos
            // 
            this.ChbMovimientos.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbMovimientos.Location = new System.Drawing.Point(16, 84);
            this.ChbMovimientos.Name = "ChbMovimientos";
            this.ChbMovimientos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbMovimientos.Size = new System.Drawing.Size(163, 18);
            this.ChbMovimientos.TabIndex = 7;
            this.ChbMovimientos.Text = "Cambios de Servicio/M�dico";
            this.ChbMovimientos.CheckStateChanged += new System.EventHandler(this.ChbMovimientos_CheckStateChanged);
            // 
            // ChbTraslados
            // 
            this.ChbTraslados.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChbTraslados.Location = new System.Drawing.Point(16, 111);
            this.ChbTraslados.Name = "ChbTraslados";
            this.ChbTraslados.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChbTraslados.Size = new System.Drawing.Size(67, 18);
            this.ChbTraslados.TabIndex = 6;
            this.ChbTraslados.Text = "Traslados";
            this.ChbTraslados.CheckStateChanged += new System.EventHandler(this.ChbTraslados_CheckStateChanged);
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.Controls.Add(this.lbPaciente);
            this.Frame3.HeaderText = "Paciente";
            this.Frame3.Location = new System.Drawing.Point(8, 9);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(345, 45);
            this.Frame3.TabIndex = 3;
            this.Frame3.Text = "Paciente";
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(9, 20);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(48, 18);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "Nombre";
            // 
            // lbPaciente
            // 
            this.lbPaciente.AutoSize = false;
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Location = new System.Drawing.Point(60, 17);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(268, 19);
            this.lbPaciente.TabIndex = 4;
            this.lbPaciente.Enabled = false;
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCancelar.Location = new System.Drawing.Point(276, 241);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(83, 30);
            this.cbCancelar.TabIndex = 1;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbImprimir
            // 
            this.cbImprimir.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbImprimir.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbImprimir.Enabled = false;
            this.cbImprimir.Location = new System.Drawing.Point(187, 241);
            this.cbImprimir.Name = "cbImprimir";
            this.cbImprimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbImprimir.Size = new System.Drawing.Size(83, 30);
            this.cbImprimir.TabIndex = 0;
            this.cbImprimir.Text = "&Imprimir";
            this.cbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbImprimir.Click += new System.EventHandler(this.cbImprimir_Click);
            // 
            // Listados_paciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 273);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbImprimir);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Listados_paciente";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Listados del paciente AGI180F1";
            this.Closed += new System.EventHandler(this.Listados_paciente_Closed);
            this.Load += new System.EventHandler(this.Listados_paciente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbMovimientosProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbMovimientosEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmFechas)).EndInit();
            this.frmFechas.ResumeLayout(false);
            this.frmFechas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAusencias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbMovimientos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChbTraslados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}