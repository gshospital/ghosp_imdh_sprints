using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace DLLPacNoHosp
{
	public partial class DFI162F1
		: RadForm
	{
		string Sql = String.Empty;
		string sqlserv = String.Empty;
		DataSet Rrsqlserv = null;
		string SQLPQUIR = String.Empty;
		DataSet RrSQLPQUIR = null;
		public DFI162F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
            DefInstance = null;
		}

		private void DFI162F1_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				string SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SEGURSOC'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLENTIDAD, MPACNOHOSP.GConexion);
				DataSet RrSQLENTIDAD = new DataSet();
				tempAdapter.Fill(RrSQLENTIDAD);
				//codigo de la SEGURIDAD SOCIAL				
				MPACNOHOSP.CodSS = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
				//texto de la seguridad social				
				MPACNOHOSP.TexSS = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
				SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'PRIVADO'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLENTIDAD, MPACNOHOSP.GConexion);
				RrSQLENTIDAD = new DataSet();
				tempAdapter_2.Fill(RrSQLENTIDAD);
				//codigo de regimen economco privado
				
				MPACNOHOSP.CodPriv = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
				//texto de regimen economco privado
				
				MPACNOHOSP.TexPriv = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
				
				RrSQLENTIDAD.Close();

				string SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SHOMBRE'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLSEXO, MPACNOHOSP.GConexion);
				DataSet RrSEXO = new DataSet();
				tempAdapter_3.Fill(RrSEXO);
				
				MPACNOHOSP.CodSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
				
				MPACNOHOSP.TexSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

				SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SMUJER'";
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLSEXO, MPACNOHOSP.GConexion);
				RrSEXO = new DataSet();
				tempAdapter_4.Fill(RrSEXO);
				
				MPACNOHOSP.CodSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
				
				MPACNOHOSP.TexSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

				SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SINDET'";
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQLSEXO, MPACNOHOSP.GConexion);
				RrSEXO = new DataSet();
				tempAdapter_5.Fill(RrSEXO);
				
				MPACNOHOSP.CodSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
				
				MPACNOHOSP.TexSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();
				
				RrSEXO.Close();

				string SQLPROV = "SELECT * FROM SCONSGLO WHERE GCONSGLO ='CODPROVI'";
				SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQLPROV, MPACNOHOSP.GConexion);
				DataSet RrSPROV = new DataSet();
				tempAdapter_6.Fill(RrSPROV);
				
				MPACNOHOSP.CodPROV = Convert.ToInt32(Double.Parse(Convert.ToString(RrSPROV.Tables[0].Rows[0]["NNUMERI1"]).Trim()));

				string SQLCONT = "SELECT * FROM SCONSGLO WHERE GCONSGLO ='DHISTORI'";
				SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(SQLCONT, MPACNOHOSP.GConexion);
				DataSet RrCONT = new DataSet();
				tempAdapter_7.Fill(RrCONT);
				
				MPACNOHOSP.CODHIST = Convert.ToString(RrCONT.Tables[0].Rows[0]["VALFANU2"]);
				
				MPACNOHOSP.TEXTHIST = Convert.ToString(RrCONT.Tables[0].Rows[0]["VALFANU1"]);
				SQLCONT = "SELECT * FROM SCONSGLO WHERE GCONSGLO ='DPLACAS'";
				SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SQLCONT, MPACNOHOSP.GConexion);
				RrCONT = new DataSet();
				tempAdapter_8.Fill(RrCONT);
				
				MPACNOHOSP.CODPLACA = Convert.ToString(RrCONT.Tables[0].Rows[0]["VALFANU2"]);
				
				MPACNOHOSP.TEXTPLACA = Convert.ToString(RrCONT.Tables[0].Rows[0]["VALFANU1"]);
				this.Cursor = Cursors.WaitCursor;
				//Me.Hide
				string SQLCitas = String.Empty;
				DataSet RrSQLCitas = null;
				string sqlmot = String.Empty;
				DataSet Rrsqlmot = null;

				//Cambiar paciente según corresponda en las opciones de menu
				string HospitalizadoIngresado = String.Empty;

				string HospitalOCentro = Serrores.ObtenerSiHospitalOCentro(MPACNOHOSP.GConexion);
				if (HospitalOCentro.Trim().ToUpper() == "N")
				{
					HospitalizadoIngresado = "ingresados";
				}
				else
				{
					HospitalizadoIngresado = "hospitalizados";
				}

				string LitPersona = Serrores.ObtenerLiteralPersona(MPACNOHOSP.GConexion);
				LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();

				Label6.Text = LitPersona + ":";
				Label21.Text = LitPersona + ":";
				Label29.Text = LitPersona + ":";
				Label31.Text = LitPersona + ":";
				this.Text = "Localización de " + LitPersona.ToLower() + "s no " + HospitalizadoIngresado + " - DFI162F1";

				string SQLSoc = String.Empty;
				DataSet RrSQLSOC = null;
				string sqlSala = String.Empty;
				DataSet RrsqlSala = null;
				string SQLAlta = String.Empty;
				DataSet RrSQLAlta = null;
				bool admision = false;
				bool urgencias = false;
				string mensaje = String.Empty;
				if (MPACNOHOSP.stIdPaciente == "")
				{
					//SI NO TRAE NINGUN PAIENTE QUE MOSTRAR SE CIERRA ESTE FORMULARIO
					cbCerrar_Click(cbCerrar, new EventArgs());
					return;
				}
				else
				{
					//BUSCA EL PACIENTE QUE HA TRAIDO DE FILIACION
					//******************** O.Frias (SQL-2005) ********************
					//'''    Sql = "SELECT * FROM DPACIENT,hdossier Where dpacient.GIDENPAC = '" & stIdPaciente & "' and dpacient.gidenpac *= hdossier.gidenpac"
					Sql = "SELECT * " + 
					      "From DPACIENT " + 
					      "LEFT OUTER JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac " + 
					      "WHERE (DPACIENT.gidenpac = '" + MPACNOHOSP.stIdPaciente + "' )";
					//******************** O.Frias (SQL-2005) ********************

					SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(Sql, MPACNOHOSP.GConexion);
					MPACNOHOSP.RrSQL = new DataSet();
					tempAdapter_9.Fill(MPACNOHOSP.RrSQL);
					if (MPACNOHOSP.RrSQL.Tables[0].Rows.Count != 0)
					{						
						lbIPac.Text = Strings.StrConv(Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DNOMBPAC"]).Trim() + " " + Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DAPE1PAC"]).Trim() + " " + Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DAPE2PAC"]).Trim(), VbStrConv.ProperCase, 0);						
						lbCPRPac.Text = Strings.StrConv(Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DNOMBPAC"]).Trim() + " " + Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DAPE1PAC"]).Trim() + " " + Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DAPE2PAC"]).Trim(), VbStrConv.ProperCase, 0);						
						lbCPEPac.Text = Strings.StrConv(Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DNOMBPAC"]).Trim() + " " + Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DAPE1PAC"]).Trim() + " " + Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DAPE2PAC"]).Trim(), VbStrConv.ProperCase, 0);						
						lbDPac.Text = Strings.StrConv(Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DNOMBPAC"]).Trim() + " " + Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DAPE1PAC"]).Trim() + " " + Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["DAPE2PAC"]).Trim(), VbStrConv.ProperCase, 0);
												
						if (!Convert.IsDBNull(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["NAFILIAC"]))
						{							
							lbINAseg.Text = Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["NAFILIAC"]).Trim();							
							lbCPRNAseg.Text = Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["NAFILIAC"]).Trim();							
							lbCPENAseg.Text = Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["NAFILIAC"]).Trim();							
							lbDNAseg.Text = Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["NAFILIAC"]).Trim();
						}
						//BUSCA LA ENTIDAD FINANCIADORA
						
						if (Convert.ToDouble(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]) == MPACNOHOSP.CodSS)
						{
							lbIentidad.Text = MPACNOHOSP.TexSS;
							lbCPREFinan.Text = MPACNOHOSP.TexSS;
							lbCPEEFinan.Text = MPACNOHOSP.TexSS;
							lbDentidad.Text = MPACNOHOSP.TexSS;
						}
						
						if (Convert.ToDouble(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]) == MPACNOHOSP.CodPriv)
						{
							lbIentidad.Text = MPACNOHOSP.TexPriv;
							lbCPREFinan.Text = MPACNOHOSP.TexPriv;
							lbCPEEFinan.Text = MPACNOHOSP.TexPriv;
							lbDentidad.Text = MPACNOHOSP.TexPriv;
						}
						
						if (Convert.ToDouble(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]) != MPACNOHOSP.CodSS && Convert.ToDouble(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]) != MPACNOHOSP.CodPriv)
						{
							//SI ES SOCIEDAD
							
							SQLSoc = "SELECT DSOCIEDA FROM DSOCIEDA Where GSOCIEDA = " + Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["GSOCIEDA"]);
							SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(SQLSoc, MPACNOHOSP.GConexion);
							RrSQLSOC = new DataSet();
							tempAdapter_10.Fill(RrSQLSOC);
							
							lbIentidad.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["dsocieda"]).Trim();							
							lbCPREFinan.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["dsocieda"]).Trim();							
							lbCPEEFinan.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["dsocieda"]).Trim();							
							lbDentidad.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["dsocieda"]).Trim();							
							RrSQLSOC.Close();
						}
												
						if (!Convert.IsDBNull(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["ghistoria"]))
						{							
							lbIHistoria.Text = Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["ghistoria"]).Trim();							
							lbCPRhist.Text = Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["ghistoria"]).Trim();							
							lbCPEhist.Text = Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["ghistoria"]).Trim();							
							lbDHistoria.Text = Convert.ToString(MPACNOHOSP.RrSQL.Tables[0].Rows[0]["ghistoria"]).Trim();
						}
						//*****************************
						//BUSCA LOS INGRESO PROGRAMADOS
						//*****************************
						System.DateTime tempRefParam = DateTime.Now;
						SQLPQUIR = "SELECT * FROM QPROGQUI Where GIDENPAC = '" + MPACNOHOSP.stIdPaciente + "' and fpreving >= " + Serrores.FormatFecha(tempRefParam) + " and isitprog = 'P' ORDER BY FINTERVE";
						SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(SQLPQUIR, MPACNOHOSP.GConexion);
						RrSQLPQUIR = new DataSet();
						tempAdapter_11.Fill(RrSQLPQUIR);
						if (RrSQLPQUIR.Tables[0].Rows.Count == 0)
						{
							SSTabHelper.SetTabEnabled(tsDatosPac, 0, false);
						}
						else
						{
							sprIngresos.MaxRows = RrSQLPQUIR.Tables[0].Rows.Count;
							
							sprIngresos.Row = 1;
							sprIngresos.OperationMode =  UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
							foreach (DataRow iteration_row in RrSQLPQUIR.Tables[0].Rows)
							{
								sprIngresos.Col = 1;
								
								if (!Convert.IsDBNull(iteration_row["fpreving"]))
								{
									sprIngresos.Text = Convert.ToString(iteration_row["fpreving"]);
								}
								sprIngresos.Col = 2;
								sprIngresos.Text = Convert.ToString(iteration_row["finterve"]);
								sprIngresos.Col = 3;
								//busca el servicio responsable
								sqlserv = "select dnomserv from dservici where gservici = " + Convert.ToString(iteration_row["gservici"]);
								SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(sqlserv, MPACNOHOSP.GConexion);
								Rrsqlserv = new DataSet();
								tempAdapter_12.Fill(Rrsqlserv);
								sprIngresos.Text = Convert.ToString(Rrsqlserv.Tables[0].Rows[0]["dnomserv"]).Trim();
								
								Rrsqlserv.Close();
								if (sprIngresos.Row < sprIngresos.MaxRows || sprIngresos.Row == sprIngresos.MaxRows)
								{
									sprIngresos.Row++;
								}
							}
						}
						
						RrSQLPQUIR.Close();
						//*****************************
						//BUSCA LAS CITAS PROGRAMADAS
						//*****************************

						//******************** O.Frias (SQL-2005) ********************

						//'''        SQLCitas = "SELECT * FROM CCITPROG ,dsalacon, dcodpres Where ccitprog.gIDENPAC = '" & stIdPaciente & "' and ccitprog.gsalacon *= dsalacon.gsalacon and ccitprog.gprestac *= dcodpres.gprestac order by ccitprog.ffeccita desc"

						SQLCitas = "SELECT  * " + 
						           "FROM CCITPROG " + 
						           "LEFT OUTER JOIN DSALACON ON CCITPROG.gsalacon = DSALACON.gsalacon " + 
						           "LEFT OUTER JOIN DCODPRES ON CCITPROG.gprestac = DCODPRES.gprestac " + 
						           "WHERE (CCITPROG.gidenpac = '" + MPACNOHOSP.stIdPaciente + "' ) " + 
						           "ORDER BY CCITPROG.ffeccita DESC ";
						//******************** O.Frias (SQL-2005) ********************

						SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(SQLCitas, MPACNOHOSP.GConexion);
						RrSQLCitas = new DataSet();
						tempAdapter_13.Fill(RrSQLCitas);
						if (RrSQLCitas.Tables[0].Rows.Count == 0)
						{
							SSTabHelper.SetTabEnabled(tsDatosPac, 1, false);
						}
						else
						{
							sprCProg.MaxRows = RrSQLCitas.Tables[0].Rows.Count;							
							sprCProg.Row = 1;
							sprCProg.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
							foreach (DataRow iteration_row_2 in RrSQLCitas.Tables[0].Rows)
							{
								sprCProg.Col = 1;
								sprCProg.Text = Convert.ToDateTime(iteration_row_2["ffeccita"]).ToString("dd/MM/yyyy");
								sprCProg.Col = 2;
								sprCProg.Text = Convert.ToDateTime(iteration_row_2["ffeccita"]).ToString("HH:mm");
								sprCProg.Col = 3;
								
								if (!Convert.IsDBNull(iteration_row_2["gagendas"]))
								{
									sprCProg.Text = proSala_paciente(Convert.ToString(iteration_row_2["gagendas"]));
								}
								//                If Not IsNull(RrSQLCitas("gsalacon")) Then
								//                    sprCProg.Text = Trim(RrSQLCitas("dsalacon"))
								//                Else
								//                    sqlSala = "select  distinct dsalacon.dsalacon  from CCITPROG, " & _
								//'                        "dsalacon, cagendas where ccitprog.gsalacon is null " & _
								//'                        "and cagendas.gagendas = '" & Trim(RrSQLCitas("gagendas")) & "' and cagendas.gsalacon *= dsalacon.gsalacon"
								//                    Set RrsqlSala = GConexion.OpenResultset(sqlSala, rdOpenKeyset)
								//                    If RrsqlSala.RowCount <> 0 Then
								//                        sprCProg.Text = Trim(RrsqlSala("dsalacon"))
								//                    End If
								//                    RrsqlSala.Close
								//                End If
								sprCProg.Col = 4;
								//busca el servicio correspondiente
								
								if (!Convert.IsDBNull(iteration_row_2["gservici"]))
								{
									sqlserv = "select dnomserv from dservici where gservici = " + Convert.ToString(iteration_row_2["gservici"]);
									SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(sqlserv, MPACNOHOSP.GConexion);
									Rrsqlserv = new DataSet();
									tempAdapter_14.Fill(Rrsqlserv);
									sprCProg.Text = Convert.ToString(Rrsqlserv.Tables[0].Rows[0]["dnomserv"]).Trim();
									
									Rrsqlserv.Close();
								}
								else
								{
									sqlserv = "select dservici.dnomserv From " + 
									          " cagendas, dservici where " + 
									          " cagendas.gagendas = '" + Convert.ToString(iteration_row_2["gagendas"]) + "'" + 
									          " and cagendas.gservici = dservici.gservici ";
									SqlDataAdapter tempAdapter_15 = new SqlDataAdapter(sqlserv, MPACNOHOSP.GConexion);
									Rrsqlserv = new DataSet();
									tempAdapter_15.Fill(Rrsqlserv);
									sprCProg.Text = Convert.ToString(Rrsqlserv.Tables[0].Rows[0]["dnomserv"]).Trim();
									
									Rrsqlserv.Close();
								}
								sprCProg.Col = 5;
								
								if (!Convert.IsDBNull(iteration_row_2["dprestac"]))
								{
									sprCProg.Text = Convert.ToString(iteration_row_2["dprestac"]);
								}
								if (sprCProg.Row < sprCProg.MaxRows || sprCProg.Row == sprCProg.MaxRows)
								{
									sprCProg.Row++;
								}
							}
						}
						
						RrSQLCitas.Close();

						//*****************************
						//busca las CITAS PENDIENTES
						//*****************************

						//******************** O.Frias (SQL-2005) ********************
						//'''        SQLCitas = "SELECT * FROM CCITPEND, dsalacon, DCODPRES Where GIDENPAC = '" & stIdPaciente & "' and CCITPEND.gsalacon *= dsalacon.gsalacon AND " & _
						//''''                " CCITPEND.gprestac *= DCODPRES.gprestac ORDER BY CCITPEND.ffeccita DESC"

						SQLCitas = "SELECT * FROM CCITPEND " + 
						           "LEFT OUTER JOIN DSALACON ON CCITPEND.gsalacon = DSALACON.gsalacon " + 
						           "LEFT OUTER JOIN DCODPRES ON CCITPEND.gprestac = DCODPRES.gprestac " + 
						           "WHERE (CCITPEND.gidenpac = '" + MPACNOHOSP.stIdPaciente + "' ) " + 
						           "ORDER BY CCITPEND.ffeccita DESC ";

						//******************** O.Frias (SQL-2005) ********************
						SqlDataAdapter tempAdapter_16 = new SqlDataAdapter(SQLCitas, MPACNOHOSP.GConexion);
						RrSQLCitas = new DataSet();
						tempAdapter_16.Fill(RrSQLCitas);
						if (RrSQLCitas.Tables[0].Rows.Count == 0)
						{
							SSTabHelper.SetTabEnabled(tsDatosPac, 2, false);
						}
						else
						{
							sprCPend.MaxRows = RrSQLCitas.Tables[0].Rows.Count;							
							sprCPend.Row = 1;
							sprCPend.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
							foreach (DataRow iteration_row_3 in RrSQLCitas.Tables[0].Rows)
							{
								sprCPend.Col = 1;
								sprCPend.Text = Convert.ToDateTime(iteration_row_3["ffeccita"]).ToString("dd/MM/yyyy");

								sprCPend.Col = 2;
								sprCPend.Text = Convert.ToDateTime(iteration_row_3["ffeccita"]).ToString("HH:mm");

								sprCPend.Col = 3;
								sprCPend.Text = Convert.ToDateTime(iteration_row_3["FSOLREPR"]).ToString("dd/MM/yyyy HH:mm");

								sprCPend.Col = 4;
								
								if (!Convert.IsDBNull(iteration_row_3["gagendas"]))
								{
									sprCPend.Text = proSala_paciente(Convert.ToString(iteration_row_3["gagendas"]));
								}
								//                If Not IsNull(RrSQLCitas("GSALACON")) Then
								//                sprCPend.Text = RrSQLCitas("DSALACON")
								//                Else
								//                sqlserv = "select dsalacon.dsalacon From cagendas, " & _
								//'                " dsalacon where cagendas.gagendas = '" & RrSQLCitas("gagendas") & "'" & _
								//'                " and dsalacon.gsalacon = cagendas.gsalacon "
								//                Set Rrsqlserv = GConexion.OpenResultset(sqlserv, rdOpenKeyset)
								//                sprCPend.Text = Trim(Rrsqlserv("dsalacon"))
								//                Rrsqlserv.Close
								//                End If
								sprCPend.Col = 5;
								//BUSCA EL SERVICIO CORRESPONDIENTE
								
								if (!Convert.IsDBNull(iteration_row_3["gservici"]))
								{
									sqlserv = "select dnomserv from dservici where gservici = " + Convert.ToString(iteration_row_3["gservici"]);
									SqlDataAdapter tempAdapter_17 = new SqlDataAdapter(sqlserv, MPACNOHOSP.GConexion);
									Rrsqlserv = new DataSet();
									tempAdapter_17.Fill(Rrsqlserv);
									sprCPend.Text = Convert.ToString(Rrsqlserv.Tables[0].Rows[0]["dnomserv"]).Trim();
									
									Rrsqlserv.Close();
								}
								sprCPend.Col = 6;
								sprCPend.Text = Convert.ToString(iteration_row_3["DPRESTAC"]);

								if (sprCPend.Row < sprCPend.MaxRows || sprCPend.Row == sprCPend.MaxRows)
								{
									sprCPend.Row++;
								}
							}
						}
						
						RrSQLCitas.Close();

						//*****************************
						//busca DADO DE ALTA
						//*****************************
						//si esta do de alta en admision
						//SELECCIONA AL PACIENTE Y QUE LA FECHA DE ALTA EN PLANTA ESTE COMPRENDIDA ENTRE LA FECHA ACTUAL Y 30 DIAS ATRAS ORDENADO POR FECHA
						System.DateTime tempRefParam2 = DateTime.Now.AddDays(-30);
						System.DateTime tempRefParam3 = DateTime.Now.AddDays(1);
						SQLAlta = "SELECT * FROM AEPISADM WHERE GIDENPAC = '" + MPACNOHOSP.stIdPaciente + "' AND aepisadm.faltplan between  " + Serrores.FormatFecha(tempRefParam2) + " and " + Serrores.FormatFecha(tempRefParam3) + " order by aepisadm.faltplan ";
						SqlDataAdapter tempAdapter_18 = new SqlDataAdapter(SQLAlta, MPACNOHOSP.GConexion);
						RrSQLAlta = new DataSet();
						tempAdapter_18.Fill(RrSQLAlta);
						if (RrSQLAlta.Tables[0].Rows.Count != 0)
						{
							admision = true;
							
							sprAlta.MaxRows = RrSQLAlta.Tables[0].Rows.Count;
							sprAlta.Row = 1;
							sprAlta.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
							foreach (DataRow iteration_row_4 in RrSQLAlta.Tables[0].Rows)
							{
								sprAlta.Col = 1;
								sprAlta.Text = "Hospitalización";

								sprAlta.Col = 2;
								sprAlta.Text = Convert.ToString(iteration_row_4["fllegada"]);

								sprAlta.Col = 3;
								sprAlta.Text = Convert.ToString(iteration_row_4["faltplan"]);

								sprAlta.Col = 4;
								//BUSCA EL motivo CORRESPONDIENTE
								sqlmot = "select dmotalta from dmotalta where gmotalta = " + Convert.ToString(iteration_row_4["gmotalta"]);
								SqlDataAdapter tempAdapter_19 = new SqlDataAdapter(sqlmot, MPACNOHOSP.GConexion);
								Rrsqlmot = new DataSet();
								tempAdapter_19.Fill(Rrsqlmot);
								sprAlta.Text = Convert.ToString(Rrsqlmot.Tables[0].Rows[0]["dmotalta"]).Trim();
								
								Rrsqlmot.Close();

								if (sprAlta.Row < sprAlta.MaxRows || sprAlta.Row == sprAlta.MaxRows)
								{
									sprAlta.Row++;
								}
							}
						}

						//BUSCA QUE ESTE DADO DE ALTA EN AURGENCIAS
						System.DateTime tempRefParam4 = DateTime.Now.AddDays(-30);
						System.DateTime tempRefParam5 = DateTime.Now.AddDays(1);
						SQLAlta = "SELECT * FROM UEPISURG WHERE GIDENPAC = '" + MPACNOHOSP.stIdPaciente + "' AND UEPISURG.FALTAURG between " + Serrores.FormatFecha(tempRefParam4) + " and " + Serrores.FormatFecha(tempRefParam5) + " order by UEPISURG.FALTAURG";
						SqlDataAdapter tempAdapter_20 = new SqlDataAdapter(SQLAlta, MPACNOHOSP.GConexion);
						RrSQLAlta = new DataSet();
						tempAdapter_20.Fill(RrSQLAlta);
						if (RrSQLAlta.Tables[0].Rows.Count != 0)
						{
							urgencias = true;
							
							sprAlta.Row = sprAlta.MaxRows + 1;
							sprAlta.MaxRows += RrSQLAlta.Tables[0].Rows.Count;
							//sprAlta.Row = sprAlta.Row + 1
							sprAlta.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
							foreach (DataRow iteration_row_5 in RrSQLAlta.Tables[0].Rows)
							{
								sprAlta.Col = 1;
								sprAlta.Text = "Urgencias";

								sprAlta.Col = 2;
								sprAlta.Text = Convert.ToString(iteration_row_5["fllegada"]);

								sprAlta.Col = 3;
								sprAlta.Text = Convert.ToString(iteration_row_5["faltaurg"]);

								sprAlta.Col = 4;
								//BUSCA EL motivo CORRESPONDIENTE
								sqlmot = "select dmotalta from dmotalta where gmotalta = " + Convert.ToString(iteration_row_5["gmotalta"]);
								SqlDataAdapter tempAdapter_21 = new SqlDataAdapter(sqlmot, MPACNOHOSP.GConexion);
								Rrsqlmot = new DataSet();
								tempAdapter_21.Fill(Rrsqlmot);
								sprAlta.Text = Convert.ToString(Rrsqlmot.Tables[0].Rows[0]["dmotalta"]).Trim();
								
								Rrsqlmot.Close();

								if (sprAlta.Row < sprAlta.MaxRows || sprAlta.Row == sprAlta.MaxRows)
								{
									sprAlta.Row++;
								}
							}
						}
						
						RrSQLAlta.Close();

						if (!urgencias && !admision)
						{
							SSTabHelper.SetTabEnabled(tsDatosPac, 3, false);
						}

						if (!SSTabHelper.GetTabEnabled(tsDatosPac, 0))
						{
							SSTabHelper.SetSelectedIndex(tsDatosPac, 1);
							if (!SSTabHelper.GetTabEnabled(tsDatosPac, 1))
							{
								SSTabHelper.SetSelectedIndex(tsDatosPac, 2);
								if (!SSTabHelper.GetTabEnabled(tsDatosPac, 2))
								{
									SSTabHelper.SetSelectedIndex(tsDatosPac, 3);
									if (!SSTabHelper.GetTabEnabled(tsDatosPac, 3))
									{
										SSTabHelper.SetSelectedIndex(tsDatosPac, 0);
										string tempRefParam6 = "";
										short tempRefParam7 = 1520;
										string[] tempRefParam8 = new string[] { "este paciente", "localización de pacientes no hospitalizados"};
										MPACNOHOSP.clasemensaje.RespuestaMensaje(MPACNOHOSP.NombreApli, tempRefParam6, tempRefParam7, MPACNOHOSP.GConexion, tempRefParam8);
                                        cbCerrar_Click(cbCerrar, new EventArgs());
                                    }
								}
							}
						}
					}
					else
					{
						string tempRefParam9 = "";
						short tempRefParam10 = 1100;
                        string[] tempRefParam11 = new string[] { "Paciente"};
						MPACNOHOSP.clasemensaje.RespuestaMensaje(MPACNOHOSP.NombreApli, tempRefParam9, tempRefParam10, MPACNOHOSP.GConexion, tempRefParam11);
						cbCerrar_Click(cbCerrar, new EventArgs());
					}
				}                
                this.Cursor = Cursors.Default;
                //Me.Show vbModal, Me
            }
		}
		
		private void DFI162F1_Load(Object eventSender, EventArgs eventArgs)
		{
			MPACNOHOSP.NombreApli = Serrores.ObtenerNombreAplicacion(MPACNOHOSP.GConexion);            
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
            (tsDatosPac.ViewElement as Telerik.WinControls.UI.RadPageViewStripElement).ShowItemCloseButton = false;
            (tsDatosPac.ViewElement as Telerik.WinControls.UI.RadPageViewStripElement).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;

            _tsDatosPac_TabPage0.Item.BorderColor = System.Drawing.Color.FromArgb(110, 153, 210);
            _tsDatosPac_TabPage1.Item.BorderColor = System.Drawing.Color.FromArgb(110, 153, 210);
            _tsDatosPac_TabPage2.Item.BorderColor = System.Drawing.Color.FromArgb(110, 153, 210);
            _tsDatosPac_TabPage3.Item.BorderColor = System.Drawing.Color.FromArgb(110, 153, 210);
        }
        
        public string proSala_paciente(string codigo)
		{
			string Sala_paciente = String.Empty;
			string sql_descripcion = "select dsalacon.dsalacon from dsalacon, cagendas where cagendas.gagendas = '" + codigo + "' and dsalacon.gsalacon = cagendas.gsalacon ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql_descripcion, MPACNOHOSP.GConexion);
			DataSet Rr_descripcion = new DataSet();
			tempAdapter.Fill(Rr_descripcion);
			if (Rr_descripcion.Tables[0].Rows.Count != 0)
			{				
				Sala_paciente = Convert.ToString(Rr_descripcion.Tables[0].Rows[0]["dsalacon"]).Trim();
			}
			else
			{
				Sala_paciente = "";
			}
			
			Rr_descripcion.Close();
			return Sala_paciente;
		}

		private void DFI162F1_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}