using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLPacNoHosp
{
	partial class DFI162F1
	{

		#region "Upgrade Support "
		private static DFI162F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI162F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI162F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbCerrar", "Label32", "lbINAseg", "lbIentidad", "lbIHistoria", "lbIPac", "Label24", "Label23", "Label6", "sprIngresos", "_tsDatosPac_TabPage0", "sprCProg", "Label21", "Label20", "Label13", "lbCPRPac", "lbCPRhist", "lbCPREFinan", "lbCPRNAseg", "Label1", "_tsDatosPac_TabPage1", "sprCPend", "Label29", "Label25", "Label16", "lbCPEPac", "lbCPEhist", "lbCPEEFinan", "lbCPENAseg", "Label2", "_tsDatosPac_TabPage2", "sprAlta", "Label31", "Label30", "Label19", "lbDPac", "lbDHistoria", "lbDentidad", "lbDNAseg", "Label3", "_tsDatosPac_TabPage3", "tsDatosPac", "sprAlta_Sheet1", "sprCPend_Sheet1", "sprCProg_Sheet1", "sprIngresos_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadLabel Label32;
		public Telerik.WinControls.UI.RadTextBox lbINAseg;
		public Telerik.WinControls.UI.RadTextBox lbIentidad;
		public Telerik.WinControls.UI.RadTextBox lbIHistoria;
		public Telerik.WinControls.UI.RadTextBox lbIPac;
		public Telerik.WinControls.UI.RadLabel Label24;
		public Telerik.WinControls.UI.RadLabel Label23;
		public Telerik.WinControls.UI.RadLabel Label6;
		public UpgradeHelpers.Spread.FpSpread sprIngresos;
		private Telerik.WinControls.UI.RadPageViewPage _tsDatosPac_TabPage0;
		public UpgradeHelpers.Spread.FpSpread sprCProg;
		public Telerik.WinControls.UI.RadLabel Label21;
		public Telerik.WinControls.UI.RadLabel Label20;
		public Telerik.WinControls.UI.RadLabel Label13;
		public Telerik.WinControls.UI.RadTextBox lbCPRPac;
		public Telerik.WinControls.UI.RadTextBox lbCPRhist;
		public Telerik.WinControls.UI.RadTextBox lbCPREFinan;
		public Telerik.WinControls.UI.RadTextBox lbCPRNAseg;
		public Telerik.WinControls.UI.RadLabel Label1;
		private Telerik.WinControls.UI.RadPageViewPage _tsDatosPac_TabPage1;
		public UpgradeHelpers.Spread.FpSpread sprCPend;
		public Telerik.WinControls.UI.RadLabel Label29;
		public Telerik.WinControls.UI.RadLabel Label25;
		public Telerik.WinControls.UI.RadLabel Label16;
		public Telerik.WinControls.UI.RadTextBox lbCPEPac;
		public Telerik.WinControls.UI.RadTextBox lbCPEhist;
		public Telerik.WinControls.UI.RadTextBox lbCPEEFinan;
		public Telerik.WinControls.UI.RadTextBox lbCPENAseg;
		public Telerik.WinControls.UI.RadLabel Label2;
		private Telerik.WinControls.UI.RadPageViewPage _tsDatosPac_TabPage2;
		public UpgradeHelpers.Spread.FpSpread sprAlta;
		public Telerik.WinControls.UI.RadLabel Label31;
		public Telerik.WinControls.UI.RadLabel Label30;
		public Telerik.WinControls.UI.RadLabel Label19;
		public Telerik.WinControls.UI.RadTextBox lbDPac;
		public Telerik.WinControls.UI.RadTextBox lbDHistoria;
		public Telerik.WinControls.UI.RadTextBox lbDentidad;
		public Telerik.WinControls.UI.RadTextBox lbDNAseg;
		public Telerik.WinControls.UI.RadLabel Label3;
		private Telerik.WinControls.UI.RadPageViewPage _tsDatosPac_TabPage3;
		public Telerik.WinControls.UI.RadPageView tsDatosPac;

        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxCl_sprIngresos;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC2_sprIngresos;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC3_sprIngresos;
        
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxCl_sprCProg;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC2_sprCProg;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC3_sprCProg;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC4_sprCProg;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC5_sprCProg;

        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxCl_sprCPend;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC2_sprCPend;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC3_sprCPend;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC4_sprCPend;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC5_sprCPend;
        
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxCl_sprAlta;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC2_sprAlta;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC3_sprAlta;
        public Telerik.WinControls.UI.GridViewTextBoxColumn grvTxC4_sprAlta;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFI162F1));
			this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
			this.cbCerrar = new Telerik.WinControls.UI.RadButton();
			this.tsDatosPac = new Telerik.WinControls.UI.RadPageView();
			this._tsDatosPac_TabPage0 = new Telerik.WinControls.UI.RadPageViewPage();
			this.Label32 = new Telerik.WinControls.UI.RadLabel();
			this.lbINAseg = new Telerik.WinControls.UI.RadTextBox();
			this.lbIentidad = new Telerik.WinControls.UI.RadTextBox();
			this.lbIHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.lbIPac = new Telerik.WinControls.UI.RadTextBox();
			this.Label24 = new Telerik.WinControls.UI.RadLabel();
			this.Label23 = new Telerik.WinControls.UI.RadLabel();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.sprIngresos = new UpgradeHelpers.Spread.FpSpread();
			this._tsDatosPac_TabPage1 = new Telerik.WinControls.UI.RadPageViewPage();
			this.sprCProg = new UpgradeHelpers.Spread.FpSpread();
			this.Label21 = new Telerik.WinControls.UI.RadLabel();
			this.Label20 = new Telerik.WinControls.UI.RadLabel();
			this.Label13 = new Telerik.WinControls.UI.RadLabel();
			this.lbCPRPac = new Telerik.WinControls.UI.RadTextBox();
			this.lbCPRhist = new Telerik.WinControls.UI.RadTextBox();
			this.lbCPREFinan = new Telerik.WinControls.UI.RadTextBox();
			this.lbCPRNAseg = new Telerik.WinControls.UI.RadTextBox();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this._tsDatosPac_TabPage2 = new Telerik.WinControls.UI.RadPageViewPage();
			this.sprCPend = new UpgradeHelpers.Spread.FpSpread();
			this.Label29 = new Telerik.WinControls.UI.RadLabel();
			this.Label25 = new Telerik.WinControls.UI.RadLabel();
			this.Label16 = new Telerik.WinControls.UI.RadLabel();
			this.lbCPEPac = new Telerik.WinControls.UI.RadTextBox();
			this.lbCPEhist = new Telerik.WinControls.UI.RadTextBox();
			this.lbCPEEFinan = new Telerik.WinControls.UI.RadTextBox();
			this.lbCPENAseg = new Telerik.WinControls.UI.RadTextBox();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this._tsDatosPac_TabPage3 = new Telerik.WinControls.UI.RadPageViewPage();
			this.sprAlta = new UpgradeHelpers.Spread.FpSpread();
			this.Label31 = new Telerik.WinControls.UI.RadLabel();
			this.Label30 = new Telerik.WinControls.UI.RadLabel();
			this.Label19 = new Telerik.WinControls.UI.RadLabel();
			this.lbDPac = new Telerik.WinControls.UI.RadTextBox();
			this.lbDHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.lbDentidad = new Telerik.WinControls.UI.RadTextBox();
			this.lbDNAseg = new Telerik.WinControls.UI.RadTextBox();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();

            grvTxCl_sprIngresos = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC2_sprIngresos = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC3_sprIngresos = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            grvTxCl_sprCProg = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC2_sprCProg = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC3_sprCProg = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC4_sprCProg = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC5_sprCProg = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            grvTxCl_sprCPend = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC2_sprCPend = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC3_sprCPend = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC4_sprCPend = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC5_sprCPend = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            grvTxCl_sprAlta = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC2_sprAlta = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC3_sprAlta = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            grvTxC4_sprAlta = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            
            this.tsDatosPac.SuspendLayout();
			this._tsDatosPac_TabPage0.SuspendLayout();
			this._tsDatosPac_TabPage1.SuspendLayout();
			this._tsDatosPac_TabPage2.SuspendLayout();
			this._tsDatosPac_TabPage3.SuspendLayout();
			this.SuspendLayout();
			// 
			// cbCerrar
			// 
			//this.cbCerrar.BackColor = System.Drawing.SystemColors.Control;
			this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cbCerrar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cbCerrar.Location = new System.Drawing.Point(528, 344);
            this.cbCerrar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCerrar.Name = "cbCerrar";
			this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCerrar.Size = new System.Drawing.Size(81, 32);
			this.cbCerrar.TabIndex = 1;
			this.cbCerrar.Text = "Ce&rrar";
			this.cbCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cbCerrar.UseVisualStyleBackColor = false;
			this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
			// 
			// tsDatosPac
			// 
			//this.tsDatosPac.Alignment = System.Windows.Forms.TabAlignment.Top;
			this.tsDatosPac.Controls.Add(this._tsDatosPac_TabPage0);
			this.tsDatosPac.Controls.Add(this._tsDatosPac_TabPage1);
			this.tsDatosPac.Controls.Add(this._tsDatosPac_TabPage2);
			this.tsDatosPac.Controls.Add(this._tsDatosPac_TabPage3);
			this.tsDatosPac.ItemSize = new System.Drawing.Size(149, 18);
			this.tsDatosPac.Location = new System.Drawing.Point(8, 16);
			//this.tsDatosPac.Multiline = true;
			this.tsDatosPac.Name = "tsDatosPac";
			this.tsDatosPac.Size = new System.Drawing.Size(605, 325);
			//this.tsDatosPac.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
			this.tsDatosPac.TabIndex = 0;
            this.tsDatosPac.AllowDrop = false;
            this.tsDatosPac.AllowShowFocusCues = false;
            this.tsDatosPac.SelectedPage = this._tsDatosPac_TabPage0;            
            // 
            // _tsDatosPac_TabPage0
            // 
            this._tsDatosPac_TabPage0.Controls.Add(this.Label32);
			this._tsDatosPac_TabPage0.Controls.Add(this.lbINAseg);
			this._tsDatosPac_TabPage0.Controls.Add(this.lbIentidad);
			this._tsDatosPac_TabPage0.Controls.Add(this.lbIHistoria);
			this._tsDatosPac_TabPage0.Controls.Add(this.lbIPac);
			this._tsDatosPac_TabPage0.Controls.Add(this.Label24);
			this._tsDatosPac_TabPage0.Controls.Add(this.Label23);
			this._tsDatosPac_TabPage0.Controls.Add(this.Label6);
			this._tsDatosPac_TabPage0.Controls.Add(this.sprIngresos);
			this._tsDatosPac_TabPage0.Text = "Ingreso programado";
			// 
			// Label32
			// 
			//this.Label32.BackColor = System.Drawing.SystemColors.Control;
			//this.Label32.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label32.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label32.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label32.Location = new System.Drawing.Point(336, 47);
			this.Label32.Name = "Label32";
			this.Label32.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label32.Size = new System.Drawing.Size(73, 17);
			this.Label32.TabIndex = 6;
			this.Label32.Text = "N� asegurado:";
			// 
			// lbINAseg
			// 
			//this.lbINAseg.BackColor = System.Drawing.SystemColors.Control;
			//this.lbINAseg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbINAseg.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbINAseg.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbINAseg.Location = new System.Drawing.Point(416, 47);
			this.lbINAseg.Name = "lbINAseg";
			this.lbINAseg.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbINAseg.Size = new System.Drawing.Size(153, 19);
			this.lbINAseg.TabIndex = 7;
            this.lbINAseg.Enabled = false;
            // 
            // lbIentidad
            // 
            //this.lbIentidad.BackColor = System.Drawing.SystemColors.Control;
            //this.lbIentidad.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbIentidad.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbIentidad.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbIentidad.Location = new System.Drawing.Point(100, 44);
			this.lbIentidad.Name = "lbIentidad";
			this.lbIentidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbIentidad.Size = new System.Drawing.Size(217, 19);
			this.lbIentidad.TabIndex = 8;
            this.lbIentidad.Enabled = false;
            // 
            // lbIHistoria
            // 
            //this.lbIHistoria.BackColor = System.Drawing.SystemColors.Control;
            //this.lbIHistoria.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbIHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbIHistoria.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbIHistoria.Location = new System.Drawing.Point(496, 12);
			this.lbIHistoria.Name = "lbIHistoria";
			this.lbIHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbIHistoria.Size = new System.Drawing.Size(73, 19);
			this.lbIHistoria.TabIndex = 9;
            this.lbIHistoria.Enabled = false;
            // 
            // lbIPac
            // 
            //this.lbIPac.BackColor = System.Drawing.SystemColors.Control;
            //this.lbIPac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbIPac.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbIPac.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbIPac.Location = new System.Drawing.Point(100, 12);
			this.lbIPac.Name = "lbIPac";
			this.lbIPac.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbIPac.Size = new System.Drawing.Size(327, 19);
			this.lbIPac.TabIndex = 10;
            this.lbIPac.Enabled = false;
            // 
            // Label24
            // 
            //this.Label24.BackColor = System.Drawing.SystemColors.Control;
            //this.Label24.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label24.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label24.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label24.Location = new System.Drawing.Point(8, 44);
			this.Label24.Name = "Label24";
			this.Label24.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label24.Size = new System.Drawing.Size(81, 17);
			this.Label24.TabIndex = 11;
			this.Label24.Text = "E. financiadora:";
			// 
			// Label23
			// 
			//this.Label23.BackColor = System.Drawing.SystemColors.Control;
			//this.Label23.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label23.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label23.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label23.Location = new System.Drawing.Point(432, 12);
			this.Label23.Name = "Label23";
			this.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label23.Size = new System.Drawing.Size(65, 17);
			this.Label23.TabIndex = 12;
			this.Label23.Text = "Hist. cl�nica:";
			// 
			// Label6
			// 
			//this.Label6.BackColor = System.Drawing.SystemColors.Control;
			//this.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label6.Location = new System.Drawing.Point(8, 12);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(73, 17);
			this.Label6.TabIndex = 13;
			this.Label6.Text = "Paciente:";
            // 
            // sprIngresos
            //    
            grvTxCl_sprIngresos.HeaderText = "Fecha prevista ingreso";
            grvTxCl_sprIngresos.Name = "Col_fpreving";
            grvTxCl_sprIngresos.Width = 100;
            grvTxC2_sprIngresos.HeaderText = "Fecha prevista intervenci�n";
            grvTxC2_sprIngresos.Name = "Col_finterve";
            grvTxC2_sprIngresos.Width = 100;
            grvTxC3_sprIngresos.HeaderText = "Servicio Reponsable";
            grvTxC3_sprIngresos.Name = "Col_dnomserv";
            grvTxC3_sprIngresos.Width = 60;

            this.sprIngresos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            this.grvTxCl_sprIngresos,
            grvTxC2_sprIngresos,
            grvTxC3_sprIngresos});
            this.sprIngresos.Location = new System.Drawing.Point(72, 76);
			this.sprIngresos.Name = "sprIngresos";
			this.sprIngresos.Size = new System.Drawing.Size(457, 185);
			this.sprIngresos.TabIndex = 2;
            this.sprIngresos.AutoScroll = true;
            this.sprIngresos.AutoSizeRows = false;
            //this.sprIngresos.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprIngresos.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.FullRowSelect;
            this.sprIngresos.TableElement.RowHeight = 20;
            this.sprIngresos.AllowColumnResize = true;
            // 
            // _tsDatosPac_TabPage1
            // 
            this._tsDatosPac_TabPage1.Controls.Add(this.sprCProg);
			this._tsDatosPac_TabPage1.Controls.Add(this.Label21);
			this._tsDatosPac_TabPage1.Controls.Add(this.Label20);
			this._tsDatosPac_TabPage1.Controls.Add(this.Label13);
			this._tsDatosPac_TabPage1.Controls.Add(this.lbCPRPac);
			this._tsDatosPac_TabPage1.Controls.Add(this.lbCPRhist);
			this._tsDatosPac_TabPage1.Controls.Add(this.lbCPREFinan);
			this._tsDatosPac_TabPage1.Controls.Add(this.lbCPRNAseg);
			this._tsDatosPac_TabPage1.Controls.Add(this.Label1);
			this._tsDatosPac_TabPage1.Text = "Citas programadas";
            // 
            // sprCProg
            // 
            grvTxCl_sprCProg.HeaderText = "Fecha cita";
            grvTxCl_sprCProg.Name = "Col_ffeccita";
            grvTxCl_sprCProg.Width = 50;
            grvTxC2_sprCProg.HeaderText = "Hora";
            grvTxC2_sprCProg.Name = "Col_fhrccita";
            grvTxC2_sprCProg.Width = 40;
            grvTxC3_sprCProg.HeaderText = "Sala consulta";
            grvTxC3_sprCProg.Name = "Col_gagendas";
            grvTxC3_sprCProg.Width = 80;
            grvTxC4_sprCProg.HeaderText = "Servicio";
            grvTxC4_sprCProg.Name = "Col_gservici";
            grvTxC4_sprCProg.Width = 80;
            grvTxC5_sprCProg.HeaderText = "Prestacion";
            grvTxC5_sprCProg.Name = "Col_dprestac";
            grvTxC5_sprCProg.Width = 120;
            this.sprCProg.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            this.grvTxCl_sprCProg,
            grvTxC2_sprCProg,
            grvTxC3_sprCProg,
            grvTxC4_sprCProg,
            grvTxC5_sprCProg});
            this.sprCProg.Location = new System.Drawing.Point(40, 76);
			this.sprCProg.Name = "sprCProg";
			this.sprCProg.Size = new System.Drawing.Size(521, 185);
			this.sprCProg.TabIndex = 3;
            this.sprCProg.AutoScroll = true;            
            this.sprCProg.AutoSizeRows = false;
            //this.sprCProg.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            //this.sprCProg.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.FullRowSelect;
            this.sprCProg.AllowColumnResize = true;
            this.sprCProg.TableElement.RowHeight = 20;
            // 
            // Label21
            // 
            //this.Label21.BackColor = System.Drawing.SystemColors.Control;
            //this.Label21.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label21.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label21.Location = new System.Drawing.Point(8, 12);
			this.Label21.Name = "Label21";
			this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label21.Size = new System.Drawing.Size(49, 17);
			this.Label21.TabIndex = 21;
			this.Label21.Text = "Paciente:";
			// 
			// Label20
			// 
			//this.Label20.BackColor = System.Drawing.SystemColors.Control;
			//this.Label20.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label20.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label20.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label20.Location = new System.Drawing.Point(432, 12);
			this.Label20.Name = "Label20";
			this.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label20.Size = new System.Drawing.Size(65, 17);
			this.Label20.TabIndex = 20;
			this.Label20.Text = "Hist. cl�nica:";
			// 
			// Label13
			// 
			//this.Label13.BackColor = System.Drawing.SystemColors.Control;
			//this.Label13.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label13.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label13.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label13.Location = new System.Drawing.Point(8, 44);
			this.Label13.Name = "Label13";
			this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label13.Size = new System.Drawing.Size(81, 17);
			this.Label13.TabIndex = 19;
			this.Label13.Text = "E. financiadora:";
			// 
			// lbCPRPac
			// 
			//this.lbCPRPac.BackColor = System.Drawing.SystemColors.Control;
			//this.lbCPRPac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbCPRPac.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbCPRPac.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbCPRPac.Location = new System.Drawing.Point(100, 12);
			this.lbCPRPac.Name = "lbCPRPac";
			this.lbCPRPac.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbCPRPac.Size = new System.Drawing.Size(327, 19);
			this.lbCPRPac.TabIndex = 18;
            this.lbCPRPac.Enabled = false;
            // 
            // lbCPRhist
            // 
            //this.lbCPRhist.BackColor = System.Drawing.SystemColors.Control;
            //this.lbCPRhist.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCPRhist.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbCPRhist.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbCPRhist.Location = new System.Drawing.Point(496, 12);
			this.lbCPRhist.Name = "lbCPRhist";
			this.lbCPRhist.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbCPRhist.Size = new System.Drawing.Size(73, 19);
			this.lbCPRhist.TabIndex = 17;
            this.lbCPRhist.Enabled = false;
            // 
            // lbCPREFinan
            // 
            //this.lbCPREFinan.BackColor = System.Drawing.SystemColors.Control;
            //this.lbCPREFinan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCPREFinan.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbCPREFinan.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbCPREFinan.Location = new System.Drawing.Point(100, 44);
			this.lbCPREFinan.Name = "lbCPREFinan";
			this.lbCPREFinan.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbCPREFinan.Size = new System.Drawing.Size(217, 19);
			this.lbCPREFinan.TabIndex = 16;
            this.lbCPREFinan.Enabled = false;
            // 
            // lbCPRNAseg
            // 
            //this.lbCPRNAseg.BackColor = System.Drawing.SystemColors.Control;
            //this.lbCPRNAseg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCPRNAseg.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbCPRNAseg.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbCPRNAseg.Location = new System.Drawing.Point(416, 47);
			this.lbCPRNAseg.Name = "lbCPRNAseg";
			this.lbCPRNAseg.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbCPRNAseg.Size = new System.Drawing.Size(153, 19);
			this.lbCPRNAseg.TabIndex = 15;
            this.lbCPRNAseg.Enabled = false;
            // 
            // Label1
            // 
            //this.Label1.BackColor = System.Drawing.SystemColors.Control;
            //this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Location = new System.Drawing.Point(336, 47);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(73, 17);
			this.Label1.TabIndex = 14;
			this.Label1.Text = "N� asegurado:";
			// 
			// _tsDatosPac_TabPage2
			// 
			this._tsDatosPac_TabPage2.Controls.Add(this.sprCPend);
			this._tsDatosPac_TabPage2.Controls.Add(this.Label29);
			this._tsDatosPac_TabPage2.Controls.Add(this.Label25);
			this._tsDatosPac_TabPage2.Controls.Add(this.Label16);
			this._tsDatosPac_TabPage2.Controls.Add(this.lbCPEPac);
			this._tsDatosPac_TabPage2.Controls.Add(this.lbCPEhist);
			this._tsDatosPac_TabPage2.Controls.Add(this.lbCPEEFinan);
			this._tsDatosPac_TabPage2.Controls.Add(this.lbCPENAseg);
			this._tsDatosPac_TabPage2.Controls.Add(this.Label2);
			this._tsDatosPac_TabPage2.Text = "Citas pendientes";
			// 
			// sprCPend
			// 
            grvTxCl_sprCPend.HeaderText = "Fecha cita";
            grvTxCl_sprCPend.Name = "Col_ffeccita";
            grvTxCl_sprCPend.Width = 50;
            grvTxC2_sprCPend.HeaderText = "Hora";
            grvTxC2_sprCPend.Name = "Col_fhrccita";
            grvTxC2_sprCPend.Width = 40;
            grvTxC3_sprCPend.HeaderText = "Sala consulta";
            grvTxC3_sprCPend.Name = "Col_gagendas";
            grvTxC3_sprCPend.Width = 80;
            grvTxC4_sprCPend.HeaderText = "Servicio";
            grvTxC4_sprCPend.Name = "Col_gservici";
            grvTxC4_sprCPend.Width = 80;
            grvTxC5_sprCPend.HeaderText = "Prestacion";
            grvTxC5_sprCPend.Name = "Col_dprestac";
            grvTxC5_sprCPend.Width = 80;
            this.sprCPend.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            this.grvTxCl_sprCPend,
            grvTxC2_sprCPend,
            grvTxC3_sprCPend,
            grvTxC4_sprCProg,
            grvTxC5_sprCProg});
			this.sprCPend.Location = new System.Drawing.Point(8, 76);
			this.sprCPend.Name = "sprCPend";
			this.sprCPend.Size = new System.Drawing.Size(561, 185);
			this.sprCPend.TabIndex = 4;
            this.sprCPend.AutoScroll = true;
            this.sprCPend.AutoSizeRows = false;
            //this.sprCPend.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprCPend.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.FullRowSelect;
            this.sprCPend.AllowColumnResize = true;
            this.sprCPend.TableElement.RowHeight = 20;
            // 
            // Label29
            // 
            //this.Label29.BackColor = System.Drawing.SystemColors.Control;
            //this.Label29.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label29.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label29.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label29.Location = new System.Drawing.Point(8, 12);
			this.Label29.Name = "Label29";
			this.Label29.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label29.Size = new System.Drawing.Size(49, 17);
			this.Label29.TabIndex = 29;
			this.Label29.Text = "Paciente:";
			// 
			// Label25
			// 
			//this.Label25.BackColor = System.Drawing.SystemColors.Control;
			//this.Label25.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label25.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label25.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label25.Location = new System.Drawing.Point(432, 12);
			this.Label25.Name = "Label25";
			this.Label25.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label25.Size = new System.Drawing.Size(65, 17);
			this.Label25.TabIndex = 28;
			this.Label25.Text = "Hist. cl�nica:";
			// 
			// Label16
			// 
			//this.Label16.BackColor = System.Drawing.SystemColors.Control;
			//this.Label16.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label16.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label16.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label16.Location = new System.Drawing.Point(8, 44);
			this.Label16.Name = "Label16";
			this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label16.Size = new System.Drawing.Size(81, 17);
			this.Label16.TabIndex = 27;
			this.Label16.Text = "E. financiadora:";
			// 
			// lbCPEPac
			// 
			//this.lbCPEPac.BackColor = System.Drawing.SystemColors.Control;
			//this.lbCPEPac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbCPEPac.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbCPEPac.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbCPEPac.Location = new System.Drawing.Point(100, 12);
			this.lbCPEPac.Name = "lbCPEPac";
			this.lbCPEPac.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbCPEPac.Size = new System.Drawing.Size(327, 19);
			this.lbCPEPac.TabIndex = 26;
            this.lbCPEPac.Enabled = false;
            // 
            // lbCPEhist
            // 
            //this.lbCPEhist.BackColor = System.Drawing.SystemColors.Control;
            //this.lbCPEhist.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCPEhist.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbCPEhist.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbCPEhist.Location = new System.Drawing.Point(496, 12);
			this.lbCPEhist.Name = "lbCPEhist";
			this.lbCPEhist.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbCPEhist.Size = new System.Drawing.Size(73, 19);
			this.lbCPEhist.TabIndex = 25;
            this.lbCPEhist.Enabled = false;
            // 
            // lbCPEEFinan
            // 
            //this.lbCPEEFinan.BackColor = System.Drawing.SystemColors.Control;
            //this.lbCPEEFinan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCPEEFinan.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbCPEEFinan.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbCPEEFinan.Location = new System.Drawing.Point(100, 44);
			this.lbCPEEFinan.Name = "lbCPEEFinan";
			this.lbCPEEFinan.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbCPEEFinan.Size = new System.Drawing.Size(217, 19);
			this.lbCPEEFinan.TabIndex = 24;
            this.lbCPEEFinan.Enabled = false;
            // 
            // lbCPENAseg
            // 
            //this.lbCPENAseg.BackColor = System.Drawing.SystemColors.Control;
            //this.lbCPENAseg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCPENAseg.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbCPENAseg.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbCPENAseg.Location = new System.Drawing.Point(416, 47);
			this.lbCPENAseg.Name = "lbCPENAseg";
			this.lbCPENAseg.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbCPENAseg.Size = new System.Drawing.Size(153, 19);
			this.lbCPENAseg.TabIndex = 23;
            this.lbCPENAseg.Enabled = false;
            // 
            // Label2
            // 
            //this.Label2.BackColor = System.Drawing.SystemColors.Control;
            //this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(336, 47);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(73, 17);
			this.Label2.TabIndex = 22;
			this.Label2.Text = "N� asegurado:";
			// 
			// _tsDatosPac_TabPage3
			// 
			this._tsDatosPac_TabPage3.Controls.Add(this.sprAlta);
			this._tsDatosPac_TabPage3.Controls.Add(this.Label31);
			this._tsDatosPac_TabPage3.Controls.Add(this.Label30);
			this._tsDatosPac_TabPage3.Controls.Add(this.Label19);
			this._tsDatosPac_TabPage3.Controls.Add(this.lbDPac);
			this._tsDatosPac_TabPage3.Controls.Add(this.lbDHistoria);
			this._tsDatosPac_TabPage3.Controls.Add(this.lbDentidad);
			this._tsDatosPac_TabPage3.Controls.Add(this.lbDNAseg);
			this._tsDatosPac_TabPage3.Controls.Add(this.Label3);
			this._tsDatosPac_TabPage3.Text = "Dado de alta";
            // 
            // sprAlta
            // 
            grvTxCl_sprAlta.HeaderText = "Area";
            grvTxCl_sprAlta.Name = "Col_Area";
            grvTxCl_sprAlta.Width = 80;
            grvTxC2_sprAlta.HeaderText = "Fecha ingreso";
            grvTxC2_sprAlta.Name = "Col_fllegada";
            grvTxC2_sprAlta.Width = 50;
            grvTxC3_sprAlta.HeaderText = "Fecha alta";
            grvTxC3_sprAlta.Name = "Col_faltplan";
            grvTxC3_sprAlta.Width = 50;
            grvTxC4_sprAlta.HeaderText = "Motivo del alta";
            grvTxC4_sprAlta.Name = "Col_dmotalta";
            grvTxC4_sprAlta.Width = 80;            
            this.sprAlta.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            this.grvTxCl_sprAlta,
            grvTxC2_sprAlta,
            grvTxC3_sprAlta,
            grvTxC4_sprAlta});
            this.sprAlta.Location = new System.Drawing.Point(32, 76);
			this.sprAlta.Name = "sprAlta";
			this.sprAlta.Size = new System.Drawing.Size(537, 185);
			this.sprAlta.TabIndex = 5;
            this.sprAlta.AutoScroll = true;
            this.sprAlta.AutoSizeRows = false;
            //this.sprAlta.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprAlta.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.FullRowSelect;
            this.sprAlta.TableElement.RowHeight = 20;
            this.sprAlta.AllowColumnResize = true;
            // 
            // Label31
            // 
            //this.Label31.BackColor = System.Drawing.SystemColors.Control;
            //this.Label31.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label31.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label31.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label31.Location = new System.Drawing.Point(8, 12);
			this.Label31.Name = "Label31";
			this.Label31.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label31.Size = new System.Drawing.Size(49, 17);
			this.Label31.TabIndex = 37;
			this.Label31.Text = "Paciente:";
			// 
			// Label30
			// 
			//this.Label30.BackColor = System.Drawing.SystemColors.Control;
			//this.Label30.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label30.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label30.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label30.Location = new System.Drawing.Point(432, 12);
			this.Label30.Name = "Label30";
			this.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label30.Size = new System.Drawing.Size(65, 17);
			this.Label30.TabIndex = 36;
			this.Label30.Text = "Hist. cl�nica:";
			// 
			// Label19
			// 
			//this.Label19.BackColor = System.Drawing.SystemColors.Control;
			//this.Label19.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.Label19.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label19.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label19.Location = new System.Drawing.Point(8, 44);
			this.Label19.Name = "Label19";
			this.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label19.Size = new System.Drawing.Size(81, 17);
			this.Label19.TabIndex = 35;
			this.Label19.Text = "E. financiadora:";
			// 
			// lbDPac
			// 
			//this.lbDPac.BackColor = System.Drawing.SystemColors.Control;
			//this.lbDPac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbDPac.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbDPac.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbDPac.Location = new System.Drawing.Point(100, 12);
			this.lbDPac.Name = "lbDPac";
			this.lbDPac.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbDPac.Size = new System.Drawing.Size(327, 19);
			this.lbDPac.TabIndex = 34;
            this.lbDPac.Enabled = false;
            // 
            // lbDHistoria
            // 
            //this.lbDHistoria.BackColor = System.Drawing.SystemColors.Control;
            //this.lbDHistoria.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbDHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbDHistoria.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbDHistoria.Location = new System.Drawing.Point(496, 12);
			this.lbDHistoria.Name = "lbDHistoria";
			this.lbDHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbDHistoria.Size = new System.Drawing.Size(73, 19);
			this.lbDHistoria.TabIndex = 33;
            this.lbDHistoria.Enabled = false;
            // 
            // lbDentidad
            // 
            //this.lbDentidad.BackColor = System.Drawing.SystemColors.Control;
            //this.lbDentidad.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbDentidad.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbDentidad.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbDentidad.Location = new System.Drawing.Point(100, 44);
			this.lbDentidad.Name = "lbDentidad";
			this.lbDentidad.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbDentidad.Size = new System.Drawing.Size(217, 19);
			this.lbDentidad.TabIndex = 32;
            this.lbDentidad.Enabled = false;
            // 
            // lbDNAseg
            // 
            //this.lbDNAseg.BackColor = System.Drawing.SystemColors.Control;
            //this.lbDNAseg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbDNAseg.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbDNAseg.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbDNAseg.Location = new System.Drawing.Point(416, 47);
			this.lbDNAseg.Name = "lbDNAseg";
			this.lbDNAseg.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbDNAseg.Size = new System.Drawing.Size(153, 19);
			this.lbDNAseg.TabIndex = 31;
            this.lbDNAseg.Enabled = false;
            // 
            // Label3
            // 
            //this.Label3.BackColor = System.Drawing.SystemColors.Control;
            //this.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label3.Location = new System.Drawing.Point(336, 47);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(73, 17);
			this.Label3.TabIndex = 30;
			this.Label3.Text = "N� asegurado:";
			// 
			// DFI162F1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(614, 383);
			this.Controls.Add(this.cbCerrar);
			this.Controls.Add(this.tsDatosPac);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location = new System.Drawing.Point(16, 58);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DFI162F1";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Localizaci�n de pacientes no hospitalizados - DFI162F1";
			this.Activated += new System.EventHandler(this.DFI162F1_Activated);
			this.Closed += new System.EventHandler(this.DFI162F1_Closed);
			this.Load += new System.EventHandler(this.DFI162F1_Load);
			this.tsDatosPac.ResumeLayout(false);
			this._tsDatosPac_TabPage0.ResumeLayout(false);
			this._tsDatosPac_TabPage1.ResumeLayout(false);
			this._tsDatosPac_TabPage2.ResumeLayout(false);
			this._tsDatosPac_TabPage3.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}