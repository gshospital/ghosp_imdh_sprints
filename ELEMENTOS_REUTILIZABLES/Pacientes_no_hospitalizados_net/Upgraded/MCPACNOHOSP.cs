using System;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace DLLPacNoHosp
{
	public class MCPACNOHOSP
	{
		public void load(MCPACNOHOSP VAR1, object VAR2, ref SqlConnection Conexion, string stIdPaciente)
		{
			MPACNOHOSP.GConexion = Conexion;
			Serrores.AjustarRelojCliente(Conexion);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			REFERENCIAS(VAR1, VAR2, stIdPaciente);
			DLLPacNoHosp.DFI162F1.DefInstance.ShowDialog();
		}

		public void REFERENCIAS(MCPACNOHOSP Class, object OBJETO, string stIdPac)
		{
			MPACNOHOSP.CLASE = Class;
			MPACNOHOSP.OBJETO2 = OBJETO;
			MPACNOHOSP.stIdPaciente = stIdPac;
		}

		public MCPACNOHOSP()
		{
			MPACNOHOSP.clasemensaje = new Mensajes.ClassMensajes();
		}
	}
}