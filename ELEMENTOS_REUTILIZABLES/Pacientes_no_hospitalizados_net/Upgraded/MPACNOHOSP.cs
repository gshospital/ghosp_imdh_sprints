using System;
using System.Data;
using System.Data.SqlClient;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace DLLPacNoHosp
{
	internal static class MPACNOHOSP
	{

		public static SqlConnection GConexion = null;
		public static DFI162F1 NuevoDFI162F1 = null;
		public static DLLPacNoHosp.MCPACNOHOSP CLASE = null;
		public static object OBJETO2 = null;
		public static DataSet RrSQL = null;
		public static string stIdPaciente = String.Empty;
		public static int CodSS = 0; //codigo de la seguridad social
		public static string TexSS = String.Empty; //texto de la seguridad social
		public static int CodPriv = 0; //codigo de privado
		public static string TexPriv = String.Empty; //texto de privado
		public static string CodSHombre = String.Empty; //codigo de sexo HOMBRE
		public static string TexSHombre = String.Empty; //texto de sexo HOMBRE
		public static string CodSMUJER = String.Empty; //codigo de sexo MUJER
		public static string TexSMUJER = String.Empty; //texto de sexo MUJER
		public static string CodSINDET = String.Empty; //codigo de sexo INDETERMINADO
		public static string TexSINDET = String.Empty; //texto de sexo INDETERMINADO
		public static int CodPROV = 0;
		public static string CODPLACA = String.Empty;
		public static string TEXTPLACA = String.Empty;
		public static string CODHIST = String.Empty;
		public static string TEXTHIST = String.Empty;
		public static Mensajes.ClassMensajes clasemensaje = null;
		public static string NombreApli = String.Empty;
	}
}