using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using System.Security;
using System.Security.Cryptography;
using System.Configuration;
using ElementosCompartidos;

namespace Conexion
{
	public class Obtener_conexion
	{


		private IntPtr WTS_CURRENT_SERVER_HANDLE = IntPtr.Zero;

        //DGMORENOG
        private const string KEY = "IMHHOSPITAL_INDRAC0L0MB1@";
        private static readonly byte[] Salt = Encoding.ASCII.GetBytes("?pt1$8f]l4g80");
        private const Int32 ITERATIONS = 1042;
        public static AesManaged _MyAes;

        public static AesManaged MyAes
        {
            get
            {
                return _MyAes;
            }
            set
            {
                _MyAes = value;
            }
        }
        //DGMORENOG

        private enum WTS_INFO_CLASS
		{
			WTSInitialProgram,
			WTSApplicationName,
			WTSWorkingDirectory,
			WTSOEMId,
			WTSSessionId,
			WTSUserName,
			WTSWinStationName,
			WTSDomainName,
			WTSConnectState,
			WTSClientBuildNumber,
			WTSClientName,
			WTSClientDirectory,
			WTSClientProductId,
			WTSClientHardwareId,
			WTSClientAddress,
			WTSClientDisplay
		}

		//Private Const WTS_CURRENT_SERVER_HANDLE     As Long = 0
		private const int WTS_CURRENT_SESSION = -1;

		public void Conexion(ref SqlConnection Nombre_Conexion, string Letra_Modulo, string Usuario_NT, ref bool Exito_conexion, ref string stUsuario, ref string STpassword, string PstServer = "", string PstDriver = "", string PstBasedatos = "", string PstUsuarioBD = "", string PstPasswordBD = "", string Sincrono = "", string Cursor_Cliente = "", string NombreModulo = "", string StUsuarioAplicacion = "")
		{
            string vstServer = String.Empty;
			string vstDriver = String.Empty;
			string vstBasedat = String.Empty;
			string vstUsuario = String.Empty;
			string vstPassword = String.Empty;
			string vstConexion = String.Empty;
			string tstTimeout = String.Empty;
			DataSet tRrTimeout = null;
			string tstFormato = String.Empty;
			System.DateTime Espera = DateTime.FromOADate(0);
			//Dim tlFor As Long   'variables sistema operativo
			//Dim dl&, s$
            int queryTimeout;

			try
			{
				Modulo_conexion.Vsterror = false;
				if (PstServer == "")
				{
					vstServer = fnQueryRegistro("SERVIDOR");
					if (!Modulo_conexion.Vsterror)
					{
						vstDriver = fnQueryRegistro("DRIVER");
					}
					if (!Modulo_conexion.Vsterror)
					{
						vstBasedat = fnQueryRegistro("BASEDATOS");
					}
					if (!Modulo_conexion.Vsterror)
					{
						vstUsuario = fnQueryRegistro("INICIO");
					}
					if (fnQueryRegistro("INICIO_PWD").Trim() != "")
					{
						if (!Modulo_conexion.Vsterror)
						{
							string tempRefParam = fnQueryRegistro("INICIO_PWD");
							vstPassword = Serrores.General_DesencriptarPassword(ref tempRefParam).Trim();
						}
					}
					else
					{
						if (!Modulo_conexion.Vsterror)
						{
							vstPassword = "INDRA";
						}
					}
				}
				else
				{
					vstServer = PstServer.Trim(); //fnQueryRegistro("SERVIDOR")
					if (!Modulo_conexion.Vsterror)
					{
						vstDriver = PstDriver.Trim();
					} //fnQueryRegistro("DRIVER")
					if (!Modulo_conexion.Vsterror)
					{
						vstBasedat = PstBasedatos.Trim();
					} //fnQueryRegistro("BASEDATOS")
					if (!Modulo_conexion.Vsterror)
					{
						vstUsuario = PstUsuarioBD.Trim();
					} //fnQueryRegistro("INICIO")
					if (!Modulo_conexion.Vsterror)
					{
						vstPassword = PstPasswordBD.Trim();
					} //"Indra"
				}
				if (!Modulo_conexion.Vsterror)
				{

					//vstConexion = "UID=" + vstUsuario + ";PWD=" + vstPassword + ";" + "DATABASE=" + vstBasedat + ";" + "SERVER=" + vstServer + ";" + "DRIVER=" + vstDriver + ";" + " DSN='';";

                    vstConexion = "User ID=" + vstUsuario + ";Password=" + vstPassword + ";" + "Data Source=" + vstServer + ";" + "Initial Catalog=" + vstBasedat + ";" + "Network Library=DBMSSOCN;";
                    if (NombreModulo != "")
					{
						vstConexion = vstConexion + "app=" + NombreModulo + "\\" + StUsuarioAplicacion + ";";
					}

					//   ''''''''''''''''''''''''''''''''''''''''''''''''
					//       Dim vstBase As String
					//       vstBase = Mid(vstBasedat, 1, InStr(1, vstBasedat, "#") - 1)
					//       vstConexion = "UID=" & vstUsuario & ";PWD=" & vstPassword & ";" _
					//'        & "DATABASE=" & vstBase & ";" _
					//'        & "SERVER=" & vstServer & ";" _
					//'        & "DRIVER=" & vstDriver & ";" _
					//'        & " DSN='';"
					//    '''''''''''''''''''''''''''''''''''

                    Nombre_Conexion = new SqlConnection(vstConexion);

					stUsuario = vstUsuario;
					STpassword = vstPassword;

					///************************************
					//obtengo el sistema operativo
					//        myVer.dwOSVersionInfoSize = 148
					//        dl& = GetVersionEx&(myVer)
					//        If myVer.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS Then '95
					//            For tlFor = 0 To 90000000
					//                tlFor = tlFor + 1
					//            Next
					//        ElseIf myVer.dwPlatformId = VER_PLATFORM_WIN32_NT Then 'NT
					//            'continuo normal
					//        End If

					///************************************
					Espera = DateTime.Now;
					//sacamos el tiempo de timeout de la conexion
					
					tstTimeout = "select nnumeri1 from sconsglo where gconsglo='TTIMEOUT'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(tstTimeout, Nombre_Conexion);
					tRrTimeout = new DataSet();
					tempAdapter.Fill(tRrTimeout);
					if (tRrTimeout.Tables[0].Rows.Count != 0)
					{
						if (Convert.ToDouble(tRrTimeout.Tables[0].Rows[0]["nnumeri1"]) >= 30)
						{
                            //Nombre_Conexion.setQueryTimeout(Convert.ToInt32(tRrTimeout.Tables[0].Rows[0]["NNUMERI1"]));
                            queryTimeout = Convert.ToInt32(tRrTimeout.Tables[0].Rows[0]["NNUMERI1"]);
						}
						else
						{
                            //Nombre_Conexion.setQueryTimeout(30);
                            queryTimeout = 30;
						}
					}
					else
					{
						//Nombre_Conexion.setQueryTimeout(30);
                        queryTimeout = 30;
					}
					tRrTimeout.Close();
					//FIN TIMEOUT
					//ponemos el formato de fechas en mmdy
					tstTimeout = "select valfanu3 from sconsglo where gconsglo='FORFECHA'";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstTimeout, Nombre_Conexion);
					tRrTimeout = new DataSet();
					tempAdapter_2.Fill(tRrTimeout);
					if (tRrTimeout.Tables[0].Rows.Count != 0)
					{
						tstFormato = Convert.ToString(tRrTimeout.Tables[0].Rows[0]["valfanu3"]).Trim();

                        Nombre_Conexion.Open();

                        SqlCommand tempCommand = new SqlCommand("SET DATEFORMAT '" + tstFormato + "'", Nombre_Conexion);

                        tempCommand.CommandTimeout = queryTimeout;

                        tempCommand.ExecuteNonQuery();
					}
					tRrTimeout.Close();
				}
				Exito_conexion = !Modulo_conexion.Vsterror;
			}
			catch
			{
				Exito_conexion = false;
			}
		}
        public void ConexionAdminSQL(ref SqlConnection Nombre_Conexion, string Letra_Modulo, string Usuario_NT, ref bool Exito_conexion, string stUsuario, ref string STpassword, string Sincrono = "", string Cursor_Cliente = "")
        {
            string vstServer = String.Empty;
            string vstDriver = String.Empty;
            string vstBasedat = String.Empty;
            string vstConexion = String.Empty;

            try
            {
                Modulo_conexion.Vsterror = false;
                vstServer = fnQueryRegistro("SERVIDOR");
                if (!Modulo_conexion.Vsterror)
                {
                    vstDriver = fnQueryRegistro("DRIVER");
                }
                if (!Modulo_conexion.Vsterror)
                {
                    vstBasedat = fnQueryRegistro("BASEDATOS");
                }

                if (!Modulo_conexion.Vsterror)
                {
                    //vstConexion = "UID=" + stUsuario + ";PWD=" + STpassword + ";" + "DATABASE=" + vstBasedat + ";" + "SERVER=" + vstServer + ";" + "DRIVER=" + vstDriver + ";" + " DSN='';";

                    vstConexion = "User ID=" + stUsuario + ";" + "Password=" + STpassword + ";" + "initial catalog=" + vstBasedat + ";" + "data source=" + vstServer + ";";
                 
                    Nombre_Conexion = new SqlConnection(vstConexion);
                }
                Exito_conexion = !Modulo_conexion.Vsterror;
            }
            catch (SqlException e)
            {
      

                string StMensaje = String.Empty;
                string StTitulo = String.Empty;
        
                if (e.Errors.Count == 0)
                {
                    Exito_conexion = false;
                    return;
                }
                else 
                {
                    //Login failed

                    if (e.Number == Convert.ToInt32(0x00004818))
                    {
                        int puntero = 1;
                        do
                        {
                            StMensaje = "Esta acci�n est� restringida." + "\r" + "Debe indicar la contrase�a del usuario " + "\r" + " SQLServer 'sa'.";

                            StTitulo = "Acci�n restringida: introduzca password";
                            STpassword = Interaction.InputBox(StMensaje, StTitulo, String.Empty, 0, 0);

                            if (STpassword != "")
                            {

                                vstConexion = "UID=" + stUsuario + ";PWD=" + STpassword.Trim() + ";" + "DATABASE=" + vstBasedat + ";" + "SERVER=" + vstServer + ";" + "DRIVER=" + vstDriver + ";" + " DSN='';";
                                try
                                {
                                    Nombre_Conexion = new SqlConnection(vstConexion);
                                    Nombre_Conexion.Open();
                                    puntero = 0;

                                }
                                catch (Exception err)
                                {
                                    puntero = 1;
                                }
                            }
                            else
                            {
                                Exito_conexion = Modulo_conexion.Vsterror;
                            }
                        }
                        while (puntero >= 1);
                    }
                
                }
            }
            catch (Exception exception)
            {
                System.Console.Write(exception.Message);
            }
        }

        #region Registro AppConfig

        static byte[] entropy = System.Text.Encoding.Unicode.GetBytes("Salt Is Not A Password");

        public static string EncryptString(System.Security.SecureString input)
        {
            byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(
                System.Text.Encoding.Unicode.GetBytes(ToInsecureString(input)),
                entropy,
                System.Security.Cryptography.DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedData);
        }

        public static SecureString DecryptString(string encryptedData)
        {
            try
            {
                byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(
                    Convert.FromBase64String(encryptedData),
                    entropy,
                    System.Security.Cryptography.DataProtectionScope.CurrentUser);
                return ToSecureString(System.Text.Encoding.Unicode.GetString(decryptedData));
            }
            catch
            {
                return new SecureString();
            }
        }

        public static SecureString ToSecureString(string input)
        {
            SecureString secure = new SecureString();
            foreach (char c in input)
            {
                secure.AppendChar(c);
            }
            secure.MakeReadOnly();
            return secure;
        }

        public static string ToInsecureString(SecureString input)
        {
            string returnValue = string.Empty;
            IntPtr ptr = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(input);
            try
            {
                returnValue = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(ptr);
            }
            return returnValue;
        }

        public static void KeyGenerator()
        {
            var key = new Rfc2898DeriveBytes(KEY, Salt, ITERATIONS);
            MyAes.Key = key.GetBytes(MyAes.KeySize / 8);
            MyAes.IV = new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        }

        public static string EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an AesManaged object
            // with the specified key and IV.
            KeyGenerator();
            using (MyAes)
            {

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = MyAes.CreateEncryptor(MyAes.Key, MyAes.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream.
            return Convert.ToBase64String(encrypted);
        }

        public static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an AesManaged object
            // with the specified key and IV.
            using (MyAes)
            {
                KeyGenerator();
                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = MyAes.CreateDecryptor(MyAes.Key, MyAes.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }
            return plaintext;
        }

        public static string GetAppConfig(string stSubKey)
        {
            string valor = string.Empty;
            KeyGenerator();

            try
            {
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = @"App.config";
                Configuration config =
                   ConfigurationManager.OpenMappedExeConfiguration(fileMap,
                   ConfigurationUserLevel.None);
                using (MyAes)
                {
                    if (config.AppSettings.Settings[stSubKey] != null)
                    {
                        valor = config.AppSettings.Settings[stSubKey].Value;
                        //SecureString stValor = DecryptString(valor);

                        ////Si luego de hacer DecryptString(valor) la variable stValor queda con longitud 0 
                        ////es porque el valor no estaba encriptado por lo tanto se encripta el valor
                        //if (stValor.Length == 0)
                        //{
                        //    SetValue("", stSubKey, valor, 1);
                        //    config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
                        //    valor = config.AppSettings.Settings[stSubKey].Value;
                        //    stValor = DecryptString(valor);
                        //}

                        //valor = ToInsecureString(stValor);
                        valor = DecryptStringFromBytes_Aes(Convert.FromBase64String(valor), MyAes.Key, MyAes.IV);
                    }
                    else
                    {
                        valor = "Mal leido";
                    }
                }
                return valor;
            }
            catch
            {
                SetValue("", stSubKey, valor, 1);
                return valor;
            }
        }
        

        internal static void SetValue(string skeyname, string Svaluename, string VValueSetting, int lValueType)
        {
            KeyGenerator();
            using (MyAes)
            {
                //DGMORENOG - REEMPLAZO DE ESCRITURA LLAVES REGISTRO WINDOWS
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = @"App.config";
                Configuration config =
                   ConfigurationManager.OpenMappedExeConfiguration(fileMap,
                   ConfigurationUserLevel.None);
                ConfigurationSection mySection = config.GetSection("appSettings");
                mySection.CurrentConfiguration.AppSettings.Settings.Remove(Svaluename);
                config.Save(ConfigurationSaveMode.Full);
                //mySection.CurrentConfiguration.AppSettings.Settings.Add(Svaluename, EncryptString(ToSecureString(VValueSetting)));
                mySection.CurrentConfiguration.AppSettings.Settings.Add(Svaluename, EncryptStringToBytes_Aes(VValueSetting, MyAes.Key, MyAes.IV));
                config.Save(ConfigurationSaveMode.Full);
            }
        }
        

        //Funci�n que obtiene los datos del servidor y del motor de base de datos
        //del registro y los devuelve como STRING para establecer la conexion
        //devolvera error si el par�metro lresult es <>0
        public string fnQueryRegistro(string stSubKey)
		{
            return GetAppConfig(stSubKey);			
		}
        #endregion

        public string NonMaquina()
		{
			StringBuilder result = new StringBuilder();
			FixedLengthString BUF = new FixedLengthString(64);
			int NBUF = 0;
			int X = 0;
			//compruebo si es Terminal Server si devuelve "" no lo es
			string tstName = fnRegistroNonMAquina("CLIENTNAME").Trim();

			if (tstName == "")
			{
				X = 1;
				NBUF = BUF.Value.Length;
				string tempRefParam = BUF.Value;
				NBUF = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetComputerName(ref tempRefParam, ref NBUF);
				BUF.Value = tempRefParam;
				result = new StringBuilder("");
				while (Strings.Asc(BUF.Value.Substring(X - 1, Math.Min(1, BUF.Value.Length - (X - 1)))[0]) != 0)
				{
					result.Append(BUF.Value.Substring(X - 1, Math.Min(1, BUF.Value.Length - (X - 1))));
					X++;
				}
			}
			else
			{
				result = new StringBuilder(tstName);
			}
			return result.ToString().Trim();
		}
		public string FormatoSoloFechaBD(SqlConnection rc)
		{
			string result = String.Empty;
			string sql = "select valfanu1 from sconsglo where gconsglo = 'FECHAS_N'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rc);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			result = Convert.ToString(RR.Tables[0].Rows[0]["valfanu1"]).Trim();
			RR.Close();
			return result;
		}
		public string FormatoFechaHoraMinBD(SqlConnection rc)
		{
			string result = String.Empty;
			string sql = "select valfanu2 from sconsglo where gconsglo = 'FECHAS_N'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rc);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			result = Convert.ToString(RR.Tables[0].Rows[0]["valfanu2"]).Trim();
			RR.Close();
			return result;
		}
		public string FormatoFechaHoraMinSegBD(SqlConnection rc)
		{
			string result = String.Empty;
            string sql = "select valfanu3 from sconsglo where gconsglo = 'FECHAS_N'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, rc);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
            result = Convert.ToString(RR.Tables[0].Rows[0]["valfanu3"]).Trim(); 
			RR.Close();
			return result;
		}

		public string FormatearFecha(string TipoBD, object fecha, string formato)
		{
			//
			//   Recibe como argumento el tipo de B.D. con la que se est� trabajando
			//   una fecha y el formato al que se quiere convertir y devuelve un string que
			//   contiene la fecha (con hora) formateada tal y como la
			//   espera la B.D. con la que se est� trabajando.

			string result = String.Empty;
			switch(TipoBD)
			{
				case "SQLSERVER" : 
					
                    if (Convert.IsDBNull(fecha))
					{
						result = "Null";
					}
					else
					{
						result = "'" + StringsHelper.Format(fecha, formato) + "'";
					} 
					break;
				case "ORACLE" : 
					if (Convert.IsDBNull(fecha))
					{
						result = "Null";
					}
					else
					{
						result = "TO_DATE('" + ConvierteformatoaVB(fecha, formato) + "','" + formato + "')";
					} 
					break;
				case "INFORMIX" : 
					break;
				default:

                    #if DEBUG
                        MessageBox.Show("La variable no contiene un tipo de base de datos reconocible: " + TipoBD, "Mensaje de depuraci�n", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    #endif
                    result = "'" + Convert.ToDateTime(fecha).ToString("MM/dd/yyyy HH:mm") + "'"; 
					break;
			}

			return result;
		}

		public string ObtenerTipoBD()
		{
			string result = String.Empty;
			try
			{
				if (!Modulo_conexion.Vsterror)
				{
					result = fnQueryRegistro("TIPOBD").ToUpper();
				}
				return result;
			}
			catch
			{
				return "SQLSERVER";
			}
		}

		public string fnRegistroNonMAquina(string stSubKey)
		{
			//SE CAMBIA PARA QUE NO SAQUE EL NOMBRE LA MAQUINA CLIENTE DEL REGISTRO
			//CARLOS 2/8/2002
			//*****************************
			//'Dim lresult As Long 'todo va bien si es cero
			//'Dim lValueType As Long  'tipo de dato
			//'Dim strBuf As String  'cadena que contiene el valor requerido
			//'Dim lDataBufSize As Long 'tama�o de la cadena en el registro
			//'Dim hKey As Long   'ubicaci�n de la clave en el registro
			//'Dim tstKeyName  As String 'clave donde estan los datos en el registro
			//'On Error GoTo etiqueta
			//'tstKeyName = "Volatile Environment"
			//'lValueType = REG_SZ
			//'
			//'lresult = RegOpenKeyEx(HKEY_CURRENT_USER, tstKeyName, 0, KEY_QUERY_VALUE, hKey)
			//'lresult = RegQueryValueEx(hKey, stSubKey, 0&, lValueType, ByVal 0&, lDataBufSize)
			//'    If lresult = ERROR_SUCCESS Then
			//'        If lValueType = REG_SZ Then
			//'            strBuf = String(lDataBufSize, " ")
			//'            lresult = RegQueryValueEx(hKey, stSubKey, 0&, 0&, ByVal strBuf, lDataBufSize)
			//'            If lresult = ERROR_SUCCESS Then
			//'                 fnRegistroNonMAquina = Mid$(strBuf, 1, lDataBufSize - 1)
			//'            End If
			//'        End If
			//'    End If
			//'    If lresult <> ERROR_SUCCESS Then
			//'        fnRegistroNonMAquina = ""
			//'    End If
			//'RegCloseKey (hKey)
			//************************************
			string result = String.Empty;
            string sVal = "";
			int lRet = 0;
			int lLen = 0;
			IntPtr lBufferAddress;

			try
			{
                // ralopezn. Indra. 26/02/2016
                // Obtenemos la informaci�n del cliente donde se est� ejecutando la aplicaci�n.
				lRet = UpgradeSupportHelper.PInvoke.SafeNative.wtsapi32
                                           .WTSQuerySessionInformation(WTS_CURRENT_SERVER_HANDLE, 
                                                                       WTS_CURRENT_SESSION, 
                                                                       (int)WTS_INFO_CLASS.WTSClientName, 
                                                                       out lBufferAddress,
                                                                       out lLen);


				if (lRet > 0)
				{ //me piro vampiro
                    UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory(out sVal, lBufferAddress, lLen - 1);
					return sVal.ToString();
				}
				else
				{
					return "";
				}
			}
			catch (Exception e)
			{
				if (e is FileNotFoundException)
				{
					result = "";
				}

				return result;
			}
		}

        private struct sVal
        {
            string val;

            public sVal(char s, int len)
            {
                val = new string(s, len);
            }
        }

		private string ConvierteformatoaVB(object Pfecha, string PFormato)
		{
			string letra = String.Empty;
			string formatoVB = String.Empty;
			int pos = 1;
			for (int I = 1; I <= PFormato.Length; I++)
			{
				letra = PFormato.Substring(I - 1, Math.Min(1, PFormato.Length - (I - 1)));
				switch(letra)
				{
					case "2" : case "4" : 
						letra = ""; 
						break;
					case "M" : 
						if (pos > 8)
						{
							letra = "N";
						} 
						break;
					case "I" : 
						letra = "N"; 
						break;
				}
				formatoVB = formatoVB + letra;
				pos++;
			}
			return StringsHelper.Format(Pfecha, formatoVB);
		}
	}
}