using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace filiacionE
{
	partial class DFI111F4
	{

		#region "Upgrade Support "
		private static DFI111F4 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI111F4 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI111F4();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprDuplicados", "_cmdDuplicados_0", "_cmdDuplicados_1", "Frame1", "cmdDuplicados", "sprDuplicados_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprDuplicados;
		private Telerik.WinControls.UI.RadButton _cmdDuplicados_0;
		private Telerik.WinControls.UI.RadButton _cmdDuplicados_1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton[] cmdDuplicados = new Telerik.WinControls.UI.RadButton[2];
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFI111F4));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.sprDuplicados = new UpgradeHelpers.Spread.FpSpread();
			this._cmdDuplicados_0 = new Telerik.WinControls.UI.RadButton();
			this._cmdDuplicados_1 = new Telerik.WinControls.UI.RadButton();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.sprDuplicados);
			this.Frame1.Controls.Add(this._cmdDuplicados_0);
			this.Frame1.Controls.Add(this._cmdDuplicados_1);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(4, 0);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(595, 173);
			this.Frame1.TabIndex = 0;
			this.Frame1.Visible = true;
            // 
            // sprDuplicados
            // 
            gridViewTextBoxColumn1.HeaderText = "Hist. Cl�nica";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 80;

            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 150;

            gridViewTextBoxColumn3.HeaderText = "F. Nacimiento";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 80;

            gridViewTextBoxColumn4.HeaderText = "D.N.I/N.I.F";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 80;

            gridViewTextBoxColumn5.HeaderText = "Ent. Financiadora";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 120;

            gridViewTextBoxColumn6.HeaderText = "N� Afiliaci�n";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 120;

            gridViewTextBoxColumn7.HeaderText = "N� Tarjeta";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.Width = 100;

            gridViewTextBoxColumn8.HeaderText = "GIDENPAC";
            gridViewTextBoxColumn8.Name = "column8";
            gridViewTextBoxColumn8.Width = 80;

            gridViewTextBoxColumn9.HeaderText = "Cod. Sociedad";
            gridViewTextBoxColumn9.Name = "column9";
            gridViewTextBoxColumn9.Width = 100;
            this.sprDuplicados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[]
           {   gridViewTextBoxColumn1,
                gridViewTextBoxColumn2,
                gridViewTextBoxColumn3,
                gridViewTextBoxColumn4,
                gridViewTextBoxColumn5,
                gridViewTextBoxColumn6,
                gridViewTextBoxColumn7,
                gridViewTextBoxColumn8,
                gridViewTextBoxColumn9
           });
            this.sprDuplicados.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprDuplicados.MasterTemplate.EnableSorting = false;
            this.sprDuplicados.MasterTemplate.ReadOnly = true;
            this.sprDuplicados.Location = new System.Drawing.Point(6, 20);
			this.sprDuplicados.Name = "sprDuplicados";
			this.sprDuplicados.Size = new System.Drawing.Size(580, 105);
			this.sprDuplicados.TabIndex = 1;
            this.sprDuplicados.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(sprDuplicados_CellClick);
            // 
            // _cmdDuplicados_0
            // 
            this._cmdDuplicados_0.Cursor = System.Windows.Forms.Cursors.Default;			
			this._cmdDuplicados_0.Location = new System.Drawing.Point(418, 134);
			this._cmdDuplicados_0.Name = "_cmdDuplicados_0";
			this._cmdDuplicados_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmdDuplicados_0.Size = new System.Drawing.Size(81, 31);
			this._cmdDuplicados_0.TabIndex = 3;
			this._cmdDuplicados_0.Text = "&Aceptar";
			this._cmdDuplicados_0.Click += new System.EventHandler(this.cmdDuplicados_Click);
			// 
			// _cmdDuplicados_1
			// 
			this._cmdDuplicados_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._cmdDuplicados_1.Location = new System.Drawing.Point(506, 134);
			this._cmdDuplicados_1.Name = "_cmdDuplicados_1";
			this._cmdDuplicados_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._cmdDuplicados_1.Size = new System.Drawing.Size(81, 31);
			this._cmdDuplicados_1.TabIndex = 2;
			this._cmdDuplicados_1.Text = "&Cancelar";
			this._cmdDuplicados_1.Click += new System.EventHandler(this.cmdDuplicados_Click);
			// 
			// DFI111F4
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(604, 184);
			this.Controls.Add(this.Frame1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location = new System.Drawing.Point(4, 23);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DFI111F4";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Control de duplicidad.";
			this.Closed += new System.EventHandler(this.DFI111F4_Closed);
			this.Load += new System.EventHandler(this.DFI111F4_Load);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializecmdDuplicados();
		}
		void InitializecmdDuplicados()
		{
			this.cmdDuplicados = new Telerik.WinControls.UI.RadButton[2];
			this.cmdDuplicados[0] = _cmdDuplicados_0;
			this.cmdDuplicados[1] = _cmdDuplicados_1;
		}
		#endregion
	}
}