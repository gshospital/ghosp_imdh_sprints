 using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;

namespace filiacionE
{
    public partial class DFI111F2
        : Telerik.WinControls.UI.RadForm
    {

        public DFI111F2()
            : base()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
        }


        private void DFI111F2_Activated(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
            {
                UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form)eventSender;
            }
        }
        public string stEmpresa = String.Empty;
        bool carga = false;
        private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
        {
            COMPROBAR_DATOS();
            this.Close();
        }

        private void cbbEmpresa_SelectionChangeCommitted(Object eventSender, EventArgs eventArgs)
        {
            cbbSociedad.Items.Clear();
            if (cbbEmpresa.SelectedIndex != -1)
            {
                string SQLEmpresa = "select dsocieda.gsocieda, dsocieda.dsocieda  from " +
                                    "dmutuaem inner join dsocieda on dmutuaem.gsocieda  = dsocieda.gsocieda where " +
                                    "dmutuaem.gcodempr =  " + cbbEmpresa.Items[cbbEmpresa.SelectedIndex].Value.ToString() + " order by dsocieda";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLEmpresa, MFiliacion.GConexion);
                DataSet RrEmpresa = new DataSet();
                tempAdapter.Fill(RrEmpresa);
                RrEmpresa.MoveFirst();
                foreach(DataRow iteration_row in RrEmpresa.Tables[0].Rows)
                {
                    cbbSociedad.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dsocieda"]).Trim(), Convert.ToInt32(iteration_row["gsocieda"])));
                }
                RrEmpresa.Close();
            }
        }

        private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }

        private void DFI111F2_Load(Object eventSender, EventArgs eventArgs)
        {
            cbbEmpresa.Items.Clear();
            SqlDataAdapter tempAdapter = new SqlDataAdapter("select * from dempresa ORDER BY dempresa", MFiliacion.GConexion);
            DataSet RrEmpresa = new DataSet();
            tempAdapter.Fill(RrEmpresa);
            RrEmpresa.MoveFirst();
            foreach (DataRow iteration_row in RrEmpresa.Tables[0].Rows)
            {
                cbbEmpresa.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dempresa"]).Trim(), Convert.ToInt32(iteration_row["gcodempr"])));
            }
            RrEmpresa.Close();
            if (stEmpresa != "")
            {
                for (int I = 0; I <= cbbEmpresa.Items.Count - 1; I++)
                {
                    if (cbbEmpresa.Items[I].Text.Trim().ToUpper() == stEmpresa.Trim().ToUpper())
                    {
                        cbbEmpresa.SelectedIndex = I;
                        break;
                    }
                }
            }
            if (cbbEmpresa.SelectedIndex == -1)
            {
                cbbEmpresa.Text = stEmpresa;
            }
        }

        private void COMPROBAR_DATOS()
        {
            if (cbbEmpresa.SelectedIndex != -1)
            {
                if (cbbSociedad.SelectedIndex != -1)
                {
                    for (int I = 1; I <= DFI111F1.DefInstance.cbbRESociedad.Items.Count - 1; I++)
                    {
                        if (DFI111F1.DefInstance.cbbRESociedad.Items[I].Value.ToString() == cbbSociedad.Items[cbbSociedad.SelectedIndex].Value.ToString())
                        {
                            DFI111F1.DefInstance.cbbRESociedad.SelectedIndex = I;
                            DFI111F1.DefInstance.tbREEmpresa.Text = cbbEmpresa.Text;
                            break;
                        }
                    }
                }
            }
        }
        private void DFI111F2_Closed(Object eventSender, EventArgs eventArgs)
        {
        }
    }
}