using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using System.Web;



namespace filiacionE
{
    internal static class MFiliacion
    {

        //(maplaza)(16/05/2006)Formato utilizado para la m�scara cuando el documento identificativo es un DNI � un NIF
        public const string stFORMATO_NIF = "########?";

        public static bool gMostrarDeudas = false; //oscar 28/10/2004
        public static bool gMostrarFacturacion = false; //oscar 22/11/2004

        public static DialogResult irespuesta = (DialogResult)0;
        public static string NombreApli = String.Empty;
        public static Mensajes.ClassMensajes clasemensaje = null;
        public static string FAyuda = String.Empty;
        public static bool Llamadaopenlab = false;
        //Como al pasar datos del primer formulario al segundo
        //no reconoce el formulario NuevoDFI120F1 en el modulo de
        //clase se defirne como global en un modulo
        public static SqlConnection GConexion = null;
        public static DFI120F1 NuevoDFI120F1 = null;
        public static DataSet RrDPACIENT = null;
        public static DataSet RrSQLUltimo = null;
        public static string stFiliacion = String.Empty;
        public static string stModulo = String.Empty;
        public static string codigo = String.Empty;
        public static bool modificar = false;
        public static string ModCodigo = String.Empty;
        public static object CLASE2 = null; //LA CLASE DE DONDE PROVIENE
        public static dynamic OBJETO2 = null; //EL OBJETO DE DONDE PROVIENE
        public static bool paso = false; //SI PASA A LA SIGUIENTE PANTALLA
        public static string ENTIDAD = String.Empty;
        public static int DestinoPob = 0;
        public static int CodSS = 0; //codigo de la seguridad social
        public static string TexSS = String.Empty; //texto de la seguridad social
        public static int CodPriv = 0; //codigo de privado
        public static string TexPriv = String.Empty; //texto de privado
        public static int ArchivoCentral = 0; //codigo de archivo central
        public static string CodSHombre = String.Empty; //codigo de sexo HOMBRE
        public static string TexSHombre = String.Empty; //texto de sexo HOMBRE
        public static string CodSMUJER = String.Empty; //codigo de sexo MUJER
        public static string TexSMUJER = String.Empty; //texto de sexo MUJER
        public static string CodSINDET = String.Empty; //codigo de sexo INDETERMINADO
        public static string TexSINDET = String.Empty; //texto de sexo INDETERMINADO
        public static string CodPROV = String.Empty;
        public static string sql = String.Empty;
        public static DataSet RrSQL = null; //FILIADOS COINCIDENTES
        public static bool CodPac = false;

        public static string stFORMFILI = String.Empty;
        public static string DNICEDULA = String.Empty;
        public static bool abierto = false;
        public static string Nombre2 = String.Empty; //AL MANDAR LOS DATOS AL FORM ORIGEN
        public static string Apellido1 = String.Empty; //       IDEM
        public static string Apellido2 = String.Empty; //       IDEM
        public static string fechaNac = String.Empty; //       IDEM
        public static string Sexo = String.Empty; //       IDEM
        public static string Domicilio = String.Empty; //       IDEM
        public static string CodPostal = String.Empty; //       IDEM
        public static string codPob = String.Empty;
        public static string Historia = String.Empty;
        public static string NIF = String.Empty; //       IDEM
        public static string Asegurado = String.Empty; //       IDEM
        public static string idPaciente = String.Empty; //       IDEM
        public static string CodigoSoc = String.Empty;
        public static string Filprov = String.Empty;
        public static string IndAcPen = String.Empty;
        public static string Inspec = String.Empty;
        public static string stDevolverDatos = String.Empty;

        public static string CondicionIVA = String.Empty; //GABRIEL 16/09/1999 ARGENTINA - ARGIFA
        public static string TipoDoc = String.Empty; //GABRIEL 22/09/1999 ARGENTINA - ARGIFA
        public static string NumeroDNI = String.Empty; //GABRIEL 22/09/1999 ARGENTINA - ARGIFA

        public static string[] Vocales = null; //ARRAY DE VOCALES Y VOCALES ACENTUADAS
        public static int caracteres = 0; //LITERALES DE APELLIDOS PARA EL GIDENPAC
        public static string cadena = String.Empty; //LITERALES DE APELLIDOS CONVERTIDOS A MAYUSCULAS
        public static string vstFechaHoraSis = String.Empty;
        public static string lSep_Fecha = String.Empty; //SEPARADOR FECHA CONFIGURACION REGIONAL
        public static string lSep_Hora = String.Empty; //SEPARADOR HORA CONFIGURACION REGIONAL
        public static string lFor_fecha = String.Empty; // FORMATO CONFIGURACION REGIONAL FECHA SOLO
        public static string lEsp_For_Siglo = String.Empty;
        public static string LFOR_HORA = String.Empty; //FORMATO DE CONFIGURACION DE HORA
        public static int IFORMATO_FECHA = 0;
        public static string VstFormato_BaseDatos = String.Empty;
        public static string VstSeparadorFecha_BaseDatos = String.Empty;
        public static string VstSeparadorHora_BaseDatos = String.Empty;
        public const int LOCALE_SDATE = 0x1D;
        public const int LOCALE_STIME = 0x1E;
        public const int LOCALE_SSHORTDATE = 0x1F;
        public const int LOCALE_SYSTEM_DEFAULT = 0x800;
        public const int LOCALE_IDATE = 0x21; // entero que devuelve el formato de fecha
        public const int LOCALE_STIMEFORMAT = 0x1003;
        public const int LOCALE_ICENTURY = 0x24; //  especificador de formato de siglo
                                                 //UPGRADE_NOTE: (2041) The following line was commented. More Information: http://www.vbtonet.com/ewis/ewi2041.aspx
                                                 //[DllImport("kernel32.dll", EntryPoint = "GetLocaleInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
                                                 //extern public static int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLCData, int cchData);
        public static string BDPACIENT = String.Empty;

        public const int ERROR_SUCCESS = 0;
        public static string RegresodeOpenlab = String.Empty;
        public static string GApellido1 = String.Empty;
        public static string GApellido2 = String.Empty;
        public static string GNombre = String.Empty;
        public static string GFechaNacimiento = String.Empty;
        public static int GSexo = 0;
        public static string GCipAuto = String.Empty;


        public static bool bCalcLetra = false;
        public static bool bCalcLetraObligatoria = false;
        public static string sUsuario = String.Empty; // para toda interacci�n con la base de datos,
                                                      //  es realmente el 'usuario de aplicaci�n'.
        public static string vstUsuario_NT = String.Empty;
        public const string LETRA_MODULO = "F";
        public static mbAcceso.strControlOculto[] astrControles = null;
        public static mbAcceso.strEventoNulo[] astrEventos = null;

        public static string tstUsuNT_IFMS = String.Empty;
        public static string tstMAquina_IFMS = String.Empty;
        public static string usuario_IFMS = String.Empty;

        public static bool consultaCancelados = false;

        //[DllImport("wtsapi32.dll", EntryPoint = "WTSQuerySessionInformationA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //extern public static int WTSQuerySessionInformation(int hServer, int SessionID, int WTSInfoClass, ref int ppBuffer, ref int lLen);
        //[DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //extern public static void CopyMemory(System.IntPtr hpvDest, System.IntPtr hpvSource, int cbCopy);

        private static IntPtr WTS_CURRENT_SERVER_HANDLE = IntPtr.Zero;
        private const int WTS_CURRENT_SESSION = -1;

        private enum WTS_INFO_CLASS
        {
            WTSInitialProgram,
            WTSApplicationName,
            WTSWorkingDirectory,
            WTSOEMId,
            WTSSessionId,
            WTSUserName,
            WTSWinStationName,
            WTSDomainName,
            WTSConnectState,
            WTSClientBuildNumber,
            WTSClientName,
            WTSClientDirectory,
            WTSClientProductId,
            WTSClientHardwareId,
            WTSClientAddress,
            WTSClientDisplay
        }

        //OSCAR C FEB 2006
        //Indica que se desea filiar a un paciente procedente de RULEQ
        public static bool bProcedentedeRULEQ = false;
        //-----
        //************* O.Frias - 31/07/2007 *************
        public static bool blnUsuWeb = false;
        //************* O.Frias - 23/02/2009 *************
        public static bool blnIMDHOPEN = false;
        public static string FormularioAnterior = String.Empty;

        //-------------------------------------


        internal static void FechaHoraSistema()
        {
            try
            {
                vstFechaHoraSis = DateTimeHelper.ToString(DateTime.Now);
            }

            catch (SqlException ex)

            {


                Serrores.GestionErrorBaseDatos(ex, GConexion); //Falta ver que se hace con el resume
            }
        }

        //' aceptar� una fecha tipo string y devolver� el formato de SQLSERVER
        //' hora_incluida : false si hay que formatear solo la fecha porque despues por ejemplo se le concatena "23:59"
        //' hora_incluida : true si no
        internal static string Formatear_fechahoraAntigua(ref string fecha)
        {
            bool hora = false; // se pondr� a true cuando encuentre el primer blanco -> meter el separador de horas
            StringBuilder stfecha_final = new StringBuilder();
            fecha = StringsHelper.Format(fecha, VstFormato_BaseDatos);
            for (int ibucle = 1; ibucle <= fecha.Length; ibucle++)
            {
                if (Strings.Asc(fecha.Substring(ibucle - 1, Math.Min(1, fecha.Length - (ibucle - 1)))[0]) >= 48 && Strings.Asc(fecha.Substring(ibucle - 1, Math.Min(1, fecha.Length - (ibucle - 1)))[0]) <= 57)
                { // si es  un numero entonces es un separado
                    stfecha_final.Append(fecha.Substring(ibucle - 1, Math.Min(1, fecha.Length - (ibucle - 1))));
                }
                else
                {
                    if (Strings.Asc(fecha.Substring(ibucle - 1, Math.Min(1, fecha.Length - (ibucle - 1)))[0]) == 32)
                    { // si es un espacio en blanco
                        hora = true;
                        stfecha_final.Append(" ");
                    }
                    else
                    {
                        //si es un separador distinguir si es de fechas o de horas
                        if (!hora)
                        {
                            stfecha_final.Append(VstSeparadorFecha_BaseDatos);
                        }
                        else
                        {
                            stfecha_final.Append(VstSeparadorHora_BaseDatos);
                        }
                    }
                }
            }
            return stfecha_final.ToString();
        }

        // meter� el formato, separador de fechas,maxdate.Se llamar� desde el LOAD DE TODAS LAS PANTALLAS
        internal static void Establecer_Propiedades_DateCombo(RadDateTimePicker SsDateCombo)
        {
            ObtenerLocale();
            //UPGRADE_ISSUE: (2064) SSCalendarWidgets_A.SSDateCombo property SsDateCombo.DateSeparator was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx

            // pario TODO _X_5
            /*SsDateCombo.setDateSeparator("/"); //lSep_Fecha*/



            SsDateCombo.CustomFormat = "dd/MM/yyyy"; //lFor_fecha
            lFor_fecha = SsDateCombo.CustomFormat;
          

            // pario TODO _X_5
            /*lSep_Fecha = SsDateCombo.getDateSeparator();*/
            SsDateCombo.MaxDate = DateTime.Parse(StringsHelper.Format(vstFechaHoraSis, lFor_fecha));

            //UPGRADE_ISSUE: (2064) SSCalendarWidgets_A.SSDateCombo property SsDateCombo.ShowCentury was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx

            // pario TODO _X_5
            /*SsDateCombo.setShowCentury(true);*/
            //UPGRADE_ISSUE: (2064) SSCalendarWidgets_A.SSDateCombo property SsDateCombo.NullDateLabel was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
            SsDateCombo.NullableValue = null; // [( " " + lSep_Fecha + " " + lSep_Fecha + " ");
        }
        // Extracts a VB string from a buffer containing a null terminated
        // string
        //UPGRADE_NOTE: (7001) The following declaration (LPSTRToVBString) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
        //private string LPSTRToVBString(string s)
        //{
        //int nullpos = (s.IndexOf(Strings.Chr(0).ToString()) + 1);
        //if (nullpos > 0)
        //{
        //return s.Substring(0, Math.Min(nullpos - 1, s.Length));
        //}
        //else
        //{
        //return "";
        //}
        //}
        // obtiene en lsep_hora y lsep_fecha los separadores
        internal static void ObtenerLocale()
        {
            //'    Dim buffer As String * 100
            //'    Dim dl&
            //'    #If Win32 Then
            //'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDATE, buffer, 99)
            //'        lSep_Fecha = LPSTRToVBString(buffer)
            //'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIME, buffer, 99)
            //'        lSep_Hora = LPSTRToVBString(buffer)
            //'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SSHORTDATE, buffer, 99)
            //'        lFor_fecha = LPSTRToVBString(buffer)
            //'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIMEFORMAT, buffer, 99)
            //'        LFOR_HORA = LPSTRToVBString(buffer)
            //'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ICENTURY, buffer, 99)
            //'        lEsp_For_Siglo = LPSTRToVBString(buffer)
            //'        dl& = GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ICENTURY, buffer, 99)
            //'        IFORMATO_FECHA = LPSTRToVBString(buffer)
            //'    #Else
            //'        Print " Not implemented under Win16"
            //'    #End If
            //'CURAIE - EQUIPO DE MIGRACION - NEUVA FORMA DE ESTABLECER LA CONFIGURACION LOCAL
        }

        internal static void ObtenerValoresTipoBD()
        {
            string formato = String.Empty;
            int i = 0;
            string tstQuery = "SELECT valfanu1,valfanu2 FROM SCONSGLO WHERE gconsglo = 'FORFECHA' ";
            SqlDataAdapter tempAdapter = new SqlDataAdapter(tstQuery, GConexion);
            DataSet tRr = new DataSet();
            tempAdapter.Fill(tRr);
           
            switch (Convert.ToString(tRr.Tables[0].Rows[0]["valfanu2"]).Trim())
            {
                case "SQL SERVER 6.5":
                 
                    VstFormato_BaseDatos = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();  //MM/DD/YYYY HH:NN 
                                                                                                       
                    formato = Convert.ToString(tRr.Tables[0].Rows[0]["valfanu1"]).Trim();
                    for (i = 1; i <= formato.Length; i++)
                    {
                        if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && VstSeparadorFecha_BaseDatos == "")
                        {
                            VstSeparadorFecha_BaseDatos = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
                        }
                        if (VstSeparadorFecha_BaseDatos != "")
                        {
                            break;
                        }
                    }
                    formato = formato.Substring(Strings.InStr(i + 1, formato, VstSeparadorFecha_BaseDatos, CompareMethod.Binary));
                    for (i = 1; i <= formato.Length; i++)
                    {
                        if (formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))) != " " && (Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) <= 65 || Strings.Asc(formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1))).ToUpper()[0]) >= 90) && VstSeparadorHora_BaseDatos == "")
                        {
                            VstSeparadorHora_BaseDatos = formato.Substring(i - 1, Math.Min(1, formato.Length - (i - 1)));
                        }
                        if (VstSeparadorHora_BaseDatos != "")
                        {
                            break;
                        }
                    }
                    break;
            }
          
            tRr.Close();
        }

        internal static string fnQueryRegistro(string stSubKey)
        {
            //'Dim lresult As Long 'todo va bien si es cero
            //'Dim lValueType As Long  'tipo de dato
            //'Dim strBuf As String  'cadena que contiene el valor requerido
            //'Dim lDataBufSize As Long 'tama�o de la cadena en el registro
            //'Dim hKey As Long   'ubicaci�n de la clave en el registro
            //'Dim tstKeyName  As String 'clave donde estan los datos en el registro
            //'tstKeyName = "SOFTWARE\INDRA\GHOSP"
            //'lValueType = REG_SZ
            //'
            //'lresult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, tstKeyName, 0, KEY_QUERY_VALUE, hKey)
            //'lresult = RegQueryValueEx(hKey, stSubKey, 0&, lValueType, ByVal 0&, lDataBufSize)
            //'    If lresult = ERROR_SUCCESS Then
            //'        If lValueType = REG_SZ Then
            //'            strBuf = String(lDataBufSize, " ")
            //'            lresult = RegQueryValueEx(hKey, stSubKey, 0&, 0&, ByVal strBuf, lDataBufSize)
            //'            If lresult = ERROR_SUCCESS Then
            //'                 fnQueryRegistro = Mid$(strBuf, 1, lDataBufSize - 1)
            //'            End If
            //'        End If
            //'    End If
            //'    If lresult <> ERROR_SUCCESS Then
            //'        'MsgBox "Pongase en contacto con inform�tica", vbCritical + vbOKOnly, "Error en conexi�n"
            //'        'End
            //'        fnQueryRegistro = ""
            //'    End If
            //'RegCloseKey (hKey)
            //'CURIAE : EQUIPO DE MIGRACION -- NUEVA FORMA DE OBTENER LOS DATOS DE CONEXION
            return String.Empty;
        }
        internal static string ValidarLetraNif(string DNI)
        {
            return ("TRWAGMYFPDXBNJZSQVHLCKE").Substring(Convert.ToInt32((Convert.ToInt32(Double.Parse(DNI.Substring(0, Math.Min(8, DNI.Length)))) % 23) + 1) - 1, Math.Min(1, ("TRWAGMYFPDXBNJZSQVHLCKE").Length - (Convert.ToInt32((Convert.ToInt32(Double.Parse(DNI.Substring(0, Math.Min(8, DNI.Length)))) % 23) + 1) - 1)));
        }

        internal static string fnRegistroNonMAquina(string stSubKey)
        {
            //SE CAMBIA PARA QUE NO SAQUE EL NOMBRE LA MAQUINA CLIENTE DEL REGISTRO
            string result = String.Empty;
            string sVal = "";
            int lRet = 0;
            int lLen = 0;
            IntPtr lBufferAddress;

            try
            {
                // ralopezn. Indra. 26/02/2016
                // Obtenemos la informaci�n del cliente donde se est� ejecutando la aplicaci�n.
                lRet = UpgradeSupportHelper.PInvoke.SafeNative.wtsapi32
                                           .WTSQuerySessionInformation(WTS_CURRENT_SERVER_HANDLE,
                                                                       WTS_CURRENT_SESSION,
                                                                       (int)WTS_INFO_CLASS.WTSClientName,
                                                                       out lBufferAddress,
                                                                       out lLen);


                if (lRet > 0)
                { //me piro vampiro
                    UpgradeSupportHelper.PInvoke.SafeNative.kernel32.CopyMemory(out sVal, lBufferAddress, lLen - 1);
                    return sVal.ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                if (e is FileNotFoundException)
                {
                    result = "";
                }

                return result;
            }
        }

        internal static string NonMaquina()
        {
            StringBuilder result = new StringBuilder();
            FixedLengthString BUF = new FixedLengthString(64);
            int NBUF = 0;
            int X = 0;
            //compruebo si es Terminal Server si devuelve "" no lo es
            string tstName = fnRegistroNonMAquina("CLIENTNAME").Trim();

            if (tstName == "")
            {
                X = 1;
                NBUF = BUF.Value.Length;
                string tempRefParam = BUF.Value;
                NBUF = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.GetComputerName(ref tempRefParam, ref NBUF);
                BUF.Value = tempRefParam;
                result = new StringBuilder("");
                while (Strings.Asc(BUF.Value.Substring(X - 1, Math.Min(1, BUF.Value.Length - (X - 1)))[0]) != 0)
                {
                    result.Append(BUF.Value.Substring(X - 1, Math.Min(1, BUF.Value.Length - (X - 1))));
                    X++;
                }
            }
            else
            {
                result = new StringBuilder(tstName);
            }
            return result.ToString().Trim();
        }

        internal static string UserName()
        {

            int lSize = 255;
            string sBuffer = new string((char)0x0, lSize);

            int lRV = UpgradeSupportHelper.PInvoke.SafeNative.advapi32.GetUserName(ref sBuffer, ref lSize);

            if (lRV != 0)
            {
                return sBuffer.Substring(0, Math.Min(lSize - 1, sBuffer.Length)); // -1 to remove trailing null character
            }
            else
            {
                return "";
            }
        }

        //(maplaza)(03/05/2006)Esta funci�n dice si la cadena pasada como argumento se corresponde con un DNI/NIF o no.
        //Es decir si la cadena se corresponde con un DNI � un NIF, la funci�n devuelve TRUE. En caso contrario, la
        //funci�n devuelve FALSE. La cadena se corresponde con un DNI si se trata de 8 d�gitos, y se corresponde con
        //un NIF si se trata de de 8 d�gitos y una letra. El DNI se compone de 8 caracteres en BBDD y el NIF se compone
        //de 9 caracteres en Base de Datos.
        //El campo al que se hace referencia en Base de Datos es el campo "NDNINIFP" (Char(9)) de la Tabla DPACIENT.
        //Por si acaso, la longitud (que es 9 actualmente), se pasa como un argumento. Es decir, al argumento "iLongitud"
        //se le pasar� un 9.
        internal static bool EsDNI_NIF(string stCadena, int iLongitud)
        {

            FixedLengthString stCaracter = new FixedLengthString(1);

            //si la longitud de la cadena es menor de 8, no se trata de un DNI ni de un NIF, puesto que en Base de Datos
            //se guarda el DNI con 8 caracteres y el NIF con 9 caracteres (campo "NDNINIFP" (Char(9)) de la Tabla DPACIENT).
            //Tambi�n, por si acaso, se a�ade la condici�n de que la cadena tenga una longitud mayor de 9, en cuyo caso
            //tampoco ser�a un NIF ni un DNI.
            if ((stCadena.Trim().Length < iLongitud - 1) || (stCadena.Trim().Length > iLongitud))
            {
                return false;
            }

            //(16/05/2006)para ver si es NIF/DNI o no, lo mejor es plantearlo del siguiente modo:
            //Caracteres del 1 al 8: Debe ser un d�gito;
            //Car�cter 9: Debe ser una letra (siempre y cuando haya car�cter 9)
            for (int iContador = 1; iContador <= stCadena.Trim().Length; iContador++)
            {
                stCaracter.Value = stCadena.Trim().Substring(iContador - 1, Math.Min(1, stCadena.Trim().Length - (iContador - 1))); //se obtiene el car�cter actual
                if (iContador <= (iLongitud - 1))
                {
                    double dbNumericTemp = 0;
                    if (!Double.TryParse(stCaracter.Value, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
                    {
                        return false;
                    }
                }
                else if (iContador == iLongitud)
                {
                    if (!((Strings.Asc(stCaracter.Value[0]) > 64 && Strings.Asc(stCaracter.Value[0]) < 91) || (Strings.Asc(stCaracter.Value[0]) > 96 && Strings.Asc(stCaracter.Value[0]) < 123)))
                    {
                        return false;
                    }
                }
            }

            //si no se trata de ninguno de los casos anteriores, se trata de un DNI o de un NIF
            return true;


        }

        internal static string GET_HASH(int Length)
        {

            double varNum = 0;
            double varAlpha = 0;
            int i = 0;

            StringBuilder stringNum = new StringBuilder();
            StringBuilder stringAlpha = new StringBuilder();
            StringBuilder stringConcat = new StringBuilder();

            VBMath.Randomize();

            for (i = 0; i <= (Length / ((int)2)); i++)
            {
                varNum = (float)Math.Floor(10 * VBMath.Rnd());
                stringNum.Append(varNum.ToString());
            }

            VBMath.Randomize();

            i = 0;

            do
            {
                varAlpha = (float)Math.Floor(100 * VBMath.Rnd());
                if (varAlpha > 65 && varAlpha < 90)
                {
                    stringAlpha.Append(Strings.Chr(Convert.ToInt32(varAlpha)).ToString());
                    i++;
                }
            }
            while (i < (Length / ((int)2)));


            for (i = 1; i <= (stringNum.ToString() + stringAlpha.ToString()).Length; i++)
            {
                stringConcat.Append(stringNum.ToString().Substring(i - 1, Math.Min(1, stringNum.ToString().Length - (i - 1))) + stringAlpha.ToString().Substring(i - 1, Math.Min(1, stringAlpha.ToString().Length - (i - 1))));
            }

            return stringConcat.ToString();

        }
        //-------------------------------------------------------------------------------------------

        internal static string UrlEncodeUtf8(string strSource)
        {

            string result = String.Empty;
            //MSScriptControl.ScriptControl objSC = null;

            var uriEncoded = Uri.EscapeDataString(strSource);
            var httpUtilityEncoded = Uri.EscapeDataString(strSource);

            try
            {

                //objSC = new MSScriptControl.ScriptControl();
                //objSC.Language = "Jscript";
               
                /// result = Convert.ToString(objSC.CodeObject.encodeURI(strSource));
                result = httpUtilityEncoded;

                //objSC = null;

                return result;
            }
            catch
            {

                return strSource;
            }
        }
    }
}
 