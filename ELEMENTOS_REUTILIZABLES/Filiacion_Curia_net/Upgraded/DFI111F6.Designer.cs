using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace filiacionE
{
	partial class DFI111F6
	{

		#region "Upgrade Support "
		private static DFI111F6 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI111F6 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI111F6();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbbMotivoFiliacion", "cbCancelar", "cbAceptar", "tbComentarioFiliacion", "Label2", "Label1", "cbbIdioma"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
        public Telerik.WinControls.RadToolTip ToolTipMain;
        public UpgradeHelpers.MSForms.MSCombobox cbbMotivoFiliacion;
        public Telerik.WinControls.UI.RadButton cbCancelar;
        public Telerik.WinControls.UI.RadButton cbAceptar;
        public Telerik.WinControls.UI.RadTextBoxControl tbComentarioFiliacion;
        public Telerik.WinControls.UI.RadLabel Label2;
        public Telerik.WinControls.UI.RadLabel Label1;
        public UpgradeHelpers.MSForms.MSCombobox cbbIdioma;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFI111F6));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.cbbMotivoFiliacion = new UpgradeHelpers.MSForms.MSCombobox();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.tbComentarioFiliacion = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.cbbIdioma = new UpgradeHelpers.MSForms.MSCombobox();
			this.SuspendLayout();
			// 
			// cbbMotivoFiliacion
			// 
			this.cbbMotivoFiliacion.CausesValidation = true;
			this.cbbMotivoFiliacion.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbbMotivoFiliacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
			this.cbbMotivoFiliacion.Enabled = true;
			this.cbbMotivoFiliacion.Location = new System.Drawing.Point(84, 12);
			this.cbbMotivoFiliacion.Name = "cbbMotivoFiliacion";
			this.cbbMotivoFiliacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbbMotivoFiliacion.Size = new System.Drawing.Size(301, 21);
			this.cbbMotivoFiliacion.TabIndex = 0;
			this.cbbMotivoFiliacion.TabStop = true;
			this.cbbMotivoFiliacion.Visible = true;
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(300, 112);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 28);
			this.cbCancelar.TabIndex = 4;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Location = new System.Drawing.Point(208, 112);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 28);
			this.cbAceptar.TabIndex = 3;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// tbComentarioFiliacion
			// 
			this.tbComentarioFiliacion.AcceptsReturn = true;
			this.tbComentarioFiliacion.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.tbComentarioFiliacion.Location = new System.Drawing.Point(84, 40);
			this.tbComentarioFiliacion.MaxLength = 255;
			this.tbComentarioFiliacion.Multiline = true;
			this.tbComentarioFiliacion.Name = "tbComentarioFiliacion";
			this.tbComentarioFiliacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tbComentarioFiliacion.Size = new System.Drawing.Size(299, 59);
			this.tbComentarioFiliacion.TabIndex = 2;
			this.tbComentarioFiliacion.Enter += new System.EventHandler(this.tbComentarioFiliacion_Enter);
			this.tbComentarioFiliacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbComentarioFiliacion_KeyPress);
			// 
			// Label2
			// 
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.Location = new System.Drawing.Point(8, 44);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(69, 13);
			this.Label2.TabIndex = 6;
			this.Label2.Text = "Comentarios:";
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(8, 16);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(73, 17);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "Motivo:";
			// 
			// cbbIdioma
			// 
			this.cbbIdioma.Location = new System.Drawing.Point(0, 251);
			this.cbbIdioma.Name = "cbbIdioma";
			this.cbbIdioma.Size = new System.Drawing.Size(209, 23);
			this.cbbIdioma.TabIndex = 1;
			// 
			// DFI111F6
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(388, 151);
			this.ControlBox = false;
			this.Controls.Add(this.cbbMotivoFiliacion);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.tbComentarioFiliacion);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.cbbIdioma);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DFI111F6";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Motivo de Filiación - DFI111F6";
			this.Closed += new System.EventHandler(this.DFI111F6_Closed);
			this.Load += new System.EventHandler(this.DFI111F6_Load);
			this.ResumeLayout(false);
		}
		#endregion
	}
}