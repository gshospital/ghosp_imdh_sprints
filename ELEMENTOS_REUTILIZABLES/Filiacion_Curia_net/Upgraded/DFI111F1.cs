using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using ElementosCompartidos;
using UpgradeSupportHelper;
using System.Reflection;
using System.Transactions;
using Microsoft.CSharp;

namespace filiacionE
{
	public partial class DFI111F1
		: RadForm
	{
		private const string CARACTER_NUMERO = "N";
		private const string CARACTER_LETRA = "L";

        private SqlDataAdapter tempAdapterPaciente;
        private SqlDataAdapter tempAdapterRrDENTIPAC;
        private SqlDataAdapter tempAdapterRrGiden1;
        private SqlDataAdapter tempAdapterRrDPRIVADO;
        private SqlDataAdapter tempAdapterRsContactos;

        private bool bCambiandoMascara = false;

		string miDNINIF = String.Empty; //OSCAR C
        Object DbNullValue = DBNull.Value;
		int intEstado = 0;
		int i = 0;
		DataSet RrDENTIPAC = null;
		DataSet RrDPRIVADO = null;
		DataSet RrDPERSCON = null;
		DataSet RrDSOCIEDA = null;
		DataSet RrDHOSPITA = null;
		DataSet RrTemporal = null;
		DataSet RrDPROVINCIA = null;
		DataSet RrDPOBLACION = null;
		DataSet RrCodPostal = null;
		DataSet RrDestaciv = null;
		DataSet RrDpais = null;
		DataSet RrCatSoc = null;
		bool DatosCorrectos = false;
		bool EncontradoPrivado = false;
		bool SEGSOCIAL = false;
		bool CSOCIEDAD = false;
		DataSet Rrsqlhist = null;
		string CodPostal = String.Empty;
		bool TPoblacion = false;
		public string SQLSOC = String.Empty;
		public DataSet RrSQLSOC = null;
		public string Codigoprov1 = String.Empty;
		public string Codigoprov2 = String.Empty;
		public string Codigoprov3 = String.Empty;
		public string Codigoprov4 = String.Empty;
		string VApellido1 = String.Empty;
		string VApellido2 = String.Empty;
		string Vfecha_nacimiento = String.Empty;
		string VSexo_persona = String.Empty;
		public bool cambiar_identificador = false;
		public bool Permitir_cambiar_Gidenpac = false;
		string Gidensql = String.Empty;
		DataSet RrGiden = null;
		string Gidensql1 = String.Empty;
		DataSet RrGiden1 = null;
		string Tablassql = String.Empty;
		DataSet RrTablas = null;
		string Tabla = String.Empty;
		string NIF_Antiguo = String.Empty;
		bool bcbaceptar = false;
		bool bcargado_privado = false;
		bool bcambiar = false;
		bool bcambiarf = false;
		bool bNocambiarCP_RE = false; //(maplaza)(31/05/2006)
		string stCodigoPostal = String.Empty;
		string stCodigoPostalf = String.Empty;
		int iFilaContactos = 0;
		int iUltimoNOrden = 0;
		bool estabuscando = false;
		string IdiomaDelPaciente = String.Empty;
		string valor_tarjeta = String.Empty;
		string Valor_tarjeta_ant = String.Empty;
		string stComienzoBanda = String.Empty;
		int iNumeroBandas = 0;
		int CodigoDeControl = 0;
		int Algoritmo = 0;
		string BinPan = String.Empty;
		bool bValidarRegimenEconomico = false; //(maplaza)(08/08/2007)
		bool bValidarAutorizacionDatos = false;
		string stMensajeValidarAutorizacionDatos = String.Empty;
		bool bFormatoCodigoPos = false;

		//******************** O.Frias (14/12/2006) **************************
		//*  Variable de control de llamada al procedimiento de duplicados.
		public bool blnDuplicados = false;
		//*  Variable de control de llamada al procedimiento de campos obligatorios.
		public bool blnRequeridos = false;

        public bool flagDbClick = false;
        //(maplaza)(23/04/2007)
        Mensajes.ClassMensajes clase_mensaje = null;
		//-----
		private SqlConnection _cnConexDos = null;
		public DFI111F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}

		SqlConnection cnConexDos
		{
			get
			{
				if (_cnConexDos == null)
				{
					_cnConexDos = new SqlConnection();
				}
				return _cnConexDos;
			}
			set
			{
				_cnConexDos = value;
			}
		}

		string stCIAS = String.Empty;
		string TitularBeneficiario = String.Empty;

		//O.Frias - 24/02/2009
		//New funcionalidad de IMDHOPEN
		//[StructLayout(LayoutKind.Sequential)]
		//public struct POINTAPI
		//{
			//public int X;
			//public int Y;
		//};
		//[DllImport("user32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static short GetAsyncKeyState(int vKey);
		//[DllImport("user32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		//extern public static int GetCursorPos(ref Filiacion_curiaSupport.PInvoke.UnsafeNative.Structures.POINTAPI lpPoint);
		public int TECLAFUNCION = 0;

		bool CipAutonomico = false;
		bool CiasProfesional = false;
		string NumCiasProfesional = String.Empty;

		static readonly Color colorSinSeleccion = SystemColors.Control;
		static readonly Color colorConSeleccion = Color.FromArgb(192, 255, 255);

		//Oscar C Mayo 2010
		bool bPacienteIdBas = false; //--Paciente cumple regla de identificacion basica?
		bool bControlIDBas = false; //--Esta el control de regla de identificacion basica activo?
		//Motivo de Filiacion cuando el paciente no cumple con el criterio de identificacion basica
		public bool bMotivoFiliacion = false;
		public int iMotivoFiliacion = 0;
		public string oMotivoFiliacion = String.Empty;
		//-------------------------------------
		string stSocAntigua = String.Empty;

		//(maplaza)(23/08/2007)Esta funci�n dice si en el control cuyo nombre se pasa como argumento, hay contenido o no.
		private bool fRellenadoCampo(string strNombreControl)
		{
			bool result = false;
			bool bRelleno = false;
			bool bControlEncontrado = false;

			foreach (Control Control in ContainerHelper.Controls(this))
			{
				if (strNombreControl.ToUpper() == Control.Name.ToUpper())
				{
					bControlEncontrado = true;
					if (Control is RadTextBoxControl)
					{
						if (Convert.ToString(Control.Text).Trim() != "")
						{
							bRelleno = true;
						}
					}
					else if ((Control is UpgradeHelpers.MSForms.MSCombobox))
					{ 
						if (Convert.ToString(Control.Text).Trim() != "")
						{
							bRelleno = true;
						}
					}
					else if ((Control is RadMaskedEditBox))
					{ 
						if ((Convert.ToString(Control.Text).Trim() != "/") && (Convert.ToString(Control.Text).Trim() != ""))
						{
							bRelleno = true;
						}
					}
					else if ((Control is RadDateTimePicker))
					{ 
						if (Control.Text != null)
						{
							bRelleno = true;
						}
					}
					else if ((Control is Label))
					{ 
						if (Control.Text != "")
						{
							bRelleno = true;
						}
					}
					else
					{
						//para el caso (por ejemplo) de los check y los option button
						bRelleno = true;
					}

					if (bRelleno)
					{
						result = true;
					}

					break;
				}
			}

			//si no se ha encontrado el control en el formulario
			if (!bControlEncontrado)
			{
				result = true;
			}
			return result;
		}

		//Oscar C Mayo 2010
		//Nueva validacion de datos de Indentificacion Basica de CAPIO: Requerimiento 995
		//
		//    4. Control de  Identificacion Basica
		//        Como esto es un nuevo requerimiento CAPIO para controlar y auditar que pacientes que cumplen o no con los requisitos de Indentidicacion basica, solamente se tendra en cuenta dichas validaciones cuando la constante global 'CTRIDBAS.VALFANU1'='S':
		//             - A) El Nombre debera ser obligatorio
		//             - B) Al menos se deberia cumplimentar uno de los siguientes campos: NIF/Otros Documentos, pasaporte, tarjeta sanitaria, tarjeta europea , cip autonomico. En caso de rellenarse, debera ser unico.
		//             - Si no se cumple la validacion de Identificacion basica del paciente (A+B), se debera comprobar el perfil de usuario (SUSUARIO.ipasafil) para ver si se permite la filiacion del paciente aun no
		//                    cumpliendo las validaciones de la identificacion  basica:
		//                    * Si el usuario tiene el campo ipasafil='N', no se debera permitir continuar con el proceso de filiacion / modificacion de pacientes indicando motivo mediante mensaje de advertencia.
		//                    * Si el usuario tiene el campo ipasafil='S', se permitira continuar con el proceso de filiacion / modificacion de pacientes mediante pregunta indicando el motivo de de dicha pregunta.
		//                            - Si se contesta negativamente, no se coninuara con el proceso de filiacion / modificacion de datos del paciente.
		//                            - Si se contesta afirmativiamente, aparecera una nueva pantalla donde se recogera el motivo de filiacion (obligatorio) y los comentarios del usuarios que se almacenaran en la tabla de pacientes,
		//                                marcandose dicho paciente como Paciente SIN identificacion Basica (DPACIENT.ifilbas='N')
		//            - Si se cumple con la validacion de Identificacion basica del paciente (A+B), el paciente quedara marcado como Paciente CON identificacion Basica (DPACIENT.ifilbas='S')
		//
		//TRUE--> Se Cumple Con el requerimiento de Identificacion Basica
		//FALSE--> No se cumple con el requerimiento de Identificacion Basica
		private bool proControlIndetificacionBasica()
		{
			bool result = false;
			string sql = String.Empty;
			DataSet rrAux = null;
			DialogResult irespuesta = (DialogResult) 0;
			string stMensaje = "";
			result = true;
			bControlIDBas = false;
			if (Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "CTRIDBAS", "VALFANU1") == "S")
			{
				bControlIDBas = true;
				bPacienteIdBas = true;
				if (tbNombre.Text.Trim() == "")
				{
					stMensaje = "- El Nombre Es Obligatorio.";
				}
				if (tbNIFOtrosDocDP.Text.Trim() == "" && tbDPPasaporte.Text.Trim() == "" && tbTarjeta.Text.Trim() == "" && tbTarjetaEuropea.Text.Trim() == "" && lbCipAutonomico.Text.Trim() == "")
				{
					stMensaje = stMensaje + Environment.NewLine + 
					            "Al menos se debe cumplimentar uno de los siguientes campos:" + Environment.NewLine + 
					            "    * NIF / Otros Documentos " + Environment.NewLine + 
					            "    * Pasaporte " + Environment.NewLine + 
					            "    * Tarjeta Sanitaria " + Environment.NewLine + 
					            "    * Tarjeta Europea " + Environment.NewLine + 
					            "    * Cip Auton�mico ";
				}
				if (tbNIFOtrosDocDP.Text.Trim() != "")
				{
					sql = "SELECT count(*) FROM DPACIENT where ndninifp='" + tbNIFOtrosDocDP.Text.Trim() + "' and gidenpac<>'" + MFiliacion.ModCodigo + "'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
					rrAux = new DataSet();
					tempAdapter.Fill(rrAux);
					if (Convert.ToDouble(rrAux.Tables[0].Rows[0][0]) > 0)
					{
						stMensaje = stMensaje + Environment.NewLine + 
						            " Existe/n " + Convert.ToString(rrAux.Tables[0].Rows[0][0]) + " Paciente/s con el mismo NIF / Otros Documentos ";
					}
					rrAux.Close();
				}

				if (tbDPPasaporte.Text.Trim() != "")
				{
					sql = "SELECT count(*) FROM DPACIENT where npasapor='" + tbDPPasaporte.Text.Trim() + "' and gidenpac<>'" + MFiliacion.ModCodigo + "'";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, MFiliacion.GConexion);
					rrAux = new DataSet();
					tempAdapter_2.Fill(rrAux);
					if (Convert.ToDouble(rrAux.Tables[0].Rows[0][0]) > 0)
					{
						stMensaje = stMensaje + Environment.NewLine + 
						            " Existe/n " + Convert.ToString(rrAux.Tables[0].Rows[0][0]) + " Paciente/s con el mismo Pasaporte ";
					}
					rrAux.Close();
				}
				if (tbTarjeta.Text.Trim() != "")
				{
					sql = "SELECT count(*) FROM DPACIENT where ntarjeta='" + tbTarjeta.Text.Trim() + "' and gidenpac<>'" + MFiliacion.ModCodigo + "'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, MFiliacion.GConexion);
					rrAux = new DataSet();
					tempAdapter_3.Fill(rrAux);
					if (Convert.ToDouble(rrAux.Tables[0].Rows[0][0]) > 0)
					{
						stMensaje = stMensaje + Environment.NewLine + 
						            " Existe/n " + Convert.ToString(rrAux.Tables[0].Rows[0][0]) + " Paciente/s con el mismo n�mero de Tarjeta Sanitaria ";
					}
					rrAux.Close();
				}

				if (tbTarjetaEuropea.Text.Trim() != "")
				{
					sql = "SELECT count(*) FROM DPACIENT where ntarjsae='" + tbTarjetaEuropea.Text.Trim() + "' and gidenpac<>'" + MFiliacion.ModCodigo + "'";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, MFiliacion.GConexion);
					rrAux = new DataSet();
					tempAdapter_4.Fill(rrAux);
					if (Convert.ToDouble(rrAux.Tables[0].Rows[0][0]) > 0)
					{
						stMensaje = stMensaje + Environment.NewLine + 
						            " Existe/n " + Convert.ToString(rrAux.Tables[0].Rows[0][0]) + " Paciente/s con el mismo n�mero de Tarjeta Sanitaria Europea ";
					}
					rrAux.Close();
				}

				if (lbCipAutonomico.Text.Trim() != "")
				{
					sql = "SELECT count(*) FROM DPACIENT where ncipauto=" + lbCipAutonomico.Text.Trim() + " and gidenpac<>'" + MFiliacion.ModCodigo + "'";
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, MFiliacion.GConexion);
					rrAux = new DataSet();
					tempAdapter_5.Fill(rrAux);
					if (Convert.ToDouble(rrAux.Tables[0].Rows[0][0]) > 0)
					{
						stMensaje = stMensaje + Environment.NewLine + 
						            " Existe/n " + Convert.ToString(rrAux.Tables[0].Rows[0][0]) + " Paciente/s con el mismo n�mero de CIP Auton�mico ";
					}
					rrAux.Close();
				}
				if (stMensaje.Trim() != "")
				{
					bPacienteIdBas = false;
					sql = "select ISNULL(ipasafil,'N') ipasafil from SUSUARIO where gusuario='" + MFiliacion.sUsuario + "'";
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql, MFiliacion.GConexion);
					rrAux = new DataSet();
					tempAdapter_6.Fill(rrAux);
					if (rrAux.Tables[0].Rows.Count != 0)
					{
						if (Convert.ToString(rrAux.Tables[0].Rows[0]["ipasafil"]) == "N")
						{
							RadMessageBox.Show("Atenci�n:" + 
							                " El Paciente no cumple las condiciones de Identificaci�n B�sica." + Environment.NewLine + Environment.NewLine + 
							                stMensaje, "Control de Identificaci�n B�sica", MessageBoxButtons.OK, RadMessageIcon.Error);
							return false;
						}
						else
						{
							irespuesta = RadMessageBox.Show("Atenci�n:" + 
							             " El Paciente no cumple las condiciones de Identificaci�n B�sica." + Environment.NewLine + 
							             stMensaje + Environment.NewLine + Environment.NewLine + 
							             " � Desea Continuar ?", "Control de Identificaci�n B�sica", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2);
							if (irespuesta == System.Windows.Forms.DialogResult.Yes)
							{								
								DFI111F6.DefInstance.ShowDialog();
								if (!bMotivoFiliacion)
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
					}
					rrAux.Close();
				}
			}
			return result;
		}
        //----------------------------------------------------------------------------------

        //********************************************************************************************************
        //*                                                                                                      *
        //*  Procedimiento: cbAceptar_Click                                                                      *
        //*                                                                                                      *
        //*  Modificaciones:                                                                                     *
        //*                                                                                                      *
        //*      O.Frias ( 14/12/2006) En el caso de ser nuevo registro y el valor de la variable blnDuplicados  *
        //*                            sea False, se realizara la llamada a la funci�n fDuplicados, que actua-   *
        //*                            lizara el valor de la nueva variable blnContinua                          *
        //*                                                                                                      *
        //*      O.Frias ( 19/12/2006) En el caso de ser nuevo registro y el valor de la variable blnRequeridos  *
        //*                            sea False, se realizara la llamada a la funci�n fRequeridos, que actua-   *
        //*                            lizara el valor de la nueva variable blnContinua                          *
        //*                                                                                                      *
        //********************************************************************************************************

        private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			bool blnContinua = false;
			int iCodSociedad = 0;

			string stTipo = String.Empty;
			if (MFiliacion.stModulo != "OTROSPROFESIONALES" && MFiliacion.stFiliacion == "MODIFICACION")
			{
				if (Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "MANSOCIR", "VALFANU1") == "S" && ExisteEpisodio(MFiliacion.OBJETO2))
				{
					stTipo = "";
					if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "CI")
					{
						stTipo = " de la Cita";
					}
					else if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "CO" || Convert.ToString(MFiliacion.OBJETO2.stTReg) == "C")
					{ 
						if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "C")
						{
							stTipo = " de la solicitud";
						}
						stTipo = stTipo + " de la Consulta";
					}
					else if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "U")
					{ 
						stTipo = " de la solicitud de la Urgencia";
					}
					else if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "Q")
					{ 
						stTipo = " de la solicitud de la Intervenci�n";
					}
					else if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "H")
					{ 
						stTipo = " de la solicitud del ingreso";
					}

					if ((StringsHelper.ToDoubleSafe(stSocAntigua) == MFiliacion.CodSS && !rbRERegimen[0].IsChecked) || (StringsHelper.ToDoubleSafe(stSocAntigua) == MFiliacion.CodPriv && !rbRERegimen[2].IsChecked) || (StringsHelper.ToDoubleSafe(stSocAntigua) != MFiliacion.CodPriv && StringsHelper.ToDoubleSafe(stSocAntigua) != MFiliacion.CodSS && !rbRERegimen[1].IsChecked))
					{

						string tempRefParam = "Filiaci�n";
						string tempRefParam2 = "";
						short tempRefParam3 = 1125;
						string[] tempRefParam4 = new string[]{"Los datos econ�micos seleccionados para el paciente, " + "\n" + "\r" + "son distintos a los del episodio" + stTipo + "." + "\n" + "\r" + "Esto puede producir efectos indeseados", "continuar"};
						if (((DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, MFiliacion.GConexion, tempRefParam4))) != System.Windows.Forms.DialogResult.Yes)
						{
							return;
						}
					}
				}
			}


			if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) == "S")
			{
				if (!rbSexo[0].IsChecked && !rbSexo[1].IsChecked)
				{
					string tempRefParam5 = "Filiaci�n";
					string tempRefParam6 = "";
					short tempRefParam7 = 1040;
					string[] tempRefParam8 = new string[] { "tipo sexo"};
					MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam5, tempRefParam6, tempRefParam7, MFiliacion.GConexion, tempRefParam8);
					return;
				}
			}

			if (bValidarAutorizacionDatos)
			{
				if (rbAutorizacion[1].IsChecked)
				{
					string tempRefParam9 = "Filiaci�n";
					string tempRefParam10 = "";
					short tempRefParam11 = 1125;
					string[] tempRefParam12 = new string[] { stMensajeValidarAutorizacionDatos, "continuar"};
					if (((DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam9, tempRefParam10, tempRefParam11, MFiliacion.GConexion, tempRefParam12))) != System.Windows.Forms.DialogResult.Yes)
					{
						return;
					}
				}
			}

			//Comprobamos si tiene que insertar el Numero de version
			Algoritmo = -1;
			BinPan = "";

			DataSet RrSQLCias = null;
			if (CiasProfesional && NumCiasProfesional != "")
			{
				//Insertamos el CIAS en la tabla para que no de problemas al comprobar si existe (siempre va a ser valido)
				SqlDataAdapter tempAdapter = new SqlDataAdapter("select * from DCODCIAS where gcodcias = '" + MEBCIAS.Text + "'  ", MFiliacion.GConexion);
				RrSQLCias = new DataSet();
				tempAdapter.Fill(RrSQLCias);
				//and gcensalu = '" & NumCiasProfesional & "'
				if (RrSQLCias.Tables[0].Rows.Count != 0)
				{
					RrSQLCias.Edit();
					RrSQLCias.Tables[0].Rows[0]["gcodcias"] = MEBCIAS.Text;
					RrSQLCias.Tables[0].Rows[0]["dnombmed"] = MEBCIAS.Text;
					RrSQLCias.Tables[0].Rows[0]["gcensalu"] = NumCiasProfesional;
					string tempQuery = RrSQLCias.Tables[0].TableName;
					//SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrSQLCias, RrSQLCias.Tables[0].TableName);
				}
				else
				{

					RrSQLCias.AddNew();
					RrSQLCias.Tables[0].Rows[0]["gcodcias"] = MEBCIAS.Text;
					RrSQLCias.Tables[0].Rows[0]["dnombmed"] = MEBCIAS.Text;
					RrSQLCias.Tables[0].Rows[0]["gcensalu"] = NumCiasProfesional;
					string tempQuery_2 = RrSQLCias.Tables[0].TableName;
					//SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery_2, "");
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrSQLCias, RrSQLCias.Tables[0].TableName);
					RrSQLCias.Close();
				}
			}
			//(maplaza)(24/09/2007)seg�n la opci�n marcada, el c�digo de sociedad ser� uno u otro.
			if (rbRERegimen[0].IsChecked)
			{
				iCodSociedad = MFiliacion.CodSS;
			}
			else if ((rbRERegimen[1].IsChecked))
			{ 
				if (cbbRESociedad.SelectedIndex != -1)
				{
					iCodSociedad = Convert.ToInt32(cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString());
				}
			}
			else if ((rbRERegimen[2].IsChecked))
			{ 
				iCodSociedad = MFiliacion.CodPriv;
			}

			//(maplaza)(23/08/2007)Los datos que est�n en la tabla DOBLSOCI, para la sociedad indicada, ser�n obligatorios.

			// O.Frias - 11/02/2013
			// Se amplia la condici�n a sociedad e inspeccion.

			string inspeccion = "0";
			if (rbRERegimen[0].IsChecked && Convert.ToDouble(cbbREInspeccion.get_ListIndex()) != -1)
			{
				object tempRefParam13 = 1;
				object tempRefParam14 = cbbREInspeccion.get_ListIndex();
				inspeccion = Convert.ToString(cbbREInspeccion.get_Column(tempRefParam13, tempRefParam14));
			}

			string strSql = "SELECT gsocieda, gcontrol, dnocampo FROM DOBLSOCI WHERE gsocieda = " + iCodSociedad.ToString() + " AND (ginspecc = '" + inspeccion + "' or ginspecc = '0')";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
			RrTemporal = new DataSet();
			tempAdapter_4.Fill(RrTemporal);
			foreach (DataRow iteration_row in RrTemporal.Tables[0].Rows)
			{
                if (!(fRellenadoCampo(Convert.ToString(iteration_row["gcontrol"]).Trim())))
                {
                    string tempRefParam15 = "R�gimen Econ�mico";
                    string tempRefParam16 = "";
                    short tempRefParam17 = 4017;
                    string[] tempRefParam18 = new string[] { Convert.ToString(iteration_row["dnocampo"]).Trim()};
					MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam15, tempRefParam16, tempRefParam17, MFiliacion.GConexion, tempRefParam18);
					RrTemporal.Close();
					return;
				}
				else
				{
				}
			}
			RrTemporal.Close();
			//------


			//Comprobamos si es obligatorio introducir CIAS si la inspecci�n tiene el indicador activo
			if (rbRERegimen[0].IsChecked && Convert.ToDouble(cbbREInspeccion.get_ListIndex()) != -1 && cbbREInspeccion.Text.Trim() != "")
			{

				object tempRefParam19 = 1;
				object tempRefParam20 = cbbREInspeccion.get_ListIndex();
				strSql = "SELECT * FROM dinspeccva WHERE ginspecc = " + Convert.ToString(cbbREInspeccion.get_Column(tempRefParam19, tempRefParam20)) + "";
				SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
				RrTemporal = new DataSet();
				tempAdapter_5.Fill(RrTemporal);
				if (RrTemporal.Tables[0].Rows.Count != 0)
				{
					if ((Convert.ToString(RrTemporal.Tables[0].Rows[0]["IOBLCIAS"]) + "").Trim() == "S" && MEBCIAS.Text.Trim() == "")
					{
						string tempRefParam21 = "R�gimen Econ�mico";
						string tempRefParam22 = "";
						short tempRefParam23 = 4039;
                        string[] tempRefParam24 = new string[] { Label29.Text};
						MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam21, tempRefParam22, tempRefParam23, MFiliacion.GConexion, tempRefParam24);
						RrTemporal.Close();
						MEBCIAS.Focus();
						return;
					}
				}
				RrTemporal.Close();
			}

			if (rbRERegimen[1].IsChecked)
			{
				//(06/09/2007)No se podr� elegir una sociedad que est� desactivada para el paciente en DENTIPAC
				if (MFiliacion.ModCodigo != "")
				{
					strSql = "SELECT gsocieda FROM DENTIPAC WHERE gsocieda = " + cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString() + "";
					strSql = strSql + " AND gidenpac='" + MFiliacion.ModCodigo + "' AND NOT(fborrado is null)";
					SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
					RrTemporal = new DataSet();
					tempAdapter_6.Fill(RrTemporal);
					if (RrTemporal.Tables[0].Rows.Count != 0)
					{
						string tempRefParam25 = "R�gimen Econ�mico";
						string tempRefParam26 = "";
						short tempRefParam27 = 4018;
                        string[] tempRefParam28 = new string[] { };
						MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam25, tempRefParam26, tempRefParam27, MFiliacion.GConexion, tempRefParam28);
						return;
					}
					RrTemporal.Close();
				}
				//---------

				strSql = "SELECT * FROM Dtiptarj WHERE  gsocieda = " + cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString() + "";
				SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
				RrTemporal = new DataSet();
				tempAdapter_7.Fill(RrTemporal);
				if (RrTemporal.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(RrTemporal.Tables[0].Rows[0]["idcontrol"]))
					{
						Algoritmo = Convert.ToInt32(RrTemporal.Tables[0].Rows[0]["idcontrol"]);
					}
					if (!Convert.IsDBNull(RrTemporal.Tables[0].Rows[0]["gtarjeta"]))
					{
						BinPan = Convert.ToString(RrTemporal.Tables[0].Rows[0]["gtarjeta"]);
					}
				}

				//If Not (RrTemporal.EOF) And Me.tbRENVersion = "" And Not IsNull(RrTemporal("initver")) And Not IsNull(RrTemporal("iversio")) And Not IsNull(RrTemporal("longver")) Then
				//    MsgBox "Debe de rellenar el numero de version", vbInformation, "Atencion"
				//    Exit Sub
				//End If
			}

			//(maplaza)(08/08/2007)Dependiendo del valor de una variable global (VALREGEC), hay que comprobar si se ha
			//aceptado o no por parte del usuario los datos del R�gimen Econ�mico.
			if (bValidarRegimenEconomico)
			{
				if (chkValidarRE.CheckState == CheckState.Unchecked)
				{
					string tempRefParam29 = "R�gimen Econ�mico";
					string tempRefParam30 = "";
					short tempRefParam31 = 4016;
                    string[] tempRefParam32 = new string[] { };
					MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam29, tempRefParam30, tempRefParam31, MFiliacion.GConexion, tempRefParam32);
					return;
				}
			}

			//(03/09/2007)

			if (Information.IsDate(mebREFechaAlta.Text) && Information.IsDate(mebREFechaCaducidad.Text))
			{
				//la fecha inicial no puede ser mayor que la fecha final
				System.DateTime TempDate2 = DateTime.FromOADate(0);
				System.DateTime TempDate = DateTime.FromOADate(0);
				if (DateTime.Parse((DateTime.TryParse("01/" + mebREFechaAlta.Text, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : "01/" + mebREFechaAlta.Text) > DateTime.Parse((DateTime.TryParse("01/" + mebREFechaCaducidad.Text, out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy") : "01/" + mebREFechaCaducidad.Text))
				{
					string tempRefParam33 = "R�gimen Econ�mico";
					string tempRefParam34 = "";
					short tempRefParam35 = 1020;
                    string[] tempRefParam36 = new string[] { "La fecha inicial de cobertura", "menor o igual", "la fecha final"};
					MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam33, tempRefParam34, tempRefParam35, MFiliacion.GConexion, tempRefParam36);
					return;
				}
			}
			//------

			string imensaje = String.Empty;
			if (Algoritmo != -1 && BinPan != "")
			{
				if (tbRENPoliza.Text != "")
				{
					if (!ComprobarControl(BinPan + tbRENPoliza.Text, ref CodigoDeControl, Algoritmo, this.tbRENVersion.Text, cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString()))
					{
						string tempRefParam37 = "";
						short tempRefParam38 = 3991;
                        string[] tempRefParam39 = new string[] { };
						imensaje = Convert.ToString(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, tempRefParam37, tempRefParam38, MFiliacion.GConexion, tempRefParam39));
						if (imensaje == ((int) System.Windows.Forms.DialogResult.No).ToString())
						{
							Algoritmo = -1;
							CodigoDeControl = -1;
							return;
						}
					}
				}
				else
				{
					//OSCAR C JUNIO 2007
					//Obligamos a introducir Numero de Poliza en funcion de cte global
					if (Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "POLIZAOB", "VALFANU1") == "S")
					{
						string tempRefParam40 = "Filiaci�n";
						string tempRefParam41 = "";
						short tempRefParam42 = 1210;
                        string[] tempRefParam43 = new string[] { "poliza"};
						imensaje = Convert.ToString(MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam40, tempRefParam41, tempRefParam42, MFiliacion.GConexion, tempRefParam43));
						return;
					}
				}
			}


			object tempRefParam44 = 1;
			object tempRefParam45 = cbbIdioma.get_ListIndex();
			object tempRefParam46 = 1;
			object tempRefParam47 = cbbIdioma.get_ListIndex();
			IdiomaDelPaciente = (Convert.IsDBNull(cbbIdioma.get_Column(tempRefParam44, tempRefParam45))) ? "" : Convert.ToString(cbbIdioma.get_Column(tempRefParam46, tempRefParam47));
			bcbaceptar = true;
			this.Cursor = Cursors.WaitCursor;
			DatosCorrectos = true;
			COMPROBAR_DATOS();
			Comprobar_numero_cuenta();
			//Jesus 27/07/2011 No se hace para Argentina
			if (bFormatoCodigoPos)
			{
				//(maplaza)(20/04/2007)Se comprueba si los dos primeros d�gitos del C�digo Postal existen o no en la tabla de Provincias
				if (ComprobarDigitosCP(0))
				{ //si los primeros d�gitos del CP en la primera pesta�a son correctos
					//Si ha introducido privado
					if (tbREApe1.Text.Trim() != "" || tbRENombre.Text.Trim() != "")
					{
						ComprobarDigitosCP(1); //se comprueba si los dos primeros d�gitos del CP en la segunda pesta�a son correctos
					}
				}
				//-----
			}
			if (MEBCIAS.Text.Trim() != "")
			{
				Comprobar_DCODCIAS();
			}
			string stMen = String.Empty;
			dynamic Objects = null;
			if (DatosCorrectos)
			{
				if (MFiliacion.bCalcLetra)
				{
					//(maplaza)(03/05/2006)Se tratar� la letra s�lo si est� seleccionado el radio button "DNI/NIF"
					if (rbDNIOtroDocDP[0].IsChecked)
					{
						if (tbNIFOtrosDocDP.Text.Trim().Length == 8)
						{
							MFiliacion.irespuesta = RadMessageBox.Show("La letra para el DNI/NIF del paciente es la " + MFiliacion.ValidarLetraNif(tbNIFOtrosDocDP.Text.Trim()) + ". �Desea asignarla?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Info);
							if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.Yes)
							{
								tbNIFOtrosDocDP.Text = tbNIFOtrosDocDP.Text.Trim() + MFiliacion.ValidarLetraNif(tbNIFOtrosDocDP.Text.Trim());
							}
							if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.No && MFiliacion.bCalcLetraObligatoria)
							{
								this.Cursor = Cursors.Default;
								return;
							}
						}
						else if (tbNIFOtrosDocDP.Text.Trim().Length == 9)
						{ 
							if (tbNIFOtrosDocDP.Text.Trim().Substring(8, Math.Min(1, tbNIFOtrosDocDP.Text.Trim().Length - 8)) != MFiliacion.ValidarLetraNif(tbNIFOtrosDocDP.Text.Trim()))
							{
								MFiliacion.irespuesta = RadMessageBox.Show("La letra " + tbNIFOtrosDocDP.Text.Trim().Substring(8, Math.Min(1, tbNIFOtrosDocDP.Text.Trim().Length - 8)) + " es incorrecta para el DNI/NIF del paciente: " + tbNIFOtrosDocDP.Text.Trim().Substring(0, Math.Min(8, tbNIFOtrosDocDP.Text.Trim().Length)) + ". La letra correcta es " + MFiliacion.ValidarLetraNif(tbNIFOtrosDocDP.Text.Trim()) + ".�Desea asignar la nueva?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Info);
								if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.Yes)
								{
									tbNIFOtrosDocDP.Text = tbNIFOtrosDocDP.Text.Trim().Substring(0, Math.Min(8, tbNIFOtrosDocDP.Text.Trim().Length)) + MFiliacion.ValidarLetraNif(tbNIFOtrosDocDP.Text.Trim());
								}
								if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.No && MFiliacion.bCalcLetraObligatoria)
								{
									this.Cursor = Cursors.Default;
									return;
								}
							}
						}
					}

					//(maplaza)(03/05/2006)Se tratar� la letra s�lo si est� seleccionado el radio button "DNI/NIF"
					if (rbDNIOtroDocRE[0].IsChecked)
					{
						if (tbNIFOtrosDocRE.Text.Trim().Length == 8)
						{
							MFiliacion.irespuesta = RadMessageBox.Show("La letra para el DNI/NIF privado es la " + MFiliacion.ValidarLetraNif(tbNIFOtrosDocRE.Text.Trim()) + ". �Desea asignarla?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Info);
							if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.Yes)
							{
								tbNIFOtrosDocRE.Text = tbNIFOtrosDocRE.Text.Trim() + MFiliacion.ValidarLetraNif(tbNIFOtrosDocRE.Text.Trim());
							}
							if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.No && MFiliacion.bCalcLetraObligatoria)
							{
								this.Cursor = Cursors.Default;
								return;
							}
						}
						else if (tbNIFOtrosDocRE.Text.Trim().Length == 9)
						{ 
							if (tbNIFOtrosDocRE.Text.Trim().Substring(8, Math.Min(1, tbNIFOtrosDocRE.Text.Trim().Length - 8)) != MFiliacion.ValidarLetraNif(tbNIFOtrosDocRE.Text.Trim()))
							{
								MFiliacion.irespuesta = RadMessageBox.Show("La letra " + tbNIFOtrosDocRE.Text.Trim().Substring(8, Math.Min(1, tbNIFOtrosDocRE.Text.Trim().Length - 8)) + " es incorrecta para el DNI/NIF privado: " + tbNIFOtrosDocRE.Text.Trim().Substring(0, Math.Min(8, tbNIFOtrosDocRE.Text.Trim().Length)) + ". La letra correcta es " + MFiliacion.ValidarLetraNif(tbNIFOtrosDocRE.Text.Trim()) + ".�Desea asignar la nueva?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Info);
								if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.Yes)
								{
									tbNIFOtrosDocRE.Text = tbNIFOtrosDocRE.Text.Trim().Substring(0, Math.Min(8, tbNIFOtrosDocRE.Text.Trim().Length)) + MFiliacion.ValidarLetraNif(tbNIFOtrosDocRE.Text.Trim());
								}
								if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.No && MFiliacion.bCalcLetraObligatoria)
								{
									this.Cursor = Cursors.Default;
									return;
								}
							}
						}
					}
				}
				else
				{
					if (MFiliacion.bCalcLetraObligatoria)
					{
						if (rbDNIOtroDocDP[0].IsChecked)
						{
							if (tbNIFOtrosDocDP.Text.Trim().Length == 8)
							{
								this.Cursor = Cursors.Default;
								RadMessageBox.Show("La letra para el DNI/NIF del paciente es obligatoria", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
								return;
							}
							else if (tbNIFOtrosDocDP.Text.Trim().Length == 9)
							{ 
								if (tbNIFOtrosDocDP.Text.Trim().Substring(8, Math.Min(1, tbNIFOtrosDocDP.Text.Trim().Length - 8)) != MFiliacion.ValidarLetraNif(tbNIFOtrosDocDP.Text.Trim()))
								{
									this.Cursor = Cursors.Default;
									RadMessageBox.Show("La letra para el DNI/NIF del paciente es incorrecta", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
									return;
								}
							}
						}
						if (rbDNIOtroDocRE[0].IsChecked)
						{
							if (tbNIFOtrosDocRE.Text.Trim().Length == 8)
							{
								this.Cursor = Cursors.Default;
								RadMessageBox.Show("La letra para el DNI/NIF del privado es obligatoria", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
								return;
							}
							else if (tbNIFOtrosDocRE.Text.Trim().Length == 9)
							{ 
								if (tbNIFOtrosDocRE.Text.Trim().Substring(8, Math.Min(1, tbNIFOtrosDocRE.Text.Trim().Length - 8)) != MFiliacion.ValidarLetraNif(tbNIFOtrosDocRE.Text.Trim()))
								{
									this.Cursor = Cursors.Default;
									RadMessageBox.Show("La letra para el DNI/NIF del privado es incorrecta", Serrores.vNomAplicacion, MessageBoxButtons.OK, RadMessageIcon.Error);
									return;
								}
							}
						}
					}
				}

				EsZona();


				//******************** O.Frias (14/12/2006) ********************
				blnContinua = true;
				if (!blnDuplicados && (MFiliacion.stFiliacion == "ALTA"))
				{
					blnContinua = fDuplicados();
					blnDuplicados = true;
				}
                //******************** O.Frias (14/12/2006) ********************

                //Si se ha seleccionado alg�n valor y es visible(por si hay control de acceso)
                //if (Convert.ToDouble(cbbCanPrev.get_ListIndex()) != -1 && cbbCanPrev.Visible)
                if (cbbCanPrev.SelectedIndex != -1 && cbbCanPrev.Visible)
                {
					stMen = "";
					int tempRefParam48 = 2;
					int tempRefParam49 = cbbCanPrev.SelectedIndex;
                    //if (Convert.ToString(cbbCanPrev.get_Column(tempRefParam48, tempRefParam49)) == "S")
                    if (Convert.ToString(getLisIndex(cbbCanPrev,tempRefParam49,tempRefParam48 )) == "S")
                    { //''Si tiene indicador de correo electr�nico.....
						if (tbEmail.Text.Trim() == "")
						{
							stMen = "Debe introducir un correo electr�nico";
						}
						else
						{
							if ((tbEmail.Text.Trim().IndexOf('@') + 1) == 0 || Strings.InStr((tbEmail.Text.Trim().IndexOf('@') + 1) + 1, tbEmail.Text.Trim(), ".", CompareMethod.Binary) == 0)
							{
								stMen = "Debe introducir un correo electr�nico con formato correcto";
							}
						}
						if (stMen != "")
						{
							string tempRefParam50 = "Filiaci�n";
							string tempRefParam51 = "";
							short tempRefParam52 = 1125;
                            string[] tempRefParam53 = new string[] { stMen, "continuar"};
							if (((DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam50, tempRefParam51, tempRefParam52, MFiliacion.GConexion, tempRefParam53))) != System.Windows.Forms.DialogResult.Yes)
							{
								tbEmail.Focus();
								this.Cursor = Cursors.Default;
								return;
							}
						}
					}
					int tempRefParam54 = 3;
					int tempRefParam55 = cbbCanPrev.SelectedIndex;

                    //if (Convert.ToString(cbbCanPrev.get_Column(tempRefParam54, tempRefParam55)) == "S")
                    if (getLisIndex(cbbCanPrev, tempRefParam55, tempRefParam54) == "S")
                    { //''Si tiene indicador de sms .....
						if (tbDPTelf3.Text.Trim() == "" || chbSMS.CheckState != CheckState.Checked)
						{
							stMen = "Debe " + "\n" + "\r";
							if (chbSMS.CheckState != CheckState.Checked)
							{
								stMen = stMen + " * Seleccionar la opci�n de " + chbSMS.Text;
								Objects = chbSMS;
							}
							if (tbDPTelf3.Text.Trim() == "")
							{
								if (stMen != "Debe " + "\n" + "\r")
								{
									stMen = stMen + "\n" + "\r";
								}
								stMen = stMen + " * Introducir un n�mero de tel�fono m�vil";
								Objects = tbDPTelf3;
							}
							if (stMen != "")
							{
								string tempRefParam56 = "Filiaci�n";
								string tempRefParam57 = "";
								short tempRefParam58 = 1125;
                                string[] tempRefParam59 = new string[] { stMen, "continuar"};
								if (((DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam56, tempRefParam57, tempRefParam58, MFiliacion.GConexion, tempRefParam59))) != System.Windows.Forms.DialogResult.Yes)
								{
									Objects.SetFocus();
									this.Cursor = Cursors.Default;
									return;
								}
							}
						}
					}
					int tempRefParam60 = 4;
					int tempRefParam61 = cbbCanPrev.SelectedIndex;
					if (getLisIndex(cbbCanPrev,tempRefParam61,tempRefParam60) == "S")
					{ //''Si tiene indicador de correo postal.....
						stMen = "Debe " + "\n" + "\r";
						if (Convert.ToDouble(cbbDPVia.get_ListIndex()) == -1)
						{
							stMen = stMen + " * Seleccionar " + ((Label53.Text.Trim().Substring(Math.Max(Label53.Text.Trim().Length - 1, 0)) == ":") ? Label53.Text.Trim().Substring(0, Math.Min(Label53.Text.Trim().Length - 1, Label53.Text.Trim().Length)) : Label53.Text.Trim());
							Objects = cbbDPVia;
						}
						if (tbDPDomicilioP.Text.Trim() == "" || (tbDPNDomic.Text.Trim() == "" && tbDPOtrosDomic.Text.Trim() == ""))
						{
							if (stMen != "Debe " + "\n" + "\r")
							{
								stMen = stMen + "\n" + "\r";
							}
							stMen = stMen + " * Introducir los datos del domicilio ";
							Objects = tbDPDomicilioP;
						}
						if (mebDPCodPostal.Text.Trim() == "")
						{
							if (stMen != "Debe " + "\n" + "\r")
							{
								stMen = stMen + "\n" + "\r";
							}
							stMen = stMen + " * Introducir " + ((Label22.Text.Trim().Substring(Math.Max(Label22.Text.Trim().Length - 1, 0)) == ":") ? Label22.Text.Trim().Substring(0, Math.Min(Label22.Text.Trim().Length - 1, Label22.Text.Trim().Length)) : Label22.Text.Trim());
							Objects = mebDPCodPostal;
						}
						if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) == -1)
						{
							if (stMen != "Debe " + "\n" + "\r")
							{
								stMen = stMen + "\n" + "\r";
							}
							stMen = stMen + " * Introducir " + ((Label4.Text.Trim().Substring(Math.Max(Label4.Text.Trim().Length - 1, 0)) == ":") ? Label4.Text.Trim().Substring(0, Math.Min(Label4.Text.Trim().Length - 1, Label4.Text.Trim().Length)) : Label4.Text.Trim());
							Objects = cbbDPPoblacion;
						}
						if (stMen != "Debe " + "\n" + "\r")
						{
							string tempRefParam62 = "Filiaci�n";
							string tempRefParam63 = "";
							short tempRefParam64 = 1125;
                            string[] tempRefParam65 = new string[] { stMen, "continuar"};
							if (((DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam62, tempRefParam63, tempRefParam64, MFiliacion.GConexion, tempRefParam65))) != System.Windows.Forms.DialogResult.Yes)
							{
								Objects.Focus();                                
                                this.Cursor = Cursors.Default;
								return;
							}
						}
					}
					
					int tempRefParam66 = 5;
					int tempRefParam67 = cbbCanPrev.SelectedIndex;

					if (getLisIndex(cbbCanPrev,tempRefParam67, tempRefParam66) == "S")
					{ //''Si tiene indicador de tel�fono.....
						if (tbDPTelf1.Text.Trim() == "" && tbDPTelf2.Text.Trim() == "" && tbDPTelf3.Text.Trim() == "")
						{
							string tempRefParam68 = "Filiaci�n";
							string tempRefParam69 = "";
							short tempRefParam70 = 1125;
                            string[] tempRefParam71 = new string[] { "Debe introducir un tel�fono de contacto", "continuar"};
							if (((DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam68, tempRefParam69, tempRefParam70, MFiliacion.GConexion, tempRefParam71))) != System.Windows.Forms.DialogResult.Yes)
							{
								tbDPTelf1.Focus();
								this.Cursor = Cursors.Default;
								return;
							}
						}
					}
				}

				if (blnContinua)
				{
					if (!blnRequeridos && (MFiliacion.stFiliacion == "ALTA" || MFiliacion.stFiliacion == "MODIFICACION"))
					{
						blnContinua = fRequeridos();
						blnRequeridos = true;
					}
					if (blnContinua)
					{

						//Oscar C Mayo 2010
						//Nueva validacion de datos de Indentificacion Basica de CAPIO: Requerimiento 995
						if (!proControlIndetificacionBasica())
						{
							this.Cursor = Cursors.Default;
							return;
						}
						//------------

						//------------------------------------------------------------
						//- Comprobamos si el n�mero de p�liza cumple con la m�scara -
						//------------------------------------------------------------

						if (rbRERegimen[1].IsChecked)
						{
							if (!proCoincideMascara(tbRENPoliza.Text))
							{

								// Mostramos mensaje

								string tempRefParam72 = "Filiaci�n";
								string tempRefParam73 = "";
								short tempRefParam74 = 1125;
                                string[] tempRefParam75 = new string[] { "El n�mero de p�liza no coincide con la m�scara establecida (" + ToolTipMain.GetToolTip(tbRENPoliza) + ")", "continuar"};
								if (((DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam72, tempRefParam73, tempRefParam74, MFiliacion.GConexion, tempRefParam75))) != System.Windows.Forms.DialogResult.Yes)
								{
									SSTabHelper.SetSelectedIndex(tsDatosPac, 1);
									this.Cursor = Cursors.Default;
									tbRENPoliza.Focus();
									return;
								}

							}
						}

						//------------------------------------------------------------

						Guardar_registros();
                        
                        DFI120F1.DefInstance.GrabarUltimoPaciente(MFiliacion.codigo);
						//************** O.Frias - 13/10/2007 **************
						CambioUsuarWeb(MFiliacion.ModCodigo, MFiliacion.codigo);
						if (!MFiliacion.CodPac)
						{
							MANDAR_DATOS();
						}
						else
						{
							MANDAR_DATOS2();
						}

						LIMPIAR_FORMULARIO();
						MFiliacion.CodPac = false;
						this.Cursor = Cursors.Default;
						MFiliacion.CodPac = false;
						this.Close();
					}
				}
			}
		}

		//(maplaza)(19/04/2007)Esta funci�n dice si las 2 primeras posiciones de un C�digo Postal est�n asociadas o
		//no a una provincia en la Base de Datos (por ejemplo, "28" est� asociado a Madrid").
		private bool fExisteCodigoProvincia(string stDosPrimerosDigitos)
		{            
			bool result = false;
			string StSql = String.Empty;
			DataSet RrSql = null;

			try
			{
				this.Cursor = Cursors.WaitCursor;
				StSql = "Select gprovinc From DPROVINC Where gprovinc='" + stDosPrimerosDigitos + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
					result = true;
				}
				RrSql.Close();
				this.Cursor = Cursors.Default;
				return result;
			}
			catch
			{
				this.Cursor = Cursors.Default;
				return result;
			}
		}

		//********************************************************************************************************
		//*                                                                                                      *
		//*  Procedimiento: fDuplicados                                                                          *
		//*                                                                                                      *
		//*  Modificaciones:                                                                                     *
		//*                                                                                                      *
		//*      O.Frias ( 14/12/2006) Realizamos la llamada al procedimiento almacenado para el retorno de los  *
		//*                            posibles pacientes duplicados, en este PA insertan sobre la tabla DBUSPACI*
		//*                            posibles registros que cumplan ciertas condiciones                        *
		//*                                                                                                      *
		//********************************************************************************************************
		private bool fDuplicados()
		{
			bool ErrDuplica = false;
			bool result = false;
			string strSql = String.Empty;
			SqlCommand rqDuplicados = null;
			DataSet rdoTemp = null;
			try
			{
				ErrDuplica = true;
                
                strSql = "DFIPA111_Duplicidad";
                rqDuplicados = MFiliacion.GConexion.CreateQuery("Duplicados", strSql);
                rqDuplicados.CommandType = CommandType.StoredProcedure;

                rqDuplicados.Parameters.Add("@DP_Apell1", SqlDbType.VarChar).Size = 100; //0
                rqDuplicados.Parameters.Add("@DP_Apell2", SqlDbType.VarChar).Size = 100; //1
                rqDuplicados.Parameters.Add("@DP_Nombre", SqlDbType.VarChar).Size = 100; //2
                rqDuplicados.Parameters.Add("@DP_FecNac", SqlDbType.DateTime);           //3   
                rqDuplicados.Parameters.Add("@DP_TiSexo", SqlDbType.TinyInt).Size = 100; //4
                rqDuplicados.Parameters.Add("@DP_TiDocu", SqlDbType.VarChar).Size = 100; //5
                rqDuplicados.Parameters.Add("@DP_NumDoc", SqlDbType.VarChar).Size = 100; //6
                rqDuplicados.Parameters.Add("@DP_NumTar", SqlDbType.VarChar).Size = 100; //7
                rqDuplicados.Parameters.Add("@DP_ViaPac", SqlDbType.VarChar).Size = 100; //8
                rqDuplicados.Parameters.Add("@DP_Domici", SqlDbType.VarChar).Size = 100; //9
                rqDuplicados.Parameters.Add("@DP_Numero", SqlDbType.VarChar).Size = 100; //10
                rqDuplicados.Parameters.Add("@DP_OtDomi", SqlDbType.VarChar).Size = 100; //11
                rqDuplicados.Parameters.Add("@DP_CoPost", SqlDbType.VarChar).Size = 100; //12
                rqDuplicados.Parameters.Add("@DP_Poblac", SqlDbType.VarChar).Size = 100; //13
                rqDuplicados.Parameters.Add("@DP_Telef1", SqlDbType.VarChar).Size = 100; //14
                rqDuplicados.Parameters.Add("@DP_Telef2", SqlDbType.VarChar).Size = 100; //15
                rqDuplicados.Parameters.Add("@DP_Telef3", SqlDbType.VarChar).Size = 100; //16
                rqDuplicados.Parameters.Add("@DP_NumPas", SqlDbType.VarChar).Size = 100; //17
                rqDuplicados.Parameters.Add("@DP_Idioma", SqlDbType.VarChar).Size = 100; //18
                rqDuplicados.Parameters.Add("@DP_Bloque", SqlDbType.VarChar).Size = 100; //19
                rqDuplicados.Parameters.Add("@DP_ResExt", SqlDbType.Bit);                //20
                rqDuplicados.Parameters.Add("@DP_PaiExt", SqlDbType.VarChar).Size = 100; //21
                rqDuplicados.Parameters.Add("@DP_PobExt", SqlDbType.VarChar).Size = 100; //22
                rqDuplicados.Parameters.Add("@DP_Autori", SqlDbType.VarChar).Size = 100; //23
                rqDuplicados.Parameters.Add("@DP_Exitus", SqlDbType.Bit);                //24
                rqDuplicados.Parameters.Add("@DP_FecFac", SqlDbType.VarChar).Size = 100; //25
                rqDuplicados.Parameters.Add("@RE_RegEco", SqlDbType.TinyInt).Size = 100; //26
                rqDuplicados.Parameters.Add("@RE_Inspec", SqlDbType.VarChar).Size = 100; //27
                rqDuplicados.Parameters.Add("@RE_SegSog", SqlDbType.VarChar);            //28
                rqDuplicados.Parameters.Add("@RE_NuCIAS", SqlDbType.VarChar).Size = 100; //29
                rqDuplicados.Parameters.Add("@Re_Zona", SqlDbType.VarChar).Size = 100;   //30
                rqDuplicados.Parameters.Add("@RE_Estado", SqlDbType.VarChar).Size = 100; //31
                rqDuplicados.Parameters.Add("@TiBeSegSo", SqlDbType.VarChar).Size = 100; //32
                rqDuplicados.Parameters.Add("@NomTiBeSS", SqlDbType.VarChar).Size = 100; //33
                rqDuplicados.Parameters.Add("@RE_SocMec", SqlDbType.SmallInt).Size = 100;//34
                rqDuplicados.Parameters.Add("@RE_NumPol", SqlDbType.VarChar).Size = 100; //35
                rqDuplicados.Parameters.Add("@RE_Empres", SqlDbType.VarChar).Size = 100; //36
                rqDuplicados.Parameters.Add("@TiBeSocie", SqlDbType.VarChar).Size = 100; //37
                rqDuplicados.Parameters.Add("@NomTiBeSo", SqlDbType.VarChar).Size = 100; //38
                rqDuplicados.Parameters.Add("@RE_TipDoc", SqlDbType.VarChar).Size = 100; //39
                rqDuplicados.Parameters.Add("@RE_NumDoc", SqlDbType.VarChar).Size = 100; //39
                rqDuplicados.Parameters.Add("@RE_NumPas", SqlDbType.VarChar).Size = 100; //40
                rqDuplicados.Parameters.Add("@RE_Apell1", SqlDbType.VarChar).Size = 100; //41
                rqDuplicados.Parameters.Add("@RE_Apell2", SqlDbType.VarChar).Size = 100; //42
                rqDuplicados.Parameters.Add("@RE_Nombre", SqlDbType.VarChar).Size = 100; //43
                rqDuplicados.Parameters.Add("@RE_Domici", SqlDbType.VarChar).Size = 100; //44
                rqDuplicados.Parameters.Add("@RE_CodPos", SqlDbType.VarChar).Size = 100; //45
                rqDuplicados.Parameters.Add("@RE_Poblac", SqlDbType.VarChar).Size = 100; //46
                rqDuplicados.Parameters.Add("@RE_ResExt", SqlDbType.Bit);                //47
                rqDuplicados.Parameters.Add("@RE_PaiExt", SqlDbType.VarChar).Size = 100; //48
                rqDuplicados.Parameters.Add("@RE_PobExt", SqlDbType.VarChar).Size = 100; //49
                rqDuplicados.Parameters.Add("@RE_ForPag", SqlDbType.VarChar).Size = 100; //50
                rqDuplicados.Parameters.Add("@RE_CodBan", SqlDbType.VarChar).Size = 100; //51
                rqDuplicados.Parameters.Add("@RE_SucBan", SqlDbType.VarChar).Size = 100; //52
                rqDuplicados.Parameters.Add("@RE_DCBanc", SqlDbType.VarChar).Size = 100; //53
                rqDuplicados.Parameters.Add("@RE_CtaBan", SqlDbType.VarChar).Size = 100; //54
                rqDuplicados.Parameters.Add("@gusuario", SqlDbType.Char).Size = 8;       //55   

                for (int intContador = 0; intContador <= 56; intContador++)
				{
					rqDuplicados.Parameters[intContador].Direction = ParameterDirection.Input;
				}

				rqDuplicados.Parameters[0].Value = tbApellido1.Text.Trim();
				rqDuplicados.Parameters[1].Value = tbApellido2.Text.Trim();
				rqDuplicados.Parameters[2].Value = tbNombre.Text.Trim();
				if (sdcFechaNac.Text != "")
				{
					rqDuplicados.Parameters[3].Value = Convert.ToDateTime(this.sdcFechaNac.Text.ToString());
				}
				else
				{
					rqDuplicados.Parameters[3].Value = DBNull.Value;
				}

				if (rbSexo[0].IsChecked)
				{
					rqDuplicados.Parameters[4].Value = 0;
				}
				else if (rbSexo[1].IsChecked)
				{ 
					rqDuplicados.Parameters[4].Value = 1;
				}
				else if (rbSexo[2].IsChecked)
				{ 
					rqDuplicados.Parameters[4].Value = 2;
				}
				else
				{
					rqDuplicados.Parameters[4].Value = 2;
				}

				if (rbDNIOtroDocDP[0].IsChecked)
				{
					rqDuplicados.Parameters[5].Value = "0";
				}
				else if (rbDNIOtroDocDP[1].IsChecked)
				{ 
					rqDuplicados.Parameters[5].Value = "1";
				}
				else
				{
					rqDuplicados.Parameters[5].Value = "";
				}

				rqDuplicados.Parameters[6].Value = tbNIFOtrosDocDP.Text.Trim();
				rqDuplicados.Parameters[7].Value = tbTarjeta.Text.Trim();
				rqDuplicados.Parameters[8].Value = Convert.ToString(cbbDPVia.Text).Trim();
				rqDuplicados.Parameters[9].Value = tbDPDomicilioP.Text.Trim();
				rqDuplicados.Parameters[10].Value = tbDPNDomic.Text.Trim();
				rqDuplicados.Parameters[11].Value = tbDPOtrosDomic.Text.Trim();
				rqDuplicados.Parameters[12].Value = mebDPCodPostal.Text.Trim();
				rqDuplicados.Parameters[13].Value = Convert.ToString(cbbDPPoblacion.Text).Trim();
				rqDuplicados.Parameters[14].Value = tbDPTelf1.Text.Trim();
				rqDuplicados.Parameters[15].Value = tbDPTelf2.Text.Trim();
				rqDuplicados.Parameters[16].Value = tbDPTelf3.Text.Trim();
				rqDuplicados.Parameters[17].Value = tbDPPasaporte.Text.Trim();
				rqDuplicados.Parameters[18].Value = Convert.ToString(cbbIdioma.Text).Trim();

				if (rbBloqueo[0].IsChecked)
				{
					rqDuplicados.Parameters[19].Value = "S";
				}
				else if (rbBloqueo[1].IsChecked)
				{ 
					rqDuplicados.Parameters[19].Value = "N";
				}
				else
				{
					rqDuplicados.Parameters[19].Value = "";
				}
                //rqDuplicados.Parameters[20].Value = (int) chbDPExtrangero.CheckState;
                rqDuplicados.Parameters[20].Value = chbDPExtrangero.Checked;
                rqDuplicados.Parameters[21].Value = Convert.ToString(cbbDPPais.Text).Trim();
				rqDuplicados.Parameters[22].Value = tbDPExPob.Text.Trim();

				if (rbAutorizacion[0].IsChecked)
				{
					rqDuplicados.Parameters[23].Value = "S";
				}
				else if (rbAutorizacion[1].IsChecked)
				{ 
					rqDuplicados.Parameters[23].Value = "N";
				}
				else
				{
					rqDuplicados.Parameters[23].Value = "";
				}

                //rqDuplicados.Parameters[24].Value = (int) chbDPExitus.CheckState;
                rqDuplicados.Parameters[24].Value = chbDPExitus.Checked;
                rqDuplicados.Parameters[25].Value = sdcDPFechaFallecimiento.Text;

				if (rbRERegimen[0].IsChecked)
				{
					rqDuplicados.Parameters[26].Value = 0;
				}
				else if (rbRERegimen[1].IsChecked)
				{ 
					rqDuplicados.Parameters[26].Value = 1;
				}
				else if (rbRERegimen[2].IsChecked)
				{ 
					rqDuplicados.Parameters[26].Value = 2;
				}

				rqDuplicados.Parameters[27].Value = Convert.ToString(cbbREInspeccion.Text).Trim();
				rqDuplicados.Parameters[28].Value =  tbRENSS.Text.Trim();
				rqDuplicados.Parameters[29].Value = MEBCIAS.Text.Trim();

				rqDuplicados.Parameters[30].Value = (Convert.ToBoolean(chkZona.Checked)) ? "S" : "N";

				if (rbActivo.IsChecked)
				{
					rqDuplicados.Parameters[31].Value = "A";
				}
				else if (rbPensionista.IsChecked)
				{ 
					rqDuplicados.Parameters[31].Value = "P";
				}
				else
				{
					rqDuplicados.Parameters[31].Value = "";
				}

				if (rbIndicadorSS[0].IsChecked)
				{
					rqDuplicados.Parameters[32].Value = "T";
				}
				else if (rbIndicadorSS[1].IsChecked)
				{ 
					rqDuplicados.Parameters[32].Value = "B";
				}
				else
				{
					rqDuplicados.Parameters[32].Value = "";
				}

				rqDuplicados.Parameters[33].Value = tbRENomTitularSS.Text.Trim();
				if (cbbRESociedad.SelectedIndex >= 0)
				{
					rqDuplicados.Parameters[34].Value = cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString();
				}
				else
				{
					rqDuplicados.Parameters[34].Value = 0;
				}
				rqDuplicados.Parameters[35].Value = tbRENPoliza.Text.Trim();
				rqDuplicados.Parameters[36].Value = tbREEmpresa.Text.Trim();

				if (rbIndicador[0].IsChecked)
				{
					rqDuplicados.Parameters[37].Value = "T";
				}
				else if (rbIndicador[1].IsChecked)
				{ 
					rqDuplicados.Parameters[37].Value = "B";
				}
				else
				{
					rqDuplicados.Parameters[37].Value = "";
				}

				rqDuplicados.Parameters[38].Value = tbRENomTitular.Text.Trim();

				if (rbDNIOtroDocRE[0].IsChecked)
				{
					rqDuplicados.Parameters[39].Value = "0";
				}
				else if (rbDNIOtroDocRE[1].IsChecked)
				{ 
					rqDuplicados.Parameters[39].Value = "1";
				}
				else
				{
					rqDuplicados.Parameters[39].Value = "";
				}

				rqDuplicados.Parameters[40].Value = tbNIFOtrosDocRE.Text.Trim();
				rqDuplicados.Parameters[41].Value = tbREPasaporte.Text.Trim();
				rqDuplicados.Parameters[42].Value = tbREApe1.Text.Trim();
				rqDuplicados.Parameters[43].Value = tbREApe2.Text.Trim();
				rqDuplicados.Parameters[44].Value = tbRENombre.Text.Trim();
				rqDuplicados.Parameters[45].Value = tbREDomicilio.Text.Trim();
				rqDuplicados.Parameters[46].Value = mebRECodPostal.Text.Trim();
				rqDuplicados.Parameters[47].Value = Convert.ToString(cbbREPoblacion.Text).Trim();
                //rqDuplicados.Parameters[48].Value = ((int) chbREExtrangero.CheckState).ToString().Trim();
                rqDuplicados.Parameters[48].Value = chbREExtrangero.Checked;
                rqDuplicados.Parameters[49].Value = Convert.ToString(cbbREPais.Text).Trim();
				rqDuplicados.Parameters[50].Value = tbREExPob.Text.Trim();
				rqDuplicados.Parameters[51].Value = Convert.ToString(cbbREPago.Text).Trim();
				rqDuplicados.Parameters[52].Value = tbBanco.Text.Trim();
				rqDuplicados.Parameters[53].Value = tbSucursal.Text.Trim();
				rqDuplicados.Parameters[54].Value = tbControl.Text.Trim();
				rqDuplicados.Parameters[55].Value = tbCuenta.Text.Trim();
				rqDuplicados.Parameters[56].Value = MFiliacion.sUsuario.Trim();

				rqDuplicados.ExecuteNonQuery();
                rqDuplicados.Dispose();                
				rqDuplicados = null;

				strSql = "SELECT * FROM DBUSPACI WHERE gusuario = '" + MFiliacion.sUsuario + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count == 0)
				{
					rdoTemp.Close();
					result = true;
				}
				else
				{
					rdoTemp.Close();
					mbAcceso.strGIdenPac = "";
                    
					DFI111F4.DefInstance.ShowDialog();
					if (mbAcceso.strGIdenPac.Trim() == "")
					{
						result = true;
					}
					else
					{
						result = false;
						MFiliacion.stFiliacion = "MODIFICACION";
						strSql = "select * from dpacient where gidenpac = '" + MFiliacion.ModCodigo + "'";
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
						MFiliacion.RrSQLUltimo = new DataSet();
						tempAdapter_2.Fill(MFiliacion.RrSQLUltimo);
						if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count != 0)
						{
							CargarLoad();
						}
					}
				}
				Cursor = Cursors.Default;
				ErrDuplica = false;
				return result;
			}
			catch (SqlException excep)
			{
				if (!ErrDuplica)
				{
					throw excep;
				}

				if (ErrDuplica)
				{					
					Serrores.GestionErrorBaseDatos(excep, MFiliacion.GConexion);
					return result;
				}
			}
			return false;
		}

		//****************************************************************************************************
		//*                                                                                                  *
		//*  Procedimiento: fRequeridos                                                                      *
		//*                                                                                                  *
		//*  Modificaciones:                                                                                 *
		//*                                                                                                  *
		//*      O.Frias ( 19/12/2006) Realizamos la llamada al procedimiento almacenado el control de los   *
		//*                            requeridos                                                            *
		//*                                                                                                  *
		//****************************************************************************************************
		private bool fRequeridos()
		{
			bool ErrRequeridos = false;
			bool result = false;
			string strSql = String.Empty;
			SqlCommand rqRequeridos = null;

			try
			{
				ErrRequeridos = true;
				
                strSql = "DFIPA112_Calidad";

                rqRequeridos = MFiliacion.GConexion.CreateQuery("Requeridos", strSql);
                rqRequeridos.CommandType = CommandType.StoredProcedure;
                
                rqRequeridos.Parameters.Add("@DP_Apell1", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Apell2", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Nombre", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_FecNac", SqlDbType.DateTime);
                rqRequeridos.Parameters.Add("@DP_TiSexo", SqlDbType.TinyInt);
                rqRequeridos.Parameters.Add("@DP_TiDocu", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_NumDoc", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_NumTar", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_ViaPac", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Domici", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Numero", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_OtDomi", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_CoPost", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Poblac", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Telef1", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Telef2", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Telef3", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_NumPas", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Idioma", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Bloque", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_ResExt", SqlDbType.Bit);
                rqRequeridos.Parameters.Add("@DP_PaiExt", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_PobExt", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Autori", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@DP_Exitus", SqlDbType.Bit);
                rqRequeridos.Parameters.Add("@DP_FecFac", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_RegEco", SqlDbType.TinyInt);
                rqRequeridos.Parameters.Add("@RE_Inspec", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_SegSog", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_NuCIAS", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_Estado", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@TiBeSegSo", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@NomTiBeSS", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_SocMec", SqlDbType.SmallInt);
                rqRequeridos.Parameters.Add("@RE_NumPol", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_Empres", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@TiBeSocie", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@NomTiBeSo", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_TipDoc", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_NumDoc", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_NumPas", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_Apell1", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_Apell2", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_Nombre", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_Domici", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_CodPos", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_Poblac", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_ResExt", SqlDbType.Bit);
                rqRequeridos.Parameters.Add("@RE_PaiExt", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_PobExt", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_ForPag", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_CodBan", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_SucBan", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_DCBanc", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@RE_CtaBan", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@gusuario", SqlDbType.Char).Size = 8;
                rqRequeridos.Parameters.Add("@Estado", SqlDbType.Bit);
                rqRequeridos.Parameters.Add("@Aviso", SqlDbType.VarChar).Size = 5000;
                rqRequeridos.Parameters.Add("@Posicion", SqlDbType.VarChar).Size = 100;
                rqRequeridos.Parameters.Add("@Zona", SqlDbType.Bit);
                rqRequeridos.Parameters.Add("@Pestania", SqlDbType.TinyInt);


                for (int intContador = 0; intContador <= 55; intContador++)
				{
					rqRequeridos.Parameters[intContador].Direction = ParameterDirection.Input;
				}

				rqRequeridos.Parameters[56].Direction = ParameterDirection.Output;
				rqRequeridos.Parameters[57].Direction = ParameterDirection.Output;
				rqRequeridos.Parameters[58].Direction = ParameterDirection.Output;
				rqRequeridos.Parameters[59].Direction = ParameterDirection.Output;
				rqRequeridos.Parameters[60].Direction = ParameterDirection.Output;

				rqRequeridos.Parameters[0].Value = tbApellido1.Text.Trim();
				rqRequeridos.Parameters[1].Value = tbApellido2.Text.Trim();
				rqRequeridos.Parameters[2].Value = tbNombre.Text.Trim();
				if (sdcFechaNac.Text != "")
				{
					rqRequeridos.Parameters[3].Value = Convert.ToDateTime(this.sdcFechaNac.Text.ToString());
				}
				else
				{					
					rqRequeridos.Parameters[3].Value = DBNull.Value;
				}

				if (rbSexo[0].IsChecked)
				{
					rqRequeridos.Parameters[4].Value = 0;
				}
				else if (rbSexo[1].IsChecked)
				{ 
					rqRequeridos.Parameters[4].Value = 1;
				}
				else if (rbSexo[2].IsChecked)
				{ 
					rqRequeridos.Parameters[4].Value = 2;
				}
				else
				{
					rqRequeridos.Parameters[4].Value = 2;
				}

				if (rbDNIOtroDocDP[0].IsChecked)
				{
					rqRequeridos.Parameters[5].Value = "0";
				}
				else if (rbDNIOtroDocDP[1].IsChecked)
				{ 
					rqRequeridos.Parameters[5].Value = "1";
				}
				else
				{
					rqRequeridos.Parameters[5].Value = "";
				}

				rqRequeridos.Parameters[6].Value = tbNIFOtrosDocDP.Text.Trim();
				rqRequeridos.Parameters[7].Value = tbTarjeta.Text.Trim();
				rqRequeridos.Parameters[8].Value = Convert.ToString(cbbDPVia.Text).Trim();

				//(maplaza)(19/09/2007)Ahora hay un nuevo campo para la direcci�n en el extranjero, as� que si est� marcado
				//el check de residencia en el extranjero, habr� que utilizar este campo (DDIREEXT).
				//rqRequeridos(9).Value = Trim(tbDPDomicilioP)
				if (chbDPExtrangero.CheckState == CheckState.Unchecked)
				{
					rqRequeridos.Parameters[9].Value = tbDPDomicilioP.Text.Trim();
				}
				else
				{
					//(chbDPExtrangero.Value = 1)
					rqRequeridos.Parameters[9].Value = tbDPDireccionEx.Text.Trim();
				}
				//-----
				rqRequeridos.Parameters[10].Value = tbDPNDomic.Text.Trim();
				rqRequeridos.Parameters[11].Value = tbDPOtrosDomic.Text.Trim();
				rqRequeridos.Parameters[12].Value = mebDPCodPostal.Text.Trim();
				rqRequeridos.Parameters[13].Value = Convert.ToString(cbbDPPoblacion.Text).Trim();
				rqRequeridos.Parameters[14].Value = tbDPTelf1.Text.Trim();
				rqRequeridos.Parameters[15].Value = tbDPTelf2.Text.Trim();
				rqRequeridos.Parameters[16].Value = tbDPTelf3.Text.Trim();
				rqRequeridos.Parameters[17].Value = tbDPPasaporte.Text.Trim();
				rqRequeridos.Parameters[18].Value = Convert.ToString(cbbIdioma.Text).Trim();

				if (rbBloqueo[0].IsChecked)
				{
					rqRequeridos.Parameters[19].Value = "S";
				}
				else if (rbBloqueo[1].IsChecked)
				{ 
					rqRequeridos.Parameters[19].Value = "N";
				}
				else
				{
					rqRequeridos.Parameters[19].Value = "";
				}
				rqRequeridos.Parameters[20].Value = (int) chbDPExtrangero.CheckState;
				rqRequeridos.Parameters[21].Value = Convert.ToString(cbbDPPais.Text).Trim();
				rqRequeridos.Parameters[22].Value = tbDPExPob.Text.Trim();

				if (rbAutorizacion[0].IsChecked)
				{
					rqRequeridos.Parameters[23].Value = "S";
				}
				else if (rbAutorizacion[1].IsChecked)
				{ 
					rqRequeridos.Parameters[23].Value = "N";
				}
				else
				{
					rqRequeridos.Parameters[23].Value = "";
				}

				rqRequeridos.Parameters[24].Value = (int) chbDPExitus.CheckState;
				rqRequeridos.Parameters[25].Value = sdcDPFechaFallecimiento.Text;

				if (rbRERegimen[0].IsChecked)
				{
					rqRequeridos.Parameters[26].Value = 0;
				}
				else if (rbRERegimen[1].IsChecked)
				{ 
					rqRequeridos.Parameters[26].Value = 1;
				}
				else if (rbRERegimen[2].IsChecked)
				{ 
					rqRequeridos.Parameters[26].Value = 2;
				}

				rqRequeridos.Parameters[27].Value = Convert.ToString(cbbREInspeccion.Text).Trim();
				rqRequeridos.Parameters[28].Value = tbRENSS.Text.Trim();
				rqRequeridos.Parameters[29].Value = MEBCIAS.Text.Trim();


				if (rbActivo.IsChecked)
				{
					rqRequeridos.Parameters[30].Value = "A";
				}
				else if (rbPensionista.IsChecked)
				{ 
					rqRequeridos.Parameters[30].Value = "P";
				}
				else
				{
					rqRequeridos.Parameters[30].Value = "";
				}

				if (rbIndicadorSS[0].IsChecked)
				{
					rqRequeridos.Parameters[31].Value = "T";
				}
				else if (rbIndicadorSS[1].IsChecked)
				{ 
					rqRequeridos.Parameters[31].Value = "B";
				}
				else
				{
					rqRequeridos.Parameters[31].Value = "";
				}

				rqRequeridos.Parameters[32].Value = tbRENomTitularSS.Text.Trim();
				if (cbbRESociedad.SelectedIndex >= 0)
				{
					rqRequeridos.Parameters[33].Value = cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value;
				}
				else
				{
					rqRequeridos.Parameters[33].Value = 0;
				}
				rqRequeridos.Parameters[34].Value = tbRENPoliza.Text.Trim();
				rqRequeridos.Parameters[35].Value = tbREEmpresa.Text.Trim();

				if (rbIndicador[0].IsChecked)
				{
					rqRequeridos.Parameters[36].Value = "T";
				}
				else if (rbIndicador[1].IsChecked)
				{ 
					rqRequeridos.Parameters[36].Value = "B";
				}
				else
				{
					rqRequeridos.Parameters[36].Value = "";
				}

				rqRequeridos.Parameters[37].Value = tbRENomTitular.Text.Trim();

				if (rbDNIOtroDocRE[0].IsChecked)
				{
					rqRequeridos.Parameters[38].Value = "0";
				}
				else if (rbDNIOtroDocRE[1].IsChecked)
				{ 
					rqRequeridos.Parameters[38].Value = "1";
				}
				else
				{
					rqRequeridos.Parameters[38].Value = "";
				}

				rqRequeridos.Parameters[39].Value = tbNIFOtrosDocRE.Text.Trim();
				rqRequeridos.Parameters[40].Value = tbREPasaporte.Text.Trim();
				rqRequeridos.Parameters[41].Value = tbREApe1.Text.Trim();
				rqRequeridos.Parameters[42].Value = tbREApe2.Text.Trim();
				rqRequeridos.Parameters[43].Value = tbRENombre.Text.Trim();
				rqRequeridos.Parameters[44].Value = tbREDomicilio.Text.Trim();
				rqRequeridos.Parameters[45].Value = mebRECodPostal.Text.Trim();
				rqRequeridos.Parameters[46].Value = Convert.ToString(cbbREPoblacion.Text).Trim();
				rqRequeridos.Parameters[47].Value = (int) chbREExtrangero.CheckState;
				rqRequeridos.Parameters[48].Value = Convert.ToString(cbbREPais.Text).Trim();
				rqRequeridos.Parameters[49].Value = tbREExPob.Text.Trim();
				rqRequeridos.Parameters[50].Value = Convert.ToString(cbbREPago.Text).Trim();
				rqRequeridos.Parameters[51].Value = tbBanco.Text.Trim();
				rqRequeridos.Parameters[52].Value = tbSucursal.Text.Trim();
				rqRequeridos.Parameters[53].Value = tbControl.Text.Trim();
				rqRequeridos.Parameters[54].Value = tbCuenta.Text.Trim();
				rqRequeridos.Parameters[55].Value = MFiliacion.sUsuario.Trim();

				rqRequeridos.ExecuteNonQuery();

				if (Convert.ToBoolean(rqRequeridos.Parameters[56].Value))
				{
					result = false;
					RadMessageBox.Show(Convert.ToString(rqRequeridos.Parameters[57].Value), "Incidencias en filiaci�n de pacientes.", MessageBoxButtons.OK, RadMessageIcon.Exclamation);


					foreach (Control Control in ContainerHelper.Controls(this))
					{
						if (Convert.ToString(rqRequeridos.Parameters[58].Value) == Control.Name)
						{

							//''' Antes de establecer el foco al control del error, hay que desplazarse a la
							//''' pesta�a donde se encuentra el control.
							SSTabHelper.SetSelectedIndex(tsDatosPac, Convert.ToInt32(rqRequeridos.Parameters[60].Value));
							if (Convert.ToString(rqRequeridos.Parameters[58].Value) == "tbNIFOtrosDocDP")
							{
								rbDNIOtroDocDP[0].IsChecked = true;
							}
							Control.Focus();
							break;
						}
					}


				}
				else
				{
					result = true;
				}
                
                rqRequeridos.Dispose();
                rqRequeridos = null;
				
				Cursor = Cursors.Default;
				ErrRequeridos = false;
				return result;
			}
			catch (SqlException excep)
			{
				if (!ErrRequeridos)
				{
					throw excep;
				}

				if (ErrRequeridos)
				{					
					Serrores.GestionErrorBaseDatos(excep, MFiliacion.GConexion);
					return result;
				}
			}
			return false;
		}

		private void cbbDPPais_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void cbbDPPais_Click(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void cbbDPPoblacion_Click(Object eventSender, EventArgs eventArgs)
		{
			string mensaje = String.Empty;
			string sCodigoPostal = String.Empty;

			if (!bFormatoCodigoPos)
			{
				sCodigoPostal = mebDPCodPostal.Text.Trim();
				mebDPCodPostal.Text = "";
				mebDPCodPostal.Tag = mebDPCodPostal.Text;
			}

			if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
			{
				//busca el codigo postal
				object tempRefParam = 1;
				object tempRefParam2 = cbbDPPoblacion.get_ListIndex();

                object tempRefParam3 = 1;
                object tempRefParam4 = cbbDPPoblacion.get_ListIndex();
                Codigoprov1 = StringsHelper.Format(Convert.ToString(cbbDPPoblacion.get_Column(tempRefParam, tempRefParam2)).Substring(0, Math.Min(2, Convert.ToString(cbbDPPoblacion.get_Column(tempRefParam, tempRefParam2)).Length)), "00");
				SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT gcodipos FROM DCODIPOS WHERE GPOBLACI = '" + StringsHelper.Format(cbbDPPoblacion.get_Column(tempRefParam3, tempRefParam4), "000000") + "'", MFiliacion.GConexion);
				RrCodPostal = new DataSet();
				tempAdapter.Fill(RrCodPostal);
				
				if (RrCodPostal.Tables[0].Rows.Count != 0)
				{
					if (RrCodPostal.Tables[0].Rows.Count == 1)
					{
						//(maplaza)(18/04/2007)
						if (mebDPCodPostal.Text.Trim() == "")
						{
							//-----
							if (bFormatoCodigoPos)
							{
								if (bcambiar)
								{
									mebDPCodPostal.Text = StringsHelper.Format(RrCodPostal.Tables[0].Rows[0]["gcodipos"], "00000");
									//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
									mebDPCodPostal.Tag = mebDPCodPostal.Text;
									//-----
								}
							}
							else
							{
								if (bcambiar)
								{
									mebDPCodPostal.Text = (Convert.ToString(RrCodPostal.Tables[0].Rows[0]["gcodipos"]) + "").Trim();
									//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
									mebDPCodPostal.Tag = mebDPCodPostal.Text;
									//-----
								}
							}
							//-------
						}
					}
				}
				RrCodPostal.Close();

				if (!bFormatoCodigoPos && sCodigoPostal.Trim() != "" && mebDPCodPostal.Text.Trim() == "")
				{
					mebDPCodPostal.Text = sCodigoPostal.Trim();
					mebDPCodPostal.Tag = mebDPCodPostal.Text;
				}
			}
		}

		//(maplaza)(08/06/2006)Si se elimina manualmente lo que hab�a escrito en el combo, hay que resetear la
		//variable "Codigoprov1"
		private void cbbDPPoblacion_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
		{
			if (cbbDPPoblacion.Text.Trim() == "")
			{
				Codigoprov1 = "";
			}
		}

		//(maplaza)(16/04/2007)Esto se hace para que cuando se haga click en el bot�n para desplegar el combo, no est�
		//a la vez carg�ndose el combo, y no se produzca un efecto visual extra�o.
		private void cbbDPPoblacion_DropButtonClick(Object eventSender, EventArgs eventArgs)
		{
			Application.DoEvents();
			Application.DoEvents();
		}

		private void CbbObraSocial_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			DataSet RrDSOCIEDA = null;

			try
			{
				cbbRESociedad.SelectedIndex = -1;
				bCambiandoMascara = true;
				tbRENPoliza.Mask = "";
				bCambiandoMascara = false;
				ToolTipMain.SetToolTip(tbRENPoliza, "");
				tbRENPoliza.Text = "";
				chkExentoIVA.CheckState = CheckState.Unchecked;
				tbRENVersion.Text = "";
				//(maplaza)(19/09/2007)
				tbRECobertura.Text = "";
				mebREFechaAlta.Text = "  /    ";
				mebREFechaCaducidad.Text = "  /    ";
				//-----
				this.tbREEmpresa.Text = "";
				rbIndicador[0].IsChecked = true;
				cbbRESociedad.Items.Clear();
				TBBandaMagnetica.Text = "";
				string strterc = String.Empty;
				string steterc = String.Empty;
				if (CbbObraSocial.SelectedIndex > 0)
				{
					strterc = CbbObraSocial.Items[CbbObraSocial.SelectedIndex].Text.ToString().Substring(Math.Max(CbbObraSocial.Items[CbbObraSocial.SelectedIndex].Text.ToString().Length - 17, 0)).Trim().Substring(0, Math.Min(2, CbbObraSocial.Items[CbbObraSocial.SelectedIndex].Text.ToString().Substring(Math.Max(CbbObraSocial.Items[CbbObraSocial.SelectedIndex].Text.ToString().Length - 17, 0)).Trim().Length));
					steterc = CbbObraSocial.Items[CbbObraSocial.SelectedIndex].Text.ToString().Substring(Math.Max(CbbObraSocial.Items[CbbObraSocial.SelectedIndex].Text.ToString().Length - 17, 0)).Trim().Substring(2);

					SqlDataAdapter tempAdapter = new SqlDataAdapter("select * from ifmsfa_dsocieda where fborrado is null and FDESACTI is null and ginspecc = '0' and rterc = '" + strterc + "' and eterc = '" + steterc + "' order by dsocieda ", MFiliacion.GConexion);
					RrDSOCIEDA = new DataSet();
					tempAdapter.Fill(RrDSOCIEDA);
					RrDSOCIEDA.MoveFirst();
                    cbbRESociedad.Items.Clear();
                    foreach (DataRow iteration_row in RrDSOCIEDA.Tables[0].Rows)
					{
						if (Convert.ToString(iteration_row["gsocieda"]).Trim().ToUpper() != MFiliacion.CodSS.ToString().Trim().ToUpper() && Convert.ToString(iteration_row["gsocieda"]).Trim().ToUpper() != MFiliacion.CodPriv.ToString().Trim().ToUpper())
						{
                            cbbRESociedad.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row["dsocieda"]).Trim(), VbStrConv.ProperCase, 0), Convert.ToInt32(iteration_row["gsocieda"])));                            
						}
					}
					RrDSOCIEDA.Close();

				}
			}
			catch
			{
				RrDSOCIEDA = null;
			}
        }

		private void cbbPCPais_SelectedIndexChanged(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void cbbPCPais_Click(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void cbbPCPoblacion_Click(Object eventSender, EventArgs eventArgs)
		{
			string sCodigoPostal = String.Empty;

			if (!estabuscando)
			{ //si no se est�n cargando datos desde BBDD

				if (!bFormatoCodigoPos)
				{
					sCodigoPostal = mebPCCodPostal.Text.Trim();
					mebPCCodPostal.Text = "";
					mebPCCodPostal.Tag = mebPCCodPostal.Text;
				}

				if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) >= 0)
				{
					//busca el codigo postal
					object tempRefParam = 1;
					object tempRefParam2 = cbbPCPoblacion.get_ListIndex();

					Codigoprov3 = StringsHelper.Format(StringsHelper.Format(cbbPCPoblacion.get_Column(tempRefParam, tempRefParam2), "000000").Substring(0, Math.Min(2, StringsHelper.Format(cbbPCPoblacion.get_Column(tempRefParam, tempRefParam2), "000000").Length)), "00");

                    object tempRefParam3 = 1;
                    object tempRefParam4 = cbbPCPoblacion.get_ListIndex();

                    SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT gcodipos FROM DCODIPOS WHERE GPOBLACI = '" + StringsHelper.Format(cbbPCPoblacion.get_Column(tempRefParam3, tempRefParam4), "000000") + "'", MFiliacion.GConexion);
					RrCodPostal = new DataSet();
					tempAdapter.Fill(RrCodPostal);
					
					if (RrCodPostal.Tables[0].Rows.Count != 0)
					{
						if (RrCodPostal.Tables[0].Rows.Count == 1)
						{

							//(maplaza)(18/04/2007)
							if (mebPCCodPostal.Text.Trim() == "")
							{
								//-----
								if (bFormatoCodigoPos)
								{
									if (mebPCCodPostal.Text != StringsHelper.Format(RrCodPostal.Tables[0].Rows[0]["gcodipos"], "00000"))
									{
										mebPCCodPostal.Text = StringsHelper.Format(RrCodPostal.Tables[0].Rows[0]["gcodipos"], "00000");
										mebPCCodPostal.Tag = mebPCCodPostal.Text;
									}
								}
								else
								{
									if (mebPCCodPostal.Text != (Convert.ToString(RrCodPostal.Tables[0].Rows[0]["gcodipos"]) + "").Trim())
									{
										mebPCCodPostal.Text = (Convert.ToString(RrCodPostal.Tables[0].Rows[0]["gcodipos"]) + "").Trim();
										mebPCCodPostal.Tag = mebPCCodPostal.Text;
									}
								}
								//-------
							}
						}
					}
					RrCodPostal.Close();
				}

				if (!bFormatoCodigoPos && sCodigoPostal.Trim() != "" && mebPCCodPostal.Text.Trim() == "")
				{
					mebPCCodPostal.Text = sCodigoPostal.Trim();
					mebPCCodPostal.Tag = mebPCCodPostal.Text;
				}

			}
		}

		//(maplaza)(16/04/2007)Esto se hace para que cuando se haga click en el bot�n para desplegar el combo, no est�
		//a la vez carg�ndose el combo, y no se produzca un efecto visual extra�o.
		private void cbbPCPoblacion_DropButtonClick(Object eventSender, EventArgs eventArgs)
		{
			Application.DoEvents();
			Application.DoEvents();
		}

		private void cbbREInspeccion_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			if (eventArgs.KeyValue == 8)
			{
				//eventArgs.KeyValue = 0;
			}
			if (eventArgs.KeyValue == 32)
			{
				//eventArgs.keyCode.Value = 0;
			}
		}

		private void cbbREInspeccion_Leave(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}


        public string getLisIndex(RadMultiColumnComboBox combo, int row, int col)
        {            
            string str_combo = ((RadGridView)combo.EditorControl).Rows[row].Cells[col].Value.ToString();
            return str_combo;
        }
		private void cbbREPago_Click(Object eventSender, EventArgs eventArgs)
		{			
			int tempRefParam = cbbREPago.SelectedIndex;
			int tempRefParam2 = 2;
            
            if (cbbREPago.SelectedIndex != -1 && getLisIndex(cbbREPago,tempRefParam,tempRefParam2) == "S")
			{
				Activa_N_Cuenta(true);
			}
			else
			{
				Activa_N_Cuenta(false);
			}
			COMPROBAR_DATOS();
		}

		private void cbbREPago_KeyDown(Object eventSender, System.Windows.Forms.KeyEventArgs eventArgs)
		{
			if (eventArgs.KeyValue == 8)
			{
                //DGMORENOG - PENDIENTE REVISAR
				//eventArgs.keyCode.Value = 0;
			}
			if (eventArgs.KeyValue == 32)
			{
                //DGMORENOG - PENDIENTE REVISAR
                //eventArgs.keyCode.Value = 0;
            }
        }

		private void cbbREPais_Click(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void cbBuscarCentro_Click(Object eventSender, EventArgs eventArgs)
		{
			Institucion.MCInstitucion Institucion = new Institucion.MCInstitucion();
			object tempRefParam = this;
			Institucion.BuscarCentro(tempRefParam, MFiliacion.GConexion);
			Institucion = null;
		}

		//Funcion para recoger los datos del centro buscado en la dll Institucion.BuscarCentro
		public void RecogerCentro(string stCodigoCentro, string stDescCentro)
		{
			if (stCodigoCentro != "")
			{
				for (int i = 0; i <= cbbHospitalReferencia.Items.Count - 1; i++)
				{
					if (int.Parse(cbbHospitalReferencia.Items[i].Value.ToString()) == Conversion.Val(stCodigoCentro))
					{
						cbbHospitalReferencia.SelectedIndex = i;
						break;
					}
				}
			}
		}
		//---------------

		private void cbBuscarSoc_Click(Object eventSender, EventArgs eventArgs)
		{            
            DFI111F2.DefInstance.stEmpresa = tbREEmpresa.Text;
			DFI111F2 tempLoadForm = DFI111F2.DefInstance;
			DFI111F2.DefInstance.ShowDialog();
        }

        private void cbDBuscar4_Click(Object eventSender, EventArgs eventArgs)
		{            
            Poblaciones.MCPoblaciones oPoblaciones = new Poblaciones.MCPoblaciones();					
			oPoblaciones.llamada(this, MFiliacion.GConexion, ref cbbNacPoblacion);			
			oPoblaciones = null;
		}

		private void cbPCAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int iFilaActual = 0;

			// Se valida que los datos introducidos sean correctos
			//(maplaza)(23/04/2007)Se comprueba si los dos primeros d�gitos del C�digo Postal existen o no en la tabla de Provincias
			//Jesus 27/07/2011 No se hace para Argentina
			if (bFormatoCodigoPos)
			{
				if (!ComprobarDigitosCP(2))
				{
					return;
				}
			}
			//-----

			// Se actualizan los datos de la persona de contacto en la lista

			if (iFilaContactos != 0)
			{
				sprContactos.Row = iFilaContactos;
			}
			else
			{
				sprContactos.MaxRows++;
				sprContactos.Row = sprContactos.MaxRows;
			}
			sprContactos.Col = 1;
			sprContactos.Text = tbPCApe1.Text.Trim();
			if (tbPCApe2.Text.Trim() != "")
			{
				sprContactos.Text = sprContactos.Text + " " + tbPCApe2.Text.Trim();
			}
			sprContactos.Text = sprContactos.Text + ", " + tbPCNombre.Text.Trim();
			//(maplaza)(03/05/2006)Se tratar� la letra s�lo si est� seleccionado el radio button "DNI/NIF"
			if (rbDNIOtroDocPC[0].IsChecked)
			{
				if (tbNIFOtrosDocPC.Text.Trim().Length == 8)
				{
					if (MFiliacion.bCalcLetra)
					{
						MFiliacion.irespuesta = RadMessageBox.Show("La letra para el DNI/NIF del contacto es la " + MFiliacion.ValidarLetraNif(tbNIFOtrosDocPC.Text.Trim()) + ". �Desea asignarla?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Info);
						if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.Yes)
						{
							tbNIFOtrosDocPC.Text = tbNIFOtrosDocPC.Text.Trim() + MFiliacion.ValidarLetraNif(tbNIFOtrosDocPC.Text.Trim());
						}
					}
				}
				else if (tbNIFOtrosDocPC.Text.Trim().Length == 9)
				{ 
					if (MFiliacion.bCalcLetraObligatoria)
					{
						if (tbNIFOtrosDocPC.Text.Trim().Substring(8, Math.Min(1, tbNIFOtrosDocPC.Text.Trim().Length - 8)) != MFiliacion.ValidarLetraNif(tbNIFOtrosDocPC.Text.Trim()))
						{
							MFiliacion.irespuesta = RadMessageBox.Show("La letra " + tbNIFOtrosDocPC.Text.Trim().Substring(8, Math.Min(1, tbNIFOtrosDocPC.Text.Trim().Length - 8)) + " es incorrecta para el DNI/NIF del contacto: " + tbNIFOtrosDocPC.Text.Trim().Substring(0, Math.Min(8, tbNIFOtrosDocPC.Text.Trim().Length)) + ". La letra correcta es " + MFiliacion.ValidarLetraNif(tbNIFOtrosDocPC.Text.Trim()) + ".�Desea asignar la nueva?", Application.ProductName, MessageBoxButtons.YesNo, RadMessageIcon.Info);
							if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.Yes)
							{
								tbNIFOtrosDocPC.Text = tbNIFOtrosDocPC.Text.Trim().Substring(0, Math.Min(8, tbNIFOtrosDocPC.Text.Trim().Length)) + MFiliacion.ValidarLetraNif(tbNIFOtrosDocPC.Text.Trim());
							}
						}
					}
				}
			}
			sprContactos.Col = 2;
			sprContactos.Text = tbNIFOtrosDocPC.Text.Trim(); //(maplaza)(03/05/2006)
			sprContactos.Col = 3;
			sprContactos.Text = tbPCDomicilio.Text.Trim();
			sprContactos.Col = 4;
			sprContactos.Text = mebPCCodPostal.Text.Trim();
			sprContactos.Col = 5;
			sprContactos.Text = Convert.ToString(cbbPCPoblacion.Text).Trim();
			sprContactos.Col = 6;
			if (tbPCTelf1.Text.Trim() != "")
			{
				sprContactos.Text = tbPCTelf1.Text.Trim();
				if (tbPCExt1.Text.Trim() != "")
				{
					sprContactos.Text = sprContactos.Text + "-" + tbPCExt1.Text.Trim();
				}
			}
			else
			{
				sprContactos.Text = "";
			}
			sprContactos.Col = 7;
			if (tbPCTelf2.Text.Trim() != "")
			{
				sprContactos.Text = tbPCTelf2.Text.Trim();
				if (tbPCExt2.Text.Trim() != "")
				{
					sprContactos.Text = sprContactos.Text + "-" + tbPCExt2.Text.Trim();
				}
			}
			else
			{
				sprContactos.Text = "";
			}
			sprContactos.Col = 8;
			sprContactos.Text = tbPCParentesco.Text.Trim();
			sprContactos.Col = 9;
			sprContactos.Text = (rbResponsable[0].IsChecked) ? "SI" : "NO";
			sprContactos.Col = 10;
			if (Convert.ToDouble(cbbPCPais.get_ListIndex()) != -1)
			{
				sprContactos.Text = cbbPCPais.Text.Trim();
			}
			else
			{
				sprContactos.Text = "";
			}
			sprContactos.Col = 11;
			sprContactos.Text = tbPCExPob.Text.Trim();
			sprContactos.Col = 13;
			if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) >= 0)
			{
				object tempRefParam = 1;
				object tempRefParam2 = cbbPCPoblacion.get_ListIndex();
				sprContactos.Text = StringsHelper.Format(cbbPCPoblacion.get_Column(tempRefParam, tempRefParam2), "000000");
			}
			else
			{
				sprContactos.Text = "";
			}
			sprContactos.Col = 14;
			if (Convert.ToDouble(cbbPCPais.get_ListIndex()) != -1)
			{
				object tempRefParam3 = 1;
				object tempRefParam4 = cbbPCPais.get_ListIndex();
				sprContactos.Text = Convert.ToString(cbbPCPais.get_Column(tempRefParam3, tempRefParam4));
			}
			else
			{
				sprContactos.Text = "";
			}
			sprContactos.Col = 15;
			sprContactos.Text = tbPCNombre.Text.Trim();
			sprContactos.Col = 16;
			sprContactos.Text = tbPCApe1.Text.Trim();
			sprContactos.Col = 17;
			sprContactos.Text = tbPCApe2.Text.Trim();
			sprContactos.Col = 19;
			if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) >= 0)
			{
				object tempRefParam5 = 1;
				object tempRefParam6 = cbbPCPoblacion.get_ListIndex();
				sprContactos.Text = StringsHelper.Format(cbbPCPoblacion.get_Column(tempRefParam5, tempRefParam6), "000000").Substring(0, Math.Min(2, StringsHelper.Format(cbbPCPoblacion.get_Column(tempRefParam5, tempRefParam6), "000000").Length));
			}
			else
			{
				sprContactos.Text = "";
			}
			// Si el paciente ya ten�a la persona de contacto se almacena que
			// ha sido modificada. En caso contrario se guarda que es nueva
			sprContactos.Col = 18;
			if (iFilaContactos != 0)
			{
				if (sprContactos.Text == "EXISTE")
				{
					sprContactos.Text = "MODIFICADA";
				}
			}
			else
			{
				sprContactos.Text = "NUEVA";
				iUltimoNOrden++;
				sprContactos.Col = 12;
				sprContactos.Text = iUltimoNOrden.ToString();
			}


			//OSCAR C 21/11/2005
			sprContactos.Col = 20;
			if (rbTutor[0].IsChecked)
			{
				sprContactos.Text = "T";
			}
			else
			{
				if (rbTutor[1].IsChecked)
				{
					sprContactos.Text = "C";
				}
				else
				{
					sprContactos.Text = "";
				}
			}
			sprContactos.Col = 21;
			if (rbTutor[0].IsChecked)
			{
				sprContactos.Text = rbTutor[0].Text.Trim().ToUpper();
			}
			else
			{
				if (rbTutor[1].IsChecked)
				{
					sprContactos.Text = rbTutor[1].Text.Trim().ToUpper();
				}
				else
				{
					sprContactos.Text = "";
				}
			}
			//------------------

			// Si se ha indicado que es la persona responsable se revisa que
			// no haya otra persona responsable. Si la hay se cambia
			if (rbResponsable[0].IsChecked)
			{
				iFilaActual = sprContactos.Row;
				int tempForVar = sprContactos.MaxRows;
				for (int i = 1; i <= tempForVar; i++)
				{
					if (i != iFilaActual)
					{
						sprContactos.Row = i;
						sprContactos.Col = 9;
						sprContactos.Text = "NO";
						sprContactos.Col = 18;
						if (sprContactos.Text == "EXISTE")
						{
							sprContactos.Text = "MODIFICADA";
						}
					}
				}
			}


			// Se limpian los datos de la parte inferior
			//(maplaza)(11/07/2006)El evento "cbPCCancelar_Click" se encarga de llamar al m�todo "CargarDatosPorDefectoFamiliarContacto"
			cbPCCancelar_Click(cbPCCancelar, new EventArgs());

		}

		//oscar 28/10/2004
		private void cmdDeudas_Click(Object eventSender, EventArgs eventArgs)
		{
			MFiliacion.gMostrarDeudas = true;
			this.Close();
		}

		//22/11/2004
		private void cmdFacturacionP_Click(Object eventSender, EventArgs eventArgs)
		{
			MFiliacion.gMostrarFacturacion = true;
			this.Close();
		}
		//--------------

		private void cmdConsultaActualizacion_Click(Object eventSender, EventArgs eventArgs)
		{            
            DFI111F5.DefInstance.ShowDialog();
        }

        private void DFI111F1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
            IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
            int hotKey = IMDHOPEN.TeclaFuncionOpen(MFiliacion.GConexion);
            if (e.KeyCode == (Keys)Enum.Parse(typeof(Keys), hotKey.ToString()))
            {
               IMDHOPEN.LlamadaSP(MFiliacion.GConexion, MFiliacion.sUsuario, this.Name, "TECLAFUNCION", MFiliacion.ModCodigo, "", true, 0, 0);
               IMDHOPEN = null;
            }
        }

        private void cmdOpen_Click(Object eventSender, EventArgs eventArgs)
		{
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
            UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
            

            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();

			string tempRefParam = this.Name;
			string tempRefParam2 = "OpenImdh";
			string tempRefParam3 = "";
			bool tempRefParam4 = false;
			IMDHOPEN.LlamadaSP(MFiliacion.GConexion, MFiliacion.sUsuario, tempRefParam, tempRefParam2, MFiliacion.ModCodigo, tempRefParam3, tempRefParam4, Mouse.X, Mouse.Y);
			IMDHOPEN = null;
		}

		private void chkExentoIVA_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		//(maplaza)(30/08/2007)Si se marca el check de "Propio Paciente", los datos del paciente de la primera pesta�a se trasladan
		//a la tabla "DPRIVADO" (apartado de "Privado" en la pesta�a de "R�gimen Econ�mico")
		private void chkPropioPaciente_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			int elemento = 0;

			if ((chkPropioPaciente.CheckState == CheckState.Unchecked))
			{
				return;
			}

			if (tbNIFOtrosDocDP.Text.Trim() != "")
			{
				if (MFiliacion.EsDNI_NIF(tbNIFOtrosDocDP.Text.Trim(), 9))
				{
					rbDNIOtroDocRE[0].IsChecked = true;
					rbDNIOtroDocRE[1].IsChecked = false;
				}
				else
				{
					//es la opci�n "Otros Documentos"
					rbDNIOtroDocRE[0].IsChecked = false;
					rbDNIOtroDocRE[1].IsChecked = true;
				}
				tbNIFOtrosDocRE.Enabled = true;
				if (tbNIFOtrosDocDP.Text.Trim().Length == 9)
				{
					tbNIFOtrosDocRE.Text = tbNIFOtrosDocDP.Text.Trim();
				}
				else
				{
					tbNIFOtrosDocRE.Text = StringsHelper.Format(tbNIFOtrosDocDP.Text.Trim(), "00000000") + " ";
				}
			}
			else
			{
				//(Trim(tbNIFOtrosDocDP.Text) = "")
				tbNIFOtrosDocRE.Mask = "";				
				tbNIFOtrosDocRE.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;
				tbNIFOtrosDocRE.Text = tbNIFOtrosDocDP.Text.Trim();
			}

			tbRENombre.Text = tbNombre.Text.Trim();
			tbREApe1.Text = tbApellido1.Text.Trim();
			tbREApe2.Text = tbApellido2.Text.Trim();
			tbREDomicilio.Text = tbDPDomicilio.Text;
			tbREPasaporte.Text = tbDPPasaporte.Text; //pasaporte
			mebRECodPostal.Text = mebDPCodPostal.Text;
			mebRECodPostal.Text = mebDPCodPostal.Text;
			if (tbREDomicilio.Text.Trim() == "")
			{
				if (Convert.ToDouble(cbbDPVia.get_ListIndex()) != -1)
				{
					object tempRefParam = 0;
					object tempRefParam2 = cbbDPVia.get_ListIndex();
					tbDPDomicilio.Text = Convert.ToString(cbbDPVia.get_Column(tempRefParam, tempRefParam2)).Trim().ToUpper();
				}
				if (tbDPDomicilioP.Text != "")
				{
					tbDPDomicilio.Text = tbDPDomicilio.Text + " " + tbDPDomicilioP.Text.Trim().ToUpper();
				}
				if (tbDPNDomic.Text != "")
				{
					tbDPDomicilio.Text = tbDPDomicilio.Text + " " + tbDPNDomic.Text.Trim().ToUpper();
				}
				if (tbDPOtrosDomic.Text != "")
				{
					tbDPDomicilio.Text = tbDPDomicilio.Text + " " + tbDPOtrosDomic.Text.Trim().ToUpper();
				}
				tbREDomicilio.Text = tbDPDomicilio.Text;
			}
			if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
			{
				for (int i = 0; i <= cbbREPoblacion.ListCount - 1; i++)
				{
					object tempRefParam3 = 1;
					object tempRefParam4 = i;
					object tempRefParam5 = 1;
					object tempRefParam6 = cbbDPPoblacion.get_ListIndex();
					if (cbbREPoblacion.get_Column(tempRefParam3, tempRefParam4).Equals(cbbDPPoblacion.get_Column(tempRefParam5, tempRefParam6)))
					{
						i = Convert.ToInt32(tempRefParam4);
						cbbREPoblacion.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam4);
					}
				}
			}
			//SI NO LA HA ENCONTRADO Y NO HAY NINGUNO SELECCIONADO
			if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) == -1 && Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
			{
				//CARGAMOS LAS DE DATOS PACIENTE EN REGIMEN ECONOMICO
				cbbREPoblacion.Clear();
				elemento = Convert.ToInt32(cbbDPPoblacion.get_ListIndex());
				for (int i = 0; i <= cbbDPPoblacion.ListCount - 1; i++)
				{
					object tempRefParam8 = 0;
					object tempRefParam9 = i;
					object tempRefParam7 = cbbDPPoblacion.get_Column(tempRefParam8, tempRefParam9);
					object tempRefParam10 = i;
					cbbREPoblacion.AddItem(tempRefParam7, tempRefParam10);
					i = Convert.ToInt32(tempRefParam10);
					i = Convert.ToInt32(tempRefParam9);
					object tempRefParam11 = 1;
					object tempRefParam12 = i;

                    cbbREPoblacion.set_List(i, 1, cbbDPPoblacion.get_Column(tempRefParam11, tempRefParam12));
					i = Convert.ToInt32(tempRefParam12);
				}
				//ponemos el elemento que tenia y
				//buscamos en regimen economico
				cbbDPPoblacion.set_ListIndex(elemento);
				if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
				{
					cbbREPoblacion.set_ListIndex(elemento);
				}
			}
			chbREExtrangero.CheckState = chbDPExtrangero.CheckState;
			cbbREPais.set_ListIndex(cbbDPPais.get_ListIndex());
			tbREExPob.Text = tbDPExPob.Text;
			tbREDireccionEx.Text = tbDPDireccionEx.Text;
		}

		private void DFI111F1_Closed(Object eventSender, EventArgs eventArgs)
		{
			if (!bcbaceptar)
			{
				this.Cursor = Cursors.WaitCursor;
				Vaciar_datos();
				MFiliacion.paso = false;
				LIMPIAR_FORMULARIO();
				this.Cursor = Cursors.Default;
				MFiliacion.CodPac = false;
			}
            DefInstance = null;
			MemoryHelper.ReleaseMemory();
		}

		private void lbCipAutonomico_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			if (KeyAscii == 8)
			{
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 9)
			{
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}

			if ((KeyAscii < 48 || KeyAscii > 57) && (KeyAscii < 65 || KeyAscii > 90) && (KeyAscii < 97 || KeyAscii > 122) && KeyAscii != 241 && KeyAscii != 209)
			{
				KeyAscii = 0;
			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void MEBCIAS_Enter(Object eventSender, EventArgs eventArgs)
		{
			MEBCIAS.SelectionStart = 0;
			MEBCIAS.SelectionLength = MEBCIAS.Text.Trim().Length;
			stCIAS = MEBCIAS.Text.Trim();
		}

		private void MEBCIAS_Leave(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
			EsZona();
			if (stCIAS != MEBCIAS.Text.Trim())
			{
				Restringir_Inspeccion(true);
			}
			//OSCAR C Noviembre 2009
			string sqlAux = "select DHOSPITA.ghospita, DHOSPITA.dnomhosp " + 
			                " From DHOSPITA " + 
			                "    inner join DCENSALU on DCENSALU.ghospref = DHOSPITA.ghospita " + 
			                "    inner join DCODCIAS on DCODCIAS.gcensalu = DCENSALU.gcensalu " + 
			                " WHERE DCODCIAS.gcodcias= '" + MEBCIAS.Text + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlAux, MFiliacion.GConexion);
			DataSet rrAux = new DataSet();
			tempAdapter.Fill(rrAux);
			if (rrAux.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(rrAux.Tables[0].Rows[0]["ghospita"]))
				{
					cbbHospitalReferencia.SelectedIndex = Serrores.fnBuscaListIndexID(cbbHospitalReferencia, rrAux.Tables[0].Rows[0]["ghospita"].ToString());
				}
			}
			rrAux.Close();
			//--------------
		}

		private void mebDPCodPostal_Enter(Object eventSender, EventArgs eventArgs)
		{
			mebDPCodPostal.SelectionStart = 0;
			mebDPCodPostal.SelectionLength = mebDPCodPostal.Text.Trim().Length;
		}

		private void mebDPCodPostal_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (mebDPCodPostal.Text.Trim().Length == 0)
			{
				bcambiar = true;
			}
			if (!bcambiar)
			{
				//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
				mebDPCodPostal.Tag = mebDPCodPostal.Text;
				//-----
				return;
			}
			//Jes�s 27/07/2011 Para Argentina no se realiza la validaci�n de que el
			//c�digo postal deba tener 5 d�gitos, no se rellena con 0 por delante ni
			//se valida que los dos primeros deben ser de la poblaci�n.
			if (bFormatoCodigoPos)
			{
				//(maplaza)(16/04/2007)
				if ((mebDPCodPostal.Text.Trim().Length > 0) && (mebDPCodPostal.Text.Trim().Length < 5))
				{
					mebDPCodPostal.Text = StringsHelper.Format(mebDPCodPostal.Text, "00000");
				}
			}

			//-----
			//(maplaza)(30/05/2006)
			if (mebDPCodPostal.Text.Trim().Length > 0)
			{
				//stCodigoPostal = mebDPCodPostal.Text  '(maplaza)(30/05/2006)
				//(maplaza)(18/04/2007)
				//Buscar_poblaciones
				//si se introduce un C�digo Postal que existe en la tabla DCODIPOS
				if (fExisteCodigoPostal(mebDPCodPostal.Text.Trim()))
				{
					//si no est� introducida la poblaci�n, cargar el combo con las poblaciones relacionadas con el c�digo postal.
					if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) == -1)
					{
						Buscar_poblaciones();
					}
				}
				else
				{
					//Jes�s 27/07/2011 para Argentina no se realiza esta validaci�n
					if (bFormatoCodigoPos)
					{
						//(maplaza)(23/04/2007)
						//si no existe el c�digo postal en la tabla DCODIPOS, pero los dos primeros d�gitos del C�digo Postal se
						//corresponden con una provincia (tabla DPROVINC) y no est� seleccionada ninguna poblaci�n, entonces se
						//cargan las poblaciones correspondientes a esta provincia asociada a los dos primeros d�gitos del C�digo Postal.
						if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) == -1)
						{
							if (fExisteCodigoProvincia(mebDPCodPostal.Text.Trim().Substring(0, Math.Min(2, mebDPCodPostal.Text.Trim().Length))))
							{                                
                                CargarPoblacionesProvincia(mebDPCodPostal.Text.Trim().Substring(0, Math.Min(2, mebDPCodPostal.Text.Trim().Length)), cbbDPPoblacion);
							}
						}
					}
					//-----
				}
				//-----
			}
			//-----
			if (mebDPCodPostal.Text.Trim().Length == 0)
			{
				//stCodigoPostal = mebDPCodPostal.Text  '(maplaza)(30/05/2006)
				if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) == -1)
				{
					Cargar_PobDefecto();
				}
			}

			if (bFormatoCodigoPos)
			{
				//(maplaza)(30/05/2006)
				if (mebDPCodPostal.Text.Trim().Length != 0)
				{
					mebDPCodPostal.Text = StringsHelper.Format(mebDPCodPostal.Text, "00000");
				}
			}
			//-----
			//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
			mebDPCodPostal.Tag = mebDPCodPostal.Text;
			//-----
		}

		//(maplaza)(23/04/2007)Se cargan en el combo (que se pasa como par�metro) las poblaciones cuyo C�digo Postal
		//se corresponde con los d�gitos de la provincia (que se pasan como par�metro).
		private void CargarPoblacionesProvincia(string stCodProvincia, RadDropDownList cbCombo)
		{
			cbCombo.Items.Clear();
			string SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + stCodProvincia + "%' order by dpoblaci";
			CargarComboAlfanumerico(SQLPOBLACION, "gpoblaci", "dpoblaci", cbCombo);
		}

		//(maplaza)(03/09/2007)
		private void mebREFechaAlta_Enter(Object eventSender, EventArgs eventArgs)
		{
			mebREFechaAlta.SelectionStart = 0;
			mebREFechaAlta.SelectionLength = Strings.Len(mebREFechaAlta.Mask);
		}

		//(maplaza)(03/09/2007)
		private void mebREFechaAlta_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (this.ActiveControl.Name == cbCancelar.Name)
			{ //Si quiere salir, le dejamos sin validar
				cbCancelar_Click(cbCancelar, new EventArgs());
				return;
			}
			if (mebREFechaAlta.Text.Trim() == "/")
			{
				return;
			}
			if (mebREFechaAlta.Text.IndexOf(' ') >= 0)
			{ // si encuentra un espacio en blanco
				string tempRefParam = "R�gimen Econ�mico";
				string tempRefParam2 = "";
				short tempRefParam3 = 1210;
				string[] tempRefParam4 = new string[] { "la fecha inicial"};
				MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, MFiliacion.GConexion, tempRefParam4);
				mebREFechaAlta.Text = "  /    ";
				mebREFechaAlta.Focus();
				return;
			}
			if (Convert.ToInt32(Double.Parse(mebREFechaAlta.Text.Substring(0, Math.Min(2, mebREFechaAlta.Text.Length)))) >= 13 || Convert.ToInt32(Double.Parse(mebREFechaAlta.Text.Substring(0, Math.Min(2, mebREFechaAlta.Text.Length)))) < 1)
			{ //si ha puesto un mes superior o igual a 13, o bien menor que 1
				string tempRefParam5 = "R�gimen Econ�mico";
				string tempRefParam6 = "";
				short tempRefParam7 = 1210;
                string[] tempRefParam8 = new string[] { "la fecha inicial"};
				MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam5, tempRefParam6, tempRefParam7, MFiliacion.GConexion, tempRefParam8);
				mebREFechaAlta.Text = "  /    ";
				mebREFechaAlta.Focus();
				return;
			}
		}

		//(maplaza)(03/09/2007)
		private void mebREFechaCaducidad_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (this.ActiveControl.Name == cbCancelar.Name)
			{ //Si quiere salir, le dejamos sin validar
				cbCancelar_Click(cbCancelar, new EventArgs());
				return;
			}
			if (mebREFechaCaducidad.Text.Trim() == "/")
			{
				return;
			}
			if (mebREFechaCaducidad.Text.IndexOf(' ') >= 0)
			{ // si encuentra un espacio en blanco
				string tempRefParam = "R�gimen Econ�mico";
				string tempRefParam2 = "";
				short tempRefParam3 = 1210;
                string[] tempRefParam4 = new string[] { "la fecha final"};
				MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, MFiliacion.GConexion, tempRefParam4);
				mebREFechaCaducidad.Text = "  /    ";
				mebREFechaCaducidad.Focus();
				return;
			}
			if (Convert.ToInt32(Double.Parse(mebREFechaCaducidad.Text.Substring(0, Math.Min(2, mebREFechaCaducidad.Text.Length)))) >= 13 || Convert.ToInt32(Double.Parse(mebREFechaCaducidad.Text.Substring(0, Math.Min(2, mebREFechaCaducidad.Text.Length)))) < 1)
			{ // Si ha puesto un mes superior o igual a trece, o bien menor que 1
				string tempRefParam5 = "R�gimen Econ�mico";
				string tempRefParam6 = "";
				short tempRefParam7 = 1210;
                string[] tempRefParam8 = new string[] { "la fecha final"};
				MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam5, tempRefParam6, tempRefParam7, MFiliacion.GConexion, tempRefParam8);
				mebREFechaCaducidad.Text = "  /    ";
				mebREFechaCaducidad.Focus();
				return;
			}
		}

		//(maplaza)(03/09/2007)
		private void mebREFechaCaducidad_Enter(Object eventSender, EventArgs eventArgs)
		{
			mebREFechaCaducidad.SelectionStart = 0;
			mebREFechaCaducidad.SelectionLength = Strings.Len(mebREFechaCaducidad.Mask);
		}
		
		private void rbTutor_DblClick(object sender, System.EventArgs e)
		{            
            int Index = Array.IndexOf(this.rbTutor, sender);
            rbTutor[Index].IsChecked = false;            
            flagDbClick = true;
        }

        private void rbTutor_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {            
            if (flagDbClick)
            {
                int Index = Array.IndexOf(this.rbTutor, eventSender);
                rbTutor[Index].IsChecked = false;               
                flagDbClick = false;
            }
        }

        private void tbBanco1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbBanco1.SelectionStart = 0;
			tbBanco1.SelectionLength = tbBanco1.Text.Trim().Length;
		}

		private void tbBanco1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8 && KeyAscii != 9)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void TBBandaMagnetica_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			KeyAscii = Strings.Asc(Strings.Chr(KeyAscii).ToString().ToUpper()[0]);

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbControl1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbControl1.SelectionStart = 0;
			tbControl1.SelectionLength = tbControl1.Text.Trim().Length;
		}

		private void tbControl1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8 && KeyAscii != 9)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbControl12_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			tbControl12.SelectionStart = 0;
			tbControl12.SelectionLength = tbControl12.Text.Trim().Length;
		}

		private void tbControl12_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8 && KeyAscii != 9)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbCuenta1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbCuenta1.SelectionStart = 0;
			tbCuenta1.SelectionLength = tbCuenta1.Text.Trim().Length;
		}

		private void tbCuenta1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8 && KeyAscii != 9)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPNDomic_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPOtrosDomic_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPTelf3_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (tbDPTelf3.Text.Trim() != "")
			{
				chbSMS.Enabled = true;
			}
			else
			{
				chbSMS.CheckState = CheckState.Unchecked;
				chbSMS.Enabled = false;
			}
		}

		private void tbDPTelf3_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (tbDPTelf3.Text.Trim() != "")
			{
				chbSMS.Enabled = true;
			}
			else
			{
				chbSMS.CheckState = CheckState.Unchecked;
				chbSMS.Enabled = false;
			}
		}

		private void tbNIFOtrosDocDP_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNIFOtrosDocDP.SelectionStart = 0;
			tbNIFOtrosDocDP.SelectionLength = tbNIFOtrosDocDP.Text.Trim().Length;
		}

		private void mebPCCodPostal_Enter(Object eventSender, EventArgs eventArgs)
		{
			mebPCCodPostal.SelectionStart = 0;
			mebPCCodPostal.SelectionLength = mebPCCodPostal.Text.Trim().Length;
		}

		private void mebPCCodPostal_Leave(Object eventSender, EventArgs eventArgs)
		{
			if (mebPCCodPostal.Text.Trim().Length == 0)
			{
				bcambiarf = true;
			}
			if (!bcambiarf)
			{
				//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
				mebPCCodPostal.Tag = mebPCCodPostal.Text;
				//-----
				return;
			}

			//(maplaza)(16/04/2007)
			if (bFormatoCodigoPos)
			{
				if ((mebPCCodPostal.Text.Trim().Length > 0) && (mebPCCodPostal.Text.Trim().Length < 5))
				{
					mebPCCodPostal.Text = StringsHelper.Format(mebPCCodPostal.Text, "00000");
				}
			}
			//-----
			if (mebPCCodPostal.Text.Trim().Length > 0)
			{
				//stCodigoPostalf = mebPCCodPostal.Text  '(maplaza)(30/05/2006)
				//(maplaza)(18/04/2007)
				//Buscar_poblaciones3
				//si se introduce un C�digo Postal que existe en la tabla DCODIPOS
				if (fExisteCodigoPostal(mebPCCodPostal.Text.Trim()))
				{
					//si no est� introducida la poblaci�n, cargar el combo con las poblaciones relacionadas con el c�digo postal.
					if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
					{
						Buscar_poblaciones3();
					}
				}
				else
				{
					if (bFormatoCodigoPos)
					{
						//(maplaza)(23/04/2007)
						//si no existe el c�digo postal en la tabla DCODIPOS, pero los dos primeros d�gitos del C�digo Postal se
						//corresponden con una provincia (tabla DPROVINC) y no est� seleccionada ninguna poblaci�n, entonces se
						//cargan las poblaciones correspondientes a esta provincia asociada a los dos primeros d�gitos del C�digo Postal.
						if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
						{
							if (fExisteCodigoProvincia(mebPCCodPostal.Text.Trim().Substring(0, Math.Min(2, mebPCCodPostal.Text.Trim().Length))))
							{
								CargarPoblacionesProvincia(mebPCCodPostal.Text.Trim().Substring(0, Math.Min(2, mebPCCodPostal.Text.Trim().Length)), cbbPCPoblacion);
							}
						}
					}
					//-----
				}
				//-----
			}
			//-----
			if (mebPCCodPostal.Text.Trim().Length == 0)
			{
				//stCodigoPostalf = mebPCCodPostal.Text  '(maplaza)(30/05/2006)
				if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
				{
					Cargar_PobDefecto3();
				}
			}

			if (bFormatoCodigoPos)
			{
				//(maplaza)(30/05/2006)
				if (mebPCCodPostal.Text.Trim().Length != 0)
				{
					mebPCCodPostal.Text = StringsHelper.Format(mebPCCodPostal.Text, "00000");
					//stCodigoPostal = mebPCCodPostal.Text   '(maplaza)(30/05/2006)
				}
				//-----
			}

			//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
			mebPCCodPostal.Tag = mebPCCodPostal.Text;
			//-----
		}

		//(maplaza)(05/03/2005)
		private void tbNIFOtrosDocPC_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		//(maplaza)(05/03/2005)
		private void tbNIFOtrosDocPC_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNIFOtrosDocPC.SelectionStart = 0;
			tbNIFOtrosDocPC.SelectionLength = Strings.Len(tbNIFOtrosDocPC.Text);
		}

		private void tbNIFOtrosDocPC_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			bool paso = false;
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = 0;
			}
			//(maplaza)(05/03/2005)si se trata de la opci�n "Otros documentos", no se realiza ninguna validaci�n de formato
			if (rbDNIOtroDocPC[1].IsChecked)
			{
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}

			if (tbNIFOtrosDocPC.Text.Trim().Length == 9)
			{
				tbNIFOtrosDocPC.Focus();
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}

			if ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123))
			{
				if (tbNIFOtrosDocPC.Text.Trim().Length == 0)
				{
					paso = true;
				}
				else
				{
					paso = Conversion.Val(tbNIFOtrosDocPC.Text) == 0;
				}
			}
			int Tama�o = 0;
			int ceros = 0;
			string resto = String.Empty;
			StringBuilder resto2 = new StringBuilder();
			if (!paso)
			{
				if (KeyAscii != 8 && ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123)))
				{
					if (KeyAscii < 48 || KeyAscii > 57)
					{
						if (tbNIFOtrosDocPC.Text.Trim().Length == 9)
						{
							tbNIFOtrosDocPC.Text = tbNIFOtrosDocPC.Text.Substring(0, Math.Min(8, tbNIFOtrosDocPC.Text.Length)) + Strings.Chr(KeyAscii).ToString().ToUpper();
						}
						Tama�o = tbNIFOtrosDocPC.Text.Trim().Length;
						resto = tbNIFOtrosDocPC.Text.Trim();
						ceros = 8 - Tama�o;
						tbNIFOtrosDocPC.Text = "         ";
						for (int i = 1; i <= ceros; i++)
						{
							resto2.Append("0");
						}
						resto2.Append(resto);
						if (resto2.ToString().Trim().Length < 9)
						{
							resto2.Append(Strings.Chr(KeyAscii).ToString().ToUpper());
						}
						tbNIFOtrosDocPC.Text = "         ";
						tbNIFOtrosDocPC.Text = resto2.ToString();
					}
				}
			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNIFOtrosDocPC_Leave(Object eventSender, EventArgs eventArgs)
		{
			//(maplaza)si se trata de la opci�n "Otros documentos", no se realiza ninguna validaci�n de formato
			if (rbDNIOtroDocPC[1].IsChecked)
			{
				return;
			}

			if (tbNIFOtrosDocPC.Text.Trim().Length == 0)
			{
				return;
			}

			if (tbNIFOtrosDocPC.Text.Trim().Length < 9)
			{
				tbNIFOtrosDocPC.Text = StringsHelper.Format(tbNIFOtrosDocPC.Text.Trim(), "00000000") + " ";
			}

			//si se ha quedado un formato incorrecto de DNI o NIF, se tendr� que deshabilitar el bot�n de "Aceptar"
			COMPROBAR_DATOS();

		}

		private void mebRECodPostal_Enter(Object eventSender, EventArgs eventArgs)
		{
			mebRECodPostal.SelectionStart = 0;
			mebRECodPostal.SelectionLength = mebRECodPostal.Text.Trim().Length;
		}

		private void tbNIFOtrosDocRE_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNIFOtrosDocRE.SelectionStart = 0;
			tbNIFOtrosDocRE.SelectionLength = tbNIFOtrosDocRE.Text.Trim().Length;
			miDNINIF = tbNIFOtrosDocRE.Text; //OSCAR C
		}
		
		private void rbAutorizacion_DblClick(object sender, System.EventArgs e)
		{            
            int Index = Array.IndexOf(this.rbAutorizacion, sender);
            rbAutorizacion[Index].IsChecked = false;           
            flagDbClick = true;
        }

        private void rbAutorizacion_CheckStateChanged(Object eventSender, EventArgs eventArgs)
        {
            int Index = Array.IndexOf(this.rbAutorizacion, eventSender);
            if (flagDbClick)
            {
                rbAutorizacion[Index].IsChecked = false;               
                flagDbClick = false;
            }
        }

        private bool isInitializingComponent;
		private void rbIndicadorSS_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.rbIndicadorSS, eventSender);

                if (flagDbClick)
                {
                    //Regimen Ecomomico
                    rbIndicadorSS[Index].IsChecked = false;                    
                    flagDbClick = false;
                }
                else
                {
                    switch (Index)
                    {
                        case 0:
                            tbRENomTitularSS.Text = "";
                            tbRENomTitularSS.Enabled = false;
                            COMPROBAR_DATOS();
                            break;
                        case 1:
                            tbRENomTitularSS.Enabled = true;
                            COMPROBAR_DATOS();
                            break;
                    }
                }
			}
		}
		
		private void rbIndicadorSS_DblClick(object sender, System.EventArgs e)
		{
            flagDbClick = true;
            int Index = Array.IndexOf(this.rbIndicadorSS, sender);
            //Regimen Ecomomico
            switch (Index)
			{
				case 0 :  //Opci�n TITULAR 					
					tbRENomTitularSS.Text = ""; 
					tbRENomTitularSS.Enabled = false; 
					rbIndicadorSS[0].IsChecked = false; 
					rbIndicadorSS[1].IsChecked = false; 
					COMPROBAR_DATOS(); 
					break;
				case 1 :  //Opcion BENEFICIARIO 					
					tbRENomTitularSS.Text = ""; 
					tbRENomTitularSS.Enabled = false; 
					rbIndicadorSS[0].IsChecked = false; 
					rbIndicadorSS[1].IsChecked = false; 
					COMPROBAR_DATOS(); 
					break;
			}
		}

		private void rbSexo_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				comprobar_gidenpac();
			}
		}
		
		private void sdcFechaNac_Closing(Object eventSender, Telerik.WinControls.UI.RadPopupClosingEventArgs eventArgs)
        {
			COMPROBAR_DATOS();
		}

		private void sdcFechaNac_Enter(Object eventSender, EventArgs eventArgs)
		{
			comprobar_gidenpac();
		}

		private void sdcFechaNac_Leave(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
			comprobar_gidenpac();
		}

		private void tbApellido1_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
			comprobar_gidenpac();
		}

		private void tbApellido1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbApellido2_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			comprobar_gidenpac();
		}

		private void tbApellido2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbBanco_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbBanco.SelectionStart = 0;
			tbBanco.SelectionLength = tbBanco.Text.Trim().Length;
		}

		private void tbBanco_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8 && KeyAscii != 9)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbControl_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbControl.SelectionStart = 0;
			tbControl.SelectionLength = tbControl.Text.Trim().Length;
		}

		private void tbControl_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8 && KeyAscii != 9)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbCuenta_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbCuenta.SelectionStart = 0;
			tbCuenta.SelectionLength = tbCuenta.Text.Trim().Length;
		}

		private void tbCuenta_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8 && KeyAscii != 9)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPDomicilio_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPDomicilio.SelectionStart = 0;
			tbDPDomicilio.SelectionLength = tbDPDomicilio.Text.Trim().Length;
		}

		private void tbDPDomicilio_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPDomicilioP_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		//(maplaza)(06/08/2007)
		private void tbNumTrabajador_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNumTrabajador.SelectionStart = 0;
			tbNumTrabajador.SelectionLength = tbNumTrabajador.Text.Trim().Length;
		}

		//OSCAR ENE 2005
		private void tbObservaciones_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		//--------

		private void tbDPExPob_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPExPob.SelectionStart = 0;
			tbDPExPob.SelectionLength = tbDPExPob.Text.Trim().Length;
		}

		//(maplaza)(09/08/2007)
		private void tbDPDireccionEx_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPDireccionEx.SelectionStart = 0;
			tbDPDireccionEx.SelectionLength = tbDPDireccionEx.Text.Trim().Length;
		}

		private void tbDPExPob_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			Serrores.ValidarTecla(ref KeyAscii);
			COMPROBAR_DATOS();
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

        private void tbDPExtension1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPExtension1.SelectionStart = 0;
			tbDPExtension1.SelectionLength = tbDPExtension1.Text.Trim().Length;
		}

		private void tbDPExtension1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPExtension2_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPExtension2.SelectionStart = 0;
			tbDPExtension2.SelectionLength = tbDPExtension2.Text.Trim().Length;
		}

		private void tbDPExtension2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPExtension3_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPExtension3.SelectionStart = 0;
			tbDPExtension3.SelectionLength = tbDPExtension3.Text.Trim().Length;
		}

		private void tbDPExtension3_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPTelf1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPTelf1.SelectionStart = 0;
			tbDPTelf1.SelectionLength = tbDPTelf1.Text.Trim().Length;
		}

		private void tbDPTelf1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPPasaporte_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPPasaporte.SelectionStart = 0;
			tbDPPasaporte.SelectionLength = tbDPPasaporte.Text.Trim().Length;
		}

		private void tbDPPasaporte_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPTelf2_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPTelf2.SelectionStart = 0;
			tbDPTelf2.SelectionLength = tbDPTelf2.Text.Trim().Length;
		}

		private void tbDPTelf2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbDPTelf3_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbDPTelf3.SelectionStart = 0;
			tbDPTelf3.SelectionLength = tbDPTelf3.Text.Trim().Length;
		}

		private void tbDPTelf3_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (tbDPTelf3.Text.Trim().Length < 2)
					{
						chbSMS.CheckState = CheckState.Unchecked;
						chbSMS.Enabled = false;
					}

					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}

			if (KeyAscii > 0)
			{
				chbSMS.Enabled = true;
			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNombre_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCApe1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbPCApe1.SelectionStart = 0;
			tbPCApe1.SelectionLength = tbPCApe1.Text.Trim().Length;
		}

		private void tbPCApe1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCApe2_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbPCApe2.SelectionStart = 0;
			tbPCApe2.SelectionLength = tbPCApe2.Text.Trim().Length;
		}

		private void tbPCApe2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCDomicilio_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbPCDomicilio.SelectionStart = 0;
			tbPCDomicilio.SelectionLength = tbPCDomicilio.Text.Trim().Length;
		}

		private void tbPCDomicilio_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCExPob_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCExt1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbPCExt1.SelectionStart = 0;
			tbPCExt1.SelectionLength = tbPCExt1.Text.Trim().Length;
		}

		private void tbPCExt1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCExt2_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbPCExt2.SelectionStart = 0;
			tbPCExt2.SelectionLength = tbPCExt2.Text.Trim().Length;
		}

		private void tbPCExt2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCNombre_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbPCNombre.SelectionStart = 0;
			tbPCNombre.SelectionLength = tbPCNombre.Text.Trim().Length;
		}

		private void tbPCNombre_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCParentesco_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbPCParentesco.SelectionStart = 0;
			tbPCParentesco.SelectionLength = tbPCParentesco.Text.Trim().Length;
			if (tbPCParentesco.Text.Trim() == "")
			{
				tbPCParentesco.Text = "";
			}
		}

		private void tbPCParentesco_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCTelf1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbPCTelf1.SelectionStart = 0;
			tbPCTelf1.SelectionLength = tbPCTelf1.Text.Trim().Length;
		}

		private void tbPCTelf1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbPCTelf2_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbPCTelf2.SelectionStart = 0;
			tbPCTelf2.SelectionLength = tbPCTelf2.Text.Trim().Length;
		}

		private void tbPCTelf2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNombre_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
			comprobar_gidenpac();
		}

		private void tbREApe1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbREApe1.SelectionStart = 0;
			tbREApe1.SelectionLength = tbREApe1.Text.Trim().Length;
		}

		private void tbREApe1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbREApe2_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbREApe2.SelectionStart = 0;
			tbREApe2.SelectionLength = tbREApe2.Text.Trim().Length;
		}

		private void tbREApe2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbREDomicilio_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbREDomicilio.SelectionStart = 0;
			tbREDomicilio.SelectionLength = tbREDomicilio.Text.Trim().Length;
		}

		private void tbREDomicilio_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbREEmpresa_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbRENomTitularSS_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		//oscar 30/09/03
		private void tbRENPoliza_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (!Serrores.Valida_Tecla_Pulsada(KeyAscii, "POLIZA"))
			{
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbRENSS_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (!Serrores.Valida_Tecla_Pulsada(KeyAscii, "NSS"))
			{
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		
		private void tbREPasaporte_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbREPasaporte.SelectionStart = 0;
			tbREPasaporte.SelectionLength = tbREPasaporte.Text.Trim().Length;
		}

		private void tbREPasaporte_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbREEmpresa_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void tbREEmpresa_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbREEmpresa.SelectionStart = 0;
			tbREEmpresa.SelectionLength = tbREEmpresa.Text.Trim().Length;
		}

		private void tbREExPob_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void cbbREInspeccion_Change(Object eventSender, Telerik.WinControls.UI.Data.PositionChangedEventArgs eventArgs)
		{
			if (Convert.ToDouble(cbbREInspeccion.get_ListIndex()) != -1 && cbbREInspeccion.Text != "")
			{
				if (TitularBeneficiario.Trim().ToUpper() == "B")
				{
					rbIndicadorSS[0].IsChecked = false;
					rbIndicadorSS[1].IsChecked = true;
				}
				else
				{
					rbIndicadorSS[0].IsChecked = true;
					rbIndicadorSS[1].IsChecked = false;
				}
			}
			COMPROBAR_DATOS();
		}

		private void cbbREInspeccion_Click(Object eventSender, EventArgs eventArgs)
		{
			if (Convert.ToDouble(cbbREInspeccion.get_ListIndex()) != -1 && cbbREInspeccion.Text != "")
			{
				if (TitularBeneficiario.Trim().ToUpper() == "B")
				{
					rbIndicadorSS[0].IsChecked = false;
					rbIndicadorSS[1].IsChecked = true;
				}
				else
				{
					rbIndicadorSS[0].IsChecked = true;
					rbIndicadorSS[1].IsChecked = false;
				}
				//''   rbIndicadorSS(0).Value = True
			}
			COMPROBAR_DATOS();
		}

		private void cbbREPais_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void cbbREPoblacion_Click(Object eventSender, EventArgs eventArgs)
		{
			string mensaje = String.Empty;
			string sCodigoPostal = String.Empty;

			if (chbREExtrangero.CheckState != CheckState.Checked)
			{

				if (!bFormatoCodigoPos)
				{
					sCodigoPostal = mebRECodPostal.Text.Trim();
					mebRECodPostal.Text = "";
				}

				//**If cbbDPPoblacion.ListIndex > -1 Then  (maplaza)
				//busca el codigo postal
				if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) > -1)
				{
					object tempRefParam = 1;
					object tempRefParam2 = cbbREPoblacion.get_ListIndex();
                    object tempRefParam3 = 1;
                    object tempRefParam4 = cbbREPoblacion.get_ListIndex();
                    Codigoprov2 = StringsHelper.Format(Convert.ToString(cbbREPoblacion.get_Column(tempRefParam, tempRefParam2)).Substring(0, Math.Min(2, Convert.ToString(cbbREPoblacion.get_Column(tempRefParam, tempRefParam2)).Length)), "00");
					SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT gcodipos FROM DCODIPOS WHERE GPOBLACI = '" + Convert.ToString(cbbREPoblacion.get_Column(tempRefParam3, tempRefParam4)) + "'", MFiliacion.GConexion);
					RrCodPostal = new DataSet();
					tempAdapter.Fill(RrCodPostal);
					
					if (RrCodPostal.Tables[0].Rows.Count != 0)
					{
						if (RrCodPostal.Tables[0].Rows.Count == 1)
						{
							//(maplaza)(18/04/2007)
							if (mebRECodPostal.Text.Trim() == "")
							{
								//-----
								if (bFormatoCodigoPos)
								{
									bNocambiarCP_RE = true; //(maplaza)(31/05/2006)para que no salte el evento "Change"
									mebRECodPostal.Text = StringsHelper.Format(RrCodPostal.Tables[0].Rows[0]["gcodipos"], "00000");
									bNocambiarCP_RE = false;
								}
								else
								{
									bNocambiarCP_RE = true; //(maplaza)(31/05/2006)para que no salte el evento "Change"
									mebRECodPostal.Text = (Convert.ToString(RrCodPostal.Tables[0].Rows[0]["gcodipos"]) + "").Trim();
									bNocambiarCP_RE = false;
								}
								//-------
							}
						}
					}
					RrCodPostal.Close();
				}

				if (!bFormatoCodigoPos && sCodigoPostal.Trim() != "" && mebRECodPostal.Text.Trim() == "")
				{
					mebRECodPostal.Text = sCodigoPostal.Trim();
				}

			}
		}
		
		private void cbbRESociedad_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			COMPROBAR_DATOS();
		}

		private void cbbRESociedad_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
			if (cbbRESociedad.SelectedIndex != -1 && cbbRESociedad.Text.Trim() != "")
			{
				//rbIndicador(0).Value = True

				// Tomamos la m�scara asociada a la Sociedad

				bCambiandoMascara = true;
				tbRENPoliza.Mask = proObtenerMascara(int.Parse(cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString()));
				bCambiandoMascara = false;
				string tempRefParam = tbRENPoliza.Mask;
				ToolTipMain.SetToolTip(tbRENPoliza, MascaraParaMostrar(ref tempRefParam));
                tbRENPoliza.MaskedEditBoxElement.ToolTipText = MascaraParaMostrar(ref tempRefParam);

                if (tbRENPoliza.Mask == "")
				{
					tbRENPoliza.MaskedEditBoxElement.TextBoxItem.MaxLength = 20;
				}
				else
				{
					tbRENPoliza.MaskedEditBoxElement.TextBoxItem.MaxLength = Strings.Len(tbRENPoliza.Mask);
				}

				if (MFiliacion.stFiliacion == "MODIFICACION")
				{
					SQLSOC = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' AND GSOCIEDA = " + cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString() + "";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLSOC, MFiliacion.GConexion);
					RrSQLSOC = new DataSet();
					tempAdapter.Fill(RrSQLSOC);
					if (RrSQLSOC.Tables[0].Rows.Count != 0)
					{
						CARGAR_SOCIEDAD();
					}
					else
					{
						tbRENPoliza.Text = MascaraVacia();
						chkExentoIVA.CheckState = CheckState.Unchecked;
						tbRENVersion.Text = "";
						//(maplaza)(19/09/2007)
						tbRECobertura.Text = "";
						mebREFechaAlta.Text = "  /    ";
						mebREFechaCaducidad.Text = "  /    ";
						TBBandaMagnetica.Text = "";
						//-----
						this.tbREEmpresa.Text = "";
						rbIndicador[0].IsChecked = true;

					}
				}
				else
				{
					rbIndicador[0].IsChecked = true;
				}
			}
			COMPROBAR_DATOS();
		}

		private void cbbRESociedad_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			//KeyAscii = 0
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void cbBuscar2_Click(Object eventSender, EventArgs eventArgs)
		{
			string codProvincia = String.Empty;

			Poblaciones.MCPoblaciones oPoblaciones = new Poblaciones.MCPoblaciones();
			object tempRefParam = this;
			object tempRefParam2 = cbbREPoblacion;
			oPoblaciones.llamada(this, MFiliacion.GConexion, ref cbbREPoblacion);
			oPoblaciones = null;

			//Jesus 27/07/2011 No se hace para Argentina
			if (bFormatoCodigoPos)
			{
				//(maplaza)(30/05/2006)Al volver de la dll, se tiene que comprobar si la poblaci�n escogida(si es que se ha
				//escogido alguna), concuerda con el C�digo Postal escrito
				if ((cbbREPoblacion.Text != "") && (mebRECodPostal.Text != "     "))
				{
					if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) != -1)
					{
						object tempRefParam3 = 1;
						object tempRefParam4 = cbbREPoblacion.get_ListIndex();
						codProvincia = StringsHelper.Format(Convert.ToString(cbbREPoblacion.get_Column(tempRefParam3, tempRefParam4)).Substring(0, Math.Min(2, Convert.ToString(cbbREPoblacion.get_Column(tempRefParam3, tempRefParam4)).Length)), "00");
						if (!mebRECodPostal.Text.Trim().StartsWith(codProvincia))
						{
							bNocambiarCP_RE = true; //(maplaza)(31/05/2006)para que no salte el evento "Change"
							mebRECodPostal.Text = "     ";
							bNocambiarCP_RE = false;
							cbbREPoblacion_Click(eventSender,eventArgs); //para que encuentre el c�digo postal correcto (si es que existe uno s�lo)
						}
					}
				}
				else if ((cbbREPoblacion.Text != ""))
				{ 
					if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) != -1)
					{
						cbbREPoblacion_Click(eventSender, eventArgs);
					}
				}
				//-----
			}
			else
			{
				if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) != -1)
				{
					cbbREPoblacion_Click(eventSender, eventArgs);
				}
			}

		}

		private void cbBuscar3_Click(Object eventSender, EventArgs eventArgs)
		{
			string codProvincia = String.Empty;

			Poblaciones.MCPoblaciones oPoblaciones = new Poblaciones.MCPoblaciones();
			oPoblaciones.llamada(this, MFiliacion.GConexion, ref cbbPCPoblacion);			
			oPoblaciones = null;

			//Jesus 27/07/2011 No se hace para Argentina
			if (bFormatoCodigoPos)
			{
				//(maplaza)(30/05/2006)Al volver de la dll, se tiene que comprobar si la poblaci�n escogida(si es que se ha
				//escogido alguna), concuerda con el C�digo Postal escrito
				if ((cbbPCPoblacion.Text != "") && (mebPCCodPostal.Text != "     "))
				{
					if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) != -1)
					{
						object tempRefParam3 = 1;
						object tempRefParam4 = cbbPCPoblacion.get_ListIndex();
						codProvincia = StringsHelper.Format(Convert.ToString(cbbPCPoblacion.get_Column(tempRefParam3, tempRefParam4)).Substring(0, Math.Min(2, Convert.ToString(cbbPCPoblacion.get_Column(tempRefParam3, tempRefParam4)).Length)), "00");
						if (!mebPCCodPostal.Text.Trim().StartsWith(codProvincia))
						{
							mebPCCodPostal.Text = "     ";
							//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
							mebPCCodPostal.Tag = mebPCCodPostal.Text;
							//-----
							cbbPCPoblacion_Click(eventSender, eventArgs); //para que encuentre el c�digo postal correcto (si es que existe uno s�lo)
						}
						else
						{
							//si se corresponde con la misma provincia
							cbbPCPoblacion_Click(eventSender, eventArgs); //para que encuentre el c�digo postal correcto (si es que existe uno s�lo)
						}
					}
				}
				else if ((cbbPCPoblacion.Text != ""))
				{ 
					if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) != -1)
					{
						cbbPCPoblacion_Click(eventSender, eventArgs);
					}
				}
				//-----
			}
			else
			{
				if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) != -1)
				{
					cbbPCPoblacion_Click(eventSender, eventArgs);
				}
			}

		}

		private void cbBuscarCIAS_Click(Object eventSender, EventArgs eventArgs)
		{
			stCIAS = MEBCIAS.Text.Trim();            
            DFI111F3 tempLoadForm = DFI111F3.DefInstance;			
			DFI111F3.DefInstance.ShowDialog();
			if (stCIAS != MEBCIAS.Text.Trim())
			{
				Restringir_Inspeccion(true);
			}
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			MFiliacion.RegresodeOpenlab = "Cancelar";
			//(maplaza)(07/09/2007)
			ComprobarSociedad();
			//------
			this.Close();
		}

		//(maplaza)(07/09/2007)Si se ha pulsado el bot�n "Cancelar" (independientemente del r�gimen econ�mico elegido), se comprueba
		//que la sociedad elegida por el paciente tiene Fecha de Borrado NULA. Si no es as�, entonces se pone NULA. Este
		//caso puede producirse debido a la interacci�n de la pantalla de Filiaci�n con la pantalla de "Consulta/Actualizaci�n
		//de Entidades".
		private void ComprobarSociedad()
		{
			string strSql = String.Empty;
			DataSet RrTemporal = null;

			try
			{
				this.Cursor = Cursors.WaitCursor;

				if (MFiliacion.RrDPACIENT!=null && MFiliacion.RrDPACIENT.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]))
					{
						if (MFiliacion.ModCodigo != "")
						{
							strSql = "SELECT fborrado FROM DENTIPAC WHERE gidenpac='" + MFiliacion.ModCodigo + "'";
							strSql = strSql + " AND gsocieda=" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]);
							SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
							RrTemporal = new DataSet();
							tempAdapter.Fill(RrTemporal);
							if (RrTemporal.Tables[0].Rows.Count != 0)
							{
								if (!Convert.IsDBNull(RrTemporal.Tables[0].Rows[0]["fborrado"]))
								{
									RrTemporal.Edit();
									RrTemporal.Tables[0].Rows[0]["fborrado"] = DBNull.Value;
									string tempQuery = RrTemporal.Tables[0].TableName;
									//SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
									SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
									tempAdapter.Update(RrTemporal, RrTemporal.Tables[0].TableName);
								}
							}
							RrTemporal.Close();
						}
					}
				}

				this.Cursor = Cursors.Default;
			}
			catch(SqlException ex)
			{
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(ex, MFiliacion.GConexion);
			}

		}

		private void cbDBuscar_Click(Object eventSender, EventArgs eventArgs)
		{
			string codProvincia = String.Empty;

			Poblaciones.MCPoblaciones oPoblaciones = new Poblaciones.MCPoblaciones();
			object tempRefParam = this;
			object tempRefParam2 = cbbDPPoblacion;
			oPoblaciones.llamada(this, MFiliacion.GConexion, ref cbbDPPoblacion);			
			oPoblaciones = null;

			//Jesus 27/07/2011 No se hace para Argentina
			if (bFormatoCodigoPos)
			{
				//(maplaza)(30/05/2006)Al volver de la dll, se tiene que comprobar si la poblaci�n escogida(si es que se ha
				//escogido alguna), concuerda con el C�digo Postal escrito
				if ((cbbDPPoblacion.Text != "") && (mebDPCodPostal.Text != "     "))
				{
					if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) != -1)
					{
						object tempRefParam3 = 1;
						object tempRefParam4 = cbbDPPoblacion.get_ListIndex();
						codProvincia = StringsHelper.Format(Convert.ToString(cbbDPPoblacion.get_Column(tempRefParam3, tempRefParam4)).Substring(0, Math.Min(2, Convert.ToString(cbbDPPoblacion.get_Column(tempRefParam3, tempRefParam4)).Length)), "00");
						if (!mebDPCodPostal.Text.Trim().StartsWith(codProvincia))
						{
							mebDPCodPostal.Text = "     ";
							//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
							mebDPCodPostal.Tag = mebDPCodPostal.Text;
							//-----
							cbbDPPoblacion_Click(eventSender, eventArgs); //para que encuentre el c�digo postal correcto (si es que existe uno s�lo)
						}
						else
						{
							//si se corresponde con la misma provincia
							cbbDPPoblacion_Click(eventSender, eventArgs); //para que encuentre el c�digo postal correcto (si es que existe uno s�lo)
						}
					}
				}
				else if ((cbbDPPoblacion.Text != ""))
				{ 
					if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) != -1)
					{
						cbbDPPoblacion_Click(eventSender, eventArgs);
					}
				}
				//-----
			}
			else
			{
				if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) != -1)
				{
					cbbDPPoblacion_Click(eventSender, eventArgs);
				}
			}
		}

		private void chbDPExitus_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			//SI SE SELECCIONA EL CHECKBOX DE EXITUS SE HABILITA LA FECHA
			//DE FALLECIMIENTO Y SE OBLIGA A INTRODUCIR

			if (chbDPExitus.CheckState == CheckState.Checked)
			{
				sdcDPFechaFallecimiento.Enabled = true;
				sdcDPFechaFallecimiento.Text = DateTime.Now.ToString("dd/MM/yyyy");
				sdcDPFechaFallecimiento.Value = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				COMPROBAR_DATOS();
			}
			if (chbDPExitus.CheckState == CheckState.Unchecked)
			{
                //sdcDPFechaFallecimiento.Value = DateTime.FromOADate(0);
                sdcDPFechaFallecimiento.NullableValue = null;
                sdcDPFechaFallecimiento.SetToNullValue();

                sdcDPFechaFallecimiento.Enabled = false;
				COMPROBAR_DATOS();
			}
		}

		private void chbDPExtrangero_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (chbDPExtrangero.CheckState == CheckState.Checked)
			{ //activado
				//desactiva la poblacion, el cod poblacion y el codigo postal
				cbbDPPais.Enabled = true;
				tbDPExPob.Enabled = true;
				tbDPDireccionEx.Enabled = true;
				Label41.Enabled = true;
				Label42.Enabled = true;
				Label69.Enabled = true;
			}
			else
			{
				cbbDPPais.Enabled = false;
				cbbDPPais.set_ListIndex(-1);
				tbDPExPob.Enabled = false;
				tbDPExPob.Text = "";
				tbDPDireccionEx.Enabled = false;
				tbDPDireccionEx.Text = "";
				Label41.Enabled = false;
				Label42.Enabled = false;
				Label69.Enabled = false;

			}
			COMPROBAR_DATOS();
		}

		private void chbPCExtrangero_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (chbPCExtrangero.CheckState == CheckState.Checked)
			{ //CHEQUEADO
				//HABILITO LAS CAJAS DE EXTRANJERO Y DESHABILITO LAS DE NACIONAL
				cbbPCPais.Enabled = true;
				tbPCExPob.Enabled = true;
				Label21.Enabled = true;
				Label45.Enabled = true;

			}
			else
			{
				//AL REVES
				cbbPCPais.set_ListIndex(-1);
				cbbPCPais.Enabled = false;
				tbPCExPob.Text = "";
				tbPCExPob.Enabled = false;
				Label45.Enabled = false;
				Label21.Enabled = false;
			}
			COMPROBAR_DATOS();
		}

		private void chbREExtrangero_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (chbREExtrangero.CheckState == CheckState.Checked)
			{ //CHEQUEADO
				cbbREPais.Enabled = true;
				tbREExPob.Enabled = true;
				tbREDireccionEx.Enabled = true;
			}
			else
			{
				//AL REVES
				cbbREPais.set_ListIndex(-1);
				cbbREPais.Enabled = false;
				tbREExPob.Text = "";
				tbREExPob.Enabled = false;
				tbREDireccionEx.Text = "";
				tbREDireccionEx.Enabled = false;
			}

			COMPROBAR_DATOS();
		}

		private void DFI111F1_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

				string imensaje = String.Empty;
				if (MFiliacion.stFiliacion == "MODIFICACION")
				{
					if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["IEXITUSP"]) == "S")
					{
						string tempRefParam = "";
						short tempRefParam2 = 1590;
						string[] tempRefParam3 = new string[] { };
						imensaje = Convert.ToString(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, tempRefParam, tempRefParam2, MFiliacion.GConexion, tempRefParam3));
						if (imensaje == ((int) System.Windows.Forms.DialogResult.No).ToString())
						{
							MFiliacion.CodPac = false;
							this.Close();
							return;
						}
					}
					VApellido1 = tbApellido1.Text;
					VApellido2 = tbApellido2.Text;
					Vfecha_nacimiento = sdcFechaNac.Value.ToString("dd/MM/yyyy");
					if (rbSexo[0].IsChecked)
					{
						VSexo_persona = "0";
					}
					else
					{
						if (rbSexo[1].IsChecked)
						{
							VSexo_persona = "1";
						}
						else
						{
							if (!rbSexo[0].IsChecked && !rbSexo[1].IsChecked)
							{
								VSexo_persona = "2";
							}
						}
					}
					//(maplaza)(07/08/2007)Para el caso de que se trate de una modificaci�n, se muestra la Fecha de �ltima Modificaci�n.
					lblTituloFechaUltMod.Visible = true;
					lblFechaUltMod.Visible = true;
					//-----
					//cambiar_identificador = False

				}

				COMPROBAR_DATOS();


				//
				switch(intEstado)
				{
					case 0 :  //Sin deuda 
						break;
					case 1 :  //Con deuda inferior al limite permitido 
						string tempRefParam4 = ""; 
						short tempRefParam5 = 3921;
                        string[] tempRefParam6 = new string[] { }; 
						MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, tempRefParam4, tempRefParam5, MFiliacion.GConexion, tempRefParam6); 
						break;
					case 2 :  //Con deuda superior al limite permitido 
						rbBloqueo[0].Enabled = true; 
						string tempRefParam7 = ""; 
						short tempRefParam8 = 3922;
                        string[] tempRefParam9 = new string[] { }; 
						MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, tempRefParam7, tempRefParam8, MFiliacion.GConexion, tempRefParam9); 
						break;
				}
				//End If
				//'-------

				//OSCAR C 02/09/2005
				//Cuando se accede desde el modulo de OTROSPROFESIONALES
				//no se debe permitir cambiar la entidad del paciente.
				if (MFiliacion.stModulo == "OTROSPROFESIONALES")
				{
					frmRERegimen.Enabled = false;
					frmTitular.Enabled = false;
					frmPrivado.Enabled = false;
					cmdDeudas.Enabled = false;
					cmdFacturacionP.Enabled = false;
					cmdConsultaActualizacion.Enabled = false;
				}
				//------------------


				ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
				if (MFiliacion.blnIMDHOPEN)
				{
					//O.Frias - 24/02/2009
					//Se activa la opci�n de tecla de funcion
					IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();

					TECLAFUNCION = Convert.ToInt32(IMDHOPEN.TeclaFuncionOpen(MFiliacion.GConexion));
					MFiliacion.FormularioAnterior = Convert.ToString(IMDHOPEN.LeerUltimoForm(MFiliacion.sUsuario));
					IMDHOPEN.EstableceUltimoForm(this.Name, MFiliacion.sUsuario);

					cmdOpen.Visible = Convert.ToBoolean(IMDHOPEN.TeclaFuncionBotonVisible(MFiliacion.GConexion));
					if (cmdOpen.Visible)
					{
						cmdOpen.Enabled = MFiliacion.ModCodigo.Trim() != "";
					}
					IMDHOPEN = null;
					TECLAFUNCION = Convert.ToInt32(Double.Parse(mbAcceso.ObtenerCodigoSConsglo("IMDHOPEN", "nnumeri1", MFiliacion.GConexion)));
				}

			}
		}

		//*******************************************************************************************
		//* O.Frias (14/12/2006) - Inicializo la bariable de control de dupliacdos a false.  *
		//* O.Frias (31/07/2007) - Incorporo la operativa para la solicitud de usuario para cita WEB
		//*******************************************************************************************
		private void DFI111F1_Load(Object eventSender, EventArgs eventArgs)
		{
            (tsDatosPac.ViewElement as Telerik.WinControls.UI.RadPageViewStripElement).ShowItemCloseButton = false;
            (tsDatosPac.ViewElement as Telerik.WinControls.UI.RadPageViewStripElement).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;

            string stSqlTemp2 = String.Empty;
			DataSet rrTemp2 = null;

			CipAutonomico = false;
			CiasProfesional = false;
			Colorear_Cajas(); //Oscar C Mayo 2010

			//Oscar C Mayo 2010
			bPacienteIdBas = false;
			bControlIDBas = false;
			//Motivo de Filiacion cuando el paciente no cumple con el criterio de identificacion basica
			bMotivoFiliacion = false;
			iMotivoFiliacion = -1;
			oMotivoFiliacion = "";
			//-------------------------------------


			//Control de la execci�n del iva
			string stsqltemp = "select count(*) from sconsglo where gconsglo= 'ARCONIVA' and valfanu1='S' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
			DataSet RrTemp = new DataSet();
			tempAdapter.Fill(RrTemp);
			chkExentoIVA.Visible = (Convert.ToDouble(RrTemp.Tables[0].Rows[0][0]) == 1);
			RrTemp.Close();

			//Control de los d�gitos de la cuenta bancaria. Si es 'S' son 21, en cualquier otro caso, como antes.
			stsqltemp = "select count(*) from sconsglo where gconsglo= 'CBARGENT' and valfanu1='S' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
			RrTemp = new DataSet();
			tempAdapter_2.Fill(RrTemp);
			//Si est� la constante y vale 'S', activamos el marco de Argentina.
			FrmCB21.Visible = (Convert.ToDouble(RrTemp.Tables[0].Rows[0][0]) == 1);
			if ((Convert.ToDouble(RrTemp.Tables[0].Rows[0][0]) == 1))
			{
				FrmCB21.Top = (int) Frame9.Top;
			}
			//En cualquier otro caso, lo dejamos como antes.
			Frame9.Visible = Convert.ToDouble(RrTemp.Tables[0].Rows[0][0]) != 1;
			RrTemp.Close();

			//Control de la diferenciaci�n de los clientes asistenciales y econ�mico. Si es 'S' aparecer� un nuevo combo.
			stsqltemp = "select count(*) from sconsglo where gconsglo= 'DIFEECAS' and valfanu1='S' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
			RrTemp = new DataSet();
			tempAdapter_3.Fill(RrTemp);
			//Si est� la constante y vale 'S', no cambiamos nada. En otro caso, recolocamos los controles.

			//aprovechamos el tag para saber si est�n activos los dos combos
			lblObraSocial.Tag = "";
			if (Convert.ToDouble(RrTemp.Tables[0].Rows[0][0]) == 1)
			{
				lblObraSocial.Tag = "visible";
				lblObraSocial.Text = "Obra soc:";
				Label12.Text = "Plan:";
				mebDPCodPostal.Mask = "";
				mebRECodPostal.Mask = "";
				mebPCCodPostal.Mask = "";
				bFormatoCodigoPos = false;
			}
			else
			{
				Label12.Top = 19;
				cbbRESociedad.Top = 15;
				Label14.Top = 45;
				tbRENPoliza.Top = 42;
				LbVersion.Top = 45;
				tbRENVersion.Top = 42;
				chkExentoIVA.Top = 42;
				Label38.Top = 71;
				tbREEmpresa.Top = 68;
				cbBuscarSoc.Top = 66;
				Label71.Top = 96;
				tbRECobertura.Top = 93;
				Label72.Top = 96;
				mebREFechaAlta.Top = 93;
				Label73.Top = 96;
				mebREFechaCaducidad.Top = 93;
				lblObraSocial.Visible = false;
				CbbObraSocial.Visible = false;
				mebDPCodPostal.Mask = "#####";
				mebRECodPostal.Mask = "#####";
				mebPCCodPostal.Mask = "#####";
				bFormatoCodigoPos = true;
			}
			RrTemp.Close();

			// Establecemos los valores l�mite de los campos de Grado de Dependencia

			for (i = 0; i <= 5; i++)
			{

				// Como m�nimo �la fecha de nacimiento?
				//sdcGradoDep(i).MinDate = Format(fnacipac, "DD/MM/YYYY")
				// Como m�ximo la fecha de hoy

				sdcGradoDep[i].MaxDate = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				if (MFiliacion.stFiliacion == "ALTA")
				{                    
                    sdcGradoDep[i].NullableValue = null;
                    sdcGradoDep[i].SetToNullValue();
                }
            }

			stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'CIPAUTMA' ";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
			RrTemp = new DataSet();
			tempAdapter_4.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					CipAutonomico = true;
					lbCipAutonomico.Enabled = true;
					if (MFiliacion.GCipAuto != "")
					{
						lbCipAutonomico.Text = MFiliacion.GCipAuto;
					}
				}
				else
				{
					lbCipAutonomico.Enabled = false;
					lbCipAutonomico.BackColor = SystemColors.Control;
				}
			}
			RrTemp.Close();

			stsqltemp = "select valfanu1,nnumeri1 from sconsglo where gconsglo= 'CIAPROMA' ";
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
			RrTemp = new DataSet();
			tempAdapter_5.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					CiasProfesional = true;
					if (!Convert.IsDBNull(RrTemp.Tables[0].Rows[0]["nnumeri1"]))
					{
						stSqlTemp2 = "Select gcensalu from dcensalu where gcensalu = " + Convert.ToString(RrTemp.Tables[0].Rows[0]["nnumeri1"]);
						SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(stSqlTemp2, MFiliacion.GConexion);
						rrTemp2 = new DataSet();
						tempAdapter_6.Fill(rrTemp2);
						if (rrTemp2.Tables[0].Rows.Count != 0)
						{
							NumCiasProfesional = Convert.ToString(RrTemp.Tables[0].Rows[0]["nnumeri1"]);
						}
						else
						{
							NumCiasProfesional = "";
						}
						rrTemp2.Close();
					}
					else
					{
						NumCiasProfesional = "";
					}
				}
			}
			RrTemp.Close();


			//******************** O.Frias (14/12/2006) ********************
			blnDuplicados = false;
			blnRequeridos = false;
			//******************** O.Frias (14/12/2006) ********************

			CargarLoad();
			stComienzoBanda = "^B"; // Valor por defecto


			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter("Select isNull(valfanu1, '^B') valfanu1, isNull(nnumeri1, 2) nnumeri1 from sConsGlo where gConsGlo = 'TIPOTARJ'", MFiliacion.GConexion);
			DataSet RrBanda = new DataSet();
			tempAdapter_7.Fill(RrBanda);
			if (RrBanda.Tables[0].Rows.Count != 0)
			{
				stComienzoBanda = Convert.ToString(RrBanda.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
				iNumeroBandas = Convert.ToInt32(RrBanda.Tables[0].Rows[0]["nnumeri1"]);
			}
			RrBanda.Close();

			//OSCAR C FEB 2006
			//Cuando se registrar un paciente en el SIA procedente de RULEQ,
			//se cargan los datos del paciente procedentes de RULEQ....
			if (MFiliacion.bProcedentedeRULEQ)
			{
				proRellenaDatosRULEQ();
			}
			//----------------


			//************* O.Frias - 31/07/2007 *************
			if (MFiliacion.blnUsuWeb)
			{
				proRellenaDatosUsuWeb();
			}


			if (!mbAcceso.AplicarControlAcceso(MFiliacion.sUsuario, MFiliacion.LETRA_MODULO, this, this.Name, ref MFiliacion.astrControles, ref MFiliacion.astrEventos))
			{
				this.Close();
			}

			// Si estamos modificando un paciente cancelado, no podremos cambiar ning�n dato.
			if (proEstaCancelado())
			{
				cbAceptar.Visible = false;
				cbPCAceptar.Visible = false;
				lbDesactivado.Visible = true;
			}


			//O.Frias - 24/02/2009
			//Displaya el new boton IMDHOPEN.
			//Set RrBanda = GConexion.OpenResultset("SELECT ISNULL(valfanu1, 'N' ) as valfanu1  FROM SCONSGLO WHERE gconsglo= 'IMDHOPEN'")
			//If Not RrBanda.EOF Then
			//    blnIMDHOPEN = (Trim(RrBanda("valfanu1")) = "S")
			//End If
			//RrBanda.Close
			cmdOpen.Enabled = false;
			cmdOpen.Visible = MFiliacion.blnIMDHOPEN;

			//Oscar C Mayo 2010
			//En la modificacion de los datos de filiacion del paciente, cuando el indicador "Revision de Regimen Economico en filiacion"
			//del usuario conectado (SUSUARIO.irevrege)="S", los radio button correspondientes al regimen economico,
			//no apareceran marcados en funcion del regimen economico del paciente como hasta ahora, si no que no aparecera ninguno seleccionado
			//siendo el usuario el que este obligado a seleccionar el regimen economico deseado.
			//Ademas en este caso, se resaltara el frame de regimen economico para indicar su cumplimentacion.
			//Nota: CURIA pidio expresamente que desde el modulo de OTROS PROFESIONALES no se pudiera cambiar el regimen economico,
			//      a si que el requerimiento 995 de CAPIO solamente se aplicara en el resto de modulos
			DataSet rrAux = null;
			bool bExisteEpisodio = false;
			if (MFiliacion.stModulo != "OTROSPROFESIONALES" && MFiliacion.stFiliacion == "MODIFICACION")
			{
				// MANTENER SOCIEDAD EN CIRCUITO AL FILIAR CUANDO SE REGISTRA ACTIVIDAD
				//Si est� activa, seleccionamos el garante del circuito
				bExisteEpisodio = false;
				if (Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "MANSOCIR", "VALFANU1") == "S")
				{

					//'Usamos esta funci�n para saber si se han declarado o no
					bExisteEpisodio = ExisteEpisodio(MFiliacion.OBJETO2);
					//Buscamo de qu� tipo es la sociedad
					if (bExisteEpisodio)
					{
						if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "CI")
						{
							SqlDataAdapter tempAdapter_8 = new SqlDataAdapter("SELECT gsocieda,ginspecc FROM CCITPROG where ganoregi =" + Convert.ToString(MFiliacion.OBJETO2.stAReg).Trim() + " and gnumregi = " + Convert.ToString(MFiliacion.OBJETO2.stNReg), MFiliacion.GConexion);
							rrAux = new DataSet();
							tempAdapter_8.Fill(rrAux);
						}
						else if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "CO" || Convert.ToString(MFiliacion.OBJETO2.stTReg) == "C")
						{ 
							SqlDataAdapter tempAdapter_9 = new SqlDataAdapter("SELECT gsocieda,ginspecc FROM CCONSULT where ganoregi =" + Convert.ToString(MFiliacion.OBJETO2.stAReg).Trim() + " and gnumregi = " + Convert.ToString(MFiliacion.OBJETO2.stNReg), MFiliacion.GConexion);
							rrAux = new DataSet();
							tempAdapter_9.Fill(rrAux);
						}
						else if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "U")
						{ 
							SqlDataAdapter tempAdapter_10 = new SqlDataAdapter("SELECT gsocieda,ginspecc FROM UEPISURG where ganourge =" + Convert.ToString(MFiliacion.OBJETO2.stAReg).Trim() + " and gnumurge = " + Convert.ToString(MFiliacion.OBJETO2.stNReg), MFiliacion.GConexion);
							rrAux = new DataSet();
							tempAdapter_10.Fill(rrAux);
						}
						else if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "Q")
						{ 
							SqlDataAdapter tempAdapter_11 = new SqlDataAdapter("SELECT gsocieda,ginspecc FROM QINTQUIR where ganoregi =" + Convert.ToString(MFiliacion.OBJETO2.stAReg).Trim() + " and gnumregi = " + Convert.ToString(MFiliacion.OBJETO2.stNReg), MFiliacion.GConexion);
							rrAux = new DataSet();
							tempAdapter_11.Fill(rrAux);
						}
						else if (Convert.ToString(MFiliacion.OBJETO2.stTReg) == "H")
						{ 
							SqlDataAdapter tempAdapter_12 = new SqlDataAdapter("SELECT gsocieda,ginspecc FROM AMOVIFIN WHERE GANOREGI = " + Convert.ToString(MFiliacion.OBJETO2.stAReg).Trim() + " AND GNUMREGI = " + Convert.ToString(MFiliacion.OBJETO2.stNReg) + " AND FFINMOVI IS NULL", MFiliacion.GConexion);
							rrAux = new DataSet();
							tempAdapter_12.Fill(rrAux);
						}
						if (rrAux.Tables[0].Rows.Count != 0)
						{
							if (Convert.ToDouble(rrAux.Tables[0].Rows[0]["gsocieda"]) == MFiliacion.CodSS)
							{
								rbRERegimen[0].IsChecked = true;
								RecuperarInspeccion(MFiliacion.ModCodigo, Convert.ToInt32(rrAux.Tables[0].Rows[0]["gsocieda"]), Convert.ToString(rrAux.Tables[0].Rows[0]["ginspecc"]).Trim());
							}
							else if (Convert.ToDouble(rrAux.Tables[0].Rows[0]["gsocieda"]) == MFiliacion.CodPriv)
							{ 
								rbRERegimen[2].IsChecked = true;

							}
							else
							{
								rbRERegimen[1].IsChecked = true;
								RecuperarSociedad(MFiliacion.ModCodigo, Convert.ToInt32(rrAux.Tables[0].Rows[0]["gsocieda"]));
							}
							stSocAntigua = Convert.ToString(rrAux.Tables[0].Rows[0]["gsocieda"]).Trim();
						}
						else
						{
							bExisteEpisodio = false;
						}
					}

				}
				//Si no existe episodio, bien porque no se haya definido o bien porque no mande, comporbamos lo del indicador
				if (!bExisteEpisodio)
				{
					SqlDataAdapter tempAdapter_13 = new SqlDataAdapter("SELECT isnull(irevrege,'N') irevrege FROM SUSUARIO where gusuario ='" + MFiliacion.sUsuario + "'", MFiliacion.GConexion);
					rrAux = new DataSet();
					tempAdapter_13.Fill(rrAux);
					if (rrAux.Tables[0].Rows.Count != 0)
					{
						if (Convert.ToString(rrAux.Tables[0].Rows[0]["irevrege"]) == "S")
						{
							rbRERegimen[0].IsChecked = false;
							rbRERegimen[1].IsChecked = false;
							rbRERegimen[2].IsChecked = false;
							Colorear_Cajas();
							//Desactivar_Cajas
						}
					}
					rrAux.Close();
				}
			}
			//-------------------

			// Si estamos modificando, comprobamos si hay que mostrar aviso de filiaci�n

			if (MFiliacion.stFiliacion == "MODIFICACION")
			{
				proAvisosFiliacion();
			}

			//Oscar C Marzo 2012. se parametriza la longitud del telefono    �
			double lngTelefono = Conversion.Val(Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "LNGTELEF", "NNUMERI1"));
			tbDPTelf1.MaxLength = Convert.ToInt32(Math.Min(15, lngTelefono));
			tbDPTelf2.MaxLength = Convert.ToInt32(Math.Min(15, lngTelefono));
			tbDPTelf3.MaxLength = Convert.ToInt32(Math.Min(15, lngTelefono));

			tbPCTelf1.MaxLength = Convert.ToInt32(Math.Min(15, lngTelefono));
			tbPCTelf2.MaxLength = Convert.ToInt32(Math.Min(15, lngTelefono));

			//Se pone aqu� por mi santos cojones.......�������������qu�!!!!!!!!!!!!!!!!!
			//�Alg�n problema?......
			//Es que en el activate, cada vez que se viene de la pantalla de la diosa(en min�sculas) esa o del RCA se volv�a a actualizar.
			//Y por tanto, al dar a guardar, no se actualizaban ni los apellidos ni el nombre ni el sexo ni la fecha de nacimiento.
			if (MFiliacion.stFiliacion == "MODIFICACION")
			{
				cambiar_identificador = false;
			}

            //INDRA_DGMORENOG_16/05/2016 - INICIO
            //Proposito: Necesario para activar el evento DoubleClick sobre el contro RadioButton 
            //Sprint 5
            ////
            MethodInfo m = typeof(RadioButton).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
            if (m != null)
            {
                m.Invoke(_rbDNIOtroDocDP_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbDNIOtroDocDP_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbDNIOtroDocPC_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbDNIOtroDocPC_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbDNIOtroDocRE_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbDNIOtroDocRE_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbIndicadorSS_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbIndicadorSS_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbIndicador_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbIndicador_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbRERegimen_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbRERegimen_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbRERegimen_2, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbResponsable_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbResponsable_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbTutor_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbTutor_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbAutorizacion_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbAutorizacion_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });

            }
            _rbDNIOtroDocDP_0.MouseDoubleClick += rbDNIOtroDocDP_DblClick;
            _rbDNIOtroDocDP_1.MouseDoubleClick += rbDNIOtroDocDP_DblClick;

            _rbDNIOtroDocPC_0.MouseDoubleClick += rbDNIOtroDocPC_DblClick;
            _rbDNIOtroDocPC_1.MouseDoubleClick += rbDNIOtroDocPC_DblClick;

            _rbDNIOtroDocRE_0.MouseDoubleClick += rbDNIOtroDocRE_DblClick;
            _rbDNIOtroDocRE_1.MouseDoubleClick += rbDNIOtroDocRE_DblClick;

            _rbIndicadorSS_0.MouseDoubleClick += rbIndicadorSS_DblClick;
            _rbIndicadorSS_1.MouseDoubleClick += rbIndicadorSS_DblClick;

            _rbIndicador_0.MouseDoubleClick += rbIndicador_DblClick;
            _rbIndicador_1.MouseDoubleClick += rbIndicador_DblClick;

            _rbTutor_0.MouseDoubleClick += rbTutor_DblClick;
            _rbTutor_1.MouseDoubleClick += rbTutor_DblClick;

            _rbRERegimen_0.MouseDoubleClick += rbRERegimen_DblClick;
            _rbRERegimen_1.MouseDoubleClick += rbRERegimen_DblClick;
            _rbRERegimen_2.MouseDoubleClick += rbRERegimen_DblClick;

            _rbAutorizacion_0.MouseDoubleClick += rbAutorizacion_DblClick;
            _rbAutorizacion_1.MouseDoubleClick += rbAutorizacion_DblClick;

            /*_rbResponsable_0.MouseDoubleClick += rbResponsable_DblClick;
            _rbResponsable_1.MouseDoubleClick += rbResponsable_DblClick;*/

            ////
            //INDRA_DGMORENOG_10/05/2016 - FIN
            //Proposito: Necesario para activar el evento DoubleClick sobre el contro RadioButton 
            //Sprint 4
            ////
                        
            this._tsDatosPac_TabPage0.Item.BorderColor = Color.FromArgb(110,153,210);            
            this._tsDatosPac_TabPage1.Item.BorderColor = Color.FromArgb(110, 153, 210);            
            this._tsDatosPac_TabPage2.Item.BorderColor = Color.FromArgb(110, 153, 210);            
            this._tsDatosPac_TabPage3.Item.BorderColor = Color.FromArgb(110, 153, 210);

            /*this._tsDatosPac_TabPage0.Item.BorderColor = tsDatosPac.ViewElement.BorderColor;
            this._tsDatosPac_TabPage1.Item.BorderColor = tsDatosPac.ViewElement.BorderColor;
            this._tsDatosPac_TabPage2.Item.BorderColor = tsDatosPac.ViewElement.BorderColor;
            this._tsDatosPac_TabPage3.Item.BorderColor = tsDatosPac.ViewElement.BorderColor;*/

            chbSMS.Text = "Recibir SMS en" + Environment.NewLine + "este tel�fono";
            chkExentoIVA.Text = "Exento" + Environment.NewLine + "de IVA";
            lbDependencia[3].Text = "Otras circunstancias" + Environment.NewLine + "de inter�s";
        }

        private bool ExisteEpisodio(dynamic obj)
		{
			//'Esta variable (stAReg) se ha de llamar igual en todos los sitios que se declare ya que se usa en filiacionE y se pasan en un objeto a trav�s de FiliacionDLL
			//'Usamos esta funci�n para saber si se han declarado o no
			try
			{
				string stEpisodio = String.Empty;
				stEpisodio = obj.stAReg;

				return stEpisodio.Trim() != "";
			}
			catch
			{
				return false;
			}
		}

		private void proAvisosFiliacion()
		{
			string StSql = String.Empty;
			DataSet RrDatos = null;
			string stMensaje = String.Empty;
			int lDias = 0;

			try
			{
				// Comprobamos si hay que mostrar los avisos

				StSql = "SELECT rTrim(isNull(valfanu2, '')) + rTrim(isNull(valfanu3, '')) Mensaje, nnumeri1 FROM SCONSGLO WHERE " + 
				        "gconsglo = 'IAVISFIL' AND valfanu1 = 'S'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					RrDatos.Close();
					return;
				}
				else
				{

					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["Mensaje"]))
					{
						stMensaje = "Ser�a recomendable revisar y actualizar los datos del paciente";
					}
					else
					{
						stMensaje = Convert.ToString(RrDatos.Tables[0].Rows[0]["Mensaje"]).Trim();
					}

					if (Convert.IsDBNull(RrDatos.Tables[0].Rows[0]["nnumeri1"]))
					{
						lDias = 30;
					}
					else
					{
						lDias = Convert.ToInt32(RrDatos.Tables[0].Rows[0]["nnumeri1"]);
					}

					RrDatos.Close();

				}

				// Obtenemos la �ltima fecha en que se actualizaron los datos de este paciente

				StSql = "SELECT 'S' FROM DPACIENT WHERE " + 
				        "gidenpac = '" + MFiliacion.ModCodigo + "' AND " + 
				        "datediff(d, isNull(fultmovi, getDate()), getDate()) >= " + lDias.ToString();

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, MFiliacion.GConexion);
				RrDatos = new DataSet();
				tempAdapter_2.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count != 0)
				{

					// Como han pasado m�s d�as de los indicados en la constante, mostramos el mensaje

					RadMessageBox.Show(stMensaje, Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);

				}

				RrDatos.Close();
			}
			catch
			{
			}
		}

		private bool proEstaCancelado()
		{
			bool result = false;
			string StSql = "select '' from SPACIENT where gidenpac = '" + MFiliacion.ModCodigo + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
			DataSet RrCancelado = new DataSet();
			tempAdapter.Fill(RrCancelado);

			result = RrCancelado.Tables[0].Rows.Count != 0;
			
			RrCancelado.Close();

			return result;
		}

		private void CargarLoad()
		{
			int i = 0;

            try
            {
                //O.Frias - 06/08/2009
                //Cargo el nuevo compo de emplazamiento.
                cbbEmplazamiento.Items.Clear();
                cbbEmplazamiento.Items.Add(new RadListDataItem("Habitual", 0));
                cbbEmplazamiento.Items.Add(new RadListDataItem("Desplazado", 1));
                cbbEmplazamiento.Items.Add(new RadListDataItem("Transeunte", 2));
                cbbEmplazamiento.SelectedIndex = 0;

                //FORMATO DE FILIACION
                SqlDataAdapter tempAdapter = new SqlDataAdapter("Select valfanu1 from sConsGlo where gConsGlo = 'FORMFILI'", MFiliacion.GConexion);
                DataSet Rrglobal = new DataSet();
                tempAdapter.Fill(Rrglobal);
                MFiliacion.stFORMFILI = Convert.ToString(Rrglobal.Tables[0].Rows[0]["valfanu1"]).Trim();
                Rrglobal.Close();
                //PONER DNI � CEDULA

                string stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'idenpers' ";
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
                DataSet RrTemp = new DataSet();
                tempAdapter_2.Fill(RrTemp);
                if (RrTemp.Tables[0].Rows.Count != 0)
                {
                    MFiliacion.DNICEDULA = Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim();
                }
                RrTemp.Close();


                rbDNIOtroDocDP[0].Text = "DNI/NIF";
                rbDNIOtroDocRE[0].Text = "DNI/NIF";
                rbDNIOtroDocPC[0].Text = "DNI/NIF";


                //(maplaza)(08/08/2007)Se obtiene el valor de la variable "VALREGEC" (Validaci�n R�gimen Econ�mico)
                if (Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "VALREGEC", "VALFANU1") == "S")
                {
                    bValidarRegimenEconomico = true;
                }
                else
                {
                    //si no hay que aceptar el r�gimen econ�mico, no aparece visible el check de aceptaci�n.
                    chkValidarRE.Visible = false;
                    bValidarRegimenEconomico = false;
                }
                //------

                stsqltemp = "SELECT " +
                            "rTrim(isNull(valfanu2, '')) + rTrim(isNull(valfanu3, '')) Mensaje " +
                            "FROM SCONSGLO WHERE gconsglo = 'VALAUDAT' AND valfanu1 = 'S'";

                SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
                RrTemp = new DataSet();
                tempAdapter_3.Fill(RrTemp);

                if (RrTemp.Tables[0].Rows.Count == 0)
                {
                    bValidarAutorizacionDatos = false;
                    stMensajeValidarAutorizacionDatos = "";
                }
                else
                {
                    bValidarAutorizacionDatos = true;

                    if (Convert.ToString(RrTemp.Tables[0].Rows[0]["Mensaje"]).Trim() == "")
                    {
                        stMensajeValidarAutorizacionDatos = "Marc� no autorizar la utilizaci�n de los datos";
                    }
                    else
                    {
                        stMensajeValidarAutorizacionDatos = Convert.ToString(RrTemp.Tables[0].Rows[0]["Mensaje"]).Trim();
                    }
                }

                RrTemp.Close();

                //Nombre de la aplicacion
                stsqltemp = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO = 'NOMAPLIC'";
                SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
                RrTemp = new DataSet();
                tempAdapter_4.Fill(RrTemp);
                MFiliacion.NombreApli = Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim();
                RrTemp.Close();

                //Comprobar si podemos cambiar el gidenpac del paciente
                //Si la constante no existe o tiene valor 'S', se continuar� como ahora cambiando el identificador.
                //Si la constante tiene valor 'N', no se cambiar� el identificador
                Permitir_cambiar_Gidenpac = true;
                stsqltemp = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO = 'GIDENPAC'";
                SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
                RrTemp = new DataSet();
                tempAdapter_5.Fill(RrTemp);
                if (RrTemp.Tables[0].Rows.Count != 0)
                {
                    if (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() != "S")
                    {
                        Permitir_cambiar_Gidenpac = false;
                    }
                }
                RrTemp.Close();



                string LitPersona = Serrores.ObtenerLiteralPersona(MFiliacion.GConexion);
                LitPersona = LitPersona.ToLower();

                DFI111F1.DefInstance.Text = "Actualizaci�n de datos del " + LitPersona + " - DFI111F1";
                SSTabHelper.SetSelectedIndex(tsDatosPac, 0);
                tsDatosPac.SelectedPage.Text = "Datos " + LitPersona;

                CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_FILIACION);

                bcargado_privado = false;
                bcbaceptar = false;
                bcambiar = true;
                bcambiarf = true;
                MFiliacion.ObtenerValoresTipoBD();
                MFiliacion.FechaHoraSistema();
                MFiliacion.Establecer_Propiedades_DateCombo(sdcFechaNac);
                // Inicialmente no hay ninguna fila seleccionada en la lista de
                // personas de contacto
                sprContactos.Row = -1;
                sprContactos.MaxRows = 0;
                sprContactos.MaxCols = 21; //19
                for (i = 12; i <= 20; i++)
                { //19
                    sprContactos.Col = i;
                    sprContactos.SetColHidden(sprContactos.Col, true);
                }

                // Mostramos la columna del NIF

                sprContactos.Col = 2;
                sprContactos.SetColHidden(sprContactos.Col, false);

                //-----------------------------

                sprContactos.setSelModeIndex(0);
                sprContactos.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
                iFilaContactos = 0;

                sprContactos.SetText(21, 0, "Tipo de Contato");
                sprContactos.SetColWidth(21, 30);
                //sdcFechaNac.MaxDate = Format(vstFechaHoraSis, "dd/mm/yyyy")
                if (MFiliacion.CodPac)
                {
                    MFiliacion.stFiliacion = "MODIFICACION";
                    MFiliacion.ModCodigo = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gidenpac"]);
                }
                string SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SEGURSOC'";
                SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
                DataSet RrSQLENTIDAD = new DataSet();
                tempAdapter_6.Fill(RrSQLENTIDAD);
                //codigo de la SEGURIDAD SOCIAL
                MFiliacion.CodSS = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
                //texto de la seguridad social
                MFiliacion.TexSS = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
                SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'PRIVADO'";
                SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
                RrSQLENTIDAD = new DataSet();
                tempAdapter_7.Fill(RrSQLENTIDAD);
                //codigo de regimen economco privado
                MFiliacion.CodPriv = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
                //texto de regimen economco privado
                MFiliacion.TexPriv = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
                RrSQLENTIDAD.Close();
                //cALCULO DE LA LETRA DEL DNI
                SQLENTIDAD = "SELECT valfanu1, valfanu2 FROM SCONSGLO WHERE GCONSGLO = 'CALDNIAU'";
                SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
                RrSQLENTIDAD = new DataSet();
                tempAdapter_8.Fill(RrSQLENTIDAD);
                if (RrSQLENTIDAD.Tables[0].Rows.Count == 0)
                {
                    MFiliacion.bCalcLetra = false;
                    MFiliacion.bCalcLetraObligatoria = false;
                }
                else
                {
                    MFiliacion.bCalcLetra = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["valfanu1"]).Trim() == "S";
                    MFiliacion.bCalcLetraObligatoria = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["valfanu2"]).Trim() == "S";
                }
                RrSQLENTIDAD.Close();
                string SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SHOMBRE'";
                SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(SQLSEXO, MFiliacion.GConexion);
                DataSet RrSEXO = new DataSet();
                tempAdapter_9.Fill(RrSEXO);
                MFiliacion.CodSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
                MFiliacion.TexSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

                SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SMUJER'";
                SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(SQLSEXO, MFiliacion.GConexion);
                RrSEXO = new DataSet();
                tempAdapter_10.Fill(RrSEXO);
                MFiliacion.CodSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
                MFiliacion.TexSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

                SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SINDET'";
                SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(SQLSEXO, MFiliacion.GConexion);
                RrSEXO = new DataSet();
                tempAdapter_11.Fill(RrSEXO);
                MFiliacion.CodSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
                MFiliacion.TexSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();
                RrSEXO.Close();


                string SQLPROV = "SELECT * FROM SCONSGLO WHERE GCONSGLO ='CODPROVI'";
                SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(SQLPROV, MFiliacion.GConexion);
                DataSet RrSPROV = new DataSet();
                tempAdapter_12.Fill(RrSPROV);
                MFiliacion.CodPROV = Convert.ToString(RrSPROV.Tables[0].Rows[0]["valfanu1"]).Trim();
                RrSPROV.Close();
                //'End If ' ^  ????
                // |
                //oscar 28/10/04
                DataSet RR = null;
                //Dim intEstado As Integer
                string sql = String.Empty;
                SqlCommand RqDeudas = null;

                cmdDeudas.Enabled = false;
                rbBloqueo[0].Enabled = false;
                rbBloqueo[1].Enabled = false;
                if (MFiliacion.stFiliacion == "MODIFICACION")
                {

                    SqlDataAdapter tempAdapter_13 = new SqlDataAdapter("select isnull(valfanu1,'') from sconsglo where gconsglo='CANTBLOQ'", MFiliacion.GConexion);
                    RR = new DataSet();
                    tempAdapter_13.Fill(RR);
                    if (RR.Tables[0].Rows.Count != 0)
                    {
                        if (Convert.ToString(RR.Tables[0].Rows[0][0]).Trim().ToUpper() == "SI")
                        {
                            cmdDeudas.Enabled = true;
                        }
                    }
                    RR.Close();

                    rbBloqueo[1].Enabled = true;

                    //Ahora estamos esperando a el funcionamiento del proc. almacenado de IFMS
                    //que devuelve un indicador que determina el estado del paciente en cuanto a deudas:
                    //0 - Sin Deuda
                    //1 - Deuda <= que la permitida
                    //2 - Deuda > que la permitida

                    sql = "IFMSFA_DEUDAPACI";
                    
                    RqDeudas = MFiliacion.GConexion.CreateQuery("ESTADOPAC", sql);
                    RqDeudas.CommandType = CommandType.StoredProcedure;
                    
                    RqDeudas.Parameters.Add("@PGIDENPAC", SqlDbType.Char).Size = 13;
                    RqDeudas.Parameters.Add("@ESTADO", SqlDbType.Int);

                    RqDeudas.Parameters[0].Direction = ParameterDirection.Input;
                    RqDeudas.Parameters[1].Direction = ParameterDirection.Output;
                    RqDeudas.Parameters[0].Value = MFiliacion.ModCodigo;

                    RqDeudas.ExecuteNonQuery();

                    intEstado = Convert.ToInt32(RqDeudas.Parameters[1].Value);
                }
                //-------

                MFiliacion.FechaHoraSistema();

                sdcDPFechaFallecimiento.MaxDate = DateTime.Parse(MFiliacion.vstFechaHoraSis);
                sdcDPFechaFallecimiento.NullableValue = null;
                sdcDPFechaFallecimiento.SetToNullValue();


                //If stModulo <> "DATOS COMUNES" Then
                frmExitus.Enabled = !(MFiliacion.stModulo.ToUpper() != "ARCHIVO" && MFiliacion.stModulo != "ADMISION" && MFiliacion.stModulo != "DATOS COMUNES");
                //Si el tag de la etiqueta est� vac�o, no est� activa la constante de diferenciaci�n de tercero.
                if (Convert.ToString(lblObraSocial.Tag) != "")
                {
                    //Cargamos los terceros econ�micos.
                    SqlDataAdapter tempAdapter_14 = new SqlDataAdapter("select b07_denom,B07_eterc,B07_rterc from FETERB07 a where exists ( " +
                                                    "select '' from ifmsfa_dsocieda b where fborrado IS NULL and FDESACTI is null and ginspecc = '0' and " +
                                                    "a.B07_rterc = b.rterc and a.B07_eterc = b.eterc group by b.rterc,b.eterc) order by b07_denom ", MFiliacion.GConexion);
                    RrDSOCIEDA = new DataSet();
                    tempAdapter_14.Fill(RrDSOCIEDA);
                    CbbObraSocial.Items.Clear();
                    CbbObraSocial.Items.Add(new RadListDataItem(""));

                    foreach (DataRow iteration_row in RrDSOCIEDA.Tables[0].Rows)
                    {
                        if (Convert.ToString(iteration_row["B07_eterc"]).Trim().ToUpper() != MFiliacion.CodSS.ToString().Trim().ToUpper() && Convert.ToString(iteration_row["B07_eterc"]).Trim().ToUpper() != MFiliacion.CodPriv.ToString().Trim().ToUpper())
                        {
                            CbbObraSocial.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row["b07_denom"]).Trim() + new String(' ', 500) + Convert.ToString(iteration_row["B07_rterc"]).Trim() + Convert.ToString(iteration_row["B07_eterc"]).Trim(), VbStrConv.ProperCase, 0), CbbObraSocial.Items.Count - 1));
                        }
                    }
                    RrDSOCIEDA.Close();
                }
                else
                {
                    //Carga la tabla de sociedades
                    //Set RrDSOCIEDA = GConexion.OpenResultset("select * from DSOCIEDAVA order by DSOCIEDAVA.dsocieda", rdOpenStatic)
                    SqlDataAdapter tempAdapter_15 = new SqlDataAdapter("select * from DSOCIEDAVA where FDESACTI is null order by DSOCIEDAVA.dsocieda", MFiliacion.GConexion);
                    RrDSOCIEDA = new DataSet();
                    tempAdapter_15.Fill(RrDSOCIEDA);
                    cbbRESociedad.Items.Clear();
                    cbbRESociedad.Items.Add(new RadListDataItem(""));
                    RrDSOCIEDA.MoveFirst();
                    foreach (DataRow iteration_row_2 in RrDSOCIEDA.Tables[0].Rows)
                    {
                        if (Convert.ToString(iteration_row_2["gsocieda"]).Trim().ToUpper() != MFiliacion.CodSS.ToString().Trim().ToUpper() && Convert.ToString(iteration_row_2["gsocieda"]).Trim().ToUpper() != MFiliacion.CodPriv.ToString().Trim().ToUpper())
                        {
                            cbbRESociedad.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row_2["dsocieda"]).Trim(), VbStrConv.ProperCase, 0), Convert.ToInt32(iteration_row_2["gsocieda"])));
                        }
                    }
                    RrDSOCIEDA.Close();
                }
                //BUSCAR RELACION SOCIEDAD/EMPRESA
                bool bbtnactivo = false;
                SQLENTIDAD = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO = 'IEMPRSOC'";
                SqlDataAdapter tempAdapter_16 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
                RrSQLENTIDAD = new DataSet();
                tempAdapter_16.Fill(RrSQLENTIDAD);
                if (RrSQLENTIDAD.Tables[0].Rows.Count == 0)
                {
                    bbtnactivo = false;
                }
                else
                {
                    bbtnactivo = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["valfanu1"]).Trim() == "S";
                }
                RrSQLENTIDAD.Close();
                cbBuscarSoc.Enabled = bbtnactivo;
                cbBuscarSoc.Visible = bbtnactivo;


                //cargar la tabla de Inspeccion
                //Set RrDHOSPITA = GConexion.OpenResultset("select * from DINSPECCVA order by DINSPECCVA.dinspecc", rdOpenStatic)
                SqlDataAdapter tempAdapter_17 = new SqlDataAdapter("select * from DINSPECCVA Where FDESACTI Is Null order by DINSPECCVA.dinspecc", MFiliacion.GConexion);
                RrDHOSPITA = new DataSet();
                tempAdapter_17.Fill(RrDHOSPITA);
                int indice = 0;
                cbbREInspeccion.Items.Clear();
                if (RrDHOSPITA.Tables[0].Rows.Count != 0)
                {
                    object tempRefParam = "";
                    object tempRefParam2 = Type.Missing;
                    cbbREInspeccion.Items.Add(new RadListDataItem("", ""));


                    foreach (DataRow iteration_row_3 in RrDHOSPITA.Tables[0].Rows)
                    {
                        string tempRefParam3 = Strings.StrConv(Convert.ToString(iteration_row_3["DINSPECC"]).Trim(), VbStrConv.ProperCase, 0);
                        object tempRefParam4 = indice;
                        cbbREInspeccion.Items.Add(new RadListDataItem(tempRefParam3, tempRefParam4));
                        cbbREInspeccion.set_List(indice, 1, iteration_row_3["GINSPECC"]);
                        indice++;
                    }
                }
                RrDHOSPITA.Close();

                //cargar la tabla de Vias
                SqlDataAdapter tempAdapter_18 = new SqlDataAdapter("select * from DTIPOVIAVA order by dtipovia", MFiliacion.GConexion);
                RrDHOSPITA = new DataSet();
                tempAdapter_18.Fill(RrDHOSPITA);

                indice = 0;
                foreach (DataRow iteration_row_4 in RrDHOSPITA.Tables[0].Rows)
                {
                    object tempRefParam5 = Strings.StrConv(Convert.ToString(iteration_row_4["dtipovia"]).Trim(), VbStrConv.ProperCase, 0);
                    object tempRefParam6 = indice;
                    cbbDPVia.AddItem(tempRefParam5, tempRefParam6);
                    cbbDPVia.set_List(tempRefParam6, 1, iteration_row_4["gtipovia"]);
                    indice++;
                }
                RrDHOSPITA.Close();

                //CARGA LAS tablas DE ESTADO CIVIL
                SqlDataAdapter tempAdapter_19 = new SqlDataAdapter("select * from Destaciv order by Destaciv.destaciv", MFiliacion.GConexion);
                RrDestaciv = new DataSet();
                tempAdapter_19.Fill(RrDestaciv);
                RrDestaciv.MoveFirst();

                indice = 0;

                foreach (DataRow iteration_row_5 in RrDestaciv.Tables[0].Rows)
                {

                    object tempRefParam7 = Strings.StrConv(Convert.ToString(iteration_row_5["destaciv"]).Trim(), VbStrConv.ProperCase, 0);
                    object tempRefParam8 = indice;
                    cbbDPEstado.AddItem(tempRefParam7, tempRefParam8);
                    cbbDPEstado.set_List(indice, 1, iteration_row_5["gestaciv"]);
                    indice++;
                }
                RrDestaciv.Close();

                //CARGA LA TABLA DE POBLACIONES
                string SQLDPOBLACION = String.Empty;

                if (MFiliacion.stFiliacion == "ALTA")
                { // SI ES UN ALTA CARGA LAS POBLACIONES POR DEFECTO
                    SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + MFiliacion.CodPROV + "%' order by dpoblaci";
                }

                if (MFiliacion.stFiliacion == "MODIFICACION")
                { //SI ES UNA MODIFICACION
                    SqlDataAdapter tempAdapter_20 = new SqlDataAdapter("SELECT * FROM DPACIENT WHERE GIDENPAC = '" + MFiliacion.ModCodigo + "'", MFiliacion.GConexion);
                    MFiliacion.RrDPACIENT = new DataSet();
                    tempAdapter_20.Fill(MFiliacion.RrDPACIENT);
                    tempAdapterPaciente = tempAdapter_20;
                    if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]))
                    {
                        //COMPRUEBA QUE LA PROVINCIA SEA LA QUE ESTA POR DEFECTO
                        if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]) == MFiliacion.CodPROV)
                        {
                            SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + MFiliacion.CodPROV + "%' order by dpoblaci";
                            Codigoprov1 = MFiliacion.CodPROV;
                            Codigoprov2 = MFiliacion.CodPROV;
                            Codigoprov3 = MFiliacion.CodPROV;
                        }
                        else
                        {
                            SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]) + "%' order by dpoblaci";
                            Codigoprov1 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]);
                            Codigoprov2 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]);
                            Codigoprov3 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]);
                            MFiliacion.CodPROV = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]);
                        }
                        MFiliacion.RrDPACIENT.Close();
                    }
                    else
                    {
                        SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + MFiliacion.CodPROV + "%' order by dpoblaci";
                    }
                }
                CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbDPPoblacion);
                CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbREPoblacion);
                CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbPCPoblacion);
                //CargarComboAlfanumerico SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbPobAnt
                TPoblacion = true;
                //RrDPOBLACION.Close
                //carga la tabla de poblaciones de nacimieto
                if (MFiliacion.stFiliacion == "ALTA")
                { // SI ES UN ALTA CARGA LAS POBLACIONES POR DEFECTO
                    SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + MFiliacion.CodPROV + "%' order by dpoblaci";
                }
                if (MFiliacion.stFiliacion == "MODIFICACION")
                { //SI ES UNA MODIFICACION
                    SqlDataAdapter tempAdapter_21 = new SqlDataAdapter("SELECT * FROM DPACIENT WHERE GIDENPAC = '" + MFiliacion.ModCodigo + "'", MFiliacion.GConexion);
                    MFiliacion.RrDPACIENT = new DataSet();
                    tempAdapter_21.Fill(MFiliacion.RrDPACIENT);
                    tempAdapterPaciente = tempAdapter_21;
                    if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBNACI"]))
                    {
                        //COMPRUEBA QUE LA PROVINCIA SEA LA QUE ESTA POR DEFECTO
                        if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBNACI"]).StartsWith(MFiliacion.CodPROV))
                        {
                            SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + MFiliacion.CodPROV + "%' order by dpoblaci";
                            Codigoprov4 = MFiliacion.CodPROV;
                        }
                        else
                        {
                            Codigoprov4 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBNACI"]).Substring(0, Math.Min(2, Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBNACI"]).Length));
                            SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + Codigoprov4 + "%' order by dpoblaci";
                        }
                        MFiliacion.RrDPACIENT.Close();
                    }
                    else
                    {
                        SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + MFiliacion.CodPROV + "%' order by dpoblaci";
                    }
                }
                CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbNacPoblacion);


                //carga la tabla de paises
                SqlDataAdapter tempAdapter_22 = new SqlDataAdapter("select * from Dpaisresva order by dpaisres", MFiliacion.GConexion);
                RrDpais = new DataSet();
                tempAdapter_22.Fill(RrDpais);
                RrDpais.MoveFirst();

                object tempRefParam9 = "";
                object tempRefParam10 = Type.Missing;
                cbbNacPais.AddItem(tempRefParam9, tempRefParam10);

                indice = 0;
                foreach (DataRow iteration_row_6 in RrDpais.Tables[0].Rows)
                {
                    object tempRefParam11 = Strings.StrConv(Convert.ToString(iteration_row_6["dpaisres"]).Trim(), VbStrConv.ProperCase, 0);
                    object tempRefParam12 = indice;
                    cbbDPPais.AddItem(tempRefParam11, tempRefParam12);
                    cbbDPPais.set_List(indice, 1, Convert.ToString(iteration_row_6["gpaisres"]).Trim());
                    object tempRefParam13 = Strings.StrConv(Convert.ToString(iteration_row_6["dpaisres"]).Trim(), VbStrConv.ProperCase, 0);
                    object tempRefParam14 = indice;
                    cbbREPais.AddItem(tempRefParam13, tempRefParam14);
                    cbbREPais.set_List(indice, 1, Convert.ToString(iteration_row_6["gpaisres"]).Trim());
                    object tempRefParam15 = Strings.StrConv(Convert.ToString(iteration_row_6["dpaisres"]).Trim(), VbStrConv.ProperCase, 0);
                    object tempRefParam16 = indice;
                    cbbPCPais.AddItem(tempRefParam15, tempRefParam16);
                    cbbPCPais.set_List(indice, 1, Convert.ToString(iteration_row_6["gpaisres"]).Trim());
                    object tempRefParam17 = Strings.StrConv(Convert.ToString(iteration_row_6["dpaisres"]).Trim(), VbStrConv.ProperCase, 0);
                    object tempRefParam18 = indice;
                    cbbNacPais.AddItem(tempRefParam17, tempRefParam18); //pais de nacimiento
                    cbbNacPais.set_List(indice, 1, Convert.ToString(iteration_row_6["gpaisres"]).Trim());
                    indice++;
                }
                RrDpais.Close();

                //CARGA LA TABLA DE CATALOGACION SOCIAL
                SqlDataAdapter tempAdapter_23 = new SqlDataAdapter("select * from DCATASOCVA order by dcatasoc", MFiliacion.GConexion);
                RrCatSoc = new DataSet();
                tempAdapter_23.Fill(RrCatSoc);

                indice = 0;
                foreach (DataRow iteration_row_7 in RrCatSoc.Tables[0].Rows)
                {
                    object tempRefParam19 = Strings.StrConv(Convert.ToString(iteration_row_7["DCATASOC"]).Trim(), VbStrConv.ProperCase, 0);
                    object tempRefParam20 = indice;
                    cbbDPCatSoc.AddItem(tempRefParam19, tempRefParam20);
                    cbbDPCatSoc.set_List(indice, 1, Convert.ToString(iteration_row_7["GCATASOC"]).Trim());
                    indice++;
                }
                RrCatSoc.Close();
                //carga de formas de pago
                SqlDataAdapter tempAdapter_24 = new SqlDataAdapter("select gforpago, dforpago,idomicil from dforpago", MFiliacion.GConexion);
                RrCatSoc = new DataSet();
                tempAdapter_24.Fill(RrCatSoc);

                DataTable dt = new DataTable();
                dt.Columns.Add("dforpago");
                dt.Columns.Add("gforpago");
                dt.Columns.Add("idomicil");

                /*object tempRefParam21 = "";
			    object tempRefParam22 = Type.Missing;
			    cbbREPago.AddItem(ref tempRefParam21, ref tempRefParam22);
			    foreach (DataRow iteration_row_8 in RrCatSoc.Tables[0].Rows)
			    {
				    object tempRefParam23 = Strings.StrConv(Convert.ToString(iteration_row_8["dforpago"]).Trim(), VbStrConv.ProperCase, 0);
				    object tempRefParam24 = RrCatSoc.getAbsolutePosition() - 1;
				    cbbREPago.AddItem(ref tempRefParam23, ref tempRefParam24);
				    cbbREPago.set_List(RrCatSoc.getAbsolutePosition() - 1, 1, Convert.ToString(iteration_row_8["gforpago"]).Trim());
				    cbbREPago.set_List(RrCatSoc.getAbsolutePosition() - 1, 2, Convert.ToString(iteration_row_8["idomicil"]).Trim());
			    }*/

                foreach (DataRow iteration_row_8 in RrCatSoc.Tables[0].Rows)
                {
                    string tempRefParam1 = Strings.StrConv(Convert.ToString(iteration_row_8["dforpago"]).Trim(), VbStrConv.ProperCase, 0);
                    string tempRefParam2 = Convert.ToString(iteration_row_8["gforpago"]).Trim();
                    string tempRefParam3 = Convert.ToString(iteration_row_8["idomicil"]).Trim();
                    dt.Rows.Add(tempRefParam1, tempRefParam2, tempRefParam3);
                }

                cbbREPago.DataSource = dt;
                cbbREPago.DisplayMember = "dforpago";
                cbbREPago.ValueMember = "gforpago";
                cbbREPago.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
                cbbREPago.EditorControl.ShowHeaderCellButtons = false;
                cbbREPago.EditorControl.ShowRowHeaderColumn = false;
                cbbREPago.EditorControl.ShowColumnHeaders = false;
                
                ((RadGridView)cbbREPago.EditorControl).Columns["gforpago"].IsVisible = false;
                ((RadGridView)cbbREPago.EditorControl).Columns["idomicil"].IsVisible = false;
                ((RadGridView)cbbREPago.EditorControl).TableElement.RowHeight = 20;
                ((RadGridView)cbbREPago.EditorControl).GridViewElement.CellFormatting += cbbREPago_CellFormatting;
                

                RrCatSoc.Close();

                //cargar el combo con los idiomas
                i = 1;
                SqlDataAdapter tempAdapter_25 = new SqlDataAdapter("select * from SIDIOMAS", MFiliacion.GConexion);
                RrCatSoc = new DataSet();
                tempAdapter_25.Fill(RrCatSoc);

                object tempRefParam25 = "";
                object tempRefParam26 = "";
                cbbIdioma.AddItem(tempRefParam25, tempRefParam26);

                foreach (DataRow iteration_row_9 in RrCatSoc.Tables[0].Rows)
                {
                    object tempRefParam27 = Convert.ToString(iteration_row_9["dnomidio"]).Trim();
                    object tempRefParam28 = Type.Missing;
                    cbbIdioma.AddItem(tempRefParam27, tempRefParam28);
                    cbbIdioma.set_Column(1, i, Convert.ToString(iteration_row_9["gidioma"]).Trim());
                    i++;
                }
                cbbIdioma.set_ListIndex(0);
                RrCatSoc.Close();

                //(maplaza)(06/08/2007)Se cargan los combos de "Parentesco con el trabajador" y "C�digo de Categorizaci�n Especial"
                //cargar el combo de "Parentesco con el trabajador"
                i = 1;
                SqlDataAdapter tempAdapter_26 = new SqlDataAdapter("select gparente, dparente from DPARENTE Where fborrado is null", MFiliacion.GConexion);
                RrCatSoc = new DataSet();
                tempAdapter_26.Fill(RrCatSoc);

                object tempRefParam29 = "";
                object tempRefParam30 = DBNull.Value;
                cbbParentesco.AddItem(tempRefParam29, tempRefParam30);

                foreach (DataRow iteration_row_10 in RrCatSoc.Tables[0].Rows)
                {
                    object tempRefParam31 = Convert.ToString(iteration_row_10["dparente"]).Trim();
                    object tempRefParam32 = Type.Missing;
                    cbbParentesco.AddItem(tempRefParam31, tempRefParam32);
                    cbbParentesco.set_Column(1, i, Convert.ToString(iteration_row_10["gparente"]).Trim());
                    i++;
                }

                cbbParentesco.set_ListIndex(0);
                RrCatSoc.Close();

                //cargar el combo de "C�digo de Categorizaci�n Especial"
                i = 1;
                SqlDataAdapter tempAdapter_27 = new SqlDataAdapter("select gcatespe, dcatespe from DCATESPE", MFiliacion.GConexion);
                RrCatSoc = new DataSet();
                tempAdapter_27.Fill(RrCatSoc);

                object tempRefParam33 = "";
                object tempRefParam34 = DBNull.Value;
                cbbCategoria.AddItem(tempRefParam33, tempRefParam34);

                foreach (DataRow iteration_row_11 in RrCatSoc.Tables[0].Rows)
                {
                    object tempRefParam35 = Convert.ToString(iteration_row_11["dcatespe"]).Trim();
                    object tempRefParam36 = Type.Missing;
                    cbbCategoria.AddItem(tempRefParam35, tempRefParam36);
                    cbbCategoria.set_Column(1, i, Convert.ToString(iteration_row_11["gcatespe"]).Trim());
                    i++;
                }

                cbbCategoria.set_ListIndex(0);
                RrCatSoc.Close();
                //------------

                //OSCAR C Noviembre 2009
                //Cargar el combo de "Hospital de Referencia"
                SqlDataAdapter tempAdapter_28 = new SqlDataAdapter("select ghospita, dnomhosp from DHOSPITA where fborrado is null order by dnomhosp", MFiliacion.GConexion);
                RrDSOCIEDA = new DataSet();
                tempAdapter_28.Fill(RrDSOCIEDA);
                cbbHospitalReferencia.Items.Clear();
                cbbHospitalReferencia.Items.Add(new RadListDataItem(""));
                if (RrDSOCIEDA.Tables[0].Rows.Count != 0)
                {
                    foreach (DataRow iteration_row_12 in RrDSOCIEDA.Tables[0].Rows)
                    {
                        cbbHospitalReferencia.Items.Add(new RadListDataItem(Convert.ToString(iteration_row_12["dnomhosp"]).Trim().ToUpper(), Convert.ToInt32(iteration_row_12["ghospita"])));
                    }
                }
                RrDSOCIEDA.Close();
                cbbHospitalReferencia.SelectedIndex = 0;

                //---------------------


                //OSCAR C Diciembre 2010
                //Cargar el combo de "Fuente de la Informacion"
                SqlDataAdapter tempAdapter_29 = new SqlDataAdapter("select GORIGFIL, DORIGFIL from AORIGFIL order by DORIGFIL", MFiliacion.GConexion);
                RrDSOCIEDA = new DataSet();
                tempAdapter_29.Fill(RrDSOCIEDA);
                cbbOrigInfo.Items.Clear();
                cbbOrigInfo.Items.Add(new RadListDataItem(""));
                if (RrDSOCIEDA.Tables[0].Rows.Count != 0)
                {
                    RrDSOCIEDA.MoveFirst();
                    foreach (DataRow iteration_row_13 in RrDSOCIEDA.Tables[0].Rows)
                    {
                        cbbOrigInfo.Items.Add(new RadListDataItem(Convert.ToString(iteration_row_13["DORIGFIL"]).Trim().ToUpper(), Convert.ToInt32(iteration_row_13["GORIGFIL"])));
                    }
                }
                RrDSOCIEDA.Close();
                cbbOrigInfo.SelectedIndex = 0;

                //---------------------
                //'''''''''''''''''''''''''''''''''''''''''''''''''
                //SI ES UN ALTA O MODIFICACION CARGA LOS DATOS DEL FORMULARIO
                //ANTERIOR EN ESTE
                if (MFiliacion.CodPac)
                {

                    if (MFiliacion.stFiliacion == "ALTA" || MFiliacion.stFiliacion == "MODIFICACION")
                    {
                        tbApellido1.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["dape1pac"]).Trim().ToUpper();
                        if (!Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["dape2pac"]))
                        {
                            tbApellido2.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["dape2pac"]).Trim().ToUpper();
                        }
                        tbNombre.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["dnombpac"]).Trim().ToUpper();
                        if (!Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["fnacipac"]))
                        {
                            sdcFechaNac.Value = DateTime.Parse(Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["fnacipac"]) + "");
                        }
                        if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["itipsexo"]) == MFiliacion.CodSHombre)
                        {
                            rbSexo[0].IsChecked = true;
                            rbSexo[1].IsChecked = false;
                            if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) == "S")
                            {
                                rbSexo[2].Enabled = false;
                                rbSexo[2].Visible = false;
                            }
                            else
                            {
                                rbSexo[2].IsChecked = false;
                            }
                        }
                        else
                        {
                            if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["itipsexo"]) == MFiliacion.CodSMUJER)
                            {
                                rbSexo[0].IsChecked = false;
                                rbSexo[1].IsChecked = true;
                                if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) == "S")
                                {
                                    rbSexo[2].Enabled = false;
                                    rbSexo[2].Visible = false;
                                }
                                else
                                {
                                    rbSexo[2].IsChecked = false;
                                }
                            }
                            else
                            {
                                //O.Frias -- 15/11/2010
                                //Seg�n la constante se opera con Indeterminado o no
                                if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) == "S")
                                {

                                    rbSexo[2].Enabled = false;
                                    rbSexo[2].Visible = false;

                                }
                                else
                                {
                                    rbSexo[0].IsChecked = false;
                                    rbSexo[1].IsChecked = false;
                                    rbSexo[2].IsChecked = true;
                                }
                            }
                        }


                        //(maplaza)(03/05/2006)
                        if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ndninifp"]))
                        {
                            rbDNIOtroDocDP[0].IsChecked = false;
                            rbDNIOtroDocDP[1].IsChecked = false;
                            tbNIFOtrosDocDP.Enabled = false;
                            if (tbNIFOtrosDocDP.Mask == MFiliacion.stFORMATO_NIF)
                            {
                                tbNIFOtrosDocDP.Text = "         ";
                            }
                            else
                            {
                                tbNIFOtrosDocDP.Text = "";
                            }
                        }
                        else
                        {
                            if (MFiliacion.EsDNI_NIF(Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ndninifp"]).Trim(), 9))
                            {
                                rbDNIOtroDocDP[0].IsChecked = true;
                                rbDNIOtroDocDP[1].IsChecked = false;
                                tbNIFOtrosDocDP.Enabled = true;
                                if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ndninifp"]).Trim().Length == 9)
                                {
                                    tbNIFOtrosDocDP.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ndninifp"]).Trim();
                                }
                                else
                                {
                                    tbNIFOtrosDocDP.Text = StringsHelper.Format(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ndninifp"], "00000000") + " ";
                                }
                            }
                            else
                            {
                                //no es DNI ni NIF (es "Otros documentos)
                                rbDNIOtroDocDP[0].IsChecked = false;
                                rbDNIOtroDocDP[1].IsChecked = true;
                                tbNIFOtrosDocDP.Enabled = true;
                                tbNIFOtrosDocDP.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ndninifp"]).Trim();
                            }
                            //tbNIFOtrosDocDP.Text = RrSQLUltimo("ndninifp")
                            tbNIFOtrosDocDP_Leave(tbNIFOtrosDocDP, new EventArgs());
                        }

                        //BUSCA LA TARJETA SANITARIA
                        if (!Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ntarjeta"]) && tbTarjeta.Text.Trim() == "")
                        {
                            tbTarjeta.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ntarjeta"]).Trim();
                        }

                        //(maplaza)(06/08/2007)
                        //BUSCA LA TARJETA SANITARIA EUROPEA
                        if (!Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ntarjsae"]) && tbTarjetaEuropea.Text.Trim() == "")
                        {
                            tbTarjetaEuropea.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ntarjsae"]).Trim();
                        }
                        //-----


                        if (CipAutonomico)
                        {
                            if (!Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ocipauto"]) && lbCipAutonomico.Text.Trim() == "")
                            {
                                lbCipAutonomico.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ocipauto"]).Trim();
                            }
                        }
                        else
                        {
                            if (!Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ncipauto"]) && lbCipAutonomico.Text.Trim() == "")
                            {
                                lbCipAutonomico.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ncipauto"]).Trim();
                            }
                        }


                        if (!Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gpaisnac"]) && cbbNacPais.Text.Trim() == "")
                        {
                            for (i = 0; i <= cbbNacPais.ListCount - 1; i++)
                            {
                                object tempRefParam37 = 1;
                                object tempRefParam38 = i;
                                if (Convert.ToString(cbbNacPais.get_Column(tempRefParam37, tempRefParam38)).Trim().ToUpper() == Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gpaisnac"]).Trim().ToUpper())
                                {
                                    i = Convert.ToInt32(tempRefParam38);
                                    cbbNacPais.set_ListIndex(i);
                                    break;
                                }
                                else
                                {
                                    i = Convert.ToInt32(tempRefParam38);
                                }
                            }

                            //           cbbNacPais.Column(1, cbbNacPais.ListIndex) = Trim(RrSQLUltimo!gpaisnac)
                        }

                        //busca la entidad financiadora
                        if (Convert.ToDouble(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gsocieda"]) == MFiliacion.CodSS)
                        {
                            MFiliacion.ENTIDAD = MFiliacion.TexSS;
                        }

                        if (Convert.ToDouble(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gsocieda"]) == MFiliacion.CodPriv)
                        {
                            MFiliacion.ENTIDAD = MFiliacion.TexPriv;
                        }




                        if (Convert.ToDouble(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gsocieda"]) != MFiliacion.CodSS && Convert.ToDouble(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gsocieda"]) != MFiliacion.CodPriv)
                        {
                            //busca la sociedad
                            //Set RrDSOCIEDA = GConexion.OpenResultset("select dsocieda from DSOCIEDAVA where gsocieda = " & RrSQLUltimo("gsocieda") & "", rdOpenKeyset)
                            SqlDataAdapter tempAdapter_30 = new SqlDataAdapter("select dsocieda from DSOCIEDAVA Where FDESACTI Is Null and gsocieda = " + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gsocieda"]) + "", MFiliacion.GConexion);
                            RrDSOCIEDA = new DataSet();
                            tempAdapter_30.Fill(RrDSOCIEDA);
                            if (RrDSOCIEDA.Tables[0].Rows.Count != 0)
                            {
                                MFiliacion.ENTIDAD = Convert.ToString(RrDSOCIEDA.Tables[0].Rows[0]["dsocieda"]).Trim();
                            }
                            RrDSOCIEDA.Close();
                        }
                        //busca la historia
                        SqlDataAdapter tempAdapter_31 = new SqlDataAdapter("select ghistoria from hdossier where gidenpac = '" + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gidenpac"]) + "'", MFiliacion.GConexion);
                        RrDSOCIEDA = new DataSet();
                        tempAdapter_31.Fill(RrDSOCIEDA);
                        if (RrDSOCIEDA.Tables[0].Rows.Count != 0)
                        {
                            lbDPHistoria.Text = Convert.ToString(RrDSOCIEDA.Tables[0].Rows[0]["ghistoria"]).Trim();
                        }
                        else
                        {
                            lbDPHistoria.Text = "";
                        }
                        RrDSOCIEDA.Close();

                        if (MFiliacion.stFiliacion == "MODIFICACION")
                        {
                            lbDPEFinanciadora.Text = Strings.StrConv(MFiliacion.ENTIDAD, VbStrConv.ProperCase, 0);
                        }
                        if (!Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["nafiliac"]))
                        {
                            if (MFiliacion.stFiliacion == "MODIFICACION")
                            {
                                lbDPNAsegurado.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["nafiliac"]).Trim();
                            }
                            //        lbFCNAsegurado.Caption = Trim(RrSQLUltimo("nafiliac"))
                        }
                        //        If stFiliacion = "MODIFICACION" Then lbFCEntidad.Caption = StrConv(ENTIDAD, vbProperCase)


                    }
                }
                else
                {

                    if (MFiliacion.stFiliacion == "ALTA" || MFiliacion.stFiliacion == "MODIFICACION")
                    {

                        tbApellido1.Text = MFiliacion.NuevoDFI120F1.tbApellido1.Text.Trim();
                        tbApellido2.Text = MFiliacion.NuevoDFI120F1.tbApellido2.Text.Trim();
                        tbNombre.Text = MFiliacion.NuevoDFI120F1.tbNombre.Text.Trim();
                        sdcFechaNac.Value = DateTime.Parse(MFiliacion.NuevoDFI120F1.sdcFechaNac.Text + "");

                        //TARJETA SANITARIA
                        if (MFiliacion.NuevoDFI120F1.tbTarjeta.Text.Trim() == "")
                        {
                            tbTarjeta.Text = "         ";
                        }
                        else
                        {
                            tbTarjeta.Text = MFiliacion.NuevoDFI120F1.tbTarjeta.Text;
                        }

                        // CIPA
                        if (MFiliacion.NuevoDFI120F1.tbCIPAutonomico.Text.Trim() == "")
                        {
                            lbCipAutonomico.Text = "";
                        }
                        else
                        {
                            lbCipAutonomico.Text = MFiliacion.NuevoDFI120F1.tbCIPAutonomico.Text;
                        }

                        if (MFiliacion.NuevoDFI120F1.rbSexo[0].IsChecked)
                        {
                            rbSexo[0].IsChecked = true;
                            rbSexo[1].IsChecked = false;
                            if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) == "S")
                            {
                                rbSexo[2].Enabled = false;
                                rbSexo[2].Visible = false;
                            }
                            else
                            {
                                rbSexo[2].IsChecked = false;
                            }
                        }
                        else
                        {
                            if (MFiliacion.NuevoDFI120F1.rbSexo[1].IsChecked)
                            {
                                rbSexo[0].IsChecked = false;
                                rbSexo[1].IsChecked = true;
                                if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) == "S")
                                {
                                    rbSexo[2].Enabled = false;
                                    rbSexo[2].Visible = false;
                                }
                                else
                                {
                                    rbSexo[2].IsChecked = false;
                                }
                            }
                            else
                            {
                                //O.Frias -- 15/11/2010
                                //Seg�n la constante se opera con Indeterminado o no
                                if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) == "S")
                                {
                                    rbSexo[2].Enabled = false;
                                    rbSexo[2].Visible = false;
                                }
                                else
                                {
                                    rbSexo[0].IsChecked = false;
                                    rbSexo[1].IsChecked = false;
                                    rbSexo[2].IsChecked = true;
                                }

                            }
                        }

                        if (MFiliacion.NuevoDFI120F1.tbsocieda.Text != "")
                        {
                            if (Conversion.Str(MFiliacion.NuevoDFI120F1.tbsocieda.Text) == Conversion.Str(MFiliacion.CodSS))
                            {
                                MFiliacion.ENTIDAD = MFiliacion.TexSS;
                                this.tbRENSS.Text = MFiliacion.NuevoDFI120F1.tbNSS.Text;
                                rbRERegimen[0].IsChecked = true;
                                this.tbTarjeta.Text = MFiliacion.NuevoDFI120F1.tbTarjeta.Text;
                            }



                            if (Conversion.Str(MFiliacion.NuevoDFI120F1.tbsocieda.Text) != Conversion.Str(MFiliacion.CodPriv) && Conversion.Str(MFiliacion.NuevoDFI120F1.tbsocieda.Text) != Conversion.Str(MFiliacion.CodSS))
                            {
                                //Si est� visible el combo de obra social, lo lle
                                if (Convert.ToString(lblObraSocial.Tag) != "")
                                {


                                    //Cargamos los terceros econ�micos.
                                    SqlDataAdapter tempAdapter_32 = new SqlDataAdapter("select b07_denom,B07_eterc,B07_rterc from FETERB07 a where exists ( " +
                                                                    "select '' from ifmsfa_dsocieda b where FDESACTI is null and ginspecc = '0' and a.B07_rterc = b.rterc and " +
                                                                    "a.B07_eterc = b.eterc group by b.rterc,b.eterc) order by b07_denom ", MFiliacion.GConexion);
                                    RrDSOCIEDA = new DataSet();
                                    tempAdapter_32.Fill(RrDSOCIEDA);
                                    CbbObraSocial.Items.Clear();
                                    CbbObraSocial.Items.Add(new RadListDataItem(("")));
                                    RrDSOCIEDA.MoveFirst();
                                    foreach (DataRow iteration_row_14 in RrDSOCIEDA.Tables[0].Rows)
                                    {
                                        CbbObraSocial.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row_14["b07_denom"]).Trim() + new String(' ', 500) + Convert.ToString(iteration_row_14["B07_rterc"]).Trim() + Convert.ToString(iteration_row_14["B07_eterc"]).Trim(), VbStrConv.ProperCase, 0), CbbObraSocial.Items.Count - 1));
                                    }
                                    RrDSOCIEDA.Close();
                                }

                                for (i = 0; i <= cbbRESociedad.Items.Count - 1; i++)
                                {
                                    if (Conversion.Str(cbbRESociedad.Items[i].Value) == Conversion.Str(MFiliacion.NuevoDFI120F1.tbsocieda.Text))
                                    {
                                        cbbRESociedad.SelectedIndex = i;
                                        break;
                                    }
                                }

                                // Antes de volcar la p�liza, comprobamos si cumple con la m�scara establecida

                                if (proCoincideMascara(MFiliacion.NuevoDFI120F1.tbNSS.Text))
                                {
                                    this.tbRENPoliza.Text = MFiliacion.NuevoDFI120F1.tbNSS.Text;
                                }
                                else
                                {
                                    if (MFiliacion.stFiliacion == "ALTA")
                                    {
                                        //MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
                                        this.tbRENPoliza.Text = MascaraVacia();
                                    }
                                    else
                                    {
                                        //MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
                                        bCambiandoMascara = true;
                                        this.tbRENPoliza.Mask = "";
                                        bCambiandoMascara = false;
                                        this.tbRENPoliza.BackColor = Color.FromArgb(255, 128, 128);
                                        this.tbRENPoliza.Text = MFiliacion.NuevoDFI120F1.tbNSS.Text;
                                    }
                                }

                                //Me.tbRENPoliza = NuevoDFI120F1!tbNSS.Text
                                this.tbRENVersion.Text = MFiliacion.NuevoDFI120F1.TbVersion.Text;
                                this.tbTarjeta.Text = MFiliacion.NuevoDFI120F1.tbTarjeta.Text;
                                rbRERegimen[1].IsChecked = true;


                                //�APA PARA INTENTAR RECOGER DE LA TARJETA EL NUMERO DE TARJETA Y VERSION CUANDO
                                //ESTAS NO COINCIDA CON LEIDA DEL LECTOR
                                if (MFiliacion.NuevoDFI120F1.stNumeroLeidoTarjeta.Trim() != "")
                                {
                                    if (MFiliacion.NuevoDFI120F1.stNumeroLeidoTarjeta.Trim() != MFiliacion.NuevoDFI120F1.tbTarjeta.Text.Trim())
                                    {
                                        this.tbTarjeta.Text = MFiliacion.NuevoDFI120F1.stNumeroLeidoTarjeta.Trim();
                                    }
                                }

                                if (MFiliacion.NuevoDFI120F1.stVersionLeidaTarjeta.Trim() != "")
                                {
                                    if (MFiliacion.NuevoDFI120F1.stVersionLeidaTarjeta.Trim() != MFiliacion.NuevoDFI120F1.TbVersion.Text.Trim())
                                    {
                                        this.tbRENVersion.Text = MFiliacion.NuevoDFI120F1.stVersionLeidaTarjeta.Trim();
                                    }
                                }
                                //------

                            }

                            if (Conversion.Str(MFiliacion.NuevoDFI120F1.tbsocieda.Text) == Conversion.Str(MFiliacion.CodPriv))
                            {
                                rbRERegimen[1].IsChecked = true;
                                this.tbTarjeta.Text = MFiliacion.NuevoDFI120F1.tbTarjeta.Text;
                            }
                        }
                        //(maplaza)(03/05/2006)
                        if (MFiliacion.NuevoDFI120F1.MEBDNI.Text.Trim() == "" && MFiliacion.NuevoDFI120F1.tbOtrosDocumentos.Text.Trim() == "")
                        {
                            //(maplaza)(03/05/2006)Si el paciente es nuevo, aparecer� marcado por defecto DNI/NIF
                            if (MFiliacion.stFiliacion == "ALTA")
                            {
                                rbDNIOtroDocDP[0].IsChecked = true;
                                rbDNIOtroDocDP[1].IsChecked = false;
                                tbNIFOtrosDocDP.Enabled = true;
                                //(maplaza)(03/05/2006)
                                if (tbNIFOtrosDocDP.Mask == MFiliacion.stFORMATO_NIF)
                                {
                                    tbNIFOtrosDocDP.Text = "         ";
                                }
                                else
                                {
                                    tbNIFOtrosDocDP.Text = "";
                                }
                                //(17/05/2006)En la tercera pesta�a, cuando se da de alta, tambi�n se debe poner por defecto
                                //marcado por defecto el campo "DNI/NIF"
                                rbDNIOtroDocPC[0].IsChecked = true;
                                rbDNIOtroDocPC[1].IsChecked = false;
                                tbNIFOtrosDocPC.Enabled = true;
                                if (tbNIFOtrosDocPC.Mask == MFiliacion.stFORMATO_NIF)
                                {
                                    tbNIFOtrosDocPC.Text = "         ";
                                }
                                else
                                {
                                    tbNIFOtrosDocPC.Text = "";
                                }
                                //-----
                            }
                            else
                            {
                                rbDNIOtroDocDP[0].IsChecked = false;
                                rbDNIOtroDocDP[1].IsChecked = false;
                                tbNIFOtrosDocDP.Enabled = false;
                                if (tbNIFOtrosDocDP.Mask == MFiliacion.stFORMATO_NIF)
                                {
                                    tbNIFOtrosDocDP.Text = "         ";
                                }
                                else
                                {
                                    tbNIFOtrosDocDP.Text = "";
                                }
                            }
                        }
                        else
                        {
                            //Trim(NuevoDFI120F1!MEBDNI.Text) <> "" Or Trim(NuevoDFI120F1!tbOtrosDocumentos.Text) <> ""
                            if (MFiliacion.NuevoDFI120F1.MEBDNI.Text.Trim() != "")
                            {
                                rbDNIOtroDocDP[0].IsChecked = true;
                                rbDNIOtroDocDP[1].IsChecked = false;
                                tbNIFOtrosDocDP.Enabled = true;
                                //(maplaza)(16/05/2006)
                                if (MFiliacion.NuevoDFI120F1.MEBDNI.Text.Trim().Length == 9)
                                {
                                    tbNIFOtrosDocDP.Text = MFiliacion.NuevoDFI120F1.MEBDNI.Text.Trim();
                                }
                                else
                                {
                                    tbNIFOtrosDocDP.Text = StringsHelper.Format(MFiliacion.NuevoDFI120F1.MEBDNI.Text.Trim(), "00000000") + " ";
                                }
                                //-----
                            }
                            else
                            {
                                //(Trim(NuevoDFI120F1!tbOtrosDocumentos.Text) <> "")
                                rbDNIOtroDocDP[0].IsChecked = false;
                                rbDNIOtroDocDP[1].IsChecked = true;
                                tbNIFOtrosDocDP.Enabled = true;
                                tbNIFOtrosDocDP.Text = MFiliacion.NuevoDFI120F1.tbOtrosDocumentos.Text.Trim();
                            }
                        }

                        if (MFiliacion.stFiliacion == "MODIFICACION")
                        {
                            lbDPEFinanciadora.Text = Strings.StrConv(MFiliacion.ENTIDAD, VbStrConv.ProperCase, 0);
                        }
                        if (MFiliacion.stFiliacion == "MODIFICACION")
                        {
                            lbDPNAsegurado.Text = MFiliacion.NuevoDFI120F1.tbNSS.Text.Trim();
                        }

                    }

                }
                if (MFiliacion.stFiliacion == "MODIFICACION")
                {
                    SqlDataAdapter tempAdapter_33 = new SqlDataAdapter("select ghistoria from hdossier where gidenpac = '" + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["gidenpac"]) + "' AND icontenido = 'H' AND gserpropiet = " + MFiliacion.ArchivoCentral.ToString(), MFiliacion.GConexion);
                    Rrsqlhist = new DataSet();
                    tempAdapter_33.Fill(Rrsqlhist);
                    if (Rrsqlhist.Tables[0].Rows.Count != 0)
                    {
                        lbDPHistoria.Text = Convert.ToString(Rrsqlhist.Tables[0].Rows[0]["ghistoria"]).Trim();
                    }
                    Rrsqlhist.Close(); //SI ES UNA MODIFICACION CARGA LOS DATOS QUE CONTENGA EN LAS TABLAS
                }


                //carga canales de preferencia
                //cbbCanPrev.Clear();
                cbbCanPrev.DataSource = null;

                SqlDataAdapter tempAdapter_34 = new SqlDataAdapter("select gcanotic,dcanotic,isnull(icorrele,'N') icorrele,isnull(ieviosms,'N') ieviosms,isnull(icpostal,'N') icpostal,isnull(itelefon,'N') itelefon from DCANOTIC where fborrado is null and isnull(ivisible,'N')='S' order by dcanotic", MFiliacion.GConexion);
                RrCatSoc = new DataSet();
                tempAdapter_34.Fill(RrCatSoc);

                DataTable dt2 = new DataTable();
                dt2.Columns.Add("dcanotic");
                dt2.Columns.Add("gcanotic");
                dt2.Columns.Add("icorrele");
                dt2.Columns.Add("ieviosms");
                dt2.Columns.Add("icpostal");
                dt2.Columns.Add("itelefon");

                foreach (DataRow iteration_row_15 in RrCatSoc.Tables[0].Rows)
                {
                    string Param1 = Strings.StrConv(Convert.ToString(iteration_row_15["dcanotic"]).Trim(), VbStrConv.ProperCase, 0);
                    string Param2 = Convert.ToString(iteration_row_15["gcanotic"]).Trim();
                    string Param3 = Convert.ToString(iteration_row_15["icorrele"]).Trim();
                    string Param4 = Convert.ToString(iteration_row_15["ieviosms"]).Trim();
                    string Param5 = Convert.ToString(iteration_row_15["icpostal"]).Trim();
                    string Param6 = Convert.ToString(iteration_row_15["itelefon"]).Trim();
                    dt2.Rows.Add(Param1, Param2, Param3, Param4, Param5, Param6);
                }

                cbbCanPrev.DataSource = dt2;
                cbbCanPrev.DisplayMember = "dcanotic";
                cbbCanPrev.ValueMember = "gcanotic";
                cbbCanPrev.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
                cbbCanPrev.EditorControl.ShowHeaderCellButtons = false;
                cbbCanPrev.EditorControl.ShowRowHeaderColumn = false;
                cbbCanPrev.EditorControl.ShowColumnHeaders = false;
                ((RadGridView)cbbCanPrev.EditorControl).Columns["gcanotic"].IsVisible = false;
                ((RadGridView)cbbCanPrev.EditorControl).Columns["icorrele"].IsVisible = false;
                ((RadGridView)cbbCanPrev.EditorControl).Columns["ieviosms"].IsVisible = false;
                ((RadGridView)cbbCanPrev.EditorControl).Columns["icpostal"].IsVisible = false;
                ((RadGridView)cbbCanPrev.EditorControl).Columns["itelefon"].IsVisible = false;
                ((RadGridView)cbbCanPrev.EditorControl).TableElement.RowHeight = 20;
                ((RadGridView)cbbCanPrev.EditorControl).GridViewElement.CellFormatting += cbbCanPrev_CellFormatting;


                /*foreach (DataRow iteration_row_15 in RrCatSoc.Tables[0].Rows)
			    {
				    object tempRefParam39 = Strings.StrConv(Convert.ToString(iteration_row_15["dcanotic"]).Trim(), VbStrConv.ProperCase, 0);
				    object tempRefParam40 = RrCatSoc.getAbsolutePosition() - 1;
				    cbbCanPrev.AddItem(ref tempRefParam39, ref tempRefParam40);
				    cbbCanPrev.set_List(RrCatSoc.getAbsolutePosition() - 1, 1, Convert.ToString(iteration_row_15["gcanotic"]).Trim());
				    cbbCanPrev.set_List(RrCatSoc.getAbsolutePosition() - 1, 2, Convert.ToString(iteration_row_15["icorrele"]).Trim());
				    cbbCanPrev.set_List(RrCatSoc.getAbsolutePosition() - 1, 3, Convert.ToString(iteration_row_15["ieviosms"]).Trim());
				    cbbCanPrev.set_List(RrCatSoc.getAbsolutePosition() - 1, 4, Convert.ToString(iteration_row_15["icpostal"]).Trim());
				    cbbCanPrev.set_List(RrCatSoc.getAbsolutePosition() - 1, 5, Convert.ToString(iteration_row_15["itelefon"]).Trim());
			    }*/

                RrCatSoc.Close();
                cbbCanPrev.SelectedIndex = -1;

                string SQLPACIENT = String.Empty;
                if (MFiliacion.stFiliacion == "MODIFICACION")
                {
                    //BUSCA EL PACIENTE QUE SE TRAE DE LA PANTALLA ANTERIOR
                    SQLPACIENT = "SELECT * FROM DPACIENT WHERE GIDENPAC = '" + MFiliacion.ModCodigo + "'";
                    SqlDataAdapter tempAdapter_35 = new SqlDataAdapter(SQLPACIENT, MFiliacion.GConexion);
                    MFiliacion.RrDPACIENT = new DataSet();
                    tempAdapter_35.Fill(MFiliacion.RrDPACIENT);
                    tempAdapterPaciente = tempAdapter_35;
                    CARGAR_DATOSTABLAS();
                }
                else
                {
                    if (MFiliacion.stFiliacion == "ALTA")
                    {
                        SqlDataAdapter tempAdapter_36 = new SqlDataAdapter("SELECT * FROM DPACIENT WHERE 1 = 2", MFiliacion.GConexion);
                        MFiliacion.RrDPACIENT = new DataSet();
                        tempAdapter_36.Fill(MFiliacion.RrDPACIENT);
                        tempAdapterPaciente = tempAdapter_36;
                    }
                }
                MFiliacion.sUsuario = Serrores.VVstUsuarioApli;
            }
            catch (SqlException ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
		}

        private void CbbREPago_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            cbbREPago_Click(sender, e);
        }

        private void cbbREPago_CellFormatting(Object eventSender, CellFormattingEventArgs e)
        {
            e.CellElement.BorderWidth = 0;
            e.CellElement.DrawBorder = false;            
        }

        private void cbbCanPrev_CellFormatting(Object eventSender, CellFormattingEventArgs e)
        {
            e.CellElement.BorderWidth = 0;
            e.CellElement.DrawBorder = false;
        }


        private void Cargar_PobDefecto()
		{
			string SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + MFiliacion.CodPROV + "%' order by Dpoblaci";
			CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbDPPoblacion, true);
			CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbNacPoblacion, true);
		}

		private void tbNIFOtrosDocDP_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			bool paso = false;

			//(maplaza)si se trata de la opci�n "Otros documentos", no se realiza ninguna validaci�n de formato
			if (rbDNIOtroDocDP[1].IsChecked)
			{
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}

			if (tbNIFOtrosDocDP.Text.Trim().Length == 9)
			{
				tbNIFOtrosDocDP.Focus();
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123))
			{
				if (tbNIFOtrosDocDP.Text.Trim().Length == 0)
				{
					paso = true;
				}
				else
				{
					paso = Conversion.Val(tbNIFOtrosDocDP.Text) == 0;
				}
			}
			int Tama�o = 0;
			int ceros = 0;
			string resto = String.Empty;
			StringBuilder resto2 = new StringBuilder();
			if (!paso)
			{
				if (KeyAscii != 8 && ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123)))
				{
					if (KeyAscii < 48 || KeyAscii > 57)
					{
						if (tbNIFOtrosDocDP.Text.Trim().Length == 9)
						{
							tbNIFOtrosDocDP.Text = tbNIFOtrosDocDP.Text.Substring(0, Math.Min(8, tbNIFOtrosDocDP.Text.Length)) + Strings.Chr(KeyAscii).ToString().ToUpper();
						}
						Tama�o = tbNIFOtrosDocDP.Text.Trim().Length;
						resto = tbNIFOtrosDocDP.Text.Trim();
						ceros = 8 - Tama�o;
						tbNIFOtrosDocDP.Text = "         ";
						for (int i = 1; i <= ceros; i++)
						{
							resto2.Append("0");
						}
						resto2.Append(resto);
						if (resto2.ToString().Trim().Length < 9)
						{
							resto2.Append(Strings.Chr(KeyAscii).ToString().ToUpper());
						}
						tbNIFOtrosDocDP.Text = "         ";
						tbNIFOtrosDocDP.Text = resto2.ToString();
					}
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		public void tbNIFOtrosDocDP_Leave(Object eventSender, EventArgs eventArgs)
		{
			//(maplaza)si se trata de la opci�n "Otros documentos", no se realiza ninguna validaci�n de formato
			if (rbDNIOtroDocDP[1].IsChecked)
			{
				return;
			}

			int Tama�o = 0;
			int ceros = 0;
			string resto = String.Empty;
			StringBuilder resto2 = new StringBuilder();
			if (tbNIFOtrosDocDP.Text.Trim().Length < 8 && tbNIFOtrosDocDP.Text.Trim().Length != 0)
			{
				Tama�o = tbNIFOtrosDocDP.Text.Trim().Length;
				resto = tbNIFOtrosDocDP.Text.Trim();
				ceros = 8 - Tama�o;
				tbNIFOtrosDocDP.Text = "         ";
				for (int i = 1; i <= ceros; i++)
				{
					resto2.Append("0");
				}
				resto2.Append(resto);
				tbNIFOtrosDocDP.Text = "         ";
				tbNIFOtrosDocDP.Text = resto2.ToString() + " ";
			}

			tbNIFOtrosDocDP.Text = tbNIFOtrosDocDP.Text.ToUpper();

			//si se ha quedado un formato incorrecto de DNI o NIF, se tendr� que deshabilitar el bot�n de "Aceptar"
			COMPROBAR_DATOS();

		}

		private void tbNIFOtrosDocRE_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			bool paso = false;

			//(maplaza)si se trata de la opci�n "Otros documentos", no se realiza ninguna validaci�n de formato
			if (rbDNIOtroDocRE[1].IsChecked)
			{
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}

			if (tbNIFOtrosDocRE.Text.Trim().Length == 9)
			{
				tbNIFOtrosDocRE.Focus();
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123))
			{
				if (tbNIFOtrosDocRE.Text.Trim().Length == 0)
				{
					paso = true;
				}
				else
				{
					paso = Conversion.Val(tbNIFOtrosDocRE.Text) == 0;
				}
			}
			int Tama�o = 0;
			int ceros = 0;
			string resto = String.Empty;
			StringBuilder resto2 = new StringBuilder();
			if (!paso)
			{
				if (KeyAscii != 8 && ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123)))
				{
					if (KeyAscii < 48 || KeyAscii > 57)
					{

						if (tbNIFOtrosDocRE.Text.Trim().Length == 9)
						{
							tbNIFOtrosDocRE.Text = tbNIFOtrosDocRE.Text.Substring(0, Math.Min(8, tbNIFOtrosDocRE.Text.Length)) + Strings.Chr(KeyAscii).ToString().ToUpper();
						}
						Tama�o = tbNIFOtrosDocRE.Text.Trim().Length;
						resto = tbNIFOtrosDocRE.Text.Trim();
						ceros = 8 - Tama�o;
						tbNIFOtrosDocRE.Text = "         ";
						for (int i = 1; i <= ceros; i++)
						{
							resto2.Append("0");
						}
						resto2.Append(resto);
						if (resto2.ToString().Trim().Length < 9)
						{
							resto2.Append(Strings.Chr(KeyAscii).ToString().ToUpper());
						}
						tbNIFOtrosDocRE.Text = "         ";
						tbNIFOtrosDocRE.Text = resto2.ToString();

					}
				}
				tbNIFOtrosDocRE.Focus();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNIFOtrosDocRE_Leave(Object eventSender, EventArgs eventArgs)
		{
			string SQLPOBLACION = String.Empty;
			DataSet RrSQLPOBLACION = null;
			string CODIGOPROVINCIA = String.Empty;

			//(maplaza)(03/05/2006)si se trata de la opci�n "Otros documentos", no se realiza ninguna validaci�n de formato
			//If rbDNIOtroDocRE(1).Value = True Then Exit Sub

			int Tama�o = 0;
			int ceros = 0;
			string resto = String.Empty;
			StringBuilder resto2 = new StringBuilder();
			string SQLPRI = String.Empty;
			string CODNIF = String.Empty;
			int Resp = 0;
			string SQLPAIS = String.Empty;
			DataSet RrSQLPAIS = null;
			if (tbNIFOtrosDocRE.Text.Trim() != "")
			{
				//(maplaza)(17/05/2006)si se trata de la opci�n "Otros documentos", no se realiza ninguna validaci�n de formato
				if (!rbDNIOtroDocRE[1].IsChecked)
				{
					if (tbNIFOtrosDocRE.Text.Trim().Length < 8 && tbNIFOtrosDocRE.Text.Trim().Length != 0)
					{
						Tama�o = tbNIFOtrosDocRE.Text.Trim().Length;
						resto = tbNIFOtrosDocRE.Text.Trim();
						ceros = 8 - Tama�o;
						tbNIFOtrosDocRE.Text = "         ";
						for (int i = 1; i <= ceros; i++)
						{
							resto2.Append("0");
						}
						resto2.Append(resto);
						tbNIFOtrosDocRE.Text = "         ";
						tbNIFOtrosDocRE.Text = resto2.ToString() + " ";
					}
				}

				if (rbRERegimen[2].IsChecked)
				{
					if (tbNIFOtrosDocRE.Text.Trim() == "")
					{ //SI NO INTRODUCE EL NIF
						DatosCorrectos = false;
					}
					else
					{
						//SI LO INTRODUCE, BUSCA SI ESTA EN LA TABLA DE PRIVADO
						CODNIF = tbNIFOtrosDocRE.Text.Trim();
						SQLPRI = "SELECT * FROM DPRIVADO WHERE NNIFPRIV = '" + CODNIF + "'";
						SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLPRI, MFiliacion.GConexion);
						RrDPRIVADO = new DataSet();
						tempAdapter.Fill(RrDPRIVADO);
                        tempAdapterRrDPRIVADO = tempAdapter;

                        if (RrDPRIVADO.Tables[0].Rows.Count != 0)
						{ //SI ESTA EN LA TABLA


							//OSCAR C 10/03/05
							//Ahora en lugar de recuperar los datos del pagador privado directamente,
							//pregunta si se desea recuperar dicha informaci�n.

							//(maplaza)(17/05/2006)Se cambia el literal de la pregunta, antes aparec�a el literal "DNI N�", y ahora se sustituye
							//por el literal "Documento Identificativo"
							Resp = (int) RadMessageBox.Show("Se ha encontrado la siguiente informaci�n para el Documento Identificativo " + CODNIF + ":" + Environment.NewLine + 
							       "Nombre: " + Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DNOMBPRI"]) + Environment.NewLine + 
							       "Apellidos: " + Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DAPE1PRI"]) + " " + ((Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"])) ? "" : Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"])) + Environment.NewLine + 
							       " � Desea Recuperar dicho pagador privado ? ", "Confirmaci�n Datos del Pagador Privado", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button1);
							//-----
							if (Resp == ((int) System.Windows.Forms.DialogResult.Yes))
							{
								EncontradoPrivado = true;
								//TRAE LOS DATOS DEL PAGADOR PRIVADO
								if (Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim() != "")
								{
									if (tbNIFOtrosDocRE.Mask == MFiliacion.stFORMATO_NIF)
									{
										if (Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim().Length == 9)
										{
											tbNIFOtrosDocRE.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim();
										}
										else
										{
											tbNIFOtrosDocRE.Text = StringsHelper.Format(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"], "00000000") + " ";
										}
									}
									else
									{
										tbNIFOtrosDocRE.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim();
									}
									//tbNIFOtrosDocRE.Text = RrDPRIVADO("NNIFPRIV")
								}
								else
								{
									if (tbNIFOtrosDocRE.Mask == MFiliacion.stFORMATO_NIF)
									{
										tbNIFOtrosDocRE.Text = "         ";
									}
									else
									{
										tbNIFOtrosDocRE.Text = "";
									}
								}
								//tbNIFOtrosDocRE.Text = RrDPRIVADO("NNIFPRIV")
								tbRENombre.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DNOMBPRI"]);
								tbREApe1.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DAPE1PRI"]);
								tbREApe2.Text = (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"])) ? "" : Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"]);
								//pasaporte
								if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["NPASAPOR"]))
								{
									tbREPasaporte.Text = "";
								}
								else
								{
									tbREPasaporte.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NPASAPOR"]);
								}
								//---
								if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["DDIREPRI"]))
								{
									tbREDomicilio.Text = "";
								}
								else
								{
									tbREDomicilio.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DDIREPRI"]);
								}
								if (bFormatoCodigoPos)
								{
									mebRECodPostal.Text = (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"])) ? "     " : StringsHelper.Format(Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"]).Trim(), "00000");
								}
								else
								{
									mebRECodPostal.Text = (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"])) ? "" : (Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"]) + "").Trim();
								}
								if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"]))
								{
									//si es nacional
									if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"]))
									{
										//BUSCA LA POBLACION
										SQLPOBLACION = "SELECT DPOBLACI FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["gpoblaci"]) + "' order by Dpoblaci";
										SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLPOBLACION, MFiliacion.GConexion);
										RrSQLPOBLACION = new DataSet();
										tempAdapter_2.Fill(RrSQLPOBLACION);
										if (RrSQLPOBLACION.Tables[0].Rows.Count != 0)
										{
											//INTRODUCE EL NOMBRE DE LA POBLACION
											//cbbREPoblacion.Text = RrSQLPOBLACION("DPOBLACI")
										}
										RrSQLPOBLACION.Close();
										//si tiene poblacion
										if (!Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"]).StartsWith(MFiliacion.CodPROV))
										{
											//si no es la del defecto
											cbbREPoblacion.Clear();
											CODIGOPROVINCIA = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"]);
											CODIGOPROVINCIA = CODIGOPROVINCIA.Substring(0, Math.Min(2, CODIGOPROVINCIA.Length));
											SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + CODIGOPROVINCIA + "%' order by Dpoblaci";
											CargarComboAlfanumerico(SQLPOBLACION, "gpoblaci", "dpoblaci", cbbREPoblacion);
										}
										for (int i = 0; i <= cbbREPoblacion.ListCount - 1; i++)
										{
											object tempRefParam = 1;
											object tempRefParam2 = i;
											if (cbbREPoblacion.get_Column(tempRefParam, tempRefParam2) == RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"])
											{
												i = Convert.ToInt32(tempRefParam2);
												cbbREPoblacion.set_ListIndex(i);
												break;
											}
											else
											{
												i = Convert.ToInt32(tempRefParam2);
											}
										}
									}
								}
								else
								{
									chbREExtrangero.CheckState = CheckState.Checked;
									mebRECodPostal.Enabled = false;
									cbbDPPoblacion.Enabled = false;
									cbbNacPoblacion.Enabled = false;
									cbbREPoblacion.Enabled = false;
									cbBuscar2.Enabled = false;
									cbDBuscar4.Enabled = false;
									//BUSCA EL PAIS
									SQLPAIS = "SELECT DPAISRES FROM Dpaisresva WHERE GPAISRES = '" + Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"]) + "'";
									SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLPAIS, MFiliacion.GConexion);
									RrSQLPAIS = new DataSet();
									tempAdapter_3.Fill(RrSQLPAIS);
									cbbREPais.Text = Convert.ToString(RrSQLPAIS.Tables[0].Rows[0]["DPAISRES"]);
									RrSQLPAIS.Close();
									if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["DPOBLACI"]))
									{
										tbREExPob.Text = "";
									}
									else
									{
										tbREExPob.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DPOBLACI"]);
									}
									//(maplaza)(10/08/2007)
									if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["ddireext"]))
									{
										tbREDireccionEx.Text = "";
									}
									else
									{
										tbREDireccionEx.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["ddireext"]);
									}
									//------
								}
							}
							else
							{
								//Contesta que no a la pregunta
								EncontradoPrivado = false;
								tbNIFOtrosDocRE.Text = miDNINIF;
							}
						}
						else
						{
							// NO ECUENTRA AL PAGADOR PRIVADO
							EncontradoPrivado = false;
						}
						RrDPRIVADO.Close();
					}
				}

				//si se ha quedado un formato incorrecto de DNI o NIF, se tendr� que deshabilitar el bot�n de "Aceptar"
				COMPROBAR_DATOS();
			}
		}

		//(maplaza)(18/04/2007)Esta funci�n dice si el C�digo Postal pasado como argumento a la funci�n, existe en
		//la tabla DCODIPOS.
		private bool fExisteCodigoPostal(string stCP)
		{
			bool result = false;
			string StSql = String.Empty;
			DataSet RrSql = null;

			try
			{
				this.Cursor = Cursors.WaitCursor;

				StSql = "Select gcodipos From DCODIPOS Where gcodipos = '" + stCP + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
					result = true;
				}

				RrSql.Close();

				this.Cursor = Cursors.Default;

				return result;
			}
			catch
			{
				this.Cursor = Cursors.Default;
				return result;
			}
		}

		private void mebRECodPostal_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (bNocambiarCP_RE)
			{
				return;
			} //(maplaza)(31/05/2006)

			//(maplaza)(30/05/2006)

			if (bFormatoCodigoPos)
			{
				if (mebRECodPostal.Text.Trim().Length == 5)
				{
					//(maplaza)(18/04/2007)
					//Buscar_poblaciones2
					//si se introduce un C�digo Postal que existe en la tabla DCODIPOS
					if (fExisteCodigoPostal(mebRECodPostal.Text.Trim()))
					{
						//si no est� introducida la poblaci�n, cargar el combo con las poblaciones relacionadas con el c�digo postal.
						if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) == -1)
						{
							Buscar_poblaciones2();
						}
					}
					else
					{
						//(maplaza)(23/04/2007)
						//si no existe el c�digo postal en la tabla DCODIPOS, pero los dos primeros d�gitos del C�digo Postal se
						//corresponden con una provincia (tabla DPROVINC) y no est� seleccionada ninguna poblaci�n, entonces se
						//cargan las poblaciones correspondientes a esta provincia asociada a los dos primeros d�gitos del C�digo Postal.
						if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) == -1)
						{
							if (fExisteCodigoProvincia(mebRECodPostal.Text.Trim().Substring(0, Math.Min(2, mebRECodPostal.Text.Trim().Length))))
							{
								CargarPoblacionesProvincia(mebRECodPostal.Text.Trim().Substring(0, Math.Min(2, mebRECodPostal.Text.Trim().Length)), cbbREPoblacion);
							}
						}
						//-----
					}
					//-----
					return;
				}
				//-----
			}
			else
			{
				if (mebRECodPostal.Text.Trim().Length > 0)
				{
					if (fExisteCodigoPostal(mebRECodPostal.Text.Trim()))
					{
						//si no est� introducida la poblaci�n, cargar el combo con las poblaciones relacionadas con el c�digo postal.
						if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) == -1)
						{
							Buscar_poblaciones2();
						}
					}
				}
			}

		}

        private void Cargar_PobDefecto3()
		{
			string SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + MFiliacion.CodPROV + "%' order by dpoblaci";
			CargarComboAlfanumerico(SQLPOBLACION, "gpoblaci", "dpoblaci", cbbREPoblacion);
		}

        //private void Cargar_PobDefecto2()
        //{
        //string SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + MFiliacion.CodPROV + "%' order by dpoblaci";
        //CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbREPoblacion, true);
        //}

        private void Buscar_poblaciones3()
		{
			string SQLPOBLACION = String.Empty;
			bool bVaciar = false;
			string stCodPoblacionAnt = String.Empty;

			//busca las poblaciones con ese c�digo postal
			//(maplaza)(24/04/2007)Conviene ordenar por la descripci�n de la poblaci�n
			//SQLCODPOSTAL = "select GPOBLACI from dcodipos where gcodipos = '" & Trim(mebPCCodPostal.Text) & "'"
			string SQLCODPOSTAL = "select dcodipos.GPOBLACI, DPOBLACI from dcodipos, dpoblaci where dcodipos.gpoblaci = dpoblaci.gpoblaci";
			SQLCODPOSTAL = SQLCODPOSTAL + " and gcodipos = '" + mebPCCodPostal.Text.Trim() + "' order by dpoblaci";
			//-----

			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLCODPOSTAL, MFiliacion.GConexion);
			DataSet RrSQLCODPOSTAL = new DataSet();
			tempAdapter.Fill(RrSQLCODPOSTAL);
			//si no encuentra ninguna poblacion
			if (RrSQLCODPOSTAL.Tables[0].Rows.Count == 0)
			{
				//SI NO ENCUENTRA POBLACIONES
				RrSQLCODPOSTAL.Close();
				//(maplaza)(30/05/2006)Si no se han encontrado poblaciones, y hab�a una poblaci�n seleccionada en el combo
				//anteriormente, habr� que desseleccionar esa poblaci�n
				cbbPCPoblacion.Text = "";
				cbbPCPoblacion.set_ListIndex(-1);
			}
			else
			{
				//SI ENCUENTRA LAS POBLACIONES
				if (RrSQLCODPOSTAL.Tables[0].Rows.Count > 1)
				{
					if (bFormatoCodigoPos)
					{
						//(maplaza)(13/04/2007)Puede darse el caso de que en el combo ya se encuentre seleccionada una poblaci�n correcta (es decir, que
						//concuerde con el C�digo Postal que aparece escrito).
						if ((Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) != -1) && (mebPCCodPostal.Text.Trim().Length == 5))
						{
							if ((mebPCCodPostal.Text != "     ") && (Convert.ToString(mebPCCodPostal.Tag) == mebPCCodPostal.Text))
							{
								object tempRefParam = 1;
								object tempRefParam2 = cbbPCPoblacion.get_ListIndex();
								stCodPoblacionAnt = Convert.ToString(cbbPCPoblacion.get_Column(tempRefParam, tempRefParam2));
							}
						}
					}
					else
					{
						if ((Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) != -1) && (mebPCCodPostal.Text.Trim().Length > 0))
						{
							if ((mebPCCodPostal.Text != "") && (Convert.ToString(mebPCCodPostal.Tag) == mebPCCodPostal.Text))
							{
								object tempRefParam3 = 1;
								object tempRefParam4 = cbbPCPoblacion.get_ListIndex();
								stCodPoblacionAnt = Convert.ToString(cbbPCPoblacion.get_Column(tempRefParam3, tempRefParam4));
							}
						}
					}

					//-----
					//SI ENCUENTRA LAS POBLACIONES
					cbbPCPoblacion.Clear();
					bVaciar = true;
					CargarComboAlfanumerico(SQLCODPOSTAL, "gpoblaci", "dpoblaci", cbbPCPoblacion, bVaciar);
					//-----
					RrSQLCODPOSTAL.Close();
					//(maplaza)(13/04/2007)Puede darse el caso de que en el combo ya se encuentre seleccionada una poblaci�n correcta (es decir, que
					//concuerde con el C�digo Postal que aparece escrito). Si es as�, una vez cargados los datos en el combo de poblaciones, habr�a
					//que restaurar esa poblaci�n que estaba seleccionada antes.
					if (stCodPoblacionAnt != "")
					{
						for (int iContador = 0; iContador <= cbbPCPoblacion.ListCount - 1; iContador++)
						{
							object tempRefParam5 = 1;
							object tempRefParam6 = iContador;
							if (Convert.ToString(cbbPCPoblacion.get_Column(tempRefParam5, tempRefParam6)) == stCodPoblacionAnt)
							{
								iContador = Convert.ToInt32(tempRefParam6);
								cbbPCPoblacion.set_ListIndex(iContador);
								break;
							}
							else
							{
								iContador = Convert.ToInt32(tempRefParam6);
							}
						}
					}
					//-----
				}
				else
				{
					//(maplaza)(30/05/2006)Si encuentra las poblaciones, deben quedarse �nicamente las poblaciones encontradas
					//en el combo, y se deben borrar las poblaciones que estaban antes cargadas en el combo.
					cbbPCPoblacion.Clear();
					bVaciar = true;
					//-----
					//SI ENCUENTRA UNA POBLACION
					//BUSCA LA POBLACION
					//(maplaza)(30/05/2006)
					//SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" & CodPROV & "%' order by dpoblaci"
					SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + StringsHelper.Format(Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Substring(0, Math.Min(2, Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Length)), "00") + "%' order by dpoblaci";
					//-----
					CargarComboAlfanumerico(SQLPOBLACION, "gpoblaci", "dpoblaci", cbbPCPoblacion);
					for (int i = 0; i <= cbbPCPoblacion.ListCount - 1; i++)
					{
						object tempRefParam7 = 1;
						object tempRefParam8 = i;
						if (StringsHelper.Format(cbbPCPoblacion.get_Column(tempRefParam7, tempRefParam8), "000000") == Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]))
						{
							i = Convert.ToInt32(tempRefParam8);
							cbbPCPoblacion.set_ListIndex(i);
							break;
						}
						else
						{
							i = Convert.ToInt32(tempRefParam8);
						}
					}
					RrSQLCODPOSTAL.Close();
				}
			}
		}

		private void Buscar_poblaciones2()
		{
			string SQLPOBLACION = String.Empty;
			bool bVaciar = false;

			//busca las poblaciones con ese codigo postal
			//(maplaza)(24/04/2007)Conviene ordenar por la descripci�n de la poblaci�n
			//SQLCODPOSTAL = "select GPOBLACI from dcodipos where gcodipos = '" & Trim(mebRECodPostal.Text) & "'"
			string SQLCODPOSTAL = "select dcodipos.GPOBLACI, DPOBLACI from dcodipos, dpoblaci where dcodipos.gpoblaci = dpoblaci.gpoblaci";
			SQLCODPOSTAL = SQLCODPOSTAL + " and gcodipos = '" + mebRECodPostal.Text.Trim() + "' order by dpoblaci";
			//-----
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLCODPOSTAL, MFiliacion.GConexion);
			DataSet RrSQLCODPOSTAL = new DataSet();
			tempAdapter.Fill(RrSQLCODPOSTAL);

			//si no encuentra ninguna poblacion
			if (RrSQLCODPOSTAL.Tables[0].Rows.Count == 0)
			{
				//SI NO ENCUENTRA POBLACIONES
				RrSQLCODPOSTAL.Close();
				//(maplaza)(30/05/2006)Si no se han encontrado poblaciones, y hab�a una poblaci�n seleccionada en el combo
				//anteriormente, habr� que desseleccionar esa poblaci�n
				cbbREPoblacion.Text = "";
				cbbREPoblacion.set_ListIndex(-1);
			}
			else
			{
				//SI ENCUENTRA LAS POBLACIONES
				if (RrSQLCODPOSTAL.Tables[0].Rows.Count > 1)
				{
					//SI ENCUENTRA LAS POBLACIONES
					cbbREPoblacion.Clear();
					bVaciar = true;

					CargarComboAlfanumerico(SQLCODPOSTAL, "gpoblaci", "dpoblaci", cbbREPoblacion, bVaciar);
					//-----
					RrSQLCODPOSTAL.Close();
				}
				else
				{
					//(maplaza)(30/05/2006)Si encuentra las poblaciones, deben quedarse �nicamente las poblaciones encontradas
					//en el combo, y se deben borrar las poblaciones que estaban antes cargadas en el combo.
					cbbREPoblacion.Clear();
					bVaciar = true;
					//-----
					//SI ENCUENTRA UNA POBLACION
					//BUSCA LA POBLACION
					//(maplaza)(30/05/2006)
					SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + StringsHelper.Format(Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Substring(0, Math.Min(2, Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Length)), "00") + "%' order by dpoblaci";
					//-----
					CargarComboAlfanumerico(SQLPOBLACION, "gpoblaci", "dpoblaci", cbbREPoblacion);
					for (int i = 0; i <= cbbREPoblacion.ListCount - 1; i++)
					{
						object tempRefParam = 1;
						object tempRefParam2 = i;
						if (cbbREPoblacion.get_Column(tempRefParam, tempRefParam2) == RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"])
						{
							i = Convert.ToInt32(tempRefParam2);
							cbbREPoblacion.set_ListIndex(i);
							break;
						}
						else
						{
							i = Convert.ToInt32(tempRefParam2);
						}
					}
					RrSQLCODPOSTAL.Close();
				}
			}
		}

        private void rbIndicador_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				
                if (flagDbClick)
                {
                    int Index = Array.IndexOf(this.rbIndicador, eventSender);
                    switch (Index)
                    {
                        case 0:  //Opci�n TITULAR                             
                            rbIndicador[0].IsChecked = false;
                            _rbIndicador_0.IsChecked = false;
                            break;
                        case 1:  //Opcion BENEFICIARIO 
                            rbIndicador[1].IsChecked = false;
                            _rbIndicador_1.IsChecked = false;
                            break;
                    }
                    flagDbClick = false;
                }
                else
                {
                    int Index = Array.IndexOf(this.rbIndicador, eventSender);
                    switch (Index)
                    {
                        case 0:
                            tbRENomTitular.Text = "";
                            tbRENomTitular.Enabled = false;
                            COMPROBAR_DATOS();
                            break;
                        case 1:
                            tbRENomTitular.Enabled = true;
                            COMPROBAR_DATOS();
                            break;
                    }
                }
			}
		}
		
		private void rbIndicador_DblClick(object sender, System.EventArgs e)
		{
            flagDbClick = true;
            int Index = Array.IndexOf(this.rbIndicador, sender);
            //Regimen Ecomomico
            switch (Index)
			{
				case 0 :  //Opci�n TITULAR 
					rbIndicador[0].IsChecked = false; 
					tbRENomTitular.Text = ""; 
					tbRENomTitular.Enabled = false; 
					rbIndicador[0].IsChecked = false; 
					rbIndicador[1].IsChecked = false; 
					COMPROBAR_DATOS(); 
					break;
				case 1 :  //Opcion BENEFICIARIO 
					rbIndicador[1].IsChecked = false; 
					tbRENomTitular.Text = ""; 
					tbRENomTitular.Enabled = false; 
					rbIndicador[0].IsChecked = false; 
					rbIndicador[1].IsChecked = false; 
					COMPROBAR_DATOS(); 
					break;
			}
		}

		private void rbRERegimen_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
            if (((RadRadioButton)eventSender).IsChecked)
            {
                if (isInitializingComponent)
                {
                    return;
                }
                //CARGA SEGURIDAD SOCIAL

                if (flagDbClick)
                {
                    int Index = Array.IndexOf(this.rbRERegimen, eventSender);
                    //Regimen Ecomomico
                    rbRERegimen[Index].IsChecked = false;
                    
                    flagDbClick = false;
                }
                else
                {
                    string sqlAux = String.Empty;
                    DataSet rrAux = null;
                    if (MFiliacion.stFiliacion == "MODIFICACION")
                    {
                        SQLSOC = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' AND GSOCIEDA = " + MFiliacion.CodSS.ToString() + "";
                        SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLSOC, MFiliacion.GConexion);
                        RrSQLSOC = new DataSet();
                        tempAdapter.Fill(RrSQLSOC);
                        if (RrSQLSOC.Tables[0].Rows.Count != 0)
                        {
                            CARGAR_SEGURIDADSOCIAL();

                            //O.Frias - 06/08/2009
                            //Nuevo dato Emplazamiento
                            if (Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["iemplaza"]))
                            {
                                cbbEmplazamiento.SelectedIndex = 0;
                            }
                            else
                            {
                                switch (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["iemplaza"]))
                                {
                                    case "H":
                                        cbbEmplazamiento.SelectedIndex = 0;
                                        break;
                                    case "D":
                                        cbbEmplazamiento.SelectedIndex = 1;
                                        break;
                                    case "T":
                                        cbbEmplazamiento.SelectedIndex = 2;
                                        break;
                                }
                            }

                            //OSCAR C Noviembre 2009
                            if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["ghospref"]))
                            {
                                cbbHospitalReferencia.SelectedIndex = Serrores.fnBuscaListIndexID(cbbHospitalReferencia, RrSQLSOC.Tables[0].Rows[0]["ghospref"].ToString());
                            }
                            else
                            {
                                //Intentamos localizar el Hospital de Referencia
                                //A partir del Centro de Salud Asociado al Codigo CIAS asociado al paciente.
                                sqlAux = "select DENTIPAC.ghospref from DENTIPAC" +
                                         "       inner join DCODCIAS on DENTIPAC.gcodcias = DCODCIAS.gcodcias " +
                                         "       inner join DCENSALU on DCENSALU.gcensalu = DCODCIAS.gcensalu " +
                                         "       where GIDENPAC = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' AND GSOCIEDA = " + MFiliacion.CodSS.ToString() + "";
                                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlAux, MFiliacion.GConexion);
                                rrAux = new DataSet();
                                tempAdapter_2.Fill(rrAux);
                                if (rrAux.Tables[0].Rows.Count != 0)
                                {
                                    if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["ghospref"]))
                                    {
                                        cbbHospitalReferencia.SelectedIndex = Serrores.fnBuscaListIndexID(cbbHospitalReferencia, RrSQLSOC.Tables[0].Rows[0]["ghospref"].ToString());
                                    }
                                }
                            }
                            //--------
                        }
                    }
                    //carga sociedad
                    if (MFiliacion.stFiliacion == "MODIFICACION")
                    {
                        SQLSOC = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' AND GSOCIEDA <> " + MFiliacion.CodSS.ToString() + "";
                        //(maplaza)(05/09/2007)Se debe cumplir tambi�n que la sociedad en DENTIPAC no tenga Fecha de Borrado (fborrado is null)
                        SQLSOC = SQLSOC + " AND (FBORRADO IS NULL)";
                        //-----
                        //OSCAR C
                        SQLSOC = SQLSOC + " order by fmovimie desc";
                        //-------
                        SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLSOC, MFiliacion.GConexion);
                        RrSQLSOC = new DataSet();
                        tempAdapter_3.Fill(RrSQLSOC);
                        if (RrSQLSOC.Tables[0].Rows.Count > 0)
                        {
                            CARGAR_SOCIEDAD();
                        }
                    }

                    int elemento = 0;
                    SQLSOC = "";
                    if (MFiliacion.stFiliacion == "MODIFICACION")
                    {
                        SQLSOC = "SELECT * FROM DPRIVADO WHERE gidenpac = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gidenpac"]) + "'";
                    }
                    else
                    {
                        if (tbNIFOtrosDocDP.Text.Trim() != "")
                        {
                            SQLSOC = "SELECT * FROM DPRIVADO WHERE NNIFPRIV = '" + tbNIFOtrosDocDP.Text.Trim() + "'";
                        }
                    }
                    if (SQLSOC != "")
                    {
                        SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLSOC, MFiliacion.GConexion);
                        RrSQLSOC = new DataSet();
                        tempAdapter_4.Fill(RrSQLSOC);
                    }
                    if (SQLSOC != "")
                    {
                        if (RrSQLSOC.Tables[0].Rows.Count != 0)
                        {
                            CARGAR_PRIVADO();
                        }
                        else
                        {
                            if (this.rbRERegimen[2].IsChecked)
                            {
                                //tbNIFOtrosDocRE.Text = Trim(tbNIFOtrosDocDP.Text)

                                //(maplaza)(06/03/2006)Se tendr� que marcar el radio button correspondiente
                                //Se pasa el valor del documento identificativo de la primera pesta�a a la segunda pesta�a
                                if (tbNIFOtrosDocDP.Text.Trim() != "")
                                {
                                    if (MFiliacion.EsDNI_NIF(tbNIFOtrosDocDP.Text.Trim(), 9))
                                    {
                                        rbDNIOtroDocRE[0].IsChecked = true;
                                        rbDNIOtroDocRE[1].IsChecked = false;
                                    }
                                    else
                                    {
                                        //es la opci�n "Otros Documentos"
                                        rbDNIOtroDocRE[0].IsChecked = false;
                                        rbDNIOtroDocRE[1].IsChecked = true;
                                    }
                                    tbNIFOtrosDocRE.Enabled = true;
                                    if (tbNIFOtrosDocDP.Text.Trim().Length == 9)
                                    {
                                        tbNIFOtrosDocRE.Text = tbNIFOtrosDocDP.Text.Trim();
                                    }
                                    else
                                    {
                                        tbNIFOtrosDocRE.Text = StringsHelper.Format(tbNIFOtrosDocDP.Text.Trim(), "00000000") + " ";
                                    }
                                }
                                else
                                {
                                    //(Trim(tbNIFOtrosDocDP.Text) = "")
                                    tbNIFOtrosDocRE.Mask = "";
                                    tbNIFOtrosDocRE.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;
                                    tbNIFOtrosDocRE.Text = tbNIFOtrosDocDP.Text.Trim();
                                }
                                //-----
                                tbRENombre.Text = tbNombre.Text.Trim();
                                tbREApe1.Text = tbApellido1.Text.Trim();
                                tbREApe2.Text = tbApellido2.Text.Trim();
                                tbREDomicilio.Text = tbDPDomicilio.Text;
                                tbREPasaporte.Text = tbDPPasaporte.Text; //pasaporte
                                mebRECodPostal.Text = mebDPCodPostal.Text;
                                mebRECodPostal.Text = mebDPCodPostal.Text;
                                if (tbREDomicilio.Text.Trim() == "")
                                {
                                    if (Convert.ToDouble(cbbDPVia.get_ListIndex()) != -1)
                                    {
                                        object tempRefParam = 0;
                                        object tempRefParam2 = cbbDPVia.get_ListIndex();
                                        tbDPDomicilio.Text = Convert.ToString(cbbDPVia.get_Column(tempRefParam, tempRefParam2)).Trim().ToUpper();
                                    }
                                    if (tbDPDomicilioP.Text != "")
                                    {
                                        tbDPDomicilio.Text = tbDPDomicilio.Text + " " + tbDPDomicilioP.Text.Trim().ToUpper();
                                    }
                                    if (tbDPNDomic.Text != "")
                                    {
                                        tbDPDomicilio.Text = tbDPDomicilio.Text + " " + tbDPNDomic.Text.Trim().ToUpper();
                                    }
                                    if (tbDPOtrosDomic.Text != "")
                                    {
                                        tbDPDomicilio.Text = tbDPDomicilio.Text + " " + tbDPOtrosDomic.Text.Trim().ToUpper();
                                    }
                                    tbREDomicilio.Text = tbDPDomicilio.Text;
                                }
                                if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
                                {
                                    for (int i = 0; i <= cbbREPoblacion.ListCount - 1; i++)
                                    {
                                        object tempRefParam3 = 1;
                                        object tempRefParam4 = i;
                                        object tempRefParam5 = 1;
                                        object tempRefParam6 = cbbDPPoblacion.get_ListIndex();
                                        if (cbbREPoblacion.get_Column(tempRefParam3, tempRefParam4).Equals(cbbDPPoblacion.get_Column(tempRefParam5, tempRefParam6)))
                                        {
                                            i = Convert.ToInt32(tempRefParam4);
                                            cbbREPoblacion.set_ListIndex(i);
                                            break;
                                        }
                                        else
                                        {
                                            i = Convert.ToInt32(tempRefParam4);
                                        }
                                    }
                                }
                                //SI NO LA HA ENCONTRADO Y NO HAY NINGUNO SELECCIONADO
                                if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) == -1 && Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
                                {
                                    //CARGAMOS LAS DE DATOS PACIENTE EN REGIMEN ECONOMICO
                                    cbbREPoblacion.Clear();
                                    elemento = Convert.ToInt32(cbbDPPoblacion.get_ListIndex());
                                    for (int i = 0; i <= cbbDPPoblacion.ListCount - 1; i++)
                                    {
                                        object tempRefParam8 = 0;
                                        object tempRefParam9 = i;
                                        object tempRefParam7 = cbbDPPoblacion.get_Column(tempRefParam8, tempRefParam9);
                                        object tempRefParam10 = i;
                                        cbbREPoblacion.AddItem(tempRefParam7, tempRefParam10);
                                        i = Convert.ToInt32(tempRefParam10);
                                        i = Convert.ToInt32(tempRefParam9);
                                        object tempRefParam11 = 1;
                                        object tempRefParam12 = i;
                                        cbbREPoblacion.set_List(i, 1, cbbDPPoblacion.get_Column(tempRefParam11, tempRefParam12));
                                        i = Convert.ToInt32(tempRefParam12);
                                    }
                                    //ponemos el elemento que tenia y
                                    //buscamos en regimen economico
                                    cbbDPPoblacion.set_ListIndex(elemento);
                                    if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
                                    {
                                        cbbREPoblacion.set_ListIndex(elemento);
                                    }
                                }
                                chbREExtrangero.CheckState = chbDPExtrangero.CheckState;
                                cbbREPais.set_ListIndex(cbbDPPais.get_ListIndex());
                                tbREExPob.Text = tbDPExPob.Text;
                                //(maplaza)(10/08/2007)
                                tbREDireccionEx.Text = tbDPDireccionEx.Text;
                                //------
                            }
                        }
                    }
                    else
                    {
                        if (this.rbRERegimen[2].IsChecked)
                        {
                            //tbNIFOtrosDocRE.Text = Trim(tbNIFOtrosDocDP.Text)

                            //(maplaza)(06/03/2006)Se tendr� que marcar el radio button correspondiente
                            //Se pasa el valor del documento identificativo de la primera pesta�a a la segunda pesta�a
                            if (tbNIFOtrosDocDP.Text.Trim() != "")
                            {
                                if (MFiliacion.EsDNI_NIF(tbNIFOtrosDocDP.Text.Trim(), 9))
                                {
                                    rbDNIOtroDocRE[0].IsChecked = true;
                                    rbDNIOtroDocRE[1].IsChecked = false;
                                }
                                else
                                {
                                    //es la opci�n "Otros Documentos"
                                    rbDNIOtroDocRE[0].IsChecked = false;
                                    rbDNIOtroDocRE[1].IsChecked = true;
                                }
                                tbNIFOtrosDocRE.Enabled = true;
                                if (tbNIFOtrosDocDP.Text.Trim().Length == 9)
                                {
                                    tbNIFOtrosDocRE.Text = tbNIFOtrosDocDP.Text.Trim();
                                }
                                else
                                {
                                    tbNIFOtrosDocRE.Text = StringsHelper.Format(tbNIFOtrosDocDP.Text.Trim(), "00000000") + " ";
                                }
                            }
                            else
                            {
                                //(Trim(tbNIFOtrosDocDP.Text) = "")
                                tbNIFOtrosDocRE.Mask = "";
                                tbNIFOtrosDocRE.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;
                                tbNIFOtrosDocRE.Text = tbNIFOtrosDocDP.Text.Trim();
                            }
                            //-----
                            tbREPasaporte.Text = tbDPPasaporte.Text;
                            if (MFiliacion.stFiliacion == "MODIFICACION")
                            {
                                tbRENombre.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DNOMBPAC"]);
                                tbREApe1.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE1PAC"]);
                                tbREPasaporte.Text = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NPASAPOR"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NPASAPOR"]); //pasaporte
                                if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE2PAC"]))
                                {
                                    tbREApe2.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE2PAC"]);
                                }
                                else
                                {
                                    tbREApe2.Text = "";
                                }
                            }
                            else
                            {
                                tbRENombre.Text = MFiliacion.NuevoDFI120F1.tbNombre.Text.Trim();
                                tbREApe1.Text = MFiliacion.NuevoDFI120F1.tbApellido1.Text.Trim();
                                tbREApe2.Text = MFiliacion.NuevoDFI120F1.tbApellido2.Text.Trim();
                            }
                            if (Convert.ToDouble(cbbDPVia.get_ListIndex()) != -1)
                            {
                                object tempRefParam13 = 0;
                                object tempRefParam14 = cbbDPVia.get_ListIndex();
                                tbREDomicilio.Text = Convert.ToString(cbbDPVia.get_Column(tempRefParam13, tempRefParam14)).Trim().ToUpper();
                            }
                            if (tbDPDomicilioP.Text != "")
                            {
                                tbREDomicilio.Text = tbREDomicilio.Text + " " + tbDPDomicilioP.Text.Trim().ToUpper();
                            }
                            if (tbDPNDomic.Text != "")
                            {
                                tbREDomicilio.Text = tbREDomicilio.Text + " " + tbDPNDomic.Text.Trim().ToUpper();
                            }
                            if (tbDPOtrosDomic.Text != "")
                            {
                                tbREDomicilio.Text = tbREDomicilio.Text + " " + tbDPOtrosDomic.Text.Trim().ToUpper();
                            }
                            mebRECodPostal.Text = mebDPCodPostal.Text;
                            if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
                            {
                                for (int i = 0; i <= cbbREPoblacion.ListCount - 1; i++)
                                {
                                    object tempRefParam15 = 1;
                                    object tempRefParam16 = i;
                                    object tempRefParam17 = 1;
                                    object tempRefParam18 = cbbDPPoblacion.get_ListIndex();
                                    if (cbbREPoblacion.get_Column(tempRefParam15, tempRefParam16).Equals(cbbDPPoblacion.get_Column(tempRefParam17, tempRefParam18)))
                                    {
                                        i = Convert.ToInt32(tempRefParam16);
                                        cbbREPoblacion.set_ListIndex(i);
                                        break;
                                    }
                                    else
                                    {
                                        i = Convert.ToInt32(tempRefParam16);
                                    }
                                }
                            }
                            //SI NO LA HA ENCONTRADO Y NO HAY NINGUNO SELECCIONADO
                            if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) == -1 && Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
                            {
                                //CARGAMOS LAS DE DATOS PACIENTE EN REGIMEN ECONOMICO
                                cbbREPoblacion.Clear();
                                elemento = Convert.ToInt32(cbbDPPoblacion.get_ListIndex());
                                for (int i = 0; i <= cbbDPPoblacion.ListCount - 1; i++)
                                {
                                    object tempRefParam20 = 0;
                                    object tempRefParam21 = i;
                                    object tempRefParam19 = cbbDPPoblacion.get_Column(tempRefParam20, tempRefParam21);
                                    object tempRefParam22 = i;
                                    cbbREPoblacion.AddItem(tempRefParam19, tempRefParam22);
                                    i = Convert.ToInt32(tempRefParam22);
                                    i = Convert.ToInt32(tempRefParam21);
                                    object tempRefParam23 = 1;
                                    object tempRefParam24 = i;
                                    cbbREPoblacion.set_List(i, 1, cbbDPPoblacion.get_Column(tempRefParam23, tempRefParam24));
                                    i = Convert.ToInt32(tempRefParam24);
                                }
                                //ponemos el elemento que tenia y
                                //buscamos en regimen economico
                                cbbDPPoblacion.set_ListIndex(elemento);
                                if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
                                {
                                    cbbREPoblacion.set_ListIndex(elemento);
                                }
                            }
                            chbREExtrangero.CheckState = chbDPExtrangero.CheckState;
                            cbbREPais.set_ListIndex(cbbDPPais.get_ListIndex());
                            tbREExPob.Text = tbDPExPob.Text;
                            //(maplaza)(10/08/2007)
                            tbREDireccionEx.Text = tbDPDireccionEx.Text;
                            //-----
                        }
                        else
                        {
                            //(maplaza)(16/05/2006)
                            if (tbNIFOtrosDocRE.Mask == MFiliacion.stFORMATO_NIF)
                            {
                                tbNIFOtrosDocRE.Text = "         ";
                            }
                            else
                            {
                                tbNIFOtrosDocRE.Text = "";
                            }
                            //-----
                            tbREPasaporte.Text = "";
                            tbRENombre.Text = "";
                            tbREApe1.Text = "";
                            tbREApe2.Text = "";
                            tbREDomicilio.Text = "";
                            cbbREPoblacion.set_ListIndex(-1);
                            chbREExtrangero.CheckState = CheckState.Unchecked;
                            cbbREPais.set_ListIndex(-1);
                            tbREExPob.Text = "";
                            //(maplaza)(10/08/2007)
                            tbREDireccionEx.Text = "";
                            //-----
                        }
                    }
                    if (SQLSOC != "")
                    {
                        RrSQLSOC.Close();
                    }

                    Activar_Cajas(); //(maplaza)(16/05/2006)
                    COMPROBAR_DATOS();
                }
            }
		}

        private void rbRERegimen_DblClick(object sender, System.EventArgs e)
        {
            flagDbClick = true;
            int Index = Array.IndexOf(this.rbRERegimen, sender);
            //Regimen Ecomomico
            rbRERegimen[Index].IsChecked = false;
           
            Desactivar_Cajas();
            COMPROBAR_DATOS();
        }

        //Oscar C Mayo 2010
        //Funcion para colorear los controles de seleccion de regimen economico en funcion del regimen economico seleccionado
        private void Colorear_Cajas()
		{
			frmRERegimen.BackColor = colorSinSeleccion;
			rbRERegimen[0].BackColor = colorSinSeleccion;
			rbRERegimen[1].BackColor = colorSinSeleccion;
			rbRERegimen[2].BackColor = colorSinSeleccion;
			//SS
			frmSegSocial.BackColor = colorSinSeleccion;
			Label11.BackColor = colorSinSeleccion;
			Label13.BackColor = colorSinSeleccion;
			Label29.BackColor = colorSinSeleccion;
			chkZona.BackColor = colorSinSeleccion;
			Label2.BackColor = colorSinSeleccion;
			Label75.BackColor = colorSinSeleccion;
			Frame10.BackColor = colorSinSeleccion;
			rbIndicadorSS[0].BackColor = colorSinSeleccion;
			rbIndicadorSS[1].BackColor = colorSinSeleccion;
			Frame1.BackColor = colorSinSeleccion;
			rbActivo.BackColor = colorSinSeleccion;
			rbPensionista.BackColor = colorSinSeleccion;
			Label47.BackColor = colorSinSeleccion;
			Label76.BackColor = colorSinSeleccion;
			//SOC
			frmSociedad.BackColor = colorSinSeleccion;
			Label12.BackColor = colorSinSeleccion;
			Label14.BackColor = colorSinSeleccion;
			LbVersion.BackColor = colorSinSeleccion;
			Label38.BackColor = colorSinSeleccion;
			Label71.BackColor = colorSinSeleccion;
			Label72.BackColor = colorSinSeleccion;
			Label73.BackColor = colorSinSeleccion;
			frmIndicador.BackColor = colorSinSeleccion;
			rbIndicador[0].BackColor = colorSinSeleccion;
			rbIndicador[1].BackColor = colorSinSeleccion;
			Label5.BackColor = colorSinSeleccion;
			//PRIV
			frmPrivado.BackColor = colorSinSeleccion;
			Frame17.BackColor = colorSinSeleccion;
			rbDNIOtroDocRE[0].BackColor = colorSinSeleccion;
			rbDNIOtroDocRE[1].BackColor = colorSinSeleccion;
			chkPropioPaciente.BackColor = colorSinSeleccion;
			Label43.BackColor = colorSinSeleccion;
			Label18.BackColor = colorSinSeleccion;
			Label25.BackColor = colorSinSeleccion;
			Label17.BackColor = colorSinSeleccion;
			Label20.BackColor = colorSinSeleccion;
			Label27.BackColor = colorSinSeleccion;
			Label30.BackColor = colorSinSeleccion;
			Frame4.BackColor = colorSinSeleccion;
			chbREExtrangero.BackColor = colorSinSeleccion;
			Label45.BackColor = colorSinSeleccion;
			Label70.BackColor = colorSinSeleccion;
			Label44.BackColor = colorSinSeleccion;
			Frame8.BackColor = colorSinSeleccion;
			//Se pasa antes por la comprobaci�n de la constante. Y si est� activa (valfanu1='S')
			//se pone el FrmCB21 a igual Top que Frame9.
			if (((float) (Frame9.Top * 15)) == ((float) (FrmCB21.Top * 15)))
			{
				FrmCB21.BackColor = colorSinSeleccion;
				lbNcuenta21.BackColor = colorSinSeleccion;
			}
			else
			{
				Frame9.BackColor = colorSinSeleccion;
				Label46.BackColor = colorSinSeleccion;
			}
			//Si el tag de la etiqueta est� vac�o, no est� activa la constante de diferenciaci�n de tercero.
			if (Convert.ToString(lblObraSocial.Tag) != "")
			{
				lblObraSocial.BackColor = colorSinSeleccion;
			}

			chkExentoIVA.BackColor = colorSinSeleccion;
			if (rbRERegimen[0].IsChecked)
			{
				//SS
				frmSegSocial.BackColor = colorConSeleccion;
				Label11.BackColor = colorConSeleccion;
				Label13.BackColor = colorConSeleccion;
				Label29.BackColor = colorConSeleccion;
				chkZona.BackColor = colorConSeleccion;
				Label2.BackColor = colorConSeleccion;
				Label75.BackColor = colorConSeleccion;
				Frame10.BackColor = colorConSeleccion;
				rbIndicadorSS[0].BackColor = colorConSeleccion;
				rbIndicadorSS[1].BackColor = colorConSeleccion;
				Frame1.BackColor = colorConSeleccion;
				rbActivo.BackColor = colorConSeleccion;
				rbPensionista.BackColor = colorConSeleccion;
				Label47.BackColor = colorConSeleccion;
				Label76.BackColor = colorConSeleccion;
			}
			else if (rbRERegimen[1].IsChecked)
			{ 
				//SOC
				frmSociedad.BackColor = colorConSeleccion;
				Label12.BackColor = colorConSeleccion;
				Label14.BackColor = colorConSeleccion;
				LbVersion.BackColor = colorConSeleccion;
				Label38.BackColor = colorConSeleccion;
				Label71.BackColor = colorConSeleccion;
				Label72.BackColor = colorConSeleccion;
				Label73.BackColor = colorConSeleccion;
				frmIndicador.BackColor = colorConSeleccion;
				rbIndicador[0].BackColor = colorConSeleccion;
				rbIndicador[1].BackColor = colorConSeleccion;
				Label5.BackColor = colorConSeleccion;
				chkExentoIVA.BackColor = colorConSeleccion;
				//Si el tag de la etiqueta est� vac�o, no est� activa la constante de diferenciaci�n de tercero.
				if (Convert.ToString(lblObraSocial.Tag) != "")
				{
					lblObraSocial.BackColor = colorConSeleccion;
				}
			}
			else if (rbRERegimen[2].IsChecked)
			{ 
				//PRIV
				frmPrivado.BackColor = colorConSeleccion;
				Frame17.BackColor = colorConSeleccion;
				rbDNIOtroDocRE[0].BackColor = colorConSeleccion;
				rbDNIOtroDocRE[1].BackColor = colorConSeleccion;
				chkPropioPaciente.BackColor = colorConSeleccion;
				Label43.BackColor = colorConSeleccion;
				Label18.BackColor = colorConSeleccion;
				Label25.BackColor = colorConSeleccion;
				Label17.BackColor = colorConSeleccion;
				Label20.BackColor = colorConSeleccion;
				Label27.BackColor = colorConSeleccion;
				Label30.BackColor = colorConSeleccion;
				Frame4.BackColor = colorConSeleccion;
				chbREExtrangero.BackColor = colorConSeleccion;
				Label45.BackColor = colorConSeleccion;
				Label70.BackColor = colorConSeleccion;
				Label44.BackColor = colorConSeleccion;
				Frame8.BackColor = colorConSeleccion;
				//Se pasa antes por la comprobaci�n de la constante. Y si est� activa (valfanu1='S')
				//se pone el FrmCB21 a igual Top que Frame9.
				if (((float) (Frame9.Top * 15)) == ((float) (FrmCB21.Top * 15)))
				{
					FrmCB21.BackColor = colorConSeleccion;
					lbNcuenta21.BackColor = colorConSeleccion;
				}
				else
				{
					Frame9.BackColor = colorConSeleccion;
					Label46.BackColor = colorConSeleccion;
				}
			}
			else
			{
				frmRERegimen.BackColor = colorConSeleccion;
				rbRERegimen[0].BackColor = colorConSeleccion;
				rbRERegimen[1].BackColor = colorConSeleccion;
				rbRERegimen[2].BackColor = colorConSeleccion;
			}

		}
        //------------------------------------------------

        //(maplaza)(16/05/2006)Este procedimiento se ha realizado para resolver el siguiente problema:
        //El tema es que si se marca una de las opciones de r�gimen econ�mico (sea Seguridad Social, Sociedad o Privados
        //la que quieras), si despu�s la desmarcas ya a partir de ah� no puedes hacer nada porque aunque marques otra vez
        //alguna de estas no se activan nunca  ni el combo de inspecciones (si marcas seguridad social) ni el de
        //sociedades (si marcas sociedad).

        private void Activar_Cajas()
		{
			cbbREInspeccion.Enabled = true;
			tbRENSS.Enabled = true;
			cbbRESociedad.Enabled = true;
			tbRENPoliza.Enabled = true;
			chkExentoIVA.Enabled = true;
			tbRENVersion.Enabled = true;
			tbREEmpresa.Enabled = true;
			tbRENomTitular.Enabled = true;
			//(maplaza)(24/08/2007)
			tbRECobertura.Enabled = true;
			mebREFechaAlta.Enabled = true;
			mebREFechaCaducidad.Enabled = true;
			//-----
			tbREApe1.Enabled = true;
			tbREApe2.Enabled = true;
			tbRENombre.Enabled = true;
			tbNIFOtrosDocRE.Enabled = true; //(maplaza)(03/05/2006)
			tbREPasaporte.Enabled = true; //pasaporte
			tbREDomicilio.Enabled = true;
			mebRECodPostal.Enabled = true;
			cbbREPoblacion.Enabled = true;
			Colorear_Cajas(); //Oscar C Mayo 2010
		}

		//(maplaza)(03/05/2006)
		private void rbDNIOtroDocDP_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.rbDNIOtroDocDP, eventSender);
				tbNIFOtrosDocDP.Enabled = true;
				switch(Index)
				{
					case 0 :
                        if (flagDbClick)
                        {
                            _rbDNIOtroDocDP_0.IsChecked = false;
                            rbDNIOtroDocDP[0].IsChecked = false;
                            tbNIFOtrosDocDP.Enabled = false;
                            flagDbClick = false;
                        }
                        else
                        {
                            if (MFiliacion.EsDNI_NIF(tbNIFOtrosDocDP.Text.Trim(), 9))
                            {
                                //Si es un DNI, se a�ade un espacio en blanco al final, para que lo acepte la m�scara
                                if (tbNIFOtrosDocDP.Text.Trim().Length == 8)
                                {
                                    tbNIFOtrosDocDP.Text = tbNIFOtrosDocDP.Text.Trim() + " ";
                                }
                            }
                            tbNIFOtrosDocDP.Mask = MFiliacion.stFORMATO_NIF;  //con m�scara, el usuario debe introducir un formato determinado 
                        }
						break;
					case 1 :
                        if (flagDbClick)
                        {
                            _rbDNIOtroDocDP_1.IsChecked = false;
                            rbDNIOtroDocDP[1].IsChecked = false;
                            tbNIFOtrosDocDP.Enabled = false;
                            flagDbClick = false;
                        }
                        else
                        {
                            tbNIFOtrosDocDP.Mask = "";  //sin m�scara, el usuario puede introducir lo que quiera 
                            tbNIFOtrosDocDP.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;
                            //para quitar los espacios en blanco 
                            if (tbNIFOtrosDocDP.Text == "         ")
                            {
                                tbNIFOtrosDocDP.Text = "";
                            }
                        }
						break;
				}
				COMPROBAR_DATOS();
			}
		}

		//(maplaza)(03/05/2006)		
		private void rbDNIOtroDocDP_DblClick(object sender, System.EventArgs e)
		{
            flagDbClick = true;
            int Index = Array.IndexOf(this.rbDNIOtroDocDP, sender);
            rbDNIOtroDocDP[Index].IsChecked = false;
			tbNIFOtrosDocDP.Mask = "";
			tbNIFOtrosDocDP.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;
			tbNIFOtrosDocDP.Text = "";
			tbNIFOtrosDocDP.Enabled = false;
			COMPROBAR_DATOS();
		}
       
        //(maplaza)(03/05/2006)
        private void rbDNIOtroDocRE_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.rbDNIOtroDocRE, eventSender);
				tbNIFOtrosDocRE.Enabled = true;
				switch(Index)
				{
					case 0 :
                        if (flagDbClick)
                        {
                            _rbDNIOtroDocRE_0.IsChecked = false;
                            rbDNIOtroDocRE[0].IsChecked = false;
                            tbNIFOtrosDocRE.Enabled = false;
                            flagDbClick = false;
                        }
                        else
                        {
                            if (MFiliacion.EsDNI_NIF(tbNIFOtrosDocRE.Text.Trim(), 9))
                            {
                                //Si es un DNI, se a�ade un espacio en blanco al final, para que lo acepte la m�scara
                                if (tbNIFOtrosDocRE.Text.Trim().Length == 8)
                                {
                                    tbNIFOtrosDocRE.Text = tbNIFOtrosDocRE.Text.Trim() + " ";
                                }
                            }
                        }
						tbNIFOtrosDocRE.Mask = MFiliacion.stFORMATO_NIF;  //con m�scara, el usuario debe introducir un formato determinado 
						break;
					case 1 :
                        if (flagDbClick)
                        {
                            _rbDNIOtroDocRE_1.IsChecked = false;
                            rbDNIOtroDocRE[1].IsChecked = false;
                            tbNIFOtrosDocRE.Enabled = false;
                            flagDbClick = false;
                        }
                        else
                        {
                            tbNIFOtrosDocRE.Mask = "";  //sin m�scara, el usuario puede introducir lo que quiera 
                            tbNIFOtrosDocRE.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;
                            //para quitar los espacios en blanco 
                            if (tbNIFOtrosDocRE.Text == "         ")
                            {
                                tbNIFOtrosDocRE.Text = "";
                            }
                        }
						break;
				}
				COMPROBAR_DATOS();
			}
		}

		//(maplaza)(03/05/2006)		
		private void rbDNIOtroDocRE_DblClick(object sender, System.EventArgs e)
		{
            flagDbClick = true;
            int Index = Array.IndexOf(this.rbDNIOtroDocRE, sender);
            rbDNIOtroDocRE[Index].IsChecked = false;
			tbNIFOtrosDocRE.Mask = "";
			tbNIFOtrosDocRE.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;
			tbNIFOtrosDocRE.Text = "";
			tbNIFOtrosDocRE.Enabled = false;
			COMPROBAR_DATOS();
		}

		//(maplaza)(03/05/2006)
		private void rbDNIOtroDocPC_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.rbDNIOtroDocPC, eventSender);
				tbNIFOtrosDocPC.Enabled = true;
				switch(Index)
				{
					case 0 :
                        if (flagDbClick)
                        {
                            _rbDNIOtroDocPC_0.IsChecked = false;
                            rbDNIOtroDocPC[0].IsChecked = false;
                            tbNIFOtrosDocPC.Enabled = false;
                            flagDbClick = false;
                        }
                        else
                        {
                            if (MFiliacion.EsDNI_NIF(tbNIFOtrosDocPC.Text.Trim(), 9))
                            {
                                //Si es un DNI, se a�ade un espacio en blanco al final, para que lo acepte la m�scara
                                if (tbNIFOtrosDocPC.Text.Trim().Length == 8)
                                {
                                    tbNIFOtrosDocPC.Text = tbNIFOtrosDocPC.Text.Trim() + " ";
                                }
                            }
                            tbNIFOtrosDocPC.Mask = MFiliacion.stFORMATO_NIF;  //con m�scara, el usuario debe introducir un formato determinado 
                        }
						break;
					case 1 :
                        if (flagDbClick)
                        {
                            _rbDNIOtroDocPC_1.IsChecked = false;
                            rbDNIOtroDocPC[1].IsChecked = false;
                            tbNIFOtrosDocPC.Enabled = false;
                            flagDbClick = false;
                        }
                        else
                        {
                            tbNIFOtrosDocPC.Mask = "";  //sin m�scara, el usuario puede introducir lo que quiera 
                            tbNIFOtrosDocPC.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;
                            //para quitar los espacios en blanco 
                            if (tbNIFOtrosDocPC.Text == "         ")
                            {
                                tbNIFOtrosDocPC.Text = "";
                            }
                        }
						break;
				}
				COMPROBAR_DATOS();
			}
		}

		//(maplaza)(03/05/2006)		
		private void rbDNIOtroDocPC_DblClick(object sender, System.EventArgs e)
		{
            flagDbClick = true;
            int Index = Array.IndexOf(this.rbDNIOtroDocPC, sender);
            rbDNIOtroDocPC[Index].IsChecked = false;
			tbNIFOtrosDocPC.Mask = "";
			tbNIFOtrosDocPC.MaskedEditBoxElement.TextBoxItem.MaxLength = 9;
			tbNIFOtrosDocPC.Text = "";
			tbNIFOtrosDocPC.Enabled = false;
			COMPROBAR_DATOS();
		}

		private void CARGAR_PRIVADO()
		{
			bcargado_privado = true;

			//(maplaza)(03/05/2006)
			if (!Convert.IsDBNull(Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NNIFPRIV"]).Trim()))
			{
				if (MFiliacion.EsDNI_NIF(Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NNIFPRIV"]).Trim(), 9))
				{
					rbDNIOtroDocRE[0].IsChecked = true;
					rbDNIOtroDocRE[1].IsChecked = false;
					tbNIFOtrosDocRE.Enabled = true;
					if (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NNIFPRIV"]).Trim().Length == 9)
					{
						tbNIFOtrosDocRE.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NNIFPRIV"]).Trim();
					}
					else
					{
						tbNIFOtrosDocRE.Text = StringsHelper.Format(RrSQLSOC.Tables[0].Rows[0]["NNIFPRIV"], "00000000") + " ";
					}
				}
				else
				{
					//si no se trata de un DNI ni de un NIF es "Otros documentos"
					rbDNIOtroDocRE[0].IsChecked = false;
					rbDNIOtroDocRE[1].IsChecked = true;
					tbNIFOtrosDocRE.Enabled = true;
					tbNIFOtrosDocRE.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NNIFPRIV"]).Trim();
				}
			}
			else
			{
				//IsNull(Trim(RrSQLSOC("NNIFPRIV")))
				rbDNIOtroDocRE[0].IsChecked = false;
				rbDNIOtroDocRE[1].IsChecked = false;
				tbNIFOtrosDocRE.Enabled = false;
				if (tbNIFOtrosDocRE.Mask == MFiliacion.stFORMATO_NIF)
				{
					tbNIFOtrosDocRE.Text = "         ";
				}
				else
				{
					tbNIFOtrosDocRE.Text = "";
				}
				tbNIFOtrosDocRE.Text = tbNIFOtrosDocDP.Text;
			}

			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["NNIFPRIV"]))
			{
				NIF_Antiguo = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NNIFPRIV"]).Trim();
			}
			else
			{
				NIF_Antiguo = "";
			}
			tbRENombre.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["DNOMBPRI"]).Trim();
			tbREApe1.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["DAPE1PRI"]).Trim();
			tbREApe2.Text = (Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["DAPE2PRI"])) ? "" : Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["DAPE2PRI"]).Trim();
			//pasaporte
			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["NPASAPOR"]))
			{
				tbREPasaporte.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NPASAPOR"]).Trim();
			}
			else
			{
				//''    tbREPasaporte.Text = ""
				tbREPasaporte.Text = tbDPPasaporte.Text;
			}
			//---
			//(maplaza)(30/08/2007)
			if (Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["IPROPACI"]))
			{
				chkPropioPaciente.CheckState = CheckState.Unchecked;
			}
			else if (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["IPROPACI"]).ToUpper() == "S")
			{ 
				chkPropioPaciente.CheckState = CheckState.Checked;
			}
			else
			{
				chkPropioPaciente.CheckState = CheckState.Unchecked;
			}
			//------
			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["DDIREPRI"]))
			{
				tbREDomicilio.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["DDIREPRI"]).Trim();
			}
			else
			{
				//''    tbREDomicilio.Text = ""
				tbREDomicilio.Text = tbDPDomicilio.Text;
			}

			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["GCODIPOS"]))
			{
				bNocambiarCP_RE = true; //(maplaza)(06/06/2006)Para que no salte el evento "Change"
				if (bFormatoCodigoPos)
				{
					mebRECodPostal.Text = StringsHelper.Format(RrSQLSOC.Tables[0].Rows[0]["GCODIPOS"], "00000");
				}
				else
				{
					mebRECodPostal.Text = (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GCODIPOS"]) + "").Trim();
				}
				bNocambiarCP_RE = false; //(maplaza)(06/06/2006)
			}
			else
			{
				//''    mebRECodPostal.Text = "     "
				mebRECodPostal.Text = mebDPCodPostal.Text;

			}

			string SQLDPOBLACION = String.Empty;
			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["GPOBLACI"]))
			{
				SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GPOBLACI"]).Substring(0, Math.Min(2, Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GPOBLACI"]).Length)) + "%' order by dpoblaci";
				CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbREPoblacion);
				for (int i = 0; i <= cbbREPoblacion.ListCount - 1; i++)
				{
					object tempRefParam = 1;
					object tempRefParam2 = i;
					if (cbbREPoblacion.get_Column(tempRefParam, tempRefParam2) == RrSQLSOC.Tables[0].Rows[0]["GPOBLACI"])
					{
						i = Convert.ToInt32(tempRefParam2);
						cbbREPoblacion.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam2);
					}
				}
			}

			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["gpaisres"]) && (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["gpaisres"]) + "").Trim() != "34")
			{
				chbREExtrangero.CheckState = CheckState.Checked;
				for (int i = 0; i <= cbbREPais.ListCount - 1; i++)
				{
					object tempRefParam3 = 1;
					object tempRefParam4 = i;
					if (Convert.ToString(cbbREPais.get_Column(tempRefParam3, tempRefParam4)).Trim().ToUpper() == Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GPAISRES"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam4);
						cbbREPais.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam4);
					}
				}
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["dpoblaci"]))
				{
					tbREExPob.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["dpoblaci"]).Trim();
				}
				else
				{
					//''            tbREExPob.Text = ""
					tbREExPob.Text = tbDPExPob.Text;
				}
				//(maplaza)(10/08/2007)Campo "ddireext"
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["ddireext"]))
				{
					tbREDireccionEx.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["ddireext"]).Trim();
				}
				else
				{
					tbREDireccionEx.Text = tbDPDireccionEx.Text;
				}
				//-----
			}

			//FORMA DE PAGO
			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["GFORPAGO"]))
			{
				for (int i = 0; i <= ((RadGridView)cbbREPago.EditorControl).Rows.Count - 1; i++)
				{					
					int tempRefParam5 = 1;
					int tempRefParam6 = i;
					if (getLisIndex(cbbREPago,tempRefParam6, tempRefParam5).Trim().ToUpper() == Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GFORPAGO"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam6);
						cbbREPago.SelectedIndex = i;
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam6);
					}
				}
			}
			//Se pasa antes por la comprobaci�n de la constante. Y si est� activa (valfanu1='S')
			//se pone el FrmCB21 a igual Top que Frame9.
			if (((float) (Frame9.Top * 15)) == ((float) (FrmCB21.Top * 15)))
			{
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["GBANCO"]))
				{
					tbBanco1.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GBANCO"]);
				}
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["GAGENCIA"]))
				{
					tbSucursal1.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GAGENCIA"]);
				}
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["NDIGCONT"]))
				{
					tbControl1.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NDIGCONT"]);
				}
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["NCUENTA"]))
				{
					tbCuenta1.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NCUENTA"]);
				}
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["Ndigcont2"]))
				{
					tbControl12.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["Ndigcont2"]);
				}
			}
			else
			{
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["GBANCO"]))
				{
					tbBanco.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GBANCO"]);
				}
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["GAGENCIA"]))
				{
					tbSucursal.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GAGENCIA"]);
				}
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["NDIGCONT"]))
				{
					tbControl.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NDIGCONT"]);
				}
				if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["NCUENTA"]))
				{
					tbCuenta.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NCUENTA"]);
				}
			}
		}

        private void CARGAR_SOCIEDAD()
		{
			string StSql = String.Empty;
			DataSet RrSql = null;

			//Si el tag de la etiqueta est� vac�o, no est� activa la constante de diferenciaci�n de tercero.
			if (Convert.ToString(lblObraSocial.Tag) != "")
			{

				StSql = "select b07_denom,B07_rterc,B07_eterc from FETERB07 a where exists ( select '' from ifmsfa_dsocieda b " + 
				        "where gsocieda='" + Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GSOCIEDA"]) + "' and a.B07_rterc = b.rterc and a.B07_eterc = b.eterc )";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);

				//buscamos el tercero econ�mico
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					for (int i = 0; i <= CbbObraSocial.Items.Count - 1; i++)
					{
						if (CbbObraSocial.Items[i].Text == Strings.StrConv(Convert.ToString(RrSql.Tables[0].Rows[0]["b07_denom"]).Trim() + new String(' ', 500) + Convert.ToString(RrSql.Tables[0].Rows[0]["B07_rterc"]).Trim() + Convert.ToString(RrSql.Tables[0].Rows[0]["B07_eterc"]).Trim(), VbStrConv.ProperCase, 0))
						{
							CbbObraSocial.SelectedIndex = i;
							break;
						}
					}
				}
			}
			for (int i = 1; i <= cbbRESociedad.Items.Count - 1; i++)
			{
				if (int.Parse(cbbRESociedad.Items[i].Value.ToString()) == Convert.ToDouble(RrSQLSOC.Tables[0].Rows[0]["GSOCIEDA"].ToString()))
				{
					cbbRESociedad.SelectedIndex = i;
					break;
				}
			}
			if ((Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["icondiva"]) + "").Trim().ToUpper() == "S")
			{
				chkExentoIVA.CheckState = CheckState.Checked;
			}
			else
			{
				chkExentoIVA.CheckState = CheckState.Unchecked;
			}
			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["NAFILIAC"]))
			{

				if (proCoincideMascara(Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NAFILIAC"]).Trim()))
				{
					tbRENPoliza.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NAFILIAC"]).Trim();
				}
				else
				{
					if (MFiliacion.stFiliacion == "ALTA")
					{
						//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
						tbRENPoliza.Text = MascaraVacia();
					}
					else
					{
						//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
						bCambiandoMascara = true;
						tbRENPoliza.Mask = "";
						bCambiandoMascara = false;
						tbRENPoliza.BackColor = Color.FromArgb(255, 128, 128);
						tbRENPoliza.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NAFILIAC"]).Trim();
					}
				}

				//        tbRENPoliza.Text = Trim(RrSQLSOC("NAFILIAC"))
				tbRENVersion.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["nversion"]) + "";

                //�APA PARA INTENTAR RECOGER DE LA TARJETA EL NUMERO DE TARJETA Y VERSION CUANDO
                //ESTAS NO COINCIDA CON LEIDA DEL LECTOR

                
                foreach (Form formu in Application.OpenForms)
				{
					if (formu.Name == "DFI120F1")
					{
						if (DFI120F1.DefInstance.stVersionLeidaTarjeta.Trim() != "")
						{
							if (DFI120F1.DefInstance.stVersionLeidaTarjeta.Trim() != Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["nversion"]).Trim())
							{
								tbRENVersion.Text = DFI120F1.DefInstance.stVersionLeidaTarjeta.Trim();
							}
						}
						if (DFI120F1.DefInstance.stNumeroLeidoTarjeta.Trim() != "")
						{
							if (DFI120F1.DefInstance.stNumeroLeidoTarjeta.Trim() != Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["nversion"]).Trim())
							{

								if (proCoincideMascara(DFI120F1.DefInstance.stNumeroLeidoTarjeta.Trim()))
								{
									tbRENPoliza.Text = DFI120F1.DefInstance.stNumeroLeidoTarjeta.Trim().Trim();
								}
								else
								{
									if (MFiliacion.stFiliacion == "ALTA")
									{
										//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
										tbRENPoliza.Text = MascaraVacia();
									}
									else
									{
										//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
										bCambiandoMascara = true;
										tbRENPoliza.Mask = "";
										bCambiandoMascara = false;
										tbRENPoliza.BackColor = Color.FromArgb(255, 128, 128);
										tbRENPoliza.Text = DFI120F1.DefInstance.stNumeroLeidoTarjeta.Trim();
									}
								}

								//                        tbRENPoliza.Text = Trim(DFI120F1.stNumeroLeidoTarjeta)
							}
						}
						break;
					}
				}                
                //------

            }
			else
			{
				tbRENPoliza.Text = MascaraVacia();
				tbRENVersion.Text = "";
			}

			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["dempresa"]))
			{
				tbREEmpresa.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["dempresa"]).Trim();
			}
			else
			{
				tbREEmpresa.Text = "";
			}
			//TitularBeneficiario = RrSQLSOC("ITITUBEN")
			if (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["ITITUBEN"]) == "B")
			{
				rbIndicador[1].IsChecked = true;
				tbRENomTitular.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["DNOMBTIT"]);
			}
			else
			{
				if (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["ITITUBEN"]) == "T")
				{
					rbIndicador[0].IsChecked = true;
				}
			}

			//(maplaza)(23/08/2007)
			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["OCOBERTU"]))
			{
				tbRECobertura.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["OCOBERTU"]).Trim();
			}
			else
			{
				tbRECobertura.Text = "";
			}

			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["FINIVIGE"]))
			{ //(formato mes/a�o)
				mebREFechaAlta.Text = StringsHelper.Format(Convert.ToDateTime(RrSQLSOC.Tables[0].Rows[0]["FINIVIGE"]).Month, "00") + "/" + ((int) Convert.ToDateTime(RrSQLSOC.Tables[0].Rows[0]["FINIVIGE"]).Year).ToString();
			}
			else
			{
				mebREFechaAlta.Text = "  /    ";
			}

			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["FFINVIGE"]))
			{ //(formato mes/a�o)
				mebREFechaCaducidad.Text = StringsHelper.Format(Convert.ToDateTime(RrSQLSOC.Tables[0].Rows[0]["FFINVIGE"]).Month, "00") + "/" + ((int)Convert.ToDateTime(RrSQLSOC.Tables[0].Rows[0]["FFINVIGE"]).Year).ToString();
			}
			else
			{
				mebREFechaCaducidad.Text = "  /    ";
			}

			//------
			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["dbandtar"]))
			{
				TBBandaMagnetica.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["dbandtar"]).Trim();
			}
			else
			{
				TBBandaMagnetica.Text = "";
			}


		}

        private void CARGAR_SEGURIDADSOCIAL()
		{
			for (int i = 0; i <= cbbREInspeccion.ListCount - 1; i++)
			{
				object tempRefParam = 1;
				object tempRefParam2 = i;
				if (Convert.ToString(cbbREInspeccion.get_Column(tempRefParam, tempRefParam2)).Trim().ToUpper() == Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GINSPECC"]).Trim().ToUpper())
				{
					i = Convert.ToInt32(tempRefParam2);
					cbbREInspeccion.set_ListIndex(i);
					break;
				}
				else
				{
					i = Convert.ToInt32(tempRefParam2);
				}
			}
			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["NAFILIAC"]))
			{
				tbRENSS.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["NAFILIAC"]);
			}
			else
			{
				tbRENSS.Text = "";
			}
			TitularBeneficiario = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["ITITUBEN"]);
			if (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["ITITUBEN"]) == "B")
			{
				rbIndicadorSS[1].IsChecked = true;
				tbRENomTitularSS.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["DNOMBTIT"]);
			}
			else
			{
				if (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["ITITUBEN"]) == "T")
				{
					rbIndicadorSS[0].IsChecked = true;
				}
			}
			if (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["IACTPENS"]) == "A")
			{
				rbActivo.IsChecked = true;
			}
			if (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["IACTPENS"]) == "P")
			{
				rbPensionista.IsChecked = true;
			}
			chkZona.CheckState = 0;
			MEBCIAS.Text = "           ";
			if (!Convert.IsDBNull(RrSQLSOC.Tables[0].Rows[0]["GCODCIAS"]))
			{
				MEBCIAS.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["GCODCIAS"]);
				Restringir_Inspeccion(false);
				chkZona.Checked = (Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["ieszona"]) == "S");
			}

			//Oscar C Mayo 2010
			bool bEncontrado = false;
			for (int i = 0; i <= cbbHospitalReferencia.Items.Count - 1; i++)
			{
				if (RrSQLSOC.Tables[0].Rows[0]["ghospref"].ToString() != "" && int.Parse(cbbHospitalReferencia.Items[i].Value.ToString()) == Convert.ToDouble(RrSQLSOC.Tables[0].Rows[0]["ghospref"]))
				{
					cbbHospitalReferencia.SelectedIndex = i;
					bEncontrado = true;
					break;
				}
			}
			if (!bEncontrado)
			{
				cbbHospitalReferencia.SelectedIndex = -1;
			}
			switch(Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["iemplaza"]))
			{
				case "H" : 
					cbbEmplazamiento.SelectedIndex = 0; 
					break;
				case "D" : 
					cbbEmplazamiento.SelectedIndex = 1; 
					break;
				case "T" : 
					cbbEmplazamiento.SelectedIndex = 2; 
					break;
			}
			//----------

		}
				
		private void sdcDPFechaFallecimiento_Enter(Object eventSender, EventArgs eventArgs)
		{
			//Para que el formato de fecha sea D�a/Mes/A�o
			//    sdcDPFechaFallecimiento.Mask = DayMonthYear
			//    sdcDPFechaFallecimiento.Format = "DD/MM/YYYY"
		}

		private void sdcDPFechaFallecimiento_Leave(Object eventSender, EventArgs eventArgs)
		{
			sdcDPFechaFallecimiento.Refresh();
			COMPROBAR_DATOS();
			string mensaje = String.Empty;
			if (this.ActiveControl.Name != cbCancelar.Name)
			{
                //si esta activado el check de exitus obliga a introducir una fecha
                if (sdcDPFechaFallecimiento.Text == "")
                {
                    string tempRefParam = "";
                    short tempRefParam2 = 1040;
                    string[] tempRefParam3 = new string[]{"fecha de fallecimiento"};
					MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, tempRefParam, tempRefParam2, MFiliacion.GConexion, tempRefParam3);
					//sdcDPFechaFallecimiento_GotFocus
				}
			}
		}

        public void Buscar_poblaciones()
		{

			string SQLPOBLACION = String.Empty;
			bool bVaciar = false;

			//busca las poblaciones con ese codigo postal

			string stCodPoblacionAnt = "";
			string SQLCODPOSTAL = "select dcodipos.GPOBLACI, DPOBLACI from dcodipos, dpoblaci where dcodipos.gpoblaci = dpoblaci.gpoblaci";
			SQLCODPOSTAL = SQLCODPOSTAL + " and gcodipos = '" + mebDPCodPostal.Text.Trim() + "' order by dpoblaci";
			//-----
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLCODPOSTAL, MFiliacion.GConexion);
			DataSet RrSQLCODPOSTAL = new DataSet();
			tempAdapter.Fill(RrSQLCODPOSTAL);
			//si no encuentra ninguna poblacion
			if (RrSQLCODPOSTAL.Tables[0].Rows.Count == 0)
			{
				//SI NO ENCUENTRA POBLACIONES
				RrSQLCODPOSTAL.Close();
				//(maplaza)(30/05/2006)Si no se han encontrado poblaciones, y hab�a una poblaci�n seleccionada en el combo
				//anteriormente, habr� que desseleccionar esa poblaci�n
				cbbDPPoblacion.Text = "";
				cbbDPPoblacion.set_ListIndex(-1);
			}
			else
			{
				//SI ENCUENTRA LAS POBLACIONES
				if (RrSQLCODPOSTAL.Tables[0].Rows.Count > 1)
				{
					if (Codigoprov1 != StringsHelper.Format(Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Substring(0, Math.Min(2, Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Length)), "00"))
					{
						//(maplaza)(31/05/2006)Si en el combo no est�n actualmente las poblaciones de la provincia correspondiente,
						//se vac�a el combo
						cbbDPPoblacion.Clear();
						//-----
						SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + StringsHelper.Format(Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Substring(0, Math.Min(2, Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Length)), "00") + "%' order by dpoblaci";
						CargarComboAlfanumerico(SQLPOBLACION, "gpoblaci", "dpoblaci", cbbDPPoblacion);
					}
					//-----
					if (bFormatoCodigoPos)
					{
						//Jes�s 27/07/2011 Para Argentina no se hace este control
						//(maplaza)(13/04/2007)Puede darse el caso de que en el combo ya se encuentre seleccionada una poblaci�n correcta (es decir, que
						//concuerde con el C�digo Postal que aparece escrito).
						if ((Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) != -1) && (mebDPCodPostal.Text.Trim().Length == 5))
						{
							if ((mebDPCodPostal.Text != "     ") && (Convert.ToString(mebDPCodPostal.Tag) == mebDPCodPostal.Text))
							{
								object tempRefParam = 1;
								object tempRefParam2 = cbbDPPoblacion.get_ListIndex();
								stCodPoblacionAnt = Convert.ToString(cbbDPPoblacion.get_Column(tempRefParam, tempRefParam2));
							}
						}
					}
					//-----
					//SI ENCUENTRA LAS POBLACIONES
					bVaciar = true;
					CargarComboAlfanumerico(SQLCODPOSTAL, "gpoblaci", "dpoblaci", cbbDPPoblacion, bVaciar);
					//-----
					RrSQLCODPOSTAL.Close();

					//(maplaza)(13/04/2007)Puede darse el caso de que en el combo ya se encuentre seleccionada una poblaci�n correcta (es decir, que
					//concuerde con el C�digo Postal que aparece escrito). Si es as�, una vez cargados los datos en el combo de poblaciones, habr�a
					//que restaurar esa poblaci�n que estaba seleccionada antes.
					if (stCodPoblacionAnt != "")
					{
						for (int iContador = 0; iContador <= cbbDPPoblacion.ListCount - 1; iContador++)
						{
							object tempRefParam3 = 1;
							object tempRefParam4 = iContador;
							if (Convert.ToString(cbbDPPoblacion.get_Column(tempRefParam3, tempRefParam4)) == stCodPoblacionAnt)
							{
								iContador = Convert.ToInt32(tempRefParam4);
								cbbDPPoblacion.set_ListIndex(iContador);
								break;
							}
							else
							{
								iContador = Convert.ToInt32(tempRefParam4);
							}
						}
					}
					//-----
				}
				else
				{
					//SI ENCUENTRA UNA POBLACION
					//BUSCA LA POBLACION
					if (Codigoprov1 != StringsHelper.Format(Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Substring(0, Math.Min(2, Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Length)), "00"))
					{
						//(maplaza)(31/05/2006)Si en el combo no est�n actualmente las poblaciones de la provincia correspondiente,
						//se vac�a el combo
						cbbDPPoblacion.Clear();
						//-----
						SQLPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + StringsHelper.Format(Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Substring(0, Math.Min(2, Convert.ToString(RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"]).Length)), "00") + "%' order by dpoblaci";
						CargarComboAlfanumerico(SQLPOBLACION, "gpoblaci", "dpoblaci", cbbDPPoblacion);
					}
					for (int i = 0; i <= cbbDPPoblacion.ListCount - 1; i++)
					{
						object tempRefParam5 = 1;
						object tempRefParam6 = i;
						if (cbbDPPoblacion.get_Column(tempRefParam5, tempRefParam6) == RrSQLCODPOSTAL.Tables[0].Rows[0]["GPOBLACI"])
						{
							i = Convert.ToInt32(tempRefParam6);
							cbbDPPoblacion.set_ListIndex(i);
							break;
						}
						else
						{
							i = Convert.ToInt32(tempRefParam6);
						}
					}
					RrSQLCODPOSTAL.Close();
				}
			}
		}

		private void Desactivar_Cajas()
		{
			cbbREInspeccion.Enabled = false;
			tbRENSS.Enabled = false;
			cbbRESociedad.Enabled = false;
			tbRENPoliza.Enabled = false;
			chkExentoIVA.Enabled = false;
			tbRENVersion.Enabled = false;
			tbREEmpresa.Enabled = false;
			tbRENomTitular.Enabled = false;
			//(maplaza)(24/08/2007)
			tbRECobertura.Enabled = false;
			mebREFechaAlta.Enabled = false;
			mebREFechaCaducidad.Enabled = false;
			//-----
			tbREApe1.Enabled = false;
			tbREApe2.Enabled = false;
			tbRENombre.Enabled = false;
			tbNIFOtrosDocRE.Enabled = false; //(maplaza)(03/05/2006)
			tbREPasaporte.Enabled = false; //pasaporte
			tbREDomicilio.Enabled = false;
			mebRECodPostal.Enabled = false;
			cbbREPoblacion.Enabled = false;
			cbbREInspeccion.set_ListIndex(-1);
			tbRENSS.Text = "";
			cbbRESociedad.SelectedIndex = -1;
			bCambiandoMascara = true;
			tbRENPoliza.Mask = "";
			bCambiandoMascara = false;
			ToolTipMain.SetToolTip(tbRENPoliza, "");
			tbRENPoliza.Text = "";
			chkExentoIVA.CheckState = CheckState.Unchecked;
			tbRENVersion.Text = "";
			CodigoDeControl = -1;
			Algoritmo = -1;

			tbREEmpresa.Text = "";
			MEBCIAS.Text = "           ";
			tbRENomTitular.Text = "";
			//(maplaza)(24/08/2007)
			tbRECobertura.Text = "";
			mebREFechaAlta.Text = "  /    ";
			mebREFechaCaducidad.Text = "  /    ";
			//-----
			tbREApe1.Text = "";
			CodigoDeControl = -1;
			Algoritmo = -1;
			tbREApe2.Text = "";
			tbRENombre.Text = "";
			//(maplaza)(16/05/2006)
			if (tbNIFOtrosDocRE.Mask == MFiliacion.stFORMATO_NIF)
			{
				tbNIFOtrosDocRE.Text = "         ";
			}
			else
			{
				tbNIFOtrosDocRE.Text = "";
			}
			//-----
			tbREPasaporte.Text = ""; //pasaporte
			tbREDomicilio.Text = "";
			if (bFormatoCodigoPos)
			{
				mebRECodPostal.Text = "     ";
			}
			else
			{
				mebRECodPostal.Text = "";
			}
			cbbREPoblacion.set_ListIndex(-1);

			Colorear_Cajas(); //Oscar C Mayo 2010
		}

		private void tbPCApe1_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void tbPCApe2_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void tbPCNombre_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void tbPCTelf1_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			//COMPROBAR_DATOS
		}

		private void tbREApe1_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		//(maplaza)(12/05/2006)
		private void tbNIFOtrosDocDP_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		//(maplaza)(05/05/2006)
		private void tbNIFOtrosDocRE_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void tbREExPob_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbREExPob.SelectionStart = 0;
			tbREExPob.SelectionLength = tbREExPob.Text.Trim().Length;
		}

		//(maplaza)(10/08/2007)
		private void tbREDireccionEx_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbREDireccionEx.SelectionStart = 0;
			tbREDireccionEx.SelectionLength = tbREDireccionEx.Text.Trim().Length;
		}

		private void tbREExPob_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbRENombre_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void tbRENombre_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbRENombre.SelectionStart = 0;
			tbRENombre.SelectionLength = tbRENombre.Text.Trim().Length;
		}

		private void tbRENombre_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbRENomTitular_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void tbRENomTitular_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbRENomTitular.SelectionStart = 0;
			tbRENomTitular.SelectionLength = tbRENomTitular.Text.Trim().Length;
		}

		private void tbRENomTitular_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbRENPoliza_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			// Si lo que  estamos cambiando es la m�scara, no hacemos nada

			if (bCambiandoMascara)
			{
				return;
			}

			// Comprobamos la m�scara

			if (tbRENPoliza.Text.Trim() == "" || tbRENPoliza.Text == MascaraVacia())
			{

				// Si hay m�scara, ponemos la vac�a

				if (tbRENPoliza.Mask.Trim() != "" || ToolTipMain.GetToolTip(tbRENPoliza).Trim() != "")
				{
					bCambiandoMascara = true;
					tbRENPoliza.Text = MascaraVacia();
					string tempRefParam = ToolTipMain.GetToolTip(tbRENPoliza);
					tbRENPoliza.Mask = MascaraParaUsar(ref tempRefParam);
					bCambiandoMascara = false;
					tbRENPoliza.BackColor = Color.White;
				}

			}

			COMPROBAR_DATOS();

		}

		private void tbRENPoliza_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbRENPoliza.SelectionStart = 0;
			tbRENPoliza.SelectionLength = tbRENPoliza.Text.Trim().Length;
		}

		private void tbRENSS_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			COMPROBAR_DATOS();
		}

		private void tbRENSS_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbRENSS.SelectionStart = 0;
			tbRENSS.SelectionLength = tbRENSS.Text.Trim().Length;
		}

		private void tbSucursal_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbSucursal.SelectionStart = 0;
			tbSucursal.SelectionLength = tbSucursal.Text.Trim().Length;
		}

		private void tbSucursal_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8 && KeyAscii != 9)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbSucursal1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbSucursal1.SelectionStart = 0;
			tbSucursal1.SelectionLength = tbSucursal1.Text.Trim().Length;
		}

		private void tbSucursal1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8 && KeyAscii != 9)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbTarjeta_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (Strings.Len(tbTarjeta.Text) < 31 && Strings.Len(tbTarjeta.Text) > 0)
			{
				Valor_tarjeta_ant = tbTarjeta.Text;
			}
		}

		private void tbTarjeta_Enter(Object eventSender, EventArgs eventArgs)
		{
			if (tbTarjeta.Text.Trim() == "")
			{
				tbTarjeta.Text = "";
			}
			tbTarjeta.SelectionStart = 0;
			tbTarjeta.SelectionLength = tbTarjeta.Text.Trim().Length;
		}

		//(maplaza)(06/08/2007)
		private void tbTarjetaEuropea_Enter(Object eventSender, EventArgs eventArgs)
		{
			if (tbTarjetaEuropea.Text.Trim() == "")
			{
				tbTarjetaEuropea.Text = "";
			}
			tbTarjetaEuropea.SelectionStart = 0;
			tbTarjetaEuropea.SelectionLength = tbTarjetaEuropea.Text.Trim().Length;
		}

		private void tsDatosPac_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{

			if (tbPCApe1.Text == "")
			{
				if (tsDatosPacPreviousTab == 0)
				{
					if (tbDPDomicilioP.Text.Trim() != "")
					{
						tbPCDomicilio.Text = "";
						if (Convert.ToDouble(cbbDPVia.get_ListIndex()) != -1)
						{
							object tempRefParam = 0;
							object tempRefParam2 = cbbDPVia.get_ListIndex();
							tbPCDomicilio.Text = Convert.ToString(cbbDPVia.get_Column(tempRefParam, tempRefParam2)).Trim().ToUpper();
						}
						if (tbDPDomicilioP.Text != "")
						{
							tbPCDomicilio.Text = tbPCDomicilio.Text + " " + tbDPDomicilioP.Text.Trim().ToUpper();
						}
						if (tbDPNDomic.Text != "")
						{
							tbPCDomicilio.Text = tbPCDomicilio.Text + " " + tbDPNDomic.Text.Trim().ToUpper();
						}
						if (tbDPOtrosDomic.Text != "")
						{
							tbPCDomicilio.Text = tbPCDomicilio.Text + " " + tbDPOtrosDomic.Text.Trim().ToUpper();
						}
						tbPCDomicilio.Text = tbPCDomicilio.Text.Trim(); //por si se ha quedado alg�n espacio en blanco a la izquierda
					}
					//-----

					if (mebDPCodPostal.Text.Trim() != "")
					{
						mebPCCodPostal.Text = mebDPCodPostal.Text;
						//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
						mebPCCodPostal.Tag = mebPCCodPostal.Text;
						//-----
					}
					if (tbDPTelf1.Text.Trim() != "")
					{
						tbPCTelf1.Text = tbDPTelf1.Text;
					}
					if (tbDPTelf2.Text.Trim() != "")
					{
						tbPCTelf2.Text = tbDPTelf2.Text;
					}

					if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
					{
						if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
						{
							cbbPCPoblacion.Clear();
							object tempRefParam3 = "";
							object tempRefParam4 = Type.Missing;
							cbbPCPoblacion.AddItem(tempRefParam3, tempRefParam4);
							for (int i = 0; i <= cbbDPPoblacion.ListCount - 1; i++)
							{
								object tempRefParam6 = 0;
								object tempRefParam7 = i;
								object tempRefParam5 = cbbDPPoblacion.get_Column(tempRefParam6, tempRefParam7);
								object tempRefParam8 = i;
								cbbPCPoblacion.AddItem(tempRefParam5, tempRefParam8);
								i = Convert.ToInt32(tempRefParam8);
								i = Convert.ToInt32(tempRefParam7);
								object tempRefParam9 = 1;
								object tempRefParam10 = i;
								cbbPCPoblacion.set_List(i, 1, cbbDPPoblacion.get_Column(tempRefParam9, tempRefParam10));
								i = Convert.ToInt32(tempRefParam10);
							}
						}
						if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
						{
							for (int i = 1; i <= cbbDPPoblacion.ListCount - 1; i++)
							{
								object tempRefParam11 = 1;
								object tempRefParam12 = i;
								object tempRefParam13 = 1;
								object tempRefParam14 = cbbDPPoblacion.get_ListIndex();
								if (cbbPCPoblacion.get_Column(tempRefParam11, tempRefParam12).Equals(cbbDPPoblacion.get_Column(tempRefParam13, tempRefParam14)))
								{
									i = Convert.ToInt32(tempRefParam12);
									//(maplaza)(11/07/2006)no queremos que salte el evento click del combo "cbbPCPoblacion"
									estabuscando = true;
									cbbPCPoblacion.set_ListIndex(i);
									estabuscando = false;
									//-----
									break;
								}
								else
								{
									i = Convert.ToInt32(tempRefParam12);
								}
							}
						}
					}

					//(maplaza)(24/04/2007)Si no existe el C�digo Postal (en Base de Datos), pero los dos primeros d�gitos del
					//C�digo Postal s� corresponden a una provincia, entonces se cargan en el combo las poblaciones de la provincia.
					if (bFormatoCodigoPos)
					{
						if (mebPCCodPostal.Text.Trim().Length == 5)
						{
							if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
							{
								if (!fExisteCodigoPostal(mebPCCodPostal.Text.Trim()))
								{
									if (fExisteCodigoProvincia(mebPCCodPostal.Text.Trim().Substring(0, Math.Min(2, mebPCCodPostal.Text.Trim().Length))))
									{
										CargarPoblacionesProvincia(mebPCCodPostal.Text.Trim().Substring(0, Math.Min(2, mebPCCodPostal.Text.Trim().Length)), cbbPCPoblacion);
									}
								}
							}
						}
					}
					//-----

					if (tbDPExtension1.Text.Trim() != "")
					{
						tbPCExt1.Text = tbDPExtension1.Text;
					}
					if (tbDPExtension2.Text.Trim() != "")
					{
						tbPCExt2.Text = tbDPExtension2.Text;
					}
				}
			}

			if (cbbPCPais.Text == "" && tbPCExPob.Text == "")
			{
				if (tsDatosPacPreviousTab == 0)
				{
					if (chbDPExtrangero.CheckState == CheckState.Checked)
					{
						this.chbPCExtrangero.CheckState = CheckState.Checked;
					}
					if (Convert.ToDouble(cbbDPPais.get_ListIndex()) != -1)
					{
						cbbPCPais.set_ListIndex(cbbDPPais.get_ListIndex());
					}
					if (tbDPExPob.Text != "")
					{
						tbPCExPob.Text = tbDPExPob.Text;
					}
				}
			}

			if (SSTabHelper.GetSelectedIndex(tsDatosPac) == 2)
			{
				if (MFiliacion.stFiliacion == "MODIFICACION" && sprContactos.MaxRows == 0)
				{
					ObtenerPersonasContacto();
				}
			}

			if (SSTabHelper.GetSelectedIndex(tsDatosPac) == 3)
			{
				lbNombrePaciente.Text = (tbNombre.Text.Trim() + " " + tbApellido1.Text.Trim() + " " + tbApellido2.Text.Trim()).Trim().ToUpper();
			}

			COMPROBAR_DATOS();
			tsDatosPacPreviousTab = tsDatosPac.SelectedPage.Item.ZIndex;
		}

		//(maplaza)(11/07/2006)Se crea un procedimiento para cargar los datos por defecto en la pesta�a
		//de familiares del contacto (los datos por defecto dependen de los datos de la primera pesta�a)
		private void CargarDatosPorDefectoFamiliarContacto()
		{

			if (tbPCApe1.Text == "")
			{
				//Por defecto mueve los datos de la primera ficha a la tercera
				//(maplaza)(22/06/2006)En la pesta�a de "Familiar de Contacto" (tercera pesta�a), se debe presentar (por defecto)
				//la direcci�n completa, partiendo de los datos de la primera pesta�a.
				if (tbDPDomicilioP.Text.Trim() != "")
				{
					tbPCDomicilio.Text = "";
					if (Convert.ToDouble(cbbDPVia.get_ListIndex()) != -1)
					{
						object tempRefParam = 0;
						object tempRefParam2 = cbbDPVia.get_ListIndex();
						tbPCDomicilio.Text = Convert.ToString(cbbDPVia.get_Column(tempRefParam, tempRefParam2)).Trim().ToUpper();
					}
					if (tbDPDomicilioP.Text != "")
					{
						tbPCDomicilio.Text = tbPCDomicilio.Text + " " + tbDPDomicilioP.Text.Trim().ToUpper();
					}
					if (tbDPNDomic.Text != "")
					{
						tbPCDomicilio.Text = tbPCDomicilio.Text + " " + tbDPNDomic.Text.Trim().ToUpper();
					}
					if (tbDPOtrosDomic.Text != "")
					{
						tbPCDomicilio.Text = tbPCDomicilio.Text + " " + tbDPOtrosDomic.Text.Trim().ToUpper();
					}
					tbPCDomicilio.Text = tbPCDomicilio.Text.Trim(); //por si se ha quedado alg�n espacio en blanco a la izquierda
				}
				//-----

				if (mebDPCodPostal.Text.Trim() != "")
				{
					mebPCCodPostal.Text = mebDPCodPostal.Text;
					//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
					mebPCCodPostal.Tag = mebPCCodPostal.Text;
					//-----
				}

				if (tbDPTelf1.Text.Trim() != "")
				{
					tbPCTelf1.Text = tbDPTelf1.Text;
				}

				if (tbDPTelf2.Text.Trim() != "")
				{
					tbPCTelf2.Text = tbDPTelf2.Text;
				}

				if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
				{
					if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
					{
						cbbPCPoblacion.Clear();
						object tempRefParam3 = "";
						object tempRefParam4 = Type.Missing;
						cbbPCPoblacion.AddItem(tempRefParam3, tempRefParam4);
						for (int i = 0; i <= cbbDPPoblacion.ListCount - 1; i++)
						{
							object tempRefParam6 = 0;
							object tempRefParam7 = i;
							object tempRefParam5 = cbbDPPoblacion.get_Column(tempRefParam6, tempRefParam7);
							object tempRefParam8 = i;
							cbbPCPoblacion.AddItem(tempRefParam5, tempRefParam8);
							i = Convert.ToInt32(tempRefParam8);
							i = Convert.ToInt32(tempRefParam7);
							object tempRefParam9 = 1;
							object tempRefParam10 = i;
							cbbPCPoblacion.set_List(i, 1, cbbDPPoblacion.get_Column(tempRefParam9, tempRefParam10));
							i = Convert.ToInt32(tempRefParam10);
						}
					}
					if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
					{
						for (int i = 0; i <= cbbDPPoblacion.ListCount - 1; i++)
						{
							object tempRefParam11 = 1;
							object tempRefParam12 = i;
							object tempRefParam13 = 1;
							object tempRefParam14 = cbbDPPoblacion.get_ListIndex();
							if (cbbPCPoblacion.get_Column(tempRefParam11, tempRefParam12).Equals(cbbDPPoblacion.get_Column(tempRefParam13, tempRefParam14)))
							{
								i = Convert.ToInt32(tempRefParam12);
								//(maplaza)(11/07/2006)no queremos que salte el evento click del combo "cbbPCPoblacion"
								estabuscando = true;
								cbbPCPoblacion.set_ListIndex(i);
								estabuscando = false;
								//-----
								break;
							}
							else
							{
								i = Convert.ToInt32(tempRefParam12);
							}
						}
					}
				}

				if (tbDPExtension1.Text.Trim() != "")
				{
					tbPCExt1.Text = tbDPExtension1.Text;
				}

				if (tbDPExtension2.Text.Trim() != "")
				{
					tbPCExt2.Text = tbDPExtension2.Text;
				}

				if (cbbPCPais.Text == "" && tbPCExPob.Text == "")
				{
					if (chbDPExtrangero.CheckState == CheckState.Checked)
					{
						this.chbPCExtrangero.CheckState = CheckState.Checked;
					}
					if (Convert.ToDouble(cbbDPPais.get_ListIndex()) != -1)
					{
						cbbPCPais.set_ListIndex(cbbDPPais.get_ListIndex());
					}
					if (tbDPExPob.Text != "")
					{
						tbPCExPob.Text = tbDPExPob.Text;
					}
				}

				//(maplaza)(24/04/2007)Si no existe el C�digo Postal (en Base de Datos), pero los dos primeros d�gitos del
				//C�digo Postal s� corresponden a una provincia, entonces se cargan en el combo las poblaciones de la provincia.
				if (bFormatoCodigoPos)
				{
					if (mebPCCodPostal.Text.Trim().Length == 5)
					{
						if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
						{
							if (!fExisteCodigoPostal(mebPCCodPostal.Text.Trim()))
							{
								if (fExisteCodigoProvincia(mebPCCodPostal.Text.Trim().Substring(0, Math.Min(2, mebPCCodPostal.Text.Trim().Length))))
								{
									CargarPoblacionesProvincia(mebPCCodPostal.Text.Trim().Substring(0, Math.Min(2, mebPCCodPostal.Text.Trim().Length)), cbbPCPoblacion);
								}
							}
						}
					}
				}
				//-----
			}
		}

		private void Guardar_registros()
		{
            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {
                try
                {
                    if (MFiliacion.stFiliacion == "MODIFICACION")
                    {
                        MFiliacion.codigo = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]);
                        if (cambiar_identificador)
                        {
                            if (Permitir_cambiar_Gidenpac)
                            {
                                if (sdcFechaNac.Text != "")
                                {
                                    MFiliacion.codigo = "";
                                    Generar_Identificador();
                                    Identificador_Repetido();
                                    CAMBIO_DE_GIDENPAC();
                                }
                                else
                                {
                                    MFiliacion.codigo = "";
                                    GENERAR_IDENTIFICADOR_PROVISIONAL();
                                    IDENTIFICADOR_PROVISIONAL_REPETIDO();
                                    CAMBIO_DE_GIDENPAC();
                                }
                            }
                            else
                            {
                                CambiarDatosPaciente();

                                //            Set RrDPACIENT = GConexion.OpenResultset("SELECT * FROM DPACIENT WHERE GIDENPAC = '" & ModCodigo & "'", rdOpenKeyset, rdConcurValues)

                            }
                        }
                        else
                        {
                            MFiliacion.codigo = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]);
                        }
                    }
                    if (MFiliacion.stFiliacion == "ALTA")
                    {
                        if (cambiar_identificador)
                        {
                            MFiliacion.codigo = "";
                            if (sdcFechaNac.Text == "")
                            {
                                GENERAR_IDENTIFICADOR_PROVISIONAL();
                                IDENTIFICADOR_PROVISIONAL_REPETIDO();
                            }
                            else
                            {
                                Generar_Identificador();
                                Identificador_Repetido();
                            }
                        }
                        else
                        {
                            MFiliacion.codigo = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]);
                        }
                    }

                    SEGSOCIAL = false;
                    CSOCIEDAD = false;
                    string SQLSOC = String.Empty;
                    //Una vez aceptado se guardar�n los registros correspondientes
                    int Sociedad = 0;
                    string codigopaciente = String.Empty;
                    string SQLCODSOC = String.Empty;
                    DataSet RrSQLCODSOC = null;
                    string SQLPRIV = String.Empty;
                    if (MFiliacion.stFiliacion == "ALTA" || MFiliacion.stFiliacion == "MODIFICACION")
                    {
                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();
                        //ALTA O MODIFICACION EN DATOS DE FILIACION DE PACIENTES
                        if (MFiliacion.stFiliacion == "ALTA")
                        {
                            MFiliacion.RrDPACIENT.AddNew();
                            //RrDPACIENT("gusuario") = IIf(Trim(fnQueryRegistro("USUARIO")) = "", Null, Trim(fnQueryRegistro("USUARIO")))
                            MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gusuario"] = (Serrores.VVstUsuarioApli.Trim() == "") ? "" : Serrores.VVstUsuarioApli.Trim();
                            MFiliacion.RrDPACIENT.Tables[0].Rows[0]["FSISTEMA"] = DateTime.Now;
                        }
                        if (MFiliacion.stFiliacion == "MODIFICACION")
                        {
                            MFiliacion.RrDPACIENT.Edit();
                        }

                        ACTUALIZAR_DPACIENT();
                        string tempQuery = MFiliacion.RrDPACIENT.Tables[0].TableName;
                        //SqlDataAdapter tempAdapter = new SqlDataAdapter(tempQuery, MFiliacion.GConexion);
                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapterPaciente);
                        tempAdapterPaciente.Update(MFiliacion.RrDPACIENT, MFiliacion.RrDPACIENT.Tables[0].TableName);



                        //Se posiciona en el registro dado de alta
                        if (MFiliacion.stFiliacion == "ALTA")
                        {
                            MFiliacion.RrDPACIENT.MoveLast(null);
                        }


                        if (!ACTUALIZAR_DPERSCON())
                        {
                            //DGMORENOG_TODO_5_5 - VERIFICAR CONTROL ERRORES
                            //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();                            
                            //Serrores.GestionErrorBaseDatos(Information.Err().Number, MFiliacion.GConexion);
                            return;
                        }

                        //ALTA O MODIFICACION EN ENTIDADES FINANCIADORAS DEL PACIENTE

                        //SE COGE EL REGIMEN ECONOMICO Y LA IDENTIDAD DEL PACIENTE
                        if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]))
                        {
                            Sociedad = Convert.ToInt32(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]);
                        }
                        codigopaciente = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]);

                        //BUSCA EL TIPO DE REGIMEN ECONOMICO
                        //si es seguridad social

                        //20/04/2004 A partir de ahora graba en las tablas donde se han introducido datos enviando a quien lo pida
                        //la entidad que ha seleccionado en el regimen econ�mico.

                        //Si ha introducido seguridad social
                        if (Convert.ToDouble(cbbREInspeccion.get_ListIndex()) != -1 && cbbREInspeccion.Text != "")
                        {
                            SEGSOCIAL = true;
                            //BUSCA SI ESTA EN LA TABLA DE ENTIDADES
                            SQLSOC = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + codigopaciente + "' AND GSOCIEDA = " + MFiliacion.CodSS.ToString() + "";
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLSOC, MFiliacion.GConexion);
                            RrDENTIPAC = new DataSet();
                            tempAdapter_2.Fill(RrDENTIPAC);
                            tempAdapterRrDENTIPAC = tempAdapter_2;
                            if (RrDENTIPAC.Tables[0].Rows.Count != 0)
                            {
                                //ES MODIFICACION EN SEGURIDAD SOCIAL
                                RrDENTIPAC.Edit();
                                ACTUALIZAR_DENTIPAC(SEGSOCIAL);
                                string tempQuery_2 = RrDENTIPAC.Tables[0].TableName;
                                //SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery_2, "");
                                SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapterRrDENTIPAC);
                                tempAdapterRrDENTIPAC.Update(RrDENTIPAC, RrDENTIPAC.Tables[0].TableName);
                                RrDENTIPAC.Close();

                            }
                            else
                            {
                                // ES ALTA EN SEGURIDAD SOCIAL
                                SEGSOCIAL = true;
                                //ABRE LA TABLA DE ENTIDADES
                                SqlDataAdapter tempAdapter_4 = new SqlDataAdapter("SELECT * FROM DENTIPAC where 1 = 2", MFiliacion.GConexion);
                                RrDENTIPAC = new DataSet();
                                tempAdapter_4.Fill(RrDENTIPAC);
                                tempAdapterRrDENTIPAC = tempAdapter_4;
                                RrDENTIPAC.AddNew();
                                ACTUALIZAR_DENTIPAC(SEGSOCIAL);
                                string tempQuery_3 = RrDENTIPAC.Tables[0].TableName;
                                //SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery_3, "");
                                SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapterRrDENTIPAC);
                                tempAdapterRrDENTIPAC.Update(RrDENTIPAC, RrDENTIPAC.Tables[0].TableName);
                                RrDENTIPAC.Close();
                            }
                        }

                        //Si ha introducido privado
                        if (tbREApe1.Text.Trim() != "" || tbRENombre.Text.Trim() != "")
                        {
                            //BUSCA SI ESTA EN LA TABLA DE PRIVADO
                            SQLPRIV = "SELECT * FROM DPRIVADO WHERE  ";
                            SQLPRIV = SQLPRIV + "GIDENPAC = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "'";
                            SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQLPRIV, MFiliacion.GConexion);
                            RrDPRIVADO = new DataSet();
                            tempAdapter_6.Fill(RrDPRIVADO);
                            tempAdapterRrDPRIVADO = tempAdapter_6;
                            if (RrDPRIVADO.Tables[0].Rows.Count != 0)
                            { //ES MODIFICACION EN PRIVADO
                                RrDPRIVADO.Edit();
                                ACTUALIZAR_DPRIVADO();
                                string tempQuery_4 = RrDPRIVADO.Tables[0].TableName;
                                //SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(tempQuery_4, "");
                                SqlCommandBuilder tempCommandBuilder_4 = new SqlCommandBuilder(tempAdapterRrDPRIVADO);
                                tempAdapterRrDPRIVADO.Update(RrDPRIVADO, RrDPRIVADO.Tables[0].TableName);
                                RrDPRIVADO.Close();
                            }
                            else
                            {
                                // ES ALTA EN PRIVADO
                                SqlDataAdapter tempAdapter_8 = new SqlDataAdapter("SELECT * FROM DPRIVADO WHERE 1 = 2", MFiliacion.GConexion);
                                RrDPRIVADO = new DataSet();
                                tempAdapter_8.Fill(RrDPRIVADO);
                                tempAdapterRrDPRIVADO = tempAdapter_8;
                                RrDPRIVADO.AddNew();
                                ACTUALIZAR_DPRIVADO();
                                string tempQuery_5 = RrDPRIVADO.Tables[0].TableName;
                                //SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(tempQuery_5, "");
                                SqlCommandBuilder tempCommandBuilder_5 = new SqlCommandBuilder(tempAdapterRrDPRIVADO);
                                tempAdapterRrDPRIVADO.Update(RrDPRIVADO, RrDPRIVADO.Tables[0].TableName);
                                RrDPRIVADO.Close();
                            }
                        }
                        //Si ha introducido Sociedad
                        if (cbbRESociedad.SelectedIndex != -1 && cbbRESociedad.Text.Trim() != "")
                        { //BUSCA SI ESTA EN LA TABLA DE SOCIEDADES
                            SEGSOCIAL = false;
                            SQLSOC = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + codigopaciente + "' AND GSOCIEDA = " + cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString() + "";
                            SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(SQLSOC, MFiliacion.GConexion);
                            RrDENTIPAC = new DataSet();
                            tempAdapter_10.Fill(RrDENTIPAC);
                            tempAdapterRrDENTIPAC = tempAdapter_10;
                            if (RrDENTIPAC.Tables[0].Rows.Count != 0)
                            { //ES UNA MODIFOCACION EN SOCIEDADES
                                RrDENTIPAC.Edit();
                                ACTUALIZAR_DENTIPAC(SEGSOCIAL);
                                string tempQuery_6 = RrDENTIPAC.Tables[0].TableName;
                                //SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(tempQuery_6, "");
                                SqlCommandBuilder tempCommandBuilder_6 = new SqlCommandBuilder(tempAdapterRrDENTIPAC);
                                tempAdapterRrDENTIPAC.Update(RrDENTIPAC, RrDENTIPAC.Tables[0].TableName);
                                RrDENTIPAC.Close();
                            }
                            else
                            {
                                //ES UN ALTA EN SOCIEDADES
                                SqlDataAdapter tempAdapter_12 = new SqlDataAdapter("SELECT * FROM DENTIPAC WHERE 1 = 2", MFiliacion.GConexion);
                                RrDENTIPAC = new DataSet();
                                tempAdapter_12.Fill(RrDENTIPAC);
                                tempAdapterRrDENTIPAC = tempAdapter_12;
                                RrDENTIPAC.AddNew();
                                ACTUALIZAR_DENTIPAC(SEGSOCIAL);
                                string tempQuery_7 = RrDENTIPAC.Tables[0].TableName;
                                //SqlDataAdapter tempAdapter_13 = new SqlDataAdapter(tempQuery_7, "");
                                SqlCommandBuilder tempCommandBuilder_7 = new SqlCommandBuilder(tempAdapterRrDENTIPAC);
                                tempAdapterRrDENTIPAC.Update(RrDENTIPAC, RrDENTIPAC.Tables[0].TableName);
                                RrDENTIPAC.Close();
                            }
                        }                        
                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.CommitTrans();
                        scope.Complete();
                    }
                }
                catch (SqlException ex)
                {                    
                    Serrores.GestionErrorBaseDatos(ex, MFiliacion.GConexion);
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
		}


		private void ACTUALIZAR_DPACIENT()
		{
			//IDENTIDAD DEL PACIENTE
			if (MFiliacion.stFiliacion == "ALTA")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"] = MFiliacion.codigo.ToUpper();
			}

			//'' comprobamos si existe registro en NIDO y han cambiado el nombre el paciente para cambiarlo en NEPISNEO
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select * from NEPISNEO where gidenpac = '" + MFiliacion.codigo + "'", MFiliacion.GConexion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrSql.Tables[0].Rows[0]["dnombreo"]).Trim().ToUpper() != tbNombre.Text.Trim().ToUpper())
				{
					RrSql.Edit();
					RrSql.Tables[0].Rows[0]["dnombreo"] = tbNombre.Text.Trim().ToUpper();
					string tempQuery = RrSql.Tables[0].TableName;
					//SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
					tempAdapter.Update(RrSql, RrSql.Tables[0].TableName);
				}
			}
			RrSql.Close();

			//''fin nepisneo

			if (MFiliacion.stFiliacion == "MODIFICACION")
			{
				//    RrDPACIENT("GIDENPAC") = CODIGO
			}
			MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DNOMBPAC"] = tbNombre.Text.Trim().ToUpper();
			//NOMBRE, APELLIDOS, FECHA DE NACIMIENTO
			if (MFiliacion.stFiliacion == "ALTA")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE1PAC"] = tbApellido1.Text.Trim().ToUpper();
				if (tbApellido2.Text.Trim().ToUpper() != "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE2PAC"] = tbApellido2.Text.Trim().ToUpper();
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE2PAC"] = DBNull.Value;
				}
				if (sdcFechaNac.Text != "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["FNACIPAC"] = sdcFechaNac.Text.Trim().ToUpper();
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["FNACIPAC"] = DBNull.Value;
				}
				//SEXO
				if (rbSexo[0].IsChecked)
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ITIPSEXO"] = MFiliacion.CodSHombre.ToUpper();
				}

				if (rbSexo[1].IsChecked)
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ITIPSEXO"] = MFiliacion.CodSMUJER.ToUpper();
				}

				if (!rbSexo[0].IsChecked && !rbSexo[1].IsChecked)
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ITIPSEXO"] = MFiliacion.CodSINDET.ToUpper();
				}
			}
			//NIF, DIRECCION, CODIGO POSTAL, CODIGO POBLACION, POBLACION,
			//TELF1, TELF2
			if (tbNIFOtrosDocDP.Text.Trim() != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"] = tbNIFOtrosDocDP.Text.Trim().ToUpper();
				if (NIF_Antiguo != "")
				{
					NIF_Antiguo = tbNIFOtrosDocDP.Text.Trim().ToUpper();
				}
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"] = DBNull.Value;
			}

			//TARJETA SANITARIA
			if (tbTarjeta.Text.Trim() != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ntarjeta"] = tbTarjeta.Text.Trim();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ntarjeta"] = DBNull.Value;
			}
			//(maplaza)(06/08/2007)
			//TARJETA SANITARIA EUROPEA
			if (tbTarjetaEuropea.Text.Trim() != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ntarjsae"] = tbTarjetaEuropea.Text.Trim();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ntarjsae"] = DBNull.Value;
			}
			//-----

			if (CipAutonomico)
			{
				if (lbCipAutonomico.Text.Trim() != "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ocipauto"] = lbCipAutonomico.Text.Trim();
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ocipauto"] = DBNull.Value;
				}
			}
			else
			{
				if (lbCipAutonomico.Text.Trim() != "" && (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ncipauto"]) + "").Trim() == "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ncipauto"] = lbCipAutonomico.Text.Trim();
				}
			}

			if (cbbNacPais.Text.Trim() != "")
			{
				object tempRefParam = 1;
				object tempRefParam2 = cbbNacPais.get_ListIndex();
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gpaisnac"] = cbbNacPais.get_Column(tempRefParam, tempRefParam2);
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gpaisnac"] = DBNull.Value;
			}


			tbDPDomicilio.Text = "";
			if (Convert.ToDouble(cbbDPVia.get_ListIndex()) != -1)
			{
				object tempRefParam3 = 1;
				object tempRefParam4 = cbbDPVia.get_ListIndex();
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gtipovia"] = cbbDPVia.get_Column(tempRefParam3, tempRefParam4);
				object tempRefParam5 = 0;
				object tempRefParam6 = cbbDPVia.get_ListIndex();
				tbDPDomicilio.Text = Convert.ToString(cbbDPVia.get_Column(tempRefParam5, tempRefParam6)).Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gtipovia"] = DBNull.Value;
			}
			if (tbDPDomicilioP.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDOMICIL"] = tbDPDomicilioP.Text.Trim().ToUpper();
				tbDPDomicilio.Text = tbDPDomicilio.Text + " " + tbDPDomicilioP.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDOMICIL"] = DBNull.Value;
			}

			//OSCAR C ENE 2005
			if (tbObservaciones.Text.Trim() != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["OOBSERVA"] = tbObservaciones.Text.Trim();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["OOBSERVA"] = DBNull.Value;
			}
			//-------------
			MFiliacion.RrDPACIENT.Tables[0].Rows[0]["iverobse"] = (chkObservaciones.CheckState == CheckState.Checked) ? "S" : "N";

			if (tbDPNDomic.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NNUMEVIA"] = tbDPNDomic.Text.Trim().ToUpper();
				tbDPDomicilio.Text = tbDPDomicilio.Text + " " + tbDPNDomic.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NNUMEVIA"] = DBNull.Value;
			}
			if (tbDPOtrosDomic.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DOTROVIA"] = tbDPOtrosDomic.Text.Trim().ToUpper();
				tbDPDomicilio.Text = tbDPDomicilio.Text + " " + tbDPOtrosDomic.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DOTROVIA"] = DBNull.Value;
			}

			if (tbDPDomicilio.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"] = tbDPDomicilio.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"] = DBNull.Value;
			}

			if (mebDPCodPostal.Text == "" || Conversion.Val(mebDPCodPostal.Text) == 0)
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCODIPOS"] = DBNull.Value;
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCODIPOS"] = mebDPCodPostal.Text;
			}

			//SI LA RESIDENCIA ES NACIONAL
			//(maplaza)(08/08/2007)Se independiza el check de residencia en el extranjero de todos los controles de Filiaci�n.

			string CODIGOPROVINCIA = String.Empty;
			string SQLPROV = String.Empty;
			DataSet RrSQLProv = null;
			if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) > -1)
			{
				//si el codigo de provincia no ha cambiado
				object tempRefParam7 = 1;
				object tempRefParam8 = cbbDPPoblacion.get_ListIndex();
				CODIGOPROVINCIA = StringsHelper.Format(cbbDPPoblacion.get_Column(tempRefParam7, tempRefParam8), "000000").Substring(0, Math.Min(2, StringsHelper.Format(cbbDPPoblacion.get_Column(tempRefParam7, tempRefParam8), "000000").Length));
				//si el codigo de provincia no ha cambiado
				if (CODIGOPROVINCIA == MFiliacion.CodPROV)
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"] = MFiliacion.CodPROV;
					object tempRefParam9 = 1;
					object tempRefParam10 = cbbDPPoblacion.get_ListIndex();
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"] = StringsHelper.Format(cbbDPPoblacion.get_Column(tempRefParam9, tempRefParam10), "000000");
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DPOBLACI"] = DBNull.Value;
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPAISRES"] = DBNull.Value;
				}
				else
				{
					//BUSCA EL CODIGO DE PROVINCIA CORRESPONDIENTE A ESA POBLACION
					//BUSCA LA PROVINCIA
					SQLPROV = "SELECT GPROVINC FROM DPROVINC WHERE GPROVINC = '" + CODIGOPROVINCIA + "'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLPROV, MFiliacion.GConexion);
					RrSQLProv = new DataSet();
					tempAdapter_3.Fill(RrSQLProv);
					if (RrSQLProv.Tables[0].Rows.Count != 0)
					{
						MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"] = RrSQLProv.Tables[0].Rows[0]["GPROVINC"];
						RrSQLProv.Close();
					}
					object tempRefParam11 = 1;
					object tempRefParam12 = cbbDPPoblacion.get_ListIndex();
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"] = StringsHelper.Format(cbbDPPoblacion.get_Column(tempRefParam11, tempRefParam12), "000000");
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DPOBLACI"] = DBNull.Value;
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPAISRES"] = DBNull.Value;
				}
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"] = DBNull.Value;
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"] = DBNull.Value;
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DPOBLACI"]))
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DPOBLACI"] = DBNull.Value;
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPAISRES"] = DBNull.Value;
				}
			}



			//SI LA RESIDENCIA ES EXTRANJERO
			if (chbDPExtrangero.CheckState == CheckState.Checked)
			{
				if (Convert.ToDouble(cbbDPPais.get_ListIndex()) != -1)
				{
					if (tbDPExPob.Text.Trim() != "")
					{
						MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DPOBLACI"] = tbDPExPob.Text;
					}
					else
					{
						MFiliacion.RrDPACIENT.Tables[0].Rows[0]["dpoblaci"] = DBNull.Value;
					}
					//(maplaza)(09/08/2007)Campo "ddireext" (direcci�n en el extranjero)
					if (tbDPDireccionEx.Text.Trim() != "")
					{
						MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ddireext"] = tbDPDireccionEx.Text.Trim();
					}
					else
					{
						MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ddireext"] = DBNull.Value;
					}
					//------
					object tempRefParam13 = 1;
					object tempRefParam14 = cbbDPPais.get_ListIndex();
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPAISRES"] = cbbDPPais.get_Column(tempRefParam13, tempRefParam14);
				}
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPAISRES"] = DBNull.Value;
			}
			if (Convert.ToDouble(cbbNacPoblacion.get_ListIndex()) >= 0)
			{
				object tempRefParam15 = 1;
				object tempRefParam16 = cbbNacPoblacion.get_ListIndex();
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gpobnaci"] = cbbNacPoblacion.get_Column(tempRefParam15, tempRefParam16);
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gpobnaci"] = DBNull.Value;
			}

			//PASAPORTE
			if (tbDPPasaporte.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NPASAPOR"] = tbDPPasaporte.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NPASAPOR"] = DBNull.Value;
			}

			//TELEFONOS 1 Y 2 ,EXTENSIONES 1 Y 2
			if (tbDPTelf1.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO1"] = tbDPTelf1.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO1"] = DBNull.Value;
			}

			if (tbDPTelf2.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO2"] = tbDPTelf2.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO2"] = DBNull.Value;
			}

			if (tbDPTelf3.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO3"] = tbDPTelf3.Text.Trim().ToUpper();
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ienvisms"] = (chbSMS.CheckState == CheckState.Checked) ? "S" : "N";
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO3"] = DBNull.Value;
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ienvisms"] = DBNull.Value;
			}

			if (tbDPExtension1.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI1"] = tbDPExtension1.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI1"] = DBNull.Value;
			}

			if (tbDPExtension2.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI2"] = tbDPExtension2.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI2"] = DBNull.Value;
			}

			if (tbDPExtension3.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI3"] = tbDPExtension3.Text.Trim().ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI3"] = DBNull.Value;
			}

			if (tbEmail.Text.Trim() != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ddiemail"] = tbEmail.Text.Trim();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ddiemail"] = DBNull.Value;
			}

			//ESTADO CIVIL
			//BUSCA EL CODIGO ASOCIADO AL ESTADO CIVIL
			if (Convert.ToDouble(cbbDPEstado.get_ListIndex()) > -1)
			{
				object tempRefParam17 = 1;
				object tempRefParam18 = cbbDPEstado.get_ListIndex();
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GESTACIV"] = cbbDPEstado.get_Column(tempRefParam17, tempRefParam18);
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GESTACIV"] = DBNull.Value;
			}

			//CATALOGACION SOCIAL
			if (Convert.ToDouble(cbbDPCatSoc.get_ListIndex()) > -1)
			{
				object tempRefParam19 = 1;
				object tempRefParam20 = cbbDPCatSoc.get_ListIndex();
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCATASOC"] = cbbDPCatSoc.get_Column(tempRefParam19, tempRefParam20);
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCATASOC"] = DBNull.Value;
			}

			//REGIMEN ECONOMICO
			//SEGURIDAD SOCIAL
			if (rbRERegimen[0].IsChecked)
			{
				//CODIGO DE LA SEGURIDAD SOCIAL
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"] = MFiliacion.CodSS;
				if (tbRENSS.Text == "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"] = "";
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"] = tbRENSS.Text;
				}
			}

			//SOCIEDAD
			if (rbRERegimen[1].IsChecked)
			{
				//CODIGO DE LA SOCIEDAD
				if (cbbRESociedad.SelectedIndex != -1)
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"] = cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString();
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"] = DBNull.Value;
				}

				if (tbRENPoliza.Text != "" && tbRENPoliza.Text != MascaraVacia())
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"] = tbRENPoliza.Text.Trim();
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"] = DBNull.Value;
				}
			}
			if (cbbRESociedad.SelectedIndex != -1 && cbbRESociedad.Text.Trim() != "")
			{
				if (tbREEmpresa.Text != "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["dempresa"] = tbREEmpresa.Text.Trim().ToUpper();
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["dempresa"] = DBNull.Value;
				}
			}
			//PRIVADO
			if (rbRERegimen[2].IsChecked)
			{
				//CODIGO DE REGIMEN PRIVADO
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"] = MFiliacion.CodPriv;
				//(maplaza)(03/05/2006)
				if (tbNIFOtrosDocRE.Text.Trim() != "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"] = tbNIFOtrosDocRE.Text.Trim();
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"] = DBNull.Value;
				}
			}

			if (!rbRERegimen[0].IsChecked && !rbRERegimen[1].IsChecked && !rbRERegimen[2].IsChecked)
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"] = DBNull.Value;
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"] = DBNull.Value;
			}
			//autorizaci�n utilizaci�n datos
			if (rbAutorizacion[0].IsChecked)
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["IAUTUTIL"] = "S";
			}
			else
			{
				if (rbAutorizacion[1].IsChecked)
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["IAUTUTIL"] = "N";
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["IAUTUTIL"] = DBNull.Value;
				}
			}


			//EXITUS
			if (chbDPExitus.CheckState == CheckState.Unchecked)
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["IEXITUSP"] = "N";
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["FFALLECI"] = DBNull.Value;
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["IEXITUSP"] = "S";
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["FFALLECI"] = sdcDPFechaFallecimiento.Text;
				Actualizar_Exitus();
			}

			//MOROSO (Oscar C 27/10/04)
			if (rbBloqueo[0].IsChecked)
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["Ibloqueo"] = "S";
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["Ibloqueo"] = "N";
			}
			//----------

			//CAMPO INEXISTENTE EN LOS FORMULARIOS
			MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gusuario"] = (Serrores.VVstUsuarioApli.Trim() == "") ? "" : Serrores.VVstUsuarioApli.Trim();
			MFiliacion.RrDPACIENT.Tables[0].Rows[0]["FULTMOVI"] = DateTime.Now;
			MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ifilprov"] = "N";

			if (IdiomaDelPaciente == "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["iidioma"] = DBNull.Value;
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["iidioma"] = IdiomaDelPaciente;
			}

			//Oscar C Diciembre 2010 -- Origen Info
			if (cbbOrigInfo.SelectedIndex > 0)
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gorigfil"] = cbbOrigInfo.Items[cbbOrigInfo.SelectedIndex].Value.ToString();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gorigfil"] = DBNull.Value;
			}

			//(maplaza)(06/08/2007)

			//N�mero de Trabajador
			if (tbNumTrabajador.Text.Trim() != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ntrabaja"] = tbNumTrabajador.Text.Trim();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ntrabaja"] = DBNull.Value;
			}

			//Parentesco con el Trabajador
			object tempRefParam21 = 1;
			object tempRefParam22 = cbbParentesco.get_ListIndex();
			if (Convert.IsDBNull(cbbParentesco.get_Column(tempRefParam21, tempRefParam22)))
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gparetra"] = DBNull.Value;
			}
			else
			{
				object tempRefParam23 = 1;
				object tempRefParam24 = cbbParentesco.get_ListIndex();
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gparetra"] = cbbParentesco.get_Column(tempRefParam23, tempRefParam24);
			}

			//C�digo de Categorizaci�n Especial
			object tempRefParam25 = 1;
			object tempRefParam26 = cbbCategoria.get_ListIndex();
			if (Convert.IsDBNull(cbbCategoria.get_Column(tempRefParam25, tempRefParam26)))
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gcatespe"] = DBNull.Value;
			}
			else
			{
				object tempRefParam27 = 1;
				object tempRefParam28 = cbbCategoria.get_ListIndex();
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gcatespe"] = cbbCategoria.get_Column(tempRefParam27, tempRefParam28);
			}
			//-----

			// Grado de dependencia

			if (Information.IsDate(sdcGradoDep[0].Text))
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg1n1"] = DateTime.Parse(sdcGradoDep[0].Value.ToString("dd/MM/yyyy"));
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg1n1"] = DBNull.Value;
			}

			if (Information.IsDate(sdcGradoDep[1].Text))
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg1n2"] = DateTime.Parse(sdcGradoDep[1].Value.ToString("dd/MM/yyyy"));
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg1n2"] = DBNull.Value;
			}

			if (Information.IsDate(sdcGradoDep[2].Text))
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg2n1"] = DateTime.Parse(sdcGradoDep[2].Value.ToString("dd/MM/yyyy"));
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg2n1"] = DBNull.Value;
			}

			if (Information.IsDate(sdcGradoDep[3].Text))
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg2n2"] = DateTime.Parse(sdcGradoDep[3].Value.ToString("dd/MM/yyyy"));
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg2n2"] = DBNull.Value;
			}

			if (Information.IsDate(sdcGradoDep[4].Text))
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg3n1"] = DateTime.Parse(sdcGradoDep[4].Value.ToString("dd/MM/yyyy"));
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg3n1"] = DBNull.Value;
			}

			if (Information.IsDate(sdcGradoDep[5].Text))
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg3n2"] = DateTime.Parse(sdcGradoDep[5].Value.ToString("dd/MM/yyyy"));
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg3n2"] = DBNull.Value;
			}

			if (tbOtrasCircunstanciasGradoDep.Text.Trim() != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["oobsgdep"] = tbOtrasCircunstanciasGradoDep.Text;
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["oobsgdep"] = DBNull.Value;
			}

			//Oscar C Mayo 2010
			//Control de Identificacion Basica del Paciente
			if (bControlIDBas)
			{
				if (bPacienteIdBas)
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ifilbasi"] = "S";
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gmotifil"] = DBNull.Value;
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["omotifil"] = DBNull.Value;
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ifilbasi"] = "N";
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gmotifil"] = iMotivoFiliacion;
					MFiliacion.RrDPACIENT.Tables[0].Rows[0]["omotifil"] = (oMotivoFiliacion.Trim() == "") ? "" : oMotivoFiliacion.Trim();
				}
			}
			//-----------


			//Canal de preferencia
			if (cbbCanPrev.SelectedIndex == -1)
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gcanotic"] = DBNull.Value;
			}
			else
			{
				int tempRefParam29 = 1;
				int tempRefParam30 = cbbCanPrev.SelectedIndex;
				MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gcanotic"] = getLisIndex(cbbCanPrev,tempRefParam30, tempRefParam29);
			}
			//-----


		}

		//********************************************************************************************************
		//*                                                                                                      *
		//*  Procedimiento: ACTUALIZAR_DENTIPAC                                                                  *
		//*                                                                                                      *
		//*  Modificaciones:                                                                                     *
		//*                                                                                                      *
		//*      O.Frias ( 08/01/2007) Si el regimen economico es Seguridad Social se actualizar� el valor del   *
		//*                            campo ieszona                                                             *
		//*                                                                                                      *
		//********************************************************************************************************
		private void ACTUALIZAR_DENTIPAC(bool SEGSOCIAL)
		{
			RrDENTIPAC.Tables[0].Rows[0]["GIDENPAC"] = MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"];

			//REGIMEN ECONOMICO(SEGURIDAD SOCIAL)
			System.DateTime dtFechaCaducidad = DateTime.FromOADate(0);
			if (SEGSOCIAL)
			{

				//guarda el codigo de la seguridad social
				RrDENTIPAC.Tables[0].Rows[0]["GSOCIEDA"] = MFiliacion.CodSS; //RrDPACIENT("gsocieda")
				//codigo del hospital
				if (Convert.ToDouble(cbbREInspeccion.get_ListIndex()) != -1)
				{
					object tempRefParam = 1;
					object tempRefParam2 = cbbREInspeccion.get_ListIndex();
					RrDENTIPAC.Tables[0].Rows[0]["GINSPECC"] = cbbREInspeccion.get_Column(tempRefParam, tempRefParam2);
				}
				else
				{
					RrDENTIPAC.Tables[0].Rows[0]["GINSPECC"] = DBNull.Value;
				}
				if (tbRENSS.Text.Trim() != "")
				{
					RrDENTIPAC.Tables[0].Rows[0]["NAFILIAC"] = tbRENSS.Text;
				}
				else
				{
					RrDENTIPAC.Tables[0].Rows[0]["NAFILIAC"] = "";
				}

				//********************* O.Frias (08/01/2007) *********************
				RrDENTIPAC.Tables[0].Rows[0]["ieszona"] = (Convert.ToBoolean(chkZona.Checked)) ? "S" : "N";
				//********************* O.Frias (08/01/2007) *********************

				//O.Frias - 06/08/2009
				//Nuevo campo Emplazamiento.

				switch(cbbEmplazamiento.SelectedIndex)
				{
					case 0 : 
						RrDENTIPAC.Tables[0].Rows[0]["iemplaza"] = "H"; 
						break;
					case 1 : 
						RrDENTIPAC.Tables[0].Rows[0]["iemplaza"] = "D"; 
						break;
					case 2 : 
						RrDENTIPAC.Tables[0].Rows[0]["iemplaza"] = "T"; 
						break;
				}


				//OSCAR C Noviembre 2009
				//Hospital de Referencia
				if (cbbHospitalReferencia.SelectedIndex > 0)
				{
					RrDENTIPAC.Tables[0].Rows[0]["ghospref"] = cbbHospitalReferencia.Items[cbbHospitalReferencia.SelectedIndex].Value.ToString();
				}
				else
				{
					RrDENTIPAC.Tables[0].Rows[0]["ghospref"] = DBNull.Value;
				}
				//----------------------

				if (chkExentoIVA.Visible)
				{
					RrDENTIPAC.Tables[0].Rows[0]["icondiva"] = "S";
				}
				else
				{
					RrDENTIPAC.Tables[0].Rows[0]["icondiva"] = DBNull.Value;
				}
			}
			else
			{
				//REGIMEN ECONOMICO(SOCIEDAD)
				if (!SEGSOCIAL)
				{
					RrDENTIPAC.Tables[0].Rows[0]["GSOCIEDA"] = cbbRESociedad.Items[cbbRESociedad.SelectedIndex].Value.ToString();

					if (tbRENPoliza.Text == "" || tbRENPoliza.Text == MascaraVacia())
					{
						RrDENTIPAC.Tables[0].Rows[0]["NAFILIAC"] = DBNull.Value;
					}
					else
					{
						RrDENTIPAC.Tables[0].Rows[0]["NAFILIAC"] = tbRENPoliza.Text;
					}

					if (chkExentoIVA.Visible)
					{
						if (chkExentoIVA.CheckState != CheckState.Unchecked)
						{
							RrDENTIPAC.Tables[0].Rows[0]["icondiva"] = "S";
						}
						else
						{
							RrDENTIPAC.Tables[0].Rows[0]["icondiva"] = "N";
						}
					}
					else
					{
						RrDENTIPAC.Tables[0].Rows[0]["icondiva"] = DBNull.Value;
					}

					if (tbRENVersion.Text == "")
					{
						RrDENTIPAC.Tables[0].Rows[0]["nversion"] = DBNull.Value;
					}
					else
					{
						RrDENTIPAC.Tables[0].Rows[0]["nversion"] = tbRENVersion.Text;
					}

					//********************* O.Frias (08/01/2007) *********************
					RrDENTIPAC.Tables[0].Rows[0]["ieszona"] = DBNull.Value;
					//********************* O.Frias (08/01/2007) *********************

					//(maplaza)(23/08/2007)
					if (tbRECobertura.Text.Trim() == "")
					{
						RrDENTIPAC.Tables[0].Rows[0]["OCOBERTU"] = DBNull.Value;
					}
					else
					{
						RrDENTIPAC.Tables[0].Rows[0]["OCOBERTU"] = tbRECobertura.Text.Trim();
					}

					if (mebREFechaAlta.Text == "  /    ")
					{
						RrDENTIPAC.Tables[0].Rows[0]["FINIVIGE"] = DBNull.Value;
					}
					else
					{
						//se coge el primer d�a del mes
						System.DateTime TempDate = DateTime.FromOADate(0);
						RrDENTIPAC.Tables[0].Rows[0]["FINIVIGE"] = DateTime.Parse((DateTime.TryParse("01/" + mebREFechaAlta.Text.Trim() + " 00:00:01", out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : "01/" + mebREFechaAlta.Text.Trim() + " 00:00:01");
					}

					if (mebREFechaCaducidad.Text == "  /    ")
					{
						RrDENTIPAC.Tables[0].Rows[0]["FFINVIGE"] = DBNull.Value;
					}
					else
					{
						//se coge el �ltimo d�a del mes
						dtFechaCaducidad = DateTime.FromOADate(0);
						System.DateTime TempDate2 = DateTime.FromOADate(0);
						dtFechaCaducidad = DateTime.Parse((DateTime.TryParse("01/" + mebREFechaCaducidad.Text.Trim() + " 23:59:59", out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm:ss") : "01/" + mebREFechaCaducidad.Text.Trim() + " 23:59:59").AddMonths(1);
						dtFechaCaducidad = dtFechaCaducidad.AddDays(-1);
						RrDENTIPAC.Tables[0].Rows[0]["FFINVIGE"] = dtFechaCaducidad;
					}
					if (TBBandaMagnetica.Text != "")
					{
						RrDENTIPAC.Tables[0].Rows[0]["dbandtar"] = TBBandaMagnetica.Text;
					}
					else
					{
						RrDENTIPAC.Tables[0].Rows[0]["dbandtar"] = DBNull.Value;
					}
					//------
				}
			}

			//INDICADOR DE TITULAR O BENEFICIARIO

			if (!SEGSOCIAL)
			{ //Si no es seguridad social va a los indicadores de sociedad
				if (rbIndicador[0].IsChecked)
				{ //SI ES TITULAR
					RrDENTIPAC.Tables[0].Rows[0]["ITITUBEN"] = "T";
					RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"] = DBNull.Value;
				}
				else
				{
					//SI ES BENEFICIARIO
					if (rbIndicador[1].IsChecked)
					{
						RrDENTIPAC.Tables[0].Rows[0]["ITITUBEN"] = "B";
						RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"] = tbRENomTitular.Text;
					}
				}
				if (!rbIndicador[0].IsChecked && !rbIndicador[1].IsChecked)
				{
					RrDENTIPAC.Tables[0].Rows[0]["ITITUBEN"] = "T";
					RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"] = DBNull.Value;
				}
			}
			else
			{
				//Si es seguridad social va a los indicadores de seguridad social
				if (rbIndicadorSS[0].IsChecked)
				{
					RrDENTIPAC.Tables[0].Rows[0]["ITITUBEN"] = "T";
					RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"] = DBNull.Value;
				}
				else
				{
					//SI ES BENEFICIARIO
					if (rbIndicadorSS[1].IsChecked)
					{
						RrDENTIPAC.Tables[0].Rows[0]["ITITUBEN"] = "B";
						RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"] = tbRENomTitularSS.Text;
					}
				}
				if (!rbIndicadorSS[0].IsChecked && !rbIndicadorSS[1].IsChecked)
				{
					RrDENTIPAC.Tables[0].Rows[0]["ITITUBEN"] = "T";
					RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"] = DBNull.Value;
				}
			}
			//INDICADOR DE ACTIVO O PENSIONISTA
			if (SEGSOCIAL)
			{
				if (rbActivo.IsChecked)
				{
					RrDENTIPAC.Tables[0].Rows[0]["IACTPENS"] = "A";
				}
				else
				{
					if (rbPensionista.IsChecked)
					{
						RrDENTIPAC.Tables[0].Rows[0]["IACTPENS"] = "P";
					}
				}
				if (MEBCIAS.Text.Trim() != "")
				{
					RrDENTIPAC.Tables[0].Rows[0]["GCODCIAS"] = MEBCIAS.Text.Trim();
				}
				else
				{
					RrDENTIPAC.Tables[0].Rows[0]["GCODCIAS"] = DBNull.Value;
				}
			}
			else
			{
				RrDENTIPAC.Tables[0].Rows[0]["IACTPENS"] = DBNull.Value;
				RrDENTIPAC.Tables[0].Rows[0]["GCODCIAS"] = DBNull.Value;
			}

			//OSCAR C
			RrDENTIPAC.Tables[0].Rows[0]["FMOVIMIE"] = DateTime.Now;

			//ponemos el usuario conectado
			RrDENTIPAC.Tables[0].Rows[0]["gusuario"] = (Serrores.VVstUsuarioApli.Trim() == "") ? "" : Serrores.VVstUsuarioApli.Trim();

			//-------

		}

		private bool ACTUALIZAR_DPERSCON()
		{
			return GrabarPersonasContacto();
		}

		private void ACTUALIZAR_DPRIVADO()
		{
			string SQLPROV = String.Empty;
			DataSet RrSQLProv = null;

			RrDPRIVADO.Tables[0].Rows[0]["GIDENPAC"] = MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"];

			//(maplaza)(16/08/2007)Ante cualquier modificaci�n de datos del paciente, si afectan al pagador privado y tiene
			//indicador de propio paciente (IPROPACI = "S), actualizar dicha informaci�n con los datos del pagador.
			string CODIGOPROVINCIA = String.Empty;
			if (chkPropioPaciente.CheckState == CheckState.Checked)
			{
				RrDPRIVADO.Tables[0].Rows[0]["IPROPACI"] = "S";
				RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["DNOMBPRI"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DNOMBPAC"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DNOMBPAC"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["DAPE1PRI"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE1PAC"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE1PAC"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE2PAC"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE2PAC"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["DDIREPRI"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCODIPOS"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCODIPOS"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["GPROVINC"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPROVINC"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["DPOBLACI"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DPOBLACI"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DPOBLACI"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPAISRES"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPAISRES"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["NPASAPOR"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NPASAPOR"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NPASAPOR"]).Trim();
				RrDPRIVADO.Tables[0].Rows[0]["DDIREEXT"] = (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREEXT"])) ? "" : Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREEXT"]).Trim();
			}
			else
			{
				//-----
				RrDPRIVADO.Tables[0].Rows[0]["IPROPACI"] = "N";
				//(maplaza)(03/05/2006)
				if (tbNIFOtrosDocRE.Text.Trim() != "")
				{
					RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"] = tbNIFOtrosDocRE.Text.Trim().ToUpper();
				}
				else
				{
					RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"] = DBNull.Value;
				}

				RrDPRIVADO.Tables[0].Rows[0]["DNOMBPRI"] = tbRENombre.Text.Trim().ToUpper();
				RrDPRIVADO.Tables[0].Rows[0]["DAPE1PRI"] = tbREApe1.Text.Trim().ToUpper();
				RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"] = tbREApe2.Text.Trim().ToUpper();
				if (tbREDomicilio.Text.Trim().ToUpper() != "")
				{
					RrDPRIVADO.Tables[0].Rows[0]["DDIREPRI"] = tbREDomicilio.Text.Trim().ToUpper();
				}
				else
				{
					RrDPRIVADO.Tables[0].Rows[0]["DDIREPRI"] = DBNull.Value;
				}
				//pasaporte
				if (tbREPasaporte.Text.Trim().ToUpper() != "")
				{
					RrDPRIVADO.Tables[0].Rows[0]["NPASAPOR"] = tbREPasaporte.Text.Trim().ToUpper();
				}
				else
				{
					RrDPRIVADO.Tables[0].Rows[0]["NPASAPOR"] = DBNull.Value;
				}
				//----
				if (mebRECodPostal.Text.Trim().Trim().ToUpper() != "")
				{
					RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"] = mebRECodPostal.Text.Trim().Trim().ToUpper();
				}
				else
				{
					RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"] = DBNull.Value;
				}


				if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) >= 0)
				{
					object tempRefParam = 1;
					object tempRefParam2 = cbbREPoblacion.get_ListIndex();
					RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"] = cbbREPoblacion.get_Column(tempRefParam, tempRefParam2);
					object tempRefParam3 = 1;
					object tempRefParam4 = cbbREPoblacion.get_ListIndex();
					CODIGOPROVINCIA = StringsHelper.Format(cbbREPoblacion.get_Column(tempRefParam3, tempRefParam4), "000000").Substring(0, Math.Min(2, StringsHelper.Format(cbbREPoblacion.get_Column(tempRefParam3, tempRefParam4), "000000").Length));
					if (CODIGOPROVINCIA == MFiliacion.CodPROV)
					{
						RrDPRIVADO.Tables[0].Rows[0]["GPROVINC"] = MFiliacion.CodPROV;
						RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"] = DBNull.Value;
						RrDPRIVADO.Tables[0].Rows[0]["dpoblaci"] = DBNull.Value;
					}
					else
					{
						SQLPROV = "SELECT GPROVINC FROM DPROVINC WHERE GPROVINC = '" + CODIGOPROVINCIA + "'";
						SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLPROV, MFiliacion.GConexion);
						RrSQLProv = new DataSet();
						tempAdapter.Fill(RrSQLProv);
						RrDPRIVADO.Tables[0].Rows[0]["GPROVINC"] = RrSQLProv.Tables[0].Rows[0]["GPROVINC"];
						RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"] = DBNull.Value;
						RrDPRIVADO.Tables[0].Rows[0]["dpoblaci"] = DBNull.Value;
						RrSQLProv.Close();
					}
				}
				else
				{
					RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["GPROVINC"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["dpoblaci"] = DBNull.Value;
				}


				//SI LA RESIDENCIA ES EXTRANJERO
				if (chbREExtrangero.CheckState == CheckState.Checked)
				{
					if (Convert.ToDouble(cbbREPais.get_ListIndex()) != -1)
					{
						if (tbREExPob.Text.Trim() == "")
						{
							RrDPRIVADO.Tables[0].Rows[0]["DPOBLACI"] = DBNull.Value;
						}
						else
						{
							RrDPRIVADO.Tables[0].Rows[0]["DPOBLACI"] = tbREExPob.Text;
						}
						//(maplaza)(10/08/2007)
						if (tbREDireccionEx.Text.Trim() == "")
						{
							RrDPRIVADO.Tables[0].Rows[0]["ddireext"] = DBNull.Value;
						}
						else
						{
							RrDPRIVADO.Tables[0].Rows[0]["ddireext"] = tbREDireccionEx.Text.Trim();
						}
						//-----
						//CODIGO DEL PAIS
						object tempRefParam5 = 1;
						object tempRefParam6 = cbbREPais.get_ListIndex();
						RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"] = cbbREPais.get_Column(tempRefParam5, tempRefParam6);
						//(maplaza)(08/08/2007)Se independiza el check de residencia en el extranjero de todos los controles de Filiaci�n.
						//------
					}
				}
			}

			//FORMA DE PAGO
			if (cbbREPago.SelectedIndex != -1 && cbbREPago.Text.Trim() != "")
			{				
				int tempRefParam7 = 1;
                int tempRefParam8 = cbbREPago.SelectedIndex;
				RrDPRIVADO.Tables[0].Rows[0]["GFORPAGO"] = getLisIndex(cbbREPago,tempRefParam8, tempRefParam7);
			}
			else
			{
				RrDPRIVADO.Tables[0].Rows[0]["GFORPAGO"] = DBNull.Value;
			}
			if (cbbREPago.SelectedIndex != -1 && cbbREPago.Text.Trim() != "")
			{
				int tempRefParam9 = 2;
                int tempRefParam10 = cbbREPago.SelectedIndex;

				if (getLisIndex(cbbREPago,tempRefParam10, tempRefParam9) == "S")
				{
					if (FrmCB21.Visible)
					{
						RrDPRIVADO.Tables[0].Rows[0]["GBANCO"] = tbBanco1.Text;
						RrDPRIVADO.Tables[0].Rows[0]["GAGENCIA"] = tbSucursal1.Text;
						RrDPRIVADO.Tables[0].Rows[0]["NDIGCONT"] = tbControl1.Text;
						RrDPRIVADO.Tables[0].Rows[0]["NCUENTA"] = tbCuenta1.Text;
						RrDPRIVADO.Tables[0].Rows[0]["Ndigcont2"] = tbControl12.Text;
					}
					else
					{
						RrDPRIVADO.Tables[0].Rows[0]["GBANCO"] = tbBanco.Text;
						RrDPRIVADO.Tables[0].Rows[0]["GAGENCIA"] = tbSucursal.Text;
						RrDPRIVADO.Tables[0].Rows[0]["NDIGCONT"] = tbControl.Text;
						RrDPRIVADO.Tables[0].Rows[0]["NCUENTA"] = tbCuenta.Text;
					}
				}
				else
				{
					if (FrmCB21.Visible)
					{
						RrDPRIVADO.Tables[0].Rows[0]["GBANCO"] = DBNull.Value;
						RrDPRIVADO.Tables[0].Rows[0]["GAGENCIA"] = DBNull.Value;
						RrDPRIVADO.Tables[0].Rows[0]["NDIGCONT"] = DBNull.Value;
						RrDPRIVADO.Tables[0].Rows[0]["NCUENTA"] = DBNull.Value;
						RrDPRIVADO.Tables[0].Rows[0]["Ndigcont2"] = DBNull.Value;
					}
					else
					{
						RrDPRIVADO.Tables[0].Rows[0]["GBANCO"] = DBNull.Value;
						RrDPRIVADO.Tables[0].Rows[0]["GAGENCIA"] = DBNull.Value;
						RrDPRIVADO.Tables[0].Rows[0]["NDIGCONT"] = DBNull.Value;
						RrDPRIVADO.Tables[0].Rows[0]["NCUENTA"] = DBNull.Value;
					}
				}
			}
			else
			{
				if (FrmCB21.Visible)
				{
					RrDPRIVADO.Tables[0].Rows[0]["GBANCO"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["GAGENCIA"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["NDIGCONT"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["NCUENTA"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["Ndigcont2"] = DBNull.Value;
				}
				else
				{
					RrDPRIVADO.Tables[0].Rows[0]["GBANCO"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["GAGENCIA"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["NDIGCONT"] = DBNull.Value;
					RrDPRIVADO.Tables[0].Rows[0]["NCUENTA"] = DBNull.Value;
				}
			}
		}

		private void CARGAR_DATOSTABLAS()
		{
			string SQLDPOBLACION = String.Empty;
			string SQLDENTIPAC = String.Empty;
			DataSet RrDENTIPAC = null;
			bool bEncontrado = false;

			//********************************
			// DATOS PERSONALES DEL PACIENTE *
			//********************************


			//NIF, DOMICILIO, CODIGO POSTAL, TELF 1 Y 2, EXTENSION 1 Y 2
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]))
			{
				rbDNIOtroDocDP[0].IsChecked = false;
				rbDNIOtroDocDP[1].IsChecked = false;
				tbNIFOtrosDocDP.Enabled = false;
				if (tbNIFOtrosDocDP.Mask == MFiliacion.stFORMATO_NIF)
				{
					tbNIFOtrosDocDP.Text = "         ";
				}
				else
				{
					tbNIFOtrosDocDP.Text = "";
				}
			}
			else
			{
				//Not IsNull(RrDPACIENT("NDNINIFP"))
				//(maplaza)(03/05/2006)Se puede tratar de un DNI/NIF, o bien de un tipo distinto de documento
				if (MFiliacion.EsDNI_NIF(Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]).Trim(), 9))
				{
					rbDNIOtroDocDP[0].IsChecked = true;
					rbDNIOtroDocDP[1].IsChecked = false;
					tbNIFOtrosDocDP.Enabled = true;
					if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]).Trim().Length == 9)
					{
						tbNIFOtrosDocDP.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]).Trim();
					}
					else
					{
						tbNIFOtrosDocDP.Text = StringsHelper.Format(Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]).Trim(), "00000000") + " ";
					}
				}
				else
				{
					//si no se trata de un DNI ni de un NIF es "Otros documentos"
					rbDNIOtroDocDP[0].IsChecked = false;
					rbDNIOtroDocDP[1].IsChecked = true;
					tbNIFOtrosDocDP.Enabled = true;
					tbNIFOtrosDocDP.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]).Trim();
				}

			}

			//TARJETA SANITARIA
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTARJETA"]))
			{
				tbTarjeta.Text = "         ";
			}
			else
			{
				tbTarjeta.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTARJETA"]);
			}

			//(maplaza)(06/08/2007)
			//TARJETA SANITARIA EUROPEA
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTARJSAE"]))
			{
				tbTarjetaEuropea.Text = "         ";
			}
			else
			{
				tbTarjetaEuropea.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTARJSAE"]);
			}
			//-----

			if (CipAutonomico)
			{
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ocipauto"]))
				{
					lbCipAutonomico.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ocipauto"]);
				}
			}
			else
			{
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ncipauto"]))
				{
					lbCipAutonomico.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ncipauto"]);
				}
			}

			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gpaisnac"]))
			{
				for (int i = 0; i <= cbbNacPais.ListCount - 1; i++)
				{
					object tempRefParam = 1;
					object tempRefParam2 = i;
					if (Convert.ToString(cbbNacPais.get_Column(tempRefParam, tempRefParam2)).Trim().ToUpper() == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gpaisnac"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam2);
						cbbNacPais.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam2);
					}
				}
			}


			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gtipovia"]))
			{
				for (int i = 0; i <= cbbDPVia.ListCount - 1; i++)
				{
					object tempRefParam3 = 1;
					object tempRefParam4 = i;
					if (Convert.ToString(cbbDPVia.get_Column(tempRefParam3, tempRefParam4)).Trim().ToUpper() == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gtipovia"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam4);
						cbbDPVia.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam4);
					}
				}
			}
			else
			{
				cbbDPVia.set_ListIndex(-1);
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NNUMEVIA"]))
			{
				tbDPNDomic.Text = "";
			}
			else
			{
				tbDPNDomic.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NNUMEVIA"]).Trim();
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DOTROVIA"]))
			{
				tbDPOtrosDomic.Text = "";
			}
			else
			{
				tbDPOtrosDomic.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DOTROVIA"]).Trim();
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDOMICIL"]))
			{
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"]))
				{
					tbDPDomicilioP.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"]);
				}
			}
			else
			{
				tbDPDomicilioP.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDOMICIL"]);
			}

			//OSCAR C ENE 2005
			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["OOBSERVA"]))
			{
				tbObservaciones.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["OOBSERVA"]).Trim();
			}
			//--------
			chkObservaciones.CheckState = (CheckState) ((Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["iverobse"]) + "" == "S") ? 1 : 0);

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"]))
			{
				tbDPDomicilio.Text = "";
			}
			else
			{
				tbDPDomicilio.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"]);
			}
			bcambiar = true;

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCODIPOS"]))
			{
				if (bFormatoCodigoPos)
				{
					mebDPCodPostal.Text = "     ";
				}
				else
				{
					mebDPCodPostal.Text = "";
				}
			}
			else
			{
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"]))
				{
					bcambiar = false;
				}
				if (bFormatoCodigoPos)
				{
					mebDPCodPostal.Text = StringsHelper.Format(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCODIPOS"], "00000");
				}
				else
				{
					mebDPCodPostal.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCODIPOS"]).Trim();
				}
			}
			bcambiar = true;
			//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
			mebDPCodPostal.Tag = mebDPCodPostal.Text;
			//-----

			//PASAPORTE
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NPASAPOR"]))
			{
				tbDPPasaporte.Text = "";
			}
			else
			{
				tbDPPasaporte.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NPASAPOR"]).Trim();
			}
			//--
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO1"]))
			{
				tbDPTelf1.Text = "";
			}
			else
			{
				tbDPTelf1.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO1"]).Trim();
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI1"]))
			{
				tbDPExtension1.Text = "";
			}
			else
			{
				tbDPExtension1.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI1"]);
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO2"]))
			{
				tbDPTelf2.Text = "";
			}
			else
			{
				tbDPTelf2.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO2"]).Trim();
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI2"]))
			{
				tbDPExtension2.Text = "";
			}
			else
			{
				tbDPExtension2.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI2"]);
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO3"]))
			{
				tbDPTelf3.Text = "";
			}
			else
			{
				tbDPTelf3.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NTELEFO3"]).Trim();
				chbSMS.Enabled = true;
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ienvisms"]))
				{
					chbSMS.CheckState = (CheckState) ((Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ienvisms"]).Trim().ToUpper() == "S") ? 1 : 0);
				}
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI3"]))
			{
				tbDPExtension3.Text = "";
			}
			else
			{
				tbDPExtension3.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NEXTESI3"]);
			}
			//email
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ddiemail"]))
			{
				tbEmail.Text = "";
			}
			else
			{
				tbEmail.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ddiemail"]).Trim();
			}

			bcambiar = false; //(maplaza)(31/05/2006)


			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"]))
			{
				cbbDPPoblacion.set_ListIndex(-1);
			}
			else
			{
				//BUSCA EL NOMBRE DE LA POBLACION
				for (int i = 0; i <= cbbDPPoblacion.ListCount - 1; i++)
				{
					object tempRefParam5 = 1;
					object tempRefParam6 = i;
					if (StringsHelper.Format(cbbDPPoblacion.get_Column(tempRefParam5, tempRefParam6), "000000") == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"]))
					{
						i = Convert.ToInt32(tempRefParam6);
						cbbDPPoblacion.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam6);
					}
				}
				//(maplaza)(09/08/2007)Se independiza el check de residencia en el extranjero de todos los controles de Filiaci�n.
				//-----
			}


			bcambiar = true; //(maplaza)(31/05/2006)


			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gpobnaci"]))
			{
				cbbNacPoblacion.set_ListIndex(-1);
			}
			else
			{
				//BUSCA EL NOMBRE DE LA POBLACION
				for (int i = 0; i <= cbbNacPoblacion.ListCount - 1; i++)
				{
					object tempRefParam7 = 1;
					object tempRefParam8 = i;
					if (StringsHelper.Format(cbbNacPoblacion.get_Column(tempRefParam7, tempRefParam8), "000000") == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gpobnaci"]))
					{
						i = Convert.ToInt32(tempRefParam8);
						cbbNacPoblacion.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam8);
					}
				}
			}

			//ESTADO CIVIL
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GESTACIV"]))
			{
				cbbDPEstado.set_ListIndex(-1);
			}
			else
			{
				for (int i = 0; i <= cbbDPEstado.ListCount - 1; i++)
				{
					object tempRefParam9 = 1;
					object tempRefParam10 = i;
					if (Convert.ToString(cbbDPEstado.get_Column(tempRefParam9, tempRefParam10)).Trim().ToUpper() == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GESTACIV"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam10);
						cbbDPEstado.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam10);
					}
				}
			}
			//catalogacion social
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCATASOC"]))
			{
				cbbDPCatSoc.set_ListIndex(-1);
			}
			else
			{
				for (int i = 0; i <= cbbDPCatSoc.ListCount - 1; i++)
				{
					object tempRefParam11 = 1;
					object tempRefParam12 = i;
					if (Convert.ToString(cbbDPCatSoc.get_Column(tempRefParam11, tempRefParam12)).Trim().ToUpper() == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCATASOC"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam12);
						cbbDPCatSoc.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam12);
					}
				}
			}

			//RESIDENCIA EN EL EXTRANJERO
			//si tiene el nombre de la poblacion
			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPAISRES"]) && (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gpaisres"]) + "").Trim() != "34")
			{
				//SI LA POBLACION NO ES NACIONAL BUSCA EL PAIS CORRESPONDIENTE AL CODIGO
				for (int i = 0; i <= cbbDPPais.ListCount - 1; i++)
				{
					object tempRefParam13 = 1;
					object tempRefParam14 = i;
					if (Convert.ToString(cbbDPPais.get_Column(tempRefParam13, tempRefParam14)).Trim().ToUpper() == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPAISRES"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam14);
						cbbDPPais.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam14);
					}
				}
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["dpoblaci"]))
				{
					tbDPExPob.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DPOBLACI"]);
				}
				else
				{
					tbDPExPob.Text = "";
				}
				//(maplaza)(09/08/2007)Campo "ddireext"
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ddireext"]))
				{
					tbDPDireccionEx.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ddireext"]).Trim();
				}
				else
				{
					tbDPDireccionEx.Text = "";
				}
				//------

				chbDPExtrangero.CheckState = CheckState.Checked;
				//(maplaza)(09/08/2007)
			}
			else
			{
				chbDPExtrangero.CheckState = CheckState.Unchecked;
				//-----
			}

			//AUTORIZACION UTILIZACION DATOS
			if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["IAUTUTIL"]) == "S")
			{
				rbAutorizacion[0].IsChecked = true;
			}
			if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["IAUTUTIL"]) == "N")
			{
				rbAutorizacion[1].IsChecked = true;
			}


			//ESTADO DE EXITUS
			if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["IEXITUSP"]) == "N")
			{
				chbDPExitus.CheckState = CheckState.Unchecked;				
                sdcDPFechaFallecimiento.NullableValue = null;
                sdcDPFechaFallecimiento.SetToNullValue();

            }
            else
			{
                if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["IEXITUSP"]) == "S")
                {
                    chbDPExitus.CheckState = CheckState.Checked;
                    sdcDPFechaFallecimiento.Value = DateTime.Parse(Convert.ToDateTime(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["FFALLECI"]).ToString("dd/MM/yyyy"));
                }
                else
                {
                    sdcDPFechaFallecimiento.NullableValue = null;
                    sdcDPFechaFallecimiento.SetToNullValue();
                }
			}

			//MOROSO (Oscar C 27/10/04)
			if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["Ibloqueo"]) == "S")
			{
				rbBloqueo[0].IsChecked = true;
			}
			else
			{
				if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ibloqueo"]) == "N")
				{
					rbBloqueo[1].IsChecked = true;
				}
			}
			//---------

			//********************
			// REGIMEN ECONOMICO *
			//********************
			string SQLPRI = String.Empty;
			string CODNIF = String.Empty;
			int CODSOCIEDAD = 0;
			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]))
			{
				//BUSCA EL C�DIGO QUE TIENE EN LA TABLA GLOBAL
				if (MFiliacion.CodSS == Convert.ToDouble(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]))
				{
					//ES SEGURIDAD SOCIAL
					rbRERegimen[0].IsChecked = true;
					//BUSCA EL PACIENTE EN LA TABLA DE ENTIDADES
					SQLDENTIPAC = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' AND GSOCIEDA = " + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]);
					SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLDENTIPAC, MFiliacion.GConexion);
					RrDENTIPAC = new DataSet();
					tempAdapter.Fill(RrDENTIPAC);
                    tempAdapterRrDENTIPAC = tempAdapter;

                    if (RrDENTIPAC.Tables[0].Rows.Count != 0)
					{

						//COGE EL NOMBRE DEL HOSPITAL Y EL NUMERO DE LA SS
						for (int i = 0; i <= cbbREInspeccion.ListCount - 1; i++)
						{
							object tempRefParam15 = 1;
							object tempRefParam16 = i;
							if (Convert.ToString(cbbREInspeccion.get_Column(tempRefParam15, tempRefParam16)).Trim().ToUpper() == Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["GINSPECC"]).Trim().ToUpper())
							{
								i = Convert.ToInt32(tempRefParam16);
								cbbREInspeccion.set_ListIndex(i);
								break;
							}
							else
							{
								i = Convert.ToInt32(tempRefParam16);
							}
						}

						if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["NAFILIAC"]))
						{
							tbRENSS.Text = Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["NAFILIAC"]).Trim();
						}
						else
						{
							tbRENSS.Text = "";
						}

						//SI TIENE NOMBRE DE TITULAR ES BENEFICIARIO DE SEGURIDAD SOCIAL
						if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"]))
						{
							rbIndicadorSS[1].IsChecked = true;
							rbIndicadorSS[0].IsChecked = false;
							tbRENomTitularSS.Text = Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"]);
						}
						else
						{
							//ES TITULAR
							rbIndicadorSS[0].IsChecked = true;
							rbIndicadorSS[1].IsChecked = false;
						}
						//SI ES ACTIVO O PENSIONISTA
						if (Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["IACTPENS"]) == "A")
						{
							rbActivo.IsChecked = true;
						}
						if (Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["IACTPENS"]) == "P")
						{
							rbPensionista.IsChecked = true;
						}
						//CIAS SI LO TIENE
						if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["GCODCIAS"]))
						{
							MEBCIAS.Text = Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["GCODCIAS"]);
							Restringir_Inspeccion(false);
						}


						//********************* O.Frias (08/01/2007) *********************
						if (Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["ieszona"]))
						{
							chkZona.Checked = false;
						}
						else
						{
							chkZona.Checked = (Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["ieszona"]) == "S");
						}
						//********************* O.Frias (08/01/2007) *********************
					}
					RrDENTIPAC.Close();
				}
				else
				{
					//PUEDE SER PRIVADO O SOCIEDAD
					chkZona.Checked = false;
					if (MFiliacion.CodPriv == Convert.ToDouble(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]))
					{
						//ES PRIVADO
						rbRERegimen[2].IsChecked = true;
						//BUSCA EL PAGADOR PRIVADO
						if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"]))
						{
							CODNIF = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"]);
						}
						else
						{
							CODNIF = "''";
						}
						if (!bcargado_privado)
						{
							SQLPRI = "SELECT * FROM DPRIVADO WHERE gidenpac = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gidenpac"]) + "' and NNIFPRIV  = '" + CODNIF + "'";
							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLPRI, MFiliacion.GConexion);
							RrDPRIVADO = new DataSet();
							tempAdapter_2.Fill(RrDPRIVADO);
                            tempAdapterRrDPRIVADO = tempAdapter_2;
                            if (RrDPRIVADO.Tables[0].Rows.Count != 0)
							{
								if (!Convert.IsDBNull(Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim()))
								{
									//(maplaza)(03/05/2006)Se puede tratar de un DNI/NIF, o bien de un tipo distinto de documento
									if (MFiliacion.EsDNI_NIF(Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim(), 9))
									{
										rbDNIOtroDocRE[0].IsChecked = true;
										rbDNIOtroDocRE[1].IsChecked = false;
										tbNIFOtrosDocRE.Enabled = true;
										if (Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim().Length == 9)
										{
											tbNIFOtrosDocRE.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim();
										}
										else
										{
											tbNIFOtrosDocRE.Text = StringsHelper.Format(Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim(), "00000000") + " ";
										}
									}
									else
									{
										//si no se trata de un DNI ni de un NIF es "Otros documentos"
										rbDNIOtroDocRE[0].IsChecked = false;
										rbDNIOtroDocRE[1].IsChecked = true;
										tbNIFOtrosDocRE.Enabled = true;
										tbNIFOtrosDocRE.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NNIFPRIV"]).Trim();
									}
								}
								else
								{
									//IsNull(Trim(RrDPRIVADO("NNIFPRIV")))
									rbDNIOtroDocRE[0].IsChecked = false;
									rbDNIOtroDocRE[1].IsChecked = false;
									tbNIFOtrosDocRE.Enabled = false;
									if (tbNIFOtrosDocRE.Mask == MFiliacion.stFORMATO_NIF)
									{
										tbNIFOtrosDocRE.Text = "         ";
									}
									else
									{
										tbNIFOtrosDocRE.Text = "";
									}
								}
								tbRENombre.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DNOMBPRI"]);
								tbREApe1.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DAPE1PRI"]);
								tbREApe2.Text = (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"])) ? "" : Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DAPE2PRI"]).Trim();
								if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["DDIREPRI"]))
								{
									tbREDomicilio.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DDIREPRI"]);
								}
								//pasaporte
								if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["NPASAPOR"]))
								{
									tbREPasaporte.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["NPASAPOR"]);
								}
								//--
								bNocambiarCP_RE = true; //(maplaza)(01/06/2006)Para que no salte el evento "Change" en la caja del CP
								if (bFormatoCodigoPos)
								{
									if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"]))
									{
										mebRECodPostal.Text = StringsHelper.Format(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"], "00000");
									}
									else
									{
										mebRECodPostal.Text = "     ";
									}
								}
								else
								{
									if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"]))
									{
										mebRECodPostal.Text = (Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GCODIPOS"]) + "").Trim();
									}
									else
									{
										mebRECodPostal.Text = "";
									}
								}
								bNocambiarCP_RE = false; //(maplaza)(01/06/2006)A partir de ahora, puede saltar el evento "Change" en la caja del CP

								//(maplaza)(30/08/2007)
								if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["IPROPACI"]))
								{
									chkPropioPaciente.CheckState = CheckState.Unchecked;
								}
								else if (Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["IPROPACI"]).ToUpper() == "S")
								{ 
									chkPropioPaciente.CheckState = CheckState.Checked;
								}
								else
								{
									chkPropioPaciente.CheckState = CheckState.Unchecked;
								}
								//------

								//(maplaza)(09/08/2007)Se debe independizar el check de residencia en el extranjero de todos
								//los controles de filiaci�n.
								//If IsNull(RrDPRIVADO("GPAISRES")) Then
								if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GPROVINC"]))
								{
									//    'RESIDE EN ESPA�A
									//comprueba que la provincia es la cargada
									if (Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GPROVINC"]) != MFiliacion.CodPROV)
									{
										SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GPROVINC"]) + "%' order by dpoblaci";
										CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbREPoblacion);
									}
									//CARGA EL NOMBRE Y EL CODIGO DE LA POBLACION
									for (int i = 0; i <= cbbREPoblacion.ListCount - 1; i++)
									{
										object tempRefParam17 = 1;
										object tempRefParam18 = i;
										if (StringsHelper.Format(cbbREPoblacion.get_Column(tempRefParam17, tempRefParam18), "000000") == Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GPOBLACI"]))
										{
											i = Convert.ToInt32(tempRefParam18);
											cbbREPoblacion.set_ListIndex(i);
											break;
										}
										else
										{
											i = Convert.ToInt32(tempRefParam18);
										}
									}

								}


								if (!Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"]) && (Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["gpaisres"]) + "").Trim() != "34")
								{
									//RESIDE EN EL EXTRANJERO
									for (int i = 0; i <= cbbREPais.ListCount - 1; i++)
									{
										object tempRefParam19 = 1;
										object tempRefParam20 = i;
										if (Convert.ToString(cbbREPais.get_Column(tempRefParam19, tempRefParam20)).Trim().ToUpper() == Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["GPAISRES"]).Trim().ToUpper())
										{
											i = Convert.ToInt32(tempRefParam20);
											cbbREPais.set_ListIndex(i);
											break;
										}
										else
										{
											i = Convert.ToInt32(tempRefParam20);
										}
									}
									if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["DPOBLACI"]))
									{
										tbREExPob.Text = "";
									}
									else
									{
										tbREExPob.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["DPOBLACI"]);
									}
									//(maplaza)(10/08/2007)Campo "ddireext"
									if (Convert.IsDBNull(RrDPRIVADO.Tables[0].Rows[0]["ddireext"]))
									{
										tbREDireccionEx.Text = "";
									}
									else
									{
										tbREDireccionEx.Text = Convert.ToString(RrDPRIVADO.Tables[0].Rows[0]["ddireext"]);
									}

									chbREExtrangero.CheckState = CheckState.Checked;
									//(maplaza)(09/08/2007)
								}
								else
								{
									chbREExtrangero.CheckState = CheckState.Unchecked;
									//------
								}

							}
						}
					}
					else
					{
						//ES UNA SOCIEDAD
						rbRERegimen[1].IsChecked = true;
						CODSOCIEDAD = Convert.ToInt32(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]);
						//COGE EL NOMBRE DE LA SOCIEDAD Y EL NUMERO DE POLIZA
						for (int i = 0; i <= cbbRESociedad.Items.Count - 1; i++)
						{
							if (int.Parse(cbbRESociedad.Items[i].Value.ToString()) == Convert.ToDouble(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]))
							{
								cbbRESociedad.SelectedIndex = i;
								break;
							}
						}

						//BUSCA EL TITULAR O BENEFICIARIO DE SOCIEDAD
						SQLDENTIPAC = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' AND GSOCIEDA = " + CODSOCIEDAD.ToString();
						SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLDENTIPAC, MFiliacion.GConexion);
						RrDENTIPAC = new DataSet();
						tempAdapter_3.Fill(RrDENTIPAC);
                        tempAdapterRrDENTIPAC = tempAdapter_3;
                        if (RrDENTIPAC.Tables[0].Rows.Count > 0)
						{


							if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["nafiliac"]))
							{

								if (proCoincideMascara(Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["nafiliac"]).Trim()))
								{
									tbRENPoliza.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["nafiliac"]).Trim();
								}
								else
								{
									if (MFiliacion.stFiliacion == "ALTA")
									{
										tbRENPoliza.Text = MascaraVacia();
									}
									else
									{
										bCambiandoMascara = true;
										tbRENPoliza.Mask = "";
										bCambiandoMascara = false;
										tbRENPoliza.BackColor = Color.FromArgb(255, 128, 128);
										tbRENPoliza.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["nafiliac"]).Trim();
									}
								}

							}
							else
							{
								tbRENPoliza.Text = MascaraVacia();
								tbRENVersion.Text = "";
								CodigoDeControl = -1;
								Algoritmo = -1;
							}
							//Condici�n de iva
							if (Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["icondiva"]).Trim().ToUpper() == "S")
							{
								chkExentoIVA.CheckState = CheckState.Checked;
							}
							else
							{
								chkExentoIVA.CheckState = CheckState.Unchecked;
							}
							if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["dempresa"]))
							{
								tbREEmpresa.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["dempresa"]).Trim();
							}
							else
							{
								tbREEmpresa.Text = "";
							}

							//(maplaza)(23/08/2007)
							if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["OCOBERTU"]))
							{
								tbRECobertura.Text = Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["OCOBERTU"]).Trim();
							}
							else
							{
								tbRECobertura.Text = "";
							}

							if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["FINIVIGE"]))
							{ //(formato mes/a�o)								
								mebREFechaAlta.Text = StringsHelper.Format(Convert.ToDateTime(RrDENTIPAC.Tables[0].Rows[0]["FINIVIGE"].ToString()).Month, "00") + "/" + ((int) Convert.ToDateTime(RrDENTIPAC.Tables[0].Rows[0]["FINIVIGE"].ToString()).Year).ToString();
							}
							else
							{
								mebREFechaAlta.Text = "  /    ";
							}

							if (!Convert.IsDBNull(RrDENTIPAC.Tables[0].Rows[0]["FFINVIGE"]))
							{ //(formato mes/a�o)
								mebREFechaCaducidad.Text = StringsHelper.Format(Convert.ToDateTime(RrDENTIPAC.Tables[0].Rows[0]["FFINVIGE"].ToString()).Month, "00") + "/" + ((int) Convert.ToDateTime(RrDENTIPAC.Tables[0].Rows[0]["FFINVIGE"].ToString()).Year).ToString();
							}
							else
							{
								mebREFechaCaducidad.Text = "  /    ";
							}
							//------

							//SI TIENE NOMBRE DE TITULAR ES BENEFICIARIO DE SOCIEDAD
							if (Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["ITITUBEN"]) == "T")
							{ // TITULAR
								rbIndicador[0].IsChecked = true;
								rbIndicador[1].IsChecked = false;
							}
							else
							{
								//BENEFICIARIO
								rbIndicador[1].IsChecked = true;
								rbIndicador[0].IsChecked = false;
								tbRENomTitular.Text = Convert.ToString(RrDENTIPAC.Tables[0].Rows[0]["DNOMBTIT"]);
							}
						}
						else
						{
							bCambiandoMascara = true;
							tbRENPoliza.Mask = "";
							bCambiandoMascara = false;
							ToolTipMain.SetToolTip(tbRENPoliza, "");
							tbRENPoliza.Text = "";
							chkExentoIVA.CheckState = CheckState.Unchecked;
							tbRENVersion.Text = "";
							CodigoDeControl = -1;
							Algoritmo = -1;
							tbREEmpresa.Text = "";
							tbRENomTitular.Text = "";
							//(maplaza)(23/08/2007)
							tbRECobertura.Text = "";
							mebREFechaAlta.Text = "  /    ";
							mebREFechaCaducidad.Text = "  /    ";
							//-----
						}
						RrDENTIPAC.Close();
					}
				}
			}
			//**********************
			// PERSONA DE CONTACTO *
			//**********************

			ObtenerPersonasContacto();

			//idioma del paciente
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["iidioma"]))
			{
				cbbIdioma.set_ListIndex(0);
			}
			else
			{
				for (int i = 0; i <= cbbIdioma.ListCount - 1; i++)
				{
					object tempRefParam21 = 1;
					object tempRefParam22 = i;
					if (Convert.ToString(cbbIdioma.get_Column(tempRefParam21, tempRefParam22)).Trim().ToUpper() == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["iidioma"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam22);
						cbbIdioma.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam22);
					}
				}
			}

			//Oscar C Diciembre 2010 -- Origen Info
			cbbOrigInfo.SelectedIndex = Serrores.fnBuscaListIndexID(cbbOrigInfo, (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gorigfil"].ToString())) ? "0" : MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gorigfil"].ToString());


			//(maplaza)(24/04/2007)Para la primera y segunda pesta�as, si no existe el C�digo Postal (en Base de Datos), pero
			//los dos primeros d�gitos del C�digo Postal s� corresponden a una provincia, entonces se cargan en el combo las
			//poblaciones de la provincia.
			//27/07/2011 Para Argentina no se utiliza
			if (bFormatoCodigoPos)
			{
				if (mebDPCodPostal.Text.Trim().Length == 5)
				{
					if (Convert.ToDouble(cbbDPPoblacion.get_ListIndex()) == -1)
					{
						if (!fExisteCodigoPostal(mebDPCodPostal.Text.Trim()))
						{
							if (fExisteCodigoProvincia(mebDPCodPostal.Text.Trim().Substring(0, Math.Min(2, mebDPCodPostal.Text.Trim().Length))))
							{
								CargarPoblacionesProvincia(mebDPCodPostal.Text.Trim().Substring(0, Math.Min(2, mebDPCodPostal.Text.Trim().Length)), cbbDPPoblacion);
							}
						}
					}
				}

				if (mebRECodPostal.Text.Trim().Length == 5)
				{
					if (Convert.ToDouble(cbbREPoblacion.get_ListIndex()) == -1)
					{
						if (!fExisteCodigoPostal(mebRECodPostal.Text.Trim()))
						{
							if (fExisteCodigoProvincia(mebRECodPostal.Text.Trim().Substring(0, Math.Min(2, mebRECodPostal.Text.Trim().Length))))
							{
								CargarPoblacionesProvincia(mebRECodPostal.Text.Trim().Substring(0, Math.Min(2, mebRECodPostal.Text.Trim().Length)), cbbREPoblacion);
							}
						}
					}
				}
			}
			//-----

			//(maplaza)(06/08/2007)

			//***************************************
			// COLECTIVOS Y CATEGORIZACI�N ESPECIAL *
			//***************************************
			//n�mero de trabajador
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ntrabaja"]))
			{
				tbNumTrabajador.Text = "";
			}
			else
			{
				tbNumTrabajador.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ntrabaja"]).Trim();
			}

			//parentesco con el trabajador
			DataSet RrTemp = null;
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gparetra"]))
			{
				cbbParentesco.set_ListIndex(0);
			}
			else
			{
				for (int i = 0; i <= cbbParentesco.ListCount - 1; i++)
				{
					object tempRefParam23 = 1;
					object tempRefParam24 = i;
					if (Convert.ToString(cbbParentesco.get_Column(tempRefParam23, tempRefParam24)).Trim().ToUpper() == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gparetra"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam24);
						bEncontrado = true;
						cbbParentesco.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam24);
					}
				}
				//si no se ha econtrado el elemento en el combo, puede deberse a que el elemento no estaba cargado en el combo
				//por tener el registro fecha de borrado distinta de NULO, as� que habr� que cargar ese elemento en el combo, y
				//dejarle seleccionado.
				if (!bEncontrado)
				{

					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter("select gparente, dparente from DPARENTE Where gparente=" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gparetra"]), MFiliacion.GConexion);
					RrTemp = new DataSet();
					tempAdapter_4.Fill(RrTemp);

					if (RrTemp.Tables[0].Rows.Count != 0)
					{
						object tempRefParam25 = Convert.ToString(RrTemp.Tables[0].Rows[0]["dparente"]).Trim();
						object tempRefParam26 = Type.Missing;
						cbbParentesco.AddItem(tempRefParam25, tempRefParam26);
						cbbParentesco.set_Column(1, cbbParentesco.ListCount - 1, Convert.ToString(RrTemp.Tables[0].Rows[0]["gparente"]).Trim());
						cbbParentesco.set_ListIndex(cbbParentesco.ListCount - 1);
					}

					RrTemp.Close();
				}
			}

			//c�digo de categorizaci�n especial
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gcatespe"]))
			{
				cbbCategoria.set_ListIndex(0);
			}
			else
			{
				for (int i = 0; i <= cbbCategoria.ListCount - 1; i++)
				{
					object tempRefParam27 = 1;
					object tempRefParam28 = i;
					if (Convert.ToString(cbbCategoria.get_Column(tempRefParam27, tempRefParam28)).Trim().ToUpper() == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gcatespe"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam28);
						cbbCategoria.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam28);
					}
				}
			}

			//***************************************
			//*  Grados de dependencia              *
			//***************************************

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg1n1"]))
			{                
                sdcGradoDep[0].NullableValue = null;
                sdcGradoDep[0].SetToNullValue();
            }
            else
			{
				sdcGradoDep[0].Value = DateTime.Parse(Convert.ToDateTime(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg1n1"]).ToString("dd/MM/yyyy"));
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg1n2"]))
			{				
                sdcGradoDep[1].NullableValue = null;
                sdcGradoDep[1].SetToNullValue();
            }
			else
			{
				sdcGradoDep[1].Value = DateTime.Parse(Convert.ToDateTime(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg1n2"]).ToString("dd/MM/yyyy"));
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg2n1"]))
			{				
                sdcGradoDep[2].NullableValue = null;
                sdcGradoDep[2].SetToNullValue();
            }
			else
			{
				sdcGradoDep[2].Value = DateTime.Parse(Convert.ToDateTime(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg2n1"]).ToString("dd/MM/yyyy"));
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg2n2"]))
			{				
                sdcGradoDep[3].NullableValue = null;
                sdcGradoDep[3].SetToNullValue();
            }
			else
			{
				sdcGradoDep[3].Value = DateTime.Parse(Convert.ToDateTime(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg2n2"]).ToString("dd/MM/yyyy"));
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg3n1"]))
			{				
                sdcGradoDep[4].NullableValue = null;
                sdcGradoDep[4].SetToNullValue();
            }
			else
			{
				sdcGradoDep[4].Value = DateTime.Parse(Convert.ToDateTime(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg3n1"]).ToString("dd/MM/yyyy"));
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg3n2"]))
			{				
                sdcGradoDep[5].NullableValue = null;
                sdcGradoDep[5].SetToNullValue();
            }
            else
			{
				sdcGradoDep[5].Value = DateTime.Parse(Convert.ToDateTime(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fdepg3n2"]).ToString("dd/MM/yyyy"));
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["oobsgdep"]))
			{
				tbOtrasCircunstanciasGradoDep.Text = "";
			}
			else
			{
				tbOtrasCircunstanciasGradoDep.Text = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["oobsgdep"]);
			}


			lblFechaUltMod.Text = "";
			ToolTipMain.SetToolTip(lblFechaUltMod, "");
			string sqlAux = "SELECT fmodific FROM DPACIMOD WHERE gidenpac ='" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gidenpac"]) + "' ORDER BY fmodific desc";
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sqlAux, MFiliacion.GConexion);
			DataSet rrAux = new DataSet();
			tempAdapter_5.Fill(rrAux);
			if (rrAux.Tables[0].Rows.Count != 0)
			{
				rrAux.MoveFirst();
				lblFechaUltMod.Text = Convert.ToDateTime(rrAux.Tables[0].Rows[0]["fmodific"]).ToString("dd/MM/yyyy");
			}
			else
			{
				lblFechaUltMod.Text = Convert.ToDateTime(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["fsistema"]).ToString("dd/MM/yyyy");
			}
			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gusuario"]))
			{
				ToolTipMain.SetToolTip(lblFechaUltMod, proObtenerusuario(Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gusuario"])));
			}
			rrAux.Close();
			//--------

			//Oscar C Mayo 2010
			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gmotifil"]))
			{
				iMotivoFiliacion = Convert.ToInt32(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gmotifil"]);
			}
			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["omotifil"]))
			{
				oMotivoFiliacion = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["omotifil"]).Trim();
			}
			//-----


			//Canal de preferencia
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gcanotic"]))
			{
				cbbCanPrev.SelectedIndex = -1;
			}
			else
			{
				for (int i = 0; i <= ((RadGridView)cbbCanPrev.EditorControl).Rows.Count -1; i++)
				{					
					int tempRefParam29 = 1;
					int tempRefParam30 = i;

					if (getLisIndex(cbbCanPrev,tempRefParam30, tempRefParam29).Trim().ToUpper() == Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gcanotic"]).Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam30);
						cbbCanPrev.SelectedIndex = i;
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam30);
					}
				}
			}
			//-----
		}

		private void COMPROBAR_DATOS()
		{
			//**************
			//datos paciente
			//**************
			DatosCorrectos = true;
			if (tbApellido1.Text == "" || tbNombre.Text == "")
			{
				DatosCorrectos = false;
			}
			if (MFiliacion.stModulo != "CITACION" && MFiliacion.stModulo != "URGENCIAS")
			{
				if (sdcFechaNac.Text == "")
				{
					DatosCorrectos = false;
				}
			}
			//si existe exitus comprobar que esta la fecha de fallecimiento
			if (chbDPExitus.CheckState == CheckState.Checked && sdcDPFechaFallecimiento.Text == "")
			{
				DatosCorrectos = false;
			}

			//si a seleccionado el extrangero y no ha introducido el pais
			if (chbDPExtrangero.CheckState == CheckState.Checked && Convert.ToDouble(cbbDPPais.get_ListIndex()) == -1)
			{
				DatosCorrectos = false;
			}

			//(maplaza)(03/05/2006)Si lo que se queda escrito en la caja de texto del DNI/NIF no se corresponde con un
			//DNI ni con un NIF, los datos no son correctos, y no se podr�n guardar esos datos.
			if (rbDNIOtroDocDP[0].IsChecked)
			{
				if (tbNIFOtrosDocDP.Text.Trim() != "")
				{
					if (!MFiliacion.EsDNI_NIF(tbNIFOtrosDocDP.Text.Trim(), 9))
					{
						DatosCorrectos = false;
					}
				}
			}

			//SI HA INTRODUCIDO EL SEGUNDO TELEFONO Y NO EL PRIMERO
			if (tbDPTelf1.Text == "" && tbDPTelf2.Text != "")
			{
				tbDPTelf1.Text = tbDPTelf2.Text;
				tbDPExtension1.Text = tbDPExtension2.Text;
				tbDPExtension2.Text = "";
				tbDPTelf2.Text = "";
			}


			//*****************
			//regimen econ�mico
			//*****************
			//And stModulo <> "URGENCIAS"
			//''If stModulo <> "CITACION" Then
			//si no ha elegido ningun regimen economico
			if (!rbRERegimen[0].IsChecked)
			{
				if (!rbRERegimen[1].IsChecked)
				{
					if (!rbRERegimen[2].IsChecked)
					{
						DatosCorrectos = false;
					}
				}
			}
			//'''End If
			//And stModulo <> "URGENCIAS"
			//'''If stModulo <> "CITACION" Then
			//si el regimen economico es S.S
			if (rbRERegimen[0].IsChecked)
			{
				if (Convert.ToDouble(cbbREInspeccion.get_ListIndex()) == -1)
				{
					DatosCorrectos = false;
				}
				if (cbbREInspeccion.Text.Trim() == "")
				{
					DatosCorrectos = false;
				}
			}
			else
			{
				//si el regimen economico es Sociedad
				if (rbRERegimen[1].IsChecked)
				{
					if (cbbRESociedad.SelectedIndex == -1)
					{
						DatosCorrectos = false;
					}
					if (cbbRESociedad.Text.Trim() == "")
					{
						DatosCorrectos = false;
					}
				}
				else
				{
					//si el regimen economico es privado
					if (rbRERegimen[2].IsChecked)
					{
						//introducir el NIF
						if (tbREApe1.Text.Trim() == "" || tbRENombre.Text.Trim() == "")
						{
							DatosCorrectos = false;
						}
					}
				}
			}
			//'''End If

			if (rbRERegimen[1].IsChecked)
			{
				if (!rbIndicador[0].IsChecked && !rbIndicador[1].IsChecked)
				{
					DatosCorrectos = false;
				}
				else
				{
					if (rbIndicador[1].IsChecked && tbRENomTitular.Text == "")
					{
						DatosCorrectos = false;
					}
				}
			}

			if (rbRERegimen[0].IsChecked)
			{
				if (!rbIndicadorSS[0].IsChecked && !rbIndicadorSS[1].IsChecked)
				{
					DatosCorrectos = false;
				}
				else
				{
					if (rbIndicadorSS[1].IsChecked && tbRENomTitularSS.Text == "")
					{
						DatosCorrectos = false;
					}
				}
			}
			//Si selecciona la inspeccion y ha elegido beneficiario y no ha puesto nombre
			if (Convert.ToDouble(cbbREInspeccion.get_ListIndex()) != -1 && rbIndicadorSS[1].IsChecked && tbRENomTitularSS.Text.Trim() == "")
			{
				DatosCorrectos = false;
			}
			//Si selecciona la inspeccion no esta ni titular ni beneficiario
			if (Convert.ToDouble(cbbREInspeccion.get_ListIndex()) != -1 && !rbIndicadorSS[1].IsChecked && !rbIndicadorSS[0].IsChecked)
			{
				DatosCorrectos = false;
			}

			//Si selecciona la sociedad y ha elegido beneficiario y no ha puesto nombre
			if (cbbRESociedad.SelectedIndex != -1 && rbIndicador[1].IsChecked && tbRENomTitular.Text.Trim() == "")
			{
				DatosCorrectos = false;
			}
			//Si selecciona la sociedad no esta ni titular ni beneficiario
			if (cbbRESociedad.SelectedIndex != -1 && !rbIndicador[1].IsChecked && !rbIndicador[0].IsChecked)
			{
				DatosCorrectos = false;
			}
			if (chbREExtrangero.CheckState == CheckState.Checked && Convert.ToDouble(cbbREPais.get_ListIndex()) == -1)
			{
				DatosCorrectos = false;
			}


			//(maplaza)(03/05/2006)Si lo que se queda escrito en la caja de texto del DNI/NIF no se corresponde con un
			//DNI ni con un NIF, los datos no son correctos, y no se podr�n guardar esos datos.
			if (rbDNIOtroDocRE[0].IsChecked)
			{
				if (tbNIFOtrosDocRE.Text.Trim() != "")
				{
					if (!MFiliacion.EsDNI_NIF(tbNIFOtrosDocRE.Text.Trim(), 9))
					{
						DatosCorrectos = false;
					}
				}
			}


			//********************
			//familiar de contacto
			//********************
			//SI HA INTRODUCIDO EL SEGUNDO TELEFONO Y NO EL PRIMERO
			if (tbPCTelf1.Text == "" && tbPCTelf2.Text != "")
			{
				tbPCTelf1.Text = tbPCTelf2.Text;
				tbPCExt1.Text = tbPCExt2.Text;
				tbPCExt2.Text = "";
				tbPCTelf2.Text = "";
			}

			//si introduce algo en el familiar de contacto
			//comprueba que por lo menos este el nombre, apellido1 y el telf
			if (tbPCNombre.Text != "" || tbPCApe1.Text != "" || tbPCApe2.Text != "")
			{
				if (tbPCNombre.Text == "")
				{
					DatosCorrectos = false;
				}
				else
				{
					if (tbPCApe1.Text == "")
					{
						DatosCorrectos = false;
					}
				}
			}

			//si ha chequeado residencia en extranjero y no ha introducido el pais
			if (chbPCExtrangero.CheckState == CheckState.Checked && Convert.ToDouble(cbbPCPais.get_ListIndex()) == -1)
			{
				DatosCorrectos = false;
			}


			//Esta variable (DatosCorrectosNIF_PC) es para que no haya problemas al calcular el NIF si el formato no es el correcto
			bool DatosCorrectosNIF_PC = true; //(maplaza)(04/05/2006)Para que no haya problemas al calcular el NIF si el formato no es el correcto //inicialmente


			//(maplaza)(03/05/2006)Si lo que se queda escrito en la caja de texto del DNI/NIF no se corresponde con un
			//DNI ni con un NIF, los datos no son correctos, y no se podr�n guardar esos datos.
			if (rbDNIOtroDocPC[0].IsChecked)
			{
				if (tbNIFOtrosDocPC.Text.Trim() != "")
				{
					if (!MFiliacion.EsDNI_NIF(tbNIFOtrosDocPC.Text.Trim(), 9))
					{
						DatosCorrectos = false;
						DatosCorrectosNIF_PC = false;
					}
				}
			}

			// Comprobamos que el formato de la p�liza sea correcto

			if (rbRERegimen[1].IsChecked)
			{
				if (!proCoincideMascara(tbRENPoliza.Text))
				{
					// Si est� vac�a, lo dejamos

					if (tbRENPoliza.Text != MascaraVacia())
					{
						tbRENPoliza.BackColor = Color.FromArgb(255, 128, 128);

						// Si estamos en el registro de uno nuevo, no permitimos continuar si no se sigue la m�scara

						if (MFiliacion.stFiliacion == "ALTA")
						{
							DatosCorrectos = false;
						}
					}
					else
					{
						tbRENPoliza.BackColor = Color.White;
					}
				}
				else
				{
					tbRENPoliza.BackColor = Color.White;
				}
			}
			else
			{
				tbRENPoliza.BackColor = Color.White;
			}

			cbAceptar.Enabled = DatosCorrectos;

			cbPCAceptar.Enabled = tbPCApe1.Text.Trim() != "" && tbPCNombre.Text.Trim() != "" && DatosCorrectosNIF_PC;

		}

        private void MANDAR_DATOS()
		{
			//AQUI SE DEBEN MANDAR LOS DATOS ACTUALIZADOS DEL PACIENTE
			MFiliacion.NuevoDFI120F1.DATOS_ACTUALIZADOS();
		}

		//private void VACIARDATOS_PRIVADO()
		//{
				//tbREApe1.Text = "";
				//tbREApe2.Text = "";
				//tbRENombre.Text = "";
				//tbREDomicilio.Text = "";
				//tbREPasaporte.Text = "";
				//cbbREPoblacion.Text = "";
				//if (bFormatoCodigoPos)
				//{
					//mebRECodPostal.Text = "     ";
				//}
				//else
				//{
					//mebRECodPostal.Text = "";
				//}
		//}

		public void LIMPIAR_FORMULARIO()
		{
			//ficha Datos paciente
			lbDPHistoria.Text = "";
			lbDPEFinanciadora.Text = "";
			lbDPNAsegurado.Text = "";
			//(maplaza)(16/05/2006)
			if (tbNIFOtrosDocDP.Mask == MFiliacion.stFORMATO_NIF)
			{
				tbNIFOtrosDocDP.Text = "         ";
			}
			else
			{
				tbNIFOtrosDocDP.Text = "";
			}
			//-----
			//TARJETA SANITARIA
			tbTarjeta.Text = "";
			//(maplaza)(07/08/2007)
			//Tarjeta Sanitaria Europea
			tbTarjetaEuropea.Text = "";
			lblFechaUltMod.Text = ""; //la fecha de la �ltima modificaci�n est� fuera de las pesta�as
			//-----
			//Domicilio
			//*******************************
			cbbDPVia.set_ListIndex(-1);
			tbDPDomicilioP.Text = "";
			tbDPNDomic.Text = "";
			tbDPOtrosDomic.Text = "";
			tbDPDomicilio.Text = "";
			//*******************************
			//O.Frias - 03/07/2012


			tbObservaciones.Text = ""; //OSCAR C ENE 2005
			chkObservaciones.CheckState = CheckState.Unchecked;
			cbbDPPoblacion.set_ListIndex(-1);
			cbbNacPoblacion.set_ListIndex(-1);
			if (bFormatoCodigoPos)
			{
				mebDPCodPostal.Text = "     ";
			}
			else
			{
				mebDPCodPostal.Text = "";
			}
			//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
			mebDPCodPostal.Tag = mebDPCodPostal.Text;
			//-----
			tbDPTelf1.Text = "";
			tbDPTelf2.Text = "";
			tbDPTelf3.Text = "";
			chbSMS.CheckState = CheckState.Unchecked;
			chbSMS.Enabled = false;
			//EXTENSIONES
			cbbDPEstado.set_ListIndex(-1);
			//tbDPCatSocial.Text = "" NO SE LLAMA ASI
			chbDPExitus.Text = "";
			//sdcDPFechaFallecimiento.Text = Null

			//ficha regimen economico
			rbRERegimen[0].IsChecked = false;
			rbRERegimen[1].IsChecked = false;
			rbRERegimen[2].IsChecked = false;
			cbbREInspeccion.set_ListIndex(-1);
			tbRENSS.Text = "";
			//cbbREIndicador.Text = ""
			tbRENomTitular.Text = "";
			cbbRESociedad.SelectedIndex = -1;
			bCambiandoMascara = true;
			tbRENPoliza.Mask = "";
			bCambiandoMascara = false;
			ToolTipMain.SetToolTip(tbRENPoliza, "");
			tbRENPoliza.Text = "";
			chkExentoIVA.CheckState = CheckState.Unchecked;
			tbRENVersion.Text = "";
			CodigoDeControl = -1;
			Algoritmo = -1;
			tbREEmpresa.Text = "";
			//(maplaza)(23/08/2007)
			tbRECobertura.Text = "";
			mebREFechaAlta.Text = "  /    ";
			mebREFechaCaducidad.Text = "  /    ";
			//-----
			tbREApe1.Text = "";
			tbREApe2.Text = "";
			tbRENombre.Text = "";
			//(maplaza)(16/05/2006)
			if (tbNIFOtrosDocRE.Mask == MFiliacion.stFORMATO_NIF)
			{
				tbNIFOtrosDocRE.Text = "         ";
			}
			else
			{
				tbNIFOtrosDocRE.Text = "";
			}
			//-----
			tbREDomicilio.Text = "";
			tbREPasaporte.Text = "";
			cbbREPoblacion.set_ListIndex(-1);
			if (bFormatoCodigoPos)
			{
				mebRECodPostal.Text = "     ";
			}
			else
			{
				mebRECodPostal.Text = "";
			}

			tbPCApe1.Text = "";
			tbPCApe2.Text = "";
			tbPCNombre.Text = "";
			tbPCDomicilio.Text = "";
			cbbPCPoblacion.set_ListIndex(-1);
			if (bFormatoCodigoPos)
			{
				mebPCCodPostal.Text = "     ";
			}
			else
			{
				mebPCCodPostal.Text = "";
			}
			//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
			mebPCCodPostal.Tag = mebPCCodPostal.Text;
			//-----
			tbPCTelf1.Text = "";
			tbPCTelf2.Text = "";
			//EXTENSIONES
			tbPCParentesco.Text = "";
			sprContactos.MaxRows = 0;
		}

		private void Actualizar_Exitus()
		{
			string SQLProgQui = String.Empty;

			//busca el codigo de motivo de suspension exitus
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select gmsusint from qmsusint where qmsusint.icexitus = 'S'", MFiliacion.GConexion);
			DataSet RrSLQMotSus = new DataSet();
			tempAdapter.Fill(RrSLQMotSus);
			string CodSus = Convert.ToString(RrSLQMotSus.Tables[0].Rows[0]["gmsusint"]);
			RrSLQMotSus.Close();

			//********************************************
			//Suspension de las programaciones quirurgicas
			//********************************************
			if (MFiliacion.stFiliacion == "ALTA")
			{
				object tempRefParam = MFiliacion.vstFechaHoraSis;
				SQLProgQui = "select * from qprogqui where qprogqui.gidenpac = ' & CODIGO & ' and qprogqui.isitprog = 'P' and qprogqui.finterve > " + Serrores.FormatFechaHM(tempRefParam) + "";
				MFiliacion.vstFechaHoraSis = Convert.ToString(tempRefParam);
			}

			if (MFiliacion.stFiliacion == "MODIFICACION")
			{
				MFiliacion.FechaHoraSistema();
				object tempRefParam2 = MFiliacion.vstFechaHoraSis;
				SQLProgQui = "select * from qprogqui where qprogqui.gidenpac = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' and qprogqui.isitprog = 'P' and qprogqui.finterve > " + Serrores.FormatFechaHM(tempRefParam2) + "";
				MFiliacion.vstFechaHoraSis = Convert.ToString(tempRefParam2);
			}

			//busca si tiene programaciones quirurgicas futuras

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLProgQui, MFiliacion.GConexion);
			DataSet RrSQLProgQui = new DataSet();
			tempAdapter_2.Fill(RrSQLProgQui);
			//si tiene se supenden
			DataSet RrSLQSusProg = null;
			if (RrSQLProgQui.Tables[0].Rows.Count != 0)
			{

				//modifica el campo de situacion de la programacion e
				//inserta una fila en la tabla de suspension de
				//programaciones quirurgicas por cada progarmacion quirurgica futura
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter("select * from qsusprog where 1 = 2", MFiliacion.GConexion);
				RrSLQSusProg = new DataSet();
				tempAdapter_3.Fill(RrSLQSusProg);
				foreach (DataRow iteration_row in RrSQLProgQui.Tables[0].Rows)
				{

					RrSQLProgQui.Edit();
					iteration_row["ISITPROG"] = "S";
					string tempQuery = RrSQLProgQui.Tables[0].TableName;
					//SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                    tempAdapter_2.Update(RrSQLProgQui, RrSQLProgQui.Tables[0].TableName);


					RrSLQSusProg.AddNew();
					RrSLQSusProg.Tables[0].Rows[0]["ganoregi"] = iteration_row["ganoregi"];
					RrSLQSusProg.Tables[0].Rows[0]["gnumregi"] = iteration_row["gnumregi"];
					RrSLQSusProg.Tables[0].Rows[0]["fintsusp"] = iteration_row["finterve"];
					RrSLQSusProg.Tables[0].Rows[0]["gmedicoc"] = iteration_row["gmedicoc"];
					RrSLQSusProg.Tables[0].Rows[0]["gmsusint"] = CodSus;
					RrSLQSusProg.Tables[0].Rows[0]["fsuspens"] = DateTime.Now;
					string tempQuery_2 = RrSLQSusProg.Tables[0].TableName;
					//SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery_2, "");
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);
					tempAdapter_3.Update(RrSLQSusProg, RrSLQSusProg.Tables[0].TableName);
					RrSLQSusProg.Close();
				}
			}
			RrSQLProgQui.Close();

			//**********************************
			//Suspension de las citas progamadas
			//**********************************
			string SQLcitas = String.Empty;
			string StSqlPres = String.Empty;
			DataSet RrSqlPres = null;

			AnulacionCita.clsAnulacionCita ClaseAnulacionCita = null;


			if (MFiliacion.stFiliacion == "ALTA")
			{
				object tempRefParam3 = DateTime.Now;
				SQLcitas = "select * from ccitprog where ccitprog.gidenpac = ' & CODIGO & ' and ccitprog.ffeccita > " + Serrores.FormatFechaHM(tempRefParam3) + "";
			}

			if (MFiliacion.stFiliacion == "MODIFICACION")
			{
				object tempRefParam4 = DateTime.Now;
				SQLcitas = "select * from ccitprog where ccitprog.gidenpac = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' and ccitprog.ffeccita > " + Serrores.FormatFechaHM(tempRefParam4) + "";
			}

			SqlDataAdapter tempAdapter_6 = new SqlDataAdapter("select gmanucit from cmanucit where cmanucit.icexitus = 'S'", MFiliacion.GConexion);
			RrSLQMotSus = new DataSet();
			tempAdapter_6.Fill(RrSLQMotSus);
			CodSus = Convert.ToString(RrSLQMotSus.Tables[0].Rows[0]["gmanucit"]);
			RrSLQMotSus.Close();

			//busca si tiene citas programadas futuras
			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(SQLcitas, MFiliacion.GConexion);
			DataSet RrSQLcitas = new DataSet();
			tempAdapter_7.Fill(RrSQLcitas);
			if (RrSQLcitas.Tables[0].Rows.Count != 0)
			{
				ClaseAnulacionCita = new AnulacionCita.clsAnulacionCita();
				//Inserta una fila en la tabla de anulacion de
				//citas por cada cita programada futura
				RrSQLcitas.MoveFirst();
				foreach (DataRow iteration_row_2 in RrSQLcitas.Tables[0].Rows)
				{
					short tempRefParam5 = (short) iteration_row_2["ganoregi"];
					int tempRefParam6 = Convert.ToInt32(iteration_row_2["gnumregi"]);
					string tempRefParam7 = "PROG";
					string tempRefParam8 = Convert.ToString(iteration_row_2["gusuario"]);
					object tempRefParam9 = CodSus;
					string tempRefParam10 = String.Empty;
					ClaseAnulacionCita.Anulacion_Cita(tempRefParam5, tempRefParam6,ref tempRefParam7, ref MFiliacion.GConexion, tempRefParam8, tempRefParam9,ref tempRefParam10);
					CodSus = Convert.ToString(tempRefParam9);
				}
				ClaseAnulacionCita = null;
			}
			RrSQLcitas.Close();

			//**********************************
			//Suspension de las citas pendientes
			//**********************************

			if (MFiliacion.stFiliacion == "ALTA")
			{
				SQLcitas = "select * from ccitpend where ccitpend.gidenpac = ' & CODIGO & '";
			}

			if (MFiliacion.stFiliacion == "MODIFICACION")
			{
				SQLcitas = "select * from ccitpend where ccitpend.gidenpac = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' ";
			}

			//busca si tiene citas pendientes
			SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SQLcitas, MFiliacion.GConexion);
			RrSQLcitas = new DataSet();
			tempAdapter_8.Fill(RrSQLcitas);
			if (RrSQLcitas.Tables[0].Rows.Count != 0)
			{
				ClaseAnulacionCita = new AnulacionCita.clsAnulacionCita();
				//Inserta una fila en la tabla de anulacion de
				//citas por cada cita pendiente
				RrSQLcitas.MoveFirst();
				foreach (DataRow iteration_row_3 in RrSQLcitas.Tables[0].Rows)
				{
					short tempRefParam11 = (short) iteration_row_3["ganoregi"];
					int tempRefParam12 = Convert.ToInt32(iteration_row_3["gnumregi"]);
					string tempRefParam13 = "PEN";
					string tempRefParam14 = Convert.ToString(iteration_row_3["gusuario"]);
					object tempRefParam15 = CodSus;
					string tempRefParam16 = String.Empty;
					ClaseAnulacionCita.Anulacion_Cita(tempRefParam11, tempRefParam12, ref tempRefParam13, ref MFiliacion.GConexion, tempRefParam14, tempRefParam15, ref tempRefParam16);
					CodSus = Convert.ToString(tempRefParam15);
				}
				ClaseAnulacionCita = null;
			}
			RrSQLcitas.Close();


			if (MFiliacion.stFiliacion == "ALTA")
			{
				SQLcitas = "select * from CCONSULT where gidenpac = ' & CODIGO & '";
			}

			if (MFiliacion.stFiliacion == "MODIFICACION")
			{
				SQLcitas = "select * from CCONSULT where gidenpac = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]) + "' ";
			}

			//busca si tiene prestaciones asociadas a las consultas pendientes o comunicadas y las marcamos como anuladas.
			SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(SQLcitas, MFiliacion.GConexion);
			RrSQLcitas = new DataSet();
			tempAdapter_9.Fill(RrSQLcitas);
			if (RrSQLcitas.Tables[0].Rows.Count != 0)
			{
				RrSQLcitas.MoveFirst();
				foreach (DataRow iteration_row_4 in RrSQLcitas.Tables[0].Rows)
				{
					StSqlPres = "select * from EPRESTAC where ganoregi =  " + Convert.ToString(iteration_row_4["ganoregi"]) + " and gnumregi = " + Convert.ToString(iteration_row_4["gnumregi"]) + " and itiposer = 'C' and (iestsoli = 'P' or iestsoli = 'C')";
					SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(StSqlPres, MFiliacion.GConexion);
					RrSqlPres = new DataSet();
					tempAdapter_10.Fill(RrSqlPres);
					if (RrSqlPres.Tables[0].Rows.Count != 0)
					{
						foreach (DataRow iteration_row_5 in RrSqlPres.Tables[0].Rows)
						{
							RrSqlPres.Edit();
							iteration_row_5["iestsoli"] = "A";
							iteration_row_5["fanulaci"] = DateTime.Now;
							iteration_row_5["itiprela"] = DBNull.Value;
							iteration_row_5["ganorela"] = DBNull.Value;
							iteration_row_5["gnumrela"] = DBNull.Value;
							iteration_row_5["fprevista"] = DBNull.Value;
							string tempQuery_3 = RrSqlPres.Tables[0].TableName;
							//SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(tempQuery_3, "");
							SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_10);
                            tempAdapter_10.Update(RrSqlPres, RrSqlPres.Tables[0].TableName);
						}
					}
				}
				ClaseAnulacionCita = null;
			}
			RrSQLcitas.Close();

		}

        public object REFERENCIAS(object CLASE, object OBJETO, string stFiliaci, string stMod)
		{
			MFiliacion.CLASE2 = CLASE;
			MFiliacion.OBJETO2 = OBJETO;
			MFiliacion.stFiliacion = stFiliaci;
			MFiliacion.stModulo = stMod;

			return null;
		}

        private void MANDAR_DATOS2()
		{
			string SQLDent = String.Empty;
			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT * FROM DPACIENT WHERE GIDENPAC = '" + MFiliacion.codigo + "'", MFiliacion.GConexion);            
            MFiliacion.RrDPACIENT = new DataSet();
			tempAdapter.Fill(MFiliacion.RrDPACIENT);
            tempAdapterPaciente = tempAdapter;
            MFiliacion.Nombre2 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DNOMBPAC"]);
			MFiliacion.Apellido1 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE1PAC"]);
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE2PAC"]))
			{
				MFiliacion.Apellido2 = "";
			}
			else
			{
				MFiliacion.Apellido2 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DAPE2PAC"]);
			}
			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["FNACIPAC"]))
			{
				MFiliacion.fechaNac = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["FNACIPAC"]);
			}
			else
			{
				MFiliacion.fechaNac = "";
			}
			MFiliacion.Sexo = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ITIPSEXO"]);
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"]))
			{
				MFiliacion.Domicilio = "";
			}
			else
			{
				MFiliacion.Domicilio = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["DDIREPAC"]);
			}
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCODIPOS"]))
			{
				CodPostal = "";
			}
			else
			{
				CodPostal = StringsHelper.Format(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GCODIPOS"], "00000");
			}

			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"]))
			{
				MFiliacion.codPob = "";
			}
			else
			{
				MFiliacion.codPob = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GPOBLACI"]);
			}


			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]))
			{
				MFiliacion.NIF = "";
			}
			else
			{
				if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]).Trim().Length == 9)
				{
					MFiliacion.NIF = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"]).Trim();
				}
				else
				{
					MFiliacion.NIF = StringsHelper.Format(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NDNINIFP"], "00000000") + " ";
				}
			}


			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"]))
			{
				MFiliacion.Asegurado = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["NAFILIAC"]);
			}
			else
			{
				MFiliacion.Asegurado = "";
			}
			MFiliacion.idPaciente = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GIDENPAC"]);
			//busca la historia clinica del paciente
			//Set Rrsqlhist = GConexion.OpenResultset("select ghistoria from hdossier where hdossier.gidenpac = '" & RrDPACIENT("gidenpac") & "' AND icontenido = 'H' AND gserpropiet = " & ArchivoCentral, rdOpenKeyset)
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter("select ghistoria from hdossier where hdossier.gidenpac = '" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gidenpac"]) + "' ", MFiliacion.GConexion);
			Rrsqlhist = new DataSet();
			tempAdapter_2.Fill(Rrsqlhist);
			if (Rrsqlhist.Tables[0].Rows.Count != 0)
			{
				MFiliacion.Historia = Convert.ToString(Rrsqlhist.Tables[0].Rows[0]["ghistoria"]).Trim();
			}
			else
			{
				MFiliacion.Historia = "";
			}
			Rrsqlhist.Close();
			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]))
			{
				MFiliacion.CodigoSoc = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]);
			}
			else
			{
				MFiliacion.CodigoSoc = "";
			}
			DataSet RrSQLDent = null;
			if (Convert.ToDouble(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["GSOCIEDA"]) == MFiliacion.CodSS)
			{
				SQLDent = "select iactpens, ginspecc from dentipac where gsocieda= " + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gsocieda"]) + " and gidenpac ='" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["gidenpac"]) + "'";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLDent, MFiliacion.GConexion);
				RrSQLDent = new DataSet();
				tempAdapter_3.Fill(RrSQLDent);
				if (RrSQLDent.Tables[0].Rows.Count != 0)
				{
					if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["iactpens"]))
					{
						MFiliacion.IndAcPen = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["iactpens"]);
					}
					if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["ginspecc"]))
					{
						MFiliacion.Inspec = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["ginspecc"]);
					}
				}
				RrSQLDent.Close();
			}
			else
			{
				MFiliacion.IndAcPen = "";
				MFiliacion.Inspec = "";
			}
			MFiliacion.Filprov = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[0]["ifilprov"]);
			MFiliacion.RrDPACIENT.Close();
			if (MFiliacion.RegresodeOpenlab != "" && MFiliacion.RegresodeOpenlab != "ConDatos")
			{
				MFiliacion.RegresodeOpenlab = MFiliacion.idPaciente;
			}
			else
			{
				if (MFiliacion.stDevolverDatos != "NO")
				{
					MFiliacion.OBJETO2.Recoger_datos(MFiliacion.Nombre2, MFiliacion.Apellido1, MFiliacion.Apellido2, MFiliacion.fechaNac, MFiliacion.Sexo, MFiliacion.Domicilio, MFiliacion.NIF, MFiliacion.idPaciente, CodPostal, MFiliacion.Asegurado, MFiliacion.codPob, MFiliacion.Historia, MFiliacion.CodigoSoc, MFiliacion.Filprov, MFiliacion.IndAcPen, MFiliacion.Inspec);
				}
			}
			if (MFiliacion.stDevolverDatos != "NO")
			{
				MFiliacion.idPaciente = "";
			}
		}

		private void tsDatosPac_Enter(Object eventSender, EventArgs eventArgs)
		{
			switch(SSTabHelper.GetSelectedIndex(tsDatosPac))
			{
				case 0 : 
					//(maplaza)(03/05/2006) 
					if (tbNIFOtrosDocDP.Enabled)
					{
						tbNIFOtrosDocDP.Focus();
					}
					else
					{
						if (tbTarjeta.Enabled)
						{
							tbTarjeta.Focus();
						}
					} 
					break;
				case 2 : 
					tbPCApe1.Focus(); 
					break;
			}
		}

        private void Vaciar_datos()
		{
			if (MFiliacion.RegresodeOpenlab != "")
			{
				return;
			}
			else
			{
				MFiliacion.Nombre2 = "";
				MFiliacion.Apellido1 = "";
				MFiliacion.Apellido2 = "";
				MFiliacion.fechaNac = "";
				MFiliacion.Sexo = "";
				MFiliacion.Domicilio = "";
				CodPostal = "";
				MFiliacion.codPob = "";
				MFiliacion.Historia = "";
				MFiliacion.NIF = "";
				MFiliacion.Asegurado = "";
				MFiliacion.idPaciente = "";
				MFiliacion.CodigoSoc = "";
				MFiliacion.Filprov = "";
				MFiliacion.IndAcPen = "";
				MFiliacion.Inspec = "";
				if (MFiliacion.stDevolverDatos != "NO")
				{					
					MFiliacion.OBJETO2.Recoger_datos(MFiliacion.Nombre2, MFiliacion.Apellido1, MFiliacion.Apellido2, MFiliacion.fechaNac, MFiliacion.Sexo, MFiliacion.Domicilio, MFiliacion.NIF, MFiliacion.idPaciente, CodPostal, MFiliacion.Asegurado, MFiliacion.codPob, MFiliacion.Historia, MFiliacion.CodigoSoc, MFiliacion.Filprov, MFiliacion.IndAcPen, MFiliacion.Inspec);
				}
				MFiliacion.idPaciente = "";
			}
		}

        private void Generar_Identificador()
		{
			//Se crea un array con las vocales para compararlas con la cadena
			MFiliacion.Vocales = new string[]{"A", "E", "I", "O", "U", "�", "�", "�", "�", "�", "�"};
			//Coge el primer apellido
			MFiliacion.caracteres = Strings.Len(tbApellido1.Text);
			//Transforma en may�sculas
			MFiliacion.cadena = tbApellido1.Text.ToUpper();
			Codigo_Paciente();
			//si tiene, coge el segundo apellido
			if (tbApellido2.Text != "")
			{
				MFiliacion.caracteres = Strings.Len(tbApellido2.Text);
				//Transforma en may�sculas
				MFiliacion.cadena = tbApellido2.Text.ToUpper();
				//Coge las dos primeras consonantes del apellido 1� y 2�
				Codigo_Paciente();
			}
			else
			{
				//si no tiene segundo apellido
				MFiliacion.codigo = MFiliacion.codigo + "XX";
			}
			//Transforma la fecha en A�o/Mes/D�a
			//sdcFechaNac.Mask = YearMonthDay
			//Coge solo los d�gitos de la fecha
			Fecha_Nacimiento();
		}

        private void Codigo_Paciente()
		{
			//declara variable para controlar que coja las dos primeras consomantes
			int cont = 0;
			//declara variable para saber si es consonante
			bool vocal = false;
			//Declara variable para introducir cada letra del apellido
			string Letra = String.Empty;
			//Declara variable para recorrer la cadena
			//Declara variable para recorrer la matriz
			for (int i = 1; i <= MFiliacion.caracteres; i++)
			{
				Letra = MFiliacion.cadena.Substring(i - 1, Math.Min(1, MFiliacion.cadena.Length - (i - 1)));
				if (Strings.Asc(Letra[0]) >= Strings.Asc('A') && Strings.Asc(Letra[0]) <= Strings.Asc('Z'))
				{
					if (Letra.Trim() != "")
					{
						if (Letra == "�")
						{
							if (cont < 2)
							{
								MFiliacion.codigo = MFiliacion.codigo + "X";
								cont++;
							}
						}
						else
						{
							for (int v = 0; v <= 10; v++)
							{
								if (Letra == MFiliacion.Vocales[v])
								{
									vocal = true;
									break;
								}
							}
							if (!vocal)
							{
								if (cont < 2)
								{
									MFiliacion.codigo = MFiliacion.codigo + Letra;
									cont++;
								}
							}
							else
							{
								vocal = false;
							}
						}
					}
				}
			}
			if (cont < 2)
			{
				vocal = false;
				for (int i = 1; i <= MFiliacion.caracteres; i++)
				{
					Letra = MFiliacion.cadena.Substring(i - 1, Math.Min(1, MFiliacion.cadena.Length - (i - 1)));
					if (Letra.Trim() != "")
					{
						//If letra = "�" Then
						//CODIGO = CODIGO & "X"
						//cont = cont + 1
						//Else
						for (int v = 0; v <= 10; v++)
						{
							if (Letra == MFiliacion.Vocales[v])
							{
								vocal = true;
								break;
							}
						}
						if (vocal)
						{
							if (cont < 2)
							{
								MFiliacion.codigo = MFiliacion.codigo + "X";
								cont++;
							}
						}
						else
						{
							vocal = false;
						}
						//End If
					}
				}
				if (cont == 1)
				{
					MFiliacion.codigo = MFiliacion.codigo + "X";
				}
				else
				{
					if (cont == 0)
					{
						MFiliacion.codigo = MFiliacion.codigo + "XX";
					}
				}
			}
		}
        //Para Coger los n�meros de la fecha de nacimento
        //para generar el identificador y no coger (/)

        private void Fecha_Nacimiento()
		{
			string Letra = sdcFechaNac.Value.Year.ToString();
			MFiliacion.codigo = MFiliacion.codigo + Letra;
			Letra = sdcFechaNac.Value.Month.ToString();
			MFiliacion.codigo = MFiliacion.codigo + Letra;
			Letra = sdcFechaNac.Value.Day.ToString();
			//si es mujer se le suma 40 al d�a de la fecha
			if (rbSexo[1].IsChecked)
			{
				Letra = (Double.Parse(Letra) + 40).ToString();
			}
			MFiliacion.codigo = MFiliacion.codigo + Letra;
		}

        private void Identificador_Repetido()
		{
			string UDIGITO = String.Empty;
			int cont = 0;
			int cont2 = 1;
			string SQLRepetidos = "Select GIDENPAC FROM DPACIENT WHERE GIDENPAC LIKE  '" + MFiliacion.codigo + "%' ORDER BY GIDENPAC";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLRepetidos, MFiliacion.GConexion);
			DataSet RrSQLRepetidos = new DataSet();
			tempAdapter.Fill(RrSQLRepetidos);
			//Controla si existen registros con los parametros seleccionados
			if (RrSQLRepetidos.Tables[0].Rows.Count == 0)
			{
				MFiliacion.codigo = MFiliacion.codigo + "0";
			}
			else
			{
				RrSQLRepetidos.MoveFirst();
				cont = 0;
				foreach (DataRow iteration_row in RrSQLRepetidos.Tables[0].Rows)
				{
					UDIGITO = Convert.ToString(iteration_row["GIDENPAC"]).Substring(12);
					if (String.CompareOrdinal(UDIGITO, "0") >= 0 && String.CompareOrdinal(UDIGITO, "9") <= 0)
					{
						if (StringsHelper.ToDoubleSafe(UDIGITO) != cont)
						{
							MFiliacion.codigo = MFiliacion.codigo + cont.ToString();
							return;
						}
						else
						{
							cont++;
						}
					}
					else
					{
						//si encuentra mas de 10 pacientes con digito
						//se busca los de las letras
						if (UDIGITO != Strings.Chr(64 + cont2).ToString())
						{
							MFiliacion.codigo = MFiliacion.codigo + Strings.Chr(64 + cont2).ToString();
							return;
						}
						else
						{
							cont2++;
						}
					}
				}
				if (cont != 10)
				{
					MFiliacion.codigo = MFiliacion.codigo + cont.ToString();
				}
				if (cont == 10)
				{
					MFiliacion.codigo = MFiliacion.codigo + Strings.Chr(64 + cont2).ToString();
				}
			}
			RrSQLRepetidos.Close();
		}

        private void comprobar_gidenpac()
		{
			string Sexo_persona = String.Empty;
			cambiar_identificador = false;
			if (tbApellido1.Text != VApellido1)
			{
				cambiar_identificador = true;
			}
			if (tbApellido2.Text != VApellido2)
			{
				cambiar_identificador = true;
			}
			if (sdcFechaNac.Value.ToString("dd/MM/yyyy") != Vfecha_nacimiento)
			{
				cambiar_identificador = true;
			}
			if (rbSexo[0].IsChecked)
			{
				Sexo_persona = "0";
			}
			if (rbSexo[1].IsChecked)
			{
				Sexo_persona = "1";
			}
			if (!rbSexo[0].IsChecked && !rbSexo[1].IsChecked)
			{
				Sexo_persona = "2";
			}
			if (Sexo_persona != VSexo_persona)
			{
				cambiar_identificador = true;
			}
		}

		//**********************************************************************************************
		//*
		//*  O.Frias (13/10/2007) - Incorporo el cambio del gidenpac en las tablas de la BD-Web.
		//*
		//**********************************************************************************************
		private void CAMBIO_DE_GIDENPAC()
		{
            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {
                try
                {
                    //UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();
                    //primero se da de alta el paciente con los datos nuevos

                    Gidensql = "select * from DPACIENT where gidenpac = '" + MFiliacion.ModCodigo + "'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(Gidensql, MFiliacion.GConexion);
                    RrGiden = new DataSet();
                    tempAdapter.Fill(RrGiden);
                    if (RrGiden.Tables[0].Rows.Count != 0)
                    {
                        Gidensql1 = "select * from DPACIENT where 1=2";
                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(Gidensql1, MFiliacion.GConexion);
                        RrGiden1 = new DataSet();
                        tempAdapter_2.Fill(RrGiden1);
                        tempAdapterRrGiden1 = tempAdapter_2;
                        RrGiden1.AddNew();
                        Generar_Registro_Nuevo();
                        string tempQuery = RrGiden1.Tables[0].TableName;
                        //SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapterRrGiden1);
                        tempAdapterRrGiden1.Update(RrGiden1, RrGiden1.Tables[0].TableName);
                        RrGiden1.Close();
                    }
                    //seguidamente se modifican las demas tablas donde estuviese el paciente
                    Tablassql = "select sysobjects.name from syscolumns,sysobjects where " +
                                "syscolumns.name = 'gidenpac' and syscolumns.id = sysobjects.id " +
                                "and sysobjects.type = 'U' and sysobjects.name <> 'DPACIENT' and sysobjects.name not like 'W%'";

                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(Tablassql, MFiliacion.GConexion);
                    RrTablas = new DataSet();
                    tempAdapter_4.Fill(RrTablas);
                    RrTablas.MoveFirst();
                    foreach (DataRow iteration_row in RrTablas.Tables[0].Rows)
                    {
                        Tabla = Convert.ToString(iteration_row["name"]);
                        if (Tabla != "DPACIBEL")
                        {
                            if (Tabla == "DINSHPAC")
                            {
                                SqlCommand tempCommand = new SqlCommand("Delete " + Tabla + " where gidenpac = '" + MFiliacion.codigo + "'", MFiliacion.GConexion);
                                tempCommand.ExecuteNonQuery();
                            }
                            SqlCommand tempCommand_2 = new SqlCommand("update " + Tabla + " set gidenpac = '" + MFiliacion.codigo + "' where gidenpac = '" + MFiliacion.ModCodigo + "'", MFiliacion.GConexion);
                            tempCommand_2.ExecuteNonQuery();
                        }
                    }
                    RrTablas.Close();

                    //oscar 28/07/03
                    Serrores.Datos_Conexion(MFiliacion.GConexion);
                    Gidensql = "CambioGidenpac";
                    //------
                    SqlCommand RrCamGiden = new SqlCommand(); 

                    RrCamGiden = MFiliacion.GConexion.CreateQuery("Cambios", Gidensql);
                    RrCamGiden.CommandType = CommandType.StoredProcedure;

                    RrCamGiden.Parameters.Add("@GidenpacAntiguo", SqlDbType.Char).Size = 13;
                    RrCamGiden.Parameters.Add("@GidenpacNuevo", SqlDbType.Char).Size = 13;
                    RrCamGiden.Parameters.Add("@gusuario", SqlDbType.Char).Size = 8;

                    RrCamGiden.Parameters[0].Direction = ParameterDirection.Input;
                    RrCamGiden.Parameters[1].Direction = ParameterDirection.Input;
                    RrCamGiden.Parameters[2].Direction = ParameterDirection.Input; //oscar 28/07/03

                    RrCamGiden.Parameters[0].Value = MFiliacion.ModCodigo;
                    RrCamGiden.Parameters[1].Value = MFiliacion.codigo;
                    RrCamGiden.Parameters[2].Value = Serrores.VVstUsuarioApli; //oscar 28/07/03

                    RrCamGiden.ExecuteNonQuery();

                    RrCamGiden.Dispose();

                    //por �ltimo se da de baja el paciente antiguo
                    Gidensql = "select * from DPACIENT where gidenpac = '" + MFiliacion.ModCodigo + "'";
                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(Gidensql, MFiliacion.GConexion);
                    RrGiden = new DataSet();
                    tempAdapter_5.Fill(RrGiden);
                    if (RrGiden.Tables[0].Rows.Count != 0)
                    {
                        RrGiden.Delete(tempAdapter_5);
                    }
                    RrGiden.Close();
                    
                    //UpgradeStubs.System_Data_SqlClient_SqlConnection.CommitTrans();
                    scope.Complete();
                    // Si tenemos Labsuite instalado, llamamos al procedimiento para actualizar all�

                    Gidensql = "select isNull(valfanu1,'N') valfanu1 from SCONSGLO where gconsglo = 'ILABSUIT'";
                    SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(Gidensql, MFiliacion.GConexion);
                    RrGiden = new DataSet();
                    tempAdapter_6.Fill(RrGiden);
                    if (RrGiden.Tables[0].Rows.Count != 0)
                    {
                        if (Convert.ToString(RrGiden.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
                        {
                            Gidensql = "ACTUALIZA_GIDENPAC_LABSUITE";
                            RrCamGiden = MFiliacion.GConexion.CreateQuery("ActualizaGidenpac", Gidensql);
                            RrCamGiden.CommandType = CommandType.StoredProcedure;

                            RrCamGiden.Parameters.Add("@GIDENPAC_ANTIGUO", SqlDbType.VarChar).Size = 13;
                            RrCamGiden.Parameters.Add("@GIDENPAC_NUEVO", SqlDbType.VarChar).Size = 13;

                            RrCamGiden.Parameters[0].Direction = ParameterDirection.Input;
                            RrCamGiden.Parameters[1].Direction = ParameterDirection.Input;

                            RrCamGiden.Parameters[0].Value = MFiliacion.ModCodigo;
                            RrCamGiden.Parameters[1].Value = MFiliacion.codigo;

                            RrCamGiden.ExecuteNonQuery();

                            RrCamGiden.Dispose();

                        }
                    }
                    RrGiden.Close();

                    //'    '************** O.Frias - 13/10/2007 **************
                    //'    CambioUsuarWeb ModCodigo, CODIGO

                    //'O.Frias - 17/09/2013
                    procesoTIGESTION(MFiliacion.ModCodigo, DateTime.Now.ToString("yyyy/MM/dd"), DateTime.Now.ToString("HH:mm"), MFiliacion.codigo);

                    SqlDataAdapter tempAdapter_7 = new SqlDataAdapter("SELECT * FROM DPACIENT WHERE GIDENPAC = '" + MFiliacion.codigo + "'", MFiliacion.GConexion);                    
                    MFiliacion.RrDPACIENT = new DataSet();
                    tempAdapter_7.Fill(MFiliacion.RrDPACIENT);
                    tempAdapterPaciente = tempAdapter_7;
                }
                catch (SqlException ex)
                {                    
                    //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();                    
                    Serrores.GestionErrorBaseDatos(ex, MFiliacion.GConexion);
                }
                catch (Exception exep)
                {
                    System.Console.WriteLine(exep.Message);
                }
            }
		}

        private void Generar_Registro_Nuevo()
		{
			//Se introducen en variables los nuevos valores que pueden afectar al
			//identificador de paciente

			RrGiden1.Tables[0].Rows[0]["GIDENPAC"] = MFiliacion.codigo.Trim().ToUpper();
			RrGiden1.Tables[0].Rows[0]["DAPE1PAC"] = tbApellido1.Text.Trim().ToUpper();
			if (tbApellido2.Text.Trim().ToUpper() == "")
			{
				RrGiden1.Tables[0].Rows[0]["DAPE2PAC"] = DBNull.Value;
			}
			else
			{
				RrGiden1.Tables[0].Rows[0]["DAPE2PAC"] = tbApellido2.Text.Trim().ToUpper();
			}

			if (tbNombre.Text.Trim().ToUpper() == "")
			{
				RrGiden1.Tables[0].Rows[0]["DNOMBPAC"] = DBNull.Value;
			}
			else
			{
				RrGiden1.Tables[0].Rows[0]["DNOMBPAC"] = tbNombre.Text.Trim().ToUpper();
			}

			if (rbSexo[0].IsChecked)
			{
				RrGiden1.Tables[0].Rows[0]["ITIPSEXO"] = "H";
			}
			if (rbSexo[1].IsChecked)
			{
				RrGiden1.Tables[0].Rows[0]["ITIPSEXO"] = "M";
			}
			if (!rbSexo[0].IsChecked && !rbSexo[1].IsChecked)
			{
				RrGiden1.Tables[0].Rows[0]["ITIPSEXO"] = "I";
			}

			//si cambiase el sexo se actualiza la tabla de dcamasbo SI ESTUBIESE
			string sqlcamas = String.Empty;
			DataSet Rrsqlcamas = null;
			if (RrGiden1.Tables[0].Rows[0]["itipsexo"] != RrGiden.Tables[0].Rows[0]["itipsexo"])
			{
				sqlcamas = "select itipsexo from dcamasbo where gidenpac = '" + MFiliacion.ModCodigo + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlcamas, MFiliacion.GConexion);
				Rrsqlcamas = new DataSet();
				tempAdapter.Fill(Rrsqlcamas);
				if (Rrsqlcamas.Tables[0].Rows.Count != 0)
				{
					Rrsqlcamas.MoveFirst();
					foreach (DataRow iteration_row in Rrsqlcamas.Tables[0].Rows)
					{
						Rrsqlcamas.Edit();
						iteration_row["itipsexo"] = RrGiden1.Tables[0].Rows[0]["itipsexo"];
						string tempQuery = Rrsqlcamas.Tables[0].TableName;
						
						SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                        tempAdapter.Update(Rrsqlcamas, Rrsqlcamas.Tables[0].TableName);
					}
				}
				Rrsqlcamas.Close();
			}
			if (sdcFechaNac.Text != "")
			{
				RrGiden1.Tables[0].Rows[0]["FNACIPAC"] = sdcFechaNac.Text;
			}
			else
			{
				RrGiden1.Tables[0].Rows[0]["FNACIPAC"] = DBNull.Value;
			}
			//********************************************************************************
			//Se introducen en variables los valores restantes que no afectan al
			//identificador de paciente

			string tstColumn = String.Empty;

            //foreach (UpgradeStubs.RDO_rdoColumn tCl in RrGiden.Tables[0].Rows[0].col)
            for (int i=0;i < RrGiden.Tables[0].Columns.Count;i++)
            {                
				tstColumn = RrGiden.Tables[0].Columns[i].ColumnName.Trim().ToUpper();
				if (tstColumn != "GIDENPAC" && tstColumn != "DAPE1PAC" && tstColumn != "DAPE2PAC" && tstColumn != "DNOMBPAC" && tstColumn != "ITIPSEXO" && tstColumn != "FNACIPAC")
				{
					if (Convert.IsDBNull(RrGiden.Tables[0].Rows[0]["" + tstColumn + ""]))
					{
						RrGiden1.Tables[0].Rows[0]["" + tstColumn + ""] = DBNull.Value;
					}
					else
					{
						RrGiden1.Tables[0].Rows[0]["" + tstColumn + ""] = Convert.ToString(RrGiden.Tables[0].Rows[0]["" + tstColumn + ""]).Trim();
					}
				}
			}
			//**********************************************************************************

			//If IsNull(RrGiden("NDNINIFP")) Then
			//    RrGiden1("NDNINIFP") = Null
			//Else
			//    RrGiden1("NDNINIFP") = Trim(RrGiden("NDNINIFP"))
			//End If
			//
			//If IsNull(RrGiden("DDIREPAC")) Then
			//    RrGiden1("DDIREPAC") = Null
			//Else
			//    RrGiden1("DDIREPAC") = RrGiden("DDIREPAC")
			//End If
			//
			//If IsNull(RrGiden("GCODIPOS")) Then
			//    RrGiden1("GCODIPOS") = Null
			//Else
			//    RrGiden1("GCODIPOS") = Format(RrGiden("GCODIPOS"), "00000")
			//End If
			//
			//If IsNull(RrGiden("GPOBLACI")) Then
			//    RrGiden1("GPOBLACI") = Null
			//Else
			//    RrGiden1("GPOBLACI") = RrGiden("GPOBLACI")
			//End If
			//
			//If IsNull(RrGiden("DPOBLACI")) Then
			//    RrGiden1("DPOBLACI") = Null
			//Else
			//    RrGiden1("DPOBLACI") = RrGiden("DPOBLACI")
			//End If
			//
			//If IsNull(RrGiden("GPAISRES")) Then
			//    RrGiden1("GPAISRES") = Null
			//Else
			//    RrGiden1("GPAISRES") = RrGiden("GPAISRES")
			//End If
			//
			//If IsNull(RrGiden("NTELEFO1")) Then
			//    RrGiden1("NTELEFO1") = Null
			//Else
			//    RrGiden1("NTELEFO1") = RrGiden("NTELEFO1")
			//End If
			//
			//If IsNull(RrGiden("NEXTESI1")) Then
			//    RrGiden1("NEXTESI1") = Null
			//Else
			//    RrGiden1("NEXTESI1") = RrGiden("NEXTESI1")
			//End If
			//
			//If IsNull(RrGiden("NTELEFO2")) Then
			//    RrGiden1("NTELEFO2") = Null
			//Else
			//    RrGiden1("NTELEFO2") = RrGiden("NTELEFO2")
			//End If
			//
			//If IsNull(RrGiden("NEXTESI2")) Then
			//    RrGiden1("NEXTESI2") = Null
			//Else
			//    RrGiden1("NEXTESI2") = RrGiden("NEXTESI2")
			//End If
			//
			//If IsNull(RrGiden("GESTACIV")) Then
			//    RrGiden1("GESTACIV") = Null
			//Else
			//    RrGiden1("GESTACIV") = RrGiden("GESTACIV")
			//End If
			//
			//If IsNull(RrGiden("GCATASOC")) Then
			//    RrGiden1("GCATASOC") = Null
			//Else
			//    RrGiden1("GCATASOC") = RrGiden("GCATASOC")
			//End If
			//If IsNull(RrGiden("GSOCIEDA")) Then
			//    RrGiden1("GSOCIEDA") = Null
			//Else
			//    RrGiden1("GSOCIEDA") = RrGiden("GSOCIEDA")
			//End If
			//
			//If IsNull(RrGiden("NAFILIAC")) Then
			//    RrGiden1("NAFILIAC") = ""
			//Else
			//    RrGiden1("NAFILIAC") = RrGiden("NAFILIAC")
			//End If
			//
			//If IsNull(RrGiden("IEXITUSP")) Then
			//    RrGiden1("IEXITUSP") = Null
			//Else
			//    RrGiden1("IEXITUSP") = RrGiden("IEXITUSP")
			//End If
			//
			//If IsNull(RrGiden("FFALLECI")) Then
			//    RrGiden1("FFALLECI") = Null
			//Else
			//    RrGiden1("FFALLECI") = RrGiden("FFALLECI")
			//End If
			RrGiden1.Tables[0].Rows[0]["gusuario"] = (Serrores.VVstUsuarioApli.Trim() == "") ? "" : Serrores.VVstUsuarioApli.Trim();
			RrGiden1.Tables[0].Rows[0]["FULTMOVI"] = DateTime.Today;
			//Se introduce filiacion definitiva
			RrGiden1.Tables[0].Rows[0]["IFILPROV"] = "N";
		}

        private void GENERAR_IDENTIFICADOR_PROVISIONAL()
		{
			MFiliacion.codigo = "";
			//Se crea un array con las vocales para compararlas con la cadena
			MFiliacion.Vocales = new string[]{"A", "E", "I", "O", "U", "�", "�", "�", "�", "�", "�", "-", ";", ",", "_", "`", "�", "."};
			//Si tiene, coge el primer apellido
			if (tbApellido1.Text != "")
			{
				MFiliacion.caracteres = Strings.Len(tbApellido1.Text);
				//Transforma en may�sculas
				MFiliacion.cadena = tbApellido1.Text.ToUpper();
				Codigo_Paciente();
				//si tiene, coge el segundo apellido
				if (tbApellido2.Text != "")
				{
					MFiliacion.caracteres = Strings.Len(tbApellido2.Text);
					//Transforma en may�sculas
					MFiliacion.cadena = tbApellido2.Text.ToUpper();
					Codigo_Paciente();
				}
				else
				{
					//si tiene apellido 1 y no tiene apellido 2
					MFiliacion.codigo = MFiliacion.codigo + "ZZ";
				}
			}
			else
			{
				//si no tiene se introducen 4 zetas
				MFiliacion.codigo = "ZZZZ";
			}
		}

        private void IDENTIFICADOR_PROVISIONAL_REPETIDO()
		{
			string SQLRepetidos = String.Empty;
			DataSet RrSQLRepetidos = null;
			string identificador = String.Empty;
			int contador = 0;
			string Scontador = String.Empty;

			if (tbApellido1.Text != "")
			{ //SI EXISTE EL PRIMER APELLIDO
				if (tbApellido2.Text != "")
				{ //SI EXISTE EL PRIMERO
					//SELECCIONO TODOS LOS QUE COINCIDAN CON
					//FILIACION PROVISIONAL = SI Y QUE TENGAN APELLIDO1 Y APELLIDO 2
					SQLRepetidos = "Select GIDENPAC FROM DPACIENT WHERE GIDENPAC LIKE '" + MFiliacion.codigo + "0000%' " + " order by GIDENPAC ";
					//& " AND (ifilprov = 'S' OR FNACIPAC IS NULL)  order by GIDENPAC"
					SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLRepetidos, MFiliacion.GConexion);
					RrSQLRepetidos = new DataSet();
					tempAdapter.Fill(RrSQLRepetidos);
					if (RrSQLRepetidos.Tables[0].Rows.Count == 0)
					{
						//NO HAY COINCIDENCIAS
						MFiliacion.codigo = MFiliacion.codigo + "000000001";
					}
					else
					{
						//coge el ultimo filiado provisionalmente
						RrSQLRepetidos.MoveLast(null);
						identificador = Convert.ToString(RrSQLRepetidos.Tables[0].Rows[0]["gidenpac"]);
						contador = Convert.ToInt32(Double.Parse(identificador.Substring(4)));
						Scontador = StringsHelper.Format(contador + 1, "000000000");
						MFiliacion.codigo = MFiliacion.codigo + Scontador;
					}
				}
				else
				{
					//SELECCIONO TODOS LOS QUE COINCIDAN CON
					//FILIACION PROVISIONAL = SI Y QUE COINCIDA APELLIDO1 Y NO TENGAN
					//APELLIDO 2
					SQLRepetidos = "Select GIDENPAC FROM DPACIENT WHERE GIDENPAC LIKE '" + MFiliacion.codigo + "0000%' " + " order by GIDENPAC ";
					//& " AND (ifilprov = 'S' OR FNACIPAC IS NULL) order by GIDENPAC"
					//AND (ifilprov = 'S' OR FNACIPAC IS NULL) AND DAPE2PAC is null"
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLRepetidos, MFiliacion.GConexion);
					RrSQLRepetidos = new DataSet();
					tempAdapter_2.Fill(RrSQLRepetidos);
					if (RrSQLRepetidos.Tables[0].Rows.Count == 0)
					{
						//NO HAY COINCIDENCIAS
						MFiliacion.codigo = MFiliacion.codigo + "000000001";
					}
					else
					{
						//coge el ultimo filiado provisionalmente
						RrSQLRepetidos.MoveLast(null);
						identificador = Convert.ToString(RrSQLRepetidos.Tables[0].Rows[0]["gidenpac"]);
						contador = Convert.ToInt32(Double.Parse(identificador.Substring(4)));
						Scontador = StringsHelper.Format(contador + 1, "000000000");
						MFiliacion.codigo = MFiliacion.codigo + Scontador;
					}
				}
			}
			else
			{
				//SELECCIONO TODOS LOS QUE COINCIDAN CON
				//FILIACION PROVISIONAL = SI Y QUE NO TENGAN APELLIDO1 NI APELLIDO 2
				SQLRepetidos = "Select GIDENPAC FROM DPACIENT WHERE GIDENPAC LIKE '" + MFiliacion.codigo + "0000%'" + " order by GIDENPAC ";
				// & " AND (ifilprov = 'S' OR FNACIPAC IS NULL) order by GIDENPAC "
				//AND DAPE1PAC is null AND DAPE2PAC is null ORDER BY gidenpac"
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLRepetidos, MFiliacion.GConexion);
				RrSQLRepetidos = new DataSet();
				tempAdapter_3.Fill(RrSQLRepetidos);
				if (RrSQLRepetidos.Tables[0].Rows.Count == 0)
				{
					//NO HAY COINCIDENCIAS
					MFiliacion.codigo = MFiliacion.codigo + "000000001";
				}
				else
				{
					//coge el ultimo filiado provisionalmente
					RrSQLRepetidos.MoveLast(null);
					identificador = Convert.ToString(RrSQLRepetidos.Tables[0].Rows[0]["gidenpac"]);
					contador = Convert.ToInt32(Double.Parse(identificador.Substring(4)));
					Scontador = StringsHelper.Format(contador + 1, "000000000");
					MFiliacion.codigo = MFiliacion.codigo + Scontador;
				}
			}
			//If stModulo <> "CITACION" Then
			//    GRABAR_REGISTRO
			//End If
		}

        private void ObtenerPersonasContacto()
		{
			string StSql = String.Empty;
			this.Cursor = Cursors.WaitCursor;
			rbResponsable[1].IsChecked = true;
			if (MFiliacion.stFiliacion == "MODIFICACION")
			{
				StSql = "SELECT dperscon.*, dpoblaci.dpoblaci AS Poblacion, dpaisres.dpaisres " + 
				        "FROM dperscon " + 
				        "left join  dpoblaci on dperscon.gpoblaci = dpoblaci.gpoblaci " + 
				        "left join  dpaisres on dperscon.gpaisres = dpaisres.gpaisres " + 
				        "Where " + 
				        "gidenpac = '" + MFiliacion.ModCodigo.ToUpper() + "'  " + 
				        "ORDER BY nnumorde";
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
			DataSet rsContactos = new DataSet();
			tempAdapter.Fill(rsContactos);
            tempAdapterRsContactos = tempAdapter;
            iUltimoNOrden = 0;
			foreach (DataRow iteration_row in rsContactos.Tables[0].Rows)
			{
				sprContactos.MaxRows++;
				sprContactos.Row = sprContactos.MaxRows;
				sprContactos.Col = 1;
				sprContactos.Text = Convert.ToString(iteration_row["dape1per"]).Trim();
				if (!Convert.IsDBNull(iteration_row["dape2per"]))
				{
					sprContactos.Text = sprContactos.Text + " " + Convert.ToString(iteration_row["dape2per"]).Trim();
				}
				sprContactos.Text = sprContactos.Text + ", " + Convert.ToString(iteration_row["dnombper"]).Trim();
				sprContactos.Col = 2;
				if (!Convert.IsDBNull(iteration_row["ndninifp"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["ndninifp"]).Trim();
				}
				sprContactos.Col = 3;
				if (!Convert.IsDBNull(iteration_row["ddireper"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["ddireper"]).Trim();
				}
				sprContactos.Col = 4;
				if (!Convert.IsDBNull(iteration_row["gcodipos"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["gcodipos"]).Trim();
				}
				sprContactos.Col = 5;
				if (!Convert.IsDBNull(iteration_row["Poblacion"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["Poblacion"]).Trim();
				}
				sprContactos.Col = 6;
				if (!Convert.IsDBNull(iteration_row["ntelefo1"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["ntelefo1"]).Trim();
				}
				if (!Convert.IsDBNull(iteration_row["nextesi1"]))
				{
					sprContactos.Text = sprContactos.Text + "-" + Convert.ToString(iteration_row["nextesi1"]);
				}
				sprContactos.Col = 7;
				if (!Convert.IsDBNull(iteration_row["ntelefo2"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["ntelefo2"]).Trim();
				}
				if (!Convert.IsDBNull(iteration_row["nextesi2"]))
				{
					sprContactos.Text = sprContactos.Text + "-" + Convert.ToString(iteration_row["nextesi2"]);
				}
				sprContactos.Col = 8;
				if (!Convert.IsDBNull(iteration_row["dparente"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["dparente"]).Trim();
				}
				sprContactos.Col = 9;
				sprContactos.Text = (Convert.ToString(iteration_row["irespons"]) == "S") ? "SI" : "NO";
				sprContactos.Col = 10;
				if (!Convert.IsDBNull(iteration_row["dpaisres"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["dpaisres"]).Trim();
				}
				sprContactos.Col = 11;
				if (!Convert.IsDBNull(iteration_row["dpoblaci"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["dpoblaci"]).Trim();
				}
				sprContactos.Col = 12;
				sprContactos.Text = Convert.ToString(iteration_row["nnumorde"]).Trim();
				iUltimoNOrden = Convert.ToInt32(iteration_row["nnumorde"]);
				sprContactos.Col = 13;
				if (!Convert.IsDBNull(iteration_row["gpoblaci"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["gpoblaci"]).Trim();
				}
				sprContactos.Col = 14;
				if (!Convert.IsDBNull(iteration_row["gpaisres"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["gpaisres"]).Trim();
				}
				sprContactos.Col = 15;
				sprContactos.Text = Convert.ToString(iteration_row["dnombper"]).Trim();
				sprContactos.Col = 16;
				sprContactos.Text = Convert.ToString(iteration_row["dape1per"]).Trim();
				sprContactos.Col = 17;
				if (!Convert.IsDBNull(iteration_row["dape2per"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["dape2per"]).Trim();
				}
				// Se guarda que la persona de contacto ya exist�a
				sprContactos.Col = 18;
				sprContactos.Text = "EXISTE";
				sprContactos.Col = 19;
				if (!Convert.IsDBNull(iteration_row["gprovinc"]))
				{
					sprContactos.Text = Convert.ToString(iteration_row["gprovinc"]).Trim();
				}

				//OSCAR C 21/11/2005
				sprContactos.Col = 20;
				sprContactos.Text = Convert.ToString(iteration_row["itipocon"]).Trim() + "";
				sprContactos.Col = 21;
				switch(Convert.ToString(iteration_row["itipocon"]).Trim() + "")
				{
					case "T" :  
						sprContactos.Text = rbTutor[0].Text.Trim().ToUpper(); 
						break;
					case "C" :  
						sprContactos.Text = rbTutor[1].Text.Trim().ToUpper(); 
						break;
					default: 
						sprContactos.Text = ""; 
						break;
				}
				//-----

			}
			this.Cursor = Cursors.Default;
		}

		private void sprContactos_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
            int Col = 0;
            int Row = 0;

            if (eventArgs != null)
            {
                Col = eventArgs.ColumnIndex + 1;
                Row = eventArgs.RowIndex + 1;
            }
            else
            {
                Col = sprContactos.Col;
                Row = sprContactos.Row;
            }

			if (Row <= 0)
			{
				return;
			}

			sprContactos.Col = Col;
			sprContactos.Row = Row;

			if (iFilaContactos == Row)
			{
				if (iFilaContactos != 0)
				{
					sprContactos.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
					iFilaContactos = 0;
					// Se limpian los datos de la parte inferior y se elimina la
					// seleccion
					//(maplaza)(11/07/2006)El evento "cbPCCancelar_Click" se encarga de llamar al m�todo "CargarDatosPorDefectoFamiliarContacto"
					cbPCCancelar_Click(cbPCCancelar, new EventArgs());
				}
				else
				{
					sprContactos.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
					iFilaContactos = Row;
					// Se cargan los datos de la fila seleccionada en la parte
					// inferior
					MostrarPersonaContacto();
				}
			}
			else
			{
				sprContactos.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.SingleSelect;
				iFilaContactos = Row;
				MostrarPersonaContacto();
			}

		}

        private void MostrarPersonaContacto()
		{
			int i = 0;
			string codProvincia = String.Empty;

			sprContactos.Row = iFilaContactos;
			sprContactos.Col = 16;
			tbPCApe1.Text = sprContactos.Text.Trim();
			sprContactos.Col = 17;
			tbPCApe2.Text = sprContactos.Text.Trim();
			sprContactos.Col = 15;
			tbPCNombre.Text = sprContactos.Text.Trim();
			sprContactos.Col = 2;

			//(maplaza)(03/05/2006)se muestran los datos correspondientes al "DNI/NIF", o bien a "Otros documentos"
			if (sprContactos.Text.Trim() != "")
			{
				if (MFiliacion.EsDNI_NIF(sprContactos.Text.Trim(), 9))
				{
					rbDNIOtroDocPC[0].IsChecked = true;
					rbDNIOtroDocPC[1].IsChecked = false;
					tbNIFOtrosDocPC.Enabled = true;
					if (sprContactos.Text.Trim().Length == 9)
					{
						tbNIFOtrosDocPC.Text = sprContactos.Text.Trim();
					}
					else
					{
						tbNIFOtrosDocPC.Text = StringsHelper.Format(sprContactos.Text.Trim(), "00000000") + " ";
					}
				}
				else
				{
					//si no se trata de un DNI ni de un NIF, es "Otros documentos"
					rbDNIOtroDocPC[0].IsChecked = false;
					rbDNIOtroDocPC[1].IsChecked = true;
					tbNIFOtrosDocPC.Enabled = true;
					tbNIFOtrosDocPC.Text = sprContactos.Text.Trim().Trim();
				}
			}
			else
			{
				//(Trim(.Text)="")
				rbDNIOtroDocPC[0].IsChecked = false;
				rbDNIOtroDocPC[1].IsChecked = false;
				tbNIFOtrosDocPC.Enabled = false;
				if (tbNIFOtrosDocPC.Mask == MFiliacion.stFORMATO_NIF)
				{
					tbNIFOtrosDocPC.Text = "         ";
				}
				else
				{
					tbNIFOtrosDocPC.Text = "";
				}
			}

			sprContactos.Col = 3;
			tbPCDomicilio.Text = sprContactos.Text.Trim();
			sprContactos.Col = 4;
			if (sprContactos.Text.Trim() != "")
			{
				mebPCCodPostal.Text = sprContactos.Text.Trim();
			}
			else
			{
				if (bFormatoCodigoPos)
				{
					mebPCCodPostal.Text = "     "; //PPoblacion_Con.BorrarCodigoPostal
				}
				else
				{
					mebPCCodPostal.Text = "";
				}
			}
			//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
			mebPCCodPostal.Tag = mebPCCodPostal.Text;
			//-----
			sprContactos.Col = 19;
			if (sprContactos.Text.Trim() == "")
			{
				sprContactos.Col = 13;
				if (sprContactos.Text.Trim() != "")
				{
					codProvincia = sprContactos.Text.Trim().Substring(0, Math.Min(2, sprContactos.Text.Trim().Length));
					sprContactos.Col = 19;
					sprContactos.Text = codProvincia;
				}
			}
			else
			{
				codProvincia = sprContactos.Text.Trim().Substring(0, Math.Min(2, sprContactos.Text.Trim().Length));
			}
			sprContactos.Col = 13;
			if (sprContactos.Text.Trim() != "")
			{
				if (sprContactos.Text.Trim().StartsWith(codProvincia))
				{
					cargar_poblaciones_PC(codProvincia);
					//BUSCAMOS LA POBLACION
					estabuscando = true;
					for (i = 0; i <= cbbPCPoblacion.ListCount - 1; i++)
					{
						object tempRefParam = 1;
						object tempRefParam2 = i;
						if (StringsHelper.Format(cbbPCPoblacion.get_Column(tempRefParam, tempRefParam2), "000000") == sprContactos.Text.Trim())
						{
							i = Convert.ToInt32(tempRefParam2);
							break;
						}
						else
						{
							i = Convert.ToInt32(tempRefParam2);
						}
					}
					cbbPCPoblacion.set_ListIndex(i);
					estabuscando = false;
				}
			}
			else
			{
				cbbPCPoblacion.set_ListIndex(-1);
			}
			sprContactos.Col = 6;
			if (sprContactos.Text.Trim() != "")
			{
				i = (sprContactos.Text.IndexOf('-') + 1);
				if (i == 0)
				{
					tbPCTelf1.Text = sprContactos.Text.Trim();
					tbPCExt1.Text = "";
				}
				else
				{
					tbPCTelf1.Text = sprContactos.Text.Substring(0, Math.Min(i - 1, sprContactos.Text.Length));
					tbPCExt1.Text = sprContactos.Text.Substring(i);
				}
			}
			else
			{
				tbPCTelf1.Text = "";
				tbPCExt1.Text = "";
			}
			sprContactos.Col = 7;
			if (sprContactos.Text.Trim() != "")
			{
				i = (sprContactos.Text.IndexOf('-') + 1);
				if (i == 0)
				{
					tbPCTelf2.Text = sprContactos.Text.Trim();
					tbPCExt2.Text = "";
				}
				else
				{
					tbPCTelf2.Text = sprContactos.Text.Substring(0, Math.Min(i - 1, sprContactos.Text.Length));
					tbPCExt2.Text = sprContactos.Text.Substring(i);
				}
			}
			else
			{
				tbPCTelf2.Text = "";
				tbPCExt2.Text = "";
			}
			sprContactos.Col = 8;
			tbPCParentesco.Text = sprContactos.Text.Trim();
			sprContactos.Col = 9;
			rbResponsable[0].IsChecked = sprContactos.Text == "SI";
			rbResponsable[1].IsChecked = sprContactos.Text == "NO";
			sprContactos.Col = 10;
			if (sprContactos.Text.Trim() != "")
			{
				chbPCExtrangero.CheckState = CheckState.Checked;
				sprContactos.Col = 14;
				for (i = 0; i <= cbbPCPais.ListCount - 1; i++)
				{
					object tempRefParam3 = 1;
					object tempRefParam4 = i;
					if (Convert.ToString(cbbPCPais.get_Column(tempRefParam3, tempRefParam4)).Trim().ToUpper() == sprContactos.Text.Trim().ToUpper())
					{
						i = Convert.ToInt32(tempRefParam4);
						cbbPCPais.set_ListIndex(i);
						break;
					}
					else
					{
						i = Convert.ToInt32(tempRefParam4);
					}
				}
			}
			else
			{
				chbPCExtrangero.CheckState = CheckState.Unchecked;
			}
			sprContactos.Col = 11;
			if (sprContactos.Text.Trim() != "")
			{
				tbPCExPob.Text = sprContactos.Text;
			}
			else
			{
				tbPCExPob.Text = "";
			}

			//OSCAR C 21/11/2005
			sprContactos.Col = 20;
			rbTutor[0].IsChecked = sprContactos.Text == "T";
			rbTutor[1].IsChecked = sprContactos.Text == "C";

			sprContactos.Col = 21;
			if (rbTutor[0].IsChecked)
			{
				sprContactos.Text = rbTutor[0].Text.Trim().ToUpper();
			}
			else
			{
				if (rbTutor[1].IsChecked)
				{
					sprContactos.Text = rbTutor[1].Text.Trim().ToUpper();
				}
				else
				{
					sprContactos.Text = "";
				}
			}
			//-----

			//(maplaza)(24/04/2007)Si no existe el C�digo Postal (en Base de Datos), pero los dos primeros d�gitos del
			//C�digo Postal s� corresponden a una provincia, entonces se cargan en el combo las poblaciones de la provincia.
			if (bFormatoCodigoPos)
			{
				if (mebPCCodPostal.Text.Trim().Length == 5)
				{
					if (Convert.ToDouble(cbbPCPoblacion.get_ListIndex()) == -1)
					{
						if (!fExisteCodigoPostal(mebPCCodPostal.Text.Trim()))
						{
							if (fExisteCodigoProvincia(mebPCCodPostal.Text.Trim().Substring(0, Math.Min(2, mebPCCodPostal.Text.Trim().Length))))
							{
								CargarPoblacionesProvincia(mebPCCodPostal.Text.Trim().Substring(0, Math.Min(2, mebPCCodPostal.Text.Trim().Length)), cbbPCPoblacion);
							}
						}
					}
				}
				//-----
			}

			COMPROBAR_DATOS(); //(maplaza)(04/05/2006)
			cbDPEliminar.Enabled = true;
		}

        private void cbDPEliminar_Click(Object eventSender, EventArgs eventArgs)
		{
			sprContactos.Row = iFilaContactos;
			sprContactos.Col = 18;
			if (sprContactos.Text == "NUEVA")
			{
				//sprContactos.Action = 5; // SS_ACTION_DELETE_ROW
				sprContactos.MaxRows--;
			}
			else
			{
				sprContactos.Col = 12;
				if (SePuedeEliminar(MFiliacion.ModCodigo, sprContactos.Text))
				{
					sprContactos.Col = 18;
					sprContactos.Text = "ELIMINADA";                    
					sprContactos.SetRowHidden(iFilaContactos,true);
				}
				else
				{
					string tempRefParam = "";
					short tempRefParam2 = 1310;
					string[] tempRefParam3 = new string[] { "eliminar", "relacionado con " + Frame3.Text + " en Urgencias"};
					MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, tempRefParam, tempRefParam2, MFiliacion.GConexion, tempRefParam3);
				}
			}

			//(maplaza)(11/07/2006)El evento "cbPCCancelar_Click" se encarga de llamar al m�todo "CargarDatosPorDefectoFamiliarContacto"
			cbPCCancelar_Click(cbPCCancelar, new EventArgs());

		}

        private void cbPCCancelar_Click(Object eventSender, EventArgs eventArgs)
		{

			tbPCApe1.Text = "";
			tbPCApe2.Text = "";
			tbPCNombre.Text = "";
			//(maplaza)(16/05/2006)
			if (tbNIFOtrosDocPC.Mask == MFiliacion.stFORMATO_NIF)
			{
				tbNIFOtrosDocPC.Text = "         ";
			}
			else
			{
				tbNIFOtrosDocPC.Text = "";
			}
			//para que no se pueda escribir en la caja sin pinchar antes en alguno de los radio button
			tbNIFOtrosDocPC.Enabled = false;
			//-----
			tbPCDomicilio.Text = "";
			if (bFormatoCodigoPos)
			{
				mebPCCodPostal.Text = "     ";
			}
			else
			{
				mebPCCodPostal.Text = "";
			}
			//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
			mebPCCodPostal.Tag = mebPCCodPostal.Text;
			//-----
			cbbPCPoblacion.set_ListIndex(-1);
			//CPPoblacion_Con.BorrarCodigoPostal
			//CPPoblacion_Con.BorrarPoblacion
			tbPCTelf1.Text = "";
			tbPCTelf2.Text = "";
			tbPCExt1.Text = "";
			tbPCExt2.Text = "";
			tbPCParentesco.Text = "";
			rbResponsable[0].IsChecked = false;
			rbResponsable[1].IsChecked = true;
			if (chbPCExtrangero.CheckState == CheckState.Checked)
			{
				chbPCExtrangero.CheckState = CheckState.Unchecked;
				cbbPCPais.set_ListIndex(-1);
				tbPCExPob.Text = "";
			}

			if (iFilaContactos != 0)
			{
				sprContactos.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
				iFilaContactos = 0;
			}
			cbPCAceptar.Enabled = false;
			cbDPEliminar.Enabled = false;

			//(maplaza)(03/05/2006)
			rbDNIOtroDocPC[0].IsChecked = false;
			rbDNIOtroDocPC[1].IsChecked = false;

			//OSCAR C 21/11/2005
			rbTutor[0].IsChecked = false;
			rbTutor[1].IsChecked = false;
			//------

			//(maplaza)(11/07/2006)Se cargan por defecto en la Ficha de Familiares de Contacto los datos existentes
			//en la primera pesta�a (Datos del Paciente)
			CargarDatosPorDefectoFamiliarContacto();
			//-----
		}

        private bool GrabarPersonasContacto()
		{
			int j = 0;
			string StSql = String.Empty;
			DataSet rsContactos = null;

            try
            {
                int tempForVar = sprContactos.MaxRows;
                for (int i = 1; i <= tempForVar; i++)
                {
                    sprContactos.Row = i;
                    sprContactos.Col = 18;

                    switch (sprContactos.Text.Trim())
                    {
                        case "MODIFICADA":
                        case "ELIMINADA":
                            sprContactos.Col = 12;
                            StSql = "SELECT * " +
                                    "FROM dperscon " +
                                    "WHERE gidenpac = '" + MFiliacion.ModCodigo + "' AND " +
                                    "nnumorde = " + sprContactos.Text;
                            SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
                            rsContactos = new DataSet();
                            tempAdapter.Fill(rsContactos);
                            tempAdapterRsContactos = tempAdapter;
                            rsContactos.Edit();
                            sprContactos.Col = 18;
                            if (sprContactos.Text.Trim() == "ELIMINADA")
                            {
                                rsContactos.Delete(tempAdapter);
                            }

                            break;
                        case "NUEVA":
                            StSql = "SELECT * " +
                                    "FROM dperscon " +
                                    "WHERE 1 = 2";
                            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, MFiliacion.GConexion);
                            rsContactos = new DataSet();
                            tempAdapter_2.Fill(rsContactos);
                            tempAdapterRsContactos = tempAdapter_2;
                            rsContactos.AddNew();
                            rsContactos.Tables[0].Rows[0]["gidenpac"] = MFiliacion.codigo.ToUpper();
                            sprContactos.Col = 12;
                            rsContactos.Tables[0].Rows[0]["nnumorde"] = Convert.ToInt32(Double.Parse(sprContactos.Text));

                            break;
                    }

                    sprContactos.Col = 18;
                    if (sprContactos.Text.Trim() == "MODIFICADA" || sprContactos.Text.Trim() == "NUEVA")
                    {
                        sprContactos.Col = 15;
                        rsContactos.Tables[0].Rows[0]["dnombper"] = (sprContactos.Text.Trim().ToUpper() == "") ? "" : sprContactos.Text.Trim().ToUpper();
                        sprContactos.Col = 16;
                        rsContactos.Tables[0].Rows[0]["dape1per"] = (sprContactos.Text.Trim().ToUpper() == "") ? "" : sprContactos.Text.Trim().ToUpper();
                        sprContactos.Col = 17;
                        if (sprContactos.Text.Trim() != "")
                        {
                            rsContactos.Tables[0].Rows[0]["dape2per"] = (sprContactos.Text.Trim().ToUpper() == "") ? "" : sprContactos.Text.Trim().ToUpper();
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["dape2per"] = DBNull.Value;
                        }
                        sprContactos.Col = 2;
                        if (sprContactos.Text.Trim() != "")
                        {
                            rsContactos.Tables[0].Rows[0]["ndninifp"] = sprContactos.Text.Trim().ToUpper();
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["ndninifp"] = DBNull.Value;
                        }
                        sprContactos.Col = 3;
                        if (sprContactos.Text.Trim() != "")
                        {
                            rsContactos.Tables[0].Rows[0]["ddireper"] = sprContactos.Text.Trim().ToUpper();
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["ddireper"] = DBNull.Value;
                        }
                        sprContactos.Col = 4;
                        if (sprContactos.Text.Trim() != "")
                        {
                            rsContactos.Tables[0].Rows[0]["gcodipos"] = sprContactos.Text.Trim();
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["gcodipos"] = DBNull.Value;
                        }
                        sprContactos.Col = 13;
                        if (sprContactos.Text.Trim() != "")
                        {
                            rsContactos.Tables[0].Rows[0]["gpoblaci"] = StringsHelper.Format(sprContactos.Text.Trim(), "000000");
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["gpoblaci"] = DBNull.Value;
                        }
                        sprContactos.Col = 11;
                        if (sprContactos.Text.Trim() != "")
                        {
                            rsContactos.Tables[0].Rows[0]["dpoblaci"] = sprContactos.Text.Trim().ToUpper();
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["dpoblaci"] = DBNull.Value;
                        }
                        sprContactos.Col = 14;
                        if (sprContactos.Text.Trim() != "")
                        {
                            rsContactos.Tables[0].Rows[0]["gpaisres"] = sprContactos.Text.Trim();
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["gpaisres"] = DBNull.Value;
                        }
                        sprContactos.Col = 6;
                        if (sprContactos.Text.Trim() != "")
                        {
                            j = (sprContactos.Text.IndexOf('-') + 1);
                            if (j == 0)
                            {
                                rsContactos.Tables[0].Rows[0]["ntelefo1"] = sprContactos.Text.Trim();
                                rsContactos.Tables[0].Rows[0]["nextesi1"] = DBNull.Value;
                            }
                            else
                            {
                                rsContactos.Tables[0].Rows[0]["ntelefo1"] = sprContactos.Text.Substring(0, Math.Min(j - 1, sprContactos.Text.Length));
                                rsContactos.Tables[0].Rows[0]["nextesi1"] = sprContactos.Text.Substring(j);
                            }
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["ntelefo1"] = DBNull.Value;
                            rsContactos.Tables[0].Rows[0]["nextesi1"] = DBNull.Value;
                        }
                        sprContactos.Col = 7;
                        if (sprContactos.Text.Trim() != "")
                        {
                            j = (sprContactos.Text.IndexOf('-') + 1);
                            if (j == 0)
                            {
                                rsContactos.Tables[0].Rows[0]["ntelefo2"] = sprContactos.Text.Trim();
                                rsContactos.Tables[0].Rows[0]["nextesi2"] = DBNull.Value;
                            }
                            else
                            {
                                rsContactos.Tables[0].Rows[0]["ntelefo2"] = sprContactos.Text.Substring(0, Math.Min(j - 1, sprContactos.Text.Length));
                                rsContactos.Tables[0].Rows[0]["nextesi2"] = sprContactos.Text.Substring(j);
                            }
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["ntelefo2"] = DBNull.Value;
                            rsContactos.Tables[0].Rows[0]["nextesi2"] = DBNull.Value;
                        }
                        sprContactos.Col = 8;
                        if (sprContactos.Text.Trim() != "")
                        {
                            rsContactos.Tables[0].Rows[0]["dparente"] = sprContactos.Text.Trim().ToUpper();
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["dparente"] = DBNull.Value;
                        }
                        sprContactos.Col = 9;
                        rsContactos.Tables[0].Rows[0]["irespons"] = (sprContactos.Text.StartsWith("S")) ? sprContactos.Text.Substring(0, Math.Min(1, sprContactos.Text.Length)) : DbNullValue;
                        sprContactos.Col = 19;
                        if (sprContactos.Text.Trim() != "")
                        {
                            rsContactos.Tables[0].Rows[0]["gprovinc"] = sprContactos.Text.Trim().ToUpper();
                        }
                        else
                        {
                            rsContactos.Tables[0].Rows[0]["gprovinc"] = DBNull.Value;
                        }

                        //OSCAR C 21/11/2005
                        sprContactos.Col = 20;
                        rsContactos.Tables[0].Rows[0]["itipocon"] = sprContactos.Text.Trim().ToUpper();
                        //--------

                        string tempQuery = rsContactos.Tables[0].TableName;
                        //SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapterRsContactos);
                        tempAdapterRsContactos.Update(rsContactos, rsContactos.Tables[0].TableName);
                    }
                }
                return true;
            }
            catch (SqlException sqexc)
            {
                System.Console.WriteLine(sqexc.Message);
                return false;
            }
            catch (Exception ex)
            {
                Serrores.GestionErrorUpdate(Information.Err().Number, MFiliacion.GConexion);
                return false;
                //DeshacerTransaccion
            }
		}

		private void cargar_poblaciones_PC(string CodPROV)
		{
			cbbPCPoblacion.Clear();
			string SQLDPOBLACION = "SELECT * FROM DPOBLACI WHERE GPOBLACI LIKE '" + CodPROV + "%' order by dpoblaci";
			CargarComboAlfanumerico(SQLDPOBLACION, "gpoblaci", "dpoblaci", cbbPCPoblacion);
		}

        public object CargarComboAlfanumerico(string sql, string CampoCod, string campoDes, RadDropDownList comboacargar, bool bVaciar = false)
		{
			int i = 0;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
			DataSet RrCombo = new DataSet();
			tempAdapter.Fill(RrCombo);
			if (bVaciar || false)
			{
				comboacargar.Items.Clear();				
			}
			foreach (DataRow iteration_row in RrCombo.Tables[0].Rows)
			{
				if (campoDes.Trim().ToUpper() == "DSOCIEDA")
				{
					if (Convert.ToString(iteration_row[CampoCod]).Trim().ToUpper() != MFiliacion.CodPriv.ToString().Trim().ToUpper())
					{
                        comboacargar.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row[campoDes]).Trim(), VbStrConv.ProperCase, 0), Convert.ToString(iteration_row[CampoCod])));                            
						i++;
					}
				}
				else
				{					
					comboacargar.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row[campoDes]).Trim(), VbStrConv.ProperCase, 0), Convert.ToString(iteration_row[CampoCod])));
				}
			}
			
			RrCombo.Close();
			return null;
		}

        public bool SePuedeEliminar(string ModCodigo, string numorde)
		{
			bool result = false;
			string sql = "SELECT * FROM UEPISURG WHERE gidenpac = '" + ModCodigo + "' AND " + 
			             "nnumorde = " + numorde;
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			result = (RR.Tables[0].Rows.Count == 0);
			RR.Close();
			return result;
		}

        private void Activa_N_Cuenta(bool activa)
		{
			if (!activa)
			{
				Elimina_N_Cuenta();
			}
			//Se pasa antes por la comprobaci�n de la constante. Y si est� activa (valfanu1='S')
			//se pone el FrmCB21 a igual Top que Frame9. Por otro lado, si no est� cargada, los dos marcos son invisibles
			if (((float) (FrmCB21.Top * 15)) == ((float) (Frame9.Top * 15)))
			{
				tbBanco1.Enabled = activa;
				tbSucursal1.Enabled = activa;
				tbControl1.Enabled = activa;
				tbCuenta1.Enabled = activa;
				tbControl12.Enabled = activa;
				//Si ya est� cargada la pantalla, uno de los dos marcos es visible
				if (activa && (FrmCB21.Visible || Frame9.Visible))
				{
					tbBanco1.Focus();
				}
			}
			else
			{
				tbBanco.Enabled = activa;
				tbSucursal.Enabled = activa;
				tbControl.Enabled = activa;
				tbCuenta.Enabled = activa;
				//Si ya est� cargada la pantalla, uno de los dos marcos es visible
				if (activa && (FrmCB21.Visible || Frame9.Visible))
				{
					tbBanco.Focus();
				}
			}
		}

        private object Elimina_N_Cuenta()
		{
			//Se pasa antes por la comprobaci�n de la constante. Y si est� activa (valfanu1='S')
			//se pone el FrmCB21 a igual Top que Frame9.
			if (((float) (FrmCB21.Top * 15)) == ((float) (Frame9.Top * 15)))
			{
				tbBanco1.Text = "";
				tbSucursal1.Text = "";
				tbControl1.Text = "";
				tbCuenta1.Text = "";
				tbControl12.Text = "";
			}
			else
			{
				tbBanco.Text = "";
				tbSucursal.Text = "";
				tbControl.Text = "";
				tbCuenta.Text = "";
			}
			return null;
		}

        public string Digito_control(RadTextBoxControl ventidad, RadTextBoxControl vsucursal, ref RadTextBoxControl vcuenta)
		{
			string result = String.Empty;
			int residuo1 = 0;
			int residuo2 = 0;
			int[] pesos = new int[]{0, 1, 2, 4, 8, 5, 10, 9, 7, 3, 6};
			result = "";
			vcuenta.Text = (new string('0', 10) + vcuenta.Text).Substring(Math.Max((new string('0', 10) + vcuenta.Text).Length - 10, 0));
			string primerdigito = ventidad.Text.Trim() + vsucursal.Text.Trim();
			if (primerdigito.Length != 8)
			{
				return result;
			}
			string segundodigito = vcuenta.Text.Trim();
			if (segundodigito.Length != 10)
			{
				return result;
			}
			double dbNumericTemp2 = 0;
			double dbNumericTemp = 0;
			if (!Double.TryParse(primerdigito, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp) || !Double.TryParse(segundodigito, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp2))
			{
				return result;
			}
			for (int m = 3; m <= 10; m++)
			{
				residuo1 = Convert.ToInt32(residuo1 + Conversion.Val(primerdigito.Substring(m - 3, Math.Min(1, primerdigito.Length - (m - 3)))) * pesos[m]);
			}
			for (int m = 1; m <= 10; m++)
			{
				residuo2 = Convert.ToInt32(residuo2 + Conversion.Val(segundodigito.Substring(m - 1, Math.Min(1, segundodigito.Length - (m - 1)))) * pesos[m]);
			}
			residuo1 = residuo1 % 11;
			residuo2 = residuo2 % 11;
			int DC1 = 11 - residuo1;
			int DC2 = 11 - residuo2;
			if (DC1 == 11)
			{
				DC1 -= 11;
			}
			else if (DC1 == 10)
			{ 
				DC1 = 11 - DC1;
			}
			if (DC2 == 11)
			{
				DC2 -= 11;
			}
			else if (DC2 == 10)
			{ 
				DC2 = 11 - DC2;
			}
			return DC1.ToString() + DC2.ToString();
		}

        //(maplaza)(20/04/2007)Se trata de saber si los dos primeros d�gitos del C�digo Postal est�n asociados a alguna
        //provincia, y de no ser as�, no se deja guardar los datos. Se pasa como par�metro el n�mero de pesta�a, porque
        //el C�digo Postal est� presente en las tres pesta�as de la pantalla, as� que la comprobaci�n se realizar� para
        //las 3 pesta�as.
        private bool ComprobarDigitosCP(int intPesta�a)
		{

			bool result = false;
			string stPrimerosDigitosCP = String.Empty;

			result = true; //inicialmente

			clase_mensaje = new Mensajes.ClassMensajes();

			if (intPesta�a == 0)
			{
				stPrimerosDigitosCP = mebDPCodPostal.Text.Substring(0, Math.Min(2, mebDPCodPostal.Text.Length));
			}
			else if ((intPesta�a == 1))
			{ 
				stPrimerosDigitosCP = mebRECodPostal.Text.Substring(0, Math.Min(2, mebRECodPostal.Text.Length));
			}
			else if ((intPesta�a == 2))
			{ 
				stPrimerosDigitosCP = mebPCCodPostal.Text.Substring(0, Math.Min(2, mebPCCodPostal.Text.Length));
			}

			if (stPrimerosDigitosCP.Trim() != "")
			{
				if (!fExisteCodigoProvincia(stPrimerosDigitosCP.Trim()))
				{
					if (intPesta�a == 0)
					{
						short tempRefParam = 4000;
						string[] tempRefParam2 = new string[] { "Datos Paciente"};
						clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, MFiliacion.GConexion, tempRefParam2);
					}
					else if ((intPesta�a == 1))
					{ 
						short tempRefParam3 = 4000;
                        string[] tempRefParam4 = new string[] { "R�gimen Econ�mico"};
						clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, MFiliacion.GConexion, tempRefParam4);
					}
					else if ((intPesta�a == 2))
					{ 
						short tempRefParam5 = 4000;
                        string[] tempRefParam6 = new string[] { "Familiar Contacto"};
						clase_mensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam5, MFiliacion.GConexion, tempRefParam6);
					}
					if (intPesta�a == 0 || intPesta�a == 1)
					{
						DatosCorrectos = false;
					}
					result = false;
					
					this.Cursor = Cursors.Default;
					if (intPesta�a == 0)
					{
						mebDPCodPostal.Focus();
					}
					else if ((intPesta�a == 1))
					{ 
						mebRECodPostal.Focus();
					}
					else if ((intPesta�a == 2))
					{ 
						mebPCCodPostal.Focus();
					}
				}
			}

			clase_mensaje = null;

			return result;
		}

		public void Comprobar_numero_cuenta()
		{
			if (cbbREPago.SelectedIndex != -1 && cbbREPago.Text.Trim() != "")
			{				
				int tempRefParam = cbbREPago.SelectedIndex;
				int tempRefParam2 = 2;
				if (getLisIndex(cbbREPago,tempRefParam, tempRefParam2) == "S")
				{
					if (FrmCB21.Visible)
					{
						if (Strings.Len(tbBanco1.Text) < 3)
						{
							RadMessageBox.Show("N�mero de cuenta incompleto", Application.ProductName);
							DatosCorrectos = false;
							this.Cursor = Cursors.Default;
							tbBanco1.Focus();
							return;
						}
						else if (Strings.Len(tbSucursal1.Text) < 4)
						{ 
							RadMessageBox.Show("N�mero de cuenta incompleto", Application.ProductName);
							DatosCorrectos = false;
							this.Cursor = Cursors.Default;
							tbSucursal1.Focus();
							return;
						}
						else if (Strings.Len(tbCuenta1.Text) < 13)
						{ 
							RadMessageBox.Show("N�mero de cuenta incompleto", Application.ProductName);
							DatosCorrectos = false;
							this.Cursor = Cursors.Default;
							tbCuenta1.Focus();
							return;
						}
						if (Strings.Len(tbControl1.Text) < 1)
						{
							RadMessageBox.Show("Primer d�gito de verificaci�n incompleto", Application.ProductName);
							DatosCorrectos = false;
							this.Cursor = Cursors.Default;
							tbControl1.Focus();
							return;
						}
						if (Strings.Len(tbControl12.Text) < 1)
						{
							RadMessageBox.Show("Segundo d�gito de verificaci�n incompleto", Application.ProductName);
							DatosCorrectos = false;
							this.Cursor = Cursors.Default;
							tbControl12.Focus();
							return;
						}
					}
					else
					{
						if (Strings.Len(tbBanco.Text) < 4)
						{
							RadMessageBox.Show("N�mero de cuenta incompleto", Application.ProductName);
							DatosCorrectos = false;
							this.Cursor = Cursors.Default;
							tbBanco.Focus();
							return;
						}
						else if (Strings.Len(tbSucursal.Text) < 4)
						{ 
							RadMessageBox.Show("N�mero de cuenta incompleto", Application.ProductName);
							DatosCorrectos = false;
							this.Cursor = Cursors.Default;
							tbSucursal.Focus();
							return;
						}
						else if (Strings.Len(tbCuenta.Text) < 10)
						{ 
							RadMessageBox.Show("N�mero de cuenta incompleto", Application.ProductName);
							DatosCorrectos = false;
							this.Cursor = Cursors.Default;
							tbCuenta.Focus();
							return;
						}
						if (tbControl.Text != Digito_control(tbBanco, tbSucursal, ref tbCuenta))
						{
							RadMessageBox.Show("N�mero de cuenta erroneo", Application.ProductName);
							DatosCorrectos = false;
							this.Cursor = Cursors.Default;
							tbControl.Focus();
							return;
						}
					}
				}
			}
		}

        public void Comprobar_DCODCIAS()
		{
			string sql = "SELECT * FROM DCODCIAS WHERE gcodcias = '" + MEBCIAS.Text.Trim() + "' AND " + 
			             "fborrado is null ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				DatosCorrectos = true;
			}
			else
			{
				RadMessageBox.Show("C�digo CIAS inexistente", Application.ProductName);
				DatosCorrectos = false;
				this.Cursor = Cursors.Default;
				MEBCIAS.Focus();
				DatosCorrectos = false;
			}
			RR.Close();
		}

		private void CambiarDatosPaciente()
		{
            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {
                try
                {                    
                    //UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();
                    //primero se da de alta el paciente con los datos nuevos

                    Gidensql = "select * from DPACIENT where gidenpac = '" + MFiliacion.ModCodigo + "'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(Gidensql, MFiliacion.GConexion);
                    RrGiden1 = new DataSet();
                    tempAdapter.Fill(RrGiden1);
                    tempAdapterRrGiden1 = tempAdapter;
                    RrGiden1.Edit();


                    RrGiden1.Tables[0].Rows[0]["DAPE1PAC"] = tbApellido1.Text.Trim().ToUpper();
                    if (tbApellido2.Text.Trim().ToUpper() == "")
                    {
                        RrGiden1.Tables[0].Rows[0]["DAPE2PAC"] = DBNull.Value;
                    }
                    else
                    {
                        RrGiden1.Tables[0].Rows[0]["DAPE2PAC"] = tbApellido2.Text.Trim().ToUpper();
                    }

                    if (tbNombre.Text.Trim().ToUpper() == "")
                    {
                        RrGiden1.Tables[0].Rows[0]["DNOMBPAC"] = DBNull.Value;
                    }
                    else
                    {
                        RrGiden1.Tables[0].Rows[0]["DNOMBPAC"] = tbNombre.Text.Trim().ToUpper();
                    }

                    if (rbSexo[0].IsChecked)
                    {
                        RrGiden1.Tables[0].Rows[0]["ITIPSEXO"] = "H";
                    }
                    if (rbSexo[1].IsChecked)
                    {
                        RrGiden1.Tables[0].Rows[0]["ITIPSEXO"] = "M";
                    }
                    if (!rbSexo[0].IsChecked && !rbSexo[1].IsChecked)
                    {
                        RrGiden1.Tables[0].Rows[0]["ITIPSEXO"] = "I";
                    }

                    string sqlcamas = String.Empty;
                    DataSet Rrsqlcamas = null;
                    sqlcamas = "select itipsexo,gplantas,ghabitac,gcamasbo from dcamasbo where gidenpac = '" + MFiliacion.ModCodigo + "'";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlcamas, MFiliacion.GConexion);
                    Rrsqlcamas = new DataSet();
                    tempAdapter_2.Fill(Rrsqlcamas);
                    if (Rrsqlcamas.Tables[0].Rows.Count != 0)
                    {
                        Rrsqlcamas.MoveFirst();
                        foreach (DataRow iteration_row in Rrsqlcamas.Tables[0].Rows)
                        {
                            Rrsqlcamas.Edit();
                            iteration_row["itipsexo"] = RrGiden1.Tables[0].Rows[0]["itipsexo"];
                            string tempQuery = Rrsqlcamas.Tables[0].TableName;
                            //SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery, "");
                            SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
                            tempAdapter_2.Update(Rrsqlcamas, Rrsqlcamas.Tables[0].TableName);
                        }
                    }
                    Rrsqlcamas.Close();
                    if (sdcFechaNac.Text != "")
                    {
                        RrGiden1.Tables[0].Rows[0]["FNACIPAC"] = sdcFechaNac.Text;
                    }
                    else
                    {
                        RrGiden1.Tables[0].Rows[0]["FNACIPAC"] = DBNull.Value;
                    }
                    RrGiden1.Tables[0].Rows[0]["FULTMOVI"] = DateTime.Today;
                    //Se introduce filiacion definitiva
                    RrGiden1.Tables[0].Rows[0]["IFILPROV"] = "N";


                    RrGiden1.Tables[0].Rows[0]["gusuario"] = (Serrores.VVstUsuarioApli.Trim() == "") ? "" : Serrores.VVstUsuarioApli.Trim();
                    string tempQuery_2 = RrGiden1.Tables[0].TableName;
                    //SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tempQuery_2, "");
                    SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapterRrGiden1);
                    tempAdapterRrGiden1.Update(RrGiden1, RrGiden1.Tables[0].TableName);
                    RrGiden1.Close();
                    
                    //UpgradeStubs.System_Data_SqlClient_SqlConnection.CommitTrans();
                    scope.Complete();
                    SqlDataAdapter tempAdapter_5 = new SqlDataAdapter("SELECT * FROM DPACIENT WHERE GIDENPAC = '" + MFiliacion.ModCodigo + "'", MFiliacion.GConexion);
                    MFiliacion.RrDPACIENT = new DataSet();                    
                    tempAdapter_5.Fill(MFiliacion.RrDPACIENT);
                    tempAdapterPaciente = tempAdapter_5;
                }
                catch (SqlException ex)
                {
                    //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();                    
                    Serrores.GestionErrorBaseDatos(ex, MFiliacion.GConexion);
                }
                catch (Exception exep)
                {
                    System.Console.WriteLine(exep.Message);
                }
            }
		}

		//OSCAR C FEB 2006
		private void proRellenaDatosRULEQ()
		{
			MEBCIAS.Text = "           ";
			int pos = (MFiliacion.stModulo.IndexOf('#') + 1);
			string stFACTIVOS_ID = MFiliacion.stModulo.Substring(pos);
			string sql = "SELECT CIP,CIPA, DNI, NASS, HP, " + 
			             " VIA, DOMICILIO, NUM, CPOSTAL, MUNICIPIO, TELEF1, TELEF2, MOVIL_TX " + 
			             " FROM RULEQ_IN WHERE FACTIVOS_ID='" + stFACTIVOS_ID + "' and ACCION_TX='CA' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);

			if (RR.Tables[0].Rows.Count != 0)
			{
				//Datos del Paciente
				//(maplaza)(03/05/2006)
				if ((Convert.ToString(RR.Tables[0].Rows[0]["DNI"]) + "").Trim() == "")
				{
					rbDNIOtroDocDP[0].IsChecked = false;
					rbDNIOtroDocDP[1].IsChecked = false;
					tbNIFOtrosDocDP.Enabled = false;
					//tbNIFOtrosDocDP.Text = ""
					if (tbNIFOtrosDocDP.Mask == MFiliacion.stFORMATO_NIF)
					{
						tbNIFOtrosDocDP.Text = "         ";
					}
					else
					{
						tbNIFOtrosDocDP.Text = "";
					}
				}
				else
				{
					//Trim(RR!DNI & "") <> ""
					if (MFiliacion.EsDNI_NIF(Convert.ToString(RR.Tables[0].Rows[0]["DNI"]).Trim(), 9))
					{
						rbDNIOtroDocDP[0].IsChecked = true;
						rbDNIOtroDocDP[1].IsChecked = false;
						tbNIFOtrosDocDP.Enabled = true;
						if (Convert.ToString(RR.Tables[0].Rows[0]["DNI"]).Trim().Length == 9)
						{
							tbNIFOtrosDocDP.Text = Convert.ToString(RR.Tables[0].Rows[0]["DNI"]).Trim();
						}
						else
						{
							tbNIFOtrosDocDP.Text = StringsHelper.Format(RR.Tables[0].Rows[0]["DNI"], "00000000") + " ";
						}
					}
					else
					{
						//no es DNI ni NIF (es "Otros documentos)
						rbDNIOtroDocDP[0].IsChecked = false;
						rbDNIOtroDocDP[1].IsChecked = true;
						tbNIFOtrosDocDP.Enabled = true;
						tbNIFOtrosDocDP.Text = Convert.ToString(RR.Tables[0].Rows[0]["DNI"]).Trim();
					}
					tbNIFOtrosDocDP_Leave(tbNIFOtrosDocDP, new EventArgs());
				}
				tbTarjeta.Text = (Convert.ToString(RR.Tables[0].Rows[0]["CIP"]) + "").Trim().ToUpper();
				lbCipAutonomico.Text = (Convert.ToString(RR.Tables[0].Rows[0]["CIPA"]) + "").Trim();
				cbbDPVia.Text = fBuscaVia((Convert.ToString(RR.Tables[0].Rows[0]["VIA"]) + "").Trim().ToUpper());
				tbDPDomicilioP.Text = (Convert.ToString(RR.Tables[0].Rows[0]["Domicilio"]) + "").Trim().ToUpper();
				tbDPNDomic.Text = (Convert.ToString(RR.Tables[0].Rows[0]["num"]) + "").Trim().ToUpper();
				if ((Convert.ToString(RR.Tables[0].Rows[0]["cpostal"]) + "").Trim() != "")
				{
					if (bFormatoCodigoPos)
					{
						mebDPCodPostal.Text = StringsHelper.Format((Convert.ToString(RR.Tables[0].Rows[0]["cpostal"]) + "").Trim().ToUpper(), "00000");
					}
					else
					{
						mebDPCodPostal.Text = (Convert.ToString(RR.Tables[0].Rows[0]["cpostal"]) + "").Trim().ToUpper();
					}
				}
				//(maplaza)(16/04/2007)El "Tag" se utiliza para controlar si hemos realizado cambios en la caja (m�scara) del C�digo Postal.
				mebDPCodPostal.Tag = mebDPCodPostal.Text;
				//-----
				cbbDPPoblacion.Text = fBuscaPoblacion((Convert.ToString(RR.Tables[0].Rows[0]["cpostal"]) + "").Trim().ToUpper(), (Convert.ToString(RR.Tables[0].Rows[0]["MUNICIPIO"]) + "").Trim().ToUpper());
				tbDPTelf1.Text = Serrores.fQuitaCaracteresNoNumericos((Convert.ToString(RR.Tables[0].Rows[0]["TELEF1"]) + "").Trim().ToUpper());
				tbDPTelf2.Text = Serrores.fQuitaCaracteresNoNumericos((Convert.ToString(RR.Tables[0].Rows[0]["TELEF2"]) + "").Trim().ToUpper());
				tbDPTelf3.Text = Serrores.fQuitaCaracteresNoNumericos((Convert.ToString(RR.Tables[0].Rows[0]["MOVIL_TX"]) + "").Trim().ToUpper());
				//Datos de la S.S
				rbRERegimen[0].IsChecked = true;
				cbbREInspeccion.Text = fBuscaInspeccionPrincipal();
				tbRENSS.Text = (Convert.ToString(RR.Tables[0].Rows[0]["NASS"]) + "").Trim().ToUpper();

				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["hp"]))
				{
					sql = "SELECT * FROM DCODCIAS WHERE gcodcias like '" + Convert.ToString(RR.Tables[0].Rows[0]["hp"]).Trim() + "%' AND fborrado is null ";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, MFiliacion.GConexion);
					RR = new DataSet();
					tempAdapter_2.Fill(RR);
					if (RR.Tables[0].Rows.Count != 0)
					{
						MEBCIAS.Text = Convert.ToString(RR.Tables[0].Rows[0]["gcodcias"]);
					}
				}

			}
			RR.Close();
		}

		public string fBuscaVia(string paraVIA)
		{
			string result = String.Empty;
			result = "";
			string sql = "SELECT dtipovia FROM DTIPOVIA WHERE cviaofic='" + paraVIA + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(RR.Tables[0].Rows[0]["dtipovia"]).Trim().ToUpper();
			}
			RR.Close();
			return result;
		}

		private string fBuscaPoblacion(string paraCodPos, string paraMUNICIPIO)
		{
			string result = String.Empty;
			string sql = String.Empty;
			DataSet RR = null;
			result = "";
			if (paraCodPos.Trim() != "")
			{
				sql = "SELECT dpoblaci FROM DPOBLACI WHERE " + 
				      " gpoblaci like '" + paraCodPos.Substring(0, Math.Min(2, paraCodPos.Length)) + paraMUNICIPIO + "%'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
				RR = new DataSet();
				tempAdapter.Fill(RR);
				if (RR.Tables[0].Rows.Count != 0)
				{
					RR.MoveFirst();
					result = Strings.StrConv(Convert.ToString(RR.Tables[0].Rows[0]["dpoblaci"]).Trim(), VbStrConv.ProperCase, 0);
				}
				RR.Close();
			}
			return result;
		}

		private string fBuscaInspeccionPrincipal()
		{
			string result = String.Empty;
			result = "";
			string sql = "SELECT dinspecc FROM DINSPECC WHERE ginspecc='" + Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "SEGURSOC", "VALFANU3") + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(RR.Tables[0].Rows[0]["dinspecc"]).Trim().ToUpper();
			}
			RR.Close();
			return result;
		}
    
        //----------------

        //*********************************************************************************************************
        //* O.Frias (14/06/2007) - Para comprobar si es de zona se opera con una nueva tabla que contendr� los
        //*                        registros desde & hasta
        //*********************************************************************************************************
        private void EsZona()
		{
			string strSql = String.Empty;
			DataSet rdoZona = null;
			chkZona.Checked = false;

			if (MEBCIAS.Text.Trim() != "")
			{
				strSql = "SELECT valfanu1, valfanu2, valfanu3 FROM SCONSGLO WHERE gconsglo = 'COMPZONA'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
				rdoZona = new DataSet();
				tempAdapter.Fill(rdoZona);
				if (rdoZona.Tables[0].Rows.Count != 0)
				{
					if (Convert.ToString(rdoZona.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
					{
						rdoZona.Close();
						strSql = "SELECT * FROM DRANCIAS WHERE ciadesde <= '" + MEBCIAS.Text.Trim() + "' AND ciahasta >= '" + MEBCIAS.Text.Trim() + "'";
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
						rdoZona = new DataSet();
						tempAdapter_2.Fill(rdoZona);
						if (rdoZona.Tables[0].Rows.Count != 0)
						{
							chkZona.Checked = true;
						}
						else
						{
							chkZona.Checked = false;
						}
					}
				}
				rdoZona.Close();
				rdoZona = null;
			}

		}

        private void Restringir_Inspeccion(bool bPonerInspeccion)
		{
			string strSql = String.Empty;
			DataSet rdorr = null;
			int lPosicion = 0;
			int iCodInspe = 0;

			string stInspe = "";
			bool btest = false;
			if (Convert.ToString(cbbREInspeccion.Text).Trim() != "")
			{
				stInspe = Convert.ToString(cbbREInspeccion.Text).Trim();
				if (Convert.ToDouble(cbbREInspeccion.get_ListIndex()) != -1)
				{
					int tempRefParam = cbbREInspeccion.get_ListIndex();
					int tempRefParam2 = 1;
					iCodInspe = Convert.ToInt32(cbbREInspeccion.get_List(tempRefParam, tempRefParam2));
				}
			}
			if (MEBCIAS.Text.Trim() != "")
			{
				strSql = "select DINSPECCVA.GINSPECC,DINSPECC,idefecto from dceninsp inner join DINSPECCVA on dceninsp.ginspecc = DINSPECCVA.ginspecc where exists ";
				strSql = strSql + "( select '' from dcodcias where gcodcias='" + MEBCIAS.Text.Trim() + "' and dcodcias.gcensalu = dceninsp.gcensalu) and ";
				strSql = strSql + "FDESACTI Is Null order by DINSPECCVA.dinspecc";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
				rdorr = new DataSet();
				tempAdapter.Fill(rdorr);
				if (rdorr.Tables[0].Rows.Count != 0)
				{
					cbbREInspeccion.Clear();
					lPosicion = -1;

                    int indice = 0; 
					foreach (DataRow iteration_row in rdorr.Tables[0].Rows)
					{
						object tempRefParam3 = Strings.StrConv(Convert.ToString(iteration_row["DINSPECC"]).Trim(), VbStrConv.ProperCase, 0);
						object tempRefParam4 = indice;
						cbbREInspeccion.AddItem(tempRefParam3, tempRefParam4);
						if (stInspe == Strings.StrConv(Convert.ToString(iteration_row["DINSPECC"]).Trim(), VbStrConv.ProperCase, 0))
						{
							btest = true;
						}
						cbbREInspeccion.set_List(indice, 1, iteration_row["GINSPECC"]);
						if (Convert.ToString(iteration_row["idefecto"]).Trim().ToUpper() == "S")
						{
							lPosicion = indice;
						}
                        indice++;
					}
					if (bPonerInspeccion)
					{
						cbbREInspeccion.set_ListIndex(lPosicion);
					}
					if (!btest)
					{
						stInspe = "";
					}
					//Si la inspecci�n que hab�a elegido, no est� asociada en la tabla DCENINSP, la incorporamos
					//            If Not bTest Then
					//                strSql = "SELECT GINSPECC FROM DENTIPAC WHERE GIDENPAC = '" & ModCodigo & "' AND GSOCIEDA = " & CodSS & " and ginspecc = " & iCodInspe
					//                Set rdorr = GConexion.OpenResultset(strSql, 1, 1)
					//                If Not rdorr.EOF Then
					//                    If Trim(rdorr("GINSPECC") & "") <> "" Then
					//                        cbbREInspeccion.AddItem StrConv(stInspe, vbProperCase), cbbREInspeccion.ListCount
					//                        cbbREInspeccion.List(cbbREInspeccion.ListCount - 1, 1) = Trim(rdorr("GINSPECC"))
					//                    End If
					//                End If
					//            End If
					object tempRefParam5 = "";
					object tempRefParam6 = Type.Missing;
					cbbREInspeccion.AddItem(tempRefParam5, tempRefParam6);
				}
				else
				{
					//cargar la tabla de Inspeccion
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter("select * from DINSPECCVA Where FDESACTI Is Null order by DINSPECCVA.dinspecc", MFiliacion.GConexion);
					rdorr = new DataSet();
					tempAdapter_2.Fill(rdorr);
					if (rdorr.Tables[0].Rows.Count != 0)
					{
						rdorr.MoveFirst();
						cbbREInspeccion.Clear();
						object tempRefParam7 = "";
						object tempRefParam8 = Type.Missing;
						cbbREInspeccion.AddItem(tempRefParam7, tempRefParam8);

                        int indice = 0;
						foreach (DataRow iteration_row_2 in rdorr.Tables[0].Rows)
						{
							object tempRefParam9 = Strings.StrConv(Convert.ToString(iteration_row_2["DINSPECC"]).Trim(), VbStrConv.ProperCase, 0);
							object tempRefParam10 = indice;
							cbbREInspeccion.AddItem(tempRefParam9, tempRefParam10);
							cbbREInspeccion.set_List(indice, 1, iteration_row_2["GINSPECC"]);
                            indice++;
						}
					}
				}
			}
			else
			{
				//cargar la tabla de Inspeccion
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter("select * from DINSPECCVA Where FDESACTI Is Null order by DINSPECCVA.dinspecc", MFiliacion.GConexion);
				rdorr = new DataSet();
				tempAdapter_3.Fill(rdorr);
				if (rdorr.Tables[0].Rows.Count != 0)
				{
					rdorr.MoveFirst();
					cbbREInspeccion.Clear();
					object tempRefParam11 = "";
					object tempRefParam12 = Type.Missing;
					cbbREInspeccion.AddItem(tempRefParam11, tempRefParam12);
                    int indice = 0;
					foreach (DataRow iteration_row_3 in rdorr.Tables[0].Rows)
					{
						object tempRefParam13 = Strings.StrConv(Convert.ToString(iteration_row_3["DINSPECC"]).Trim(), VbStrConv.ProperCase, 0);
						object tempRefParam14 = indice;
						cbbREInspeccion.AddItem(tempRefParam13, tempRefParam14);
						cbbREInspeccion.set_List(indice, 1, iteration_row_3["GINSPECC"]);
                        indice++;
					}
				}
			}
			rdorr.Close();
			rdorr = null;
			if (stInspe.Trim() != "")
			{
				cbbREInspeccion.Text = stInspe;
                
                for (int intContador = 0; intContador <= cbbREInspeccion.ListCount - 1; intContador++)
                {                    
                    if (cbbREInspeccion.Items[intContador].Text == stInspe)
                    {                        
                        cbbREInspeccion.set_ListIndex(intContador);
                        lbDPEFinanciadora.Text = cbbREInspeccion.Text;
                        break;
                    }                    
                }
            }
		}

        private void tbTarjeta_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			// Si se ha recibido un INTRO suponemos que se trata de una tarjeta
			// leida con el lector de tarjetas. Si la secuencia recibida contiene
			// un %B suponemos que se trata de la tarjeta sanitaria del INSALUD y
			// descomponemos la secuencia recibida
			//   'NumSS = ""
			//   'LIMPIAR_FORMULARIO_tarjeta

			if (KeyAscii == 13)
			{

				tbTarjeta.Text = tbTarjeta.Text.ToUpper();

				if (tbTarjeta.Text.IndexOf(stComienzoBanda) >= 0)
				{
					proFuncionTarjetas(1);

					valor_tarjeta = tbTarjeta.Text;
					if (tbTarjeta.Text.Trim() == "" && Valor_tarjeta_ant.Trim() != "")
					{
						tbTarjeta.Text = Valor_tarjeta_ant;
					}

				}
				else
				{
					// Como leer� la segunda banda y lo pondr� en el campo,
					// pasamos de ello y ponemos lo que hab�amos tomado antes.

					proFuncionTarjetas(2);

					tbTarjeta.Text = valor_tarjeta;
					if (tbTarjeta.Text.Trim() == "" && Valor_tarjeta_ant.Trim() != "")
					{
						tbTarjeta.Text = Valor_tarjeta_ant;
					}
				}
			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		//(maplaza)(06/08/2007)��ES NECESARIO PONER C�DIGO EN EL "KEYPRESS"?? ��SE HACE LECTURA DE TARJETAS EUROPEAS, AL
		//IGUAL QUE SE HACE CON LA LECTURA DE TARJETAS SANITARIAS ESPA�OLAS??
		private void tbTarjetaEuropea_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			// Si se ha recibido un INTRO suponemos que se trata de una tarjeta
			// leida con el lector de tarjetas. Si la secuencia recibida contiene
			// un %B suponemos que se trata de la tarjeta sanitaria del INSALUD y
			// descomponemos la secuencia recibida
			//   'NumSS = ""
			//   'LIMPIAR_FORMULARIO_tarjeta

			//''    If KeyAscii = 13 Then
			//''
			//''        tbTarjetaEuropea.Text = UCase(tbTarjetaEuropea.Text)
			//''
			//''        If InStr(1, tbTarjetaEuropea.Text, stComienzoBanda) > 0 Then
			//''            proFuncionTarjetas 1
			//''
			//''            valor_tarjetaEuropea = tbTarjetaEuropea.Text
			//''            If (Trim(tbTarjetaEuropea.Text) = "" And Trim(Valor_tarjeta_ant) <> "") Then
			//''                tbTarjetaEuropea.Text = Valor_tarjetaEuropea_ant
			//''            End If
			//''        Else
			//''            ' Como leer� la segunda banda y lo pondr� en el campo,
			//''            ' pasamos de ello y ponemos lo que hab�amos tomado antes.
			//''            proFuncionTarjetas 2
			//''
			//''            tbTarjetaEuropea.Text = valor_tarjeta
			//''            If (Trim(tbTarjetaEuropea.Text) = "" And Trim(Valor_tarjeta_ant) <> "") Then
			//''                tbTarjetaEuropea.Text = Valor_tarjeta_ant
			//''            End If
			//''        End If
			//''
			//''    End If

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void TarjetaInsalud()
		{
			int iComienzo1 = 0;
			int iBlanco2 = 0;

			// Se busca donde comienzan los datos de la primera pista
			int iComienzo = (tbTarjeta.Text.IndexOf(stComienzoBanda) + 1);

			if (iComienzo1 > iComienzo)
			{
				iComienzo = iComienzo1;
			}

			// Fecha de nacimiento y sexo
			int iA�oNac = Convert.ToInt32(1900 + Conversion.Val(tbTarjeta.Text.Substring(iComienzo + 12, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 12)))));
			int iMesNac = Convert.ToInt32(Conversion.Val(tbTarjeta.Text.Substring(iComienzo + 14, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 14)))));
			int iDiaNac = Convert.ToInt32(Conversion.Val(tbTarjeta.Text.Substring(iComienzo + 16, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 16)))));
			if (iDiaNac > 40)
			{
				iDiaNac -= 40;
				rbSexo[1].IsChecked = true;
			}
			else
			{
				rbSexo[0].IsChecked = true;
			}
			sdcFechaNac.Text = StringsHelper.Format(iDiaNac, "00") + "/" + StringsHelper.Format(iMesNac, "00") + "/" + 
			                   StringsHelper.Format(iA�oNac, "0000");

			// Apellidos y nombre
			string stNombre = tbTarjeta.Text.Substring(iComienzo + 25, Math.Min(30, tbTarjeta.Text.Length - (iComienzo + 25)));
			int iBlanco = (stNombre.IndexOf(' ') + 1);
			while (iBlanco > 0 && DosConsonantes(stNombre.Substring(iBlanco)) != tbTarjeta.Text.Substring(iComienzo + 10, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 10))))
			{
				iBlanco = Strings.InStr(iBlanco + 1, stNombre, " ", CompareMethod.Binary);
			}
			tbApellido1.Text = stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length));
			stNombre = stNombre.Substring(iBlanco, Math.Min(30, stNombre.Length - iBlanco));
			iBlanco = (stNombre.IndexOf(' ') + 1);
			while (iBlanco > 0 && DosConsonantes(stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length))) != tbTarjeta.Text.Substring(iComienzo + 10, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 10))))
			{
				iBlanco = Strings.InStr(iBlanco + 1, stNombre, " ", CompareMethod.Binary);
			}
			tbApellido2.Text = stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length));
			tbNombre.Text = stNombre.Substring(iBlanco, Math.Min(30, stNombre.Length - iBlanco));
			// Si hay dos o mas palabras en el nombre y el segundo apellido terminaba en particula
			// se pasa la primera palabra del nombre al segundo apellido
			iBlanco = (tbNombre.Text.Trim().IndexOf(' ') + 1);
			if (iBlanco > 0)
			{
				iBlanco2 = 0;
				for (int i = tbApellido2.Text.Trim().Length; i >= 1; i--)
				{
					if (tbApellido2.Text.Trim().Substring(i - 1, Math.Min(1, tbApellido2.Text.Trim().Length - (i - 1))) == " ")
					{
						iBlanco2 = i;
						break;
					}
				}
				if (iBlanco2 > 0)
				{
					stNombre = tbApellido2.Text.Trim().Substring(iBlanco2);
					if (stNombre == "DE" || stNombre == "DEL" || stNombre == "LA" || stNombre == "LAS" || stNombre == "LOS" || stNombre == "EL" || stNombre == "SAN" || stNombre == "Y")
					{
						tbApellido2.Text = tbApellido2.Text + " " + tbNombre.Text.Substring(0, Math.Min(iBlanco - 1, tbNombre.Text.Length));
						tbNombre.Text = tbNombre.Text.Substring(iBlanco);
					}
				}
			}
			tbTarjeta.Text = tbTarjeta.Text.Substring(iComienzo + 8, Math.Min(16, tbTarjeta.Text.Length - (iComienzo + 8)));
		}

        private string DosConsonantes(string Apellido)
		{
			StringBuilder result = new StringBuilder();
			string Letra = String.Empty;

			for (int i = 1; i <= Apellido.Length; i++)
			{

				Letra = Apellido.Substring(i - 1, Math.Min(1, Apellido.Length - (i - 1)));

				if (Letra == "�" || (Strings.Asc(Letra[0]) >= Strings.Asc('A') && Strings.Asc(Letra[0]) <= Strings.Asc('Z')))
				{

					if (Letra.Trim() != "")
					{

						if (Letra == "�")
						{
							result.Append("X");
							if (result.ToString().Length == 2)
							{
								break;
							}
						}
						else
						{
							if (Regex.IsMatch(Letra, "[A, E, I, O, U, �, �, �, �, �]"))
							{
							}
							else
							{
								result.Append(Letra);
								if (result.ToString().Length == 2)
								{
									break;
								}
							}
						}

					}

				}

			}

			// Si no se han encontrado dos consonantes se cogen las vocales
			if (result.ToString().Length < 2)
			{

				for (int i = 1; i <= Apellido.Length; i++)
				{

					Letra = Apellido.Substring(i - 1, Math.Min(1, Apellido.Length - (i - 1)));

					if (Strings.Asc(Letra[0]) >= Strings.Asc('A') && Strings.Asc(Letra[0]) <= Strings.Asc('Z'))
					{

						if (Letra.Trim() != "")
						{

							if (Regex.IsMatch(Letra, "[A, E, I, O, U, �, �, �, �, �]"))
							{
								result.Append(Letra);
								if (result.ToString().Length == 2)
								{
									break;
								}
							}

						}

					}

				}

			}

			// Si el apellido tiene menos de dos letras se completa con X
			if (result.ToString().Length < 1)
			{
				result = new StringBuilder("XX");
			}
			else
			{
				if (result.ToString().Length < 2)
				{
					result.Append("X");
				}
			}

			return result.ToString();
		}

		private void proFuncionTarjetas(int iBanda)
		{
			string imensaje = String.Empty;
			string stCodigo = String.Empty;

			// Seg�n la banda, el c�digo habr� que tomarlo en un punto u otro

			if (iBanda == 1)
			{
				stCodigo = tbTarjeta.Text.Substring(2, Math.Min(6, tbTarjeta.Text.Length - 2));
			}
			else if (iBanda == 2)
			{ 
				stCodigo = tbTarjeta.Text.Substring(1, Math.Min(6, tbTarjeta.Text.Length - 1));
			}
			BinPan = stCodigo;
			// Tomamos el c�digo de la tarjeta para ver si est� en la BD

			string StSql = "select * from DTIPTARJ where gtarjeta = '" + stCodigo + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
			DataSet RrTarjeta = new DataSet();
			tempAdapter.Fill(RrTarjeta);


			if (RrTarjeta.Tables[0].Rows.Count == 0)
			{
				string tempRefParam = "Filiaci�n";
				string tempRefParam2 = "";
				short tempRefParam3 = 3992;
				string[] tempRefParam4 = new string[] { stCodigo};
				imensaje = Convert.ToString(MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, MFiliacion.GConexion, tempRefParam4));
				return;
			}
			if (!Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["gsocieda"]))
			{
				for (i = 0; i <= cbbRESociedad.Items.Count - 1; i++)
				{
					if (Conversion.Str(cbbRESociedad.Items[i].Value) == Conversion.Str(Convert.ToString(RrTarjeta.Tables[0].Rows[0]["gsocieda"]) + ""))
					{
						cbbRESociedad.SelectedIndex = i;
						break;
					}
				}
			}
			else
			{
				cbbRESociedad.SelectedIndex = -1;
			}
			if (Convert.ToDouble(RrTarjeta.Tables[0].Rows[0]["gsocieda"]) == MFiliacion.CodSS)
			{
				rbRERegimen[0].IsChecked = true;
				this.tbRENSS.Text = tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longtarj"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1)));
				cbbREInspeccion.set_ListIndex(-1);
				if (!Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["ginspecc"]))
				{
					object tempRefParam5 = Type.Missing;
					object tempRefParam6 = Type.Missing;
					cbbREInspeccion.set_List(tempRefParam5, tempRefParam6, RrTarjeta.Tables[0].Rows[0]["ginspecc"]);
				}
			}
			else
			{
				rbRERegimen[1].IsChecked = true;
			}

			// Si la tarjeta es del insalud, la tratamos de forma distinta

			if (Convert.ToString(RrTarjeta.Tables[0].Rows[0]["iinsalud"]).Trim().ToUpper() == "S" && iBanda == 1)
			{
				for (i = 0; i <= cbbRESociedad.Items.Count - 1; i++)
				{
					if (StringsHelper.ToDoubleSafe(Conversion.Str(cbbRESociedad.Items[i].Value.ToString())) == MFiliacion.CodSS)
					{
						cbbRESociedad.SelectedIndex = i;
						break;
					}
				}
				TarjetaInsalud();
				return;
			}

			// En funci�n de los datos, atacaremos la sentencia de una u otra forma

			proDatosTarjeta(RrTarjeta, iBanda);

			// Cerramos el identificador

			RrTarjeta.Close();

		}

		private void proDatosTarjeta(DataSet RrTarjeta, int iBanda)
		{
			// Comprobamos si en esta pasada tenemos que buscar la tarjeta
			if (!Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["gsocieda"]))
			{
				for (i = 0; i <= cbbRESociedad.Items.Count - 1; i++)
				{
					if (Conversion.Str(cbbRESociedad.Items[i].Value) == Conversion.Str(Convert.ToString(RrTarjeta.Tables[0].Rows[0]["gsocieda"]) + ""))
					{
						cbbRESociedad.SelectedIndex = i;
						break;
					}
				}
			}
			else
			{
				cbbRESociedad.SelectedIndex = -1;
			}
			if (Convert.ToDouble(RrTarjeta.Tables[0].Rows[0]["itarjeta"]) == iBanda)
			{

				// Tomamos el n�mero de tarjeta del asegurado, aunque lo que haremos ser� dejarlo al
				// final en el campo de texto de la tarjeta.
				if (Convert.ToDouble(RrTarjeta.Tables[0].Rows[0]["gsocieda"]) == MFiliacion.CodSS)
				{
					this.tbRENSS.Text = tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longtarj"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1)));
					cbbREInspeccion.set_ListIndex(-1);
					if (!Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["ginspecc"]))
					{
						object tempRefParam = Type.Missing;
						object tempRefParam2 = Type.Missing;
						cbbREInspeccion.set_List(tempRefParam, tempRefParam2, RrTarjeta.Tables[0].Rows[0]["ginspecc"]);
					}
				}
				else
				{
					if (proCoincideMascara(tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longtarj"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1)))))
					{
						tbRENPoliza.Text = tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longtarj"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1)));
					}
					else
					{
						if (MFiliacion.stFiliacion == "ALTA")
						{
							//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
							tbRENPoliza.Text = MascaraVacia();
						}
						else
						{
							//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
							bCambiandoMascara = true;
							tbRENPoliza.Mask = "";
							bCambiandoMascara = false;
							tbRENPoliza.BackColor = Color.FromArgb(255, 128, 128);
							tbRENPoliza.Text = tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longtarj"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1)));
						}
					}

					//                Me.tbRENPoliza.Text = Mid(tbTarjeta.Text, RrTarjeta("iinitarj"), RrTarjeta("longtarj"))
				}
				// Si la tarjeta es de Adeslas, la tratamos de otra forma

				if (Convert.ToString(RrTarjeta.Tables[0].Rows[0]["iadeslas"]).Trim().ToUpper() == "S")
				{

					if (proCoincideMascara(this.tbRENSS.Text.Substring(0, Math.Min(8, this.tbRENSS.Text.Length)) + this.tbRENSS.Text.Substring(10)))
					{
						tbRENPoliza.Text = this.tbRENSS.Text.Substring(0, Math.Min(8, this.tbRENSS.Text.Length)) + this.tbRENSS.Text.Substring(10);
					}
					else
					{
						if (MFiliacion.stFiliacion == "ALTA")
						{
							//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
							tbRENPoliza.Text = MascaraVacia();
						}
						else
						{
							//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
							bCambiandoMascara = true;
							tbRENPoliza.Mask = "";
							bCambiandoMascara = false;
							tbRENPoliza.BackColor = Color.FromArgb(255, 128, 128);
							tbRENPoliza.Text = this.tbRENSS.Text.Substring(0, Math.Min(8, this.tbRENSS.Text.Length)) + this.tbRENSS.Text.Substring(10);
						}
					}

					//            Me.tbRENPoliza.Text = Mid(Me.tbRENSS.Text, 1, 8) & Mid(Me.tbRENSS.Text, 11)
				}
			}
			//Cogemos el numero de version

			if (Convert.ToDouble(RrTarjeta.Tables[0].Rows[0]["iversio"]) == iBanda && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["initver"]) && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["longver"]))
			{
				tbRENVersion.Text = tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["initver"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longver"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["initver"]) - 1)));
			}

			if (Convert.ToDouble(RrTarjeta.Tables[0].Rows[0]["icontro"]) == iBanda && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["initcon"]) && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["longcon"]) && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["idcontrol"]))
			{
				CodigoDeControl = Convert.ToInt32(Double.Parse(tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["initcon"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longcon"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["initcon"]) - 1)))));
				Algoritmo = Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["idcontrol"]);
			}

			// Comprobamos si en esta pasada tenemos que buscar el nombre

			tbTarjeta.Text = "";
		}

		//OSCAR C FEB 2006
		private bool ComprobarControl(string b, ref int Control, int Algoritmo, string ParamVersion, string Sociedad)
		{
			bool result = false;
			string poliza = String.Empty;
			string a = b;

			int Suma = 0, LHUN_Sanitario = 0;
			if (Algoritmo == 1)
			{
				double dbNumericTemp = 0;
				if (Double.TryParse(a, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp) && 0 == (a.IndexOf('.') + 1))
				{
					result = true;
					poliza = a.Substring(Math.Max(6, 0));
					Control = Convert.ToInt32(Double.Parse(a.Substring(Math.Max(a.Length - 1, 0))));
					a = a.Substring(0, Math.Min(a.Length - 1, a.Length));

					if (a != "" && !Convert.IsDBNull(a))
					{
						for (int Conta = a.Length; Conta >= 1; Conta--)
						{
							if ((Conta - a.Length + 1) % 2 != 0)
							{
								Suma = Convert.ToInt32(Suma + (2 * Double.Parse(a.Substring(Conta - 1, Math.Min(1, a.Length - (Conta - 1))))));
							}
							else
							{
								Suma = Convert.ToInt32(Suma + Double.Parse(a.Substring(Conta - 1, Math.Min(1, a.Length - (Conta - 1)))));
							}
						}
						LHUN_Sanitario = ((((int) ((((double) (Suma / ((int) 10))) > 0) ? Math.Floor((double) (Suma / ((int) 10))) : Math.Ceiling((double) (Suma / ((int) 10))))) + 1) * 10) - Suma;
						if (LHUN_Sanitario == 10)
						{
							LHUN_Sanitario = 0;
						}

						if (LHUN_Sanitario != Control)
						{
							result = false;
						}

					}
				}
				else
				{
					result = false;
				}
			}

			return result;
		}

		//OSCAR C FEB 2006
		private void proRellenaDatosUsuWeb()
		{
			bool blnContactos = false;
			string stInspeccion = String.Empty;

			MEBCIAS.Text = "           ";

			int intContador = (MFiliacion.stModulo.IndexOf('#') + 1);

			string stFecSolic = MFiliacion.stModulo.Substring(intContador, Math.Min(19, MFiliacion.stModulo.Length - intContador));
			string stCodUsuar = MFiliacion.stModulo.Substring(intContador + 19);

			object tempRefParam = stFecSolic;
			string strSql = "SELECT CSUSCWEB.codusuar, CSUSCWEB.fsolicit, CSUSCWEB.contrase, CSUSCWEB.dnombpac, CSUSCWEB.dape1pac, " + 
			                "CSUSCWEB.dape2pac, CSUSCWEB.fnacipac, CSUSCWEB.itipsexo, CSUSCWEB.ndninifp, CSUSCWEB.iregieco, " + 
			                "CSUSCWEB.ntarjsan, CSUSCWEB.gsocieda, DSOCIEDAVA.dsocieda, CSUSCWEB.nafiliac, CSUSCWEB.dnombpri, " + 
			                "CSUSCWEB.dape1pri, CSUSCWEB.dape2pri, CSUSCWEB.nnifpriv, CSUSCWEB.gtipovia, DTIPOVIA.dtipovia, " + 
			                "CSUSCWEB.dnombvia, CSUSCWEB.nnumevia, CSUSCWEB.dotravia, CSUSCWEB.gcodipos, CSUSCWEB.gpoblaci, " + 
			                "DPOBLACI.dpoblaci, CSUSCWEB.ntelefo1, CSUSCWEB.nnumovil, CSUSCWEB.ddiemail, CSUSCWEB.iestsoli, " + 
			                "CSUSCWEB.festsoli, SUSUARIO.dusuario, CSUSCWEB.npasapor, CSUSCWEB.npasagar, CSUSCWEB.ienvisms, " + 
			                "CSUSCWEB.nversion, CSUSCWEB.gcodcias, CSUSCWEB.gpaisres " + 
			                "From CSUSCWEB " + 
			                "Left JOIN DSOCIEDAVA ON CSUSCWEB.gsocieda = DSOCIEDAVA.gsocieda AND " + 
			                "DSOCIEDAVA.fborrado Is Null and DSOCIEDAVa.FDESACTI Is Null " + 
			                "left JOIN DPOBLACI ON CSUSCWEB.gpoblaci = DPOBLACI.gpoblaci " + 
			                "LEFT JOIN SUSUARIO ON CSUSCWEB.gusuario = SUSUARIO.gusuario " + 
			                "LEFT JOIN DTIPOVIA ON CSUSCWEB.gtipovia = DTIPOVIA.gtipovia " + 
			                "WHERE fsolicit = " + Serrores.FormatFechaHMS(tempRefParam) + " AND " + 
			                "codusuar = '" + stCodUsuar + "' AND " + 
			                "festsoli IS NULL AND " + 
			                "iestsoli = 'P' ";
			stFecSolic = Convert.ToString(tempRefParam);

			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);

			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				// Primera Pesta�a
				if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["ndninifp"]) + "").Trim() == "")
				{
					rbDNIOtroDocDP[0].IsChecked = false;
					rbDNIOtroDocDP[1].IsChecked = false;
					tbNIFOtrosDocDP.Enabled = false;
					if (tbNIFOtrosDocDP.Mask == MFiliacion.stFORMATO_NIF)
					{
						tbNIFOtrosDocDP.Text = "         ";
					}
					else
					{
						tbNIFOtrosDocDP.Text = "";
					}
				}
				else
				{
					if (MFiliacion.EsDNI_NIF(Convert.ToString(rdoTemp.Tables[0].Rows[0]["ndninifp"]), 9))
					{
						rbDNIOtroDocDP[0].IsChecked = true;
						rbDNIOtroDocDP[1].IsChecked = false;
						tbNIFOtrosDocDP.Enabled = true;
						if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["ndninifp"]).Trim().Length == 9)
						{
							tbNIFOtrosDocDP.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["ndninifp"]).Trim();
						}
						else
						{
							tbNIFOtrosDocDP.Text = StringsHelper.Format(rdoTemp.Tables[0].Rows[0]["ndninifp"], "00000000") + " ";
						}
					}
					else
					{
						rbDNIOtroDocDP[0].IsChecked = false;
						rbDNIOtroDocDP[1].IsChecked = true;
						tbNIFOtrosDocDP.Enabled = true;
						tbNIFOtrosDocDP.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["ndninifp"]).Trim();
					}                
				}

				tbDPPasaporte.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["npasapor"]) + "").Trim().ToUpper();
				tbTarjeta.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["ntarjsan"]) + "").Trim().ToUpper();

				tbDPOtrosDomic.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dotravia"]) + "").Trim().ToUpper();

				if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodipos"]) + "").Trim() != "")
				{
					if (bFormatoCodigoPos)
					{
						mebDPCodPostal.Text = StringsHelper.Format((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodipos"]) + "").Trim().ToUpper(), "00000");
					}
					else
					{
						mebDPCodPostal.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodipos"]) + "").Trim().ToUpper();
					}
				}
				mebDPCodPostal.Tag = mebDPCodPostal.Text;

				tbDPTelf1.Text = Serrores.fQuitaCaracteresNoNumericos((Convert.ToString(rdoTemp.Tables[0].Rows[0]["ntelefo1"]) + "").Trim().ToUpper());
				tbDPTelf2.Text = Serrores.fQuitaCaracteresNoNumericos((Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnumovil"]) + "").Trim().ToUpper());
				tbEmail.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["ddiemail"]) + "").Trim();
				chbSMS.CheckState = (CheckState) (((Convert.ToString(rdoTemp.Tables[0].Rows[0]["ienvisms"]) + "").Trim() == "S") ? 1 : 0);


				if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gpaisres"]) + "").Trim() != "34" && (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gpaisres"]) + "").Trim() != "")
				{
					chbDPExtrangero.CheckState = CheckState.Checked;
					for (intContador = 0; intContador <= cbbDPPais.ListCount - 1; intContador++)
					{
						object tempRefParam2 = 1;
						object tempRefParam3 = intContador;
						if (Convert.ToString(cbbDPPais.get_Column(tempRefParam2, tempRefParam3)) == (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gpaisres"]) + "").Trim())
						{
							intContador = Convert.ToInt32(tempRefParam3);
							cbbDPPais.set_ListIndex(intContador);
							break;
						}
						else
						{
							intContador = Convert.ToInt32(tempRefParam3);
						}
					}
					tbDPExPob.Text = fpoblaci((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gpoblaci"]) + "").Trim());
					tbDPDireccionEx.Text = fBuscaVia((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gtipovia"]) + "").Trim().ToUpper()) + " " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombvia"]) + "").Trim().ToUpper() + ", " + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnumevia"]) + "").Trim().ToUpper();
				}
				else
				{
					chbDPExtrangero.CheckState = CheckState.Unchecked;
					cbbDPVia.Text = fBuscaVia((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gtipovia"]) + "").Trim().ToUpper());
					tbDPDomicilioP.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombvia"]) + "").Trim().ToUpper();
					tbDPNDomic.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnumevia"]) + "").Trim().ToUpper();
					for (intContador = 0; intContador <= cbbDPPoblacion.ListCount - 1; intContador++)
					{
						object tempRefParam4 = 1;
						object tempRefParam5 = intContador;
						if (Convert.ToString(cbbDPPoblacion.get_Column(tempRefParam4, tempRefParam5)) == (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gpoblaci"]) + "").Trim())
						{
							intContador = Convert.ToInt32(tempRefParam5);
							cbbDPPoblacion.set_ListIndex(intContador);
							break;
						}
						else
						{
							intContador = Convert.ToInt32(tempRefParam5);
						}
					}
				}

				// Segunda Pesta�a
				switch(Convert.ToString(rdoTemp.Tables[0].Rows[0]["iregieco"]))
				{
					case "S" : 
						rbRERegimen[0].IsChecked = true; 
						stInspeccion = fBuscaInspeccionPrincipal().Trim().ToUpper(); 
						lbDPNAsegurado.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nafiliac"]) + "").Trim(); 
						tbRENSS.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nafiliac"]) + "").Trim(); 
						for (intContador = 0; intContador <= cbbREInspeccion.ListCount - 1; intContador++)
						{
							object tempRefParam6 = 0;
							object tempRefParam7 = intContador;
							if (Convert.ToString(cbbREInspeccion.get_Column(tempRefParam6, tempRefParam7)).Trim().ToUpper() == stInspeccion)
							{
								intContador = Convert.ToInt32(tempRefParam7);
								cbbREInspeccion.set_ListIndex(intContador);
								lbDPEFinanciadora.Text = cbbREInspeccion.Text;
								break;
							}
							else
							{
								intContador = Convert.ToInt32(tempRefParam7);
							}
						} 
						 
						break;
					case "A" : 
						rbRERegimen[1].IsChecked = true; 
						lbDPNAsegurado.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nafiliac"]) + "").Trim(); 
						 
						if (proCoincideMascara((Convert.ToString(rdoTemp.Tables[0].Rows[0]["nafiliac"]) + "").Trim()))
						{
							tbRENPoliza.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nafiliac"]) + "").Trim();
						}
						else
						{
							if (MFiliacion.stFiliacion == "ALTA")
							{
								//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
								tbRENPoliza.Text = MascaraVacia();
							}
							else
							{
								//MsgBox "La p�liza introducida no coincide con la m�scara establecida (" & tbRENPoliza.ToolTipText & "). Por favor corrija ese valor.", vbCritical
								bCambiandoMascara = true;
								tbRENPoliza.Mask = "";
								bCambiandoMascara = false;
								tbRENPoliza.BackColor = Color.FromArgb(255, 128, 128);
								tbRENPoliza.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nafiliac"]) + "").Trim();
							}
						} 
						 
						//            tbRENPoliza = Trim(rdoTemp("nafiliac") & "") 
						chkExentoIVA.CheckState = CheckState.Unchecked; 
						tbRENVersion.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nversion"]) + "").Trim(); 
						for (intContador = 0; intContador <= cbbRESociedad.Items.Count - 1; intContador++)
						{
							if (int.Parse(cbbRESociedad.Items[intContador].Value.ToString()) == Convert.ToDouble(rdoTemp.Tables[0].Rows[0]["gsocieda"]))
							{
								cbbRESociedad.SelectedIndex = intContador;
								if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["iregieco"]) == "A")
								{
									lbDPEFinanciadora.Text = cbbRESociedad.Text;
								}
								break;
							}
						} 
						break;
					case "P" : 
						rbRERegimen[2].IsChecked = true; 
						break;
				}

				if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["gcodcias"]))
				{
					MEBCIAS.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodcias"]);
				}


				if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pri"]) + "").Trim() != "")
				{
					if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]) + "").Trim() == "")
					{
						rbDNIOtroDocRE[0].IsChecked = false;
						rbDNIOtroDocRE[1].IsChecked = false;
						tbNIFOtrosDocRE.Enabled = false;
						if (tbNIFOtrosDocRE.Mask == MFiliacion.stFORMATO_NIF)
						{
							tbNIFOtrosDocRE.Text = "         ";
						}
						else
						{
							tbNIFOtrosDocRE.Text = "";
						}
					}
					else
					{
						if (MFiliacion.EsDNI_NIF(Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]), 9))
						{
							rbDNIOtroDocRE[0].IsChecked = true;
							rbDNIOtroDocRE[1].IsChecked = false;
							tbNIFOtrosDocRE.Enabled = true;
							if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]).Trim().Length == 9)
							{
								tbNIFOtrosDocRE.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]).Trim();
							}
							else
							{
								tbNIFOtrosDocRE.Text = StringsHelper.Format(rdoTemp.Tables[0].Rows[0]["nnifpriv"], "00000000") + " ";
							}
						}
						else
						{
							rbDNIOtroDocRE[0].IsChecked = false;
							rbDNIOtroDocRE[1].IsChecked = true;
							tbNIFOtrosDocRE.Enabled = true;
							tbNIFOtrosDocRE.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]).Trim();
						}
					}

					tbREPasaporte.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["npasagar"]) + "").Trim();
					tbREApe1.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pri"]) + "").Trim();
					tbREApe2.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape2pri"]) + "").Trim();
					tbRENombre.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpri"]) + "").Trim();
					tbREDomicilio.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombvia"]) + "").Trim();

					if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodipos"]) + "").Trim() != "")
					{
						if (bFormatoCodigoPos)
						{
							mebRECodPostal.Text = StringsHelper.Format((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodipos"]) + "").Trim().ToUpper(), "00000");
						}
						else
						{
							mebRECodPostal.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodipos"]) + "").Trim().ToUpper();
						}
					}

					for (intContador = 0; intContador <= cbbREPoblacion.ListCount - 1; intContador++)
					{
						object tempRefParam8 = 1;
						object tempRefParam9 = intContador;
						if (Convert.ToString(cbbREPoblacion.get_Column(tempRefParam8, tempRefParam9)) == (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gpoblaci"]) + "").Trim())
						{
							intContador = Convert.ToInt32(tempRefParam9);
							cbbREPoblacion.set_ListIndex(intContador);
							break;
						}
						else
						{
							intContador = Convert.ToInt32(tempRefParam9);
						}
					}

				}

				// Tercera Pesta�a
				blnContactos = false;
				if (((int) DateAndTime.DateDiff("YYYY", Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fnacipac"]), DateTime.Now,  Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) == 18)
				{					
					if (Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fnacipac"].ToString()).Month < DateTime.Now.Month)
					{
						blnContactos = true;
					}
					else if (Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fnacipac"].ToString()).Month == DateTime.Now.Month)
					{ 
						if (Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fnacipac"].ToString()).Day < DateTime.Now.Day)
						{
							blnContactos = true;
						}
					}
				}
				else if (((int) DateAndTime.DateDiff("YYYY", Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fnacipac"]), DateTime.Now, Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)) < 18)
				{ 
					blnContactos = true;
				}
				if (blnContactos)
				{
					if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]) + "").Trim() == "")
					{
						rbDNIOtroDocPC[0].IsChecked = false;
						rbDNIOtroDocPC[1].IsChecked = false;
						tbNIFOtrosDocPC.Enabled = false;
						if (tbNIFOtrosDocPC.Mask == MFiliacion.stFORMATO_NIF)
						{
							tbNIFOtrosDocPC.Text = "         ";
						}
						else
						{
							tbNIFOtrosDocPC.Text = "";
						}
					}
					else
					{
						if (MFiliacion.EsDNI_NIF(Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]), 9))
						{
							rbDNIOtroDocPC[0].IsChecked = true;
							rbDNIOtroDocPC[1].IsChecked = false;
							tbNIFOtrosDocPC.Enabled = true;
							if (Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]).Trim().Length == 9)
							{
								tbNIFOtrosDocPC.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]).Trim();
							}
							else
							{
								tbNIFOtrosDocPC.Text = StringsHelper.Format(rdoTemp.Tables[0].Rows[0]["nnifpriv"], "00000000") + " ";
							}
						}
						else
						{
							rbDNIOtroDocPC[0].IsChecked = false;
							rbDNIOtroDocPC[1].IsChecked = true;
							tbNIFOtrosDocPC.Enabled = true;
							tbNIFOtrosDocPC.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["nnifpriv"]).Trim();
						}
					}

					tbPCApe1.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pri"]) + "").Trim();
					tbPCApe2.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape2pri"]) + "").Trim();
					tbPCNombre.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpri"]) + "").Trim();
					tbPCDomicilio.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombvia"]) + "").Trim();

					if ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodipos"]) + "").Trim() != "")
					{
						if (bFormatoCodigoPos)
						{
							mebPCCodPostal.Text = StringsHelper.Format((Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodipos"]) + "").Trim().ToUpper(), "00000");
						}
						else
						{
							mebPCCodPostal.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gcodipos"]) + "").Trim().ToUpper();
						}
					}

					for (intContador = 0; intContador <= cbbPCPoblacion.ListCount - 1; intContador++)
					{
						object tempRefParam10 = 1;
						object tempRefParam11 = intContador;
						if (Convert.ToString(cbbPCPoblacion.get_Column(tempRefParam10, tempRefParam11)) == (Convert.ToString(rdoTemp.Tables[0].Rows[0]["gpoblaci"]) + "").Trim())
						{
							intContador = Convert.ToInt32(tempRefParam11);
							cbbPCPoblacion.set_ListIndex(intContador);
							break;
						}
						else
						{
							intContador = Convert.ToInt32(tempRefParam11);
						}
					}
					rbTutor[0].IsChecked = true;
				}

			}
			rdoTemp.Close();
		}

		private string fpoblaci(string stCodPobla)
		{
			string result = String.Empty;
			string StSql = "SELECT dpoblaci FROM DPOBLACI WHERE gpoblaci = '" + stCodPobla + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);

			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dpoblaci"]);
			}
			else
			{
				result = "";
			}

			rdoTemp.Close();

			return result;
		}

		//Oscar C Mayo 2010
		public void RecuperarInspeccion(string stIdpaciente, int lngCodSociedad, string stInspeccion)
		{
			SQLSOC = "SELECT * FROM DINSHPAC WHERE GIDENPAC = '" + stIdpaciente + "' AND GSOCIEDA = " + lngCodSociedad.ToString() + "" + " AND GINSPECC='" + stInspeccion + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLSOC, MFiliacion.GConexion);
			RrSQLSOC = new DataSet();
			tempAdapter.Fill(RrSQLSOC);

			if (RrSQLSOC.Tables[0].Rows.Count != 0)
			{
				CARGAR_SEGURIDADSOCIAL();
			}
			else
			{
				cbbREInspeccion.set_ListIndex(-1);
				tbRENSS.Text = "";
				MEBCIAS.Text = "";
				cbbEmplazamiento.SelectedIndex = -1;
				chkZona.Checked = false;
				cbbHospitalReferencia.SelectedIndex = -1;
				rbIndicadorSS[0].IsChecked = true;
				rbActivo.IsChecked = false;
				rbPensionista.IsChecked = false;
				tbRENomTitularSS.Text = "";
			}

		}

		//---------------

		//(maplaza)(28/08/2007)Este m�todo sirve para cuando se interact�a desde la pantalla de "Consulta/Actualizaci�n de Entidades".
		//Este m�todo se ejecuta cuando se selecciona una sociedad activa desde la pantalla de "Consulta/Actualizaci�n de Entidades",
		//en ese caso pasa a visualizarse en la pesta�a de R�gimen Econ�mico.
		public void RecuperarSociedad(string stIdpaciente, int lngCodSociedad)
		{
			SQLSOC = "SELECT * FROM DENTIPAC WHERE GIDENPAC = '" + stIdpaciente + "' AND GSOCIEDA = " + lngCodSociedad.ToString() + "";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLSOC, MFiliacion.GConexion);
			RrSQLSOC = new DataSet();
			tempAdapter.Fill(RrSQLSOC);

			if (RrSQLSOC.Tables[0].Rows.Count != 0)
			{
				CARGAR_SOCIEDAD();
			}
			else
			{
				cbbRESociedad.SelectedIndex = -1;
				bCambiandoMascara = true;
				tbRENPoliza.Mask = "";
				bCambiandoMascara = false;
				ToolTipMain.SetToolTip(tbRENPoliza, "");
				tbRENPoliza.Text = "";
				chkExentoIVA.CheckState = CheckState.Unchecked;
				tbRENVersion.Text = "";
				tbRECobertura.Text = "";
				mebREFechaAlta.Text = "  /    ";
				mebREFechaCaducidad.Text = "  /    ";
				this.tbREEmpresa.Text = "";
				rbIndicador[0].IsChecked = true;
			}

		}
		//--------------
		public void RecuperarPrivado(string stIdpaciente)
		{
			SQLSOC = "SELECT * FROM DPRIVADO WHERE gidenpac = '" + stIdpaciente + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLSOC, MFiliacion.GConexion);
			RrSQLSOC = new DataSet();
			tempAdapter.Fill(RrSQLSOC);

			if (RrSQLSOC.Tables[0].Rows.Count != 0)
			{
				CARGAR_PRIVADO();
			}
			else
			{
				tbNIFOtrosDocDP.Text = "";
				rbDNIOtroDocRE[0].IsChecked = false;
				rbDNIOtroDocRE[1].IsChecked = false;
				tbNIFOtrosDocRE.Text = "";
				tbRENombre.Text = "";
				tbREApe1.Text = "";
				tbREApe2.Text = "";
				tbREDomicilio.Text = "";
				tbREPasaporte.Text = "";
				mebRECodPostal.Text = "";
				mebRECodPostal.Text = "";
				cbbDPVia.set_ListIndex(-1);
				tbDPDomicilioP.Text = "";
				tbDPDomicilio.Text = "";
				cbbDPPoblacion.set_ListIndex(-1);
				cbbREPoblacion.set_ListIndex(-1);
				cbbREPoblacion.set_ListIndex(-1);
				chbREExtrangero.CheckState = CheckState.Unchecked;
				cbbREPais.set_ListIndex(-1);
				tbREExPob.Text = "";
				tbNIFOtrosDocDP.Text = "";
				rbDNIOtroDocRE[0].IsChecked = false;
				rbDNIOtroDocRE[1].IsChecked = false;

			}

		}
		//--------------

		private void CambioUsuarWeb(string stIdAntiguo, string stIDNew)
		{
			bool ErrActuali = false;
			string StSql = String.Empty;
			string stErrores = String.Empty;
			DataSet rdoTemp = null;
			string stServidor = String.Empty;
			string stBaseDatos = String.Empty;
			string stUsuarioBD = String.Empty;
			string stPassBD = String.Empty;
			bool blnConDos = false;
			bool blnDelete = false;
			int intCodCentro = 0;
			string stActuCentro = String.Empty;
			string stCodUsuar = String.Empty;

            using (TransactionScope scope = TransScopeBuilder.CreateReadCommited())
            {
                try
                {
                    ErrActuali = true;

                    blnConDos = false;
                    blnDelete = false;
                    StSql = "SELECT nnumeri1 " +
                            "From SCONSGLO " +
                            "WHERE GCONSGLO = 'CODHOSPI' ";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
                    rdoTemp = new DataSet();
                    tempAdapter.Fill(rdoTemp);

                    if (rdoTemp.Tables[0].Rows.Count != 0)
                    {
                        intCodCentro = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["nnumeri1"])) ? 0 : Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["nnumeri1"]);
                    }
                    rdoTemp.Close();
                    rdoTemp = null;

                    //O.Frias - 15/06/2009
                    //Para ver si actualizo los datos del paciente en Web
                    stActuCentro = "N";
                    StSql = "SELECT valfanu1 " +
                            "From SCONSGLO " +
                            "WHERE GCONSGLO = 'IMDHWEB' ";
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, MFiliacion.GConexion);
                    rdoTemp = new DataSet();
                    tempAdapter_2.Fill(rdoTemp);

                    if (rdoTemp.Tables[0].Rows.Count != 0)
                    {
                        stActuCentro = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["valfanu1"])) ? "N" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]);
                    }
                    rdoTemp.Close();
                    rdoTemp = null;

                    if (stActuCentro.Trim().ToUpper() == "S")
                    {

                        StSql = "SELECT valfanu1, valfanu2, valfanu3, valfanu1i1 " +
                                "From SCONSGLO " +
                                "WHERE GCONSGLO = 'BDCITWEB' ";

                        SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, MFiliacion.GConexion);
                        rdoTemp = new DataSet();
                        tempAdapter_3.Fill(rdoTemp);
                        if (rdoTemp.Tables[0].Rows.Count != 0)
                        {
                            stServidor = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]) + "").Trim();
                            stBaseDatos = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu2"]) + "").Trim();
                            stUsuarioBD = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu3"]) + "").Trim();
                            //stPassBD = Trim(General_DesencriptarPassword(rdoTemp("valfanu1i1") & ""))

                            // Si no est� vac�a, la desencriptamos

                            stPassBD = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1i1"]) + "").Trim();
                            rdoTemp.Close();
                            rdoTemp = null;

                            if (stServidor != "" && stBaseDatos != "" && stUsuarioBD != "" && stPassBD != "")
                            {
                                stPassBD = Serrores.General_DesencriptarPassword(ref stPassBD);
                                if (fConex(ref stServidor, ref stBaseDatos, ref stUsuarioBD, ref stPassBD))
                                {
                                    if (intCodCentro == 0)
                                    {
                                        RadMessageBox.Show("El c�digo del centro no esta establecido.", "Error - CambioUsuarWeb", MessageBoxButtons.OK, RadMessageIcon.Error);
                                        ErrActuali = false;
                                        return;
                                    }

                                    StSql = "SELECT * FROM USWEBCEN WHERE gidenpac = '" + stIdAntiguo + "' AND gcodcent = " + Conversion.Str(intCodCentro).Trim();
                                    SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSql, cnConexDos);
                                    rdoTemp = new DataSet();
                                    tempAdapter_4.Fill(rdoTemp);
                                    if (rdoTemp.Tables[0].Rows.Count != 0)
                                    {
                                        blnConDos = true;
                                        stCodUsuar = Convert.ToString(rdoTemp.Tables[0].Rows[0]["codusuar"]);
                                        
                                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.BeginTrans();
                                        
                                        rdoTemp.Edit();
                                        rdoTemp.Tables[0].Rows[0]["gidenpac"] = stIDNew;
                                        string tempQuery = rdoTemp.Tables[0].TableName;
                                        //SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(tempQuery, "");
                                        SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_4);
                                        tempAdapter_4.Update(rdoTemp, rdoTemp.Tables[0].TableName);

                                        //UpgradeStubs.System_Data_SqlClient_SqlConnection.CommitTrans();
                                        scope.Complete();

                                    }
                                    rdoTemp.Close();
                                    rdoTemp = null;


                                    //O.Frias - 15/06/2009
                                    //Para ver si actualizo los datos del paciente en Web
                                    //If UCase(Trim(stActuCentro)) = "S" Then
                                    mbAcceso.ActulizaDatosWeb(MFiliacion.GConexion, cnConexDos, stIDNew, stCodUsuar);
                                    //End If

                                }
                            }
                        }

                    }
                }
                catch (Exception excep)
                {
                    if (!ErrActuali)
                    {
                        throw excep;
                    }

                    if (ErrActuali)
                    {
                        stErrores = excep.Message;

                        /*for (int intContador = 0; intContador <= UpgradeHelpers.UpgradeSupport.RDO_rdoEngine_definst.rdoErrors.getCount() - 1; intContador++)
                        {
                            stErrores = stErrores + "- " + UpgradeSupport.RDO_rdoEngine_definst.rdoErrors.Item(intContador).Message + "\r";
                        }*/

                        RadMessageBox.Show(stErrores, Application.ProductName);

                        StSql = "SELECT * FROM USUWEBER WHERE 1 = 2";
                        SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(StSql, MFiliacion.GConexion);
                        rdoTemp = new DataSet();
                        tempAdapter_6.Fill(rdoTemp);

                        rdoTemp.AddNew();
                        rdoTemp.Tables[0].Rows[0]["idpacold"] = stIdAntiguo;
                        rdoTemp.Tables[0].Rows[0]["idpacnew"] = stIDNew;
                        rdoTemp.Tables[0].Rows[0]["fsistema"] = DateTime.Now;
                        rdoTemp.Tables[0].Rows[0]["deserror"] = stErrores;
                        rdoTemp.Tables[0].Rows[0]["gusuario"] = Serrores.VVstUsuarioApli;
                        string tempQuery_2 = rdoTemp.Tables[0].TableName;
                        //SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(tempQuery_2, "");
                        SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_6);
                        tempAdapter_6.Update(rdoTemp, rdoTemp.Tables[0].TableName);

                        rdoTemp.Close();


                        if (blnConDos)
                        {
                            //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
                        }

                    }
                }
            }
		}


		private bool fConex(ref string pstServidor, ref string PstBasedatos, ref string pstUsuar, ref string pstPassBD)
		{
            bool result = false;
            try
            {                
                Conexion.Obtener_conexion objInstancia = null;
                bool blnConectoSinError = false;
                
                objInstancia = new Conexion.Obtener_conexion();

                SqlConnection tempRefParam = cnConexDos;
                string tempRefParam2 = "";
                string tempRefParam3 = "";
                string tempRefParam4 = "";
                string tempRefParam5 = "";
                string tempRefParam6 = "{SQL Server}";
                string tempRefParam7 = "0";
                string tempRefParam8 = "0";
                string tempRefParam9 = "";
                objInstancia.Conexion(ref tempRefParam, tempRefParam2, tempRefParam3, ref blnConectoSinError, ref tempRefParam4, ref tempRefParam5, pstServidor, tempRefParam6, PstBasedatos, pstUsuar, pstPassBD, tempRefParam7, tempRefParam8, tempRefParam9, Serrores.VVstUsuarioApli);
                cnConexDos = tempRefParam;

                if (cnConexDos != null)
                {
                    result = true;
                    //UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("On Error Goto Label (0)");
                    return result;
                }
            }
            catch (Exception ex)
            {
                RadMessageBox.Show("Imposible realizar conexi�n temporal con la B.D.", Application.ProductName);
                return result;
            }
            return result;
        }

		private string proObtenerusuario(string gsuario)
		{
			string result = String.Empty;
			result = "";
			string sql = "select ISNULL(dnompers,'') dnompers, ISNULL(dap1pers,'') dap1pers, ISNULL(dap2pers,'') dap2pers " + 
			             " From dpersona " + 
			             " inner join susuario on susuario.gpersona = dpersona.gpersona " + 
			             " Where susuario.gusuario = '" + gsuario + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(RR.Tables[0].Rows[0]["dnompers"]).Trim() + " " + Convert.ToString(RR.Tables[0].Rows[0]["dap1pers"]).Trim() + " " + Convert.ToString(RR.Tables[0].Rows[0]["dap2pers"]).Trim();
			}
			RR.Close();
			return result;
		}

		private bool proCoincideMascara(string stPoliza)
		{
			try
			{
				string stMascara = String.Empty;

				// Apuntamos la m�scara

				string tempRefParam = (tbRENPoliza.Mask.Trim() != "") ? tbRENPoliza.Mask.Trim() : ToolTipMain.GetToolTip(tbRENPoliza).Trim();
				stMascara = MascaraParaUsar(ref tempRefParam);

				// Si no hay m�scara, no hay nada que comprobar y salimos

				if (stMascara.Trim() == "")
				{
					return true;
				}

				// Si la p�liza es como la m�scara vac�a, salimos

				if (stPoliza == MascaraVacia())
				{
					return true;
				}

				// Comprobamos si el c�digo y la m�scara son de la misma longitud

				if (stMascara.Trim().Length != stPoliza.Trim().Length)
				{
					return false;
				}

				// Comprobamos si mantienen la estructura

				for (int i = 1; i <= stPoliza.Trim().Length; i++)
				{


					switch(stMascara.Trim().Substring(i - 1, Math.Min(1, stMascara.Trim().Length - (i - 1))))
					{
						case "#" : 
							// Si no es un n�mero, error 
							 
							if (!(String.CompareOrdinal(stPoliza.Trim().Substring(i - 1, Math.Min(1, stPoliza.Trim().Length - (i - 1))), "0") >= 0 && String.CompareOrdinal(stPoliza.Trim().Substring(i - 1, Math.Min(1, stPoliza.Trim().Length - (i - 1))), "9") <= 0))
							{
								return false;
							} 
							 
							break;
						case "?" : 
							 
							// Si no es una letra, error 
							 
							if (!(String.CompareOrdinal(stPoliza.Trim().Substring(i - 1, Math.Min(1, stPoliza.Trim().Length - (i - 1))).ToUpper(), "A") >= 0 && String.CompareOrdinal(stPoliza.Trim().Substring(i - 1, Math.Min(1, stPoliza.Trim().Length - (i - 1))).ToUpper(), "Z") <= 0))
							{
								return false;
							} 
							 
							break;
						default:							 
							// Si no coinciden los caracteres, error 							 
							if (stMascara.Trim().Substring(i - 1, Math.Min(1, stMascara.Trim().Length - (i - 1))).Trim().ToUpper() != stPoliza.Trim().Substring(i - 1, Math.Min(1, stPoliza.Trim().Length - (i - 1))).Trim().ToUpper())
							{
								return false;
							} 							 
							break;
					}
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

		private string proObtenerMascara(int ParamSociedad)
		{
			string result = String.Empty;
			try
			{
				string StSql = String.Empty;
				DataSet RrDatos = null;

				StSql = "SELECT * FROM DSOCMASC WHERE gsocieda = " + ParamSociedad.ToString();

				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
				RrDatos = new DataSet();
				tempAdapter.Fill(RrDatos);

				if (RrDatos.Tables[0].Rows.Count == 0)
				{
					result = "";
				}
				else
				{
					// Comprobamos que los caracteres que hay sean correctos (s�lo se aceptar� # para n�mero y ? para caracter)

					result = Convert.ToString(RrDatos.Tables[0].Rows[0]["omascara"]).Trim();

					for (int i = 1; i <= result.Length; i++)
					{
						if (!Serrores.Valida_Tecla_Pulsada(Strings.Asc(result.Substring(i - 1, Math.Min(1, result.Length - (i - 1)))[0]), "MASCARA"))
						{
							result = "";
							break;
						}
					}

				}

				if (result.Length > 20)
				{
					// Si la longitud de la m�scara es mayor de 20 d�gitos la ignoraremos

					result = "";
				}

				RrDatos.Close();

				return result;
			}
			catch
			{
				return "";
			}
		}

		private string MascaraVacia()
		{
			string tempRefParam = ((tbRENPoliza.Mask.Trim() != "") ? tbRENPoliza.Mask.Trim() : ToolTipMain.GetToolTip(tbRENPoliza).Trim()).Trim();
			string stMascara = MascaraParaUsar(ref tempRefParam);

			string tempRefParam3 = "#";
			string tempRefParam2 = Serrores.proReplace(ref stMascara, tempRefParam3, " ");
			string tempRefParam4 = "?";
			return Serrores.proReplace(ref tempRefParam2, tempRefParam4, " ");

		}

		private string MascaraParaMostrar(ref string stMascara)
		{
			string tempRefParam2 = "#";
			string tempRefParam = Serrores.proReplace(ref stMascara, tempRefParam2, CARACTER_NUMERO);
			string tempRefParam3 = "?";
			return Serrores.proReplace(ref tempRefParam, tempRefParam3, CARACTER_LETRA);
		}

		private string MascaraParaUsar(ref string stMascara)
		{
			string tempRefParam2 = CARACTER_NUMERO;
			string tempRefParam = Serrores.proReplace(ref stMascara, tempRefParam2, "#");
			string tempRefParam3 = CARACTER_LETRA;
			return Serrores.proReplace(ref tempRefParam, tempRefParam3, "?");
		}

		public void procesoTIGESTION(string IdPacienteOld, string fechaCita, string HoraCita, string IdPacienteNew)
		{
			DataSet rdoTemp = null;
			string cadenaSql = String.Empty;
			bool blnTiGestion = false;
			string codOftamologia = String.Empty;
			string rutaUbicacion = String.Empty;
			string cadena = String.Empty;
			int irespuesta = 0;
			bool existeActividad = false;

            try
            {
                cadenaSql = "select * from sconsglo where gconsglo='ITIGESTI'";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(cadenaSql, MFiliacion.GConexion);
                rdoTemp = new DataSet();
                tempAdapter.Fill(rdoTemp);
                if (rdoTemp.Tables[0].Rows.Count != 0)
                {
                    blnTiGestion = ((Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1"]) + "").Trim() == "S");
                    codOftamologia = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu2"]) + "").Trim();
                    rutaUbicacion = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu3"]) + "").Trim() + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1i1"]) + "").Trim() + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu2i1"]) + "").Trim() +
                                    (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu3i1"]) + "").Trim() + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu1i2"]) + "").Trim() + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu2i2"]) + "").Trim() + (Convert.ToString(rdoTemp.Tables[0].Rows[0]["valfanu3i2"]) + "").Trim();

                }
                rdoTemp.Close();

                if (blnTiGestion)
                {

                    if (codOftamologia != "" && rutaUbicacion != "")
                    {
                        cadenaSql = "SELECT * From CCONSULT " +
                                    "INNER JOIN DSERVICI ON CCONSULT.gservici = DSERVICI.gservici  AND DSERVICI.ggrupweb = " + codOftamologia + " " +
                                    "WHERE CCONSULT.Gidenpac = '" + IdPacienteOld + "' OR CCONSULT.Gidenpac = '" + IdPacienteNew + "'";

                        SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(cadenaSql, MFiliacion.GConexion);
                        rdoTemp = new DataSet();
                        tempAdapter_2.Fill(rdoTemp);
                        if (rdoTemp.Tables[0].Rows.Count == 0)
                        {
                            existeActividad = false;

                            cadenaSql = "SELECT * From CCITPROG " +
                                        "INNER JOIN DSERVICI ON CCITPROG.gservici = DSERVICI.gservici  AND DSERVICI.ggrupweb = " + codOftamologia + " " +
                                        "WHERE CCITPROG.Gidenpac = '" + IdPacienteOld + "' OR CCITPROG.Gidenpac = '" + IdPacienteNew + "'";

                            SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(cadenaSql, MFiliacion.GConexion);
                            rdoTemp = new DataSet();
                            tempAdapter_3.Fill(rdoTemp);
                            existeActividad = rdoTemp.Tables[0].Rows.Count != 0;
                            rdoTemp.Close();
                        }
                        else
                        {
                            rdoTemp.Close();
                            existeActividad = true;
                        }

                        if (existeActividad)
                        {

                            cadenaSql = "SELECT dnombpac, dape1pac, ISNULL(dape2pac, '') as dape2pac  FROM DPACIENT WHERE gidenpac = '" + IdPacienteNew + "'";
                            SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(cadenaSql, MFiliacion.GConexion);
                            rdoTemp = new DataSet();
                            tempAdapter_4.Fill(rdoTemp);
                            if (rdoTemp.Tables[0].Rows.Count != 0)
                            {
                                cadena = IdPacienteOld + "|F|" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpac"]) + "|" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pac"]) + "|" + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape2pac"]) + "|";
                            }
                            else
                            {
                                RadMessageBox.Show("Error al crear el fichero de integraci�n TIGESTION, no se pudo recupetar los datos del paciente.", Application.ProductName);
                                return;
                            }

                            cadena = cadena + fechaCita + " " + HoraCita + "|" + IdPacienteNew;

                            FileSystem.FileOpen(1, rutaUbicacion + IdPacienteOld, OpenMode.Append, OpenAccess.Default, OpenShare.Default, -1);
                            FileSystem.PrintLine(1, cadena);
                            FileSystem.FileClose(1);
                            rdoTemp.Close();
                        }
                    }
                    else
                    {
                        short tempRefParam = 4080;
                        string[] tempRefParam2 = new string[] { "el proceso de TIGESTION, revise los valores de la constante ITIGESTI." };
                        irespuesta = Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam, MFiliacion.GConexion, tempRefParam2));
                        return;
                    }
                }
            }
            catch
            {
                short tempRefParam3 = 4080;
                string[] tempRefParam4 = new string[] { "el proceso de TIGESTION, revise los valores de la constante ITIGESTI."};
				irespuesta = Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, tempRefParam3, MFiliacion.GConexion, tempRefParam4));
			}

		}
	}
}