using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Reflection;

namespace filiacionE
{
	public partial class DFI120F1
		: Telerik.WinControls.UI.RadForm
    {
        bool flagDbClick = false;
        bool Alta = false; //CONTROLA SI SE ESTA PRODUCIENDO UN ALTA
		bool seleccionado = false; //SI SE SELECCIONA ALGUN FILIADO DEL GRID
		//Dim x As Integer
		string sql = String.Empty; //LA CONSULTA DE DATOS A BUSCAR
		string SQLRepetidos = String.Empty; //FILIADOS DUPLICADOS MENOS EL DIGITO DE DUPLICIDAD
		//Dim stFiliacion, stModulo As String     'DATOS RECIBIDOS POR EL FORMULARIO ORIGEN
		string sqlult = String.Empty;
		string valor_tarjeta = String.Empty;
		public bool B_ALTA_PACIENTE = false;
		DataSet RrSQLRepetidos = null; //FILIADOS CON EL MISMO GIDENPAC
        Int32 IndiceRrSQLRepetidos = 0;
        bool GridCargado = false; //SI SE CARGA ALGUN FILIADO ENCONTRADO
		bool ABIERTORrSQL = false; //SI ABRE LA BUSQUEDA
		int Archivo = 0;
		int fila = 0;
		bool Actbuscar = false;
		string mensaje = String.Empty;
		private SqlCommand _RqSQL = null;
        private Int32 IndiceRrSQLUltimo = 0;
        Int32 IndiceRrDPACIENT = 0;
        bool botonIzquierdo = false;
        public DFI120F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			isInitializingComponent = true;
			InitializeComponent();
			isInitializingComponent = false;
			ReLoadForm(false);
		}


		SqlCommand RqSQL
		{
			get
			{
				if (_RqSQL == null)
				{
					_RqSQL = new SqlCommand();
				}
				return _RqSQL;
			}
			set
			{
				_RqSQL = value;
			}
		}

		int NRegistros = 0;
		bool CargarMas = false;
		int tope = 0;
		string sql2 = String.Empty;
		string SQLNHIST = String.Empty;
		DataSet RrSQLNHIST = null;
        Int32 IndiceRrSQLNHIST = 0;
		bool P1 = false;
		int fila_seleccionada = 0;
		bool bcbaceptar = false;
		string stComienzoBanda = String.Empty;
		int iNumeroBandas = 0;
		DataSet RrDENTIPAC = null;
        Int32 IndiceRrDENTIPAC = 0;
        int CodigoDeControl = 0;
		int Algoritmo = 0;
		string BinPan = String.Empty;

		public string stVersionLeidaTarjeta = String.Empty;
		public string stNumeroLeidoTarjeta = String.Empty;

		public int TECLAFUNCION = 0;
		bool bBuscarHistoricoDeHistoria = false;
		bool bFormatoCodigoPos = false;

		bool bCipAutonomicoManual = false;
		bool bCIPEnviado = false;

		bool bBusquedaConTarjeta = false;
		bool bRespuestaSexo = false;

		private void Realizar_Busqueda()
		{
			// Aqu� se realiza la consulta de los pacientes que cumplan las caracter�sticas introducidas

			sql = "";

			// Si hay CIP introducido y es de Seguridad Social le�do por tarjeta (campo oculto tbSocieda relleno),
			// buscamos EXCLUSIVAMENTE por ese campo

			int iLongitud = 0;
			int iPosicion = 0;
			if (tbsocieda.Text.Trim() == "" + MFiliacion.CodSS.ToString() && tbCIPAutonomico.Text.Trim() != "")
			{

				sql = "SELECT " + 
				      "* " + 
				      "FROM " + 
				      "DPACIENT " + 
				      "WHERE ";

				if (bCipAutonomicoManual)
				{
					sql = sql + "ocipauto = '" + tbCIPAutonomico.Text + "' ";
				}
				else
				{
					sql = sql + "ncipauto = " + tbCIPAutonomico.Text + " ";
				}

				sql = sql + " ORDER BY dape1pac, dape2pac, dnombpac, gidenpac";

			}
			else
			{

				if (tbNSS.Text != "")
				{

					sql = "SELECT distinct * FROM DPACIENT where ";

					//sql = sql & "  DPACIENT.gidenpac not in (select gidenpac from SPACIENT where DPACIENT.gidenpac = SPACIENT.gidenpac) and "

					sql = sql + "  DPACIENT.dape1pac <> '******' and ";

					if (bBusquedaConTarjeta)
					{
						sql = sql + "  dpacient.gidenpac in (select gidenpac from dentipac where dentipac.nafiliac LIKE '" + tbNSS.Text + "%'";
					}
					else
					{
						sql = sql + "  dpacient.gidenpac in (select gidenpac from dentipac where dentipac.nafiliac = '" + tbNSS.Text + "'";
					}

					if (this.tbsocieda.Text != "")
					{
						sql = sql + " and dentipac.gsocieda=" + this.tbsocieda.Text + " ";
					}

					sql = sql + " ) ";

					sql = sql + " UNION ";

					if (bBusquedaConTarjeta)
					{
						sql = sql + " SELECT distinct * FROM DPACIENT WHERE ( dpacient.NAFILIAC LIKE '" + tbNSS.Text + "%' ";
					}
					else
					{
						sql = sql + " SELECT distinct * FROM DPACIENT WHERE ( dpacient.NAFILIAC = '" + tbNSS.Text + "' ";
					}

					//sql = sql & " and DPACIENT.gidenpac not in (select gidenpac from SPACIENT where DPACIENT.gidenpac = SPACIENT.gidenpac) "

					sql = sql + " and DPACIENT.dape1pac <> '******' ";

					if (this.tbsocieda.Text != "")
					{
						sql = sql + " and dpacient.gsocieda=" + this.tbsocieda.Text + " ";
					}

					sql = sql + " ) UNION ";

				}

				if (this.tbTarjeta.Text != "")
				{

					sql = sql + " SELECT distinct * FROM DPACIENT where dpacient.NTARJETA = '" + this.tbTarjeta.Text + "'";

					//sql = sql & " and DPACIENT.gidenpac not in (select gidenpac from SPACIENT where DPACIENT.gidenpac = SPACIENT.gidenpac) "

					sql = sql + " and DPACIENT.dape1pac <> '******' ";

					if (this.tbsocieda.Text != "")
					{
						sql = sql + " and dpacient.gsocieda=" + this.tbsocieda.Text + " ";
					}

					sql = sql + " UNION ";

				}

				sql = sql + " SELECT distinct * FROM DPACIENT ";

				if (rbBuscar[0].IsChecked && tbNHistoria.Text.Trim() != "")
				{
					sql = sql + " ,HDOSSIER ";
				}

				if (chbFiliacPov.CheckState == CheckState.Checked)
				{
					sql = sql + " Where dpacient.IFILPROV = 'S'";
				}
				else
				{
					sql = sql + " Where dpacient.IFILPROV = 'N' ";
				}

				//sql = sql & " and DPACIENT.gidenpac not in (select gidenpac from SPACIENT where DPACIENT.gidenpac = SPACIENT.gidenpac) "

				sql = sql + " and DPACIENT.dape1pac <> '******' ";

				// Controla si se busca por el N� de DNI

				if (MEBDNI.Text.Trim() != "")
				{
					sql = sql + " And dpacient.NDNINIFP LIKE '" + MEBDNI.Text.Trim() + "%'";
				}

				// Controla si se busca por el N� de TARJETA SANITARIA
				if (tbTarjeta.Text.Trim() != "" && tbApellido1.Text == "")
				{
					sql = sql + " And dpacient.ntarjeta = '" + tbTarjeta.Text + "'";
				}

				// Controla si se busca por Otros documentos (que no sea el DNI/NIF, aunque el campo en base de datos
				// se corresponde con el mismo que el del DNI).

				if (tbOtrosDocumentos.Text.Trim() != "")
				{
					sql = sql + " And dpacient.NDNINIFP = '" + tbOtrosDocumentos.Text.Trim() + "'";
				}

				// Controla si se busca por el N� de la Seguridad Social

				if (tbNSS.Text.Trim() != "")
				{

					if (bBusquedaConTarjeta)
					{

						sql = sql + " And ( dpacient.NAFILIAC LIKE '" + tbNSS.Text + "%' ";

						if (tbsocieda.Text.Trim() != "")
						{
							sql = sql + " AND dpacient.GSOCIEDA = " + tbsocieda.Text.Trim();
						}

						sql = sql + " ) ";

					}
					else
					{

						sql = sql + " And ( dpacient.NAFILIAC = '" + tbNSS.Text + "')";

					}

				}

				// Controla si se busca por el N� de telefono

				if (tbDPTelf1.Text != "")
				{

					sql = sql + " And (dpacient.ntelefo1 = '" + tbDPTelf1.Text + "' or " + 
					      " dpacient.ntelefo2 = '" + tbDPTelf1.Text + "' OR dpacient.ntelefo3 = '" + tbDPTelf1.Text + "')";

				}

				// Apellido1

				if (tbApellido1.Text != "")
				{

					sql = sql + " And dpacient.DAPE1PAC ";

					if (MFiliacion.BDPACIENT == "C")
					{
						sql = sql + " = '" + tbApellido1.Text.Trim() + "'";
					}
					if (MFiliacion.BDPACIENT == "I")
					{
						sql = sql + " like '" + tbApellido1.Text.Trim() + "%'";
					}
					if (MFiliacion.BDPACIENT == "P")
					{
						sql = sql + " like '%" + tbApellido1.Text.Trim() + "%'";
					}

				}

				// Apellido2

				if (tbApellido2.Text != "")
				{

					sql = sql + " And dpacient.DAPE2PAC ";

					if (MFiliacion.BDPACIENT == "C")
					{
						sql = sql + " = '" + tbApellido2.Text.Trim() + "'";
					}
					if (MFiliacion.BDPACIENT == "I")
					{
						sql = sql + " like '" + tbApellido2.Text.Trim() + "%'";
					}
					if (MFiliacion.BDPACIENT == "P")
					{
						sql = sql + " like '%" + tbApellido2.Text.Trim() + "%'";
					}

				}

				// Nombre

				if (tbNombre.Text != "")
				{

					if (tbNombre.Text != "" && tbApellido1.Text == "" && tbApellido2.Text == "" && sdcFechaNac.Text == "" && !rbSexo[0].IsChecked && !rbSexo[1].IsChecked && !rbBuscar[0].IsChecked && !rbBuscar[1].IsChecked && !rbBuscar[2].IsChecked && !rbBuscar[3].IsChecked && !rbBuscar[4].IsChecked && !rbBuscar[5].IsChecked && !rbBuscar[6].IsChecked && !rbBuscar[7].IsChecked)
					{

						sql = sql + " And dpacient.DNOMBPAC = '" + tbNombre.Text.Trim() + "'";

					}
					else
					{

						if (tbNombre.Text.Trim().IndexOf(' ') >= 0)
						{

							sql = sql + " And (";


							iLongitud = tbNombre.Text.Trim().Length;
							iPosicion = 1;


							while(iPosicion < iLongitud)
							{

								if (Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) != 0)
								{

									if (tbNombre.Text.Trim().Substring(iPosicion - 1, Math.Min(Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) - iPosicion, tbNombre.Text.Trim().Length - (iPosicion - 1))).Trim() != "")
									{

										sql = sql + " dpacient.DNOMBPAC LIKE '%" + tbNombre.Text.Trim().Substring(iPosicion - 1, Math.Min(Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) - iPosicion, tbNombre.Text.Trim().Length - (iPosicion - 1))) + "%'";

										if (iPosicion <= iLongitud)
										{
											sql = sql + " or ";
										}

									}

									iPosicion = Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) + 1;

								}
								else
								{

									break;

								}

							};

							sql = sql + " dpacient.DNOMBPAC LIKE '%" + tbNombre.Text.Trim().Substring(iPosicion - 1) + "%')";

						}
						else
						{

							sql = sql + " And dpacient.DNOMBPAC LIKE '%" + tbNombre.Text.Trim() + "%'";

						}

					}

				}

				// Fecha nacimiento

				if (sdcFechaNac.Text != "")
				{

					// Cambia el formato de la fecha a la de la tabla
					//      sdcFechaNac.Mask = MonthDayYear

					sql = sql + " And dpacient.FNACIPAC = " + Serrores.FormatFecha(sdcFechaNac.Text) + "";

				}

				if (rbBuscar[0].IsChecked)
				{

					// Si ha introducido el n�mero de historia pregunta qu� archivo desea consultar

					if (tbNHistoria.Text != "")
					{

						if (cbbArchivo.SelectedIndex == -1)
						{

							// Si no ha introducido ninguno, coge el archivo central

							cbbArchivo.SelectedIndex = Archivo - 1;

							sql = sql + " And hdossier.gserpropiet = " + cbbArchivo.SelectedValue.ToString() + " and hdossier.gidenpac = dpacient.gidenpac and hdossier.ghistoria = " + tbNHistoria.Text; //& " and (hdossier.icontenido = 'H' or hdossier.icontenido = 'P') "

						}
						else
						{

							// Si lo ha introducido coge el archivo seleccionado que se iguale

							sql = sql + " And hdossier.gserpropiet = " + cbbArchivo.SelectedValue.ToString() + " and hdossier.gidenpac = dpacient.gidenpac and hdossier.ghistoria =  " + tbNHistoria.Text; //& " and hdossier.icontenido = 'H'"

						}

					}

				}

				// Sexo

				// Aqu� hay que cambiar y coger los c�digos de sexo

				if (rbSexo[0].IsChecked)
				{

					MFiliacion.Sexo = (true).ToString();
					sql = sql + " And dpacient.ITIPSEXO = '" + MFiliacion.CodSHombre + "'";

				}
				else
				{

					if (rbSexo[1].IsChecked)
					{

						MFiliacion.Sexo = (true).ToString();
						sql = sql + " And dpacient.ITIPSEXO = '" + MFiliacion.CodSMUJER + "'";

					}

				}

				// Controla si se busca por el N� de CIP Autonomico

				if (tbCIPAutonomico.Text.Trim() != "")
				{

					if (bCipAutonomicoManual)
					{
						sql = sql + " and dpacient.ocipauto = '" + tbCIPAutonomico.Text + "' ";
					}
					else
					{
						sql = sql + " And dpacient.ncipauto = " + tbCIPAutonomico.Text + " ";
					}

				}


				// Controla si se busca por el correo electr�nico

				if (tbCorreoElectronico.Text.Trim() != "")
				{

					sql = sql + " And dpacient.ddiemail = '" + tbCorreoElectronico.Text.Trim() + "' ";

				}


				// Condiciones de requery

				if (chbFiliacPov.CheckState != CheckState.Checked)
				{
					sql2 = " And (dpacient.dape1pac > @dape1pac or " +
                           "(dpacient.dape1pac = @dape1pac and dpacient.dape2pac > @dape2pac) or" +
                           "(dpacient.dape1pac = @dape1pac and dpacient.dape2pac = @dape2pac and dpacient.dnombpac > @dnombpac) or " +
                           "(dpacient.dape1pac = @dape1pac and dpacient.dape2pac = @dape2pac and dpacient.dnombpac = @dnombpac and dpacient.gidenpac > @gidenpac))";
				}
				else
				{

					sql2 = "";

				}

				sql = sql + sql2 + " order by dpacient.DAPE1PAC, dpacient.DAPE2PAC ,dpacient.DnombPAC ,DPACIENT.GIDENPAC";

			}

			string SQL7 = sql;

			if (MFiliacion.abierto)
			{	
				MFiliacion.RrSQLUltimo.Close();
				MFiliacion.abierto = false;
			}

			RqSQL.CommandText = SQL7;
			tope = 0;

			if (chbFiliacPov.CheckState != CheckState.Checked && !(tbsocieda.Text.Trim() == "" + MFiliacion.CodSS.ToString() && tbCIPAutonomico.Text.Trim() != ""))
			{
                if (RqSQL.Parameters.Count == 0)
                {
                    RqSQL.Parameters.Add("@dape1pac", SqlDbType.VarChar);
                    RqSQL.Parameters.Add("@dape2pac", SqlDbType.VarChar);
                    RqSQL.Parameters.Add("@dnombpac", SqlDbType.VarChar);
                    RqSQL.Parameters.Add("@gidenpac", SqlDbType.VarChar);
                }

                RqSQL.Parameters["@dape1pac"].Value = "";
				RqSQL.Parameters["@dape2pac"].Value = "";
				RqSQL.Parameters["@dnombpac"].Value = "";
				RqSQL.Parameters["@gidenpac"].Value = "";
			}

		}

		private void Realizar_Busqueda_Cancelados()
		{

			//AQUI SE REALIZA LA CONSULTA DE LOS PACIENTES QUE CUMPLAN LAS CARACTERISTICAS
			//INTRODUCIDAS

			sql = "";
			if (tbNSS.Text != "")
			{
				sql = "SELECT distinct * FROM SPACIENT where ";
				sql = sql + "  SPACIENT.gidenpac in (select gidenpac from SENTIPAC where SENTIPAC.nafiliac='" + tbNSS.Text + "'";
				if (this.tbsocieda.Text != "")
				{
					sql = sql + " and SENTIPAC.gsocieda=" + this.tbsocieda.Text + " ";
				}
				sql = sql + " ) ";
				sql = sql + " UNION ";
				sql = sql + " SELECT distinct * FROM SPACIENT  WHERE ( SPACIENT.NAFILIAC = '" + tbNSS.Text + "' ";
				if (this.tbsocieda.Text != "")
				{
					sql = sql + " and SPACIENT.gsocieda=" + this.tbsocieda.Text + " ";
				}
				sql = sql + " ) UNION ";

			}


			if (this.tbTarjeta.Text != "")
			{
				sql = sql + " SELECT distinct * FROM SPACIENT where SPACIENT.NTARJETA = '" + this.tbTarjeta.Text + "'";
				if (this.tbsocieda.Text != "")
				{
					sql = sql + " and SPACIENT.gsocieda=" + this.tbsocieda.Text + " ";
				}
				sql = sql + " UNION ";
			}


			sql = sql + " SELECT distinct * FROM SPACIENT ";

			if (rbBuscar[0].IsChecked && tbNHistoria.Text.Trim() != "")
			{
				sql = sql + " ,SDOSSIER ";
			}
			if (chbFiliacPov.CheckState == CheckState.Checked)
			{
				sql = sql + " Where SPACIENT.IFILPROV = 'S'";
			}
			else
			{
				sql = sql + " Where SPACIENT.IFILPROV = 'N' ";
			}

			//Controla si se busca por el N� de DNI
			if (MEBDNI.Text.Trim() != "")
			{
				sql = sql + " And SPACIENT.NDNINIFP LIKE '" + MEBDNI.Text.Trim() + "%'";
			}

			//Controla si se busca por el N� de TARJETA SANITARIA
			if (tbTarjeta.Text.Trim() != "" && tbApellido1.Text == "")
			{

				sql = sql + " And SPACIENT.ntarjeta = '" + tbTarjeta.Text + "'";
			}

			//(maplaza)(03/05/2006)Controla si se busca por Otros documentos (que no sea el DNI/NIF, aunque el campo en base de datos
			//se corresponde con el mismo que el del DNI).
			if (tbOtrosDocumentos.Text.Trim() != "")
			{

				sql = sql + " And SPACIENT.NDNINIFP = '" + tbOtrosDocumentos.Text.Trim() + "'";
			}

			//Controla si se busca por el N� de la Seguridad Social


			if (tbNSS.Text.Trim() != "")
			{

				sql = sql + " And ( SPACIENT.NAFILIAC = '" + tbNSS.Text + "')";
			}



			//Controla si se busca por el N� de telefono
			if (tbDPTelf1.Text != "")
			{
				sql = sql + " And (SPACIENT.ntelefo1 = '" + tbDPTelf1.Text + "' or " + 
				      " SPACIENT.ntelefo2 = '" + tbDPTelf1.Text + "' OR SPACIENT.ntelefo3 = '" + tbDPTelf1.Text + "')";
			}

			//Apellido1
			if (tbApellido1.Text != "")
			{
				sql = sql + " And SPACIENT.DAPE1PAC ";
				if (MFiliacion.BDPACIENT == "C")
				{
					sql = sql + " = '" + tbApellido1.Text.Trim() + "'";
				}
				if (MFiliacion.BDPACIENT == "I")
				{
					sql = sql + " like '" + tbApellido1.Text.Trim() + "%'";
				}
				if (MFiliacion.BDPACIENT == "P")
				{
					sql = sql + " like '%" + tbApellido1.Text.Trim() + "%'";
				}
			}

			//Apellido2
			if (tbApellido2.Text != "")
			{
				sql = sql + " And SPACIENT.DAPE2PAC ";
				if (MFiliacion.BDPACIENT == "C")
				{
					sql = sql + " = '" + tbApellido2.Text.Trim() + "'";
				}
				if (MFiliacion.BDPACIENT == "I")
				{
					sql = sql + " like '" + tbApellido2.Text.Trim() + "%'";
				}
				if (MFiliacion.BDPACIENT == "P")
				{
					sql = sql + " like '%" + tbApellido2.Text.Trim() + "%'";
				}
			}

			//Nombre
			int iLongitud = 0;
			int iPosicion = 0;
			if (tbNombre.Text != "")
			{
				if (tbNombre.Text != "" && tbApellido1.Text == "" && tbApellido2.Text == "" && sdcFechaNac.Text == "" && !rbSexo[0].IsChecked && !rbSexo[1].IsChecked && !rbBuscar[0].IsChecked && !rbBuscar[1].IsChecked && !rbBuscar[2].IsChecked && !rbBuscar[3].IsChecked && !rbBuscar[4].IsChecked && !rbBuscar[5].IsChecked && !rbBuscar[6].IsChecked && !rbBuscar[7].IsChecked)
				{
					sql = sql + " And SPACIENT.DNOMBPAC = '" + tbNombre.Text.Trim() + "'";
				}
				else
				{
					if (tbNombre.Text.Trim().IndexOf(' ') >= 0)
					{
						sql = sql + " And (";
						iLongitud = tbNombre.Text.Trim().Length;
						iPosicion = 1;

						while(iPosicion < iLongitud)
						{
							if (Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) != 0)
							{
								if (tbNombre.Text.Trim().Substring(iPosicion - 1, Math.Min(Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) - iPosicion, tbNombre.Text.Trim().Length - (iPosicion - 1))).Trim() != "")
								{
									sql = sql + " SPACIENT.DNOMBPAC LIKE '%" + tbNombre.Text.Trim().Substring(iPosicion - 1, Math.Min(Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) - iPosicion, tbNombre.Text.Trim().Length - (iPosicion - 1))) + "%'";
									if (iPosicion <= iLongitud)
									{
										sql = sql + " or ";
									}
								}
								iPosicion = Strings.InStr(iPosicion, tbNombre.Text.Trim(), " ", CompareMethod.Text) + 1;
							}
							else
							{
								break;
							}
						};
						sql = sql + " SPACIENT.DNOMBPAC LIKE '%" + tbNombre.Text.Trim().Substring(iPosicion - 1) + "%')";
					}
					else
					{
						sql = sql + " And SPACIENT.DNOMBPAC LIKE '%" + tbNombre.Text.Trim() + "%'";
					}
				}
			}


			//Fecha nacimiento
			if (sdcFechaNac.Text != "")
			{
				//Cambia el formato de la fecha a la de la tabla
				//      sdcFechaNac.Mask = MonthDayYear
				sql = sql + " And SPACIENT.FNACIPAC = " + Serrores.FormatFecha(sdcFechaNac.Text) + "";
			}

			if (rbBuscar[0].IsChecked)
			{
				//si ha introducido el numero de historia pergunta que archivo desea consultar
				if (tbNHistoria.Text != "")
				{
					if (cbbArchivo.SelectedIndex == -1)
					{
						//si no ha introducido ninguno coge el archivo central
						cbbArchivo.SelectedIndex = Archivo - 1;
						sql = sql + " And SDOSSIER.gserpropiet = " + cbbArchivo.SelectedValue.ToString() + " and SDOSSIER.gidenpac = SPACIENT.gidenpac and SDOSSIER.ghistoria = " + tbNHistoria.Text; //& " and (SDOSSIER.icontenido = 'H' or SDOSSIER.icontenido = 'P') "
					}
					else
					{
						//si lo ha introducido coge el archivo seleccionado
						//que se iguale
						sql = sql + " And SDOSSIER.gserpropiet = " + cbbArchivo.SelectedValue.ToString() + " and SDOSSIER.gidenpac = SPACIENT.gidenpac and SDOSSIER.ghistoria =  " + tbNHistoria.Text; //& " and SDOSSIER.icontenido = 'H'"
					}
				}
			}

			//sexo
			//aqui hay que cambiar y coger los codigos de sexo
			if (rbSexo[0].IsChecked)
			{
				MFiliacion.Sexo = (true).ToString();
				sql = sql + " And SPACIENT.ITIPSEXO = '" + MFiliacion.CodSHombre + "'";
			}
			else
			{
				if (rbSexo[1].IsChecked)
				{
					MFiliacion.Sexo = (true).ToString();
					sql = sql + " And SPACIENT.ITIPSEXO = '" + MFiliacion.CodSMUJER + "'";
				}
			}

			//Oscar C. Octubre Controla si se busca por el N� de CIP Autonomico
			if (tbCIPAutonomico.Text.Trim() != "")
			{
				if (bCipAutonomicoManual)
				{
					sql = sql + " and SPACIENT.ocipauto = '" + tbCIPAutonomico.Text + "' ";
				}
				else
				{
					sql = sql + " And SPACIENT.ncipauto = " + tbCIPAutonomico.Text + " ";
				}
			}


			// Controla si se busca por el correo electr�nico
			if (tbCorreoElectronico.Text.Trim() != "")
			{

				sql = sql + " And SPACIENT.ddiemail = '" + tbCorreoElectronico.Text.Trim() + "' ";

			}


			// condiciones de requery
			if (chbFiliacPov.CheckState != CheckState.Checked)
			{
				sql2 = " And (SPACIENT.dape1pac > @dape1pac or " +
                       "(SPACIENT.dape1pac = @dape1pac and SPACIENT.dape2pac > @dape2pac) or" +
                       "(SPACIENT.dape1pac = @dape1pac and SPACIENT.dape2pac = @dape2pac and SPACIENT.dnombpac >  @dnombpac) or " +
                       "(SPACIENT.dape1pac = @dape1pac and SPACIENT.dape2pac = @dape2pac and SPACIENT.dnombpac =  @dnombpac and SPACIENT.gidenpac > @gidenpac))";
			}
			else
			{
				sql2 = "";
			}

			sql = sql + sql2 + " order by SPACIENT.DAPE1PAC, SPACIENT.DAPE2PAC ,SPACIENT.DnombPAC ,SPACIENT.GIDENPAC";
			string SQL7 = sql;
			if (MFiliacion.abierto)
			{
				MFiliacion.RrSQLUltimo.Close();
				MFiliacion.abierto = false;
			}
			RqSQL.CommandText = SQL7;
			tope = 0;
			if (chbFiliacPov.CheckState != CheckState.Checked)
			{
                if (RqSQL.Parameters.Count == 0)
                {
                    RqSQL.Parameters.Add("@dape1pac", SqlDbType.VarChar);
                    RqSQL.Parameters.Add("@dape2pac", SqlDbType.VarChar);
                    RqSQL.Parameters.Add("@dnombpac", SqlDbType.VarChar);
                    RqSQL.Parameters.Add("@gidenpac", SqlDbType.VarChar);
                }

                RqSQL.Parameters["@dape1pac"].Value = "";
                RqSQL.Parameters["@dape2pac"].Value = "";
                RqSQL.Parameters["@dnombpac"].Value = "";
                RqSQL.Parameters["@gidenpac"].Value = "";
            }

		}

		//*******************************************************************************************
		//* O.Frias (31/07/2007) - Incorporo la operativa para la solicitud de usuario para cita WEB
		//*******************************************************************************************
		private void Resultado_Busqueda()
		{
			bool blnContinuaProceso = false;
			string StSqlTarjeta = String.Empty;


			ABIERTORrSQL = true;
            //Abre la consulta con los registros coincidentes           
           
           SqlDataAdapter tempAdapter = new SqlDataAdapter(RqSQL);
           MFiliacion.RrSQLUltimo = new DataSet();
           MFiliacion.RrSQLUltimo.Tables.Add( new DataTable());
           tempAdapter.Fill(0, NRegistros, MFiliacion.RrSQLUltimo.Tables[0]);
           IndiceRrSQLUltimo = 0;
           MFiliacion.abierto = true;

           if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count == 0)
           {
               if (this.tbTarjeta.Text != "")
               {
                   if (mbAcceso.ObtenerCodigoSConsglo("BCRTRJTA", "valfanu1", MFiliacion.GConexion) == "S")
                   {
                       MFiliacion.RrSQLUltimo.Close();
                       StSqlTarjeta = "select top 1 dpacient.* from dpacient inner join dentipac on dpacient.gidenpac = dentipac.gidenpac";
                       StSqlTarjeta = StSqlTarjeta + " where dentipac.nafiliac LIKE '" + tbTarjeta.Text + "%'";
                       StSqlTarjeta = StSqlTarjeta + " order by fmovimie desc";
                       SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSqlTarjeta, MFiliacion.GConexion);
                       MFiliacion.RrSQLUltimo = new DataSet();
                       tempAdapter_2.Fill(MFiliacion.RrSQLUltimo);
                       IndiceRrSQLUltimo = 0;
                       //BuscoPacientePorNumTarjeta
                   }
               }
           }
           //
           //Controla si existen registros con los parametros seleccionados
           if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count == 0)
           {
               Serrores.ObtenerNombreAplicacion(MFiliacion.GConexion);
               //-----------------------------------------
               //Si se ha buscado por n�mero de historia, no se encuentra nada y est� activa la constantes, se busca la historia activa para presentarla en pantalla
               if (bBuscarHistoricoDeHistoria && rbBuscar[0].IsChecked)
               {
                   if (HistoriaActiva())
                   {
                       return;
                   }
               }
               //-----------------------------------------


               if (sprFiliados.MaxRows == 0)
               {

                   if (MFiliacion.stFiliacion == "MODIFICACION" && chbFiliacPov.CheckState == CheckState.Checked)
                   {
                       MFiliacion.stFiliacion = "ALTA";
                   }


                   //Si no existe ninguno analiza que viene de alta, en caso
                   //afirmativo deja dar el alta
                   if (MFiliacion.stFiliacion == "ALTA" || MFiliacion.stFiliacion == "MODIFICACION")
                   {

                       if (chbFiliacPov.CheckState != CheckState.Checked)
                       {
                           if (MFiliacion.stModulo == "URGENCIAS" && (sdcFechaNac.Text == "" || (!rbSexo[0].IsChecked && !rbSexo[1].IsChecked)))
                           {
                               if (blnContinuaProceso)
                               {
                                   if (!cbNuevo.Visible)
                                   {
                                       MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1600, MFiliacion.GConexion, ""));
                                   }
                                   else
                                   {
                                       MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1370, MFiliacion.GConexion, "pacientes filiados con estas caracteristicas", "un paciente nuevo", "completar todos sus datos personales (Nombre, apellidos, fecha de nacimiento y sexo) o filiarlo como provisional"));
                                   }
                               }
                           }
                           else
                           {
                               if (MFiliacion.stModulo == "URGENCIAS" || MFiliacion.stModulo == "CITACION" || MFiliacion.stModulo == "CITARAPIDA")
                               {
                                   if (!cbNuevo.Visible)
                                   {
                                       if (blnContinuaProceso)
                                       {
                                           MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "",1600, MFiliacion.GConexion, ""));
                                       }
                                   }
                                   else
                                   {
                                       if (tbApellido1.Text == "" || tbNombre.Text == "")
                                       {
                                           if (blnContinuaProceso)
                                           {
                                               MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1370, MFiliacion.GConexion, "pacientes filiados con estas caracteristicas", "un paciente nuevo", "completar todos sus datos personales (Nombre, apellidos, fecha de nacimiento y sexo)"));
                                           }
                                       }
                                       else
                                       {
                                           MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1360, MFiliacion.GConexion, ""));
                                       }

                                       if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.Yes)
                                       {
                                           if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) == "S" && mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu2", MFiliacion.GConexion) == "S" && MFiliacion.stModulo == "CITARAPIDA" && !rbSexo[0].IsChecked && !rbSexo[1].IsChecked)
                                           {
                                               MFiliacion.clasemensaje.RespuestaMensaje("Filiaci�n", "",1040, MFiliacion.GConexion, "tipo sexo");
                                               return;
                                           }
                                           if (sdcFechaNac.Text == "")
                                           {
                                               MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1390, MFiliacion.GConexion, Label3.Text));
                                               if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.No)
                                               {
                                                   return;
                                               }
                                           }

                                           //O.Frias -- 15/11/2010
                                           //Seg�n la constante se opera con Indeterminado o no
                                           if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) != "S")
                                           {
                                               if (!rbSexo[0].IsChecked && !rbSexo[1].IsChecked)
                                               {

                                                   MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1400, MFiliacion.GConexion, ""));
                                                   if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.No)
                                                   {
                                                       return;
                                                   }
                                                   bRespuestaSexo = true;
                                               }
                                           }

                                           if (tbApellido2.Text == "")
                                           {
                                               MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1390, MFiliacion.GConexion, "Segundo apellido"));
                                               if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.No)
                                               {
                                                   tbApellido2.Focus();
                                                   return;
                                               }
                                           }
                                       }
                                       else
                                       {
                                           //respuesta distinto de vbyes
                                           return;
                                       }

                                   }

                               }
                               else
                               {
                                   //no es "URGENCIAS" Or "CITACION" Or  "CITARAPIDA"
                                   if (!cbNuevo.Visible)
                                   {
                                       if (blnContinuaProceso)
                                       {
                                           MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1600, MFiliacion.GConexion, ""));
                                       }
                                   }
                                   else
                                   {
                                       if (tbApellido1.Text == "" || tbNombre.Text == "" || sdcFechaNac.Text == "")
                                       {
                                           if (blnContinuaProceso)
                                           {
                                               MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1370, MFiliacion.GConexion, "pacientes filiados con estas caracteristicas", "un paciente nuevo", "completar todos sus datos personales (Nombre, apellidos, fecha de nacimiento y sexo)"));
                                           }
                                       }
                                       else
                                       {	
                                           MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1360, MFiliacion.GConexion, ""));
                                       }
                                   }
                               }

                           } //fin modulo urgencias

                       }
                       else
                       {
                           //es filiacion provisional
                           mensaje = ((int) System.Windows.Forms.DialogResult.Yes).ToString();
                       }

                       //Si no existen registros coincidentes se pregunta si se le quiere
                       //dar de alta
                       if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.Yes)
                       {
                           //Empieza a realizar las operaciones para realizar
                           //un alta
                           sprFiliados.MaxRows = 0;
                           Alta_Paciente();
                       }
                       else
                       {
                           //            tbApellido1.SetFocus
                       }
                       if (MFiliacion.stModulo == "CITARAPIDA")
                       {
                           return;
                       }

                       //OSCAR C FEB 2006
                       //************* O.Frias - 31/07/2007 *************
                       if ((MFiliacion.bProcedentedeRULEQ))
                       {
                           bcbaceptar = true;
                       }
                       if ((MFiliacion.bProcedentedeRULEQ) && MFiliacion.irespuesta != System.Windows.Forms.DialogResult.Yes)
                       {
                           this.Close();
                       }
                       //------

                   }
                   else
                   {
                       // no es en modo "ALTA" Or "MODIFICACION"
                       MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1600, MFiliacion.GConexion, "");
                       Activar_buscar();
                       tbApellido1.Focus();
                       sprFiliados.MaxRows = 0;
                   }
               }
           }
           else
           {
               //encuentra algun paciente
               // si encuentra un solo paciente y se ha seleccionado por historia,
               // dni o numero de filiacion se considera seleccionado
               if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count == 1 && ((rbBuscar[0].IsChecked && tbNHistoria.Text.Trim() != "") || (rbBuscar[1].IsChecked && MEBDNI.Text.Trim() != "") || (rbBuscar[2].IsChecked && tbNSS.Text.Trim() != "") || (rbBuscar[3].IsChecked && tbDPTelf1.Text.Trim() != "") || (rbBuscar[4].IsChecked && tbTarjeta.Text.Trim() != "") || (rbBuscar[5].IsChecked && tbOtrosDocumentos.Text.Trim() != "") || (rbBuscar[6].IsChecked && tbCIPAutonomico.Text.Trim() != "") || (rbBuscar[7].IsChecked && tbCorreoElectronico.Text.Trim() != "")))
               {
                   MFiliacion.ModCodigo = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gidenpac"]);
                   GridCargado = true;
                   cbBuscar.Enabled = false;
                   seleccionado = true;
                   CargarMas = false;
                   MFiliacion.CodPac = true;
                   cbAceptar_Click(cbAceptar, new EventArgs());
               }
               else
               {
                   if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count == NRegistros)
                   {
                       Cargar_Grid();
                       tope = MFiliacion.RrSQLUltimo.Tables[0].Rows.Count + tope - 5;
                       CargarMas = true;
                       //Aqui hay mas de 200 pacientes (hay que cargar los siguientes
                       //pacientes al hacer scrool en el grid hacia abajo)
                   }
                   else
                   {
                       if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count < NRegistros)
                       {
                           Cargar_Grid();
                           CargarMas = false;
                           //Aqui hay menos de 200 pacientes (No hay que cargar m�s pacientes)
                       }
                   }
               }
           }

       }
       private void Alta_Paciente()
       {
           B_ALTA_PACIENTE = false;
           bcbaceptar = true;
           //Controla que se introduzcan todos los datos personales para dar
           //de alta un nuevo paciente
           if (chbFiliacPov.CheckState != CheckState.Checked)
           {
               if ((MFiliacion.stModulo == "URGENCIAS" || MFiliacion.stModulo == "CITACION" || MFiliacion.stModulo == "CITARAPIDA") && sdcFechaNac.Text == "")
               {
                   B_ALTA_PACIENTE = true;
                   GENERAR_IDENTIFICADOR_PROVISIONAL();
                   IDENTIFICADOR_PROVISIONAL_REPETIDO();

                   if (MFiliacion.stModulo == "CITARAPIDA")
                   {
                       return;
                   }
                   if (MFiliacion.stModulo != "DATOS COMUNES")
                   {
                       if (MFiliacion.stModulo != "CITACION")
                       {
                           if (MFiliacion.stModulo == "URGENCIAS" && chbFiliacPov.CheckState == CheckState.Checked)
                           {
                               cbCancelar_Click(cbCancelar, new EventArgs());
                           }
                           else
                           {
                               if (MFiliacion.stModulo == "URGENCIAS" && chbFiliacPov.CheckState == CheckState.Unchecked)
                               {
                                   Alta = true;
                                   MFiliacion.stFiliacion = "ALTA";
                                   MFiliacion.paso = true;
                                   DFI111F1 tempLoadForm = DFI111F1.DefInstance;
                                   DFI111F1.DefInstance.ShowDialog();
                                   cbCancelar_Click(cbCancelar, new EventArgs());
                                   return;
                                   GridCargado = false;
                                   seleccionado = false;
                                   Limpiar_Cajas();
                               }
                           }
                       }
                       else
                       {
                           Alta = true;
                           MFiliacion.stFiliacion = "ALTA";
                           MFiliacion.paso = true;
                           DFI111F1 tempLoadForm2 = DFI111F1.DefInstance;
                           DFI111F1.DefInstance.ShowDialog();
                           GridCargado = false;
                           seleccionado = false;
                           bcbaceptar = true;
                           Limpiar_Cajas();
                           if (MFiliacion.stModulo != "DATOS COMUNES")
                           {
                               cbCancelar_Click(cbCancelar, new EventArgs());
                               return;
                           }
                       }
                   }
               }
               else
               {

                   if (tbApellido1.Text == "" || tbNombre.Text == "" || sdcFechaNac.Text == "")
                   {
                       MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1380, MFiliacion.GConexion, "paciente nuevo", "completar los datos personales del paciente (Nombre, apellidos, fecha de nacimiento y sexo)");
                       tbApellido1.Focus();
                       return;
                   }


                   //O.Frias -- 15/11/2010
                   //Seg�n la constante se opera con Indeterminado o no
                   if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) != "S")
                   {
                       if (!rbSexo[0].IsChecked && !rbSexo[1].IsChecked && !bRespuestaSexo)
                       {
                           MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1400, MFiliacion.GConexion, ""));
                           if (MFiliacion.irespuesta == System.Windows.Forms.DialogResult.No)
                           {
                               return;
                           }
                       }
                   }

                   //Limpia la variable del codigo para generar uno nuevo
                   MFiliacion.codigo = "";
                   //Genera el identificador �nico de cada pacente
                   Generar_Identificador();
                   //Una vez generado el identificador controla que haya coincidencias
                   Identificador_Repetido();

                   //            GrabarUltimoPaciente UCase(CODIGO)

                   if (MFiliacion.stModulo == "CITARAPIDA")
                   {
                       B_ALTA_PACIENTE = true;
                       GRABAR_REGISTRO();
                       bcbaceptar = true;
                       this.Close();
                       return;
                   }
                   MFiliacion.stFiliacion = "ALTA";
                   MFiliacion.paso = true;
                  DFI111F1 tempLoadForm3 = DFI111F1.DefInstance;
                  DFI111F1.DefInstance.ShowDialog();
                  GridCargado = false;
                  seleccionado = false;
                  Limpiar_Cajas();
                  if (MFiliacion.stModulo != "DATOS COMUNES")
                  {
                      cbCancelar_Click(cbCancelar, new EventArgs());
                      return;
                  }
              }
          }
          else
          {
              GENERAR_IDENTIFICADOR_PROVISIONAL();
              IDENTIFICADOR_PROVISIONAL_REPETIDO();

              if (MFiliacion.stModulo != "DATOS COMUNES")
              {
                  cbCancelar_Click(cbCancelar, new EventArgs());
              }

          }

      }
      private void Generar_Identificador()
      {
          //Se crea un array con las vocales para compararlas con la cadena
          MFiliacion.Vocales = new string[]{"A", "E", "I", "O", "U", "�", "�", "�", "�", "�", "�"};
          //Coge el primer apellido
          MFiliacion.caracteres = Strings.Len(tbApellido1.Text);
          //Transforma en may�sculas
          MFiliacion.cadena = tbApellido1.Text.ToUpper();
          Codigo_Paciente();
          //si tiene, coge el segundo apellido
          if (tbApellido2.Text != "")
          {
              MFiliacion.caracteres = Strings.Len(tbApellido2.Text);
              //Transforma en may�sculas
              MFiliacion.cadena = tbApellido2.Text.ToUpper();
              //Coge las dos primeras consonantes del apellido 1� y 2�
              Codigo_Paciente();
          }
          else
          {
              //si no tiene segundo apellido
              MFiliacion.codigo = MFiliacion.codigo + "XX";
          }
          //Transforma la fecha en A�o/Mes/D�a
          //Coge solo los d�gitos de la fecha
          Fecha_Nacimiento();
      }
      //Para Coger los n�meros de la fecha de nacimento
      //para generar el identificador y no coger (/)
      private void Fecha_Nacimiento()
      {
            //string Letra = DateTime.Parse(sdcFechaNac.Value.Date).ToString("yyyy");
          string Letra = sdcFechaNac.Value.Date.ToString("yyyy");
          MFiliacion.codigo = MFiliacion.codigo + Letra;
          Letra = sdcFechaNac.Value.Date.ToString("MM");
          MFiliacion.codigo = MFiliacion.codigo + Letra;
          Letra = sdcFechaNac.Value.Date.ToString("dd");
          //si es mujer se le suma 40 al d�a de la fecha
          if (rbSexo[1].IsChecked)
          {
              Letra = (Double.Parse(Letra) + 40).ToString();
          }
          MFiliacion.codigo = MFiliacion.codigo + Letra;
      }
      private void Codigo_Paciente()
      {
          //declara variable para controlar que coja las dos primeras consomantes
          int cont = 0;
          //declara variable para saber si es consonante
          bool vocal = false;
          //Declara variable para introducir cada letra del apellido
          string Letra = String.Empty;
          //Declara variable para recorrer la cadena
          //Declara variable para recorrer la matriz
          for (int i = 1; i <= MFiliacion.caracteres; i++)
          {
              Letra = MFiliacion.cadena.Substring(i - 1, Math.Min(1, MFiliacion.cadena.Length - (i - 1)));
              if (Strings.Asc(Letra[0]) >= Strings.Asc('A') && Strings.Asc(Letra[0]) <= Strings.Asc('Z'))
              {
                  if (Letra.Trim() != "")
                  {
                      if (Letra == "�")
                      {
                          if (cont < 2)
                          {
                              MFiliacion.codigo = MFiliacion.codigo + "X";
                              cont++;
                          }
                      }
                      else
                      {
                          for (int v = 0; v <= 10; v++)
                          {
                              if (Letra == MFiliacion.Vocales[v])
                              {
                                  vocal = true;
                                  break;
                              }
                          }
                          if (!vocal)
                          {
                              if (cont < 2)
                              {
                                  MFiliacion.codigo = MFiliacion.codigo + Letra;
                                  cont++;
                              }
                          }
                          else
                          {
                              vocal = false;
                          }
                      }
                  }
              }
          }
          if (cont < 2)
          {
              vocal = false;
              for (int i = 1; i <= MFiliacion.caracteres; i++)
              {
                  Letra = MFiliacion.cadena.Substring(i - 1, Math.Min(1, MFiliacion.cadena.Length - (i - 1)));
                  if (Letra.Trim() != "")
                  {
                      //If letra = "�" Then
                      //CODIGO = CODIGO & "X"
                      //cont = cont + 1
                      //Else
                      for (int v = 0; v <= 10; v++)
                      {
                          if (Letra == MFiliacion.Vocales[v])
                          {
                              vocal = true;
                              break;
                          }
                      }
                      if (vocal)
                      {
                          if (cont < 2)
                          {
                              MFiliacion.codigo = MFiliacion.codigo + "X";
                              cont++;
                          }
                      }
                      else
                      {
                          vocal = false;
                      }
                      //End If
                  }
              }
              if (cont == 1)
              {
                  MFiliacion.codigo = MFiliacion.codigo + "X";
              }
              else
              {
                  if (cont == 0)
                  {
                      MFiliacion.codigo = MFiliacion.codigo + "XX";
                  }
              }
          }
      }
      public void GrabarUltimoPaciente(string stGidenpac)
      {
          string tstDatos = String.Empty;
          string usuarioconectado = String.Empty;

          int tlIdConexion = Serrores.ConexionTS();
          //obtengo UsuarioNT
          string tstUsuNT = Serrores.obtener_usuario();
          //obtengo nombre maquina

          Conexion.Obtener_conexion tInstancia = new Conexion.Obtener_conexion();
          string tstMAquina = tInstancia.NonMaquina();
          tInstancia = null;

          if (MFiliacion.GConexion.getConnect().IndexOf("APP=") >= 0)
          {
              usuarioconectado = MFiliacion.GConexion.getConnect().Substring(Strings.InStr(MFiliacion.GConexion.getConnect().IndexOf("APP=") + 1, MFiliacion.GConexion.getConnect(), "\\", CompareMethod.Binary), Math.Min(Strings.InStr(MFiliacion.GConexion.getConnect().IndexOf("APP=") + 1, MFiliacion.GConexion.getConnect(), ";", CompareMethod.Binary) - Strings.InStr(MFiliacion.GConexion.getConnect().IndexOf("APP=") + 1, MFiliacion.GConexion.getConnect(), "\\", CompareMethod.Binary) - 1, MFiliacion.GConexion.getConnect().Length - Strings.InStr(MFiliacion.GConexion.getConnect().IndexOf("APP=") + 1, MFiliacion.GConexion.getConnect(), "\\", CompareMethod.Binary)));
              if (tstMAquina == "")
              {
                  tstMAquina = usuarioconectado;
              }
          }

          if (MFiliacion.stModulo == "IFMS")
          {
              //tlIdConexion = 0
              tstDatos = "select * from SSESUSUA where idconexi=" + tlIdConexion.ToString() + " and dusuarnt='" + MFiliacion.tstUsuNT_IFMS + "' " + " and puestopc='" + MFiliacion.tstMAquina_IFMS + "'" + " and gusuario = '" + MFiliacion.usuario_IFMS + "'";
          }
          else
          {
              tstDatos = "select * from SSESUSUA where idconexi=" + tlIdConexion.ToString() + " and dusuarnt='" + tstUsuNT + "' " + " and puestopc='" + tstMAquina + "'" + " and gusuario = '" + Serrores.VVstUsuarioApli + "'";
          }

          SqlDataAdapter tempAdapter = new SqlDataAdapter(tstDatos, MFiliacion.GConexion);
          DataSet tRrDatos = new DataSet();
            Int32 IndicetRrDatos = 0;
          tempAdapter.Fill(tRrDatos);
        
          if (tRrDatos.Tables[0].Rows.Count != 0)
          {
              tRrDatos.Edit();
              tRrDatos.Tables[0].Rows[IndicetRrDatos]["gidenpac"] = stGidenpac;
              SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
              tempAdapter.Update(tRrDatos, tRrDatos.Tables[0].TableName);
          }
          tRrDatos.Close();

      }
      private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
      {
          this.Cursor = Cursors.WaitCursor;

          GrabarUltimoPaciente(MFiliacion.ModCodigo);

          if (chbFiliacPov.CheckState == CheckState.Checked)
          {
              if (seleccionado)
              {
                  MFiliacion.stFiliacion = "SELECCION";
              }
              else
              {
                  mensaje = ((int) RadMessageBox.Show("�Desea dar de alta a un paciente nuevo?", "Fliacion de pacientes", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2)).ToString();
                  if (mensaje == ((int) System.Windows.Forms.DialogResult.Yes).ToString())
                  {
                      chbFiliacPov.CheckState = CheckState.Checked;
                      seleccionado = false;
                      MFiliacion.stFiliacion = "ALTA";
                  }
                  else
                  {   
                    this.Cursor = Cursors.Default;
                    return;
			      }
				}
			}
			if (chbFiliacPov.CheckState == CheckState.Checked)
			{
				//SI ES FILIACION PROVISIONAL
				//Y SI NO TIENE LOS DATOS PARA UNA FILIACION DEFINITIVA
				//Y ES UN ALTA
				if (!seleccionado)
				{
					if (tbApellido1.Text == "" || sdcFechaNac.Text == "")
					{
						if (MFiliacion.stFiliacion == "ALTA")
						{
							GENERAR_IDENTIFICADOR_PROVISIONAL();
							IDENTIFICADOR_PROVISIONAL_REPETIDO();
						}
						else
						{
							MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1240, MFiliacion.GConexion, "");
						}
					}
					else
					{
						chbFiliacPov.CheckState = CheckState.Unchecked;
						cbBuscar_Click(cbBuscar, new EventArgs());
					}
				}
				else
				{
					MANDAR_DATOS();
					CERRAR_TODO();
				}
			}
			else
			{
				//SI NO ES FILIACION PROVISIONAL SIGUE TODO IGUAL
				//Si ha cargado algun registro
				if (GridCargado)
				{
					//y selecciona algun filiado existente
					if (seleccionado)
					{
						//Si es de modificacion modificar los datos ******'
						if (MFiliacion.stFiliacion == "ALTA")
						{
							MFiliacion.stFiliacion = "MODIFICACION";
						}
						if (MFiliacion.stFiliacion == "MODIFICACION")
						{
							MFiliacion.paso = true;
							bcbaceptar = true;
							if (MFiliacion.stModulo != "CITARAPIDA")
							{
                                DFI111F1 tempLoadForm = DFI111F1.DefInstance;
								DFI111F1.DefInstance.ShowDialog();
							}
							if (MFiliacion.stModulo == "CITARAPIDA")
							{
								MANDAR_DATOS();
							}
							GridCargado = false;
							seleccionado = false;
							Limpiar_Cajas();
                            
							this.Cursor = Cursors.Default;
                            if (MFiliacion.stModulo != "DATOS COMUNES")
							{
								cbCancelar_Click(cbCancelar, new EventArgs());
							}
							return;
						}
						//Si es de alta modifica los datos y
						//recoge los datos al seleccionarlo
						if (MFiliacion.stFiliacion == "ALTA")
						{
							if (MFiliacion.stModulo == "CITARAPIDA")
							{
								MFiliacion.stFiliacion = "SELECCION";
							}
							else
							{
								MFiliacion.stFiliacion = "MODIFICACION";
								MFiliacion.paso = true;
								MFiliacion.CodPac = true;
                                DFI111F1 tempLoadForm2 = DFI111F1.DefInstance;
								DFI111F1.DefInstance.ShowDialog();
                                if (MFiliacion.stModulo != "DATOS COMUNES")
								{
									cbCancelar_Click(cbCancelar, new EventArgs());
								}
							}
						}
						//Si es de seleccion recoge los datos al seleccionarlo
						if (MFiliacion.stFiliacion == "SELECCION")
						{
							this.Visible = false;
							sqlult = "select * from dpacient where gidenpac = '" + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gidenpac"]) + "'";
							SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlult, MFiliacion.GConexion);
							MFiliacion.RrSQLUltimo = new DataSet();
							tempAdapter.Fill(MFiliacion.RrSQLUltimo);
                            IndiceRrSQLUltimo = 0;
                            bcbaceptar = true;
							MANDAR_DATOS();

							CERRAR_TODO();
						}
						//Si no selecciona ninguno
					}
					else
					{
						// y es una alta
						if (MFiliacion.stFiliacion == "ALTA")
						{
							mensaje = ((int) RadMessageBox.Show("�Desea dar de alta a un paciente nuevo?", "Fliacion de pacientes", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2)).ToString();
							//Si es afirmativo realiza el alta
							if (mensaje == ((int) System.Windows.Forms.DialogResult.Yes).ToString())
							{
								sprFiliados.MaxRows = 0;
								Alta = true;
								Alta_Paciente();
							}
						}
						//Y es una seleccion
						if (MFiliacion.stFiliacion == "SELECCION")
						{
							CERRAR_TODO();
						}
						//Y es una modificacion
						if (MFiliacion.stFiliacion == "MODIFICACION")
						{
							CERRAR_TODO();
						}
					}
				}
				else
				{
					//Si no ha encontrado ningun registro
					if (!GridCargado)
					{
						//Si proviene de una alta comprueba que los datos no
						//se repitan
						if (MFiliacion.stFiliacion == "ALTA")
						{
							if (Alta)
							{
								GridCargado = false;
								sprFiliados.MaxRows = 0;
								Alta_Paciente();
							}
						}
						//Si proviene de una seleccion
						if (MFiliacion.stFiliacion == "SELECCION")
						{
							CERRAR_TODO();
						}
						//Si proviene de una modificacion
						if (MFiliacion.stFiliacion == "MODIFICACION")
						{
						}
					}
				}
			}

           
			this.Cursor = Cursors.Default;
           
}


        private void cbbArchivo_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			KeyAscii = 0;
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		//(maplaza)(03/05/2006)Se realiza una validaci�n, pues puede darse el caso de que el usuario escriba
		//un DNI � un NIF en la caja de texto de "Otros documentos"
		//UPGRADE_NOTE: (7001) The following declaration (ComprobarBusqueda) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private bool ComprobarBusqueda()
		//{
				//
				//bool result = false;
				//result = true;
				//
				//if (rbBuscar[5].Checked)
				//{
					//if (tbOtrosDocumentos.Text.Trim() != "")
					//{
						//if (MFiliacion.EsDNI_NIF(tbOtrosDocumentos.Text.Trim(), 9))
						//{
							//result = false;
							//MessageBox.Show("Debe escribir el DNI/NIF en el campo indicado para ello.", Application.ProductName);
						//}
					//}
				//}
				//
				//return result;
		//}

		public void cbBuscar_Click(Object eventSender, EventArgs eventArgs)
		{

			//(maplaza)Se realiza una validaci�n previa a la b�squeda, pues puede darse el caso de que el usuario escriba
			//un DNI � un NIF en la caja de texto de "Otros documentos"
			//No parece que sea necesaria esta validaci�n, puesto que se trata del mismo campo en BBDD para "NIF/DNI" y "Otros documentos"
			//If Not ComprobarBusqueda Then Exit Sub
			string imensaje = String.Empty;
			if (Algoritmo != -1 && CodigoDeControl != -1 && tbNSS.Text != "" && TbVersion.Text != "")
			{
				if (!ComprobarControl(BinPan + tbNSS.Text, CodigoDeControl, Algoritmo, this.TbVersion.Text, this.tbsocieda.Text))
				{
					imensaje = Convert.ToString(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 3991, MFiliacion.GConexion, ""));
					if (imensaje == ((int) DialogResult.No).ToString())
					{
						Algoritmo = -1;
						CodigoDeControl = -1;
						tbNSS.Text = "";
						TbVersion.Text = "";
						return;
					}
				}
			}
			if (tbCorreoElectronico.Text.Trim() != "")
			{
				if ((tbCorreoElectronico.Text.Trim().IndexOf('@') + 1) == 0 || Strings.InStr((tbCorreoElectronico.Text.Trim().IndexOf('@') + 1) + 1, tbCorreoElectronico.Text.Trim(), ".", CompareMethod.Binary) == 0)
				{
                    imensaje = Convert.ToString(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1230, MFiliacion.GConexion, "correo", "los caracteres '.' y '@' en dicho correo"));
					tbCorreoElectronico.Focus();
					return;
				}
			}


			this.Cursor = Cursors.WaitCursor;
			cbAceptar.Enabled = false;
			sprFiliados.MaxRows = 0;
			P1 = true;
			CargarMas = false;
			RqSQL.Connection = MFiliacion.GConexion;

			if (MFiliacion.consultaCancelados)
			{
				Realizar_Busqueda_Cancelados();
			}
			else
			{
				Realizar_Busqueda();
			}

			Resultado_Busqueda();

			cbAceptar.Enabled = false;
			CBPacienteAnt.Enabled = true;
			bBusquedaConTarjeta = false;
            if (sprFiliados.Rows.Count != 0)
            {
                sprFiliados.CurrentRow = sprFiliados.Rows[0];
                fila_seleccionada = 0;
                botonIzquierdo = true;
            }
                
            sprFiliados_CellClick(sprFiliados, null);
            this.Cursor = Cursors.Default;
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			//Vaciar_datos
			this.Cursor = Cursors.WaitCursor;
			if (MFiliacion.RegresodeOpenlab != "")
			{
				if (MFiliacion.RegresodeOpenlab == "OpenLab")
				{
					MFiliacion.RegresodeOpenlab = "Cancelar";
				}
				this.Close();
			}
			else
			{
				if (MFiliacion.stFiliacion == "SELECCION" && MFiliacion.stModulo != "URGENCIAS")
				{
					if (MFiliacion.stModulo != "CITARAPIDA")
					{
						Vaciar_datos();
					}
				}
				if (MFiliacion.stFiliacion == "MODIFICACION" && !MFiliacion.paso)
				{
					Vaciar_datos();
				}
				if (MFiliacion.stFiliacion == "ALTA" && !MFiliacion.paso)
				{
					Vaciar_datos();
				}
				this.Close();
				if (MFiliacion.stModulo == "CITARAPIDA")
				{
					return;
				}
			}
			this.Cursor = Cursors.Default;
		}

		private void cbNuevo_Click(Object eventSender, EventArgs eventArgs)
		{

			cbNuevo.Enabled = false;
			if (mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu1", MFiliacion.GConexion) == "S" && mbAcceso.ObtenerCodigoSConsglo("SEXINDET", "valfanu2", MFiliacion.GConexion) == "S" && MFiliacion.stModulo == "CITARAPIDA" && !rbSexo[0].IsChecked && !rbSexo[1].IsChecked)
			{
				MFiliacion.clasemensaje.RespuestaMensaje("Filiaci�n", "", 1040, MFiliacion.GConexion, "tipo sexo");
				return;
			}
			sprFiliados.MaxRows = 0;
			Alta = true;
			MFiliacion.stFiliacion = "ALTA";
			Alta_Paciente();

		}

		private void CBPacienteAnt_Click(Object eventSender, EventArgs eventArgs)
		{
			string tstDatos = String.Empty;
			string usuarioconectado = String.Empty;



			int tlIdConexion = Serrores.ConexionTS();
			//obtengo UsuarioNT
			string tstUsuNT = Serrores.obtener_usuario();
			//obtengo nombre maquina

			Conexion.Obtener_conexion tInstancia = new Conexion.Obtener_conexion();
			string tstMAquina = tInstancia.NonMaquina();
			tInstancia = null;

			string GidenpacAnt = "";

            if (MFiliacion.GConexion.getConnect().IndexOf("APP=") >= 0)
			{
				usuarioconectado = MFiliacion.GConexion.getConnect().Substring(Strings.InStr(MFiliacion.GConexion.getConnect().IndexOf("APP=") + 1, MFiliacion.GConexion.getConnect(), "\\", CompareMethod.Binary), Math.Min(Strings.InStr(MFiliacion.GConexion.getConnect().IndexOf("APP=") + 1, MFiliacion.GConexion.getConnect(), ";", CompareMethod.Binary) - Strings.InStr(MFiliacion.GConexion.getConnect().IndexOf("APP=") + 1, MFiliacion.GConexion.getConnect(), "\\", CompareMethod.Binary) - 1, MFiliacion.GConexion.getConnect().Length - Strings.InStr(MFiliacion.GConexion.getConnect().IndexOf("APP=") + 1, MFiliacion.GConexion.getConnect(), "\\", CompareMethod.Binary)));
				if (tstMAquina == "")
				{
					tstMAquina = usuarioconectado;
				}
			}
			if (MFiliacion.stModulo == "IFMS")
			{
				//tlIdConexion = 0
				tstDatos = "select * from SSESUSUA where idconexi=" + tlIdConexion.ToString() + " and dusuarnt='" + MFiliacion.tstUsuNT_IFMS + "' " + " and puestopc='" + MFiliacion.tstMAquina_IFMS + "'" + " and gusuario = '" + MFiliacion.usuario_IFMS + "'";
			}
			else
			{
				//buscamos el ultimo paciente seleccionado por el usuario
				tstDatos = "select * from SSESUSUA where idconexi=" + tlIdConexion.ToString() + " and dusuarnt='" + tstUsuNT + "' " + " and puestopc='" + tstMAquina + "'" + " and gusuario = '" + Serrores.VVstUsuarioApli + "'";
			}
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstDatos, MFiliacion.GConexion);
			DataSet tRrDatos = new DataSet();
            Int32 IndicetRrDatos = 0;
            tempAdapter.Fill(tRrDatos);
            IndicetRrDatos = 0;
            if (tRrDatos.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(tRrDatos.Tables[0].Rows[IndicetRrDatos]["gidenpac"]))
				{
					GidenpacAnt = Convert.ToString(tRrDatos.Tables[0].Rows[IndicetRrDatos]["gidenpac"]);
				}
			}
			tRrDatos.Close();
            
			//cargamos en pantalla los datos del paciente
			if (GidenpacAnt != "")
			{
				LIMPIAR_FORMULARIO();
				sprFiliados.MaxRows = 0;
				CargarMas = false;

				RqSQL.Connection = MFiliacion.GConexion;
				tstDatos = "Select * from DPACIENT where gidenpac = '" + GidenpacAnt + "'";
				if (MFiliacion.abierto)
				{
					MFiliacion.RrSQLUltimo.Close();
					MFiliacion.abierto = false;
				}

				RqSQL.CommandText = tstDatos;

				Resultado_Busqueda();
				fila_seleccionada = 0;
				sprFiliados_CellClick(sprFiliados, null);
				CBPacienteAnt.Enabled = false;
			}

		}

		private void cmdOpen_Click(Object eventSender, EventArgs eventArgs)
		{
            UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI Mouse = new UpgradeSupportHelper.PInvoke.UnsafeNative.Structures.POINTAPI();
         	int i = 0;

			sprFiliados.Col = 11;

			string Paciente = sprFiliados.Text;

            UpgradeSupportHelper.PInvoke.SafeNative.user32.GetCursorPos(ref Mouse);
			ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
         
			string Episodio = "";

			string tempRefParam = (Serrores.VVstUsuarioApli.Trim() == "") ? Convert.ToString(DBNull.Value) : Serrores.VVstUsuarioApli.Trim();
			string tempRefParam2 = this.Name;
			string tempRefParam3 = "OpenImdh";
			bool tempRefParam4 = false;
			IMDHOPEN.LlamadaSP(MFiliacion.GConexion, tempRefParam, tempRefParam2, tempRefParam3, Paciente, Episodio, tempRefParam4, Mouse.X, Mouse.Y);
			IMDHOPEN = null;
		}

		private void chbFiliacPov_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			if (chbFiliacPov.CheckState == CheckState.Checked)
			{ //SI ES FILIACION PROVISIONAL
				cbBuscar.Enabled = true;
				cbAceptar.Enabled = false;
				cbNuevo.Enabled = true;
			}
			else
			{
				Activar_buscar();
				cbAceptar.Enabled = false;
				cbNuevo.Enabled = false;
			}
		}


		//*******************************************************************************************
		//* O.Frias (31/07/2007) - Incorporo la operativa para la solicitud de usuario para cita WEB
		//*******************************************************************************************
		private void DFI120F1_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

                if (!mbAcceso.AplicarControlAcceso(Serrores.VVstUsuarioApli, MFiliacion.LETRA_MODULO, this, this.Name, ref MFiliacion.astrControles, ref MFiliacion.astrEventos, MFiliacion.GConexion))
                {
                    return;
                }

                Application.DoEvents();
				int N = Application.OpenForms.Count;
				if (MFiliacion.stFiliacion != "SELECCION")
				{
					cbbArchivo.Enabled = false;
					sprFiliados.MaxRows = 0;
					Activar_buscar();
					Desbloquear_cajas();
					cbAceptar.Enabled = false;
					tbApellido1.Focus();
				}
				if (MFiliacion.idPaciente == "")
				{
					cbAceptar.Enabled = false;
					cbBuscar.Enabled = false;
				}
				if (MFiliacion.RegresodeOpenlab != "" && MFiliacion.RegresodeOpenlab != "Cancelar")
				{
					Activar_buscar();
					if (MFiliacion.RegresodeOpenlab != "ConDatos" && cbBuscar.Enabled)
					{
						cbBuscar_Click(cbBuscar, new EventArgs());
					}
				}
				else
				{
					if (!MFiliacion.CodPac && sprFiliados.CurrentRow == null ) 
					{
						Limpiar_Cajas();
						sprFiliados.MaxRows = 0;
						tbApellido1.Focus();
					}
				}

				//OSCAR C FEB 2006
				//Cuando se intenta registrar un paciente en el SIA procedente de RULEQ,
				//se cargan los datos por pantalla para poder buscar pacientes similares o
				//filiar a un nuevo paciente....
				if (MFiliacion.bProcedentedeRULEQ)
				{
					proRellenaDatosRULEQ();
				}
				//----------------

				//************* O.Frias - 31/07/2007 *************
				if (MFiliacion.blnUsuWeb)
				{
					proRellenaDatosUsuWeb();
				}

                ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
                if (MFiliacion.blnIMDHOPEN)
                {
                    //O.Frias - 24/02/2009
                    //Se activa la opci�n de tecla de funcion
                    IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();

                    TECLAFUNCION = Convert.ToInt32(IMDHOPEN.TeclaFuncionOpen(MFiliacion.GConexion));
                    MFiliacion.FormularioAnterior = Convert.ToString(IMDHOPEN.LeerUltimoForm((Serrores.VVstUsuarioApli.Trim() == "") ? null : Serrores.VVstUsuarioApli.Trim()));
                    IMDHOPEN.EstableceUltimoForm(this.Name, (Serrores.VVstUsuarioApli.Trim() == "") ? null : Serrores.VVstUsuarioApli.Trim());
                    cmdOpen.Visible = Convert.ToBoolean(IMDHOPEN.TeclaFuncionBotonVisible(MFiliacion.GConexion));
                    if (cmdOpen.Visible)
                    {
                        cmdOpen.Enabled = MFiliacion.ModCodigo.Trim() != "";
                    }
                    IMDHOPEN = null;
                    TECLAFUNCION = Convert.ToInt32(Double.Parse(mbAcceso.ObtenerCodigoSConsglo("IMDHOPEN", "nnumeri1", MFiliacion.GConexion)));
                }

                if (bCIPEnviado)
				{
					bCIPEnviado = false;
					if (tbCIPAutonomico.Text.Trim() != "")
					{
						rbBuscar[6].IsChecked = true;
					}
				}
			}
		}

		private void DFI120F1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);

			if (KeyAscii == 13 && this.cbBuscar.Enabled && this.ActiveControl.Name != "tbTarjeta")
			{
				cbBuscar_Click(cbBuscar, new EventArgs());
				KeyAscii = 0;
			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void DFI120F1_Load(Object eventSender, EventArgs eventArgs)
		{

            activarDoblrClickRadioButton();
			bCIPEnviado = false;
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_FILIACION);
            bcbaceptar = false;
			MFiliacion.ObtenerValoresTipoBD();
			MFiliacion.FechaHoraSistema();
			MFiliacion.Establecer_Propiedades_DateCombo(sdcFechaNac);
			MFiliacion.ObtenerValoresTipoBD();
			//RegresodeOpenlab = ""
			//FORMATO DE FILIACION
			SqlDataAdapter tempAdapter = new SqlDataAdapter("Select valfanu1 from sConsGlo where gConsGlo = 'FORMFILI'", MFiliacion.GConexion);
			DataSet Rrglobal = new DataSet();
			tempAdapter.Fill(Rrglobal);
			MFiliacion.stFORMFILI = Convert.ToString(Rrglobal.Tables[0].Rows[0]["valfanu1"]).Trim();
			Rrglobal.Close();

			// CARACTERES DE COMIENZO DE BANDA MAGN�TICA DE TARJETAS

			stComienzoBanda = "^B"; // Valor por defecto

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter("Select isNull(valfanu1, '^B') valfanu1, isNull(nnumeri1, 2) nnumeri1 from sConsGlo where gConsGlo = 'TIPOTARJ'", MFiliacion.GConexion);
			DataSet RrBanda = new DataSet();
			tempAdapter_2.Fill(RrBanda);
			if (RrBanda.Tables[0].Rows.Count != 0)
			{
				stComienzoBanda = Convert.ToString(RrBanda.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
				iNumeroBandas = Convert.ToInt32(RrBanda.Tables[0].Rows[0]["nnumeri1"]);
			}
			
			RrBanda.Close();
            //PONER DNI � CEDULA

			string stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'idenpers' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
			DataSet RrTemp = new DataSet();
			tempAdapter_3.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				MFiliacion.DNICEDULA = Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim();
			}
			RrTemp.Close();

			rbBuscar[1].Text = "NIF/DNI:";
			sprFiliados.Row = 0;
			sprFiliados.Col = 5;
			sprFiliados.Text = "D.N.I./N.I.F.";

			string SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SEGURSOC'";
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
			DataSet RrSQLENTIDAD = new DataSet();
			tempAdapter_4.Fill(RrSQLENTIDAD);
			//codigo de la SEGURIDAD SOCIAL
			MFiliacion.CodSS = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
			//texto de la seguridad social
			MFiliacion.TexSS = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
			SQLENTIDAD = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'PRIVADO'";
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_5.Fill(RrSQLENTIDAD);
			//codigo de regimen economco privado
			MFiliacion.CodPriv = Convert.ToInt32(RrSQLENTIDAD.Tables[0].Rows[0]["NNUMERI1"]);
			//texto de regimen economco privado
			MFiliacion.TexPriv = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["VALFANU1"]).Trim();
			//Nombre de la aplicacion
			SQLENTIDAD = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO = 'NOMAPLIC'";
			SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_6.Fill(RrSQLENTIDAD);
			MFiliacion.NombreApli = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["valfanu1"]).Trim();

			//Maximo n�mero de filas a traer
			SQLENTIDAD = "SELECT nnumeri1 FROM SCONSGLO WHERE GCONSGLO = 'MAXFILAS'";
			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_7.Fill(RrSQLENTIDAD);
			NRegistros = Convert.ToInt32(Double.Parse(Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["nnumeri1"]).Trim()));

			//cALCULO DE LA LETRA DEL DNI
			SQLENTIDAD = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO = 'CALDNIAU'";
			SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_8.Fill(RrSQLENTIDAD);
			if (RrSQLENTIDAD.Tables[0].Rows.Count == 0)
			{
				MFiliacion.bCalcLetra = false;
			}
			else
			{
				MFiliacion.bCalcLetra = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["valfanu1"]).Trim() == "S";
			}
			RrSQLENTIDAD.Close();

			string SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SHOMBRE'";
			SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(SQLSEXO, MFiliacion.GConexion);
			DataSet RrSEXO = new DataSet();
			tempAdapter_9.Fill(RrSEXO);
			MFiliacion.CodSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
			MFiliacion.TexSHombre = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

			SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SMUJER'";
			SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(SQLSEXO, MFiliacion.GConexion);
			RrSEXO = new DataSet();
			tempAdapter_10.Fill(RrSEXO);
			MFiliacion.CodSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
			MFiliacion.TexSMUJER = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();

			SQLSEXO = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SINDET'";
			SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(SQLSEXO, MFiliacion.GConexion);
			RrSEXO = new DataSet();
			tempAdapter_11.Fill(RrSEXO);
			MFiliacion.CodSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU1"]).Trim();
			MFiliacion.TexSINDET = Convert.ToString(RrSEXO.Tables[0].Rows[0]["VALFANU2"]).Trim();
			RrSEXO.Close();

			string SQLPROV = "SELECT * FROM SCONSGLO WHERE GCONSGLO ='CODPROVI'";
			SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(SQLPROV, MFiliacion.GConexion);
			DataSet RrSPROV = new DataSet();
			tempAdapter_12.Fill(RrSPROV);
			MFiliacion.CodPROV = Convert.ToString(RrSPROV.Tables[0].Rows[0]["valfanu1"]).Trim();
			SqlDataAdapter tempAdapter_13 = new SqlDataAdapter("select * from hserarchva", MFiliacion.GConexion);
			DataSet RrArchivo = new DataSet();
			tempAdapter_13.Fill(RrArchivo);
            Int32 indiceCombo = 0;
			foreach (DataRow iteration_row in RrArchivo.Tables[0].Rows)
			{
                cbbArchivo.Items.Add(new RadListDataItem(Convert.ToString(iteration_row["dserarch"]).Trim(), indiceCombo));

                if (Convert.ToString(iteration_row["icentral"]) == "S")
				{
					MFiliacion.ArchivoCentral = Convert.ToInt32(iteration_row["gserarch"]);
					cbbArchivo.SelectedIndex = RrArchivo.Tables[0].Rows.Count-1;
                    Archivo = cbbArchivo.Items.Count;
				}
                indiceCombo++;
			}			
			RrArchivo.Close();
			SQLENTIDAD = "SELECT valfanu1 FROM SCONSGLO WHERE GCONSGLO = 'BPACIENT'";
			SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_14.Fill(RrSQLENTIDAD);
			//codigo de regimen economco privado
			if (RrSQLENTIDAD.Tables[0].Rows.Count == 0)
			{
				MFiliacion.BDPACIENT = "C";
			}
			else
			{	
				MFiliacion.BDPACIENT = Convert.ToString(RrSQLENTIDAD.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper();
			}
			RrSQLENTIDAD.Close();
            
			SQLENTIDAD = "SELECT count(*) FROM SCONSGLO WHERE GCONSGLO = 'HISREUTI' and valfanu1='S'";
			SqlDataAdapter tempAdapter_15 = new SqlDataAdapter(SQLENTIDAD, MFiliacion.GConexion);
			RrSQLENTIDAD = new DataSet();
			tempAdapter_15.Fill(RrSQLENTIDAD);
			//control de b�squeda de historia activa.
			bBuscarHistoricoDeHistoria = (Convert.ToDouble(RrSQLENTIDAD.Tables[0].Rows[0][0]) == 1);
			RrSQLENTIDAD.Close();


			string LitPersona = Serrores.ObtenerLiteralPersona(MFiliacion.GConexion);
			LitPersona = LitPersona.ToLower();

			this.Text = "Selecci�n de " + LitPersona + " - DFI120F1";
			CBPacienteAnt.Text = "�ltimo " + LitPersona; //& " - DFI120F1"

			// Si estamos consultando pacientes cancelados, ocultamos el bot�n de "�ltimo Paciente"

			CBPacienteAnt.Visible = !MFiliacion.consultaCancelados;

			bFormatoCodigoPos = true;
			stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'DIFEECAS' ";
			SqlDataAdapter tempAdapter_16 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
			RrTemp = new DataSet();
			tempAdapter_16.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				bFormatoCodigoPos = (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]) + "").Trim().ToUpper() != "S";
			}
			RrTemp.Close();

			//Oscar C Octubre 2011
			bCipAutonomicoManual = false;
			stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'CIPAUTMA' ";
			SqlDataAdapter tempAdapter_17 = new SqlDataAdapter(stsqltemp, MFiliacion.GConexion);
			RrTemp = new DataSet();
			tempAdapter_17.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				if (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() == "S")
				{
					bCipAutonomicoManual = true;
				}
			}
			RrTemp.Close();
			cmdOpen.Enabled = false;
			cmdOpen.Visible = MFiliacion.blnIMDHOPEN;

            Line1.X1 = 84;
			Line1.X2 = 156;
            
			if (bCipAutonomicoManual)
			{
				tbCIPAutonomico.MaxLength = 16;
			}
			else
			{
				tbCIPAutonomico.MaxLength = 10;
			}

			bRespuestaSexo = false;

			//Oscar C Marzo 2012. se parametriza la longitud del telefono
			var lngTelefono = Conversion.Val(Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "LNGTELEF", "NNUMERI1"));
			tbDPTelf1.MaxLength =  Convert.ToInt32(Math.Min(15, lngTelefono));
		}

		private void DFI120F1_Closed(Object eventSender, EventArgs eventArgs)
		{
			if (!bcbaceptar)
			{
				Vaciar_datos();
			}
		}

		public void CIP_Enviado()
		{
			bCIPEnviado = true;
		}

		private void MEBDNI_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

        private void sprFiliados_MouseDown(Object eventSender, MouseEventArgs eventArgs)
        {
            if (eventArgs.Button == MouseButtons.Left)
                botonIzquierdo = true;
            else
                botonIzquierdo = false;
        }

        private void sprFiliados_MouseUp(Object eventSender, MouseEventArgs eventArgs)
		{
            if (eventArgs.Button == MouseButtons.Right)
			{
				if (fila_seleccionada > 0)
				{
					if (fila_seleccionada == sprFiliados.Row)
					{
						cmdOpen_Click(cmdOpen, new EventArgs());
					}
				}
			}
		}
        
		private void tbCIPAutonomico_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbCIPAutonomico_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbCIPAutonomico.SelectionStart = 0;
			tbCIPAutonomico.SelectionLength = tbCIPAutonomico.Text.Trim().Length;
		}

		private void tbCIPAutonomico_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 8)
			{
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 9)
			{
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (bCipAutonomicoManual)
			{
				if ((KeyAscii < 48 || KeyAscii > 57) && (KeyAscii < 65 || KeyAscii > 90) && (KeyAscii < 97 || KeyAscii > 122) && KeyAscii != 241 && KeyAscii != 209)
				{
					KeyAscii = 0;
				}
			}
			else
			{
				double dbNumericTemp = 0;
				if (!Double.TryParse(Strings.Chr(KeyAscii).ToString(), NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp))
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbCIPAutonomico_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbCorreoElectronico_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbOtrosDocumentos_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void MEBDNI_Enter(Object eventSender, EventArgs eventArgs)
		{
			MEBDNI.SelectionStart = 0;
			MEBDNI.SelectionLength = MEBDNI.Text.Trim().Length;
		}

		private void tbOtrosDocumentos_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbOtrosDocumentos.SelectionStart = 0;
			tbOtrosDocumentos.SelectionLength = tbOtrosDocumentos.Text.Trim().Length;
		}

		private void MEBDNI_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			bool paso = false;
			if ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123))
			{
				if (MEBDNI.Text.Trim().Length == 0)
				{
					paso = true;
				}
				else
				{
					paso = Conversion.Val(MEBDNI.Text) == 0;
				}
			}
			int Tama�o = 0;
			int ceros = 0;
			string resto = String.Empty;
			StringBuilder resto2 = new StringBuilder();
			if (!paso)
			{
				if (KeyAscii != 8 && ((KeyAscii > 64 && KeyAscii < 91) || (KeyAscii > 96 && KeyAscii < 123)))
				{
					if (KeyAscii < 48 || KeyAscii > 57)
					{
						if (MEBDNI.Text.Trim().Length == 9)
						{
							MEBDNI.Text = MEBDNI.Text.Substring(0, Math.Min(8, MEBDNI.Text.Length)) + Strings.Chr(KeyAscii).ToString().ToUpper();
						}
						Tama�o = MEBDNI.Text.Trim().Length;
						resto = MEBDNI.Text.Trim();
						ceros = 8 - Tama�o;
						MEBDNI.Text = "         ";
						for (int i = 1; i <= ceros; i++)
						{
							resto2.Append("0");
						}
						resto2.Append(resto);
						if (resto2.ToString().Trim().Length < 9)
						{
							resto2.Append(Strings.Chr(KeyAscii).ToString().ToUpper());
						}
						MEBDNI.Text = "         ";
						MEBDNI.Text = resto2.ToString();
					}
				}
				MEBDNI.Focus();
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void MEBDNI_Leave(Object eventSender, EventArgs eventArgs)
		{
			int Tama�o = 0;
			int ceros = 0;
			string resto = String.Empty;
			StringBuilder resto2 = new StringBuilder();
			if (MEBDNI.Text.Trim().Length < 8 && MEBDNI.Text.Trim().Length != 0)
			{
				Tama�o = MEBDNI.Text.Trim().Length;
				resto = MEBDNI.Text.Trim();
				ceros = 8 - Tama�o;
				for (int i = 1; i <= ceros; i++)
				{
					resto2.Append("0");
				}
				MEBDNI.Text = resto2.ToString() + resto + " ";
			}
			MEBDNI.Text = MEBDNI.Text.ToUpper();

		}

		private bool isInitializingComponent;
        private void rbBuscar_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
				int Index = Array.IndexOf(this.rbBuscar, eventSender);
                //Habilita la caja seleccionada para buscar y deshabilita las
                //restantes
                if (flagDbClick)
                {
                    rbBuscar[Index].IsChecked = false;                    
                    flagDbClick = false;
                }
                else
                {
                    switch (Index)
                    {
                        case 0:  //Opci�n N� de Historia Clinica 
                            this.tbsocieda.Text = "";
                            LbSociedad.Text = "";
                            tbNHistoria.Enabled = true;
                            cbbArchivo.Enabled = true;
                            MEBDNI.Enabled = false;
                            MEBDNI.Text = "         ";
                            tbDPTelf1.Enabled = false;
                            tbDPTelf1.Text = "";
                            tbNSS.Enabled = false;
                            tbNSS.Text = "";
                            TbVersion.Enabled = false;
                            TbVersion.Text = "";
                            CodigoDeControl = -1;
                            Algoritmo = -1;
                            tbTarjeta.Enabled = false;
                            tbTarjeta.Text = "";
                            tbOtrosDocumentos.Enabled = false;
                            tbOtrosDocumentos.Text = "";
                            tbCIPAutonomico.Enabled = false;
                            tbCIPAutonomico.Text = "";
                            tbCorreoElectronico.Enabled = false;
                            tbCorreoElectronico.Text = "";
                            tbNHistoria.Focus();
                            break;
                        case 1:  //Opcion de DNI 
                            this.tbsocieda.Text = "";
                            LbSociedad.Text = "";
                            tbNHistoria.Enabled = false;
                            tbNHistoria.Text = "";
                            cbbArchivo.Enabled = false;
                            MEBDNI.Enabled = true;
                            tbNSS.Enabled = false;
                            tbNSS.Text = "";
                            TbVersion.Enabled = false;
                            TbVersion.Text = "";
                            CodigoDeControl = -1;
                            Algoritmo = -1;
                            tbDPTelf1.Enabled = false;
                            tbDPTelf1.Text = "";
                            tbTarjeta.Enabled = false;
                            tbTarjeta.Text = "";
                            tbOtrosDocumentos.Enabled = false;
                            tbOtrosDocumentos.Text = "";
                            tbCIPAutonomico.Enabled = false;
                            tbCIPAutonomico.Text = "";
                            tbCorreoElectronico.Enabled = false;
                            tbCorreoElectronico.Text = "";
                            if (MEBDNI.Enabled)
                            {
                                MEBDNI.Focus();
                            }
                            break;
                        case 2:  //Opcion de NSS 
                            this.tbsocieda.Text = "";
                            LbSociedad.Text = "";
                            tbNHistoria.Enabled = false;
                            tbNHistoria.Text = "";
                            cbbArchivo.Enabled = false;
                            MEBDNI.Enabled = false;
                            MEBDNI.Text = "         ";
                            tbDPTelf1.Enabled = false;
                            tbDPTelf1.Text = "";
                            tbNSS.Enabled = true;
                            TbVersion.Enabled = true;
                            tbTarjeta.Enabled = false;
                            tbTarjeta.Text = "";
                            CodigoDeControl = -1;
                            Algoritmo = -1;
                            tbOtrosDocumentos.Enabled = false;
                            tbOtrosDocumentos.Text = "";
                            tbCIPAutonomico.Enabled = false;
                            tbCIPAutonomico.Text = "";
                            tbCorreoElectronico.Enabled = false;
                            tbCorreoElectronico.Text = "";
                            tbNSS.Focus();
                            break;
                        case 3:
                            this.tbsocieda.Text = "";
                            LbSociedad.Text = "";
                            tbNHistoria.Enabled = false;
                            tbNHistoria.Text = "";
                            cbbArchivo.Enabled = false;
                            MEBDNI.Enabled = false;
                            MEBDNI.Text = "         ";
                            tbNSS.Enabled = false;
                            tbNSS.Text = "";
                            TbVersion.Enabled = false;
                            TbVersion.Text = "";
                            CodigoDeControl = -1;
                            Algoritmo = -1;
                            tbDPTelf1.Enabled = true;
                            tbTarjeta.Enabled = false;
                            tbTarjeta.Text = "";
                            tbOtrosDocumentos.Enabled = false;
                            tbOtrosDocumentos.Text = "";
                            tbCIPAutonomico.Enabled = false;
                            tbCIPAutonomico.Text = "";
                            tbCorreoElectronico.Enabled = false;
                            tbCorreoElectronico.Text = "";
                            tbDPTelf1.Focus();
                            break;
                        case 4:
                            this.tbsocieda.Text = "";
                            LbSociedad.Text = "";
                            tbNHistoria.Enabled = false;
                            tbNHistoria.Text = "";
                            cbbArchivo.Enabled = false;
                            MEBDNI.Enabled = false;
                            MEBDNI.Text = "         ";
                            tbNSS.Enabled = false;
                            tbNSS.Text = "";
                            TbVersion.Enabled = false;
                            TbVersion.Text = "";
                            CodigoDeControl = -1;
                            Algoritmo = -1;
                            tbDPTelf1.Enabled = false;
                            tbDPTelf1.Text = "";
                            tbTarjeta.Enabled = true;
                            tbTarjeta.Text = "";
                            tbOtrosDocumentos.Enabled = false;
                            tbOtrosDocumentos.Text = "";
                            tbCIPAutonomico.Enabled = false;
                            tbCIPAutonomico.Text = "";
                            tbCorreoElectronico.Enabled = false;
                            tbCorreoElectronico.Text = "";
                            tbTarjeta.Focus();
                            break;
                        case 5:  //(maplaza)(03/05/2006) Opci�n de "Otros documentos" 
                            this.tbsocieda.Text = "";
                            LbSociedad.Text = "";
                            tbNHistoria.Enabled = false;
                            tbNHistoria.Text = "";
                            cbbArchivo.Enabled = false;
                            MEBDNI.Enabled = false;
                            MEBDNI.Text = "         ";
                            tbNSS.Enabled = false;
                            tbNSS.Text = "";
                            TbVersion.Enabled = false;
                            TbVersion.Text = "";
                            CodigoDeControl = -1;
                            Algoritmo = -1;
                            tbDPTelf1.Enabled = false;
                            tbDPTelf1.Text = "";
                            tbTarjeta.Enabled = false;
                            tbTarjeta.Text = "";
                            tbCIPAutonomico.Enabled = false;
                            tbCIPAutonomico.Text = "";
                            tbOtrosDocumentos.Enabled = true;
                            tbCorreoElectronico.Enabled = false;
                            tbCorreoElectronico.Text = "";
                            if (tbOtrosDocumentos.Enabled)
                            {
                                tbOtrosDocumentos.Focus();
                            }
                            break;
                        case 6:  //Oscar C Octubre 2011 - Busqueda por CIP AUTONOMICO 
                            this.tbsocieda.Text = "";
                            LbSociedad.Text = "";
                            tbNHistoria.Enabled = false;
                            tbNHistoria.Text = "";
                            cbbArchivo.Enabled = false;
                            MEBDNI.Enabled = false;
                            MEBDNI.Text = "         ";
                            tbNSS.Enabled = false;
                            tbNSS.Text = "";
                            TbVersion.Enabled = false;
                            TbVersion.Text = "";
                            CodigoDeControl = -1;
                            Algoritmo = -1;
                            tbDPTelf1.Enabled = false;
                            tbDPTelf1.Text = "";
                            tbTarjeta.Enabled = false;
                            tbTarjeta.Text = "";
                            tbOtrosDocumentos.Enabled = false;
                            tbOtrosDocumentos.Text = "";
                            tbCIPAutonomico.Enabled = true;
                            tbCIPAutonomico.Focus();
                            tbCorreoElectronico.Enabled = false;
                            tbCorreoElectronico.Text = "";
                            break;
                        case 7:  //Rub�n Septiembre 2014 - Busqueda por CORREO ELECTR�NICO 
                            this.tbsocieda.Text = "";
                            LbSociedad.Text = "";
                            tbNHistoria.Enabled = false;
                            tbNHistoria.Text = "";
                            cbbArchivo.Enabled = false;
                            MEBDNI.Enabled = false;
                            MEBDNI.Text = "         ";
                            tbNSS.Enabled = false;
                            tbNSS.Text = "";
                            TbVersion.Enabled = false;
                            TbVersion.Text = "";
                            CodigoDeControl = -1;
                            Algoritmo = -1;
                            tbDPTelf1.Enabled = false;
                            tbDPTelf1.Text = "";
                            tbTarjeta.Enabled = false;
                            tbTarjeta.Text = "";
                            tbOtrosDocumentos.Enabled = false;
                            tbOtrosDocumentos.Text = "";
                            tbCIPAutonomico.Enabled = false;
                            tbCIPAutonomico.Text = "";
                            tbCorreoElectronico.Enabled = true;
                            tbCorreoElectronico.Focus();
                            break;
                    }
                }
			}
		}

		private void rbBuscar_DblClick(object sender, System.EventArgs e)
        {
            int Index = Array.IndexOf(this.rbBuscar, sender);
            //Desselecciona la opcion seleccionada, deshabilitando la caja
            //y limpiando su contenido
            switch (Index)
			{
				case 0 :  //Opci�n N� de Historia Clinica 
					rbBuscar[0].IsChecked = false;
                    //_rbBuscar_0.IsChecked = false;
                    tbNHistoria.Enabled = false; 
					tbNHistoria.Text = ""; 
					cbbArchivo.Enabled = false; 
					Activar_buscar(); 
					break;
				case 1 :  //Opcion de DNI 
					rbBuscar[1].IsChecked = false;
                    //_rbBuscar_1.IsChecked = false;
                    MEBDNI.Enabled = false; 
					MEBDNI.Text = "         "; 
					break;
				case 2 :  //Opcion de NSS 
					rbBuscar[2].IsChecked = false;
                    //_rbBuscar_2.IsChecked = false;
                    break;
				case 3 : 
					rbBuscar[3].IsChecked = false;
                    //_rbBuscar_3.IsChecked = false;
                    tbDPTelf1.Enabled = false; 
					tbDPTelf1.Text = ""; 
					break;
				case 4 : 
					rbBuscar[4].IsChecked = false;
                    //_rbBuscar_4.IsChecked = false;
                    tbTarjeta.Enabled = false; 
					tbTarjeta.Text = ""; 
					this.tbsocieda.Text = ""; 
					LbSociedad.Text = ""; 
					break;
				case 5 :  //(maplaza)(03/05/2006) Opci�n de "Otros documentos" 
					rbBuscar[5].IsChecked = false;
                    //_rbBuscar_5.IsChecked = false;
                    tbOtrosDocumentos.Enabled = false; 
					tbOtrosDocumentos.Text = ""; 
					break;
				case 6 :  //Oscar C Octubre 2011 - Busqueda por CIP AUTONOMICO 
					rbBuscar[6].IsChecked = false;
                    //_rbBuscar_6.IsChecked = false;
                    tbCIPAutonomico.Enabled = false; 
					tbCIPAutonomico.Text = ""; 
					break;
				case 7 :  //Rub�n Septiembre 2014 - Busqueda por CORREO ELECTR�NICO 
					rbBuscar[7].IsChecked = false;
                    //_rbBuscar_7.IsChecked = false;
                    tbCorreoElectronico.Enabled = false; 
					tbCorreoElectronico.Text = ""; 
					break;
			}
            flagDbClick = true;
        }

		//Aqui recibe los datos enviados por el formulario origen
		//a traves del modulo de clase
		public void RecibirDatos(ref string stFiliacion, ref string stModulo)
		{
			stFiliacion = stFiliacion.ToUpper();
			stModulo = stModulo.ToUpper();
			if (stModulo == "URGENCIAS")
			{
				chbFiliacPov.Visible = true;
			}
			MFiliacion.NuevoDFI120F1.Text = MFiliacion.NuevoDFI120F1.Text + "  [" + stFiliacion + "]  " + "  [" + stModulo + "]  ";
		}

		private void rbSexo_CheckedChanged(Object eventSender, EventArgs eventArgs)
		{
			if (((RadRadioButton) eventSender).IsChecked)
			{
				if (isInitializingComponent)
				{
					return;
				}
                int Index = Array.IndexOf(this.rbSexo, eventSender);
                if (flagDbClick)
                {
                    rbSexo[Index].IsChecked = false;                    
                    flagDbClick = false;
                }
                else
                {
                    Activar_buscar();
                }
			}
		}
        private void rbSexo_DblClick(object sender, System.EventArgs e)
        {
            int Index = Array.IndexOf(this.rbSexo, sender);
            //Desselecciona la opcion seleccionada
            switch (Index)
			{
				case 0 :  //Opci�n Hombre 
					rbSexo[0].IsChecked = false;                    
                    Activar_buscar(); 
					break;
				case 1 :  //Opcion Mujer 
					rbSexo[1].IsChecked = false;                    
                    Activar_buscar(); 
					break;
			}
            flagDbClick = true;
		}

        private void sdcFechaNac_Closed(object sender, RadPopupClosedEventArgs args)
		{
            Activar_buscar();
		}


		private void Identificador_Repetido()
		{
			string UDIGITO = String.Empty;
			int cont = 0;
			int cont2 = 1;
			SQLRepetidos = "Select GIDENPAC FROM DPACIENT WHERE GIDENPAC LIKE  '" + MFiliacion.codigo + "%' ORDER BY GIDENPAC";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLRepetidos, MFiliacion.GConexion);
			RrSQLRepetidos = new DataSet();
			tempAdapter.Fill(RrSQLRepetidos);
            IndiceRrSQLRepetidos = 0;
			//Controla si existen registros con los parametros seleccionados
			if (RrSQLRepetidos.Tables[0].Rows.Count == 0)
			{
				MFiliacion.codigo = MFiliacion.codigo + "0";
			}
			else
			{
				cont = 0;
				foreach (DataRow iteration_row in RrSQLRepetidos.Tables[0].Rows)
				{
					UDIGITO = Convert.ToString(iteration_row["GIDENPAC"]).Substring(12);
					if (String.CompareOrdinal(UDIGITO, "0") >= 0 && String.CompareOrdinal(UDIGITO, "9") <= 0)
					{
						if (StringsHelper.ToDoubleSafe(UDIGITO) != cont)
						{
							MFiliacion.codigo = MFiliacion.codigo + cont.ToString();
							return;
						}
						else
						{
							cont++;
						}
					}
					else
					{
						//si encuentra mas de 10 pacientes con digito
						//se busca los de las letras
						if (UDIGITO != Strings.Chr(64 + cont2).ToString())
						{
							MFiliacion.codigo = MFiliacion.codigo + Strings.Chr(64 + cont2).ToString();
							return;
						}
						else
						{
							cont2++;
						}
					}
				}
				if (cont != 10)
				{
					MFiliacion.codigo = MFiliacion.codigo + cont.ToString();
				}
				if (cont == 10)
				{
					MFiliacion.codigo = MFiliacion.codigo + Strings.Chr(64 + cont2).ToString();
				}
			}
			RrSQLRepetidos.Close();
		}

		private void sdcFechaNac_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == ((int) Keys.Tab))
			{
				sdcFechaNac.Focus();
			} // tabulador

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void sdcFechaNac_Leave(Object eventSender, EventArgs eventArgs)
		{
            sdcFechaNac.Refresh();
            if (sdcFechaNac.Value == sdcFechaNac.MinDate)
                sdcFechaNac.NullableValue = null;
			Activar_buscar();
		}
        private void sprFiliados_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
		{

            if (botonIzquierdo)             
            {
                if (sprFiliados.CurrentRow == null)
                    return;

                int Col = sprFiliados.CurrentColumn.Index + 1;
                int Row = sprFiliados.CurrentRow.Index + 1;

                string SqlVersion = String.Empty;
                DataSet RrSQLVersion = null;
                int i = 0;
                string Aux = String.Empty;

                //O.Frias - 23/02/2009
                //Segun la constante desactivo el cmbOpen
                if (MFiliacion.blnIMDHOPEN)
                {
                    cmdOpen.Enabled = false;
                }

                if (Row > 0)
                {
                    if (fila_seleccionada == Row)
                    {
                        sprFiliados.CurrentRow = null;
                        fila_seleccionada = 0;
                        LIMPIAR_FORMULARIO();
                    }
                    else
                    {
                        //O.Frias - 23/02/2009
                        //Segun la constante activo el cmbOpen
                        if (MFiliacion.blnIMDHOPEN)
                        {
                            cmdOpen.Enabled = true;
                        }

                        if (GridCargado)
                        {
                            cbAceptar.Enabled = true;
                            cbBuscar.Enabled = false;
                            cbNuevo.Enabled = false;
                            this.CBPacienteAnt.Enabled = false;
                            seleccionado = true;
                            fila = Row;
                            sprFiliados.Row = Row;

                            sprFiliados.Col = 11;
                            sqlult = "select * from dpacient where gidenpac = '" + sprFiliados.Text + "'";
                            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlult, MFiliacion.GConexion);
                            MFiliacion.RrSQLUltimo = new DataSet();
                            tempAdapter.Fill(MFiliacion.RrSQLUltimo);
                            IndiceRrSQLUltimo = 0;
                            MFiliacion.ModCodigo = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gidenpac"]);
                            //Mover los datos a las cajas de texto
                            if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DNOMBPAC"]))
                            {
                                tbNombre.Text = "";
                            }
                            else
                            {
                                tbNombre.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DNOMBPAC"]);
                            }
                            if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE1PAC"]))
                            {
                                tbApellido1.Text = "";
                            }
                            else
                            {
                                tbApellido1.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE1PAC"]);
                            }
                            if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE2PAC"]))
                            {
                                tbApellido2.Text = "";
                            }
                            else
                            {
                                tbApellido2.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE2PAC"]);
                            }
                            if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["FNACIPAC"]))
                            {
                                sdcFechaNac.NullableValue = null;
                            }
                            else
                            {
                                sdcFechaNac.Value = Convert.ToDateTime(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["FNACIPAC"]);
                            }

                            if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]))
                            {
                                //(maplaza)(03/05/2006)Las dos cajas de texto (las correspondientes al "DNI/NIF" y a la de
                                //"Otros documentos") dependen del mismo campo de Base de Datos
                                rbBuscar[1].IsChecked = false;
                                rbBuscar[5].IsChecked = false;
                                MEBDNI.Text = "         ";
                                tbOtrosDocumentos.Text = "";
                            }
                            else
                            {
                                //(maplaza)(03/05/2006)Una vez extra�do el valor del campo de base de datos, hay que comprobar si
                                //ese valor se corresponde (por caracter�sticas del formato) con un DNI/NIF o con un
                                //documento distinto de esos (ser�a entonces "Otros documentos")
                                if (MFiliacion.EsDNI_NIF(Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim(), 9))
                                {
                                    rbBuscar[1].IsChecked = true;
                                    rbBuscar[5].IsChecked = false;
                                    tbOtrosDocumentos.Text = "";
                                    if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim().Length == 9)
                                    {
                                        MEBDNI.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim();
                                    }
                                    else
                                    {
                                        MEBDNI.Text = StringsHelper.Format(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"], "00000000") + " ";
                                    }
                                }
                                else
                                {
                                    //si no se trata de un DNI ni de un NIF es "Otros documentos"
                                    rbBuscar[1].IsChecked = false;
                                    rbBuscar[5].IsChecked = true;
                                    MEBDNI.Text = "         ";
                                    tbOtrosDocumentos.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim();
                                }
                                //MEBDNI.Text = RrSQLUltimo("NDNINIFP")
                            }


                            //SEXO HOMBRE
                            if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ITIPSEXO"]) == MFiliacion.CodSHombre)
                            {
                                rbSexo[0].IsChecked = true;
                            }
                            else
                            {
                                //SEXO MUJER
                                if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ITIPSEXO"]) == MFiliacion.CodSMUJER)
                                {
                                    rbSexo[1].IsChecked = true;
                                }
                                else
                                {
                                    //SEXO INDETERMINADO
                                    if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ITIPSEXO"]) == MFiliacion.CodSINDET)
                                    {
                                        rbSexo[0].IsChecked = false;
                                        rbSexo[1].IsChecked = false;
                                    }
                                }
                            }
                            //TARJETA SANITARIA
                            if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NTARJETA"]))
                            {
                                tbTarjeta.Text = "";
                            }
                            else
                            {
                                tbTarjeta.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NTARJETA"]);
                            }
                            if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NAFILIAC"]))
                            {
                                tbNSS.Text = "";
                                TbVersion.Text = "";
                                CodigoDeControl = -1;
                                Algoritmo = -1;
                                this.tbsocieda.Text = "";
                            }
                            else
                            {
                                tbNSS.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NAFILIAC"]);
                                TbVersion.Text = "";
                                CodigoDeControl = -1;
                                Algoritmo = -1;
                            }

                            //�APA PARA INTENTAR RECOGER DE LA TARJETA EL NUMERO DE VERSION CUANDO
                            //ESTA NO COINCIDA CON LEIDA DEL LECTOR
                            if (!Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]))
                            {
                                SqlVersion = "SELECT NAFILIAC, NVERSION FROM DENTIPAC WHERE " +
                                             " DENTIPAC.gidenpac ='" + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gidenpac"]).Trim() + "' AND" +
                                             " DENTIPAC.gsocieda = " + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]);
                                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SqlVersion, MFiliacion.GConexion);
                                RrSQLVersion = new DataSet();
                                tempAdapter_2.Fill(RrSQLVersion);
                                if (RrSQLVersion.Tables[0].Rows.Count != 0)
                                {
                                    if (Convert.IsDBNull(RrSQLVersion.Tables[0].Rows[0]["NAFILIAC"]))
                                    {
                                        tbNSS.Text = "";
                                        TbVersion.Text = "";
                                        CodigoDeControl = -1;
                                        Algoritmo = -1;
                                        this.tbsocieda.Text = "";
                                    }
                                    else
                                    {
                                        tbNSS.Text = Convert.ToString(RrSQLVersion.Tables[0].Rows[0]["NAFILIAC"]).Trim();
                                        TbVersion.Text = Convert.ToString(RrSQLVersion.Tables[0].Rows[0]["NVERSION"]) + "";
                                        CodigoDeControl = -1;
                                        Algoritmo = -1;
                                    }
                                }
                                RrSQLVersion.Close();
                                RrSQLVersion = null;
                            }
                            //--------------------------
                            if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ntelefo1"]))
                            {
                                if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ntelefo2"]))
                                {
                                    if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ntelefo3"]))
                                    {
                                        tbDPTelf1.Text = "";
                                    }
                                    else
                                    {
                                        tbDPTelf1.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ntelefo3"]);
                                    }
                                }
                                else
                                {
                                    tbDPTelf1.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ntelefo2"]);
                                }
                            }
                            else
                            {
                                tbDPTelf1.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ntelefo1"]);
                            }

                            //Oscar C Ocutbre 2011 - CIP Autonomico
                            if (bCipAutonomicoManual)
                            {
                                tbCIPAutonomico.Text = (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ocipauto"]) + "").Trim();
                            }
                            else
                            {
                                tbCIPAutonomico.Text = (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ncipauto"]) + "").Trim();
                            }

                            //Rub�n Septiembre 2014 - Busqueda por CORREO ELECTR�NICO
                            if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ddiemail"]))
                            {
                                tbCorreoElectronico.Text = "";
                            }
                            else
                            {
                                tbCorreoElectronico.Text = (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ddiemail"]) + "").Trim();
                            }

                            sprFiliados.Col = 6;
                            sprFiliados.Row = Row;
                            MFiliacion.ENTIDAD = sprFiliados.Text;
                            sprFiliados.Col = 1;
                            sprFiliados.Row = Row;
                            tbNHistoria.Text = sprFiliados.Text;
                            Bloquear_Cajas();
                            fila_seleccionada = Row;
                        }
                    }
                }
            }
		}

        public void sprFiliados_CellDoubleClick(object eventSender, GridViewCellEventArgs eventArgs)
    	{
            if (!botonIzquierdo)
                return;

            if (sprFiliados.CurrentRow == null)
                return;

            int Col = sprFiliados.CurrentColumn.Index + 1;
            int Row = sprFiliados.CurrentRow.Index + 1;


            int i = 0;
			string Aux = String.Empty;
			if (Row > 0)
			{
                if (GridCargado)
				{
					cbAceptar.Enabled = true;
					cbBuscar.Enabled = false;
					cbNuevo.Enabled = false;
					this.CBPacienteAnt.Enabled = false;
					seleccionado = true;
					fila = Row;
                    
                    IndiceRrSQLUltimo = 0;
					MFiliacion.ModCodigo = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gidenpac"]);
					//Mover los datos a las cajas de texto
					if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DNOMBPAC"]))
					{
						tbNombre.Text = "";
					}
					else
					{	
						tbNombre.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DNOMBPAC"]);
					}
                    
					if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE1PAC"]))
					{
						tbApellido1.Text = "";
					}
					else
					{	
						tbApellido1.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE1PAC"]);
					}
					
					if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE2PAC"]))
					{
						tbApellido2.Text = "";
					}
					else
					{	
						tbApellido2.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE2PAC"]);
					}
					
					if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["FNACIPAC"]))
					{
                        sdcFechaNac.NullableValue = null;
                    }
					else
					{
						sdcFechaNac.Value = Convert.ToDateTime(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["FNACIPAC"]);
					}
					//SEXO HOMBRE
					if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ITIPSEXO"]) == MFiliacion.CodSHombre)
					{
						rbSexo[0].IsChecked = true;
					}
					else
					{
						//SEXO MUJER
						if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ITIPSEXO"]) == MFiliacion.CodSMUJER)
						{
							rbSexo[1].IsChecked = true;
						}
						else
						{
							//SEXO INDETERMINADO
							if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ITIPSEXO"]) == MFiliacion.CodSINDET)
							{
								rbSexo[0].IsChecked = false;
								rbSexo[1].IsChecked = false;
							}
						}
					}
					if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]))
					{
						//(maplaza)(03/05/2006)Las dos cajas de texto (las correspondientes al "DNI/NIF" y a la de
						//"Otros documentos") dependen del mismo campo de Base de Datos
						rbBuscar[1].IsChecked = false;
						rbBuscar[5].IsChecked = false;
						MEBDNI.Text = "         ";
						tbOtrosDocumentos.Text = "";
					}
					else
					{
						//(maplaza)(03/05/2006)Una vez extra�do el valor del campo de base de datos, hay que comprobar si
						//ese valor se corresponde (por caracter�sticas del formato) con un DNI/NIF o con un
						//documento distinto de esos (Otros documentos)
						if (MFiliacion.EsDNI_NIF(Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim(), 9))
						{
							rbBuscar[1].IsChecked = true;
							rbBuscar[5].IsChecked = false;
							tbOtrosDocumentos.Text = "";
							if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim().Length == 9)
							{	
								MEBDNI.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim();
							}
							else
							{
                                MEBDNI.Text = StringsHelper.Format(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"], "00000000") + " ";
							}
						}
						else
						{
							//si no se trata de un DNI ni de un NIF es "Otros documentos"
							rbBuscar[1].IsChecked = false;
							rbBuscar[5].IsChecked = true;
							MEBDNI.Text = "         ";
							tbOtrosDocumentos.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim();
						}
						//MEBDNI.Text = RrSQLUltimo("NDNINIFP")
					}
					//TARJETA SANITARIA
					
					if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NTARJETA"]))
					{
						tbTarjeta.Text = "         ";
					}
					else
					{
						tbTarjeta.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NTARJETA"]);
					}
					
					if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NAFILIAC"]))
					{
						tbNSS.Text = "";
						TbVersion.Text = "";
						CodigoDeControl = -1;
						Algoritmo = -1;
					}
					else
					{
						tbNSS.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NAFILIAC"]);
						TbVersion.Text = "";
						CodigoDeControl = -1;
						Algoritmo = -1;
					}

					//Oscar C Ocutbre 2011 - CIP Autonomico
					if (bCipAutonomicoManual)
					{
						tbCIPAutonomico.Text = (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ocipauto"]) + "").Trim();
					}
					else
					{
						tbCIPAutonomico.Text = (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ncipauto"]) + "").Trim();
					}

					sprFiliados.Col = 6;
					sprFiliados.Row = Row;
					MFiliacion.ENTIDAD = sprFiliados.Text;
					sprFiliados.Col = 9;
					sprFiliados.Row = Row;
					tbNHistoria.Text = sprFiliados.Text;
					Bloquear_Cajas();
					cbAceptar_Click(cbAceptar, new EventArgs());
				}
			}
		}

		private void sprFiliados_GotFocus(object eventSender, EventArgs eventArgs)
		{
			//sprFiliados.OperationMode = OperationModeRead
		}

        private void sprFiliados_TopLeftChange(object eventSender, UpgradeHelpers.Spread.TopLeftChangeEventArgs eventArgs)
        {
            if (CargarMas)
            {
                if (eventArgs.NewTop >= tope)
                {
                    this.Cursor = Cursors.WaitCursor;
                    sprFiliados.Col = 10;
                    sprFiliados.Row = sprFiliados.MaxRows; //- 2
                    sqlult = "select * from dpacient where gidenpac = '" + sprFiliados.Text + "'";
                    SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlult, MFiliacion.GConexion);
                    MFiliacion.RrSQL = new DataSet();
                    tempAdapter.Fill(MFiliacion.RrSQL);

                    if (RqSQL.Parameters.Count == 0)
                    {
                        RqSQL.Parameters.Add("@dape1pac", SqlDbType.VarChar);
                        RqSQL.Parameters.Add("@dape2pac", SqlDbType.VarChar);
                        RqSQL.Parameters.Add("@dnombpac", SqlDbType.VarChar);
                        RqSQL.Parameters.Add("@gidenpac", SqlDbType.VarChar);
                    }

                    RqSQL.Parameters["@dape1pac"].Value = MFiliacion.RrSQL.Tables[0].Rows[0]["dape1pac"];
                    RqSQL.Parameters["@dape2pac"].Value = MFiliacion.RrSQL.Tables[0].Rows[0]["dape2pac"];
                    RqSQL.Parameters["@dnombpac"].Value = MFiliacion.RrSQL.Tables[0].Rows[0]["dnombpac"];
                    RqSQL.Parameters["@gidenpac"].Value = MFiliacion.RrSQL.Tables[0].Rows[0]["gidenpac"];
                    MFiliacion.RrSQLUltimo.Reset();
                    SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(RqSQL);
                    tempAdapter_2.Fill(MFiliacion.RrSQLUltimo);
                    if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count < NRegistros)
                    {
                        CargarMas = false;
                    }
                    tope = MFiliacion.RrSQLUltimo.Tables[0].Rows.Count + sprFiliados.MaxRows - 5;
                    Cargar_Grid();

                    this.Cursor = Cursors.Default;
                }

            }
        }


        private void tbApellido1_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbApellido1_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbApellido1.SelectionStart = 0;
			tbApellido1.SelectionLength = tbApellido1.Text.Trim().Length;
		}

		private void tbApellido1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbApellido1_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbApellido2_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbApellido2_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbApellido2.SelectionStart = 0;
			tbApellido2.SelectionLength = tbApellido2.Text.Trim().Length;
		}

		private void tbApellido2_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbApellido2_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void Limpiar_Cajas()
		{
			LbSociedad.Text = "";
			tbNombre.Text = "";
			tbApellido1.Text = "";
			tbApellido2.Text = "";
			rbSexo[0].IsChecked = false;
			rbSexo[1].IsChecked = false;
			sdcFechaNac.Value = DateTime.Today;
            sdcFechaNac.NullableValue = null;
            tbNHistoria.Text = "";
			MEBDNI.Text = "         ";
			tbNSS.Text = "";
			TbVersion.Text = "";
			CodigoDeControl = -1;
			Algoritmo = -1;
			this.tbsocieda.Text = "";
			tbDPTelf1.Text = "";
			tbTarjeta.Text = "";
			tbOtrosDocumentos.Text = "";
			tbCIPAutonomico.Text = "";
			tbCorreoElectronico.Text = "";
			rbBuscar[0].IsChecked = false;
			rbBuscar[1].IsChecked = false;
			rbBuscar[2].IsChecked = false;
			rbBuscar[3].IsChecked = false;
			rbBuscar[5].IsChecked = false;
			rbBuscar[6].IsChecked = false; //Oscar C Octubre 2011 - Busqueda por CIP AUTONOMICO
			rbBuscar[7].IsChecked = false; //Rub�n 09/2014 - Busqueda por correo e.
			sprFiliados.MaxRows = 0;
			cbAceptar.Enabled = false;
			this.CBPacienteAnt.Enabled = true;
		}
		private void Bloquear_Cajas()
		{
			tbNombre.IsReadOnly = true;
			tbApellido1.IsReadOnly = true;
			tbApellido2.IsReadOnly = true;
			frmSexo.Enabled = false;
			sdcFechaNac.Enabled = false;
			tbNHistoria.IsReadOnly = true;
			tbOtrosDocumentos.IsReadOnly = true;
            cbbArchivo.ReadOnly = true;
            tbCIPAutonomico.IsReadOnly = true;
			FrmBuscar.Enabled = false;
		}
		private void Desbloquear_cajas()
		{
			tbNombre.IsReadOnly = false;
			tbApellido1.IsReadOnly = false;
			tbApellido2.IsReadOnly = false;
			frmSexo.Enabled = true;
			sdcFechaNac.Enabled = true;
			tbNHistoria.IsReadOnly = false;
            cbbArchivo.ReadOnly = false;
            tbNSS.IsReadOnly = false;
			TbVersion.IsReadOnly = false;
			tbOtrosDocumentos.IsReadOnly = false;
			tbCIPAutonomico.IsReadOnly = false;
			FrmBuscar.Enabled = true;
		}
		public void Cargar_Grid()
		{
			string fecha = String.Empty;
			Color lcolor = new Color();
			StringBuilder SQLSOC = new StringBuilder();
			DataSet RrSQLSOC = null;

			GridCargado = true;
			cbBuscar.Enabled = false;
			if (MFiliacion.stFiliacion == "ALTA" || MFiliacion.stFiliacion == "MODIFICACION")
			{
				COMPROBAR_NUEVO();
			}
			IndiceRrSQLUltimo = MFiliacion.RrSQLUltimo.MoveFirst();
			sprFiliados.MaxRows = MFiliacion.RrSQLUltimo.Tables[0].Rows.Count + sprFiliados.MaxRows;
			if (!P1)
			{
				sprFiliados.Row += 2;
				P1 = true;
			}
			else
			{
				if (CargarMas)
				{
					sprFiliados.Row = sprFiliados.MaxRows - NRegistros + 1;
				}
				else
				{
					sprFiliados.Row = sprFiliados.MaxRows - MFiliacion.RrSQLUltimo.Tables[0].Rows.Count + 1;
				}
			}
            sprFiliados.BeginUpdate();
            foreach (DataRow iteration_row in MFiliacion.RrSQLUltimo.Tables[0].Rows)
			{
				//HISTORIA CLINICA
				sprFiliados.Col = 1;
				string SQLNHIST = String.Empty;
				DataSet RrSQLNHIST = null;
				if (rbBuscar[0].IsChecked && tbNHistoria.Text != "")
				{
					//si la busqueda es por numero de historia
					sprFiliados.Text = Convert.ToString(iteration_row["ghistoria"]);
					lcolor = Line1.BorderColor;
				}
				else
				{
					//si no es por numero de historia lo busca
					cbbArchivo.SelectedIndex = Archivo - 1;
					if (MFiliacion.consultaCancelados)
					{
						SQLNHIST = "SELECT GHISTORIA FROM SDOSSIER WHERE SDOSSIER.GIDENPAC = '" + Convert.ToString(iteration_row["GIDENPAC"]) + "' "; //AND (icontenido = 'H' or icontenido = 'P') AND SDOSSIER.GSERPROPIET = " & ArchivoCentral
					}
					else
					{
						SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + Convert.ToString(iteration_row["GIDENPAC"]) + "' "; //AND (icontenido = 'H' or icontenido = 'P') AND HDOSSIER.GSERPROPIET = " & ArchivoCentral
					}
					SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLNHIST, MFiliacion.GConexion);
					RrSQLNHIST = new DataSet();
					tempAdapter.Fill(RrSQLNHIST);
                    IndiceRrSQLNHIST = 0;
					if (RrSQLNHIST.Tables[0].Rows.Count != 0)
					{
						sprFiliados.Text = Convert.ToString(RrSQLNHIST.Tables[0].Rows[IndiceRrSQLNHIST]["ghistoria"]);
						lcolor = Line1.BorderColor;						
					}
					else
					{
						lcolor = Color.Black;
					}
					RrSQLNHIST.Close();
				}
				sprFiliados.Col = 2;
				sprFiliados.Text = Convert.ToString(iteration_row["DAPE1PAC"]).Trim() + " " + Convert.ToString(iteration_row["DAPE2PAC"]).Trim() + ", " + Convert.ToString(iteration_row["DNOMBPAC"]).Trim();
				//FECHA DE NACIMIENTO
				sprFiliados.Col = 3;
				if (!Convert.IsDBNull(iteration_row["FNACIPAC"]))
				{
					fecha = Convert.ToDateTime(iteration_row["FNACIPAC"]).ToString("dd/MM/yyyy");
					sprFiliados.Text = fecha;				
				}
				//SEXO
				sprFiliados.Col = 4;
				if (Convert.ToString(iteration_row["ITIPSEXO"]) == MFiliacion.CodSHombre)
				{
					sprFiliados.Text = MFiliacion.TexSHombre;
				}
				if (Convert.ToString(iteration_row["ITIPSEXO"]) == MFiliacion.CodSMUJER)
				{
					sprFiliados.Text = MFiliacion.TexSMUJER;
				}
				if (Convert.ToString(iteration_row["ITIPSEXO"]) == MFiliacion.CodSINDET)
				{
					sprFiliados.Text = MFiliacion.TexSINDET;
				}
				//DNI
				sprFiliados.Col = 5;
				if (!Convert.IsDBNull(iteration_row["ndninifp"]))
				{
					sprFiliados.Text = Convert.ToString(iteration_row["ndninifp"]).Trim();					
				}
				//ENTIDAD FINANCIADORA
				sprFiliados.Col = 6;
				if (!Convert.IsDBNull(iteration_row["GSOCIEDA"]))
				{
					//COGE EL CODIGO DE LA SEGURIDAD SOCIAL
					if (MFiliacion.CodSS == Convert.ToDouble(iteration_row["GSOCIEDA"]))
					{
						//ES SEGURIDAD SOCIAL
						//a�adimos que en lugar de SS salga la descripcion de la inspeccion
						SQLSOC = new StringBuilder("select dinspecc.dinspecc from dentipac ");
						SQLSOC.Append(" inner join dinspecc on dinspecc.ginspecc = dentipac.ginspecc ");
						SQLSOC.Append(" where gidenpac = '" + Convert.ToString(iteration_row["GIDENPAC"]) + "' and gsocieda =" + Convert.ToString(iteration_row["GSOCIEDA"]));
						SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLSOC.ToString(), MFiliacion.GConexion);
						RrSQLSOC = new DataSet();
						tempAdapter_2.Fill(RrSQLSOC);
						if (RrSQLSOC.Tables[0].Rows.Count != 0)
						{
							sprFiliados.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["DINSPECC"]) + "";
						}
						RrSQLSOC.Close();
						//sprFiliados.Text = TexSS
					}
					else
					{
						//COGE EL NUMERO DE PRIVADO
						if (MFiliacion.CodPriv == Convert.ToDouble(iteration_row["GSOCIEDA"]))
						{ //ES SOCIEDAD
							//ES PRIVADO
							sprFiliados.Text = MFiliacion.TexPriv;
						}
						else
						{
							//ES SOCIEDAD
							//BUSCA EL NOMBRE DE LA SOCIEDAD PARA ESE FILIADO
							SQLSOC = new StringBuilder("SELECT DSOCIEDA FROM DSOCIEDA WHERE GSOCIEDA = " + Convert.ToString(iteration_row["GSOCIEDA"]));
							SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLSOC.ToString(), MFiliacion.GConexion);
							RrSQLSOC = new DataSet();
							tempAdapter_3.Fill(RrSQLSOC);
							if (RrSQLSOC.Tables[0].Rows.Count != 0)
							{
								sprFiliados.Text = Convert.ToString(RrSQLSOC.Tables[0].Rows[0]["DSOCIEDA"]);
							}
							else
							{
								sprFiliados.Text = "";
							}
							RrSQLSOC.Close();
						}
					}
				}
				//DOMICILIO
				sprFiliados.Col = 7;
				if (!Convert.IsDBNull(iteration_row["DDIREPAC"]))
				{
					sprFiliados.Text = Convert.ToString(iteration_row["DDIREPAC"]);
				}
				//CODIGO POSTAL
				sprFiliados.Col = 8;
				if (!Convert.IsDBNull(iteration_row["GCODIPOS"]))
				{
					if (bFormatoCodigoPos)
					{
						sprFiliados.Text = StringsHelper.Format(iteration_row["GCODIPOS"], "00000");
					}
					else
					{
						sprFiliados.Text = (Convert.ToString(iteration_row["GCODIPOS"]) + "").Trim();
					}
				}
				//POBLACION
				sprFiliados.Col = 9;
				string SQLPOBLACION = String.Empty;
				DataSet RrSQLPOBLACION = null;
				if (!Convert.IsDBNull(iteration_row["gPOBLACI"]))
				{
					//SI NO ES EXTRANJERO SE BUSCA LA POBLACION
					SQLPOBLACION = "SELECT DPOBLACI FROM DPOBLACI WHERE GPOBLACI = '" + Convert.ToString(iteration_row["gPOBLACI"]).Trim() + "' order by dpoblaci";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLPOBLACION, MFiliacion.GConexion);
					RrSQLPOBLACION = new DataSet();
					tempAdapter_4.Fill(RrSQLPOBLACION);
					sprFiliados.Text = Convert.ToString(RrSQLPOBLACION.Tables[0].Rows[0]["DPOBLACI"]).Trim();
					RrSQLPOBLACION.Close();
				}
				else
				{
					//SI NO ES NACIONAL SE INTRODUCE LA POBLACION DEL EXTRANGERO
					if (!Convert.IsDBNull(iteration_row["DPOBLACI"]))
					{
						sprFiliados.Text = Convert.ToString(iteration_row["DPOBLACI"]);
					}
				}
				sprFiliados.Col = 11;
				sprFiliados.Text = Convert.ToString(iteration_row["GIDENPAC"]);
				sprFiliados.foreColor = lcolor;
				//SE A�ADE UNA FILA Y SE POSICIONA EN LA PRIMERA COLUMNA
				sprFiliados.Row++;
				sprFiliados.Col = 1;
			}
            sprFiliados.EndUpdate();
        }

		public void REFERENCIAS(object CLASE, object OBJETO, string stFiliac, string stMod)
		{
			MFiliacion.CLASE2 = CLASE;
			MFiliacion.OBJETO2 = OBJETO;
			MFiliacion.stFiliacion = stFiliac;
			MFiliacion.stModulo = stMod;
			if (MFiliacion.stModulo == ("Urgencias").ToUpper())
			{
				chbFiliacPov.Visible = true;
			}
		}

		private void MANDAR_DATOS()
		{
			string SQLDent = String.Empty;
			DataSet RrSQLDent = null;
			if (!MFiliacion.paso)
			{
				//si se selecciona un paciente de la primera panatalla
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DNOMBPAC"]))
				{
					MFiliacion.Nombre2 = "";
				}
				else
				{
					MFiliacion.Nombre2 = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DNOMBPAC"]);
				}
				
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE1PAC"]))
				{
					MFiliacion.Apellido1 = "";
				}
				else
				{
					MFiliacion.Apellido1 = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE1PAC"]);
				}
				
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE2PAC"]))
				{
					MFiliacion.Apellido2 = "";
				}
				else
				{
					MFiliacion.Apellido2 = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE2PAC"]);
				}
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["FNACIPAC"]))
				{
					MFiliacion.fechaNac = "";
				}
				else
				{
					MFiliacion.fechaNac = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["FNACIPAC"]);
				}
				MFiliacion.Sexo = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ITIPSEXO"]);
				SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GIDENPAC"]) + "' AND icontenido = 'H' AND HDOSSIER.GSERPROPIET = " + MFiliacion.ArchivoCentral.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLNHIST, MFiliacion.GConexion);
				RrSQLNHIST = new DataSet();
				tempAdapter.Fill(RrSQLNHIST);
                IndiceRrSQLNHIST = 0;
				if (RrSQLNHIST.Tables[0].Rows.Count != 0)
				{
                    IndiceRrSQLNHIST = RrSQLNHIST.MoveFirst();
					MFiliacion.Historia = Convert.ToString(RrSQLNHIST.Tables[0].Rows[IndiceRrSQLNHIST]["ghistoria"]);
				}
				else
				{
					MFiliacion.Historia = "";
				}
				RrSQLNHIST.Close();
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DDIREPAC"]))
				{
					MFiliacion.Domicilio = "";
				}
				else
				{
					MFiliacion.Domicilio = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DDIREPAC"]);
				}
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GCODIPOS"]))
				{
					MFiliacion.CodPostal = "";
				}
				else
				{
					if (bFormatoCodigoPos)
					{
						MFiliacion.CodPostal = StringsHelper.Format(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GCODIPOS"], "00000");
					}
					else
					{
						MFiliacion.CodPostal = (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GCODIPOS"]) + "").Trim();
					}
				}
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GPOBLACI"]))
				{
					MFiliacion.codPob = "";
				}
				else
				{
					MFiliacion.codPob = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GPOBLACI"]);
				}
				
				SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GIDENPAC"]) + "' AND icontenido = 'H' AND HDOSSIER.GSERPROPIET = " + MFiliacion.ArchivoCentral.ToString();
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLNHIST, MFiliacion.GConexion);
				RrSQLNHIST = new DataSet();
				tempAdapter_2.Fill(RrSQLNHIST);
                IndiceRrSQLNHIST = 0;
				if (RrSQLNHIST.Tables[0].Rows.Count != 0)
				{
					
					IndiceRrSQLNHIST = RrSQLNHIST.MoveFirst();
					sprFiliados.Text = Conversion.Val(Convert.ToString(RrSQLNHIST.Tables[0].Rows[IndiceRrSQLNHIST]["ghistoria"])).ToString();
					MFiliacion.Historia = Conversion.Val(Convert.ToString(RrSQLNHIST.Tables[0].Rows[IndiceRrSQLNHIST]["ghistoria"])).ToString();
				}
				else
				{
					MFiliacion.Historia = "";
				}
				RrSQLNHIST.Close();
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ndninifp"]))
				{
					MFiliacion.NIF = "";
				}
				else
				{
					if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ndninifp"]).Trim().Length == 9)
					{
						MFiliacion.NIF = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ndninifp"]).Trim();
					}
					else
					{
						MFiliacion.NIF = StringsHelper.Format(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ndninifp"], "00000000") + " ";
					}
				}
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NAFILIAC"]))
				{
					MFiliacion.Asegurado = "";
				}
				else
				{
					MFiliacion.Asegurado = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NAFILIAC"]);
				}
				MFiliacion.idPaciente = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GIDENPAC"]);
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]))
				{
					MFiliacion.CodigoSoc = "";
				}
				else
				{
					MFiliacion.CodigoSoc = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]);
				}
				if (Convert.ToDouble(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]) == MFiliacion.CodSS)
				{
					SQLDent = "select iactpens, ginspecc from dentipac where gsocieda= " + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]) + " and gidenpac ='" + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gidenpac"]) + "'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLDent, MFiliacion.GConexion);
					RrSQLDent = new DataSet();
					tempAdapter_3.Fill(RrSQLDent);
					if (RrSQLDent.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["iactpens"]))
						{
							MFiliacion.IndAcPen = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["iactpens"]);
						}
						else
						{
							MFiliacion.IndAcPen = "";
						}
						
						if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["ginspecc"]))
						{
							MFiliacion.Inspec = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["ginspecc"]);
						}
						else
						{
							MFiliacion.Inspec = "";
						}
					}
					RrSQLDent.Close();
				}
				else
				{
					MFiliacion.IndAcPen = "";
					MFiliacion.Inspec = "";
				}

				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ifilprov"]))
				{
					MFiliacion.Filprov = "";
				}
				else
				{
					MFiliacion.Filprov = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ifilprov"]);
				}
			}
			else
			{
				//si se da de alta a un paciente de la segunda pantalla
				//o se modifica
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DNOMBPAC"]))
				{
					MFiliacion.Nombre2 = "";
				}
				else
				{
					MFiliacion.Nombre2 = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DNOMBPAC"]);
				}
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE1PAC"]))
				{
					MFiliacion.Apellido1 = "";
				}
				else
				{
					MFiliacion.Apellido1 = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE1PAC"]);
				}
				
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE2PAC"]))
				{
					MFiliacion.Apellido2 = "";
				}
				else
				{
					MFiliacion.Apellido2 = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DAPE2PAC"]);
				}
				
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["FNACIPAC"]))
				{
					MFiliacion.fechaNac = "";
				}
				else
				{
					MFiliacion.fechaNac = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["FNACIPAC"]);
				}
				MFiliacion.Sexo = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ITIPSEXO"]);
				SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GIDENPAC"]) + "' AND icontenido = 'H' AND HDOSSIER.GSERPROPIET = " + MFiliacion.ArchivoCentral.ToString();
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(SQLNHIST, MFiliacion.GConexion);
				RrSQLNHIST = new DataSet();
				tempAdapter_4.Fill(RrSQLNHIST);
                IndiceRrSQLNHIST = 0;
                if (RrSQLNHIST.Tables[0].Rows.Count != 0)
				{
					IndiceRrSQLNHIST = RrSQLNHIST.MoveFirst();
					sprFiliados.Text = Conversion.Val(Convert.ToString(RrSQLNHIST.Tables[0].Rows[IndiceRrSQLNHIST]["ghistoria"])).ToString();
					MFiliacion.Historia = Conversion.Val(Convert.ToString(RrSQLNHIST.Tables[0].Rows[IndiceRrSQLNHIST]["ghistoria"])).ToString();
				}
				else
				{
					MFiliacion.Historia = "";
				}
				RrSQLNHIST.Close();
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DDIREPAC"]))
				{
					MFiliacion.Domicilio = "";
				}
				else
				{
					MFiliacion.Domicilio = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["DDIREPAC"]);
				}
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GCODIPOS"]))
				{
					MFiliacion.CodPostal = "";
				}
				else
				{
					if (bFormatoCodigoPos)
					{
						MFiliacion.CodPostal = StringsHelper.Format(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GCODIPOS"], "00000");
					}
					else
					{
						MFiliacion.CodPostal = (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GCODIPOS"]) + "").Trim();
					}
				}
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GPOBLACI"]))
				{
					MFiliacion.codPob = "";
				}
				else
				{
					MFiliacion.codPob = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GPOBLACI"]);
				}

				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]))
				{
					MFiliacion.NIF = "";
				}
				else
				{
					if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim().Length == 9)
					{
						MFiliacion.NIF = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"]).Trim();
					}
					else
					{
						MFiliacion.NIF = StringsHelper.Format(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NDNINIFP"], "00000000") + " ";
					}
				}

				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NAFILIAC"]))
				{
					MFiliacion.Asegurado = "";
				}
				else
				{
					MFiliacion.Asegurado = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["NAFILIAC"]);
				}
				MFiliacion.idPaciente = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["GIDENPAC"]);
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]))
				{
					MFiliacion.CodigoSoc = "";
				}
				else
				{
					MFiliacion.CodigoSoc = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]);
				}
				if (Convert.ToDouble(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]) == MFiliacion.CodSS)
				{
					SQLDent = "select iactpens, ginspecc from dentipac where gsocieda= " + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gsocieda"]) + " and gidenpac ='" + Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["gidenpac"]) + "'";
					SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(SQLDent, MFiliacion.GConexion);
					RrSQLDent = new DataSet();
					tempAdapter_5.Fill(RrSQLDent);
					if (RrSQLDent.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["iactpens"]))
						{
							MFiliacion.IndAcPen = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["iactpens"]);
						}
						else
						{
							MFiliacion.IndAcPen = "";
						}
				
						if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["ginspecc"]))
						{
							MFiliacion.Inspec = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["ginspecc"]);
						}
						else
						{
							MFiliacion.Inspec = "";
						}
					}
					else
					{
						MFiliacion.IndAcPen = "";
						MFiliacion.Inspec = "";
					}
					RrSQLDent.Close();
				}
				else
				{
					MFiliacion.IndAcPen = "";
					MFiliacion.Inspec = "";
				}
				if (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ifilprov"]))
				{
					MFiliacion.Filprov = "";
				}
				else
				{
					MFiliacion.Filprov = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[IndiceRrSQLUltimo]["ifilprov"]);
				}
			}
			if (MFiliacion.stDevolverDatos != "NO")
			{
				
				MFiliacion.OBJETO2.Recoger_datos(MFiliacion.Nombre2, MFiliacion.Apellido1, MFiliacion.Apellido2, MFiliacion.fechaNac, MFiliacion.Sexo, MFiliacion.Domicilio, MFiliacion.NIF, MFiliacion.idPaciente, MFiliacion.CodPostal, MFiliacion.Asegurado, MFiliacion.codPob, MFiliacion.Historia, MFiliacion.CodigoSoc, MFiliacion.Filprov, MFiliacion.IndAcPen, MFiliacion.Inspec);
			}
			if (MFiliacion.stDevolverDatos != "NO")
			{
				MFiliacion.idPaciente = "";
			}
		}
		private void Vaciar_datos()
		{
			if (MFiliacion.RegresodeOpenlab != "")
			{
				return;
			}
			else
			{
				MFiliacion.Nombre2 = "";
				MFiliacion.Apellido1 = "";
				MFiliacion.Apellido2 = "";
				MFiliacion.fechaNac = "";
				MFiliacion.Sexo = "";
				MFiliacion.Domicilio = "";
				MFiliacion.CodPostal = "";
				MFiliacion.codPob = "";
				MFiliacion.Historia = "";
				MFiliacion.NIF = "";
				MFiliacion.Asegurado = "";
				MFiliacion.idPaciente = "";
				MFiliacion.CodigoSoc = "";
				MFiliacion.Filprov = "";
				MFiliacion.IndAcPen = "";
				MFiliacion.Inspec = "";
				if (MFiliacion.stDevolverDatos != "NO")
				{
					MFiliacion.OBJETO2.Recoger_datos(MFiliacion.Nombre2, MFiliacion.Apellido1, MFiliacion.Apellido2, MFiliacion.fechaNac, MFiliacion.Sexo, MFiliacion.Domicilio, MFiliacion.NIF, MFiliacion.idPaciente, MFiliacion.CodPostal, MFiliacion.Asegurado, MFiliacion.codPob, MFiliacion.Historia, MFiliacion.CodigoSoc, MFiliacion.Filprov, MFiliacion.IndAcPen, MFiliacion.Inspec);
				}
				MFiliacion.idPaciente = "";
			}
		}
		private void CERRAR_TODO()
		{
			if (ABIERTORrSQL)
			{
				MFiliacion.RrSQLUltimo.Close();
				MFiliacion.abierto = false;
			}
			this.Close();
		}

		public void DATOS_ACTUALIZADOS()
		{
			SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT * FROM DPACIENT WHERE GIDENPAC = '" + MFiliacion.codigo + "'", MFiliacion.GConexion);
			MFiliacion.RrDPACIENT = new DataSet();
			tempAdapter.Fill(MFiliacion.RrDPACIENT);
            IndiceRrDPACIENT = 0;
			string SQLDent = String.Empty;
			DataSet RrSQLDent = null;
			if (MFiliacion.RrDPACIENT.Tables[0].Rows.Count != 0)
			{
				MFiliacion.Nombre2 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DNOMBPAC"]);
				MFiliacion.Apellido1 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DAPE1PAC"]);
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DAPE2PAC"]))
				{
					MFiliacion.Apellido2 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DAPE2PAC"]);
				}
				else
				{
					MFiliacion.Apellido2 = "";
				}
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["FNACIPAC"]))
				{
					MFiliacion.fechaNac = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["FNACIPAC"]);
				}
				else
				{
					MFiliacion.fechaNac = "";
				}
				MFiliacion.Sexo = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ITIPSEXO"]);
				if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DDIREPAC"]))
				{
					MFiliacion.Domicilio = "";
				}
				else
				{
					MFiliacion.Domicilio = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DDIREPAC"]);
				}
				
				if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GCODIPOS"]))
				{
					MFiliacion.CodPostal = "";
				}
				else
				{
					if (bFormatoCodigoPos)
					{
						MFiliacion.CodPostal = StringsHelper.Format(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GCODIPOS"], "00000");
					}
					else
					{
						MFiliacion.CodPostal = (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GCODIPOS"]) + "").Trim();
					}
				}
				if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GPOBLACI"]))
				{
					MFiliacion.codPob = "";
				}
				else
				{
					MFiliacion.codPob = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GPOBLACI"]);
				}

				if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["NDNINIFP"]))
				{
					MFiliacion.NIF = "";
				}
				else
				{
					if (Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["NDNINIFP"]).Trim().Length == 9)
					{
						MFiliacion.NIF = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["NDNINIFP"]).Trim();
					}
					else
					{
						MFiliacion.NIF = StringsHelper.Format(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["NDNINIFP"], "00000000") + " ";
					}
				}

				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["NAFILIAC"]))
				{
					MFiliacion.Asegurado = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["NAFILIAC"]);
				}
				else
				{
					MFiliacion.Asegurado = "";
				}
				MFiliacion.idPaciente = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GIDENPAC"]);
				SQLNHIST = "SELECT GHISTORIA FROM HDOSSIER WHERE HDOSSIER.GIDENPAC = '" + MFiliacion.idPaciente + "' ";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLNHIST, MFiliacion.GConexion);
				RrSQLNHIST = new DataSet();
				tempAdapter_2.Fill(RrSQLNHIST);
                IndiceRrSQLNHIST = 0;
                if (RrSQLNHIST.Tables[0].Rows.Count != 0)
				{

                    IndiceRrSQLNHIST = RrSQLNHIST.MoveFirst();
					MFiliacion.Historia = Convert.ToString(RrSQLNHIST.Tables[0].Rows[IndiceRrSQLNHIST]["ghistoria"]);
				}
				else
				{
					MFiliacion.Historia = "";
				}
				RrSQLNHIST.Close();
				if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GSOCIEDA"]))
				{
					MFiliacion.CodigoSoc = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GSOCIEDA"]);
				}
				if (Convert.ToDouble(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GSOCIEDA"]) == MFiliacion.CodSS)
				{
					SQLDent = "select iactpens, ginspecc from dentipac where gsocieda= " + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["gsocieda"]) + " and gidenpac ='" + Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["gidenpac"]) + "'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLDent, MFiliacion.GConexion);
					RrSQLDent = new DataSet();
					tempAdapter_3.Fill(RrSQLDent);
					if (RrSQLDent.Tables[0].Rows.Count != 0)
					{
						if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["iactpens"]))
						{
							MFiliacion.IndAcPen = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["iactpens"]);
						}
						else
						{
							MFiliacion.IndAcPen = "";
						}
						
						if (!Convert.IsDBNull(RrSQLDent.Tables[0].Rows[0]["ginspecc"]))
						{
							MFiliacion.Inspec = Convert.ToString(RrSQLDent.Tables[0].Rows[0]["ginspecc"]);
						}
						else
						{
							MFiliacion.Inspec = "";
						}
					}
					RrSQLDent.Close();
				}
				else
				{
					MFiliacion.IndAcPen = "";
					MFiliacion.Inspec = "";
				}
				MFiliacion.Filprov = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ifilprov"]);
				if (MFiliacion.RegresodeOpenlab != "" && MFiliacion.RegresodeOpenlab != "ConDatos")
				{
					MFiliacion.RegresodeOpenlab = MFiliacion.idPaciente;
				}
				else
				{
					if (MFiliacion.stDevolverDatos != "NO")
					{
						MFiliacion.OBJETO2.Recoger_datos(MFiliacion.Nombre2, MFiliacion.Apellido1, MFiliacion.Apellido2, MFiliacion.fechaNac, MFiliacion.Sexo, MFiliacion.Domicilio, MFiliacion.NIF, MFiliacion.idPaciente, MFiliacion.CodPostal, MFiliacion.Asegurado, MFiliacion.codPob, MFiliacion.Historia, MFiliacion.CodigoSoc, MFiliacion.Filprov, MFiliacion.IndAcPen, MFiliacion.Inspec);
					}
				}
				if (MFiliacion.stDevolverDatos != "NO")
				{
					MFiliacion.idPaciente = "";
				}
			}
		}
		private void GENERAR_IDENTIFICADOR_PROVISIONAL()
		{
			MFiliacion.codigo = "";
			//Se crea un array con las vocales para compararlas con la cadena
			MFiliacion.Vocales = new string[]{"A", "E", "I", "O", "U", "�", "�", "�", "�", "�", "�", "-", ";", ",", "_", "`", "�", "."};
			//Si tiene, coge el primer apellido
			if (tbApellido1.Text != "")
			{
				MFiliacion.caracteres = Strings.Len(tbApellido1.Text);
				//Transforma en may�sculas
				MFiliacion.cadena = tbApellido1.Text.ToUpper();
				Codigo_Paciente();
				//si tiene, coge el segundo apellido
				if (tbApellido2.Text != "")
				{
					MFiliacion.caracteres = Strings.Len(tbApellido2.Text);
					//Transforma en may�sculas
					MFiliacion.cadena = tbApellido2.Text.ToUpper();
					Codigo_Paciente();
				}
				else
				{
					//si tiene apellido 1 y no tiene apellido 2
					MFiliacion.codigo = MFiliacion.codigo + "ZZ";
				}
			}
			else
			{
				//si no tiene se introducen 4 zetas
				MFiliacion.codigo = "ZZZZ";
			}
		}
		private void IDENTIFICADOR_PROVISIONAL_REPETIDO()
		{
			string identificador = String.Empty;
			int contador = 0;
			string Scontador = String.Empty;
			//para evitar que cuando se filia a un paciente de forma provisional
			//se inserte un gidenpac duplicado modificamos la busqueda de gidenpac repetido
			//buscamos con el codigo generado (dos primer apellido, dos segundo apellido)concatenamos con cuatro 0000
			//porque en estos casos nunca hay fecha de nacimiento y si se introduce se ignora.

			if (tbApellido1.Text != "")
			{ //SI EXISTE EL PRIMER APELLIDO
				if (tbApellido2.Text != "")
				{ //SI EXISTE EL PRIMERO
					//SELECCIONO TODOS LOS QUE COINCIDAN CON
					//FILIACION PROVISIONAL = SI Y QUE TENGAN APELLIDO1 Y APELLIDO 2
					SQLRepetidos = "Select GIDENPAC FROM DPACIENT WHERE GIDENPAC LIKE '" + MFiliacion.codigo + "0000%' " + " order by GIDENPAC";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLRepetidos, MFiliacion.GConexion);
					RrSQLRepetidos = new DataSet();
					tempAdapter.Fill(RrSQLRepetidos);
                    IndiceRrSQLRepetidos = 0;
                    if (RrSQLRepetidos.Tables[0].Rows.Count == 0)
					{
						//NO HAY COINCIDENCIAS
						MFiliacion.codigo = MFiliacion.codigo + "000000001";
					}
					else
					{
						//coge el ultimo filiado provisionalmente
						IndiceRrSQLRepetidos = RrSQLRepetidos.MoveLast(null);
						identificador = Convert.ToString(RrSQLRepetidos.Tables[0].Rows[IndiceRrSQLRepetidos]["gidenpac"]);
						contador = Convert.ToInt32(Double.Parse(identificador.Substring(4)));
						Scontador = StringsHelper.Format(contador + 1, "000000000");
						MFiliacion.codigo = MFiliacion.codigo + Scontador;
					}
				}
				else
				{
					//SELECCIONO TODOS LOS QUE COINCIDAN CON
					//FILIACION PROVISIONAL = SI Y QUE COINCIDA APELLIDO1 Y NO TENGAN
					//APELLIDO 2
					SQLRepetidos = "Select GIDENPAC FROM DPACIENT WHERE GIDENPAC LIKE '" + MFiliacion.codigo + "0000%' " + " order by GIDENPAC";
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLRepetidos, MFiliacion.GConexion);
					RrSQLRepetidos = new DataSet();
					tempAdapter_2.Fill(RrSQLRepetidos);
                    IndiceRrSQLRepetidos = 0;
                    if (RrSQLRepetidos.Tables[0].Rows.Count == 0)
					{
						//NO HAY COINCIDENCIAS
						MFiliacion.codigo = MFiliacion.codigo + "000000001";
					}
					else
					{
						//coge el ultimo filiado provisionalmente
						IndiceRrSQLRepetidos = RrSQLRepetidos.MoveLast(null);
						identificador = Convert.ToString(RrSQLRepetidos.Tables[0].Rows[IndiceRrSQLRepetidos]["gidenpac"]);
						contador = Convert.ToInt32(Double.Parse(identificador.Substring(4)));
						Scontador = StringsHelper.Format(contador + 1, "000000000");
						MFiliacion.codigo = MFiliacion.codigo + Scontador;
					}
				}
			}
			else
			{
				//SELECCIONO TODOS LOS QUE COINCIDAN CON
				//FILIACION PROVISIONAL = SI Y QUE NO TENGAN APELLIDO1 NI APELLIDO 2
				SQLRepetidos = "Select GIDENPAC FROM DPACIENT WHERE GIDENPAC LIKE '" + MFiliacion.codigo + "0000%' " + "  order by GIDENPAC ";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(SQLRepetidos, MFiliacion.GConexion);
				RrSQLRepetidos = new DataSet();
				tempAdapter_3.Fill(RrSQLRepetidos);
                IndiceRrSQLRepetidos = 0;
                if (RrSQLRepetidos.Tables[0].Rows.Count == 0)
				{
					//NO HAY COINCIDENCIAS
					MFiliacion.codigo = MFiliacion.codigo + "000000001";
				}
				else
				{
					//coge el ultimo filiado provisionalmente
					IndiceRrSQLRepetidos = RrSQLRepetidos.MoveLast(null);
					identificador = Convert.ToString(RrSQLRepetidos.Tables[0].Rows[IndiceRrSQLRepetidos]["gidenpac"]);
					contador = Convert.ToInt32(Double.Parse(identificador.Substring(4)));
					Scontador = StringsHelper.Format(contador + 1, "000000000");
					MFiliacion.codigo = MFiliacion.codigo + Scontador;
				}
			}
			if (MFiliacion.stModulo != "CITACION")
			{
				if (MFiliacion.stModulo == "URGENCIAS" && chbFiliacPov.CheckState == CheckState.Checked)
				{
					GRABAR_REGISTRO();
				}
				if (MFiliacion.stModulo == "CITARAPIDA")
				{
					GRABAR_REGISTRO();
					cbCancelar_Click(cbCancelar, new EventArgs());
				}
			}
		}
		private void MANDAR_DATOS_PROVISIONAL()
		{
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DNOMBPAC"]))
			{
				MFiliacion.Nombre2 = "";
			}
			else
			{
				MFiliacion.Nombre2 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DNOMBPAC"]);
			}
			
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DAPE1PAC"]))
			{
				MFiliacion.Apellido1 = "";
			}
			else
			{
				MFiliacion.Apellido1 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DAPE1PAC"]);
			}
			
			if (Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DAPE2PAC"]))
			{
				MFiliacion.Apellido2 = "";
			}
			else
			{
				MFiliacion.Apellido2 = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DAPE2PAC"]);
			}
			MFiliacion.fechaNac = "";
			MFiliacion.Sexo = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ITIPSEXO"]);
			MFiliacion.Domicilio = "";
			MFiliacion.NIF = "";
			MFiliacion.idPaciente = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GIDENPAC"]);
			MFiliacion.CodPostal = "";
			MFiliacion.Asegurado = "";
			MFiliacion.codPob = "";
			MFiliacion.Historia = "";
			if (!Convert.IsDBNull(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GSOCIEDA"]))
			{
				MFiliacion.CodigoSoc = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GSOCIEDA"]);
			}
			else
			{
				MFiliacion.CodigoSoc = "";
			}
			MFiliacion.Filprov = Convert.ToString(MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ifilprov"]);
			if (MFiliacion.stDevolverDatos != "NO")
			{
				MFiliacion.OBJETO2.Recoger_datos(MFiliacion.Nombre2, MFiliacion.Apellido1, MFiliacion.Apellido2, MFiliacion.fechaNac, MFiliacion.Sexo, MFiliacion.Domicilio, MFiliacion.NIF, MFiliacion.idPaciente, MFiliacion.CodPostal, MFiliacion.Asegurado, MFiliacion.codPob, MFiliacion.Historia, MFiliacion.CodigoSoc, MFiliacion.Filprov, MFiliacion.IndAcPen, MFiliacion.Inspec);
			}
			if (MFiliacion.stDevolverDatos != "NO")
			{
				MFiliacion.idPaciente = "";
			}
		}

		private void tbDPTelf1_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbDPTelf1_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii == 8)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				if (KeyAscii == 9)
				{
					if (KeyAscii == 0)
					{
						eventArgs.Handled = true;
					}
					return;
				}
				KeyAscii = 0;
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNHistoria_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbNHistoria_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNHistoria.SelectionStart = 0;
			tbNHistoria.SelectionLength = tbNHistoria.Text.Trim().Length;
		}

		private void tbNHistoria_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii < 48 || KeyAscii > 57)
			{
				if (KeyAscii != 8)
				{
					KeyAscii = 0;
				}
			}
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNHistoria_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbNombre_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbNombre_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNombre.SelectionStart = 0;
			tbNombre.SelectionLength = tbNombre.Text.Trim().Length;
		}
		private void tbNombre_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			if (KeyAscii == 42)
			{
				KeyAscii = 0;
				if (KeyAscii == 0)
				{
					eventArgs.Handled = true;
				}
				return;
			}
			if (KeyAscii == 39 || KeyAscii == 34)
			{
				KeyAscii = Serrores.SustituirComilla();
			}
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}

		private void tbNombre_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbNSS_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}

		private void tbNSS_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbNSS.SelectionStart = 0;
			tbNSS.SelectionLength = tbNSS.Text.Trim().Length;
		}

		private void tbNSS_Leave(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}
		private void Activar_buscar()
		{
			//Se activa el boton de buscar si se ha rellena
			//dato excepto el sexo
			if (chbFiliacPov.CheckState != CheckState.Checked)
			{
				cbBuscar.Enabled = tbApellido1.Text != "" || tbApellido2.Text != "" || tbNombre.Text != "" || tbNHistoria.Text != "" || MEBDNI.Text != "         " || tbNSS.Text != "" || sdcFechaNac.Text != "" || tbOtrosDocumentos.Text != "" || tbDPTelf1.Text != "" || tbTarjeta.Text != "" || tbCIPAutonomico.Text.Trim() != "" || tbCorreoElectronico.Text.Trim() != "";
				if (chbFiliacPov.CheckState == CheckState.Checked)
				{
					cbBuscar.Enabled = true;
				}
			}

		}
		public void limpiar_grid()
		{
			DFI120F1.DefInstance.sprFiliados.MaxRows = 0;
		}
		//UPGRADE_NOTE: (7001) The following declaration (LIMPIAR_FORMULARIO_tarjeta) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private void LIMPIAR_FORMULARIO_tarjeta()
		//{
				//LbSociedad.Text = "";
				//tbNombre.Text = "";
				//tbApellido1.Text = "";
				//tbApellido2.Text = "";
				//rbSexo[0].Checked = false;
				//rbSexo[1].Checked = false;
				//sdcFechaNac.Value = DateTime.Parse("");
				//tbNHistoria.Text = "";
				//MEBDNI.Text = "         ";
				//tbNSS.Text = "";
				//TbVersion.Text = "";
				//CodigoDeControl = -1;
				//Algoritmo = -1;
				//this.tbsocieda.Text = "";
				//LbSociedad.Text = "";
				//tbDPTelf1.Text = "";
				//tbOtrosDocumentos.Text = ""; //(maplaza)(03/05/2006)
				//tbCIPAutonomico.Text = "";
				//tbCorreoElectronico.Text = "";
				//
				//rbBuscar[0].Checked = false;
				//rbBuscar[1].Checked = false;
				//rbBuscar[2].Checked = false;
				//rbBuscar[3].Checked = false;
				//rbBuscar[5].Checked = false; //(maplaza)(03/05/2006)
				//rbBuscar[6].Checked = false; //Oscar C Octubre 2011 - Busqueda por CIP AUTONOMICO
				//rbBuscar[7].Checked = false; //Rub�n 09/2011 - Busqueda por C. E.
				//if (MFiliacion.stModulo != "CITARAPIDA")
				//{ //Si es cita r�pida no puede ser nuevo activo porque podr�a acceder sin ning�n dato
					//cbNuevo.Enabled = true;
				//}
				//cbAceptar.Enabled = false;
				//this.CBPacienteAnt.Enabled = true;
				//tbNombre.Enabled = true;
				//tbNombre.ReadOnly = false;
				//tbApellido1.Enabled = true;
				//tbApellido1.ReadOnly = false;
				//tbApellido2.Enabled = true;
				//tbApellido2.ReadOnly = false;
				//frmSexo.Enabled = true;
				//tbNHistoria.Enabled = true;
				//tbNHistoria.ReadOnly = false;
				//tbNSS.Enabled = true;
				//tbNSS.ReadOnly = false;
				//TbVersion.Enabled = true;
				//TbVersion.ReadOnly = false;
				//tbOtrosDocumentos.Enabled = true; //(maplaza)(03/05/2006)
				//tbOtrosDocumentos.ReadOnly = false; //(maplaza)(03/05/2006)
				//tbDPTelf1.Enabled = true;
				//tbDPTelf1.ReadOnly = false;
				//sdcFechaNac.Enabled = true;
				//tbCIPAutonomico.Enabled = true;
				//tbCIPAutonomico.ReadOnly = false;
				//tbCorreoElectronico.Enabled = true;
				//tbCorreoElectronico.ReadOnly = false;
				//this.FrmBuscar.Enabled = true;
		//}
		private void LIMPIAR_FORMULARIO()
		{
			LbSociedad.Text = "";
			tbNombre.Text = "";
			tbApellido1.Text = "";
			tbApellido2.Text = "";
			rbSexo[0].IsChecked = false;
			rbSexo[1].IsChecked = false;
            sdcFechaNac.NullableValue = null; 
			tbNHistoria.Text = "";
			MEBDNI.Text = "         ";
			tbNSS.Text = "";
			TbVersion.Text = "";
			CodigoDeControl = -1;
			Algoritmo = -1;
			this.tbsocieda.Text = "";
			LbSociedad.Text = "";
			tbDPTelf1.Text = "";
			tbTarjeta.Text = "";
			tbOtrosDocumentos.Text = ""; //(maplaza)(03/05/2006)
			tbCIPAutonomico.Text = "";
			tbCorreoElectronico.Text = "";

			rbBuscar[0].IsChecked = false;
			rbBuscar[1].IsChecked = false;
			rbBuscar[2].IsChecked = false;
			rbBuscar[3].IsChecked = false;
			rbBuscar[5].IsChecked = false; //(maplaza)(03/05/2006)
			rbBuscar[6].IsChecked = false; //Oscar C Octubre 2011 - Busqueda por CIP AUTONOMICO
			rbBuscar[7].IsChecked = false; //Rub�n 09/2014 - Busqueda por C. E.

			if (MFiliacion.stModulo != "CITARAPIDA")
			{ //Si es cita r�pida no puede ser nuevo activo porque podr�a acceder sin ning�n dato
				cbNuevo.Enabled = true;
			}
			cbAceptar.Enabled = false;
			this.CBPacienteAnt.Enabled = true;
			tbNombre.Enabled = true;
			tbNombre.IsReadOnly = false;
			tbApellido1.Enabled = true;
			tbApellido1.IsReadOnly = false;
			tbApellido2.Enabled = true;
			tbApellido2.IsReadOnly = false;
			frmSexo.Enabled = true;
			tbNHistoria.Enabled = true;
			tbNHistoria.IsReadOnly = false;
			tbNSS.Enabled = true;
			tbNSS.IsReadOnly = false;
			TbVersion.Enabled = true;
			TbVersion.IsReadOnly = false;
			tbOtrosDocumentos.Enabled = true; //(maplaza)(03/05/2006)
			tbOtrosDocumentos.IsReadOnly = false; //(maplaza)(03/05/2006)
			tbDPTelf1.Enabled = true;
			tbDPTelf1.IsReadOnly = false;
			tbCIPAutonomico.Enabled = true;
			tbCIPAutonomico.IsReadOnly = false;
			sdcFechaNac.Enabled = true;
			tbCorreoElectronico.Enabled = true;
			tbCorreoElectronico.IsReadOnly = false;
			this.FrmBuscar.Enabled = true;
		}
		private void GRABAR_REGISTRO()
		{

            SqlDataAdapter tempAdapter_2 = new SqlDataAdapter();
            SqlDataAdapter tempAdapter = new SqlDataAdapter("SELECT * FROM DPACIENT WHERE 1 = 2", MFiliacion.GConexion);
			MFiliacion.RrDPACIENT = new DataSet();
			tempAdapter.Fill(MFiliacion.RrDPACIENT);
            IndiceRrDPACIENT = 0;
			MFiliacion.RrDPACIENT.AddNew();

			MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GIDENPAC"] = MFiliacion.codigo.ToUpper();

			if (tbNombre.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DNOMBPAC"] = tbNombre.Text.Trim().ToUpper();
			}

			if (tbApellido1.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DAPE1PAC"] = tbApellido1.Text.Trim().ToUpper();
			}

			if (tbApellido2.Text != "")
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["DAPE2PAC"] = tbApellido2.Text.Trim().ToUpper();
			}
			
			if (!Convert.IsDBNull(sdcFechaNac.Value.Date))
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["fnacipac"] = sdcFechaNac.Value.Date.ToString();
			}
			MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["IEXITUSP"] = ("N").ToUpper();
			//SEXO
			//BUSCA LOS CODIGOS DE SEXO
			if (rbSexo[0].IsChecked)
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ITIPSEXO"] = MFiliacion.CodSHombre.ToUpper();
			}
			else
			{
				if (rbSexo[1].IsChecked)
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ITIPSEXO"] = MFiliacion.CodSMUJER.ToUpper();
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ITIPSEXO"] = MFiliacion.CodSINDET.ToUpper();
				}
			}
			if (!B_ALTA_PACIENTE)
			{
				if (MFiliacion.stModulo != "URGENCIAS")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GSOCIEDA"] = MFiliacion.CodSS;

					if (this.tbNSS.Text == "")
					{
						MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["nafiliac"] = DBNull.Value;
					}
					else
					{
						MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["nafiliac"] = this.tbNSS.Text;
					}
				}
			}
			else
			{


				if (this.tbsocieda.Text == "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GSOCIEDA"] = DBNull.Value;
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["GSOCIEDA"] = this.tbsocieda.Text;
				}
				if (this.tbNSS.Text == "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["nafiliac"] = DBNull.Value;
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["nafiliac"] = this.tbNSS.Text;
				}

				if (this.sdcFechaNac.Value.Date.ToString() == "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["fnacipac"] = DBNull.Value;
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["fnacipac"] = this.sdcFechaNac.Value.Date;
				}
				if (this.tbTarjeta.Text == "")
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ntarjeta"] = "";
				}
				else
				{
					MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ntarjeta"] = this.tbTarjeta.Text;
				}
				if (B_ALTA_PACIENTE && this.tbsocieda.Text.Trim() != "")
				{
					tempAdapter_2 = new SqlDataAdapter("SELECT * FROM Dentipac WHERE 1 = 2", MFiliacion.GConexion);
					RrDENTIPAC = new DataSet();
					tempAdapter_2.Fill(RrDENTIPAC);
                    IndiceRrDENTIPAC = 0;
					RrDENTIPAC.AddNew();
					RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["GIDENPAC"] = MFiliacion.codigo.ToUpper();
					if (this.tbsocieda.Text == "")
					{
						RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["GSOCIEDA"] = DBNull.Value;
					}
					else
					{						
						RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["GSOCIEDA"] = this.tbsocieda.Text.Trim();
					}
					if (this.tbNSS.Text == "")
					{
						RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["nafiliac"] = DBNull.Value;
					}
					else
					{						
						RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["nafiliac"] = this.tbNSS.Text.Trim();
					}
					if (this.TbVersion.Text == "")
					{					
						RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["nversion"] = DBNull.Value;
					}
					else
					{						
						RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["nversion"] = this.TbVersion.Text.Trim();
					}
					
					RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["ginspecc"] = DBNull.Value;
					RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["fmovimie"] = DateTime.Now;					
					RrDENTIPAC.Tables[0].Rows[IndiceRrDENTIPAC]["itituben"] = "T";
				}

			}
			if (B_ALTA_PACIENTE)
			{				
				MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ifilprov"] = ("N").ToUpper();
			}
			else
			{
				MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["ifilprov"] = ("S").ToUpper();
			}
			
			MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["FULTMOVI"] = DateTime.Now;
			//RrDPACIENT("gusuario") = IIf(Trim(fnQueryRegistro("USUARIO")) = "", Null, Trim(fnQueryRegistro("USUARIO")))
			MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["gusuario"] = (Serrores.VVstUsuarioApli.Trim() == "") ? null : Serrores.VVstUsuarioApli.Trim();
			MFiliacion.RrDPACIENT.Tables[0].Rows[IndiceRrDPACIENT]["FSISTEMA"] = DateTime.Now;
			
			SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
            tempAdapter.Update(MFiliacion.RrDPACIENT, MFiliacion.RrDPACIENT.Tables[0].TableName);
			IndiceRrDPACIENT = MFiliacion.RrDPACIENT.MoveLast(null);
			if (B_ALTA_PACIENTE && this.tbsocieda.Text.Trim() != "")
			{
				SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_2);
                tempAdapter_2.Update(RrDENTIPAC, RrDENTIPAC.Tables[0].TableName);
				IndiceRrDENTIPAC = RrDENTIPAC.MoveLast(null);
			}
			MANDAR_DATOS_PROVISIONAL();
			MFiliacion.stFiliacion = "SELECCION";
		}
		private void COMPROBAR_NUEVO()
		{
			this.cbNuevo.Enabled = !(tbApellido1.Text == "" || tbNombre.Text == "");
			if (this.chbFiliacPov.CheckState == CheckState.Checked)
			{
				this.cbNuevo.Enabled = true;
			}
		}

		private void tbTarjeta_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			Activar_buscar();
		}


		private void tbTarjeta_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			// Si se ha recibido un INTRO suponemos que se trata de una tarjeta
			// leida con el lector de tarjetas. Si la secuencia recibida contiene
			// un %B suponemos que se trata de la tarjeta sanitaria del INSALUD y
			// descomponemos la secuencia recibida

			if (KeyAscii == 13)
			{

				tbTarjeta.Text = tbTarjeta.Text.ToUpper();

				if (tbTarjeta.Text.IndexOf(stComienzoBanda) >= 0)
				{

					proFuncionTarjetas(1);

					valor_tarjeta = tbTarjeta.Text;

					if (iNumeroBandas == 1 && cbBuscar.Enabled)
					{
						bBusquedaConTarjeta = true;
						cbBuscar_Click(cbBuscar, new EventArgs());
					}

				}
				else
				{

					if (tbTarjeta.Text.StartsWith(stComienzoBanda.Substring(0, Math.Min(1, stComienzoBanda.Length))))
					{

						// Estamos leyendo otro tipo de tarjeta (probablemente la europea). Le pasamos el c�digo
						// directamente (porque est� en otro punto del texto recibido)

						proFuncionTarjetas(1, tbTarjeta.Text.Substring(78, Math.Min(6, tbTarjeta.Text.Length - 78)));

						valor_tarjeta = tbTarjeta.Text;

						if (iNumeroBandas == 1 && cbBuscar.Enabled)
						{
							bBusquedaConTarjeta = true;
							cbBuscar_Click(cbBuscar, new EventArgs());
						}

					}
					else
					{

						// Como leer� la segunda banda y lo pondr� en el campo,
						// pasamos de ello y ponemos lo que hab�amos tomado antes.

						proFuncionTarjetas(2);

						tbTarjeta.Text = valor_tarjeta;

						if (cbBuscar.Enabled)
						{
							bBusquedaConTarjeta = true;
							cbBuscar_Click(cbBuscar, new EventArgs());
						}

					}

				}

			}

			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}


		private void TarjetaInsalud()
		{

			int iComienzo1 = 0;
			string stNombre = String.Empty;
			int iBlanco2 = 0;

			// Se busca donde comienzan los datos de la primera pista
			int iComienzo = (tbTarjeta.Text.IndexOf(stComienzoBanda) + 1) + stComienzoBanda.Length - 1;

			if (iComienzo1 > iComienzo)
			{
				iComienzo = iComienzo1;
			}

			// Fecha de nacimiento y sexo
			int iA�oNac = Convert.ToInt32(1900 + Conversion.Val(tbTarjeta.Text.Substring(iComienzo + 11, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 11)))));
			int iMesNac = Convert.ToInt32(Conversion.Val(tbTarjeta.Text.Substring(iComienzo + 13, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 13)))));
			int iDiaNac = Convert.ToInt32(Conversion.Val(tbTarjeta.Text.Substring(iComienzo + 15, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 15)))));
			if (iDiaNac > 40)
			{
				iDiaNac -= 40;
				rbSexo[1].IsChecked = true;
			}
			else
			{
				rbSexo[0].IsChecked = true;
			}
			sdcFechaNac.Text = StringsHelper.Format(iDiaNac, "00") + "/" + StringsHelper.Format(iMesNac, "00") + "/" + 
			                   StringsHelper.Format(iA�oNac, "0000");

			// Apellidos y nombre
			if (tbTarjeta.Text.Substring(iComienzo + 34, Math.Min(1, tbTarjeta.Text.Length - (iComienzo + 34))) == "^")
			{
				stNombre = tbTarjeta.Text.Substring(iComienzo + 35, Math.Min(30, tbTarjeta.Text.Length - (iComienzo + 35)));
			}
			else
			{
				stNombre = tbTarjeta.Text.Substring(iComienzo + 25, Math.Min(30, tbTarjeta.Text.Length - (iComienzo + 25)));
			}
			stNombre = stNombre.Trim();
			string tempRefParam = "$";
			string tempRefParam2 = "�";
			stNombre = ReemplazarCaracteres(stNombre, ref tempRefParam, ref tempRefParam2);

			int iBlanco = (stNombre.IndexOf(' ') + 1);
			string DC1 = DosConsonantes(stNombre.Substring(iBlanco));
			string DC2 = tbTarjeta.Text.Substring(iComienzo + 9, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 9)));
			while (iBlanco > 0 && DC1 != DC2 && DC2.Trim() != "XX" && DC1.Trim() != "XX")
			{
				iBlanco = Strings.InStr(iBlanco + 1, stNombre, " ", CompareMethod.Binary);
				DC1 = DosConsonantes(stNombre.Substring(iBlanco));
				DC2 = tbTarjeta.Text.Substring(iComienzo + 9, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 9)));
			}

			tbApellido1.Text = stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length)).Trim();
			stNombre = stNombre.Substring(iBlanco, Math.Min(30, stNombre.Length - iBlanco));
			iBlanco = (stNombre.IndexOf(' ') + 1);
			if (iBlanco == 0)
			{
				tbNombre.Text = stNombre.Trim();
			}
			else
			{
				while (iBlanco > 0 && tbTarjeta.Text.Substring(iComienzo + 9, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 9))) != "XX" && DosConsonantes(stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length))) != tbTarjeta.Text.Substring(iComienzo + 9, Math.Min(2, tbTarjeta.Text.Length - (iComienzo + 9))))
				{
					iBlanco = Strings.InStr(iBlanco + 1, stNombre, " ", CompareMethod.Binary);
				}
				tbApellido2.Text = stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length));
				tbNombre.Text = stNombre.Substring(iBlanco, Math.Min(30, stNombre.Length - iBlanco));
			}
			// Si hay dos o mas palabras en el nombre y el segundo apellido terminaba en particula
			// se pasa la primera palabra del nombre al segundo apellido
			iBlanco = (tbNombre.Text.Trim().IndexOf(' ') + 1);
			if (iBlanco > 0)
			{
				iBlanco2 = 0;
				for (int i = tbApellido2.Text.Trim().Length; i >= 1; i--)
				{
					if (tbApellido2.Text.Trim().Substring(i - 1, Math.Min(1, tbApellido2.Text.Trim().Length - (i - 1))) == " ")
					{
						iBlanco2 = i;
						break;
					}
				}
				if (iBlanco2 > 0)
				{
					stNombre = tbApellido2.Text.Trim().Substring(iBlanco2);
					if (stNombre == "DE" || stNombre == "DEL" || stNombre == "LA" || stNombre == "LAS" || stNombre == "LOS" || stNombre == "EL" || stNombre == "SAN" || stNombre == "Y")
					{
						tbApellido2.Text = tbApellido2.Text + " " + tbNombre.Text.Substring(0, Math.Min(iBlanco - 1, tbNombre.Text.Length));
						tbNombre.Text = tbNombre.Text.Substring(iBlanco);
					}
				}
			}

			tbCIPAutonomico.Text = tbTarjeta.Text.Substring(iComienzo + 24, Math.Min(10, tbTarjeta.Text.Length - (iComienzo + 24)));

			tbTarjeta.Text = tbTarjeta.Text.Substring(iComienzo + 7, Math.Min(16, tbTarjeta.Text.Length - (iComienzo + 7)));

		}
        
		private string ReemplazarCaracteres(string cadena, ref string Buscar, ref string Reemplazo)
		{
			string Letra = String.Empty;
			StringBuilder CadenaAux = new StringBuilder();
			Buscar = Buscar.Trim();
			Reemplazo = Reemplazo.Trim();
			for (int i = 1; i <= cadena.Length; i++)
			{
				Letra = cadena.Substring(i - 1, Math.Min(1, cadena.Length - (i - 1)));
				if (Letra == "$")
				{
					CadenaAux.Append(Reemplazo);
				}
				else
				{
					CadenaAux.Append(Letra);
				}
			}
			return CadenaAux.ToString();
		}

		private string DosConsonantes(string Apellido)
		{
			StringBuilder result = new StringBuilder();
			string Letra = String.Empty;

			for (int i = 1; i <= Apellido.Length; i++)
			{

				Letra = Apellido.Substring(i - 1, Math.Min(1, Apellido.Length - (i - 1)));
				if ((Letra == "�") || (Strings.Asc(Letra[0]) == 36) || (Strings.Asc(Letra[0]) >= Strings.Asc('A') && Strings.Asc(Letra[0]) <= Strings.Asc('Z')))
				{

					if (Letra.Trim() != "")
					{

						if (Letra == "�" || (Strings.Asc(Letra[0]) == 36))
						{
							result.Append("X");
							if (result.ToString().Length == 2)
							{
								break;
							}
						}
						else
						{
							if (Regex.IsMatch(Letra, "[A, E, I, O, U, �, �, �, �, �]"))
							{
							}
							else
							{
								result.Append(Letra);
								if (result.ToString().Length == 2)
								{
									break;
								}
							}
						}

					}
				}
				else
				{
					if (Regex.IsMatch(Letra, "[A, E, I, O, U, �, �, �, �, �]"))
					{

					}
					else
					{
						result.Append("X");
						if (result.ToString().Length == 2)
						{
							break;
						}
					}

				}

			}

			// Si no se han encontrado dos consonantes se cogen las vocales
			if (result.ToString().Length < 2)
			{

				for (int i = 1; i <= Apellido.Length; i++)
				{

					Letra = Apellido.Substring(i - 1, Math.Min(1, Apellido.Length - (i - 1)));

					if (Strings.Asc(Letra[0]) >= Strings.Asc('A') && Strings.Asc(Letra[0]) <= Strings.Asc('Z'))
					{

						if (Letra.Trim() != "")
						{

							if (Regex.IsMatch(Letra, "[A, E, I, O, U, �, �, �, �, �]"))
							{
								result.Append(Letra);
								if (result.ToString().Length == 2)
								{
									break;
								}
							}

						}

					}

				}

			}

			// Si el apellido tiene menos de dos letras se completa con X
			if (result.ToString().Length < 1)
			{
				result = new StringBuilder("XX");
			}
			else
			{
				if (result.ToString().Length < 2)
				{
					result.Append("X");
				}
			}

			return result.ToString();
		}

		private void proFuncionTarjetas(int iBanda, string paramCodigo = "")
		{

			int imensaje = 0;
			string stCodigo = String.Empty;

			// Seg�n la banda, el c�digo habr� que tomarlo en un punto u otro

			if (paramCodigo.Trim() == "")
			{
				if (iBanda == 1 && tbTarjeta.Text != string.Empty)
				{
					stCodigo = tbTarjeta.Text.Substring(1 + stComienzoBanda.Length - 1, Math.Min(6, tbTarjeta.Text.Length - (1 + stComienzoBanda.Length - 1)));
				}
				else if (iBanda == 2 && tbTarjeta.Text != string.Empty)
				{ 
					stCodigo = tbTarjeta.Text.Substring(1, Math.Min(6, tbTarjeta.Text.Length - 1));
				}
			}
			else
			{
				stCodigo = paramCodigo;
			}

			BinPan = stCodigo;

			// Tomamos el c�digo de la tarjeta para ver si est� en la BD

			string StSql = "select * from DTIPTARJ where gtarjeta = '" + stCodigo + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, MFiliacion.GConexion);
			DataSet RrTarjeta = new DataSet();
			tempAdapter.Fill(RrTarjeta);


			if (RrTarjeta.Tables[0].Rows.Count == 0)
			{
				imensaje = Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje("Filiaci�n", "", 3992, MFiliacion.GConexion, stCodigo));

				tbTarjeta.Text = "";
				LbSociedad.Text = "";
				return;
			}
            LbSociedad.Text = Convert.ToString(RrTarjeta.Tables[0].Rows[0]["dtarjeta"]);
			// Si la tarjeta es del insalud, la tratamos de forma distinta

			if (Convert.ToString(RrTarjeta.Tables[0].Rows[0]["iinsalud"]).Trim().ToUpper() == "S" && iBanda == 1)
			{
				this.tbsocieda.Text = MFiliacion.CodSS.ToString();
				TarjetaInsalud();
				return;
			}

			// En funci�n de los datos, analizaremos la sentencia de una u otra forma

			proDatosTarjeta(RrTarjeta, iBanda);

			// Cerramos el identificador
			RrTarjeta.Close();
		}

		private void proDatosTarjeta(DataSet RrTarjeta, int iBanda)
		{

			string stNombre = String.Empty;
			string stSeparador = String.Empty;
			string stSitio = String.Empty;
			string stNumTarjeta = String.Empty;
			int iBlanco = 0;
			int iBlanco2 = 0;

			// Vemos el separador de los nombres y d�nde dejar la tarjeta
            if (Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["iseparad"]))
			{
				stSeparador = " ";
			}
			else
			{
				stSeparador = Convert.ToString(RrTarjeta.Tables[0].Rows[0]["iseparad"]);
			}

			if (Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["iloctarj"]))
			{
				stSitio = "P";
			}
			else
			{
				stSitio = Convert.ToString(RrTarjeta.Tables[0].Rows[0]["iloctarj"]);
			}

			// Comprobamos si en esta pasada tenemos que buscar la tarjeta

			this.tbsocieda.Text = Convert.ToString(RrTarjeta.Tables[0].Rows[0]["gsocieda"]) + "";

			if (Convert.ToDouble(RrTarjeta.Tables[0].Rows[0]["itarjeta"]) == iBanda)
			{

				// Tomamos el n�mero de tarjeta del asegurado, aunque lo que haremos ser� dejarlo al
				// final en el campo de texto de la tarjeta.

				if (stSitio != "T")
				{
					tbNSS.Text = tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longtarj"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1)));

					// Si la tarjeta es de Adeslas, la tratamos de otra forma

					if (Convert.ToString(RrTarjeta.Tables[0].Rows[0]["iadeslas"]).Trim().ToUpper() == "S")
					{
						tbNSS.Text = tbNSS.Text.Substring(0, Math.Min(8, tbNSS.Text.Length)) + tbNSS.Text.Substring(10);
					}

					stNumeroLeidoTarjeta = tbNSS.Text;
					stNumTarjeta = "";
				}
				else
				{
					stNumTarjeta = tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longtarj"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iinitarj"]) - 1)));
				}
			}


			//Cogemos el numero de version

			if (Convert.ToDouble(RrTarjeta.Tables[0].Rows[0]["iversio"]) == iBanda && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["initver"]) && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["longver"]))
			{
				TbVersion.Text = tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["initver"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longver"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["initver"]) - 1)));
				stVersionLeidaTarjeta = TbVersion.Text;
			}

			if (Convert.ToDouble(RrTarjeta.Tables[0].Rows[0]["icontro"]) == iBanda && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["initcon"]) && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["longcon"]) && !Convert.IsDBNull(RrTarjeta.Tables[0].Rows[0]["idcontrol"]))
			{
				CodigoDeControl = Convert.ToInt32(Double.Parse(tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["initcon"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longcon"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["initcon"]) - 1)))));
				Algoritmo = Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["idcontrol"]);
			}


			// Comprobamos si en esta pasada tenemos que buscar el nombre

			if (Convert.ToDouble(RrTarjeta.Tables[0].Rows[0]["inombrep"]) == iBanda)
			{

				// Tomamos ahora el nombre y apellidos

				stNombre = tbTarjeta.Text.Substring(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iininomb"]) - 1, Math.Min(Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["longnomb"]), tbTarjeta.Text.Length - (Convert.ToInt32(RrTarjeta.Tables[0].Rows[0]["iininomb"]) - 1)));

				// En funci�n de la forma en que venga el nombre, separamos nombre y apellidos

				if (Convert.ToString(RrTarjeta.Tables[0].Rows[0]["itipnomb"]).Trim().ToUpper() == "A")
				{
					iBlanco = (stNombre.IndexOf(stSeparador) + 1);

					if (iBlanco <= 4)
					{
						switch(stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length)).ToUpper())
						{
							case "DE" : case "DEL" : case "LA" : case "LAS" : case "LOS" : case "EL" : case "SAN" : case "Y" : 
								iBlanco = Strings.InStr(iBlanco + 1, stNombre, stSeparador, CompareMethod.Binary); 
								break;
						}
					}

					tbApellido1.Text = stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length));
					stNombre = stNombre.Substring(iBlanco, Math.Min(30, stNombre.Length - iBlanco));

					iBlanco = (stNombre.IndexOf(stSeparador) + 1);

					if (iBlanco <= 4)
					{
						switch(stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length)).ToUpper())
						{
							case "DE" : case "DEL" : case "LA" : case "LAS" : case "LOS" : case "EL" : case "SAN" : case "Y" : 
								iBlanco = Strings.InStr(iBlanco + 1, stNombre, stSeparador, CompareMethod.Binary); 
								break;
						}
					}

					tbApellido2.Text = stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length));

					tbNombre.Text = stNombre.Substring(iBlanco).Trim();
				}
				else
				{
					iBlanco = (stNombre.IndexOf(stSeparador) + 1);
					iBlanco2 = Strings.InStr(iBlanco + 1, stNombre, stSeparador, CompareMethod.Binary);

					if (iBlanco2 - iBlanco < 3)
					{
						iBlanco = iBlanco2;
					}

					tbNombre.Text = stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length));

					stNombre = stNombre.Substring(iBlanco, Math.Min(30, stNombre.Length - iBlanco));

					iBlanco = (stNombre.IndexOf(stSeparador) + 1);

					if (iBlanco <= 4)
					{
						switch(stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length)).ToUpper())
						{
							case "DE" : case "DEL" : case "LA" : case "LAS" : case "LOS" : case "EL" : case "SAN" : case "Y" : 
								iBlanco = Strings.InStr(iBlanco + 1, stNombre, stSeparador, CompareMethod.Binary); 
								break;
						}
					}

					tbApellido1.Text = stNombre.Substring(0, Math.Min(iBlanco - 1, stNombre.Length));

					tbApellido2.Text = stNombre.Substring(iBlanco).Trim();
				}
			}
			tbTarjeta.Text = stNumTarjeta;
		}


		//OSCAR C FEB 2006
		private void proRellenaDatosRULEQ()
		{

			string CodH = Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "SHOMBRE", "VALFANU3");
			string CodM = Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "SMUJER", "VALFANU3");

			int pos = (MFiliacion.stModulo.IndexOf('#') + 1);
			string stFACTIVOS_ID = MFiliacion.stModulo.Substring(pos);
			string sql = "SELECT SX, FN, NOMBRE, APELLIDO1, APELLIDO2" + 
			             " FROM RULEQ_IN WHERE FACTIVOS_ID='" + stFACTIVOS_ID + "' AND ACCION_TX = 'CA'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{
				tbApellido1.Text = (Convert.ToString(RR.Tables[0].Rows[0]["Apellido1"]) + "").Trim().ToUpper();
				tbApellido2.Text = (Convert.ToString(RR.Tables[0].Rows[0]["Apellido2"]) + "").Trim().ToUpper();
				tbNombre.Text = (Convert.ToString(RR.Tables[0].Rows[0]["nombre"]) + "").Trim().ToUpper();
				if (!Convert.IsDBNull(RR.Tables[0].Rows[0]["fn"]))
				{
					sdcFechaNac.Text = Convert.ToDateTime(RR.Tables[0].Rows[0]["fn"]).ToString("dd/MM/yyyy");
				}
				if (Convert.ToString(RR.Tables[0].Rows[0]["SX"]) + "" == CodH)
				{
					rbSexo[0].IsChecked = true;
				}
				else if (Convert.ToString(RR.Tables[0].Rows[0]["SX"]) + "" == CodM)
				{ 
					rbSexo[1].IsChecked = true;
				}
			}
			RR.Close();
			cbBuscar_Click(cbBuscar, new EventArgs());
		}

		private bool ComprobarControl(string b, int Control, int Algoritmo, string ParamVersion, string Sociedad)
		{
			bool result = false;
			string poliza = String.Empty;
			string a = b;

			int Suma = 0, LHUN_Sanitario = 0;
			if (Algoritmo == 1)
			{
				double dbNumericTemp = 0;
				if (Double.TryParse(a, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out dbNumericTemp) && 0 == (a.IndexOf('.') + 1))
				{
					result = true;
					poliza = a.Substring(Math.Max(6, 0));
					a = a.Substring(0, Math.Min(a.Length - 1, a.Length));

					if (a != "" && !Convert.IsDBNull(a))
					{
						for (int Conta = a.Length; Conta >= 1; Conta--)
						{
							if ((Conta - a.Length + 1) % 2 != 0)
							{
								Suma = Convert.ToInt32(Suma + (2 * Double.Parse(a.Substring(Conta - 1, Math.Min(1, a.Length - (Conta - 1))))));
							}
							else
							{
								Suma = Convert.ToInt32(Suma + Double.Parse(a.Substring(Conta - 1, Math.Min(1, a.Length - (Conta - 1)))));
							}
						}
						LHUN_Sanitario = ((((int) ((((double) (Suma / ((int) 10))) > 0) ? Math.Floor((double) (Suma / ((int) 10))) : Math.Ceiling((double) (Suma / ((int) 10))))) + 1) * 10) - Suma;
						if (LHUN_Sanitario == 10)
						{
							LHUN_Sanitario = 0;
						}

						if (LHUN_Sanitario != Control)
						{
							result = false;

						}

					}
				}
				else
				{
					result = false;
				}
			}
			return result;
		}
        
		private void proRellenaDatosUsuWeb()
		{

			string stHombre = Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "SHOMBRE", "VALFANU1");
			string stMujer = Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "SMUJER", "VALFANU1");

			int intContador = (MFiliacion.stModulo.IndexOf('#') + 1);

			string stFecSolic = MFiliacion.stModulo.Substring(intContador, Math.Min(19, MFiliacion.stModulo.Length - intContador));
			string stCodUsuar = MFiliacion.stModulo.Substring(intContador + 19);

			string strSql = "SELECT CSUSCWEB.dnombpac, CSUSCWEB.dape1pac, CSUSCWEB.dape2pac, CSUSCWEB.fnacipac, CSUSCWEB.itipsexo " + 
			                "From CSUSCWEB " + 
			                "WHERE fsolicit = " + Serrores.FormatFechaHMS(stFecSolic) + " AND " + 
			                "codusuar = '" + stCodUsuar + "' AND " + 
			                "festsoli IS NULL AND " + 
			                "iestsoli = 'P' ";
			
			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);

			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				tbApellido1.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pac"]) + "").Trim().ToUpper();
				tbApellido2.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape2pac"]) + "").Trim().ToUpper();
				tbNombre.Text = (Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpac"]) + "").Trim().ToUpper();
				sdcFechaNac.Text = Convert.ToDateTime(rdoTemp.Tables[0].Rows[0]["fnacipac"]).ToString("dd/MM/yyyy");

				string switchVar = rdoTemp.Tables[0].Rows[0]["itipsexo"].ToString();
				if (switchVar == stHombre)
				{
					rbSexo[0].IsChecked = true;
				}
				else if (switchVar == stMujer)
				{ 
					rbSexo[1].IsChecked = true;
				}

			}
			rdoTemp.Close();
			cbBuscar_Click(cbBuscar, new EventArgs());
		}


		private void sprFiliados_KeyUp(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;
			sprFiliados_CellClick(sprFiliados, null);
		}
		//-------------------------------------
		private bool HistoriaActiva()
		{

			//Comprobamos que si se busca por historia y si la historia ha estado utilizada en una fusi�n y cu�l es la actual activa.
            
			bool bSalir = false;

			bool btest = false;

			string stHistoActiva = "";

			StringBuilder stHistoRevisadas = new StringBuilder();
			stHistoRevisadas.Append(tbNHistoria.Text.Trim());

			bool bHistoriaEliminada = false;

			//Buscamos en la tablas de hist�rico de historias con el n�mero marcado.

			string stDatos = "select * from hhistcar where ghistant = " + tbNHistoria.Text.Trim();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(stDatos, MFiliacion.GConexion);
			DataSet tRrDatos = new DataSet();
            Int32 IndicetRrDatos = 0;

            tempAdapter.Fill(tRrDatos);
            IndicetRrDatos = 0;
			if (tRrDatos.Tables[0].Rows.Count != 0)
			{

				//Si existen registros....nos movemos al final.

				IndicetRrDatos = tRrDatos.MoveLast(null);
				if (tRrDatos.Tables[0].Rows.Count == 1)
				{

					//Si s�lo hay un registro que no sea de los eliminados...

					//Si el registro �nico se elimin�, se sacar� un mensaje, fuera de esta funci�n, gen�rico de no filiaci�n con las caracter�sticas especificadas.
                    
					if (Convert.ToString(tRrDatos.Tables[0].Rows[IndicetRrDatos]["itipcamb"]) != "E")
					{

						//guardamos la nueva, como activa.
						stHistoActiva = Convert.ToString(tRrDatos.Tables[0].Rows[IndicetRrDatos]["ghistnue"]);

						do 
						{

							//Para evitar que se quede en el bucle, guardamos todas las historias antiguas, si la nueva est�, nos salimos.

							if (stHistoRevisadas.ToString().IndexOf(Convert.ToString(tRrDatos.Tables[0].Rows[IndicetRrDatos]["ghistnue"])) >= 0)
							{
								break;
							}

							//Guardamos la nueva

							stHistoRevisadas.Append("," + Convert.ToString(tRrDatos.Tables[0].Rows[IndicetRrDatos]["ghistnue"]));

							//COMPROBAMOS SI LA HISTORIA NUEVA, EN LA QUE SE FUSION� LA ANTIGUA, TIENE OTRA FUSI�N

							stDatos = stDatos.Substring(0, Math.Min((stDatos.IndexOf('=') + 1) + 1, stDatos.Length)) + "( " + "select ghistnue " + stDatos.Substring(stDatos.IndexOf("from")) + ")";

							SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stDatos, MFiliacion.GConexion);
							tRrDatos = new DataSet();
							tempAdapter_2.Fill(tRrDatos);
                            IndicetRrDatos = 0;

							//Nos movemos al final.

							IndicetRrDatos = tRrDatos.MoveLast(null);
                            //Actualizamos la variable para salir. Mientra siga siendo 1,o sea, mientras

							//la nueva siga teniendo otra fusi�n m�s, nos quedamos. Si ya no hay m�s registros

							//o existen m�s de uno, nos salimos.

							bSalir = tRrDatos.Tables[0].Rows.Count != 1;

							//guardamos la nueva, como activa.

							if (tRrDatos.Tables[0].Rows.Count == 1)
							{
								if (Convert.IsDBNull(tRrDatos.Tables[0].Rows[IndicetRrDatos]["ghistnue"]))
								{
									//el ultimo movimiento es eliminaci�n jesus
									bSalir = true;
									bHistoriaEliminada = true;
								}
								else
								{
									stHistoActiva = Convert.ToString(tRrDatos.Tables[0].Rows[IndicetRrDatos]["ghistnue"]).Trim();
								}
							}
						}
						while(!bSalir);

						//Salimos del bucle, si ya no hay fila, hemos encontrado la historia activa, o hay m�s de uno, mal asunto porque se ha reutilizado la historia.

						if (tRrDatos.Tables[0].Rows.Count == 0)
						{	
							MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli,"", 1130, MFiliacion.GConexion, "mostrar los", "la historia introducida." + "\n" + "\r" + "Introduzca mejor la historia activa siguente " + stHistoActiva));
                            tbNHistoria.Text = stHistoActiva;
                            tbNHistoria.Focus();

							btest = true;
						}
						else if (!bHistoriaEliminada)
						{ 

							//Si hay m�s de un registro, con esta historia antigua, significa que se ha reutilizado dicha historia.

							//Con lo cual, no se puede saber qu� historia final activa es la buena.
                            MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1200, MFiliacion.GConexion, "En el hist�rico de esa historia hay alguna historia que", " reutilizada m�s veces"));
							tbNHistoria.Focus();

							btest = true;

						}

					}

				}
				else
				{
					//Si hay m�s de un registro, con esta historia antigua, significa que se ha reutilizado dicha historia.
                    //Con lo cual, no se puede saber qu� historia final activa es la buena.
                    MFiliacion.irespuesta = (DialogResult) Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, "", 1200, MFiliacion.GConexion, "La historia", " reutilizada m�s veces"));
					tbNHistoria.Focus();
                    btest = true;
				}
			}
			else
			{
                //Si no existen, se saca un mensaje,fuera de esta funci�n gen�rico de no filiaci�n con las caracter�sticas especificadas.
			}

			tRrDatos = null;
			return btest;
		}

        private void DFI120F1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            string usuario = (Serrores.VVstUsuarioApli.Trim() == "") ? Convert.ToString(DBNull.Value) : Serrores.VVstUsuarioApli.Trim();
            ProcesoIMDHOpen.clsIMDHOpen IMDHOPEN = null;
            IMDHOPEN = new ProcesoIMDHOpen.clsIMDHOpen();
            int hotKey = IMDHOPEN.TeclaFuncionOpen(MFiliacion.GConexion);
            if (e.KeyCode == (Keys)Enum.Parse(typeof(Keys), hotKey.ToString()))
            {
                IMDHOPEN.LlamadaSP(MFiliacion.GConexion, usuario, this.Name, "TECLAFUNCION", MFiliacion.ModCodigo, "", true, 0, 0);
                IMDHOPEN = null;
            }
        }

        /// <summary>
        /// INDRA jproche Necesario para activar el evento DoubleClick sobre el control RadioButton 
        /// </summary>
        private void activarDoblrClickRadioButton()
        {
            MethodInfo m = typeof(RadioButton).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
            if (m != null)
            {
                m.Invoke(_rbSexo_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbSexo_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_0, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_1, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_2, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_3, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_4, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_5, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_6, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
                m.Invoke(_rbBuscar_7, new object[] { ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true });
            }

            _rbSexo_0.MouseDoubleClick += rbSexo_DblClick;
            _rbSexo_1.MouseDoubleClick += rbSexo_DblClick;
            _rbBuscar_0.MouseDoubleClick += rbBuscar_DblClick;
            _rbBuscar_1.MouseDoubleClick += rbBuscar_DblClick;
            _rbBuscar_2.MouseDoubleClick += rbBuscar_DblClick;
            _rbBuscar_3.MouseDoubleClick += rbBuscar_DblClick;
            _rbBuscar_4.MouseDoubleClick += rbBuscar_DblClick;
            _rbBuscar_5.MouseDoubleClick += rbBuscar_DblClick;
            _rbBuscar_6.MouseDoubleClick += rbBuscar_DblClick;
            _rbBuscar_7.MouseDoubleClick += rbBuscar_DblClick;
        }
    }
}
 
 