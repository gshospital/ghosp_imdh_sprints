using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace filiacionE
{
	public partial class DFI111F3
      : Telerik.WinControls.UI.RadForm
    {

		bool carga = false;
		public DFI111F3()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


        private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
        {
            
            if (lblCIAS.Text == "")
            {
                DFI111F1.DefInstance.MEBCIAS.Text = "  ";
            }
            else
            {
                DFI111F1.DefInstance.MEBCIAS.Text = lblCIAS.Text;
            }
            //OSCAR C Noviembre 2009
            string sqlAux = "select DHOSPITA.ghospita, DHOSPITA.dnomhosp from " +
                            " DHOSPITA inner join DCENSALU on DCENSALU.ghospref = DHOSPITA.ghospita " +
                            " WHERE DCENSALU.gcensalu= " + cbbCentro.Items[cbbCentro.SelectedIndex].Value.ToString();
       
            SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlAux, MFiliacion.GConexion);
			DataSet RRAux = new DataSet();
			tempAdapter.Fill(RRAux);
			if (RRAux.Tables[0].Rows.Count != 0)
			{


                if (!Convert.IsDBNull(RRAux.Tables[0].Rows[0]["ghospita"]))
                {
					
                 
			DFI111F1.DefInstance.cbbHospitalReferencia.SelectedIndex = Serrores.fnBuscaListIndexID(DFI111F1.DefInstance.cbbHospitalReferencia, RRAux.Tables[0].Rows[0]["ghospita"].ToString());
				}
			}
			
			RRAux.Close();
			//--------------()
			this.Close();
		}

		
		private bool isInitializingComponent;
		private void cbbCentro_TextChanged(Object eventSender, EventArgs eventArgs)
		{
			if (isInitializingComponent)
			{
				return;
			}
			cbbCentro_SelectedIndexChanged(cbbCentro, new EventArgs());
		}

		private void cbbCentro_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
            cbbmedico_centro.Items.Clear();

            if (cbbCentro.SelectedIndex >= 0)
            {
                string SQLcias = "select * from dcodciasva Where gcensalu  = " + cbbCentro.Items[cbbCentro.SelectedIndex].Value.ToString() + " order by dnombmed";
                SqlDataAdapter tempAdapter = new SqlDataAdapter(SQLcias, MFiliacion.GConexion);
                DataSet RrCodCias = new DataSet();
                tempAdapter.Fill(RrCodCias);
                if (RrCodCias.Tables[0].Rows.Count != 0)
                {

                    for (int i = 0; i <= RrCodCias.Tables[0].Rows.Count - 1; i++)
                    {

                        cbbmedico_centro.Items.Add((new RadListDataItem(RrCodCias.Tables[0].Rows[i]["dnombmed"].ToString(), RrCodCias.Tables[0].Rows[i]["gcodcias"])));

                    }
                }
                if (cbbmedico_centro.Items.Count != 0)
                {
                    cbbmedico_centro.SelectedIndex = 0;
                }
                else
                {
                    this.lblCIAS.Text = "";
                }
                COMPROBAR_DATOS();

                //OSCAR C Noviembre 2009
                string sqlAux = "select DHOSPITA.ghospita, DHOSPITA.dnomhosp from " +
                                " DHOSPITA inner join DCENSALU on DCENSALU.ghospref = DHOSPITA.ghospita " +
                                " WHERE DCENSALU.gcensalu= " + cbbCentro.Items[cbbCentro.SelectedIndex].Value.ToString();
                SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlAux, MFiliacion.GConexion);
                DataSet RRAux = new DataSet();
                tempAdapter_2.Fill(RRAux);
                if (RRAux.Tables[0].Rows.Count != 0)
                {

                    Label4.Text = (Convert.ToString(RRAux.Tables[0].Rows[0]["dnomhosp"]).Trim() + "").ToUpper();
                }

                RRAux.Close();
                //--------------
            }   
		}

		private void cbbmedico_centro_SelectedIndexChanged(Object eventSender, EventArgs eventArgs)
		{
		
			object tempRefParam = 1;
            object tempRefParam2 = cbbmedico_centro.get_ListIndex();

            this.lblCIAS.Text = Convert.ToString(this.cbbmedico_centro.get_Column( tempRefParam, tempRefParam2));
			COMPROBAR_DATOS();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}
        
		private void DFI111F3_Load(Object eventSender, EventArgs eventArgs)
		{
			string SQLcias = String.Empty;
			DataSet RrCodCias = null;
           cbbmedico_centro.ColumnWidths= "20;0";

            cbbCentro.Items.Clear();
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select * from DCENSALU ORDER BY DNOMCENS", MFiliacion.GConexion);
			DataSet RrCensalu = new DataSet();
			tempAdapter.Fill(RrCensalu);
			
			foreach (DataRow iteration_row in RrCensalu.Tables[0].Rows)
			{
				cbbCentro.Items.Add(Convert.ToString(iteration_row["DNOMCENS"]).Trim());
				cbbCentro.SetItemData(cbbCentro.GetNewIndex(), (iteration_row["GCENSALU"]));
			}
			
			RrCensalu.Close();

           if (DFI111F1.DefInstance.MEBCIAS.Text.Trim() == "")
			{
				if (cbbCentro.SelectedIndex != -1)
				{
					cbbCentro.SelectedIndex = 0;
				}
			}
			else
			{
				SQLcias = "select gcensalu from dcodciasva Where gcodcias  = '" + DFI111F1.DefInstance.MEBCIAS.Text + "'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(SQLcias, MFiliacion.GConexion);
				RrCodCias = new DataSet();
				tempAdapter_2.Fill(RrCodCias);
				if (RrCodCias.Tables[0].Rows.Count != 0)
				{
					for (int i = 0; i <= cbbCentro.Items.Count - 1; i++)
					{
                       
                        if (cbbCentro.Items[i].Value.ToString() == RrCodCias.Tables[0].Rows[0]["gcensalu"].ToString())
                        {
							cbbCentro.SelectedIndex = i;
							break;
						}
					}
				}

            for (int i = 0; i <= cbbmedico_centro.ListCount - 1; i++)
            {
               
                object tempRefParam = 1;
                object tempRefParam2 = i;
             
                if (DFI111F1.DefInstance.MEBCIAS.Text.Trim() == Convert.ToString(cbbmedico_centro.get_Column(tempRefParam, tempRefParam2)))
                {
                    i = Convert.ToInt32(tempRefParam2);
                    cbbmedico_centro.set_ListIndex(i);
                    break;
                }
                else
                {
                i = Convert.ToInt32(tempRefParam2);
            }
        }
        
        COMPROBAR_DATOS();

    }
            }

        private void COMPROBAR_DATOS()
		{
            if (cbbCentro.SelectedIndex != -1)
            {
                if (Convert.ToDouble(this.cbbmedico_centro.get_ListIndex()) != -1)
                {
                    cbAceptar.Enabled = lblCIAS.Text != "";
                }
                else
                {
                    cbAceptar.Enabled = false;
                }
            }
            else
            {
                cbAceptar.Enabled = false;
            }
        }
        private void DFI111F3_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}
 