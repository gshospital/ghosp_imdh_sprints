using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace filiacionE
{
	public partial class DFI111F6
		: Telerik.WinControls.UI.RadForm
	{

		public DFI111F6()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void DFI111F6_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int imensaje = 0;
			if (cbbMotivoFiliacion.SelectedIndex == -1)
			{
				string tempRefParam = "Filiación";
				string tempRefParam2 = "";
				short tempRefParam3 = 1210;
				string[] tempRefParam4 = new string[]{"Motivo de Filiación"};
                imensaje = Convert.ToInt32(MFiliacion.clasemensaje.RespuestaMensaje( tempRefParam,  tempRefParam2,  tempRefParam3,  MFiliacion.GConexion,  tempRefParam4));
				return;
			}
			DFI111F1.DefInstance.bMotivoFiliacion = true;
            DFI111F1.DefInstance.iMotivoFiliacion = Convert.ToInt32(cbbMotivoFiliacion.Items[cbbMotivoFiliacion.SelectedIndex].Value.ToString());
			DFI111F1.DefInstance.oMotivoFiliacion = tbComentarioFiliacion.Text.Trim();
			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			DFI111F1.DefInstance.bMotivoFiliacion = false;
			this.Close();
		}

		private void DFI111F6_Load(Object eventSender, EventArgs eventArgs)
		{
			//Carga la tabla de Motivos de filiacion
			SqlDataAdapter tempAdapter = new SqlDataAdapter("select * from AMOTIFIL  order by DMOTIFIL", MFiliacion.GConexion);
			DataSet rrMotivos = new DataSet();
			tempAdapter.Fill(rrMotivos);
			rrMotivos.MoveFirst();
			foreach (DataRow iteration_row in rrMotivos.Tables[0].Rows)
			{
                cbbMotivoFiliacion.Items.Add(new RadListDataItem(Strings.StrConv(Convert.ToString(iteration_row["DMOTIFIL"]).Trim(), VbStrConv.ProperCase, 0), Convert.ToInt32(iteration_row["GMOTIFIL"])));
			}
			rrMotivos.Close();
			cbbMotivoFiliacion.SelectedIndex = -1;
			if (DFI111F1.DefInstance.iMotivoFiliacion != -1)
			{
				cbbMotivoFiliacion.SelectedIndex = Serrores.fnBuscaListIndexID(cbbMotivoFiliacion, DFI111F1.DefInstance.iMotivoFiliacion.ToString());
			}
			if (DFI111F1.DefInstance.oMotivoFiliacion.Trim() != "")
			{
				tbComentarioFiliacion.Text = DFI111F1.DefInstance.oMotivoFiliacion.Trim();
			}
		}

		private void tbComentarioFiliacion_Enter(Object eventSender, EventArgs eventArgs)
		{
			tbComentarioFiliacion.SelectionStart = 0;
			tbComentarioFiliacion.SelectionLength = tbComentarioFiliacion.Text.Trim().Length;
		}

		private void tbComentarioFiliacion_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
		{
			int KeyAscii = Strings.Asc(eventArgs.KeyChar);
			Serrores.ValidarTecla(ref KeyAscii);
			if (KeyAscii == 0)
			{
				eventArgs.Handled = true;
			}
			eventArgs.KeyChar = Convert.ToChar(KeyAscii);
		}
		private void DFI111F6_Closed(Object eventSender, EventArgs eventArgs)
		{
			MemoryHelper.ReleaseMemory();
		}
	}
}