using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace filiacionE
{
	public partial class DFI111F5
		: Telerik.WinControls.UI.RadForm
    {

		public DFI111F5()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void DFI111F5_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		//Constantes para los n�meros de columna del spread
		int Col_DSocieda = 0;
		int Col_NAfiliac = 0;
		int Col_TitBenef = 0;
		int Col_Cobertura = 0;
		int Col_FechaAlta = 0;
		int Col_FechaCad = 0;
		int Col_FBorrado = 0;
		int Col_GSocieda = 0;
		int Col_FBorradoSoc = 0;
		int Col_FDesactiSoc = 0;
		int Col_GInspecc = 0;
		int Col_TipoEntidad = 0;
		int Col_IconIVA = 0;
		//Dim BEsconderIconIVA As Boolean
		int fila_seleccionada = 0;

		private void cmdActivar_Click(Object eventSender, EventArgs eventArgs)
		{

			string stSql = String.Empty;
			DataSet RrSQL = null;
			string stCodSociedad = String.Empty;
			string stCodInspeccion = String.Empty;


			try
			{

				this.Cursor = Cursors.WaitCursor;

				sprSociedades.Row = sprSociedades.ActiveRowIndex;
				sprSociedades.Col = Col_GSocieda;
				stCodSociedad = sprSociedades.Text.Trim();

				sprSociedades.Col = Col_GInspecc;
				stCodInspeccion = sprSociedades.Text.Trim();

				sprSociedades.Col = Col_TipoEntidad;
				switch(sprSociedades.Text.Trim())
				{
					case "S" : 
						stSql = "Select gidenpac, gsocieda, fborrado From DENTIPAC Where Gidenpac='" + MFiliacion.ModCodigo + "' And GSocieda=" + stCodSociedad; 
						break;
					case "I" : 
						stSql = "Select gidenpac, gsocieda, ginspecc, fborrado From DINSHPAC Where Gidenpac='" + MFiliacion.ModCodigo + "' And GSocieda=" + stCodSociedad + " AND ginspecc='" + stCodInspeccion + "'"; 
						break;
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, MFiliacion.GConexion);
				RrSQL = new DataSet();
				tempAdapter.Fill(RrSQL);

				if (RrSQL.Tables[0].Rows.Count != 0)
				{
					RrSQL.Edit();
					RrSQL.Tables[0].Rows[0]["fborrado"] = DBNull.Value;
					string tempQuery = RrSQL.Tables[0].TableName;
					//SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrSQL, RrSQL.Tables[0].TableName);
					//se habilitan o deshabilitan los botones, seg�n corresponda
					cmdActivar.Enabled = false;
					cmdSeleccionar.Enabled = true;
					cmdDesactivar.Enabled = true;
					//se actualiza tambi�n el nuevo valor (que es NULO) de "fborrado" en el spread
					sprSociedades.Col = Col_FBorrado;
					sprSociedades.Text = "";
					//el color del registro pasa a ser el propio de la sociedad activada
					sprSociedades.foreColor = shpActivado.BorderColor;
					sprSociedades.Col = Col_DSocieda;
                    //sdsalazar aqui mientras tanto
                    sprSociedades.foreColor = shpActivado.BorderColor;
                    sprSociedades.Col = Col_NAfiliac;
                    sprSociedades.foreColor = shpActivado.BorderColor;
                    sprSociedades.Col = Col_TitBenef;
                    sprSociedades.foreColor = shpActivado.BorderColor;
                    sprSociedades.Col = Col_Cobertura;
                    sprSociedades.foreColor = shpActivado.BorderColor;
                    sprSociedades.Col = Col_FechaAlta;
                    sprSociedades.foreColor = shpActivado.BorderColor;
                    sprSociedades.Col = Col_FechaCad;
                    sprSociedades.foreColor = shpActivado.BorderColor;
                    sprSociedades.Col = Col_FBorrado;
                    sprSociedades.foreColor = shpActivado.BorderColor;
                    sprSociedades.Col = Col_GSocieda;
                    sprSociedades.foreColor = shpActivado.BorderColor;
                    sprSociedades.Col = Col_IconIVA;
                    sprSociedades.foreColor = shpActivado.BorderColor;
                }

				RrSQL.Close();
				RrSQL = null;
				this.Cursor = Cursors.Default;
			}
			catch(SqlException SqlExep)
			{
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(SqlExep, MFiliacion.GConexion);
			}
            catch(Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }

		}

		private void cmdDesactivar_Click(Object eventSender, EventArgs eventArgs)
		{

			string stSql = String.Empty;
			DataSet RrSQL = null;
			string stCodSociedad = String.Empty;
			string stCodInspeccion = String.Empty;
			string stFechaActual = String.Empty;

            try
            {

                this.Cursor = Cursors.WaitCursor;

                sprSociedades.Row = sprSociedades.ActiveRowIndex;
                sprSociedades.Col = Col_GSocieda;
                stCodSociedad = sprSociedades.Text.Trim();

                sprSociedades.Col = Col_GInspecc;
                stCodInspeccion = sprSociedades.Text.Trim();

                sprSociedades.Col = Col_TipoEntidad;
                switch (sprSociedades.Text.Trim())
                {
                    case "S":
                        //Si la sociedad que se pretende desactivar es la que est� actualmente activada (en la pantalla de Filiaci�n) 
                        //para el paciente, se informa de ello al usuario, no dejando al usuario desactivar la sociedad. 
                        if ((DFI111F1.DefInstance.rbRERegimen[1].IsChecked) || (!DFI111F1.DefInstance.rbRERegimen[0].IsChecked && !DFI111F1.DefInstance.rbRERegimen[1].IsChecked && !DFI111F1.DefInstance.rbRERegimen[2].IsChecked))
                        {
                            if (DFI111F1.DefInstance.cbbRESociedad.SelectedIndex != -1)
                            {
                                if (Convert.ToDouble(DFI111F1.DefInstance.cbbRESociedad.Items[DFI111F1.DefInstance.cbbRESociedad.SelectedIndex].Value.ToString()) == StringsHelper.ToDoubleSafe(stCodSociedad))
                                {
                                    this.Cursor = Cursors.Default;
                                    string tempRefParam = "R�gimen Econ�mico";
                                    string tempRefParam2 = "";
                                    short tempRefParam3 = 4019;
                                    string[] tempRefParam4 = new string[] { };
                                    MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam, tempRefParam2, tempRefParam3, MFiliacion.GConexion, tempRefParam4);
                                    return;
                                }
                            }
                        }

                        //se pone la Fecha de Borrado con valor distinto de NULO 
                        stSql = "Select gidenpac, gsocieda, fborrado From Dentipac Where Gidenpac='" + MFiliacion.ModCodigo + "' And GSocieda=" + stCodSociedad;
                        break;
                    case "I":
                        //Si la inspeccion que se pretende desactivar es la que esta actualmente activada (en la pantalla de filiacion) 
                        //para el paciente, se informa de ello al usuairo, no dejando al usuario desactivar la inspeccion. 
                        if (DFI111F1.DefInstance.rbRERegimen[0].IsChecked || (!DFI111F1.DefInstance.rbRERegimen[0].IsChecked && !DFI111F1.DefInstance.rbRERegimen[1].IsChecked && !DFI111F1.DefInstance.rbRERegimen[2].IsChecked))
                        {
                            if (Convert.ToDouble(DFI111F1.DefInstance.cbbREInspeccion.get_ListIndex()) != -1)
                            {
                                object tempRefParam5 = 1;
                                object tempRefParam6 = DFI111F1.DefInstance.cbbREInspeccion.get_ListIndex();
                                if (Convert.ToString(DFI111F1.DefInstance.cbbREInspeccion.get_Column(tempRefParam5, tempRefParam6)).Trim().ToUpper() == stCodInspeccion.Trim().ToUpper())
                                {
                                    this.Cursor = Cursors.Default;
                                    string tempRefParam7 = "R�gimen Econ�mico";
                                    string tempRefParam8 = "";
                                    short tempRefParam9 = 4019;
                                    string[] tempRefParam10 = new string[] { };
                                    MFiliacion.clasemensaje.RespuestaMensaje(tempRefParam7, tempRefParam8, tempRefParam9, MFiliacion.GConexion, tempRefParam10);
                                    return;
                                }
                            }
                        }

                        if (Convert.ToDouble(DFI111F1.DefInstance.cbbREInspeccion.get_ListIndex()) != -1)
                        {
                            object tempRefParam11 = 1;
                            object tempRefParam12 = DFI111F1.DefInstance.cbbREInspeccion.get_ListIndex();
                            if (Convert.ToString(DFI111F1.DefInstance.cbbREInspeccion.get_Column(tempRefParam11, tempRefParam12)).Trim().ToUpper() == stCodInspeccion.Trim().ToUpper())
                            {
                                stSql = "DELETE DENTIPAC Where Gidenpac='" + MFiliacion.ModCodigo + "' And GSocieda=" + stCodSociedad + " AND ginspecc='" + stCodInspeccion + "'";
                                SqlCommand tempCommand = new SqlCommand(stSql, MFiliacion.GConexion);
                                tempCommand.ExecuteNonQuery();
                            }
                        }

                        //se pone la Fecha de Borrado con valor distinto de NULO 
                        stSql = "Select gidenpac, gsocieda, ginspecc, fborrado From DINSHPAC Where Gidenpac='" + MFiliacion.ModCodigo + "' And GSocieda=" + stCodSociedad + " AND ginspecc='" + stCodInspeccion + "'";
                        break;
                }

                SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, MFiliacion.GConexion);
                RrSQL = new DataSet();
                tempAdapter.Fill(RrSQL);

                if (RrSQL.Tables[0].Rows.Count != 0)
                {
                    RrSQL.Edit();
                    stFechaActual = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    RrSQL.Tables[0].Rows[0]["fborrado"] = DateTime.Parse(stFechaActual);
                    string tempQuery = RrSQL.Tables[0].TableName;
                    SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter);
                    tempAdapter.Update(RrSQL, RrSQL.Tables[0].TableName);

                    //se habilitan o deshabilitan los botones, seg�n corresponda
                    cmdActivar.Enabled = true;
                    cmdSeleccionar.Enabled = false;
                    cmdDesactivar.Enabled = false;
                    //se actualiza tambi�n el nuevo valor de "fborrado" en el spread
                    sprSociedades.Col = Col_FBorrado;
                    sprSociedades.Text = stFechaActual;
                    //el color del registro pasa a ser el propio de la sociedad desactivada

                    //sdsalazar aqui mientras tanto
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                    sprSociedades.Col = Col_DSocieda;
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                    sprSociedades.Col = Col_NAfiliac;
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                    sprSociedades.Col = Col_TitBenef;
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                    sprSociedades.Col = Col_Cobertura;
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                    sprSociedades.Col = Col_FechaAlta;
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                    sprSociedades.Col = Col_FechaCad;
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                    sprSociedades.Col = Col_FBorrado;
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                    sprSociedades.Col = Col_GSocieda;
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                    sprSociedades.Col = Col_IconIVA;
                    sprSociedades.foreColor = shpDesactivado.BorderColor;
                }

                RrSQL.Close();
                RrSQL = null;

                this.Cursor = Cursors.Default;
            }
            catch(SqlException SqlExep)
            {
                this.Cursor = Cursors.Default;
                Serrores.GestionErrorBaseDatos(SqlExep, MFiliacion.GConexion);
            }
            catch(Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }

		}

		private void cmdSalir_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}

		private void cmdSeleccionar_Click(Object eventSender, EventArgs eventArgs)
		{

			string stCodSociedad = String.Empty;
			string stCodInspeccion = String.Empty;

			try
			{

				this.Cursor = Cursors.WaitCursor;

				sprSociedades.Row = sprSociedades.ActiveRowIndex;

				sprSociedades.Col = Col_GSocieda;
				stCodSociedad = sprSociedades.Text.Trim();

				sprSociedades.Col = Col_GInspecc;
				stCodInspeccion = sprSociedades.Text.Trim();

				sprSociedades.Col = Col_TipoEntidad;
				switch(sprSociedades.Text.Trim())
				{
					case "S" : 
						//la sociedad pasa a visualizarse en la pantalla de Filiaci�n 
						DFI111F1.DefInstance.RecuperarSociedad(MFiliacion.ModCodigo, Convert.ToInt32(Double.Parse(stCodSociedad))); 
						break;
					case "I" : 
						//la inspeccion pasa a visualizarse en la pantalla de filiacion 
						DFI111F1.DefInstance.RecuperarInspeccion(MFiliacion.ModCodigo, Convert.ToInt32(Double.Parse(stCodSociedad)), stCodInspeccion); 
						break;
				}

				this.Cursor = Cursors.Default;
				this.Close();
			}
			catch(SqlException SqlExep)
			{
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(SqlExep, MFiliacion.GConexion);
			}
            catch(Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }

		}

		private void DFI111F5_Load(Object eventSender, EventArgs eventArgs)
		{

			try
			{

				this.Cursor = Cursors.WaitCursor;

				cmdActivar.Enabled = false;
				cmdDesactivar.Enabled = false;
				cmdSeleccionar.Enabled = false;


				Col_DSocieda = 1;
				Col_NAfiliac = 2;
				Col_TitBenef = 3;
				Col_Cobertura = 4;
				Col_FechaAlta = 5;
				Col_FechaCad = 6;
				Col_FBorrado = 7;
				Col_GSocieda = 8;
				Col_FBorradoSoc = 9;
				Col_IconIVA = 10;
				Col_FDesactiSoc = 11;
				Col_GInspecc = 12;
				Col_TipoEntidad = 13;

				sprSociedades.MaxRows = 0;
				sprSociedades.MaxCols = 13; //10

				MostrarNombrePaciente(MFiliacion.ModCodigo);
				CargaGrid();

				this.Cursor = Cursors.Default;
			}
			catch(SqlException SqlExep)
			{
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(SqlExep, MFiliacion.GConexion);
			}
            catch(Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }

		}

		private void MostrarNombrePaciente(string stId_Paciente)
		{
			DataSet rdoTemp = null;
			string stSql = String.Empty;

			try
			{

				this.Cursor = Cursors.WaitCursor;

				stSql = "SELECT DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac FROM DPACIENT";
				stSql = stSql + " WHERE gidenpac = '" + stId_Paciente + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, MFiliacion.GConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);

				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					lbPaciente.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape2pac"]).Trim() + ", " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpac"]).Trim();
				}

                rdoTemp.Close();
				rdoTemp = null;
				this.Cursor = Cursors.Default;
			}
			catch(SqlException SqlExep)
			{
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(SqlExep, MFiliacion.GConexion);
			}
            catch(Exception Exep)
            {
                System.Console.Write(Exep.Message);
            }

		}

		private void CargaGrid()
		{

			string stSql = String.Empty;
			DataSet RrSQL = null;
			int intContador = 0;
			int lngColorRegistro = 0;

			try
			{

				this.Cursor = Cursors.WaitCursor;

				//hay que descartar los registros de DENTIPAC cuya sociedad se correspondan con la Seguridad Social.
				stSql = "SELECT 'S' TipoEntidad, D.nafiliac, D.ginspecc, D.itituben, D.fborrado, D.gsocieda, D.ocobertu,";
				stSql = stSql + " D.finivige, D.ffinvige, DS.dsocieda,";
				stSql = stSql + " DS.fborrado fborradoSoc, DS.fdesacti fdesactiSoc,  D.fmovimie, D.icondiva ";
				stSql = stSql + " FROM DENTIPAC D, DSOCIEDA DS";
				stSql = stSql + " WHERE D.GSOCIEDA=DS.GSOCIEDA AND D.GIDENPAC = '" + MFiliacion.ModCodigo + "'";
				stSql = stSql + " AND D.GSOCIEDA <> " + MFiliacion.CodSS.ToString() + " AND (D.ginspecc is null)";


				//Oscar C Mayo 2010
				//Se a�aden los registros de DINSHPAC cuya sociedad se correspondan con la Seguridad Social.
				//(en principio todos los registros de esta tabla seran de seguridad social ya que son inspecciones del paciente)
				stSql = stSql + " UNION ALL ";
				stSql = stSql + 
				        "SELECT 'I' TipoEntidad, D.nafiliac, D.ginspecc, D.itituben, D.fborrado, D.gsocieda, D.ocobertu," + " D.finivige, D.ffinvige, DS.dinspecc, " + " DS.fborrado fborradoSoc, DS.fdesacti fdesactiSoc,  D.fmovimie, D.icondiva " + " FROM DINSHPAC D, DINSPECC DS" + " WHERE D.GINSPECC=DS.GINSPECC AND D.GIDENPAC = '" + MFiliacion.ModCodigo + "'" + " AND D.GSOCIEDA = " + MFiliacion.CodSS.ToString() + " AND (D.ginspecc is NOT null)";
				//------------------


				stSql = stSql + " ORDER BY FMOVIMIE DESC";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stSql, MFiliacion.GConexion);
				RrSQL = new DataSet();
				tempAdapter.Fill(RrSQL);

				if (RrSQL.Tables[0].Rows.Count != 0)
				{
					sprSociedades.MaxRows = 0;
					RrSQL.MoveFirst();
					intContador = 1;
					foreach (DataRow iteration_row in RrSQL.Tables[0].Rows)
					{
						sprSociedades.MaxRows++;
						sprSociedades.Row = intContador;

						if (Convert.IsDBNull(iteration_row["fborrado"]))
						{
                            //sdsalazar aqui mientras tanto
							lngColorRegistro = ColorTranslator.ToOle(shpActivado.BorderColor);
						}
						else
						{
                            //sdsalazar aqui mientras tanto
							lngColorRegistro = ColorTranslator.ToOle(shpDesactivado.BorderColor);
						}


						sprSociedades.Col = Col_DSocieda;
						sprSociedades.Text = Convert.ToString(iteration_row["dsocieda"]).Trim();
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);

						sprSociedades.Col = Col_NAfiliac;
						if (Convert.IsDBNull(iteration_row["nafiliac"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							sprSociedades.Text = Convert.ToString(iteration_row["nafiliac"]).Trim();
						}
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);

						//Titular/Beneficiario
						sprSociedades.Col = Col_TitBenef;
						if (Convert.IsDBNull(iteration_row["itituben"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							if (Convert.ToString(iteration_row["itituben"]).Trim().ToUpper() == "T")
							{
								sprSociedades.Text = "Titular";
							}
							else if ((Convert.ToString(iteration_row["itituben"]).Trim().ToUpper() == "B"))
							{ 
								sprSociedades.Text = "Beneficiario";
							}
						}
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);

						//Cobertura
						sprSociedades.Col = Col_Cobertura;
						if (Convert.IsDBNull(iteration_row["ocobertu"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							sprSociedades.Text = Convert.ToString(iteration_row["ocobertu"]).Trim();
						}
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);

						//Fecha de Alta
						sprSociedades.Col = Col_FechaAlta;
						if (Convert.IsDBNull(iteration_row["finivige"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							//SDSALAZAR TODO SPRINT_5_5
                            //revisar en tiempo de ejecuci�n
							//sprSociedades.Text = StringsHelper.Format(UpgradeStubs.SSCalendarWidgets_A_WIConstants.getMonth(Convert.ToString(iteration_row["finivige"]).Trim()), "00") + "/" + ((int) UpgradeStubs.SSCalendarWidgets_A_WIConstants.getYear(Convert.ToString(iteration_row["finivige"]).Trim())).ToString();
                            sprSociedades.Text = Convert.ToDateTime(iteration_row["finivige"].ToString().Trim()).Month + "/" + Convert.ToDateTime(iteration_row["finivige"].ToString().Trim()).Year;
                        }
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);

						//Fecha de Caducidad
						sprSociedades.Col = Col_FechaCad;
						if (Convert.IsDBNull(iteration_row["ffinvige"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
                            //SDSALAZAR TODO SPRINT_5_5
                            //revisar en modo de ejecuci�n
                            //sprSociedades.Text = StringsHelper.Format(UpgradeStubs.SSCalendarWidgets_A_WIConstants.getMonth(Convert.ToString(iteration_row["ffinvige"]).Trim()), "00") + "/" + ((int) UpgradeStubs.SSCalendarWidgets_A_WIConstants.getYear(Convert.ToString(iteration_row["ffinvige"]).Trim())).ToString();
                            sprSociedades.Text = Convert.ToDateTime(iteration_row["ffinvige"].ToString().Trim()).Month + "/" + Convert.ToDateTime(iteration_row["ffinvige"].ToString().Trim()).Year;
                        }
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);

						//Fecha de Borrado
						sprSociedades.Col = Col_FBorrado;
						if (Convert.IsDBNull(iteration_row["fborrado"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							System.DateTime TempDate = DateTime.FromOADate(0);
							sprSociedades.Text = (DateTime.TryParse(Convert.ToString(iteration_row["fborrado"]).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy HH:mm:ss") : Convert.ToString(iteration_row["fborrado"]).Trim();
						}
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);

						//C�digo de Sociedad
						sprSociedades.Col = Col_GSocieda;
						if (Convert.IsDBNull(iteration_row["gsocieda"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							sprSociedades.Text = Convert.ToString(iteration_row["gsocieda"]).Trim();
						}
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);
						sprSociedades.SetColHidden(sprSociedades.Col, true);

						//C�digo de Inspeccion
						sprSociedades.Col = Col_GInspecc;
						if (Convert.IsDBNull(iteration_row["ginspecc"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							sprSociedades.Text = Convert.ToString(iteration_row["ginspecc"]).Trim();
						}
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);
						sprSociedades.SetColHidden(sprSociedades.Col, true);

						//Fecha de borrado de la sociedad
						sprSociedades.Col = Col_FBorradoSoc;
						if (Convert.IsDBNull(iteration_row["fborradosoc"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							System.DateTime TempDate2 = DateTime.FromOADate(0);
							sprSociedades.Text = (DateTime.TryParse(Convert.ToString(iteration_row["fborradosoc"]).Trim(), out TempDate2)) ? TempDate2.ToString("dd/MM/yyyy HH:mm:ss") : Convert.ToString(iteration_row["fborradosoc"]).Trim();
						}
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);
						sprSociedades.SetColHidden(sprSociedades.Col, true);

						//Fecha de desactivacion de la sociedad
						sprSociedades.Col = Col_FDesactiSoc;
						if (Convert.IsDBNull(iteration_row["fdesactisoc"]))
						{
							sprSociedades.Text = "";
						}
						else
						{
							System.DateTime TempDate3 = DateTime.FromOADate(0);
							sprSociedades.Text = (DateTime.TryParse(Convert.ToString(iteration_row["fdesactisoc"]).Trim(), out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy HH:mm:ss") : Convert.ToString(iteration_row["fdesactisoc"]).Trim();
						}
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);
						sprSociedades.SetColHidden(sprSociedades.Col, true);

						//Tipo de Entidad : Sociedad / Inspeccion
						sprSociedades.Col = Col_TipoEntidad;
						sprSociedades.Text = Convert.ToString(iteration_row["TipoEntidad"]).Trim();
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);
						sprSociedades.SetColHidden(sprSociedades.Col, true);
						//Condici�n de iva
						sprSociedades.Col = Col_IconIVA;
						if ((Convert.ToString(iteration_row["icondiva"]) + "").Trim().ToUpper() == "S" || Convert.ToString(iteration_row["TipoEntidad"]).Trim() == "I")
						{
							sprSociedades.Text = "S�";
						}
						else
						{
							sprSociedades.Text = "No";
						}
						sprSociedades.foreColor = ColorTranslator.FromOle(lngColorRegistro);
						sprSociedades.SetColHidden(sprSociedades.Col, !DFI111F1.DefInstance.chkExentoIVA.Visible); //BEsconderIconIVA

						intContador++;
					}
					sprSociedades.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.ReadOnly;
				}

				RrSQL.Close();
				RrSQL = null;
				this.Cursor = Cursors.Default;
			}
			catch(SqlException sqlexep)
			{
				this.Cursor = Cursors.Default;
				Serrores.GestionErrorBaseDatos(sqlexep, MFiliacion.GConexion);
			}
            catch(Exception exep)
            {
                System.Console.Write(exep.Message);
            }
		}

		private void sprSociedades_CellClick(object eventSender, Telerik.WinControls.UI.GridViewCellEventArgs eventArgs)
		{
			//int Col = eventArgs.Column;
			//int Row = eventArgs.Row;

            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;

            if (Row > 0)
			{
				if (fila_seleccionada == Row)
				{

                    sprSociedades.CurrentRow = null;
					fila_seleccionada = 0;
				}
				else
				{
                   fila_seleccionada = Row;
				}

				ActivarDesactivarBotones(fila_seleccionada);
			}

		}

		//dependiendo de la fila seleccionada, se activar�n unas opciones u otras.
		private void ActivarDesactivarBotones(int lngFilaSeleccionada)
		{
			string stfborradoSoc = String.Empty;
			string stfdesactiSoc = String.Empty;
			string stfborradoPac = String.Empty;

			if (lngFilaSeleccionada == 0)
			{ //si no hay ninguna fila seleccionada
				cmdActivar.Enabled = false;
				cmdDesactivar.Enabled = false;
				cmdSeleccionar.Enabled = false;
			}
			else
			{
				//si hay alguna fila seleccionada
				sprSociedades.Row = lngFilaSeleccionada;
				sprSociedades.Col = Col_FBorradoSoc;
				stfborradoSoc = sprSociedades.Text.Trim();
				sprSociedades.Col = Col_FDesactiSoc;
				stfdesactiSoc = sprSociedades.Text.Trim();
				sprSociedades.Col = Col_FBorrado;
				stfborradoPac = sprSociedades.Text.Trim();
				if (stfborradoSoc.Trim() != "" || stfdesactiSoc.Trim() != "")
				{
					cmdActivar.Enabled = false;
					cmdDesactivar.Enabled = false;
					cmdSeleccionar.Enabled = false;
				}
				else
				{
					if (stfborradoPac.Trim() == "")
					{
						//si est� activada la sociedad
						cmdActivar.Enabled = false;
						cmdSeleccionar.Enabled = true;
						cmdDesactivar.Enabled = true;
					}
					else
					{
						//si est� desactivada la sociedad
						cmdActivar.Enabled = true;
						cmdSeleccionar.Enabled = false;
						cmdDesactivar.Enabled = false;
					}
				}
			}

		}

		private void sprSociedades_KeyDown(Object eventSender, KeyEventArgs eventArgs)
		{
			int KeyCode = (int) eventArgs.KeyCode;
			int Shift = (eventArgs.Shift) ? 1 : 0;

			if (KeyCode == ((int) Keys.Up) || KeyCode == ((int) Keys.Down))
			{
                //if ((KeyCode == ((int) Keys.Up) && sprSociedades.ActiveSheet.ActiveRowIndex != 1) || (KeyCode == ((int) Keys.Down) && sprSociedades.ActiveSheet.ActiveRowIndex != sprSociedades.MaxRows))
                if ((KeyCode == ((int)Keys.Up) && sprSociedades.ActiveRowIndex != 1) || (KeyCode == ((int)Keys.Down) && sprSociedades.ActiveRowIndex != sprSociedades.MaxRows))
                {

                    //if (KeyCode == ((int) Keys.Up) && sprSociedades.ActiveSheet.ActiveRowIndex != 1)
                    if (KeyCode == ((int)Keys.Up) && sprSociedades.ActiveRowIndex != 1)
                    {
						//sprSociedades.Row = sprSociedades.ActiveSheet.ActiveRowIndex - 1;
                        sprSociedades.Row = sprSociedades.ActiveRowIndex - 1;
                        sprSociedades.Col = 1;
					}
                    //else if ((KeyCode == ((int) Keys.Down) && sprSociedades.ActiveSheet.ActiveRowIndex != sprSociedades.MaxRows))
                    else if ((KeyCode == ((int)Keys.Down) && sprSociedades.ActiveRowIndex != sprSociedades.MaxRows))
                    { 
						//sprSociedades.Row = sprSociedades.ActiveSheet.ActiveRowIndex + 1;
                        sprSociedades.Row = sprSociedades.ActiveRowIndex + 1;
                        sprSociedades.Col = 1;
					}
                    //sprSociedades.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.RowMode;
                    sprSociedades.OperationMode = UpgradeHelpers.Spread.OperationModeEnums.RowMode; 
                    //sprSociedades.ActiveSheet.OperationMode = (FarPoint.Win.Spread.OperationMode) (((int) FarPoint.Win.Spread.OperationMode.ReadOnly) + ((int) FarPoint.Win.Spread.OperationMode.RowMode));
                    sprSociedades.OperationMode = (UpgradeHelpers.Spread.OperationModeEnums)(((int)UpgradeHelpers.Spread.OperationModeEnums.ReadOnly) + ((int)UpgradeHelpers.Spread.OperationModeEnums.RowMode));
                    fila_seleccionada = sprSociedades.Row;
					ActivarDesactivarBotones(fila_seleccionada);
				}
			}

		}
		private void DFI111F5_Closed(Object eventSender, EventArgs eventArgs)
		{
            this.Dispose();
		}
	}
}