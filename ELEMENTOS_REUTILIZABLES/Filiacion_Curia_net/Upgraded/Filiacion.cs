using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls;

namespace filiacionE
{
	public class Filiacion
	{
		public Filiacion()
		{
			MFiliacion.clasemensaje = new Mensajes.ClassMensajes();
			MFiliacion.gMostrarDeudas = false;
			MFiliacion.gMostrarFacturacion = false;
		}

		~Filiacion()
		{
			//oscar 28/10/2004

			string Apellido1 = String.Empty;
			string sql = String.Empty;
			DataSet cursor = null;

			try
			{

				LlamarOperacionesFMS.clsOperFMS ob = null;
				if (MFiliacion.gMostrarDeudas)
				{
					ob = new LlamarOperacionesFMS.clsOperFMS();
					
					object tempRefParam = DFI120F1.DefInstance;
					string tempRefParam2 = "DEUDASPACIENTE";
					string tempRefParam3 = MFiliacion.ModCodigo;
					string tempRefParam4 = String.Empty;
					object tempRefParam5 = Type.Missing;
					string tempRefParam6 = String.Empty;
                    string tempRefParam7 = String.Empty;
					string tempRefParam8 = String.Empty;
					string tempRefParam9 = String.Empty;
					object tempRefParam10 = Type.Missing;
					string tempRefParam11 = String.Empty;
					object tempRefParam12 = Type.Missing;
					ob.LlamarOperacionesFMS(Serrores.VVstUsuarioApli, MFiliacion.GConexion, tempRefParam, tempRefParam2, tempRefParam3, tempRefParam4, tempRefParam5, tempRefParam6,tempRefParam7, tempRefParam8, tempRefParam9, tempRefParam10, tempRefParam11, tempRefParam12);
					MFiliacion.ModCodigo = Convert.ToString(tempRefParam3);
					ob = null;
				}

				//22/11/2004
				if (MFiliacion.gMostrarFacturacion)
				{

					sql = "select dape1pac from dpacient where gidenpac='" + MFiliacion.ModCodigo + "'";
					SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
					cursor = new DataSet();
					tempAdapter.Fill(cursor);
					if (cursor.Tables[0].Rows.Count != 0)
					{						
						Apellido1 = Convert.ToString(cursor.Tables[0].Rows[0]["dape1pac"]).Trim();
					}
					else
					{
						Apellido1 = " ";
					}					
					cursor.Close();

					ob = new LlamarOperacionesFMS.clsOperFMS();					
					object tempRefParam13 = DFI120F1.DefInstance;
					string tempRefParam14 = "FACTURA";
					string tempRefParam15 = MFiliacion.ModCodigo;
					string tempRefParam16 = Apellido1;
					object tempRefParam17 = Type.Missing;
					string tempRefParam18 = String.Empty;
					string tempRefParam19 = String.Empty;
					string tempRefParam20 = String.Empty;
					string tempRefParam21 = String.Empty;
					object tempRefParam22 = Type.Missing;
					string tempRefParam23 = String.Empty;
					object tempRefParam24 = Type.Missing;
					ob.LlamarOperacionesFMS(Serrores.VVstUsuarioApli, MFiliacion.GConexion, tempRefParam13, tempRefParam14,tempRefParam15, tempRefParam16, tempRefParam17, tempRefParam18, tempRefParam19, tempRefParam20, tempRefParam21, tempRefParam22, tempRefParam23, tempRefParam24);
					Apellido1 = Convert.ToString(tempRefParam16);
					MFiliacion.ModCodigo = Convert.ToString(tempRefParam15);
					ob = null;

				}
			}
			catch
			{

				if (MFiliacion.gMostrarDeudas)
				{
                    RadMessageBox.Show("Error accediendo Deudas paciente", Application.ProductName);
				}
				if (MFiliacion.gMostrarFacturacion)
				{
                    RadMessageBox.Show("Error accediendo Facturación Pendiente", Application.ProductName);
				}
				//-------------
			}

		}


		//*******************************************************************************************
		//* O.Frias (31/07/2007) - Incorporo la operativa para la solicitud de usuario para cita WEB
		//*******************************************************************************************
		public void Load(object VAR1, object VAR2, string stFiliacion, ref string stModulo, SqlConnection Conexion, object stIdpaciente_optional, string Codigo_Usuario, string UserName, string ComputerName)
		{
			string stIdpaciente = (stIdpaciente_optional == Type.Missing) ? String.Empty : stIdpaciente_optional as string;

			//O.Frias - 23/02/2009
			//Displaya el new boton IMDHOPEN.
			string stsqltemp = "SELECT ISNULL(valfanu1, 'N' ) as valfanu1  FROM SCONSGLO WHERE gconsglo= 'IMDHOPEN' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsqltemp, Conexion);
			DataSet RrTemp = new DataSet();
			tempAdapter.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{				
				MFiliacion.blnIMDHOPEN = (Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim() == "S");
			}
			
			RrTemp.Close();

			if (stModulo == "IFMS" && stIdpaciente_optional == Type.Missing)
			{
				MFiliacion.GSexo = -1;
			}

			if (stModulo == "ArchivoC")
			{
				MFiliacion.consultaCancelados = true;
				stModulo = "Archivo";
			}
			else
			{
				MFiliacion.consultaCancelados = false;
			}

			MFiliacion.GConexion = Conexion;
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref MFiliacion.GConexion);
			Serrores.AjustarRelojCliente(MFiliacion.GConexion);			
			//camaya todo_x_5
			//UpgradeStubs.VB_Global.getApp().setHelpFile(Path.GetDirectoryName(Application.ExecutablePath) + "\\ayuda_dlls.hlp");
            //CDAyuda.Instance.setHelpFile(this,CDAyuda.FICHERO_AYUDA_OTRASDLLS);

            string tstDatos = String.Empty;
			DataSet tRrDatos = null;
			int tlIdConexion = 0;
			string sql = String.Empty;
			if (stModulo != "IFMS" && stModulo != "OPENLAB")
			{
				//para evitar que en la llamada de IFMS busque en la tabla SSESUSUA
				Serrores.Datos_Conexion(MFiliacion.GConexion);
			}
			else
			{
				MFiliacion.tstUsuNT_IFMS = UserName;
				MFiliacion.tstMAquina_IFMS = ComputerName;
				MFiliacion.usuario_IFMS = Codigo_Usuario;

				tlIdConexion = 0; //le manda siempre 0 por defecto, en TServer no funciona

				tstDatos = "select * from SSESUSUA where idconexi=" + tlIdConexion.ToString() + " and dusuarnt='" + MFiliacion.tstUsuNT_IFMS + "' " + " and puestopc='" + MFiliacion.tstMAquina_IFMS + "'" + " and gusuario = '" + MFiliacion.usuario_IFMS + "'";

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstDatos, MFiliacion.GConexion);
				tRrDatos = new DataSet();
				tempAdapter_2.Fill(tRrDatos);
				if (tRrDatos.Tables[0].Rows.Count != 0)
				{					
					Serrores.VVstDriver = Convert.ToString(tRrDatos.Tables[0].Rows[0]["ddriver"]).Trim();					
					Serrores.VVstBasedatosActual = Convert.ToString(tRrDatos.Tables[0].Rows[0]["basedato"]).Trim();					
					Serrores.VVstServerActual = Convert.ToString(tRrDatos.Tables[0].Rows[0]["servidor"]).Trim();					
					Serrores.VVstUsuarioBD = Convert.ToString(tRrDatos.Tables[0].Rows[0]["usuaribd"]).Trim();					
					Serrores.VVstPasswordBD = Convert.ToString(tRrDatos.Tables[0].Rows[0]["pwbaseda"]).Trim();					
					Serrores.VVstUsuarioApli = Convert.ToString(tRrDatos.Tables[0].Rows[0]["gusuario"]).Trim();
				}
				//desencripto la pasword
				if (Serrores.VVstPasswordBD != "")
				{
					Serrores.VVstPasswordBD = Serrores.General_DesencriptarPassword(ref Serrores.VVstPasswordBD).Trim();
				}

				//        blnIMDHOPEN = False

			}
			int pos = 0;
			if (stIdpaciente_optional== null || stIdpaciente_optional == Type.Missing)
			{

				//OSCAR C FEB 2006
				//Cuando se intenta registrar un paciente en el SIA procedente de RULEQ,
				//se cargan los datos por pantalla para poder buscar pacientes similares o
				//filiar a un nuevo paciente....
				MFiliacion.bProcedentedeRULEQ = false;
				MFiliacion.blnUsuWeb = false;
				pos = (stModulo.IndexOf('#') + 1);
				if (pos > 0)
				{
					//************* O.Frias - 31/07/2007 *************
					if (stModulo.StartsWith("RULEQ"))
					{
						MFiliacion.bProcedentedeRULEQ = true;
					}
					else if (stModulo.StartsWith("USUWEB"))
					{ 
						MFiliacion.blnUsuWeb = true;
					}
				}
				//----------------

				//Crea un formulario
				MFiliacion.NuevoDFI120F1 = new DFI120F1();
				MFiliacion.NuevoDFI120F1.REFERENCIAS(VAR1, VAR2, stFiliacion, stModulo.ToUpper());
				//Muestra el formulario

				if (MFiliacion.GApellido1 != "" || MFiliacion.GApellido2 != "" || MFiliacion.GNombre != "" || MFiliacion.GFechaNacimiento != "" || MFiliacion.GSexo >= 0 || MFiliacion.GCipAuto != "")
				{
					MFiliacion.NuevoDFI120F1.tbApellido1.Text = MFiliacion.GApellido1;
					MFiliacion.NuevoDFI120F1.tbApellido2.Text = MFiliacion.GApellido2;
					MFiliacion.NuevoDFI120F1.tbNombre.Text = MFiliacion.GNombre;

                    if(MFiliacion.GFechaNacimiento.Trim().Length > 0)
					    MFiliacion.NuevoDFI120F1.sdcFechaNac.Value = DateTime.Parse(MFiliacion.GFechaNacimiento);

					if (MFiliacion.GSexo >= 0)
					{
						MFiliacion.NuevoDFI120F1.rbSexo[MFiliacion.GSexo].IsChecked = true;
					}

					if (MFiliacion.GCipAuto != "")
					{
						MFiliacion.NuevoDFI120F1.tbCIPAutonomico.Text = MFiliacion.GCipAuto;
						MFiliacion.NuevoDFI120F1.CIP_Enviado();

					}

				}
				MFiliacion.NuevoDFI120F1.ShowDialog();

			}
			else
			{
				if (stFiliacion == "SELECCION")
				{
					MFiliacion.CodPac = true;
					//Crea un formulario
					MFiliacion.NuevoDFI120F1 = new DFI120F1();
					MFiliacion.NuevoDFI120F1.REFERENCIAS(VAR1, VAR2, stFiliacion, stModulo.ToUpper());
					//Muestra el formulario
					sql = "SELECT DISTINCT * FROM DPACIENT WHERE DPACIENT.GIDENPAC = '" + stIdpaciente + "'";
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, MFiliacion.GConexion);
					MFiliacion.RrSQLUltimo = new DataSet();
					tempAdapter_3.Fill(MFiliacion.RrSQLUltimo);
					MFiliacion.abierto = true;
					if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count != 0)
					{						
						if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ifilprov"]) == "N")
						{
							MFiliacion.NuevoDFI120F1.chbFiliacPov.Enabled = false;
						}
						else
						{
							MFiliacion.NuevoDFI120F1.chbFiliacPov.CheckState = CheckState.Checked;
							MFiliacion.NuevoDFI120F1.chbFiliacPov.Enabled = true;
						}
						MFiliacion.NuevoDFI120F1.Cargar_Grid();
                        //camaya todo_x_5
						//MFiliacion.NuevoDFI120F1.sprFiliados_Click(1, 1);
						MFiliacion.NuevoDFI120F1.ShowDialog();
					}
					else
					{
						string tempRefParam = "";
						short tempRefParam2 = 1100;
						string[] tempRefParam3 = new string[]{"Paciente"};
						MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, tempRefParam, tempRefParam2, MFiliacion.GConexion, tempRefParam3);
					}
				}
				else
				{
					MFiliacion.CodPac = true;
					DFI111F1.DefInstance.REFERENCIAS(VAR1, VAR2, stFiliacion, stModulo.ToUpper());
					sql = "SELECT DISTINCT * FROM DPACIENT WHERE DPACIENT.GIDENPAC = '" + stIdpaciente + "'";
					SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, MFiliacion.GConexion);
					MFiliacion.RrSQLUltimo = new DataSet();
					tempAdapter_4.Fill(MFiliacion.RrSQLUltimo);
					MFiliacion.abierto = true;
					if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count != 0)
					{						
						if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["ifilprov"]) == "S")
						{
							string tempRefParam4 = "";
							short tempRefParam5 = 1130;
							string [] tempRefParam6 = new string[]{"completar los", "este paciente sin estar filiado definitivamente"};
							MFiliacion.clasemensaje.RespuestaMensaje(MFiliacion.NombreApli, tempRefParam4, tempRefParam5, MFiliacion.GConexion, tempRefParam6);
							//Class_Terminate_Renamed();
						}
						else
						{							
							DFI111F1.DefInstance.ShowDialog();
						}
					}
					//Hay que poner esto para que estas variables se descargue  de memoria. Si no, cuando se descarga RegModConsulta no se hace es terminate
					MFiliacion.CLASE2 = null;
					MFiliacion.OBJETO2 = null;
				}
			}
			if (!MFiliacion.Llamadaopenlab)
			{
				MFiliacion.RegresodeOpenlab = "";
			}
		}

		public void Load(object VAR1, object VAR2, string stFiliacion, ref string stModulo, SqlConnection Conexion, object stIdpaciente_optional, string Codigo_Usuario, string UserName)
		{
			Load(VAR1, VAR2, stFiliacion, ref stModulo, Conexion, stIdpaciente_optional, Codigo_Usuario, UserName, String.Empty);
		}

		public void Load(object VAR1, object VAR2, string stFiliacion, ref string stModulo, SqlConnection Conexion, object stIdpaciente_optional, string Codigo_Usuario)
		{
			Load(VAR1, VAR2, stFiliacion, ref stModulo, Conexion, stIdpaciente_optional, Codigo_Usuario, String.Empty, String.Empty);
		}

		public void Load(object VAR1, object VAR2, string stFiliacion, ref string stModulo, SqlConnection Conexion, object stIdpaciente_optional)
		{
			Load(VAR1, VAR2, stFiliacion, ref stModulo, Conexion, stIdpaciente_optional, String.Empty, String.Empty, String.Empty);
		}

		public void Load(object VAR1, object VAR2, string stFiliacion, ref string stModulo, SqlConnection Conexion)
		{
			Load(VAR1, VAR2, stFiliacion, ref stModulo, Conexion, Type.Missing, String.Empty, String.Empty, String.Empty);
		}

		public void Openlab(ref string VarOpenlab, string VarApellido1 = "", string VarApellido2 = "", string VarNombre = "", string stserver = "", string stbase = "", object oObjeto = null, string VarFecNacimiento = "", int VarSexo = -1)
		{
			object K = null;

			MFiliacion.Llamadaopenlab = true;

			if (!ABRIRNUEVACONEXION(stserver, stbase))
			{
				VarOpenlab = "Error";
			}
			else
			{
				MFiliacion.RegresodeOpenlab = "OpenLab";
				MFiliacion.GApellido1 = VarApellido1;
				MFiliacion.GApellido2 = VarApellido2;
				MFiliacion.GNombre = VarNombre;
				MFiliacion.GFechaNacimiento = VarFecNacimiento;
				MFiliacion.GSexo = VarSexo;
				Serrores.AjustarRelojCliente(MFiliacion.GConexion);
				if (VarOpenlab == "")
				{
					string tempRefParam = "OPENLAB";
					Load(K, K, "MODIFICACION", ref tempRefParam, MFiliacion.GConexion, String.Empty, MFiliacion.sUsuario, MFiliacion.UserName(), MFiliacion.NonMaquina());
				}
				else
				{
					string tempRefParam2 = "OPENLAB";
					Load(K, K, "MODIFICACION", ref tempRefParam2, MFiliacion.GConexion, VarOpenlab, MFiliacion.sUsuario, MFiliacion.UserName(), MFiliacion.NonMaquina());
				}
				VarOpenlab = MFiliacion.RegresodeOpenlab;
				MFiliacion.RegresodeOpenlab = "";
			}
		}

		public void OpenConDatos(SqlConnection rcConexion, string VarApellido1, string VarApellido2, string VarNombre, string VarFecNacimiento, int VarSexo, string varCipAuto, object oObjeto, string stModo = "")
		{

			object K = null;
			MFiliacion.Llamadaopenlab = false;


			MFiliacion.GApellido1 = VarApellido1;
			MFiliacion.GApellido2 = VarApellido2;
			MFiliacion.GNombre = VarNombre;
			MFiliacion.GFechaNacimiento = VarFecNacimiento;
			MFiliacion.GSexo = VarSexo;
			MFiliacion.GCipAuto = varCipAuto;
			MFiliacion.RegresodeOpenlab = "ConDatos";
			if (stModo == "")
			{
				string tempRefParam = "";
				Load(K, oObjeto, "MODIFICACION", ref tempRefParam, rcConexion, String.Empty, MFiliacion.sUsuario, MFiliacion.UserName(), MFiliacion.NonMaquina());
			}
			else
			{
				string tempRefParam2 = "";
				Load(K, oObjeto, stModo, ref tempRefParam2, rcConexion, String.Empty, MFiliacion.sUsuario, MFiliacion.UserName(), MFiliacion.NonMaquina());
			}

		}


		public void Web(ref string VarOpenlab, ref string stIdpaciente)
		{
			object K = null;

			string stTipo = String.Empty;
			//------------------
			if (!ABRIRNUEVACONEXION("", ""))
			{
				VarOpenlab = "Error";
			}
			else
			{
				MFiliacion.GApellido1 = "";
				MFiliacion.GApellido2 = "";
				MFiliacion.GNombre = "";
				Serrores.AjustarRelojCliente(MFiliacion.GConexion);
				MFiliacion.Llamadaopenlab = false;
				MFiliacion.RegresodeOpenlab = "";
				stTipo = VarOpenlab;
				VarOpenlab = "";
				MFiliacion.stDevolverDatos = "NO";
				if (stIdpaciente.Trim() != "")
				{
					string tempRefParam = "OPENLAB";
					Load(K, K, stTipo, ref tempRefParam, MFiliacion.GConexion, stIdpaciente, MFiliacion.sUsuario, MFiliacion.UserName(), MFiliacion.NonMaquina());
				}
				else
				{
					string tempRefParam2 = "OPENLAB";
					Load(K, K, stTipo, ref tempRefParam2, MFiliacion.GConexion, String.Empty, MFiliacion.sUsuario, MFiliacion.UserName(), MFiliacion.NonMaquina());
				}

				VarOpenlab = MFiliacion.RegresodeOpenlab;
				MFiliacion.RegresodeOpenlab = "";
				stIdpaciente = MFiliacion.idPaciente;
				MFiliacion.idPaciente = "";
				MFiliacion.stDevolverDatos = "";
			}
		}

		private bool ABRIRNUEVACONEXION(string stserver = "", string stbase = "")
		{


			bool result = false;
			string stServer2 = (stserver.Trim() == "") ? MFiliacion.fnQueryRegistro("SERVIDOR") : stserver.Trim();
			string vstbase = (stbase.Trim() == "") ? MFiliacion.fnQueryRegistro("BASEDATOS") : stbase.Trim();

			string stDriver2 = MFiliacion.fnQueryRegistro("DRIVER");
			string stusuBD = MFiliacion.fnQueryRegistro("INICIO");
			string tempRefParam = MFiliacion.fnQueryRegistro("INICIO_PWD");
			string stPasBd = Serrores.General_DesencriptarPassword(ref tempRefParam).Trim();


			//**********************************
			//****Path y confirmación de la existencia de
			// ****** conexion al servidor SQL_SERVER 6.5
			result = true;
			string NOMBRE_DSN_SQLSERVER = "GHOSP";

			string tstconexion = "Description=" + "SQL Server en  SERVICURIA" + "\r" + "OemToAnsi=No" + "\r" + "SERVER=" + stServer2 + "" + "\r" + "Database=" + vstbase + "";

			int tiDriver = (stDriver2.IndexOf('{') + 1);
			stDriver2 = stDriver2.Substring(tiDriver);
			tiDriver = (stDriver2.IndexOf('}') + 1);
			stDriver2 = stDriver2.Substring(0, Math.Min(tiDriver - 1, stDriver2.Length));
			//UPGRADE_ISSUE: (2064) RDO.rdoEngine method rdoEngine.rdoRegisterDataSource was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
			//UpgradeSupport.RDO_rdoEngine_definst.rdoRegisterDataSource(NOMBRE_DSN_SQLSERVER, stDriver2, true, tstconexion);


			string gusuario = String.Empty;
			string GPassword = String.Empty;
			string vstUsuario_NT = Serrores.obtener_usuario();
			string VstLetra = "C"; // Caso de urgencias
			Conexion.Obtener_conexion instancia = new Conexion.Obtener_conexion();

			bool VstError = true;
			string tempRefParam2 = String.Empty;
			string tempRefParam3 = String.Empty;
			string tempRefParam4 = String.Empty;
			string tempRefParam5 = String.Empty;
			instancia.Conexion(ref MFiliacion.GConexion, VstLetra, vstUsuario_NT, ref VstError, ref gusuario, ref GPassword, stServer2, stDriver2, vstbase, stusuBD, stPasBd, tempRefParam2, tempRefParam3, tempRefParam4, tempRefParam5);
			if (!VstError)
			{ // si hay error
                RadMessageBox.Show("Conexión fallida.Se va a proceder a cerrar la aplicación", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
				return false;
			}
			MFiliacion.sUsuario = Serrores.ObternerValor_CTEGLOBAL(MFiliacion.GConexion, "PRESPROD", "VALFANU2");
			MFiliacion.GConexion = null;

			VstError = true;
			string tempRefParam6 = "0";
			string tempRefParam7 = "0";
			string tempRefParam8 = "OPENLAB";
			instancia.Conexion(ref MFiliacion.GConexion, VstLetra, vstUsuario_NT, ref VstError, ref gusuario, ref GPassword, stServer2, stDriver2, vstbase, stusuBD, stPasBd, tempRefParam6, tempRefParam7, tempRefParam8, MFiliacion.sUsuario);
			if (!VstError)
			{ // si hay error
                RadMessageBox.Show("Conexión fallida.Se va a proceder a cerrar la aplicación", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
				return false;
			}

			VstError = GrabaAcceso(MFiliacion.sUsuario, stServer2, vstbase);
			if (VstError)
			{ // si hay error
                RadMessageBox.Show("Conexión fallida.Se va a proceder a cerrar la aplicación", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Error);
				return false;
			}

			return result;
		}


		//UPGRADE_NOTE: (7001) The following declaration (obtener_usuarioIFMS) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string obtener_usuarioIFMS()
		//{
				//string stBuffer = new String(' ', 256);
				//int lSize = stBuffer.Length;
				//Filiacion_curiaSupport.PInvoke.SafeNative.advapi32.GetUserName(ref stBuffer, ref lSize);
				//int lpos = (stBuffer.IndexOf(Strings.Chr(0).ToString()) + 1);
				//if (lSize > 0)
				//{
					//return stBuffer.Substring(0, Math.Min(lpos - 1, stBuffer.Length));
				//}
				//else
				//{
					//return "";
				//}
				//
		//}

		public bool GrabaAcceso(string pUsuario, string PstServer = "", string pstbase = "")
		{
			//Solo se grabara en esta tabla cuando se logre conectar a la B.D sin incidencias.

			bool result = false;

			int tlIdConexion = Serrores.ConexionTS();


			string stserver = (PstServer.Trim() == "") ? MFiliacion.fnQueryRegistro("SERVIDOR") : PstServer.Trim();
			string stbase = (pstbase.Trim() == "") ? MFiliacion.fnQueryRegistro("BASEDATOS") : pstbase.Trim();

			string stDriver = MFiliacion.fnQueryRegistro("DRIVER");
			string stusuBD = MFiliacion.fnQueryRegistro("INICIO");
			string stPasBd = MFiliacion.fnQueryRegistro("INICIO_PWD");

			string usuarioNT = MFiliacion.UserName();

			string tstNombreMaq = MFiliacion.NonMaquina();


			string sql = "SELECT * FROM SSESUSUA WHERE " + 
			             "IDCONEXI= " + tlIdConexion.ToString() + 
			             " AND DUSUARNT='" + usuarioNT + "' " + 
			             " AND PUESTOPC='" + tstNombreMaq + "'" + 
			             " and gusuario = '" + pUsuario + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, MFiliacion.GConexion);
			DataSet tablardo = new DataSet();
			tempAdapter.Fill(tablardo);

			if (tablardo.Tables[0].Rows.Count == 0)
			{				
				tablardo.AddNew();
			}
			else
			{				
				tablardo.Edit();
			}

			
			tablardo.Tables[0].Rows[0]["IDCONEXI"] = tlIdConexion;			
			tablardo.Tables[0].Rows[0]["GUSUARIO"] = pUsuario.Trim();			
			tablardo.Tables[0].Rows[0]["BASEDATO"] = stbase.Trim();			
			tablardo.Tables[0].Rows[0]["SERVIDOR"] = stserver.Trim();			
			tablardo.Tables[0].Rows[0]["USUARIBD"] = stusuBD.Trim();			
			tablardo.Tables[0].Rows[0]["PWBASEDA"] = stPasBd.Trim();			
			tablardo.Tables[0].Rows[0]["DUSUARNT"] = usuarioNT.Trim();			
			tablardo.Tables[0].Rows[0]["PUESTOPC"] = tstNombreMaq.Trim();			
			tablardo.Tables[0].Rows[0]["DDRIVER"] = stDriver.Trim();

			
			string tempQuery = tablardo.Tables[0].TableName;
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
			SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
			tempAdapter_2.Update(tablardo, tablardo.Tables[0].TableName);
			
			tablardo.Close();

			return result;
            			
			if (Information.Err().Number != 0)
			{
				result = true;
			}

			return result;
		}
	}
}