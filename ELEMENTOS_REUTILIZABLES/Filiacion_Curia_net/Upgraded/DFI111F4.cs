using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace filiacionE
{
	public partial class DFI111F4
		: Telerik.WinControls.UI.RadForm
    {

		public DFI111F4()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}


		private void DFI111F4_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		int Col_Historia = 0;
		int Col_Nombre = 0;
		int Col_FecNaci = 0;
		int Col_DNI = 0;
		int Col_Entidad = 0;
		int Col_Afilia = 0;
		int Col_Tarjeta = 0;
		int Col_gIdenPac = 0;
		int Col_CodiSoci = 0;
		int fila_seleccionada = 0;

		private void cmdDuplicados_Click(Object eventSender, EventArgs eventArgs)
		{
			int Index = Array.IndexOf(this.cmdDuplicados, eventSender);
			string strSql = String.Empty;
			string CodigoNIF = String.Empty;
			DataSet rdoPaciente = null;
			DataSet rdoTemp = null;
			int intContador = 0;
			bool blnSS = false;

			if (Index == 0)
			{
				sprDuplicados.Row = fila_seleccionada;
				sprDuplicados.Col = Col_gIdenPac;
				mbAcceso.strGIdenPac = sprDuplicados.Text;
				MFiliacion.ModCodigo = mbAcceso.strGIdenPac.Trim();
				this.Visible = false;

				strSql = "SELECT * FROM DPACIENT WHERE GIDENPAC = '" + MFiliacion.ModCodigo + "'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
				MFiliacion.RrSQLUltimo = new DataSet();
				tempAdapter.Fill(MFiliacion.RrSQLUltimo);
				if (MFiliacion.RrSQLUltimo.Tables[0].Rows.Count != 0)
				{
                    
					DFI111F1.DefInstance.LIMPIAR_FORMULARIO();
					DFI111F1.DefInstance.cbbDPVia.Clear();
					DFI111F1.DefInstance.cbbDPPoblacion.Clear();
					DFI111F1.DefInstance.cbbNacPoblacion.Clear();
					DFI111F1.DefInstance.cbbIdioma.Clear();
					DFI111F1.DefInstance.cbbDPPais.Clear();
					DFI111F1.DefInstance.cbbREInspeccion.Clear();
					DFI111F1.DefInstance.cbbRESociedad.Items.Clear();
					DFI111F1.DefInstance.cbbREPoblacion.Clear();
					DFI111F1.DefInstance.cbbREPais.Clear();

                    DFI111F1.DefInstance.cbbREPago.DataSource = null;

                    DFI111F1.DefInstance.cbbPCPoblacion.Clear();
					DFI111F1.DefInstance.cbbPCPais.Clear();
					DFI111F1.DefInstance.cbbDPEstado.Clear();
					DFI111F1.DefInstance.cbbDPCatSoc.Clear();

					DFI111F1.DefInstance.sprContactos.MaxRows = 0;
                    
					MFiliacion.NuevoDFI120F1.tbApellido1.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["dape1pac"]).Trim();
					MFiliacion.NuevoDFI120F1.tbApellido2.Text = (Convert.IsDBNull(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["dape2pac"])) ? "" : Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["dape2pac"]).Trim();
					MFiliacion.NuevoDFI120F1.tbNombre.Text = Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["dnombpac"]);
					MFiliacion.NuevoDFI120F1.sdcFechaNac.Value = DateTime.Parse(Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["fnacipac"]) + "");

					if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["itipsexo"]).Trim().ToUpper() == "H")
					{
						MFiliacion.NuevoDFI120F1.rbSexo[0].IsChecked = true;
					}
					else if (Convert.ToString(MFiliacion.RrSQLUltimo.Tables[0].Rows[0]["itipsexo"]).Trim().ToUpper() == "M")
					{ 
						MFiliacion.NuevoDFI120F1.rbSexo[1].IsChecked = true;
					}
                }
            }
			this.Close();
		}

		private void DFI111F4_Load(Object eventSender, EventArgs eventArgs)
		{

			Col_Historia = 1;
			Col_Nombre = 2;
			Col_FecNaci = 3;
			Col_DNI = 4;
			Col_Entidad = 5;
			Col_Afilia = 6;
			Col_Tarjeta = 7;
			Col_gIdenPac = 8;
			Col_CodiSoci = 9;
            Cursor =  Cursors.Default;
            
			cmdDuplicados[0].Enabled = false;
			CargaGrid();

		}

		private void CargaGrid()
		{
			int intContador = 0;
			string strSql = String.Empty;
			DataSet rdoTemp = null;

			try
			{

				strSql = "SELECT DISTINCT * FROM DBUSPACI WHERE gusuario = '" + MFiliacion.sUsuario + "' ";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
				rdoTemp = new DataSet();
				tempAdapter.Fill(rdoTemp);
				if (rdoTemp.Tables[0].Rows.Count != 0)
				{
					sprDuplicados.MaxRows = 0;
					intContador = 1;
					foreach (DataRow iteration_row in rdoTemp.Tables[0].Rows)
					{
						sprDuplicados.MaxRows++;
						sprDuplicados.Row = intContador;
						sprDuplicados.Col = Col_Historia;
						sprDuplicados.Text = fHistoria(Convert.ToString(iteration_row["gidenpac"]));

						DatosPaciente(Convert.ToString(iteration_row["gidenpac"]));

						sprDuplicados.Col = Col_gIdenPac;
						sprDuplicados.Text = Convert.ToString(iteration_row["gidenpac"]);
						sprDuplicados.SetColHidden(sprDuplicados.Col, true);

						intContador++;
					}					

				}
				rdoTemp.Close();
				rdoTemp = null;
			}
			catch
			{
			}

		}
        private void sprDuplicados_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
        {
            int Col = eventArgs.ColumnIndex + 1;
            int Row = eventArgs.RowIndex + 1;

			if (Row > 0)
			{

				if (fila_seleccionada == Row)
				{
					sprDuplicados.CurrentRow = null;
					fila_seleccionada = 0;
					cmdDuplicados[0].Enabled = false;
				}
				else
				{
					cmdDuplicados[0].Enabled = true;
					fila_seleccionada = Row;
				}
			}
		}

		private void DatosPaciente(string strgIndPac)
		{

			string strSql = "SELECT DPACIENT.dnombpac, DPACIENT.dape1pac, DPACIENT.dape2pac, DPACIENT.fnacipac, " + 
			                "DPACIENT.ndninifp, DPACIENT.nafiliac, DPACIENT.ntarjeta " + 
			                "FROM DPACIENT " + 
			                "WHERE gidenpac = '" + strgIndPac + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				sprDuplicados.Col = Col_Nombre;
				sprDuplicados.Text = Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dape2pac"]).Trim() + ", " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["dnombpac"]).Trim();
				sprDuplicados.ForeColor = Color.Black;

				sprDuplicados.Col = Col_FecNaci;
				sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["fnacipac"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["fnacipac"]);
				sprDuplicados.ForeColor = Color.Black;

				sprDuplicados.Col = Col_DNI;
				sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["ndninifp"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["ndninifp"]);
				sprDuplicados.ForeColor = Color.Black;



				sprDuplicados.Col = Col_Tarjeta;
				sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["ntarjeta"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["ntarjeta"]);
				sprDuplicados.ForeColor = Color.Black;

                CargaSociedad(strgIndPac);
			}
			rdoTemp.Close();

		}

		private void CargaSociedad(string strgIndPac)
		{

			string strSql = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'SEGURSOC'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			int intCodSS = Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["NNUMERI1"]);
			rdoTemp.Close();

			strSql = "SELECT * FROM SCONSGLO WHERE GCONSGLO = 'PRIVADO'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
			rdoTemp = new DataSet();
			tempAdapter_2.Fill(rdoTemp);
			int intCodPriv = Convert.ToInt32(rdoTemp.Tables[0].Rows[0]["NNUMERI1"]);
			string strTexPriv = Convert.ToString(rdoTemp.Tables[0].Rows[0]["VALFANU1"]).Trim();
			rdoTemp.Close();


			strSql = "SELECT DPACIENT.gsocieda, DPACIENT.nafiliac " + 
			         "FROM DPACIENT " + 
			         "WHERE gidenpac = '" + strgIndPac + "'";

			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
			rdoTemp = new DataSet();
			tempAdapter_3.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				if (!Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["gsocieda"]))
				{
					if (intCodSS == Convert.ToDouble(rdoTemp.Tables[0].Rows[0]["gsocieda"]))
					{
						rdoTemp.Close();

						strSql = "SELECT DENTIPAC.gsocieda, DENTIPAC.nafiliac, DSOCIEDAVA.dsocieda " + 
						         "FROM DENTIPAC " + 
						         "LEFT JOIN DSOCIEDAVA ON DENTIPAC.gsocieda = DSOCIEDAVA.gsocieda " + 
						         "WHERE DENTIPAC.gidenpac = '" + strgIndPac + "' AND " + 
						         "DENTIPAC.gsocieda = " + intCodSS.ToString();

						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
						rdoTemp = new DataSet();
						tempAdapter_4.Fill(rdoTemp);
						if (rdoTemp.Tables[0].Rows.Count != 0)
						{

							sprDuplicados.Col = Col_Entidad;
							sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["dsocieda"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["dsocieda"]);
							sprDuplicados.ForeColor = Color.Black;

							sprDuplicados.Col = Col_Afilia;
							sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["nafiliac"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["nafiliac"]);
							sprDuplicados.ForeColor = Color.Black;

							sprDuplicados.Col = Col_CodiSoci;
							sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["gsocieda"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["gsocieda"]);
							sprDuplicados.SetColHidden(sprDuplicados.Col, true);

						}
						rdoTemp.Close();
					}
					else
					{
						if (intCodPriv == Convert.ToDouble(rdoTemp.Tables[0].Rows[0]["gsocieda"]))
						{
							sprDuplicados.Col = Col_Entidad;
							sprDuplicados.Text = (Convert.IsDBNull(strTexPriv)) ? "" : strTexPriv;
							sprDuplicados.ForeColor = Color.Black;

							sprDuplicados.Col = Col_Afilia;
							sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["nafiliac"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["nafiliac"]);
							sprDuplicados.ForeColor = Color.Black;

							sprDuplicados.Col = Col_CodiSoci;
							sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["gsocieda"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["gsocieda"]);
							sprDuplicados.SetColHidden(sprDuplicados.Col, true);
							rdoTemp.Close();
						}
						else
						{
                            strSql = "SELECT DENTIPAC.gsocieda, DENTIPAC.nafiliac, DSOCIEDAVA.dsocieda " + 
							         "FROM DENTIPAC " + 
							         "LEFT JOIN DSOCIEDAVA ON DENTIPAC.gsocieda = DSOCIEDAVA.gsocieda " + 
							         "WHERE DENTIPAC.gidenpac = '" + strgIndPac + "' AND " + 
							         "DENTIPAC.gsocieda = " + Convert.ToString(rdoTemp.Tables[0].Rows[0]["gsocieda"]);

							rdoTemp.Close();
							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(strSql, MFiliacion.GConexion);
							rdoTemp = new DataSet();
							tempAdapter_5.Fill(rdoTemp);
							if (rdoTemp.Tables[0].Rows.Count != 0)
							{
								sprDuplicados.Col = Col_Entidad;
								sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["dsocieda"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["dsocieda"]);
								sprDuplicados.ForeColor = Color.Black;

								sprDuplicados.Col = Col_Afilia;
								sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["nafiliac"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["nafiliac"]);
								sprDuplicados.ForeColor = Color.Black;

								sprDuplicados.Col = Col_CodiSoci;
								sprDuplicados.Text = (Convert.IsDBNull(rdoTemp.Tables[0].Rows[0]["gsocieda"])) ? "" : Convert.ToString(rdoTemp.Tables[0].Rows[0]["gsocieda"]);
								sprDuplicados.SetColHidden(sprDuplicados.Col, true);
							}
							rdoTemp.Close();
						}
					}
				}
			}
			else
			{
				rdoTemp.Close();
			}
		}

		private string fHistoria(string strgIndPac)
		{

			string result = String.Empty;
			result = "";
			string strSql = "SELECT ghistoria FROM HDOSSIER WHERE gidenpac = '" + strgIndPac + " '";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(strSql, MFiliacion.GConexion);
			DataSet rdoTemp = new DataSet();
			tempAdapter.Fill(rdoTemp);
			if (rdoTemp.Tables[0].Rows.Count != 0)
			{
				result = Convert.ToString(rdoTemp.Tables[0].Rows[0]["ghistoria"]);
			}
			rdoTemp.Close();

			return result;
		}
		private void DFI111F4_Closed(Object eventSender, EventArgs eventArgs)
		{
		}
	}
}