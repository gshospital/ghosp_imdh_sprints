using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace filiacionE
{
	partial class DFI111F1
	{

		#region "Upgrade Support "
		private static DFI111F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI111F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI111F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbCancelar", "cbAceptar", "cmdOpen", "Label4", "Label6", "Label7", "Label8", "Label9", "lbDPHistoria", "lbDPEFinanciadora", "lbDPNAsegurado", "Label22", "Label55", "cbbDPEstado", "cbbDPCatSoc", "cbbDPPoblacion", "cbbNacPoblacion", "Label37", "Label39", "Label48", "Label15", "Label16", "Label36", "Label77", "cbbNacPais", "Label78", "cbbIdioma", "Label35", "cbbDtroPostal", "Label21", "_lblDatosPaciente_4", "_lblDatosPaciente_5", "_lblDatosPaciente_6", "_lblDatosPaciente_7", "_lblDatosPaciente_8", "_lblDatosPaciente_10", "cbbCanPrev", "lblCanPrev", "mebDPCodPostal", "tbDPDomicilio", "chbDPExitus", "sdcDPFechaFallecimiento", "Label10", "frmExitus", "tbDPDireccionEx", "tbDPExPob", "chbDPExtrangero", "Label69", "cbbDPPais", "Label42", "Label41", "Frame18", "tbDPExtension1", "tbDPTelf1", "cbDBuscar", "_rbSexo_2", "_rbSexo_0", "_rbSexo_1", "frmSexo", "tbNombre", "tbApellido1", "tbApellido2", "chbFiliacPov", "sdcFechaNac", "_lblDatosPaciente_3", "_lblDatosPaciente_2", "_lblDatosPaciente_1", "_lblDatosPaciente_0", "Label52", "Label28", "Label50", "FrmDatosPersonales", "_rbAutorizacion_1", "_rbAutorizacion_0", "Frame5", "tbTarjeta", "tbDPOtrosDomic", "tbDPNDomic", "tbDPDomicilioP", "cbbDPVia", "_Label54_1", "_Label54_0", "Label3", "Label53", "Frame6", "cbDBuscar4", "tbDPExtension3", "tbEmail", "tbDPPasaporte", "tbDPTelf3", "tbDPTelf2", "tbDPExtension2", "chkObservaciones", "tbObservaciones", "Frame13", "_rbDNIOtroDocDP_1", "_rbDNIOtroDocDP_0", "tbNIFOtrosDocDP", "frmDNI_OtrosDocumentos", "tbTarjetaEuropea", "chbSMS", "lbCipAutonomico", "cbbOrigInfo", "_rbBloqueo_1", "_rbBloqueo_0", "Frame12", "_tsDatosPac_TabPage0", "cmdConsultaActualizacion", "chkValidarRE", "cmdFacturacionP", "cmdDeudas", "chkPropioPaciente", "tbREPasaporte", "cbBuscar2", "tbREDireccionEx", "chbREExtrangero", "tbREExPob", "Label70", "cbbREPais", "Label45", "Label44", "Frame4", "tbREApe2", "tbREApe1", "tbRENombre", "tbREDomicilio", "mebRECodPostal", "tbControl12", "tbBanco1", "tbSucursal1", "tbControl1", "tbCuenta1", "lbNcuenta21", "FrmCB21", "tbBanco", "tbSucursal", "tbControl", "tbCuenta", "Label46", "Frame9", "cbbREPago", "Frame8", "_rbDNIOtroDocRE_1", "_rbDNIOtroDocRE_0", "tbNIFOtrosDocRE", "Frame17", "Label43", "cbbREPoblacion", "Label27", "Label30", "Label25", "Label18", "Label17", "Label20", "frmPrivado", "TBBandaMagnetica", "CbbObraSocial", "chkExentoIVA", "_rbIndicador_1", "_rbIndicador_0", "tbRENomTitular", "Label5", "frmIndicador", "tbRECobertura", "tbRENVersion", "cbBuscarSoc", "tbREEmpresa", "cbbRESociedad", "mebREFechaCaducidad", "mebREFechaAlta", "tbRENPoliza", "Label1", "lblObraSocial", "Label73", "Label72", "Label71", "LbVersion", "Label38", "Label12", "Label14", "frmSociedad", "cbbHospitalReferencia", "cbbEmplazamiento", "tbRENomTitularSS", "_rbIndicadorSS_0", "_rbIndicadorSS_1", "rbPensionista", "rbActivo", "Frame1", "Label47", "Frame10", "tbRENSS", "cbBuscarCIAS", "MEBCIAS", "cbBuscarCentro", "Label76", "Label75", "cbbREInspeccion", "Label2", "chkZona", "Label29", "Label11", "Label13", "frmSegSocial", "frmTitular", "_rbRERegimen_2", "_rbRERegimen_1", "_rbRERegimen_0", "frmRERegimen", "_tsDatosPac_TabPage1", "_rbDNIOtroDocPC_0", "_rbDNIOtroDocPC_1", "tbNIFOtrosDocPC", "Frame16", "_rbResponsable_1", "_rbResponsable_0", "_LabelContacto_10", "Frame15", "_rbTutor_1", "_rbTutor_0", "Frame14", "cbBuscar3", "chbPCExtrangero", "tbPCExPob", "cbbPCPais", "_LabelContacto_11", "_LabelContacto_12", "Frame7", "tbPCApe2", "tbPCApe1", "tbPCNombre", "tbPCDomicilio", "tbPCTelf2", "tbPCTelf1", "tbPCExt2", "tbPCParentesco", "cbPCAceptar", "cbPCCancelar", "cbDPEliminar", "tbPCExt1", "mebPCCodPostal", "Label49", "cbbPCPoblacion", "_LabelContacto_4", "_LabelContacto_5", "Label64", "_LabelContacto_9", "_LabelContacto_8", "_LabelContacto_7", "_LabelContacto_6", "_LabelContacto_3", "_LabelContacto_2", "_LabelContacto_1", "_LabelContacto_0", "Frame3", "sprContactos", "Frame11", "_tsDatosPac_TabPage2", "tbOtrasCircunstanciasGradoDep", "_sdcGradoDep_0", "_sdcGradoDep_1", "_sdcGradoDep_2", "_sdcGradoDep_3", "_sdcGradoDep_4", "_sdcGradoDep_5", "_lbDependencia_3", "_lbDependencia_5", "_lbDependencia_4", "_lbDependencia_2", "_lbDependencia_1", "_lbDependencia_0", "frmGradoDependencia", "tbNumTrabajador", "cbbCategoria", "cbbParentesco", "_labelColectivos_2", "_labelColectivos_1", "_labelColectivos_0", "fraColectivos", "lbNombrePaciente", "lbPaciente", "_tsDatosPac_TabPage3", "tsDatosPac", "lbDesactivado", "lblFechaUltMod", "lblTituloFechaUltMod", "Label54", "LabelContacto", "labelColectivos", "lbDependencia", "lblDatosPaciente", "rbAutorizacion", "rbBloqueo", "rbDNIOtroDocDP", "rbDNIOtroDocPC", "rbDNIOtroDocRE", "rbIndicador", "rbIndicadorSS", "rbRERegimen", "rbResponsable", "rbSexo", "rbTutor", "sdcGradoDep", "commandButtonHelper1", "sprContactos_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.ToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cmdOpen;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadLabel Label9;
		public Telerik.WinControls.UI.RadTextBox lbDPHistoria;
		public Telerik.WinControls.UI.RadTextBox lbDPEFinanciadora;
		public Telerik.WinControls.UI.RadTextBox lbDPNAsegurado;
		public Telerik.WinControls.UI.RadLabel Label22;
		public Telerik.WinControls.UI.RadLabel Label55;
		public UpgradeHelpers.MSForms.MSCombobox cbbDPEstado;
		public UpgradeHelpers.MSForms.MSCombobox cbbDPCatSoc;
		public UpgradeHelpers.MSForms.MSCombobox cbbDPPoblacion;
		public UpgradeHelpers.MSForms.MSCombobox cbbNacPoblacion;
		public Telerik.WinControls.UI.RadLabel Label37;
		public Telerik.WinControls.UI.RadLabel Label39;
		public Telerik.WinControls.UI.RadLabel Label48;
		public Telerik.WinControls.UI.RadLabel Label15;
		public Telerik.WinControls.UI.RadLabel Label16;
		public Telerik.WinControls.UI.RadLabel Label36;
		public Telerik.WinControls.UI.RadLabel Label77;
		public  UpgradeHelpers.MSForms.MSCombobox cbbNacPais;
		public Telerik.WinControls.UI.RadLabel Label78;
		public UpgradeHelpers.MSForms.MSCombobox cbbIdioma;
		public Telerik.WinControls.UI.RadLabel Label35;
		public UpgradeHelpers.MSForms.MSCombobox cbbDtroPostal;
		public Telerik.WinControls.UI.RadLabel Label21;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_4;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_5;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_6;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_7;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_8;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_10;
		public Telerik.WinControls.UI.RadMultiColumnComboBox cbbCanPrev;
		public Telerik.WinControls.UI.RadLabel lblCanPrev;
		public Telerik.WinControls.UI.RadMaskedEditBox mebDPCodPostal;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPDomicilio;
		public Telerik.WinControls.UI.RadCheckBox chbDPExitus;
		public Telerik.WinControls.UI.RadDateTimePicker sdcDPFechaFallecimiento;
		public Telerik.WinControls.UI.RadLabel Label10;
		public Telerik.WinControls.UI.RadGroupBox frmExitus;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPDireccionEx;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPExPob;
		public Telerik.WinControls.UI.RadCheckBox chbDPExtrangero;
		public Telerik.WinControls.UI.RadLabel Label69;
		public UpgradeHelpers.MSForms.MSCombobox cbbDPPais;
		public Telerik.WinControls.UI.RadLabel Label42;
		public Telerik.WinControls.UI.RadLabel Label41;
		public Telerik.WinControls.UI.RadGroupBox Frame18;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPExtension1;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPTelf1;
		public Telerik.WinControls.UI.RadButton cbDBuscar;
		private Telerik.WinControls.UI.RadRadioButton _rbSexo_2;
		private Telerik.WinControls.UI.RadRadioButton _rbSexo_0;
		private Telerik.WinControls.UI.RadRadioButton _rbSexo_1;
		public Telerik.WinControls.UI.RadGroupBox frmSexo;
		public Telerik.WinControls.UI.RadTextBoxControl tbNombre;
		public Telerik.WinControls.UI.RadTextBoxControl tbApellido1;
		public Telerik.WinControls.UI.RadTextBoxControl tbApellido2;
		public Telerik.WinControls.UI.RadCheckBox chbFiliacPov;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechaNac;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_3;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_2;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_1;
		private Telerik.WinControls.UI.RadLabel _lblDatosPaciente_0;
		public Telerik.WinControls.UI.RadLabel Label52;
		public Telerik.WinControls.UI.RadLabel Label28;
		public Telerik.WinControls.UI.RadLabel Label50;
		public Telerik.WinControls.UI.RadGroupBox FrmDatosPersonales;
		private Telerik.WinControls.UI.RadRadioButton _rbAutorizacion_1;
		private Telerik.WinControls.UI.RadRadioButton _rbAutorizacion_0;
		public Telerik.WinControls.UI.RadGroupBox Frame5;
		public Telerik.WinControls.UI.RadTextBoxControl tbTarjeta;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPOtrosDomic;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPNDomic;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPDomicilioP;
		public UpgradeHelpers.MSForms.MSCombobox cbbDPVia;
		private Telerik.WinControls.UI.RadLabel _Label54_1;
		private Telerik.WinControls.UI.RadLabel _Label54_0;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadLabel Label53;
		public Telerik.WinControls.UI.RadGroupBox Frame6;
		public Telerik.WinControls.UI.RadButton cbDBuscar4;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPExtension3;
		public Telerik.WinControls.UI.RadTextBoxControl tbEmail;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPPasaporte;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPTelf3;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPTelf2;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPExtension2;
		public Telerik.WinControls.UI.RadCheckBox chkObservaciones;
		public Telerik.WinControls.UI.RadTextBoxControl tbObservaciones;
		public Telerik.WinControls.UI.RadGroupBox Frame13;
		private Telerik.WinControls.UI.RadRadioButton _rbDNIOtroDocDP_1;
		private Telerik.WinControls.UI.RadRadioButton _rbDNIOtroDocDP_0;
		public Telerik.WinControls.UI.RadMaskedEditBox tbNIFOtrosDocDP;
		public Telerik.WinControls.UI.RadGroupBox frmDNI_OtrosDocumentos;
		public Telerik.WinControls.UI.RadTextBoxControl tbTarjetaEuropea;
		public Telerik.WinControls.UI.RadCheckBox chbSMS;
		public Telerik.WinControls.UI.RadTextBoxControl lbCipAutonomico;
		public Telerik.WinControls.UI.RadDropDownList cbbOrigInfo;
		private Telerik.WinControls.UI.RadRadioButton _rbBloqueo_1;
		private Telerik.WinControls.UI.RadRadioButton _rbBloqueo_0;
		public Telerik.WinControls.UI.RadGroupBox Frame12;
		private Telerik.WinControls.UI.RadPageViewPage _tsDatosPac_TabPage0;
		public Telerik.WinControls.UI.RadButton cmdConsultaActualizacion;
		public Telerik.WinControls.UI.RadCheckBox chkValidarRE;
		public Telerik.WinControls.UI.RadButton cmdFacturacionP;
		public Telerik.WinControls.UI.RadButton cmdDeudas;
		public Telerik.WinControls.UI.RadCheckBox chkPropioPaciente;
		public Telerik.WinControls.UI.RadTextBoxControl tbREPasaporte;
		public Telerik.WinControls.UI.RadButton cbBuscar2;
		public Telerik.WinControls.UI.RadTextBoxControl tbREDireccionEx;
		public Telerik.WinControls.UI.RadCheckBox chbREExtrangero;
		public Telerik.WinControls.UI.RadTextBoxControl tbREExPob;
		public Telerik.WinControls.UI.RadLabel Label70;
		public UpgradeHelpers.MSForms.MSCombobox cbbREPais;
		public Telerik.WinControls.UI.RadLabel Label45;
		public Telerik.WinControls.UI.RadLabel Label44;
		public Telerik.WinControls.UI.RadGroupBox Frame4;
		public Telerik.WinControls.UI.RadTextBoxControl tbREApe2;
		public Telerik.WinControls.UI.RadTextBoxControl tbREApe1;
		public Telerik.WinControls.UI.RadTextBoxControl tbRENombre;
		public Telerik.WinControls.UI.RadTextBoxControl tbREDomicilio;
		public Telerik.WinControls.UI.RadMaskedEditBox mebRECodPostal;
		public Telerik.WinControls.UI.RadTextBoxControl tbControl12;
		public Telerik.WinControls.UI.RadTextBoxControl tbBanco1;
		public Telerik.WinControls.UI.RadTextBoxControl tbSucursal1;
		public Telerik.WinControls.UI.RadTextBoxControl tbControl1;
		public Telerik.WinControls.UI.RadTextBoxControl tbCuenta1;
		public Telerik.WinControls.UI.RadLabel lbNcuenta21;
		public Telerik.WinControls.UI.RadGroupBox FrmCB21;
		public Telerik.WinControls.UI.RadTextBoxControl tbBanco;
		public Telerik.WinControls.UI.RadTextBoxControl tbSucursal;
		public Telerik.WinControls.UI.RadTextBoxControl tbControl;
		public Telerik.WinControls.UI.RadTextBoxControl tbCuenta;
		public Telerik.WinControls.UI.RadLabel Label46;
		public Telerik.WinControls.UI.RadGroupBox Frame9;
        //public UpgradeHelpers.MSForms.MSCombobox cbbREPago;
        public Telerik.WinControls.UI.RadMultiColumnComboBox cbbREPago;
        public Telerik.WinControls.UI.RadGroupBox Frame8;
		private Telerik.WinControls.UI.RadRadioButton _rbDNIOtroDocRE_1;
		private Telerik.WinControls.UI.RadRadioButton _rbDNIOtroDocRE_0;
		public Telerik.WinControls.UI.RadMaskedEditBox tbNIFOtrosDocRE;
		public Telerik.WinControls.UI.RadGroupBox Frame17;
		public Telerik.WinControls.UI.RadLabel Label43;
		public UpgradeHelpers.MSForms.MSCombobox cbbREPoblacion;
		public Telerik.WinControls.UI.RadLabel Label27;
		public Telerik.WinControls.UI.RadLabel Label30;
		public Telerik.WinControls.UI.RadLabel Label25;
		public Telerik.WinControls.UI.RadLabel Label18;
		public Telerik.WinControls.UI.RadLabel Label17;
		public Telerik.WinControls.UI.RadLabel Label20;
		public Telerik.WinControls.UI.RadGroupBox frmPrivado;
		public Telerik.WinControls.UI.RadTextBoxControl TBBandaMagnetica;
		public Telerik.WinControls.UI.RadDropDownList CbbObraSocial;
		public Telerik.WinControls.UI.RadCheckBox chkExentoIVA;
		private Telerik.WinControls.UI.RadRadioButton _rbIndicador_1;
		private Telerik.WinControls.UI.RadRadioButton _rbIndicador_0;
		public Telerik.WinControls.UI.RadTextBoxControl tbRENomTitular;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadGroupBox frmIndicador;
		public Telerik.WinControls.UI.RadTextBoxControl tbRECobertura;
		public Telerik.WinControls.UI.RadTextBoxControl tbRENVersion;
		public Telerik.WinControls.UI.RadButton cbBuscarSoc;
		public Telerik.WinControls.UI.RadTextBoxControl tbREEmpresa;
		public Telerik.WinControls.UI.RadDropDownList cbbRESociedad;
		public Telerik.WinControls.UI.RadMaskedEditBox mebREFechaCaducidad;
		public Telerik.WinControls.UI.RadMaskedEditBox mebREFechaAlta;
		public Telerik.WinControls.UI.RadMaskedEditBox tbRENPoliza;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel lblObraSocial;
		public Telerik.WinControls.UI.RadLabel Label73;
		public Telerik.WinControls.UI.RadLabel Label72;
		public Telerik.WinControls.UI.RadLabel Label71;
		public Telerik.WinControls.UI.RadLabel LbVersion;
		public Telerik.WinControls.UI.RadLabel Label38;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadLabel Label14;
		public Telerik.WinControls.UI.RadGroupBox frmSociedad;
		public Telerik.WinControls.UI.RadDropDownList cbbHospitalReferencia;
		public Telerik.WinControls.UI.RadDropDownList cbbEmplazamiento;
		public Telerik.WinControls.UI.RadTextBoxControl tbRENomTitularSS;
		private Telerik.WinControls.UI.RadRadioButton _rbIndicadorSS_0;
		private Telerik.WinControls.UI.RadRadioButton _rbIndicadorSS_1;
		public Telerik.WinControls.UI.RadRadioButton rbPensionista;
		public Telerik.WinControls.UI.RadRadioButton rbActivo;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadLabel Label47;
		public Telerik.WinControls.UI.RadGroupBox Frame10;
		public Telerik.WinControls.UI.RadTextBoxControl tbRENSS;
		public Telerik.WinControls.UI.RadButton cbBuscarCIAS;
		public Telerik.WinControls.UI.RadMaskedEditBox MEBCIAS;
		public Telerik.WinControls.UI.RadButton cbBuscarCentro;
		public Telerik.WinControls.UI.RadLabel Label76;
		public Telerik.WinControls.UI.RadLabel Label75;
		public UpgradeHelpers.MSForms.MSCombobox cbbREInspeccion;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadCheckBox chkZona;
		public Telerik.WinControls.UI.RadLabel Label29;
		public Telerik.WinControls.UI.RadLabel Label11;
		public Telerik.WinControls.UI.RadLabel Label13;
		public Telerik.WinControls.UI.RadGroupBox frmSegSocial;
		public Telerik.WinControls.UI.RadGroupBox frmTitular;
		private Telerik.WinControls.UI.RadRadioButton _rbRERegimen_2;
		private Telerik.WinControls.UI.RadRadioButton _rbRERegimen_1;
		private Telerik.WinControls.UI.RadRadioButton _rbRERegimen_0;
		public Telerik.WinControls.UI.RadGroupBox frmRERegimen;
		private Telerik.WinControls.UI.RadPageViewPage _tsDatosPac_TabPage1;
		private Telerik.WinControls.UI.RadRadioButton _rbDNIOtroDocPC_0;
		private Telerik.WinControls.UI.RadRadioButton _rbDNIOtroDocPC_1;
		public Telerik.WinControls.UI.RadMaskedEditBox tbNIFOtrosDocPC;
		public Telerik.WinControls.UI.RadGroupBox Frame16;
		private Telerik.WinControls.UI.RadRadioButton _rbResponsable_1;
		private Telerik.WinControls.UI.RadRadioButton _rbResponsable_0;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_10;
		public Telerik.WinControls.UI.RadGroupBox Frame15;
		private Telerik.WinControls.UI.RadRadioButton _rbTutor_1;
		private Telerik.WinControls.UI.RadRadioButton _rbTutor_0;
		public Telerik.WinControls.UI.RadGroupBox Frame14;
		public Telerik.WinControls.UI.RadButton cbBuscar3;
		public Telerik.WinControls.UI.RadCheckBox chbPCExtrangero;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCExPob;
		public UpgradeHelpers.MSForms.MSCombobox cbbPCPais;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_11;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_12;
		public Telerik.WinControls.UI.RadGroupBox Frame7;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCApe2;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCApe1;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCNombre;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCDomicilio;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCTelf2;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCTelf1;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCExt2;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCParentesco;
		public Telerik.WinControls.UI.RadButton cbPCAceptar;
		public Telerik.WinControls.UI.RadButton cbPCCancelar;
		public Telerik.WinControls.UI.RadButton cbDPEliminar;
		public Telerik.WinControls.UI.RadTextBoxControl tbPCExt1;
		public Telerik.WinControls.UI.RadMaskedEditBox mebPCCodPostal;
		public Telerik.WinControls.UI.RadLabel Label49;
		public UpgradeHelpers.MSForms.MSCombobox cbbPCPoblacion;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_4;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_5;
		public Telerik.WinControls.UI.RadLabel Label64;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_9;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_8;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_7;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_6;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_3;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_2;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_1;
		private Telerik.WinControls.UI.RadLabel _LabelContacto_0;
		public Telerik.WinControls.UI.RadGroupBox Frame3;
		public UpgradeHelpers.Spread.FpSpread sprContactos;
		public Telerik.WinControls.UI.RadGroupBox Frame11;
		private Telerik.WinControls.UI.RadPageViewPage _tsDatosPac_TabPage2;
		public Telerik.WinControls.UI.RadTextBoxControl tbOtrasCircunstanciasGradoDep;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcGradoDep_0;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcGradoDep_1;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcGradoDep_2;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcGradoDep_3;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcGradoDep_4;
		private Telerik.WinControls.UI.RadDateTimePicker _sdcGradoDep_5;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_3;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_5;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_4;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_2;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_1;
		private Telerik.WinControls.UI.RadLabel _lbDependencia_0;
		public Telerik.WinControls.UI.RadGroupBox frmGradoDependencia;
		public Telerik.WinControls.UI.RadTextBoxControl tbNumTrabajador;
		public UpgradeHelpers.MSForms.MSCombobox cbbCategoria;
		public UpgradeHelpers.MSForms.MSCombobox cbbParentesco;
		private Telerik.WinControls.UI.RadLabel _labelColectivos_2;
		private Telerik.WinControls.UI.RadLabel _labelColectivos_1;
		private Telerik.WinControls.UI.RadLabel _labelColectivos_0;
		public Telerik.WinControls.UI.RadGroupBox fraColectivos;
		public Telerik.WinControls.UI.RadTextBox lbNombrePaciente;
		public Telerik.WinControls.UI.RadLabel lbPaciente;
		private Telerik.WinControls.UI.RadPageViewPage _tsDatosPac_TabPage3;
		public Telerik.WinControls.UI.RadPageView tsDatosPac;
		public Telerik.WinControls.UI.RadLabel lbDesactivado;
		public Telerik.WinControls.UI.RadLabel lblTituloFechaUltMod;
        public Telerik.WinControls.UI.RadTextBox lblFechaUltMod;
        public Telerik.WinControls.UI.RadLabel[] Label54 = new Telerik.WinControls.UI.RadLabel[2];
		public Telerik.WinControls.UI.RadLabel[] LabelContacto = new Telerik.WinControls.UI.RadLabel[13];
		public Telerik.WinControls.UI.RadLabel[] labelColectivos = new Telerik.WinControls.UI.RadLabel[3];
		public Telerik.WinControls.UI.RadLabel[] lbDependencia = new Telerik.WinControls.UI.RadLabel[6];
		public Telerik.WinControls.UI.RadLabel[] lblDatosPaciente = new Telerik.WinControls.UI.RadLabel[11];
		public Telerik.WinControls.UI.RadRadioButton[] rbAutorizacion = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbBloqueo = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbDNIOtroDocDP = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbDNIOtroDocPC = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbDNIOtroDocRE = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbIndicador = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbIndicadorSS = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbRERegimen = new Telerik.WinControls.UI.RadRadioButton[3];
		public Telerik.WinControls.UI.RadRadioButton[] rbResponsable = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadRadioButton[] rbSexo = new Telerik.WinControls.UI.RadRadioButton[3];
		public Telerik.WinControls.UI.RadRadioButton[] rbTutor = new Telerik.WinControls.UI.RadRadioButton[2];
		public Telerik.WinControls.UI.RadDateTimePicker[] sdcGradoDep = new Telerik.WinControls.UI.RadDateTimePicker[6];
		private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
		private int tsDatosPacPreviousTab;


        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19;
        public Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFI111F1));
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.lblFechaUltMod = new Telerik.WinControls.UI.RadTextBox();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.cmdOpen = new Telerik.WinControls.UI.RadButton();
            this.tsDatosPac = new Telerik.WinControls.UI.RadPageView();
            this._tsDatosPac_TabPage0 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Frame12 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbBloqueo_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbBloqueo_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.Label7 = new Telerik.WinControls.UI.RadLabel();
            this.Label8 = new Telerik.WinControls.UI.RadLabel();
            this.Label9 = new Telerik.WinControls.UI.RadLabel();
            this.lbDPHistoria = new Telerik.WinControls.UI.RadTextBox();
            this.lbDPEFinanciadora = new Telerik.WinControls.UI.RadTextBox();
            this.lbDPNAsegurado = new Telerik.WinControls.UI.RadTextBox();
            this.Label22 = new Telerik.WinControls.UI.RadLabel();
            this.Label55 = new Telerik.WinControls.UI.RadLabel();
            this.cbbDPEstado = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbDPCatSoc = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbDPPoblacion = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbNacPoblacion = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label37 = new Telerik.WinControls.UI.RadLabel();
            this.Label39 = new Telerik.WinControls.UI.RadLabel();
            this.Label48 = new Telerik.WinControls.UI.RadLabel();
            this.Label15 = new Telerik.WinControls.UI.RadLabel();
            this.Label16 = new Telerik.WinControls.UI.RadLabel();
            this.Label77 = new Telerik.WinControls.UI.RadLabel();
            this.cbbNacPais = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label78 = new Telerik.WinControls.UI.RadLabel();
            this.cbbIdioma = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label35 = new Telerik.WinControls.UI.RadLabel();
            this.cbbDtroPostal = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label21 = new Telerik.WinControls.UI.RadLabel();
            this._lblDatosPaciente_4 = new Telerik.WinControls.UI.RadLabel();
            this._lblDatosPaciente_5 = new Telerik.WinControls.UI.RadLabel();
            this._lblDatosPaciente_6 = new Telerik.WinControls.UI.RadLabel();
            this._lblDatosPaciente_7 = new Telerik.WinControls.UI.RadLabel();
            this._lblDatosPaciente_8 = new Telerik.WinControls.UI.RadLabel();
            this._lblDatosPaciente_10 = new Telerik.WinControls.UI.RadLabel();
            this.cbbCanPrev = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblCanPrev = new Telerik.WinControls.UI.RadLabel();
            this.mebDPCodPostal = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.frmExitus = new Telerik.WinControls.UI.RadGroupBox();
            this.chbDPExitus = new Telerik.WinControls.UI.RadCheckBox();
            this.sdcDPFechaFallecimiento = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Label10 = new Telerik.WinControls.UI.RadLabel();
            this.Frame18 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbDPDireccionEx = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbDPExPob = new Telerik.WinControls.UI.RadTextBoxControl();
            this.chbDPExtrangero = new Telerik.WinControls.UI.RadCheckBox();
            this.Label69 = new Telerik.WinControls.UI.RadLabel();
            this.cbbDPPais = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label42 = new Telerik.WinControls.UI.RadLabel();
            this.Label41 = new Telerik.WinControls.UI.RadLabel();
            this.tbDPExtension1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbDPTelf1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbDBuscar = new Telerik.WinControls.UI.RadButton();
            this.FrmDatosPersonales = new Telerik.WinControls.UI.RadGroupBox();
            this.frmSexo = new Telerik.WinControls.UI.RadGroupBox();
            this._rbSexo_2 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbSexo_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbSexo_1 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbNombre = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbApellido1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbApellido2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.chbFiliacPov = new Telerik.WinControls.UI.RadCheckBox();
            this.sdcFechaNac = new Telerik.WinControls.UI.RadDateTimePicker();
            this._lblDatosPaciente_3 = new Telerik.WinControls.UI.RadLabel();
            this._lblDatosPaciente_2 = new Telerik.WinControls.UI.RadLabel();
            this._lblDatosPaciente_1 = new Telerik.WinControls.UI.RadLabel();
            this._lblDatosPaciente_0 = new Telerik.WinControls.UI.RadLabel();
            this.Label52 = new Telerik.WinControls.UI.RadLabel();
            this.Label28 = new Telerik.WinControls.UI.RadLabel();
            this.Label50 = new Telerik.WinControls.UI.RadLabel();
            this.Frame5 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbAutorizacion_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbAutorizacion_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbTarjeta = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Frame6 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbDPOtrosDomic = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbDPNDomic = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbDPDomicilioP = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbbDPVia = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this._Label54_1 = new Telerik.WinControls.UI.RadLabel();
            this._Label54_0 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.Label53 = new Telerik.WinControls.UI.RadLabel();
            this.cbDBuscar4 = new Telerik.WinControls.UI.RadButton();
            this.tbDPExtension3 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbEmail = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbDPPasaporte = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbDPTelf3 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbDPTelf2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbDPExtension2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Frame13 = new Telerik.WinControls.UI.RadGroupBox();
            this.chkObservaciones = new Telerik.WinControls.UI.RadCheckBox();
            this.tbObservaciones = new Telerik.WinControls.UI.RadTextBoxControl();
            this.frmDNI_OtrosDocumentos = new Telerik.WinControls.UI.RadGroupBox();
            this._rbDNIOtroDocDP_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbDNIOtroDocDP_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbNIFOtrosDocDP = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbTarjetaEuropea = new Telerik.WinControls.UI.RadTextBoxControl();
            this.chbSMS = new Telerik.WinControls.UI.RadCheckBox();
            this.lbCipAutonomico = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbbOrigInfo = new Telerik.WinControls.UI.RadDropDownList();
            this.tbDPDomicilio = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Label36 = new Telerik.WinControls.UI.RadLabel();
            this._tsDatosPac_TabPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.cmdConsultaActualizacion = new Telerik.WinControls.UI.RadButton();
            this.chkValidarRE = new Telerik.WinControls.UI.RadCheckBox();
            this.frmPrivado = new Telerik.WinControls.UI.RadGroupBox();
            this.chkPropioPaciente = new Telerik.WinControls.UI.RadCheckBox();
            this.tbREPasaporte = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cmdFacturacionP = new Telerik.WinControls.UI.RadButton();
            this.cmdDeudas = new Telerik.WinControls.UI.RadButton();
            this.cbBuscar2 = new Telerik.WinControls.UI.RadButton();
            this.Frame4 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbREDireccionEx = new Telerik.WinControls.UI.RadTextBoxControl();
            this.chbREExtrangero = new Telerik.WinControls.UI.RadCheckBox();
            this.tbREExPob = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Label70 = new Telerik.WinControls.UI.RadLabel();
            this.cbbREPais = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label45 = new Telerik.WinControls.UI.RadLabel();
            this.Label44 = new Telerik.WinControls.UI.RadLabel();
            this.tbREApe2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbREApe1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbRENombre = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbREDomicilio = new Telerik.WinControls.UI.RadTextBoxControl();
            this.mebRECodPostal = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Frame8 = new Telerik.WinControls.UI.RadGroupBox();
            this.FrmCB21 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbControl12 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbBanco1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbSucursal1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbControl1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbCuenta1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.lbNcuenta21 = new Telerik.WinControls.UI.RadLabel();
            this.Frame9 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbBanco = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbSucursal = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbControl = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbCuenta = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Label46 = new Telerik.WinControls.UI.RadLabel();
            this.cbbREPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Frame17 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbDNIOtroDocRE_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbDNIOtroDocRE_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbNIFOtrosDocRE = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Label43 = new Telerik.WinControls.UI.RadLabel();
            this.cbbREPoblacion = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label27 = new Telerik.WinControls.UI.RadLabel();
            this.Label30 = new Telerik.WinControls.UI.RadLabel();
            this.Label25 = new Telerik.WinControls.UI.RadLabel();
            this.Label18 = new Telerik.WinControls.UI.RadLabel();
            this.Label17 = new Telerik.WinControls.UI.RadLabel();
            this.Label20 = new Telerik.WinControls.UI.RadLabel();
            this.frmTitular = new Telerik.WinControls.UI.RadGroupBox();
            this.frmSociedad = new Telerik.WinControls.UI.RadGroupBox();
            this.TBBandaMagnetica = new Telerik.WinControls.UI.RadTextBoxControl();
            this.CbbObraSocial = new Telerik.WinControls.UI.RadDropDownList();
            this.chkExentoIVA = new Telerik.WinControls.UI.RadCheckBox();
            this.frmIndicador = new Telerik.WinControls.UI.RadGroupBox();
            this._rbIndicador_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbIndicador_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbRENomTitular = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.tbRECobertura = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbRENVersion = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbBuscarSoc = new Telerik.WinControls.UI.RadButton();
            this.tbREEmpresa = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbbRESociedad = new Telerik.WinControls.UI.RadDropDownList();
            this.mebREFechaCaducidad = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.mebREFechaAlta = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tbRENPoliza = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.lblObraSocial = new Telerik.WinControls.UI.RadLabel();
            this.Label73 = new Telerik.WinControls.UI.RadLabel();
            this.Label72 = new Telerik.WinControls.UI.RadLabel();
            this.Label71 = new Telerik.WinControls.UI.RadLabel();
            this.LbVersion = new Telerik.WinControls.UI.RadLabel();
            this.Label38 = new Telerik.WinControls.UI.RadLabel();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.Label14 = new Telerik.WinControls.UI.RadLabel();
            this.frmSegSocial = new Telerik.WinControls.UI.RadGroupBox();
            this.cbbHospitalReferencia = new Telerik.WinControls.UI.RadDropDownList();
            this.cbbEmplazamiento = new Telerik.WinControls.UI.RadDropDownList();
            this.Frame10 = new Telerik.WinControls.UI.RadGroupBox();
            this.tbRENomTitularSS = new Telerik.WinControls.UI.RadTextBoxControl();
            this._rbIndicadorSS_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbIndicadorSS_1 = new Telerik.WinControls.UI.RadRadioButton();
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.rbPensionista = new Telerik.WinControls.UI.RadRadioButton();
            this.rbActivo = new Telerik.WinControls.UI.RadRadioButton();
            this.Label47 = new Telerik.WinControls.UI.RadLabel();
            this.tbRENSS = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbBuscarCIAS = new Telerik.WinControls.UI.RadButton();
            this.MEBCIAS = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.cbBuscarCentro = new Telerik.WinControls.UI.RadButton();
            this.Label76 = new Telerik.WinControls.UI.RadLabel();
            this.Label75 = new Telerik.WinControls.UI.RadLabel();
            this.cbbREInspeccion = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.chkZona = new Telerik.WinControls.UI.RadCheckBox();
            this.Label29 = new Telerik.WinControls.UI.RadLabel();
            this.Label11 = new Telerik.WinControls.UI.RadLabel();
            this.Label13 = new Telerik.WinControls.UI.RadLabel();
            this.frmRERegimen = new Telerik.WinControls.UI.RadGroupBox();
            this._rbRERegimen_2 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbRERegimen_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbRERegimen_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._tsDatosPac_TabPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.Frame3 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame16 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbDNIOtroDocPC_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbDNIOtroDocPC_1 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbNIFOtrosDocPC = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Frame15 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbResponsable_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbResponsable_0 = new Telerik.WinControls.UI.RadRadioButton();
            this._LabelContacto_10 = new Telerik.WinControls.UI.RadLabel();
            this.Frame14 = new Telerik.WinControls.UI.RadGroupBox();
            this._rbTutor_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbTutor_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.cbBuscar3 = new Telerik.WinControls.UI.RadButton();
            this.Frame7 = new Telerik.WinControls.UI.RadGroupBox();
            this.chbPCExtrangero = new Telerik.WinControls.UI.RadCheckBox();
            this.tbPCExPob = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbbPCPais = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this._LabelContacto_11 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_12 = new Telerik.WinControls.UI.RadLabel();
            this.tbPCApe2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbPCApe1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbPCNombre = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbPCDomicilio = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbPCTelf2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbPCTelf1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbPCExt2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbPCParentesco = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbPCAceptar = new Telerik.WinControls.UI.RadButton();
            this.cbPCCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbDPEliminar = new Telerik.WinControls.UI.RadButton();
            this.tbPCExt1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.mebPCCodPostal = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Label49 = new Telerik.WinControls.UI.RadLabel();
            this.cbbPCPoblacion = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this._LabelContacto_4 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_5 = new Telerik.WinControls.UI.RadLabel();
            this.Label64 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_9 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_8 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_7 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_6 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_3 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_2 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_1 = new Telerik.WinControls.UI.RadLabel();
            this._LabelContacto_0 = new Telerik.WinControls.UI.RadLabel();
            this.Frame11 = new Telerik.WinControls.UI.RadGroupBox();
            this.sprContactos = new UpgradeHelpers.Spread.FpSpread();
            this._tsDatosPac_TabPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.fraColectivos = new Telerik.WinControls.UI.RadGroupBox();
            this.tbNumTrabajador = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbbCategoria = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbParentesco = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this._labelColectivos_2 = new Telerik.WinControls.UI.RadLabel();
            this._labelColectivos_1 = new Telerik.WinControls.UI.RadLabel();
            this._labelColectivos_0 = new Telerik.WinControls.UI.RadLabel();
            this.frmGradoDependencia = new Telerik.WinControls.UI.RadGroupBox();
            this.tbOtrasCircunstanciasGradoDep = new Telerik.WinControls.UI.RadTextBoxControl();
            this._sdcGradoDep_0 = new Telerik.WinControls.UI.RadDateTimePicker();
            this._sdcGradoDep_1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this._sdcGradoDep_2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this._sdcGradoDep_3 = new Telerik.WinControls.UI.RadDateTimePicker();
            this._sdcGradoDep_4 = new Telerik.WinControls.UI.RadDateTimePicker();
            this._sdcGradoDep_5 = new Telerik.WinControls.UI.RadDateTimePicker();
            this._lbDependencia_3 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_5 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_4 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_2 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_1 = new Telerik.WinControls.UI.RadLabel();
            this._lbDependencia_0 = new Telerik.WinControls.UI.RadLabel();
            this.lbNombrePaciente = new Telerik.WinControls.UI.RadTextBox();
            this.lbPaciente = new Telerik.WinControls.UI.RadLabel();
            this.lbDesactivado = new Telerik.WinControls.UI.RadLabel();
            this.lblTituloFechaUltMod = new Telerik.WinControls.UI.RadLabel();
            this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaUltMod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOpen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsDatosPac)).BeginInit();
            this.tsDatosPac.SuspendLayout();
            this._tsDatosPac_TabPage0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).BeginInit();
            this.Frame12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbBloqueo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBloqueo_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDPHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDPEFinanciadora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDPNAsegurado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPCatSoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNacPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNacPais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbIdioma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDtroPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCanPrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCanPrev.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCanPrev.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCanPrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDPCodPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmExitus)).BeginInit();
            this.frmExitus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbDPExitus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDPFechaFallecimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame18)).BeginInit();
            this.Frame18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPDireccionEx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPExPob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbDPExtrangero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPPais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPExtension1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPTelf1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmDatosPersonales)).BeginInit();
            this.FrmDatosPersonales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmSexo)).BeginInit();
            this.frmSexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFiliacPov)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaNac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbAutorizacion_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbAutorizacion_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTarjeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPOtrosDomic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPNDomic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPDomicilioP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPVia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label54_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label54_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDBuscar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPExtension3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPPasaporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPTelf3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPTelf2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPExtension2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame13)).BeginInit();
            this.Frame13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkObservaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbObservaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmDNI_OtrosDocumentos)).BeginInit();
            this.frmDNI_OtrosDocumentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocDP_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocDP_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNIFOtrosDocDP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTarjetaEuropea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbSMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCipAutonomico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOrigInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
            this._tsDatosPac_TabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdConsultaActualizacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkValidarRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPrivado)).BeginInit();
            this.frmPrivado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropioPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREPasaporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFacturacionP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeudas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbREDireccionEx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbREExtrangero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREExPob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREApe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREApe1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebRECodPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).BeginInit();
            this.Frame8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrmCB21)).BeginInit();
            this.FrmCB21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBanco1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSucursal1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCuenta1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNcuenta21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
            this.Frame9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame17)).BeginInit();
            this.Frame17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocRE_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocRE_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNIFOtrosDocRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmTitular)).BeginInit();
            this.frmTitular.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmSociedad)).BeginInit();
            this.frmSociedad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TBBandaMagnetica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbObraSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExentoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmIndicador)).BeginInit();
            this.frmIndicador.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicador_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicador_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENomTitular)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRECobertura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscarSoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRESociedad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebREFechaCaducidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebREFechaAlta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENPoliza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObraSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmSegSocial)).BeginInit();
            this.frmSegSocial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHospitalReferencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbEmplazamiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame10)).BeginInit();
            this.Frame10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENomTitularSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicadorSS_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicadorSS_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbPensionista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscarCIAS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MEBCIAS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscarCentro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREInspeccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkZona)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmRERegimen)).BeginInit();
            this.frmRERegimen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_0)).BeginInit();
            this._tsDatosPac_TabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame16)).BeginInit();
            this.Frame16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocPC_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocPC_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNIFOtrosDocPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame15)).BeginInit();
            this.Frame15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbResponsable_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbResponsable_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame14)).BeginInit();
            this.Frame14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbTutor_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbTutor_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbPCExtrangero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCExPob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPCPais)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCApe2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCApe1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCTelf2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCTelf1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCExt2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCParentesco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPCAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPCCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDPEliminar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCExt1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebPCCodPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPCPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame11)).BeginInit();
            this.Frame11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sprContactos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprContactos.MasterTemplate)).BeginInit();
            this._tsDatosPac_TabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraColectivos)).BeginInit();
            this.fraColectivos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNumTrabajador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentesco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._labelColectivos_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._labelColectivos_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._labelColectivos_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmGradoDependencia)).BeginInit();
            this.frmGradoDependencia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOtrasCircunstanciasGradoDep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombrePaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDesactivado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTituloFechaUltMod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFechaUltMod
            // 
            this.lblFechaUltMod.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblFechaUltMod.Enabled = false;
            this.lblFechaUltMod.Location = new System.Drawing.Point(148, 577);
            this.lblFechaUltMod.Name = "lblFechaUltMod";
            this.lblFechaUltMod.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFechaUltMod.Size = new System.Drawing.Size(75, 20);
            this.lblFechaUltMod.TabIndex = 220;
            this.ToolTipMain.SetToolTip(this.lblFechaUltMod, "a");
            this.lblFechaUltMod.Visible = false;
            // 
            // cbCancelar
            // 
            this.cbCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbCancelar.Location = new System.Drawing.Point(675, 572);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.Size = new System.Drawing.Size(81, 28);
            this.cbCancelar.TabIndex = 0;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbAceptar.Enabled = false;
            this.cbAceptar.Location = new System.Drawing.Point(583, 572);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.Size = new System.Drawing.Size(81, 28);
            this.cbAceptar.TabIndex = 1;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // cmdOpen
            // 
            this.cmdOpen.Image = ((System.Drawing.Image)(resources.GetObject("cmdOpen.Image")));
            this.cmdOpen.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdOpen.Location = new System.Drawing.Point(547, 577);
            this.cmdOpen.Name = "cmdOpen";
            this.cmdOpen.Size = new System.Drawing.Size(27, 23);
            this.cmdOpen.TabIndex = 2;
            this.cmdOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdOpen.Click += new System.EventHandler(this.cmdOpen_Click);
            // 
            // tsDatosPac
            // 
            this.tsDatosPac.Controls.Add(this._tsDatosPac_TabPage0);
            this.tsDatosPac.Controls.Add(this._tsDatosPac_TabPage1);
            this.tsDatosPac.Controls.Add(this._tsDatosPac_TabPage2);
            this.tsDatosPac.Controls.Add(this._tsDatosPac_TabPage3);
            this.tsDatosPac.ItemSize = new System.Drawing.Size(187, 26);
            this.tsDatosPac.Location = new System.Drawing.Point(6, 4);
            this.tsDatosPac.Name = "tsDatosPac";
            this.tsDatosPac.SelectedPage = this._tsDatosPac_TabPage3;
            this.tsDatosPac.Size = new System.Drawing.Size(759, 567);
            this.tsDatosPac.TabIndex = 105;
            this.tsDatosPac.SelectedPageChanged += new System.EventHandler(this.tsDatosPac_SelectedIndexChanged);
            this.tsDatosPac.Enter += new System.EventHandler(this.tsDatosPac_Enter);
            // 
            // _tsDatosPac_TabPage0
            // 
            this._tsDatosPac_TabPage0.Controls.Add(this.Frame12);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label4);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label6);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label7);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label8);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label9);
            this._tsDatosPac_TabPage0.Controls.Add(this.lbDPHistoria);
            this._tsDatosPac_TabPage0.Controls.Add(this.lbDPEFinanciadora);
            this._tsDatosPac_TabPage0.Controls.Add(this.lbDPNAsegurado);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label22);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label55);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbbDPEstado);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbbDPCatSoc);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbbDPPoblacion);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbbNacPoblacion);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label37);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label39);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label48);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label15);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label16);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label77);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbbNacPais);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label78);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbbIdioma);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label35);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbbDtroPostal);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label21);
            this._tsDatosPac_TabPage0.Controls.Add(this._lblDatosPaciente_4);
            this._tsDatosPac_TabPage0.Controls.Add(this._lblDatosPaciente_5);
            this._tsDatosPac_TabPage0.Controls.Add(this._lblDatosPaciente_6);
            this._tsDatosPac_TabPage0.Controls.Add(this._lblDatosPaciente_7);
            this._tsDatosPac_TabPage0.Controls.Add(this._lblDatosPaciente_8);
            this._tsDatosPac_TabPage0.Controls.Add(this._lblDatosPaciente_10);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbbCanPrev);
            this._tsDatosPac_TabPage0.Controls.Add(this.lblCanPrev);
            this._tsDatosPac_TabPage0.Controls.Add(this.mebDPCodPostal);
            this._tsDatosPac_TabPage0.Controls.Add(this.frmExitus);
            this._tsDatosPac_TabPage0.Controls.Add(this.Frame18);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbDPExtension1);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbDPTelf1);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbDBuscar);
            this._tsDatosPac_TabPage0.Controls.Add(this.FrmDatosPersonales);
            this._tsDatosPac_TabPage0.Controls.Add(this.Frame5);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbTarjeta);
            this._tsDatosPac_TabPage0.Controls.Add(this.Frame6);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbDBuscar4);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbDPExtension3);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbEmail);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbDPPasaporte);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbDPTelf3);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbDPTelf2);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbDPExtension2);
            this._tsDatosPac_TabPage0.Controls.Add(this.Frame13);
            this._tsDatosPac_TabPage0.Controls.Add(this.frmDNI_OtrosDocumentos);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbTarjetaEuropea);
            this._tsDatosPac_TabPage0.Controls.Add(this.chbSMS);
            this._tsDatosPac_TabPage0.Controls.Add(this.lbCipAutonomico);
            this._tsDatosPac_TabPage0.Controls.Add(this.cbbOrigInfo);
            this._tsDatosPac_TabPage0.Controls.Add(this.tbDPDomicilio);
            this._tsDatosPac_TabPage0.Controls.Add(this.Label36);
            this._tsDatosPac_TabPage0.ItemSize = new System.Drawing.SizeF(91F, 28F);
            this._tsDatosPac_TabPage0.Location = new System.Drawing.Point(10, 37);
            this._tsDatosPac_TabPage0.Name = "_tsDatosPac_TabPage0";
            this._tsDatosPac_TabPage0.Size = new System.Drawing.Size(738, 519);
            this._tsDatosPac_TabPage0.Text = "Datos paciente";
            // 
            // Frame12
            // 
            this.Frame12.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame12.Controls.Add(this._rbBloqueo_1);
            this.Frame12.Controls.Add(this._rbBloqueo_0);
            this.Frame12.HeaderText = "Paciente bloqueado";
            this.Frame12.Location = new System.Drawing.Point(306, 337);
            this.Frame12.Name = "Frame12";
            this.Frame12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame12.Size = new System.Drawing.Size(129, 41);
            this.Frame12.TabIndex = 239;
            this.Frame12.Text = "Paciente bloqueado";
            // 
            // _rbBloqueo_1
            // 
            this._rbBloqueo_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBloqueo_1.Location = new System.Drawing.Point(74, 17);
            this._rbBloqueo_1.Name = "_rbBloqueo_1";
            this._rbBloqueo_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBloqueo_1.Size = new System.Drawing.Size(35, 18);
            this._rbBloqueo_1.TabIndex = 241;
            this._rbBloqueo_1.Text = "No";
            // 
            // _rbBloqueo_0
            // 
            this._rbBloqueo_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBloqueo_0.Location = new System.Drawing.Point(20, 17);
            this._rbBloqueo_0.Name = "_rbBloqueo_0";
            this._rbBloqueo_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBloqueo_0.Size = new System.Drawing.Size(29, 18);
            this._rbBloqueo_0.TabIndex = 240;
            this._rbBloqueo_0.Text = "Si";
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(134, 214);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(58, 18);
            this.Label4.TabIndex = 200;
            this.Label4.Text = "Poblaci�n:";
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Location = new System.Drawing.Point(10, 238);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(62, 18);
            this.Label6.TabIndex = 201;
            this.Label6.Text = "Tel�fono 1:";
            // 
            // Label7
            // 
            this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label7.Location = new System.Drawing.Point(252, 240);
            this.Label7.Name = "Label7";
            this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label7.Size = new System.Drawing.Size(62, 18);
            this.Label7.TabIndex = 202;
            this.Label7.Text = "Tel�fono 2:";
            // 
            // Label8
            // 
            this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label8.Location = new System.Drawing.Point(10, 288);
            this.Label8.Name = "Label8";
            this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label8.Size = new System.Drawing.Size(64, 18);
            this.Label8.TabIndex = 203;
            this.Label8.Text = "Estado civil:";
            // 
            // Label9
            // 
            this.Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label9.Location = new System.Drawing.Point(209, 288);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label9.Size = new System.Drawing.Size(94, 18);
            this.Label9.TabIndex = 204;
            this.Label9.Text = "Valoraci�n Social:";
            // 
            // lbDPHistoria
            // 
            this.lbDPHistoria.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDPHistoria.Enabled = false;
            this.lbDPHistoria.Location = new System.Drawing.Point(646, 143);
            this.lbDPHistoria.Name = "lbDPHistoria";
            this.lbDPHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDPHistoria.Size = new System.Drawing.Size(89, 20);
            this.lbDPHistoria.TabIndex = 108;
            // 
            // lbDPEFinanciadora
            // 
            this.lbDPEFinanciadora.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDPEFinanciadora.Enabled = false;
            this.lbDPEFinanciadora.Location = new System.Drawing.Point(324, 93);
            this.lbDPEFinanciadora.Name = "lbDPEFinanciadora";
            this.lbDPEFinanciadora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDPEFinanciadora.Size = new System.Drawing.Size(260, 20);
            this.lbDPEFinanciadora.TabIndex = 107;
            // 
            // lbDPNAsegurado
            // 
            this.lbDPNAsegurado.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDPNAsegurado.Enabled = false;
            this.lbDPNAsegurado.Location = new System.Drawing.Point(96, 91);
            this.lbDPNAsegurado.Name = "lbDPNAsegurado";
            this.lbDPNAsegurado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDPNAsegurado.Size = new System.Drawing.Size(124, 20);
            this.lbDPNAsegurado.TabIndex = 106;
            // 
            // Label22
            // 
            this.Label22.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label22.Location = new System.Drawing.Point(10, 213);
            this.Label22.Name = "Label22";
            this.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label22.Size = new System.Drawing.Size(65, 18);
            this.Label22.TabIndex = 205;
            this.Label22.Text = "C�d. postal:";
            // 
            // Label55
            // 
            this.Label55.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label55.Location = new System.Drawing.Point(10, 315);
            this.Label55.Name = "Label55";
            this.Label55.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label55.Size = new System.Drawing.Size(132, 18);
            this.Label55.TabIndex = 206;
            this.Label55.Text = "Poblaci�n de nacimiento:";
            // 
            // cbbDPEstado
            // 
            this.cbbDPEstado.ColumnWidths = "";
            this.cbbDPEstado.Location = new System.Drawing.Point(76, 283);
            this.cbbDPEstado.Name = "cbbDPEstado";
            this.cbbDPEstado.Size = new System.Drawing.Size(123, 20);
            this.cbbDPEstado.TabIndex = 29;
            // 
            // cbbDPCatSoc
            // 
            this.cbbDPCatSoc.ColumnWidths = "";
            this.cbbDPCatSoc.Location = new System.Drawing.Point(309, 283);
            this.cbbDPCatSoc.Name = "cbbDPCatSoc";
            this.cbbDPCatSoc.Size = new System.Drawing.Size(238, 20);
            this.cbbDPCatSoc.TabIndex = 30;
            // 
            // cbbDPPoblacion
            // 
            this.cbbDPPoblacion.ColumnWidths = "";
            this.cbbDPPoblacion.Location = new System.Drawing.Point(208, 211);
            this.cbbDPPoblacion.Name = "cbbDPPoblacion";
            this.cbbDPPoblacion.Size = new System.Drawing.Size(273, 20);
            this.cbbDPPoblacion.TabIndex = 19;
            this.cbbDPPoblacion.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbDPPoblacion_SelectedIndexChanged);
            this.cbbDPPoblacion.Click += new System.EventHandler(this.cbbDPPoblacion_Click);
            // 
            // cbbNacPoblacion
            // 
            this.cbbNacPoblacion.ColumnWidths = "";
            this.cbbNacPoblacion.Location = new System.Drawing.Point(152, 314);
            this.cbbNacPoblacion.Name = "cbbNacPoblacion";
            this.cbbNacPoblacion.Size = new System.Drawing.Size(253, 20);
            this.cbbNacPoblacion.TabIndex = 32;
            // 
            // Label37
            // 
            this.Label37.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label37.Location = new System.Drawing.Point(10, 260);
            this.Label37.Name = "Label37";
            this.Label37.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label37.Size = new System.Drawing.Size(40, 18);
            this.Label37.TabIndex = 208;
            this.Label37.Text = "E-mail:";
            // 
            // Label39
            // 
            this.Label39.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label39.Location = new System.Drawing.Point(416, 260);
            this.Label39.Name = "Label39";
            this.Label39.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label39.Size = new System.Drawing.Size(58, 18);
            this.Label39.TabIndex = 209;
            this.Label39.Text = "Pasaporte:";
            // 
            // Label48
            // 
            this.Label48.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label48.Location = new System.Drawing.Point(10, 355);
            this.Label48.Name = "Label48";
            this.Label48.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label48.Size = new System.Drawing.Size(107, 18);
            this.Label48.TabIndex = 210;
            this.Label48.Text = "Idioma del paciente:";
            // 
            // Label15
            // 
            this.Label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label15.Location = new System.Drawing.Point(182, 239);
            this.Label15.Name = "Label15";
            this.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label15.Size = new System.Drawing.Size(23, 18);
            this.Label15.TabIndex = 221;
            this.Label15.Text = "Ext:";
            // 
            // Label16
            // 
            this.Label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label16.Location = new System.Drawing.Point(412, 239);
            this.Label16.Name = "Label16";
            this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label16.Size = new System.Drawing.Size(23, 18);
            this.Label16.TabIndex = 222;
            this.Label16.Text = "Ext:";
            // 
            // Label77
            // 
            this.Label77.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label77.Location = new System.Drawing.Point(454, 327);
            this.Label77.Name = "Label77";
            this.Label77.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label77.Size = new System.Drawing.Size(105, 18);
            this.Label77.TabIndex = 234;
            this.Label77.Text = "Pa�s de Nacimiento:";
            // 
            // cbbNacPais
            // 
            this.cbbNacPais.ColumnWidths = "";
            this.cbbNacPais.Location = new System.Drawing.Point(582, 322);
            this.cbbNacPais.Name = "cbbNacPais";
            this.cbbNacPais.Size = new System.Drawing.Size(153, 20);
            this.cbbNacPais.TabIndex = 34;
            // 
            // Label78
            // 
            this.Label78.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label78.Location = new System.Drawing.Point(454, 357);
            this.Label78.Name = "Label78";
            this.Label78.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label78.Size = new System.Drawing.Size(122, 18);
            this.Label78.TabIndex = 237;
            this.Label78.Text = "Fuente de Informaci�n:";
            // 
            // cbbIdioma
            // 
            this.cbbIdioma.ColumnWidths = "";
            this.cbbIdioma.Location = new System.Drawing.Point(152, 351);
            this.cbbIdioma.Name = "cbbIdioma";
            this.cbbIdioma.Size = new System.Drawing.Size(145, 20);
            this.cbbIdioma.TabIndex = 242;
            // 
            // Label35
            // 
            this.Label35.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label35.Location = new System.Drawing.Point(493, 240);
            this.Label35.Name = "Label35";
            this.Label35.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label35.Size = new System.Drawing.Size(36, 18);
            this.Label35.TabIndex = 207;
            this.Label35.Text = "M�vil:";
            // 
            // cbbDtroPostal
            // 
            this.cbbDtroPostal.ColumnWidths = "";
            this.cbbDtroPostal.Location = new System.Drawing.Point(590, 211);
            this.cbbDtroPostal.Name = "cbbDtroPostal";
            this.cbbDtroPostal.Size = new System.Drawing.Size(143, 20);
            this.cbbDtroPostal.TabIndex = 263;
            this.cbbDtroPostal.Visible = false;
            // 
            // Label21
            // 
            this.Label21.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label21.Location = new System.Drawing.Point(520, 214);
            this.Label21.Name = "Label21";
            this.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label21.Size = new System.Drawing.Size(68, 18);
            this.Label21.TabIndex = 264;
            this.Label21.Text = "D.Municipal:";
            this.Label21.Visible = false;
            // 
            // _lblDatosPaciente_4
            // 
            this._lblDatosPaciente_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_4.Location = new System.Drawing.Point(10, 91);
            this._lblDatosPaciente_4.Name = "_lblDatosPaciente_4";
            this._lblDatosPaciente_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_4.Size = new System.Drawing.Size(78, 18);
            this._lblDatosPaciente_4.TabIndex = 272;
            this._lblDatosPaciente_4.Text = "N� asegurado:";
            // 
            // _lblDatosPaciente_5
            // 
            this._lblDatosPaciente_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_5.Location = new System.Drawing.Point(233, 92);
            this._lblDatosPaciente_5.Name = "_lblDatosPaciente_5";
            this._lblDatosPaciente_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_5.Size = new System.Drawing.Size(82, 18);
            this._lblDatosPaciente_5.TabIndex = 273;
            this._lblDatosPaciente_5.Text = "E. financiadora:";
            // 
            // _lblDatosPaciente_6
            // 
            this._lblDatosPaciente_6.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_6.Location = new System.Drawing.Point(202, 122);
            this._lblDatosPaciente_6.Name = "_lblDatosPaciente_6";
            this._lblDatosPaciente_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_6.Size = new System.Drawing.Size(89, 18);
            this._lblDatosPaciente_6.TabIndex = 274;
            this._lblDatosPaciente_6.Text = "Tarjeta Sanitaria:";
            // 
            // _lblDatosPaciente_7
            // 
            this._lblDatosPaciente_7.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_7.Location = new System.Drawing.Point(202, 146);
            this._lblDatosPaciente_7.Name = "_lblDatosPaciente_7";
            this._lblDatosPaciente_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_7.Size = new System.Drawing.Size(92, 18);
            this._lblDatosPaciente_7.TabIndex = 275;
            this._lblDatosPaciente_7.Text = "T. Sanit. Europea:";
            // 
            // _lblDatosPaciente_8
            // 
            this._lblDatosPaciente_8.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_8.Location = new System.Drawing.Point(474, 122);
            this._lblDatosPaciente_8.Name = "_lblDatosPaciente_8";
            this._lblDatosPaciente_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_8.Size = new System.Drawing.Size(90, 18);
            this._lblDatosPaciente_8.TabIndex = 276;
            this._lblDatosPaciente_8.Text = "CIP Auton�mico:";
            // 
            // _lblDatosPaciente_10
            // 
            this._lblDatosPaciente_10.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_10.Location = new System.Drawing.Point(590, 146);
            this._lblDatosPaciente_10.Name = "_lblDatosPaciente_10";
            this._lblDatosPaciente_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_10.Size = new System.Drawing.Size(55, 18);
            this._lblDatosPaciente_10.TabIndex = 277;
            this._lblDatosPaciente_10.Text = "H. Cl�nica:";
            // 
            // cbbCanPrev
            // 
            // 
            // cbbCanPrev.NestedRadGridView
            // 
            this.cbbCanPrev.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cbbCanPrev.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbCanPrev.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbCanPrev.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cbbCanPrev.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cbbCanPrev.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cbbCanPrev.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.cbbCanPrev.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cbbCanPrev.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cbbCanPrev.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.cbbCanPrev.EditorControl.Name = "NestedRadGridView";
            this.cbbCanPrev.EditorControl.ReadOnly = true;
            this.cbbCanPrev.EditorControl.ShowGroupPanel = false;
            this.cbbCanPrev.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cbbCanPrev.EditorControl.TabIndex = 0;
            this.cbbCanPrev.Location = new System.Drawing.Point(630, 283);
            this.cbbCanPrev.Name = "cbbCanPrev";
            this.cbbCanPrev.Size = new System.Drawing.Size(103, 20);
            this.cbbCanPrev.TabIndex = 31;
            this.cbbCanPrev.TabStop = false;
            // 
            // lblCanPrev
            // 
            this.lblCanPrev.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCanPrev.Location = new System.Drawing.Point(561, 288);
            this.lblCanPrev.Name = "lblCanPrev";
            this.lblCanPrev.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCanPrev.Size = new System.Drawing.Size(64, 18);
            this.lblCanPrev.TabIndex = 278;
            this.lblCanPrev.Text = "Canal Prev.:";
            // 
            // mebDPCodPostal
            // 
            this.mebDPCodPostal.Location = new System.Drawing.Point(76, 214);
            this.mebDPCodPostal.Name = "mebDPCodPostal";
            this.mebDPCodPostal.PromptChar = ' ';
            this.mebDPCodPostal.Size = new System.Drawing.Size(49, 20);
            this.mebDPCodPostal.TabIndex = 18;
            this.mebDPCodPostal.TabStop = false;
            this.mebDPCodPostal.Enter += new System.EventHandler(this.mebDPCodPostal_Enter);
            this.mebDPCodPostal.Leave += new System.EventHandler(this.mebDPCodPostal_Leave);
            // 
            // frmExitus
            // 
            this.frmExitus.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmExitus.Controls.Add(this.chbDPExitus);
            this.frmExitus.Controls.Add(this.sdcDPFechaFallecimiento);
            this.frmExitus.Controls.Add(this.Label10);
            this.frmExitus.HeaderText = "Exitus";
            this.frmExitus.Location = new System.Drawing.Point(8, 480);
            this.frmExitus.Name = "frmExitus";
            this.frmExitus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmExitus.Size = new System.Drawing.Size(367, 37);
            this.frmExitus.TabIndex = 185;
            this.frmExitus.Text = "Exitus";
            // 
            // chbDPExitus
            // 
            this.chbDPExitus.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbDPExitus.Location = new System.Drawing.Point(10, 16);
            this.chbDPExitus.Name = "chbDPExitus";
            this.chbDPExitus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbDPExitus.Size = new System.Drawing.Size(49, 18);
            this.chbDPExitus.TabIndex = 41;
            this.chbDPExitus.Text = "Exitus";
            this.chbDPExitus.CheckStateChanged += new System.EventHandler(this.chbDPExitus_CheckStateChanged);
            // 
            // sdcDPFechaFallecimiento
            // 
            this.sdcDPFechaFallecimiento.CustomFormat = "dd/MM/yyyy";
            this.sdcDPFechaFallecimiento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcDPFechaFallecimiento.Location = new System.Drawing.Point(194, 14);
            this.sdcDPFechaFallecimiento.Name = "sdcDPFechaFallecimiento";
            this.sdcDPFechaFallecimiento.Size = new System.Drawing.Size(123, 20);
            this.sdcDPFechaFallecimiento.TabIndex = 42;
            this.sdcDPFechaFallecimiento.TabStop = false;
            this.sdcDPFechaFallecimiento.Text = "01/06/2016";
            this.sdcDPFechaFallecimiento.Value = new System.DateTime(2016, 6, 1, 11, 22, 42, 928);
            this.sdcDPFechaFallecimiento.Enter += new System.EventHandler(this.sdcDPFechaFallecimiento_Enter);
            this.sdcDPFechaFallecimiento.Leave += new System.EventHandler(this.sdcDPFechaFallecimiento_Leave);
            // 
            // Label10
            // 
            this.Label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label10.Location = new System.Drawing.Point(70, 16);
            this.Label10.Name = "Label10";
            this.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label10.Size = new System.Drawing.Size(105, 18);
            this.Label10.TabIndex = 186;
            this.Label10.Text = "Fecha fallecimiento:";
            // 
            // Frame18
            // 
            this.Frame18.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame18.Controls.Add(this.tbDPDireccionEx);
            this.Frame18.Controls.Add(this.tbDPExPob);
            this.Frame18.Controls.Add(this.chbDPExtrangero);
            this.Frame18.Controls.Add(this.Label69);
            this.Frame18.Controls.Add(this.cbbDPPais);
            this.Frame18.Controls.Add(this.Label42);
            this.Frame18.Controls.Add(this.Label41);
            this.Frame18.HeaderText = "";
            this.Frame18.Location = new System.Drawing.Point(8, 380);
            this.Frame18.Name = "Frame18";
            this.Frame18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame18.Size = new System.Drawing.Size(727, 55);
            this.Frame18.TabIndex = 187;
            // 
            // tbDPDireccionEx
            // 
            this.tbDPDireccionEx.AcceptsReturn = true;
            this.tbDPDireccionEx.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPDireccionEx.Enabled = false;
            this.tbDPDireccionEx.Location = new System.Drawing.Point(213, 29);
            this.tbDPDireccionEx.MaxLength = 100;
            this.tbDPDireccionEx.Name = "tbDPDireccionEx";
            this.tbDPDireccionEx.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPDireccionEx.Size = new System.Drawing.Size(509, 19);
            this.tbDPDireccionEx.TabIndex = 38;
            this.tbDPDireccionEx.Enter += new System.EventHandler(this.tbDPDireccionEx_Enter);
            // 
            // tbDPExPob
            // 
            this.tbDPExPob.AcceptsReturn = true;
            this.tbDPExPob.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPExPob.Enabled = false;
            this.tbDPExPob.Location = new System.Drawing.Point(498, 4);
            this.tbDPExPob.MaxLength = 40;
            this.tbDPExPob.Name = "tbDPExPob";
            this.tbDPExPob.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPExPob.Size = new System.Drawing.Size(224, 19);
            this.tbDPExPob.TabIndex = 37;
            this.tbDPExPob.Enter += new System.EventHandler(this.tbDPExPob_Enter);
            this.tbDPExPob.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPExPob_KeyPress);
            // 
            // chbDPExtrangero
            // 
            this.chbDPExtrangero.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbDPExtrangero.Location = new System.Drawing.Point(8, 11);
            this.chbDPExtrangero.Name = "chbDPExtrangero";
            this.chbDPExtrangero.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbDPExtrangero.Size = new System.Drawing.Size(127, 18);
            this.chbDPExtrangero.TabIndex = 35;
            this.chbDPExtrangero.Text = "Residencia extranjero";
            this.chbDPExtrangero.CheckStateChanged += new System.EventHandler(this.chbDPExtrangero_CheckStateChanged);
            // 
            // Label69
            // 
            this.Label69.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label69.Enabled = false;
            this.Label69.Location = new System.Drawing.Point(144, 31);
            this.Label69.Name = "Label69";
            this.Label69.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label69.Size = new System.Drawing.Size(55, 18);
            this.Label69.TabIndex = 224;
            this.Label69.Text = "Direcci�n:";
            // 
            // cbbDPPais
            // 
            this.cbbDPPais.ColumnWidths = "";
            this.cbbDPPais.Enabled = false;
            this.cbbDPPais.Location = new System.Drawing.Point(213, 5);
            this.cbbDPPais.Name = "cbbDPPais";
            this.cbbDPPais.Size = new System.Drawing.Size(212, 20);
            this.cbbDPPais.TabIndex = 36;
            this.cbbDPPais.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbDPPais_SelectedIndexChanged);
            this.cbbDPPais.Click += new System.EventHandler(this.cbbDPPais_Click);
            // 
            // Label42
            // 
            this.Label42.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label42.Enabled = false;
            this.Label42.Location = new System.Drawing.Point(434, 4);
            this.Label42.Name = "Label42";
            this.Label42.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label42.Size = new System.Drawing.Size(58, 18);
            this.Label42.TabIndex = 189;
            this.Label42.Text = "Poblaci�n:";
            // 
            // Label41
            // 
            this.Label41.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label41.Enabled = false;
            this.Label41.Location = new System.Drawing.Point(144, 11);
            this.Label41.Name = "Label41";
            this.Label41.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label41.Size = new System.Drawing.Size(28, 18);
            this.Label41.TabIndex = 188;
            this.Label41.Text = "Pa�s:";
            // 
            // tbDPExtension1
            // 
            this.tbDPExtension1.AcceptsReturn = true;
            this.tbDPExtension1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPExtension1.Location = new System.Drawing.Point(208, 237);
            this.tbDPExtension1.MaxLength = 5;
            this.tbDPExtension1.Name = "tbDPExtension1";
            this.tbDPExtension1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPExtension1.Size = new System.Drawing.Size(42, 19);
            this.tbDPExtension1.TabIndex = 22;
            this.tbDPExtension1.Enter += new System.EventHandler(this.tbDPExtension1_Enter);
            this.tbDPExtension1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPExtension1_KeyPress);
            // 
            // tbDPTelf1
            // 
            this.tbDPTelf1.AcceptsReturn = true;
            this.tbDPTelf1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPTelf1.Location = new System.Drawing.Point(76, 237);
            this.tbDPTelf1.MaxLength = 15;
            this.tbDPTelf1.Name = "tbDPTelf1";
            this.tbDPTelf1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPTelf1.Size = new System.Drawing.Size(102, 19);
            this.tbDPTelf1.TabIndex = 21;
            this.tbDPTelf1.Enter += new System.EventHandler(this.tbDPTelf1_Enter);
            this.tbDPTelf1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPTelf1_KeyPress);
            // 
            // cbDBuscar
            // 
            this.cbDBuscar.Image = ((System.Drawing.Image)(resources.GetObject("cbDBuscar.Image")));
            this.cbDBuscar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbDBuscar.Location = new System.Drawing.Point(488, 212);
            this.cbDBuscar.Name = "cbDBuscar";
            this.cbDBuscar.Size = new System.Drawing.Size(25, 21);
            this.cbDBuscar.TabIndex = 279;
            this.cbDBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbDBuscar.Click += new System.EventHandler(this.cbDBuscar_Click);
            // 
            // FrmDatosPersonales
            // 
            this.FrmDatosPersonales.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmDatosPersonales.Controls.Add(this.frmSexo);
            this.FrmDatosPersonales.Controls.Add(this.tbNombre);
            this.FrmDatosPersonales.Controls.Add(this.tbApellido1);
            this.FrmDatosPersonales.Controls.Add(this.tbApellido2);
            this.FrmDatosPersonales.Controls.Add(this.chbFiliacPov);
            this.FrmDatosPersonales.Controls.Add(this.sdcFechaNac);
            this.FrmDatosPersonales.Controls.Add(this._lblDatosPaciente_3);
            this.FrmDatosPersonales.Controls.Add(this._lblDatosPaciente_2);
            this.FrmDatosPersonales.Controls.Add(this._lblDatosPaciente_1);
            this.FrmDatosPersonales.Controls.Add(this._lblDatosPaciente_0);
            this.FrmDatosPersonales.Controls.Add(this.Label52);
            this.FrmDatosPersonales.Controls.Add(this.Label28);
            this.FrmDatosPersonales.Controls.Add(this.Label50);
            this.FrmDatosPersonales.HeaderText = "Datos Personales";
            this.FrmDatosPersonales.Location = new System.Drawing.Point(6, 0);
            this.FrmDatosPersonales.Name = "FrmDatosPersonales";
            this.FrmDatosPersonales.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmDatosPersonales.Size = new System.Drawing.Size(726, 85);
            this.FrmDatosPersonales.TabIndex = 190;
            this.FrmDatosPersonales.Text = "Datos Personales";
            // 
            // frmSexo
            // 
            this.frmSexo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmSexo.Controls.Add(this._rbSexo_2);
            this.frmSexo.Controls.Add(this._rbSexo_0);
            this.frmSexo.Controls.Add(this._rbSexo_1);
            this.frmSexo.HeaderText = "Sexo";
            this.frmSexo.Location = new System.Drawing.Point(606, 13);
            this.frmSexo.Name = "frmSexo";
            this.frmSexo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmSexo.Size = new System.Drawing.Size(113, 67);
            this.frmSexo.TabIndex = 191;
            this.frmSexo.Text = "Sexo";
            // 
            // _rbSexo_2
            // 
            this._rbSexo_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbSexo_2.Location = new System.Drawing.Point(6, 48);
            this._rbSexo_2.Name = "_rbSexo_2";
            this._rbSexo_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbSexo_2.Size = new System.Drawing.Size(94, 18);
            this._rbSexo_2.TabIndex = 7;
            this._rbSexo_2.Text = "Indeterminado";
            this._rbSexo_2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbSexo_CheckedChanged);
            // 
            // _rbSexo_0
            // 
            this._rbSexo_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbSexo_0.Location = new System.Drawing.Point(6, 14);
            this._rbSexo_0.Name = "_rbSexo_0";
            this._rbSexo_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbSexo_0.Size = new System.Drawing.Size(61, 18);
            this._rbSexo_0.TabIndex = 5;
            this._rbSexo_0.Text = "Hombre";
            this._rbSexo_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbSexo_CheckedChanged);
            // 
            // _rbSexo_1
            // 
            this._rbSexo_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbSexo_1.Location = new System.Drawing.Point(6, 30);
            this._rbSexo_1.Name = "_rbSexo_1";
            this._rbSexo_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbSexo_1.Size = new System.Drawing.Size(49, 18);
            this._rbSexo_1.TabIndex = 6;
            this._rbSexo_1.Text = "Mujer";
            this._rbSexo_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbSexo_CheckedChanged);
            // 
            // tbNombre
            // 
            this.tbNombre.AcceptsReturn = true;
            this.tbNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNombre.Location = new System.Drawing.Point(70, 49);
            this.tbNombre.MaxLength = 35;
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNombre.Size = new System.Drawing.Size(223, 19);
            this.tbNombre.TabIndex = 2;
            this.tbNombre.TextChanged += new System.EventHandler(this.tbNombre_TextChanged);
            this.tbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombre_KeyPress);
            // 
            // tbApellido1
            // 
            this.tbApellido1.AcceptsReturn = true;
            this.tbApellido1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbApellido1.Location = new System.Drawing.Point(70, 19);
            this.tbApellido1.MaxLength = 35;
            this.tbApellido1.Name = "tbApellido1";
            this.tbApellido1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbApellido1.Size = new System.Drawing.Size(223, 19);
            this.tbApellido1.TabIndex = 0;
            this.tbApellido1.TextChanged += new System.EventHandler(this.tbApellido1_TextChanged);
            this.tbApellido1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellido1_KeyPress);
            // 
            // tbApellido2
            // 
            this.tbApellido2.AcceptsReturn = true;
            this.tbApellido2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbApellido2.Location = new System.Drawing.Point(371, 15);
            this.tbApellido2.MaxLength = 35;
            this.tbApellido2.Name = "tbApellido2";
            this.tbApellido2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbApellido2.Size = new System.Drawing.Size(215, 19);
            this.tbApellido2.TabIndex = 1;
            this.tbApellido2.TextChanged += new System.EventHandler(this.tbApellido2_TextChanged);
            this.tbApellido2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellido2_KeyPress);
            // 
            // chbFiliacPov
            // 
            this.chbFiliacPov.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbFiliacPov.Location = new System.Drawing.Point(462, 62);
            this.chbFiliacPov.Name = "chbFiliacPov";
            this.chbFiliacPov.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbFiliacPov.Size = new System.Drawing.Size(74, 18);
            this.chbFiliacPov.TabIndex = 4;
            this.chbFiliacPov.Text = "Provisional";
            this.chbFiliacPov.Visible = false;
            // 
            // sdcFechaNac
            // 
            this.sdcFechaNac.CustomFormat = "dd/MM/yyyy";
            this.sdcFechaNac.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechaNac.Location = new System.Drawing.Point(410, 39);
            this.sdcFechaNac.Name = "sdcFechaNac";
            this.sdcFechaNac.Size = new System.Drawing.Size(123, 20);
            this.sdcFechaNac.TabIndex = 3;
            this.sdcFechaNac.TabStop = false;
            this.sdcFechaNac.Text = "17/05/2016";
            this.sdcFechaNac.Value = new System.DateTime(2016, 5, 17, 0, 0, 0, 0);
            this.sdcFechaNac.Closing += new Telerik.WinControls.UI.RadPopupClosingEventHandler(this.sdcFechaNac_Closing);
            this.sdcFechaNac.Enter += new System.EventHandler(this.sdcFechaNac_Enter);
            this.sdcFechaNac.Leave += new System.EventHandler(this.sdcFechaNac_Leave);
            // 
            // _lblDatosPaciente_3
            // 
            this._lblDatosPaciente_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_3.Location = new System.Drawing.Point(306, 42);
            this._lblDatosPaciente_3.Name = "_lblDatosPaciente_3";
            this._lblDatosPaciente_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_3.Size = new System.Drawing.Size(98, 18);
            this._lblDatosPaciente_3.TabIndex = 271;
            this._lblDatosPaciente_3.Text = "Fecha Nacimiento:";
            // 
            // _lblDatosPaciente_2
            // 
            this._lblDatosPaciente_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_2.Location = new System.Drawing.Point(306, 17);
            this._lblDatosPaciente_2.Name = "_lblDatosPaciente_2";
            this._lblDatosPaciente_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_2.Size = new System.Drawing.Size(59, 18);
            this._lblDatosPaciente_2.TabIndex = 270;
            this._lblDatosPaciente_2.Text = "Apellido 2:";
            // 
            // _lblDatosPaciente_1
            // 
            this._lblDatosPaciente_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_1.Location = new System.Drawing.Point(4, 51);
            this._lblDatosPaciente_1.Name = "_lblDatosPaciente_1";
            this._lblDatosPaciente_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_1.Size = new System.Drawing.Size(50, 18);
            this._lblDatosPaciente_1.TabIndex = 269;
            this._lblDatosPaciente_1.Text = "Nombre:";
            // 
            // _lblDatosPaciente_0
            // 
            this._lblDatosPaciente_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lblDatosPaciente_0.Location = new System.Drawing.Point(4, 23);
            this._lblDatosPaciente_0.Name = "_lblDatosPaciente_0";
            this._lblDatosPaciente_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lblDatosPaciente_0.Size = new System.Drawing.Size(59, 18);
            this._lblDatosPaciente_0.TabIndex = 268;
            this._lblDatosPaciente_0.Text = "Apellido 1:";
            // 
            // Label52
            // 
            this.Label52.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label52.Location = new System.Drawing.Point(2, 254);
            this.Label52.Name = "Label52";
            this.Label52.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label52.Size = new System.Drawing.Size(122, 18);
            this.Label52.TabIndex = 214;
            this.Label52.Text = "N�mero de Trabajador:";
            // 
            // Label28
            // 
            this.Label28.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label28.Location = new System.Drawing.Point(2, 290);
            this.Label28.Name = "Label28";
            this.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label28.Size = new System.Drawing.Size(151, 18);
            this.Label28.TabIndex = 213;
            this.Label28.Text = "Parentesco con el trabajador:";
            // 
            // Label50
            // 
            this.Label50.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label50.Location = new System.Drawing.Point(2, 328);
            this.Label50.Name = "Label50";
            this.Label50.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label50.Size = new System.Drawing.Size(91, 18);
            this.Label50.TabIndex = 212;
            this.Label50.Text = "Categor�a Espcial";
            // 
            // Frame5
            // 
            this.Frame5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame5.Controls.Add(this._rbAutorizacion_1);
            this.Frame5.Controls.Add(this._rbAutorizacion_0);
            this.Frame5.HeaderText = "Autorizaci�n utilizaci�n datos";
            this.Frame5.Location = new System.Drawing.Point(8, 437);
            this.Frame5.Name = "Frame5";
            this.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame5.Size = new System.Drawing.Size(184, 37);
            this.Frame5.TabIndex = 192;
            this.Frame5.Text = "Autorizaci�n utilizaci�n datos";
            // 
            // _rbAutorizacion_1
            // 
            this._rbAutorizacion_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbAutorizacion_1.Location = new System.Drawing.Point(96, 14);
            this._rbAutorizacion_1.Name = "_rbAutorizacion_1";
            this._rbAutorizacion_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbAutorizacion_1.Size = new System.Drawing.Size(35, 18);
            this._rbAutorizacion_1.TabIndex = 40;
            this._rbAutorizacion_1.Text = "No";
            this._rbAutorizacion_1.CheckStateChanged += new System.EventHandler(this.rbAutorizacion_CheckStateChanged);
            // 
            // _rbAutorizacion_0
            // 
            this._rbAutorizacion_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbAutorizacion_0.Location = new System.Drawing.Point(24, 14);
            this._rbAutorizacion_0.Name = "_rbAutorizacion_0";
            this._rbAutorizacion_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbAutorizacion_0.Size = new System.Drawing.Size(29, 18);
            this._rbAutorizacion_0.TabIndex = 39;
            this._rbAutorizacion_0.Text = "Si";
            this._rbAutorizacion_0.CheckStateChanged += new System.EventHandler(this.rbAutorizacion_CheckStateChanged);
            // 
            // tbTarjeta
            // 
            this.tbTarjeta.AcceptsReturn = true;
            this.tbTarjeta.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbTarjeta.Location = new System.Drawing.Point(299, 119);
            this.tbTarjeta.MaxLength = 30;
            this.tbTarjeta.Name = "tbTarjeta";
            this.tbTarjeta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbTarjeta.Size = new System.Drawing.Size(172, 19);
            this.tbTarjeta.TabIndex = 11;
            this.tbTarjeta.TextChanged += new System.EventHandler(this.tbTarjeta_TextChanged);
            this.tbTarjeta.Enter += new System.EventHandler(this.tbTarjeta_Enter);
            this.tbTarjeta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTarjeta_KeyPress);
            // 
            // Frame6
            // 
            this.Frame6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame6.Controls.Add(this.tbDPOtrosDomic);
            this.Frame6.Controls.Add(this.tbDPNDomic);
            this.Frame6.Controls.Add(this.tbDPDomicilioP);
            this.Frame6.Controls.Add(this.cbbDPVia);
            this.Frame6.Controls.Add(this._Label54_1);
            this.Frame6.Controls.Add(this._Label54_0);
            this.Frame6.Controls.Add(this.Label3);
            this.Frame6.Controls.Add(this.Label53);
            this.Frame6.HeaderText = "Domicilio";
            this.Frame6.Location = new System.Drawing.Point(6, 166);
            this.Frame6.Name = "Frame6";
            this.Frame6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame6.Size = new System.Drawing.Size(727, 42);
            this.Frame6.TabIndex = 193;
            this.Frame6.Text = "Domicilio";
            // 
            // tbDPOtrosDomic
            // 
            this.tbDPOtrosDomic.AcceptsReturn = true;
            this.tbDPOtrosDomic.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPOtrosDomic.Location = new System.Drawing.Point(590, 18);
            this.tbDPOtrosDomic.MaxLength = 100;
            this.tbDPOtrosDomic.Name = "tbDPOtrosDomic";
            this.tbDPOtrosDomic.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPOtrosDomic.Size = new System.Drawing.Size(117, 19);
            this.tbDPOtrosDomic.TabIndex = 17;
            this.tbDPOtrosDomic.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPOtrosDomic_KeyPress);
            // 
            // tbDPNDomic
            // 
            this.tbDPNDomic.AcceptsReturn = true;
            this.tbDPNDomic.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPNDomic.Location = new System.Drawing.Point(512, 18);
            this.tbDPNDomic.MaxLength = 100;
            this.tbDPNDomic.Name = "tbDPNDomic";
            this.tbDPNDomic.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPNDomic.Size = new System.Drawing.Size(37, 19);
            this.tbDPNDomic.TabIndex = 16;
            this.tbDPNDomic.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPNDomic_KeyPress);
            // 
            // tbDPDomicilioP
            // 
            this.tbDPDomicilioP.AcceptsReturn = true;
            this.tbDPDomicilioP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPDomicilioP.Location = new System.Drawing.Point(218, 18);
            this.tbDPDomicilioP.MaxLength = 100;
            this.tbDPDomicilioP.Name = "tbDPDomicilioP";
            this.tbDPDomicilioP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPDomicilioP.Size = new System.Drawing.Size(257, 19);
            this.tbDPDomicilioP.TabIndex = 15;
            this.tbDPDomicilioP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPDomicilioP_KeyPress);
            // 
            // cbbDPVia
            // 
            this.cbbDPVia.ColumnWidths = "";
            this.cbbDPVia.Location = new System.Drawing.Point(30, 16);
            this.cbbDPVia.Name = "cbbDPVia";
            this.cbbDPVia.Size = new System.Drawing.Size(121, 20);
            this.cbbDPVia.TabIndex = 14;
            // 
            // _Label54_1
            // 
            this._Label54_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._Label54_1.Location = new System.Drawing.Point(558, 20);
            this._Label54_1.Name = "_Label54_1";
            this._Label54_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Label54_1.Size = new System.Drawing.Size(36, 18);
            this._Label54_1.TabIndex = 197;
            this._Label54_1.Text = "Otros:";
            // 
            // _Label54_0
            // 
            this._Label54_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._Label54_0.Location = new System.Drawing.Point(494, 20);
            this._Label54_0.Name = "_Label54_0";
            this._Label54_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._Label54_0.Size = new System.Drawing.Size(20, 18);
            this._Label54_0.TabIndex = 196;
            this._Label54_0.Text = "N�";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(156, 20);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(56, 18);
            this.Label3.TabIndex = 195;
            this.Label3.Text = "Domicilio:";
            // 
            // Label53
            // 
            this.Label53.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label53.Location = new System.Drawing.Point(6, 20);
            this.Label53.Name = "Label53";
            this.Label53.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label53.Size = new System.Drawing.Size(24, 18);
            this.Label53.TabIndex = 194;
            this.Label53.Text = "Via:";
            // 
            // cbDBuscar4
            // 
            this.cbDBuscar4.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbDBuscar4.Image = ((System.Drawing.Image)(resources.GetObject("cbDBuscar4.Image")));
            this.cbDBuscar4.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbDBuscar4.Location = new System.Drawing.Point(410, 311);
            this.cbDBuscar4.Name = "cbDBuscar4";
            this.cbDBuscar4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbDBuscar4.Size = new System.Drawing.Size(25, 25);
            this.cbDBuscar4.TabIndex = 33;
            this.cbDBuscar4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbDBuscar4.Click += new System.EventHandler(this.cbDBuscar4_Click);
            // 
            // tbDPExtension3
            // 
            this.tbDPExtension3.AcceptsReturn = true;
            this.tbDPExtension3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPExtension3.Location = new System.Drawing.Point(531, 237);
            this.tbDPExtension3.MaxLength = 5;
            this.tbDPExtension3.Name = "tbDPExtension3";
            this.tbDPExtension3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPExtension3.Size = new System.Drawing.Size(41, 19);
            this.tbDPExtension3.TabIndex = 26;
            this.tbDPExtension3.Visible = false;
            this.tbDPExtension3.Enter += new System.EventHandler(this.tbDPExtension3_Enter);
            this.tbDPExtension3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPExtension3_KeyPress);
            // 
            // tbEmail
            // 
            this.tbEmail.AcceptsReturn = true;
            this.tbEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbEmail.Location = new System.Drawing.Point(76, 261);
            this.tbEmail.MaxLength = 100;
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbEmail.Size = new System.Drawing.Size(328, 19);
            this.tbEmail.TabIndex = 27;
            // 
            // tbDPPasaporte
            // 
            this.tbDPPasaporte.AcceptsReturn = true;
            this.tbDPPasaporte.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPPasaporte.Location = new System.Drawing.Point(474, 259);
            this.tbDPPasaporte.MaxLength = 15;
            this.tbDPPasaporte.Name = "tbDPPasaporte";
            this.tbDPPasaporte.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPPasaporte.Size = new System.Drawing.Size(121, 19);
            this.tbDPPasaporte.TabIndex = 28;
            this.tbDPPasaporte.Enter += new System.EventHandler(this.tbDPPasaporte_Enter);
            this.tbDPPasaporte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPPasaporte_KeyPress);
            // 
            // tbDPTelf3
            // 
            this.tbDPTelf3.AcceptsReturn = true;
            this.tbDPTelf3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPTelf3.Location = new System.Drawing.Point(531, 237);
            this.tbDPTelf3.MaxLength = 15;
            this.tbDPTelf3.Name = "tbDPTelf3";
            this.tbDPTelf3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPTelf3.Size = new System.Drawing.Size(103, 19);
            this.tbDPTelf3.TabIndex = 25;
            this.tbDPTelf3.TextChanged += new System.EventHandler(this.tbDPTelf3_TextChanged);
            this.tbDPTelf3.Enter += new System.EventHandler(this.tbDPTelf3_Enter);
            this.tbDPTelf3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPTelf3_KeyPress);
            this.tbDPTelf3.Leave += new System.EventHandler(this.tbDPTelf3_Leave);
            // 
            // tbDPTelf2
            // 
            this.tbDPTelf2.AcceptsReturn = true;
            this.tbDPTelf2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPTelf2.Location = new System.Drawing.Point(320, 237);
            this.tbDPTelf2.MaxLength = 15;
            this.tbDPTelf2.Name = "tbDPTelf2";
            this.tbDPTelf2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPTelf2.Size = new System.Drawing.Size(84, 19);
            this.tbDPTelf2.TabIndex = 23;
            this.tbDPTelf2.Enter += new System.EventHandler(this.tbDPTelf2_Enter);
            this.tbDPTelf2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPTelf2_KeyPress);
            // 
            // tbDPExtension2
            // 
            this.tbDPExtension2.AcceptsReturn = true;
            this.tbDPExtension2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPExtension2.Location = new System.Drawing.Point(442, 237);
            this.tbDPExtension2.MaxLength = 5;
            this.tbDPExtension2.Name = "tbDPExtension2";
            this.tbDPExtension2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPExtension2.Size = new System.Drawing.Size(41, 19);
            this.tbDPExtension2.TabIndex = 24;
            this.tbDPExtension2.Enter += new System.EventHandler(this.tbDPExtension2_Enter);
            this.tbDPExtension2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPExtension2_KeyPress);
            // 
            // Frame13
            // 
            this.Frame13.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame13.Controls.Add(this.chkObservaciones);
            this.Frame13.Controls.Add(this.tbObservaciones);
            this.Frame13.HeaderText = "Observaciones";
            this.Frame13.Location = new System.Drawing.Point(379, 438);
            this.Frame13.Name = "Frame13";
            this.Frame13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame13.Size = new System.Drawing.Size(356, 79);
            this.Frame13.TabIndex = 198;
            this.Frame13.Text = "Observaciones";
            // 
            // chkObservaciones
            // 
            this.chkObservaciones.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkObservaciones.Location = new System.Drawing.Point(118, 58);
            this.chkObservaciones.Name = "chkObservaciones";
            this.chkObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkObservaciones.Size = new System.Drawing.Size(226, 18);
            this.chkObservaciones.TabIndex = 44;
            this.chkObservaciones.Text = "Mostrar Observaciones registro Actividad";
            // 
            // tbObservaciones
            // 
            this.tbObservaciones.AcceptsReturn = true;
            this.tbObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbObservaciones.Location = new System.Drawing.Point(4, 16);
            this.tbObservaciones.MaxLength = 255;
            this.tbObservaciones.Multiline = true;
            this.tbObservaciones.Name = "tbObservaciones";
            this.tbObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbObservaciones.Size = new System.Drawing.Size(342, 40);
            this.tbObservaciones.TabIndex = 43;
            this.tbObservaciones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbObservaciones_KeyPress);
            // 
            // frmDNI_OtrosDocumentos
            // 
            this.frmDNI_OtrosDocumentos.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmDNI_OtrosDocumentos.Controls.Add(this._rbDNIOtroDocDP_1);
            this.frmDNI_OtrosDocumentos.Controls.Add(this._rbDNIOtroDocDP_0);
            this.frmDNI_OtrosDocumentos.Controls.Add(this.tbNIFOtrosDocDP);
            this.frmDNI_OtrosDocumentos.HeaderText = "";
            this.frmDNI_OtrosDocumentos.Location = new System.Drawing.Point(6, 114);
            this.frmDNI_OtrosDocumentos.Name = "frmDNI_OtrosDocumentos";
            this.frmDNI_OtrosDocumentos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmDNI_OtrosDocumentos.Size = new System.Drawing.Size(193, 50);
            this.frmDNI_OtrosDocumentos.TabIndex = 199;
            // 
            // _rbDNIOtroDocDP_1
            // 
            this._rbDNIOtroDocDP_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbDNIOtroDocDP_1.Location = new System.Drawing.Point(70, 4);
            this._rbDNIOtroDocDP_1.Name = "_rbDNIOtroDocDP_1";
            this._rbDNIOtroDocDP_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbDNIOtroDocDP_1.Size = new System.Drawing.Size(113, 18);
            this._rbDNIOtroDocDP_1.TabIndex = 9;
            this._rbDNIOtroDocDP_1.Text = "Otros documentos";
            this._rbDNIOtroDocDP_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbDNIOtroDocDP_CheckedChanged);
            // 
            // _rbDNIOtroDocDP_0
            // 
            this._rbDNIOtroDocDP_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbDNIOtroDocDP_0.Location = new System.Drawing.Point(6, 4);
            this._rbDNIOtroDocDP_0.Name = "_rbDNIOtroDocDP_0";
            this._rbDNIOtroDocDP_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbDNIOtroDocDP_0.Size = new System.Drawing.Size(61, 18);
            this._rbDNIOtroDocDP_0.TabIndex = 8;
            this._rbDNIOtroDocDP_0.Text = "DNI/NIF";
            this._rbDNIOtroDocDP_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbDNIOtroDocDP_CheckedChanged);
            // 
            // tbNIFOtrosDocDP
            // 
            this.tbNIFOtrosDocDP.Location = new System.Drawing.Point(32, 25);
            this.tbNIFOtrosDocDP.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbNIFOtrosDocDP.Name = "tbNIFOtrosDocDP";
            this.tbNIFOtrosDocDP.PromptChar = ' ';
            this.tbNIFOtrosDocDP.Size = new System.Drawing.Size(125, 20);
            this.tbNIFOtrosDocDP.TabIndex = 10;
            this.tbNIFOtrosDocDP.TabStop = false;
            this.tbNIFOtrosDocDP.Text = "            ";
            this.tbNIFOtrosDocDP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNIFOtrosDocDP_KeyPress);
            this.tbNIFOtrosDocDP.TextChanged += new System.EventHandler(this.tbNIFOtrosDocDP_TextChanged);
            this.tbNIFOtrosDocDP.Enter += new System.EventHandler(this.tbNIFOtrosDocDP_Enter);
            this.tbNIFOtrosDocDP.Leave += new System.EventHandler(this.tbNIFOtrosDocDP_Leave);
            // 
            // tbTarjetaEuropea
            // 
            this.tbTarjetaEuropea.AcceptsReturn = true;
            this.tbTarjetaEuropea.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbTarjetaEuropea.Location = new System.Drawing.Point(299, 143);
            this.tbTarjetaEuropea.MaxLength = 30;
            this.tbTarjetaEuropea.Name = "tbTarjetaEuropea";
            this.tbTarjetaEuropea.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbTarjetaEuropea.Size = new System.Drawing.Size(172, 19);
            this.tbTarjetaEuropea.TabIndex = 12;
            this.tbTarjetaEuropea.Enter += new System.EventHandler(this.tbTarjetaEuropea_Enter);
            this.tbTarjetaEuropea.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTarjetaEuropea_KeyPress);
            // 
            // chbSMS
            // 
            this.chbSMS.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbSMS.Enabled = false;
            this.chbSMS.Location = new System.Drawing.Point(638, 234);
            this.chbSMS.Name = "chbSMS";
            this.chbSMS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbSMS.Size = new System.Drawing.Size(163, 18);
            this.chbSMS.TabIndex = 229;
            this.chbSMS.Text = "Recibir SMS en este tel�fono";
            // 
            // lbCipAutonomico
            // 
            this.lbCipAutonomico.AcceptsReturn = true;
            this.lbCipAutonomico.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lbCipAutonomico.Location = new System.Drawing.Point(570, 119);
            this.lbCipAutonomico.MaxLength = 16;
            this.lbCipAutonomico.Name = "lbCipAutonomico";
            this.lbCipAutonomico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbCipAutonomico.Size = new System.Drawing.Size(165, 19);
            this.lbCipAutonomico.TabIndex = 13;
            this.lbCipAutonomico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lbCipAutonomico_KeyPress);
            // 
            // cbbOrigInfo
            // 
            this.cbbOrigInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbOrigInfo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbOrigInfo.Location = new System.Drawing.Point(582, 355);
            this.cbbOrigInfo.Name = "cbbOrigInfo";
            this.cbbOrigInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbOrigInfo.Size = new System.Drawing.Size(153, 20);
            this.cbbOrigInfo.TabIndex = 238;
            // 
            // tbDPDomicilio
            // 
            this.tbDPDomicilio.AcceptsReturn = true;
            this.tbDPDomicilio.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPDomicilio.Location = new System.Drawing.Point(3, 176);
            this.tbDPDomicilio.MaxLength = 100;
            this.tbDPDomicilio.Name = "tbDPDomicilio";
            this.tbDPDomicilio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPDomicilio.Size = new System.Drawing.Size(41, 19);
            this.tbDPDomicilio.TabIndex = 134;
            this.tbDPDomicilio.Visible = false;
            this.tbDPDomicilio.Enter += new System.EventHandler(this.tbDPDomicilio_Enter);
            this.tbDPDomicilio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPDomicilio_KeyPress);
            // 
            // Label36
            // 
            this.Label36.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label36.Location = new System.Drawing.Point(494, 239);
            this.Label36.Name = "Label36";
            this.Label36.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label36.Size = new System.Drawing.Size(23, 18);
            this.Label36.TabIndex = 223;
            this.Label36.Text = "Ext:";
            this.Label36.Visible = false;
            // 
            // _tsDatosPac_TabPage1
            // 
            this._tsDatosPac_TabPage1.Controls.Add(this.cmdConsultaActualizacion);
            this._tsDatosPac_TabPage1.Controls.Add(this.chkValidarRE);
            this._tsDatosPac_TabPage1.Controls.Add(this.frmPrivado);
            this._tsDatosPac_TabPage1.Controls.Add(this.frmTitular);
            this._tsDatosPac_TabPage1.Controls.Add(this.frmRERegimen);
            this._tsDatosPac_TabPage1.ItemSize = new System.Drawing.SizeF(119F, 28F);
            this._tsDatosPac_TabPage1.Location = new System.Drawing.Point(10, 37);
            this._tsDatosPac_TabPage1.Name = "_tsDatosPac_TabPage1";
            this._tsDatosPac_TabPage1.Size = new System.Drawing.Size(738, 519);
            this._tsDatosPac_TabPage1.Text = "R�gimen Econ�mico";
            // 
            // cmdConsultaActualizacion
            // 
            this.cmdConsultaActualizacion.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdConsultaActualizacion.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cmdConsultaActualizacion.Location = new System.Drawing.Point(592, 7);
            this.cmdConsultaActualizacion.Name = "cmdConsultaActualizacion";
            this.cmdConsultaActualizacion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdConsultaActualizacion.Size = new System.Drawing.Size(135, 29);
            this.cmdConsultaActualizacion.TabIndex = 49;
            this.cmdConsultaActualizacion.Text = "Consulta/Actualizaci�n";
            this.cmdConsultaActualizacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdConsultaActualizacion.Click += new System.EventHandler(this.cmdConsultaActualizacion_Click);
            // 
            // chkValidarRE
            // 
            this.chkValidarRE.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkValidarRE.Location = new System.Drawing.Point(372, 11);
            this.chkValidarRE.Name = "chkValidarRE";
            this.chkValidarRE.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkValidarRE.Size = new System.Drawing.Size(200, 18);
            this.chkValidarRE.TabIndex = 48;
            this.chkValidarRE.Text = "Aceptaci�n del R�gimen Econ�mico";
            // 
            // frmPrivado
            // 
            this.frmPrivado.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmPrivado.Controls.Add(this.chkPropioPaciente);
            this.frmPrivado.Controls.Add(this.tbREPasaporte);
            this.frmPrivado.Controls.Add(this.cmdFacturacionP);
            this.frmPrivado.Controls.Add(this.cmdDeudas);
            this.frmPrivado.Controls.Add(this.cbBuscar2);
            this.frmPrivado.Controls.Add(this.Frame4);
            this.frmPrivado.Controls.Add(this.tbREApe2);
            this.frmPrivado.Controls.Add(this.tbREApe1);
            this.frmPrivado.Controls.Add(this.tbRENombre);
            this.frmPrivado.Controls.Add(this.tbREDomicilio);
            this.frmPrivado.Controls.Add(this.mebRECodPostal);
            this.frmPrivado.Controls.Add(this.Frame8);
            this.frmPrivado.Controls.Add(this.Frame17);
            this.frmPrivado.Controls.Add(this.Label43);
            this.frmPrivado.Controls.Add(this.cbbREPoblacion);
            this.frmPrivado.Controls.Add(this.Label27);
            this.frmPrivado.Controls.Add(this.Label30);
            this.frmPrivado.Controls.Add(this.Label25);
            this.frmPrivado.Controls.Add(this.Label18);
            this.frmPrivado.Controls.Add(this.Label17);
            this.frmPrivado.Controls.Add(this.Label20);
            this.frmPrivado.HeaderText = "Privado";
            this.frmPrivado.Location = new System.Drawing.Point(4, 261);
            this.frmPrivado.Name = "frmPrivado";
            this.frmPrivado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmPrivado.Size = new System.Drawing.Size(744, 258);
            this.frmPrivado.TabIndex = 143;
            this.frmPrivado.Text = "Privado";
            // 
            // chkPropioPaciente
            // 
            this.chkPropioPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkPropioPaciente.Location = new System.Drawing.Point(401, 21);
            this.chkPropioPaciente.Name = "chkPropioPaciente";
            this.chkPropioPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkPropioPaciente.Size = new System.Drawing.Size(98, 18);
            this.chkPropioPaciente.TabIndex = 80;
            this.chkPropioPaciente.Text = "Propio Paciente";
            this.chkPropioPaciente.CheckStateChanged += new System.EventHandler(this.chkPropioPaciente_CheckStateChanged);
            // 
            // tbREPasaporte
            // 
            this.tbREPasaporte.AcceptsReturn = true;
            this.tbREPasaporte.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbREPasaporte.Location = new System.Drawing.Point(583, 19);
            this.tbREPasaporte.MaxLength = 15;
            this.tbREPasaporte.Name = "tbREPasaporte";
            this.tbREPasaporte.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbREPasaporte.Size = new System.Drawing.Size(121, 19);
            this.tbREPasaporte.TabIndex = 81;
            this.tbREPasaporte.Enter += new System.EventHandler(this.tbREPasaporte_Enter);
            this.tbREPasaporte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbREPasaporte_KeyPress);
            // 
            // cmdFacturacionP
            // 
            this.cmdFacturacionP.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdFacturacionP.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cmdFacturacionP.Location = new System.Drawing.Point(156, 234);
            this.cmdFacturacionP.Name = "cmdFacturacionP";
            this.cmdFacturacionP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdFacturacionP.Size = new System.Drawing.Size(129, 20);
            this.cmdFacturacionP.TabIndex = 104;
            this.cmdFacturacionP.Text = "Facturaci�n Pendiente";
            this.cmdFacturacionP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdFacturacionP.Click += new System.EventHandler(this.cmdFacturacionP_Click);
            // 
            // cmdDeudas
            // 
            this.cmdDeudas.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdDeudas.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cmdDeudas.Location = new System.Drawing.Point(18, 234);
            this.cmdDeudas.Name = "cmdDeudas";
            this.cmdDeudas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdDeudas.Size = new System.Drawing.Size(129, 20);
            this.cmdDeudas.TabIndex = 103;
            this.cmdDeudas.Text = "&Consulta de Deudas ";
            this.cmdDeudas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdDeudas.Click += new System.EventHandler(this.cmdDeudas_Click);
            // 
            // cbBuscar2
            // 
            this.cbBuscar2.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscar2.Image = ((System.Drawing.Image)(resources.GetObject("cbBuscar2.Image")));
            this.cbBuscar2.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbBuscar2.Location = new System.Drawing.Point(516, 99);
            this.cbBuscar2.Name = "cbBuscar2";
            this.cbBuscar2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbBuscar2.Size = new System.Drawing.Size(25, 21);
            this.cbBuscar2.TabIndex = 88;
            this.cbBuscar2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbBuscar2.Click += new System.EventHandler(this.cbBuscar2_Click);
            // 
            // Frame4
            // 
            this.Frame4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame4.Controls.Add(this.tbREDireccionEx);
            this.Frame4.Controls.Add(this.chbREExtrangero);
            this.Frame4.Controls.Add(this.tbREExPob);
            this.Frame4.Controls.Add(this.Label70);
            this.Frame4.Controls.Add(this.cbbREPais);
            this.Frame4.Controls.Add(this.Label45);
            this.Frame4.Controls.Add(this.Label44);
            this.Frame4.HeaderText = "Residencia en el extranjero";
            this.Frame4.Location = new System.Drawing.Point(7, 123);
            this.Frame4.Name = "Frame4";
            this.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame4.Size = new System.Drawing.Size(712, 58);
            this.Frame4.TabIndex = 150;
            this.Frame4.Text = "Residencia en el extranjero";
            // 
            // tbREDireccionEx
            // 
            this.tbREDireccionEx.AcceptsReturn = true;
            this.tbREDireccionEx.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbREDireccionEx.Enabled = false;
            this.tbREDireccionEx.Location = new System.Drawing.Point(202, 35);
            this.tbREDireccionEx.MaxLength = 100;
            this.tbREDireccionEx.Name = "tbREDireccionEx";
            this.tbREDireccionEx.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbREDireccionEx.Size = new System.Drawing.Size(505, 19);
            this.tbREDireccionEx.TabIndex = 92;
            this.tbREDireccionEx.Enter += new System.EventHandler(this.tbREDireccionEx_Enter);
            // 
            // chbREExtrangero
            // 
            this.chbREExtrangero.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbREExtrangero.Location = new System.Drawing.Point(8, 18);
            this.chbREExtrangero.Name = "chbREExtrangero";
            this.chbREExtrangero.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbREExtrangero.Size = new System.Drawing.Size(127, 18);
            this.chbREExtrangero.TabIndex = 89;
            this.chbREExtrangero.Text = "Residencia extranjero";
            this.chbREExtrangero.CheckStateChanged += new System.EventHandler(this.chbREExtrangero_CheckStateChanged);
            // 
            // tbREExPob
            // 
            this.tbREExPob.AcceptsReturn = true;
            this.tbREExPob.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbREExPob.Enabled = false;
            this.tbREExPob.Location = new System.Drawing.Point(490, 13);
            this.tbREExPob.MaxLength = 40;
            this.tbREExPob.Name = "tbREExPob";
            this.tbREExPob.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbREExPob.Size = new System.Drawing.Size(217, 19);
            this.tbREExPob.TabIndex = 91;
            this.tbREExPob.TextChanged += new System.EventHandler(this.tbREExPob_TextChanged);
            this.tbREExPob.Enter += new System.EventHandler(this.tbREExPob_Enter);
            this.tbREExPob.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbREExPob_KeyPress);
            // 
            // Label70
            // 
            this.Label70.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label70.Location = new System.Drawing.Point(142, 35);
            this.Label70.Name = "Label70";
            this.Label70.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label70.Size = new System.Drawing.Size(55, 18);
            this.Label70.TabIndex = 225;
            this.Label70.Text = "Direcci�n:";
            // 
            // cbbREPais
            // 
            this.cbbREPais.ColumnWidths = "";
            this.cbbREPais.Enabled = false;
            this.cbbREPais.Location = new System.Drawing.Point(202, 13);
            this.cbbREPais.Name = "cbbREPais";
            this.cbbREPais.Size = new System.Drawing.Size(219, 20);
            this.cbbREPais.TabIndex = 90;
            this.cbbREPais.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbREPais_SelectedIndexChanged);
            this.cbbREPais.Click += new System.EventHandler(this.cbbREPais_Click);
            // 
            // Label45
            // 
            this.Label45.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label45.Location = new System.Drawing.Point(143, 15);
            this.Label45.Name = "Label45";
            this.Label45.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label45.Size = new System.Drawing.Size(28, 18);
            this.Label45.TabIndex = 152;
            this.Label45.Text = "Pa�s:";
            // 
            // Label44
            // 
            this.Label44.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label44.Location = new System.Drawing.Point(426, 16);
            this.Label44.Name = "Label44";
            this.Label44.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label44.Size = new System.Drawing.Size(58, 18);
            this.Label44.TabIndex = 151;
            this.Label44.Text = "Poblaci�n:";
            // 
            // tbREApe2
            // 
            this.tbREApe2.AcceptsReturn = true;
            this.tbREApe2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbREApe2.Location = new System.Drawing.Point(412, 54);
            this.tbREApe2.MaxLength = 35;
            this.tbREApe2.Name = "tbREApe2";
            this.tbREApe2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbREApe2.Size = new System.Drawing.Size(253, 19);
            this.tbREApe2.TabIndex = 83;
            this.tbREApe2.Enter += new System.EventHandler(this.tbREApe2_Enter);
            this.tbREApe2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbREApe2_KeyPress);
            // 
            // tbREApe1
            // 
            this.tbREApe1.AcceptsReturn = true;
            this.tbREApe1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbREApe1.Location = new System.Drawing.Point(76, 54);
            this.tbREApe1.MaxLength = 35;
            this.tbREApe1.Name = "tbREApe1";
            this.tbREApe1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbREApe1.Size = new System.Drawing.Size(248, 19);
            this.tbREApe1.TabIndex = 82;
            this.tbREApe1.TextChanged += new System.EventHandler(this.tbREApe1_TextChanged);
            this.tbREApe1.Enter += new System.EventHandler(this.tbREApe1_Enter);
            this.tbREApe1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbREApe1_KeyPress);
            // 
            // tbRENombre
            // 
            this.tbRENombre.AcceptsReturn = true;
            this.tbRENombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbRENombre.Location = new System.Drawing.Point(76, 75);
            this.tbRENombre.MaxLength = 35;
            this.tbRENombre.Name = "tbRENombre";
            this.tbRENombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRENombre.Size = new System.Drawing.Size(248, 19);
            this.tbRENombre.TabIndex = 84;
            this.tbRENombre.TextChanged += new System.EventHandler(this.tbRENombre_TextChanged);
            this.tbRENombre.Enter += new System.EventHandler(this.tbRENombre_Enter);
            this.tbRENombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRENombre_KeyPress);
            // 
            // tbREDomicilio
            // 
            this.tbREDomicilio.AcceptsReturn = true;
            this.tbREDomicilio.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbREDomicilio.Location = new System.Drawing.Point(412, 75);
            this.tbREDomicilio.MaxLength = 100;
            this.tbREDomicilio.Name = "tbREDomicilio";
            this.tbREDomicilio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbREDomicilio.Size = new System.Drawing.Size(278, 19);
            this.tbREDomicilio.TabIndex = 85;
            this.tbREDomicilio.Enter += new System.EventHandler(this.tbREDomicilio_Enter);
            this.tbREDomicilio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbREDomicilio_KeyPress);
            // 
            // mebRECodPostal
            // 
            this.mebRECodPostal.Location = new System.Drawing.Point(76, 99);
            this.mebRECodPostal.Name = "mebRECodPostal";
            this.mebRECodPostal.PromptChar = ' ';
            this.mebRECodPostal.Size = new System.Drawing.Size(40, 20);
            this.mebRECodPostal.TabIndex = 86;
            this.mebRECodPostal.TabStop = false;
            this.mebRECodPostal.TextChanged += new System.EventHandler(this.mebRECodPostal_TextChanged);
            this.mebRECodPostal.Enter += new System.EventHandler(this.mebRECodPostal_Enter);
            // 
            // Frame8
            // 
            this.Frame8.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame8.Controls.Add(this.FrmCB21);
            this.Frame8.Controls.Add(this.Frame9);
            this.Frame8.Controls.Add(this.cbbREPago);
            this.Frame8.HeaderText = "Forma de Pago:";
            this.Frame8.Location = new System.Drawing.Point(6, 183);
            this.Frame8.Name = "Frame8";
            this.Frame8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame8.Size = new System.Drawing.Size(574, 49);
            this.Frame8.TabIndex = 173;
            this.Frame8.Text = "Forma de Pago:";
            // 
            // FrmCB21
            // 
            this.FrmCB21.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmCB21.Controls.Add(this.tbControl12);
            this.FrmCB21.Controls.Add(this.tbBanco1);
            this.FrmCB21.Controls.Add(this.tbSucursal1);
            this.FrmCB21.Controls.Add(this.tbControl1);
            this.FrmCB21.Controls.Add(this.tbCuenta1);
            this.FrmCB21.Controls.Add(this.lbNcuenta21);
            this.FrmCB21.HeaderText = "";
            this.FrmCB21.Location = new System.Drawing.Point(251, 15);
            this.FrmCB21.Name = "FrmCB21";
            this.FrmCB21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmCB21.Size = new System.Drawing.Size(318, 29);
            this.FrmCB21.TabIndex = 243;
            // 
            // tbControl12
            // 
            this.tbControl12.AcceptsReturn = true;
            this.tbControl12.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbControl12.Enabled = false;
            this.tbControl12.Location = new System.Drawing.Point(288, 5);
            this.tbControl12.MaxLength = 1;
            this.tbControl12.Name = "tbControl12";
            this.tbControl12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbControl12.Size = new System.Drawing.Size(17, 19);
            this.tbControl12.TabIndex = 102;
            this.tbControl12.TextChanged += new System.EventHandler(this.tbControl12_TextChanged);
            this.tbControl12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbControl12_KeyPress);
            // 
            // tbBanco1
            // 
            this.tbBanco1.AcceptsReturn = true;
            this.tbBanco1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbBanco1.Enabled = false;
            this.tbBanco1.Location = new System.Drawing.Point(87, 5);
            this.tbBanco1.MaxLength = 3;
            this.tbBanco1.Name = "tbBanco1";
            this.tbBanco1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbBanco1.Size = new System.Drawing.Size(31, 19);
            this.tbBanco1.TabIndex = 98;
            this.tbBanco1.Enter += new System.EventHandler(this.tbBanco1_Enter);
            this.tbBanco1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBanco1_KeyPress);
            // 
            // tbSucursal1
            // 
            this.tbSucursal1.AcceptsReturn = true;
            this.tbSucursal1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbSucursal1.Enabled = false;
            this.tbSucursal1.Location = new System.Drawing.Point(125, 5);
            this.tbSucursal1.MaxLength = 4;
            this.tbSucursal1.Name = "tbSucursal1";
            this.tbSucursal1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbSucursal1.Size = new System.Drawing.Size(34, 19);
            this.tbSucursal1.TabIndex = 99;
            this.tbSucursal1.Enter += new System.EventHandler(this.tbSucursal1_Enter);
            this.tbSucursal1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSucursal1_KeyPress);
            // 
            // tbControl1
            // 
            this.tbControl1.AcceptsReturn = true;
            this.tbControl1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbControl1.Enabled = false;
            this.tbControl1.Location = new System.Drawing.Point(166, 5);
            this.tbControl1.MaxLength = 1;
            this.tbControl1.Name = "tbControl1";
            this.tbControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbControl1.Size = new System.Drawing.Size(17, 19);
            this.tbControl1.TabIndex = 100;
            this.tbControl1.Enter += new System.EventHandler(this.tbControl1_Enter);
            this.tbControl1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbControl1_KeyPress);
            // 
            // tbCuenta1
            // 
            this.tbCuenta1.AcceptsReturn = true;
            this.tbCuenta1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCuenta1.Enabled = false;
            this.tbCuenta1.Location = new System.Drawing.Point(191, 5);
            this.tbCuenta1.MaxLength = 13;
            this.tbCuenta1.Name = "tbCuenta1";
            this.tbCuenta1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCuenta1.Size = new System.Drawing.Size(90, 19);
            this.tbCuenta1.TabIndex = 101;
            this.tbCuenta1.Enter += new System.EventHandler(this.tbCuenta1_Enter);
            this.tbCuenta1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCuenta1_KeyPress);
            // 
            // lbNcuenta21
            // 
            this.lbNcuenta21.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNcuenta21.Location = new System.Drawing.Point(8, 6);
            this.lbNcuenta21.Name = "lbNcuenta21";
            this.lbNcuenta21.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNcuenta21.Size = new System.Drawing.Size(60, 18);
            this.lbNcuenta21.TabIndex = 244;
            this.lbNcuenta21.Text = "N� Cuenta:";
            // 
            // Frame9
            // 
            this.Frame9.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame9.Controls.Add(this.tbBanco);
            this.Frame9.Controls.Add(this.tbSucursal);
            this.Frame9.Controls.Add(this.tbControl);
            this.Frame9.Controls.Add(this.tbCuenta);
            this.Frame9.Controls.Add(this.Label46);
            this.Frame9.HeaderText = "";
            this.Frame9.Location = new System.Drawing.Point(251, 11);
            this.Frame9.Name = "Frame9";
            this.Frame9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame9.Size = new System.Drawing.Size(318, 35);
            this.Frame9.TabIndex = 174;
            // 
            // tbBanco
            // 
            this.tbBanco.AcceptsReturn = true;
            this.tbBanco.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbBanco.Enabled = false;
            this.tbBanco.Location = new System.Drawing.Point(57, 11);
            this.tbBanco.MaxLength = 4;
            this.tbBanco.Name = "tbBanco";
            this.tbBanco.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbBanco.Size = new System.Drawing.Size(40, 19);
            this.tbBanco.TabIndex = 94;
            this.tbBanco.Enter += new System.EventHandler(this.tbBanco_Enter);
            this.tbBanco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBanco_KeyPress);
            // 
            // tbSucursal
            // 
            this.tbSucursal.AcceptsReturn = true;
            this.tbSucursal.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbSucursal.Enabled = false;
            this.tbSucursal.Location = new System.Drawing.Point(105, 11);
            this.tbSucursal.MaxLength = 4;
            this.tbSucursal.Name = "tbSucursal";
            this.tbSucursal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbSucursal.Size = new System.Drawing.Size(41, 19);
            this.tbSucursal.TabIndex = 95;
            this.tbSucursal.Enter += new System.EventHandler(this.tbSucursal_Enter);
            this.tbSucursal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSucursal_KeyPress);
            // 
            // tbControl
            // 
            this.tbControl.AcceptsReturn = true;
            this.tbControl.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbControl.Enabled = false;
            this.tbControl.Location = new System.Drawing.Point(153, 11);
            this.tbControl.MaxLength = 2;
            this.tbControl.Name = "tbControl";
            this.tbControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbControl.Size = new System.Drawing.Size(27, 19);
            this.tbControl.TabIndex = 96;
            this.tbControl.Enter += new System.EventHandler(this.tbControl_Enter);
            this.tbControl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbControl_KeyPress);
            // 
            // tbCuenta
            // 
            this.tbCuenta.AcceptsReturn = true;
            this.tbCuenta.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCuenta.Enabled = false;
            this.tbCuenta.Location = new System.Drawing.Point(187, 11);
            this.tbCuenta.MaxLength = 10;
            this.tbCuenta.Name = "tbCuenta";
            this.tbCuenta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCuenta.Size = new System.Drawing.Size(125, 19);
            this.tbCuenta.TabIndex = 97;
            this.tbCuenta.Enter += new System.EventHandler(this.tbCuenta_Enter);
            this.tbCuenta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCuenta_KeyPress);
            // 
            // Label46
            // 
            this.Label46.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label46.Location = new System.Drawing.Point(4, 14);
            this.Label46.Name = "Label46";
            this.Label46.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label46.Size = new System.Drawing.Size(60, 18);
            this.Label46.TabIndex = 175;
            this.Label46.Text = "N� Cuenta:";
            // 
            // cbbREPago
            // 
            // 
            // cbbREPago.NestedRadGridView
            // 
            this.cbbREPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cbbREPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbREPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbbREPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cbbREPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cbbREPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cbbREPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.cbbREPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cbbREPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cbbREPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.cbbREPago.EditorControl.Name = "NestedRadGridView";
            this.cbbREPago.EditorControl.ReadOnly = true;
            this.cbbREPago.EditorControl.ShowGroupPanel = false;
            this.cbbREPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cbbREPago.EditorControl.TabIndex = 0;
            this.cbbREPago.Location = new System.Drawing.Point(6, 20);
            this.cbbREPago.Name = "cbbREPago";
            this.cbbREPago.Size = new System.Drawing.Size(240, 20);
            this.cbbREPago.TabIndex = 93;
            this.cbbREPago.TabStop = false;
            this.cbbREPago.Click += new System.EventHandler(this.cbbREPago_Click);
            this.cbbREPago.SelectedIndexChanged += new System.EventHandler(this.CbbREPago_SelectedIndexChanged);
            this.cbbREPago.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbbREPago_KeyDown);
            // 
            // Frame17
            // 
            this.Frame17.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame17.Controls.Add(this._rbDNIOtroDocRE_1);
            this.Frame17.Controls.Add(this._rbDNIOtroDocRE_0);
            this.Frame17.Controls.Add(this.tbNIFOtrosDocRE);
            this.Frame17.HeaderText = "";
            this.Frame17.Location = new System.Drawing.Point(8, 18);
            this.Frame17.Name = "Frame17";
            this.Frame17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame17.Size = new System.Drawing.Size(365, 33);
            this.Frame17.TabIndex = 183;
            // 
            // _rbDNIOtroDocRE_1
            // 
            this._rbDNIOtroDocRE_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbDNIOtroDocRE_1.Location = new System.Drawing.Point(92, 8);
            this._rbDNIOtroDocRE_1.Name = "_rbDNIOtroDocRE_1";
            this._rbDNIOtroDocRE_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbDNIOtroDocRE_1.Size = new System.Drawing.Size(113, 18);
            this._rbDNIOtroDocRE_1.TabIndex = 78;
            this._rbDNIOtroDocRE_1.Text = "Otros documentos";
            this._rbDNIOtroDocRE_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbDNIOtroDocRE_CheckedChanged);
            // 
            // _rbDNIOtroDocRE_0
            // 
            this._rbDNIOtroDocRE_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbDNIOtroDocRE_0.Location = new System.Drawing.Point(8, 8);
            this._rbDNIOtroDocRE_0.Name = "_rbDNIOtroDocRE_0";
            this._rbDNIOtroDocRE_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbDNIOtroDocRE_0.Size = new System.Drawing.Size(61, 18);
            this._rbDNIOtroDocRE_0.TabIndex = 77;
            this._rbDNIOtroDocRE_0.Text = "DNI/NIF";
            this._rbDNIOtroDocRE_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbDNIOtroDocRE_CheckedChanged);
            // 
            // tbNIFOtrosDocRE
            // 
            this.tbNIFOtrosDocRE.Location = new System.Drawing.Point(220, 5);
            this.tbNIFOtrosDocRE.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbNIFOtrosDocRE.Name = "tbNIFOtrosDocRE";
            this.tbNIFOtrosDocRE.PromptChar = ' ';
            this.tbNIFOtrosDocRE.Size = new System.Drawing.Size(125, 20);
            this.tbNIFOtrosDocRE.TabIndex = 79;
            this.tbNIFOtrosDocRE.TabStop = false;
            this.tbNIFOtrosDocRE.Enabled = false;
            this.tbNIFOtrosDocRE.Text = "            ";
            this.tbNIFOtrosDocRE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNIFOtrosDocRE_KeyPress);
            this.tbNIFOtrosDocRE.TextChanged += new System.EventHandler(this.tbNIFOtrosDocRE_TextChanged);
            this.tbNIFOtrosDocRE.Enter += new System.EventHandler(this.tbNIFOtrosDocRE_Enter);
            this.tbNIFOtrosDocRE.Leave += new System.EventHandler(this.tbNIFOtrosDocRE_Leave);
            // 
            // Label43
            // 
            this.Label43.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label43.Location = new System.Drawing.Point(521, 21);
            this.Label43.Name = "Label43";
            this.Label43.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label43.Size = new System.Drawing.Size(58, 18);
            this.Label43.TabIndex = 172;
            this.Label43.Text = "Pasaporte:";
            // 
            // cbbREPoblacion
            // 
            this.cbbREPoblacion.ColumnWidths = "";
            this.cbbREPoblacion.Location = new System.Drawing.Point(184, 97);
            this.cbbREPoblacion.Name = "cbbREPoblacion";
            this.cbbREPoblacion.Size = new System.Drawing.Size(329, 20);
            this.cbbREPoblacion.TabIndex = 87;
            this.cbbREPoblacion.Click += new System.EventHandler(this.cbbREPoblacion_Click);
            // 
            // Label27
            // 
            this.Label27.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label27.Location = new System.Drawing.Point(9, 100);
            this.Label27.Name = "Label27";
            this.Label27.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label27.Size = new System.Drawing.Size(65, 18);
            this.Label27.TabIndex = 149;
            this.Label27.Text = "C�d. postal:";
            // 
            // Label30
            // 
            this.Label30.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label30.Location = new System.Drawing.Point(120, 100);
            this.Label30.Name = "Label30";
            this.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label30.Size = new System.Drawing.Size(58, 18);
            this.Label30.TabIndex = 148;
            this.Label30.Text = "Poblaci�n:";
            // 
            // Label25
            // 
            this.Label25.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label25.Location = new System.Drawing.Point(345, 56);
            this.Label25.Name = "Label25";
            this.Label25.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label25.Size = new System.Drawing.Size(59, 18);
            this.Label25.TabIndex = 147;
            this.Label25.Text = "Apellido 2:";
            // 
            // Label18
            // 
            this.Label18.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label18.Location = new System.Drawing.Point(9, 56);
            this.Label18.Name = "Label18";
            this.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label18.Size = new System.Drawing.Size(59, 18);
            this.Label18.TabIndex = 146;
            this.Label18.Text = "Apellido 1:";
            // 
            // Label17
            // 
            this.Label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label17.Location = new System.Drawing.Point(10, 78);
            this.Label17.Name = "Label17";
            this.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label17.Size = new System.Drawing.Size(50, 18);
            this.Label17.TabIndex = 145;
            this.Label17.Text = "Nombre:";
            // 
            // Label20
            // 
            this.Label20.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label20.Location = new System.Drawing.Point(348, 75);
            this.Label20.Name = "Label20";
            this.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label20.Size = new System.Drawing.Size(56, 18);
            this.Label20.TabIndex = 144;
            this.Label20.Text = "Domicilio:";
            // 
            // frmTitular
            // 
            this.frmTitular.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmTitular.Controls.Add(this.frmSociedad);
            this.frmTitular.Controls.Add(this.frmSegSocial);
            this.frmTitular.HeaderText = "";
            this.frmTitular.Location = new System.Drawing.Point(-4, 51);
            this.frmTitular.Name = "frmTitular";
            this.frmTitular.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmTitular.Size = new System.Drawing.Size(752, 205);
            this.frmTitular.TabIndex = 136;
            // 
            // frmSociedad
            // 
            this.frmSociedad.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmSociedad.Controls.Add(this.TBBandaMagnetica);
            this.frmSociedad.Controls.Add(this.CbbObraSocial);
            this.frmSociedad.Controls.Add(this.chkExentoIVA);
            this.frmSociedad.Controls.Add(this.frmIndicador);
            this.frmSociedad.Controls.Add(this.tbRECobertura);
            this.frmSociedad.Controls.Add(this.tbRENVersion);
            this.frmSociedad.Controls.Add(this.cbBuscarSoc);
            this.frmSociedad.Controls.Add(this.tbREEmpresa);
            this.frmSociedad.Controls.Add(this.cbbRESociedad);
            this.frmSociedad.Controls.Add(this.mebREFechaCaducidad);
            this.frmSociedad.Controls.Add(this.mebREFechaAlta);
            this.frmSociedad.Controls.Add(this.tbRENPoliza);
            this.frmSociedad.Controls.Add(this.Label1);
            this.frmSociedad.Controls.Add(this.lblObraSocial);
            this.frmSociedad.Controls.Add(this.Label73);
            this.frmSociedad.Controls.Add(this.Label72);
            this.frmSociedad.Controls.Add(this.Label71);
            this.frmSociedad.Controls.Add(this.LbVersion);
            this.frmSociedad.Controls.Add(this.Label38);
            this.frmSociedad.Controls.Add(this.Label12);
            this.frmSociedad.Controls.Add(this.Label14);
            this.frmSociedad.HeaderText = "Sociedad";
            this.frmSociedad.Location = new System.Drawing.Point(374, 8);
            this.frmSociedad.Name = "frmSociedad";
            this.frmSociedad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmSociedad.Size = new System.Drawing.Size(367, 195);
            this.frmSociedad.TabIndex = 140;
            this.frmSociedad.Text = "Sociedad";
            // 
            // TBBandaMagnetica
            // 
            this.TBBandaMagnetica.AcceptsReturn = true;
            this.TBBandaMagnetica.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TBBandaMagnetica.Location = new System.Drawing.Point(293, 165);
            this.TBBandaMagnetica.MaxLength = 0;
            this.TBBandaMagnetica.Name = "TBBandaMagnetica";
            this.TBBandaMagnetica.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TBBandaMagnetica.Size = new System.Drawing.Size(67, 19);
            this.TBBandaMagnetica.TabIndex = 76;
            this.TBBandaMagnetica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TBBandaMagnetica_KeyPress);
            // 
            // CbbObraSocial
            // 
            this.CbbObraSocial.Cursor = System.Windows.Forms.Cursors.Default;
            this.CbbObraSocial.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.CbbObraSocial.Location = new System.Drawing.Point(66, 15);
            this.CbbObraSocial.Name = "CbbObraSocial";
            this.CbbObraSocial.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CbbObraSocial.Size = new System.Drawing.Size(291, 20);
            this.CbbObraSocial.TabIndex = 63;
            this.CbbObraSocial.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.CbbObraSocial_SelectedIndexChanged);
            // 
            // chkExentoIVA
            // 
            this.chkExentoIVA.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.chkExentoIVA.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkExentoIVA.Location = new System.Drawing.Point(305, 61);
            this.chkExentoIVA.Name = "chkExentoIVA";
            this.chkExentoIVA.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkExentoIVA.Size = new System.Drawing.Size(90, 18);
            this.chkExentoIVA.TabIndex = 67;
            this.chkExentoIVA.Text = "Exento de IVA";
            this.chkExentoIVA.CheckStateChanged += new System.EventHandler(this.chkExentoIVA_CheckStateChanged);
            // 
            // frmIndicador
            // 
            this.frmIndicador.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmIndicador.Controls.Add(this._rbIndicador_1);
            this.frmIndicador.Controls.Add(this._rbIndicador_0);
            this.frmIndicador.Controls.Add(this.tbRENomTitular);
            this.frmIndicador.Controls.Add(this.Label5);
            this.frmIndicador.HeaderText = "Indicador de Titular/Beneficiario Sociedad";
            this.frmIndicador.Location = new System.Drawing.Point(3, 127);
            this.frmIndicador.Name = "frmIndicador";
            this.frmIndicador.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmIndicador.Size = new System.Drawing.Size(286, 65);
            this.frmIndicador.TabIndex = 235;
            this.frmIndicador.Text = "Indicador de Titular/Beneficiario Sociedad";
            // 
            // _rbIndicador_1
            // 
            this._rbIndicador_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbIndicador_1.Location = new System.Drawing.Point(71, 18);
            this._rbIndicador_1.Name = "_rbIndicador_1";
            this._rbIndicador_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbIndicador_1.Size = new System.Drawing.Size(78, 18);
            this._rbIndicador_1.TabIndex = 74;
            this._rbIndicador_1.Text = "Beneficiario";
            this._rbIndicador_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbIndicador_CheckedChanged);
            // 
            // _rbIndicador_0
            // 
            this._rbIndicador_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbIndicador_0.Location = new System.Drawing.Point(8, 18);
            this._rbIndicador_0.Name = "_rbIndicador_0";
            this._rbIndicador_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbIndicador_0.Size = new System.Drawing.Size(52, 18);
            this._rbIndicador_0.TabIndex = 73;
            this._rbIndicador_0.Text = "Titular";
            this._rbIndicador_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbIndicador_CheckedChanged);
            // 
            // tbRENomTitular
            // 
            this.tbRENomTitular.AcceptsReturn = true;
            this.tbRENomTitular.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbRENomTitular.Enabled = false;
            this.tbRENomTitular.Location = new System.Drawing.Point(69, 38);
            this.tbRENomTitular.MaxLength = 60;
            this.tbRENomTitular.Name = "tbRENomTitular";
            this.tbRENomTitular.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRENomTitular.Size = new System.Drawing.Size(212, 19);
            this.tbRENomTitular.TabIndex = 75;
            this.tbRENomTitular.WordWrap = false;
            this.tbRENomTitular.TextChanged += new System.EventHandler(this.tbRENomTitular_TextChanged);
            this.tbRENomTitular.Enter += new System.EventHandler(this.tbRENomTitular_Enter);
            this.tbRENomTitular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRENomTitular_KeyPress);
            // 
            // Label5
            // 
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Location = new System.Drawing.Point(10, 42);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(51, 18);
            this.Label5.TabIndex = 236;
            this.Label5.Text = "N.Titular:";
            // 
            // tbRECobertura
            // 
            this.tbRECobertura.AcceptsReturn = true;
            this.tbRECobertura.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbRECobertura.Location = new System.Drawing.Point(66, 105);
            this.tbRECobertura.MaxLength = 20;
            this.tbRECobertura.Name = "tbRECobertura";
            this.tbRECobertura.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRECobertura.Size = new System.Drawing.Size(76, 20);
            this.tbRECobertura.TabIndex = 70;
            // 
            // tbRENVersion
            // 
            this.tbRENVersion.AcceptsReturn = true;
            this.tbRENVersion.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbRENVersion.Location = new System.Drawing.Point(268, 61);
            this.tbRENVersion.MaxLength = 2;
            this.tbRENVersion.Name = "tbRENVersion";
            this.tbRENVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRENVersion.Size = new System.Drawing.Size(32, 20);
            this.tbRENVersion.TabIndex = 66;
            // 
            // cbBuscarSoc
            // 
            this.cbBuscarSoc.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscarSoc.Enabled = false;
            this.cbBuscarSoc.Image = ((System.Drawing.Image)(resources.GetObject("cbBuscarSoc.Image")));
            this.cbBuscarSoc.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbBuscarSoc.Location = new System.Drawing.Point(270, 83);
            this.cbBuscarSoc.Name = "cbBuscarSoc";
            this.cbBuscarSoc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbBuscarSoc.Size = new System.Drawing.Size(22, 20);
            this.cbBuscarSoc.TabIndex = 69;
            this.cbBuscarSoc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbBuscarSoc.Visible = false;
            this.cbBuscarSoc.Click += new System.EventHandler(this.cbBuscarSoc_Click);
            // 
            // tbREEmpresa
            // 
            this.tbREEmpresa.AcceptsReturn = true;
            this.tbREEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbREEmpresa.Location = new System.Drawing.Point(66, 83);
            this.tbREEmpresa.MaxLength = 30;
            this.tbREEmpresa.Name = "tbREEmpresa";
            this.tbREEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbREEmpresa.Size = new System.Drawing.Size(200, 20);
            this.tbREEmpresa.TabIndex = 68;
            this.tbREEmpresa.TextChanged += new System.EventHandler(this.tbREEmpresa_TextChanged);
            this.tbREEmpresa.Enter += new System.EventHandler(this.tbREEmpresa_Enter);
            this.tbREEmpresa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbREEmpresa_KeyPress);
            // 
            // cbbRESociedad
            // 
            this.cbbRESociedad.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbRESociedad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbRESociedad.Location = new System.Drawing.Point(66, 38);
            this.cbbRESociedad.Name = "cbbRESociedad";
            this.cbbRESociedad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbRESociedad.Size = new System.Drawing.Size(291, 20);
            this.cbbRESociedad.TabIndex = 64;
            this.cbbRESociedad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbRESociedad_KeyPress);
            this.cbbRESociedad.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbRESociedad_SelectedIndexChanged);
            this.cbbRESociedad.TextChanged += new System.EventHandler(this.cbbRESociedad_TextChanged);
            // 
            // mebREFechaCaducidad
            // 
            this.mebREFechaCaducidad.Location = new System.Drawing.Point(268, 105);
            this.mebREFechaCaducidad.Mask = "99/9999";
            this.mebREFechaCaducidad.Name = "mebREFechaCaducidad";
            this.mebREFechaCaducidad.PromptChar = ' ';
            this.mebREFechaCaducidad.Size = new System.Drawing.Size(51, 20);
            this.mebREFechaCaducidad.TabIndex = 72;
            this.mebREFechaCaducidad.TabStop = false;
            this.mebREFechaCaducidad.Enter += new System.EventHandler(this.mebREFechaCaducidad_Enter);
            this.mebREFechaCaducidad.Leave += new System.EventHandler(this.mebREFechaCaducidad_Leave);
            // 
            // mebREFechaAlta
            // 
            this.mebREFechaAlta.Location = new System.Drawing.Point(188, 105);
            this.mebREFechaAlta.Mask = "99/9999";
            this.mebREFechaAlta.Name = "mebREFechaAlta";
            this.mebREFechaAlta.PromptChar = ' ';
            this.mebREFechaAlta.Size = new System.Drawing.Size(51, 20);
            this.mebREFechaAlta.TabIndex = 71;
            this.mebREFechaAlta.TabStop = false;
            this.mebREFechaAlta.Enter += new System.EventHandler(this.mebREFechaAlta_Enter);
            this.mebREFechaAlta.Leave += new System.EventHandler(this.mebREFechaAlta_Leave);
            // 
            // tbRENPoliza
            // 
            this.tbRENPoliza.Location = new System.Drawing.Point(66, 61);
            this.tbRENPoliza.Name = "tbRENPoliza";
            this.tbRENPoliza.PromptChar = ' ';
            this.tbRENPoliza.Size = new System.Drawing.Size(144, 20);
            this.tbRENPoliza.TabIndex = 65;
            this.tbRENPoliza.TabStop = false;
            this.tbRENPoliza.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRENPoliza_KeyPress);
            this.tbRENPoliza.TextChanged += new System.EventHandler(this.tbRENPoliza_TextChanged);
            this.tbRENPoliza.Enter += new System.EventHandler(this.tbRENPoliza_Enter);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(293, 141);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(65, 18);
            this.Label1.TabIndex = 279;
            this.Label1.Text = "Banda Mag.";
            // 
            // lblObraSocial
            // 
            this.lblObraSocial.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblObraSocial.Location = new System.Drawing.Point(4, 20);
            this.lblObraSocial.Name = "lblObraSocial";
            this.lblObraSocial.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblObraSocial.Size = new System.Drawing.Size(30, 18);
            this.lblObraSocial.TabIndex = 245;
            this.lblObraSocial.Text = "Plan:";
            // 
            // Label73
            // 
            this.Label73.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label73.Location = new System.Drawing.Point(240, 108);
            this.Label73.Name = "Label73";
            this.Label73.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label73.Size = new System.Drawing.Size(23, 18);
            this.Label73.TabIndex = 228;
            this.Label73.Text = "Fin:";
            // 
            // Label72
            // 
            this.Label72.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label72.Location = new System.Drawing.Point(152, 108);
            this.Label72.Name = "Label72";
            this.Label72.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label72.Size = new System.Drawing.Size(35, 18);
            this.Label72.TabIndex = 227;
            this.Label72.Text = "Inicio:";
            // 
            // Label71
            // 
            this.Label71.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label71.Location = new System.Drawing.Point(4, 108);
            this.Label71.Name = "Label71";
            this.Label71.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label71.Size = new System.Drawing.Size(59, 18);
            this.Label71.TabIndex = 226;
            this.Label71.Text = "Cobertura:";
            // 
            // LbVersion
            // 
            this.LbVersion.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbVersion.Location = new System.Drawing.Point(222, 65);
            this.LbVersion.Name = "LbVersion";
            this.LbVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbVersion.Size = new System.Drawing.Size(46, 18);
            this.LbVersion.TabIndex = 211;
            this.LbVersion.Text = "Version:";
            // 
            // Label38
            // 
            this.Label38.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label38.Location = new System.Drawing.Point(4, 86);
            this.Label38.Name = "Label38";
            this.Label38.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label38.Size = new System.Drawing.Size(51, 18);
            this.Label38.TabIndex = 171;
            this.Label38.Text = "Empresa:";
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Location = new System.Drawing.Point(4, 44);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(54, 18);
            this.Label12.TabIndex = 142;
            this.Label12.Text = "Sociedad:";
            // 
            // Label14
            // 
            this.Label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label14.Location = new System.Drawing.Point(4, 65);
            this.Label14.Name = "Label14";
            this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label14.Size = new System.Drawing.Size(55, 18);
            this.Label14.TabIndex = 141;
            this.Label14.Text = "N� p�liza:";
            // 
            // frmSegSocial
            // 
            this.frmSegSocial.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmSegSocial.Controls.Add(this.cbbHospitalReferencia);
            this.frmSegSocial.Controls.Add(this.cbbEmplazamiento);
            this.frmSegSocial.Controls.Add(this.Frame10);
            this.frmSegSocial.Controls.Add(this.tbRENSS);
            this.frmSegSocial.Controls.Add(this.cbBuscarCIAS);
            this.frmSegSocial.Controls.Add(this.MEBCIAS);
            this.frmSegSocial.Controls.Add(this.cbBuscarCentro);
            this.frmSegSocial.Controls.Add(this.Label76);
            this.frmSegSocial.Controls.Add(this.Label75);
            this.frmSegSocial.Controls.Add(this.cbbREInspeccion);
            this.frmSegSocial.Controls.Add(this.Label2);
            this.frmSegSocial.Controls.Add(this.chkZona);
            this.frmSegSocial.Controls.Add(this.Label29);
            this.frmSegSocial.Controls.Add(this.Label11);
            this.frmSegSocial.Controls.Add(this.Label13);
            this.frmSegSocial.HeaderText = "Seguridad social";
            this.frmSegSocial.Location = new System.Drawing.Point(7, 8);
            this.frmSegSocial.Name = "frmSegSocial";
            this.frmSegSocial.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmSegSocial.Size = new System.Drawing.Size(362, 193);
            this.frmSegSocial.TabIndex = 137;
            this.frmSegSocial.Text = "Seguridad social";
            // 
            // cbbHospitalReferencia
            // 
            this.cbbHospitalReferencia.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbHospitalReferencia.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbHospitalReferencia.Location = new System.Drawing.Point(205, 96);
            this.cbbHospitalReferencia.Name = "cbbHospitalReferencia";
            this.cbbHospitalReferencia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbHospitalReferencia.Size = new System.Drawing.Size(149, 20);
            this.cbbHospitalReferencia.TabIndex = 56;
            // 
            // cbbEmplazamiento
            // 
            this.cbbEmplazamiento.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbEmplazamiento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbEmplazamiento.Location = new System.Drawing.Point(246, 68);
            this.cbbEmplazamiento.Name = "cbbEmplazamiento";
            this.cbbEmplazamiento.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbEmplazamiento.Size = new System.Drawing.Size(108, 20);
            this.cbbEmplazamiento.TabIndex = 54;
            // 
            // Frame10
            // 
            this.Frame10.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame10.Controls.Add(this.tbRENomTitularSS);
            this.Frame10.Controls.Add(this._rbIndicadorSS_0);
            this.Frame10.Controls.Add(this._rbIndicadorSS_1);
            this.Frame10.Controls.Add(this.Frame1);
            this.Frame10.Controls.Add(this.Label47);
            this.Frame10.HeaderText = "Indicadores S.S.";
            this.Frame10.Location = new System.Drawing.Point(5, 120);
            this.Frame10.Name = "Frame10";
            this.Frame10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame10.Size = new System.Drawing.Size(351, 69);
            this.Frame10.TabIndex = 176;
            this.Frame10.Text = "Indicadores S.S.";
            // 
            // tbRENomTitularSS
            // 
            this.tbRENomTitularSS.AcceptsReturn = true;
            this.tbRENomTitularSS.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbRENomTitularSS.Enabled = false;
            this.tbRENomTitularSS.Location = new System.Drawing.Point(65, 43);
            this.tbRENomTitularSS.MaxLength = 60;
            this.tbRENomTitularSS.Name = "tbRENomTitularSS";
            this.tbRENomTitularSS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRENomTitularSS.Size = new System.Drawing.Size(221, 19);
            this.tbRENomTitularSS.TabIndex = 61;
            this.tbRENomTitularSS.WordWrap = false;
            this.tbRENomTitularSS.TextChanged += new System.EventHandler(this.tbRENomTitularSS_TextChanged);
            // 
            // _rbIndicadorSS_0
            // 
            this._rbIndicadorSS_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbIndicadorSS_0.Location = new System.Drawing.Point(10, 19);
            this._rbIndicadorSS_0.Name = "_rbIndicadorSS_0";
            this._rbIndicadorSS_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbIndicadorSS_0.Size = new System.Drawing.Size(52, 18);
            this._rbIndicadorSS_0.TabIndex = 57;
            this._rbIndicadorSS_0.Text = "Titular";
            this._rbIndicadorSS_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbIndicadorSS_CheckedChanged);
            // 
            // _rbIndicadorSS_1
            // 
            this._rbIndicadorSS_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbIndicadorSS_1.Location = new System.Drawing.Point(71, 17);
            this._rbIndicadorSS_1.Name = "_rbIndicadorSS_1";
            this._rbIndicadorSS_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbIndicadorSS_1.Size = new System.Drawing.Size(78, 18);
            this._rbIndicadorSS_1.TabIndex = 58;
            this._rbIndicadorSS_1.Text = "Beneficiario";
            this._rbIndicadorSS_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbIndicadorSS_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.rbPensionista);
            this.Frame1.Controls.Add(this.rbActivo);
            this.Frame1.HeaderText = "";
            this.Frame1.Location = new System.Drawing.Point(200, 12);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(140, 29);
            this.Frame1.TabIndex = 184;
            // 
            // rbPensionista
            // 
            this.rbPensionista.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbPensionista.Location = new System.Drawing.Point(57, 6);
            this.rbPensionista.Name = "rbPensionista";
            this.rbPensionista.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbPensionista.Size = new System.Drawing.Size(77, 18);
            this.rbPensionista.TabIndex = 60;
            this.rbPensionista.Text = "Pensionista";
            // 
            // rbActivo
            // 
            this.rbActivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbActivo.Location = new System.Drawing.Point(6, 6);
            this.rbActivo.Name = "rbActivo";
            this.rbActivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbActivo.Size = new System.Drawing.Size(51, 18);
            this.rbActivo.TabIndex = 59;
            this.rbActivo.Text = "Activo";
            // 
            // Label47
            // 
            this.Label47.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label47.Location = new System.Drawing.Point(10, 44);
            this.Label47.Name = "Label47";
            this.Label47.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label47.Size = new System.Drawing.Size(51, 18);
            this.Label47.TabIndex = 177;
            this.Label47.Text = "N.Titular:";
            // 
            // tbRENSS
            // 
            this.tbRENSS.AcceptsReturn = true;
            this.tbRENSS.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbRENSS.Location = new System.Drawing.Point(117, 42);
            this.tbRENSS.MaxLength = 20;
            this.tbRENSS.Name = "tbRENSS";
            this.tbRENSS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRENSS.Size = new System.Drawing.Size(154, 19);
            this.tbRENSS.TabIndex = 51;
            this.tbRENSS.TextChanged += new System.EventHandler(this.tbRENSS_TextChanged);
            this.tbRENSS.Enter += new System.EventHandler(this.tbRENSS_Enter);
            this.tbRENSS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRENSS_KeyPress);
            // 
            // cbBuscarCIAS
            // 
            this.cbBuscarCIAS.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscarCIAS.Image = ((System.Drawing.Image)(resources.GetObject("cbBuscarCIAS.Image")));
            this.cbBuscarCIAS.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbBuscarCIAS.Location = new System.Drawing.Point(120, 68);
            this.cbBuscarCIAS.Name = "cbBuscarCIAS";
            this.cbBuscarCIAS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbBuscarCIAS.Size = new System.Drawing.Size(25, 21);
            this.cbBuscarCIAS.TabIndex = 53;
            this.cbBuscarCIAS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbBuscarCIAS.Click += new System.EventHandler(this.cbBuscarCIAS_Click);
            // 
            // MEBCIAS
            // 
            this.MEBCIAS.Location = new System.Drawing.Point(32, 68);
            this.MEBCIAS.Mask = "##########?";
            this.MEBCIAS.Name = "MEBCIAS";
            this.MEBCIAS.PromptChar = ' ';
            this.MEBCIAS.Size = new System.Drawing.Size(86, 20);
            this.MEBCIAS.TabIndex = 52;
            this.MEBCIAS.TabStop = false;
            this.MEBCIAS.Enter += new System.EventHandler(this.MEBCIAS_Enter);
            this.MEBCIAS.Leave += new System.EventHandler(this.MEBCIAS_Leave);
            // 
            // cbBuscarCentro
            // 
            this.cbBuscarCentro.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscarCentro.Image = ((System.Drawing.Image)(resources.GetObject("cbBuscarCentro.Image")));
            this.cbBuscarCentro.Location = new System.Drawing.Point(330, 96);
            this.cbBuscarCentro.Name = "cbBuscarCentro";
            this.cbBuscarCentro.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbBuscarCentro.Size = new System.Drawing.Size(25, 21);
            this.cbBuscarCentro.TabIndex = 265;
            this.cbBuscarCentro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbBuscarCentro.Visible = false;
            this.cbBuscarCentro.Click += new System.EventHandler(this.cbBuscarCentro_Click);
            // 
            // Label76
            // 
            this.Label76.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label76.Location = new System.Drawing.Point(111, 97);
            this.Label76.Name = "Label76";
            this.Label76.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label76.Size = new System.Drawing.Size(93, 18);
            this.Label76.TabIndex = 233;
            this.Label76.Text = "Hosp. Referencia:";
            // 
            // Label75
            // 
            this.Label75.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label75.Location = new System.Drawing.Point(157, 70);
            this.Label75.Name = "Label75";
            this.Label75.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label75.Size = new System.Drawing.Size(85, 18);
            this.Label75.TabIndex = 232;
            this.Label75.Text = "Emplazamiento:";
            // 
            // cbbREInspeccion
            // 
            this.cbbREInspeccion.ColumnWidths = "";
            this.cbbREInspeccion.Location = new System.Drawing.Point(70, 18);
            this.cbbREInspeccion.Name = "cbbREInspeccion";
            this.cbbREInspeccion.Size = new System.Drawing.Size(284, 20);
            this.cbbREInspeccion.TabIndex = 50;
            this.cbbREInspeccion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbbREInspeccion_KeyDown);
            this.cbbREInspeccion.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbREInspeccion_Change);
            this.cbbREInspeccion.Click += new System.EventHandler(this.cbbREInspeccion_Click);
            this.cbbREInspeccion.Leave += new System.EventHandler(this.cbbREInspeccion_Leave);
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(22, 97);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(85, 18);
            this.Label2.TabIndex = 55;
            this.Label2.Text = "Dentro de Zona";
            // 
            // chkZona
            // 
            this.chkZona.Location = new System.Drawing.Point(4, 98);
            this.chkZona.Name = "chkZona";
            this.chkZona.Size = new System.Drawing.Size(15, 15);
            this.chkZona.TabIndex = 62;
            // 
            // Label29
            // 
            this.Label29.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label29.Location = new System.Drawing.Point(2, 71);
            this.Label29.Name = "Label29";
            this.Label29.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label29.Size = new System.Drawing.Size(32, 18);
            this.Label29.TabIndex = 153;
            this.Label29.Text = "CIAS:";
            // 
            // Label11
            // 
            this.Label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label11.Location = new System.Drawing.Point(5, 17);
            this.Label11.Name = "Label11";
            this.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label11.Size = new System.Drawing.Size(62, 18);
            this.Label11.TabIndex = 139;
            this.Label11.Text = "Inspecci�n:";
            // 
            // Label13
            // 
            this.Label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label13.Location = new System.Drawing.Point(5, 43);
            this.Label13.Name = "Label13";
            this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label13.Size = new System.Drawing.Size(108, 18);
            this.Label13.TabIndex = 138;
            this.Label13.Text = "N� Seguridad Social:";
            // 
            // frmRERegimen
            // 
            this.frmRERegimen.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmRERegimen.Controls.Add(this._rbRERegimen_2);
            this.frmRERegimen.Controls.Add(this._rbRERegimen_1);
            this.frmRERegimen.Controls.Add(this._rbRERegimen_0);
            this.frmRERegimen.HeaderText = "R�gimen Econ�mico";
            this.frmRERegimen.Location = new System.Drawing.Point(6, 1);
            this.frmRERegimen.Name = "frmRERegimen";
            this.frmRERegimen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmRERegimen.Size = new System.Drawing.Size(353, 43);
            this.frmRERegimen.TabIndex = 135;
            this.frmRERegimen.Text = "R�gimen Econ�mico";
            // 
            // _rbRERegimen_2
            // 
            this._rbRERegimen_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbRERegimen_2.Location = new System.Drawing.Point(261, 17);
            this._rbRERegimen_2.Name = "_rbRERegimen_2";
            this._rbRERegimen_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbRERegimen_2.Size = new System.Drawing.Size(58, 18);
            this._rbRERegimen_2.TabIndex = 47;
            this._rbRERegimen_2.Text = "Privado";
            this._rbRERegimen_2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbRERegimen_CheckedChanged);
            // 
            // _rbRERegimen_1
            // 
            this._rbRERegimen_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbRERegimen_1.Location = new System.Drawing.Point(149, 17);
            this._rbRERegimen_1.Name = "_rbRERegimen_1";
            this._rbRERegimen_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbRERegimen_1.Size = new System.Drawing.Size(66, 18);
            this._rbRERegimen_1.TabIndex = 46;
            this._rbRERegimen_1.Text = "Sociedad";
            this._rbRERegimen_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbRERegimen_CheckedChanged);
            // 
            // _rbRERegimen_0
            // 
            this._rbRERegimen_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbRERegimen_0.Location = new System.Drawing.Point(13, 17);
            this._rbRERegimen_0.Name = "_rbRERegimen_0";
            this._rbRERegimen_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbRERegimen_0.Size = new System.Drawing.Size(102, 18);
            this._rbRERegimen_0.TabIndex = 45;
            this._rbRERegimen_0.Text = "Seguridad social";
            this._rbRERegimen_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbRERegimen_CheckedChanged);
            // 
            // _tsDatosPac_TabPage2
            // 
            this._tsDatosPac_TabPage2.Controls.Add(this.Frame3);
            this._tsDatosPac_TabPage2.Controls.Add(this.Frame11);
            this._tsDatosPac_TabPage2.ItemSize = new System.Drawing.SizeF(118F, 28F);
            this._tsDatosPac_TabPage2.Location = new System.Drawing.Point(10, 37);
            this._tsDatosPac_TabPage2.Name = "_tsDatosPac_TabPage2";
            this._tsDatosPac_TabPage2.Size = new System.Drawing.Size(738, 519);
            this._tsDatosPac_TabPage2.Text = "Familiar de contacto";
            // 
            // Frame3
            // 
            this.Frame3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame3.Controls.Add(this.Frame16);
            this.Frame3.Controls.Add(this.Frame15);
            this.Frame3.Controls.Add(this.Frame14);
            this.Frame3.Controls.Add(this.cbBuscar3);
            this.Frame3.Controls.Add(this.Frame7);
            this.Frame3.Controls.Add(this.tbPCApe2);
            this.Frame3.Controls.Add(this.tbPCApe1);
            this.Frame3.Controls.Add(this.tbPCNombre);
            this.Frame3.Controls.Add(this.tbPCDomicilio);
            this.Frame3.Controls.Add(this.tbPCTelf2);
            this.Frame3.Controls.Add(this.tbPCTelf1);
            this.Frame3.Controls.Add(this.tbPCExt2);
            this.Frame3.Controls.Add(this.tbPCParentesco);
            this.Frame3.Controls.Add(this.cbPCAceptar);
            this.Frame3.Controls.Add(this.cbPCCancelar);
            this.Frame3.Controls.Add(this.cbDPEliminar);
            this.Frame3.Controls.Add(this.tbPCExt1);
            this.Frame3.Controls.Add(this.mebPCCodPostal);
            this.Frame3.Controls.Add(this.Label49);
            this.Frame3.Controls.Add(this.cbbPCPoblacion);
            this.Frame3.Controls.Add(this._LabelContacto_4);
            this.Frame3.Controls.Add(this._LabelContacto_5);
            this.Frame3.Controls.Add(this.Label64);
            this.Frame3.Controls.Add(this._LabelContacto_9);
            this.Frame3.Controls.Add(this._LabelContacto_8);
            this.Frame3.Controls.Add(this._LabelContacto_7);
            this.Frame3.Controls.Add(this._LabelContacto_6);
            this.Frame3.Controls.Add(this._LabelContacto_3);
            this.Frame3.Controls.Add(this._LabelContacto_2);
            this.Frame3.Controls.Add(this._LabelContacto_1);
            this.Frame3.Controls.Add(this._LabelContacto_0);
            this.Frame3.HeaderText = "Persona de contacto";
            this.Frame3.Location = new System.Drawing.Point(8, 158);
            this.Frame3.Name = "Frame3";
            this.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame3.Size = new System.Drawing.Size(723, 348);
            this.Frame3.TabIndex = 156;
            this.Frame3.Text = "Persona de contacto";
            // 
            // Frame16
            // 
            this.Frame16.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame16.Controls.Add(this._rbDNIOtroDocPC_0);
            this.Frame16.Controls.Add(this._rbDNIOtroDocPC_1);
            this.Frame16.Controls.Add(this.tbNIFOtrosDocPC);
            this.Frame16.HeaderText = "";
            this.Frame16.Location = new System.Drawing.Point(326, 42);
            this.Frame16.Name = "Frame16";
            this.Frame16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame16.Size = new System.Drawing.Size(233, 56);
            this.Frame16.TabIndex = 182;
            // 
            // _rbDNIOtroDocPC_0
            // 
            this._rbDNIOtroDocPC_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbDNIOtroDocPC_0.Location = new System.Drawing.Point(8, 4);
            this._rbDNIOtroDocPC_0.Name = "_rbDNIOtroDocPC_0";
            this._rbDNIOtroDocPC_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbDNIOtroDocPC_0.Size = new System.Drawing.Size(61, 18);
            this._rbDNIOtroDocPC_0.TabIndex = 112;
            this._rbDNIOtroDocPC_0.Text = "DNI/NIF";
            this._rbDNIOtroDocPC_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbDNIOtroDocPC_CheckedChanged);
            // 
            // _rbDNIOtroDocPC_1
            // 
            this._rbDNIOtroDocPC_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbDNIOtroDocPC_1.Location = new System.Drawing.Point(112, 4);
            this._rbDNIOtroDocPC_1.Name = "_rbDNIOtroDocPC_1";
            this._rbDNIOtroDocPC_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbDNIOtroDocPC_1.Size = new System.Drawing.Size(113, 18);
            this._rbDNIOtroDocPC_1.TabIndex = 113;
            this._rbDNIOtroDocPC_1.Text = "Otros documentos";
            this._rbDNIOtroDocPC_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbDNIOtroDocPC_CheckedChanged);
            // 
            // tbNIFOtrosDocPC
            // 
            this.tbNIFOtrosDocPC.Enabled = false;
            this.tbNIFOtrosDocPC.Location = new System.Drawing.Point(32, 27);
            this.tbNIFOtrosDocPC.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.tbNIFOtrosDocPC.Name = "tbNIFOtrosDocPC";
            this.tbNIFOtrosDocPC.PromptChar = ' ';
            this.tbNIFOtrosDocPC.Size = new System.Drawing.Size(125, 20);
            this.tbNIFOtrosDocPC.TabIndex = 114;
            this.tbNIFOtrosDocPC.TabStop = false;
            this.tbNIFOtrosDocPC.Text = "            ";
            this.tbNIFOtrosDocPC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNIFOtrosDocPC_KeyPress);
            this.tbNIFOtrosDocPC.TextChanged += new System.EventHandler(this.tbNIFOtrosDocPC_TextChanged);
            this.tbNIFOtrosDocPC.Enter += new System.EventHandler(this.tbNIFOtrosDocPC_Enter);
            this.tbNIFOtrosDocPC.Leave += new System.EventHandler(this.tbNIFOtrosDocPC_Leave);
            // 
            // Frame15
            // 
            this.Frame15.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame15.Controls.Add(this._rbResponsable_1);
            this.Frame15.Controls.Add(this._rbResponsable_0);
            this.Frame15.Controls.Add(this._LabelContacto_10);
            this.Frame15.HeaderText = "";
            this.Frame15.Location = new System.Drawing.Point(294, 209);
            this.Frame15.Name = "Frame15";
            this.Frame15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame15.Size = new System.Drawing.Size(281, 37);
            this.Frame15.TabIndex = 180;
            // 
            // _rbResponsable_1
            // 
            this._rbResponsable_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbResponsable_1.Location = new System.Drawing.Point(134, 10);
            this._rbResponsable_1.Name = "_rbResponsable_1";
            this._rbResponsable_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbResponsable_1.Size = new System.Drawing.Size(35, 18);
            this._rbResponsable_1.TabIndex = 127;
            this._rbResponsable_1.Text = "No";
            // 
            // _rbResponsable_0
            // 
            this._rbResponsable_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbResponsable_0.Location = new System.Drawing.Point(88, 10);
            this._rbResponsable_0.Name = "_rbResponsable_0";
            this._rbResponsable_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbResponsable_0.Size = new System.Drawing.Size(29, 18);
            this._rbResponsable_0.TabIndex = 126;
            this._rbResponsable_0.Text = "Si";
            // 
            // _LabelContacto_10
            // 
            this._LabelContacto_10.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_10.Location = new System.Drawing.Point(8, 12);
            this._LabelContacto_10.Name = "_LabelContacto_10";
            this._LabelContacto_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_10.Size = new System.Drawing.Size(72, 18);
            this._LabelContacto_10.TabIndex = 181;
            this._LabelContacto_10.Text = "Responsable:";
            // 
            // Frame14
            // 
            this.Frame14.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame14.Controls.Add(this._rbTutor_1);
            this.Frame14.Controls.Add(this._rbTutor_0);
            this.Frame14.HeaderText = "";
            this.Frame14.Location = new System.Drawing.Point(6, 209);
            this.Frame14.Name = "Frame14";
            this.Frame14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame14.Size = new System.Drawing.Size(285, 37);
            this.Frame14.TabIndex = 179;
            // 
            // _rbTutor_1
            // 
            this._rbTutor_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbTutor_1.Location = new System.Drawing.Point(134, 10);
            this._rbTutor_1.Name = "_rbTutor_1";
            this._rbTutor_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbTutor_1.Size = new System.Drawing.Size(111, 18);
            this._rbTutor_1.TabIndex = 125;
            this._rbTutor_1.Text = "Cuidador Habitual";
            this._rbTutor_1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbTutor_CheckedChanged);
            // 
            // _rbTutor_0
            // 
            this._rbTutor_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbTutor_0.Location = new System.Drawing.Point(32, 10);
            this._rbTutor_0.Name = "_rbTutor_0";
            this._rbTutor_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbTutor_0.Size = new System.Drawing.Size(76, 18);
            this._rbTutor_0.TabIndex = 124;
            this._rbTutor_0.Text = "Tutor Legal";
            this._rbTutor_0.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbTutor_CheckedChanged);
            // 
            // cbBuscar3
            // 
            this.cbBuscar3.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscar3.Image = ((System.Drawing.Image)(resources.GetObject("cbBuscar3.Image")));
            this.cbBuscar3.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbBuscar3.Location = new System.Drawing.Point(540, 128);
            this.cbBuscar3.Name = "cbBuscar3";
            this.cbBuscar3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbBuscar3.Size = new System.Drawing.Size(25, 21);
            this.cbBuscar3.TabIndex = 118;
            this.cbBuscar3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbBuscar3.Click += new System.EventHandler(this.cbBuscar3_Click);
            // 
            // Frame7
            // 
            this.Frame7.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame7.Controls.Add(this.chbPCExtrangero);
            this.Frame7.Controls.Add(this.tbPCExPob);
            this.Frame7.Controls.Add(this.cbbPCPais);
            this.Frame7.Controls.Add(this._LabelContacto_11);
            this.Frame7.Controls.Add(this._LabelContacto_12);
            this.Frame7.HeaderText = "Residencia en el extranjero";
            this.Frame7.Location = new System.Drawing.Point(6, 249);
            this.Frame7.Name = "Frame7";
            this.Frame7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame7.Size = new System.Drawing.Size(709, 53);
            this.Frame7.TabIndex = 157;
            this.Frame7.Text = "Residencia en el extranjero";
            // 
            // chbPCExtrangero
            // 
            this.chbPCExtrangero.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbPCExtrangero.Location = new System.Drawing.Point(8, 20);
            this.chbPCExtrangero.Name = "chbPCExtrangero";
            this.chbPCExtrangero.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbPCExtrangero.Size = new System.Drawing.Size(127, 18);
            this.chbPCExtrangero.TabIndex = 128;
            this.chbPCExtrangero.Text = "Residencia extranjero";
            this.chbPCExtrangero.CheckStateChanged += new System.EventHandler(this.chbPCExtrangero_CheckStateChanged);
            // 
            // tbPCExPob
            // 
            this.tbPCExPob.AcceptsReturn = true;
            this.tbPCExPob.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCExPob.Enabled = false;
            this.tbPCExPob.Location = new System.Drawing.Point(458, 19);
            this.tbPCExPob.MaxLength = 40;
            this.tbPCExPob.Name = "tbPCExPob";
            this.tbPCExPob.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCExPob.Size = new System.Drawing.Size(245, 19);
            this.tbPCExPob.TabIndex = 130;
            this.tbPCExPob.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCExPob_KeyPress);
            // 
            // cbbPCPais
            // 
            this.cbbPCPais.ColumnWidths = "";
            this.cbbPCPais.Enabled = false;
            this.cbbPCPais.Location = new System.Drawing.Point(168, 19);
            this.cbbPCPais.Name = "cbbPCPais";
            this.cbbPCPais.Size = new System.Drawing.Size(219, 20);
            this.cbbPCPais.TabIndex = 129;
            this.cbbPCPais.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbPCPais_SelectedIndexChanged);
            this.cbbPCPais.Click += new System.EventHandler(this.cbbPCPais_Click);
            // 
            // _LabelContacto_11
            // 
            this._LabelContacto_11.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_11.Enabled = false;
            this._LabelContacto_11.Location = new System.Drawing.Point(138, 20);
            this._LabelContacto_11.Name = "_LabelContacto_11";
            this._LabelContacto_11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_11.Size = new System.Drawing.Size(28, 18);
            this._LabelContacto_11.TabIndex = 159;
            this._LabelContacto_11.Text = "Pa�s:";
            // 
            // _LabelContacto_12
            // 
            this._LabelContacto_12.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_12.Enabled = false;
            this._LabelContacto_12.Location = new System.Drawing.Point(394, 20);
            this._LabelContacto_12.Name = "_LabelContacto_12";
            this._LabelContacto_12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_12.Size = new System.Drawing.Size(58, 18);
            this._LabelContacto_12.TabIndex = 158;
            this._LabelContacto_12.Text = "Poblaci�n:";
            // 
            // tbPCApe2
            // 
            this.tbPCApe2.AcceptsReturn = true;
            this.tbPCApe2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCApe2.Location = new System.Drawing.Point(378, 18);
            this.tbPCApe2.MaxLength = 35;
            this.tbPCApe2.Name = "tbPCApe2";
            this.tbPCApe2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCApe2.Size = new System.Drawing.Size(237, 19);
            this.tbPCApe2.TabIndex = 110;
            this.tbPCApe2.TextChanged += new System.EventHandler(this.tbPCApe2_TextChanged);
            this.tbPCApe2.Enter += new System.EventHandler(this.tbPCApe2_Enter);
            this.tbPCApe2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCApe2_KeyPress);
            // 
            // tbPCApe1
            // 
            this.tbPCApe1.AcceptsReturn = true;
            this.tbPCApe1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCApe1.Location = new System.Drawing.Point(73, 19);
            this.tbPCApe1.MaxLength = 35;
            this.tbPCApe1.Name = "tbPCApe1";
            this.tbPCApe1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCApe1.Size = new System.Drawing.Size(237, 19);
            this.tbPCApe1.TabIndex = 109;
            this.tbPCApe1.TextChanged += new System.EventHandler(this.tbPCApe1_TextChanged);
            this.tbPCApe1.Enter += new System.EventHandler(this.tbPCApe1_Enter);
            this.tbPCApe1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCApe1_KeyPress);
            // 
            // tbPCNombre
            // 
            this.tbPCNombre.AcceptsReturn = true;
            this.tbPCNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCNombre.Location = new System.Drawing.Point(73, 48);
            this.tbPCNombre.MaxLength = 35;
            this.tbPCNombre.Name = "tbPCNombre";
            this.tbPCNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCNombre.Size = new System.Drawing.Size(237, 19);
            this.tbPCNombre.TabIndex = 111;
            this.tbPCNombre.TextChanged += new System.EventHandler(this.tbPCNombre_TextChanged);
            this.tbPCNombre.Enter += new System.EventHandler(this.tbPCNombre_Enter);
            this.tbPCNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCNombre_KeyPress);
            // 
            // tbPCDomicilio
            // 
            this.tbPCDomicilio.AcceptsReturn = true;
            this.tbPCDomicilio.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCDomicilio.Location = new System.Drawing.Point(73, 102);
            this.tbPCDomicilio.MaxLength = 100;
            this.tbPCDomicilio.Name = "tbPCDomicilio";
            this.tbPCDomicilio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCDomicilio.Size = new System.Drawing.Size(339, 19);
            this.tbPCDomicilio.TabIndex = 115;
            this.tbPCDomicilio.Enter += new System.EventHandler(this.tbPCDomicilio_Enter);
            this.tbPCDomicilio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCDomicilio_KeyPress);
            // 
            // tbPCTelf2
            // 
            this.tbPCTelf2.AcceptsReturn = true;
            this.tbPCTelf2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCTelf2.Location = new System.Drawing.Point(351, 159);
            this.tbPCTelf2.MaxLength = 15;
            this.tbPCTelf2.Name = "tbPCTelf2";
            this.tbPCTelf2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCTelf2.Size = new System.Drawing.Size(105, 19);
            this.tbPCTelf2.TabIndex = 121;
            this.tbPCTelf2.Enter += new System.EventHandler(this.tbPCTelf2_Enter);
            this.tbPCTelf2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCTelf2_KeyPress);
            // 
            // tbPCTelf1
            // 
            this.tbPCTelf1.AcceptsReturn = true;
            this.tbPCTelf1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCTelf1.Location = new System.Drawing.Point(73, 159);
            this.tbPCTelf1.MaxLength = 15;
            this.tbPCTelf1.Name = "tbPCTelf1";
            this.tbPCTelf1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCTelf1.Size = new System.Drawing.Size(92, 19);
            this.tbPCTelf1.TabIndex = 119;
            this.tbPCTelf1.TextChanged += new System.EventHandler(this.tbPCTelf1_TextChanged);
            this.tbPCTelf1.Enter += new System.EventHandler(this.tbPCTelf1_Enter);
            this.tbPCTelf1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCTelf1_KeyPress);
            // 
            // tbPCExt2
            // 
            this.tbPCExt2.AcceptsReturn = true;
            this.tbPCExt2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCExt2.Location = new System.Drawing.Point(526, 159);
            this.tbPCExt2.MaxLength = 5;
            this.tbPCExt2.Name = "tbPCExt2";
            this.tbPCExt2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCExt2.Size = new System.Drawing.Size(41, 19);
            this.tbPCExt2.TabIndex = 122;
            this.tbPCExt2.Enter += new System.EventHandler(this.tbPCExt2_Enter);
            this.tbPCExt2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCExt2_KeyPress);
            // 
            // tbPCParentesco
            // 
            this.tbPCParentesco.AcceptsReturn = true;
            this.tbPCParentesco.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCParentesco.Location = new System.Drawing.Point(73, 186);
            this.tbPCParentesco.MaxLength = 10;
            this.tbPCParentesco.Name = "tbPCParentesco";
            this.tbPCParentesco.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCParentesco.Size = new System.Drawing.Size(80, 19);
            this.tbPCParentesco.TabIndex = 123;
            this.tbPCParentesco.Enter += new System.EventHandler(this.tbPCParentesco_Enter);
            this.tbPCParentesco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCParentesco_KeyPress);
            // 
            // cbPCAceptar
            // 
            this.cbPCAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbPCAceptar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbPCAceptar.Enabled = false;
            this.cbPCAceptar.Location = new System.Drawing.Point(546, 309);
            this.cbPCAceptar.Name = "cbPCAceptar";
            this.cbPCAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbPCAceptar.Size = new System.Drawing.Size(81, 28);
            this.cbPCAceptar.TabIndex = 132;
            this.cbPCAceptar.Text = "Ace&ptar";
            this.cbPCAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbPCAceptar.Click += new System.EventHandler(this.cbPCAceptar_Click);
            // 
            // cbPCCancelar
            // 
            this.cbPCCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbPCCancelar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbPCCancelar.Location = new System.Drawing.Point(634, 309);
            this.cbPCCancelar.Name = "cbPCCancelar";
            this.cbPCCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbPCCancelar.Size = new System.Drawing.Size(81, 28);
            this.cbPCCancelar.TabIndex = 133;
            this.cbPCCancelar.Text = "Ca&ncelar";
            this.cbPCCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbPCCancelar.Click += new System.EventHandler(this.cbPCCancelar_Click);
            // 
            // cbDPEliminar
            // 
            this.cbDPEliminar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbDPEliminar.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.cbDPEliminar.Enabled = false;
            this.cbDPEliminar.Location = new System.Drawing.Point(458, 309);
            this.cbDPEliminar.Name = "cbDPEliminar";
            this.cbDPEliminar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbDPEliminar.Size = new System.Drawing.Size(81, 28);
            this.cbDPEliminar.TabIndex = 131;
            this.cbDPEliminar.Text = "&Eliminar";
            this.cbDPEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cbDPEliminar.Click += new System.EventHandler(this.cbDPEliminar_Click);
            // 
            // tbPCExt1
            // 
            this.tbPCExt1.AcceptsReturn = true;
            this.tbPCExt1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPCExt1.Location = new System.Drawing.Point(237, 159);
            this.tbPCExt1.MaxLength = 5;
            this.tbPCExt1.Name = "tbPCExt1";
            this.tbPCExt1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbPCExt1.Size = new System.Drawing.Size(41, 19);
            this.tbPCExt1.TabIndex = 120;
            this.tbPCExt1.Enter += new System.EventHandler(this.tbPCExt1_Enter);
            this.tbPCExt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPCExt1_KeyPress);
            // 
            // mebPCCodPostal
            // 
            this.mebPCCodPostal.Location = new System.Drawing.Point(73, 130);
            this.mebPCCodPostal.Name = "mebPCCodPostal";
            this.mebPCCodPostal.PromptChar = ' ';
            this.mebPCCodPostal.Size = new System.Drawing.Size(32, 20);
            this.mebPCCodPostal.TabIndex = 116;
            this.mebPCCodPostal.TabStop = false;
            this.mebPCCodPostal.Enter += new System.EventHandler(this.mebPCCodPostal_Enter);
            this.mebPCCodPostal.Leave += new System.EventHandler(this.mebPCCodPostal_Leave);
            // 
            // Label49
            // 
            this.Label49.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label49.Location = new System.Drawing.Point(248, 221);
            this.Label49.Name = "Label49";
            this.Label49.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label49.Size = new System.Drawing.Size(2, 2);
            this.Label49.TabIndex = 178;
            // 
            // cbbPCPoblacion
            // 
            this.cbbPCPoblacion.ColumnWidths = "";
            this.cbbPCPoblacion.Location = new System.Drawing.Point(188, 128);
            this.cbbPCPoblacion.Name = "cbbPCPoblacion";
            this.cbbPCPoblacion.Size = new System.Drawing.Size(345, 20);
            this.cbbPCPoblacion.TabIndex = 117;
            this.cbbPCPoblacion.Click += new System.EventHandler(this.cbbPCPoblacion_Click);
            // 
            // _LabelContacto_4
            // 
            this._LabelContacto_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_4.Location = new System.Drawing.Point(4, 132);
            this._LabelContacto_4.Name = "_LabelContacto_4";
            this._LabelContacto_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_4.Size = new System.Drawing.Size(65, 18);
            this._LabelContacto_4.TabIndex = 170;
            this._LabelContacto_4.Text = "C�d. postal:";
            // 
            // _LabelContacto_5
            // 
            this._LabelContacto_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_5.Location = new System.Drawing.Point(124, 132);
            this._LabelContacto_5.Name = "_LabelContacto_5";
            this._LabelContacto_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_5.Size = new System.Drawing.Size(58, 18);
            this._LabelContacto_5.TabIndex = 169;
            this._LabelContacto_5.Text = "Poblaci�n:";
            // 
            // Label64
            // 
            this.Label64.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label64.Location = new System.Drawing.Point(4, 185);
            this.Label64.Name = "Label64";
            this.Label64.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label64.Size = new System.Drawing.Size(63, 18);
            this.Label64.TabIndex = 168;
            this.Label64.Text = "Parentesco:";
            // 
            // _LabelContacto_9
            // 
            this._LabelContacto_9.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_9.Location = new System.Drawing.Point(466, 159);
            this._LabelContacto_9.Name = "_LabelContacto_9";
            this._LabelContacto_9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_9.Size = new System.Drawing.Size(56, 18);
            this._LabelContacto_9.TabIndex = 167;
            this._LabelContacto_9.Text = "Extensi�n:";
            // 
            // _LabelContacto_8
            // 
            this._LabelContacto_8.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_8.Location = new System.Drawing.Point(288, 159);
            this._LabelContacto_8.Name = "_LabelContacto_8";
            this._LabelContacto_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_8.Size = new System.Drawing.Size(62, 18);
            this._LabelContacto_8.TabIndex = 166;
            this._LabelContacto_8.Text = "Tel�fono 2:";
            // 
            // _LabelContacto_7
            // 
            this._LabelContacto_7.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_7.Location = new System.Drawing.Point(176, 159);
            this._LabelContacto_7.Name = "_LabelContacto_7";
            this._LabelContacto_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_7.Size = new System.Drawing.Size(56, 18);
            this._LabelContacto_7.TabIndex = 165;
            this._LabelContacto_7.Text = "Extensi�n:";
            // 
            // _LabelContacto_6
            // 
            this._LabelContacto_6.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_6.Location = new System.Drawing.Point(4, 159);
            this._LabelContacto_6.Name = "_LabelContacto_6";
            this._LabelContacto_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_6.Size = new System.Drawing.Size(62, 18);
            this._LabelContacto_6.TabIndex = 164;
            this._LabelContacto_6.Text = "Tel�fono 1:";
            // 
            // _LabelContacto_3
            // 
            this._LabelContacto_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_3.Location = new System.Drawing.Point(4, 101);
            this._LabelContacto_3.Name = "_LabelContacto_3";
            this._LabelContacto_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_3.Size = new System.Drawing.Size(56, 18);
            this._LabelContacto_3.TabIndex = 163;
            this._LabelContacto_3.Text = "Domicilio:";
            // 
            // _LabelContacto_2
            // 
            this._LabelContacto_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_2.Location = new System.Drawing.Point(4, 50);
            this._LabelContacto_2.Name = "_LabelContacto_2";
            this._LabelContacto_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_2.Size = new System.Drawing.Size(50, 18);
            this._LabelContacto_2.TabIndex = 162;
            this._LabelContacto_2.Text = "Nombre:";
            // 
            // _LabelContacto_1
            // 
            this._LabelContacto_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_1.Location = new System.Drawing.Point(316, 20);
            this._LabelContacto_1.Name = "_LabelContacto_1";
            this._LabelContacto_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_1.Size = new System.Drawing.Size(59, 18);
            this._LabelContacto_1.TabIndex = 161;
            this._LabelContacto_1.Text = "Apellido 2:";
            // 
            // _LabelContacto_0
            // 
            this._LabelContacto_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._LabelContacto_0.Location = new System.Drawing.Point(4, 22);
            this._LabelContacto_0.Name = "_LabelContacto_0";
            this._LabelContacto_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._LabelContacto_0.Size = new System.Drawing.Size(59, 18);
            this._LabelContacto_0.TabIndex = 160;
            this._LabelContacto_0.Text = "Apellido 1:";
            // 
            // Frame11
            // 
            this.Frame11.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame11.Controls.Add(this.sprContactos);
            this.Frame11.HeaderText = "Personas de Contacto";
            this.Frame11.Location = new System.Drawing.Point(8, 6);
            this.Frame11.Name = "Frame11";
            this.Frame11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame11.Size = new System.Drawing.Size(723, 145);
            this.Frame11.TabIndex = 154;
            this.Frame11.Text = "Personas de Contacto";
            // 
            // sprContactos
            // 
            this.sprContactos.AutoScroll = true;
            this.sprContactos.Location = new System.Drawing.Point(8, 18);
            // 
            // 
            // 
            gridViewTextBoxColumn1.HeaderText = "Apellidos y Nombre";
            gridViewTextBoxColumn1.Name = "dpersonac";
            gridViewTextBoxColumn1.Width = 180;
            gridViewTextBoxColumn2.HeaderText = "DNI/NIF";
            gridViewTextBoxColumn2.Name = "ndninifp";
            gridViewTextBoxColumn2.Width = 60;
            gridViewTextBoxColumn3.HeaderText = "Domicilio";
            gridViewTextBoxColumn3.Name = "ddireper";
            gridViewTextBoxColumn3.Width = 180;
            gridViewTextBoxColumn4.HeaderText = "C�d.";
            gridViewTextBoxColumn4.Name = "gcodipos";
            gridViewTextBoxColumn4.Width = 60;
            gridViewTextBoxColumn5.HeaderText = "Poblaci�n";
            gridViewTextBoxColumn5.Name = "Poblacion";
            gridViewTextBoxColumn5.Width = 120;
            gridViewTextBoxColumn6.HeaderText = "Telefono1";
            gridViewTextBoxColumn6.Name = "ntelefo1";
            gridViewTextBoxColumn6.Width = 120;
            gridViewTextBoxColumn7.HeaderText = "Telefono2";
            gridViewTextBoxColumn7.Name = "ntelefo2";
            gridViewTextBoxColumn7.Width = 120;
            gridViewTextBoxColumn8.HeaderText = "Parentesco";
            gridViewTextBoxColumn8.Name = "dparente";
            gridViewTextBoxColumn8.Width = 120;
            gridViewTextBoxColumn9.HeaderText = "Responsable";
            gridViewTextBoxColumn9.Name = "irespons";
            gridViewTextBoxColumn9.Width = 120;
            gridViewTextBoxColumn10.HeaderText = "Pa�s";
            gridViewTextBoxColumn10.Name = "dpaisres";
            gridViewTextBoxColumn10.Width = 120;
            gridViewTextBoxColumn11.HeaderText = "Poblacion";
            gridViewTextBoxColumn11.Name = "dpoblaci";
            gridViewTextBoxColumn11.Width = 120;
            gridViewTextBoxColumn12.HeaderText = "L";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "nnumorde";
            gridViewTextBoxColumn12.Width = 30;
            gridViewTextBoxColumn13.HeaderText = "M";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "gpoblaci";
            gridViewTextBoxColumn13.Width = 30;
            gridViewTextBoxColumn14.HeaderText = "N";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "gpaisres";
            gridViewTextBoxColumn14.Width = 30;
            gridViewTextBoxColumn15.HeaderText = "O";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "dnombper";
            gridViewTextBoxColumn15.Width = 30;
            gridViewTextBoxColumn16.HeaderText = "P";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "dape1per";
            gridViewTextBoxColumn16.Width = 30;
            gridViewTextBoxColumn17.HeaderText = "Q";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "dape2per";
            gridViewTextBoxColumn17.Width = 30;
            gridViewTextBoxColumn18.HeaderText = "R";
            gridViewTextBoxColumn18.IsVisible = false;
            gridViewTextBoxColumn18.Name = "EXISTE";
            gridViewTextBoxColumn18.Width = 30;
            gridViewTextBoxColumn19.HeaderText = "S";
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "gprovinc";
            gridViewTextBoxColumn19.Width = 30;
            gridViewTextBoxColumn20.HeaderText = "Tipo de Contacto";
            gridViewTextBoxColumn20.Name = "itipocon";
            gridViewTextBoxColumn20.Width = 200;
            this.sprContactos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20});
            this.sprContactos.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.sprContactos.Name = "sprContactos";
            this.sprContactos.Size = new System.Drawing.Size(709, 121);
            this.sprContactos.TabIndex = 155;
            this.sprContactos.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprContactos_CellClick);
            ((Telerik.WinControls.UI.GridTableElement)(this.sprContactos.GetChildAt(0).GetChildAt(0).GetChildAt(2))).RowHeight = 20;
            ((Telerik.WinControls.UI.GridTableElement)(this.sprContactos.GetChildAt(0).GetChildAt(0).GetChildAt(2))).Text = "";
            // 
            // _tsDatosPac_TabPage3
            // 
            this._tsDatosPac_TabPage3.Controls.Add(this.fraColectivos);
            this._tsDatosPac_TabPage3.Controls.Add(this.frmGradoDependencia);
            this._tsDatosPac_TabPage3.Controls.Add(this.lbNombrePaciente);
            this._tsDatosPac_TabPage3.Controls.Add(this.lbPaciente);
            this._tsDatosPac_TabPage3.ItemSize = new System.Drawing.SizeF(104F, 28F);
            this._tsDatosPac_TabPage3.Location = new System.Drawing.Point(10, 37);
            this._tsDatosPac_TabPage3.Name = "_tsDatosPac_TabPage3";
            this._tsDatosPac_TabPage3.Size = new System.Drawing.Size(738, 519);
            this._tsDatosPac_TabPage3.Text = "Datos adicionales";
            // 
            // fraColectivos
            // 
            this.fraColectivos.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.fraColectivos.Controls.Add(this.tbNumTrabajador);
            this.fraColectivos.Controls.Add(this.cbbCategoria);
            this.fraColectivos.Controls.Add(this.cbbParentesco);
            this.fraColectivos.Controls.Add(this._labelColectivos_2);
            this.fraColectivos.Controls.Add(this._labelColectivos_1);
            this.fraColectivos.Controls.Add(this._labelColectivos_0);
            this.fraColectivos.HeaderText = "Colectivos y Categ. Especial ";
            this.fraColectivos.Location = new System.Drawing.Point(104, 266);
            this.fraColectivos.Name = "fraColectivos";
            this.fraColectivos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fraColectivos.Size = new System.Drawing.Size(557, 165);
            this.fraColectivos.TabIndex = 215;
            this.fraColectivos.Text = "Colectivos y Categ. Especial ";
            // 
            // tbNumTrabajador
            // 
            this.tbNumTrabajador.AcceptsReturn = true;
            this.tbNumTrabajador.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNumTrabajador.Location = new System.Drawing.Point(238, 36);
            this.tbNumTrabajador.MaxLength = 10;
            this.tbNumTrabajador.Name = "tbNumTrabajador";
            this.tbNumTrabajador.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNumTrabajador.Size = new System.Drawing.Size(147, 19);
            this.tbNumTrabajador.TabIndex = 260;
            this.tbNumTrabajador.Enter += new System.EventHandler(this.tbNumTrabajador_Enter);
            // 
            // cbbCategoria
            // 
            this.cbbCategoria.ColumnWidths = "";
            this.cbbCategoria.Location = new System.Drawing.Point(238, 108);
            this.cbbCategoria.Name = "cbbCategoria";
            this.cbbCategoria.Size = new System.Drawing.Size(297, 20);
            this.cbbCategoria.TabIndex = 262;
            // 
            // cbbParentesco
            // 
            this.cbbParentesco.ColumnWidths = "";
            this.cbbParentesco.Location = new System.Drawing.Point(238, 70);
            this.cbbParentesco.Name = "cbbParentesco";
            this.cbbParentesco.Size = new System.Drawing.Size(297, 20);
            this.cbbParentesco.TabIndex = 261;
            // 
            // _labelColectivos_2
            // 
            this._labelColectivos_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._labelColectivos_2.Location = new System.Drawing.Point(18, 111);
            this._labelColectivos_2.Name = "_labelColectivos_2";
            this._labelColectivos_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._labelColectivos_2.Size = new System.Drawing.Size(100, 18);
            this._labelColectivos_2.TabIndex = 218;
            this._labelColectivos_2.Text = "Categor�a Especial:";
            // 
            // _labelColectivos_1
            // 
            this._labelColectivos_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._labelColectivos_1.Location = new System.Drawing.Point(18, 73);
            this._labelColectivos_1.Name = "_labelColectivos_1";
            this._labelColectivos_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._labelColectivos_1.Size = new System.Drawing.Size(151, 18);
            this._labelColectivos_1.TabIndex = 217;
            this._labelColectivos_1.Text = "Parentesco con el trabajador:";
            // 
            // _labelColectivos_0
            // 
            this._labelColectivos_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._labelColectivos_0.Location = new System.Drawing.Point(18, 37);
            this._labelColectivos_0.Name = "_labelColectivos_0";
            this._labelColectivos_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._labelColectivos_0.Size = new System.Drawing.Size(122, 18);
            this._labelColectivos_0.TabIndex = 216;
            this._labelColectivos_0.Text = "N�mero de Trabajador:";
            // 
            // frmGradoDependencia
            // 
            this.frmGradoDependencia.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmGradoDependencia.Controls.Add(this.tbOtrasCircunstanciasGradoDep);
            this.frmGradoDependencia.Controls.Add(this._sdcGradoDep_0);
            this.frmGradoDependencia.Controls.Add(this._sdcGradoDep_1);
            this.frmGradoDependencia.Controls.Add(this._sdcGradoDep_2);
            this.frmGradoDependencia.Controls.Add(this._sdcGradoDep_3);
            this.frmGradoDependencia.Controls.Add(this._sdcGradoDep_4);
            this.frmGradoDependencia.Controls.Add(this._sdcGradoDep_5);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_3);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_5);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_4);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_2);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_1);
            this.frmGradoDependencia.Controls.Add(this._lbDependencia_0);
            this.frmGradoDependencia.HeaderText = "Grado de dependencia ";
            this.frmGradoDependencia.Location = new System.Drawing.Point(104, 54);
            this.frmGradoDependencia.Name = "frmGradoDependencia";
            this.frmGradoDependencia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmGradoDependencia.Size = new System.Drawing.Size(557, 197);
            this.frmGradoDependencia.TabIndex = 246;
            this.frmGradoDependencia.Text = "Grado de dependencia ";
            // 
            // tbOtrasCircunstanciasGradoDep
            // 
            this.tbOtrasCircunstanciasGradoDep.AcceptsReturn = true;
            this.tbOtrasCircunstanciasGradoDep.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbOtrasCircunstanciasGradoDep.Location = new System.Drawing.Point(140, 148);
            this.tbOtrasCircunstanciasGradoDep.MaxLength = 255;
            this.tbOtrasCircunstanciasGradoDep.Multiline = true;
            this.tbOtrasCircunstanciasGradoDep.Name = "tbOtrasCircunstanciasGradoDep";
            this.tbOtrasCircunstanciasGradoDep.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbOtrasCircunstanciasGradoDep.Size = new System.Drawing.Size(397, 37);
            this.tbOtrasCircunstanciasGradoDep.TabIndex = 259;
            // 
            // _sdcGradoDep_0
            // 
            this._sdcGradoDep_0.CustomFormat = "dd/MM/yyyy";
            this._sdcGradoDep_0.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sdcGradoDep_0.Location = new System.Drawing.Point(188, 44);
            this._sdcGradoDep_0.Name = "_sdcGradoDep_0";
            this._sdcGradoDep_0.Size = new System.Drawing.Size(123, 20);
            this._sdcGradoDep_0.TabIndex = 252;
            this._sdcGradoDep_0.TabStop = false;
            this._sdcGradoDep_0.Text = "17/05/2016";
            this._sdcGradoDep_0.Value = new System.DateTime(2016, 5, 17, 16, 48, 49, 182);
            // 
            // _sdcGradoDep_1
            // 
            this._sdcGradoDep_1.CustomFormat = "dd/MM/yyyy";
            this._sdcGradoDep_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sdcGradoDep_1.Location = new System.Drawing.Point(364, 44);
            this._sdcGradoDep_1.Name = "_sdcGradoDep_1";
            this._sdcGradoDep_1.Size = new System.Drawing.Size(123, 20);
            this._sdcGradoDep_1.TabIndex = 253;
            this._sdcGradoDep_1.TabStop = false;
            this._sdcGradoDep_1.Text = "17/05/2016";
            this._sdcGradoDep_1.Value = new System.DateTime(2016, 5, 17, 16, 48, 49, 192);
            // 
            // _sdcGradoDep_2
            // 
            this._sdcGradoDep_2.CustomFormat = "dd/MM/yyyy";
            this._sdcGradoDep_2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sdcGradoDep_2.Location = new System.Drawing.Point(188, 76);
            this._sdcGradoDep_2.Name = "_sdcGradoDep_2";
            this._sdcGradoDep_2.Size = new System.Drawing.Size(123, 20);
            this._sdcGradoDep_2.TabIndex = 254;
            this._sdcGradoDep_2.TabStop = false;
            this._sdcGradoDep_2.Text = "17/05/2016";
            this._sdcGradoDep_2.Value = new System.DateTime(2016, 5, 17, 16, 48, 49, 212);
            // 
            // _sdcGradoDep_3
            // 
            this._sdcGradoDep_3.CustomFormat = "dd/MM/yyyy";
            this._sdcGradoDep_3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sdcGradoDep_3.Location = new System.Drawing.Point(364, 76);
            this._sdcGradoDep_3.Name = "_sdcGradoDep_3";
            this._sdcGradoDep_3.Size = new System.Drawing.Size(123, 20);
            this._sdcGradoDep_3.TabIndex = 255;
            this._sdcGradoDep_3.TabStop = false;
            this._sdcGradoDep_3.Text = "17/05/2016";
            this._sdcGradoDep_3.Value = new System.DateTime(2016, 5, 17, 16, 48, 49, 222);
            // 
            // _sdcGradoDep_4
            // 
            this._sdcGradoDep_4.CustomFormat = "dd/MM/yyyy";
            this._sdcGradoDep_4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sdcGradoDep_4.Location = new System.Drawing.Point(188, 108);
            this._sdcGradoDep_4.Name = "_sdcGradoDep_4";
            this._sdcGradoDep_4.Size = new System.Drawing.Size(123, 20);
            this._sdcGradoDep_4.TabIndex = 256;
            this._sdcGradoDep_4.TabStop = false;
            this._sdcGradoDep_4.Text = "17/05/2016";
            this._sdcGradoDep_4.Value = new System.DateTime(2016, 5, 17, 16, 48, 49, 232);
            // 
            // _sdcGradoDep_5
            // 
            this._sdcGradoDep_5.CustomFormat = "dd/MM/yyyy";
            this._sdcGradoDep_5.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._sdcGradoDep_5.Location = new System.Drawing.Point(364, 108);
            this._sdcGradoDep_5.MinDate = new System.DateTime(1800, 1, 1, 0, 0, 0, 0);
            this._sdcGradoDep_5.Name = "_sdcGradoDep_5";
            this._sdcGradoDep_5.Size = new System.Drawing.Size(123, 20);
            this._sdcGradoDep_5.TabIndex = 257;
            this._sdcGradoDep_5.TabStop = false;
            this._sdcGradoDep_5.Text = "17/05/2016";
            this._sdcGradoDep_5.Value = new System.DateTime(2016, 5, 17, 16, 48, 49, 262);
            // 
            // _lbDependencia_3
            // 
            this._lbDependencia_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_3.Location = new System.Drawing.Point(18, 148);
            this._lbDependencia_3.Name = "_lbDependencia_3";
            this._lbDependencia_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_3.Size = new System.Drawing.Size(158, 18);
            this._lbDependencia_3.TabIndex = 258;
            this._lbDependencia_3.Text = "Otras circunstancias de inter�s";
            // 
            // _lbDependencia_5
            // 
            this._lbDependencia_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_5.Location = new System.Drawing.Point(400, 20);
            this._lbDependencia_5.Name = "_lbDependencia_5";
            this._lbDependencia_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_5.Size = new System.Drawing.Size(41, 18);
            this._lbDependencia_5.TabIndex = 251;
            this._lbDependencia_5.Text = "Nivel II";
            // 
            // _lbDependencia_4
            // 
            this._lbDependencia_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_4.Location = new System.Drawing.Point(228, 20);
            this._lbDependencia_4.Name = "_lbDependencia_4";
            this._lbDependencia_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_4.Size = new System.Drawing.Size(38, 18);
            this._lbDependencia_4.TabIndex = 250;
            this._lbDependencia_4.Text = "Nivel I";
            // 
            // _lbDependencia_2
            // 
            this._lbDependencia_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_2.Location = new System.Drawing.Point(18, 115);
            this._lbDependencia_2.Name = "_lbDependencia_2";
            this._lbDependencia_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_2.Size = new System.Drawing.Size(52, 18);
            this._lbDependencia_2.TabIndex = 249;
            this._lbDependencia_2.Text = "Grado III:";
            // 
            // _lbDependencia_1
            // 
            this._lbDependencia_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_1.Location = new System.Drawing.Point(18, 81);
            this._lbDependencia_1.Name = "_lbDependencia_1";
            this._lbDependencia_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_1.Size = new System.Drawing.Size(49, 18);
            this._lbDependencia_1.TabIndex = 248;
            this._lbDependencia_1.Text = "Grado II:";
            // 
            // _lbDependencia_0
            // 
            this._lbDependencia_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._lbDependencia_0.Location = new System.Drawing.Point(18, 48);
            this._lbDependencia_0.Name = "_lbDependencia_0";
            this._lbDependencia_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._lbDependencia_0.Size = new System.Drawing.Size(46, 18);
            this._lbDependencia_0.TabIndex = 247;
            this._lbDependencia_0.Text = "Grado I:";
            // 
            // lbNombrePaciente
            // 
            this.lbNombrePaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbNombrePaciente.Enabled = false;
            this.lbNombrePaciente.Location = new System.Drawing.Point(108, 11);
            this.lbNombrePaciente.Name = "lbNombrePaciente";
            this.lbNombrePaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbNombrePaciente.Size = new System.Drawing.Size(559, 20);
            this.lbNombrePaciente.TabIndex = 281;
            // 
            // lbPaciente
            // 
            this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbPaciente.Location = new System.Drawing.Point(44, 12);
            this.lbPaciente.Name = "lbPaciente";
            this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbPaciente.Size = new System.Drawing.Size(51, 18);
            this.lbPaciente.TabIndex = 280;
            this.lbPaciente.Text = "Paciente:";
            // 
            // lbDesactivado
            // 
            this.lbDesactivado.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbDesactivado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDesactivado.Location = new System.Drawing.Point(242, 580);
            this.lbDesactivado.Name = "lbDesactivado";
            this.lbDesactivado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbDesactivado.Size = new System.Drawing.Size(147, 15);
            this.lbDesactivado.TabIndex = 230;
            this.lbDesactivado.Text = "PACIENTE DESACTIVADO";
            this.lbDesactivado.Visible = false;
            // 
            // lblTituloFechaUltMod
            // 
            this.lblTituloFechaUltMod.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTituloFechaUltMod.Location = new System.Drawing.Point(4, 577);
            this.lblTituloFechaUltMod.Name = "lblTituloFechaUltMod";
            this.lblTituloFechaUltMod.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTituloFechaUltMod.Size = new System.Drawing.Size(141, 18);
            this.lblTituloFechaUltMod.TabIndex = 219;
            this.lblTituloFechaUltMod.Text = "Fecha �ltima Modificaci�n:";
            this.lblTituloFechaUltMod.Visible = false;
            // 
            // DFI111F1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 605);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.cmdOpen);
            this.Controls.Add(this.tsDatosPac);
            this.Controls.Add(this.lbDesactivado);
            this.Controls.Add(this.lblFechaUltMod);
            this.Controls.Add(this.lblTituloFechaUltMod);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(15, 34);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI111F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Actualizaci�n de datos del paciente - DFI111F1";
            this.Activated += new System.EventHandler(this.DFI111F1_Activated);
            this.Closed += new System.EventHandler(this.DFI111F1_Closed);
            this.Load += new System.EventHandler(this.DFI111F1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DFI111F1_PreviewKeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaUltMod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOpen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsDatosPac)).EndInit();
            this.tsDatosPac.ResumeLayout(false);
            this._tsDatosPac_TabPage0.ResumeLayout(false);
            this._tsDatosPac_TabPage0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).EndInit();
            this.Frame12.ResumeLayout(false);
            this.Frame12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbBloqueo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBloqueo_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDPHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDPEFinanciadora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDPNAsegurado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPCatSoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNacPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbNacPais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbIdioma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDtroPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCanPrev.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCanPrev.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCanPrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCanPrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebDPCodPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmExitus)).EndInit();
            this.frmExitus.ResumeLayout(false);
            this.frmExitus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbDPExitus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcDPFechaFallecimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame18)).EndInit();
            this.Frame18.ResumeLayout(false);
            this.Frame18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPDireccionEx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPExPob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbDPExtrangero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPPais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPExtension1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPTelf1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmDatosPersonales)).EndInit();
            this.FrmDatosPersonales.ResumeLayout(false);
            this.FrmDatosPersonales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frmSexo)).EndInit();
            this.frmSexo.ResumeLayout(false);
            this.frmSexo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFiliacPov)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaNac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lblDatosPaciente_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbAutorizacion_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbAutorizacion_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTarjeta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            this.Frame6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPOtrosDomic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPNDomic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPDomicilioP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbDPVia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label54_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._Label54_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDBuscar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPExtension3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPPasaporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPTelf3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPTelf2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPExtension2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame13)).EndInit();
            this.Frame13.ResumeLayout(false);
            this.Frame13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkObservaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbObservaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmDNI_OtrosDocumentos)).EndInit();
            this.frmDNI_OtrosDocumentos.ResumeLayout(false);
            this.frmDNI_OtrosDocumentos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocDP_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocDP_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNIFOtrosDocDP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTarjetaEuropea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbSMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbCipAutonomico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbOrigInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
            this._tsDatosPac_TabPage1.ResumeLayout(false);
            this._tsDatosPac_TabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdConsultaActualizacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkValidarRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmPrivado)).EndInit();
            this.frmPrivado.ResumeLayout(false);
            this.frmPrivado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropioPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREPasaporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFacturacionP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeudas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbREDireccionEx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbREExtrangero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREExPob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREApe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREApe1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebRECodPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame8)).EndInit();
            this.Frame8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FrmCB21)).EndInit();
            this.FrmCB21.ResumeLayout(false);
            this.FrmCB21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBanco1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSucursal1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCuenta1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNcuenta21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
            this.Frame9.ResumeLayout(false);
            this.Frame9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame17)).EndInit();
            this.Frame17.ResumeLayout(false);
            this.Frame17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocRE_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocRE_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNIFOtrosDocRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmTitular)).EndInit();
            this.frmTitular.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frmSociedad)).EndInit();
            this.frmSociedad.ResumeLayout(false);
            this.frmSociedad.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TBBandaMagnetica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CbbObraSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExentoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmIndicador)).EndInit();
            this.frmIndicador.ResumeLayout(false);
            this.frmIndicador.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicador_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicador_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENomTitular)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRECobertura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscarSoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbREEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbRESociedad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebREFechaCaducidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebREFechaAlta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENPoliza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblObraSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmSegSocial)).EndInit();
            this.frmSegSocial.ResumeLayout(false);
            this.frmSegSocial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbHospitalReferencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbEmplazamiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame10)).EndInit();
            this.Frame10.ResumeLayout(false);
            this.Frame10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENomTitularSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicadorSS_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbIndicadorSS_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbPensionista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRENSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscarCIAS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MEBCIAS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscarCentro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbREInspeccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkZona)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmRERegimen)).EndInit();
            this.frmRERegimen.ResumeLayout(false);
            this.frmRERegimen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbRERegimen_0)).EndInit();
            this._tsDatosPac_TabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame16)).EndInit();
            this.Frame16.ResumeLayout(false);
            this.Frame16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocPC_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbDNIOtroDocPC_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNIFOtrosDocPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame15)).EndInit();
            this.Frame15.ResumeLayout(false);
            this.Frame15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbResponsable_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbResponsable_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame14)).EndInit();
            this.Frame14.ResumeLayout(false);
            this.Frame14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbTutor_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbTutor_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            this.Frame7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbPCExtrangero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCExPob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPCPais)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCApe2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCApe1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCTelf2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCTelf1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCExt2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCParentesco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPCAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPCCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbDPEliminar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPCExt1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mebPCCodPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbPCPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._LabelContacto_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame11)).EndInit();
            this.Frame11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sprContactos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprContactos)).EndInit();
            this._tsDatosPac_TabPage3.ResumeLayout(false);
            this._tsDatosPac_TabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraColectivos)).EndInit();
            this.fraColectivos.ResumeLayout(false);
            this.fraColectivos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNumTrabajador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbParentesco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._labelColectivos_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._labelColectivos_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._labelColectivos_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmGradoDependencia)).EndInit();
            this.frmGradoDependencia.ResumeLayout(false);
            this.frmGradoDependencia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOtrasCircunstanciasGradoDep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._sdcGradoDep_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lbDependencia_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbNombrePaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbDesactivado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTituloFechaUltMod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}        

        void ReLoadForm(bool addEvents)
		{
			InitializesdcGradoDep();
			InitializerbTutor();
			InitializerbSexo();
			InitializerbResponsable();
			InitializerbRERegimen();
			InitializerbIndicadorSS();
			InitializerbIndicador();
			InitializerbDNIOtroDocRE();
			InitializerbDNIOtroDocPC();
			InitializerbDNIOtroDocDP();
			InitializerbBloqueo();
			InitializerbAutorizacion();
			InitializelblDatosPaciente();
			InitializelbDependencia();
			InitializelabelColectivos();
			InitializeLabelContacto();
			InitializeLabel54();
            tsDatosPacPreviousTab = tsDatosPac.SelectedPage.Item.ZIndex;
		}
		void InitializesdcGradoDep()
		{
			this.sdcGradoDep = new Telerik.WinControls.UI.RadDateTimePicker[6];
			this.sdcGradoDep[0] = _sdcGradoDep_0;
			this.sdcGradoDep[1] = _sdcGradoDep_1;
			this.sdcGradoDep[2] = _sdcGradoDep_2;
			this.sdcGradoDep[3] = _sdcGradoDep_3;
			this.sdcGradoDep[4] = _sdcGradoDep_4;
			this.sdcGradoDep[5] = _sdcGradoDep_5;
		}
		void InitializerbTutor()
		{
			this.rbTutor = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbTutor[1] = _rbTutor_1;
			this.rbTutor[0] = _rbTutor_0;
		}
		void InitializerbSexo()
		{
			this.rbSexo = new Telerik.WinControls.UI.RadRadioButton[3];
			this.rbSexo[2] = _rbSexo_2;
			this.rbSexo[0] = _rbSexo_0;
			this.rbSexo[1] = _rbSexo_1;
		}
		void InitializerbResponsable()
		{
			this.rbResponsable = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbResponsable[1] = _rbResponsable_1;
			this.rbResponsable[0] = _rbResponsable_0;
		}
		void InitializerbRERegimen()
		{
			this.rbRERegimen = new Telerik.WinControls.UI.RadRadioButton[3];
			this.rbRERegimen[2] = _rbRERegimen_2;
			this.rbRERegimen[1] = _rbRERegimen_1;
			this.rbRERegimen[0] = _rbRERegimen_0;
		}
		void InitializerbIndicadorSS()
		{
			this.rbIndicadorSS = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbIndicadorSS[0] = _rbIndicadorSS_0;
			this.rbIndicadorSS[1] = _rbIndicadorSS_1;
		}
		void InitializerbIndicador()
		{
			this.rbIndicador = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbIndicador[1] = _rbIndicador_1;
			this.rbIndicador[0] = _rbIndicador_0;
		}
		void InitializerbDNIOtroDocRE()
		{
			this.rbDNIOtroDocRE = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbDNIOtroDocRE[1] = _rbDNIOtroDocRE_1;
			this.rbDNIOtroDocRE[0] = _rbDNIOtroDocRE_0;
		}
		void InitializerbDNIOtroDocPC()
		{
			this.rbDNIOtroDocPC = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbDNIOtroDocPC[0] = _rbDNIOtroDocPC_0;
			this.rbDNIOtroDocPC[1] = _rbDNIOtroDocPC_1;
		}
		void InitializerbDNIOtroDocDP()
		{
			this.rbDNIOtroDocDP = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbDNIOtroDocDP[1] = _rbDNIOtroDocDP_1;
			this.rbDNIOtroDocDP[0] = _rbDNIOtroDocDP_0;
		}
		void InitializerbBloqueo()
		{
			this.rbBloqueo = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbBloqueo[1] = _rbBloqueo_1;
			this.rbBloqueo[0] = _rbBloqueo_0;
		}
		void InitializerbAutorizacion()
		{
			this.rbAutorizacion = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbAutorizacion[1] = _rbAutorizacion_1;
			this.rbAutorizacion[0] = _rbAutorizacion_0;
		}
		void InitializelblDatosPaciente()
		{
			this.lblDatosPaciente = new Telerik.WinControls.UI.RadLabel[11];
			this.lblDatosPaciente[3] = _lblDatosPaciente_3;
			this.lblDatosPaciente[2] = _lblDatosPaciente_2;
			this.lblDatosPaciente[1] = _lblDatosPaciente_1;
			this.lblDatosPaciente[0] = _lblDatosPaciente_0;
			this.lblDatosPaciente[10] = _lblDatosPaciente_10;
			this.lblDatosPaciente[8] = _lblDatosPaciente_8;
			this.lblDatosPaciente[7] = _lblDatosPaciente_7;
			this.lblDatosPaciente[6] = _lblDatosPaciente_6;
			this.lblDatosPaciente[5] = _lblDatosPaciente_5;
			this.lblDatosPaciente[4] = _lblDatosPaciente_4;
		}
		void InitializelbDependencia()
		{
			this.lbDependencia = new Telerik.WinControls.UI.RadLabel[6];
			this.lbDependencia[3] = _lbDependencia_3;
			this.lbDependencia[5] = _lbDependencia_5;
			this.lbDependencia[4] = _lbDependencia_4;
			this.lbDependencia[2] = _lbDependencia_2;
			this.lbDependencia[1] = _lbDependencia_1;
			this.lbDependencia[0] = _lbDependencia_0;
		}
		void InitializelabelColectivos()
		{
			this.labelColectivos = new Telerik.WinControls.UI.RadLabel[3];
			this.labelColectivos[2] = _labelColectivos_2;
			this.labelColectivos[1] = _labelColectivos_1;
			this.labelColectivos[0] = _labelColectivos_0;
		}
		void InitializeLabelContacto()
		{
			this.LabelContacto = new Telerik.WinControls.UI.RadLabel[13];
			this.LabelContacto[10] = _LabelContacto_10;
			this.LabelContacto[11] = _LabelContacto_11;
			this.LabelContacto[12] = _LabelContacto_12;
			this.LabelContacto[4] = _LabelContacto_4;
			this.LabelContacto[5] = _LabelContacto_5;
			this.LabelContacto[9] = _LabelContacto_9;
			this.LabelContacto[8] = _LabelContacto_8;
			this.LabelContacto[7] = _LabelContacto_7;
			this.LabelContacto[6] = _LabelContacto_6;
			this.LabelContacto[3] = _LabelContacto_3;
			this.LabelContacto[2] = _LabelContacto_2;
			this.LabelContacto[1] = _LabelContacto_1;
			this.LabelContacto[0] = _LabelContacto_0;
		}
		void InitializeLabel54()
		{
			this.Label54 = new Telerik.WinControls.UI.RadLabel[2];
			this.Label54[1] = _Label54_1;
			this.Label54[0] = _Label54_0;
		}
        #endregion        
    }
}