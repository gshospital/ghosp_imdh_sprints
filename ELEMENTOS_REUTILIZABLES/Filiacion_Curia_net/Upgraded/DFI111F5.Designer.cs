
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace filiacionE
{
	partial class DFI111F5
	{

		#region "Upgrade Support "
		private static DFI111F5 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI111F5 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI111F5();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprSociedades", "cmdActivar", "cmdDesactivar", "cmdSeleccionar", "cmdSalir", "Label2", "shpDesactivado", "shpActivado", "Label4", "lbTituloPaciente", "lbPaciente", "Frame1", "ShapeContainer1", "sprSociedades_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprSociedades;
		public Telerik.WinControls.UI.RadButton cmdActivar;
		public Telerik.WinControls.UI.RadButton cmdDesactivar;
		public Telerik.WinControls.UI.RadButton cmdSeleccionar;
		public Telerik.WinControls.UI.RadButton cmdSalir;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Microsoft.VisualBasic.PowerPacks.LineShape shpDesactivado;
		public Microsoft.VisualBasic.PowerPacks.LineShape shpActivado;       
        public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel lbTituloPaciente;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
        //public Telerik.WinControls.UI.RadGroupBox Frame1;
        public System.Windows.Forms.Panel Frame1;
        public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		//private FarPoint.Win.Spread.SheetView sprSociedades_Sheet1 = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFI111F5));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			//this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Frame1 = new System.Windows.Forms.Panel();
            this.sprSociedades = new UpgradeHelpers.Spread.FpSpread();
			this.cmdActivar = new Telerik.WinControls.UI.RadButton();
			this.cmdDesactivar = new Telerik.WinControls.UI.RadButton();
			this.cmdSeleccionar = new Telerik.WinControls.UI.RadButton();
			this.cmdSalir = new Telerik.WinControls.UI.RadButton();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.shpDesactivado = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.shpActivado = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.lbTituloPaciente = new Telerik.WinControls.UI.RadLabel();
			this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Frame1.SuspendLayout();
            this.SuspendLayout();

            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            // 
            // ShapeContainer1
            // 

            //this.ShapeContainer1.Size = new System.Drawing.Size(269, 33);
            this.ShapeContainer1.Location = new System.Drawing.Point(3, 16);
            //this.ShapeContainer1.Location = new System.Drawing.Point(70, 312);
            this.ShapeContainer1.Size = new System.Drawing.Size(737, 355);
            this.ShapeContainer1.Shapes.Add(shpDesactivado);
            this.ShapeContainer1.Shapes.Add(shpActivado);

            //sprSociedades 
            gridViewTextBoxColumn1.HeaderText = "Sociedad / Inspecci�n";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 250;

            //gridViewTextBoxColumn2.HeaderText = "Sociedad / Inspecci�n";
            //gridViewTextBoxColumn2.Name = "column2";
            //gridViewTextBoxColumn2.Width = 80;

            gridViewTextBoxColumn2.HeaderText = "N� Afiliaci�n";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 100;

            gridViewTextBoxColumn3.HeaderText = "Titular/Benef.";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 100;

            gridViewTextBoxColumn4.HeaderText = "Cobertura";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 100;

            gridViewTextBoxColumn5.HeaderText = "F. Inicio";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 100;

            gridViewTextBoxColumn6.HeaderText = "F. Fin";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 100;

            gridViewTextBoxColumn7.HeaderText = "F. Borrado";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.Width = 100;

            gridViewTextBoxColumn8.HeaderText = "Cod. Sociedad";
            gridViewTextBoxColumn8.Name = "column8";
            gridViewTextBoxColumn8.Width = 100;

            gridViewTextBoxColumn9.HeaderText = "Cod. Inspecci�n";
            gridViewTextBoxColumn9.Name = "column9";
            gridViewTextBoxColumn9.Width = 100;

            gridViewTextBoxColumn10.HeaderText = "Exec. IVA";
            gridViewTextBoxColumn10.Name = "column10";
            gridViewTextBoxColumn10.Width = 100;
            gridViewTextBoxColumn10.IsVisible = false; 

            this.sprSociedades.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[]
            {   
                            gridViewTextBoxColumn1,
                            gridViewTextBoxColumn2,
                            gridViewTextBoxColumn3,
                            gridViewTextBoxColumn4,
                            gridViewTextBoxColumn5,
                            gridViewTextBoxColumn6,
                            gridViewTextBoxColumn7,
                            gridViewTextBoxColumn8,
                            gridViewTextBoxColumn9,
                            gridViewTextBoxColumn10
        });
            // 
            // /*Frame1*/
            // 
            //this.Frame1.BackColor = System.Drawing.SystemColors.Control;
            this.Frame1.Controls.Add(this.sprSociedades);
			this.Frame1.Controls.Add(this.cmdActivar);
			this.Frame1.Controls.Add(this.cmdDesactivar);
			this.Frame1.Controls.Add(this.cmdSeleccionar);
			this.Frame1.Controls.Add(this.cmdSalir);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label4);
			this.Frame1.Controls.Add(this.lbTituloPaciente);
			this.Frame1.Controls.Add(this.lbPaciente);
			this.Frame1.Enabled = true;
			//this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Frame1.Location = new System.Drawing.Point(0, 6);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(737, 355);
			this.Frame1.TabIndex = 0;
			this.Frame1.Visible = true;

			// 
			// sprSociedades
			// 
			this.sprSociedades.Location = new System.Drawing.Point(7, 40);
			this.sprSociedades.Name = "sprSociedades";
			this.sprSociedades.Size = new System.Drawing.Size(724, 265);
			this.sprSociedades.TabIndex = 1;
			this.sprSociedades.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprSociedades_CellClick);
			this.sprSociedades.KeyDown += new System.Windows.Forms.KeyEventHandler(this.sprSociedades_KeyDown);
            this.sprSociedades.AutoScroll = true;
            this.sprSociedades.AutoSizeRows = true;
            this.sprSociedades.AllowColumnResize = true;
            this.sprSociedades.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.FullRowSelect;
            // 
            // cmdActivar
            // 
            //this.cmdActivar.BackColor = System.Drawing.SystemColors.Control;
            this.cmdActivar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cmdActivar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdActivar.Location = new System.Drawing.Point(386, 314);
			this.cmdActivar.Name = "cmdActivar";
			this.cmdActivar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdActivar.Size = new System.Drawing.Size(81, 31);
			this.cmdActivar.TabIndex = 2;
			this.cmdActivar.Text = "&Activar";
			this.cmdActivar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cmdActivar.UseVisualStyleBackColor = false;
			this.cmdActivar.Click += new System.EventHandler(this.cmdActivar_Click);
            this.cmdActivar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cmdDesactivar
			// 
			//this.cmdDesactivar.BackColor = System.Drawing.SystemColors.Control;
			this.cmdDesactivar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cmdDesactivar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdDesactivar.Location = new System.Drawing.Point(474, 314);
			this.cmdDesactivar.Name = "cmdDesactivar";
			this.cmdDesactivar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdDesactivar.Size = new System.Drawing.Size(81, 31);
			this.cmdDesactivar.TabIndex = 3;
			this.cmdDesactivar.Text = "&Desactivar";
			this.cmdDesactivar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cmdDesactivar.UseVisualStyleBackColor = false;
			this.cmdDesactivar.Click += new System.EventHandler(this.cmdDesactivar_Click);
            this.cmdDesactivar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdSeleccionar
            // 
            //this.cmdSeleccionar.BackColor = System.Drawing.SystemColors.Control;
            this.cmdSeleccionar.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cmdSeleccionar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdSeleccionar.Location = new System.Drawing.Point(562, 314);
			this.cmdSeleccionar.Name = "cmdSeleccionar";
			this.cmdSeleccionar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdSeleccionar.Size = new System.Drawing.Size(81, 31);
			this.cmdSeleccionar.TabIndex = 4;
			this.cmdSeleccionar.Text = "S&eleccionar";
			this.cmdSeleccionar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cmdSeleccionar.UseVisualStyleBackColor = false;
			this.cmdSeleccionar.Click += new System.EventHandler(this.cmdSeleccionar_Click);
            this.cmdSeleccionar.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdSalir
            // 
            //this.cmdSalir.BackColor = System.Drawing.SystemColors.Control;
            this.cmdSalir.Cursor = System.Windows.Forms.Cursors.Default;
			//this.cmdSalir.ForeColor = System.Drawing.SystemColors.ControlText;
			this.cmdSalir.Location = new System.Drawing.Point(650, 314);
			this.cmdSalir.Name = "cmdSalir";
			this.cmdSalir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cmdSalir.Size = new System.Drawing.Size(81, 31);
			this.cmdSalir.TabIndex = 5;
			this.cmdSalir.Text = "&Salir";
			this.cmdSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			//this.cmdSalir.UseVisualStyleBackColor = false;
			this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            this.cmdSalir.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label2
            // 
            //this.Label2.BackColor = System.Drawing.SystemColors.Control;
            //this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label2.Location = new System.Drawing.Point(70, 312);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(50, 17);
			this.Label2.TabIndex = 9;
			this.Label2.Text = "Activada";
            // 
            // shpDesactivado
            // 
            //this.shpDesactivado.BackColor = System.Drawing.SystemColors.Window;
            //this.shpDesactivado.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Transparent;
            //this.shpDesactivado.BorderColor = System.Drawing.SystemColors.WindowText;
            //this.shpDesactivado.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            //this.shpDesactivado.BorderWidth = 1;
            //this.shpDesactivado.Enabled = false;
            //this.shpDesactivado.FillColor = System.Drawing.Color.Gray;
            //this.shpDesactivado.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            //this.shpDesactivado.Location = new System.Drawing.Point(8, 316);
            //this.shpDesactivado.Name = "shpDesactivado";
            //this.shpDesactivado.Size = new System.Drawing.Size(43, 7);
            //this.shpDesactivado.Visible = true;

            this.shpDesactivado.SelectionColor = System.Drawing.Color.Gray;
            this.shpDesactivado.BorderColor = System.Drawing.Color.Gray;
            this.shpDesactivado.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.shpDesactivado.BorderWidth = 7;
            this.shpDesactivado.Enabled = false;
            this.shpDesactivado.Name = "shpDesactivado";
            this.shpDesactivado.Visible = true;
            this.shpDesactivado.X1 = (int)100;
            this.shpDesactivado.X2 = (int)80;
            this.shpDesactivado.Y1 = (int)0;
            this.shpDesactivado.Y2 = (int)337;
            this.shpDesactivado.StartPoint = new System.Drawing.Point(8, 337);

            // 
            // shpActivado
            // 
            //this.shpActivado.BackColor = System.Drawing.SystemColors.Window;
            //         this.shpActivado.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Transparent;
            //this.shpActivado.BorderColor = System.Drawing.SystemColors.WindowText;
            //this.shpActivado.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            //this.shpActivado.BorderWidth = 1;
            //this.shpActivado.Enabled = false;
            //this.shpActivado.FillColor = System.Drawing.Color.Blue;
            //this.shpActivado.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            //this.shpActivado.Location = new System.Drawing.Point(8, 302);
            //         this.shpActivado.Name = "shpActivado";
            //this.shpActivado.Size = new System.Drawing.Size(43, 7);
            //this.shpActivado.Visible = true;

            this.shpActivado.BorderColor = System.Drawing.Color.Blue;
            this.shpActivado.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.shpActivado.BorderWidth = 7;
            this.shpActivado.Enabled = false;
            this.shpActivado.Name = "shpActivado";
            this.shpActivado.Visible = true;
            this.shpActivado.X1 = (int)100;
            this.shpActivado.X2 = (int)80;
            this.shpActivado.Y1 = (int)0;
            this.shpActivado.Y2 = (int)320;
            this.shpActivado.StartPoint = new System.Drawing.Point(8, 320);

            // 
            // Label4
            // 
            //this.Label4.BackColor = System.Drawing.SystemColors.Control;
            //this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			//this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label4.Location = new System.Drawing.Point(70, 329);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(78, 17);
			this.Label4.TabIndex = 8;
			this.Label4.Text = "Desactivada";
			// 
			// lbTituloPaciente
			// 
			//this.lbTituloPaciente.BackColor = System.Drawing.SystemColors.Control;
			//this.lbTituloPaciente.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lbTituloPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbTituloPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbTituloPaciente.Location = new System.Drawing.Point(8, 16);
			this.lbTituloPaciente.Name = "lbTituloPaciente";
			this.lbTituloPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbTituloPaciente.Size = new System.Drawing.Size(57, 15);
			this.lbTituloPaciente.TabIndex = 7;
			this.lbTituloPaciente.Text = "PACIENTE:";
			// 
			// lbPaciente
			// 
			//this.lbPaciente.BackColor = System.Drawing.SystemColors.Control;
			//this.lbPaciente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			//this.lbPaciente.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbPaciente.Location = new System.Drawing.Point(80, 14);
			this.lbPaciente.Name = "lbPaciente";
			this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbPaciente.Size = new System.Drawing.Size(377, 17);
			this.lbPaciente.TabIndex = 6;
            this.lbPaciente.Enabled = false;
			// 
			// DFI111F5
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(740, 364);
			this.Controls.Add(this.Frame1);
			this.Frame1.Controls.Add(ShapeContainer1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DFI111F5";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Consulta/Actualizaci�n de Entidades Aseguradoras - DFI111F5";
			this.Closed += new System.EventHandler(this.DFI111F5_Closed);
			this.Load += new System.EventHandler(this.DFI111F5_Load);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}