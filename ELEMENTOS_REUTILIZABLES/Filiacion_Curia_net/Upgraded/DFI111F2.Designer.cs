using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace filiacionE
{
    partial class DFI111F2
    {

        #region "Upgrade Support "
        private static DFI111F2 m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static DFI111F2 DefInstance
        {
            get
            {
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new DFI111F2();
                    m_InitializingDefInstance = false;
                }
                return m_vb6FormDefInstance;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }

        #endregion
        #region "Windows Form Designer generated code "
        private string[] visualControls = new string[] { "components", "ToolTipMain", "cbbSociedad", "cbbEmpresa", "Label1", "Label12", "Frame1", "cbCancelar", "cbAceptar" };
        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTipMain;        
        public Telerik.WinControls.UI.RadDropDownList cbbSociedad;
        public Telerik.WinControls.UI.RadDropDownList cbbEmpresa;
        public Telerik.WinControls.UI.RadLabel Label1;
        public Telerik.WinControls.UI.RadLabel Label12;
        public Telerik.WinControls.UI.RadGroupBox Frame1;
        public Telerik.WinControls.UI.RadButton cbCancelar;
        public Telerik.WinControls.UI.RadButton cbAceptar;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFI111F2));
            this.ToolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbbSociedad = new Telerik.WinControls.UI.RadDropDownList();
            this.cbbEmpresa = new Telerik.WinControls.UI.RadDropDownList();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.Frame1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Frame1
            // 			
            this.Frame1.Controls.Add(this.cbbSociedad);
            this.Frame1.Controls.Add(this.cbbEmpresa);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.Enabled = true;
            this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame1.Location = new System.Drawing.Point(8, 8);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(473, 105);
            this.Frame1.TabIndex = 2;
            this.Frame1.Text = "B�squeda de sociedad";
            this.Frame1.Visible = true;
            // 
            // cbbSociedad
            // 			
            this.cbbSociedad.CausesValidation = true;
            this.cbbSociedad.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbSociedad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbSociedad.Enabled = true;
            this.cbbSociedad.Location = new System.Drawing.Point(64, 62);
            this.cbbSociedad.Name = "cbbSociedad";
            this.cbbSociedad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbSociedad.Size = new System.Drawing.Size(405, 21);
            this.cbbSociedad.TabIndex = 6;
            this.cbbSociedad.TabStop = true;
            this.cbbSociedad.Visible = true;
            // 
            // cbbEmpresa
            // 			
            this.cbbEmpresa.CausesValidation = true;
            this.cbbEmpresa.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbEmpresa.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            this.cbbEmpresa.Enabled = true;
            this.cbbEmpresa.Location = new System.Drawing.Point(64, 24);
            this.cbbEmpresa.Name = "cbbEmpresa";
            this.cbbEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbEmpresa.Size = new System.Drawing.Size(405, 21);
            this.cbbEmpresa.TabIndex = 4;
            this.cbbEmpresa.TabStop = true;
            this.cbbEmpresa.Text = "cbbEmpresa";
            this.cbbEmpresa.Visible = true;
            this.cbbEmpresa.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbEmpresa_SelectionChangeCommitted);
            // 
            // Label1
            //			
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(8, 28);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(49, 17);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "Empresa:";
            // 
            // Label12
            // 			
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label12.Location = new System.Drawing.Point(8, 64);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(49, 17);
            this.Label12.TabIndex = 3;
            this.Label12.Text = "Sociedad:";
            // 
            // cbCancelar
            // 			
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(402, 116);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 1;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 			
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(314, 116);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 0;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // DFI111F2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 152);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(84, 139);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI111F2";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "B�squeda de relaci�n sociedad-empresa  (DFI111F2)";
            this.Closed += new System.EventHandler(this.DFI111F2_Closed);
            this.Load += new System.EventHandler(this.DFI111F2_Load);
            this.Frame1.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        #endregion
    }
}