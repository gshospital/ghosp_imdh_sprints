using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace filiacionE
{
	partial class DFI111F3
	{

		#region "Upgrade Support "
		private static DFI111F3 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI111F3 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI111F3();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbbCentro", "cbbmedico_centro", "Label1", "Label12", "Frame1", "cbCancelar", "cbAceptar", "Label4", "Label3", "lblCIAS", "Label2", "sprSociedades_Sheet1", "sprDuplicados_Sheet1", "sprContactos_Sheet1", "sprFiliados_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.MSForms.MSCombobox cbbCentro;
		public UpgradeHelpers.MSForms.MSCombobox cbbmedico_centro;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label12;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadTextBox Label4;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadTextBox lblCIAS;
		public Telerik.WinControls.UI.RadLabel Label2;

		
            //NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
            this.cbbCentro = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.cbbmedico_centro = new UpgradeHelpers.MSForms.MSCombobox(this.components);
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Label12 = new Telerik.WinControls.UI.RadLabel();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.Label4 = new Telerik.WinControls.UI.RadTextBox();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.lblCIAS = new Telerik.WinControls.UI.RadTextBox();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCentro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbmedico_centro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCIAS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Frame1.Controls.Add(this.cbbCentro);
            this.Frame1.Controls.Add(this.cbbmedico_centro);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.HeaderText = "B�squeda de c�digo";
            this.Frame1.Location = new System.Drawing.Point(8, 8);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(473, 105);
            this.Frame1.TabIndex = 4;
            this.Frame1.Text = "B�squeda de c�digo";
            // 
            // cbbCentro
            // 
            this.cbbCentro.ColumnWidths = "";
            this.cbbCentro.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbCentro.Location = new System.Drawing.Point(56, 24);
            this.cbbCentro.Name = "cbbCentro";
            this.cbbCentro.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbCentro.Size = new System.Drawing.Size(405, 20);
            this.cbbCentro.TabIndex = 6;
            this.cbbCentro.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbCentro_SelectedIndexChanged);
            this.cbbCentro.TextChanged += new System.EventHandler(this.cbbCentro_TextChanged);
            // 
            // cbbmedico_centro
            // 
            this.cbbmedico_centro.ColumnWidths = "";
            this.cbbmedico_centro.Location = new System.Drawing.Point(56, 64);
            this.cbbmedico_centro.Name = "cbbmedico_centro";
            this.cbbmedico_centro.Size = new System.Drawing.Size(405, 20);
            this.cbbmedico_centro.TabIndex = 8;
            this.cbbmedico_centro.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbbmedico_centro_SelectedIndexChanged);


            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(8, 24);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(42, 18);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "Centro:";
            // 
            // Label12
            // 
            this.Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label12.Location = new System.Drawing.Point(8, 64);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(46, 18);
            this.Label12.TabIndex = 5;
            this.Label12.Text = "M�dico:";
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(404, 160);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 1;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Enabled = false;
            this.cbAceptar.Location = new System.Drawing.Point(316, 160);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 0;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Enabled = false;
            this.Label4.Location = new System.Drawing.Point(8, 172);
            this.Label4.Name = "Label4";
            this.Label4.ReadOnly = true;
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(297, 20);
            this.Label4.TabIndex = 10;
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(8, 152);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(121, 18);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "Hospital de Referencia:";
            // 
            // lblCIAS
            // 
            this.lblCIAS.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCIAS.Enabled = false;
            this.lblCIAS.Location = new System.Drawing.Point(80, 124);
            this.lblCIAS.Name = "lblCIAS";
            this.lblCIAS.ReadOnly = true;
            this.lblCIAS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCIAS.Size = new System.Drawing.Size(97, 20);
            this.lblCIAS.TabIndex = 3;
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(8, 124);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(71, 18);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "C�digo CIAS:";
            // 
            // DFI111F3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 199);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.lblCIAS);
            this.Controls.Add(this.Label2);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(84, 139);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI111F3";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "B�squeda de c�digo CIAS - DFI111F3";
            this.Closed += new System.EventHandler(this.DFI111F3_Closed);
            this.Load += new System.EventHandler(this.DFI111F3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbCentro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbmedico_centro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCIAS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
	}
}