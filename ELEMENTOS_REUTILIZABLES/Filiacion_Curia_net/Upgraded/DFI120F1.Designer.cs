using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace filiacionE
{
	partial class DFI120F1
	{

		#region "Upgrade Support "
		private static DFI120F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static DFI120F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new DFI120F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprFiliados", "cbNuevo", "cbCancelar", "cbAceptar", "CBPacienteAnt", "cmdOpen", "tbCorreoElectronico", "_rbBuscar_7", "_rbBuscar_6", "tbCIPAutonomico", "TbVersion", "tbsocieda", "tbOtrosDocumentos", "_rbBuscar_5", "tbTarjeta", "_rbBuscar_4", "tbDPTelf1", "_rbBuscar_3", "tbNHistoria", "_rbBuscar_2", "_rbBuscar_1", "_rbBuscar_0", "tbNSS", "cbbArchivo", "MEBDNI", "Label6", "LbSociedad", "Label5", "FrmBuscar", "sdcFechaNac", "chbFiliacPov", "tbApellido2", "tbApellido1", "tbNombre", "_rbSexo_1", "_rbSexo_0", "frmSexo", "Label4", "lbApellido1", "Label2", "Label3", "FrmDatosPersonales", "cbBuscar", "Line1", "Label1", "rbBuscar", "rbSexo", "ShapeContainer1", "commandButtonHelper1", "sprFiliados_Sheet1", "Frame1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
        public UpgradeHelpers.Spread.FpSpread sprFiliados;
        public Telerik.WinControls.UI.RadButton cbNuevo;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton CBPacienteAnt;
		public Telerik.WinControls.UI.RadButton cmdOpen;
		public Telerik.WinControls.UI.RadTextBoxControl tbCorreoElectronico;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_7;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_6;
		public Telerik.WinControls.UI.RadTextBoxControl tbCIPAutonomico;
		public Telerik.WinControls.UI.RadTextBoxControl TbVersion;
		public Telerik.WinControls.UI.RadTextBoxControl tbsocieda;
		public Telerik.WinControls.UI.RadTextBoxControl tbOtrosDocumentos;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_5;
		public Telerik.WinControls.UI.RadTextBoxControl tbTarjeta;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_4;
		public Telerik.WinControls.UI.RadTextBoxControl tbDPTelf1;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_3;
		public Telerik.WinControls.UI.RadTextBoxControl tbNHistoria;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_2;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_1;
		private Telerik.WinControls.UI.RadRadioButton _rbBuscar_0;
		public Telerik.WinControls.UI.RadTextBoxControl tbNSS;
		public Telerik.WinControls.UI.RadDropDownList cbbArchivo;
		public Telerik.WinControls.UI.RadMaskedEditBox MEBDNI;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel LbSociedad;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadGroupBox FrmBuscar;
		public Telerik.WinControls.UI.RadDateTimePicker sdcFechaNac;
		public Telerik.WinControls.UI.RadCheckBox chbFiliacPov;
		public Telerik.WinControls.UI.RadTextBoxControl tbApellido2;
		public Telerik.WinControls.UI.RadTextBoxControl tbApellido1;
		public Telerik.WinControls.UI.RadTextBoxControl tbNombre;
		private Telerik.WinControls.UI.RadRadioButton _rbSexo_1;
		private Telerik.WinControls.UI.RadRadioButton _rbSexo_0;
		public Telerik.WinControls.UI.RadGroupBox frmSexo;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel lbApellido1;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadGroupBox FrmDatosPersonales;
		public Telerik.WinControls.UI.RadButton cbBuscar;
        public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
		public Telerik.WinControls.UI.RadLabel Label1;
        public System.Windows.Forms.Panel Frame1;
        public Telerik.WinControls.UI.RadRadioButton[] rbBuscar = new Telerik.WinControls.UI.RadRadioButton[8];
		public Telerik.WinControls.UI.RadRadioButton[] rbSexo = new Telerik.WinControls.UI.RadRadioButton[2];
        public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		private UpgradeHelpers.Gui.CommandButtonHelper commandButtonHelper1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DFI120F1));
            this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
            this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.sprFiliados = new UpgradeHelpers.Spread.FpSpread();
            this.cbNuevo = new Telerik.WinControls.UI.RadButton();
            this.cbCancelar = new Telerik.WinControls.UI.RadButton();
            this.cbAceptar = new Telerik.WinControls.UI.RadButton();
            this.CBPacienteAnt = new Telerik.WinControls.UI.RadButton();
            this.cmdOpen = new Telerik.WinControls.UI.RadButton();
            this.FrmBuscar = new Telerik.WinControls.UI.RadGroupBox();
            this.tbCorreoElectronico = new Telerik.WinControls.UI.RadTextBoxControl();
            this._rbBuscar_7 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbBuscar_6 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbCIPAutonomico = new Telerik.WinControls.UI.RadTextBoxControl();
            this.TbVersion = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbsocieda = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbOtrosDocumentos = new Telerik.WinControls.UI.RadTextBoxControl();
            this._rbBuscar_5 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbTarjeta = new Telerik.WinControls.UI.RadTextBoxControl();
            this._rbBuscar_4 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbDPTelf1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this._rbBuscar_3 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbNHistoria = new Telerik.WinControls.UI.RadTextBoxControl();
            this._rbBuscar_2 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbBuscar_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbBuscar_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.tbNSS = new Telerik.WinControls.UI.RadTextBoxControl();
            this.cbbArchivo = new Telerik.WinControls.UI.RadDropDownList();
            this.MEBDNI = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Label6 = new Telerik.WinControls.UI.RadLabel();
            this.LbSociedad = new Telerik.WinControls.UI.RadLabel();
            this.Label5 = new Telerik.WinControls.UI.RadLabel();
            this.FrmDatosPersonales = new Telerik.WinControls.UI.RadGroupBox();
            this.sdcFechaNac = new Telerik.WinControls.UI.RadDateTimePicker();
            this.chbFiliacPov = new Telerik.WinControls.UI.RadCheckBox();
            this.tbApellido2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbApellido1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.tbNombre = new Telerik.WinControls.UI.RadTextBoxControl();
            this.frmSexo = new Telerik.WinControls.UI.RadGroupBox();
            this._rbSexo_1 = new Telerik.WinControls.UI.RadRadioButton();
            this._rbSexo_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.Label4 = new Telerik.WinControls.UI.RadLabel();
            this.lbApellido1 = new Telerik.WinControls.UI.RadLabel();
            this.Label2 = new Telerik.WinControls.UI.RadLabel();
            this.Label3 = new Telerik.WinControls.UI.RadLabel();
            this.cbBuscar = new Telerik.WinControls.UI.RadButton();
            this.Label1 = new Telerik.WinControls.UI.RadLabel();
            this.Frame1 = new System.Windows.Forms.Panel();
            this.commandButtonHelper1 = new UpgradeHelpers.Gui.CommandButtonHelper(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.sprFiliados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprFiliados.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNuevo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBPacienteAnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOpen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmBuscar)).BeginInit();
            this.FrmBuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorreoElectronico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCIPAutonomico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbsocieda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOtrosDocumentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTarjeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPTelf1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNHistoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbArchivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MEBDNI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbSociedad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmDatosPersonales)).BeginInit();
            this.FrmDatosPersonales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaNac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFiliacPov)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmSexo)).BeginInit();
            this.frmSexo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbApellido1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            this.Frame1.SuspendLayout();

            this.Frame1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Frame1.Enabled = true;
            this.Frame1.Location = new System.Drawing.Point(0, 525);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(779, 573);
            this.Frame1.TabIndex = 0;
            this.Frame1.Visible = true;

            // 
            // ShapeContainer1
            // 
            this.ShapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.ShapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.ShapeContainer1.Name = "ShapeContainer1";
            //this.ShapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            //this.Line1});
            this.ShapeContainer1.Size = new System.Drawing.Size(779, 573);
            this.ShapeContainer1.TabIndex = 44;
            this.ShapeContainer1.TabStop = false;
            this.ShapeContainer1.Shapes.Add(Line1);
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.Color.Red;
            this.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.Line1.BorderWidth = 3;
            this.Line1.Enabled = false;
            this.Line1.Name = "Line1";
            this.Line1.X1 = 84;
            this.Line1.X2 = 156;
            this.Line1.Y1 = 0;
            this.Line1.Y2 = 0;
            this.Line1.Visible = true;
            // 
            // sprFiliados
            // 
            this.sprFiliados.Location = new System.Drawing.Point(4, 308);
            // 
            // 
            // 
            gridViewTextBoxColumn1.HeaderText = "Hist. cl�nica";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 110;

            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 200;

            gridViewTextBoxColumn3.HeaderText = "F. Nacimiento";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 80;

            gridViewTextBoxColumn4.HeaderText = "Sexo";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 60;

            gridViewTextBoxColumn5.HeaderText = "D.N.I/N.I.F";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 80;

            gridViewTextBoxColumn6.HeaderText = "Ent. Financiadora";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 150;

            gridViewTextBoxColumn7.HeaderText = "Domicilio";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.Width = 170;

            gridViewTextBoxColumn8.HeaderText = "C�d. Postal";
            gridViewTextBoxColumn8.Name = "column8";
            gridViewTextBoxColumn8.Width = 80;

            gridViewTextBoxColumn9.HeaderText = "Poblaci�n";
            gridViewTextBoxColumn9.Name = "column9";
            gridViewTextBoxColumn9.Width = 130;

            gridViewTextBoxColumn10.HeaderText = "Paciente";
            gridViewTextBoxColumn10.Name = "column10";
            gridViewTextBoxColumn10.IsVisible = false;
            gridViewTextBoxColumn10.Width = 100;

            gridViewTextBoxColumn11.HeaderText = "idPaciente";
            gridViewTextBoxColumn11.Name = "column11";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Width = 100;


            this.sprFiliados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[]
           {   gridViewTextBoxColumn1,
                gridViewTextBoxColumn2,
                gridViewTextBoxColumn3,
                gridViewTextBoxColumn4,
                gridViewTextBoxColumn5,
                gridViewTextBoxColumn6,
                gridViewTextBoxColumn7,
                gridViewTextBoxColumn8,
                gridViewTextBoxColumn9,
                gridViewTextBoxColumn10,
                gridViewTextBoxColumn11
           });
            this.sprFiliados.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprFiliados.MasterTemplate.EnableSorting = false;
            this.sprFiliados.MasterTemplate.ReadOnly = true;

            this.sprFiliados.Name = "sprFiliados";
            this.sprFiliados.Size = new System.Drawing.Size(768, 207);
            this.sprFiliados.TabIndex = 27;
            this.sprFiliados.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprFiliados_CellClick);
            this.sprFiliados.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprFiliados_CellDoubleClick);
            this.sprFiliados.GotFocus += new System.EventHandler(this.sprFiliados_GotFocus);
            this.sprFiliados.KeyUp += new System.Windows.Forms.KeyEventHandler(this.sprFiliados_KeyUp);
            this.sprFiliados.MouseUp += new System.Windows.Forms.MouseEventHandler(this.sprFiliados_MouseUp);
            this.sprFiliados.MouseDown += new System.Windows.Forms.MouseEventHandler(this.sprFiliados_MouseDown);
            this.sprFiliados.TopLeftChange += new UpgradeHelpers.Spread.TopLeftChangeEventHandler(sprFiliados_TopLeftChange);
            // 
            // cbNuevo
            // 
            this.cbNuevo.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbNuevo.Enabled = false;
            this.cbNuevo.Location = new System.Drawing.Point(690, 533);
            this.cbNuevo.Name = "cbNuevo";
            this.cbNuevo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbNuevo.Size = new System.Drawing.Size(81, 32);
            this.cbNuevo.TabIndex = 31;
            this.cbNuevo.Text = "&Nuevo";
            this.cbNuevo.Click += new System.EventHandler(this.cbNuevo_Click);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCancelar.Location = new System.Drawing.Point(604, 533);
            this.cbCancelar.Name = "cbCancelar";
            this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbCancelar.Size = new System.Drawing.Size(81, 32);
            this.cbCancelar.TabIndex = 30;
            this.cbCancelar.Text = "&Cancelar";
            this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbAceptar.Location = new System.Drawing.Point(516, 533);
            this.cbAceptar.Name = "cbAceptar";
            this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbAceptar.Size = new System.Drawing.Size(81, 32);
            this.cbAceptar.TabIndex = 29;
            this.cbAceptar.Text = "&Aceptar";
            this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
            // 
            // CBPacienteAnt
            // 
            this.CBPacienteAnt.Cursor = System.Windows.Forms.Cursors.Default;
            this.CBPacienteAnt.Location = new System.Drawing.Point(428, 533);
            this.CBPacienteAnt.Name = "CBPacienteAnt";
            this.CBPacienteAnt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CBPacienteAnt.Size = new System.Drawing.Size(81, 32);
            this.CBPacienteAnt.TabIndex = 28;
            this.CBPacienteAnt.Text = "�ltimo Paciente";
            this.CBPacienteAnt.TextWrap = true;
            this.CBPacienteAnt.Click += new System.EventHandler(this.CBPacienteAnt_Click);
            // 
            // cmdOpen
            // 
            this.cmdOpen.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdOpen.Image = ((System.Drawing.Image)(resources.GetObject("cmdOpen.Image")));
            this.cmdOpen.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cmdOpen.Location = new System.Drawing.Point(212, 544);
            this.cmdOpen.Name = "cmdOpen";
            this.cmdOpen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdOpen.Size = new System.Drawing.Size(27, 23);
            this.cmdOpen.TabIndex = 43;
            this.cmdOpen.Click += new System.EventHandler(this.cmdOpen_Click);
            // 
            // FrmBuscar
            // 
            this.FrmBuscar.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmBuscar.Controls.Add(this.tbCorreoElectronico);
            this.FrmBuscar.Controls.Add(this._rbBuscar_7);
            this.FrmBuscar.Controls.Add(this._rbBuscar_6);
            this.FrmBuscar.Controls.Add(this.tbCIPAutonomico);
            this.FrmBuscar.Controls.Add(this.TbVersion);
            this.FrmBuscar.Controls.Add(this.tbsocieda);
            this.FrmBuscar.Controls.Add(this.tbOtrosDocumentos);
            this.FrmBuscar.Controls.Add(this._rbBuscar_5);
            this.FrmBuscar.Controls.Add(this.tbTarjeta);
            this.FrmBuscar.Controls.Add(this._rbBuscar_4);
            this.FrmBuscar.Controls.Add(this.tbDPTelf1);
            this.FrmBuscar.Controls.Add(this._rbBuscar_3);
            this.FrmBuscar.Controls.Add(this.tbNHistoria);
            this.FrmBuscar.Controls.Add(this._rbBuscar_2);
            this.FrmBuscar.Controls.Add(this._rbBuscar_1);
            this.FrmBuscar.Controls.Add(this._rbBuscar_0);
            this.FrmBuscar.Controls.Add(this.tbNSS);
            this.FrmBuscar.Controls.Add(this.cbbArchivo);
            this.FrmBuscar.Controls.Add(this.MEBDNI);
            this.FrmBuscar.Controls.Add(this.Label6);
            this.FrmBuscar.Controls.Add(this.LbSociedad);
            this.FrmBuscar.Controls.Add(this.Label5);
            this.FrmBuscar.HeaderText = "B�squeda por...";
            this.FrmBuscar.Location = new System.Drawing.Point(4, 109);
            this.FrmBuscar.Name = "FrmBuscar";
            this.FrmBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmBuscar.Size = new System.Drawing.Size(595, 193);
            this.FrmBuscar.TabIndex = 34;
            this.FrmBuscar.Text = "B�squeda por...";
            // 
            // tbCorreoElectronico
            // 
            this.tbCorreoElectronico.AcceptsReturn = true;
            this.tbCorreoElectronico.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCorreoElectronico.Enabled = false;
            this.tbCorreoElectronico.Location = new System.Drawing.Point(150, 170);
            this.tbCorreoElectronico.MaxLength = 100;
            this.tbCorreoElectronico.Name = "tbCorreoElectronico";
            this.tbCorreoElectronico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCorreoElectronico.Size = new System.Drawing.Size(431, 19);
            this.tbCorreoElectronico.TabIndex = 25;
            this.tbCorreoElectronico.TextChanged += new System.EventHandler(this.tbCorreoElectronico_TextChanged);
            // 
            // _rbBuscar_7
            // 
            this._rbBuscar_7.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_7.Location = new System.Drawing.Point(16, 171);
            this._rbBuscar_7.Name = "_rbBuscar_7";
            this._rbBuscar_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_7.Size = new System.Drawing.Size(113, 18);
            this._rbBuscar_7.TabIndex = 24;
            this._rbBuscar_7.Text = "Correo electr�nico";
            this._rbBuscar_7.CheckStateChanged += new System.EventHandler(this.rbBuscar_CheckedChanged);
            // 
            // _rbBuscar_6
            // 
            this._rbBuscar_6.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_6.Location = new System.Drawing.Point(16, 148);
            this._rbBuscar_6.Name = "_rbBuscar_6";
            this._rbBuscar_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_6.Size = new System.Drawing.Size(120, 18);
            this._rbBuscar_6.TabIndex = 22;
            this._rbBuscar_6.Text = "N� CIP Auton�mico:";
            this._rbBuscar_6.CheckStateChanged += new System.EventHandler(this.rbBuscar_CheckedChanged);
            // 
            // tbCIPAutonomico
            // 
            this.tbCIPAutonomico.AcceptsReturn = true;
            this.tbCIPAutonomico.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbCIPAutonomico.Enabled = false;
            this.tbCIPAutonomico.Location = new System.Drawing.Point(150, 147);
            this.tbCIPAutonomico.MaxLength = 16;
            this.tbCIPAutonomico.Name = "tbCIPAutonomico";
            this.tbCIPAutonomico.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbCIPAutonomico.Size = new System.Drawing.Size(191, 19);
            this.tbCIPAutonomico.TabIndex = 23;
            this.tbCIPAutonomico.TextChanged += new System.EventHandler(this.tbCIPAutonomico_TextChanged);
            this.tbCIPAutonomico.Enter += new System.EventHandler(this.tbCIPAutonomico_Enter);
            this.tbCIPAutonomico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCIPAutonomico_KeyPress);
            this.tbCIPAutonomico.Leave += new System.EventHandler(this.tbCIPAutonomico_Leave);
            // 
            // TbVersion
            // 
            this.TbVersion.AcceptsReturn = true;
            this.TbVersion.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TbVersion.Enabled = false;
            this.TbVersion.Location = new System.Drawing.Point(533, 100);
            this.TbVersion.MaxLength = 2;
            this.TbVersion.Name = "TbVersion";
            this.TbVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TbVersion.Size = new System.Drawing.Size(45, 19);
            this.TbVersion.TabIndex = 19;
            // 
            // tbsocieda
            // 
            this.tbsocieda.AcceptsReturn = true;
            this.tbsocieda.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbsocieda.Enabled = false;
            this.tbsocieda.Location = new System.Drawing.Point(536, 48);
            this.tbsocieda.MaxLength = 4;
            this.tbsocieda.Name = "tbsocieda";
            this.tbsocieda.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbsocieda.Size = new System.Drawing.Size(43, 19);
            this.tbsocieda.TabIndex = 12;
            this.tbsocieda.Visible = false;
            // 
            // tbOtrosDocumentos
            // 
            this.tbOtrosDocumentos.AcceptsReturn = true;
            this.tbOtrosDocumentos.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbOtrosDocumentos.Enabled = false;
            this.tbOtrosDocumentos.Location = new System.Drawing.Point(441, 72);
            this.tbOtrosDocumentos.MaxLength = 9;
            this.tbOtrosDocumentos.Name = "tbOtrosDocumentos";
            this.tbOtrosDocumentos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbOtrosDocumentos.Size = new System.Drawing.Size(141, 19);
            this.tbOtrosDocumentos.TabIndex = 16;
            this.tbOtrosDocumentos.TextChanged += new System.EventHandler(this.tbOtrosDocumentos_TextChanged);
            this.tbOtrosDocumentos.Enter += new System.EventHandler(this.tbOtrosDocumentos_Enter);
            // 
            // _rbBuscar_5
            // 
            this._rbBuscar_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_5.Location = new System.Drawing.Point(326, 74);
            this._rbBuscar_5.Name = "_rbBuscar_5";
            this._rbBuscar_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_5.Size = new System.Drawing.Size(116, 18);
            this._rbBuscar_5.TabIndex = 15;
            this._rbBuscar_5.Text = "Otros documentos:";
            this._rbBuscar_5.CheckStateChanged += new System.EventHandler(this.rbBuscar_CheckedChanged);
            // 
            // tbTarjeta
            // 
            this.tbTarjeta.AcceptsReturn = true;
            this.tbTarjeta.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbTarjeta.Enabled = false;
            this.tbTarjeta.Location = new System.Drawing.Point(150, 48);
            this.tbTarjeta.MaxLength = 256;
            this.tbTarjeta.Name = "tbTarjeta";
            this.tbTarjeta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbTarjeta.Size = new System.Drawing.Size(322, 19);
            this.tbTarjeta.TabIndex = 11;
            this.tbTarjeta.TextChanged += new System.EventHandler(this.tbTarjeta_TextChanged);
            this.tbTarjeta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTarjeta_KeyPress);
            // 
            // _rbBuscar_4
            // 
            this._rbBuscar_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_4.Location = new System.Drawing.Point(16, 50);
            this._rbBuscar_4.Name = "_rbBuscar_4";
            this._rbBuscar_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_4.Size = new System.Drawing.Size(117, 18);
            this._rbBuscar_4.TabIndex = 10;
            this._rbBuscar_4.Text = "Id. Tarjeta sanitaria:";
            this._rbBuscar_4.CheckStateChanged += new System.EventHandler(this.rbBuscar_CheckedChanged);
            // 
            // tbDPTelf1
            // 
            this.tbDPTelf1.AcceptsReturn = true;
            this.tbDPTelf1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDPTelf1.Location = new System.Drawing.Point(150, 124);
            this.tbDPTelf1.MaxLength = 15;
            this.tbDPTelf1.Name = "tbDPTelf1";
            this.tbDPTelf1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDPTelf1.Size = new System.Drawing.Size(117, 19);
            this.tbDPTelf1.TabIndex = 21;
            this.tbDPTelf1.TextChanged += new System.EventHandler(this.tbDPTelf1_TextChanged);
            this.tbDPTelf1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDPTelf1_KeyPress);
            // 
            // _rbBuscar_3
            // 
            this._rbBuscar_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_3.Location = new System.Drawing.Point(16, 127);
            this._rbBuscar_3.Name = "_rbBuscar_3";
            this._rbBuscar_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_3.Size = new System.Drawing.Size(66, 18);
            this._rbBuscar_3.TabIndex = 20;
            this._rbBuscar_3.Text = "Tel�fono:";
            this._rbBuscar_3.CheckStateChanged += new System.EventHandler(this.rbBuscar_CheckedChanged);
            // 
            // tbNHistoria
            // 
            this.tbNHistoria.AcceptsReturn = true;
            this.tbNHistoria.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNHistoria.Enabled = false;
            this.tbNHistoria.Location = new System.Drawing.Point(150, 24);
            this.tbNHistoria.MaxLength = 10;
            this.tbNHistoria.Name = "tbNHistoria";
            this.tbNHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNHistoria.Size = new System.Drawing.Size(98, 19);
            this.tbNHistoria.TabIndex = 8;
            this.tbNHistoria.TextChanged += new System.EventHandler(this.tbNHistoria_TextChanged);
            this.tbNHistoria.Enter += new System.EventHandler(this.tbNHistoria_Enter);
            this.tbNHistoria.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNHistoria_KeyPress);
            this.tbNHistoria.Leave += new System.EventHandler(this.tbNHistoria_Leave);
            // 
            // _rbBuscar_2
            // 
            this._rbBuscar_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_2.Location = new System.Drawing.Point(16, 101);
            this._rbBuscar_2.Name = "_rbBuscar_2";
            this._rbBuscar_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_2.Size = new System.Drawing.Size(226, 18);
            this._rbBuscar_2.TabIndex = 17;
            this._rbBuscar_2.Text = "N� Seguridad Social / N� P�liza sociedad:";
            this._rbBuscar_2.CheckStateChanged += new System.EventHandler(this.rbBuscar_CheckedChanged);
            // 
            // _rbBuscar_1
            // 
            this._rbBuscar_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_1.Location = new System.Drawing.Point(16, 76);
            this._rbBuscar_1.Name = "_rbBuscar_1";
            this._rbBuscar_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_1.Size = new System.Drawing.Size(63, 18);
            this._rbBuscar_1.TabIndex = 13;
            this._rbBuscar_1.Text = "NIF/DNI:";
            this._rbBuscar_1.CheckStateChanged += new System.EventHandler(this.rbBuscar_CheckedChanged);
            // 
            // _rbBuscar_0
            // 
            this._rbBuscar_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbBuscar_0.Location = new System.Drawing.Point(16, 24);
            this._rbBuscar_0.Name = "_rbBuscar_0";
            this._rbBuscar_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbBuscar_0.Size = new System.Drawing.Size(126, 18);
            this._rbBuscar_0.TabIndex = 7;
            this._rbBuscar_0.Text = "N� de historia cl�nica:";
            this._rbBuscar_0.CheckStateChanged += new System.EventHandler(this.rbBuscar_CheckedChanged);
            // 
            // tbNSS
            // 
            this.tbNSS.AcceptsReturn = true;
            this.tbNSS.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNSS.Enabled = false;
            this.tbNSS.Location = new System.Drawing.Point(251, 99);
            this.tbNSS.MaxLength = 20;
            this.tbNSS.Name = "tbNSS";
            this.tbNSS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNSS.Size = new System.Drawing.Size(187, 19);
            this.tbNSS.TabIndex = 18;
            this.tbNSS.TextChanged += new System.EventHandler(this.tbNSS_TextChanged);
            this.tbNSS.Enter += new System.EventHandler(this.tbNSS_Enter);
            this.tbNSS.Leave += new System.EventHandler(this.tbNSS_Leave);
            // 
            // cbbArchivo
            // 
            this.cbbArchivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbArchivo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbbArchivo.Location = new System.Drawing.Point(350, 22);
            this.cbbArchivo.Name = "cbbArchivo";
            this.cbbArchivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbbArchivo.Size = new System.Drawing.Size(232, 20);
            this.cbbArchivo.TabIndex = 9;
            this.cbbArchivo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbbArchivo_KeyPress);
            // 
            // MEBDNI
            // 
            this.MEBDNI.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.MEBDNI.AllowPromptAsInput = false;
            this.MEBDNI.Enabled = false;
            this.MEBDNI.Location = new System.Drawing.Point(150, 73);
            this.MEBDNI.Mask = "########?";
            this.MEBDNI.Name = "MEBDNI";
            this.MEBDNI.PromptChar = ' ';
            this.MEBDNI.Size = new System.Drawing.Size(75, 20);
            this.MEBDNI.TabIndex = 14;
            this.MEBDNI.TabStop = false;
            this.MEBDNI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MEBDNI_KeyPress);
            this.MEBDNI.TextChanged += new System.EventHandler(this.MEBDNI_TextChanged);
            this.MEBDNI.Enter += new System.EventHandler(this.MEBDNI_Enter);
            this.MEBDNI.Leave += new System.EventHandler(this.MEBDNI_Leave);
            // 
            // Label6
            // 
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Location = new System.Drawing.Point(481, 104);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(46, 18);
            this.Label6.TabIndex = 42;
            this.Label6.Text = "Version:";
            // 
            // LbSociedad
            // 
            this.LbSociedad.Cursor = System.Windows.Forms.Cursors.Default;
            this.LbSociedad.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbSociedad.Location = new System.Drawing.Point(269, 124);
            this.LbSociedad.Name = "LbSociedad";
            this.LbSociedad.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LbSociedad.Size = new System.Drawing.Size(2, 2);
            this.LbSociedad.TabIndex = 41;
            // 
            // Label5
            // 
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Location = new System.Drawing.Point(302, 26);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(46, 18);
            this.Label5.TabIndex = 39;
            this.Label5.Text = "Archivo:";
            // 
            // FrmDatosPersonales
            // 
            this.FrmDatosPersonales.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.FrmDatosPersonales.Controls.Add(this.sdcFechaNac);
            this.FrmDatosPersonales.Controls.Add(this.chbFiliacPov);
            this.FrmDatosPersonales.Controls.Add(this.tbApellido2);
            this.FrmDatosPersonales.Controls.Add(this.tbApellido1);
            this.FrmDatosPersonales.Controls.Add(this.tbNombre);
            this.FrmDatosPersonales.Controls.Add(this.frmSexo);
            this.FrmDatosPersonales.Controls.Add(this.Label4);
            this.FrmDatosPersonales.Controls.Add(this.lbApellido1);
            this.FrmDatosPersonales.Controls.Add(this.Label2);
            this.FrmDatosPersonales.Controls.Add(this.Label3);
            this.FrmDatosPersonales.HeaderText = "Datos Personales";
            this.FrmDatosPersonales.Location = new System.Drawing.Point(4, 2);
            this.FrmDatosPersonales.Name = "FrmDatosPersonales";
            this.FrmDatosPersonales.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FrmDatosPersonales.Size = new System.Drawing.Size(593, 107);
            this.FrmDatosPersonales.TabIndex = 33;
            this.FrmDatosPersonales.Text = "Datos Personales";
            // 
            // sdcFechaNac
            // 
            this.sdcFechaNac.CustomFormat = "dd/MM/yyyy";
            this.sdcFechaNac.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.sdcFechaNac.Location = new System.Drawing.Point(105, 79);
            this.sdcFechaNac.MinDate = new System.DateTime(1800, 1, 1, 0, 0, 0, 0);
            this.sdcFechaNac.Name = "sdcFechaNac";
            this.sdcFechaNac.Size = new System.Drawing.Size(123, 20);
            this.sdcFechaNac.TabIndex = 3;
            this.sdcFechaNac.TabStop = false;
            this.sdcFechaNac.Text = "13/05/2016";
            this.sdcFechaNac.Value = new System.DateTime(2016, 5, 13, 0, 0, 0, 0);
            this.sdcFechaNac.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sdcFechaNac_KeyPress);
            this.sdcFechaNac.Leave += new System.EventHandler(this.sdcFechaNac_Leave);
            this.sdcFechaNac.Closed += new Telerik.WinControls.UI.RadPopupClosedEventHandler(this.sdcFechaNac_Closed);
            // 
            // chbFiliacPov
            // 
            this.chbFiliacPov.Cursor = System.Windows.Forms.Cursors.Default;
            this.chbFiliacPov.Location = new System.Drawing.Point(454, 84);
            this.chbFiliacPov.Name = "chbFiliacPov";
            this.chbFiliacPov.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbFiliacPov.Size = new System.Drawing.Size(118, 18);
            this.chbFiliacPov.TabIndex = 6;
            this.chbFiliacPov.Text = "Filiaci�n provisional";
            this.chbFiliacPov.Visible = false;
            this.chbFiliacPov.CheckStateChanged += new System.EventHandler(this.chbFiliacPov_CheckStateChanged);
            // 
            // tbApellido2
            // 
            this.tbApellido2.AcceptsReturn = true;
            this.tbApellido2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbApellido2.Location = new System.Drawing.Point(362, 24);
            this.tbApellido2.MaxLength = 35;
            this.tbApellido2.Name = "tbApellido2";
            this.tbApellido2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbApellido2.Size = new System.Drawing.Size(224, 19);
            this.tbApellido2.TabIndex = 1;
            this.tbApellido2.TextChanged += new System.EventHandler(this.tbApellido2_TextChanged);
            this.tbApellido2.Enter += new System.EventHandler(this.tbApellido2_Enter);
            this.tbApellido2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellido2_KeyPress);
            this.tbApellido2.Leave += new System.EventHandler(this.tbApellido2_Leave);
            // 
            // tbApellido1
            // 
            this.tbApellido1.AcceptsReturn = true;
            this.tbApellido1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbApellido1.Location = new System.Drawing.Point(67, 24);
            this.tbApellido1.MaxLength = 35;
            this.tbApellido1.Name = "tbApellido1";
            this.tbApellido1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbApellido1.Size = new System.Drawing.Size(224, 19);
            this.tbApellido1.TabIndex = 0;
            this.tbApellido1.TextChanged += new System.EventHandler(this.tbApellido1_TextChanged);
            this.tbApellido1.Enter += new System.EventHandler(this.tbApellido1_Enter);
            this.tbApellido1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbApellido1_KeyPress);
            this.tbApellido1.Leave += new System.EventHandler(this.tbApellido1_Leave);
            // 
            // tbNombre
            // 
            this.tbNombre.AcceptsReturn = true;
            this.tbNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNombre.Location = new System.Drawing.Point(67, 48);
            this.tbNombre.MaxLength = 35;
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbNombre.Size = new System.Drawing.Size(224, 19);
            this.tbNombre.TabIndex = 2;
            this.tbNombre.TextChanged += new System.EventHandler(this.tbNombre_TextChanged);
            this.tbNombre.Enter += new System.EventHandler(this.tbNombre_Enter);
            this.tbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombre_KeyPress);
            this.tbNombre.Leave += new System.EventHandler(this.tbNombre_Leave);
            // 
            // frmSexo
            // 
            this.frmSexo.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frmSexo.Controls.Add(this._rbSexo_1);
            this.frmSexo.Controls.Add(this._rbSexo_0);
            this.frmSexo.HeaderText = "Sexo";
            this.frmSexo.Location = new System.Drawing.Point(358, 43);
            this.frmSexo.Name = "frmSexo";
            this.frmSexo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frmSexo.Size = new System.Drawing.Size(73, 59);
            this.frmSexo.TabIndex = 32;
            this.frmSexo.Text = "Sexo";
            // 
            // _rbSexo_1
            // 
            this._rbSexo_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbSexo_1.Location = new System.Drawing.Point(8, 36);
            this._rbSexo_1.Name = "_rbSexo_1";
            this._rbSexo_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbSexo_1.Size = new System.Drawing.Size(49, 18);
            this._rbSexo_1.TabIndex = 5;
            this._rbSexo_1.Text = "Mujer";
            this._rbSexo_1.CheckStateChanged += new System.EventHandler(this.rbSexo_CheckedChanged);
            // 
            // _rbSexo_0
            // 
            this._rbSexo_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._rbSexo_0.Location = new System.Drawing.Point(8, 17);
            this._rbSexo_0.Name = "_rbSexo_0";
            this._rbSexo_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._rbSexo_0.Size = new System.Drawing.Size(61, 18);
            this._rbSexo_0.TabIndex = 4;
            this._rbSexo_0.Text = "Hombre";
            this._rbSexo_0.CheckStateChanged += new System.EventHandler(this.rbSexo_CheckedChanged);
            // 
            // Label4
            // 
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(303, 26);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(59, 18);
            this.Label4.TabIndex = 38;
            this.Label4.Text = "Apellido 2:";
            // 
            // lbApellido1
            // 
            this.lbApellido1.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbApellido1.Location = new System.Drawing.Point(8, 25);
            this.lbApellido1.Name = "lbApellido1";
            this.lbApellido1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbApellido1.Size = new System.Drawing.Size(59, 18);
            this.lbApellido1.TabIndex = 37;
            this.lbApellido1.Text = "Apellido 1:";
            // 
            // Label2
            // 
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(8, 49);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(50, 18);
            this.Label2.TabIndex = 36;
            this.Label2.Text = "Nombre:";
            // 
            // Label3
            // 
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(8, 80);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(98, 18);
            this.Label3.TabIndex = 35;
            this.Label3.Text = "Fecha Nacimiento:";
            // 
            // cbBuscar
            // 
            this.cbBuscar.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbBuscar.Location = new System.Drawing.Point(690, 272);
            this.cbBuscar.Name = "cbBuscar";
            this.cbBuscar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbBuscar.Size = new System.Drawing.Size(81, 32);
            this.cbBuscar.TabIndex = 26;
            this.cbBuscar.Text = "&Buscar";
            this.cbBuscar.Click += new System.EventHandler(this.cbBuscar_Click);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(8, 517);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(66, 18);
            this.Label1.TabIndex = 40;
            this.Label1.Text = "Con historia";
            // 
            // DFI120F1
            // 
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(DFI120F1_PreviewKeyDown);
            this.KeyPreview = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 573);
            this.Controls.Add(this.sprFiliados);
            this.Controls.Add(this.cbNuevo);
            this.Controls.Add(this.cbCancelar);
            this.Controls.Add(this.cbAceptar);
            this.Controls.Add(this.CBPacienteAnt);
            this.Controls.Add(this.cmdOpen);
            this.Controls.Add(this.FrmBuscar);
            this.Controls.Add(this.FrmDatosPersonales);
            this.Controls.Add(this.cbBuscar);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Frame1);
            this.Frame1.Controls.Add(ShapeContainer1);
            //this.Controls.Add(this.ShapeContainer1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(24, 35);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DFI120F1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Selecci�n de paciente - DFI120F1";
            this.Activated += new System.EventHandler(this.DFI120F1_Activated);
            this.Closed += new System.EventHandler(this.DFI120F1_Closed);
            this.Load += new System.EventHandler(this.DFI120F1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DFI120F1_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.sprFiliados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprFiliados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNuevo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CBPacienteAnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOpen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmBuscar)).EndInit();
            this.FrmBuscar.ResumeLayout(false);
            this.FrmBuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorreoElectronico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCIPAutonomico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbsocieda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbOtrosDocumentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTarjeta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDPTelf1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNHistoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbBuscar_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbArchivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MEBDNI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LbSociedad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrmDatosPersonales)).EndInit();
            this.FrmDatosPersonales.ResumeLayout(false);
            this.FrmDatosPersonales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdcFechaNac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbFiliacPov)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApellido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmSexo)).EndInit();
            this.frmSexo.ResumeLayout(false);
            this.frmSexo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._rbSexo_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbApellido1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandButtonHelper1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		void ReLoadForm(bool addEvents)
		{
			InitializerbSexo();
			InitializerbBuscar();
		}
		void InitializerbSexo()
		{
			this.rbSexo = new Telerik.WinControls.UI.RadRadioButton[2];
			this.rbSexo[1] = _rbSexo_1;
			this.rbSexo[0] = _rbSexo_0;
		}
		void InitializerbBuscar()
		{
			this.rbBuscar = new Telerik.WinControls.UI.RadRadioButton[8];
			this.rbBuscar[7] = _rbBuscar_7;
			this.rbBuscar[6] = _rbBuscar_6;
			this.rbBuscar[5] = _rbBuscar_5;
			this.rbBuscar[4] = _rbBuscar_4;
			this.rbBuscar[3] = _rbBuscar_3;
			this.rbBuscar[2] = _rbBuscar_2;
			this.rbBuscar[1] = _rbBuscar_1;
			this.rbBuscar[0] = _rbBuscar_0;
		}
        #endregion
    }
}