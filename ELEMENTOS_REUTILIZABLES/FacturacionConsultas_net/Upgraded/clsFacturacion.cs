using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;

namespace FacturacionConsultas
{
	public class clsFacturacion
	{

		string StSql = String.Empty;
		public string FechaFactur = String.Empty;
		// indicador de si la llamada a la dll es en modo consulta o definitiva
		public bool bTemp = false;

		public void GenerarMovimientosFacturacion(int AñoRegistro, int NumeroRegistro, ref SqlConnection Conexion, dynamic Formulario, object FechaFact_optional, object Consulta_optional, object TablaFacturacion_optional)
		{
			string FechaFact = (FechaFact_optional == Type.Missing) ? String.Empty : FechaFact_optional as string;
			object Consulta = (Consulta_optional == Type.Missing) ? null : Consulta_optional as object;
			string TablaFacturacion = (TablaFacturacion_optional == Type.Missing) ? String.Empty : TablaFacturacion_optional as string;
			DataSet rsConsulta = null;
			DataSet rsOtrasPrestaciones = null;
			DataSet rsProcesos = null;
			DataSet rsEpisodio = null;
			string FechaSistema = String.Empty;
			DataSet rsDanalsol = null;
            Int32 indicersDanalsol = 0;
            bool ExisteProtocolo = false;
			string sql = String.Empty;
			bool Continua_Bucle = false;
			string PrestacionFactura = String.Empty;
			int iCantidad = 0;
			int ServicioSolicitantePrincipal = 0;
			string stTablaFacturacion = String.Empty;
			DataSet rsFacturacion = null;
            Int32 indicersFacturacion = 0;


            string stSeparadorDecimalBD = Serrores.ObtenerSeparadorDecimalBD(Conexion);
			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			if (FechaFact_optional != Type.Missing)
			{
				FechaFactur = FechaFact;
			}
			else
			{
				FechaFactur = "";
			}

			bTemp = Consulta_optional != Type.Missing;

			//OSCAR C DICIEMBRE 2008
			// Si no existe la tabla temporal correspondiente al episodio de consulta a facturar, se debera crear dicha tabla.
			// Si ya existiera, significara que se ha cargada previamente en la funcion GENERADO de esta dll.

			string stTablaFacturacion_W = "W_DMOVFACT_C" + Conversion.Str(AñoRegistro).Trim() + Conversion.Str(NumeroRegistro).Trim();

			StSql = "SELECT '' FROM sysobjects where name='" + stTablaFacturacion_W + "' and type='U'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet rsPrivado = new DataSet();
			tempAdapter.Fill(rsPrivado);
			if (rsPrivado.Tables[0].Rows.Count == 0 )
			{
				proCrearTablaFacturacion(stTablaFacturacion_W, Conexion);
			}
			rsPrivado.Close();
			//-----------------------

			if (TablaFacturacion_optional == Type.Missing)
			{
				stTablaFacturacion = "DMOVFACT";
			}
			else
			{
				stTablaFacturacion = TablaFacturacion;
			}

			try
			{
				FechaSistema = DateTimeHelper.ToString(DateTime.Now);

				// Se obtiene el codigo de financiadora correspondiente a privado
				StSql = "SELECT nnumeri1 " + 
				        "FROM sconsglo " + 
				        "WHERE gconsglo = 'PRIVADO'";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
				rsPrivado = new DataSet();
				tempAdapter_2.Fill(rsPrivado);

				// Se obtiene la prestacion principal de la consulta
				StSql = "SELECT cconsult.*, dcodpres.iprimsuc " + 
				        "FROM cconsult, dcodpres " + 
				        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
				        "gnumregi = " + NumeroRegistro.ToString() + " AND " + 
				        "cconsult.gprestac = dcodpres.gprestac";
				SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, Conexion);
				rsConsulta = new DataSet();
				tempAdapter_3.Fill(rsConsulta);

				bool EstaDentroFechaProceso = false;
				if (rsConsulta.Tables[0].Rows.Count != 0)
				{

					//*luis 07/10/1999
					//si la prestacion de la consulta tiene protocolos se factura lo que hay en danalsol
					if (HayProtocolo(Convert.ToString(rsConsulta.Tables[0].Rows[0]["gprestac"]), Conexion))
					{
						ExisteProtocolo = true;
						sql = SqlPruebasDanalsol(Convert.ToString(rsConsulta.Tables[0].Rows[0]["GPRESTAC"]), AñoRegistro, NumeroRegistro, Conexion);
						//sql = "select * from danalsol where itiposer = 'C' and ganoregi=" & AñoRegistro & " and " _
						//& "gnumregi=" & NumeroRegistro & " AND GPRESTAC='" & rsConsulta("GPRESTAC") & "'"
						SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion);
						rsDanalsol = new DataSet();
						tempAdapter_4.Fill(rsDanalsol);
						if (rsDanalsol.Tables[0].Rows.Count != 0)
						{
							indicersDanalsol = rsDanalsol.MoveFirst();
						}
					}
					else
					{
						ExisteProtocolo = false;
						PrestacionFactura = Convert.ToString(rsConsulta.Tables[0].Rows[0]["gprestac"]);
						iCantidad = (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ncantida"])) ? 1 : Convert.ToInt32(rsConsulta.Tables[0].Rows[0]["ncantida"]);
					}

					Continua_Bucle = true;
					while (Continua_Bucle)
					{
						if (ExisteProtocolo)
						{
							PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
							iCantidad = Convert.ToInt32(rsDanalsol.Tables[0].Rows[indicersDanalsol]["ncantida"]);
						}
						else
						{
							Continua_Bucle = false;
						}

						//OSCAR C DICIEMBRE 2008
						//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
						//de facturacion. Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
						//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
						//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
						//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
						//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
						//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL


						if (stTablaFacturacion != "DMOVFACT")
						{

							if (bTemp)
							{
								StSql = "INSERT INTO dmovfacc " + 
								        "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
								        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
								        "gconcfac, ncantida, fsistema, ffinicio, gdiagalt, " + 
								        "ginspecc, gpersona, ndninifp, iprimsuc, iautosoc, " + 
								        "iexiauto, fperfact,itiporig,ganoorig,gnumorig) " + 
								        "VALUES('" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gidenpac"]) + "',";
							}
							else
							{
								//                stSql = "INSERT INTO dmovfact " & _
								//'                           "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " & _
								//'                            "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " & _
								//'                            "gconcfac, ncantida, fsistema, ffinicio, gdiagalt, " & _
								//'                            "ginspecc, gpersona, ndninifp, iprimsuc, iautosoc, " & _
								//'                            "iexiauto, fperfact,itiporig,ganoorig,gnumorig) " & _
								//'                           "VALUES('" & rsConsulta!gidenpac & "',"

								StSql = "INSERT INTO " + stTablaFacturacion + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
								        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
								        "gconcfac, ncantida, fsistema, ffinicio, gdiagalt, " + 
								        "ginspecc, gpersona, ndninifp, iprimsuc, iautosoc, " + 
								        "iexiauto, fperfact,itiporig,ganoorig,gnumorig, cpagpaci) " + 
								        "VALUES('" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gidenpac"]) + "',";
							}
							// Si el paciente no estaba hospitalizado la prestación principal
							// se graba como externa. Si estaba hospitalizado se graba como interna
							ServicioSolicitantePrincipal = 0;
							if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
							{
								StSql = StSql + 
								        "'I','" + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "'," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + "," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + "," + 
								        "'PR',";
							}
							else
							{
								if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gsersoli"]))
								{
									// si el servicio peticionario se considera externo (4/3/99)
									StSql = StSql + 
									        "'E',";
									ServicioSolicitantePrincipal = 0;
								}
								else
								{
									StSql = StSql + 
									        "'I',";
									ServicioSolicitantePrincipal = Convert.ToInt32(rsConsulta.Tables[0].Rows[0]["gsersoli"]);
								}

								StSql = StSql + 
								        "'C'," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + "," + 
								        "'PR',";
							}
							if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gsersoli"]))
							{
								StSql = StSql + 
								        "(Null),";
							}
							else
							{
								StSql = StSql + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsersoli"]) + ",";
							}
							StSql = StSql + 
							        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gservici"]) + ",";
							// Modificación del 2/2/99: si no ha traído la autorización necesaria
							// la prestación se cobra como privado
							if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
							{
								StSql = StSql + 
								        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
							}
							else
							{
								StSql = StSql + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsocieda"]) + ",";
							}
							if (rsConsulta.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
							{
								StSql = StSql + 
								        "(Null),";
							}
							else
							{
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
							}
							StSql = StSql + 
							        "'" + PrestacionFactura + "'," + 
							        iCantidad.ToString() + "," + 
							        "" + Serrores.FormatFechaHMS(FechaSistema) + "," + 
							        "" + Serrores.FormatFechaHM(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString()) + ",";
							
							if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H" || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gdiagnos"]))
							{
								StSql = StSql + 
								        "(Null),";
							}
							else
							{
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gdiagnos"]) + "',";
							}
							
							if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ginspecc"]))
							{
								StSql = StSql + 
								        "(Null),";
							}
							else
							{
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ginspecc"]) + "',";
							}
							
							if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gpersona"]))
							{
								StSql = StSql + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gpersona"]) + ",";
							}
							else
							{
								StSql = StSql + "(Null),";
							}
							
							if (rsConsulta.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
							{
								StSql = StSql + 
								        "(Null),";
							}
							else
							{
								
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
							}
							
							if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iprimsuc"]))
							{
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["iprimsuc"]) + "',";
							}
							else
							{
								StSql = StSql + 
								        "(Null),";
							}
							
							if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]))
							{
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["iautosoc"]).Trim() + "',";
							}
							else
							{
								StSql = StSql + 
								        "(Null),";
							}
							
							if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
							{
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["iexiauto"]).Trim() + "',";
							}
							else
							{
								StSql = StSql + 
								        "(Null),";
							}
							if (FechaFactur != "")
							{
								StSql = StSql + "" + Serrores.FormatFechaHMS(FechaFactur) + ",";
							}
							else
							{
								StSql = StSql + "" + Serrores.FormatFechaHMS(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString()) + ",";
							}
							//luis añadir campos origen del episodio
							StSql = StSql + "'C'," + 
							        Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
							        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + ",";

							//Oscar C Agosto 2011. Copago
							if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["cpagpaci"]))
							{
								StSql = StSql + "(Null) ";
							}
							else
							{
								string tempRefParam5 = Convert.ToString(rsConsulta.Tables[0].Rows[0]["cpagpaci"]);
								StSql = StSql + "'" + Serrores.ConvertirDecimales(ref tempRefParam5, ref stSeparadorDecimalBD, 2) + "'";
							}
							//-----------
							StSql = StSql + ")";

							SqlCommand tempCommand = new SqlCommand(StSql, Conexion);
							tempCommand.ExecuteNonQuery();
						}
						else
						{

							//OSCAR C DICIEMBRE 2008
							//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
							//de facturacion. Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
							//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
							//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
							//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
							//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
							//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
							// NOTA:
							// La condicion de busqueda sera por:
							// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
							// ffechfin*, gdiagalt, ginspecc, iregiqui*, gpersona, gserresp*, gcodiart*, gcodlote*,
							// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
							// y que ademas tuviera la marca de borrado logico (iborrado=NULL)
							//* No aplica en Consultas

							StSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE ";

							StSql = StSql + " gcodeven = 'PR' ";

							if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
							{
								StSql = StSql + " AND itiposer ='" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "'" + 
								        " AND ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + 
								        " AND gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]);
							}
							else
							{
								StSql = StSql + " AND itiposer ='C'" + 
								        " AND ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + 
								        " AND gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]);
							}

							if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gsersoli"]))
							{
								StSql = StSql + " AND gsersoli IS NULL ";
							}
							else
							{
								StSql = StSql + " AND gsersoli = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsersoli"]);
							}
                            
							if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gservici"]))
							{
								StSql = StSql + " AND gserreal IS NULL ";
							}
							else
							{
								StSql = StSql + " AND gserreal = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gservici"]);
							}

							if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
							{
								StSql = StSql + " AND gsocieda = " + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
							}
							else
							{
								StSql = StSql + " AND gsocieda = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsocieda"]);
							}

							if (rsConsulta.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
							{
								StSql = StSql + " AND (nafiliac is null or nafiliac='') ";
							}
							else
							{
								StSql = StSql + " AND nafiliac = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() + "'";
							}

							StSql = StSql + " AND gconcfac ='" + PrestacionFactura + "'";
							StSql = StSql + " AND ncantida = " + iCantidad.ToString();
							
							StSql = StSql + " AND ffinicio = " + Serrores.FormatFechaHM(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString());

							if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H" || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gdiagnos"]))
							{
								StSql = StSql + " AND gdiagalt IS NULL ";
							}
							else
							{	
								StSql = StSql + " AND gdiagalt = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gdiagnos"]) + "'";
							}

							if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ginspecc"]))
							{
								StSql = StSql + " AND ginspecc IS NULL ";
							}
							else
							{
								StSql = StSql + " AND ginspecc = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ginspecc"]) + "'";
							}

							if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gpersona"]))
							{
								StSql = StSql + " AND gpersona = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gpersona"]);
							}
							else
							{
								StSql = StSql + " AND gpersona IS NULL ";
							}

							StSql = StSql + 
							        " AND itiporig = 'C' " + 
							        " AND ganoorig = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + 
							        " AND gnumorig = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + 
							        " AND iborrado ='S'";

							SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSql, Conexion);
							rsFacturacion = new DataSet();
							tempAdapter_5.Fill(rsFacturacion);
							if (rsFacturacion.Tables[0].Rows.Count != 0)
							{

								//Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
								//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)

								indicersFacturacion = rsFacturacion.MoveFirst();
								//MsgBox "encontrado"

								StSql = " UPDATE " + stTablaFacturacion_W + " SET ";
								StSql = StSql + " gidenpac ='" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gidenpac"]) + "', ";

								// Si el paciente no estaba hospitalizado la prestación principal
								// se graba como externa. Si estaba hospitalizado se graba como interna
								ServicioSolicitantePrincipal = 0;
								if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
								{
									StSql = StSql + " itpacfac ='I', " + 
									        " itiposer ='" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "'," + 
									        " ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + "," + 
									        " gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + "," + 
									        " gcodeven = 'PR',";
								}
								else
								{
									if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gsersoli"]))
									{
										StSql = StSql + " itpacfac ='E', ";
										ServicioSolicitantePrincipal = 0;
									}
									else
									{
										StSql = StSql + " itpacfac ='I', ";
										ServicioSolicitantePrincipal = Convert.ToInt32(rsConsulta.Tables[0].Rows[0]["gsersoli"]);
									}
									StSql = StSql + " itiposer ='C'," + 
									        " ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
									        " gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + "," + 
									        " gcodeven = 'PR',";
								}
								
								if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gsersoli"]))
								{
									StSql = StSql + " gsersoli = (Null), ";
								}
								else
								{
									StSql = StSql + " gsersoli = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsersoli"]) + ", ";
								}
								
								StSql = StSql + " gserreal = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gservici"]) + ", ";
								// Modificación del 2/2/99: si no ha traído la autorización necesaria
								// la prestación se cobra como privado
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
								{
									StSql = StSql + " gsocieda = " + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
								}
								else
								{
									StSql = StSql + " gsocieda = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsocieda"]) + ",";
								}
								
								if (rsConsulta.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
								{
									StSql = StSql + " nafiliac = (Null), ";
								}
								else
								{
									StSql = StSql + " nafiliac = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
								}
								StSql = StSql + " gconcfac = " + "'" + PrestacionFactura + "',";
								StSql = StSql + " ncantida = " + iCantidad.ToString() + ",";
								
								StSql = StSql + " fsistema = " + Serrores.FormatFechaHMS(FechaSistema) + ",";
								
								StSql = StSql + " ffinicio = " + Serrores.FormatFechaHM(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString()) + ",";
								
								if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H" || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gdiagnos"]))
								{
									StSql = StSql + " gdiagalt = (Null),";
								}
								else
								{
									StSql = StSql + " gdiagalt = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gdiagnos"]) + "',";
								}
								
								if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ginspecc"]))
								{
									StSql = StSql + " ginspecc = (Null), ";
								}
								else
								{
									StSql = StSql + " ginspecc = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ginspecc"]) + "',";
								}
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gpersona"]))
								{
									StSql = StSql + " gpersona = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gpersona"]) + ",";
								}
								else
								{
									StSql = StSql + " gpersona = (Null),";
								}
								
								if (rsConsulta.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
								{
									StSql = StSql + " ndninifp = (Null),";
								}
								else
								{
									StSql = StSql + " ndninifp = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
								}
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iprimsuc"]))
								{
									StSql = StSql + " iprimsuc = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["iprimsuc"]) + "',";
								}
								else
								{
									StSql = StSql + " iprimsuc = (Null),";
								}
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]))
								{
									StSql = StSql + " iautosoc = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["iautosoc"]).Trim() + "',";
								}
								else
								{
									StSql = StSql + " iautosoc = (Null),";
								}
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
								{
									StSql = StSql + " iexiauto = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["iexiauto"]).Trim() + "',";
								}
								else
								{
									StSql = StSql + " iexiauto = (Null),";
								}
								if (FechaFactur != "")
								{
									StSql = StSql + " fperfact = " + Serrores.FormatFechaHMS(FechaFactur) + ",";
								}
								else
								{
									StSql = StSql + " fperfact = " + Serrores.FormatFechaHMS(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString()) + ",";
								}
								
								StSql = StSql + " itiporig = 'C', " + 
								        " ganoorig = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
								        " gnumorig = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + ",";

								StSql = StSql + " iborrado = (NULL), ";
								//Oscar C Agosto 2011. Copago
								if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["cpagpaci"]))
								{
									StSql = StSql + " cpagpaci = (Null) ";
								}
								else
								{
									string tempRefParam11 = Convert.ToString(rsConsulta.Tables[0].Rows[0]["cpagpaci"]);
									StSql = StSql + " cpagpaci = '" + Serrores.ConvertirDecimales(ref tempRefParam11, ref stSeparadorDecimalBD, 2) + "'";
								}
								//-----------
								StSql = StSql + " Where fila_id = " + Convert.ToString(rsFacturacion.Tables[0].Rows[indicersFacturacion]["fila_id"]);

								SqlCommand tempCommand_2 = new SqlCommand(StSql, Conexion);
								tempCommand_2.ExecuteNonQuery();

							}
							else
							{

								//MsgBox "No encontrado"
								//Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
								//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
                                
								StSql = "INSERT INTO " + stTablaFacturacion_W + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
								        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
								        "gconcfac, ncantida, fsistema, ffinicio, gdiagalt, " + 
								        "ginspecc, gpersona, ndninifp, iprimsuc, iautosoc, " + 
								        "iexiauto, fperfact, itiporig, ganoorig, gnumorig, cpagpaci) " + 
								        "VALUES('" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gidenpac"]) + "',";

								// Si el paciente no estaba hospitalizado la prestación principal
								// se graba como externa. Si estaba hospitalizado se graba como interna
								ServicioSolicitantePrincipal = 0;
								if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
								{
									StSql = StSql + 
									        "'I','" + 
									        Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "'," + 
									        Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + "," + 
									        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + "," + 
									        "'PR',";
								}
								else
								{
									if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gsersoli"]))
									{
										// si el servicio peticionario se considera externo (4/3/99)
										StSql = StSql + 
										        "'E',";
										ServicioSolicitantePrincipal = 0;
									}
									else
									{
										StSql = StSql + 
										        "'I',";
										ServicioSolicitantePrincipal = Convert.ToInt32(rsConsulta.Tables[0].Rows[0]["gsersoli"]);
									}

									StSql = StSql + 
									        "'C'," + 
									        Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
									        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + "," + 
									        "'PR',";
								}
								
								if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gsersoli"]))
								{
									StSql = StSql + 
									        "(Null),";
								}
								else
								{
									StSql = StSql + 
									        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsersoli"]) + ",";
								}
								
								StSql = StSql + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gservici"]) + ",";
								// Modificación del 2/2/99: si no ha traído la autorización necesaria
								// la prestación se cobra como privado
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
								{
								
									StSql = StSql + 
									        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
								}
								else
								{
									StSql = StSql + 
									        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsocieda"]) + ",";
								}
								if (rsConsulta.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
								{
									StSql = StSql + 
									        "(Null),";
								}
								else
								{
									StSql = StSql + 
									        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
								}
								
								StSql = StSql + 
								        "'" + PrestacionFactura + "'," + 
								        iCantidad.ToString() + "," + 
								        "" + Serrores.FormatFechaHMS(FechaSistema) + "," + 
								        "" + Serrores.FormatFechaHM(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString()) + ",";
								
								if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H" || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gdiagnos"]))
								{
									StSql = StSql + 
									        "(Null),";
								}
								else
								{
									StSql = StSql + 
									        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gdiagnos"]) + "',";
								}
								
								if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ginspecc"]))
								{
									StSql = StSql + 
									        "(Null),";
								}
								else
								{
									StSql = StSql + 
									        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ginspecc"]) + "',";
								}
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["gpersona"]))
								{
									StSql = StSql + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gpersona"]) + ",";
								}
								else
								{
									StSql = StSql + "(Null),";
								}
								
								if (rsConsulta.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
								{
									StSql = StSql + 
									        "(Null),";
								}
								else
								{
									StSql = StSql + 
									        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() + "',";
								}
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iprimsuc"]))
								{
									StSql = StSql + 
									        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["iprimsuc"]) + "',";
								}
								else
								{
									StSql = StSql + 
									        "(Null),";
								}
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]))
								{
									StSql = StSql + 
									        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["iautosoc"]).Trim() + "',";
								}
								else
								{
									StSql = StSql + 
									        "(Null),";
								}
								
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
								{
									StSql = StSql + 
									        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["iexiauto"]).Trim() + "',";
								}
								else
								{
									StSql = StSql + 
									        "(Null),";
								}
								if (FechaFactur != "")
								{
									StSql = StSql + "" + Serrores.FormatFechaHMS(FechaFactur) + ",";
								}
								else
								{
									StSql = StSql + "" + Serrores.FormatFechaHMS(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString()) + ",";
								}
								//luis añadir campos origen del episodio
								StSql = StSql + "'C'," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + ",";
								//Oscar C Agosto 2011. Copago
								if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["cpagpaci"]))
								{
									StSql = StSql + " (Null) ";
								}
								else
								{
									
									string tempRefParam16 = Convert.ToString(rsConsulta.Tables[0].Rows[0]["cpagpaci"]);
									StSql = StSql + "'" + Serrores.ConvertirDecimales(ref tempRefParam16, ref stSeparadorDecimalBD, 2) + "'";
								}
								//-----------
								StSql = StSql + ")";
								SqlCommand tempCommand_3 = new SqlCommand(StSql, Conexion);
								tempCommand_3.ExecuteNonQuery();

							}
    						rsFacturacion.Close();
							//----------------------FIN OSCAR C DICIEMBRE 2008----------------------------------------
						}

						if (ExisteProtocolo)
						{
							indicersDanalsol = rsDanalsol.MoveNext();
							if (indicersDanalsol < rsDanalsol.Tables[0].Rows.Count )
							{
								PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
								iCantidad = Convert.ToInt32(rsDanalsol.Tables[0].Rows[indicersDanalsol]["ncantida"]);
							}
							else
							{
								Continua_Bucle = false;
								rsDanalsol.Close();
							}
						}

					}
					rsConsulta.Edit();
					rsConsulta.Tables[0].Rows[0]["fpasfact"] = FechaSistema;
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_3);
					tempAdapter_3.Update(rsConsulta, rsConsulta.Tables[0].TableName);
					EstaDentroFechaProceso = false;
					//-------------rsProcesos
					//Actualizamos la tabla de procesos de consultas para las otras prestaciones de una consulta o un quirofano
					if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ganoprin"]) && (Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) == "C" || Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) == "Q"))
					{
						StSql = "SELECT * from DEPIPROC inner join DPROCESO ON " + 
						        "DEPIPROC.ganoproc= DPROCESO.ganoproc and DEPIPROC.gnumproc= DPROCESO.gnumproc " + 
						        "WHERE itipprin = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "' AND " + 
						        "ganoprin = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
						        "gnumprin = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " AND " + 
						        "gnumprin = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " AND " + 
						        "(DPROCESO.ffinproc>=" + Serrores.FormatFechaHMS(rsConsulta.Tables[0].Rows[0]["fmecaniz"].ToString()) + " or DPROCESO.ffinproc is null)";

						SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(StSql, Conexion);
						rsProcesos = new DataSet();
						tempAdapter_7.Fill(rsProcesos);
						if (rsProcesos.Tables[0].Rows.Count != 0)
						{
							EstaDentroFechaProceso = true;
						}
						rsProcesos.Close();
                        
						if (EstaDentroFechaProceso)
						{
							StSql = "SELECT depiproc.ganoproc, depiproc.gnumproc,depiproc.gusuario " + 
							        "FROM DEPIPROC WITH (NOLOCK) " + 
							        "WHERE itipprin = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "' AND " + 
							        "ganoprin = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
							        "gnumprin = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " AND " + 
							        "gnumprin = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " ";


							SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(StSql, Conexion);
							rsProcesos = new DataSet();
							tempAdapter_8.Fill(rsProcesos);
							if (rsProcesos.Tables[0].Rows.Count != 0)
							{
								StSql = "SELECT * " + 
								        "FROM DEPIPROC WITH (NOLOCK) " + 
								        "WHERE itipprin = 'C' AND " + 
								        "ganoprin = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
								        "gnumprin = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " AND " + 
								        "itiposer = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "' AND " + 
								        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
								        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]);

								SqlDataAdapter tempAdapter_9 = new SqlDataAdapter(StSql, Conexion);
								rsEpisodio = new DataSet();
								tempAdapter_9.Fill(rsEpisodio);
								if (rsEpisodio.Tables[0].Rows.Count == 0)
								{
						
									StSql = "insert into DEPIPROC values (" + Convert.ToString(rsProcesos.Tables[0].Rows[0]["ganoproc"]) + "," + Convert.ToString(rsProcesos.Tables[0].Rows[0]["gnumproc"]) + 
									        ",'C'," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + ",'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + 
									        "'," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + "," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + ",'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gusuario"]).Trim() + "')";

									SqlCommand tempCommand_4 = new SqlCommand(StSql, Conexion);
									tempCommand_4.ExecuteNonQuery();
								}
							}
						}
					}
					rsProcesos = null;
					rsEpisodio = null;
					//-------------

				}

				// Se obtienen las otras prestaciones que se realizaron en la consulta
				StSql = "SELECT epresalt.*, dcodpres.gservici, dcodpres.iprimsuc " + 
				        "FROM epresalt, dcodpres " + 
				        "WHERE itiposer = 'C' AND " + 
				        "ganoregi = " + AñoRegistro.ToString() + " AND " + 
				        "gnumregi = " + NumeroRegistro.ToString() + " AND " + 
				        "epresalt.gprestac = dcodpres.gprestac";
				SqlDataAdapter tempAdapter_10 = new SqlDataAdapter(StSql, Conexion);
				rsOtrasPrestaciones = new DataSet();
				tempAdapter_10.Fill(rsOtrasPrestaciones);

				foreach (DataRow iteration_row in rsOtrasPrestaciones.Tables[0].Rows)
				{

					if (HayProtocolo(Convert.ToString(iteration_row["gprestac"]), Conexion))
					{
						ExisteProtocolo = true;
						sql = SqlPruebasDanalsol(Convert.ToString(iteration_row["gprestac"]), AñoRegistro, NumeroRegistro, Conexion);
						//sql = "select * from danalsol where itiposer = 'C' and ganoregi=" & AñoRegistro & " and " _
						//& "gnumregi=" & NumeroRegistro & " AND GPRESTAC='" & rsOtrasPrestaciones("GPRESTAC") & "'"
						SqlDataAdapter tempAdapter_11 = new SqlDataAdapter(sql, Conexion);
						rsDanalsol = new DataSet();
						tempAdapter_11.Fill(rsDanalsol);
						if (rsDanalsol.Tables[0].Rows.Count != 0)
						{
							indicersDanalsol = rsDanalsol.MoveFirst();
						}
					}
					else
					{
						ExisteProtocolo = false;
						PrestacionFactura = Convert.ToString(iteration_row["gprestac"]);
						iCantidad = (Convert.IsDBNull(iteration_row["ncantida"])) ? 1 : Convert.ToInt32(iteration_row["ncantida"]);
					}

					Continua_Bucle = true;
					while (Continua_Bucle)
					{

						if (ExisteProtocolo)
						{
							PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
							iCantidad = Convert.ToInt32(rsDanalsol.Tables[0].Rows[indicersDanalsol]["ncantida"]);
						}
						else
						{
							Continua_Bucle = false;
						}

						//OSCAR C DICIEMBRE 2008
						//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
						//de facturacion. Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
						//Si es DMOVFACT (el ELSE, se debera realizar lo siguiente):
						//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
						//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
						//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
						//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL

						if (stTablaFacturacion != "DMOVFACT")
						{

							if (bTemp)
							{
								StSql = "INSERT INTO dmovfacc " + 
								        "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
								        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
								        "gconcfac, ncantida, fsistema, ffinicio, ginspecc, " + 
								        "gpersona, ndninifp, iprimsuc, iautosoc, iexiauto, fperfact,itiporig,ganoorig,gnumorig) " + 
								        "VALUES('" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gidenpac"]) + "',";
							}
							else
							{
								//stSql = "INSERT INTO dmovfact " & _
								//'           "(gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " & _
								//'            "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " & _
								//'            "gconcfac, ncantida, fsistema, ffinicio, ginspecc, " & _
								//'            "gpersona, ndninifp, iprimsuc, iautosoc, iexiauto, fperfact,itiporig,ganoorig,gnumorig) " & _
								//'           "VALUES('" & rsConsulta!gidenpac & "',"
								StSql = "INSERT INTO " + stTablaFacturacion + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
								        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
								        "gconcfac, ncantida, fsistema, ffinicio, ginspecc, " + 
								        "gpersona, ndninifp, iprimsuc, iautosoc, iexiauto, fperfact,itiporig,ganoorig,gnumorig,cpagpaci) " + 
								        "VALUES('" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gidenpac"]) + "',";
							}
							// Si el paciente no estaba hospitalizado se graba el episodio de la
							// consulta. Si estaba hospitalizado se graba el episodio principal
							if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
							{
								StSql = StSql + 
								        "'I','" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "'," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + "," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + "," + 
								        "'PR',";
								if (Convert.IsDBNull(iteration_row["gsersoli"]))
								{
									StSql = StSql + 
									        "(Null),";
								}
								else
								{
									StSql = StSql + 
									        Convert.ToString(iteration_row["gsersoli"]) + ",";
								}
							}
							else
							{
								if (ServicioSolicitantePrincipal == 0)
								{
									StSql = StSql + 
									        "'E',";
								}
								else
								{
									StSql = StSql + 
									        "'I',";
								}
								StSql = StSql + "'C'," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + "," + 
								        "'PR',";
								if (ServicioSolicitantePrincipal == 0)
								{
									StSql = StSql + 
									        "(Null),";
								}
								else
								{
									StSql = StSql + 
									        ServicioSolicitantePrincipal.ToString() + ",";
								}
							}
							StSql = StSql + 
							        Convert.ToString(iteration_row["gservici"]) + ",";
							// Modificación del 2/2/99: si no ha traído la autorización necesaria
							// la prestación se cobra como privado
							if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
							{
								StSql = StSql + 
								        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
							}
							else
							{
								StSql = StSql + 
								        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsocieda"]) + ",";
							}

							if (rsConsulta.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
							{
								StSql = StSql + 
								        "(Null),";
							}
							else
							{
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]) + "',";
							}
							StSql = StSql + 
							        "'" + PrestacionFactura + "'," + iCantidad.ToString() + ",";
							
							StSql = StSql + 
							        "" + Serrores.FormatFechaHMS(FechaSistema) + "," + 
							        "" + Serrores.FormatFechaHMS(iteration_row["frealiza"].ToString()) + ",";
							if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ginspecc"]))
							{
								StSql = StSql + 
								        "(Null),";
							}
							else
							{
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ginspecc"]) + "',";
							}

							//oscar
							if (!Convert.IsDBNull(iteration_row["gperreal"]))
							{
								StSql = StSql + Convert.ToString(iteration_row["gperreal"]) + ",";
							}
							else
							{
								StSql = StSql + "(Null),";
							}
							//-----

							if (rsConsulta.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
							{
								StSql = StSql + 
								        "(Null),";
							}
							else
							{
								StSql = StSql + 
								        "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]) + "',";
							}
							if (!Convert.IsDBNull(iteration_row["iprimsuc"]))
							{
								StSql = StSql + 
								        "'" + Convert.ToString(iteration_row["iprimsuc"]) + "',";
							}
							else
							{
								StSql = StSql + 
								        "(Null),";
							}

                            if (!Convert.IsDBNull(iteration_row["iautosoc"]))
							{
								StSql = StSql + 
								        "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
							}
							else
							{
								StSql = StSql + 
								        "(Null),";
							}
							
							if (!Convert.IsDBNull(iteration_row["iexiauto"]))
							{
								StSql = StSql + 
								        "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
							}
							else
							{
								StSql = StSql + 
								        "(Null),";
							}
							if (FechaFactur != "")
							{
								StSql = StSql + "" + Serrores.FormatFechaHMS(FechaFactur) + ",";
							}
							else
							{
								StSql = StSql + "" + Serrores.FormatFechaHMS(iteration_row["frealiza"].ToString()) + ",";
							}
							StSql = StSql + "'C'," + 
							        Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
							        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + ",";
							//Oscar C Agosto 2011. Copago
							if (Convert.IsDBNull(iteration_row["cpagpaci"]))
							{
								StSql = StSql + " (Null) ";
							}
							else
							{
								string tempRefParam22 = Convert.ToString(iteration_row["cpagpaci"]);
								StSql = StSql + " '" + Serrores.ConvertirDecimales(ref tempRefParam22, ref stSeparadorDecimalBD, 2) + "'";
							}
							//-----------
							StSql = StSql + ")";

							SqlCommand tempCommand_5 = new SqlCommand(StSql, Conexion);
							tempCommand_5.ExecuteNonQuery();


						}
						else
						{
							//OSCAR C DICIEMBRE 2008
							//A la hora de generar los movimientos de facturacion, se debe tener en cuenta el nombre de la tabla de movimientos
							//de facturacion. Si NO es DMOVFACT, funcionara como hasta ahora (stTablaFacturacion <> "DMOVFACT" del IF).
							//Si es DMOVFACT (este ELSE, se debera realizar lo siguiente):
							//- Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
							//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)
							//- Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
							//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
							// NOTA:
							// La condicion de busqueda sera por:
							// gcodeven, gsersoli, gserreal, gsocieda, nafiliac, gconcfac, ncantida, ffinicio,
							// ffechfin*, gdiagalt*, ginspecc, iregiqui*, gpersona, gserresp*, gcodiart*, gcodlote*,
							// itiposer, ganoregi, gnumregi, itiporig, ganoorig, gnumorig
							// y que ademas tuviera la marca de borrado logico (iborrado=NULL)
							//* No aplica
							StSql = "SELECT * FROM " + stTablaFacturacion_W + " WHERE ";
							StSql = StSql + " gcodeven = 'PR' ";
							if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
							{
								StSql = StSql + " AND itiposer ='" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "'" + " AND ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + " AND gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]);
								if (Convert.IsDBNull(iteration_row["gsersoli"]))
								{
									StSql = StSql + " AND gsersoli IS NULL ";
								}
								else
								{
									StSql = StSql + " AND gsersoli = " + Convert.ToString(iteration_row["gsersoli"]);
								}
							}
							else
							{
								StSql = StSql + " AND itiposer ='C'" + " AND ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + " AND gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]);
								if (ServicioSolicitantePrincipal == 0)
								{
									StSql = StSql + " AND gsersoli IS NULL ";
								}
								else
								{
									StSql = StSql + " AND gsersoli = " + ServicioSolicitantePrincipal.ToString();
								}
							}

							if (Convert.IsDBNull(iteration_row["gservici"]))
							{
								StSql = StSql + " AND gserreal IS NULL ";
							}
							else
							{
								StSql = StSql + " AND gserreal = " + Convert.ToString(iteration_row["gservici"]);
							}

							if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
							{
								StSql = StSql + " AND gsocieda = " + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]);
							}
							else
							{
								StSql = StSql + " AND gsocieda = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsocieda"]);
							}

							if (rsConsulta.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
							{
								StSql = StSql + " AND (nafiliac is null or nafiliac='')";
							}
							else
							{
								StSql = StSql + " AND nafiliac = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]) + "'";
							}

							StSql = StSql + " AND gconcfac ='" + PrestacionFactura + "'";
							StSql = StSql + " AND ncantida = " + iCantidad.ToString();

							StSql = StSql + " AND ffinicio = " + Serrores.FormatFechaHMS(iteration_row["frealiza"].ToString());

							//If rsConsulta!ihoscons = "H" Or IsNull(rsConsulta!gdiagnos) Then
							//    stSql = stSql & " AND gdiagalt IS NULL "
							//Else
							//    stSql = stSql & " AND gdiagalt = '" & rsConsulta!gdiagnos & "'"
							//End If

							if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ginspecc"]))
							{
								StSql = StSql + " AND ginspecc IS NULL ";
							}
							else
							{
								StSql = StSql + " AND ginspecc = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ginspecc"]) + "'";
							}

							if (!Convert.IsDBNull(iteration_row["gperreal"]))
							{
								StSql = StSql + " AND gpersona = " + Convert.ToString(iteration_row["gperreal"]);
							}
							else
							{
								StSql = StSql + " AND gpersona IS NULL ";
							}

							StSql = StSql + 
							        " AND itiporig = 'C' " + 
							        " AND ganoorig = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + 
							        " AND gnumorig = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + 
							        " AND iborrado ='S'";

							SqlDataAdapter tempAdapter_12 = new SqlDataAdapter(StSql, Conexion);
							rsFacturacion = new DataSet();
							tempAdapter_12.Fill(rsFacturacion);
							if (rsFacturacion.Tables[0].Rows.Count != 0)
							{

								//Si la actividad a facturar, existe en la tabla temporalt TablaFacturacion_W, se actualizarán todos los campos de la misma forma que
								//   que el insert en DMOVFACT original, pero quitando la marca de borrado (Iborrado=NULL)

								indicersFacturacion = rsFacturacion.MoveFirst();
								//MsgBox "encontrado"


								StSql = "UPDATE " + stTablaFacturacion_W + " SET ";
								StSql = StSql + " gidenpac ='" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gidenpac"]) + "', ";
								// Si el paciente no estaba hospitalizado se graba el episodio de la
								// consulta. Si estaba hospitalizado se graba el episodio principal
								if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
								{
									StSql = StSql + " itpacfac = 'I', " + 
									        " itiposer = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "'," + 
									        " ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + "," + 
									        " gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + "," + 
									        " gcodeven = 'PR',";
									
									if (Convert.IsDBNull(iteration_row["gsersoli"]))
									{
										StSql = StSql + " gsersoli = (Null),";
									}
									else
									{
										StSql = StSql + " gsersoli =  " + Convert.ToString(iteration_row["gsersoli"]) + ",";
									}
								}
								else
								{
									if (ServicioSolicitantePrincipal == 0)
									{
										StSql = StSql + " itpacfac = 'E',";
									}
									else
									{
										StSql = StSql + " itpacfac = 'I',";
									}
									StSql = StSql + " itiposer = 'C'," + 
									        " ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
									        " gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + "," + 
									        " gcodeven= 'PR',";
									if (ServicioSolicitantePrincipal == 0)
									{
										StSql = StSql + " gsersoli = (Null),";
									}
									else
									{
										StSql = StSql + " gsersoli = " + ServicioSolicitantePrincipal.ToString() + ",";
									}
								}
								StSql = StSql + " gserreal = " + Convert.ToString(iteration_row["gservici"]) + ",";
								// Modificación del 2/2/99: si no ha traído la autorización necesaria
								// la prestación se cobra como privado
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
								{
									StSql = StSql + " gsocieda = " + Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
								}
								else
								{
									StSql = StSql + " gsocieda = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsocieda"]) + ",";
								}
								if (rsConsulta.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
								{
									StSql = StSql + " nafiliac = (Null),";
								}
								else
								{
									StSql = StSql + " nafiliac = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]) + "',";
								}
								StSql = StSql + " gconcfac = '" + PrestacionFactura + "',";
								StSql = StSql + " ncantida = " + iCantidad.ToString() + ",";
								
								StSql = StSql + " fsistema = " + Serrores.FormatFechaHMS(FechaSistema) + ",";
								
								StSql = StSql + " ffinicio = " + Serrores.FormatFechaHMS(iteration_row["frealiza"].ToString()) + ",";
								if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ginspecc"]))
								{
									StSql = StSql + " ginspecc = (Null),";
								}
								else
								{
									StSql = StSql + " ginspecc = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ginspecc"]) + "',";
								}
								//oscar
								if (!Convert.IsDBNull(iteration_row["gperreal"]))
								{
									StSql = StSql + " gpersona = " + Convert.ToString(iteration_row["gperreal"]) + ",";
								}
								else
								{
									StSql = StSql + " gpersona = (Null),";
								}
								//-----
								if (rsConsulta.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
								{
									StSql = StSql + " ndninifp = (Null),";
								}
								else
								{
									StSql = StSql + " ndninifp = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]) + "',";
								}
								if (!Convert.IsDBNull(iteration_row["iprimsuc"]))
								{
									StSql = StSql + " iprimsuc = '" + Convert.ToString(iteration_row["iprimsuc"]) + "',";
								}
								else
								{
									StSql = StSql + " iprimsuc = (Null),";
								}
								if (!Convert.IsDBNull(iteration_row["iautosoc"]))
								{
									StSql = StSql + " iautosoc = '" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
								}
								else
								{
									StSql = StSql + " iautosoc = (Null),";
								}
								if (!Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									StSql = StSql + " iexiauto = '" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
								}
								else
								{
									StSql = StSql + " iexiauto = (Null),";
								}
								if (FechaFactur != "")
								{
									StSql = StSql + " fperfact = " + Serrores.FormatFechaHMS(FechaFactur) + ",";
								}
								else
								{
									StSql = StSql + " fperfact = " + Serrores.FormatFechaHMS(iteration_row["frealiza"].ToString()) + ",";
								}
								StSql = StSql + " itiporig = 'C', " + 
								        " ganoorig = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + 
								        " gnumorig = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + ",";
								StSql = StSql + " iborrado = (NULL), ";
								//Oscar C Agosto 2011. Copago
								if (Convert.IsDBNull(iteration_row["cpagpaci"]))
								{
									StSql = StSql + " cpagpaci = (Null) ";
								}
								else
								{
									string tempRefParam28 = Convert.ToString(iteration_row["cpagpaci"]);
									StSql = StSql + " cpagpaci = '" + Serrores.ConvertirDecimales(ref tempRefParam28, ref stSeparadorDecimalBD, 2) + "'";
								}
								//-----------
								StSql = StSql + " Where fila_id = " + Convert.ToString(rsFacturacion.Tables[0].Rows[indicersFacturacion]["fila_id"]);


								SqlCommand tempCommand_6 = new SqlCommand(StSql, Conexion);
								tempCommand_6.ExecuteNonQuery();


							}
							else
							{

								//Si la actividad a afacturar no existe en la tabla temporal TablaFacturacion_W, se debera insertar dicha actividad en la tabla
								//   temporal segun los datos del INSERT, en la que el campo fila_id y la marca de borrado quedará a NULL
								//MsgBox "No encontrado"

								StSql = "INSERT INTO " + stTablaFacturacion_W + " (gidenpac, itpacfac, itiposer, ganoregi, gnumregi, " + 
								        "gcodeven, gsersoli, gserreal, gsocieda, nafiliac, " + 
								        "gconcfac, ncantida, fsistema, ffinicio, ginspecc, " + 
								        "gpersona, ndninifp, iprimsuc, iautosoc, iexiauto, fperfact,itiporig,ganoorig,gnumorig,cpagpaci) ";

								StSql = StSql + "VALUES('" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gidenpac"]) + "',";

								// Si el paciente no estaba hospitalizado se graba el episodio de la
								// consulta. Si estaba hospitalizado se graba el episodio principal
								if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
								{
									StSql = StSql + "'I','" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "'," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + "," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + "," + "'PR',";
									if (Convert.IsDBNull(iteration_row["gsersoli"]))
									{
										StSql = StSql + "(Null),";
									}
									else
									{
										StSql = StSql + Convert.ToString(iteration_row["gsersoli"]) + ",";
									}
								}
								else
								{
									if (ServicioSolicitantePrincipal == 0)
									{
										StSql = StSql + "'E',";
									}
									else
									{
										StSql = StSql + "'I',";
									}
									StSql = StSql + "'C'," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + "," + "'PR',";
									if (ServicioSolicitantePrincipal == 0)
									{
										StSql = StSql + "(Null),";
									}
									else
									{
										StSql = StSql + ServicioSolicitantePrincipal.ToString() + ",";
									}
								}
								StSql = StSql + Convert.ToString(iteration_row["gservici"]) + ",";
								// Modificación del 2/2/99: si no ha traído la autorización necesaria
								// la prestación se cobra como privado
								if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"]))
								{
									StSql = StSql + 
									        Convert.ToString(rsPrivado.Tables[0].Rows[0]["nnumeri1"]) + ",";
								}
								else
								{
									StSql = StSql + 
									        Convert.ToString(rsConsulta.Tables[0].Rows[0]["gsocieda"]) + ",";
								}

								if (rsConsulta.Tables[0].Rows[0]["gsocieda"] == rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
								{
									StSql = StSql + "(Null),";
								}
								else
								{
									StSql = StSql + "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]) + "',";
								}

								StSql = StSql + "'" + PrestacionFactura + "'," + iCantidad.ToString() + ",";
								StSql = StSql + "" + Serrores.FormatFechaHMS(FechaSistema) + "," + "" + Serrores.FormatFechaHMS(iteration_row["frealiza"].ToString()) + ",";
								
								if (Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["ginspecc"]))
								{
									StSql = StSql + "(Null),";
								}
								else
								{
									StSql = StSql + "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ginspecc"]) + "',";
								}

								//oscar
								if (!Convert.IsDBNull(iteration_row["gperreal"]))
								{
									StSql = StSql + Convert.ToString(iteration_row["gperreal"]) + ",";
								}
								else
								{
									StSql = StSql + "(Null),";
								}
								//-----

								if (rsConsulta.Tables[0].Rows[0]["gsocieda"] != rsPrivado.Tables[0].Rows[0]["nnumeri1"] || Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["nafiliac"]) || Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]).Trim() == "" || (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iautosoc"]) && Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["iexiauto"])))
								{
									StSql = StSql + "(Null),";
								}
								else
								{
									StSql = StSql + "'" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["nafiliac"]) + "',";
								}
								if (!Convert.IsDBNull(iteration_row["iprimsuc"]))
								{
									StSql = StSql + "'" + Convert.ToString(iteration_row["iprimsuc"]) + "',";
								}
								else
								{
									StSql = StSql + "(Null),";
								}
								if (!Convert.IsDBNull(iteration_row["iautosoc"]))
								{
									StSql = StSql + "'" + Convert.ToString(iteration_row["iautosoc"]).Trim() + "',";
								}
								else
								{
									StSql = StSql + "(Null),";
								}
								if (!Convert.IsDBNull(iteration_row["iexiauto"]))
								{
									StSql = StSql + "'" + Convert.ToString(iteration_row["iexiauto"]).Trim() + "',";
								}
								else
								{
									StSql = StSql + "(Null),";
								}
                                if (FechaFactur != "")
                                {
                                    StSql = StSql + "" + Serrores.FormatFechaHMS(FechaFactur) + ",";
                                }
                                else
                                {
                                    StSql = StSql + "" + Serrores.FormatFechaHMS(iteration_row["frealiza"].ToString()) + ",";
                                }

								StSql = StSql + "'C'," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + "," + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + ",";
								//Oscar C Agosto 2011. Copago
								if (Convert.IsDBNull(iteration_row["cpagpaci"]))
								{
									StSql = StSql + " (Null) ";
								}
								else
								{
									string tempRefParam33 = Convert.ToString(iteration_row["cpagpaci"]);
									StSql = StSql + "'" + Serrores.ConvertirDecimales(ref tempRefParam33, ref stSeparadorDecimalBD, 2) + "'";
								}
								//-----------
								StSql = StSql + " )";
								SqlCommand tempCommand_7 = new SqlCommand(StSql, Conexion);
								tempCommand_7.ExecuteNonQuery();


							}
							rsFacturacion.Close();
							//-------------FIN OSCAR C Diciembre 2008-------------
						}

						if (ExisteProtocolo)
						{
							indicersDanalsol = rsDanalsol.MoveNext();
							if (indicersDanalsol < rsDanalsol.Tables[0].Rows.Count)
							{
								PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
								iCantidad = Convert.ToInt32(rsDanalsol.Tables[0].Rows[indicersDanalsol]["ncantida"]);
							}
							else
							{
								Continua_Bucle = false;
								rsDanalsol.Close();
							}
						}

					}

					rsOtrasPrestaciones.Edit();
					iteration_row["fpasfact"] = FechaSistema;
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_10);
					tempAdapter_10.Update(rsOtrasPrestaciones, rsOtrasPrestaciones.Tables[0].TableName);
				}

				rsPrivado.Close();
				rsConsulta.Close();
				rsOtrasPrestaciones.Close();


				//OSCAR C Diciembre 2008:
				//Una vez generados todos los movimientos de facturacion en la tabla temporal del tipo W_DMOVFACT_C20081,
				//se debera realizar lo siguiente, siempre y cuando la tabla temporal tenga registros:
				//    - Se deberá pasar de la tabla temporal de facturacion a la tabla de facturacion real (DMOVFACT), de acuerdo a:
				//        * Se borrara de DMOVFACT aquellos registros que coincidan con W_DMOVFACT por fila_id y que tengan la marca de borrado (iborrado='S')
				//        * Se modificara DMOVFACT actualizando todos los campos de aquellos registros que coincidan con la tabla temporal por fila_id y
				//            cuyo indicador bde borrado logico estubiera a NULO (iborrado = NULL)
				//        * Se insertatan los registrso en DMOVFACT de la tabla temporal aquellos registros cuya fila_id estubiera a NUL
				//    - Se borrará la tabla temporal
				DataSet rsfactura = null;
				StSql = "SELECT * FROM " + stTablaFacturacion_W;
				SqlDataAdapter tempAdapter_14 = new SqlDataAdapter(StSql, Conexion);
				rsfactura = new DataSet();
				tempAdapter_14.Fill(rsfactura);
				if (rsfactura.Tables[0].Rows.Count != 0)
				{
                    StSql = " DELETE DMOVFACT WHERE Fila_id IN" + 
					        "   (SELECT fila_id FROM " + stTablaFacturacion_W + " WHERE iborrado='S' and fila_id is NOT null)";
					SqlCommand tempCommand_8 = new SqlCommand(StSql, Conexion);
					tempCommand_8.ExecuteNonQuery();

					StSql = " UPDATE DMOVFACT SET " + 
					        " gidenpac = " + stTablaFacturacion_W + ".gidenpac, " + 
					        " itpacfac = " + stTablaFacturacion_W + ".itpacfac, " + 
					        " itiposer = " + stTablaFacturacion_W + ".itiposer, " + 
					        " ganoregi = " + stTablaFacturacion_W + ".ganoregi, " + 
					        " gnumregi = " + stTablaFacturacion_W + ".gnumregi, " + 
					        " gcodeven = " + stTablaFacturacion_W + ".gcodeven, " + 
					        " gsersoli = " + stTablaFacturacion_W + ".gsersoli, " + 
					        " gserreal = " + stTablaFacturacion_W + ".gserreal, " + 
					        " gsocieda = " + stTablaFacturacion_W + ".gsocieda, " + 
					        " nafiliac = " + stTablaFacturacion_W + ".nafiliac, " + 
					        " gconcfac = " + stTablaFacturacion_W + ".gconcfac, " + 
					        " ncantida = " + stTablaFacturacion_W + ".ncantida, " + 
					        " fsistema = " + stTablaFacturacion_W + ".fsistema, " + 
					        " ffinicio = " + stTablaFacturacion_W + ".ffinicio, " + 
					        " ffechfin = " + stTablaFacturacion_W + ".ffechfin, " + 
					        " ireingre = " + stTablaFacturacion_W + ".ireingre, ";
					StSql = StSql + 
					        " gtipocam = " + stTablaFacturacion_W + ".gtipocam, " + 
					        " gtipodie = " + stTablaFacturacion_W + ".gtipodie, " + 
					        " iregaloj = " + stTablaFacturacion_W + ".iregaloj, " + 
					        " gdiagalt = " + stTablaFacturacion_W + ".gdiagalt, " + 
					        " ginspecc = " + stTablaFacturacion_W + ".ginspecc, " + 
					        " iorigqui = " + stTablaFacturacion_W + ".iorigqui, " + 
					        " iregiqui = " + stTablaFacturacion_W + ".iregiqui, " + 
					        " gpersona = " + stTablaFacturacion_W + ".gpersona, " + 
					        " ndninifp = " + stTablaFacturacion_W + ".ndninifp, " + 
					        " iexitusp = " + stTablaFacturacion_W + ".iexitusp, " + 
					        " iprimsuc = " + stTablaFacturacion_W + ".iprimsuc, " + 
					        " gmotalta = " + stTablaFacturacion_W + ".gmotalta, " + 
					        " ipaseobs = " + stTablaFacturacion_W + ".ipaseobs, " + 
					        " ffactura = " + stTablaFacturacion_W + ".ffactura, " + 
					        " iautosoc = " + stTablaFacturacion_W + ".iautosoc, " + 
					        " iexiauto = " + stTablaFacturacion_W + ".iexiauto, " + 
					        " gserresp = " + stTablaFacturacion_W + ".gserresp, " + 
					        " fperfact = " + stTablaFacturacion_W + ".fperfact, ";
					StSql = StSql + 
					        " area = " + stTablaFacturacion_W + ".area, " + 
					        " gcodiart = " + stTablaFacturacion_W + ".gcodiart, " + 
					        " numfact = " + stTablaFacturacion_W + ".numfact, " + 
					        " anof = " + stTablaFacturacion_W + ".anof, " + 
					        " NUMFACTSOM = " + stTablaFacturacion_W + ".NUMFACTSOM, " + 
					        " anofsom = " + stTablaFacturacion_W + ".anofsom, " + 
					        " irepetid = " + stTablaFacturacion_W + ".irepetid, " + 
					        " idlinc = " + stTablaFacturacion_W + ".idlinc, " + 
					        " gcodlote = " + stTablaFacturacion_W + ".gcodlote, " + 
					        " itiporig = " + stTablaFacturacion_W + ".itiporig, " + 
					        " ganoorig = " + stTablaFacturacion_W + ".ganoorig, " + 
					        " gnumorig = " + stTablaFacturacion_W + ".gnumorig, " + 
					        " cpagpaci = " + stTablaFacturacion_W + ".cpagpaci ";
					StSql = StSql + 
					        " FROM DMOVFACT INNER JOIN " + stTablaFacturacion_W + " ON DMOVFACT.fila_id = " + stTablaFacturacion_W + ".fila_id " + 
					        " and " + stTablaFacturacion_W + ".iborrado Is Null AND " + 
					        stTablaFacturacion_W + ".fila_id Is NOT Null ";
					SqlCommand tempCommand_9 = new SqlCommand(StSql, Conexion);
					tempCommand_9.ExecuteNonQuery();


					StSql = " INSERT INTO DMOVFACT " + 
					        "  (gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal,gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre,gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona,ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto,gserresp,fperfact,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom,irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,cpagpaci)" + 
					        " SELECT " + 
					        "   gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal,gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre,gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona,ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto,gserresp,fperfact,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom,irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,cpagpaci" + 
					        " FROM " + stTablaFacturacion_W + 
					        " WHERE fila_id IS NULL ";
					SqlCommand tempCommand_10 = new SqlCommand(StSql, Conexion);
					tempCommand_10.ExecuteNonQuery();

				}
				rsfactura.Close();

				StSql = " DROP TABLE " + stTablaFacturacion_W;
				SqlCommand tempCommand_11 = new SqlCommand(StSql, Conexion);
				tempCommand_11.ExecuteNonQuery();

				// -------------- FIN OSCAR C Diciembre 2008 --------------------------
                
				Formulario.ProRecibirError(null, null);
			}
			catch (System.Exception excep)
			{
				Formulario.ProRecibirError(Information.Err().Number, excep.Message);
			}
		}

		public bool Facturado(int AñoRegistro, int NumeroRegistro, ref SqlConnection Conexion)
		{
			bool result = false;
			DataSet rsfactura = null;
			DataSet rsOtrasPrestaciones = null;
			DataSet rsDanalsol = null;
            Int32 indicersDanalsol = 0;
            bool ExisteProtocolo = false;
			string sql = String.Empty;
			bool Continua_Bucle = false;
			string PrestacionFactura = String.Empty;

			Serrores.vfMostrandoHistoricos = true;

			Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
			// Se obtiene los datos de la consulta
			StSql = "SELECT cconsult.* " + 
			        "FROM cconsult " + 
			        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
			        "gnumregi = " + NumeroRegistro.ToString();
			SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
			DataSet rsConsulta = new DataSet();
			tempAdapter.Fill(rsConsulta);
			// Si la prestacion principal se ha pasado a facturacion se comprueba
			// si esta marcada ya como facturada
			if (!Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["fpasfact"]) && rsConsulta.Tables[0].Rows.Count != 0)
			{
				//si la prestacion de la consulta es un protocolo hay que comprobar lo que hay en DANALSOL
				if (HayProtocolo(Convert.ToString(rsConsulta.Tables[0].Rows[0]["gprestac"]), Conexion))
				{
					ExisteProtocolo = true;
					sql = SqlPruebasDanalsol(Convert.ToString(rsConsulta.Tables[0].Rows[0]["gprestac"]), Convert.ToInt32(rsConsulta.Tables[0].Rows[0]["ganoregi"]), Convert.ToInt32(rsConsulta.Tables[0].Rows[0]["gnumregi"]), Conexion);
					//sql = "select * from danalsol where itiposer = 'C' and ganoregi=" & rsConsulta("ganoregi") & " and " _
					//& "gnumregi=" & rsConsulta("gnumregi") & " and gprestac = '" & rsConsulta("gprestac") & "' "

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
					rsDanalsol = new DataSet();
					tempAdapter_2.Fill(rsDanalsol);
					if (rsDanalsol.Tables[0].Rows.Count != 0)
					{
                        indicersDanalsol = rsDanalsol.MoveFirst();
					}
				}
				else
				{
					ExisteProtocolo = false;
					PrestacionFactura = Convert.ToString(rsConsulta.Tables[0].Rows[0]["gprestac"]);
				}
				Continua_Bucle = true;

				while (Continua_Bucle)
				{
					if (ExisteProtocolo)
					{
						PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
					}
					else
					{
						Continua_Bucle = false;
					}
					StSql = "SELECT ffactura FROM dmovfact ";
					// Si el paciente estaba hospitalizado se accede con el episodio
					// principal. En caso contrario se accede con el de consultas
					//*************
					if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
					{
						StSql = StSql + 
						        "WHERE itiposer = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + "' AND " + 
						        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
						        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " AND ";
					}
					else
					{
						StSql = StSql + 
						        "WHERE itiposer = 'C' AND " + 
						        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
						        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + " AND ";
					}
					
					StSql = StSql + 
					        "gcodeven = 'PR' AND " + 
					        "gconcfac = '" + PrestacionFactura + "' AND " + 
					        "fsistema = " + Serrores.FormatFechaHMS(rsConsulta.Tables[0].Rows[0]["fpasfact"].ToString()) + " AND " + 
					        "ffinicio = " + Serrores.FormatFechaHM(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString()) + "";

					string tempRefParam3 = "DMOVFACT";
					Serrores.PonerVistas(ref StSql, ref tempRefParam3, Conexion);

					if (Serrores.proExisteTabla("DMOVFACH", Conexion))
					{
						StSql = StSql + " UNION ALL " + Serrores.proReplace(ref StSql, "DMOVFACT", "DMOVFACH");
					}

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, Conexion);
					rsfactura = new DataSet();
					tempAdapter_3.Fill(rsfactura);
					if (rsfactura.Tables[0].Rows.Count != 0 && !Convert.IsDBNull(rsfactura.Tables[0].Rows[0]["ffactura"]))
					{
						result = true;
						if (ExisteProtocolo)
						{
							rsDanalsol.Close();
						}
						rsfactura.Close();
						rsConsulta.Close();
						return result;
					}
					else
					{
						if (ExisteProtocolo)
						{
						 	indicersDanalsol = rsDanalsol.MoveNext();
							if (indicersDanalsol < rsDanalsol.Tables[0].Rows.Count)
							{
								PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
							}
							else
							{
								Continua_Bucle = false;
							}

						}
						else
						{
							Continua_Bucle = false;
						}
					}
				}
				//comprobamos otras prestaciones
				// Se comprueba si realizaron otras prestaciones en la consulta y estan facturadas
				StSql = "SELECT epresalt.* " + 
				        "FROM epresalt " + 
				        "WHERE itiposer = 'C' AND " + 
				        "ganoregi = " + AñoRegistro.ToString() + " AND " + 
				        "gnumregi = " + NumeroRegistro.ToString();
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(StSql, Conexion);
				rsOtrasPrestaciones = new DataSet();
				tempAdapter_4.Fill(rsOtrasPrestaciones);

				foreach (DataRow iteration_row in rsOtrasPrestaciones.Tables[0].Rows)
				{
					if (HayProtocolo(Convert.ToString(iteration_row["gprestac"]), Conexion))
					{
						ExisteProtocolo = true;
						sql = SqlPruebasDanalsol(Convert.ToString(iteration_row["gprestac"]), Convert.ToInt32(iteration_row["ganoregi"]), Convert.ToInt32(iteration_row["gnumregi"]), Conexion);
						//sql = "select * from danalsol where itiposer = 'C' and ganoregi=" & rsOtrasPrestaciones("ganoregi") & " and " _
						//& "gnumregi=" & rsOtrasPrestaciones("gnumregi") & " and gprestac = '" & rsOtrasPrestaciones("gprestac") & "' "

						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(sql, Conexion);
						rsDanalsol = new DataSet();
						tempAdapter_5.Fill(rsDanalsol);
						if (rsDanalsol.Tables[0].Rows.Count != 0)
						{
							indicersDanalsol = rsDanalsol.MoveFirst();
						}
					}
					else
					{
						ExisteProtocolo = false;

						PrestacionFactura = Convert.ToString(iteration_row["gprestac"]);
					}
					Continua_Bucle = true;
					while (Continua_Bucle)
					{
						if (ExisteProtocolo)
						{
							PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
						}
						else
						{
							Continua_Bucle = false;
						}
						StSql = "SELECT ffactura " + 
						        "FROM dmovfact ";
						// Si el paciente estaba hospitalizado se accede con el episodio
						// principal. En caso contrario se accede con el de consultas
						if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
						{
							StSql = StSql + 
							        "WHERE itiposer = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + " ' AND " + 
							        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
							        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " AND ";
						}
						else
						{
							StSql = StSql + 
							        "WHERE itiposer = 'C' AND " + 
							        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
							        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + " AND ";
						}
						
						StSql = StSql + 
						        "gcodeven = 'PR' AND " + 
						        "gconcfac = '" + PrestacionFactura + "' AND " + 
						        "fsistema = " + Serrores.FormatFechaHMS(iteration_row["fpasfact"].ToString()) + " AND " + 
						        "ffinicio = " + Serrores.FormatFechaHM(iteration_row["frealiza"].ToString()) + "";

						string tempRefParam7 = "DMOVFACT";
						Serrores.PonerVistas(ref StSql, ref tempRefParam7, Conexion);

						if (Serrores.proExisteTabla("DMOVFACH", Conexion))
						{
							StSql = StSql + " UNION ALL " + Serrores.proReplace(ref StSql, "DMOVFACT", "DMOVFACH");
						}

						SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(StSql, Conexion);
						rsfactura = new DataSet();
						tempAdapter_6.Fill(rsfactura);
						if (rsfactura.Tables[0].Rows.Count != 0 && !Convert.IsDBNull(rsfactura.Tables[0].Rows[0]["ffactura"]))
						{
							result = true;
							if (ExisteProtocolo)
							{
								rsDanalsol.Close();
							}
							rsfactura.Close();
							rsOtrasPrestaciones.Close();
							rsConsulta.Close();
							return result;
						}
						else
						{
							if (ExisteProtocolo)
							{
								indicersDanalsol = rsDanalsol.MoveNext();
								if (indicersDanalsol < rsDanalsol.Tables[0].Rows.Count)
								{
									PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
								}
								else
								{
									Continua_Bucle = false;
									rsDanalsol.Close();
								}
							}
						}
					}
				}
				rsOtrasPrestaciones.Close();
			}
			rsConsulta.Close();
			return false;
		}

		public string Estado1(int AñoRegistro, int NumeroRegistro, SqlConnection Conexion, object Formulario)
		{
			return String.Empty;
		}

		//Oscar C Diciembre 2008;
		//El nuevo parametro bAnular indica que se ha accedido a la funcion estado desde la operacion de anular consulta.
		//No desde modificacion de actividad, por lo que en este caso si queremos que se borre DMOVFACT.
		public string Estado(int AñoRegistro, int NumeroRegistro, ref SqlConnection Conexion, dynamic Formulario, bool bAnular = false)
		{
			string result = String.Empty;
			DataSet rsConsulta = null;
			DataSet rsfactura = null;
			DataSet rsOtrasPrestaciones = null;
			DataSet rsDanalsol = null;
            Int32 indicersDanalsol = 0;
			bool ExisteProtocolo = false;
			string sql = String.Empty;
			bool Continua_Bucle = false;
			string PrestacionFactura = String.Empty;

			try
			{
				Serrores.vfMostrandoHistoricos = true;

				Serrores.Obtener_TipoBD_FormatoFechaBD(ref Conexion);
				result = "PENDIENTE";

				// Se obtiene los datos de la consulta
				StSql = "SELECT cconsult.* " + 
				        "FROM cconsult " + 
				        "WHERE ganoregi = " + AñoRegistro.ToString() + " AND " + 
				        "gnumregi = " + NumeroRegistro.ToString();
				SqlDataAdapter tempAdapter = new SqlDataAdapter(StSql, Conexion);
				rsConsulta = new DataSet();
				tempAdapter.Fill(rsConsulta);

				// Si la prestacion principal se ha pasado a facturacion se comprueba
				// si esta marcada ya como facturada. Si no lo esta se borra
				string stTabla = String.Empty;

                if (rsConsulta.Tables[0].Rows.Count != 0 && !Convert.IsDBNull(rsConsulta.Tables[0].Rows[0]["fpasfact"]))
				{
					//buscamos si las lineas que hay en DMOVFACT estan facturadas
					StSql = "SELECT ffactura FROM dmovfact ";
					// Si el paciente estaba hospitalizado se accede con el episodio
					// principal. En caso contrario se accede con el de consultas
					if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
					{
						StSql = StSql + 
						        "WHERE itiposer = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + " ' AND " + 
						        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
						        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " AND ";
					}
					else
					{
						StSql = StSql + 
						        "WHERE itiposer = 'C' AND " + 
						        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
						        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + " AND ";
					}
				
					StSql = StSql + 
					        "gcodeven = 'PR' AND " + 
					        "fsistema = " + Serrores.FormatFechaHMS(rsConsulta.Tables[0].Rows[0]["fpasfact"].ToString()) + " AND " + 
					        "ffinicio = " + Serrores.FormatFechaHM(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString()) + "";

					string tempRefParam3 = "DMOVFACT";
					Serrores.PonerVistas(ref StSql, ref tempRefParam3, Conexion);

					if (Serrores.proExisteTabla("DMOVFACH", Conexion))
					{
						StSql = StSql + " UNION ALL " + Serrores.proReplace(ref StSql, "DMOVFACT", "DMOVFACH");
					}

					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(StSql, Conexion);
					rsfactura = new DataSet();
					tempAdapter_2.Fill(rsfactura);
					if (rsfactura.Tables[0].Rows.Count != 0)
					{
						foreach (DataRow iteration_row in rsfactura.Tables[0].Rows)
						{
							if (!Convert.IsDBNull(iteration_row["ffactura"]))
							{
								result = "FACTURADO";
								rsfactura.Close();
								return result;
							}
							else
							{
								result = "GENERADO";
							}
						}
					}
					else
					{
						//no encuentra
					}
					rsfactura.Close();
					//nos aseguramos que lo ha encontrado
					StSql = "Select * FROM dmovfact ";
					StSql = StSql + " where itiporig = 'C' and ganoorig = " + AñoRegistro.ToString() + " ";
					StSql = StSql + "and gnumorig = " + NumeroRegistro.ToString() + " ";

					string tempRefParam5 = "DMOVFACT";
					Serrores.PonerVistas(ref StSql, ref tempRefParam5, Conexion);

					if (Serrores.proExisteTabla("DMOVFACH", Conexion))
					{
						StSql = StSql + " UNION ALL " + Serrores.proReplace(ref StSql, "DMOVFACT", "DMOVFACH");
					}

					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(StSql, Conexion);
					rsfactura = new DataSet();
					tempAdapter_3.Fill(rsfactura);

					if (rsfactura.Tables[0].Rows.Count != 0)
					{
						foreach (DataRow iteration_row_2 in rsfactura.Tables[0].Rows)
						{
							if (!Convert.IsDBNull(iteration_row_2["ffactura"]))
							{
								result = "FACTURADO";
								rsfactura.Close();
								return result;
							}
							else
							{
								result = "GENERADO";
							}
						}
					}
					rsfactura.Close();

					if (result == "GENERADO")
					{

						//OSCAR C Diciembre 2008
						//BORRAMOS LA TABLA TEMPORAL QUE SE UTILIZARA PARA REALIZAR LOS MOVIMIENTOS DE FACTURACION EN LA FUNCION
						//GENERARMOVIMIENTOSFACTURACION DE ESTA MISMA DLL
						stTabla = "W_DMOVFACT_C" + Conversion.Str(AñoRegistro).Trim() + Conversion.Str(NumeroRegistro).Trim();

						sql = "if exists (select * from sysobjects where name='" + stTabla + "'  and type='U') drop table " + stTabla;
						SqlCommand tempCommand = new SqlCommand(sql, Conexion);
						tempCommand.ExecuteNonQuery();

						proCrearTablaFacturacion(stTabla, Conexion);
						//-------------------------------

						//tenemos que borrar las lineas de DMOVFACT correspondientes

						//OSCAR C Diciembre 2008
						//EN LUGAR DE BORRAR LOS REGISTROS FISICOS DE DMOVFACT, SE TRASPASAN A LA NUEVA TABLA TEMPORAL.
						//LOS REGISTROS FISICOS DE DMOVFACT SE BORRARAN CUANTO TERMINE TODO EL PROCESO

						//stSql = "Delete FROM dmovfact "
						//stSql = stSql & " where itiporig = 'C' and ganoorig = " & AñoRegistro & " "
						//stSql = stSql & "and gnumorig = " & NumeroRegistro & " "

						StSql = "INSERT INTO " + stTabla + "(gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
						        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
						        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
						        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
						        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
						        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,IBORRADO, cpagpaci)";
						StSql = StSql + " select gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
						        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
						        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
						        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
						        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
						        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,'S',cpagpaci";
						StSql = StSql + " FROM DMOVFACT where " + 
						        "  itiporig = 'C' and ganoorig = " + AñoRegistro.ToString() + " and gnumorig = " + NumeroRegistro.ToString() + " ";
						StSql = StSql + "  AND fila_id NOT IN (SELECT fila_id from " + stTabla + ")";

						SqlCommand tempCommand_2 = new SqlCommand(StSql, Conexion);
						tempCommand_2.ExecuteNonQuery();

						//hasta aqui se borran las lineas correspondientes a toda la consulta
						//nos aseguramos que se borran todas las lineas correspondientes
						//buscamos si la prestacion principal de la consulta es un protocolo
						if (HayProtocolo(Convert.ToString(rsConsulta.Tables[0].Rows[0]["gprestac"]), Conexion))
						{
							ExisteProtocolo = true;
							sql = SqlPruebasDanalsol("", Convert.ToInt32(rsConsulta.Tables[0].Rows[0]["ganoregi"]), Convert.ToInt32(rsConsulta.Tables[0].Rows[0]["gnumregi"]), Conexion);
							//sql = "select * from danalsol where itiposer = 'C' and ganoregi=" & rsConsulta("ganoregi") & " and " _
							//& "gnumregi=" & rsConsulta("gnumregi") 'habria que ver la prestacion

							SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(sql, Conexion);
							rsDanalsol = new DataSet();
							tempAdapter_4.Fill(rsDanalsol);
							if (rsDanalsol.Tables[0].Rows.Count != 0)
							{
								indicersDanalsol = rsDanalsol.MoveFirst();
							}
						}
						else
						{
							ExisteProtocolo = false;
							PrestacionFactura = Convert.ToString(rsConsulta.Tables[0].Rows[0]["gprestac"]);
						}
						Continua_Bucle = true;
						while (Continua_Bucle)
						{
							if (ExisteProtocolo)
							{
								PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
							}
							else
							{
								Continua_Bucle = false;
							}

							//OSCAR C Diciembre 2008
							//EN LUGAR DE BORRAR LOS REGISTROS FISICOS DE DMOVFACT, SE TRASPASAN A LA NUEVA TABLA TEMPORAL.
							//LOS REGISTROS FISICOS DE DMOVFACT SE BORRARAN CUANTO TERMINE TODO EL PROCESO
							//stSql = "DELETE dmovfact where "
							StSql = "INSERT INTO " + stTabla + "(gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
							        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
							        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
							        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
							        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
							        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,IBORRADO,cpagpaci)";
							StSql = StSql + " select gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
							        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
							        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
							        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
							        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
							        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,'S',cpagpaci";
							StSql = StSql + " FROM DMOVFACT where ";
							//--------------------------

							// Si el paciente estaba hospitalizado se accede con el episodio
							// principal. En caso contrario se accede con el de consultas
							if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
							{
								StSql = StSql + 
								        "itiposer = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + " ' AND " + 
								        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
								        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " AND ";
							}
							else
							{
								StSql = StSql + 
								        "itiposer = 'C' AND " + 
								        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
								        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + " AND ";
							}
							
							StSql = StSql + 
							        "gcodeven = 'PR' AND " + 
							        "gconcfac = '" + PrestacionFactura + "' AND " + 
							        "ffinicio = " + Serrores.FormatFechaHM(rsConsulta.Tables[0].Rows[0]["ffeccita"].ToString()) + "and " + 
							        "fsistema = " + Serrores.FormatFechaHMS(rsConsulta.Tables[0].Rows[0]["fpasfact"].ToString()) + " ";
							StSql = StSql + " AND fila_id NOT IN (SELECT fila_id from " + stTabla + ")";
							SqlCommand tempCommand_3 = new SqlCommand(StSql, Conexion);
							tempCommand_3.ExecuteNonQuery();

							if (ExisteProtocolo)
							{
								indicersDanalsol = rsDanalsol.MoveNext();
								if (indicersDanalsol < rsDanalsol.Tables[0].Rows.Count)
								{
									PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
								}
								else
								{
									Continua_Bucle = false;
									rsDanalsol.Close();
								}
							}
						}

						//A continuacion comprobamos si hay otras prestaciones
						StSql = "SELECT epresalt.* " + 
						        "FROM epresalt " + 
						        "WHERE itiposer = 'C' AND " + 
						        "ganoregi = " + AñoRegistro.ToString() + " AND " + 
						        "gnumregi = " + NumeroRegistro.ToString();
						SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(StSql, Conexion);
						rsOtrasPrestaciones = new DataSet();
						tempAdapter_5.Fill(rsOtrasPrestaciones);

						foreach (DataRow iteration_row_3 in rsOtrasPrestaciones.Tables[0].Rows)
						{
							if (HayProtocolo(Convert.ToString(iteration_row_3["gprestac"]), Conexion))
							{
								ExisteProtocolo = true;
								sql = SqlPruebasDanalsol("", Convert.ToInt32(iteration_row_3["ganoregi"]), Convert.ToInt32(iteration_row_3["gnumregi"]), Conexion);
								//sql = "select * from danalsol where itiposer = 'C' and ganoregi=" & rsOtrasPrestaciones("ganoregi") & " and " _
								//& "gnumregi=" & rsOtrasPrestaciones("gnumregi")
								SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql, Conexion);
								rsDanalsol = new DataSet();
								tempAdapter_6.Fill(rsDanalsol);
								if (rsDanalsol.Tables[0].Rows.Count != 0)
								{
									indicersDanalsol = rsDanalsol.MoveFirst();
								}
							}
							else
							{
								ExisteProtocolo = false;
								PrestacionFactura = Convert.ToString(iteration_row_3["gprestac"]);
							}
							Continua_Bucle = true;
							while (Continua_Bucle)
							{
								if (ExisteProtocolo)
								{
									PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
								}
								else
								{
									Continua_Bucle = false;
								}

								//OSCAR C Diciembre 2008
								//EN LUGAR DE BORRAR LOS REGISTROS FISICOS DE DMOVFACT, SE TRASPASAN A LA NUEVA TABLA TEMPORAL.
								//LOS REGISTROS FISICOS DE DMOVFACT SE BORRARAN CUANTO TERMINE TODO EL PROCESO
								//stSql = "DELETE dmovfact where "
								StSql = "INSERT INTO " + stTabla + "(gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
								        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
								        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
								        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
								        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
								        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,IBORRADO,cpagpaci)";
								StSql = StSql + " select gidenpac,itpacfac,itiposer,ganoregi,gnumregi,gcodeven,gsersoli,gserreal," + 
								        "gsocieda,nafiliac,gconcfac,ncantida,fsistema,ffinicio,ffechfin,ireingre," + 
								        "gtipocam,gtipodie,iregaloj,gdiagalt,ginspecc,iorigqui,iregiqui,gpersona," + 
								        "ndninifp,iexitusp,iprimsuc,gmotalta,ipaseobs,ffactura,iautosoc,iexiauto," + 
								        "gserresp,fperfact,fila_id,area,gcodiart,numfact,anof,NUMFACTSOM,anofsom," + 
								        "irepetid,idlinc,gcodlote,itiporig,ganoorig,gnumorig,'S',cpagpaci";
								StSql = StSql + " FROM DMOVFACT where ";
								//--------------------------


								// Si el paciente estaba hospitalizado se accede con el episodio
								// principal. En caso contrario se accede con el de consultas
								if (Convert.ToString(rsConsulta.Tables[0].Rows[0]["ihoscons"]) == "H")
								{
									StSql = StSql + 
									        "itiposer = '" + Convert.ToString(rsConsulta.Tables[0].Rows[0]["itiposer"]) + " ' AND " + 
									        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoprin"]) + " AND " + 
									        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumprin"]) + " AND ";
								}
								else
								{
									StSql = StSql + 
									        "itiposer = 'C' AND " + 
									        "ganoregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["ganoregi"]) + " AND " + 
									        "gnumregi = " + Convert.ToString(rsConsulta.Tables[0].Rows[0]["gnumregi"]) + " AND ";
								}
								StSql = StSql + 
								        "gcodeven = 'PR' AND " + 
								        "gconcfac = '" + PrestacionFactura + "' AND " + 
								        "ffinicio = " + Serrores.FormatFechaHM(iteration_row_3["frealiza"].ToString()) + " and " + 
								        "fsistema = " + Serrores.FormatFechaHMS(iteration_row_3["fpasfact"].ToString()) + " ";
								StSql = StSql + " AND fila_id NOT IN (SELECT fila_id from " + stTabla + ")";

								SqlCommand tempCommand_4 = new SqlCommand(StSql, Conexion);
								tempCommand_4.ExecuteNonQuery();

								if (ExisteProtocolo)
								{
									indicersDanalsol = rsDanalsol.MoveNext();
									if (indicersDanalsol < rsDanalsol.Tables[0].Rows.Count )
									{
										PrestacionFactura = Convert.ToString(rsDanalsol.Tables[0].Rows[indicersDanalsol]["gpruelab"]);
									}
									else
									{
										Continua_Bucle = false;
										rsDanalsol.Close();
									}
								}
							}
						}
						rsOtrasPrestaciones.Close();


						//OSCAR C Diciembre 2008
						//SI SE HA ACCEDIDO DESDE UNA OPERACION DE ANULAR (bANULAR=true)
						//SE BORRARAN LOS REGISTROS FISICOS DE DMOVFACT QUE ESTEN EN LA NUEVA TABLA TEMPORAL.
						//bAnular viene a true desde:
						//    - Funciona Anular de la Clase RegModCon de la dll RegModConsulta.dll
						//    - Funcion Anular de la Clase MCAnulaConsultaVitro de la dll AnulaConsultaVITRO.dll
						//    - Boton Aceptar de la pantalla de Captura de Actividad Radiologica de Rayos.exe cuando no se realiza examen.
						if (bAnular)
						{
							StSql = " DELETE DMOVFACT WHERE fila_id IN (SELECT fila_id from " + stTabla + ")";
							SqlCommand tempCommand_5 = new SqlCommand(StSql, Conexion);
							tempCommand_5.ExecuteNonQuery();

							StSql = " DROP TABLE " + stTabla;
							SqlCommand tempCommand_6 = new SqlCommand(StSql, Conexion);
							tempCommand_6.ExecuteNonQuery();
						}
						//-----------------------


					}
					else
					{
						//no esta generado
					}

				}
				else
				{

				}
                
				rsConsulta.Close();
				Formulario.ProRecibirError(null, null);
				return result;
			}
			catch (System.Exception excep)
			{
				Formulario.ProRecibirError(Information.Err().Number, excep.Message);
				return result;
			}
		}
        
		public bool HayProtocolo(string Prestacion, SqlConnection Conexion)
		{
			bool result = false;
			string sql = "select * from dprotlab where gprestac='" + Prestacion + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet rsProtocolo = new DataSet();
			tempAdapter.Fill(rsProtocolo);
			if (rsProtocolo.Tables[0].Rows.Count != 0)
			{
				sql = "select * from dcodpres where gprestac='" + Prestacion + "' and iprotcer = 'C' ";
				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, Conexion);
				rsProtocolo = new DataSet();
				tempAdapter_2.Fill(rsProtocolo);
				result = (rsProtocolo.Tables[0].Rows.Count == 0);

			}
			else
			{
				result = false;
			}
			rsProtocolo.Close();
			return result;
		}

		public string SqlPruebasDanalsol(string Prestacion, int AñoRegistro, int NumeroRegistro, SqlConnection Conexion)
		{

			string result = String.Empty;
			string sql = "SELECT fmecaniz,ffeccita from CCONSULT where ganoregi = " + AñoRegistro.ToString() + " and gnumregi=" + NumeroRegistro.ToString();

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, Conexion);
			DataSet rrAux = new DataSet();
			tempAdapter.Fill(rrAux);

			result = "" + 
			         "select  itiposer,ganoregi,gnumregi,fpeticio,gprestac,finivali,gpruelab,finivalp, " + 
			         "    ncantida " + 
			         "From danalsol " + 
			         "Where itiposer='C' and ganoregi = " + AñoRegistro.ToString() + " And gnumregi =  " + NumeroRegistro.ToString() + 
			         "   and  (fpeticio=" + Serrores.FormatFechaHMS(rrAux.Tables[0].Rows[0]["fmecaniz"].ToString());
			if (Serrores.ObternerValor_CTEGLOBAL(Conexion, "OPENLAB", "VALFANU1").Trim().ToUpper() == "S")
			{
				result = result + ")";
			}
			else
			{
				result = result + " or fpeticio= " + Serrores.FormatFechaHMS(rrAux.Tables[0].Rows[0]["ffeccita"].ToString()) + ")";
			}
			result = result + ((Prestacion == "") ? "" : " AND danalsol.GPRESTAC = '" + Prestacion + "'") + "  And Not Exists (" + 
			         "            select '' " + 
			         "            From dcodpres " + 
			         "            inner join dprotlab on dcodpres.gprestac = dprotlab.gprestac " + 
			         "            where iprotcer='C' and danalsol.gpruelab = dprotlab.gpruelab ) ";

			result = result + " Union All ";

			result = result + 
			         "select  distinct danalsol.itiposer,danalsol.ganoregi,danalsol.gnumregi,danalsol.fpeticio, " + 
			         "    danalsol.gprestac,danalsol.finivali,dprotlab.gprestac,dprotlab.finivali finivalp, " + 
			         "    ncantida " + 
			         "From danalsol " + 
			         "inner join dprotlab on  danalsol.gpruelab = dprotlab.gpruelab and dprotlab.gprestac in " + 
			         "           (select dcodpres.gprestac from dcodpres " + 
			         "            inner join dprotlab on dcodpres.gprestac = dprotlab.gprestac " + 
			         "            where iprotcer='C' and danalsol.gpruelab = dprotlab.gpruelab) " + 
			         "Where itiposer='C' and ganoregi = " + AñoRegistro.ToString() + " And gnumregi =  " + NumeroRegistro.ToString() + 
			         "  and (fpeticio=" + Serrores.FormatFechaHMS(rrAux.Tables[0].Rows[0]["fmecaniz"].ToString());

			if (Serrores.ObternerValor_CTEGLOBAL(Conexion, "OPENLAB", "VALFANU1").Trim().ToUpper() == "S")
			{
				result = result + ")";
			}
			else
			{
				result = result + " or fpeticio= " + Serrores.FormatFechaHMS(rrAux.Tables[0].Rows[0]["ffeccita"].ToString()) + ")";
			}

			result = result + ((Prestacion == "") ? "" : " AND danalsol.GPRESTAC = '" + Prestacion + "'") + "  And Exists (" + 
			         "           select * from dcodpres " + 
			         "           inner join dprotlab on dcodpres.gprestac = dprotlab.gprestac " + 
			         "           where iprotcer='C' and danalsol.gpruelab = dprotlab.gpruelab )";

			rrAux.Close();

			return result;
		}

        private void proCrearTablaFacturacion(string stTabla, SqlConnection Conexion)
		{
			string sql = " CREATE TABLE " + stTabla + "(" + 
			             "gidenpac char (13)  NOT NULL , " + 
			             "itpacfac char (1)  NULL , " + 
			             "itiposer char (1)  NOT NULL ," + 
			             "ganoregi smallint NOT NULL ," + 
			             "gnumregi int NOT NULL ," + 
			             "gcodeven char (2)  NULL ," + 
			             "gsersoli smallint NULL ," + 
			             "gserreal smallint NULL ," + 
			             "gsocieda smallint NOT NULL ," + 
			             "nafiliac char (20)  NULL ," + 
			             "gconcfac char (10)  NULL ," + 
			             "ncantida numeric(14, 2) NULL ," + 
			             "fsistema datetime NOT NULL ," + 
			             "ffinicio datetime NULL ," + 
			             "ffechfin datetime NULL ," + 
			             "ireingre char (1)  NULL ," + 
			             "gtipocam smallint NULL ," + 
			             "gtipodie smallint NULL ," + 
			             "iregaloj char (1)  NULL ," + 
			             "gdiagalt char (8)  NULL ," + 
			             "ginspecc char (4)  NULL ," + 
			             "iorigqui char (1)  NULL ," + 
			             "iregiqui char (1)  NULL ," + 
			             "gpersona int NULL ,";
			sql = sql + 
			      "ndninifp char (9)  NULL ," + 
			      "iexitusp char (1)  NULL ," + 
			      "iprimsuc char (1)  NULL ," + 
			      "gmotalta smallint NULL ," + 
			      "ipaseobs char (1)  NULL ," + 
			      "ffactura datetime NULL ," + 
			      "iautosoc char (1)  NULL ," + 
			      "iexiauto char (1)  NULL ," + 
			      "gserresp smallint NULL ," + 
			      "fperfact datetime NOT NULL ," + 
			      "fila_id int  NULL ," + 
			      "area char (1)  NULL ," + 
			      "gcodiart varchar (10)  NULL ," + 
			      "numfact varchar (10)  NULL ," + 
			      "anof char (4)  NULL ," + 
			      "NUMFACTSOM varchar (10)  NULL ," + 
			      "anofsom char (4)  NULL ," + 
			      "irepetid varchar (1)  NULL ," + 
			      "idlinc int NULL ," + 
			      "gcodlote varchar (10)  NULL ," + 
			      "itiporig varchar (1)  NULL ," + 
			      "ganoorig smallint NULL ," + 
			      "gnumorig int NULL, " + 
			      "IBORRADO char (1)  NULL, ";
			sql = sql + 
			      "cpagpaci float NULL )";

			SqlCommand tempCommand = new SqlCommand(sql, Conexion);
			tempCommand.ExecuteNonQuery();
		}
	}
}