using System;
using System.Windows.Forms;
using Telerik.WinControls;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ConfiguracionRegional
{
	public class clsConfigRegional
	{



		// CONSTANTES UTILIZADAS PARA OBTENER VALORES DE LA CONFIGURACION
		// REGIONAL DEL ORDENADOR
		const int LOCALE_SYSTEM_DEFAULT = 0x800;
		const int LOCALE_SMONTHOUSANDSEP = 0x17; //  separador de miles en moneda
		const int LOCALE_ICENTURY = 0x24; //  especificador de formato de siglo
		const int LOCALE_SMONDECIMALSEP = 0x16; //  separador de decimales en moneda
		const int LOCALE_STIMEFORMAT = 0x1003; //  cadena de formato de hora
		const int LOCALE_SSHORTDATE = 0x1F; //  cadena de formato de fecha corta
		const int LOCALE_SDATE = 0x1D; //  separador de fecha
		const int LOCALE_STIME = 0x1E; //  separador de hora


		const string SEPARADOR_HORA = ":";
		const string SEPARADOR_FECHA = "/";
		const string FORMATO_FECHA = "dd/MM/yyyy";
		const string FORMATO_HORA = "HH:mm:ss";
		const string SEPARADOR_MILES = ".";
		const string SEPARADOR_DECIMAL = ",";
		const int MOSTRAR_SIGLO4CIFRAS = 1;

		public void ConfigurarEquipo()
		{
			string tempRefParam = SEPARADOR_HORA;
			int iCodError = 0;
			string tempRefParam2 = SEPARADOR_FECHA;
			string tempRefParam3 = FORMATO_FECHA;
			string tempRefParam4 = FORMATO_HORA;
			try
			{
				iCodError = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIME, ref tempRefParam);
			}
			catch
			{
			}
			if (iCodError == 0)
			{
				try
				{
					MostrarError();
					return;
				}
				catch
				{
				}
			}
			try
			{
				iCodError = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDATE, ref tempRefParam2);
			}
			catch
			{
			}
			if (iCodError == 0)
			{
				try
				{
					MostrarError();
					return;
				}
				catch
				{
				}
			}
			try
			{
				iCodError = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SSHORTDATE, ref tempRefParam3);
			}
			catch
			{
			}
			if (iCodError == 0)
			{
				try
				{
					MostrarError();
					return;
				}
				catch
				{
				}
			}
			try
			{
				iCodError = UpgradeSupportHelper.PInvoke.SafeNative.kernel32.SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_STIMEFORMAT, ref tempRefParam4);
			}
			catch
			{
			}
			if (iCodError == 0)
			{
				try
				{
					MostrarError();
					return;
				}
				catch
				{
				}
			}
			//iCodError = SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SMONTHOUSANDSEP, SEPARADOR_MILES)
			//If iCodError = 0 Then
			//    MostrarError
			//    Exit Sub
			//End If
			//iCodError = SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SMONDECIMALSEP, SEPARADOR_DECIMAL)
			//If iCodError = 0 Then
			//    MostrarError
			//    Exit Sub
			//End If
			//iCodError = SetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ICENTURY, MOSTRAR_SIGLO4CIFRAS)
			//If iCodError = 0 Then
			//    MostrarError
			//    Exit Sub
			//End If
		}


		private void MostrarError()
		{
			RadMessageBox.Show("Error configurando equipo cliente, consulte con su administrador", Application.ProductName, MessageBoxButtons.OK, RadMessageIcon.Info);
		}
	}
}