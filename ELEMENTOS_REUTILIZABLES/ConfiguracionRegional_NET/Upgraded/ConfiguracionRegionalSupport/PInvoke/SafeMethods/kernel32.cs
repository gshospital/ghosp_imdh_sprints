using System.Runtime.InteropServices;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ConfiguracionRegionalSupport.PInvoke.SafeNative
{
	public static class kernel32
	{

		public static int SetLocaleInfo(int Locale, int LCType, ref string lpLCData)
		{
			return ConfiguracionRegionalSupport.PInvoke.UnsafeNative.kernel32.SetLocaleInfo(Locale, LCType, ref lpLCData);
		}
	}
}