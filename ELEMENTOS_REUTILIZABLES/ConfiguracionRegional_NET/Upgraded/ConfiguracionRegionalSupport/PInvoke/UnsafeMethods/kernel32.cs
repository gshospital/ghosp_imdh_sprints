using System.Runtime.InteropServices;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace ConfiguracionRegionalSupport.PInvoke.UnsafeNative
{
	[System.Security.SuppressUnmanagedCodeSecurity]
	public static class kernel32
	{

		[DllImport("kernel32.dll", EntryPoint = "SetLocaleInfoA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static int SetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLCData);
	}
}