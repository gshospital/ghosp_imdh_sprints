using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;
using Telerik.WinControls;


namespace ImpNotificarEnf
{
	public class ClImpNotificarEnf
	{

		SqlConnection RcValoracion = null;
		string stGrupoHo = String.Empty;
		string stNombreHo = String.Empty;
		string stDireccHo = String.Empty;
		string stNomServicio = String.Empty;
		//variables para el listado de notificacion

		string vstNombrePaciente = String.Empty; //nombre y apellidos del paciente
		string vstNumeroSS = String.Empty; //
		string vstNumeroDNI = String.Empty;
		string vstLiteralDNI = String.Empty;
		string vstDireccionPaciente = String.Empty;
		string vstPoblacion = String.Empty;
		string vstProvincia = String.Empty;
		string vstFNaciPaciente = String.Empty; //fecha de nacimiento
		//Dim stDescSexo          As String 'descripcion del sexo del paciente
		//Dim stNombreHo As String
		//Dim stDireccHo As String
		string vstFallecimiento = String.Empty; //si/no & Fecha
		string stIdPaciente = String.Empty; //gidenpac del paciente
		string stCodigoPD = String.Empty;
		string stFechaValidez = String.Empty;
		string vstDiagnosticoNotifica = String.Empty;
		string stiTipoSer = String.Empty; //itiposer le llega cuando le llaman
		int iA�oRegistro = 0; //a�o del registro
		int iNumeroRegistro = 0; //n�mero del registro
		string vstNombreNotifica = String.Empty;
		string vstFechaNotifica = String.Empty;
		string vstDireccionNotifica = String.Empty;
		string vstTelefonoNotifica = String.Empty;
		string vstCargoNotifica = String.Empty;
		string vstMedicoResponsable = String.Empty;
		string vstMedicoTelefono = String.Empty;
		string stpathBD = String.Empty;
		string stNombreBD = String.Empty;
		//UPGRADE_ISSUE: (2068) Database object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		//PARIO TODO_X_5 BD ACCESS
        /*Database bs = null;
		//UPGRADE_ISSUE: (2068) Recordset object was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2068.aspx
		Recordset Rs = null;*/
		string stUsuario = String.Empty;
		string stTablaListadoValoracion = String.Empty;

        dynamic LISTADO_NOTIFICACION = null;
		string stSexo = String.Empty;
		string stDSNACCESS = String.Empty;
		string stFORMFILI = String.Empty;
		string VstCrystal_C = String.Empty;
		string gUsuario_C = String.Empty;
		string gContrase�a_C = String.Empty;

		bool vfHistoriaClinica = false;
		//UPGRADE_NOTE: (7001) The following declaration (DescripcionPoblacion) seems to be dead code More Information: http://www.vbtonet.com/ewis/ewi7001.aspx
		//private string DescripcionPoblacion(string stCodigo)
		//{
				//
				//string result = String.Empty;
				//result = "";
				//string sql = "select dpoblaci from DPOBLACI where gpoblaci = '" + stCodigo + "' ";
				//
				//SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
				//DataSet RrSql = new DataSet();
				//tempAdapter.Fill(RrSql);
				//if (RrSql.Tables[0].Rows.Count != 0)
				//{
					////UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
					//result = Convert.ToString(RrSql.Tables[0].Rows[0]["dpoblaci"]).Trim();
				//}
				////UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				//RrSql.Close();
				//
				//return result;
		//}
		public void Iniciar(SqlConnection Conexion, string stCaminoBD, string stBaseDatos, object oCristal, string stACCESS, string stUsua, string stTabla, string stNombrePaciente, string stFNaciPaciente, string stSex, string stIdPac, string stCodDiag, string stFechValDiag, string stEpisodio, ref string stModoImpresion, ref object stActualizaImp_optional, string VstCrystal, string gUsuario, string gContrase�a, string IdiomaPaciente, bool HistoriaClinica, bool MostrarHistoricos)
		{
			string stActualizaImp = (stActualizaImp_optional == Type.Missing) ? String.Empty : stActualizaImp_optional as string;
			try
			{

				vfHistoriaClinica = HistoriaClinica;
				Serrores.vfMostrandoHistoricos = MostrarHistoricos;

				RcValoracion = Conexion;
				Serrores.AjustarRelojCliente(Conexion);
				stpathBD = stCaminoBD;
				stNombreBD = stBaseDatos;
				LISTADO_NOTIFICACION = oCristal;
				stDSNACCESS = stACCESS;
				stUsuario = stUsua;
				vstNombrePaciente = stNombrePaciente;
				vstFNaciPaciente = stFNaciPaciente;
				stTablaListadoValoracion = stTabla;
				stSexo = stSex;
				stIdPaciente = stIdPac;
				stCodigoPD = stCodDiag;
				stFechaValidez = stFechValDiag;
				stiTipoSer = stEpisodio.Substring(0, Math.Min(1, stEpisodio.Length));
				if (stEpisodio.Trim().Length > 1)
				{
					iA�oRegistro = Convert.ToInt32(Double.Parse(stEpisodio.Substring(1, Math.Min(4, stEpisodio.Length - 1))));
					iNumeroRegistro = Convert.ToInt32(Double.Parse(stEpisodio.Substring(5, Math.Min(stEpisodio.Length - 5, stEpisodio.Length - 5))));
				}
				else
				{
					iA�oRegistro = 0;
					iNumeroRegistro = 0;
				}

				VstCrystal_C = VstCrystal;
				gUsuario_C = gUsuario;
				gContrase�a_C = gContrase�a;

				
				if (String.IsNullOrEmpty(IdiomaPaciente))
				{
					Serrores.CampoIdioma = "IDIOMA0"; //idioma del paciente para el multilenguaje
				}
				else
				{
					Serrores.CampoIdioma = IdiomaPaciente;
				}

				//FORMATO DE FILIACION
				SqlDataAdapter tempAdapter = new SqlDataAdapter("Select valfanu1 from sConsGlo where gConsGlo = 'FORMFILI'", RcValoracion);
				DataSet Rrglobal = new DataSet();
				tempAdapter.Fill(Rrglobal);
				
				stFORMFILI = Convert.ToString(Rrglobal.Tables[0].Rows[0]["valfanu1"]).Trim();
			
				Rrglobal.Close();

				if (stActualizaImp_optional == Type.Missing)
				{
					stActualizaImp = "S";
				}
				if (false)
				{
					stModoImpresion = "Pantalla";
				}

				ListadoNotificacion(stModoImpresion, stActualizaImp);
			}
			finally
			{
				stActualizaImp_optional = stActualizaImp;
			}


		}

		public void Iniciar(SqlConnection Conexion, string stCaminoBD, string stBaseDatos, object oCristal, string stACCESS, string stUsua, string stTabla, string stNombrePaciente, string stFNaciPaciente, string stSex, string stIdPac, string stCodDiag, string stFechValDiag, string stEpisodio, ref string stModoImpresion, ref object stActualizaImp_optional, string VstCrystal, string gUsuario, string gContrase�a, string IdiomaPaciente, bool HistoriaClinica)
		{
			Iniciar(Conexion, stCaminoBD, stBaseDatos, oCristal, stACCESS, stUsua, stTabla, stNombrePaciente, stFNaciPaciente, stSex, stIdPac, stCodDiag, stFechValDiag, stEpisodio, ref stModoImpresion, ref stActualizaImp_optional, VstCrystal, gUsuario, gContrase�a, IdiomaPaciente, HistoriaClinica, false);
		}

		public void Iniciar(SqlConnection Conexion, string stCaminoBD, string stBaseDatos, object oCristal, string stACCESS, string stUsua, string stTabla, string stNombrePaciente, string stFNaciPaciente, string stSex, string stIdPac, string stCodDiag, string stFechValDiag, string stEpisodio, ref string stModoImpresion, ref object stActualizaImp_optional, string VstCrystal, string gUsuario, string gContrase�a, string IdiomaPaciente)
		{
			Iniciar(Conexion, stCaminoBD, stBaseDatos, oCristal, stACCESS, stUsua, stTabla, stNombrePaciente, stFNaciPaciente, stSex, stIdPac, stCodDiag, stFechValDiag, stEpisodio, ref stModoImpresion, ref stActualizaImp_optional, VstCrystal, gUsuario, gContrase�a, IdiomaPaciente, false, false);
		}

		public void Iniciar(SqlConnection Conexion, string stCaminoBD, string stBaseDatos, object oCristal, string stACCESS, string stUsua, string stTabla, string stNombrePaciente, string stFNaciPaciente, string stSex, string stIdPac, string stCodDiag, string stFechValDiag, string stEpisodio, ref string stModoImpresion, ref object stActualizaImp_optional, string VstCrystal, string gUsuario, string gContrase�a)
		{
			Iniciar(Conexion, stCaminoBD, stBaseDatos, oCristal, stACCESS, stUsua, stTabla, stNombrePaciente, stFNaciPaciente, stSex, stIdPac, stCodDiag, stFechValDiag, stEpisodio, ref stModoImpresion, ref stActualizaImp_optional, VstCrystal, gUsuario, gContrase�a, String.Empty, false, false);
		}

		public void Iniciar(SqlConnection Conexion, string stCaminoBD, string stBaseDatos, object oCristal, string stACCESS, string stUsua, string stTabla, string stNombrePaciente, string stFNaciPaciente, string stSex, string stIdPac, string stCodDiag, string stFechValDiag, string stEpisodio, ref string stModoImpresion, ref object stActualizaImp_optional, string VstCrystal, string gUsuario)
		{
			Iniciar(Conexion, stCaminoBD, stBaseDatos, oCristal, stACCESS, stUsua, stTabla, stNombrePaciente, stFNaciPaciente, stSex, stIdPac, stCodDiag, stFechValDiag, stEpisodio, ref stModoImpresion, ref stActualizaImp_optional, VstCrystal, gUsuario, String.Empty, String.Empty, false, false);
		}

		public void Iniciar(SqlConnection Conexion, string stCaminoBD, string stBaseDatos, object oCristal, string stACCESS, string stUsua, string stTabla, string stNombrePaciente, string stFNaciPaciente, string stSex, string stIdPac, string stCodDiag, string stFechValDiag, string stEpisodio, ref string stModoImpresion, ref object stActualizaImp_optional, string VstCrystal)
		{
			Iniciar(Conexion, stCaminoBD, stBaseDatos, oCristal, stACCESS, stUsua, stTabla, stNombrePaciente, stFNaciPaciente, stSex, stIdPac, stCodDiag, stFechValDiag, stEpisodio, ref stModoImpresion, ref stActualizaImp_optional, VstCrystal, String.Empty, String.Empty, String.Empty, false, false);
		}

		public void Iniciar(SqlConnection Conexion, string stCaminoBD, string stBaseDatos, object oCristal, string stACCESS, string stUsua, string stTabla, string stNombrePaciente, string stFNaciPaciente, string stSex, string stIdPac, string stCodDiag, string stFechValDiag, string stEpisodio, ref string stModoImpresion, ref object stActualizaImp_optional)
		{
			Iniciar(Conexion, stCaminoBD, stBaseDatos, oCristal, stACCESS, stUsua, stTabla, stNombrePaciente, stFNaciPaciente, stSex, stIdPac, stCodDiag, stFechValDiag, stEpisodio, ref stModoImpresion, ref stActualizaImp_optional, String.Empty, String.Empty, String.Empty, String.Empty, false, false);
		}

		public void Iniciar(SqlConnection Conexion, string stCaminoBD, string stBaseDatos, object oCristal, string stACCESS, string stUsua, string stTabla, string stNombrePaciente, string stFNaciPaciente, string stSex, string stIdPac, string stCodDiag, string stFechValDiag, string stEpisodio, ref string stModoImpresion)
		{
			object tempRefParam2 = Type.Missing;
			Iniciar(Conexion, stCaminoBD, stBaseDatos, oCristal, stACCESS, stUsua, stTabla, stNombrePaciente, stFNaciPaciente, stSex, stIdPac, stCodDiag, stFechValDiag, stEpisodio, ref stModoImpresion, ref tempRefParam2, String.Empty, String.Empty, String.Empty, String.Empty, false, false);
		}

		public void Iniciar(SqlConnection Conexion, string stCaminoBD, string stBaseDatos, object oCristal, string stACCESS, string stUsua, string stTabla, string stNombrePaciente, string stFNaciPaciente, string stSex, string stIdPac, string stCodDiag, string stFechValDiag, string stEpisodio)
		{
			string tempRefParam3 = String.Empty;
			object tempRefParam4 = Type.Missing;
			Iniciar(Conexion, stCaminoBD, stBaseDatos, oCristal, stACCESS, stUsua, stTabla, stNombrePaciente, stFNaciPaciente, stSex, stIdPac, stCodDiag, stFechValDiag, stEpisodio, ref tempRefParam3, ref tempRefParam4, String.Empty, String.Empty, String.Empty, String.Empty, false, false);
		}
		private string DescripcionProvincia(string stCodigo)
		{

			string result = String.Empty;
			result = "";
			string sql = "select dnomprov,dnomprovi1,dnomprovi2 from DPROVINC where gprovinc = '" + stCodigo + "' ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				switch(Serrores.CampoIdioma)
				{
					case "IDIOMA0" : 
						result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnomprov"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dnomprov"]).Trim(); 
						break;
					case "IDIOMA1" :
						
						result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnomprovI1"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dnomprovi1"]).Trim(); 
						break;
					case "IDIOMA2" : 
						result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnomprovi2"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dnomprovi2"]).Trim(); 
						break;
				}
				//    DescripcionProvincia = Trim(Rrsql("dnomprov"))
			}

			
			RrSql.Close();
			return result;
		}
		private string Descripcion(string stCodigo, string stFechaFormat)
		{

			string result = String.Empty;
			result = "";
			string sql = "Select dnombdia,dnombdiai1,dnombdiai2 from DDIAGNOS where gcodidia= '" + stCodigo + "' ";
			sql = sql + "and finvaldi <= " + stFechaFormat + " ";
			sql = sql + "and (ffivaldi >= " + stFechaFormat + " or ffivaldi is null ) ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
			DataSet RrDescripcion = new DataSet();
			tempAdapter.Fill(RrDescripcion);
			if (RrDescripcion.Tables[0].Rows.Count != 0)
			{
				switch(Serrores.CampoIdioma)
				{
					case "IDIOMA0" :
						result = (Convert.IsDBNull(RrDescripcion.Tables[0].Rows[0]["dnombdia"])) ? "" : Convert.ToString(RrDescripcion.Tables[0].Rows[0]["dnombdia"]).Substring(0, Math.Min(60, Convert.ToString(RrDescripcion.Tables[0].Rows[0]["dnombdia"]).Length)); 
						break;
					case "IDIOMA1" : 
						
						result = (Convert.IsDBNull(RrDescripcion.Tables[0].Rows[0]["dnombdiai1"])) ? "" : Convert.ToString(RrDescripcion.Tables[0].Rows[0]["dnombdiai1"]).Substring(0, Math.Min(60, Convert.ToString(RrDescripcion.Tables[0].Rows[0]["dnombdiai1"]).Length)); 
						break;
					case "IDIOMA2" : 
						result = (Convert.IsDBNull(RrDescripcion.Tables[0].Rows[0]["dnombdiai2"])) ? "" : Convert.ToString(RrDescripcion.Tables[0].Rows[0]["dnombdiai2"]).Substring(0, Math.Min(60, Convert.ToString(RrDescripcion.Tables[0].Rows[0]["dnombdiai2"]).Length)); 
						break;
				}
				//    Descripcion = IIf(IsNull(RrDescripcion(0)), "", Mid(RrDescripcion(0), 1, 60))
			}
			
			RrDescripcion.Close();

			return result;
		}

		private void DatosEDO()
		{
			string stCadenaAux = String.Empty;
			string stUsuarioNotifica = String.Empty;


			string sql = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
		
			
			stGrupoHo = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["VALFANU1"]).Trim();

			sql = "select valfanu1 from sconsglo where gconsglo = 'NOMBREHO'";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RcValoracion);
			RrSql = new DataSet();
			tempAdapter_2.Fill(RrSql);
			
			stNombreHo = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim();

			sql = "select valfanu1 from sconsglo where gconsglo = 'direccho'";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(sql, RcValoracion);
			RrSql = new DataSet();
			tempAdapter_3.Fill(RrSql);
			stDireccHo = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu1"]).Trim();

			//FORMATO DE FILIACION
			SqlDataAdapter tempAdapter_4 = new SqlDataAdapter("Select valfanu1 from sConsGlo where gConsGlo = 'FORMFILI'", RcValoracion);
			DataSet Rrglobal = new DataSet();
			tempAdapter_4.Fill(Rrglobal);
		
			stFORMFILI = Convert.ToString(Rrglobal.Tables[0].Rows[0]["valfanu1"]).Trim();
			
			Rrglobal.Close();

			//PONER DNI � CEDULA


			string stsqltemp = "select valfanu1 from sconsglo where gconsglo= 'idenpers' ";
			SqlDataAdapter tempAdapter_5 = new SqlDataAdapter(stsqltemp, RcValoracion);
			DataSet RrTemp = new DataSet();
			tempAdapter_5.Fill(RrTemp);
			if (RrTemp.Tables[0].Rows.Count != 0)
			{
				
				vstLiteralDNI = Convert.ToString(RrTemp.Tables[0].Rows[0]["valfanu1"]).Trim();
			}
		
			RrTemp.Close();


            RrSql.Close();

			sql = "select * from DPACIENT where gidenpac = '" + stIdPaciente.Trim() + "'";
			SqlDataAdapter tempAdapter_6 = new SqlDataAdapter(sql, RcValoracion);
			RrSql = new DataSet();
			tempAdapter_6.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				
				vstNumeroSS = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["nafiliac"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["nafiliac"]).Trim();
				vstNumeroDNI = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ndninifp"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["ndninifp"]).Trim();
				
				if (Convert.ToString(RrSql.Tables[0].Rows[0]["iexitusp"]) == "N")
				{
					vstFallecimiento = "N";
				}
				else
				{
					//        vstFallecimiento = "S  " & "Fecha: " & IIf(IsNull(Rrsql("ffalleci")), "", Trim(Rrsql("ffalleci")))
				
					
					vstFallecimiento = "S  " + Serrores.VarRPT[1] + " " + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ffalleci"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["ffalleci"]).Trim());
				}
			
				if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ddirepac"]))
				{
					vstDireccionPaciente = "";
				}
				else
				{
				
				
					vstDireccionPaciente = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["ddirepac"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["ddirepac"]).Trim();
				
					vstPoblacion = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gcodipos"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["gcodipos"]).Trim();
					
					if (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gprovinc"]))
					{
						vstProvincia = "";
					}
					else
					{
						//buscar Descripcion
					
						stCadenaAux = DescripcionProvincia(Convert.ToString(RrSql.Tables[0].Rows[0]["gprovinc"]));
						vstProvincia = stCadenaAux;
					}
				}
			}
			else
			{
				//no hay datos
				vstNumeroSS = "";
				vstNumeroDNI = "";
				vstDireccionPaciente = "";
				vstPoblacion = "";
				vstProvincia = "";
				vstFallecimiento = "";
			}

			
			RrSql.Close();

		
			object tempRefParam = stFechaValidez;
			vstDiagnosticoNotifica = Descripcion(stCodigoPD, Serrores.FormatFechaHMS( tempRefParam));
			stFechaValidez = Convert.ToString(tempRefParam);

			//datos de la notificacion DNOTIEDO
			sql = "Select DNOTIEDO.*, DPERSONA.* ";
			sql = sql + " From DNOTIEDO inner join susuario on DNOTIEDO.gusunoti = susuario.gusuario ";
			sql = sql + " inner join dpersona on susuario.gpersona = dpersona.gpersona ";
			sql = sql + " where itiposer = '" + stiTipoSer + "' ";
			sql = sql + " and ganoregi = " + iA�oRegistro.ToString() + " and gnumregi = " + iNumeroRegistro.ToString();
			sql = sql + " and gcodidia = '" + stCodigoPD + "' ";
			object tempRefParam2 = stFechaValidez;
			sql = sql + " and finvaldi = " + Serrores.FormatFechaHMS( tempRefParam2) + " ";
			stFechaValidez = Convert.ToString(tempRefParam2);
			sql = sql + " and DNOTIEDO.gidenpac = '" + stIdPaciente.Trim() + "' ";

			SqlDataAdapter tempAdapter_7 = new SqlDataAdapter(sql, RcValoracion);
			RrSql = new DataSet();
			tempAdapter_7.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//siempre tengo que recuperar un registro
				
				stUsuarioNotifica = Convert.ToString(RrSql.Tables[0].Rows[0]["gpersona"]);
				vstNombreNotifica = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnompers"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dnompers"]).Trim();
				vstNombreNotifica = vstNombreNotifica + " " + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dap1pers"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dap1pers"]).Trim());
				
				vstNombreNotifica = vstNombreNotifica + " " + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dap2pers"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dap2pers"]).Trim());
				
				
				if (!Convert.IsDBNull(RrSql.Tables[0].Rows[0]["gtipopro"]))
				{
					
					vstCargoNotifica = DescripcionCategoria(Convert.ToString(RrSql.Tables[0].Rows[0]["gtipopro"]));
				}
				else
				{
					vstCargoNotifica = "";
				}
		
				vstFechaNotifica = Convert.ToDateTime(RrSql.Tables[0].Rows[0]["fnotiedo"]).ToString("dd/MM/yyyy HH:mm");
                vstDireccionNotifica = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dvia"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dvia"]).Trim();
                vstDireccionNotifica = vstDireccionNotifica + " " + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnumero1"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dnumero1"]).Trim());
                vstTelefonoNotifica = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dtfno"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dtfno"]).Trim();
			}
			else
			{
				vstNombreNotifica = "";
				vstCargoNotifica = "";
				vstFechaNotifica = "";
				vstDireccionNotifica = "";
				vstTelefonoNotifica = "";
			}
		
			RrSql.Close();

			//medico responsable
			sql = "";
			switch(stiTipoSer)
			{
				case "H" : 
					sql = "select DPERSONA.* "; 
					sql = sql + " From AEPISADM inner join DPERSONA on AEPISADM.gperulti = dpersona.gpersona "; 
					sql = sql + " where AEPISADM.ganoadme = " + iA�oRegistro.ToString() + " and AEPISADM.gnumadme = " + iNumeroRegistro.ToString(); 
					sql = sql + " and AEPISADM.gidenpac = '" + stIdPaciente.Trim() + "' "; 
					 
					break;
				case "U" : 
					sql = "select DPERSONA.*   "; 
					sql = sql + " From UEPISURG inner join DPERSONA on UEPISURG.gpersona = dpersona.gpersona "; 
					sql = sql + " where UEPISURG.ganourge = " + iA�oRegistro.ToString() + " and UEPISURG.gnumurge = " + iNumeroRegistro.ToString(); 
					sql = sql + " and UEPISURG.gidenpac = '" + stIdPaciente.Trim() + "' "; 
					 
					break;
				case "C" : 
					sql = "select DPERSONA.*   "; 
					sql = sql + " From CCONSULT inner join DPERSONA on CCONSULT.gpersona = dpersona.gpersona "; 
					sql = sql + " where CCONSULT.ganoregi = " + iA�oRegistro.ToString() + " and CCONSULT.gnumregi = " + iNumeroRegistro.ToString(); 
					sql = sql + " and CCONSULT.gidenpac = '" + stIdPaciente.Trim() + "' "; 
					break;
			}
			if (sql.Trim() != "")
			{

				if (vfHistoriaClinica)
				{
					string tempRefParam3 = "CCONSULT";
					Serrores.PonerVistas(ref sql, ref tempRefParam3, RcValoracion);
				}

				SqlDataAdapter tempAdapter_8 = new SqlDataAdapter(sql, RcValoracion);
				RrSql = new DataSet();
				tempAdapter_8.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
				
					if (Convert.ToString(RrSql.Tables[0].Rows[0]["gpersona"]) != stUsuarioNotifica)
					{
						
					
						vstMedicoResponsable = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dnompers"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dnompers"]).Trim();
					
						vstMedicoResponsable = vstMedicoResponsable + " " + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dap1pers"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dap1pers"]).Trim());
				
						vstMedicoResponsable = vstMedicoResponsable + " " + ((Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dap2pers"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dap2pers"]).Trim());
				
						vstMedicoTelefono = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dtfno"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dtfno"]).Trim();
					}
				}
				else
				{
					vstMedicoResponsable = "";
					vstMedicoTelefono = "";
				}
			
				RrSql.Close();
			}

		}
		private string DescripcionCategoria(string stCodigo)
		{

			string result = String.Empty;
			result = "";
			string sql = "select * from DTIPOPRO where gtipopro = " + stCodigo + " ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{
				switch(Serrores.CampoIdioma)
				{
					case "IDIOMA0" : 
						
				
						result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dtipopro"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dtipopro"]).Trim(); 
						break;
					case "IDIOMA1" : 
					
						
						result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dtipoproi1"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dtipoproi1"]).Trim(); 
						break;
					case "IDIOMA2" : 
						
					
						result = (Convert.IsDBNull(RrSql.Tables[0].Rows[0]["dtipoproi2"])) ? "" : Convert.ToString(RrSql.Tables[0].Rows[0]["dtipoproi2"]).Trim(); 
						break;
				}
			}
		
			RrSql.Close();

			return result;
		}

		private void DatosPrestaciones()
		{
			object dbLangSpanish = null;
			object DBEngine = null;
			//exploraciones complementarias
			string sqltemporal = String.Empty;
			string sql = String.Empty;
			DataSet RrSql = null;
			bool bDadoDeAlta = false;
			

			try
			{
				//tenemos que saber si el paciente esta dado de alta o no para buscar en una tabla u otra
				sql = "select faltplan ";
				sql = sql + " From AEPISADM ";
				sql = sql + " Where ganoadme = " + iA�oRegistro.ToString() + " and gnumadme = " + iNumeroRegistro.ToString();
				sql = sql + " and gidenpac ='" + stIdPaciente.Trim() + "'";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
				RrSql = new DataSet();
				tempAdapter.Fill(RrSql);
				if (RrSql.Tables[0].Rows.Count != 0)
				{
					
					
					bDadoDeAlta = !Convert.IsDBNull(RrSql.Tables[0].Rows[0]["faltplan"]);
				}
				else
				{
					bDadoDeAlta = false;
				}
			
				RrSql.Close();

				//Se crea una base de datos temporal para realizar el informe
              
                // pario TODO_X_5 BD ACCESS
                  /*
				if (FileSystem.Dir(stpathBD + "\\" + stNombreBD, FileAttribute.Normal) != "")
				{
					//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					bs = (Database) DBEngine.Workspaces(0).OpenDatabase(stpathBD + "\\" + stNombreBD);
					//UPGRADE_TODO: (1067) Member TableDefs is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					foreach (TableDef Tabla in bs.TableDefs)
					{
						//UPGRADE_TODO: (1067) Member Name is not defined in type TableDef. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						if (Convert.ToString(Tabla.Name) == "Prestaciones_Paciente")
						{
							//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
							bs.Execute("DROP TABLE Prestaciones_Paciente");
						}
					}
				}
				else
				{
					//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					bs = (Database) DBEngine.Workspaces(0).CreateDatabase(stpathBD + "\\" + stNombreBD, dbLangSpanish);
				}

				//UPGRADE_TODO: (1067) Member Execute is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				bs.Execute("CREATE table Prestaciones_Paciente (Titulo text,cab_Prest1 text,cab_Prest2 text," + 
				           "Prestacion text,Fecha text,Servicio text)");

				sqltemporal = "select * from Prestaciones_Paciente where 1=2";
				//UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Rs = (Recordset) bs.OpenRecordset(sqltemporal);

				if (bDadoDeAlta)
				{ //prestaciones realizadas al alta

					sql = "select epresalt.ganoregi,epresalt.frealiza,dservici.dnomserv,dservici.dnomservi1,dservici.dnomservi2, ";
					sql = sql + "dcodpres.dprestac,dcodpres.dprestaci1,dcodpres.dprestaci2 ";
					sql = sql + " From EPRESALT INNER JOIN DSERVICI ON epresalt.gsersoli = dservici.gservici ";
					sql = sql + " INNER JOIN DCODPRES ON epresalt.gprestac = dcodpres.gprestac ";
					sql = sql + " Where epresalt.ganoregi = " + iA�oRegistro.ToString() + " and ";
					sql = sql + " epresalt.gnumregi = " + iNumeroRegistro.ToString() + " and ";
					sql = sql + " epresalt.ITIPOSER = 'H' and epresalt.iestsoli = 'R' ";

				}
				else
				{
					// PRESTACIONES EN EL ACTIVO
					sql = "select eprestac.ganoregi,eprestac.frealiza,dservici.dnomserv,dservici.dnomservi1,dservici.dnomservi2, ";
					sql = sql + "dcodpres.dprestac,dcodpres.dprestaci1,dcodpres.dprestaci2 ";
					sql = sql + " From EPRESTAC INNER JOIN DSERVICI ON eprestac.gsersoli = dservici.gservici ";
					sql = sql + " INNER JOIN DCODPRES ON eprestac.gprestac = dcodpres.gprestac ";
					sql = sql + " Where eprestac.ganoregi = " + iA�oRegistro.ToString() + " and ";
					sql = sql + " eprestac.gnumregi = " + iNumeroRegistro.ToString() + " and ";
					sql = sql + " eprestac.ITIPOSER = 'H' and eprestac.iestsoli = 'R' ";
				}

				SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sql, RcValoracion);
				RrSql = new DataSet();
				tempAdapter_2.Fill(RrSql);

				if (RrSql.Tables[0].Rows.Count != 0)
				{
					
					
					foreach (DataRow iteration_row in RrSql.Tables[0].Rows)
					{
						//UPGRADE_TODO: (1067) Member AddNew is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						Rs.AddNew();
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["titulo"] = (Recordset) Serrores.VarRPT[2];
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["cab_prest1"] = (Recordset) Serrores.VarRPT[3];
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["cab_prest2"] = (Recordset) Serrores.VarRPT[4];
						switch(Serrores.CampoIdioma)
						{
							case "IDIOMA0" : 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Prestacion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Prestacion"] = (Recordset) ((Convert.IsDBNull(iteration_row["dprestac"])) ? "" : Convert.ToString(iteration_row["dprestac"]).Trim()); 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Servicio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Servicio"] = (Recordset) ((Convert.IsDBNull(iteration_row["dnomserv"])) ? "" : Convert.ToString(iteration_row["dnomserv"]).Trim()); 
								break;
							case "IDIOMA1" : 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Prestacion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Prestacion"] = (Recordset) ((Convert.IsDBNull(iteration_row["dprestaci1"])) ? "" : Convert.ToString(iteration_row["dprestaci1"]).Trim()); 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Servicio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Servicio"] = (Recordset) ((Convert.IsDBNull(iteration_row["dnomservi1"])) ? "" : Convert.ToString(iteration_row["dnomservi1"]).Trim()); 
								break;
							case "IDIOMA2" : 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Prestacion). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Prestacion"] = (Recordset) ((Convert.IsDBNull(iteration_row["dprestaci2"])) ? "" : Convert.ToString(iteration_row["dprestaci2"]).Trim()); 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Servicio). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								//UPGRADE_WARNING: (1049) Use of Null/IsNull() detected. More Information: http://www.vbtonet.com/ewis/ewi1049.aspx 
								//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx 
								Rs["Servicio"] = (Recordset) ((Convert.IsDBNull(iteration_row["dnomservi2"])) ? "" : Convert.ToString(iteration_row["dnomservi2"]).Trim()); 
								break;
						}
						//        Rs("Prestacion") = Rrsql("dprestac")
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(Fecha). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						//UPGRADE_WARNING: (1037) Couldn't resolve default property of object Rs(). More Information: http://www.vbtonet.com/ewis/ewi1037.aspx
						Rs["Fecha"] = (Recordset) Convert.ToDateTime(iteration_row["frealiza"]).ToString("dd/MM/yyyy HH:mm");
						//        Rs("Servicio") = Rrsql("dnomserv")
						//UPGRADE_TODO: (1067) Member Update is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
						Rs.Update();
					}
				}
				else
				{
				}
				//UPGRADE_ISSUE: (2064) RDO.rdoResultset method RrSql.Close was not upgraded. More Information: http://www.vbtonet.com/ewis/ewi2064.aspx
				RrSql.Close();

				//UPGRADE_TODO: (1067) Member Close is not defined in type Recordset. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Rs.Close();*/
			}
			catch(Exception ex)
			{

			Serrores.AnalizaError("InformeAlta", Path.GetDirectoryName(Application.ExecutablePath), stUsuario, "DatosPrestaciones",  ex);
				//    Resume
			}
		}

		private void ListadoNotificacion(string stModo, string stActualizaImp)
		{
			object DBEngine = null;
			char[] bocultar = new char[7];
			string sqltemporal = String.Empty;
			string stTitulo = String.Empty;

			try
			{
				//antes de generar el listado tiene que haber llenado la tabla temporal con los datos
				//que necesite
				//Obtener_Multilenguaje RcValoracion, stIdPaciente
				Serrores.ObtenerTextosRPT(RcValoracion, "NOTIFICARENFERMERIA", "UAT150R2");

				DatosEDO();
				DatosPrestaciones();

				//Set LISTADO_PARTICULAR = ObjetoCristal
				//para ocultar los subreport que no tienen valores


				sqltemporal = "select count(*) from ";
				bocultar[0] = 'T';
                //pario todo_x_5 BD ACCESS
                /*
				//UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Rs = (Recordset) bs.OpenRecordset(sqltemporal + stTablaListadoValoracion);
				//UPGRADE_WARNING: (1068) Rs() of type Recordset is being forced to double. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
				bocultar[1] = (((double) Rs(0)) == 0) ? "F" : "T"[0];

				//UPGRADE_TODO: (1067) Member OpenRecordset is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				Rs = (Recordset) bs.OpenRecordset(sqltemporal + " Prestaciones_Paciente");
				//UPGRADE_WARNING: (1068) Rs() of type Recordset is being forced to double. More Information: http://www.vbtonet.com/ewis/ewi1068.aspx
				bocultar[2] = (((double) Rs(0)) == 0) ? "F" : "T"[0];
				//
				for (int I = 0; I <= 1; I++)
				{
					//UPGRADE_TODO: (1067) Member SectionFormat is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
					LISTADO_NOTIFICACION.SectionFormat[I] = "DETAIL.0." + I.ToString() + ";" + Convert.ToString(bocultar[I]) + ";X;X;X;X;X;X;X";
				}
				//''una vez que vemos que datos tiene la base de datos la cerramos
				//UPGRADE_TODO: (1067) Member Close is not defined in type Database. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				bs.Close();
				//UPGRADE_TODO: (1067) Member Workspaces is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
				DBEngine.Workspaces(0).Close();*/
				//****************************************************************************

				//hay que cuidar para el caso de los diagnosticos el listado
				//Buscar las prestaciones

				
				LISTADO_NOTIFICACION.ReportFileName = stpathBD + "\\..\\ElementosComunes\\RPT\\UAT150R2.rpt";
                //LISTADO_NOTIFICACION.Formulas["NombreCentro"] = "\"" + stNombreHo + "\"";
                LISTADO_NOTIFICACION.Formulas["NombreCentro"] = "\"" + stNombreHo + "\"";
                //LISTADO_NOTIFICACION.Formulas["DireccionCentro"] = "\"" + stDireccHo + "\"";
                LISTADO_NOTIFICACION.Formulas["DireccionCentro"] = "\"" + stDireccHo + "\"";
                //LISTADO_NOTIFICACION.Formulas["NPaciente"] = "\"" + vstNombrePaciente + "\"";
                LISTADO_NOTIFICACION.Formulas["NPaciente"] = "\"" + vstNombrePaciente + "\"";
                //LISTADO_NOTIFICACION.Formulas[3] = "NumeroSS= \"" + vstNumeroSS + "\"";
                LISTADO_NOTIFICACION.Formulas["NumeroSS"] = "\"" + vstNumeroSS + "\"";
				//LISTADO_NOTIFICACION.Formulas[4] = "LiteralDNI= \"" + vstLiteralDNI + "\"";
                LISTADO_NOTIFICACION.Formulas["LiteralDNI"] = "\"" + vstLiteralDNI + "\"";
                //LISTADO_NOTIFICACION.Formulas[5] = "NumeroDNI= \"" + vstNumeroDNI + "\"";
                LISTADO_NOTIFICACION.Formulas["NumeroDNI"] = "\"" + vstNumeroDNI + "\"";
                //LISTADO_NOTIFICACION.Formulas[6] = "DirePaciente= \"" + vstDireccionPaciente + "\"";
                LISTADO_NOTIFICACION.Formulas["DirePaciente"] = "\"" + vstDireccionPaciente + "\"";
                //LISTADO_NOTIFICACION.Formulas[7] = "Poblacion = \"" + BuscarPoblacion(stIdPaciente) + "\""; //vstPoblacion
                LISTADO_NOTIFICACION.Formulas["Poblacion"] = "\"" + BuscarPoblacion(stIdPaciente) + "\""; //vstPoblacion
                //LISTADO_NOTIFICACION.Formulas[8] = "Provincia = \"" + vstProvincia + "\"";
                LISTADO_NOTIFICACION.Formulas["Provincia"] = "\"" + vstProvincia + "\"";
                System.DateTime TempDate = DateTime.FromOADate(0);
				//LISTADO_NOTIFICACION.Formulas[9] = "FNaciPaciente = \"" + ((DateTime.TryParse(vstFNaciPaciente, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : vstFNaciPaciente) + "\"";
                LISTADO_NOTIFICACION.Formulas["FNaciPaciente"] = "\"" + ((DateTime.TryParse(vstFNaciPaciente, out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : vstFNaciPaciente) + "\"";
                //LISTADO_NOTIFICACION.Formulas[10] = "SexoPaciente= \"" + BuscarDescSexo(stSexo) + "\"";
                LISTADO_NOTIFICACION.Formulas["SexoPaciente"] = "\"" + BuscarDescSexo(stSexo) + "\"";
                //LISTADO_NOTIFICACION.Formulas[11] = "FechaFallecimiento= \"" + vstFallecimiento + "\"";
                LISTADO_NOTIFICACION.Formulas["FechaFallecimiento"] = "\"" + vstFallecimiento + "\"";
                //LISTADO_NOTIFICACION.Formulas[12] = "NombreNotifica= \"" + vstNombreNotifica + "\"";
                LISTADO_NOTIFICACION.Formulas["NombreNotifica"] = "\"" + vstNombreNotifica + "\"";
                //LISTADO_NOTIFICACION.Formulas[13] = "CargoNotifica= \"" + vstCargoNotifica + "\"";
                LISTADO_NOTIFICACION.Formulas["CargoNotifica"] = "\"" + vstCargoNotifica + "\"";
                //LISTADO_NOTIFICACION.Formulas[14] = "FechaNotifica = \"" + vstFechaNotifica + "\"";
                LISTADO_NOTIFICACION.Formulas["FechaNotifica"] = "\"" + vstFechaNotifica + "\"";
                //LISTADO_NOTIFICACION.Formulas[15] = "DireNotifica = \"" + vstDireccionNotifica + "\"";
                LISTADO_NOTIFICACION.Formulas["DireNotifica"] = "\"" + vstDireccionNotifica + "\"";
                //LISTADO_NOTIFICACION.Formulas[16] = "TeleNotifica= \"" + vstTelefonoNotifica + "\"";
                LISTADO_NOTIFICACION.Formulas["TeleNotifica"] = "\"" + vstTelefonoNotifica + "\"";
                //LISTADO_NOTIFICACION.Formulas[17] = "NombreResponsable= \"" + vstMedicoResponsable + "\"";
                LISTADO_NOTIFICACION.Formulas["NombreResponsable"] = "\"" + vstMedicoResponsable + "\"";
                //LISTADO_NOTIFICACION.Formulas[18] = "TeleResponsable= \"" + vstMedicoTelefono + "\"";
                LISTADO_NOTIFICACION.Formulas["TeleResponsable"] = "\"" + vstMedicoTelefono + "\"";
                //stTitulo = "NOTIFICACION DE CASO DE " & vstDiagnosticoNotifica
                stTitulo = Serrores.VarRPT[5] + " " + vstDiagnosticoNotifica;
				//LISTADO_NOTIFICACION.Formulas[19] = "Titulo = \"" + stTitulo + "\"";
                LISTADO_NOTIFICACION.Formulas["Titulo"] = "\"" + stTitulo + "\"";
                //LISTADO_NOTIFICACION.Formulas[20] = "LOGOIDC= \"" + Serrores.ExisteLogo(RcValoracion) + "\"";
                LISTADO_NOTIFICACION.Formulas["LOGOIDC"] = "\"" + Serrores.ExisteLogo(RcValoracion) + "\"";
                //LISTADO_NOTIFICACION.Formulas[21] = "GrupoCentro= \"" + stGrupoHo + "\"";
                LISTADO_NOTIFICACION.Formulas["GrupoCentro"] = "\"" + stGrupoHo + "\"";
                //UPGRADE_TODO: (1067) Member WindowState is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                //pario TODO_X_5
                /*  LISTADO_NOTIFICACION.WindowState = (int) Crystal.WindowStateConstants.crptMaximized;*/
                //UPGRADE_TODO: (1067) Member DataFiles is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                LISTADO_NOTIFICACION.DataFiles[0] = stpathBD + "\\" + stNombreBD;

			
				LISTADO_NOTIFICACION.SubreportToChange = "Valoraciones";
			
				LISTADO_NOTIFICACION.Connect = "dsn = " + stDSNACCESS;
				Serrores.LLenarFormulas(RcValoracion, LISTADO_NOTIFICACION, "UAT150R2", "Valoraciones", stDSNACCESS, gUsuario_C, gContrase�a_C);
				LISTADO_NOTIFICACION.SubreportToChange = "";
				LISTADO_NOTIFICACION.SubreportToChange = "Prestaciones";
				LISTADO_NOTIFICACION.Connect = "dsn = " + stDSNACCESS;
				LISTADO_NOTIFICACION.SubreportToChange = "";

				//LISTADO_NOTIFICACION.SQLQuery = "select * from " & stTablaListadoValoracion
				if (stModo == "Pantalla")
				{
                    //UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //pario TODO_X_5
                    /*LISTADO_NOTIFICACION.Destination = (int) Crystal.DestinationConstants.crptToWindow;*/
                }
                else
				{
                    //UPGRADE_TODO: (1067) Member Destination is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                    //pario TODO_X_5
                    /*	LISTADO_NOTIFICACION.Destination = (int) Crystal.DestinationConstants.crptToPrinter;*/
                }
                //LISTADO_NOTIFICACION.WindowTitle = "Notificaci�n"
                //UPGRADE_TODO: (1067) Member WindowTitle is not defined in type Variant. More Information: http://www.vbtonet.com/ewis/ewi1067.aspx
                LISTADO_NOTIFICACION.WindowTitle = Serrores.VarRPT[6];
				LISTADO_NOTIFICACION.SubreportToChange = "LogotipoIDC";
				LISTADO_NOTIFICACION.Connect = "DSN=" + VstCrystal_C + ";UID=" + gUsuario_C + ";PWD=" + gContrase�a_C + "";
				LISTADO_NOTIFICACION.SubreportToChange = "";
				Serrores.LLenarFormulas(RcValoracion, LISTADO_NOTIFICACION, "UAT150R2", "UAT150R2");
				LISTADO_NOTIFICACION.Action = 1;
				LISTADO_NOTIFICACION.PageCount();
				LISTADO_NOTIFICACION.PrinterStopPage = LISTADO_NOTIFICACION.PageCount;
				LISTADO_NOTIFICACION.Reset();

				if (stActualizaImp == "S")
				{
					ActualizarDNOTIEDO("IMPRESION");
				}

				for (int tiForm = 0; tiForm <= 19; tiForm++)
				{
				
					LISTADO_NOTIFICACION.Formulas[tiForm] = "";
				}
			}
			catch
			{

				RadMessageBox.Show("error en la notificacion", Application.ProductName);
			}

		}

		public string BuscarPoblacion(string gidenpac)
		{
			string result = String.Empty;

			result = "";
			result = vstPoblacion.Trim(); //codigopostal
			string sqlPob = "select DPOBLACI.dpoblaci,DPOBLACI.dpoblacii1,DPOBLACI.dpoblacii2,DPOBLACI.gpoblaci from dpacient left join dpoblaci on dpacient.gpoblaci = dpoblaci.gpoblaci where dpacient.gidenpac ='" + gidenpac + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlPob, RcValoracion);
			DataSet RrPob = new DataSet();
			tempAdapter.Fill(RrPob);
			if (RrPob.Tables[0].Rows.Count != 0)
			{
				
				if (!Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaci"]))
				{
					switch(Serrores.CampoIdioma)
					{
						case "IDIOMA0" : 
							result = result + " " + ((Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaci"])) ? "" : Convert.ToString(RrPob.Tables[0].Rows[0]["dpoblaci"]).Trim()); 
							break;
						case "IDIOMA1" : 
							result = result + " " + ((Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaciI1"])) ? "" : Convert.ToString(RrPob.Tables[0].Rows[0]["dpoblaciI1"]).Trim()); 
							break;
						case "IDIOMS2" : 
							result = result + " " + ((Convert.IsDBNull(RrPob.Tables[0].Rows[0]["dpoblaciI2"])) ? "" : Convert.ToString(RrPob.Tables[0].Rows[0]["dpoblaciI2"]).Trim()); 
							break;
					}
				}
			}
		
			RrPob.Close();

			return result;
		}


		private string BuscarDescSexo(string stSexo)
		{

			string result = String.Empty;
			result = "";
			string sql = "SELECT valfanu1, valfanu2 FROM SCONSGLO ";
			switch(stSexo)
			{
				case "M" : 
					sql = sql + "WHERE gconsglo = 'SMUJER' "; 
					break;
				case "H" : 
					sql = sql + "WHERE gconsglo = 'SHOMBRE' "; 
					break;
				case "I" : 
					sql = sql + "WHERE gconsglo = 'SINDET' "; 
					break;
			}

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);
			if (RrSql.Tables[0].Rows.Count != 0)
			{

			
				result = Convert.ToString(RrSql.Tables[0].Rows[0]["valfanu2"]).Trim();
			}
		
			RrSql.Close();
			return result;
		}

		private void ActualizarDNOTIEDO(string stCampos)
		{

			string stFechaHoraSis = DateTimeHelper.ToString(DateTime.Now);
			string sql = "Select * from DNOTIEDO ";
			sql = sql + "where itiposer = '" + stiTipoSer + "' ";
			sql = sql + " and ganoregi = " + iA�oRegistro.ToString() + " and gnumregi = " + iNumeroRegistro.ToString();
			sql = sql + " and gcodidia = '" + stCodigoPD + "' ";
			object tempRefParam = stFechaValidez;
			sql = sql + " and finvaldi = " + Serrores.FormatFechaHMS( tempRefParam) + " ";
			stFechaValidez = Convert.ToString(tempRefParam);

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, RcValoracion);
			DataSet RrSql = new DataSet();
			tempAdapter.Fill(RrSql);

			if (RrSql.Tables[0].Rows.Count != 0)
			{
				//si existen datos para el apunte se actualiza
				//se actualizan los datos en DANTEPAC
				if (stCampos == "NOTIFICACION")
				{
					
					RrSql.Edit();
					RrSql.Tables[0].Rows[0]["fnotiedo"] = DateTime.Parse(stFechaHoraSis); //fecha del episodio
					RrSql.Tables[0].Rows[0]["gusunoti"] = stUsuario;
					string tempQuery = RrSql.Tables[0].TableName;
					SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tempQuery, "");
					SqlCommandBuilder tempCommandBuilder = new SqlCommandBuilder(tempAdapter_2);
					tempAdapter_2.Update(RrSql, RrSql.Tables[0].TableName);
				}
				if (stCampos == "IMPRESION")
				{
				
					RrSql.Edit();
					RrSql.Tables[0].Rows[0]["fimpredo"] = DateTime.Parse(stFechaHoraSis);
					RrSql.Tables[0].Rows[0]["gusuimpn"] = stUsuario;
					string tempQuery_2 = RrSql.Tables[0].TableName;
					SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tempQuery_2, "");
					SqlCommandBuilder tempCommandBuilder_2 = new SqlCommandBuilder(tempAdapter_3);
					tempAdapter_3.Update(RrSql, RrSql.Tables[0].TableName);
				}
			}
			else
			{
			
				RrSql.AddNew();
				
				RrSql.Tables[0].Rows[0]["itiposer"] = stiTipoSer;
				RrSql.Tables[0].Rows[0]["ganoregi"] = iA�oRegistro;
				RrSql.Tables[0].Rows[0]["gnumregi"] = iNumeroRegistro;
				RrSql.Tables[0].Rows[0]["gcodidia"] = stCodigoPD;
				RrSql.Tables[0].Rows[0]["finvaldi"] = DateTime.Parse(stFechaValidez);
				RrSql.Tables[0].Rows[0]["gidenpac"] = stIdPaciente;
				RrSql.Tables[0].Rows[0]["fnotiedo"] = DateTime.Parse(stFechaHoraSis); //fecha del episodio
				RrSql.Tables[0].Rows[0]["gusunoti"] = stUsuario;
				RrSql.Tables[0].Rows[0]["fimpredo"] = DBNull.Value;
				RrSql.Tables[0].Rows[0]["gusuimpn"] = DBNull.Value;
				string tempQuery_3 = RrSql.Tables[0].TableName;
				SqlDataAdapter tempAdapter_4 = new SqlDataAdapter(tempQuery_3, "");
				SqlCommandBuilder tempCommandBuilder_3 = new SqlCommandBuilder(tempAdapter_4);
				tempAdapter_4.Update(RrSql, RrSql.Tables[0].TableName);
			}
		
			RrSql.Close();

		}
	}
}