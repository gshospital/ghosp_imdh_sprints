using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	partial class MDO220F1
	{

		#region "Upgrade Support "
		private static MDO220F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static MDO220F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new MDO220F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprEpisodios", "optImpresora", "optPantalla", "frmImpresion", "LB_Hist", "Label3", "lb_paciente", "Label1", "Frame1", "cbAceptar", "cbCancelar", "SprEpisodios_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprEpisodios;
		public Telerik.WinControls.UI.RadRadioButton optImpresora;
		public Telerik.WinControls.UI.RadRadioButton optPantalla;
		public Telerik.WinControls.UI.RadGroupBox frmImpresion;
		public Telerik.WinControls.UI.RadTextBox LB_Hist;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadTextBox lb_paciente;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDO220F1));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.SprEpisodios = new UpgradeHelpers.Spread.FpSpread();
			this.frmImpresion = new Telerik.WinControls.UI.RadGroupBox();
			this.optImpresora = new Telerik.WinControls.UI.RadRadioButton();
			this.optPantalla = new Telerik.WinControls.UI.RadRadioButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.LB_Hist = new Telerik.WinControls.UI.RadTextBox();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.lb_paciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.frmImpresion.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// SprEpisodios
			// 
            this.SprEpisodios.MasterTemplate.EnableSorting = false;
            this.SprEpisodios.MasterTemplate.ReadOnly = true;
			this.SprEpisodios.Location = new System.Drawing.Point(10, 50);
			this.SprEpisodios.Name = "SprEpisodios";
			this.SprEpisodios.Size = new System.Drawing.Size(575, 179);
			this.SprEpisodios.TabIndex = 0;
            this.SprEpisodios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprEpisodios_CellClick);
            this.SprEpisodios.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SprEpisodios_KeyUp);
			this.SprEpisodios.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SprEpisodios_MouseUp);
			// 
			// frmImpresion
			// 
			this.frmImpresion.Controls.Add(this.optImpresora);
			this.frmImpresion.Controls.Add(this.optPantalla);
			this.frmImpresion.Enabled = true;
			this.frmImpresion.Location = new System.Drawing.Point(174, 233);
			this.frmImpresion.Name = "frmImpresion";
			this.frmImpresion.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.frmImpresion.Size = new System.Drawing.Size(181, 35);
			this.frmImpresion.TabIndex = 10;
			this.frmImpresion.Text = "Impresi�n";
			this.frmImpresion.Visible = false;
			// 
			// optImpresora
			// 
			this.optImpresora.CausesValidation = true;
			this.optImpresora.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.optImpresora.IsChecked = false;
			this.optImpresora.Cursor = System.Windows.Forms.Cursors.Default;
			this.optImpresora.Enabled = true;
			this.optImpresora.Location = new System.Drawing.Point(97, 13);
			this.optImpresora.Name = "optImpresora";
			this.optImpresora.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.optImpresora.Size = new System.Drawing.Size(73, 13);
			this.optImpresora.TabIndex = 2;
			this.optImpresora.TabStop = true;
			this.optImpresora.Text = "Impresora";
			this.optImpresora.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.optImpresora.Visible = true;
			// 
			// optPantalla
			// 
			this.optPantalla.CausesValidation = true;
			this.optPantalla.RadioCheckAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.optPantalla.IsChecked = true;
			this.optPantalla.Cursor = System.Windows.Forms.Cursors.Default;
			this.optPantalla.Enabled = true;
			this.optPantalla.Location = new System.Drawing.Point(25, 13);
			this.optPantalla.Name = "optPantalla";
			this.optPantalla.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.optPantalla.Size = new System.Drawing.Size(59, 13);
			this.optPantalla.TabIndex = 1;
			this.optPantalla.TabStop = true;
			this.optPantalla.Text = "Pantalla";
			this.optPantalla.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
			this.optPantalla.Visible = true;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.LB_Hist);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.lb_paciente);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(10, 2);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(573, 47);
			this.Frame1.TabIndex = 5;
			this.Frame1.Text = "Datos del paciente";
			this.Frame1.Visible = true;
            // 
            // LB_Hist
            // 
            this.LB_Hist.Enabled = false;
			this.LB_Hist.Cursor = System.Windows.Forms.Cursors.Default;
			this.LB_Hist.Location = new System.Drawing.Point(463, 19);
			this.LB_Hist.Name = "LB_Hist";
			this.LB_Hist.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LB_Hist.Size = new System.Drawing.Size(84, 19);
			this.LB_Hist.TabIndex = 9;
			// 
			// Label3
			// 
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.Location = new System.Drawing.Point(390, 19);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(65, 16);
			this.Label3.TabIndex = 8;
			this.Label3.Text = "Hist. cl�nica:";
            // 
            // lb_paciente
            // 
            this.lb_paciente.Enabled = false;
			this.lb_paciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lb_paciente.Location = new System.Drawing.Point(72, 19);
			this.lb_paciente.Name = "lb_paciente";
			this.lb_paciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lb_paciente.Size = new System.Drawing.Size(296, 19);
			this.lb_paciente.TabIndex = 7;
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(11, 19);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(52, 17);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "Paciente:";
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Location = new System.Drawing.Point(414, 236);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 3;
			this.cbAceptar.Text = "Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(504, 236);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 29);
			this.cbCancelar.TabIndex = 4;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// MDO220F1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(593, 273);
			this.Controls.Add(this.SprEpisodios);
			this.Controls.Add(this.frmImpresion);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCancelar);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = (System.Drawing.Icon) resources.GetObject("MDO220F1.Icon");
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MDO220F1";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Selecci�n de episodios - MDO220F1";
			this.Closed += new System.EventHandler(this.MDO220F1_Closed);
			this.Load += new System.EventHandler(this.MDO220F1_Load);
			this.frmImpresion.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}