using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	partial class LEQ001F1
	{

		#region "Upgrade Support "
		private static LEQ001F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static LEQ001F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new LEQ001F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprPacientes", "cbCancelar", "cbAceptar", "TxtPaciente", "Line6", "Line4", "Label8", "Label5", "Label7", "Label6", "Label4", "Label3", "Line5", "Line2", "Line1", "Line3", "Label1", "ShapeContainer2", "SprPacientes_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprPacientes;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadTextBoxControl TxtPaciente;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line6;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line4;
		public Telerik.WinControls.UI.RadLabel Label8;
		public Telerik.WinControls.UI.RadLabel Label5;
		public Telerik.WinControls.UI.RadLabel Label7;
		public Telerik.WinControls.UI.RadLabel Label6;
		public Telerik.WinControls.UI.RadLabel Label4;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line5;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line2;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line3;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer2;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LEQ001F1));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.ShapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.SprPacientes = new UpgradeHelpers.Spread.FpSpread();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.TxtPaciente = new Telerik.WinControls.UI.RadTextBoxControl();
			this.Line6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label8 = new Telerik.WinControls.UI.RadLabel();
			this.Label5 = new Telerik.WinControls.UI.RadLabel();
			this.Label7 = new Telerik.WinControls.UI.RadLabel();
			this.Label6 = new Telerik.WinControls.UI.RadLabel();
			this.Label4 = new Telerik.WinControls.UI.RadLabel();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.Line5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Line3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.SuspendLayout();
			// 
			// ShapeContainer2
			// 
			this.ShapeContainer2.Location = new System.Drawing.Point(0, 0);
			this.ShapeContainer2.Size = new System.Drawing.Size(789, 303);
			this.ShapeContainer2.Shapes.Add(Line6);
			this.ShapeContainer2.Shapes.Add(Line4);
			this.ShapeContainer2.Shapes.Add(Line5);
			this.ShapeContainer2.Shapes.Add(Line2);
			this.ShapeContainer2.Shapes.Add(Line1);
			this.ShapeContainer2.Shapes.Add(Line3);
            // 
            // SprPacientes
            // 
            gridViewTextBoxColumn1.HeaderText = "Servicio";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 120;

            gridViewTextBoxColumn2.HeaderText = "Prestaci�n";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 120;

            gridViewTextBoxColumn3.HeaderText = "Entidad Financiadora";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 120;

            gridViewTextBoxColumn4.HeaderText = "Fecha Inclusi�n";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 100;

            gridViewTextBoxColumn5.HeaderText = "A�o Epi.";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 60;

            gridViewTextBoxColumn6.HeaderText = "Num. Epi.";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 60;

            this.SprPacientes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[]
           {    gridViewTextBoxColumn1,
                gridViewTextBoxColumn2,
                gridViewTextBoxColumn3,
                gridViewTextBoxColumn4,
                gridViewTextBoxColumn5,
                gridViewTextBoxColumn6

           });
            this.SprPacientes.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.SprPacientes.MasterTemplate.EnableSorting = false;
            this.SprPacientes.MasterTemplate.ReadOnly = true;
            this.SprPacientes.Location = new System.Drawing.Point(4, 32);
			this.SprPacientes.Name = "SprPacientes";
			this.SprPacientes.Size = new System.Drawing.Size(781, 211);
			this.SprPacientes.TabIndex = 0;
            this.SprPacientes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprPacientes_CellClick);
            // 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(702, 256);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 33);
			this.cbCancelar.TabIndex = 3;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Location = new System.Drawing.Point(606, 256);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 33);
			this.cbAceptar.TabIndex = 2;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// TxtPaciente
			// 
			this.TxtPaciente.AcceptsReturn = true;
			this.TxtPaciente.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.TxtPaciente.Enabled = false;
			this.TxtPaciente.Location = new System.Drawing.Point(70, 4);
			this.TxtPaciente.MaxLength = 0;
			this.TxtPaciente.Name = "TxtPaciente";
			this.TxtPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.TxtPaciente.Size = new System.Drawing.Size(713, 19);
			this.TxtPaciente.TabIndex = 1;
			// 
			// Line6
			// 
			this.Line6.BorderColor = System.Drawing.Color.FromArgb(192, 0, 192);
			this.Line6.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line6.BorderWidth = 7;
			this.Line6.Enabled = false;
			this.Line6.Name = "Line6";
			this.Line6.Visible = false;
			this.Line6.X1 = (int) 244;
			this.Line6.X2 = (int) 272;
			this.Line6.Y1 = (int) 272;
			this.Line6.Y2 = (int) 272;
			// 
			// Line4
			// 
			this.Line4.BorderColor = System.Drawing.SystemColors.AppWorkspace;
			this.Line4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line4.BorderWidth = 7;
			this.Line4.Enabled = false;
			this.Line4.Name = "Line4";
			this.Line4.Visible = false;
			this.Line4.X1 = (int) 244;
			this.Line4.X2 = (int) 272;
			this.Line4.Y1 = (int) 256;
			this.Line4.Y2 = (int) 256;
			// 
			// Label8
			// 
			this.Label8.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label8.Location = new System.Drawing.Point(278, 248);
			this.Label8.Name = "Label8";
			this.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label8.Size = new System.Drawing.Size(51, 13);
			this.Label8.TabIndex = 10;
			this.Label8.Text = "Devueltos";
			this.Label8.Visible = false;
			// 
			// Label5
			// 
			this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label5.Location = new System.Drawing.Point(278, 266);
			this.Label5.Name = "Label5";
			this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label5.Size = new System.Drawing.Size(115, 13);
			this.Label5.TabIndex = 9;
			this.Label5.Text = "Atendidos / Salidas";
			this.Label5.Visible = false;
			// 
			// Label7
			// 
			this.Label7.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label7.Location = new System.Drawing.Point(170, 248);
			this.Label7.Name = "Label7";
			this.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label7.Size = new System.Drawing.Size(51, 13);
			this.Label7.TabIndex = 8;
			this.Label7.Text = "No aptos";
			// 
			// Label6
			// 
			this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label6.Location = new System.Drawing.Point(170, 266);
			this.Label6.Name = "Label6";
			this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label6.Size = new System.Drawing.Size(43, 13);
			this.Label6.TabIndex = 7;
			this.Label6.Text = "Aptos";
			// 
			// Label4
			// 
			this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label4.Location = new System.Drawing.Point(58, 264);
			this.Label4.Name = "Label4";
			this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label4.Size = new System.Drawing.Size(59, 13);
			this.Label4.TabIndex = 6;
			this.Label4.Text = "En Estudio";
			// 
			// Label3
			// 
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.Location = new System.Drawing.Point(58, 248);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(61, 13);
			this.Label3.TabIndex = 5;
			this.Label3.Text = "Pendientes";
			// 
			// Line5
			// 
			this.Line5.BorderColor = System.Drawing.Color.Blue;
			this.Line5.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line5.BorderWidth = 7;
			this.Line5.Enabled = false;
			this.Line5.Name = "Line5";
			this.Line5.Visible = true;
			this.Line5.X1 = (int) 128;
			this.Line5.X2 = (int) 156;
			this.Line5.Y1 = (int) 272;
			this.Line5.Y2 = (int) 272;
			// 
			// Line2
			// 
			this.Line2.BorderColor = System.Drawing.Color.Green;
			this.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line2.BorderWidth = 7;
			this.Line2.Enabled = false;
			this.Line2.Name = "Line2";
			this.Line2.Visible = true;
			this.Line2.X1 = (int) 16;
			this.Line2.X2 = (int) 44;
			this.Line2.Y1 = (int) 272;
			this.Line2.Y2 = (int) 272;
			// 
			// Line1
			// 
			this.Line1.BorderColor = System.Drawing.Color.Black;
			this.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line1.BorderWidth = 7;
			this.Line1.Enabled = false;
			this.Line1.Name = "Line1";
			this.Line1.Visible = true;
			this.Line1.X1 = (int) 16;
			this.Line1.X2 = (int) 44;
			this.Line1.Y1 = (int) 256;
			this.Line1.Y2 = (int) 256;
			// 
			// Line3
			// 
			this.Line3.BorderColor = System.Drawing.Color.Red;
			this.Line3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line3.BorderWidth = 7;
			this.Line3.Enabled = false;
			this.Line3.Name = "Line3";
			this.Line3.Visible = true;
			this.Line3.X1 = (int) 128;
			this.Line3.X2 = (int) 156;
			this.Line3.Y1 = (int) 256;
			this.Line3.Y2 = (int) 256;
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(6, 8);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(57, 17);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "Paciente:";
			// 
			// LEQ001F1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(789, 303);
			this.ControlBox = false;
			this.Controls.Add(this.SprPacientes);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.TxtPaciente);
			this.Controls.Add(this.Label8);
			this.Controls.Add(this.Label5);
			this.Controls.Add(this.Label7);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label1);
			this.Controls.Add(ShapeContainer2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "LEQ001F1";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Selecci�n de Episodio de Lista de Espera - LEQ001F1";
			this.Closed += new System.EventHandler(this.LEQ001F1_Closed);
			this.Load += new System.EventHandler(this.LEQ001F1_Load);
			this.ResumeLayout(false);
		}
		#endregion
	}
}