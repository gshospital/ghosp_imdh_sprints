using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	public partial class MDO230F1
		: Telerik.WinControls.UI.RadForm
    {

		string stConsultaDatos = String.Empty;
		int iAñoEpisodio = 0;
		int lNumeroEpisodio = 0;

		const int col_EPISODIO = 1;
		const int col_FECHA_INICIO_DEMANDA = 2;
		const int col_TIPO_DEMANDA = 3;
		const int col_ESTADO = 4;
		const int col_FECHA_ESTADO = 5;
		const int col_MOTIVO_DEMANDA = 6;
		const int col_ORIGEN = 7;
		const int col_FORMA_REALIZACION = 8;
		const int col_ACTUACION_REALIZADA = 9;
		const int col_PERSONA_INICIA_DEMANDA = 10;
		const int col_TRABAJADOR_SOCIAL = 11;
		const int col_GIDENPAC = 12;
		public MDO230F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


        private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//debo tener seleccionado un registro para devolver la dll
			BasSelEpisodio.Llamador.RecogerDemanda(iAñoEpisodio, lNumeroEpisodio);
			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			BasSelEpisodio.Llamador.RecogerDemanda(0, 0);
			this.Close();
		}

		private void MDO230F1_Load(Object eventSender, EventArgs eventArgs)
		{
            //If Not AplicarControlAcceso(gUsuario, "O", Me, "frmHistoriaSocial", astrControles(), astrEventos(), rcConexion) Then Exit Sub
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

			string LitPersona = Serrores.ObtenerLiteralPersona(BasSelEpisodio.rcConexion);
			LitPersona = LitPersona.Substring(0, Math.Min(1, LitPersona.Length)).ToUpper() + LitPersona.Substring(1).ToLower();

			Frame1.Text = "Datos del " + LitPersona + ":";

			proBuscarDatosPaciente();

			proPrepararGridDemandas();
			ProCargaDemandas();

			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);

		}
        
		private object proPrepararGridDemandas()
		{
			sprDemandas.MaxCols = 12;
			sprDemandas.MaxRows = 0;
			sprDemandas.SetText(col_FECHA_INICIO_DEMANDA, 0, "Fecha I. Demanda");
			sprDemandas.SetColWidth(col_FECHA_INICIO_DEMANDA, 15);
			sprDemandas.SetText(col_TIPO_DEMANDA, 0, "Tipo Demanda");
			sprDemandas.SetColWidth(col_TIPO_DEMANDA, 25);
			sprDemandas.SetText(col_ESTADO, 0, "Estado");
			sprDemandas.SetColWidth(col_ESTADO, 10);
			sprDemandas.SetText(col_FECHA_ESTADO, 0, "Fecha Estado");
			sprDemandas.SetColWidth(col_FECHA_ESTADO, 15);
			sprDemandas.SetText(col_MOTIVO_DEMANDA, 0, "Motivo Demanda");
			sprDemandas.SetColWidth(col_MOTIVO_DEMANDA, 25);
			sprDemandas.SetText(col_ORIGEN, 0, "Origen");
			sprDemandas.SetColWidth(col_ORIGEN, 20);
			sprDemandas.SetText(col_FORMA_REALIZACION, 0, "Forma Realización");
			sprDemandas.SetColWidth(col_FORMA_REALIZACION, 30);
			sprDemandas.SetText(col_ACTUACION_REALIZADA, 0, "Actuación Realizada");
			sprDemandas.SetColWidth(col_ACTUACION_REALIZADA, 30);
			sprDemandas.SetText(col_PERSONA_INICIA_DEMANDA, 0, "Persona que inicia la Demanda");
			sprDemandas.SetColWidth(col_PERSONA_INICIA_DEMANDA, 30);
			sprDemandas.SetText(col_TRABAJADOR_SOCIAL, 0, "Trabajador Social");
			sprDemandas.SetColWidth(col_TRABAJADOR_SOCIAL, 40);
			sprDemandas.SetText(col_GIDENPAC, 0, "Id. Paciente");
			sprDemandas.SetText(col_EPISODIO, 0, "Id. Episodio");

			sprDemandas.Col = col_GIDENPAC;
			sprDemandas.SetColHidden(sprDemandas.Col, true);
			//    sprDemandas.Col = col_EPISODIO
			//    sprDemandas.ColHidden = True
			return null;
		}

		private void ProCargaDemandas()
		{
			int iFilas = 0;
			string sql = "SELECT" + 
			             " pepisoci.ganoregi, pepisoci.gnumregi, pepisoci.gidenpac, pepisoci.finidema, " + 
			             " pepisoci.iestadod, pepisoci.festadod, pepisoci.oactreal, pepisoci.operinid, " + 
			             " ptipdema.dtipdema, " + 
			             " pmotdema.dmotdema motivoC, pepisoci.omotdema motivoL, " + 
			             " pforreal.dforreal, " + 
			             " poripaci.doripaci, " + 
			             " ISNULL(rtrim(DPERSONA.dap1pers),'') + ' ' + " + 
			             " ISNULL(rtrim(DPERSONA.dap2pers),'') + ', ' + " + 
			             " ISNULL(rtrim(DPERSONA.dnompers),'') as TrabajadorSocial " + 
			             " From pepisoci " + 
			             " left join ptipdema on ptipdema.gtipdema = pepisoci.gtipdema " + 
			             " left join pmotdema on pmotdema.gmotdema = pepisoci.gmotdema " + 
			             " left join pforreal on pforreal.gforreal = pepisoci.gforreal " + 
			             " left join poripaci on poripaci.goripaci = pepisoci.goripaci " + 
			             " left join dpersona on dpersona.gpersona = pepisoci.gpersona " + 
			             " where pepisoci.gidenpac='" + BasSelEpisodio.gstIdenPaciente + "'" + 
			             " order by finidema desc ";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BasSelEpisodio.rcConexion);
			DataSet RR = new DataSet();
			tempAdapter.Fill(RR);
			if (RR.Tables[0].Rows.Count != 0)
			{ //si hay datos se cargan en el grid
				sprDemandas.MaxRows = 0;
				iFilas = 1;
				sprDemandas.Row = iFilas;
				foreach (DataRow iteration_row in RR.Tables[0].Rows)
				{
					sprDemandas.MaxRows++;
					sprDemandas.Row = iFilas;

					//FECHA DE Inicio de demanda
					sprDemandas.Col = col_FECHA_INICIO_DEMANDA;
					if (Convert.IsDBNull(iteration_row["finidema"]))
					{
						sprDemandas.Text = "";
					}
					else
					{
						sprDemandas.Text = Convert.ToDateTime(iteration_row["finidema"]).ToString("dd/MM/yyyy HH:mm");
					}
					sprDemandas.Lock = true;
					// tipo demanda
					sprDemandas.Col = col_TIPO_DEMANDA;
					sprDemandas.Text = Convert.ToString(iteration_row["dtipdema"]) + "";
					sprDemandas.Lock = true;
					//Estado
					sprDemandas.Col = col_ESTADO;
					if (Convert.IsDBNull(iteration_row["iestadod"]))
					{
						sprDemandas.Text = "";
					}
					else
					{
						switch(Convert.ToString(iteration_row["iestadod"]).ToUpper())
						{
							case "P" :  
								sprDemandas.Text = "PENDIENTE"; 
								break;
							case "R" :  
								sprDemandas.Text = "REALIZADA"; 
								break;
							case "A" :  
								sprDemandas.Text = "ANULADA"; 
								break;
						}
					}
					sprDemandas.Lock = true;
					//Fecha de estado
					sprDemandas.Col = col_FECHA_ESTADO;
					if (Convert.IsDBNull(iteration_row["festadod"]))
					{
						sprDemandas.Text = "";
					}
					else
					{
						sprDemandas.Text = Convert.ToDateTime(iteration_row["festadod"]).ToString("dd/MM/yyyy HH:mm");
					}
					sprDemandas.Lock = true;
					// motivo de la demanda
					sprDemandas.Col = col_MOTIVO_DEMANDA;
					sprDemandas.Text = Convert.ToString(iteration_row["motivoC"]).Trim();
					sprDemandas.Lock = true;
					//origen
					sprDemandas.Col = col_ORIGEN;
					sprDemandas.Text = Convert.ToString(iteration_row["doripaci"]) + "";
					sprDemandas.Lock = true;
					//Forma realizacion
					sprDemandas.Col = col_FORMA_REALIZACION;
					sprDemandas.Text = Convert.ToString(iteration_row["dforreal"]) + "";
					sprDemandas.Lock = true;
					//Actuacion realizada
					sprDemandas.Col = col_ACTUACION_REALIZADA;
					sprDemandas.Text = Convert.ToString(iteration_row["oactreal"]) + "";
					sprDemandas.Lock = true;
					//Persona que inicia la demanda
					sprDemandas.Col = col_PERSONA_INICIA_DEMANDA;
					sprDemandas.Text = Convert.ToString(iteration_row["operinid"]) + "";
					sprDemandas.Lock = true;
					//Trabajador social
					sprDemandas.Col = col_TRABAJADOR_SOCIAL;
					sprDemandas.Text = Convert.ToString(iteration_row["TrabajadorSocial"]) + "";
					sprDemandas.Lock = true;
					//Id. paciente (oculto)
					sprDemandas.Col = col_GIDENPAC;
					sprDemandas.Text = Convert.ToString(iteration_row["gidenpac"]) + "";
					sprDemandas.Lock = true;
					//Id. episodio (oculto)
					sprDemandas.Col = col_EPISODIO;
					sprDemandas.Text = Convert.ToString(iteration_row["ganoregi"]).Trim() + "/" + Convert.ToString(iteration_row["gnumregi"]).Trim();
					sprDemandas.Lock = true;
					iFilas++;
				}
			}
			else
			{
				//NO HAY DATOS
				sprDemandas.MaxRows = 0;
				cbAceptar.Enabled = false;
			}

			if (sprDemandas.MaxRows != 0)
			{
				sprDemandas.Row = 1;
				sprDemandas.Col = col_EPISODIO;
				iFilas = (sprDemandas.Text.IndexOf('/') + 1);
				iAñoEpisodio = Convert.ToInt32(Double.Parse(sprDemandas.Text.Substring(0, Math.Min(iFilas - 1, sprDemandas.Text.Length))));
				lNumeroEpisodio = Convert.ToInt32(Double.Parse(sprDemandas.Text.Substring(iFilas)));
				sprDemandas.Action = 0;
			}

			RR.Close();
			//sprDemandas.OperationMode = OperationModeRead
		}


		private void proBuscarDatosPaciente()
		{
			string stCadena = String.Empty;


			string sql = "SELECT ISNULL(DPACIENT.dnombpac, '') AS dnombpac, ISNULL(DPACIENT.dape1pac, '') AS dape1pac, " + 
			             "ISNULL(DPACIENT.dape2pac, '') AS dape2pac, HDOSSIER.ghistoria " + 
			             "FROM DPACIENT " + 
			             "LEFT OUTER JOIN  HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac " + 
			             "WHERE (DPACIENT.gidenpac = '" + BasSelEpisodio.gstIdenPaciente + "'  ) ";


			//************************* O.Frias (SQL-2005) *************************
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BasSelEpisodio.rcConexion);
			DataSet RrSQL = new DataSet();
			tempAdapter.Fill(RrSQL);
			if (RrSQL.Tables[0].Rows.Count != 0)
			{
				LB_Hist.Text = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["ghistoria"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["ghistoria"]).Trim();
				stCadena = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["dnombpac"]).Trim();
				stCadena = stCadena + ((Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dape1pac"])) ? "" : " " + Convert.ToString(RrSQL.Tables[0].Rows[0]["dape1pac"]).Trim());
				stCadena = stCadena + ((Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dape2pac"])) ? "" : " " + Convert.ToString(RrSQL.Tables[0].Rows[0]["dape2pac"]).Trim());
				lb_paciente.Text = stCadena;
			}
			else
			{
				LB_Hist.Text = "";
				lb_paciente.Text = "";
			}
			RrSQL.Close();
		}


        private void sprDemandas_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			//cuando selecciono un episodio
			int pos = 0;
			sprDemandas.Row = Row;
			if (sprDemandas.Row == 0)
			{
				return;
			}
			else
			{
				sprDemandas.Col = col_EPISODIO;
				pos = (sprDemandas.Text.IndexOf('/') + 1);
				iAñoEpisodio = Convert.ToInt32(Double.Parse(sprDemandas.Text.Substring(0, Math.Min(pos - 1, sprDemandas.Text.Length))));
				lNumeroEpisodio = Convert.ToInt32(Double.Parse(sprDemandas.Text.Substring(pos)));
			}
		}
		private void MDO230F1_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
        }
	}
}