using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	partial class CCS820F2
	{

		#region "Upgrade Support "
		private static CCS820F2 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static CCS820F2 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new CCS820F2();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprListaEspera", "Frame1", "cbAceptar", "cbCerrar", "lbHistoria", "lbPaciente", "Label23", "Label1", "sprListaEspera_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprListaEspera;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadLabel Label23;
		public Telerik.WinControls.UI.RadLabel Label1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CCS820F2));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.sprListaEspera = new UpgradeHelpers.Spread.FpSpread();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.cbCerrar = new Telerik.WinControls.UI.RadButton();
			this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label23 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.sprListaEspera);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(2, 32);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(595, 257);
			this.Frame1.TabIndex = 2;
			this.Frame1.Text = "Lista de Espera de Programas de Sesiones";
			this.Frame1.Visible = true;
            // 
            // sprListaEspera
            // 
            gridViewTextBoxColumn1.HeaderText = "Apellidos, Nombre";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 200;

            gridViewTextBoxColumn2.HeaderText = "D�as de Espera";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.Width = 100;

            gridViewTextBoxColumn3.HeaderText = "Estado";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 60;

            gridViewTextBoxColumn4.HeaderText = "Gidenpac";
            gridViewTextBoxColumn4.Name = "column4";           

            gridViewTextBoxColumn5.HeaderText = "Observaciones";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 180;

            this.sprListaEspera.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[]
           {    gridViewTextBoxColumn1,
                gridViewTextBoxColumn2,
                gridViewTextBoxColumn3,
                gridViewTextBoxColumn4,
                gridViewTextBoxColumn5
            
           });
            this.sprListaEspera.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprListaEspera.MasterTemplate.EnableSorting = false;
            this.sprListaEspera.MasterTemplate.ReadOnly = true;
            this.sprListaEspera.Location = new System.Drawing.Point(8, 22);
			this.sprListaEspera.Name = "sprListaEspera";
			this.sprListaEspera.Size = new System.Drawing.Size(580, 229);
			this.sprListaEspera.TabIndex = 7;
            this.sprListaEspera.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprListaEspera_CellClick);
            this.sprListaEspera.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprListaEspera_CellDoubleClick);
            this.sprListaEspera.MouseDown += new System.Windows.Forms.MouseEventHandler(sprListaEspera_MouseDown);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Enabled = false;
			this.cbAceptar.Location = new System.Drawing.Point(416, 296);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 32);
			this.cbAceptar.TabIndex = 1;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// cbCerrar
			// 
			this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCerrar.Location = new System.Drawing.Point(512, 296);
			this.cbCerrar.Name = "cbCerrar";
			this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCerrar.Size = new System.Drawing.Size(81, 32);
			this.cbCerrar.TabIndex = 0;
			this.cbCerrar.Text = "&Ce&rrar";
			this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
            // 
            // lbHistoria
            // 
            this.lbHistoria.Enabled = false;
			this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbHistoria.Location = new System.Drawing.Point(524, 4);
			this.lbHistoria.Name = "lbHistoria";
			this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbHistoria.Size = new System.Drawing.Size(73, 19);
			this.lbHistoria.TabIndex = 6;
            // 
            // lbPaciente
            // 
            this.lbPaciente.Enabled = false;
			this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbPaciente.Location = new System.Drawing.Point(84, 4);
			this.lbPaciente.Name = "lbPaciente";
			this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbPaciente.Size = new System.Drawing.Size(337, 19);
			this.lbPaciente.TabIndex = 5;
			// 
			// Label23
			// 
			this.Label23.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label23.Location = new System.Drawing.Point(460, 4);
			this.Label23.Name = "Label23";
			this.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label23.Size = new System.Drawing.Size(65, 17);
			this.Label23.TabIndex = 4;
			this.Label23.Text = "Hist. cl�nica:";
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(4, 4);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(49, 17);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "Paciente:";
			// 
			// CCS820F2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(599, 333);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCerrar);
			this.Controls.Add(this.lbHistoria);
			this.Controls.Add(this.lbPaciente);
			this.Controls.Add(this.Label23);
			this.Controls.Add(this.Label1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CCS820F2";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Selecci�n de LE de Programas de Sesiones - CCS820F2";
			this.Closed += new System.EventHandler(this.CCS820F2_Closed);
			this.Load += new System.EventHandler(this.CCS820F2_Load);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}