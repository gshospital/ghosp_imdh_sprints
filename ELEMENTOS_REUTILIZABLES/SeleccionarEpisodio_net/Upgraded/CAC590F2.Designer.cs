using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	partial class CAC590F2
	{

		#region "Upgrade Support "
		private static CAC590F2 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static CAC590F2 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new CAC590F2();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbCerrar", "cbAceptar", "sprConsultas", "Frame1", "Label1", "Label23", "lbPaciente", "lbHistoria", "sprConsultas_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbCerrar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public UpgradeHelpers.Spread.FpSpread sprConsultas;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label23;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            
            this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CAC590F2));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.cbCerrar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.sprConsultas = new UpgradeHelpers.Spread.FpSpread();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Label23 = new Telerik.WinControls.UI.RadLabel();
			this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// cbCerrar
			// 
			this.cbCerrar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCerrar.Location = new System.Drawing.Point(520, 264);
			this.cbCerrar.Name = "cbCerrar";
			this.cbCerrar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCerrar.Size = new System.Drawing.Size(81, 32);
			this.cbCerrar.TabIndex = 2;
			this.cbCerrar.Text = "&Ce&rrar";
			this.cbCerrar.Click += new System.EventHandler(this.cbCerrar_Click);
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Enabled = false;
			this.cbAceptar.Location = new System.Drawing.Point(424, 264);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 32);
			this.cbAceptar.TabIndex = 1;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.sprConsultas);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(8, 38);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(595, 217);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Consultas realizadas";
			this.Frame1.Visible = true;
            // 
            // sprConsultas
            // 
            gridViewTextBoxColumn1.HeaderText = "Entidad financiador";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.WrapText = true;
            gridViewTextBoxColumn1.Width = 80;

            gridViewTextBoxColumn2.HeaderText = "N� asegura";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.WrapText = true;
            gridViewTextBoxColumn1.Width = 80;

            gridViewTextBoxColumn3.HeaderText = "Servicio";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 80;
            
            gridViewTextBoxColumn4.HeaderText = "Responsable";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 100;

            gridViewTextBoxColumn5.HeaderText = "Sala consulta";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.WrapText = true;
            gridViewTextBoxColumn5.Width = 70;

            gridViewTextBoxColumn6.HeaderText = "Prestaci�n";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 80;

            gridViewTextBoxColumn7.HeaderText = "Fecha consulta";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.WrapText = true;
            gridViewTextBoxColumn7.Width = 70;

            gridViewTextBoxColumn8.HeaderText = "Hora consulta";
            gridViewTextBoxColumn8.WrapText = true;
            gridViewTextBoxColumn8.Name = "column8";
            gridViewTextBoxColumn8.Width = 60;

            gridViewTextBoxColumn9.HeaderText = "N�m -";
            gridViewTextBoxColumn9.Name = "column9";
            gridViewTextBoxColumn9.Width = 80;

            gridViewTextBoxColumn10.HeaderText = "A�o Reg";
            gridViewTextBoxColumn10.Name = "Num Reg";
            gridViewTextBoxColumn10.Width = 80;

            gridViewTextBoxColumn11.HeaderText = "Num Reg";
            gridViewTextBoxColumn11.Name = "column11";
            gridViewTextBoxColumn11.Width = 80;

            gridViewTextBoxColumn12.HeaderText = "codServ";
            gridViewTextBoxColumn12.Name = "column12";
            gridViewTextBoxColumn12.Width = 80;

            gridViewTextBoxColumn13.HeaderText = "ganoPrin";
            gridViewTextBoxColumn13.Name = "column13";
            gridViewTextBoxColumn13.Width = 80;

            gridViewTextBoxColumn14.HeaderText = "gnumprin";
            gridViewTextBoxColumn14.Name = "column14";
            gridViewTextBoxColumn14.Width = 80;

            gridViewTextBoxColumn15.HeaderText = "dias";
            gridViewTextBoxColumn15.Name = "column15";
            
            gridViewTextBoxColumn16.HeaderText = "horas";
            gridViewTextBoxColumn16.Name = "column16";
            gridViewTextBoxColumn16.IsVisible = false;

            gridViewTextBoxColumn17.HeaderText = "ghospita";
            gridViewTextBoxColumn17.Name = "column17";
            gridViewTextBoxColumn17.IsVisible = false;

            gridViewTextBoxColumn18.HeaderText = "R";
            gridViewTextBoxColumn18.Name = "column18";
            gridViewTextBoxColumn18.IsVisible = false;

            gridViewTextBoxColumn19.HeaderText = "S";
            gridViewTextBoxColumn19.Name = "column19";

            gridViewTextBoxColumn20.HeaderText = "T";
            gridViewTextBoxColumn20.Name = "column20";

            gridViewTextBoxColumn21.HeaderText = "U";
            gridViewTextBoxColumn21.Name = "column21";

            gridViewTextBoxColumn22.HeaderText = "V";
            gridViewTextBoxColumn22.Name = "column22";


            this.sprConsultas.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] 
            {   gridViewTextBoxColumn1,
                gridViewTextBoxColumn2,
                gridViewTextBoxColumn3,
                gridViewTextBoxColumn4,
                gridViewTextBoxColumn5,
                gridViewTextBoxColumn6,
                gridViewTextBoxColumn7,
                gridViewTextBoxColumn8,
                gridViewTextBoxColumn9,
                gridViewTextBoxColumn10,
                gridViewTextBoxColumn11,
                gridViewTextBoxColumn12,
                gridViewTextBoxColumn13,
                gridViewTextBoxColumn14,
                gridViewTextBoxColumn15,
                gridViewTextBoxColumn16,
                gridViewTextBoxColumn17,
                gridViewTextBoxColumn18,
                gridViewTextBoxColumn19,
                gridViewTextBoxColumn20,
                gridViewTextBoxColumn21,
                gridViewTextBoxColumn22,
            });
            this.sprConsultas.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprConsultas.MasterTemplate.EnableSorting = false;
            this.sprConsultas.MasterTemplate.ReadOnly = true;
            this.sprConsultas.Location = new System.Drawing.Point(8, 18);
			this.sprConsultas.Name = "sprConsultas";
			this.sprConsultas.Size = new System.Drawing.Size(577, 193);
			this.sprConsultas.TabIndex = 7;
            this.sprConsultas.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprConsultas_CellClick);
            this.sprConsultas.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprConsultas_CellDoubleClick);
            this.sprConsultas.MouseDown += new System.Windows.Forms.MouseEventHandler(sprConsultas_MouseDown);
            // 
            // Label1
            // 
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(10, 6);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(49, 17);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "Paciente:";
			// 
			// Label23
			// 
			this.Label23.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label23.Location = new System.Drawing.Point(466, 6);
			this.Label23.Name = "Label23";
			this.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label23.Size = new System.Drawing.Size(65, 17);
			this.Label23.TabIndex = 5;
			this.Label23.Text = "Hist. cl�nica:";
            // 
            // lbPaciente
            // 
            this.lbPaciente.Enabled = false;
			this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbPaciente.Location = new System.Drawing.Point(90, 6);
			this.lbPaciente.Name = "lbPaciente";
			this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbPaciente.Size = new System.Drawing.Size(337, 19);
			this.lbPaciente.TabIndex = 4;
            // 
            // lbHistoria
            // 
            this.lbHistoria.Enabled = false;
			this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbHistoria.Location = new System.Drawing.Point(530, 6);
			this.lbHistoria.Name = "lbHistoria";
			this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbHistoria.Size = new System.Drawing.Size(73, 19);
			this.lbHistoria.TabIndex = 3;
			// 
			// CAC590F2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(613, 305);
			this.Controls.Add(this.cbCerrar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.Label23);
			this.Controls.Add(this.lbPaciente);
			this.Controls.Add(this.lbHistoria);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CAC590F2";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Selecci�n de consultas - CAC590F2";
			this.Closed += new System.EventHandler(this.CAC590F2_Closed);
			this.Load += new System.EventHandler(this.CAC590F2_Load);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}