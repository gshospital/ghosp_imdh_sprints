using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	public partial class LEQ001F1
		: Telerik.WinControls.UI.RadForm
    {

		public LEQ001F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void LEQ001F1_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		bool fFilaSeleccionada = false;
		int lRegistroSeleccionado = 0;


		public void Recibir_Parametros(string Paragidenpac, string ParaPrestacion)
		{
			int icodPrivado = 0;
			string stLiteralPrivado = String.Empty;
			Color lcolor = new Color();

			int icodSEgurSoc = Convert.ToInt32(Double.Parse(Serrores.ObternerValor_CTEGLOBAL(BasSelEpisodio.rcConexion, "SEGURSOC", "nnumeri1")));
			string sqlAepisleq = "SELECT NNUMERI1,VALFANU1 FROM  SCONSGLO WHERE gconsglo = 'PRIVADO'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sqlAepisleq, BasSelEpisodio.rcConexion);
			DataSet RrAepisleq = new DataSet();
			tempAdapter.Fill(RrAepisleq);
			if (RrAepisleq.Tables[0].Rows.Count != 0)
			{
				stLiteralPrivado = Convert.ToString(RrAepisleq.Tables[0].Rows[0]["VALFANU1"]).Trim();
				icodPrivado = Convert.ToInt32(RrAepisleq.Tables[0].Rows[0]["nnumeri1"]);
			}
			RrAepisleq.Close();
            Datos_Paciente(Paragidenpac);

			switch(((RadForm) BasSelEpisodio.Llamador).Name )
			{
				case "CCI310F4" : case "CCI310F2" : 
					sqlAepisleq = "SELECT distinct DSERVICI.dnomserv, DCODPRES.dprestac,  AEPISLEQ.gsocieda, DSOCIEDA.dsocieda, AEPISLEQ.ginspecc," + 
					              " DINSPECC.DINSPECC , AEPISLEQ.ganoregi, AEPISLEQ.GNUMREGI, AEPISLEQ.finclusi, AEPISLEQ.iestadop " + 
					              " From AEPISLEQ " + 
					              " INNER JOIN APREOLEQ ON APREOLEQ.GANOREGI=AEPISLEQ.GANOREGI AND " + 
					              "                        APREOLEQ.GNUMREGI=AEPISLEQ.GNUMREGI AND " + 
					              "                        APREOLEQ.IREALIZA='P' AND " + 
					              "                        APREOLEQ.gprestac='" + ParaPrestacion + "'" + 
					              " INNER JOIN DCODPRES on AEPISLEQ.gprestac = DCODPRES.gprestac " + 
					              " INNER JOIN DSERVICI ON AEPISLEQ.gservici = DSERVICI.gservici " + 
					              " INNER JOIN DSOCIEDA ON AEPISLEQ.gsocieda = DSOCIEDA.gsocieda " + 
					              " LEFT JOIN DINSPECC ON AEPISLEQ.ginspecc = DINSPECC.ginspecc " + 
					              " WHERE " + 
					              " AEPISLEQ.GIDENPAC = '" + Paragidenpac + "'" + 
					              " AND AEPISLEQ.FSALIDAL IS NULL AND AEPISLEQ.FDEVOLUC IS NULL "; 
					break;
			}
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(sqlAepisleq, BasSelEpisodio.rcConexion);
			RrAepisleq = new DataSet();
			tempAdapter_2.Fill(RrAepisleq);
			if (RrAepisleq.Tables[0].Rows.Count != 0)
			{
				SprPacientes.Row = 1;
				foreach (DataRow iteration_row in RrAepisleq.Tables[0].Rows)
				{
					SprPacientes.MaxRows = SprPacientes.Row;
					switch(Convert.ToString(iteration_row["iestadop"]))
					{
						case "E" :  
							lcolor = Line2.BorderColor;  //en estudio 
							break;
						case "P" :  
							lcolor = Line1.BorderColor;  //pendiente 
							break;
						case "A" :  
							lcolor = Line5.BorderColor;  //apto 
							break;
						case "N" :  
							lcolor = Line3.BorderColor;  //No aptos 
							break;
						case "I" :  
							lcolor = Line6.BorderColor;  //Intervenidos 
							break;
						case "F" :  
							lcolor = Line4.BorderColor;  //Devueltos 
							break;
					}
					SprPacientes.Col = 1;
					SprPacientes.Text = Convert.ToString(iteration_row["dnomserv"]).Trim();
					SprPacientes.ForeColor = lcolor;
					SprPacientes.Col = 2;
					SprPacientes.Text = Convert.ToString(iteration_row["dprestac"]).Trim();
					SprPacientes.ForeColor = lcolor;
					SprPacientes.Col = 3;
					SprPacientes.Text = "";
					int switchVar = Convert.ToInt32(iteration_row["gsocieda"]);
					if (switchVar == icodSEgurSoc)
					{
						SprPacientes.Text = Convert.ToString(iteration_row["dinspecc"]) + "";
					}
					else if (switchVar == icodPrivado)
					{ 
						SprPacientes.Text = stLiteralPrivado + "";
					}
					else
					{
						SprPacientes.Text = Convert.ToString(iteration_row["dsocieda"]) + "";
					}
					SprPacientes.ForeColor = lcolor;
					SprPacientes.Col = 4;
					SprPacientes.Text = Convert.ToDateTime(iteration_row["finclusi"]).ToString("dd/MM/yyyy");
					SprPacientes.ForeColor = lcolor;
					SprPacientes.Col = 5;
					SprPacientes.Text = Convert.ToString(iteration_row["ganoregi"]).Trim();
					SprPacientes.ForeColor = lcolor;
					SprPacientes.Col = 6;
					SprPacientes.Text = Convert.ToString(iteration_row["gnumregi"]).Trim();
					SprPacientes.ForeColor = lcolor;
					SprPacientes.Row++;
				}
			}
			RrAepisleq.Close();
            fFilaSeleccionada = false;
			cbAceptar.Enabled = false;

			LEQ001F1.DefInstance.ShowDialog();
		}
        

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int iAno = 0;
			int iNum = 0;
			if (fFilaSeleccionada)
			{
				SprPacientes.Col = 5;
				iAno = Convert.ToInt32(Double.Parse(SprPacientes.Text));
				SprPacientes.Col = 6;
				iNum = Convert.ToInt32(Double.Parse(SprPacientes.Text));
				BasSelEpisodio.Llamador.proRecogerLEQ(iAno, iNum);
				this.Close();
			}
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			BasSelEpisodio.Llamador.proRecogerLEQ(0, 0);
			this.Close();
		}

        private void LEQ001F1_Load(Object eventSender, EventArgs eventArgs)
		{
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
		}

        private void SprPacientes_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
        {
            int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			if (Row == 0)
			{ //Compruebo que es la cabecera

			}
			else
			{
				SprPacientes.Row = Row;
				if (lRegistroSeleccionado == Row)
				{
					//si el seleccionado coincide con el seleccionado anterior
					if (fFilaSeleccionada)
					{
                        SprPacientes.CurrentRow = null;
                        fFilaSeleccionada = false;
					}
					else
					{
                        fFilaSeleccionada = true;
					}
				}
				else
				{
                    fFilaSeleccionada = true;
				}
				if (fFilaSeleccionada)
				{
					cbAceptar.Enabled = true;
					lRegistroSeleccionado = Row;
					SprPacientes.Row = lRegistroSeleccionado;
				}
				else
				{
					cbAceptar.Enabled = false;
					//''''''''''
				}
			}
		}

		private void Datos_Paciente(string ParaPaciente)
		{
			string stsql = "SELECT ISNULL(DPACIENT.dnombpac, '') AS dnombpac, ISNULL(DPACIENT.dape1pac, '') AS dape1pac, " + 
			               "ISNULL(DPACIENT.dape2pac, '') AS dape2pac " + 
			               "FROM DPACIENT " + 
			               "WHERE (DPACIENT.gidenpac = '" + ParaPaciente + "' ) ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsPaciente = new DataSet();
			tempAdapter.Fill(rsPaciente);
			this.TxtPaciente.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["dnombpac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape2pac"]).Trim();
			rsPaciente.Close();
		}
		private void LEQ001F1_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
        }
	}
}