using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace SelEpisodio
{
    public partial class MDO220F1
        : RadForm
    {

		int iA�oEpisodio = 0;
		int lNumeroEpisodio = 0;
		string stFAltaEpisodio = String.Empty;
		int iSerultiEpisodio = 0;
		string stTipoEpisodio = String.Empty;
		public MDO220F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}
        
		private void CargarSprEpisodios(string Identificador)
		{
			//Carga en una spread todos los episodios
			string Comando = String.Empty;
			DataSet RrSQL = null;

			try
			{

				//*********************** O.Frias (SQL-2005) ***********************

				Comando = "SELECT B.dnomserv, A.fllegada, C.dnomserv , A.faltplan AS FALTPLAN, A.ganoadme AS GANOADME, " + 
				          "A.gnumadme AS GNUMADME, A.gservici, A.gserulti AS GSERULTI, 'H' AS TipoEpisodio " + 
				          "FROM AEPISADM AS A " + 
				          "INNER JOIN DSERVICI AS B ON A.gservici = B.gservici " + 
				          "LEFT OUTER JOIN DSERVICI AS C ON A.gserulti = C.gservici " + 
				          "WHERE (A.gidenpac = '" + Identificador.Trim() + "' ) ";

				if (BasSelEpisodio.stTipoLlamada.Trim().ToUpper() == "A")
				{
					Comando = Comando + "AND (A.faltplan IS NOT NULL) AND (A.faltplan <> '') ";
				}


				if (BasSelEpisodio.stTipoLlamada.Trim().ToUpper() == "U")
				{
					Comando = Comando + "UNION ";
					//para que ademas de los episodios de admisi�n salgan los de urgencias

					//*********************** O.Frias (SQL-2005) **********************


					Comando = Comando + "SELECT B.dnomserv, A.fllegada, C.dnomserv, A.faltaurg AS FALTPLAN, " + 
					          "A.ganourge AS GANOADME, A.gnumurge AS GNUMADME, A.gservici, " + 
					          "isnull(A.gseralta,'0') AS GSERULTI, 'U' AS TipoEpisodio " + 
					          "FROM UEPISURG AS A " + 
					          "INNER JOIN DSERVICI AS B ON A.gservici = B.gservici " + 
					          "LEFT OUTER JOIN DSERVICI AS C ON A.gseralta = C.gservici " + 
					          "WHERE (A.gidenpac = '" + Identificador.Trim() + "' ) ";


				}


				//' O.Frias - 20/05/2011
				//' En el caso de venir de urgencias no ser� necesanio que este dado de alta.
				if (BasSelEpisodio.vblnInformeUrgencia)
				{
					//Si este par�metro opcional viene, es porque se ha de mostrar s�lo los episodios de urgencias dados de alta.
					if (BasSelEpisodio.stEpiAlta == "S")
					{
						Comando = Comando.Substring(Comando.IndexOf(" UNION ") + 7);
					}

				}
				else
				{
					//Si este par�metro opcional viene, es porque se ha de mostrar s�lo los episodios de urgencias dados de alta.
					if (BasSelEpisodio.stEpiAlta == "S")
					{
						Comando = Comando.Substring(Comando.IndexOf(" UNION ") + 7) + " and A.faltaurg is not null ";
					}
				}

				//para que salgan todos los episodios
				//comando = comando & " and A.FALTPLAN is not NULL"
				//comando = comando & " and A.FALTPLAN<>''"
				Comando = Comando + " order by 2";

				SqlDataAdapter tempAdapter = new SqlDataAdapter(Comando, BasSelEpisodio.rcConexion);
				RrSQL = new DataSet();
				tempAdapter.Fill(RrSQL);

                foreach (DataRow iteration_row in RrSQL.Tables[0].Rows)
				{
					SprEpisodios.MaxRows++;
					SprEpisodios.Row = SprEpisodios.MaxRows;
					SprEpisodios.Col = 1;
					SprEpisodios.Text = Strings.StrConv(Convert.ToString(iteration_row[0]).Trim(), VbStrConv.ProperCase, 0); //DNOMSERV
					SprEpisodios.Col = 2;
                    
					SprEpisodios.Columns[SprEpisodios.Col - 1].TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
                    System.DateTime TempDate = DateTime.FromOADate(0);
					SprEpisodios.Text = ((DateTime.TryParse(Convert.ToString(iteration_row[1]).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : Convert.ToString(iteration_row[1]).Trim()).Substring(0, Math.Min(10, ((DateTime.TryParse(Convert.ToString(iteration_row[1]).Trim(), out TempDate)) ? TempDate.ToString("dd/MM/yyyy") : Convert.ToString(iteration_row[1]).Trim()).Length)); //FLLEGADA
                    SprEpisodios.Col = 3;
                    SprEpisodios.Columns[SprEpisodios.Col - 1].TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
                    System.DateTime TempDate2 = DateTime.FromOADate(0);
                    SprEpisodios.Text = (DateTime.TryParse(Convert.ToString(iteration_row[1]).Trim(), out TempDate2)) ? TempDate2.ToString("HH:mm") : Convert.ToString(iteration_row[1]).Trim(); //FLLEGADA
                    SprEpisodios.Col = 4; //DNOMSER
                    
					if (Convert.IsDBNull(iteration_row[2]))
					{
						SprEpisodios.Text = "";
					}
					else
					{
						SprEpisodios.Text = Strings.StrConv(Convert.ToString(iteration_row[2]).Trim(), VbStrConv.ProperCase, 0);
					}
					SprEpisodios.Col = 5; //FALTPLAN
					if (Convert.IsDBNull(iteration_row[3]))
					{
						SprEpisodios.Text = "";
						SprEpisodios.Col = 6;
						SprEpisodios.Text = "";
					}
					else
					{
                        SprEpisodios.Columns[SprEpisodios.Col - 1].TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
						System.DateTime TempDate3 = DateTime.FromOADate(0);
						SprEpisodios.Text = ((DateTime.TryParse(Convert.ToString(iteration_row[3]).Trim(), out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : Convert.ToString(iteration_row[3]).Trim()).Substring(0, Math.Min(10, ((DateTime.TryParse(Convert.ToString(iteration_row[3]).Trim(), out TempDate3)) ? TempDate3.ToString("dd/MM/yyyy") : Convert.ToString(iteration_row[3]).Trim()).Length)); //FLLEGADA
						SprEpisodios.Col = 6;
						SprEpisodios.Columns[SprEpisodios.Col - 1].TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
						System.DateTime TempDate4 = DateTime.FromOADate(0);
						SprEpisodios.Text = (DateTime.TryParse(Convert.ToString(iteration_row[3]).Trim(), out TempDate4)) ? TempDate4.ToString("HH:mm") : Convert.ToString(iteration_row[3]).Trim(); //FLLEGADA
					}

					SprEpisodios.Col = 7;
					SprEpisodios.Text = Conversion.Val(Convert.ToString(iteration_row["GANOADME"])).ToString();
					SprEpisodios.Col = 8;
					SprEpisodios.Text = Conversion.Val(Convert.ToString(iteration_row["GNUMADME"])).ToString();
					SprEpisodios.Col = 9;
					SprEpisodios.Text = Conversion.Val(Convert.ToString(iteration_row["GSERULTI"])).ToString();
					SprEpisodios.Col = 10;
					SprEpisodios.Text = Convert.ToDateTime(iteration_row["FALTPLAN"]).ToString("dd/MM/yyyy HH:mm:ss");
					SprEpisodios.Col = 11;
					SprEpisodios.Text = Convert.ToString(iteration_row["TipoEpisodio"]).Trim();
				}
				RrSQL.Close();

				if (BasSelEpisodio.stTipoLlamada != "SM")
				{
					if (SprEpisodios.MaxRows != 0)
					{
                        SprEpisodios.Row = 1;
						CargarDatos(1);
						SprEpisodios.Action = 0;
					}
				}
				else
				{
                    CargarDatos(1);
					//    SprEpisodios.Row = -1
					//    SprEpisodios.Col = -1
					//SprEpisodios.ActiveCol = Nothing
					//SprEpisodios.ActiveRow = 0
					for (int i = 0; i < SprEpisodios.MaxCols; i++)
					{
						for (int j = 0; j < SprEpisodios.MaxRows; j++)
						{
							SprEpisodios.Col = i;
							SprEpisodios.Row = j;
							SprEpisodios.Lock = true;
						}
					}
					SprEpisodios.Row = -1;
					SprEpisodios.Col = -1;
				}
			}
			catch
			{
				// Call ErrorSelect
			}
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//Si se le llama desde SSMM para imprimir el informe de alta de urgencias, se recibe este par�metro y es visible
			//el marco que contiene la selecci�n del modo de impresi�n.
			if (optPantalla.Visible)
			{
				if (optPantalla.IsChecked)
				{
					stTipoEpisodio = stTipoEpisodio + "P";
				}
				else
				{
					stTipoEpisodio = stTipoEpisodio + "I";
				}
			}
			if (SprEpisodios.Row != 0)
			{
			}
			if (this.SprEpisodios.Row == -1)
			{
				iA�oEpisodio = 0;
				lNumeroEpisodio = 0;
			}
			//debo tener seleccionado un registro para devolver la dll
			BasSelEpisodio.Llamador.RecogerEpisodio(iA�oEpisodio, lNumeroEpisodio, stFAltaEpisodio, iSerultiEpisodio, stTipoEpisodio);
            this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			BasSelEpisodio.Llamador.RecogerEpisodio(0, 0, "00/00/0000", 0, "CANCELA");
			this.Close();
		}

		private void MDO220F1_Load(Object eventSender, EventArgs eventArgs)
		{
			try
			{
                CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

				this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
				this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);

				if (BasSelEpisodio.stEpiAlta == "S")
				{
					frmImpresion.Visible = true;
				}

                SprEpisodios.MaxCols = 11;
                SprEpisodios.Columns[0].HeaderText = "Servicio de Ingreso";
                SprEpisodios.Columns[0].Width = 100;
                SprEpisodios.Columns[1].HeaderText = "Fecha de Ingreso";
                SprEpisodios.Columns[1].Width = 50;
                SprEpisodios.Columns[1].WrapText = true;
                SprEpisodios.Columns[2].HeaderText = "Hora de Ingreso";
                SprEpisodios.Columns[2].Width = 30;
                SprEpisodios.Columns[2].WrapText = true;
                SprEpisodios.Columns[3].HeaderText = "Servicio de Alta";
                SprEpisodios.Columns[3].Width = 100;
                SprEpisodios.Columns[4].HeaderText = "Fecha de Alta";
                SprEpisodios.Columns[4].Width = 50;
                SprEpisodios.Columns[4].WrapText = true;
                SprEpisodios.Columns[5].HeaderText = "Hora de Alta";
                SprEpisodios.Columns[5].Width = 40;
                SprEpisodios.Columns[5].WrapText = true;

                SprEpisodios.MaxRows = 0;
				SprEpisodios.Col = 7;
				SprEpisodios.SetColHidden(SprEpisodios.Col, true);
				SprEpisodios.Col = 8;
				SprEpisodios.SetColHidden(SprEpisodios.Col, true);
				SprEpisodios.Col = 9;
				SprEpisodios.SetColHidden(SprEpisodios.Col, true);
				SprEpisodios.Col = 10;
				SprEpisodios.SetColHidden(SprEpisodios.Col, true);
				SprEpisodios.Col = 11;
				SprEpisodios.SetColHidden(SprEpisodios.Col, true);

				//con el identificador que nos llega buscamos los datos DEL PACIENTE
				//para cargar los datos en pantalla
				BuscarDatosPaciente(BasSelEpisodio.gstIdenPaciente);

				CargarSprEpisodios(BasSelEpisodio.gstIdenPaciente);
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("Seleccion Episodio", Path.GetDirectoryName(Application.ExecutablePath), BasSelEpisodio.gUsuario, "SeleccionEpisodio:Form_load", ex);
			}
		}

        private void SprEpisodios_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
		{
            if (eventArgs != null)
            {
                SprEpisodios.Row = eventArgs.RowIndex + 1;
            }
            else
            {
                SprEpisodios.Row = SprEpisodios.ActiveRowIndex;
            }

			//cuando selecciono un episodio
			if (SprEpisodios.Row <= 0)
			{
				return;
			}
			else
			{
				CargarDatos(SprEpisodios.Row);
			}
		}

		private void SprEpisodios_KeyUp(Object eventSender, KeyEventArgs eventArgs)
		{
			if (SprEpisodios.MaxRows == 0)
			{
				return;
			}
			SprEpisodios_CellClick(SprEpisodios, null);
		}

		private void SprEpisodios_MouseUp(Object eventSender, MouseEventArgs eventArgs)
		{
			SprEpisodios.Row = SprEpisodios.ActiveRowIndex;
		}

		private void BuscarDatosPaciente(string Paciente)
		{
			string stCadena = String.Empty;

			//************************* O.Frias (SQL-2005) *************************

			string sql = "SELECT ISNULL(DPACIENT.dnombpac, '') AS dnombpac, ISNULL(DPACIENT.dape1pac, '') AS dape1pac, " + 
			             "ISNULL(DPACIENT.dape2pac, '') AS dape2pac, HDOSSIER.ghistoria " + 
			             "FROM DPACIENT " + 
			             "LEFT OUTER JOIN  HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac " + 
			             "WHERE (DPACIENT.gidenpac = '" + Paciente + "' ) ";
			//************************* O.Frias (SQL-2005) *************************

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BasSelEpisodio.rcConexion);
			DataSet RrSQL = new DataSet();
			tempAdapter.Fill(RrSQL);
			if (RrSQL.Tables[0].Rows.Count != 0)
			{
				LB_Hist.Text = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["ghistoria"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["ghistoria"]).Trim();
				stCadena = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["dnombpac"]).Trim();
				stCadena = stCadena + ((Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dape1pac"])) ? "" : " " + Convert.ToString(RrSQL.Tables[0].Rows[0]["dape1pac"]).Trim());
				stCadena = stCadena + ((Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dape2pac"])) ? "" : " " + Convert.ToString(RrSQL.Tables[0].Rows[0]["dape2pac"]).Trim());
				lb_paciente.Text = stCadena;
			}
			else
			{
				LB_Hist.Text = "";
				lb_paciente.Text = "";
			}
			RrSQL.Close();
		}

		private void CargarDatos(int Fila)
		{
			SprEpisodios.Row = Fila;
			SprEpisodios.Col = 10;
			if (SprEpisodios.Text.Trim() == "")
			{
				stFAltaEpisodio = "";
			}
			else
			{
				stFAltaEpisodio = SprEpisodios.Text.Trim();
			}

			SprEpisodios.Col = 9;
			iSerultiEpisodio = Convert.ToInt32(Double.Parse(SprEpisodios.Text));

			SprEpisodios.Col = 7;
			iA�oEpisodio = Convert.ToInt32(Conversion.Val(SprEpisodios.Text));

			SprEpisodios.Col = 8;
			lNumeroEpisodio = Convert.ToInt32(Double.Parse(SprEpisodios.Text));

			SprEpisodios.Col = 11;
			stTipoEpisodio = SprEpisodios.Text.Trim();

		}
		private void MDO220F1_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
		}
	}
}