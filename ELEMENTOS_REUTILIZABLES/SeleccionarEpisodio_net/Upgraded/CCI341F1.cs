using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;

namespace SelEpisodio
{
	public partial class CCI341F1
		: Telerik.WinControls.UI.RadForm
    {


		public bool fFilaSeleccionada = false; //Indica si hay o no una fila marcada en el spread
		public int iFilaSeleccionada = 0; //Indsica cual fu� la �ltima fila que se marc�
		int iSeleccionada = 0;
        private bool _isMouseLeftDown = false;

        bool MostrarMensaje = false;

		bool bCTRLAUTO = false;
		string stCodSociedad = String.Empty;
		string stCodInspeccion = String.Empty;
		public CCI341F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			ReLoadForm(false);
		}
        
		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int iA�oRegistro = 0;
			int iNumeroRegistro = 0;
			string CodSocedad = String.Empty;
			string CodInspeccion = String.Empty;

			if (fFilaSeleccionada)
			{

				sprCitasP.Col = 14;
				CodSocedad = sprCitasP.Text.Trim();
				sprCitasP.Col = 15;
				CodInspeccion = sprCitasP.Text.Trim();

				if (Serrores.ObternerValor_CTEGLOBAL(BasSelEpisodio.rcConexion, "MANSOCIR", "VALFANU1") == "S" && stCodSociedad != CodSocedad && (stCodInspeccion != CodInspeccion || stCodInspeccion == ""))
				{
					if (((DialogResult) Convert.ToInt32(BasSelEpisodio.claseMensaje.RespuestaMensaje("Selecci�n de Consultas", "", 1125, BasSelEpisodio.rcConexion, "Los datos econ�micos seleccionados para el paciente, " + "\n" + "\r" + "son distintos a los del episodio elegido" + "." + "\n" + "\r" + "Esto puede producir efectos indeseados", "continuar"))) != System.Windows.Forms.DialogResult.Yes)
					{
						this.Cursor = Cursors.Default;
						return;
					}
				}


				sprCitasP.Col = 9;
				iA�oRegistro = Convert.ToInt32(Conversion.Val(sprCitasP.Text));
				sprCitasP.Col = 10;
				iNumeroRegistro = Convert.ToInt32(Conversion.Val(sprCitasP.Text));

				BasSelEpisodio.Llamador.RecogerEpisodio(iA�oRegistro, iNumeroRegistro);
			}
			else
			{
				BasSelEpisodio.Llamador.RecogerEpisodio(0, 0);
			}

			this.Close();
		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
			BasSelEpisodio.Llamador.RecogerEpisodio(0, 0);
		}


		private void CCI341F1_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;

                CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

				Application.DoEvents();
				sprCitasP.Row = -1;

				// Se obtienen las citas del paciente
				sprCitasP.MaxRows = 0;
                iFilaSeleccionada = 0;
				fFilaSeleccionada = false;
				cbAceptar.Enabled = true;

				MostrarMensaje = true; //mostrar mensaje de q el paciente no tiene citas
				string stsql = "SELECT valfanu1 FROM sconsglo WHERE gconsglo = 'MENCITAS'";
				SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
				DataSet RrSQL = new DataSet();
				tempAdapter.Fill(RrSQL);
				if (RrSQL.Tables[0].Rows.Count != 0)
				{
					if (Convert.ToString(RrSQL.Tables[0].Rows[0]["valfanu1"]).Trim().ToUpper() != "S")
					{
						MostrarMensaje = false;
					}
				}
				RrSQL.Close();

				bCTRLAUTO = false;
				//diego 21/03/2007 - oculto las columnas con los n�meros de autorizaci�n, DU y oper. electr�nica dependiendo del valor de la cte
				if (Serrores.ObternerValor_CTEGLOBAL(BasSelEpisodio.rcConexion, "CTRLAUTO", "valfanu1") == "S")
				{
					bCTRLAUTO = true;
				}
				sprCitasP.Col = 11;
				sprCitasP.SetColHidden(sprCitasP.Col, !bCTRLAUTO);
				sprCitasP.Col = 12;
				sprCitasP.SetColHidden(sprCitasP.Col, !bCTRLAUTO);
				sprCitasP.Col = 13;
				sprCitasP.SetColHidden(sprCitasP.Col, !bCTRLAUTO);

				lblCitasNoAutorizadas.Visible = bCTRLAUTO;
				//con el identificador que nos llega buscamos los datos DEL PACIENTE
				//para cargar los datos en pantalla
				Datos_Paciente();
				Cargar_Citas_Programadas();

				if (sprCitasP.MaxRows == 0)
				{
					if (MostrarMensaje)
					{
						BasSelEpisodio.claseMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1520, BasSelEpisodio.rcConexion, "citas programadas", "el paciente seleccionado");
					}
					cbAceptar_Click(cbAceptar, new EventArgs());
				}

			}
		}


		private void sprCitasP_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
        {
            
            if (_isMouseLeftDown)
            {
                int Col = eventArgs.ColumnIndex + 1;
				int Row = eventArgs.RowIndex + 1;
				sprCitasP.Col = 1;
				sprCitasP.Row = Row;

				if (Row > 0)
				{

					if (iFilaSeleccionada == Row)
					{
						if (fFilaSeleccionada)
						{
                            sprCitasP.CurrentRow = null;
                            fFilaSeleccionada = false;
						}
						else
						{
                            fFilaSeleccionada = true;
						}
					}
					else
					{
                        fFilaSeleccionada = true;
					}
					iFilaSeleccionada = Row;
				}
            }
		}

        private void sprCitasP_CellDoubleClick(object eventSender, GridViewCellEventArgs eventArgs)            
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;

			if (Row > 0)
			{
				sprCitasP.Row = Row;
				cbAceptar_Click(cbAceptar, new EventArgs());
			}

		}

        /*INDRA JPROCHE TODO_X_4 03/05/2016
		private void sprCitasP_LeaveRow(object eventSender, UpgradeHelpers.Spread.FpSpread.LeaveRowEventArgs eventArgs)
		{
			if (NewRow != -1)
			{
				sprCitasP.Col = 1;
				sprCitasP.Row = NewRow;
				iSeleccionada = Row;
				if (ColorTranslator.ToOle(sprCitasP.ForeColor) == 0)
				{
					//SePrograma (False)
				}
				else
				{
					//SePrograma (True)
				}
				fFilaSeleccionada = true; //Al hacer click alguna se selecciona
			}
		}
        */

		private void Cargar_Citas_Programadas()
		{
			Color Color = new Color();

			switch(BasSelEpisodio.iotrprof)
			{
				case "P" :  
					BasSelEpisodio.stAgrupacion = Serrores.ObternerValor_CTEGLOBAL(BasSelEpisodio.rcConexion, "GRUPPSIC", "VALFANU1"); 
					break;
				case "T" :  
					BasSelEpisodio.stAgrupacion = Serrores.ObternerValor_CTEGLOBAL(BasSelEpisodio.rcConexion, "GRUPTEOC", "VALFANU1"); 
					break;
			}

			string tempRefParam = DateTimeHelper.ToString(DateTime.Today) + " 23:59";
			string stsql = "SELECT ccitprog.ganoregi,ccitprog.gnumregi,ncitapac,dagendas,ccitprog.gsocieda,ccitprog.ginspecc, " + 
			               "ffeccita,nordcita,dnomserv," + 
			               "cagendas.*,dcodpres.dprestac, " + 
			               "rtrim(ISNULL(dpersona.dap1pers,''))+' '+rtrim(ISNULL(dpersona.dap2pers,''))+', '+rtrim(ISNULL(dpersona.dnompers,'')) dpersona, " + 
			               "dsalacon, " + 
			               "ifmsfa_autoriza.dnautori, ifmsfa_autoriza.dnvolant, ifmsfa_autoriza.dnopelec,ifmsfa_autoriza.gestsoli   " + 
			               "FROM " + 
			               "ccitprog INNER JOIN cagendas ON ccitprog.gagendas = cagendas.gagendas " + 
			               " INNER JOIN dcodpres ON ccitprog.gprestac = dcodpres.gprestac " + 
			               " INNER JOIN dservici ON ccitprog.gservici = dservici.gservici " + 
			               " INNER JOIN dpersona ON cagendas.gpersona = dpersona.gpersona " + 
			               " LEFT JOIN dsalacon ON cagendas.gsalacon = dsalacon.gsalacon " + 
			               " LEFT JOIN ifmsfa_activtau ON " + 
			               "        (ccitprog.gprestac = ifmsfa_activtau.econc AND " + 
			               "         ccitprog.ganoregi = ifmsfa_activtau.ganoregi AND " + 
			               "         ccitprog.gnumregi = ifmsfa_activtau.gnumregi AND " + 
			               "         ifmsfa_activtau.itipacti = 'P' AND " + 
			               "         ifmsfa_activtau.itiposer='C' AND " + 
			               "         ifmsfa_activtau.rconc='PR')  " + 
			               " LEFT JOIN ifmsfa_autoriza on " + 
			               "        (ifmsfa_activtau.ganoauto = ifmsfa_autoriza.ganoauto and " + 
			               "         ifmsfa_activtau.gnumauto = ifmsfa_autoriza.gnumauto) " + 
			               "WHERE ccitprog.gidenpac = '" + BasSelEpisodio.gstIdenPaciente + "' AND " + 
			               "      ccitprog.ffeccita <= " + Serrores.FormatFechaHM(tempRefParam);

			//stsql = "SELECT ganoregi,gnumregi,ncitapac,dagendas," & _
			//"ffeccita,nordcita,dnomserv," & _
			//"cagendas.*,dcodpres.dprestac, " & _
			//"rtrim(ISNULL(dpersona.dap1pers,''))+' '+rtrim(ISNULL(dpersona.dap2pers,''))+', '+rtrim(ISNULL(dpersona.dnompers,'')) dpersona, " & _
			//"dsalacon " & _
			//"FROM ccitprog, cagendas, dcodpres, dservici, dpersona, dsalacon  " & _
			//"WHERE gidenpac = '" & gstIdenPaciente & "' AND " & _
			//"ccitprog.gagendas = cagendas.gagendas AND " & _
			//"ccitprog.gprestac = dcodpres.gprestac AND " & _
			//"ccitprog.gservici = dservici.gservici AND " & _
			//"cagendas.gpersona = dpersona.gpersona AND " & _
			//"cagendas.gsalacon *= dsalacon.gsalacon AND " & _
			//"ccitprog.ffeccita <= " & FormatFechaHM(Date & " 23:59")

			//Jes�s 19/06/2006 Si viene de Otros Profesionales - Psicolog�a debe filtrar por las
			//Consultas de la agrupaci�n de psicolog�a
			if (BasSelEpisodio.iotrprof == "P")
			{
				stsql = stsql + " and dservici.ggrupser = '" + BasSelEpisodio.stAgrupacion + "' ";
			}
			//fin 19/06/2006

			string sql = "SELECT ISNULL(iperfqui,'G') iperfqui, gpersona FROM SUSUARIO WHERE GUSUARIO = '" + BasSelEpisodio.gUsuario + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BasSelEpisodio.rcConexion);
			DataSet rsAux = new DataSet();
			tempAdapter.Fill(rsAux);
			switch(Convert.ToString(rsAux.Tables[0].Rows[0]["iperfqui"]).Trim().ToUpper())
			{
				case "G" : 
					break;
				case "J" :  
					stsql = stsql + 
					        " AND ccitprog.gservici IN (SELECT gservici FROM DPERSERV where gpersona = " + Convert.ToString(rsAux.Tables[0].Rows[0]["gpersona"]) + ") "; 
					break;
				case "C" :  
					stsql = stsql + 
					        " AND Ccitprog.gpersona = " + Convert.ToString(rsAux.Tables[0].Rows[0]["gpersona"]); 
					break;
			}
			rsAux.Close();

			stsql = stsql + " ORDER BY ffeccita desc ,nordcita";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet Rs = new DataSet();
			tempAdapter_2.Fill(Rs);

			if (Rs.Tables[0].Rows.Count > 0)
			{
				foreach (DataRow iteration_row in Rs.Tables[0].Rows)
				{

					sprCitasP.MaxRows++;
					if (Convert.ToDateTime(iteration_row["ffeccita"]).ToString("dd/MM/yyyy") == DateTime.Today.ToString("dd/MM/yyyy"))
					{
						Color = System.Drawing.Color.Red;
					}
					else
					{
						Color = System.Drawing.Color.Black;
					}

					sprCitasP.Row = sprCitasP.MaxRows;
					sprCitasP.Col = 1;
					sprCitasP.Text = Convert.ToString(iteration_row["dagendas"]);
					sprCitasP.ForeColor = Color;
					sprCitasP.Col = 2;
					sprCitasP.Text = Convert.ToString(iteration_row["dnomserv"]);
					sprCitasP.ForeColor = Color;
					sprCitasP.Col = 3;
					sprCitasP.Text = Convert.ToString(iteration_row["dpersona"]);
					sprCitasP.ForeColor = Color;
					sprCitasP.Col = 4;
					sprCitasP.Text = Convert.ToString(iteration_row["dsalacon"]) + "";
					sprCitasP.ForeColor = Color;

					//Fecha, hora y numero de orden de la cita
					sprCitasP.Col = 5;
					sprCitasP.Text = Convert.ToDateTime(iteration_row["ffeccita"]).ToString("dd/MM/yyyy");
					sprCitasP.ForeColor = Color;
					sprCitasP.Col = 6;
					sprCitasP.Text = Convert.ToDateTime(iteration_row["ffeccita"]).ToString("HH:mm");
					sprCitasP.ForeColor = Color;
					sprCitasP.Col = 7;
					sprCitasP.Text = Convert.ToString(iteration_row["nordcita"]);
					sprCitasP.ForeColor = Color;
					// Se obtiene la descripcion de la prestacion
					sprCitasP.Col = 8;
					sprCitasP.Text = Convert.ToString(iteration_row["dprestac"]);
					sprCitasP.ForeColor = Color;
					//Se a�aden al Spread el A�o y el n�mero de registro de la cita programada
					sprCitasP.Col = 9;
					sprCitasP.Text = Convert.ToString(iteration_row["ganoregi"]);
					sprCitasP.ForeColor = Color;
					sprCitasP.Col = 10;
					sprCitasP.Text = Convert.ToString(iteration_row["gnumregi"]);
					sprCitasP.ForeColor = Color;

					//DIEGO - 22/03/2007 - A�ado las nuevas columnas
					sprCitasP.Col = 11;
					sprCitasP.SetColHidden(sprCitasP.Col, !bCTRLAUTO);
					sprCitasP.Text = (Convert.IsDBNull(iteration_row["dnautori"])) ? "" : Convert.ToString(iteration_row["dnautori"]);
					sprCitasP.Col = 12;
					sprCitasP.SetColHidden(sprCitasP.Col, !bCTRLAUTO);
					sprCitasP.Text = (Convert.IsDBNull(iteration_row["dnvolant"])) ? "" : Convert.ToString(iteration_row["dnvolant"]);
					sprCitasP.Col = 13;
					sprCitasP.SetColHidden(sprCitasP.Col, !bCTRLAUTO);
					sprCitasP.Text = (Convert.IsDBNull(iteration_row["dnopelec"])) ? "" : Convert.ToString(iteration_row["dnopelec"]);
					sprCitasP.Col = 14;
					sprCitasP.Text = ((Convert.IsDBNull(iteration_row["gsocieda"])) ? "" : Convert.ToString(iteration_row["gsocieda"])).Trim();
					sprCitasP.ForeColor = Color;

					sprCitasP.Col = 15;
					sprCitasP.Text = ((Convert.IsDBNull(iteration_row["ginspecc"])) ? "" : Convert.ToString(iteration_row["ginspecc"])).Trim();
					sprCitasP.ForeColor = Color;

					if (!Convert.IsDBNull(iteration_row["gestsoli"]))
					{
						if (Convert.IsDBNull(iteration_row["dnautori"]) && bCTRLAUTO && Convert.ToString(iteration_row["gestsoli"]).ToUpper() != "A")
						{
							int tempForVar = sprCitasP.MaxCols;
							for (int x = 0; x < tempForVar; x++)
							{
								sprCitasP.Col = x;
								sprCitasP.BackColor = lblCitasNoAutorizadas.BackColor;
							}
						}
					}
					//------
				}

			}
			Rs.Close();
		}


		private void Datos_Paciente()
		{
			string stsql = "SELECT gserarch " + 
			               "FROM hserarch " + 
			               "WHERE icentral = 'S'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsCentral = new DataSet();
			tempAdapter.Fill(rsCentral);
			//****************** OFrias (SQL-2005) ******************

			stsql = "SELECT ISNULL(DPACIENT.dnombpac,'') AS dnombpac, ISNULL(DPACIENT.dape1pac,'') AS dape1pac, " + 
			        "ISNULL(DPACIENT.dape2pac,'') AS dape2pac, HDOSSIER.ghistoria, " + 
			        "DPACIENT.gsocieda as gsocieda , ISNULL(DENTIPAC.ginspecc,'') ginspecc " + 
			        "FROM DPACIENT " + 
			        "LEFT OUTER JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac AND " + 
			        "HDOSSIER.icontenido = 'H' AND HDOSSIER.gserpropiet = " + Convert.ToString(rsCentral.Tables[0].Rows[0]["gserarch"]) + " " + 
			        "LEFT JOIN DENTIPAC ON DPACIENT.gsocieda = DENTIPAC.gsocieda " + 
			        "LEFT JOIN DPRIVADO ON DPACIENT.gidenpac = DPRIVADO.gidenpac and " + 
			        "DPACIENT.gsocieda=(SELECT NNUMERI1 FROM SCONSGLO WHERE GCONSGLO='PRIVADO') " + 
			        "WHERE (DPACIENT.gidenpac = '" + BasSelEpisodio.gstIdenPaciente + "' )";


			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsPaciente = new DataSet();
			tempAdapter_2.Fill(rsPaciente);


			//****************** OFrias (SQL-2005) ******************

			this.lbPaciente.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["dnombpac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape2pac"]).Trim();
			if (!Convert.IsDBNull(rsPaciente.Tables[0].Rows[0]["ghistoria"]))
			{
				lbHistoria.Text = Conversion.Str(rsPaciente.Tables[0].Rows[0]["ghistoria"]);
			}
			stCodSociedad = Convert.ToString(rsPaciente.Tables[0].Rows[0]["gsocieda"]).Trim();
			stCodInspeccion = Convert.ToString(rsPaciente.Tables[0].Rows[0]["ginspecc"]).Trim();
			rsPaciente.Close();
			rsCentral.Close();

		}
		private void CCI341F1_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
        }
        private void sprCitasP_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _isMouseLeftDown = true;
            }
            else
            {
                _isMouseLeftDown = false;
            }
        }
    }
}