using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Helpers;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace SelEpisodio
{
	public partial class MDO221F1
		: Telerik.WinControls.UI.RadForm
    {

		public MDO221F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void MDO221F1_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}

		const int COL_AREASERV = 1;
		const int COL_FECHAEPI = 2;
		const int COL_DESCSERV = 3;
		const int COL_DESCMEDI = 4;
		const int COL_DIAGNOST = 5;
		const int COL_ACTUAMED = 6;
		const int COL_MOTATEND = 7;
		const int COL_CAMAPACI = 8;

		const int COL_GANOREGI = 9;
		const int COL_GNUMREGI = 10;




		string stAñoEpisodio = String.Empty;
		string stNumeroEpisodio = String.Empty;
		string stTipoEpisodio = String.Empty;
		string stFEpisodio = String.Empty;
		string stCamaEpisodio = String.Empty;


		private void CargarSprEpisodios(string Identificador, string stUsuario)
		{
			//Carga en una spread todos los episodios
			string stSqlH = String.Empty;
			string stSqlC = String.Empty;
			string stSqlQ = String.Empty;
			string stSqlU = String.Empty;
			string stSqlO = String.Empty;
			string stSqlN = String.Empty;

			string stcomando = String.Empty;
			DataSet RrSQL = null;

			try
			{

				stSqlC = "SELECT 'C',ganoregi,gnumregi,ffeccita,dnomserv,dnompers,dap1pers,dap2pers, ";
				stSqlC = stSqlC + "CASE WHEN GDIAGNOS IS NULL THEN DDIAGNOS ELSE (SELECT DNOMBDIA FROM DDIAGNOS WHERE DDIAGNOS.GCODIDIA=GDIAGNOS) END, DPRESTAC,OMOTATEN,null,null,null FROM CCONSULT ";
				stSqlC = stSqlC + "INNER JOIN DSERVICI ON CCONSULT.GSERVICI = DSERVICI.GSERVICI  ";
				stSqlC = stSqlC + "INNER JOIN DPERSONA ON CCONSULT.GPERSONA = DPERSONA.GPERSONA ";
				stSqlC = stSqlC + "INNER JOIN DCODPRES ON CCONSULT.GPRESTAC = DCODPRES.GPRESTAC WHERE (ITIPOSER is null or ITIPOSER NOT IN ('H','U')) and GIDENPAC ='" + Identificador + "' and CCONSULT.GSERVICI in (select dperserv.gservici from dperserv inner join susuario on dperserv.gpersona = susuario.gpersona where gusuario = '" + stUsuario + "') ";

				stSqlQ = "SELECT 'Q',ganoregi,gnumregi,finterve,dnomserv,dnompers,dap1pers,dap2pers,ODIAGPOS, A.DPRESTAC, B.DPRESTAC,null,null,null FROM QINTQUIR ";
				stSqlQ = stSqlQ + "INNER JOIN DSERVICI ON QINTQUIR.GSERVICI = DSERVICI.GSERVICI INNER JOIN DPERSONA ON QINTQUIR.GMEDICOC = DPERSONA.GPERSONA INNER JOIN DCODPRES A ON QINTQUIR.GPROCPOS = A.GPRESTAC ";
				stSqlQ = stSqlQ + "INNER JOIN DCODPRES B ON QINTQUIR.GPRESTAC = B.GPRESTAC WHERE GIDENPAC ='" + Identificador + "' and QINTQUIR.GSERVICI in (select dperserv.gservici from dperserv inner join susuario on dperserv.gpersona = susuario.gpersona where gusuario = '" + stUsuario + "') ";

				stSqlU = "SELECT 'U',ganourge,gnumurge,fllegada,dnomserv,dnompers,dap1pers,dap2pers, ";
				stSqlU = stSqlU + "CASE WHEN GDIAGNOS IS NULL THEN ODIAGNOS ELSE (SELECT DNOMBDIA FROM DDIAGNOS WHERE DDIAGNOS.GCODIDIA=GDIAGNOS) END,DCAUSING, ";
				stSqlU = stSqlU + "CASE WHEN GMOTATEN IS NULL THEN OMOTATEN ELSE (SELECT DMOTINGR FROM DMOTINGR WHERE DMOTINGR.GMOTINGR=GMOTATEN) END,GPLANTAS,GHABITAC,GCAMASBO FROM UEPISURG ";
				stSqlU = stSqlU + "LEFT JOIN DSERVICI ON UEPISURG.GSERVICI = DSERVICI.GSERVICI ";
				stSqlU = stSqlU + "LEFT JOIN DPERSONA ON UEPISURG.GPERSONA = DPERSONA.GPERSONA ";
				stSqlU = stSqlU + "LEFT JOIN UCAUINGR ON UEPISURG.GCAUSING = UCAUINGR.GCAUSING ";
				stSqlU = stSqlU + "LEFT JOIN DMOTINGR ON UEPISURG.GMOTATEN = DMOTINGR.GMOTINGR WHERE GIDENPAC ='" + Identificador + "' AND faltaurg IS  NULL and UEPISURG.GSERVICI in (select dperserv.gservici from dperserv inner join susuario on dperserv.gpersona = susuario.gpersona where gusuario = '" + stUsuario + "') ";

				stSqlH = "SELECT 'H',ganoadme,gnumadme,fllegada,dnomserv,dnompers,dap1pers,dap2pers, ";
				stSqlH = stSqlH + "CASE WHEN GDIAGINI IS NULL THEN ODIAGINI ELSE (SELECT DNOMBDIA FROM DDIAGNOS WHERE DDIAGNOS.GCODIDIA=GDIAGINI) END,null,DMOTINGR,GPLANTOR,GHABITOR,GCAMAORI FROM AEPISADM ";
				stSqlH = stSqlH + "INNER JOIN DSERVICI ON AEPISADM.GSERULTI = DSERVICI.GSERVICI ";
				stSqlH = stSqlH + "INNER JOIN DPERSONA ON AEPISADM.GPERULTI = DPERSONA.GPERSONA ";
				stSqlH = stSqlH + "INNER JOIN DMOTINGR ON AEPISADM.GMOTINGR = DMOTINGR.GMOTINGR ";
				stSqlH = stSqlH + "INNER JOIN AMOVIMIE ON AEPISADM.GANOADME = AMOVIMIE.GANOREGI AND AEPISADM.GNUMADME = AMOVIMIE.GNUMREGI AND 'H'= AMOVIMIE.ITIPOSER AND AMOVIMIE.FFINMOVI IS NULL ";
				stSqlH = stSqlH + "WHERE GIDENPAC ='" + Identificador + "' AND faltplan IS NULL and AEPISADM.GSERULTI in (select dperserv.gservici from dperserv inner join susuario on dperserv.gpersona = susuario.gpersona where gusuario = '" + stUsuario + "') ";
				stSqlN = " Union all ";
				stSqlO = " ORDER BY 4 DESC";
				if (((((int) ChbC.CheckState) & ((int) ChbQ.CheckState) & ((int) ChbH.CheckState) & ((int) ChbU.CheckState)) | ((ChbC.CheckState == CheckState.Unchecked && ChbQ.CheckState == CheckState.Unchecked && ChbH.CheckState == CheckState.Unchecked && ChbU.CheckState == CheckState.Unchecked) ? -1 : 0)) != 0)
				{
					stcomando = stSqlC + stSqlN + stSqlQ + stSqlN + stSqlU + stSqlN + stSqlH + stSqlO;
				}
				else
				{
					stcomando = "";
					if (ChbC.CheckState != CheckState.Unchecked)
					{
						stcomando = stSqlC + stSqlN;
					}
					if (ChbQ.CheckState != CheckState.Unchecked)
					{
						stcomando = stcomando + stSqlQ + stSqlN;
					}
					if (ChbH.CheckState != CheckState.Unchecked)
					{
						stcomando = stcomando + stSqlH + stSqlN;
					}
					if (ChbU.CheckState != CheckState.Unchecked)
					{
						stcomando = stcomando + stSqlU + stSqlN;
					}
					stcomando = stcomando.Substring(0, Math.Min(stcomando.Length - 10, stcomando.Length));
					stcomando = stcomando + stSqlO;
				}

				SqlDataAdapter tempAdapter = new SqlDataAdapter(stcomando, BasSelEpisodio.rcConexion);
				RrSQL = new DataSet();
				tempAdapter.Fill(RrSQL);
				SprEpisodios.MaxRows = 0;
				foreach (DataRow iteration_row in RrSQL.Tables[0].Rows)
				{
					SprEpisodios.MaxRows++;
					SprEpisodios.Row = SprEpisodios.MaxRows;
					SprEpisodios.Col = COL_AREASERV;
					SprEpisodios.Text = (Convert.ToString(iteration_row[0]) + "").Trim();
					SprEpisodios.Col = COL_FECHAEPI;
					SprEpisodios.Text = (Convert.ToString(iteration_row[3]) + "").Trim();
					SprEpisodios.Col = COL_DESCSERV;
					SprEpisodios.Text = (Convert.ToString(iteration_row[4]) + "").Trim();
					SprEpisodios.Col = COL_DESCMEDI;
					SprEpisodios.Text = (Convert.ToString(iteration_row[6]) + "").Trim() + " " + (Convert.ToString(iteration_row[7]) + "").Trim() + "," + (Convert.ToString(iteration_row[5]) + "").Trim();
					SprEpisodios.Col = COL_DIAGNOST;
					SprEpisodios.Text = (Convert.ToString(iteration_row[8]) + "").Trim();

					SprEpisodios.Col = COL_MOTATEND;
					SprEpisodios.Text = (Convert.ToString(iteration_row[9]) + "").Trim();
					SprEpisodios.Col = COL_CAMAPACI;
					SprEpisodios.Text = StringsHelper.Format((Convert.ToString(iteration_row[11]) + "").Trim(), "00") + StringsHelper.Format((Convert.ToString(iteration_row[12]) + "").Trim(), "00") + StringsHelper.Format((Convert.ToString(iteration_row[13]) + "").Trim(), "00");
					SprEpisodios.Col = COL_GANOREGI;
					SprEpisodios.Text = (Convert.ToString(iteration_row[1]) + "").Trim();
					SprEpisodios.Col = COL_GNUMREGI;
					SprEpisodios.Text = (Convert.ToString(iteration_row[2]) + "").Trim();
				}
				RrSQL.Close();

				if (SprEpisodios.MaxRows != 0)
				{
                    SprEpisodios.Row = 1;
					CargarDatos(1);
					SprEpisodios.Action = 0;
				}
			}
			catch
			{
				// Call ErrorSelect
			}
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			if (SprEpisodios.Row < 1)
			{
				stAñoEpisodio = "0";
				stNumeroEpisodio = "0";
				stFEpisodio = "";
				stTipoEpisodio = "";
			}
			//debo tener seleccionado un registro para devolver la dll
			BasSelEpisodio.Llamador.RecogerEpiPetiPrueba(stAñoEpisodio, stNumeroEpisodio, stFEpisodio, stTipoEpisodio, stCamaEpisodio);
			this.Close();

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			BasSelEpisodio.Llamador.RecogerEpiPetiPrueba("0", "0", "", "", "");
			this.Close();
		}

		private void ChbC_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			CargarSprEpisodios(BasSelEpisodio.gstIdenPaciente, BasSelEpisodio.gUsuario);
		}

		private void ChbH_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			CargarSprEpisodios(BasSelEpisodio.gstIdenPaciente, BasSelEpisodio.gUsuario);
		}

		private void ChbQ_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			CargarSprEpisodios(BasSelEpisodio.gstIdenPaciente, BasSelEpisodio.gUsuario);
		}

		private void ChbU_CheckStateChanged(Object eventSender, EventArgs eventArgs)
		{
			CargarSprEpisodios(BasSelEpisodio.gstIdenPaciente, BasSelEpisodio.gUsuario);
		}

		private void MDO221F1_Load(Object eventSender, EventArgs eventArgs)
		{

			try
			{
                CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);

				this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);
				this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
                
				SprEpisodios.MaxRows = 0;
				SprEpisodios.Col = COL_GNUMREGI;
				SprEpisodios.SetColHidden(SprEpisodios.Col, true);
				SprEpisodios.Col = COL_GANOREGI;
				SprEpisodios.SetColHidden(SprEpisodios.Col, true);

				CargarSprEpisodios(BasSelEpisodio.gstIdenPaciente, BasSelEpisodio.gUsuario);
				BuscarDatosPaciente(BasSelEpisodio.gstIdenPaciente);
			}
			catch(Exception ex)
			{
				Serrores.AnalizaError("Seleccion Episodio", Path.GetDirectoryName(Application.ExecutablePath), BasSelEpisodio.gUsuario, "SeleccionEpisodio:Form_load", ex);
			}
		}

        private void SprEpisodios_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
        {
            if (eventArgs != null)
            {
                SprEpisodios.Row = eventArgs.RowIndex + 1;
            }
            else
            {
                SprEpisodios.Row = SprEpisodios.ActiveRowIndex;
            }
            //cuando selecciono un episodio			
			if (SprEpisodios.Row == 0)
			{
				return;
			}
			else
			{
				CargarDatos(SprEpisodios.Row);
			}
		}


		private void SprEpisodios_KeyUp(Object eventSender, KeyEventArgs eventArgs)
		{
			if (SprEpisodios.MaxRows == 0)
			{
				return;
			}
			SprEpisodios_CellClick(SprEpisodios, null);
		}

		private void SprEpisodios_MouseUp(Object eventSender, MouseEventArgs eventArgs)
		{
            SprEpisodios.Row = SprEpisodios.ActiveRowIndex;
        }

		private void CargarDatos(int Fila)
		{
			SprEpisodios.Row = Fila;

			SprEpisodios.Col = COL_FECHAEPI;
			stFEpisodio = SprEpisodios.Text.Trim();

			SprEpisodios.Col = COL_AREASERV;
			stTipoEpisodio = SprEpisodios.Text.Trim();

			SprEpisodios.Col = COL_GANOREGI;
			stAñoEpisodio = SprEpisodios.Text.Trim();

			SprEpisodios.Col = COL_GNUMREGI;
			stNumeroEpisodio = SprEpisodios.Text.Trim();

			SprEpisodios.Col = COL_CAMAPACI;
			stCamaEpisodio = SprEpisodios.Text.Trim();

		}
		private void BuscarDatosPaciente(string Paciente)
		{
			string stCadena = String.Empty;

			string sql = "Select DPACIENT.dnombpac,DPACIENT.dape1pac,DPACIENT.dape2pac,HDOSSIER.GHISTORIA ";
			sql = sql + " from DPACIENT LEFT JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac ";
			sql = sql + "where DPACIENT.GIDENPAC = '" + Paciente + "'";

			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BasSelEpisodio.rcConexion);
			DataSet RrSQL = new DataSet();
			tempAdapter.Fill(RrSQL);
			if (RrSQL.Tables[0].Rows.Count != 0)
			{
				LB_Hist.Text = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["ghistoria"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["ghistoria"]).Trim();
				stCadena = (Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dnombpac"])) ? "" : Convert.ToString(RrSQL.Tables[0].Rows[0]["dnombpac"]).Trim();
				stCadena = stCadena + ((Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dape1pac"])) ? "" : " " + Convert.ToString(RrSQL.Tables[0].Rows[0]["dape1pac"]).Trim());
				stCadena = stCadena + ((Convert.IsDBNull(RrSQL.Tables[0].Rows[0]["dape2pac"])) ? "" : " " + Convert.ToString(RrSQL.Tables[0].Rows[0]["dape2pac"]).Trim());
				lb_paciente.Text = stCadena;
			}
			else
			{
				LB_Hist.Text = "";
				lb_paciente.Text = "";
			}
			RrSQL.Close();

		}
		private void MDO221F1_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
        }
	}
}