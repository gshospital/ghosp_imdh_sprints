using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	partial class MDO221F1
	{

		#region "Upgrade Support "
		private static MDO221F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static MDO221F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new MDO221F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "SprEpisodios", "ChbH", "ChbQ", "ChbC", "ChbU", "FrmArea", "LB_Hist", "Label3", "lb_paciente", "Label1", "Frame1", "cbAceptar", "cbCancelar", "SprEpisodios_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread SprEpisodios;
		public Telerik.WinControls.UI.RadCheckBox ChbH;
		public Telerik.WinControls.UI.RadCheckBox ChbQ;
		public Telerik.WinControls.UI.RadCheckBox ChbC;
		public Telerik.WinControls.UI.RadCheckBox ChbU;
		public Telerik.WinControls.UI.RadGroupBox FrmArea;
		public Telerik.WinControls.UI.RadTextBox LB_Hist;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadTextBox lb_paciente;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDO221F1));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.SprEpisodios = new UpgradeHelpers.Spread.FpSpread();
			this.FrmArea = new Telerik.WinControls.UI.RadGroupBox();
			this.ChbH = new Telerik.WinControls.UI.RadCheckBox();
			this.ChbQ = new Telerik.WinControls.UI.RadCheckBox();
			this.ChbC = new Telerik.WinControls.UI.RadCheckBox();
			this.ChbU = new Telerik.WinControls.UI.RadCheckBox();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.LB_Hist = new Telerik.WinControls.UI.RadTextBox();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.lb_paciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.FrmArea.SuspendLayout();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
            // 
            // SprEpisodios
            // 
            gridViewTextBoxColumn1.HeaderText = "�rea";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 80;

            gridViewTextBoxColumn2.HeaderText = "Fecha Atenci�n";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn1.Width = 80;

            gridViewTextBoxColumn3.HeaderText = "Servicio";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 80;

            gridViewTextBoxColumn4.HeaderText = "Responsable";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 100;

            gridViewTextBoxColumn5.HeaderText = "Diagn�stico";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 100;

            gridViewTextBoxColumn6.HeaderText = "Actuaci�n";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 80;

            gridViewTextBoxColumn7.HeaderText = "Motivo Atenci�n";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.WrapText = true;
            gridViewTextBoxColumn7.Width = 70;

            gridViewTextBoxColumn8.HeaderText = "Cama";
            gridViewTextBoxColumn8.WrapText = true;
            gridViewTextBoxColumn8.Name = "column8";
            gridViewTextBoxColumn8.Width = 60;

            gridViewTextBoxColumn9.HeaderText = "I";
            gridViewTextBoxColumn9.Name = "column9";
            
            gridViewTextBoxColumn10.HeaderText = "J";
            gridViewTextBoxColumn10.Name = "Num Reg";
            
            this.SprEpisodios.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[]
            {   gridViewTextBoxColumn1,
                gridViewTextBoxColumn2,
                gridViewTextBoxColumn3,
                gridViewTextBoxColumn4,
                gridViewTextBoxColumn5,
                gridViewTextBoxColumn6,
                gridViewTextBoxColumn7,
                gridViewTextBoxColumn8,
                gridViewTextBoxColumn9,
                gridViewTextBoxColumn10
            });
            this.SprEpisodios.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;

            this.SprEpisodios.MasterTemplate.EnableSorting = false;
            this.SprEpisodios.MasterTemplate.ReadOnly = true;
            this.SprEpisodios.Location = new System.Drawing.Point(3, 51);
			this.SprEpisodios.Name = "SprEpisodios";
			this.SprEpisodios.Size = new System.Drawing.Size(690, 288);
			this.SprEpisodios.TabIndex = 0;
            this.SprEpisodios.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.SprEpisodios_CellClick);
            this.SprEpisodios.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SprEpisodios_KeyUp);
			this.SprEpisodios.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SprEpisodios_MouseUp);
			// 
			// FrmArea
			// 
			this.FrmArea.Controls.Add(this.ChbH);
			this.FrmArea.Controls.Add(this.ChbQ);
			this.FrmArea.Controls.Add(this.ChbC);
			this.FrmArea.Controls.Add(this.ChbU);
			this.FrmArea.Enabled = true;
			this.FrmArea.Location = new System.Drawing.Point(509, 1);
			this.FrmArea.Name = "FrmArea";
			this.FrmArea.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.FrmArea.Size = new System.Drawing.Size(184, 47);
			this.FrmArea.TabIndex = 8;
			this.FrmArea.Text = "Filtrar por �rea";
			this.FrmArea.Visible = true;
			// 
			// ChbH
			// 
			this.ChbH.CausesValidation = true;
			this.ChbH.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
			this.ChbH.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.ChbH.Cursor = System.Windows.Forms.Cursors.Default;
			this.ChbH.Enabled = true;
			this.ChbH.Location = new System.Drawing.Point(6, 18);
			this.ChbH.Name = "ChbH";
			this.ChbH.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ChbH.Size = new System.Drawing.Size(31, 13);
			this.ChbH.TabIndex = 12;
			this.ChbH.TabStop = true;
			this.ChbH.Text = "H";
			this.ChbH.Visible = true;
			this.ChbH.CheckStateChanged += new System.EventHandler(this.ChbH_CheckStateChanged);
			// 
			// ChbQ
			// 
			this.ChbQ.CausesValidation = true;
			this.ChbQ.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
			this.ChbQ.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.ChbQ.Cursor = System.Windows.Forms.Cursors.Default;
			this.ChbQ.Enabled = true;
			this.ChbQ.Location = new System.Drawing.Point(141, 18);
			this.ChbQ.Name = "ChbQ";
			this.ChbQ.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ChbQ.Size = new System.Drawing.Size(31, 13);
			this.ChbQ.TabIndex = 11;
			this.ChbQ.TabStop = true;
			this.ChbQ.Text = "Q";
			this.ChbQ.Visible = true;
			this.ChbQ.CheckStateChanged += new System.EventHandler(this.ChbQ_CheckStateChanged);
			// 
			// ChbC
			// 
			this.ChbC.CausesValidation = true;
			this.ChbC.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
			this.ChbC.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.ChbC.Cursor = System.Windows.Forms.Cursors.Default;
			this.ChbC.Enabled = true;
			this.ChbC.Location = new System.Drawing.Point(96, 18);
			this.ChbC.Name = "ChbC";
			this.ChbC.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ChbC.Size = new System.Drawing.Size(31, 13);
			this.ChbC.TabIndex = 10;
			this.ChbC.TabStop = true;
			this.ChbC.Text = "C";
			this.ChbC.Visible = true;
			this.ChbC.CheckStateChanged += new System.EventHandler(this.ChbC_CheckStateChanged);
			// 
			// ChbU
			// 
			this.ChbU.CausesValidation = true;
			this.ChbU.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
			this.ChbU.CheckState = System.Windows.Forms.CheckState.Unchecked;
			this.ChbU.Cursor = System.Windows.Forms.Cursors.Default;
			this.ChbU.Enabled = true;
			this.ChbU.Location = new System.Drawing.Point(51, 18);
			this.ChbU.Name = "ChbU";
			this.ChbU.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ChbU.Size = new System.Drawing.Size(31, 13);
			this.ChbU.TabIndex = 9;
			this.ChbU.TabStop = true;
			this.ChbU.Text = "U";
			this.ChbU.Visible = true;
			this.ChbU.CheckStateChanged += new System.EventHandler(this.ChbU_CheckStateChanged);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.LB_Hist);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.lb_paciente);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(3, 1);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(503, 47);
			this.Frame1.TabIndex = 3;
			this.Frame1.Text = "Datos del paciente";
			this.Frame1.Visible = true;
            // 
            // LB_Hist
            // 
            this.LB_Hist.Enabled = false;
			this.LB_Hist.Cursor = System.Windows.Forms.Cursors.Default;
			this.LB_Hist.Location = new System.Drawing.Point(405, 17);
			this.LB_Hist.Name = "LB_Hist";
			this.LB_Hist.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LB_Hist.Size = new System.Drawing.Size(88, 19);
			this.LB_Hist.TabIndex = 7;
			// 
			// Label3
			// 
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.Location = new System.Drawing.Point(341, 19);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(61, 16);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "Hist. cl�nica:";
            // 
            // lb_paciente
            // 
            this.lb_paciente.Enabled = false;
			this.lb_paciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lb_paciente.Location = new System.Drawing.Point(52, 17);
			this.lb_paciente.Name = "lb_paciente";
			this.lb_paciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lb_paciente.Size = new System.Drawing.Size(282, 19);
			this.lb_paciente.TabIndex = 5;
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(6, 19);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(52, 17);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "Paciente:";
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Location = new System.Drawing.Point(529, 342);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 30);
			this.cbAceptar.TabIndex = 1;
			this.cbAceptar.Text = "Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(613, 342);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 30);
			this.cbCancelar.TabIndex = 2;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// MDO221F1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(695, 374);
			this.Controls.Add(this.SprEpisodios);
			this.Controls.Add(this.FrmArea);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCancelar);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = (System.Drawing.Icon) resources.GetObject("MDO221F1.Icon");
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MDO221F1";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Selecci�n de episodios - MDO221F1";
			this.Closed += new System.EventHandler(this.MDO221F1_Closed);
			this.Load += new System.EventHandler(this.MDO221F1_Load);
			this.FrmArea.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}