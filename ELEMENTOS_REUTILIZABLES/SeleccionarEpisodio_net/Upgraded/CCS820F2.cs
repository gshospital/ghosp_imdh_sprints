using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace SelEpisodio
{
	public partial class CCS820F2
		: Telerik.WinControls.UI.RadForm
    {

		public CCS820F2()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		private void CCS820F2_Activated(System.Object eventSender, System.EventArgs eventArgs)
		{
			if (UpgradeHelpers.Gui.ActivateHelper.myActiveForm != eventSender)
			{
				UpgradeHelpers.Gui.ActivateHelper.myActiveForm = (System.Windows.Forms.Form) eventSender;
			}
		}
		int iFila = 0;
		string stsql = String.Empty;
        private bool _isMouseLeftDown = false;

        private void CCS820F2_Load(Object eventSender, EventArgs eventArgs)
		{

			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);


            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);


            Application.DoEvents();
			sprListaEspera.Row = -1;

			proConfiguraSpreadListaEspera();

		}

		public void CargarDatos()
		{
			//con el identificador que nos llega buscamos los datos DEL PACIENTE
			//para cargar los datos en pantalla
			Datos_Paciente();
			Cargar_LE();

			if (sprListaEspera.MaxRows == 0)
			{
				cbCerrar_Click(cbCerrar, new EventArgs());
			}
			else
			{
				sprListaEspera.Row = 1;
				CCS820F2.DefInstance.ShowDialog();
			}

		}


		private void Datos_Paciente()
		{

			stsql = "SELECT gserarch " + 
			        "FROM hserarch " + 
			        "WHERE icentral = 'S'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsCentral = new DataSet();
			tempAdapter.Fill(rsCentral);


			//*********************** O.Frias (SQL-2005) **********************

			stsql = "SELECT  ISNULL(DPACIENT.dnombpac, '') AS dnombpac, ISNULL(DPACIENT.dape1pac, '') AS dape1pac, " + 
			        "ISNULL(DPACIENT.dape2pac, '') AS dape2pac, HDOSSIER.ghistoria " + 
			        "From DPACIENT " + 
			        "LEFT OUTER JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac AND " + 
			        "HDOSSIER.icontenido = 'H' AND HDOSSIER.gserpropiet = " + Convert.ToString(rsCentral.Tables[0].Rows[0]["gserarch"]) + " " + 
			        "WHERE (DPACIENT.gidenpac = '" + BasSelEpisodio.gstIdenPaciente + "')";


			//*********************** O.Frias (SQL-2005) ***********************



			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsPaciente = new DataSet();
			tempAdapter_2.Fill(rsPaciente);

			this.lbPaciente.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["dnombpac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape2pac"]).Trim();
			if (!Convert.IsDBNull(rsPaciente.Tables[0].Rows[0]["ghistoria"]))
			{
				lbHistoria.Text = Conversion.Str(rsPaciente.Tables[0].Rows[0]["ghistoria"]);
			}
			rsPaciente.Close();
			rsCentral.Close();

		}

		private void Cargar_LE()
		{

			sprListaEspera.MaxRows = 0;

			//*********************** O.Frias (SQL-2005) ***********************

			string stsql = "SELECT * From CLISESPE " + 
			               "INNER JOIN DPACIENT ON CLISESPE.gidenpac = DPACIENT.gidenpac " + 
			               "INNER JOIN DSERVICI ON CLISESPE.gservici = DSERVICI.gservici ";
			if (BasSelEpisodio.iotrprof == "T")
			{
				stsql = stsql + " AND dservici.ggrupser = '" + BasSelEpisodio.stAgrupacion + "' ";
			}

			stsql = stsql + "LEFT OUTER JOIN CTIPOTRA ON CLISESPE.gtipotra = CTIPOTRA.gtipotra " + 
			        "WHERE (CLISESPE.fregistro IS NULL) AND " + 
			        "(CLISESPE.gidenpac = '" + BasSelEpisodio.gstIdenPaciente + "' ) " + 
			        "ORDER BY FESPERA, DAPE1PAC, DAPE2PAC, DNOMBPAC ";


			//*********************** O.Frias (SQL-2005) ***********************


			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet RsConsultas = new DataSet();
			tempAdapter.Fill(RsConsultas);
			sprListaEspera.MaxRows = 0;

			if (RsConsultas.Tables[0].Rows.Count != 0)
			{
                foreach (DataRow iteration_row in RsConsultas.Tables[0].Rows)
				{

					sprListaEspera.MaxRows++;
					sprListaEspera.Row = sprListaEspera.MaxRows;

					sprListaEspera.Col = 1; //gidenpac(oculto)
					sprListaEspera.Text = Convert.ToString(iteration_row["gidenpac"]);
					sprListaEspera.SetColHidden(sprListaEspera.Col, true);

					sprListaEspera.Col = 2; //Paciente
					sprListaEspera.Text = Convert.ToString(iteration_row["dape1pac"]).Trim() + " " + Convert.ToString(iteration_row["dape2pac"]).Trim() + " " + Convert.ToString(iteration_row["dnombpac"]).Trim();

					sprListaEspera.Col = 3; //Origen del Paciente
					if (!Convert.IsDBNull(iteration_row["iorigpac"]))
					{
						switch(Convert.ToString(iteration_row["iorigpac"]))
						{
							case "H" : 
								sprListaEspera.Text = "Hospitalización"; 
								break;
							case "C" : 
								sprListaEspera.Text = "Consulta del Hospital"; 
								break;
							case "O" : 
								sprListaEspera.Text = "Consulta fuera del Hospital"; 
								break;
						}
					}

					sprListaEspera.Col = 4; //Fecha de Inclusion
					sprListaEspera.Text = Convert.ToDateTime(iteration_row["fespera"]).ToString("dd/MM/yyyy");

					sprListaEspera.Col = 5; //Fecha de Prescripcion
					sprListaEspera.Text = Convert.ToDateTime(iteration_row["fprescri"]).ToString("dd/MM/yyyy");

					sprListaEspera.Col = 6; //Dias de espera
					sprListaEspera.Text = ((int) DateAndTime.DateDiff("d", DateTime.Parse(Convert.ToDateTime(iteration_row["fprescri"]).ToString("dd/MM/yyyy")), DateTime.Today, Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)).ToString();

					sprListaEspera.Col = 7; //Tipo Paciente
					sprListaEspera.Text = (Convert.ToString(iteration_row["tipoesta"]) == "N") ? "NORMAL" : "PREFERENTE";

					sprListaEspera.Col = 8; //cod. tipo de tratamiento (oculto)
					sprListaEspera.SetColHidden(sprListaEspera.Col, true);
					sprListaEspera.Text = Convert.ToString(iteration_row["gtipotra"]) + "";

					sprListaEspera.Col = 9; //Tipo de tratamiento
					sprListaEspera.Text = (Convert.ToString(iteration_row["dtipotra"]) + "").ToUpper();

					sprListaEspera.Col = 10; //cod servicio realizador
					sprListaEspera.SetColHidden(sprListaEspera.Col, true);
					sprListaEspera.Text = Convert.ToString(iteration_row["gservici"]) + "";

					sprListaEspera.Col = 11; //servicio realizador
					sprListaEspera.Text = Convert.ToString(iteration_row["dnomserv"]) + "";

					sprListaEspera.Col = 12;
					sprListaEspera.Text = Convert.ToString(iteration_row["observaciones"]) + "";

					sprListaEspera.Col = 13;
					sprListaEspera.Text = Convert.ToString(iteration_row["ganoregi"]);

					sprListaEspera.Col = 14;
					sprListaEspera.Text = Convert.ToString(iteration_row["gnumregi"]);
				}
			}
			else
			{
				sprListaEspera.MaxRows = 0;
			}

			sprListaEspera.setSelModeIndex(0); //para que no haya ninguno seleccionado
		}

        private void sprListaEspera_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
		{
            if (_isMouseLeftDown)
            {
				int Col = eventArgs.ColumnIndex + 1;
				int Row = eventArgs.RowIndex + 1;
				if (Row > 0)
				{
					if (eventArgs.RowIndex != -1)
					{
						if (iFila == Row)
						{
                            sprListaEspera.CurrentRow = null;
                            cbAceptar.Enabled = false;
						}
						else
						{
							sprListaEspera.Row = Row;
							iFila = Row;
							cbAceptar.Enabled = true;
						}
					}
					else
					{
                        //INDRA JPROCHE TODO_X_4 03/05/2015
                        //sprListaEspera.ActiveSheet.OperationMode = (FarPoint.Win.Spread.OperationMode) (((int) FarPoint.Win.Spread.OperationMode.ReadOnly) + ((int) FarPoint.Win.Spread.OperationMode.RowMode));
						sprListaEspera.Row = Row;
						cbAceptar.Enabled = true;
						iFila = Row;
					}
				}
            }
		}

        private void sprListaEspera_CellDoubleClick(object eventSender, GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			if (Row > 0)
			{
				if (cbAceptar.Enabled)
				{
					cbAceptar_Click(cbAceptar, new EventArgs());
				}
			}
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//debo tener seleccionado un registro para devolver la dll

			sprListaEspera.Col = 13;
			int iAñoRegistro = Convert.ToInt32(Conversion.Val(sprListaEspera.Text));
			sprListaEspera.Col = 14;
			int iNumeroRegistro = Convert.ToInt32(Conversion.Val(sprListaEspera.Text));

			BasSelEpisodio.Llamador.RecogerEpisodio(iAñoRegistro, iNumeroRegistro);
			this.Close();
		}

		public void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			BasSelEpisodio.Llamador.RecogerEpisodio(0, 0);
			this.Close();
		}

		private void proConfiguraSpreadListaEspera()
		{
           sprListaEspera.MaxCols = 14;

			sprListaEspera.Col = 1; //gidenpac(oculto)
			sprListaEspera.SetColHidden(sprListaEspera.Col, true);

			sprListaEspera.SetColWidth(2, 30); //Paciente
			sprListaEspera.SetText(2, 0, "Nombre");

			sprListaEspera.SetColWidth(3, 20); //Origen de Paciente
			sprListaEspera.SetText(3, 0, "Origen");

			sprListaEspera.SetColWidth(4, 10); //Fecha de Inclusion
			sprListaEspera.SetText(4, 0, "F. Inclusion");

			sprListaEspera.SetColWidth(5, 10); //Fecha de Prescripcion
			sprListaEspera.SetText(5, 0, "F. Prescripción");

			sprListaEspera.SetColWidth(6, 10); //Dias de espera
			sprListaEspera.SetText(6, 0, "Dias de Espera");

			sprListaEspera.SetColWidth(7, 10); //Tipo Paciente
			sprListaEspera.SetText(7, 0, "Estado");

			sprListaEspera.Col = 8; //cod. tipo de tratamiento (oculto)
			sprListaEspera.SetColHidden(sprListaEspera.Col, true);

			sprListaEspera.SetColWidth(9, 30); //Tipo de tratamiento
			sprListaEspera.SetText(9, 0, "Tipo de Tratamiento");

			sprListaEspera.Col = 10; //cod servicio realizador
			sprListaEspera.SetColHidden(sprListaEspera.Col, true);

			sprListaEspera.SetColWidth(11, 30); //servicio realizador
			sprListaEspera.SetText(11, 0, "Servicio Realizador");

			sprListaEspera.SetColWidth(12, 50); //observaciones
			sprListaEspera.SetText(12, 0, "Observaciones");

			sprListaEspera.Col = 13; //Id Lista/Espera
			sprListaEspera.SetColHidden(sprListaEspera.Col, true);
			sprListaEspera.Col = 14;
			sprListaEspera.SetColHidden(sprListaEspera.Col, true);
		}
		private void CCS820F2_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
        }
        private void sprListaEspera_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _isMouseLeftDown = true;
            }
            else
            {
                _isMouseLeftDown = false;
            }
        }
    }
}