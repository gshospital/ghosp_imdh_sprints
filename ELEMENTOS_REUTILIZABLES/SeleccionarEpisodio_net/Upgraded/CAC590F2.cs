using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using Telerik.WinControls.UI;
using ElementosCompartidos;

namespace SelEpisodio
{
	public partial class CAC590F2
		: Telerik.WinControls.UI.RadForm
    {


		int iFila = 0;
		string stsql = String.Empty;
        private bool _isMouseLeftDown = false;


        public CAC590F2()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}

        private void CAC590F2_Load(Object eventSender, EventArgs eventArgs)
		{
            CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
            this.Cursor = Cursors.WaitCursor;

            Application.DoEvents();
			sprConsultas.Row = -1;

			for (int i = 10; i <= 14; i++)
			{
				sprConsultas.Col = i;
				sprConsultas.SetColHidden(sprConsultas.Col, true);
			}

			for (int i = 18; i <= 22; i++)
			{
				sprConsultas.Col = i;
				sprConsultas.SetColHidden(sprConsultas.Col, true);
			}

			//con el identificador que nos llega buscamos los datos DEL PACIENTE
			//para cargar los datos en pantalla
			Datos_Paciente();
			Cargar_Consultas();

			this.Cursor = Cursors.Default;

		}

		private void Datos_Paciente()
		{

			stsql = "SELECT gserarch " + 
			        "FROM hserarch " + 
			        "WHERE icentral = 'S'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsCentral = new DataSet();
			tempAdapter.Fill(rsCentral);
            
			stsql = "SELECT ISNULL(DPACIENT.dnombpac, '') AS dnombpac, ISNULL(DPACIENT.dape1pac, '') AS dape1pac, " + 
			        "ISNULL(DPACIENT.dape2pac, '') AS dape2pac, HDOSSIER.ghistoria " + 
			        "FROM DPACIENT " + 
			        "LEFT OUTER JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac AND " + 
			        "HDOSSIER.icontenido = 'H' AND " + 
			        "hdossier.gserpropiet = " + Convert.ToString(rsCentral.Tables[0].Rows[0]["gserarch"]) + " " + 
			        "WHERE (DPACIENT.gidenpac = '" + BasSelEpisodio.gstIdenPaciente + "' ) ";

			//****************** OFrias (SQL-2005) ******************

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsPaciente = new DataSet();
			tempAdapter_2.Fill(rsPaciente);

			this.lbPaciente.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["dnombpac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape2pac"]).Trim();
			if (!Convert.IsDBNull(rsPaciente.Tables[0].Rows[0]["ghistoria"]))
			{
				lbHistoria.Text = Conversion.Str(rsPaciente.Tables[0].Rows[0]["ghistoria"]);
			}
			rsPaciente.Close();
			rsCentral.Close();

		}

		private void Cargar_Consultas()
		{

			sprConsultas.MaxRows = 0;

			switch(BasSelEpisodio.iotrprof)
			{
				case "P" :  
					BasSelEpisodio.stAgrupacion = Serrores.ObternerValor_CTEGLOBAL(BasSelEpisodio.rcConexion, "GRUPPSIC", "VALFANU1"); 
					break;
				case "T" :  
					BasSelEpisodio.stAgrupacion = Serrores.ObternerValor_CTEGLOBAL(BasSelEpisodio.rcConexion, "GRUPTEOC", "VALFANU1"); 
					break;
			}

			string stsql = "SELECT nafiliac,dprestac,ffeccita,nordcita,ganoregi,gnumregi,cconsult.gservici," + 
			               "ganoprin,gnumprin,ndiasinc,nhorainc,ghospita,iTiposer," + 
			               "cconsult.gpersona, dsocieda, dprestac, dnomserv, dap1pers, dap2pers, dnompers, dsalacon, " + 
			               "dsalacon.gsalacon,dservici.gservici,ghistoria " + 
			               "From " + 
			               "cconsult inner join dsocieda on cconsult.gsocieda = dsocieda.gsocieda " + 
			               "inner join dcodpres on cconsult.gprestac = dcodpres.gprestac " + 
			               "inner join dservici on cconsult.gservici = dservici.gservici " + 
			               "inner join dpersona on cconsult.gpersona = dpersona.gpersona " + 
			               "left join dsalacon on cconsult.gsalacon = dsalacon.gsalacon " + 
			               "left join hdossier on cconsult.gidenpac = hdossier.gidenpac " + 
			               "WHERE cconsult.gidenpac = '" + BasSelEpisodio.gstIdenPaciente + "' AND cconsult.ipresent = 'S' ";

			//Jes�s 19/06/2006 Si viene de Otros Profesionales - Psicolog�a debe filtrar por las
			//Consultas de la agrupaci�n de psicolog�a
			if (BasSelEpisodio.iotrprof == "P")
			{
				stsql = stsql + " and dservici.ggrupser = '" + BasSelEpisodio.stAgrupacion + "' ";
			}
			//fin 19/06/2006

			string sql = "SELECT ISNULL(iperfqui,'G') iperfqui, gpersona FROM SUSUARIO WHERE GUSUARIO = '" + BasSelEpisodio.gUsuario + "'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(sql, BasSelEpisodio.rcConexion);
			DataSet rsAux = new DataSet();
			tempAdapter.Fill(rsAux);
			switch(Convert.ToString(rsAux.Tables[0].Rows[0]["iperfqui"]).Trim().ToUpper())
			{
				case "G" : 
					break;
				case "J" :  
					stsql = stsql + 
					        " AND CCONSULT.gservici IN (SELECT gservici FROM DPERSERV where gpersona = " + Convert.ToString(rsAux.Tables[0].Rows[0]["gpersona"]) + ") "; 
					break;
				case "C" :  
					stsql = stsql + 
					        " AND CCONSULT.gpersona = " + Convert.ToString(rsAux.Tables[0].Rows[0]["gpersona"]); 
					break;
			}
			rsAux.Close();

			stsql = stsql + " ORDER BY ffeccita DESC";

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet RsConsultas = new DataSet();
			tempAdapter_2.Fill(RsConsultas);
            foreach (DataRow iteration_row in RsConsultas.Tables[0].Rows)
			{

				sprConsultas.MaxRows++;
				sprConsultas.Row = sprConsultas.MaxRows;
				sprConsultas.Col = 1;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["dsocieda"])) ? "" : Convert.ToString(iteration_row["dsocieda"]).Trim();
				sprConsultas.Col = 2;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["nafiliac"])) ? "" : Convert.ToString(iteration_row["nafiliac"]).Trim();
				sprConsultas.Col = 3;
				sprConsultas.Text = Convert.ToString(iteration_row["dnomserv"]);
				sprConsultas.Col = 4;
				sprConsultas.Text = Convert.ToString(iteration_row["dap1pers"]).Trim() + " " + Convert.ToString(iteration_row["dap2pers"]).Trim() + ", " + Convert.ToString(iteration_row["dnompers"]).Trim();
				sprConsultas.Col = 5;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["dsalacon"])) ? "" : Convert.ToString(iteration_row["dsalacon"]).Trim();
				sprConsultas.Col = 6;
				sprConsultas.Text = Convert.ToString(iteration_row["dprestac"]);
				sprConsultas.Col = 7;
				sprConsultas.Text = Convert.ToDateTime(iteration_row["ffeccita"]).ToString("dd/MM/yyyy");
				if (Convert.ToDateTime(iteration_row["ffeccita"]).ToString("HH:mm") != "00:00")
				{
					sprConsultas.Col = 8;
					sprConsultas.Text = Convert.ToDateTime(iteration_row["ffeccita"]).ToString("HH:mm");
				}
				sprConsultas.Col = 9;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["nordcita"])) ? "" : Convert.ToString(iteration_row["nordcita"]).Trim();
				sprConsultas.Col = 10;
				sprConsultas.Text = Convert.ToString(iteration_row["ganoregi"]);
				sprConsultas.Col = 11;
				sprConsultas.Text = Convert.ToString(iteration_row["gnumregi"]);
				//a�adido para asisencia
				sprConsultas.Col = 12;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["gservici"])) ? "" : Convert.ToString(iteration_row["gservici"]).Trim();
				if (Convert.ToString(iteration_row["iTiposer"]) == "C")
				{
					sprConsultas.Col = 13;
					sprConsultas.Text = (Convert.IsDBNull(iteration_row["ganoprin"])) ? "" : Convert.ToString(iteration_row["ganoprin"]);
					sprConsultas.Col = 14;
					sprConsultas.Text = (Convert.IsDBNull(iteration_row["gnumprin"])) ? "" : Convert.ToString(iteration_row["gnumprin"]);
				}
				sprConsultas.Col = 15;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["ndiasinc"])) ? "" : Convert.ToString(iteration_row["ndiasinc"]);
				sprConsultas.Col = 16;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["nhorainc"])) ? "" : Convert.ToString(iteration_row["nhorainc"]);
				sprConsultas.Col = 17;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["ghospita"])) ? "" : Convert.ToString(iteration_row["ghospita"]);
				sprConsultas.Col = 18;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["iTiposer"])) ? "" : Convert.ToString(iteration_row["iTiposer"]);
				sprConsultas.Col = 19;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["gsalacon"])) ? "" : Convert.ToString(iteration_row["gsalacon"]);
				sprConsultas.Col = 20;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["gservici"])) ? "" : Convert.ToString(iteration_row["gservici"]);
				sprConsultas.Col = 21;
				sprConsultas.Text = (Convert.IsDBNull(iteration_row["gpersona"])) ? "" : Convert.ToString(iteration_row["gpersona"]);
				if (Convert.IsDBNull(iteration_row["ghistoria"]))
				{
					sprConsultas.Col = 22;
					sprConsultas.Text = "";
				}
				else
				{
					sprConsultas.Col = 22;
					sprConsultas.Text = Convert.ToString(iteration_row["ghistoria"]);
				}
			}
			sprConsultas.setSelModeIndex(0); //para que no haya ninguno seleccionado
		}


		private void sprConsultas_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
        {            
            if (_isMouseLeftDown)
            {
                int Col = eventArgs.ColumnIndex;
                int Row = eventArgs.RowIndex + 1;
				if (Row > 0)
				{
					if (eventArgs.RowIndex != -1)
					{
						if (iFila == Row)
						{
                            sprConsultas.CurrentRow = null;
                            cbAceptar.Enabled = false;
						}
						else
						{
							sprConsultas.Row = Row;
							iFila = Row;
							cbAceptar.Enabled = true;
						}
					}
					else
					{
                        sprConsultas.Row = Row;
						cbAceptar.Enabled = true;
						iFila = Row;
					}
				}
            }
		}

        private void sprConsultas_CellDoubleClick(object eventSender, GridViewCellEventArgs eventArgs)
        {
            int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;
			if (Row > 0)
			{
				if (cbAceptar.Enabled)
				{
					cbAceptar_Click(cbAceptar, new EventArgs());
				}
			}
		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			//debo tener seleccionado un registro para devolver la dll

			sprConsultas.Col = 10;
			int iA�oRegistro = Convert.ToInt32(Conversion.Val(sprConsultas.Text));
			sprConsultas.Col = 11;
			int iNumeroRegistro = Convert.ToInt32(Conversion.Val(sprConsultas.Text));

			BasSelEpisodio.Llamador.RecogerEpisodio(iA�oRegistro, iNumeroRegistro);
			this.Close();
		}

		private void cbCerrar_Click(Object eventSender, EventArgs eventArgs)
		{
			BasSelEpisodio.Llamador.RecogerEpisodio(0, 0);
			this.Close();
		}
		private void CAC590F2_Closed(Object eventSender, EventArgs eventArgs)
		{
            m_vb6FormDefInstance.Dispose();
        }
        private void sprConsultas_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _isMouseLeftDown = true;
            }
            else
            {
                _isMouseLeftDown = false;
            }
        }
    }
}