using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	partial class CCI341F1
	{

		#region "Upgrade Support "
		private static CCI341F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static CCI341F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new CCI341F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprCitasP", "Frame1", "cbAceptar", "cbCancelar", "lblCitasNoAutorizadas", "lbHistoria", "lbPaciente", "Label23", "Label1", "_Label2_0", "Line1", "Label2", "ShapeContainer1", "sprCitasP_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprCitasP;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadLabel lblCitasNoAutorizadas;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		public Telerik.WinControls.UI.RadTextBox lbPaciente;
		public Telerik.WinControls.UI.RadLabel Label23;
		public Telerik.WinControls.UI.RadLabel Label1;
		private Telerik.WinControls.UI.RadLabel _Label2_0;
		public Microsoft.VisualBasic.PowerPacks.LineShape Line1;
		public Telerik.WinControls.UI.RadLabel[] Label2 = new Telerik.WinControls.UI.RadLabel[1];
		public Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CCI341F1));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.ShapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.sprCitasP = new UpgradeHelpers.Spread.FpSpread();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.lblCitasNoAutorizadas = new Telerik.WinControls.UI.RadLabel();
			this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.lbPaciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label23 = new Telerik.WinControls.UI.RadLabel();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this._Label2_0 = new Telerik.WinControls.UI.RadLabel();
			this.Line1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.Frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// ShapeContainer1
			// 
			this.ShapeContainer1.Location = new System.Drawing.Point(0, 0);
			this.ShapeContainer1.Size = new System.Drawing.Size(594, 338);
			this.ShapeContainer1.Shapes.Add(Line1);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.sprCitasP);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(2, 38);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(589, 255);
			this.Frame1.TabIndex = 2;
			this.Frame1.Text = "Citas programadas";
			this.Frame1.Visible = true;
            // 
            // sprCitasP
            // 
            gridViewTextBoxColumn1.HeaderText = "Agenda";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 80;

            gridViewTextBoxColumn2.HeaderText = "Servicio";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn1.Width = 80;

            gridViewTextBoxColumn3.HeaderText = "Responsable";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 80;

            gridViewTextBoxColumn4.HeaderText = "Sala de Cosulta";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 100;

            gridViewTextBoxColumn5.HeaderText = "Fecha Cita";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.Width = 70;

            gridViewTextBoxColumn6.HeaderText = "Hora Cita";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 80;

            gridViewTextBoxColumn7.HeaderText = "N�m. Orden";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.Width = 70;

            gridViewTextBoxColumn8.HeaderText = "Prestaci�n";
            gridViewTextBoxColumn8.Name = "column8";
            gridViewTextBoxColumn8.Width = 60;

            gridViewTextBoxColumn9.HeaderText = "Numero";
            gridViewTextBoxColumn9.Name = "column9";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Width = 80;

            gridViewTextBoxColumn10.HeaderText = "A�o";
            gridViewTextBoxColumn10.Name = "Num Reg";
            gridViewTextBoxColumn10.Width = 80;

            gridViewTextBoxColumn11.HeaderText = "N� Autorizaci�n";
            gridViewTextBoxColumn11.Name = "column11";
            gridViewTextBoxColumn11.Width = 80;

            gridViewTextBoxColumn12.HeaderText = "N� DU";
            gridViewTextBoxColumn12.Name = "column12";
            gridViewTextBoxColumn12.Width = 80;

            gridViewTextBoxColumn13.HeaderText = "N� Oper. Electr�nica";
            gridViewTextBoxColumn13.Name = "column13";
            gridViewTextBoxColumn13.Width = 80;
            this.sprCitasP.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[]
          {   gridViewTextBoxColumn1,
                gridViewTextBoxColumn2,
                gridViewTextBoxColumn3,
                gridViewTextBoxColumn4,
                gridViewTextBoxColumn5,
                gridViewTextBoxColumn6,
                gridViewTextBoxColumn7,
                gridViewTextBoxColumn8,
                gridViewTextBoxColumn9,
                gridViewTextBoxColumn10,
                gridViewTextBoxColumn11,
                gridViewTextBoxColumn12,
                gridViewTextBoxColumn13
          });
            this.sprCitasP.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprCitasP.MasterTemplate.EnableSorting = false;
            this.sprCitasP.MasterTemplate.ReadOnly = true;
            this.sprCitasP.Location = new System.Drawing.Point(8, 16);
			this.sprCitasP.Name = "sprCitasP";
			this.sprCitasP.Size = new System.Drawing.Size(561, 229);
			this.sprCitasP.TabIndex = 3;
            this.sprCitasP.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprCitasP_CellClick);
            this.sprCitasP.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprCitasP_CellDoubleClick);
            this.sprCitasP.MouseDown += new System.Windows.Forms.MouseEventHandler(sprCitasP_MouseDown);
            //this.sprCitasP.LeaveRow += new UpgradeHelpers.Spread.FpSpread.LeaveRowEventHandler(this.sprCitasP_LeaveRow);
            // 
            // cbAceptar
            // 
            this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Enabled = false;
			this.cbAceptar.Location = new System.Drawing.Point(414, 300);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 32);
			this.cbAceptar.TabIndex = 1;
			this.cbAceptar.Text = "&Aceptar";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(505, 300);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 32);
			this.cbCancelar.TabIndex = 0;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
            // 
            // lblCitasNoAutorizadas
            // 
            this.lblCitasNoAutorizadas.BackColor = System.Drawing.Color.FromArgb(255, 128, 128);
            this.lblCitasNoAutorizadas.Cursor = System.Windows.Forms.Cursors.Default;
			this.lblCitasNoAutorizadas.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblCitasNoAutorizadas.Location = new System.Drawing.Point(98, 318);
			this.lblCitasNoAutorizadas.Name = "lblCitasNoAutorizadas";
			this.lblCitasNoAutorizadas.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblCitasNoAutorizadas.Size = new System.Drawing.Size(171, 19);
			this.lblCitasNoAutorizadas.TabIndex = 9;
			this.lblCitasNoAutorizadas.Text = "Citas con autorizaci�n no aprobada";
            // 
            // lbHistoria
            // 
            this.lbHistoria.Enabled = false;
			this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbHistoria.Location = new System.Drawing.Point(516, 10);
			this.lbHistoria.Name = "lbHistoria";
			this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbHistoria.Size = new System.Drawing.Size(73, 19);
			this.lbHistoria.TabIndex = 8;
            // 
            // lbPaciente
            // 
            this.lbPaciente.Enabled = false;
			this.lbPaciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbPaciente.Location = new System.Drawing.Point(82, 10);
			this.lbPaciente.Name = "lbPaciente";
			this.lbPaciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbPaciente.Size = new System.Drawing.Size(337, 19);
			this.lbPaciente.TabIndex = 7;
			// 
			// Label23
			// 
			this.Label23.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label23.Location = new System.Drawing.Point(452, 10);
			this.Label23.Name = "Label23";
			this.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label23.Size = new System.Drawing.Size(65, 17);
			this.Label23.TabIndex = 6;
			this.Label23.Text = "Hist. cl�nica:";
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(8, 10);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(49, 17);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "Paciente:";
			// 
			// _Label2_0
			// 
			this._Label2_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label2_0.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label2_0.Location = new System.Drawing.Point(97, 300);
			this._Label2_0.Name = "_Label2_0";
			this._Label2_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this._Label2_0.Size = new System.Drawing.Size(173, 14);
			this._Label2_0.TabIndex = 4;
			this._Label2_0.Text = "Citas para el d�a de hoy";
			// 
			// Line1
			// 
			this.Line1.BorderColor = System.Drawing.Color.Red;
			this.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			this.Line1.BorderWidth = 4;
			this.Line1.Enabled = false;
			this.Line1.Name = "Line1";
			this.Line1.Visible = true;
			this.Line1.X1 = (int) 10;
			this.Line1.X2 = (int) 76;
			this.Line1.Y1 = (int) 306;
			this.Line1.Y2 = (int) 306;
			// 
			// CCI341F1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(594, 338);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.lblCitasNoAutorizadas);
			this.Controls.Add(this.lbHistoria);
			this.Controls.Add(this.lbPaciente);
			this.Controls.Add(this.Label23);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this._Label2_0);
			this.Controls.Add(ShapeContainer1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(18, 60);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CCI341F1";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Selecci�n de Citas - CCI341F1";
			this.Activated += new System.EventHandler(this.CCI341F1_Activated);
			this.Closed += new System.EventHandler(this.CCI341F1_Closed);
			this.Frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		void ReLoadForm(bool addEvents)
		{
			InitializeLabel2();
		}
		void InitializeLabel2()
		{
			this.Label2 = new Telerik.WinControls.UI.RadLabel[1];
			this.Label2[0] = _Label2_0;
		}
		#endregion
	}
}