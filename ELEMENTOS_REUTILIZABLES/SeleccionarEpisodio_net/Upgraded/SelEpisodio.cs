using System;
using System.Data;
using System.Data.SqlClient;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	internal static class BasSelEpisodio
	{


		public static SqlConnection rcConexion = null;

		public static string gstIdenPaciente = String.Empty;

		public static dynamic Llamador = null;
		public static string gUsuario = String.Empty;
		public static string stTipoLlamada = String.Empty;

		public static Mensajes.ClassMensajes claseMensaje = null; //dll de mensajes

		public static string iotrprof = String.Empty;
		public static string stEpiAlta = String.Empty;
		public static string stAgrupacion = String.Empty;


		public struct CabCrystal
		{
			public string VstGrupoHo;
			public string VstNombreHo;
			public string VstDireccHo;
			public bool VfCabecera;
			public string VstTelefoHo;
			public static CabCrystal CreateInstance()
			{
					CabCrystal result = new CabCrystal();
					result.VstGrupoHo = String.Empty;
					result.VstNombreHo = String.Empty;
					result.VstDireccHo = String.Empty;
					result.VstTelefoHo = String.Empty;
					return result;
			}
		}

		public static BasSelEpisodio.CabCrystal VstrCrystal = BasSelEpisodio.CabCrystal.CreateInstance();

		public static bool vblnInformeUrgencia = false;

		internal static void proCabCrystal()
		{
			//**************************************************************************
			//Busqueda de la cabecera de la hoja
			//************************************************************************

			string tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'GRUPOHO' ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(tstCab, rcConexion);
			DataSet tRrCrystal = new DataSet();
			tempAdapter.Fill(tRrCrystal);
			VstrCrystal.VstGrupoHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tstCab = "SELECT valfanu1, valfanu2 FROM SCONSGLO WHERE gconsglo = 'NOMBREHO' ";
			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(tstCab, rcConexion);
			tRrCrystal = new DataSet();
			tempAdapter_2.Fill(tRrCrystal);
			VstrCrystal.VstNombreHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			VstrCrystal.VstTelefoHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU2"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU2"]).Trim();
			tstCab = "SELECT valfanu1 FROM SCONSGLO WHERE gconsglo = 'DIRECCHO' ";
			SqlDataAdapter tempAdapter_3 = new SqlDataAdapter(tstCab, rcConexion);
			tRrCrystal = new DataSet();
			tempAdapter_3.Fill(tRrCrystal);
			VstrCrystal.VstDireccHo = (Convert.IsDBNull(tRrCrystal.Tables[0].Rows[0]["VALFANU1"])) ? "" : Convert.ToString(tRrCrystal.Tables[0].Rows[0]["VALFANU1"]).Trim();
			tRrCrystal.Close();
			VstrCrystal.VfCabecera = true;
		}

         /*INDRA JPROCHE TODO_X_4 CONTIENE CODIGO CRYSTAL
		internal static void vacia_formulas(Crystal.CrystalReport listado, int Numero)
		{
			for (int tiForm = 0; tiForm <= Numero; tiForm++)
			{
				listado.set_Formulas((short) tiForm, "");
			}
			for (int tiForm = 0; tiForm <= 10; tiForm++)
			{
				listado.set_SortFields((short) tiForm, "");
			}
			listado.Reset();
		}
        */
	}
}