using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	partial class CCS820F1
	{

		#region "Upgrade Support "
		private static CCS820F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static CCS820F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new CCS820F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "sprSolicitudes", "cbCancelar", "cbAceptar", "CbImpimir", "Label1", "Label23", "lbNombre", "lbHistoria", "Label2", "lbTelefono1", "lbExtension1", "Label3", "lbTelefono2", "lbExtension2", "sprSolicitudes_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public UpgradeHelpers.Spread.FpSpread sprSolicitudes;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadButton CbImpimir;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadLabel Label23;
		public Telerik.WinControls.UI.RadTextBox lbNombre;
		public Telerik.WinControls.UI.RadTextBox lbHistoria;
		public Telerik.WinControls.UI.RadLabel Label2;
		public Telerik.WinControls.UI.RadTextBox lbTelefono1;
		public Telerik.WinControls.UI.RadTextBox lbExtension1;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadTextBox lbTelefono2;
		public Telerik.WinControls.UI.RadTextBox lbExtension2;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();

            this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CCS820F1));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.sprSolicitudes = new UpgradeHelpers.Spread.FpSpread();
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.CbImpimir = new Telerik.WinControls.UI.RadButton();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.Label23 = new Telerik.WinControls.UI.RadLabel();
			this.lbNombre = new Telerik.WinControls.UI.RadTextBox();
			this.lbHistoria = new Telerik.WinControls.UI.RadTextBox();
			this.Label2 = new Telerik.WinControls.UI.RadLabel();
			this.lbTelefono1 = new Telerik.WinControls.UI.RadTextBox();
			this.lbExtension1 = new Telerik.WinControls.UI.RadTextBox();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.lbTelefono2 = new Telerik.WinControls.UI.RadTextBox();
			this.lbExtension2 = new Telerik.WinControls.UI.RadTextBox();
			this.SuspendLayout();
            // 
            // sprSolicitudes
            // 
            gridViewTextBoxColumn1.HeaderText = "Entidad financiadora";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.WrapText = true;
            gridViewTextBoxColumn1.Width = 80;

            gridViewTextBoxColumn2.HeaderText = "N� asegurado";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.WrapText = true;
            gridViewTextBoxColumn2.Width = 60;

            gridViewTextBoxColumn3.HeaderText = "Origen del Paciente";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.WrapText = true;
            gridViewTextBoxColumn3.Width = 80;

            gridViewTextBoxColumn4.HeaderText = "Turno";
            gridViewTextBoxColumn4.Name = "column4";
            gridViewTextBoxColumn4.Width = 60;

            gridViewTextBoxColumn5.HeaderText = "N� Sesiones";
            gridViewTextBoxColumn5.Name = "column5";
            gridViewTextBoxColumn5.WrapText = true;
            gridViewTextBoxColumn5.Width = 60;

            gridViewTextBoxColumn6.HeaderText = "Fecha Solicitada";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.WrapText = true;
            gridViewTextBoxColumn6.Width = 80;

            gridViewTextBoxColumn7.HeaderText = "Inicio Tratamiento";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.WrapText = true;
            gridViewTextBoxColumn7.Width = 70;

            gridViewTextBoxColumn8.HeaderText = "Pr�xima Sesi�n";
            gridViewTextBoxColumn8.Name = "column8";
            gridViewTextBoxColumn8.WrapText = true;
            gridViewTextBoxColumn8.Width = 60;

            gridViewTextBoxColumn9.HeaderText = "Final tratamiento";
            gridViewTextBoxColumn9.Name = "column9";
            gridViewTextBoxColumn9.WrapText = true;
            gridViewTextBoxColumn9.Width = 80;

            gridViewTextBoxColumn10.HeaderText = "Prestaci�n solicitada";
            gridViewTextBoxColumn10.Name = "Num Reg";
            gridViewTextBoxColumn10.WrapText = true;
            gridViewTextBoxColumn10.Width = 80;

            gridViewTextBoxColumn11.HeaderText = "Servicio solicitado";
            gridViewTextBoxColumn11.Name = "column11";
            gridViewTextBoxColumn11.WrapText = true;
            gridViewTextBoxColumn11.Width = 80;

            gridViewTextBoxColumn12.HeaderText = "Responsable solicitado";
            gridViewTextBoxColumn12.Name = "column12";
            gridViewTextBoxColumn12.WrapText = true;
            gridViewTextBoxColumn12.Width = 80;

            gridViewTextBoxColumn13.HeaderText = "Sala consulta solicitada";
            gridViewTextBoxColumn13.Name = "column13";
            gridViewTextBoxColumn13.WrapText = true;
            gridViewTextBoxColumn13.Width = 80;

            gridViewTextBoxColumn14.HeaderText = "A�o registro";
            gridViewTextBoxColumn14.Name = "column14";
            gridViewTextBoxColumn14.Width = 80;

            gridViewTextBoxColumn15.HeaderText = "N�mero registro";
            gridViewTextBoxColumn15.Name = "column15";
            gridViewTextBoxColumn15.WrapText = true;
            gridViewTextBoxColumn15.Width = 80;

            gridViewTextBoxColumn16.HeaderText = "Sociedad";
            gridViewTextBoxColumn16.Name = "column16";
            gridViewTextBoxColumn16.Width = 80;

            gridViewTextBoxColumn17.HeaderText = "Q";
            gridViewTextBoxColumn17.Name = "column17";
            gridViewTextBoxColumn17.Width = 80;

            this.sprSolicitudes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[]
            {   gridViewTextBoxColumn1,
                gridViewTextBoxColumn2,
                gridViewTextBoxColumn3,
                gridViewTextBoxColumn4,
                gridViewTextBoxColumn5,
                gridViewTextBoxColumn6,
                gridViewTextBoxColumn7,
                gridViewTextBoxColumn8,
                gridViewTextBoxColumn9,
                gridViewTextBoxColumn10,
                gridViewTextBoxColumn11,
                gridViewTextBoxColumn12,
                gridViewTextBoxColumn13,
                gridViewTextBoxColumn14,
                gridViewTextBoxColumn15,
                gridViewTextBoxColumn16,
                gridViewTextBoxColumn17
            });
            this.sprSolicitudes.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.None;
            this.sprSolicitudes.MasterTemplate.EnableSorting = false;
            this.sprSolicitudes.MasterTemplate.ReadOnly = true;
            this.sprSolicitudes.Location = new System.Drawing.Point(8, 78);
			this.sprSolicitudes.Name = "sprSolicitudes";
			this.sprSolicitudes.Size = new System.Drawing.Size(545, 225);
			this.sprSolicitudes.TabIndex = 0;
            this.sprSolicitudes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprSolicitudes_CellClick);
            this.sprSolicitudes.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprSolicitudes_CellDoubleClick);
            this.sprSolicitudes.MouseDown += new System.Windows.Forms.MouseEventHandler(sprSolicitudes_MouseDown);
            // 
            // cbCancelar
            // 
            this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(472, 318);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 32);
			this.cbCancelar.TabIndex = 3;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Location = new System.Drawing.Point(374, 318);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 32);
			this.cbAceptar.TabIndex = 2;
			this.cbAceptar.Text = "&Justificantes";
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// CbImpimir
			// 
			this.CbImpimir.Cursor = System.Windows.Forms.Cursors.Default;
			this.CbImpimir.Location = new System.Drawing.Point(280, 318);
			this.CbImpimir.Name = "CbImpimir";
			this.CbImpimir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.CbImpimir.Size = new System.Drawing.Size(81, 32);
			this.CbImpimir.TabIndex = 1;
			this.CbImpimir.Text = "&Tratamientos";
			this.CbImpimir.Click += new System.EventHandler(this.CbImpimir_Click);
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(8, 15);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(49, 17);
			this.Label1.TabIndex = 13;
			this.Label1.Text = "Paciente:";
			// 
			// Label23
			// 
			this.Label23.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label23.Location = new System.Drawing.Point(416, 15);
			this.Label23.Name = "Label23";
			this.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label23.Size = new System.Drawing.Size(65, 17);
			this.Label23.TabIndex = 12;
			this.Label23.Text = "Hist. cl�nica:";
            // 
            // lbNombre
            // 
            this.lbNombre.Enabled = false;
			this.lbNombre.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbNombre.Location = new System.Drawing.Point(64, 14);
			this.lbNombre.Name = "lbNombre";
			this.lbNombre.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbNombre.Size = new System.Drawing.Size(337, 19);
			this.lbNombre.TabIndex = 11;
            // 
            // lbHistoria
            // 
            this.lbHistoria.Enabled = false;
			this.lbHistoria.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbHistoria.Location = new System.Drawing.Point(480, 14);
			this.lbHistoria.Name = "lbHistoria";
			this.lbHistoria.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbHistoria.Size = new System.Drawing.Size(73, 19);
			this.lbHistoria.TabIndex = 10;
			// 
			// Label2
			// 
			this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label2.Location = new System.Drawing.Point(8, 46);
			this.Label2.Name = "Label2";
			this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label2.Size = new System.Drawing.Size(49, 17);
			this.Label2.TabIndex = 9;
			this.Label2.Text = "Tel�fonos:";
            // 
            // lbTelefono1
            // 
            this.lbTelefono1.Enabled = false;
			this.lbTelefono1.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbTelefono1.Location = new System.Drawing.Point(64, 45);
			this.lbTelefono1.Name = "lbTelefono1";
			this.lbTelefono1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbTelefono1.Size = new System.Drawing.Size(73, 19);
			this.lbTelefono1.TabIndex = 8;
            // 
            // lbExtension1
            // 
            this.lbExtension1.Enabled = false;
			this.lbExtension1.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbExtension1.Location = new System.Drawing.Point(144, 45);
			this.lbExtension1.Name = "lbExtension1";
			this.lbExtension1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbExtension1.Size = new System.Drawing.Size(41, 19);
			this.lbExtension1.TabIndex = 7;
			// 
			// Label3
			// 
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.Location = new System.Drawing.Point(200, 46);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(9, 17);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "/";
            // 
            // lbTelefono2
            // 
            this.lbTelefono2.Enabled = false;
			this.lbTelefono2.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbTelefono2.Location = new System.Drawing.Point(216, 45);
			this.lbTelefono2.Name = "lbTelefono2";
			this.lbTelefono2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbTelefono2.Size = new System.Drawing.Size(73, 19);
			this.lbTelefono2.TabIndex = 5;
            // 
            // lbExtension2
            // 
            this.lbExtension2.Enabled = false;
			this.lbExtension2.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbExtension2.Location = new System.Drawing.Point(296, 45);
			this.lbExtension2.Name = "lbExtension2";
			this.lbExtension2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lbExtension2.Size = new System.Drawing.Size(41, 19);
			this.lbExtension2.TabIndex = 4;
			// 
			// CCS820F1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(571, 364);
			this.Controls.Add(this.sprSolicitudes);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.CbImpimir);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.Label23);
			this.Controls.Add(this.lbNombre);
			this.Controls.Add(this.lbHistoria);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.lbTelefono1);
			this.Controls.Add(this.lbExtension1);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.lbTelefono2);
			this.Controls.Add(this.lbExtension2);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CCS820F1";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Selecci�n de solicitud de cita - CCS820F1";
			this.Activated += new System.EventHandler(this.CCS820F1_Activated);
			this.Closed += new System.EventHandler(this.CCS820F1_Closed);
			this.Load += new System.EventHandler(this.CCS820F1_Load);
			this.ResumeLayout(false);
		}
		#endregion
	}
}