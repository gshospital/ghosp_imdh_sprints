using System;
using System.Data.SqlClient;
using ElementosCompartidos;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	public class MCSelEpisodio
	{


		public MCSelEpisodio()
		{
			BasSelEpisodio.claseMensaje = new Mensajes.ClassMensajes();
		}

		public void Llamada(object Objeto, SqlConnection Conexion, string stPaciente, string usuario, string TipoLlamada = "", string Paraiotrprof = "", string stParaAccion = "", string stAlta = "", bool inforUrg = false)
		{
			BasSelEpisodio.rcConexion = Conexion;

			//' O.Frias - 20/05/2011
			//' Para saber si viene desde urgencias o no
			BasSelEpisodio.vblnInformeUrgencia = inforUrg;

			BasSelEpisodio.gstIdenPaciente = stPaciente;
			BasSelEpisodio.gUsuario = usuario;

			BasSelEpisodio.iotrprof = Paraiotrprof;
			BasSelEpisodio.stEpiAlta = stAlta;
			switch(BasSelEpisodio.iotrprof)
			{
				case "P" :  
					BasSelEpisodio.stAgrupacion = Serrores.ObternerValor_CTEGLOBAL(BasSelEpisodio.rcConexion, "GRUPPSIC", "VALFANU1"); 
					break;
				case "T" :  
					BasSelEpisodio.stAgrupacion = Serrores.ObternerValor_CTEGLOBAL(BasSelEpisodio.rcConexion, "GRUPTEOC", "VALFANU1"); 
					break;
			}

			Serrores.vNomAplicacion = Serrores.ObtenerNombreAplicacion(BasSelEpisodio.rcConexion);

			if (!true)
			{
				BasSelEpisodio.stTipoLlamada = "";
			}
			else
			{
				BasSelEpisodio.stTipoLlamada = TipoLlamada;
			}

			BasSelEpisodio.Llamador = Objeto;

			switch(BasSelEpisodio.stTipoLlamada)
			{
				case "O" :
                    MDO230F1.DefInstance.ShowDialog(); 
                    break;
				case "CO" :  
					CAC590F2.DefInstance.ShowDialog(); 
					break;
				case "CI" :
                    CCI341F1.DefInstance.ShowDialog(); 
                    break;
				case "SE" : 
                    CCS820F1.DefInstance.Recibir_Parametros(stPaciente, stParaAccion); 
                    break;
				case "LESE" :  
                    CCS820F2.DefInstance.CargarDatos(); 
                    break;
				case "PP-PSOE" :  
                    MDO221F1.DefInstance.ShowDialog(); 
                    break;
				case "LEQ" :  
                    LEQ001F1.DefInstance.Recibir_Parametros(stPaciente, stParaAccion); 
                   break;
				default:
                   MDO220F1.DefInstance.ShowDialog(); 
                   break;
			}
		}

		~MCSelEpisodio()
		{
			BasSelEpisodio.claseMensaje = null;
		}
	}
}