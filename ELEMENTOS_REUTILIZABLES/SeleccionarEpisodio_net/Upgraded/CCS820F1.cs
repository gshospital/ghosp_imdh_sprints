using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using UpgradeHelpers.Gui;
using UpgradeStubs;
using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;
using ElementosCompartidos;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace SelEpisodio
{
	public partial class CCS820F1
		: Telerik.WinControls.UI.RadForm
    {

		string stsql = String.Empty;
		string stPaciente = String.Empty;
		string stAccion = String.Empty;
		bool bErrorProcesoComprobar = false;
		string stDescripcionProcesoComprobar = String.Empty;
		bool Seleccionado = false;
		int FilaGridSeleccionada = 0;
        private bool _isMouseLeftDown = false;
        /*INDRA JPROCHE TODO_X_4 CONTIENE CODIGO CRYSTAL
        Crystal.CrystalReport listado_local = null;
        */

        //Procedimiento que recoge las incidencias de la Dll de ProcesoComprobar.
        //Hay que definir bErroAsociarProcesor y stDescripcionAsociarProceso en el general.
        public CCS820F1()
			: base()
		{
			if (m_vb6FormDefInstance == null)
			{
				if (m_InitializingDefInstance)
				{
					m_vb6FormDefInstance = this;
				}
				else
				{
					try
					{
						//For the start-up form, the first instance created is the default instance.
						if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
						{
							m_vb6FormDefInstance = this;
						}
					}
					catch
					{
					}
				}
			}
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}


		public void DevolverAsociarProceso(string stError, string stDescripcionError)
		{
			if (stError.Trim() != "")
			{
				bErrorProcesoComprobar = true;
				stDescripcionProcesoComprobar = stDescripcionError;
			}
		}

		private void CbImpimir_Click(Object eventSender, EventArgs eventArgs)
		{
			pro_Imprimir();
		}

		public void pro_Imprimir()
		{
			int Anoregi = 0;
			int Numregi = 0;
			string sql = String.Empty;
			string CodPaciente = String.Empty;



			string stNombreDsn = String.Empty;
			string stNombreDsnACCESS = String.Empty;

			Serrores.CrearCadenaDSN(ref stNombreDsn, ref stNombreDsnACCESS);

			Serrores.Datos_Conexion(BasSelEpisodio.rcConexion);
			string vstUsuarioImpresion = Serrores.VVstUsuarioBD;
			string vstContraseñaImpresion = Serrores.VVstPasswordBD;

            //borrar tabla si la tuviera sino crearla
            //rellenar la tabla con los datos del grid
            //imprimir
            /*INDRA JPROCHE TODO_X_4 CONTIENE CODIGO CRYSTAL 03/05/2016
			listado_local.ReportFileName = Path.GetDirectoryName(Application.ExecutablePath) + "\\RPT\\CCS892R4.rpt";
            */
			if (Seleccionado)
			{
				sprSolicitudes.Row = FilaGridSeleccionada;
				sprSolicitudes.Col = 14;
				Anoregi = Convert.ToInt32(Double.Parse(sprSolicitudes.Text));
				sprSolicitudes.Col = 15;
				Numregi = Convert.ToInt32(Double.Parse(sprSolicitudes.Text));
				sprSolicitudes.Col = 17;
				CodPaciente = sprSolicitudes.Text;

				sql = "select * from ((((((csolcita  inner join dpacient  on " + " csolcita.gidenpac=dpacient.gidenpac) inner join csesione on " + " csolcita.ganoregi=csesione.ganoregi and csolcita.gnumregi=csesione.gnumregi) inner join dsocieda on " + " csolcita.gsocieda=dsocieda.gsocieda) left join dpoblaci on " + " dpoblaci.gpoblaci=dpacient.gpoblaci) left join hdossier on " + " hdossier.gidenpac=csolcita.gidenpac) left join dcodpres on " + " dcodpres.gprestac=csolcita.gprestac) " + " Where csolcita.ganoregi=" + Anoregi.ToString() + " and csolcita.gnumregi=" + Numregi.ToString() + " and ((dcodpres.finivali<=csolcita.fsolicit) and (dcodpres.ffinvali>=csolcita.fsolicit or dcodpres.ffinvali is null))";

			}
			else
			{
				sprSolicitudes.Col = 17;
				CodPaciente = sprSolicitudes.Text;
				sql = "select * from ((((((csolcita  inner join dpacient  on " + " csolcita.gidenpac=dpacient.gidenpac) inner join csesione on " + " csolcita.ganoregi=csesione.ganoregi and csolcita.gnumregi=csesione.gnumregi) inner join dsocieda on " + " csolcita.gsocieda=dsocieda.gsocieda) left join dpoblaci on " + " dpoblaci.gpoblaci=dpacient.gpoblaci) left join hdossier on " + " hdossier.gidenpac=csolcita.gidenpac) left join dcodpres on " + " dcodpres.gprestac=csolcita.gprestac) " + " Where csolcita.gidenpac='" + CodPaciente + "' and " + " ((dcodpres.finivali<=csolcita.fsolicit) and (dcodpres.ffinvali>=csolcita.fsolicit or dcodpres.ffinvali is null))";
			}
			//en ambos casos de debe tener en cuenta si se ha accedido desde el modulo de
			//otros profesionales - terapia ocupacional para realizar el filtrado correspondiente
			if (BasSelEpisodio.iotrprof == "T")
			{
				stsql = stsql + " AND Csolcita.gservici IN (Select gservici from dservici where ggrupser ='" + BasSelEpisodio.stAgrupacion + "')";
			}

			sql = sql + "\r" + "\n" + " order by dape1pac,dape2pac,dnombpac";

			string cabecera = "LISTADO DE LOS TRATAMIENTOS DEL PACIENTE";
			BasSelEpisodio.proCabCrystal(); //llena las 4 propiedades de vstrCrystal con datos de la cabecera.

            /*INDRA JPROCHE TODO_X_4 CONTIENE CODIGO CRYSTAL 03/05/2016
			listado_local.WindowTitle = cabecera;
			listado_local.WindowState = Crystal.WindowStateConstants.crptMaximized;
			listado_local.Destination = Crystal.DestinationConstants.crptToWindow; //crptToWindow
			listado_local.set_Formulas(0, "FORM1= \"" + BasSelEpisodio.VstrCrystal.VstGrupoHo + "\"");
			listado_local.set_Formulas(1, "FORM2= \"" + BasSelEpisodio.VstrCrystal.VstNombreHo + "\"");
			listado_local.set_Formulas(2, "FORM3= \"" + BasSelEpisodio.VstrCrystal.VstDireccHo + "\"");
			listado_local.set_Formulas(3, "cabecera= \"" + cabecera + "\"");
			listado_local.set_ParameterFields(0, "fechainicio;DATE(1900,01,01);TRUE");
			listado_local.set_ParameterFields(1, "fechaFIN;DATE(3000,01,01);TRUE");

			listado_local.Connect = "DSN=" + stNombreDsn + ";UID=" + vstUsuarioImpresion + ";PWD=" + vstContraseñaImpresion;
			listado_local.SubreportToChange = "PRESTACIONES";
			listado_local.Connect = "DSN=" + stNombreDsn + ";UID=" + vstUsuarioImpresion + ";PWD=" + vstContraseñaImpresion;
			listado_local.SubreportToChange = "";

			listado_local.SQLQuery = sql;
			listado_local.WindowShowPrintSetupBtn = true;
			listado_local.Action = 1;
			listado_local.PageCount();
			listado_local.PrinterStopPage = listado_local.PageCount();
			BasSelEpisodio.vacia_formulas(listado_local, 3);
			listado_local.Reset();
            */
		}

		private void CCS820F1_Activated(Object eventSender, EventArgs eventArgs)
		{
			if (ActivateHelper.myActiveForm != eventSender)
			{
				ActivateHelper.myActiveForm = (Form) eventSender;
				sprSolicitudes.Col = 0;
				sprSolicitudes.Col2 = sprSolicitudes.MaxCols;
				sprSolicitudes.Row = 0;
				sprSolicitudes.Row2 = sprSolicitudes.MaxRows;
				sprSolicitudes.BlockMode = true;
				sprSolicitudes.Lock = true;
				sprSolicitudes.BlockMode = false;

				//oscar 20/06/03
				CbImpimir.Visible = stAccion != "CAMBIARENTIDAD";
				//-------
			}
		}

		private void CCS820F1_Load(Object eventSender, EventArgs eventArgs)
		{

			this.Cursor = Cursors.WaitCursor;

			Application.DoEvents();
			this.Top = (int) ((Screen.PrimaryScreen.Bounds.Height - this.Height) / 2);
			this.Left = (int) ((Screen.PrimaryScreen.Bounds.Width - this.Width) / 2);

			CDAyuda.Instance.setHelpFile(this, CDAyuda.FICHERO_AYUDA_OTRASDLLS);
			sprSolicitudes.Row = -1;

			for (int i = 14; i <= 16; i++)
			{
				sprSolicitudes.Col = i;
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
			}
			Seleccionado = true;
			FilaGridSeleccionada = 1;
			this.Cursor = Cursors.Default;

            /*INDRA JPROCHE TODO_X_4 CONTIENE CODIGO CRYSTAL 03/05/2016
			listado_local = new Crystal.CrystalReport();
            */

		}

		public void Recibir_Parametros(string Paciente, string Accion)
		{

			this.Cursor = Cursors.WaitCursor;

			stPaciente = Paciente;
			stAccion = Accion;

			if (stAccion == "CONSULTARTRATAMIENTOS")
			{
				this.Text = "Consulta de tratamientos de un paciente - CCS820F1";
				cbAceptar.Enabled = false;
			}
			else
			{
				cbAceptar.Enabled = true;
			}

			sprSolicitudes.Row = -1;
			// Se ocultan las columnas de la lista que no se deben visualizar
			// si se trata de un cambio o borrado de solicitud
			if (stAccion == "CAMBIARSOLICITUD" || stAccion == "BORRARSOLICITUD" || stAccion == "PROGRAMARSOLICITUD")
			{
				for (int i = 7; i <= 9; i++)
				{
					sprSolicitudes.Col = i;
					sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
				}
			}

			// Se obtienen los datos del paciente
			Datos_Paciente();

			// Se obtienen las solicitudes de cita del paciente
			Cargar_Solicitudes_Cita();

			this.Cursor = Cursors.Default;

			// Si el paciente no tiene solicitudes de cita se muestra un mensaje
			// y se termina salvo que se este programando una solicitud en cuyo
			// caso se continua
			if (sprSolicitudes.MaxRows == 0)
			{
				BasSelEpisodio.claseMensaje.RespuestaMensaje(Serrores.vNomAplicacion, Serrores.vAyuda, 1520, BasSelEpisodio.rcConexion, "solicitudes de cita", "el paciente seleccionado");
				//      If stAccion = "PROGRAMARSOLICITUD" Then
				//         CCS830F1.Show
				//      End If
				cbCancelar_Click(cbCancelar, new EventArgs());
			}
			else
			{
				sprSolicitudes.Row = 1;
				CCS820F1.DefInstance.ShowDialog();
			}
		}

		private void Datos_Paciente()
		{

			stsql = "SELECT gserarch " + 
			        "FROM hserarch " + 
			        "WHERE icentral = 'S'";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsCentral = new DataSet();
			tempAdapter.Fill(rsCentral);

			//*********************** O.Frias (SQL-2005) ***********************

			//UPGRADE_WARNING: (2077) Change the default 0 index in the Rows property with the correct one. More Information: http://www.vbtonet.com/ewis/ewi2077.aspx
			stsql = "SELECT ISNULL(DPACIENT.dnombpac, '') AS dnombpac, ISNULL(DPACIENT.dape1pac, '') AS dape1pac, " + 
			        "ISNULL(DPACIENT.dape2pac, '') AS dape2pac, DPACIENT.ntelefo1, DPACIENT.nextesi1 , " + 
			        "DPACIENT.ntelefo2, DPACIENT.nextesi2, HDOSSIER.ghistoria " + 
			        "FROM DPACIENT " + 
			        "LEFT OUTER JOIN HDOSSIER ON DPACIENT.gidenpac = HDOSSIER.gidenpac AND " + 
			        "HDOSSIER.icontenido = 'H' AND " + 
			        "HDOSSIER.gserpropiet = " + Convert.ToString(rsCentral.Tables[0].Rows[0]["gserarch"]) + " " + 
			        "WHERE (DPACIENT.gidenpac = '" + stPaciente + "') ";

			//*********************** O.Frias (SQL-2005) ***********************

			SqlDataAdapter tempAdapter_2 = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsPaciente = new DataSet();
			tempAdapter_2.Fill(rsPaciente);

			lbNombre.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["dnombpac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape1pac"]).Trim() + " " + Convert.ToString(rsPaciente.Tables[0].Rows[0]["dape2pac"]).Trim();
			if (!Convert.IsDBNull(rsPaciente.Tables[0].Rows[0]["ntelefo1"]))
			{
				lbTelefono1.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["ntelefo1"]).Trim();
			}
			if (!Convert.IsDBNull(rsPaciente.Tables[0].Rows[0]["nextesi1"]))
			{
				lbExtension1.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["nextesi1"]).Trim();
			}
			if (!Convert.IsDBNull(rsPaciente.Tables[0].Rows[0]["ntelefo2"]))
			{
				lbTelefono2.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["ntelefo2"]).Trim();
			}
			if (!Convert.IsDBNull(rsPaciente.Tables[0].Rows[0]["nextesi2"]))
			{
				lbExtension2.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["nextesi2"]).Trim();
			}
			
			if (!Convert.IsDBNull(rsPaciente.Tables[0].Rows[0]["ghistoria"]))
			{
				lbHistoria.Text = Convert.ToString(rsPaciente.Tables[0].Rows[0]["ghistoria"]).Trim();
			}
			rsPaciente.Close();
			rsCentral.Close();
		}

		private void Cargar_Solicitudes_Cita()
		{

			sprSolicitudes.MaxRows = 0;



			//*********************** O.Frias (SQL-2005) ***********************


			string stsql = "SELECT CSOLCITA.*, DSOCIEDA.dsocieda, DCODPRES.dprestac, DSERVICI.dnomserv, " + 
			               "ISNULL(DPERSONA.dap1pers, '') AS dap1pers, ISNULL(DPERSONA.dap2pers, '') AS dap2pers, " + 
			               "ISNULL(DPERSONA.dnompers, '') AS dnompers, dsalacon.dsalacon " + 
			               "From CSOLCITA " + 
			               "LEFT OUTER JOIN DSOCIEDA ON CSOLCITA.gsocieda = DSOCIEDA.gsocieda " + 
			               "INNER JOIN DCODPRES ON CSOLCITA.gprestac = DCODPRES.gprestac " + 
			               "LEFT OUTER JOIN DSERVICI ON CSOLCITA.gservici = DSERVICI.gservici " + 
			               "LEFT OUTER JOIN DPERSONA ON CSOLCITA.gpersona = DPERSONA.gpersona " + 
			               "LEFT OUTER JOIN DSALACON ON CSOLCITA.gsalacon = DSALACON.gsalacon " + 
			               "WHERE (CSOLCITA.gidenpac = '" + stPaciente + "' ) ";

			//*********************** O.Frias (SQL-2005) ***********************


			// Si se va a realizar un cambio o borrado de solicitud se seleccionan
			// aquellas que tengan como estado pendiente de programación
			if (stAccion == "CAMBIARSOLICITUD" || stAccion == "BORRARSOLICITUD" || stAccion == "PROGRAMARSOLICITUD")
			{
				stsql = stsql + " AND ( csolcita.iesolcit = 'N' ) ";
			}
			else
			{
				//***************** O.Fias 17/07/2007 ********************
				if (stAccion == "CONFIRMARASISTENCIA")
				{ //'''' Or stAccion = "JUSTIFICANTE" Then
					stsql = stsql + " AND " + 
					        "(csolcita.iesolcit = 'P' OR csolcita.iesolcit = 'T') ";
				}
				else
				{
					if (stAccion == "CAMBIARPROGRAMACION")
					{
						stsql = stsql + " AND (csolcita.iesolcit = 'P' OR csolcita.iesolcit = 'T' OR csolcita.iesolcit = 'C') ";
						//***************** O.Fias 17/07/2007 ********************
					}
					else if (stAccion == "JUSTIFICANTE" || stAccion == "JUSTIFICA_2")
					{ 
						stsql = stsql + " AND (csolcita.iesolcit = 'P' OR csolcita.iesolcit = 'T' OR csolcita.iesolcit = 'F' ) ";

					}
				}
			}

			//Si se accede desde Otros Profesionales - TERAPIA OCUPACIONAL, solo se
			//mostraran los de la agrupacion correspondiente.
			if (BasSelEpisodio.iotrprof == "T")
			{
				stsql = stsql + " AND Csolcita.gservici IN (Select gservici from dservici where ggrupser ='" + BasSelEpisodio.stAgrupacion + "') ";
			}

			stsql = stsql + "ORDER BY fsolicit ";
			SqlDataAdapter tempAdapter = new SqlDataAdapter(stsql, BasSelEpisodio.rcConexion);
			DataSet rsSolicitudes = new DataSet();
			tempAdapter.Fill(rsSolicitudes);

			foreach (DataRow iteration_row in rsSolicitudes.Tables[0].Rows)
			{
                sprSolicitudes.MaxRows++;
				sprSolicitudes.Row = sprSolicitudes.MaxRows;
				if (!Convert.IsDBNull(iteration_row["dsocieda"]))
				{
					sprSolicitudes.Col = 1;
					sprSolicitudes.Text = Convert.ToString(iteration_row["dsocieda"]);
				}
				if (!Convert.IsDBNull(iteration_row["nafiliac"]))
				{
					sprSolicitudes.Col = 2;
					sprSolicitudes.Text = Convert.ToString(iteration_row["nafiliac"]);
				}
				sprSolicitudes.Col = 3;
				switch(Convert.ToString(iteration_row["iorigpac"]))
				{
					case "H" : 
						sprSolicitudes.Text = "HOSPITALIZACION"; 
						break;
					case "C" : 
						sprSolicitudes.Text = "CONSULTA DEL HOSPITAL"; 
						break;
					case "O" : 
						sprSolicitudes.Text = "OTRO HOSPITAL"; 
						break;
				}
				sprSolicitudes.Col = 4;
				switch(Convert.ToString(iteration_row["iturnomt"]))
				{
					case "M" : 
						sprSolicitudes.Text = "MAÑANA"; 
						break;
					case "T" : 
						sprSolicitudes.Text = "TARDE"; 
						break;
				}
				sprSolicitudes.Col = 5;
				if (!Convert.IsDBNull(iteration_row["nsesione"]))
				{
					sprSolicitudes.Text = Conversion.Str(iteration_row["nsesione"]);
				}
				sprSolicitudes.Col = 6;
				sprSolicitudes.Text = Convert.ToDateTime(iteration_row["fsolicit"]).ToString("dd/MM/yyyy");
				sprSolicitudes.Col = 7;
				sprSolicitudes.Text = Convert.ToDateTime(iteration_row["finitrat"]).ToString("dd/MM/yyyy");
				sprSolicitudes.Col = 8;
				sprSolicitudes.Text = Convert.ToDateTime(iteration_row["fproxses"]).ToString("dd/MM/yyyy");
				sprSolicitudes.Col = 9;
				sprSolicitudes.Text = Convert.ToDateTime(iteration_row["ffintrat"]).ToString("dd/MM/yyyy");
				sprSolicitudes.Col = 10;
				sprSolicitudes.Text = Convert.ToString(iteration_row["dprestac"]);
				sprSolicitudes.Col = 11;
				if (!Convert.IsDBNull(iteration_row["dnomserv"]))
				{
					sprSolicitudes.Text = Convert.ToString(iteration_row["dnomserv"]);
				}
				sprSolicitudes.Col = 12;
				if (!Convert.IsDBNull(iteration_row["dap1pers"]))
				{
					sprSolicitudes.Text = Convert.ToString(iteration_row["dap1pers"]).Trim() + " " + Convert.ToString(iteration_row["dap2pers"]).Trim() + ", " + Convert.ToString(iteration_row["dnompers"]).Trim();
				}
				sprSolicitudes.Col = 13;
				if (!Convert.IsDBNull(iteration_row["dsalacon"]))
				{
					sprSolicitudes.Text = Convert.ToString(iteration_row["dsalacon"]);
				}
				sprSolicitudes.Col = 14;
				sprSolicitudes.Text = Convert.ToString(iteration_row["ganoregi"]);
				sprSolicitudes.Col = 15;
				sprSolicitudes.Text = Convert.ToString(iteration_row["gnumregi"]);
				sprSolicitudes.Col = 16;
				if (!Convert.IsDBNull(iteration_row["gsocieda"]))
				{
					sprSolicitudes.Text = Convert.ToString(iteration_row["gsocieda"]);
				}
				sprSolicitudes.Col = 17;
				sprSolicitudes.SetColHidden(sprSolicitudes.Col, true);
				sprSolicitudes.Text = Convert.ToString(iteration_row["gidenpac"]);
			}

		}


		private void CCS820F1_Closed(Object eventSender, EventArgs eventArgs)
		{
            /*INDRA JPROCHE TODO_X_4 CONTIENE CODIGO CRYSTAL 03/05/2016
			listado_local = null;
            */
        }

        private void sprSolicitudes_CellClick(object eventSender, GridViewCellEventArgs eventArgs)
		{
            
            if (_isMouseLeftDown)
            {
                int Col = eventArgs.ColumnIndex;
				int Row = eventArgs.RowIndex + 1;

				if (Row > 0)
				{

					if (FilaGridSeleccionada == Row)
					{
                        sprSolicitudes.CurrentRow = null;
                        FilaGridSeleccionada = 0;
						Seleccionado = false;
					}
					else
					{
                        FilaGridSeleccionada = Row;
						Seleccionado = true;
					}
					sprSolicitudes.Row = Row;

				}

				//oscar 20/06/03
				if (stAccion == "CAMBIARENTIDAD")
				{
					cbAceptar.Enabled = FilaGridSeleccionada != 0;
				}
				//-------

            }
		}

        private void sprSolicitudes_CellDoubleClick(object eventSender, GridViewCellEventArgs eventArgs)
		{
			int Col = eventArgs.ColumnIndex + 1;
			int Row = eventArgs.RowIndex + 1;

			//oscar 20/06/03
			if (stAccion == "CAMBIARENTIDAD")
			{
				return;
			}
			//-------

			if (Row > 0)
			{
				if (stAccion == "CONSULTARTRATAMIENTOS")
				{
					CbImpimir_Click(CbImpimir, new EventArgs());
				}
				else
				{
					cbAceptar_Click(cbAceptar, new EventArgs());
				}
			}

		}

		private void cbAceptar_Click(Object eventSender, EventArgs eventArgs)
		{
			int iAñoRegistro = 0;
			int lNumeroRegistro = 0;
			string stSociedad = String.Empty;
			string stsql = String.Empty;
			DataSet cursor = null;
			object ClaseProceso = null;

			bool ErrorenFacturacion = false;
			try
			{
				this.Cursor = Cursors.WaitCursor;

				//oscar 20/06/03
                //INDRA JPROCHE TODO_X_4 03/05/2016
                //sprSolicitudes.Row = sprSolicitudes.ActiveSheet.ActiveRowIndex;
				//------

				sprSolicitudes.Col = 14;
				iAñoRegistro = Convert.ToInt32(Conversion.Val(sprSolicitudes.Text));
				sprSolicitudes.Col = 15;
				lNumeroRegistro = Convert.ToInt32(Conversion.Val(sprSolicitudes.Text));


				//Oscar C Marzo 2007
				// Si no descargamos la pantalla de Seleccion de Solicitud de Cita, es posible que se
				// produzca un error cuando se invoca a la funcion de IFMS de generar factura ya que no
				// puede haber ningun formulario modal cargado.
				//Unload Me
				if (stAccion == "CAMBIARSOLICITUD" || stAccion == "PROGRAMARSOLICITUD" || stAccion == "CAMBIARPROGRAMACION" || stAccion == "CONFIRMARASISTENCIA")
				{
					this.Close();
				}
				//--------

				// En funcion de la accion se muestra el formulario correspondiente

				switch(stAccion)
				{
					case "CAMBIARSOLICITUD" : 
						BasSelEpisodio.Llamador.func_proCambioSolicitud(iAñoRegistro, lNumeroRegistro); 
						 
						break;
					case "BORRARSOLICITUD" : 
						BasSelEpisodio.Llamador.func_proBorrado_Solicitud(iAñoRegistro, lNumeroRegistro); 
						 
						break;
					case "PROGRAMARSOLICITUD" : 
						BasSelEpisodio.Llamador.func_proProgramacionSolicitud(iAñoRegistro, lNumeroRegistro); 
						break;
					case "CAMBIARPROGRAMACION" : 
						BasSelEpisodio.Llamador.func_proCambioSesiones(iAñoRegistro, lNumeroRegistro); 
                        break;
					case "CONFIRMARASISTENCIA" : 
						BasSelEpisodio.Llamador.func_ConfirmacionAsistencia(iAñoRegistro, lNumeroRegistro, stPaciente); 
                        break;
					case "JUSTIFICA_2" : 
						BasSelEpisodio.Llamador.func_ImprimeJustificanteCitaSesiones2(iAñoRegistro, lNumeroRegistro); 
						break;
					case "JUSTIFICANTE" : 
						BasSelEpisodio.Llamador.func_ImprimeJustificanteCitaSesiones(iAñoRegistro, lNumeroRegistro); 
						break;
					case "ASISTENCIA" : 
						BasSelEpisodio.Llamador.func_ImprimeJustificanteAsistencia(iAñoRegistro, lNumeroRegistro); 
						break;
					case "HOJAFISIO" : 
						BasSelEpisodio.Llamador.func_ImprimeHojaFisio(stPaciente, iAñoRegistro, lNumeroRegistro); 
                        break;
					case "CAMBIARENTIDAD" : 
						if (FilaGridSeleccionada == 0)
						{
							return;
						} 
						 
						BasSelEpisodio.Llamador.func_proCambioEntidad(iAñoRegistro, lNumeroRegistro, stPaciente, true); 
						Cargar_Solicitudes_Cita(); 
						break;
				}

				this.Cursor = Cursors.Default;

				//oscar 19/03/03
				//Unload Me
				if (stAccion != "CAMBIARENTIDAD")
				{
					this.Close();
				}
				//--------
			}
			catch (System.Exception excep)
			{
				if (ErrorenFacturacion)
				{
                    //INDRA JPROCHE TODO_X_4 03/05/2016
                    //UpgradeStubs.System_Data_SqlClient_SqlConnection.RollbackTrans();
					RadMessageBox.Show(excep.Message, Application.ProductName);
				}
			}

		}

		private void cbCancelar_Click(Object eventSender, EventArgs eventArgs)
		{
			this.Close();
		}
        private void sprSolicitudes_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _isMouseLeftDown = true;
            }
            else
            {
                _isMouseLeftDown = false;
            }
        }
    }
}
 
 
 