using VB6 = Microsoft.VisualBasic.Compatibility.VB6.Support;

namespace SelEpisodio
{
	partial class MDO230F1
	{

		#region "Upgrade Support "
		private static MDO230F1 m_vb6FormDefInstance;
		private static bool m_InitializingDefInstance;
		public static MDO230F1 DefInstance
		{
			get
			{
				if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
				{
					m_InitializingDefInstance = true;
					m_vb6FormDefInstance = new MDO230F1();
					m_InitializingDefInstance = false;
				}
				return m_vb6FormDefInstance;
			}
			set
			{
				m_vb6FormDefInstance = value;
			}
		}

		#endregion
		#region "Windows Form Designer generated code "
		private string[] visualControls = new string[]{"components", "ToolTipMain", "cbCancelar", "cbAceptar", "Label1", "lb_paciente", "Label3", "LB_Hist", "Frame1", "sprDemandas", "fraDemandas", "sprDemandas_Sheet1"};
		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components;
		public Telerik.WinControls.RadToolTip ToolTipMain;
		public Telerik.WinControls.UI.RadButton cbCancelar;
		public Telerik.WinControls.UI.RadButton cbAceptar;
		public Telerik.WinControls.UI.RadLabel Label1;
		public Telerik.WinControls.UI.RadTextBox lb_paciente;
		public Telerik.WinControls.UI.RadLabel Label3;
		public Telerik.WinControls.UI.RadTextBox LB_Hist;
		public Telerik.WinControls.UI.RadGroupBox Frame1;
		public UpgradeHelpers.Spread.FpSpread sprDemandas;
		public Telerik.WinControls.UI.RadGroupBox fraDemandas;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDO230F1));
			this.ToolTipMain = new Telerik.WinControls.RadToolTip(this.components);
			this.cbCancelar = new Telerik.WinControls.UI.RadButton();
			this.cbAceptar = new Telerik.WinControls.UI.RadButton();
			this.Frame1 = new Telerik.WinControls.UI.RadGroupBox();
			this.Label1 = new Telerik.WinControls.UI.RadLabel();
			this.lb_paciente = new Telerik.WinControls.UI.RadTextBox();
			this.Label3 = new Telerik.WinControls.UI.RadLabel();
			this.LB_Hist = new Telerik.WinControls.UI.RadTextBox();
			this.fraDemandas = new Telerik.WinControls.UI.RadGroupBox();
			this.sprDemandas = new UpgradeHelpers.Spread.FpSpread();
			this.Frame1.SuspendLayout();
			this.fraDemandas.SuspendLayout();
			this.SuspendLayout();
			// 
			// cbCancelar
			// 
			this.cbCancelar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbCancelar.Location = new System.Drawing.Point(654, 368);
			this.cbCancelar.Name = "cbCancelar";
			this.cbCancelar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbCancelar.Size = new System.Drawing.Size(81, 29);
			this.cbCancelar.TabIndex = 8;
			this.cbCancelar.Text = "&Cancelar";
			this.cbCancelar.Click += new System.EventHandler(this.cbCancelar_Click);
			// 
			// cbAceptar
			// 
			this.cbAceptar.Cursor = System.Windows.Forms.Cursors.Default;
			this.cbAceptar.Location = new System.Drawing.Point(564, 368);
			this.cbAceptar.Name = "cbAceptar";
			this.cbAceptar.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.cbAceptar.Size = new System.Drawing.Size(81, 29);
			this.cbAceptar.TabIndex = 7;
			this.cbAceptar.Text = "Aceptar";
			
			this.cbAceptar.Click += new System.EventHandler(this.cbAceptar_Click);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Controls.Add(this.lb_paciente);
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.LB_Hist);
			this.Frame1.Enabled = true;
			this.Frame1.Location = new System.Drawing.Point(0, 4);
			this.Frame1.Name = "Frame1";
			this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Frame1.Size = new System.Drawing.Size(573, 47);
			this.Frame1.TabIndex = 2;
			this.Frame1.Text = "Datos del paciente";
			this.Frame1.Visible = true;
			// 
			// Label1
			// 
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(11, 19);
			this.Label1.Name = "Label1";
			this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label1.Size = new System.Drawing.Size(52, 17);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "Paciente:";
            // 
            // lb_paciente
            // 
            this.lb_paciente.Enabled = false;
			this.lb_paciente.Cursor = System.Windows.Forms.Cursors.Default;
			this.lb_paciente.Location = new System.Drawing.Point(72, 17);
			this.lb_paciente.Name = "lb_paciente";
			this.lb_paciente.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lb_paciente.Size = new System.Drawing.Size(296, 19);
			this.lb_paciente.TabIndex = 5;
			// 
			// Label3
			// 
			this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label3.Location = new System.Drawing.Point(390, 19);
			this.Label3.Name = "Label3";
			this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Label3.Size = new System.Drawing.Size(65, 16);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "Hist. cl�nica:";
            // 
            // LB_Hist
            // 
            this.LB_Hist.Enabled = false;
			this.LB_Hist.Cursor = System.Windows.Forms.Cursors.Default;
			this.LB_Hist.Location = new System.Drawing.Point(463, 17);
			this.LB_Hist.Name = "LB_Hist";
			this.LB_Hist.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.LB_Hist.Size = new System.Drawing.Size(84, 19);
			this.LB_Hist.TabIndex = 3;
			// 
			// fraDemandas
			// 
			this.fraDemandas.Controls.Add(this.sprDemandas);
			this.fraDemandas.Enabled = true;
			this.fraDemandas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			this.fraDemandas.Location = new System.Drawing.Point(0, 54);
			this.fraDemandas.Name = "fraDemandas";
			this.fraDemandas.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.fraDemandas.Size = new System.Drawing.Size(735, 307);
			this.fraDemandas.TabIndex = 0;
			this.fraDemandas.Text = "Demandas";
			this.fraDemandas.Visible = true;
            // 
            // sprDemandas
            // 
            this.sprDemandas.MasterTemplate.EnableSorting = false;
            this.sprDemandas.MasterTemplate.ReadOnly = true;
            this.sprDemandas.Location = new System.Drawing.Point(8, 22);
			this.sprDemandas.Name = "sprDemandas";
			this.sprDemandas.Size = new System.Drawing.Size(721, 277);
			this.sprDemandas.TabIndex = 1;
            this.sprDemandas.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.sprDemandas_CellClick);
            // 
			// MDO230F1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6, 13);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(737, 401);
			this.Controls.Add(this.cbCancelar);
			this.Controls.Add(this.cbAceptar);
			this.Controls.Add(this.Frame1);
			this.Controls.Add(this.fraDemandas);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MDO230F1";
			this.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Text = "Selecci�n de Demandas - MDO230F1";
			this.Closed += new System.EventHandler(this.MDO230F1_Closed);
			this.Load += new System.EventHandler(this.MDO230F1_Load);
			this.Frame1.ResumeLayout(false);
			this.fraDemandas.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}